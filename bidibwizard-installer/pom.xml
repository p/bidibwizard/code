<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>org.bidib.jbidib</groupId>
        <artifactId>wizard</artifactId>
        <version>1.10-SNAPSHOT</version>
    </parent>
    <artifactId>bidibwizard-installer</artifactId>
    <packaging>jar</packaging>
    <name>jBiDiB :: BiDiB Wizard Installer</name>
    <description>jBiDiB BiDiB Wizard Installer POM</description>
    <properties>
        <projectname>BiDiB-Wizard</projectname>
        <projectversion>${project.version}</projectversion>
        <projectartifact>bidibwizard</projectartifact>
        <main-class>org.bidib.wizard.main.BiDiBWizard</main-class>
        <spy-main-class>org.bidib.wizard.spy.BidibSpyLauncher</spy-main-class>
        <maven.build.timestamp.format>dd.MM.yyyy HH:mm</maven.build.timestamp.format>
        <build-timestamp>${maven.build.timestamp}</build-timestamp>
        <!-- replaced during release in separate profile -->
        <buildnumber-and-branch-info>${buildNumber}, ${git.branch}</buildnumber-and-branch-info>
    </properties>
    <dependencies>
        <dependency>
            <groupId>org.bidib.jbidib</groupId>
            <artifactId>bidibwizard</artifactId>
            <version>${project.version}</version>
        </dependency>
        <dependency>
            <groupId>org.bidib.jbidib</groupId>
            <artifactId>bidibwizard-installer-panel</artifactId>
            <version>${project.version}</version>
            <exclusions>
                <exclusion>
                    <groupId>org.codehaus.izpack</groupId>
                    <artifactId>izpack-installer</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>org.codehaus.izpack</groupId>
                    <artifactId>izpack-api</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>org.codehaus.izpack</groupId>
                    <artifactId>izpack-panel</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>org.bidib.jbidib</groupId>
            <artifactId>bidibwizard-spy</artifactId>
            <version>${project.version}</version>
        </dependency>
    </dependencies>
    <build>
        <resources>
            <resource>
                <directory>src/main/izpack</directory>
            </resource>
            <resource>
                <directory>src/main/filtered-resources</directory>
                <filtering>true</filtering>
                <targetPath>${izpack-staging}</targetPath>
            </resource>
        </resources>
        <plugins>
            <!-- copy izpack resources into izpack staging area, expected by izpack.xml -->
            <plugin>
                <artifactId>maven-resources-plugin</artifactId>
                <executions>
                    <execution>
                        <id>copy-izpack-resource</id>
                        <phase>validate</phase>
                        <goals>
                            <goal>copy-resources</goal>
                        </goals>
                        <configuration>
                            <outputDirectory>${izpack-staging}</outputDirectory>
                            <resources>
                                <resource>
                                    <directory>src/main/izpack</directory>
                                    <excludes>
                                        <exclude>icons/BiDiBWizardSplash.png</exclude>
                                    </excludes>
                                </resource>
                                <resource>
                                    <directory>src/main/filtered-resources</directory>
                                </resource>
                                <resource>
                                    <directory>target/classes</directory>
                                    <includes>
                                        <include>icons/*.bmp</include>
                                        <include>images/*.png</include>
                                    </includes>
                                </resource>
                            </resources>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-dependency-plugin</artifactId>
                <executions>
                    <execution>
                        <!-- copy *application dependencies* jars to izpack staging lib -->
                        <id>copy-product-dependencies</id>
                        <phase>prepare-package</phase>
                        <goals>
                            <goal>copy-dependencies</goal>
                        </goals>
                        <configuration>
                            <outputDirectory>${izpack-staging}/lib</outputDirectory>
                            <excludeTransitive>false</excludeTransitive>
                            <!--stripVersion>true</stripVersion-->
                            <overWriteReleases>true</overWriteReleases>
                            <overWriteSnapshots>true</overWriteSnapshots>
                            <overWriteIfNewer>true</overWriteIfNewer>
                            <excludeScope>system</excludeScope>
                            <!-- this excludes tools.jar, e.g. -->
                            <excludeArtifactIds>bidibwizard,bidibwizard-spy</excludeArtifactIds>
                            <!-- IMPORTANT: don't copy custom panels where our application jars live -->
                            <excludeGroupIds>org.codehaus.izpack</excludeGroupIds>
                            <!-- IMPORTANT: we don't want to copy the izpack dependency where our application jars live -->
                        </configuration>
                    </execution>
                    <execution>
                        <!-- copy *application* jars to izpack staging lib -->
                        <id>copy-product</id>
                        <phase>prepare-package</phase>
                        <goals>
                            <goal>copy-dependencies</goal>
                        </goals>
                        <configuration>
                            <outputDirectory>${izpack-staging}</outputDirectory>
                            <excludeTransitive>true</excludeTransitive>
                            <!--stripVersion>true</stripVersion-->
                            <overWriteReleases>true</overWriteReleases>
                            <overWriteSnapshots>true</overWriteSnapshots>
                            <overWriteIfNewer>true</overWriteIfNewer>
                            <excludeScope>system</excludeScope>
                            <!-- this excludes tools.jar, e.g. -->
                            <includeArtifactIds>bidibwizard,bidibwizard-spy</includeArtifactIds>
                        </configuration>
                    </execution>
                    <!-- copy the artefact to top-level target directory -->
                    <execution>
                        <id>copy-artifact</id>
                        <!-- must run in verify phase because the assembly is prepared in package phase by izpack-maven-plugin -->
                        <phase>verify</phase>
                        <goals>
                            <goal>copy</goal>
                        </goals>
                        <configuration>
                            <artifactItems>
                                <artifactItem>
                                    <groupId>${project.groupId}</groupId>
                                    <artifactId>${project.artifactId}</artifactId>
                                    <version>${project.version}</version>
                                    <type>${project.packaging}</type>
                                </artifactItem>
                            </artifactItems>
                            <outputDirectory>../target</outputDirectory>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.bidib.jbidib.de.akquinet.jbosscc.maven</groupId>
                <artifactId>image-maven-plugin</artifactId>
                <executions>
                    <execution>
                        <!--phase>validate</phase-->
                        <id>runtime-splash</id>
                        <phase>process-resources</phase>
                        <goals>
                            <goal>add-text</goal>
                        </goals>
                        <configuration>
                            <targetFormat>bmp</targetFormat>
                            <sourceImage>src/main/izpack/icons/BiDiBWizardSplash.png</sourceImage>
                            <targetImage>target/classes/icons/BiDiBWizardSplash.bmp</targetImage>
                            <defaults>
                                <fontName>Sans Serif</fontName>
                                <fontStyle>1</fontStyle>
                                <fontSize>10</fontSize>
                                <fontColor>BLACK</fontColor>
                            </defaults>
                            <texts>
                                <text>
                                    <text>${project.version} (${buildnumber-and-branch-info})</text>
                                    <offsetX>-10</offsetX>
                                    <offsetY>10</offsetY>
                                </text>
                            </texts>
                        </configuration>
                    </execution>
                    <execution>
                        <id>installer-splash</id>
                        <!--phase>validate</phase-->
                        <phase>process-resources</phase>
                        <goals>
                            <goal>add-text</goal>
                        </goals>
                        <configuration>
                            <targetFormat>png</targetFormat>
                            <sourceImage>src/main/resources/images/BiDiBWizardSplash.png</sourceImage>
                            <targetImage>target/classes/images/BiDiBWizardSplash.png</targetImage>
                            <defaults>
                                <fontName>Sans Serif</fontName>
                                <fontStyle>1</fontStyle>
                                <fontSize>10</fontSize>
                                <fontColor>BLACK</fontColor>
                            </defaults>
                            <texts>
                                <text>
                                    <text>${project.version} (${buildnumber-and-branch-info})</text>
                                    <offsetX>-10</offsetX>
                                    <offsetY>10</offsetY>
                                </text>
                            </texts>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>com.akathist.maven.plugins.launch4j</groupId>
                <artifactId>launch4j-maven-plugin</artifactId>
                <version>${launch4j-plugin-version}</version>
                <executions>
                    <execution>
                        <id>l4j-gui</id>
                        <phase>package</phase>
                        <goals>
                            <goal>launch4j</goal>
                        </goals>
                        <configuration>
                            <headerType>gui</headerType>
                            <stayAlive>true</stayAlive>
                            <outfile>${izpack-staging}/BiDiB-Wizard.exe</outfile>
                            <dontWrapJar>true</dontWrapJar>
                            <jar>${projectartifact}-${project.version}.jar</jar>
                            <errTitle>Start BiDiB-Wizard failed.</errTitle>
                            <classPath>
                                <mainClass>${main-class}</mainClass>
                                <preCp>.</preCp>
                            </classPath>
                            <singleInstance>
                                <mutexName>bidibwizard</mutexName>
                                <windowTitle>BiDiB-Wizard Application</windowTitle>
                            </singleInstance>
                            <icon>${izpack-staging}/icons/pic1.ico</icon>
                            <jre>
                                <minVersion>1.8.0</minVersion>
                                <jdkPreference>preferJre</jdkPreference>
                            </jre>
                            <splash>
                                <file>${izpack-staging}/icons/BiDiBWizardSplash.bmp</file>
                                <waitForWindow>true</waitForWindow>
                                <timeout>60</timeout>
                            </splash>
                            <versionInfo>
                                <fileVersion>1.9.0.0</fileVersion>
                                <txtFileVersion>BiDiB-Wizard Application</txtFileVersion>
                                <fileDescription>BiDiB-Wizard Application</fileDescription>
                                <copyright>BiDiB.org</copyright>
                                <productVersion>1.9.0.0</productVersion>
                                <txtProductVersion>1.9.0.0</txtProductVersion>
                                <productName>BiDiB-Wizard Application</productName>
                                <internalName>wizard</internalName>
                                <originalFilename>BiDiB-Wizard.exe</originalFilename>
                            </versionInfo>
                            <messages>
                                <startupErr>Start BiDiB-Wizard failed.</startupErr>
                                <bundledJreErr>Start BiDiB-Wizard with bundled JRE failed.</bundledJreErr>
                                <jreVersionErr>Start BiDiB-Wizard failed because of no matching JRE was found.</jreVersionErr>
                                <launcherErr>Start BiDiB-Wizard failed because of launcher error.</launcherErr>
                                <!-- Used by console header only. -->
                                <instanceAlreadyExistsMsg>Start BiDiB-Wizard failed because an instance already exists.</instanceAlreadyExistsMsg>
                            </messages>
                        </configuration>
                    </execution>
                    <execution>
                        <id>l4j-spy</id>
                        <phase>package</phase>
                        <goals>
                            <goal>launch4j</goal>
                        </goals>
                        <configuration>
                            <headerType>gui</headerType>
                            <stayAlive>true</stayAlive>
                            <outfile>${izpack-staging}/BiDiB-Wizard-Spy.exe</outfile>
                            <dontWrapJar>true</dontWrapJar>
                            <jar>${projectartifact}-spy-${project.version}.jar</jar>
                            <errTitle>Start BiDiB-Wizard-Spy failed.</errTitle>
                            <classPath>
                                <mainClass>${spy-main-class}</mainClass>
                                <!--preCp>.</preCp-->
                            </classPath>
                            <singleInstance>
                                <mutexName>bidibwizard-spy</mutexName>
                                <windowTitle>BiDiB-Wizard Spy Application</windowTitle>
                            </singleInstance>
                            <icon>${izpack-staging}/icons/pic1.ico</icon>
                            <jre>
                                <minVersion>1.8.0</minVersion>
                                <jdkPreference>preferJre</jdkPreference>
                            </jre>
                            <splash>
                                <file>${izpack-staging}/icons/BiDiBWizardSplash.bmp</file>
                                <waitForWindow>false</waitForWindow>
                                <timeout>5</timeout>
                            </splash>
                            <versionInfo>
                                <fileVersion>1.9.0.0</fileVersion>
                                <txtFileVersion>BiDiB-Wizard Spy Application</txtFileVersion>
                                <fileDescription>BiDiB-Wizard Spy Application</fileDescription>
                                <copyright>BiDiB.org</copyright>
                                <productVersion>1.9.0.0</productVersion>
                                <txtProductVersion>BiDiB-Wizard Spy Application</txtProductVersion>
                                <productName>BiDiB-Wizard-Spy</productName>
                                <internalName>wizard-spy</internalName>
                                <originalFilename>BiDiB-Wizard-Spy.exe</originalFilename>
                            </versionInfo>
                            <messages>
                                <startupErr>Start BiDiB-Wizard Spy failed.</startupErr>
                                <bundledJreErr>Start BiDiB-Wizard Spy with bundled JRE failed.</bundledJreErr>
                                <jreVersionErr>Start BiDiB-Wizard Spy failed because of no matching JRE was found.</jreVersionErr>
                                <launcherErr>Start BiDiB-Wizard Spy failed because of launcher error.</launcherErr>
                                <!-- Used by console header only. -->
                                <instanceAlreadyExistsMsg>Start BiDiB-Wizard Spy failed because an instance already exists.</instanceAlreadyExistsMsg>
                            </messages>
                        </configuration>
                    </execution>
                    <!-- create an exe for the installer itself -->
                    <!--
                    <execution>
                        <id>l4j-installer</id>
                        <phase>verify</phase>
                        <goals>
                            <goal>launch4j</goal>
                        </goals>
                        <configuration>
                            <headerType>gui</headerType>
                            <stayAlive>true</stayAlive>
                            <dontWrapJar>false</dontWrapJar>
                            <jar>target/${projectartifact}-installer-${project.version}.jar</jar>
                            <outfile>target/BiDiB-Wizard-Installer-${project.version}.exe</outfile>
                            <errTitle>Start BiDiB-Wizard Installer failed.</errTitle>
                            <classPath>
                                <mainClass>com.izforge.izpack.installer.bootstrap.Installer</mainClass>
                                <preCp>.</preCp>
                            </classPath>
                            <singleInstance>
                                <mutexName>bidibwizard-installer</mutexName>
                                <windowTitle>BiDiB-Wizard Installer Application</windowTitle>
                            </singleInstance>
                            <icon>${izpack-staging}/icons/pic1.ico</icon>
                            <jre>
                                <minVersion>1.8.0</minVersion>
                                <jdkPreference>preferJre</jdkPreference>
                            </jre>
                            <splash>
                                <file>${izpack-staging}/icons/BiDiBWizardSplash.bmp</file>
                                <waitForWindow>true</waitForWindow>
                                <timeout>60</timeout>
                            </splash>
                            <versionInfo>
                                <fileVersion>1.9.0.0</fileVersion>
                                <txtFileVersion>BiDiB-Wizard Installer Application</txtFileVersion>
                                <fileDescription>BiDiB-Wizard Installer Application</fileDescription>
                                <copyright>BiDiB.org</copyright>
                                <productVersion>1.9.0.0</productVersion>
                                <txtProductVersion>BiDiB-Wizard Installer Application</txtProductVersion>
                                <productName>BiDiB-Wizard-Installer</productName>
                                <internalName>wizard-installer</internalName>
                                <originalFilename>BiDiB-Wizard-Installer.exe</originalFilename>
                            </versionInfo>
                            <messages>
                                <startupErr>Start BiDiB-Wizard Installer failed.</startupErr>
                                <bundledJreErr>Start BiDiB-Wizard Installer with bundled JRE failed.</bundledJreErr>
                                <jreVersionErr>Start BiDiB-Wizard Installer failed because of no matching JRE was found.</jreVersionErr>
                                <launcherErr>Start BiDiB-Wizard Installer failed because of launcher error.</launcherErr>
                                <instanceAlreadyExistsMsg>Start BiDiB-Wizard Installer failed because an instance already exists.</instanceAlreadyExistsMsg>
                            </messages>
                        </configuration>
                    </execution>
                    -->
                </executions>
            </plugin>
            <plugin>
                <groupId>org.codehaus.izpack</groupId>
                <artifactId>izpack-maven-plugin</artifactId>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>izpack</goal>
                        </goals>
                        <configuration>
                            <baseDir>${izpack-staging}</baseDir>
                            <installFile>${basedir}/src/main/izpack/installer.xml</installFile>
                            <!--finalName>${project.build.finalName}-installer</finalName-->
                            <mkdirs>true</mkdirs>
                            <enableAttachArtifact>false</enableAttachArtifact>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
        <!--This plugin's configuration is used to store Eclipse m2e settings only. It has no influence on the Maven build itself.-->
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.eclipse.m2e</groupId>
                    <artifactId>lifecycle-mapping</artifactId>
                    <version>1.0.0</version>
                    <configuration>
                        <lifecycleMappingMetadata>
                            <pluginExecutions>
                                <pluginExecution>
                                    <pluginExecutionFilter>
                                        <groupId>org.apache.maven.plugins</groupId>
                                        <artifactId>maven-antrun-plugin</artifactId>
                                        <versionRange>[1.7,)</versionRange>
                                        <goals>
                                            <goal>run</goal>
                                        </goals>
                                    </pluginExecutionFilter>
                                    <action>
                                        <execute />
                                    </action>
                                </pluginExecution>
                                <pluginExecution>
                                    <pluginExecutionFilter>
                                        <groupId>org.apache.maven.plugins</groupId>
                                        <artifactId>maven-dependency-plugin</artifactId>
                                        <versionRange>[2.8,)</versionRange>
                                        <goals>
                                            <goal>copy-dependencies</goal>
                                        </goals>
                                    </pluginExecutionFilter>
                                    <action>
                                        <ignore />
                                    </action>
                                </pluginExecution>
                            </pluginExecutions>
                        </lifecycleMappingMetadata>
                    </configuration>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>
    <profiles>
        <profile>
            <id>release</id>
            <activation>
                <property>
                    <name>performRelease</name>
                    <value>true</value>
                </property>
            </activation>
            <properties>
                <buildnumber-and-branch-info>${buildNumber}</buildnumber-and-branch-info>
            </properties>
        </profile>
    </profiles>
</project>
