## Script only for NeoControl EWS!
<Head PID=129 VID=13 VER=1.0>

## This script defines a group of LEDs 
## Mode: Switch on and off one after another
##
## Hardware: Neocontrol with NeoControl EWS firmware
## Software:
##
## history: 2017-01-04 V.Dierkes, first version
##          2016-01-09 V.Dierkes, add relative addressing for stripe positions 2-8
##          2016-02-02 V.Dierkes, improve text descriptions
##          2017-02-17 V.Dierkes, use predefined variables to calculate CV addresses
##          2017-02-17 V.Dierkes, check VID and PID
##          2017-02-18 V.Dierkes, predefined variable light_count was renamed
##          2017-03-18 V.Dierkes, replaced tabs by spaces
##
## how to use:
## A) configure
##    Not needed
## B) load and run this script
##    what will happen?
##    1. the led group will be configured with the given parameters
## C) have fun
##    Switching on/off the SPORT will control the group of light elements accordingly
##

##instruction(text:de="NeoEWS: Konfiguration einer Lichtgruppe für Modus 1 - Alle nacheinander An/Aus", text:en="NeoEWS: Configuration of a light group for mode 1 - One after another on/off")
##
##input($my_group:int, text:de="Nummer der Lichtgruppe (0-15)", text:en="Number of the group", default=0)
##input($start_led:light, text:de="Nummer des ersten Lightports der Gruppe", text:en="Number of the first Lightport of the group", default=0)
##input($group_size:int, text:de="Anzahl der Lightports der Gruppe<br/>| >=1", text:en="Number of Lightports of the group<br/>| >=1", default=2)
## ##input($last_led:light, text:de="Nummer des letzten Lightports der Gruppe", text:en="Number of the last Lightport of the group", default=0)
## ##input($speed_factor:int, text:de="Geschwindigkeitsfaktor der Gruppe<br>| 0=10ms, 99=1s, 189=10s, 215=1min, 249=10min", text:en="Speed factor of the group<br>| 0=10ms, 99=1s, 189=10s, 215=1min, 249=10min", default=0)
##input($speed_min:int, text:de="Zeit pro Schritt: Minuten<br/>| Min: 0,01s - Max: 10min 55sec", text:en="Time per step: Minutes<br/>Min: 0.01s - Max: 10min 55sec", default=0)
##input($speed_sec:int, text:de="Zeit pro Schritt: Sekunden", text:en="Time per step: Seconds", default=0)
##input($speed_hun:int, text:de="Zeit pro Schritt: Hundertstel", text:en="Time per step: Hundredths", default=50)
##input($group_on:int, text:de="Einschaltverhalten der Gruppe<br>| 1=An,3=Dimm,4=Neon,5=BlinkA,6=BlinkB,7=FlashA,8=FlashB,9=Double", text:en="Turn on behavior of the group<br>1=On,3=Dimm,4=Neon,5=BlinkA,6=BlinkB,7=FlashA,8=FlashB,9=Double", default=1)
##input($group_off:int, text:de="Ausschaltverhalten der Gruppe<br>| 0=Aus,2=Dimm", text:en="Turn off behavior of the group<br>0=Off,2=Dimm", default=0)
##input($group_option:int, text:de="Wartefaktor zwischen Aus und An zur nächsten LED", text:en="Delay factor between Off and On for the next LED", default=0)
##input($group_endless:boolean, text:de="Endloser Lauf der Gruppe", text:en="Endless running of the group", default=true)
##input($led_tabval1:int, text:de="Stripe-Pos der 1. LED", text:en="Stripe position of 1st LED", default=0)
##input($led_tab2:string, text:de="Stripe-Pos der 2. LED<br/>| Wenn min. 2 LEDs in dieser Gruppe<br/>| Nummer oder +Offset oder -Offset", text:en="Stripe position of 2nd LED<br/>| in case more than 1 LED in this group<br/>| Number or +Offset or -Offset", default="+1")
##input($led_tab3:string, text:de="Stripe-Pos der 3. LED<br/>| Wenn min. 3 LEDs in dieser Gruppe<br/>| Nummer oder +Offset oder -Offset", text:en="Stripe position of 3rd LED<br/>| in case more than 2 LED in this group<br/>| Number or +Offset or -Offset", default="+1")
##input($led_tab4:string, text:de="Stripe-Pos der 4. LED<br/>| Wenn min. 4 LEDs in dieser Gruppe<br/>| Nummer oder +Offset oder -Offset", text:en="Stripe position of 4th LED<br/>| in case more than 3 LED in this group<br/>| Number or +Offset or -Offset", default="+1")
##input($led_tab5:string, text:de="Stripe-Pos der 5. LED<br/>| Wenn min. 5 LEDs in dieser Gruppe<br/>| Nummer oder +Offset oder -Offset", text:en="Stripe position of 5th LED<br/>| in case more than 4 LED in this group<br/>| Number or +Offset or -Offset", default="+1")
##input($led_tab6:string, text:de="Stripe-Pos der 6. LED<br/>| Wenn min. 6 LEDs in dieser Gruppe<br/>| Nummer oder +Offset oder -Offset", text:en="Stripe position of 6th LED<br/>| in case more than 5 LED in this group<br/>| Number or +Offset or -Offset", default="+1")
##input($led_tab7:string, text:de="Stripe-Pos der 7. LED<br/>| Wenn min. 7 LEDs in dieser Gruppe<br/>| Nummer oder +Offset oder -Offset", text:en="Stripe position of 7th LED<br/>| in case more than 6 LED in this group<br/>| Number or +Offset or -Offset", default="+1")
##input($led_tab8:string, text:de="Stripe-Pos der 8. LED<br/>| Wenn min. 8 LEDs in dieser Gruppe<br/>| Nummer oder +Offset oder -Offset<br/>| Mehr Zuordnungen können manuell gesetzt werden", text:en="Stripe position of 8th LED<br/>| in case more than 7 LED in this group<br/>| Number or +Offset or -Offset<br/>| More can be set manually", default="+1")

## -------------- 1. Select Target

#if((${vid} == 13) && (${pid} == 129))
  ## Only for Neo_EWS

## -------------- 2. Define default values
  #set($group_mode = 1)
  #set($Integer = 0)
  #set($cv_base_lights    = 81)
  #set($cv_base_accessory = $cv_base_lights + ( 5 * ${node_light_count}) )
  #set($cv_base_colors    = $cv_base_accessory + ( 10 * ${node_accessory_count}) )
  #set($cv_base_groups    = $cv_base_colors + ( 3 * 32 ) )
  #set($cv_base_mapping   = $cv_base_groups + ( 6 * 16 ) )
  #set($cv_base_config    = $cv_base_mapping + ( 1 * 256 ) )
  #set($cv_base_acc_cfg   = $cv_base_config + ( 6 ) )
  
## -------------- 2. Define default values

## -------------- 3. execute part of the script

  #set($led_tabval2 = -1)
  #set($led_tabval3 = -1)
  #set($led_tabval4 = -1)
  #set($led_tabval5 = -1)
  #set($led_tabval6 = -1)
  #set($led_tabval7 = -1)
  #set($led_tabval8 = -1)

  #if($led_tab2.matches("\d+"))
    #set($led_tabval2 = $Integer.parseInt($led_tab2))
  #elseif($led_tab2.matches("[\+\-]\d+"))
    #if($led_tabval1 != -1)
      #if($led_tab2.substring(0,1) == "+")
        #set($led_tabval2 = $led_tabval1 + $Integer.parseInt($led_tab2.substring(1)))
      #else
        #set($led_tabval2 = $led_tabval1 - $Integer.parseInt($led_tab2.substring(1)))
      #end
    #end
  #end
  #if($led_tab3.matches("\d+"))
    #set($led_tabval3 = $Integer.parseInt($led_tab3))
  #elseif($led_tab3.matches("[\+\-]\d+"))
    #if($led_tabval2 != -1)
      #if($led_tab3.substring(0,1) == "+")
        #set($led_tabval3 = $led_tabval2 + $Integer.parseInt($led_tab3.substring(1)))
      #else
        #set($led_tabval3 = $led_tabval2 - $Integer.parseInt($led_tab3.substring(1)))
      #end
    #end
  #end
  #if($led_tab4.matches("\d+"))
    #set($led_tabval4 = $Integer.parseInt($led_tab4))
  #elseif($led_tab4.matches("[\+\-]\d+"))
    #if($led_tabval3 != -1)
      #if($led_tab4.substring(0,1) == "+")
        #set($led_tabval4 = $led_tabval3 + $Integer.parseInt($led_tab4.substring(1)))
      #else
        #set($led_tabval4 = $led_tabval3 - $Integer.parseInt($led_tab4.substring(1)))
      #end
    #end
  #end
  #if($led_tab5.matches("\d+"))
    #set($led_tabval5 = $Integer.parseInt($led_tab5))
  #elseif($led_tab5.matches("[\+\-]\d+"))
    #if($led_tabval4 != -1)
      #if($led_tab5.substring(0,1) == "+")
        #set($led_tabval5 = $led_tabval4 + $Integer.parseInt($led_tab5.substring(1)))
      #else
        #set($led_tabval5 = $led_tabval4 - $Integer.parseInt($led_tab5.substring(1)))
      #end
    #end
  #end
  #if($led_tab6.matches("\d+"))
    #set($led_tabval6 = $Integer.parseInt($led_tab6))
  #elseif($led_tab6.matches("[\+\-]\d+"))
    #if($led_tabval5 != -1)
      #if($led_tab6.substring(0,1) == "+")
        #set($led_tabval6 = $led_tabval5 + $Integer.parseInt($led_tab6.substring(1)))
      #else
        #set($led_tabval6 = $led_tabval5 - $Integer.parseInt($led_tab6.substring(1)))
      #end
    #end
  #end
  #if($led_tab7.matches("\d+"))
    #set($led_tabval7 = $Integer.parseInt($led_tab7))
  #elseif($led_tab7.matches("[\+\-]\d+"))
    #if($led_tabval6 != -1)
      #if($led_tab7.substring(0,1) == "+")
        #set($led_tabval7 = $led_tabval6 + $Integer.parseInt($led_tab7.substring(1)))
      #else
        #set($led_tabval7 = $led_tabval6 - $Integer.parseInt($led_tab7.substring(1)))
      #end
    #end
  #end
  #if($led_tab8.matches("\d+"))
    #set($led_tabval8 = $Integer.parseInt($led_tab8))
  #elseif($led_tab8.matches("[\+\-]\d+"))
    #if($led_tabval7 != -1)
      #if($led_tab8.substring(0,1) == "+")
        #set($led_tabval8 = $led_tabval7 + $Integer.parseInt($led_tab8.substring(1)))
      #else
        #set($led_tabval8 = $led_tabval7 - $Integer.parseInt($led_tab8.substring(1)))
      #end
    #end
  #end

  #set($last_led = $start_led + $group_size - 1)

  #if(($my_group>=0) && ($my_group<=15) && ($last_led >= $start_led) && ($led_tabval1 >= 0) && ($led_tabval2 >= 0) && ($led_tabval3 >= 0) && ($led_tabval4 >= 0) && ($led_tabval5 >= 0) && ($led_tabval6 >= 0) && ($led_tabval7 >= 0) && ($led_tabval8 >= 0))
    ## do it only in case the group number is in the allowed range
  
    ## do some naming stuff
    set switch $my_group name="Group${my_group}_Sequence_LP${start_led}-${last_led}"
    #foreach ($i in [$start_led..$last_led])
      set light $i name="Group${my_group}_Sequence_Light${foreach.index}"
    #end

    ## Set mode

    ## Set endless mode in group value
    #if ($group_endless)
    #set($group_mode = $group_mode + 128)
    #end

    ## Set on/off value
    #set($group_onoff = (16 * $group_on) + $group_off)

    ## calculate speed_faktor
    #set($speed_factor = 0)
    #set($speed_time = (6000*$speed_min) + (100*$speed_sec) + $speed_hun)

    #if($speed_time > 65500)
      #set($speed_factor=251)
    #elseif($speed_time > 33000)
      #set($speed_factor=($speed_time - 33000) / 3000 + 240)
    #elseif($speed_time > 6000)
      #set($speed_factor=($speed_time - 7000) / 1000 + 216)
    #elseif($speed_time > 3000)
      #set($speed_factor = ($speed_time - 3500) / 500 + 210)
    #elseif($speed_time > 1000)
      #set($speed_factor=$speed_time / 100 + 179)
    #elseif($speed_time > 100)
      #set($speed_factor=($speed_time - 110) / 10 + 100)
    #elseif($speed_time > 0)
      #set($speed_factor=$speed_time - 1)
    #end
  
## --------------- 4. Write the data to the node

    #set($cv_base = $cv_base_groups + 6 * $my_group)
    #set($cv_addr = $cv_base)

    set CV ${cv_addr} ${group_mode}
    #set($cv_addr = $cv_addr + 1)
    set CV ${cv_addr} ${start_led}
    #set($cv_addr = $cv_addr + 1)
    set CV ${cv_addr} ${last_led}
    #set($cv_addr = $cv_addr + 1)
    set CV ${cv_addr} ${speed_factor}
    #set($cv_addr = $cv_addr + 1)
    set CV ${cv_addr} ${group_onoff}
    #set($cv_addr = $cv_addr + 1)
    set CV ${cv_addr} ${group_option}

    ## Stripe position

    #set($cv_base = $cv_base_mapping)
    #set($cv_addr = $cv_base + $led_tabval1)
    set CV ${cv_addr} ${start_led}

    #if($start_led<$last_led)
      #set($cv_addr = $cv_base + $led_tabval2)
      #set($start_led = $start_led + 1)
      set CV ${cv_addr} ${start_led}
    #end

    #if($start_led<$last_led)
      #set($cv_addr = $cv_base + $led_tabval3)
      #set($start_led = $start_led + 1)
      set CV ${cv_addr} ${start_led}
    #end

    #if($start_led<$last_led)
      #set($cv_addr = $cv_base + $led_tabval4)
      #set($start_led = $start_led + 1)
      set CV ${cv_addr} ${start_led}
    #end

    #if($start_led<$last_led)
      #set($cv_addr = $cv_base + $led_tabval5)
      #set($start_led = $start_led + 1)
      set CV ${cv_addr} ${start_led}
    #end

    #if($start_led<$last_led)
      #set($cv_addr = $cv_base + $led_tabval6)
      #set($start_led = $start_led + 1)
      set CV ${cv_addr} ${start_led}
    #end

    #if($start_led<$last_led)
      #set($cv_addr = $cv_base + $led_tabval7)
      #set($start_led = $start_led + 1)
      set CV ${cv_addr} ${start_led}
    #end

    #if($start_led<$last_led)
      #set($cv_addr = $cv_base + $led_tabval8)
      #set($start_led = $start_led + 1)
      set CV ${cv_addr} ${start_led}
    #end

  #end

#end

#######################################
