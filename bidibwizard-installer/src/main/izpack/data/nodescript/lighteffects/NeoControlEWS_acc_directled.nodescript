## Script only for NeoControl EWS!
<Head PID=129 VID=13 VER=1.0>

## This script defines an accessory to control up to 8 LEDs 
## in a direct way using the aspects.
##
## Hardware: Neocontrol with NeoControl EWS firmware
## Software: 1 accessory
##
## history: 2016-01-06 V.Dierkes, first version
##          2016-01-09 V.Dierkes, add relative addressing for stripe positions 2-8
##          2017-02-17 V.Dierkes, use predefined variables to calculate CV addresses
##          2017-02-17 V.Dierkes, check VID and PID
##          2017-02-18 V.Dierkes, predefined variable light_count was renamed
##          2017-03-16 V.Dierkes, added names for the light ports
##          2017-03-18 V.Dierkes, replaced tabs by spaces
##          2017-04-19 V.Dierkes, removed test default value for name
##                                changed delay to time value
##                                fix to keep accessory name, add accessory name to lightports
##
## how to use:
## A) configure
##    Not needed
## B) load and run this script
##    what will happen?
##    1. the accessory will be configured with the given parameters
## C) have fun
##    Selecting the aspects of the accessory will set the light elements accordingly
##

##instruction(text:de="NeoEWS: Konfiguration eines Accessories für direkte LED-Ansteuerung", text:en="NeoEWS: Configuration of an accessory for direct LED control")
##
##input($my_accessory:accessory, text:de="Nummer des Accessories", text:en="Number of the accessory", default=0)
##input($name_accessory:string, text:de="Name des Accessories<br/>| Leer lassen um den Namen zu behalten", text:en="Name of the accessory<br/>| Leave it empty to keep the name", default="")
##input($acc_leds:int, text:de="Anzahl LEDs (1-8)", text:en="Quantity of LEDs (1-8)", default=3)
##input($acc_onoff:int, text:de="Ein-/Ausschaltverhalten<br/>| 0=An/Aus, 1=Dimm, 2=Neon/Aus, 3=Neon/Dimm,<br/>| 4=BlinkA, 5=BlinkB, 6=FlashA, 7=FlashB, 8=Double", text:en="On-/Off behavior<br/>| 0=On/Off, 1=Dimm, 2=Neon/Off, 3=Neon/Dimm,<br/>| 4=BlinkA, 5=BlinkB, 6=FlashA, 7=FlashB, 8=Double", default=0)
##input($start_led:light, text:de="Nummer des ersten Lightports des Accessory", text:en="Number of the first Lightport of the accessory", default=0)
##input($acc_onoffdelay:int, text:de="Verzögerung zwischen Aus und Ein in ms<br/>| Übergangsdauer zum nächsten Begriff<br/>| 0ms..1270ms", text:en="Delay between Off and On in ms<br/>Delay to next aspect<br/>| 0ms..1270ms", default=0)
##input($acc_aspect1:string, text:de="LED-Muster Begriff 0<br/>| Bit0=erster Lightport, Bit1=Zweiter, ...<br/>| Dezimal (xxx), Binär ([bB]xxx), Hex ([hHxX]xxx)", text:en="LED pattern, aspect 0<br/>| Bit0=First light port, Bit1=Second, ...", default=0)
##input($acc_aspect2:string, text:de="LED-Muster Begriff 1", text:en="LED pattern, aspect 1", default=0)
##input($acc_aspect3:string, text:de="LED-Muster Begriff 2", text:en="LED pattern, aspect 2", default=0)
##input($acc_aspect4:string, text:de="LED-Muster Begriff 3", text:en="LED pattern, aspect 3", default=0)
##input($acc_aspect5:string, text:de="LED-Muster Begriff 4", text:en="LED pattern, aspect 4", default=0)
##input($acc_aspect6:string, text:de="LED-Muster Begriff 5", text:en="LED pattern, aspect 5", default=0)
##input($acc_aspect7:string, text:de="LED-Muster Begriff 6", text:en="LED pattern, aspect 6", default=0)
##input($acc_aspect8:string, text:de="LED-Muster Begriff 7", text:en="LED pattern, aspect 7", default=0)
##input($acc_tabval1:int, text:de="Stripe-Pos der 1. LED", text:en="Stripe position of 1st LED", default=0)
##input($acc_tab2:string, text:de="Stripe-Pos der 2. LED<br/>| Nummer oder +Offset oder -Offset", text:en="Stripe position of 2nd LED<br/>| Number or +Offset or -Offset", default="+1")
##input($acc_tab3:string, text:de="Stripe-Pos der 3. LED<br/>| Nummer oder +Offset oder -Offset", text:en="Stripe position of 3rd LED<br/>| Number or +Offset or -Offset", default="+1")
##input($acc_tab4:string, text:de="Stripe-Pos der 4. LED<br/>| Nummer oder +Offset oder -Offset", text:en="Stripe position of 4th LED<br/>| Number or +Offset or -Offset", default="+1")
##input($acc_tab5:string, text:de="Stripe-Pos der 5. LED<br/>| Nummer oder +Offset oder -Offset", text:en="Stripe position of 5th LED<br/>| Number or +Offset or -Offset", default="+1")
##input($acc_tab6:string, text:de="Stripe-Pos der 6. LED<br/>| Nummer oder +Offset oder -Offset", text:en="Stripe position of 6th LED<br/>| Number or +Offset or -Offset", default="+1")
##input($acc_tab7:string, text:de="Stripe-Pos der 7. LED<br/>| Nummer oder +Offset oder -Offset", text:en="Stripe position of 7th LED<br/>| Number or +Offset or -Offset", default="+1")
##input($acc_tab8:string, text:de="Stripe-Pos der 8. LED<br/>| Nummer oder +Offset oder -Offset<br/>| Mehr Zuordnungen können manuell gesetzt werden", text:en="Stripe position of 8th LED<br/>| <Number> or +<Offset> or <-Offset><br/>| More can be set manually", default="+1")

## -------------- 1. Select Target

#if((${vid} == 13) && (${pid} == 129))
  ## Only for Neo_EWS

## -------------- 2. Define default values

  #set($Integer = 0)
  #set($cv_base_lights    = 81)
  #set($cv_base_accessory = $cv_base_lights + ( 5 * ${node_light_count}) )
  #set($cv_base_colors    = $cv_base_accessory + ( 10 * ${node_accessory_count}) )
  #set($cv_base_groups    = $cv_base_colors + ( 3 * 32 ) )
  #set($cv_base_mapping   = $cv_base_groups + ( 6 * 16 ) )
  #set($cv_base_config    = $cv_base_mapping + ( 1 * 256 ) )
  #set($cv_base_acc_cfg   = $cv_base_config + ( 5 ) )

## -------------- 3. execute part of the script

  #set($acc_onoffdelay = ($acc_onoffdelay + 5) / 10)
  #if($acc_onoffdelay > 127)
    #set($acc_onoffdelay = 127)
  #end
  
  #set($acc_val1 = -1)
  #set($acc_val2 = -1)
  #set($acc_val3 = -1)
  #set($acc_val4 = -1)
  #set($acc_val5 = -1)
  #set($acc_val6 = -1)
  #set($acc_val7 = -1)
  #set($acc_val8 = -1)
  #set($acc_tabval2 = -1)
  #set($acc_tabval3 = -1)
  #set($acc_tabval4 = -1)
  #set($acc_tabval5 = -1)
  #set($acc_tabval6 = -1)
  #set($acc_tabval7 = -1)
  #set($acc_tabval8 = -1)

  #if($acc_aspect1.matches("\d+"))
    #set($acc_val1 = $Integer.parseInt($acc_aspect1))
  #elseif($acc_aspect1.matches("[bB]\d+"))
    #set($acc_val1 = $Integer.parseInt($acc_aspect1.substring(1),2))
  #elseif($acc_aspect1.matches("[hHxX][\da-fA-F]+"))
    #set($acc_val1 = $Integer.parseInt($acc_aspect1.substring(1),16))
  #end
  #if($acc_aspect2.matches("\d+"))
    #set($acc_val2 = $Integer.parseInt($acc_aspect2))
  #elseif($acc_aspect2.matches("[bB]\d+"))
    #set($acc_val2 = $Integer.parseInt($acc_aspect2.substring(1),2))
  #elseif($acc_aspect2.matches("[hHxX][\da-fA-F]+"))
    #set($acc_val2 = $Integer.parseInt($acc_aspect2.substring(1),16))
  #end
  #if($acc_aspect3.matches("\d+"))
    #set($acc_val3 = $Integer.parseInt($acc_aspect3))
  #elseif($acc_aspect3.matches("[bB]\d+"))
    #set($acc_val3 = $Integer.parseInt($acc_aspect3.substring(1),2))
  #elseif($acc_aspect3.matches("[hHxX][\da-fA-F]+"))
    #set($acc_val3 = $Integer.parseInt($acc_aspect3.substring(1),16))
  #end
  #if($acc_aspect4.matches("\d+"))
    #set($acc_val4 = $Integer.parseInt($acc_aspect4))
  #elseif($acc_aspect4.matches("[bB]\d+"))
    #set($acc_val4 = $Integer.parseInt($acc_aspect4.substring(1),2))
  #elseif($acc_aspect4.matches("[hHxX][\da-fA-F]+"))
    #set($acc_val4 = $Integer.parseInt($acc_aspect4.substring(1),16))
  #end
  #if($acc_aspect5.matches("\d+"))
    #set($acc_val5 = $Integer.parseInt($acc_aspect5))
  #elseif($acc_aspect5.matches("[bB]\d+"))
    #set($acc_val5 = $Integer.parseInt($acc_aspect5.substring(1),2))
  #elseif($acc_aspect5.matches("[hHxX][\da-fA-F]+"))
    #set($acc_val5 = $Integer.parseInt($acc_aspect5.substring(1),16))
  #end
  #if($acc_aspect6.matches("\d+"))
    #set($acc_val6 = $Integer.parseInt($acc_aspect6))
  #elseif($acc_aspect6.matches("[bB]\d+"))
    #set($acc_val6 = $Integer.parseInt($acc_aspect6.substring(1),2))
  #elseif($acc_aspect6.matches("[hHxX][\da-fA-F]+"))
    #set($acc_val6 = $Integer.parseInt($acc_aspect6.substring(1),16))
  #end
  #if($acc_aspect7.matches("\d+"))
    #set($acc_val7 = $Integer.parseInt($acc_aspect7))
  #elseif($acc_aspect7.matches("[bB]\d+"))
    #set($acc_val7 = $Integer.parseInt($acc_aspect7.substring(1),2))
  #elseif($acc_aspect7.matches("[hHxX][\da-fA-F]+"))
    #set($acc_val7 = $Integer.parseInt($acc_aspect7.substring(1),16))
  #end
  #if($acc_aspect8.matches("\d+"))
    #set($acc_val8 = $Integer.parseInt($acc_aspect8))
  #elseif($acc_aspect8.matches("[bB]\d+"))
    #set($acc_val8 = $Integer.parseInt($acc_aspect8.substring(1),2))
  #elseif($acc_aspect8.matches("[hHxX][\da-fA-F]+"))
    #set($acc_val8 = $Integer.parseInt($acc_aspect8.substring(1),16))
  #end

  #if($acc_tab2.matches("\d+"))
    #set($acc_tabval2 = $Integer.parseInt($acc_tab2))
  #elseif($acc_tab2.matches("[\+\-]\d+"))
    #if($acc_tabval1 != -1)
      #if($acc_tab2.substring(0,1) == "+")
        #set($acc_tabval2 = $acc_tabval1 + $Integer.parseInt($acc_tab2.substring(1)))
      #else
        #set($acc_tabval2 = $acc_tabval1 - $Integer.parseInt($acc_tab2.substring(1)))
      #end
    #end
  #end
  #if($acc_tab3.matches("\d+"))
    #set($acc_tabval3 = $Integer.parseInt($acc_tab3))
  #elseif($acc_tab3.matches("[\+\-]\d+"))
    #if($acc_tabval2 != -1)
      #if($acc_tab3.substring(0,1) == "+")
        #set($acc_tabval3 = $acc_tabval2 + $Integer.parseInt($acc_tab3.substring(1)))
      #else
        #set($acc_tabval3 = $acc_tabval2 - $Integer.parseInt($acc_tab3.substring(1)))
      #end
    #end
  #end
  #if($acc_tab4.matches("\d+"))
    #set($acc_tabval4 = $Integer.parseInt($acc_tab4))
  #elseif($acc_tab4.matches("[\+\-]\d+"))
    #if($acc_tabval3 != -1)
      #if($acc_tab4.substring(0,1) == "+")
        #set($acc_tabval4 = $acc_tabval3 + $Integer.parseInt($acc_tab4.substring(1)))
      #else
        #set($acc_tabval4 = $acc_tabval3 - $Integer.parseInt($acc_tab4.substring(1)))
      #end
    #end
  #end
  #if($acc_tab5.matches("\d+"))
    #set($acc_tabval5 = $Integer.parseInt($acc_tab5))
  #elseif($acc_tab5.matches("[\+\-]\d+"))
    #if($acc_tabval4 != -1)
      #if($acc_tab5.substring(0,1) == "+")
        #set($acc_tabval5 = $acc_tabval4 + $Integer.parseInt($acc_tab5.substring(1)))
      #else
        #set($acc_tabval5 = $acc_tabval4 - $Integer.parseInt($acc_tab5.substring(1)))
      #end
    #end
  #end
  #if($acc_tab6.matches("\d+"))
    #set($acc_tabval6 = $Integer.parseInt($acc_tab6))
  #elseif($acc_tab6.matches("[\+\-]\d+"))
    #if($acc_tabval5 != -1)
      #if($acc_tab6.substring(0,1) == "+")
        #set($acc_tabval6 = $acc_tabval5 + $Integer.parseInt($acc_tab6.substring(1)))
      #else
        #set($acc_tabval6 = $acc_tabval5 - $Integer.parseInt($acc_tab6.substring(1)))
      #end
    #end
  #end
  #if($acc_tab7.matches("\d+"))
    #set($acc_tabval7 = $Integer.parseInt($acc_tab7))
  #elseif($acc_tab7.matches("[\+\-]\d+"))
    #if($acc_tabval6 != -1)
      #if($acc_tab7.substring(0,1) == "+")
        #set($acc_tabval7 = $acc_tabval6 + $Integer.parseInt($acc_tab7.substring(1)))
      #else
        #set($acc_tabval7 = $acc_tabval6 - $Integer.parseInt($acc_tab7.substring(1)))
      #end
    #end
  #end
  #if($acc_tab8.matches("\d+"))
    #set($acc_tabval8 = $Integer.parseInt($acc_tab8))
  #elseif($acc_tab8.matches("[\+\-]\d+"))
    #if($acc_tabval7 != -1)
      #if($acc_tab8.substring(0,1) == "+")
        #set($acc_tabval8 = $acc_tabval7 + $Integer.parseInt($acc_tab8.substring(1)))
      #else
        #set($acc_tabval8 = $acc_tabval7 - $Integer.parseInt($acc_tab8.substring(1)))
      #end
    #end
  #end


  #if(($acc_leds>=1) && ($acc_leds<=8) && ($acc_val1 >= 0) && ($acc_tabval2 >= 0) && ($acc_tabval3 >= 0) && ($acc_tabval4 >= 0) && ($acc_tabval5 >= 0) && ($acc_tabval6 >= 0) && ($acc_tabval7 >= 0) && ($acc_tabval8 >= 0))
    ## do it only in case the number of LEDs is in the allowed range

    ## --------------- 4. Write the data to the node

    #set($cv_base = $cv_base_acc_cfg + (3 * $my_accessory))
    #set($cv_addr = $cv_base)
    #set($acc_mode = $acc_leds + (16 * $acc_onoff))

    set CV ${cv_addr} ${acc_mode}
    #set($cv_addr = $cv_addr + 1)
    set CV ${cv_addr} ${start_led}
    #set($cv_addr = $cv_addr + 1)
    set CV ${cv_addr} ${acc_onoffdelay}

    #set($cv_base = $cv_base_accessory + (10 * $my_accessory))
    #set($cv_addr = $cv_base + 1)
  
    set CV ${cv_addr} ${acc_val1}
    #set($cv_addr = $cv_addr + 1)
    set CV ${cv_addr} ${acc_val2}
    #set($cv_addr = $cv_addr + 1)
    set CV ${cv_addr} ${acc_val3}
    #set($cv_addr = $cv_addr + 1)
    set CV ${cv_addr} ${acc_val4}
    #set($cv_addr = $cv_addr + 1)
    set CV ${cv_addr} ${acc_val5}
    #set($cv_addr = $cv_addr + 1)
    set CV ${cv_addr} ${acc_val6}
    #set($cv_addr = $cv_addr + 1)
    set CV ${cv_addr} ${acc_val7}
    #set($cv_addr = $cv_addr + 1)
    set CV ${cv_addr} ${acc_val8}

    ## Stripe position

    #set($cv_base = $cv_base_mapping)
    #set($cv_addr = $cv_base + $acc_tabval1)
    #set($led_pos = $start_led)
    set CV ${cv_addr} ${led_pos}

    #if($acc_leds>1)
      #set($cv_addr = $cv_base + $acc_tabval2)
      #set($led_pos = $led_pos + 1)
      set CV ${cv_addr} ${led_pos}
    #end

    #if($acc_leds>2)
      #set($cv_addr = $cv_base + $acc_tabval3)
      #set($led_pos = $led_pos + 1)
      set CV ${cv_addr} ${led_pos}
    #end

    #if($acc_leds>3)
      #set($cv_addr = $cv_base + $acc_tabval4)
      #set($led_pos = $led_pos + 1)
      set CV ${cv_addr} ${led_pos}
    #end

    #if($acc_leds>4)
      #set($cv_addr = $cv_base + $acc_tabval5)
      #set($led_pos = $led_pos + 1)
      set CV ${cv_addr} ${led_pos}
    #end

    #if($acc_leds>5)
      #set($cv_addr = $cv_base + $acc_tabval6)
      #set($led_pos = $led_pos + 1)
      set CV ${cv_addr} ${led_pos}
    #end

    #if($acc_leds>6)
      #set($cv_addr = $cv_base + $acc_tabval7)
      #set($led_pos = $led_pos + 1)
      set CV ${cv_addr} ${led_pos}
    #end

    #if($acc_leds>7)
      #set($cv_addr = $cv_base + $acc_tabval8)
      #set($led_pos = $led_pos + 1)
      set CV ${cv_addr} ${led_pos}
    #end

    ## Name of the accessory

    #if("$!name_accessory" != "") ## test for NULL
      #if("$name_accessory" != "") ## test for empty
        set accessory ${my_accessory} name="${name_accessory}"
        #set($name_accessory="_$name_accessory")
      #end ## test for empty
    #else ## test for NULL
      #set($name_accessory="")
    #end
    #set($i = $start_led)
    set light $i name="Acc${my_accessory}${name_accessory}_Light0"
    #if($acc_leds>1)
      #set($i = $i + 1)
      set light $i name="Acc${my_accessory}${name_accessory}_Light1"
    #end
    #if($acc_leds>2)
      #set($i = $i + 1)
      set light $i name="Acc${my_accessory}${name_accessory}_Light2"
    #end
    #if($acc_leds>3)
      #set($i = $i + 1)
      set light $i name="Acc${my_accessory}${name_accessory}_Light3"
    #end
    #if($acc_leds>4)
      #set($i = $i + 1)
      set light $i name="Acc${my_accessory}${name_accessory}_Light4"
    #end
    #if($acc_leds>5)
      #set($i = $i + 1)
      set light $i name="Acc${my_accessory}${name_accessory}_Light5"
    #end
    #if($acc_leds>6)
      #set($i = $i + 1)
      set light $i name="Acc${my_accessory}${name_accessory}_Light6"
    #end
    #if($acc_leds>7)
      #set($i = $i + 1)
      set light $i name="Acc${my_accessory}${name_accessory}_Light7"
    #end

  #######################################
  #else
    # Number of LEDs not in the allowed range

    # An error needs to be reported to the user!

  #end

#end
