### Dieses Script definiert ein Asseccory, 
### um die fuenf/sechs LED eines Lichteinfahrsignal (DB)
### anzusteuern.
###
### Hardware: 3 Lightport (Port 0 = gn, 1 = rt, 2 = ge)
### Software: 1 accessory
###           3 macros, 0 = Hp0, 1 = Hp1, 2 = Hp2
###
### history: 2016-12-06 A.Tillner, erste Version
###          2017-01-17 A.Tillner, 
###                     - Dimming unabhängig vom Node 
###                     - Accessory Namen eingeben 
###          2017-05-20 A.Tillner, gn bei Hp2 fehlte
###
### how to use:
### A) configure
###    In section 1, define your ports and the desired accessory number
###    In section 2, define your settings (like brightness and speed)
### B) load and run this script
###    what will happen?
###    1. the ports will be configured with the given parameters
###    2. three new macros for the aspects will be defined
###    3. these macros will be assigned to the accessory
### C) have fun
###
### -------------- 1. Select Target
##application(text:de="DB H/V Licht-Einfahrsignal", text:en="DB H/V entry signal")
##instruction(text:de="DB H/V Licht-Einfahrsignal: <br>Lightports = gn, rt, ge <br>Macros = Hp0, Hp1, Hp2", text:en="DB H/V entry signal: <br>lightports = gn, rt, ge <br>macros = Hp0, Hp1, Hp2")
###
##input($AccessoryName:string, text:de="Name des Einfahrsignals-Accessories: ", text:en="Name of the entry signal Accessory: ", default="Einfahrsignal")
##input($my_accessory:accessory, text:de="Nummer des zu erzeugenden Accessory: ", text:en="Nummber of the Accessory: ", default=0)
##input($my_macro:macro, text:de="Nummer des ersten Macros (Hp0): ", text:en="Number of the first macro: ", default=0)
##input($start_led:light, text:de="Nummer des ersten Lightport (gn LED): ", text:en="Number of the first Lightport: ", default=0)
##input($prevent_replace_labels:boolean, text:de="Keine Namen füer Accessory, Makros, Ports ersetzen", text:en="Prevent replace labels for accessory, macro and ports", default=false)

#set($led_gap = 1)           ### die anderen LED sind an den folgenden  Lightports angeschlossen
###
### -------------- 2. set Parameters for Ports
#set($WertbeiOff = 0)
#set($WertbeiOn = 200)
#set($Ueberblenden = 15)
#set($OnTime = 0)

#if (${dimm_range} == 8)  ### Es handelt sich um einen Knoten mit 8 Bit Dimming
#set($DimmzeitOff = 10)
#set($DimmzeitOn = 10)
#set($DimmzeitOffParam = "DimmOff=${DimmzeitOff}")
#set($DimmzeitOnParam = "DimmOn=${DimmzeitOn}")
#else                     ### Es handelt sich um einen Knoten mit 16 Bit Dimming
#set($DimmzeitOff = 2500)
#set($DimmzeitOn = 2500)
#set($DimmzeitOffParam = "DimmOff88=${DimmzeitOff}")
#set($DimmzeitOnParam = "DimmOn88=${DimmzeitOn}")
#end

### -------------- 3. execute part of the script

#set($macro0 = $my_macro)    ### need some additional macros
#set($macro1 = $macro0 + 1)    ### need some additional macros
#set($macro2 = $macro0 + 2)    ### need some additional macros
#set($led_gn = $start_led)    ### LED gn an erstem lightport, der eingegeben wurde
#set($led_rt = 1 * $led_gap + $start_led) ### LED rt an nächstem lightport
#set($led_ge = 2 * $led_gap + $start_led)  ### LED ge an nächstem lightport

########################################
### Setzt label, wenn ${prevent_replace_labels} NICHT true
#if (!${prevent_replace_labels})
### 
### Set label of Ports
set light ${led_rt} name="${AccessoryName}_${my_accessory}_${led_rt}_rt"
set light ${led_gn} name="${AccessoryName}_${my_accessory}_${led_gn}_gn"
set light ${led_ge} name="${AccessoryName}_${my_accessory}_${led_ge}_ge"

### Set label of Macro
set macro ${macro0} name="${AccessoryName}_${my_accessory}_Hp0"
set macro ${macro1} name="${AccessoryName}_${my_accessory}_Hp1"
set macro ${macro2} name="${AccessoryName}_${my_accessory}_Hp2"

### Set label of Accessories
set accessory ${my_accessory} name="${AccessoryName}_${my_accessory}"

#end
########################################

### 
### Ports: (set Parameters)
config port ptype=light ValueOff=${WertbeiOff} ValueOn=${WertbeiOn} DimmOff=${DimmzeitOff} DimmOn=${DimmzeitOn} number=${led_rt}
config port ptype=light ValueOff=${WertbeiOff} ValueOn=${WertbeiOn} DimmOff=${DimmzeitOff} DimmOn=${DimmzeitOn} number=${led_ge}
config port ptype=light ValueOff=${WertbeiOff} ValueOn=${WertbeiOn} DimmOff=${DimmzeitOff} DimmOn=${DimmzeitOn} number=${led_gn}



################################################/
### Macro Ausfahrsignal Hp0
###
select macro ${macro0}
config macro repeat=1 slowdown=1
### Signal dunkel
add step ptype=light action=down number=${led_rt}
add step ptype=light action=down number=${led_ge}
add step ptype=light action=down number=${led_gn}

### Hp0 setzten, $Ueberblenden Ticks dunkel
add step ptype=light action=up delay=${Ueberblenden} number=${led_rt}
###
### Ende Macro Ausfahrsignal Hp0
###
################################################/
### Macro Ausfahrsignal Hp1
###
select macro ${macro1}
config macro repeat=1 slowdown=1
### Signal dunkel
add step ptype=light action=down number=${led_rt}
add step ptype=light action=down number=${led_ge}
add step ptype=light action=down number=${led_gn}

### Hp1 setzten, $Ueberblenden Ticks dunkel
add step ptype=light action=up delay=${Ueberblenden} number=${led_gn}
###
### Ende Macro Ausfahrsignal Hp1
###
################################################/
### Macro Ausfahrsignal Hp2
###
select macro ${macro2}
config macro repeat=1 slowdown=1
### Signal dunkel
add step ptype=light action=down number=${led_rt}
add step ptype=light action=down number=${led_ge}
add step ptype=light action=down number=${led_gn}

### Hp2 setzten, $Ueberblenden Ticks dunkel
add step ptype=light action=up delay=${Ueberblenden} number=${led_ge}
add step ptype=light action=up number=${led_gn}
###
### Ende Macro Ausfahrsignal Hp2
###
################################################/

### Define Accessory
select accessory ${my_accessory}
add aspect 0 macroname="${AccessoryName}_${my_accessory}_Hp0"
add aspect 1 macroname="${AccessoryName}_${my_accessory}_Hp1"
add aspect 2 macroname="${AccessoryName}_${my_accessory}_Hp2"
