### Dieses Script definiert ein Asseccory, 
### um die vier LED eines �BB Verschubsignals (1980)
### anzusteuern.
###
### Hardware: 2 Lightport (Port 0 = ws_links u. ws_rechts, 1 = ws_oben u. ws_unten)
### Software: 1 accessory
###           2 macros, 0 = Verbot, 1 = Frei
###
### history: 2017-03-19 A.Tillner, erste Version
###
### how to use:
### A) configure
###    In section 1, define your ports and the desired accessory number
###    In section 2, define your settings (like brightness and speed)
### B) load and run this script
###    what will happen?
###    1. the ports will be configured with the given parameters
###    2. three new macros for the aspects will be defined
###    3. these macros will be assigned to the accessory
### C) have fun
###
### -------------- 1. Select Target
##application(text:de="�BB IV-VI Verschubsignal, 2-begriffig", text:en="�BB IV-VI Shunting signal, 2 Aspects")
##instruction(text:de="�BB IV-VI Verschubsignal, 2-begriffig: <br>Lightports = ws_horizontal, ws_diagonal <br>Macros = Verbot, Frei", text:en="�BB IV-VI Shunting signal, 2 Aspects: <br>lightports = ws_horizontal, ws_diagonal<br>macros = forbidden, allowed")
###
##input($AccessoryName:string, text:de="Name des Verschubsignal-Accessories: ", text:en="Name of the shunting signal Accessory: ", default="Verschubsignal, 2-begriffig")
##input($my_accessory:accessory, text:de="Nummer des zu erzeugenden Accessory: ", text:en="Nummber of the Accessory: ", default=0)
##input($my_macro:macro, text:de="Nummer des ersten Macros (Halt): ", text:en="Number of the first macro (Halt): ", default=0)
##input($start_led:light, text:de="Nummer des ersten Lightport (ws_horizontal): ", text:en="Number of the first Lightport (ws_horizontal): ", default=0)
##input($prevent_replace_labels:boolean, text:de="Keine Namen fuer Accessory, Makros, Ports ersetzen", text:en="Prevent replace labels for accessory, macro and ports", default=false)

#set($led_gap = 1)           ### die anderen vier LED sind an den folgenden vier Lightports angeschlossen
###
### -------------- 2. set Parameters for Ports
#set($WertbeiOff = 0)
#set($WertbeiOn = 200)
#set($Ueberblenden = 30)
#set($OnTime = 0)

#if (${dimm_range} == 8)  ### Es handelt sich um einen Knoten mit 8 Bit Dimming
#set($DimmzeitOff = 10)
#set($DimmzeitOn = 10)
#set($DimmzeitOffParam = "DimmOff=${DimmzeitOff}")
#set($DimmzeitOnParam = "DimmOn=${DimmzeitOn}")
#else                     ### Es handelt sich um einen Knoten mit 16 Bit Dimming
#set($DimmzeitOff = 2500)
#set($DimmzeitOn = 2500)
#set($DimmzeitOffParam = "DimmOff88=${DimmzeitOff}")
#set($DimmzeitOnParam = "DimmOn88=${DimmzeitOn}")
#end

### -------------- 3. execute part of the script

#set($macro0 = $my_macro)    ### need some additional macros
#set($macro1 = $macro0 + 1)  ### need some additional macros
#set($led_ws_horizontal = $start_led)                 ### LED gn_links an erstem lightport, der eingegeben wurde
#set($led_ws_diagonal = 1 * $led_gap + $start_led)  ### LED gn_rechts an n�chstem lightport

########################################
### Setzt label, wenn ${prevent_replace_labels} NICHT true
#if (!${prevent_replace_labels})
### 
### Set label of Ports
set light ${led_ws_horizontal} name="${AccessoryName}_${my_accessory}_${led_ws_horizontal}_ws_horizontal"
set light ${led_ws_diagonal} name="${AccessoryName}_${my_accessory}_${led_ws_diagonal}_ws_diagonal"

### Set label of Macro
set macro ${macro0} name="${AccessoryName}_${my_accessory}_Verbot"
set macro ${macro1} name="${AccessoryName}_${my_accessory}_Erlaubt"

### Set label of Accessories
set accessory ${my_accessory} name="${AccessoryName}_${my_accessory}"

#end
########################################

### 
### Ports: (set Parameters)
config port ptype=light ValueOff=${WertbeiOff} ValueOn=${WertbeiOn} ${DimmzeitOffParam} ${DimmzeitOnParam} number=${led_ws_horizontal}
config port ptype=light ValueOff=${WertbeiOff} ValueOn=${WertbeiOn} ${DimmzeitOffParam} ${DimmzeitOnParam} number=${led_ws_diagonal}


################################################/
### Macro Verschubsignal - Verschubverbot
###
select macro ${macro0}
config macro repeat=1 slowdown=1
### nicht benoetigte LED's ausschalten
add step ptype=light action=down number=${led_ws_diagonal}

### Vorsicht setzten, nach $Ueberblenden Ticks
add step ptype=light action=up delay=${Ueberblenden} number=${led_ws_horizontal}
###
### Ende Macro Verschubsignal - Verschubverbot
###
################################################/
### Macro Verschubsignal - Verschubverbot aufgehoben
###
select macro ${macro1}
config macro repeat=1 slowdown=1
### nicht benoetigte LED's ausschalten
add step ptype=light action=down number=${led_ws_horizontal}

### Vorsicht setzten, nach $Ueberblenden Ticks
add step ptype=light action=up delay=${Ueberblenden} number=${led_ws_diagonal}
##
### Ende Macro Verschubsignal - Verschubverbot aufgehoben
###
################################################/

### Define Accessory
select accessory ${my_accessory}
add aspect 0 macroname="${AccessoryName}_${my_accessory}_Verbot"
add aspect 1 macroname="${AccessoryName}_${my_accessory}_Erlaubt"
