set TAG_NAME=3.0.3
set NEXT_DEV_VERSION=3.0.4-SNAPSHOT

set PROJECT_NAME=vldocking

set SVN_USERNAME=<your-svn-username>
set SVN_PASSWORD=<your-svn-password>

rem the local path to the directory where the release will be performed (clean checkout, build, create tag, build release version)
SET LOCAL_RELEASE_BASE=D:\release

SET JAVA_HOME=C:\Program Files\Java\jdk1.6.0_39
SET M2_HOME=D:\tools\apache-maven-3.0.4
SET ANT_HOME=D:\tools\apache-ant-1.8.4
SET SVN_HOME=D:\tools\svn-win64-1.7.8

SET SVN_BASE_PATH=https://svn.code.sf.net/p/bidibwizard/code/trunk/additional
SET SVN_TAGS_PATH=https://svn.code.sf.net/p/bidibwizard/code/tags/vldocking

SET PATH=%M2_HOME%\bin;%JAVA_HOME%\bin;%SVN_HOME%\bin;%ANT_HOME%\bin;%PATH%

set MAVEN_OPTS=-Xmx1024m -XX:MaxPermSize=256m
