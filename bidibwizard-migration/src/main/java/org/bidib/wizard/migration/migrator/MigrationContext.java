package org.bidib.wizard.migration.migrator;

import java.util.LinkedHashMap;
import java.util.Map;

import org.bidib.wizard.common.context.ApplicationContext;

public class MigrationContext implements ApplicationContext {
    private Map<String, Object> registry = new LinkedHashMap<>();

    @Override
    public Object register(String key, Object content) {
        return registry.put(key, content);
    }

    @Override
    public Object unregister(String key) {
        return registry.remove(key);
    }

    @Override
    public Object get(String key) {
        return registry.get(key);
    }

    @Override
    public <T> T get(String key, Class<T> type) {
        return get(type, get(key));
    }

    protected <T> T get(Class<T> type, Object body) {
        // if same type
        if (type.isInstance(body)) {
            return type.cast(body);
        }
        return null;
    }
}
