package org.bidib.wizard.migration.labels;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.bidib.jbidibc.core.schema.BidibFactory;
import org.bidib.jbidibc.core.schema.bidiblabels.NodeLabels;
import org.bidib.jbidibc.core.utils.NodeUtils;
import org.bidib.wizard.common.context.ApplicationContext;
import org.bidib.wizard.migration.migrator.MigrationException;
import org.bidib.wizard.migration.schema.nodes.NodeLabel;
import org.bidib.wizard.migration.schema.nodes.Nodes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Bidib2LabelMigrator extends AbstractWizardLabelMigrator {

    private static final Logger LOGGER = LoggerFactory.getLogger(Bidib2LabelMigrator.class);

    private static final String INPUT_XSL = "/migration/labels-migration.xsl";

    private static final String BACKUP_DIR = "migrated";

    public static String moveSourceFileToBackupDir(String searchpath, long uniqueId) {
        LOGGER.info("Move source file to backup dir for uniqueId: {}, searchpath: {}", uniqueId, searchpath);

        StringBuilder sb = new StringBuilder();
        try {
            String nodeFileName = BidibFactory.prepareNodeFilename(uniqueId);
            LOGGER.info("Search for node file: {}", nodeFileName);

            File migratedDir = new File(searchpath, BACKUP_DIR);
            if (!migratedDir.exists()) {
                migratedDir.mkdirs();
            }

            File source = new File(searchpath, nodeFileName);
            File target = new File(migratedDir, nodeFileName);

            LOGGER.info("Move file from: '{}' to: '{}'", source, target);
            Files.move(source.toPath(), target.toPath(), StandardCopyOption.REPLACE_EXISTING);

            sb.append("Moved file from: '").append(source).append("' to: '").append(target).append("'.");
        }
        catch (Exception ex) {
            LOGGER.warn("Move source file to backup dir for uniqueId: {}, searchpath: {}", uniqueId, searchpath, ex);
        }

        return sb.toString();
    }

    @Override
    public NodeLabels performWizardLabelsMigration(final ApplicationContext context, long uniqueId, String searchpath) {
        LOGGER.info("perform bidib2 migration for uniqueId: {}", uniqueId);

        NodeLabels nodeLabels = null;
        FileInputStream dataXML = null;
        try {

            String nodeFileName = BidibFactory.prepareNodeFilename(uniqueId);
            LOGGER.info("Search for node file: {}", nodeFileName);

            dataXML = new FileInputStream(new File(searchpath, nodeFileName));
            LOGGER.info("Prepared dataXML: {}", dataXML);

            Map<String, String> params = new HashMap<>();
            params.put("search_uniqueId", Long.toString(uniqueId));

            nodeLabels = performMigration(params, dataXML, INPUT_XSL);
        }
        catch (MigrationException ex) {
            LOGGER.warn("Perform transformation of bidib2 migration failed for uniqueId: {}",
                NodeUtils.formatHexUniqueId(uniqueId), ex);

            throw new MigrationException("Perform transformation for label migration failed for uniqueId: "
                + NodeUtils.formatHexUniqueId(uniqueId), ex.getCause());
        }
        catch (Exception ex) {
            // TODO: handle exception
            LOGGER.warn("performBidib2Migration failed.", ex);
        }
        finally {
            if (dataXML != null) {
                try {
                    dataXML.close();
                }
                catch (IOException ex) {
                    LOGGER.warn("Close dataXML stream failed.", ex);
                }
            }
        }

        return nodeLabels;
    }

    @Override
    public Nodes findAllNodesInWizardLabels(String searchpath) {
        // search all .xml files
        Iterator<File> labelFiles = FileUtils.iterateFiles(new File(searchpath), new String[] { "xml" }, false);

        Nodes nodes = null;

        while (labelFiles.hasNext()) {
            File labelFile = labelFiles.next();
            LOGGER.info("Found labelFile: {}", labelFile);

            String fileName = FilenameUtils.getBaseName(labelFile.getName());

            long uniqueId = NodeUtils.parseUniqueId(fileName);

            if (nodes == null) {

                // create default empty Nodes instance
                nodes = new Nodes().withSearchPath(searchpath);
            }

            nodes.withNodeLabel(new NodeLabel().withUniqueId(uniqueId));
        }

        if (nodes != null) {
            LOGGER.info("Set the migrator class.");
            nodes.setMigratorClass(getClass().getName());
        }

        return nodes;
    }

}
