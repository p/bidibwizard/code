package org.bidib.wizard.migration.progress;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.JFrame;
import javax.swing.Timer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MigrationProgressFrame extends JFrame {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(MigrationProgressFrame.class);

    private Object closeLock = new Object();

    private Timer closeChecker;

    private AtomicBoolean finished = new AtomicBoolean();

    public MigrationProgressFrame(String title) {
        super(title);
        setIconImage(ImageUtils.createImageIcon(getClass(), "/icons/frameIcon_48x48.png").getImage());
        setAlwaysOnTop(true);
    }

    public void startCloseChecker() {
        closeChecker = new Timer(1000, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                LOGGER.info("Check if migration has finished.");
                synchronized (closeLock) {
                    try {
                        closeLock.wait(2000);

                        if (finished.get()) {
                            LOGGER.info("Migration has finished.");
                            closeChecker.stop();

                            MigrationProgressFrame.this.setVisible(false);
                        }
                        else {
                            LOGGER.info("Migration has not finished yet.");
                        }
                    }
                    catch (InterruptedException ex) {
                        LOGGER.warn("Wait for close lock was interrupted.", ex);
                    }
                }
            }
        });
        closeChecker.setInitialDelay(100);
        closeChecker.start();
    }

    public Object getCloseLock() {
        return closeLock;
    }

    public AtomicBoolean getFinished() {
        return finished;
    }
}
