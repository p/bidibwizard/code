package org.bidib.wizard.migration.migrator;

public class MigrationException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public MigrationException(String message, Throwable cause) {
        super(message, cause);
    }
}
