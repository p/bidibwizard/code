package org.bidib.wizard.migration.migrator;

import java.awt.BorderLayout;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.bidib.jbidibc.core.schema.bidiblabels.LabelFactory;
import org.bidib.jbidibc.core.schema.bidiblabels.NodeLabels;
import org.bidib.jbidibc.core.utils.NodeUtils;
import org.bidib.wizard.common.context.ApplicationContext;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.migration.labels.AbstractWizardLabelMigrator;
import org.bidib.wizard.migration.labels.Bidib2LabelMigrator;
import org.bidib.wizard.migration.labels.OldWizardLabelMigrator;
import org.bidib.wizard.migration.labels.WizardLabelMigrator;
import org.bidib.wizard.migration.progress.MigrationProgressFrame;
import org.bidib.wizard.migration.schema.nodes.Nodes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WizardMigrator {

    private static final Logger LOGGER = LoggerFactory.getLogger(WizardMigrator.class);

    private static final String MIGRATION_FILE = "wizard-migration.log";

    private static final String MIGRATION_LABELS_MARKER = "## Transformation of wizard labels performed.";

    private static final String EOL = "\r\n";

    private MigrationProgressFrame frame;

    /**
     * the migration worker
     */
    private final ScheduledExecutorService migrationWorkers = Executors.newScheduledThreadPool(1);

    /**
     * Migrate the labels.
     * <p>
     * The new (bidib2) labels are stored in the labels subfolder, e.g. {@code <user.home>\.bidib\labels}. The older
     * labels are stored under {@code <user.home>\.BiDiBWizard} or at a customized location.
     * </p>
     * 
     * @param context
     *            the migration context
     * @param uniqueId
     *            the uniqueId of the node
     * @param searchpath
     *            the search path. The provided paths are the root path of the locations of the labels, e.g.
     *            {@code <user.home>\.bidib} or {@code <user.home>\.BiDiBWizard}.
     * @return the node labels
     */
    protected NodeLabels migrateLabels(
        final ApplicationContext context, Class<?> migratorClass, long uniqueId, String... searchpaths) {
        LOGGER.info("Migrate labels for node with uniqueId: {}, migratorClass: {}, searchpaths: {}", uniqueId,
            migratorClass, searchpaths);

        NodeLabels nodeLabels = null;

        for (String searchpath : searchpaths) {

            // check which migration is needed
            try {

                if (migratorClass != null) {
                    LOGGER.info("Use migrator to convert labels: {}", migratorClass);

                    AbstractWizardLabelMigrator wizardLabelMigrator =
                        (AbstractWizardLabelMigrator) migratorClass.newInstance();
                    nodeLabels = wizardLabelMigrator.performWizardLabelsMigration(context, uniqueId, searchpath);
                }
                else {
                    LOGGER.info("Try all available migrators to convert labels.");
                    Bidib2LabelMigrator bidib2LabelMigrator = new Bidib2LabelMigrator();
                    nodeLabels = bidib2LabelMigrator.performWizardLabelsMigration(context, uniqueId, searchpath);

                    if (nodeLabels != null) {
                        LOGGER.info("Migration of labels from Bidib2 format passed.");
                    }
                    else {
                        WizardLabelMigrator wizardLabelMigrator = new WizardLabelMigrator();
                        nodeLabels = wizardLabelMigrator.performWizardLabelsMigration(context, uniqueId, searchpath);

                        if (nodeLabels != null) {
                            LOGGER.info("Migration of labels from Wizard format passed.");
                        }
                        else {
                            OldWizardLabelMigrator oldWizardLabelMigrator = new OldWizardLabelMigrator();
                            nodeLabels =
                                oldWizardLabelMigrator.performWizardLabelsMigration(context, uniqueId, searchpath);
                        }
                    }
                }
            }
            catch (MigrationException ex) {
                LOGGER.warn("Perform transformation of bidib2 migration failed.", ex);

                throw new MigrationException("Perform transformation for label migration failed.", ex);
            }
            catch (Exception ex) {
                // TODO: handle exception
                LOGGER.warn("perform bidib2 or wizard labels migration failed.", ex);
            }

            if (nodeLabels != null) {
                LOGGER.info("Return found labels.");
                break;
            }
        }
        return nodeLabels;
    }

    /**
     * Find all nodes in wizard labels.
     * 
     * @param searchpath
     *            the search path
     * @return the node labels with all nodes
     */
    public Nodes findAllNodesInWizardLabels(final Class<?>[] migratorClassHolder, String... searchpaths) {

        Nodes nodes = null;

        for (String searchpath : searchpaths) {
            LOGGER.info("Search for nodes in path: {}", searchpath);
            // check which migration is needed
            try {

                Bidib2LabelMigrator bidib2LabelMigrator = new Bidib2LabelMigrator();
                nodes = bidib2LabelMigrator.findAllNodesInWizardLabels(searchpath);

                if (nodes != null && CollectionUtils.isNotEmpty(nodes.getNodeLabel())) {
                    LOGGER.info("Migration of labels from Bidib2 format passed.");
                    migratorClassHolder[0] = Bidib2LabelMigrator.class;
                }
                else {
                    WizardLabelMigrator wizardLabelMigrator = new WizardLabelMigrator();
                    nodes = wizardLabelMigrator.findAllNodesInWizardLabels(searchpath);

                    if (nodes != null && CollectionUtils.isNotEmpty(nodes.getNodeLabel())) {
                        LOGGER.info("Migration of labels from Wizard format passed.");
                        migratorClassHolder[0] = WizardLabelMigrator.class;
                    }
                    else {
                        OldWizardLabelMigrator oldWizardLabelMigrator = new OldWizardLabelMigrator();
                        nodes = oldWizardLabelMigrator.findAllNodesInWizardLabels(searchpath);

                        if (nodes != null && CollectionUtils.isNotEmpty(nodes.getNodeLabel())) {
                            LOGGER.info("Migration of node labels from old Wizard format passed.");
                            migratorClassHolder[0] = OldWizardLabelMigrator.class;
                        }
                    }
                }
            }
            catch (Exception ex) {
                // TODO: handle exception
                LOGGER.warn("perform bidib2 or wizard labels migration failed.", ex);
            }

            if (nodes != null) {
                LOGGER.info("Return found labels in searchPath: {}", searchpath);
                nodes.setSearchPath(searchpath);
                break;
            }
        }

        return nodes;
    }

    private enum MigrationTaskType {
        labelMigration;
    }

    /**
     * Check if migration is required and perform the migration is necessary.
     * 
     * @param targetPath
     *            the target path were the migration file s are stored
     * @param searchRoots
     *            the search roots
     * @param migrationLogFile
     *            the migration log file
     */
    public String checkAndPerformMigration(String targetPath, final File migrationLogFile, String... searchRoots) {

        // check if the migration file exists
        try {
            LOGGER.info("Check if the migration log exists under: {}", migrationLogFile);
            if (migrationLogFile.exists() && scanFileForMarker(migrationLogFile, MIGRATION_LABELS_MARKER)) {
                LOGGER.info("The labels migration was performed already, the migration log exists under: {}",
                    migrationLogFile);

                return null;
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Check for migration failed.", ex);
        }

        LOGGER.info("Perform migration of labels, targetPath: {}", targetPath);

        File migrationLogFileTemp = migrationLogFile;

        // TODO check if the 2nd searchPath is different and show a message dialog
        if (searchRoots.length > 1 && !targetPath.equalsIgnoreCase(searchRoots[1])) {
            LOGGER.info("The legacy labels are not stored at the default location: {}", searchRoots[1]);

            int result =
                JOptionPane.showConfirmDialog(null,
                    Resources.getString(getClass(), "migration_alternate_location.text", searchRoots[1], targetPath),
                    Resources.getString(getClass(), "migration_alternate_location.title"), JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE);

            if (result == JOptionPane.YES_OPTION) {

                File bidibBaseDir = new File(searchRoots[1], BIDIB_DEFAULT_LABEL_SUBDIR);

                try {
                    LOGGER.info("Make sure the directory exists: {}", bidibBaseDir);
                    bidibBaseDir.mkdirs();
                }
                catch (Exception ex) {
                    LOGGER.warn("Create new labels directory at custom location failed: {}", bidibBaseDir, ex);
                }

                targetPath = bidibBaseDir.getPath();
                LOGGER.info("Changed target path to location: {}", targetPath);

                // must change location of logfile
                migrationLogFileTemp = getMigrationLogfile(targetPath);
            }
        }

        executeMigrationTask(targetPath, MigrationTaskType.labelMigration, migrationLogFileTemp, searchRoots);

        return targetPath;
    }

    private static final String BIDIB_DEFAULT_LABEL_SUBDIR = "labels";

    protected void executeMigrationTask(
        String targetPath, final MigrationTaskType migrationTask, final File migrationLogFile, String... searchRoots) {
        LOGGER.info("Execute migration task for task type: {}", migrationTask);

        FileWriter migrationFileWriter = null;

        try {
            String headless = System.getProperty("java.awt.headless");
            boolean isHeadless = false;
            if (StringUtils.isNotEmpty(headless)) {
                try {
                    isHeadless = Boolean.parseBoolean(headless);
                }
                catch (Exception ex) {
                    LOGGER.warn("Convert headless flag failed.", ex);
                }
            }

            if (!isHeadless) {
                SwingUtilities.invokeAndWait(new Runnable() {
                    public void run() {

                        JFrame frame = createMigrationFrame();
                        LOGGER.info("Created migrationFrame: {}", frame);
                    }
                });
            }
            else {
                LOGGER.info("Do not show migration frame on headless env.");
            }

            if (!migrationLogFile.getParentFile().exists()) {
                migrationLogFile.getParentFile().mkdirs();
            }

            // prepare migration logger
            migrationLogFile.createNewFile();

            migrationFileWriter = new FileWriter(migrationLogFile, true);
            migrationFileWriter
                .write("Migration started: " + new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(new Date()) + EOL);

            AbstractMigrationTask migrationTaskTask = null;
            switch (migrationTask) {

                default:
                    migrationTaskTask = new LabelMigrationTask(targetPath, migrationFileWriter, frame, searchRoots);
                    break;
            }

            // migrate old labels
            Future<?> future = startMigrationWorker(migrationFileWriter, migrationTaskTask);
            LOGGER.info("Wait for completion of task: {}", migrationTask);
            future.get();

            LOGGER.info("Migration task has finished: {}", migrationTask);
        }
        catch (Exception ex) {
            LOGGER.warn("Create migration logfile failed.", ex);
            if (migrationFileWriter != null) {
                try {
                    migrationFileWriter
                        .write("Migration failed: " + new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(new Date()));
                }
                catch (IOException ex1) {
                    LOGGER.warn("Write migration failed to migration logfile failed.", ex1);
                }
            }
        }
    }

    private Future<?> startMigrationWorker(
        final FileWriter migrationFileWriter, final AbstractMigrationTask migrationTask) {

        LOGGER.info("Start the migration worker. Submit task: {}", migrationTask);

        // add a task to the worker to let the migration works be performed
        return migrationWorkers.submit(migrationTask);
    }

    private abstract class AbstractMigrationTask implements Runnable {

        protected final FileWriter migrationFileWriter;

        protected final MigrationProgressFrame frame;

        protected AbstractMigrationTask(final FileWriter migrationFileWriter, final MigrationProgressFrame frame) {
            this.migrationFileWriter = migrationFileWriter;
            this.frame = frame;
        }

        @Override
        public void run() {
            try {
                LOGGER.info("Process the migration task.");

                doMigrationTask();

                LOGGER.info("Process the migration task passed.");

                migrationFileWriter.write(
                    "Migration finished: " + new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(new Date()) + EOL);

            }
            catch (Exception ex) {
                LOGGER.warn("Process the migration task failed.", ex);
            }
            finally {
                if (migrationFileWriter != null) {
                    try {
                        migrationFileWriter.close();
                    }
                    catch (IOException ex) {
                        LOGGER.warn("Close migration file writer failed.", ex);
                    }
                }

                if (frame != null) {
                    LOGGER.info("Signal that migration has finished.");
                    frame.getFinished().set(true);

                    // notify the task has finished
                    Object lock = frame.getCloseLock();
                    synchronized (lock) {
                        lock.notifyAll();
                    }
                    LOGGER.info("Signalled that migration has finished.");
                }
                else {
                    LOGGER.info("No frame available to signal that the migration has finished.");
                }
            }
        }

        protected abstract void doMigrationTask() throws Exception;
    }

    private final class LabelMigrationTask extends AbstractMigrationTask {

        private final String[] searchRoot;

        private final String targetPath;

        protected LabelMigrationTask(String targetPath, final FileWriter migrationFileWriter,
            final MigrationProgressFrame frame, String[] searchRoots) {
            super(migrationFileWriter, frame);
            this.searchRoot = searchRoots;
            this.targetPath = targetPath;
        }

        protected void doMigrationTask() throws Exception {
            LOGGER.info("Perform the migration of labels to targetPath: {}", targetPath);

            performLabelMigration(targetPath, migrationFileWriter, searchRoot);
            LOGGER.info("Perform the migration of labels passed.");
        }
    }

    private JFrame createMigrationFrame() {

        // open a frame to show migration is running
        frame = new MigrationProgressFrame(Resources.getString(WizardMigrator.class, "migrationFrame.title"));
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        JLabel progressLabel =
            new JLabel(Resources.getString(WizardMigrator.class, "migrationFrame.migrationInProgress"));
        progressLabel.setBorder(new EmptyBorder(5, 5, 5, 5));
        frame.getContentPane().add(progressLabel, BorderLayout.CENTER);
        frame.pack();
        frame.setSize(300, 100);
        frame.setLocationRelativeTo(null);
        frame.startCloseChecker();
        frame.setVisible(true);

        return frame;
    }

    private void performLabelMigration(String targetPath, final FileWriter migrationFileWriter, String[] searchPaths)
        throws IOException {
        LOGGER.info("Perform the label migration, targetPath: {}, searchPaths: {}.", targetPath,
            new Object[] { searchPaths });

        final Class<?>[] migratorClassHolder = new Class[1];

        // find all nodes in wizard labels
        Nodes nodes = findAllNodesInWizardLabels(migratorClassHolder, searchPaths);

        LOGGER.info("Found nodes: {}", nodes);
        LOGGER.info("Returned migratorClassHolder: {}", migratorClassHolder[0]);

        if (nodes == null) {
            LOGGER.info("No nodes to migrate found.");
            return;
        }

        String searchPathForLabels = nodes.getSearchPath();
        LOGGER.info("Found searchPathForLabels: {}", searchPathForLabels);

        ApplicationContext context = new MigrationContext();

        // create factory to store the migrated labels
        LabelFactory labelFactory = new LabelFactory();

        boolean errorDetected = false;

        for (org.bidib.wizard.migration.schema.nodes.NodeLabel nodeLabel : nodes.getNodeLabel()) {
            LOGGER.info("Collect labels for node: {}", nodeLabel);

            migrationFileWriter.write(
                "Process labels of node with uniqueId '" + NodeUtils.formatHexUniqueId(nodeLabel.getUniqueId()) + EOL);

            try {
                // migrate the labels of the current node

                NodeLabels nodeLabels =
                    migrateLabels(context, migratorClassHolder[0], nodeLabel.getUniqueId(), searchPathForLabels);

                LOGGER.info("Found nodeLabels: {}", nodeLabels);

                File file = new File(targetPath, LabelFactory.prepareNodeFilename(nodeLabel.getUniqueId()));
                LOGGER.info("Prepared file: {}", file);

                if (!file.getParentFile().exists()) {
                    file.getParentFile().mkdirs();
                }

                labelFactory.saveNodeLabel(nodeLabels, file, false);

                LOGGER.info("Saved labels of node with uniqueId: {}", nodeLabel.getUniqueId());

                migrationFileWriter.write("Saved labels of node with uniqueId '"
                    + NodeUtils.formatHexUniqueId(nodeLabel.getUniqueId()) + "' to file: " + file.getPath() + EOL);

                if (Bidib2LabelMigrator.class.getName().equals(nodes.getMigratorClass())) {

                    String result =
                        Bidib2LabelMigrator.moveSourceFileToBackupDir(nodes.getSearchPath(), nodeLabel.getUniqueId());
                    if (StringUtils.isNotBlank(result)) {
                        migrationFileWriter.write(result + EOL);
                    }
                }
            }
            catch (MigrationException ex) {
                LOGGER.warn("Perform transformation of bidib2 migration failed for uniqueId: {} ({})",
                    nodeLabel.getUniqueId(), NodeUtils.formatHexUniqueId(nodeLabel.getUniqueId()), ex);

                // set the error flag
                errorDetected = true;

                migrationFileWriter
                    .write("Perform transformation for label migration failed. Save labels of node with uniqueId '"
                        + NodeUtils.formatHexUniqueId(nodeLabel.getUniqueId()) + "' was skipped. Reason: "
                        + ex.getMessage() + EOL);
            }
        }

        if (!errorDetected) {
            LOGGER.info("The migration was performed without errors.");

            // TODO move the migrated files to a backup directory
        }

        LOGGER.info("Perform the label migration finished.");
    }

    /**
     * Scan the file for the provided marker.
     * 
     * @param migrationLogFile
     *            the migration log file
     * @param marker
     *            the marker to search
     * @return {@code true} if the marker was found, otherwise {@code false}
     */
    private boolean scanFileForMarker(final File migrationLogFile, String marker) {
        // search for marker in migration log file
        boolean performMigrationTask = true;
        Scanner scanner = null;
        try {
            scanner = new Scanner(migrationLogFile);
            while (scanner.hasNextLine()) {
                final String lineFromFile = scanner.nextLine();
                if (lineFromFile.contains(marker)) {
                    // a match!
                    LOGGER.info("Found '{}' in file: {}", marker, migrationLogFile.getName());
                    performMigrationTask = false;
                    break;
                }
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Scan migration log for uniqueId marker failed.", ex);
        }
        finally {
            if (scanner != null) {
                try {
                    scanner.close();
                }
                catch (Exception ex) {
                    LOGGER.warn("Close scanner failed.", ex);
                }
            }
        }
        return performMigrationTask;
    }

    public File getMigrationLogfile(String path) {

        File migrationLogFile = new File(path, MIGRATION_FILE);
        return migrationLogFile;
    }
}
