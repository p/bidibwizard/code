package org.bidib.wizard.migration.labels;

import java.io.InputStream;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.UnmarshalException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.bidib.jbidibc.core.exception.UnexpectedCharacterException;
import org.bidib.jbidibc.core.schema.BidibFactory;
import org.bidib.jbidibc.core.schema.bidiblabels.LabelFactory;
import org.bidib.jbidibc.core.schema.exception.InvalidSchemaException;
import org.bidib.wizard.migration.schema.nodes.Nodes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

public class NodesFactory {
    private static final Logger LOGGER = LoggerFactory.getLogger(NodesFactory.class);

    public static final String XSD_LOCATION = "/xsd/migrationHelper.xsd";

    public static final String XSD_LOCATION_BASE = "/xsd/bidib2Base.xsd";

    public static final String JAXB_SCHEMA_LOCATION =
        "http://www.bidib.org/schema/migration/1.0 xsd/migrationHelper.xsd";

    private static JAXBContext jaxbContext;

    public static Nodes loadNodes(final InputStream is) {

        Nodes nodes = null;
        try {

            if (jaxbContext == null) {
                LOGGER.info("Create the jaxb context for JAXB_PACKAGE: {}", Nodes.class.getPackage());
                jaxbContext = JAXBContext.newInstance(Nodes.class);
            }

            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            StreamSource streamSource = new StreamSource(BidibFactory.class.getResourceAsStream(XSD_LOCATION));
            StreamSource streamSourceBase = new StreamSource(LabelFactory.class.getResourceAsStream(XSD_LOCATION_BASE));
            Schema schema = schemaFactory.newSchema(new Source[] { streamSourceBase, streamSource });

            unmarshaller.setSchema(schema);

            XMLInputFactory factory = XMLInputFactory.newInstance();

            XMLStreamReader xmlr = factory.createXMLStreamReader(is);

            try {
                JAXBElement<Nodes> jaxbElement = (JAXBElement<Nodes>) unmarshaller.unmarshal(xmlr, Nodes.class);
                nodes = jaxbElement.getValue();
            }
            catch (UnmarshalException ex) {
                LOGGER.warn("Load content from file failed due to an unmarshal exception.", ex);
                if (ex.getLinkedException() instanceof SAXException) {
                    // validate(is);
                    throw new InvalidSchemaException("Load Nodes from file failed");
                }
                if (ex.getCause() instanceof XMLStreamException) {
                    XMLStreamException wex = (XMLStreamException) ex.getCause();
                    LOGGER.warn("The inner exception signals an unexpected character.");

                    throw new UnexpectedCharacterException(wex.getMessage());
                }
            }

        }
        catch (XMLStreamException ex) {
            LOGGER.warn("Load content from file failed.", ex);

            throw new UnexpectedCharacterException(ex.getMessage());
        }
        catch (JAXBException | SAXException ex) {
            LOGGER.warn("Load content from file failed.", ex);
        }
        return nodes;
    }
}
