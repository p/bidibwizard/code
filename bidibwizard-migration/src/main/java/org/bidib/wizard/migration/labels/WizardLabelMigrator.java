package org.bidib.wizard.migration.labels;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.bidib.jbidibc.core.exception.UnexpectedCharacterException;
import org.bidib.jbidibc.core.schema.bidiblabels.AccessoryLabels;
import org.bidib.jbidibc.core.schema.bidiblabels.FeedbackPortLabels;
import org.bidib.jbidibc.core.schema.bidiblabels.MacroLabels;
import org.bidib.jbidibc.core.schema.bidiblabels.NodeLabel;
import org.bidib.jbidibc.core.schema.bidiblabels.NodeLabels;
import org.bidib.wizard.common.context.ApplicationContext;
import org.bidib.wizard.migration.schema.nodes.Nodes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WizardLabelMigrator extends AbstractWizardLabelMigrator {

    private static final Logger LOGGER = LoggerFactory.getLogger(WizardLabelMigrator.class);

    private static final String INPUT3_XSL = "/migration/labels-migration3.xsl";

    private static final String INPUT3A_XSL = "/migration/labels-migration3a.xsl";

    /**
     * Find all nodes in wizard labels.
     * 
     * @param searchpath
     *            the search path
     * @return the node labels with all nodes
     */
    @Override
    public Nodes findAllNodesInWizardLabels(String searchpath) {
        LOGGER.info("Find all nodes in wizard labels, searchpath: {}", searchpath);

        Nodes nodes = null;
        try {

            // search in NodeLabels.labels
            nodes = performFindAllNodesInWizardLabels("NodeLabels.labels", INPUT3_XSL, searchpath);
            if (nodes == null) {
                LOGGER.info("No nodes found in searchPath: {}", searchpath);
                // create default empty Nodes instance
                // nodes = new Nodes();
                return nodes;
            }

            String migrationXsl = nodes.getMigrationXsl();
            if (StringUtils.isBlank(migrationXsl)) {
                migrationXsl = INPUT3_XSL;
            }

            Nodes macroNodes = performFindAllNodesInWizardLabels("MacroLabels.labels", migrationXsl, searchpath);
            if (macroNodes != null && CollectionUtils.isNotEmpty(macroNodes.getNodeLabel())) {
                // merge the nodes
                appendNodeLabels(nodes.getNodeLabel(), macroNodes.getNodeLabel());
            }
            Nodes accessoryNodes =
                performFindAllNodesInWizardLabels("AccessoryLabels.labels", migrationXsl, searchpath);
            if (accessoryNodes != null && CollectionUtils.isNotEmpty(accessoryNodes.getNodeLabel())) {
                // merge the nodes
                appendNodeLabels(nodes.getNodeLabel(), accessoryNodes.getNodeLabel());
            }
            Nodes feedbackPortNodes =
                performFindAllNodesInWizardLabels("FeedbackPortLabels.labels", migrationXsl, searchpath);
            if (feedbackPortNodes != null && CollectionUtils.isNotEmpty(feedbackPortNodes.getNodeLabel())) {
                // merge the nodes
                appendNodeLabels(nodes.getNodeLabel(), feedbackPortNodes.getNodeLabel());
            }
            Nodes switchPortNodes =
                performFindAllNodesInWizardLabels("SwitchPortLabels.labels", migrationXsl, searchpath);
            if (switchPortNodes != null && CollectionUtils.isNotEmpty(switchPortNodes.getNodeLabel())) {
                // merge the nodes
                appendNodeLabels(nodes.getNodeLabel(), switchPortNodes.getNodeLabel());
            }
            Nodes servoPortNodes =
                performFindAllNodesInWizardLabels("ServoPortLabels.labels", migrationXsl, searchpath);
            if (servoPortNodes != null && CollectionUtils.isNotEmpty(servoPortNodes.getNodeLabel())) {
                // merge the nodes
                appendNodeLabels(nodes.getNodeLabel(), servoPortNodes.getNodeLabel());
            }
            Nodes backlightPortNodes =
                performFindAllNodesInWizardLabels("BacklightPortLabels.labels", migrationXsl, searchpath);
            if (backlightPortNodes != null && CollectionUtils.isNotEmpty(backlightPortNodes.getNodeLabel())) {
                // merge the nodes
                appendNodeLabels(nodes.getNodeLabel(), backlightPortNodes.getNodeLabel());
            }
            Nodes lightPortNodes =
                performFindAllNodesInWizardLabels("LightPortLabels.labels", migrationXsl, searchpath);
            if (lightPortNodes != null && CollectionUtils.isNotEmpty(lightPortNodes.getNodeLabel())) {
                // merge the nodes
                appendNodeLabels(nodes.getNodeLabel(), lightPortNodes.getNodeLabel());
            }
            Nodes inputPortNodes =
                performFindAllNodesInWizardLabels("InputPortLabels.labels", migrationXsl, searchpath);
            if (inputPortNodes != null && CollectionUtils.isNotEmpty(inputPortNodes.getNodeLabel())) {
                // merge the nodes
                appendNodeLabels(nodes.getNodeLabel(), inputPortNodes.getNodeLabel());
            }

        }
        catch (Exception ex) {
            // TODO: handle exception
            LOGGER.warn("find all nodes in wizard labels failed.", ex);
        }

        if (nodes != null) {
            LOGGER.info("Set the migrator class.");
            nodes.setMigratorClass(getClass().getName());
        }

        return nodes;
    }

    @Override
    protected Nodes performFindAllNodesInWizardLabels(String fileName, String transformationXSL, String searchpath) {

        Nodes nodes = null;
        try {
            nodes = super.performFindAllNodesInWizardLabels(fileName, transformationXSL, searchpath);
            if (nodes != null) {
                nodes.setMigrationXsl(transformationXSL);
            }
        }
        catch (UnexpectedCharacterException ex) {
            LOGGER.warn("Perform find nodes with transformation failed due to an unexpected character.", ex);

            nodes = super.performFindAllNodesInWizardLabels(fileName, INPUT3A_XSL, searchpath);
            if (nodes != null) {
                nodes.setMigrationXsl(INPUT3A_XSL);
            }
        }

        return nodes;
    }

    /**
     * Perform migration from wizard labels to new node labels.
     * <p>
     * The old wizard labels of a node must be collected from multiple files, because each label type has it's own file.
     * </p>
     * 
     * @param context
     *            the migration context
     * @param uniqueId
     *            the uniqueId
     * @param dataXML
     *            the data XML
     * @return the nodeLabels that were migrated from dataXML
     */
    @Override
    public NodeLabels performWizardLabelsMigration(final ApplicationContext context, long uniqueId, String searchpath) {

        // the wizard labels must be collected from multiple files
        // collect the labels from all types ...

        // search for 'node'
        String nodeFileName = "NodeLabels.labels";
        LOGGER.info("Search for node file: {}", nodeFileName);

        Map<String, String> params = new HashMap<>();
        params.put("search_uniqueId", Long.toString(uniqueId));
        params.put("forced_porttype", "");

        String migrationXsl = context.get(KEY_MIGRATIONXSL, String.class);
        LOGGER.info("Fetched migrationXsl from context: {}", migrationXsl);
        if (StringUtils.isBlank(migrationXsl)) {
            migrationXsl = AbstractWizardLabelMigrator.INPUT2_XSL;
        }

        NodeLabels nodeLabels =
            performWizardLabelsMigration(params, nodeFileName,
                migrationXsl /* AbstractWizardLabelMigrator.INPUT2_XSL */, searchpath);

        if (nodeLabels == null || nodeLabels.getNodeLabel() == null) {
            LOGGER.info("No migrated node labels found for uniqueId: {}", uniqueId);
            if (nodeLabels == null) {
                nodeLabels = new NodeLabels();
            }

            nodeLabels.withNodeLabel(new NodeLabel().withUniqueId(uniqueId));
        }

        migrationXsl = params.get(KEY_MIGRATIONXSL);
        if (StringUtils.isBlank(migrationXsl)) {
            migrationXsl = AbstractWizardLabelMigrator.INPUT2_XSL;
        }
        else {
            context.register(KEY_MIGRATIONXSL, migrationXsl);
        }

        // search macro labels
        NodeLabels macroNodeLabels =
            performWizardLabelsMigration(params, "MacroLabels.labels", migrationXsl, searchpath);
        if (macroNodeLabels != null && macroNodeLabels.getMacroLabels() != null) {
            LOGGER.info("Merge macro labels for uniqueId: {}", uniqueId);
            // merge the macro labels into the node labels
            MacroLabels macroLabels = macroNodeLabels.getMacroLabels();
            nodeLabels.setMacroLabels(macroLabels);
        }
        else {
            LOGGER.info("No migrated macro labels found for uniqueId: {}", uniqueId);
        }

        // search accessory labels
        NodeLabels accessoryNodeLabels =
            performWizardLabelsMigration(params, "AccessoryLabels.labels", migrationXsl, searchpath);
        if (accessoryNodeLabels != null && accessoryNodeLabels.getAccessoryLabels() != null) {
            LOGGER.info("Merge accessory labels for uniqueId: {}", uniqueId);
            // merge the accessory labels into the node labels
            AccessoryLabels accessoryLabels = accessoryNodeLabels.getAccessoryLabels();
            nodeLabels.setAccessoryLabels(accessoryLabels);
        }
        else {
            LOGGER.info("No migrated accessory labels found for uniqueId: {}", uniqueId);
        }

        // search feedback port labels
        NodeLabels feedbackPortNodeLabels =
            performWizardLabelsMigration(params, "FeedbackPortLabels.labels", migrationXsl, searchpath);
        if (feedbackPortNodeLabels != null && feedbackPortNodeLabels.getFeedbackPortLabels() != null) {
            LOGGER.info("Merge feedback port labels for uniqueId: {}", uniqueId);
            // merge the feedback port labels into the node labels
            FeedbackPortLabels feedbackPortLabels = feedbackPortNodeLabels.getFeedbackPortLabels();
            nodeLabels.setFeedbackPortLabels(feedbackPortLabels);
        }
        else {
            LOGGER.info("No migrated feedback port labels found for uniqueId: {}", uniqueId);
        }

        // search servoport labels
        migrateAndAppendWizardPortLabels(nodeLabels, params, searchpath, "ServoPortLabels.labels", migrationXsl,
            "servo");

        // search switchport labels
        migrateAndAppendWizardPortLabels(nodeLabels, params, searchpath, "SwitchPortLabels.labels", migrationXsl,
            "switch");

        // search lightport labels
        migrateAndAppendWizardPortLabels(nodeLabels, params, searchpath, "LightPortLabels.labels", migrationXsl,
            "light");

        // search backlightport labels
        migrateAndAppendWizardPortLabels(nodeLabels, params, searchpath, "BacklightPortLabels.labels", migrationXsl,
            "backlight");

        // search inputport labels
        migrateAndAppendWizardPortLabels(nodeLabels, params, searchpath, "InputPortLabels.labels", migrationXsl,
            "input");

        return nodeLabels;
    }

    @Override
    protected NodeLabels performWizardLabelsMigration(
        Map<String, String> params, String fileName, String migrationXSL, String searchpath) {

        NodeLabels nodeLabels = null;
        try {
            nodeLabels = super.performWizardLabelsMigration(params, fileName, migrationXSL, searchpath);

            params.put(KEY_MIGRATIONXSL, migrationXSL);
        }
        catch (UnexpectedCharacterException ex) {
            LOGGER.warn("Perform find nodes with transformation failed due to an unexpected character.", ex);

            nodeLabels =
                super.performWizardLabelsMigration(params, fileName, AbstractWizardLabelMigrator.INPUT2A_XSL,
                    searchpath);

            params.put(KEY_MIGRATIONXSL, AbstractWizardLabelMigrator.INPUT2A_XSL);
        }

        return nodeLabels;
    }
}
