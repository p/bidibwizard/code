package org.bidib.wizard.migration.labels;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.bidib.jbidibc.core.schema.bidiblabels.AccessoryLabels;
import org.bidib.jbidibc.core.schema.bidiblabels.FeedbackPortLabels;
import org.bidib.jbidibc.core.schema.bidiblabels.MacroLabels;
import org.bidib.jbidibc.core.schema.bidiblabels.NodeLabel;
import org.bidib.jbidibc.core.schema.bidiblabels.NodeLabels;
import org.bidib.wizard.common.context.ApplicationContext;
import org.bidib.wizard.migration.migrator.MigrationException;
import org.bidib.wizard.migration.schema.nodes.Nodes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OldWizardLabelMigrator extends AbstractWizardLabelMigrator {

    private static final Logger LOGGER = LoggerFactory.getLogger(OldWizardLabelMigrator.class);

    protected static final String INPUT4_XSL = "/migration/labels-migration4.xsl";

    protected static final String INPUT5_XSL = "/migration/labels-migration5.xsl";

    /**
     * Perform migration from wizard labels to new node labels.
     * <p>
     * The old wizard labels of a node must be collected from multiple files, because each label type has it's own file.
     * </p>
     * 
     * @param context
     *            the migration context
     * @param uniqueId
     *            the uniqueId
     * @param dataXML
     *            the data XML
     * @return the nodeLabels that were migrated from dataXML
     */
    @Override
    public NodeLabels performWizardLabelsMigration(final ApplicationContext context, long uniqueId, String searchpath) {

        // TODO the wizard labels must be collected from multiple files
        // collect the labels from all types ...

        // search for 'node'
        String nodeFileName = "NodeLabels";
        LOGGER.info("Search for node file: {}", nodeFileName);

        Map<String, String> params = new HashMap<>();
        params.put("search_uniqueId", Long.toString(uniqueId));

        NodeLabels nodeLabels =
            performWizardLabelsMigration(params, nodeFileName, AbstractWizardLabelMigrator.INPUT2_XSL, searchpath);

        if (nodeLabels == null || nodeLabels.getNodeLabel() == null) {
            LOGGER.info("No migrated node labels found for uniqueId: {}", uniqueId);
            if (nodeLabels == null) {
                nodeLabels = new NodeLabels();
            }

            nodeLabels.withNodeLabel(new NodeLabel().withUniqueId(uniqueId));
        }

        // search macro labels
        params.put("XSL_FILE", "/migration/labels-migration4a.xsl");
        NodeLabels macroNodeLabels =
            performWizardLabelsMigration(params, "MacroLabels", AbstractWizardLabelMigrator.INPUT2_XSL, searchpath);
        if (macroNodeLabels != null && macroNodeLabels.getMacroLabels() != null) {
            LOGGER.info("Merge macro labels for uniqueId: {}", uniqueId);
            // merge the macro labels into the node labels
            MacroLabels macroLabels = macroNodeLabels.getMacroLabels();
            nodeLabels.setMacroLabels(macroLabels);
        }
        else {
            LOGGER.info("No migrated macro labels found for uniqueId: {}", uniqueId);
        }

        // search accessory labels
        params.put("XSL_FILE", "/migration/labels-migration4b.xsl");
        NodeLabels accessoryNodeLabels =
            performWizardLabelsMigration(params, "AccessoryLabels", AbstractWizardLabelMigrator.INPUT2_XSL, searchpath);
        if (accessoryNodeLabels != null && accessoryNodeLabels.getAccessoryLabels() != null) {
            LOGGER.info("Merge accessory labels for uniqueId: {}", uniqueId);
            // merge the accessory labels into the node labels
            AccessoryLabels accessoryLabels = accessoryNodeLabels.getAccessoryLabels();
            nodeLabels.setAccessoryLabels(accessoryLabels);
        }
        else {
            LOGGER.info("No migrated accessory labels found for uniqueId: {}", uniqueId);
        }

        // search feedback port labels
        params.put("XSL_FILE", "/migration/labels-migration4c.xsl");
        NodeLabels feedbackPortNodeLabels =
            performWizardLabelsMigration(params, "FeedbackPortLabels", AbstractWizardLabelMigrator.INPUT2_XSL,
                searchpath);
        if (feedbackPortNodeLabels != null && feedbackPortNodeLabels.getFeedbackPortLabels() != null) {
            LOGGER.info("Merge feedback port labels for uniqueId: {}", uniqueId);
            // merge the feedback port labels into the node labels
            FeedbackPortLabels feedbackPortLabels = feedbackPortNodeLabels.getFeedbackPortLabels();
            nodeLabels.setFeedbackPortLabels(feedbackPortLabels);
        }
        else {
            LOGGER.info("No migrated feedback port labels found for uniqueId: {}", uniqueId);
        }
        params.remove("XSL_FILE");

        // search servoport labels
        params.put("port_type", "servo");
        migrateAndAppendWizardPortLabels(nodeLabels, params, searchpath, "ServoPortLabels",
            AbstractWizardLabelMigrator.INPUT2_XSL, "servo");

        // search switchport labels
        params.put("port_type", "switch");
        migrateAndAppendWizardPortLabels(nodeLabels, params, searchpath, "SwitchPortLabels",
            AbstractWizardLabelMigrator.INPUT2_XSL, "switch");

        // search lightport labels
        params.put("port_type", "light");
        migrateAndAppendWizardPortLabels(nodeLabels, params, searchpath, "LightPortLabels",
            AbstractWizardLabelMigrator.INPUT2_XSL, "light");

        // search backlightport labels
        params.put("port_type", "backlight");
        migrateAndAppendWizardPortLabels(nodeLabels, params, searchpath, "BacklightPortLabels",
            AbstractWizardLabelMigrator.INPUT2_XSL, "backlight");

        // search inputport labels
        params.put("port_type", "input");
        migrateAndAppendWizardPortLabels(nodeLabels, params, searchpath, "InputPortLabels",
            AbstractWizardLabelMigrator.INPUT2_XSL, "input");

        return nodeLabels;
    }

    /**
     * Search labels of one type.
     * 
     * @param uniqueId
     *            the uniqueId of the node
     * @param fileName
     *            the filename to search
     * @param searchpath
     *            the search path
     * @return the labels or {@code null} if no labels were found
     */
    @Override
    protected NodeLabels performWizardLabelsMigration(
        final Map<String, String> params, String fileName, String migrationXSL, String searchpath) {

        LOGGER.info("perform wizard label migration for params: {}, fileName: {}, migrationXSL: {}, searchpath: {}",
            params, fileName, migrationXSL, searchpath);

        NodeLabels nodeLabels = null;
        FileInputStream dataXML = null;
        try {
            File searchFile = new File(searchpath, fileName);
            LOGGER.info("Search for node file: {}", searchFile);

            dataXML = new FileInputStream(searchFile);
            LOGGER.info("Prepared dataXML: {}", dataXML);

            String xslFile = params.get("XSL_FILE");
            if (StringUtils.isNotBlank(xslFile)) {
                LOGGER.info("Use XSL file: {}", xslFile);
                nodeLabels = performMigration(params, dataXML, xslFile);
            }
            else {
                nodeLabels = performMigration(params, dataXML, INPUT4_XSL);
            }
        }
        catch (MigrationException ex) {
            LOGGER.warn("Perform transformation of label migration failed for fileName: {}", fileName, ex);

            throw new MigrationException("Perform transformation for label migration failed for fileName: " + fileName,
                ex.getCause());
        }
        catch (FileNotFoundException ex) {
            // TODO: handle exception
            LOGGER.warn("Perform migration of old wizard labels was not possible: {}", ex.getMessage());
        }
        catch (Exception ex) {
            // TODO: handle exception
            LOGGER.warn("performWizardLabelsMigration failed.", ex);
        }
        finally {
            if (dataXML != null) {
                try {
                    dataXML.close();
                }
                catch (IOException ex) {
                    LOGGER.warn("Close dataXML stream failed.", ex);
                }
            }
        }

        return nodeLabels;
    }

    /**
     * Find all nodes in wizard labels.
     * 
     * @param searchpath
     *            the search path
     * @return the node labels with all nodes
     */
    @Override
    public Nodes findAllNodesInWizardLabels(String searchpath) {
        LOGGER.info("Find all nodes in old wizard labels, searchpath: {}", new Object[] { searchpath });

        Nodes nodes = null;
        try {

            // search in NodeLabels.labels
            nodes = performFindAllNodesInWizardLabels("NodeLabels", INPUT5_XSL, searchpath);
            if (nodes == null) {
                LOGGER.info("No nodes found in searchPath: {}", searchpath);
                // create default empty Nodes instance
                // nodes = new Nodes();
                // return nodes;
            }
            LOGGER.info("Found nodes in searchPath: {}", searchpath);
            LOGGER.info("Continue migration of other labels.");

            Nodes macroNodes = performFindAllNodesInWizardLabels("MacroLabels", INPUT5_XSL, searchpath);
            if (macroNodes != null && CollectionUtils.isNotEmpty(macroNodes.getNodeLabel())) {
                if (nodes == null) {
                    nodes = new Nodes();
                }
                // merge the nodes
                appendNodeLabels(nodes.getNodeLabel(), macroNodes.getNodeLabel());
            }
            Nodes accessoryNodes = performFindAllNodesInWizardLabels("AccessoryLabels", INPUT5_XSL, searchpath);
            if (accessoryNodes != null && CollectionUtils.isNotEmpty(accessoryNodes.getNodeLabel())) {
                if (nodes == null) {
                    nodes = new Nodes();
                }
                // merge the nodes
                appendNodeLabels(nodes.getNodeLabel(), accessoryNodes.getNodeLabel());
            }
            Nodes feedbackPortNodes = performFindAllNodesInWizardLabels("FeedbackPortLabels", INPUT5_XSL, searchpath);
            if (feedbackPortNodes != null && CollectionUtils.isNotEmpty(feedbackPortNodes.getNodeLabel())) {
                if (nodes == null) {
                    nodes = new Nodes();
                }
                // merge the nodes
                appendNodeLabels(nodes.getNodeLabel(), feedbackPortNodes.getNodeLabel());
            }
            Nodes switchPortNodes = performFindAllNodesInWizardLabels("SwitchPortLabels", INPUT5_XSL, searchpath);
            if (switchPortNodes != null && CollectionUtils.isNotEmpty(switchPortNodes.getNodeLabel())) {
                if (nodes == null) {
                    nodes = new Nodes();
                }
                // merge the nodes
                appendNodeLabels(nodes.getNodeLabel(), switchPortNodes.getNodeLabel());
            }
            Nodes servoPortNodes = performFindAllNodesInWizardLabels("ServoPortLabels", INPUT5_XSL, searchpath);
            if (servoPortNodes != null && CollectionUtils.isNotEmpty(servoPortNodes.getNodeLabel())) {
                if (nodes == null) {
                    nodes = new Nodes();
                }
                // merge the nodes
                appendNodeLabels(nodes.getNodeLabel(), servoPortNodes.getNodeLabel());
            }
            Nodes backlightPortNodes = performFindAllNodesInWizardLabels("BacklightPortLabels", INPUT5_XSL, searchpath);
            if (backlightPortNodes != null && CollectionUtils.isNotEmpty(backlightPortNodes.getNodeLabel())) {
                // merge the nodes
                appendNodeLabels(nodes.getNodeLabel(), backlightPortNodes.getNodeLabel());
            }
            Nodes lightPortNodes = performFindAllNodesInWizardLabels("LightPortLabels", INPUT5_XSL, searchpath);
            if (lightPortNodes != null && CollectionUtils.isNotEmpty(lightPortNodes.getNodeLabel())) {
                LOGGER.info("Found lightPort labels to merge: {}", lightPortNodes);
                if (nodes == null) {
                    nodes = new Nodes();
                }
                // merge the nodes
                appendNodeLabels(nodes.getNodeLabel(), lightPortNodes.getNodeLabel());
            }
            Nodes inputPortNodes = performFindAllNodesInWizardLabels("InputPortLabels", INPUT5_XSL, searchpath);
            if (inputPortNodes != null && CollectionUtils.isNotEmpty(inputPortNodes.getNodeLabel())) {
                if (nodes == null) {
                    nodes = new Nodes();
                }
                // merge the nodes
                appendNodeLabels(nodes.getNodeLabel(), inputPortNodes.getNodeLabel());
            }

        }
        catch (Exception ex) {
            // TODO: handle exception
            LOGGER.warn("find all nodes in old wizard labels failed.", ex);
        }

        if (nodes != null) {
            LOGGER.info("Set the migrator class.");
            nodes.setMigratorClass(getClass().getName());
        }

        return nodes;
    }
}
