<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="2.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.bidib.org/schema/migration/1.0"
	xmlns:base="http://www.bidib.org/schema/bidibbase/1.0"
	xmlns:wizard="http://www.bidib.org/wizard/labels" 
	exclude-result-prefixes="wizard" >
	
	<xsl:output method='xml' encoding="UTF-8" indent="yes" />
	<xsl:strip-space elements="*"/>
	
	<xsl:template match="wizard:Labels">
		<nodes>
			<xsl:apply-templates />
		</nodes>
	</xsl:template>
	
	<xsl:template match="wizard:Labels/wizard:labelNode" priority="10">
		<xsl:choose>
			<xsl:when test="wizard:label[@type='node']">
				<nodeLabel>
				<xsl:attribute name="userName">
					<xsl:value-of select="wizard:label/wizard:labelString" />
			    </xsl:attribute>
				<xsl:attribute name="uniqueId">
					<xsl:value-of select="wizard:uniqueId" />
			    </xsl:attribute>
				</nodeLabel>
			</xsl:when>
			<xsl:when test="wizard:label[@type='accessory']">
				<nodeLabel>
					<xsl:attribute name="uniqueId">
						<xsl:value-of select="wizard:uniqueId" />
				    </xsl:attribute>
				</nodeLabel>
			</xsl:when>
			<xsl:when test="wizard:label[@type='macro']">
				<nodeLabel>
					<xsl:attribute name="uniqueId">
						<xsl:value-of select="wizard:uniqueId" />
				    </xsl:attribute>
				</nodeLabel>
			</xsl:when>
			<xsl:when test="wizard:label[@type='feedbackPort']">
				<nodeLabel>
					<xsl:attribute name="uniqueId">
						<xsl:value-of select="wizard:uniqueId" />
				    </xsl:attribute>
				</nodeLabel>
			</xsl:when>
			<xsl:when test="wizard:label[contains('|backlightPort|lightPort|servoPort|switchPort|inputPort|', concat('|', @type, '|'))]">
				<nodeLabel>
					<xsl:attribute name="uniqueId">
						<xsl:value-of select="wizard:uniqueId" />
				    </xsl:attribute>
				</nodeLabel>
			</xsl:when>
			<xsl:otherwise>
<!-- 	            <xsl:value-of select="'not recognized'"/> -->
	        </xsl:otherwise>
		</xsl:choose>
	    <xsl:apply-templates />
	</xsl:template>
	
	<xsl:template match="wizard:Labels/wizard:labelNode/*">
	</xsl:template>
	
	<xsl:template match="wizard:Labels/wizard:labelNode">
	</xsl:template>
	

</xsl:stylesheet>
