<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="2.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.bidib.org/schema/nodeLabels/1.0"
	xmlns:bidib2="http://www.bidib.org/schema/bidib/2.0" 
	exclude-result-prefixes="bidib2" >
	
	<xsl:output method='xml' encoding="UTF-8" indent="yes" />
	<xsl:strip-space elements="*"/>
	
	<xsl:template name="hex2num">
	    <xsl:param name="hex"/>
	    <xsl:param name="num" select="0"/>
	    <xsl:param name="MSB" select="translate(substring($hex, 1, 1), 'abcdef', 'ABCDEF')"/>
	    <xsl:param name="value" select="string-length(substring-before('0123456789ABCDEF', $MSB))"/>
	    <xsl:param name="result" select="16 * $num + $value"/>
	    <xsl:choose>
	        <xsl:when test="string-length($hex) > 1">
	            <xsl:call-template name="hex2num">
	                <xsl:with-param name="hex" select="substring($hex, 2)"/>
	                <xsl:with-param name="num" select="$result"/>
	            </xsl:call-template>
	        </xsl:when>
	        <xsl:otherwise>
	            <xsl:value-of select="$result"/>
	        </xsl:otherwise>
	    </xsl:choose>
	</xsl:template>
 
	<xsl:template match="bidib2:BiDiB">
		<nodeLabels>
			<xsl:apply-templates />
		</nodeLabels>
	</xsl:template>
	
	<xsl:template match="bidib2:nodes">
		<xsl:for-each select="bidib2:node">
			<nodeLabel>
			<xsl:attribute name="userName">
				<xsl:value-of select="@userName" />
		    </xsl:attribute>
			<xsl:attribute name="uniqueId">
				<xsl:choose>
					<!-- Check if we must convert the uniqueId from hex to decimal number -->
					<xsl:when test="starts-with(@uniqueId, '0x')">
						<xsl:call-template name="hex2num">
						    <xsl:with-param name="hex"><xsl:value-of select="substring-after(@uniqueId, 'x')" /></xsl:with-param>
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="@uniqueId" />
					</xsl:otherwise>
				</xsl:choose>
		    </xsl:attribute>
			</nodeLabel>
		    <xsl:apply-templates />
		</xsl:for-each>
	</xsl:template>

	<xsl:template match="bidib2:feedbackPorts">
	<feedbackPortLabels>
	<xsl:for-each select="bidib2:feedbackPort">
		<portLabel>
		<xsl:attribute name="index">
			<xsl:value-of select="@number" />
	    </xsl:attribute>
		<xsl:attribute name="label">
			<xsl:value-of select="@name" />
	    </xsl:attribute>
		</portLabel>
	</xsl:for-each>
	</feedbackPortLabels>
	</xsl:template>
	
	<xsl:template match="bidib2:ports">
	<portLabels>
	<xsl:for-each select="bidib2:port">
		<portLabel>
		<xsl:attribute name="type">
			<xsl:choose>
				<xsl:when test="@xsi:type='OutputBacklight'">
					<xsl:text>BACKLIGHT</xsl:text>
				</xsl:when>
				<xsl:when test="@xsi:type='OutputLight'">
					<xsl:text>LIGHT</xsl:text>
				</xsl:when>
				<xsl:when test="@xsi:type='OutputSwitch'">
					<xsl:text>SWITCH</xsl:text>
				</xsl:when>
				<xsl:when test="@xsi:type='OutputServo'">
					<xsl:text>SERVO</xsl:text>
				</xsl:when>
				<xsl:when test="@xsi:type='InputKey'">
					<xsl:text>INPUT</xsl:text>
				</xsl:when>
			</xsl:choose>
	    </xsl:attribute>
		<xsl:attribute name="index">
			<xsl:value-of select="@number" />
	    </xsl:attribute>
		<xsl:attribute name="label">
			<xsl:value-of select="@name" />
	    </xsl:attribute>
		</portLabel>
	</xsl:for-each>
	</portLabels>
	</xsl:template>
	
	<xsl:template match="bidib2:macros">
	<macroLabels>
	<xsl:for-each select="bidib2:macro">
		<macroLabel>
		<xsl:attribute name="index">
			<xsl:value-of select="@number" />
	    </xsl:attribute>
		<xsl:attribute name="label">
			<xsl:value-of select="@name" />
	    </xsl:attribute>
		</macroLabel>
	</xsl:for-each>
	</macroLabels>
	</xsl:template>
	
	<xsl:template match="bidib2:accessories">
	<accessoryLabels>
	<xsl:for-each select="bidib2:accessory">
		<accessoryLabel>
		<xsl:attribute name="index">
			<xsl:value-of select="@number" />
	    </xsl:attribute>
		<xsl:attribute name="label">
			<xsl:value-of select="@name" />
	    </xsl:attribute>
			<xsl:for-each select="bidib2:aspects/bidib2:aspect">
				<xsl:sort select="@number" />
				<aspectLabel>
				<xsl:attribute name="index">
					<xsl:value-of select="@number" />
			    </xsl:attribute>
				<xsl:attribute name="label">
					<xsl:value-of select="@name" />
			    </xsl:attribute>
			    <xsl:apply-templates />
				</aspectLabel>
			</xsl:for-each>
		</accessoryLabel>
	</xsl:for-each>
	</accessoryLabels>
	</xsl:template>

</xsl:stylesheet>
