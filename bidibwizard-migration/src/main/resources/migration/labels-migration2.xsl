<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="2.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.bidib.org/schema/nodeLabels/1.0"
	xmlns:wizard="http://www.bidib.org/schema/labels" 
	exclude-result-prefixes="wizard" >
	
	<xsl:output method='xml' encoding="UTF-8" indent="yes" />
	<xsl:strip-space elements="*"/>
	
	<!-- provide the uniqueId of the node as parameter -->
	<xsl:param name="wizard:search_uniqueId" />
	
	<xsl:template match="wizard:Labels">
		<nodeLabels>
			<xsl:apply-templates />
		</nodeLabels>
	</xsl:template>
	
	<xsl:template match="wizard:Labels/wizard:LabelNode[matches(wizard:UniqueId, $wizard:search_uniqueId)]" priority="10">
		<xsl:choose>
			<xsl:when test="wizard:Label[@Type='node']">
				<nodeLabel>
				<xsl:attribute name="userName">
					<xsl:value-of select="wizard:Label/wizard:LabelString" />
			    </xsl:attribute>
				<xsl:attribute name="uniqueId">
					<xsl:value-of select="wizard:UniqueId" />
			    </xsl:attribute>
				</nodeLabel>
			</xsl:when>
			<xsl:when test="wizard:Label[@Type='accessory']">
				<nodeLabel>
					<xsl:attribute name="uniqueId">
						<xsl:value-of select="wizard:UniqueId" />
				    </xsl:attribute>
				</nodeLabel>
				<accessoryLabels>
					<xsl:for-each select="wizard:Label">
						<xsl:sort select="wizard:Index" data-type="number" />
						<accessoryLabel>
							<xsl:attribute name="index">
								<xsl:value-of select="wizard:Index" />
						    </xsl:attribute>
							<xsl:attribute name="label">
								<xsl:value-of select="wizard:LabelString" />
						    </xsl:attribute>
						</accessoryLabel>
					</xsl:for-each>
				</accessoryLabels>
			</xsl:when>
			<xsl:when test="wizard:Label[@Type='macro']">
				<nodeLabel>
					<xsl:attribute name="uniqueId">
						<xsl:value-of select="wizard:UniqueId" />
				    </xsl:attribute>
				</nodeLabel>
				<macroLabels>
					<xsl:for-each select="wizard:Label">
						<xsl:sort select="wizard:Index" data-type="number" />
						<macroLabel>
							<xsl:attribute name="index">
								<xsl:value-of select="wizard:Index" />
						    </xsl:attribute>
							<xsl:attribute name="label">
								<xsl:value-of select="wizard:LabelString" />
						    </xsl:attribute>
						</macroLabel>
					</xsl:for-each>
				</macroLabels>
			</xsl:when>
			<xsl:when test="wizard:Label[@Type='feedbackPort']">
				<nodeLabel>
					<xsl:attribute name="uniqueId">
						<xsl:value-of select="wizard:UniqueId" />
				    </xsl:attribute>
				</nodeLabel>
				<feedbackPortLabels>
					<xsl:for-each select="wizard:Label">
						<xsl:sort select="wizard:Index" data-type="number" />
						<portLabel>
							<xsl:attribute name="index">
								<xsl:value-of select="wizard:Index" />
						    </xsl:attribute>
							<xsl:attribute name="label">
								<xsl:value-of select="wizard:LabelString" />
						    </xsl:attribute>
						</portLabel>
					</xsl:for-each>
				</feedbackPortLabels>
			</xsl:when>
			<xsl:when test="wizard:Label[contains('|backlightPort|lightPort|servoPort|switchPort|inputPort|', concat('|', @Type, '|'))]">
				<nodeLabel>
					<xsl:attribute name="uniqueId">
						<xsl:value-of select="wizard:UniqueId" />
				    </xsl:attribute>
				</nodeLabel>
				<portLabels>
					<xsl:for-each select="wizard:Label">
						<xsl:sort select="wizard:Index" data-type="number" />
						<portLabel>
							<xsl:attribute name="type">
								<xsl:choose>
									<xsl:when test="@Type='backlightPort'">
										<xsl:text>BACKLIGHT</xsl:text>
									</xsl:when>
									<xsl:when test="@Type='lightPort'">
										<xsl:text>LIGHT</xsl:text>
									</xsl:when>
									<xsl:when test="@Type='switchPort'">
										<xsl:text>SWITCH</xsl:text>
									</xsl:when>
									<xsl:when test="@Type='servoPort'">
										<xsl:text>SERVO</xsl:text>
									</xsl:when>
									<xsl:when test="@Type='inputPort'">
										<xsl:text>INPUT</xsl:text>
									</xsl:when>
								</xsl:choose>
						    </xsl:attribute>						
							<xsl:attribute name="index">
								<xsl:value-of select="wizard:Index" />
						    </xsl:attribute>
							<xsl:attribute name="label">
								<xsl:value-of select="wizard:LabelString" />
						    </xsl:attribute>
						</portLabel>
					</xsl:for-each>
				</portLabels>
			</xsl:when>
			<xsl:when test="wizard:Label">
	            <xsl:value-of select="'not recognized'"/>
			</xsl:when>
			<xsl:otherwise>
	        </xsl:otherwise>
		</xsl:choose>
<!-- 	    <xsl:apply-templates /> -->
	</xsl:template>
	
	<xsl:template match="wizard:Labels/wizard:LabelNode/*">
	</xsl:template>
	
	<xsl:template match="wizard:Labels/wizard:LabelNode">
	</xsl:template>
	

</xsl:stylesheet>
