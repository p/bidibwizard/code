<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="2.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.bidib.org/schema/nodeLabels/1.0"
	xmlns:wizard="http://www.bidib.org/schema/labels" 
	exclude-result-prefixes="wizard" >
	
	<xsl:output method='xml' encoding="UTF-8" indent="yes" />
	<xsl:strip-space elements="*"/>
	
	<!-- provide the uniqueId of the node as parameter -->
	<xsl:param name="wizard:search_uniqueId" />
	<!-- provide the port type of the node as parameter -->
	<xsl:param name="wizard:port_type" />
	
	<xsl:template match="java">
		<nodeLabels>
			<xsl:apply-templates />
		</nodeLabels>
	</xsl:template>
	
	<xsl:template match="java/object/void[@property='labelMap']/void[@method='put'][matches(long, $wizard:search_uniqueId)]"  priority="10">
		<nodeLabel>
			<xsl:attribute name="uniqueId">
				<xsl:value-of select="long" />
		    </xsl:attribute>
   			<xsl:choose>
				<xsl:when test="string">
					<xsl:attribute name="userName">
						<xsl:value-of select="string" />
				    </xsl:attribute>
				</xsl:when>
   			</xsl:choose>
		</nodeLabel>
		<xsl:choose>
			<xsl:when test="object">
				<feedbackPortLabels>
					<xsl:for-each select="object/void[@method='put']">
						<xsl:sort select="int" data-type="number" />
						<portLabel>
							<xsl:attribute name="index">
								<xsl:value-of select="int" />
						    </xsl:attribute>
							<xsl:attribute name="label">
								<xsl:value-of select="string" />
						    </xsl:attribute>
						</portLabel>
					</xsl:for-each>
				</feedbackPortLabels>
			</xsl:when>
  		</xsl:choose>
	</xsl:template>
	
	<xsl:template match="java/object/void[@property='labelMap']/void[@method='put']"/>
	

</xsl:stylesheet>
