<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="2.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.bidib.org/schema/migration/1.0" >
	
	<xsl:output method='xml' encoding="UTF-8" indent="yes" />
	<xsl:strip-space elements="*"/>
	
	<xsl:template match="java">
		<nodes>
			<xsl:apply-templates />
		</nodes>
	</xsl:template>
	
	<xsl:template match="java/object/void[@property='labelMap']/void[@method='put']"  priority="10">
		<nodeLabel>
			<xsl:attribute name="uniqueId">
				<xsl:value-of select="long" />
		    </xsl:attribute>
   			<xsl:choose>
				<xsl:when test="string">
					<xsl:attribute name="userName">
						<xsl:value-of select="string" />
				    </xsl:attribute>
				</xsl:when>
   			</xsl:choose>
		</nodeLabel>
	</xsl:template>
	
	<xsl:template match="java/object/void[@property='labelMap']/void[@method='put']"/>
	

</xsl:stylesheet>
