<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="2.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.bidib.org/schema/nodeLabels/1.0"
	xmlns:wizard="http://www.bidib.org/wizard/labels"
	xmlns:labels="http://www.bidib.org/schema/labels" 
	exclude-result-prefixes="wizard labels" >
	
	<xsl:output method='xml' encoding="UTF-8" indent="yes" />
	<xsl:strip-space elements="*"/>
	
	<!-- provide the uniqueId of the node as parameter -->
	<xsl:param name="labels:search_uniqueId" />
	<xsl:param name="labels:forced_porttype" />
	
	<xsl:template match="wizard:Labels">
		<nodeLabels>
			<xsl:apply-templates />
		</nodeLabels>
	</xsl:template>
	
	<xsl:template match="wizard:Labels/wizard:labelNode[matches(wizard:uniqueId, $labels:search_uniqueId)]" priority="10">
		<xsl:choose>
			<xsl:when test="wizard:label[@type='node']">
				<nodeLabel>
				<xsl:attribute name="userName">
					<xsl:value-of select="wizard:label/wizard:labelString" />
			    </xsl:attribute>
				<xsl:attribute name="uniqueId">
					<xsl:value-of select="wizard:uniqueId" />
			    </xsl:attribute>
				</nodeLabel>
			</xsl:when>
			<xsl:when test="wizard:label[@type='accessory']">
				<nodeLabel>
					<xsl:attribute name="uniqueId">
						<xsl:value-of select="wizard:uniqueId" />
				    </xsl:attribute>
				</nodeLabel>
				<accessoryLabels>
					<xsl:for-each select="wizard:label">
						<xsl:sort select="wizard:index" data-type="number" />
						<accessoryLabel>
							<xsl:attribute name="index">
								<xsl:value-of select="wizard:index" />
						    </xsl:attribute>
							<xsl:attribute name="label">
								<xsl:value-of select="wizard:labelString" />
						    </xsl:attribute>
						</accessoryLabel>
					</xsl:for-each>
				</accessoryLabels>
			</xsl:when>
			<xsl:when test="wizard:label[@type='macro']">
				<nodeLabel>
					<xsl:attribute name="uniqueId">
						<xsl:value-of select="wizard:uniqueId" />
				    </xsl:attribute>
				</nodeLabel>
				<macroLabels>
					<xsl:for-each select="wizard:label">
						<xsl:sort select="wizard:index" data-type="number" />
						<macroLabel>
							<xsl:attribute name="index">
								<xsl:value-of select="wizard:index" />
						    </xsl:attribute>
							<xsl:attribute name="label">
								<xsl:value-of select="wizard:labelString" />
						    </xsl:attribute>
						</macroLabel>
					</xsl:for-each>
				</macroLabels>
			</xsl:when>
			<xsl:when test="wizard:label[@type='feedbackPort']">
				<nodeLabel>
					<xsl:attribute name="uniqueId">
						<xsl:value-of select="wizard:uniqueId" />
				    </xsl:attribute>
				</nodeLabel>
				<feedbackPortLabels>
					<xsl:for-each select="wizard:label">
						<xsl:sort select="wizard:index" data-type="number" />
						<portLabel>
							<xsl:attribute name="index">
								<xsl:value-of select="wizard:index" />
						    </xsl:attribute>
							<xsl:attribute name="label">
								<xsl:value-of select="wizard:labelString" />
						    </xsl:attribute>
						</portLabel>
					</xsl:for-each>
				</feedbackPortLabels>
			</xsl:when>
			<xsl:when test="wizard:label[contains('|backlightPort|lightPort|servoPort|switchPort|inputPort|', concat('|', @type, '|'))]">
				<nodeLabel>
					<xsl:attribute name="uniqueId">
						<xsl:value-of select="wizard:uniqueId" />
				    </xsl:attribute>
				</nodeLabel>
				<portLabels>
					<xsl:for-each select="wizard:label">
						<xsl:sort select="wizard:index" data-type="number" />
						<portLabel>
							<xsl:attribute name="type">
								<xsl:choose>
									<xsl:when test="matches($labels:forced_porttype,'backlight')">
										<xsl:text>BACKLIGHT</xsl:text>
									</xsl:when>
									<xsl:when test="matches($labels:forced_porttype,'light')">
										<xsl:text>LIGHT</xsl:text>
									</xsl:when>
									<xsl:when test="matches($labels:forced_porttype,'switch')">
										<xsl:text>SWITCH</xsl:text>
									</xsl:when>
									<xsl:when test="matches($labels:forced_porttype,'servo')">
										<xsl:text>SERVO</xsl:text>
									</xsl:when>
									<xsl:when test="matches($labels:forced_porttype,'input')">
										<xsl:text>INPUT</xsl:text>
									</xsl:when>
								</xsl:choose>
						    </xsl:attribute>						
							<xsl:attribute name="index">
								<xsl:value-of select="wizard:index" />
						    </xsl:attribute>
							<xsl:attribute name="label">
								<xsl:value-of select="wizard:labelString" />
						    </xsl:attribute>
						</portLabel>
					</xsl:for-each>
				</portLabels>
			</xsl:when>
			<xsl:when test="wizard:label">
	            <xsl:value-of select="'not recognized'"/>
			</xsl:when>
			<xsl:otherwise>
	        </xsl:otherwise>
		</xsl:choose>
<!-- 	    <xsl:apply-templates /> -->
	</xsl:template>
	
	<xsl:template match="wizard:Labels/wizard:labelNode/*">
	</xsl:template>
	
	<xsl:template match="wizard:Labels/wizard:labelNode">
	</xsl:template>

</xsl:stylesheet>
