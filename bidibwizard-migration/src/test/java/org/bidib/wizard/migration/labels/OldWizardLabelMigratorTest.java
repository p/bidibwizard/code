package org.bidib.wizard.migration.labels;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.transform.TransformerException;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.bidib.jbidibc.core.schema.bidibbase.PortType;
import org.bidib.jbidibc.core.schema.bidiblabels.LabelFactory;
import org.bidib.jbidibc.core.schema.bidiblabels.NodeLabel;
import org.bidib.jbidibc.core.schema.bidiblabels.NodeLabels;
import org.bidib.jbidibc.core.schema.bidiblabels.PortLabel;
import org.bidib.jbidibc.core.schema.bidiblabels.PortLabels;
import org.bidib.wizard.common.context.ApplicationContext;
import org.bidib.wizard.migration.migrator.MigrationContext;
import org.bidib.wizard.migration.schema.nodes.Nodes;
import org.custommonkey.xmlunit.XMLAssert;
import org.custommonkey.xmlunit.XMLUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class OldWizardLabelMigratorTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(OldWizardLabelMigratorTest.class);

    private static final String DATA1_XML = "/xml-test/labels/wizard-old/LightPortLabels";

    private static final String DATA2_XML = "/xml-test/labels/wizard-old/NodeLabels";

    @Test(description = "Test XSL transformation and compare to xml from file.")
    public void transformationLightPortLabelsTest()
        throws TransformerException, URISyntaxException, SAXException, IOException {

        InputStream inputXSL = WizardLabelMigratorTest.class.getResourceAsStream(OldWizardLabelMigrator.INPUT4_XSL);
        Assert.assertNotNull(inputXSL);

        InputStream dataXML = WizardLabelMigratorTest.class.getResourceAsStream(DATA1_XML);
        Assert.assertNotNull(dataXML);

        StringWriter outputXML = new StringWriter();

        try {
            OldWizardLabelMigrator migrator = new OldWizardLabelMigrator();
            Map<String, String> params = new HashMap<>();

            params.put("port_type", "light");
            params.put("search_uniqueId", Long.toString(1464607285711414L));

            migrator.doTransform(params, inputXSL, dataXML, outputXML);
        }
        finally {
            if (outputXML != null) {
                try {
                    outputXML.close();
                }
                catch (IOException ex) {
                    LOGGER.warn("Close outputXML writer failed.", ex);
                }
            }

            if (inputXSL != null) {
                try {
                    inputXSL.close();
                }
                catch (IOException ex) {
                    LOGGER.warn("Close inputXSL stream failed.", ex);
                }
            }
        }

        LOGGER.info("The generated XML document is:\r\n{}", outputXML);

        Assert.assertTrue(outputXML.getBuffer().length() > 0);

        Document testDoc = XMLUnit.buildControlDocument(outputXML.toString());

        InputStream is =
            WizardLabelMigratorTest.class.getResourceAsStream("/xml-test/result/wizard-old/LightPortLabels.xml");
        final String xmlContent = IOUtils.toString(is, "UTF-8");
        Document controlDoc = XMLUnit.buildControlDocument(xmlContent);

        XMLAssert.assertXMLEqual(controlDoc, testDoc);
    }

    @Test(description = "Test XSL transformation and compare to xml from file.")
    public void transformationNodeLabelsTest()
        throws TransformerException, URISyntaxException, SAXException, IOException {

        InputStream inputXSL = OldWizardLabelMigratorTest.class.getResourceAsStream(OldWizardLabelMigrator.INPUT5_XSL);
        Assert.assertNotNull(inputXSL);

        InputStream dataXML = OldWizardLabelMigratorTest.class.getResourceAsStream(DATA2_XML);
        Assert.assertNotNull(dataXML);

        StringWriter outputXML = new StringWriter();

        try {
            OldWizardLabelMigrator migrator = new OldWizardLabelMigrator();

            Map<String, String> params = new HashMap<>();
            params.put("port_type", "node");
            params.put("search_uniqueId", Long.toString(1464607285711414L));

            migrator.doTransform(params, inputXSL, dataXML, outputXML);
        }
        finally {
            if (outputXML != null) {
                try {
                    outputXML.close();
                }
                catch (IOException ex) {
                    LOGGER.warn("Close outputXML writer failed.", ex);
                }
            }

            if (inputXSL != null) {
                try {
                    inputXSL.close();
                }
                catch (IOException ex) {
                    LOGGER.warn("Close inputXSL stream failed.", ex);
                }
            }
        }

        LOGGER.info("The generated XML document is:\r\n{}", outputXML);

        Assert.assertTrue(outputXML.getBuffer().length() > 0);

        Document testDoc = XMLUnit.buildControlDocument(outputXML.toString());

        InputStream is =
            OldWizardLabelMigratorTest.class.getResourceAsStream("/xml-test/result/wizard-old/NodeLabels.xml");
        final String xmlContent = IOUtils.toString(is, "UTF-8");
        Document controlDoc = XMLUnit.buildControlDocument(xmlContent);

        XMLAssert.assertXMLEqual(controlDoc, testDoc);
    }

    @Test
    public void findAllNodesInWizardLightPortLabelsTest() {
        URL url = OldWizardLabelMigratorTest.class.getResource(DATA1_XML);
        Assert.assertNotNull(url);

        String searchPath = url.getPath();
        // get the path only
        searchPath = FilenameUtils.getPath(searchPath);

        LOGGER.info("Prepared search path: {}", searchPath);

        OldWizardLabelMigrator migrator = new OldWizardLabelMigrator();
        Nodes nodes =
            migrator.performFindAllNodesInWizardLabels("LightPortLabels", OldWizardLabelMigrator.INPUT5_XSL,
                searchPath);

        Assert.assertNotNull(nodes);
        Assert.assertNotNull(nodes.getNodeLabel());

        List<org.bidib.wizard.migration.schema.nodes.NodeLabel> nodeLabels = nodes.getNodeLabel();

        LOGGER.info("Current nodeLabels: {}", nodeLabels);
        Assert.assertEquals(nodeLabels.size(), 2);
    }

    @Test
    public void findAllNodesInWizardLabelsTest() {
        URL url = OldWizardLabelMigratorTest.class.getResource(DATA1_XML);
        Assert.assertNotNull(url);

        String searchPath = url.getPath();
        // get the path only
        searchPath = FilenameUtils.getPath(searchPath);

        LOGGER.info("Prepared search path: {}", searchPath);

        OldWizardLabelMigrator migrator = new OldWizardLabelMigrator();
        Nodes nodes = migrator.findAllNodesInWizardLabels(searchPath);

        Assert.assertNotNull(nodes);
        Assert.assertNotNull(nodes.getNodeLabel());

        List<org.bidib.wizard.migration.schema.nodes.NodeLabel> nodeLabels = nodes.getNodeLabel();

        LOGGER.info("Current nodeLabels: {}", nodeLabels);
        Assert.assertEquals(nodeLabels.size(), 3);
    }

    @Test
    public void migrationOldAccessoryLabelsToNodeLabelsTest() {
        URL url = OldWizardLabelMigratorTest.class.getResource(DATA2_XML);
        Assert.assertNotNull(url);

        String searchPath = url.getPath();
        // get the path only
        searchPath = FilenameUtils.getPath(searchPath);

        LOGGER.info("Prepared search path: {}", searchPath);

        final ApplicationContext context = new MigrationContext();

        OldWizardLabelMigrator migrator = new OldWizardLabelMigrator();
        NodeLabels nodeLabels = migrator.performWizardLabelsMigration(context, 0x5340D75001236L, searchPath);

        Assert.assertNotNull(nodeLabels);
        Assert.assertNotNull(nodeLabels.getNodeLabel());

        NodeLabel nodeLabel = nodeLabels.getNodeLabel();
        Assert.assertEquals(nodeLabel.getUniqueId(), 0x5340D75001236L);
        Assert.assertEquals(nodeLabel.getUserName(), "Node_0_Test");

        Assert.assertNotNull(nodeLabels.getPortLabels());
        PortLabels portLabels = nodeLabels.getPortLabels();
        Assert.assertNotNull(portLabels.getPortLabel());
        List<PortLabel> portLabelList = portLabels.getPortLabel();

        // check total number of ports
        Assert.assertEquals(portLabelList.size(), 3);

        // check the ports
        Assert.assertEquals(LabelFactory.getPortsOfType(portLabelList, PortType.SERVO).size(), 0);
        Assert.assertEquals(LabelFactory.getPortsOfType(portLabelList, PortType.SWITCH).size(), 0);
        Assert.assertEquals(LabelFactory.getPortsOfType(portLabelList, PortType.INPUT).size(), 0);

        Assert.assertEquals(LabelFactory.getPortsOfType(portLabelList, PortType.LIGHT).size(), 3);

    }
}
