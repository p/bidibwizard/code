package org.bidib.wizard.migration.labels;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.bidib.jbidibc.core.schema.bidibbase.PortType;
import org.bidib.jbidibc.core.schema.bidiblabels.AccessoryLabels;
import org.bidib.jbidibc.core.schema.bidiblabels.FeedbackPortLabels;
import org.bidib.jbidibc.core.schema.bidiblabels.LabelFactory;
import org.bidib.jbidibc.core.schema.bidiblabels.MacroLabels;
import org.bidib.jbidibc.core.schema.bidiblabels.NodeLabel;
import org.bidib.jbidibc.core.schema.bidiblabels.NodeLabels;
import org.bidib.jbidibc.core.schema.bidiblabels.PortLabel;
import org.bidib.jbidibc.core.schema.bidiblabels.PortLabels;
import org.bidib.wizard.common.context.ApplicationContext;
import org.bidib.wizard.migration.migrator.MigrationContext;
import org.bidib.wizard.migration.schema.nodes.Nodes;
import org.custommonkey.xmlunit.XMLAssert;
import org.custommonkey.xmlunit.XMLUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class WizardLabelMigratorTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(WizardLabelMigratorTest.class);

    private static final String NAMESPACE_PREFIX = "{http://www.bidib.org/schema/labels}";

    private static final String INPUT_XSL = "/migration/labels-migration.xsl";

    private static final String INPUT2_XSL = "/migration/labels-migration2.xsl";

    private static final String DATA1_XML = "/xml-test/labels/bidib2/05000DD00043ED.xml";

    private static final String DATA2_XML = "/xml-test/labels/bidib2/05000D6B009AEA.xml";

    private static final String DATA3_XML = "/xml-test/labels/wizard/NodeLabels.labels";

    private static final String DATA4_XML = "/xml-test/labels/wizard/AccessoryLabels.labels";

    private static final String DATA5_XML = "/xml-test/labels/wizard/MacroLabels.labels";

    private static final String DATA6_XML = "/xml-test/labels/wizard-wrong-type/SwitchPortLabels.labels";

    @Test
    public void migrationWithUniqueIdAsIntegerTest() {

        URL url = WizardLabelMigratorTest.class.getResource(DATA1_XML);
        Assert.assertNotNull(url);

        String searchPath = url.getPath();

        // get the path only
        searchPath = FilenameUtils.getPath(searchPath);

        LOGGER.info("Prepared search path: {}", searchPath);

        final ApplicationContext context = new MigrationContext();

        Bidib2LabelMigrator migrator = new Bidib2LabelMigrator();
        NodeLabels nodeLabels = migrator.performWizardLabelsMigration(context, 0x05000DD00043EDL, searchPath);

        Assert.assertNotNull(nodeLabels);
        Assert.assertNotNull(nodeLabels.getNodeLabel());

        NodeLabel nodeLabel = nodeLabels.getNodeLabel();
        Assert.assertEquals(nodeLabel.getUniqueId(), 0x05000DD00043EDL);
        Assert.assertEquals(nodeLabel.getUserName(), "NeoTest");
    }

    @Test(description = "Test XSL transformation and compare to xml from file.")
    public void transformationWithUniqueIdAsIntegerTest()
        throws TransformerException, URISyntaxException, SAXException, IOException {

        InputStream inputXSL = WizardLabelMigratorTest.class.getResourceAsStream(INPUT_XSL);
        Assert.assertNotNull(inputXSL);

        InputStream dataXML = WizardLabelMigratorTest.class.getResourceAsStream(DATA1_XML);
        Assert.assertNotNull(dataXML);

        StringWriter outputXML = new StringWriter();

        try {
            WizardLabelMigrator migrator = new WizardLabelMigrator();

            Map<String, String> params = new HashMap<>();
            params.put("search_uniqueId", Long.toString(0x05000DD00043EDL));

            migrator.doTransform(params, inputXSL, dataXML, outputXML);
        }
        finally {
            if (outputXML != null) {
                try {
                    outputXML.close();
                }
                catch (IOException ex) {
                    LOGGER.warn("Close outputXML writer failed.", ex);
                }
            }

            if (inputXSL != null) {
                try {
                    inputXSL.close();
                }
                catch (IOException ex) {
                    LOGGER.warn("Close inputXSL stream failed.", ex);
                }
            }
        }

        LOGGER.info("The generated XML document is:\r\n{}", outputXML);

        Assert.assertTrue(outputXML.getBuffer().length() > 0);

        Document testDoc = XMLUnit.buildControlDocument(outputXML.toString());

        InputStream is = WizardLabelMigratorTest.class.getResourceAsStream("/xml-test/result/05000DD00043ED.xml");
        final String xmlContent = IOUtils.toString(is, "UTF-8");
        Document controlDoc = XMLUnit.buildControlDocument(xmlContent);

        XMLAssert.assertXMLEqual(controlDoc, testDoc);
    }

    @Test
    public void migrationWithUniqueIdAsHexTest() {

        URL url = WizardLabelMigratorTest.class.getResource(DATA2_XML);
        Assert.assertNotNull(url);

        String searchPath = url.getPath();

        // get the path only
        searchPath = FilenameUtils.getPath(searchPath);

        LOGGER.info("Prepared search path: {}", searchPath);

        final ApplicationContext context = new MigrationContext();

        Bidib2LabelMigrator migrator = new Bidib2LabelMigrator();
        NodeLabels nodeLabels = migrator.performWizardLabelsMigration(context, 0x05000D6B009AEAL, searchPath);

        Assert.assertNotNull(nodeLabels);
        Assert.assertNotNull(nodeLabels.getNodeLabel());

        NodeLabel nodeLabel = nodeLabels.getNodeLabel();
        Assert.assertEquals(nodeLabel.getUniqueId(), 0x05000D6B009AEAL);
        Assert.assertEquals(nodeLabel.getUserName(), "");
    }

    @Test(description = "Test XSL transformation and compare to xml from file.")
    public void transformationWithUniqueIdAsHexTest()
        throws TransformerException, URISyntaxException, SAXException, IOException {

        InputStream inputXSL = WizardLabelMigratorTest.class.getResourceAsStream(INPUT_XSL);
        Assert.assertNotNull(inputXSL);

        InputStream dataXML = WizardLabelMigratorTest.class.getResourceAsStream(DATA2_XML);
        Assert.assertNotNull(dataXML);

        StringWriter outputXML = new StringWriter();

        try {
            WizardLabelMigrator migrator = new WizardLabelMigrator();

            Map<String, String> params = new HashMap<>();
            params.put("search_uniqueId", Long.toString(0x05000D6B009AEAL));

            migrator.doTransform(params, inputXSL, dataXML, outputXML);
        }
        finally {
            if (outputXML != null) {
                try {
                    outputXML.close();
                }
                catch (IOException ex) {
                    LOGGER.warn("Close outputXML writer failed.", ex);
                }
            }

            if (inputXSL != null) {
                try {
                    inputXSL.close();
                }
                catch (IOException ex) {
                    LOGGER.warn("Close inputXSL stream failed.", ex);
                }
            }
        }

        LOGGER.info("The generated XML document is:\r\n{}", outputXML);

        Assert.assertTrue(outputXML.getBuffer().length() > 0);

        Document testDoc = XMLUnit.buildControlDocument(outputXML.toString());

        InputStream is = WizardLabelMigratorTest.class.getResourceAsStream("/xml-test/result/05000D6B009AEA.xml");
        final String xmlContent = IOUtils.toString(is, "UTF-8");
        Document controlDoc = XMLUnit.buildControlDocument(xmlContent);

        XMLAssert.assertXMLEqual(controlDoc, testDoc);
    }

    @Test(description = "Test XSL transformation and compare to xml from file.")
    public void transformationOldNodeLabelsTest()
        throws TransformerException, URISyntaxException, SAXException, IOException {

        InputStream inputXSL = WizardLabelMigratorTest.class.getResourceAsStream(INPUT2_XSL);
        Assert.assertNotNull(inputXSL);

        InputStream dataXML = WizardLabelMigratorTest.class.getResourceAsStream(DATA3_XML);
        Assert.assertNotNull(dataXML);

        TransformerFactory factory = TransformerFactory.newInstance();

        StreamSource xslStream = new StreamSource(inputXSL);

        Transformer transformer = factory.newTransformer(xslStream);
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "3");

        transformer.setParameter(NAMESPACE_PREFIX + "search_uniqueId", "61361602502354154");

        StreamSource in = new StreamSource(dataXML);

        StringWriter outputXML = new StringWriter();
        StreamResult out = new StreamResult(outputXML);

        transformer.transform(in, out);

        LOGGER.info("The generated XML document is:\r\n{}", outputXML);

        Assert.assertTrue(outputXML.getBuffer().length() > 0);

        Document testDoc = XMLUnit.buildControlDocument(outputXML.toString());

        InputStream is = WizardLabelMigratorTest.class.getResourceAsStream("/xml-test/result/DA000D680064EA-node.xml");
        final String xmlContent = IOUtils.toString(is, "UTF-8");
        Document controlDoc = XMLUnit.buildControlDocument(xmlContent);

        XMLAssert.assertXMLEqual(controlDoc, testDoc);
    }

    @Test
    public void migrationOldNodeLabelsTest() {
        URL url = WizardLabelMigratorTest.class.getResource(DATA3_XML);
        Assert.assertNotNull(url);

        String searchPath = url.getPath();
        // get the path only
        searchPath = FilenameUtils.getPath(searchPath);

        LOGGER.info("Prepared search path: {}", searchPath);

        Map<String, String> params = new HashMap<>();
        params.put("search_uniqueId", Long.toString(0xDA000D680064EAL));

        WizardLabelMigrator migrator = new WizardLabelMigrator();
        NodeLabels nodeLabels =
            migrator.performWizardLabelsMigration(params, "NodeLabels.labels", AbstractWizardLabelMigrator.INPUT2_XSL,
                searchPath);

        Assert.assertNotNull(nodeLabels);
        Assert.assertNotNull(nodeLabels.getNodeLabel());

        NodeLabel nodeLabel = nodeLabels.getNodeLabel();
        Assert.assertEquals(nodeLabel.getUniqueId(), 0xDA000D680064EAL);
        Assert.assertEquals(nodeLabel.getUserName(), "Test Booster Main");
    }

    @Test(description = "Test XSL transformation and compare to xml from file.")
    public void transformationOldAccessoryLabelsTest()
        throws TransformerException, URISyntaxException, SAXException, IOException {

        InputStream inputXSL = WizardLabelMigratorTest.class.getResourceAsStream(INPUT2_XSL);
        Assert.assertNotNull(inputXSL);

        InputStream dataXML = WizardLabelMigratorTest.class.getResourceAsStream(DATA4_XML);
        Assert.assertNotNull(dataXML);

        TransformerFactory factory = TransformerFactory.newInstance();

        StreamSource xslStream = new StreamSource(inputXSL);

        Transformer transformer = factory.newTransformer(xslStream);
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "3");

        transformer.setParameter(NAMESPACE_PREFIX + "search_uniqueId", "1464607285711414");

        StreamSource in = new StreamSource(dataXML);

        StringWriter outputXML = new StringWriter();
        StreamResult out = new StreamResult(outputXML);

        transformer.transform(in, out);

        LOGGER.info("The generated XML document is:\r\n{}", outputXML);

        Assert.assertTrue(outputXML.getBuffer().length() > 0);

        Document testDoc = XMLUnit.buildControlDocument(outputXML.toString());

        InputStream is =
            WizardLabelMigratorTest.class.getResourceAsStream("/xml-test/result/5340D75001236-accessories.xml");
        final String xmlContent = IOUtils.toString(is, "UTF-8");
        Document controlDoc = XMLUnit.buildControlDocument(xmlContent);

        XMLAssert.assertXMLEqual(controlDoc, testDoc);
    }

    @Test
    public void migrationOldAccessoryLabelsTest() {
        URL url = WizardLabelMigratorTest.class.getResource(DATA4_XML);
        Assert.assertNotNull(url);

        String searchPath = url.getPath();
        // get the path only
        searchPath = FilenameUtils.getPath(searchPath);

        LOGGER.info("Prepared search path: {}", searchPath);

        Map<String, String> params = new HashMap<>();
        params.put("search_uniqueId", Long.toString(0x5340D75001236L));

        WizardLabelMigrator migrator = new WizardLabelMigrator();
        NodeLabels nodeLabels =
            migrator.performWizardLabelsMigration(params, "AccessoryLabels.labels",
                AbstractWizardLabelMigrator.INPUT2_XSL, searchPath);

        Assert.assertNotNull(nodeLabels);
        Assert.assertNotNull(nodeLabels.getNodeLabel());

        NodeLabel nodeLabel = nodeLabels.getNodeLabel();
        Assert.assertEquals(nodeLabel.getUniqueId(), 0x5340D75001236L);
        Assert.assertNull(nodeLabel.getUserName());
    }

    @Test
    public void migrationOldAccessoryLabelsToNodeLabelsTest() {
        URL url = WizardLabelMigratorTest.class.getResource(DATA4_XML);
        Assert.assertNotNull(url);

        String searchPath = url.getPath();
        // get the path only
        searchPath = FilenameUtils.getPath(searchPath);

        LOGGER.info("Prepared search path: {}", searchPath);

        final ApplicationContext context = new MigrationContext();

        WizardLabelMigrator migrator = new WizardLabelMigrator();
        NodeLabels nodeLabels = migrator.performWizardLabelsMigration(context, 0x5340D75001236L, searchPath);

        Assert.assertNotNull(nodeLabels);
        Assert.assertNotNull(nodeLabels.getNodeLabel());

        NodeLabel nodeLabel = nodeLabels.getNodeLabel();
        Assert.assertEquals(nodeLabel.getUniqueId(), 0x5340D75001236L);
        Assert.assertEquals(nodeLabel.getUserName(), "OneControl Test 1");

        Assert.assertNotNull(nodeLabels.getMacroLabels());
        MacroLabels macroLabels = nodeLabels.getMacroLabels();
        Assert.assertNotNull(macroLabels.getMacroLabel());
        Assert.assertEquals(macroLabels.getMacroLabel().size(), 32);

        Assert.assertNotNull(nodeLabels.getAccessoryLabels());
        AccessoryLabels acecssoryLabels = nodeLabels.getAccessoryLabels();
        Assert.assertNotNull(acecssoryLabels.getAccessoryLabel());
        Assert.assertEquals(acecssoryLabels.getAccessoryLabel().size(), 20);

        Assert.assertNotNull(nodeLabels.getFeedbackPortLabels());
        FeedbackPortLabels feedbackPortLabels = nodeLabels.getFeedbackPortLabels();
        Assert.assertNotNull(feedbackPortLabels.getPortLabel());
        Assert.assertEquals(feedbackPortLabels.getPortLabel().size(), 1);

        Assert.assertNotNull(nodeLabels.getPortLabels());
        PortLabels portLabels = nodeLabels.getPortLabels();
        Assert.assertNotNull(portLabels.getPortLabel());
        List<PortLabel> portLabelList = portLabels.getPortLabel();

        // check total number of ports
        Assert.assertEquals(portLabelList.size(), 40);

        // check the ports
        Assert.assertEquals(LabelFactory.getPortsOfType(portLabelList, PortType.SERVO).size(), 8);
        Assert.assertEquals(LabelFactory.getPortsOfType(portLabelList, PortType.SWITCH).size(), 16);
        Assert.assertEquals(LabelFactory.getPortsOfType(portLabelList, PortType.INPUT).size(), 16);

        Assert.assertEquals(LabelFactory.getPortsOfType(portLabelList, PortType.LIGHT).size(), 0);

    }

    @Test(description = "Test XSL transformation and compare to xml from file.")
    public void transformationOldMacroLabelsTest()
        throws TransformerException, URISyntaxException, SAXException, IOException {

        InputStream inputXSL = WizardLabelMigratorTest.class.getResourceAsStream(INPUT2_XSL);
        Assert.assertNotNull(inputXSL);

        InputStream dataXML = WizardLabelMigratorTest.class.getResourceAsStream(DATA5_XML);
        Assert.assertNotNull(dataXML);

        TransformerFactory factory = TransformerFactory.newInstance();

        StreamSource xslStream = new StreamSource(inputXSL);

        Transformer transformer = factory.newTransformer(xslStream);
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "3");

        transformer.setParameter(NAMESPACE_PREFIX + "search_uniqueId", "1464607285711414");

        StreamSource in = new StreamSource(dataXML);

        StringWriter outputXML = new StringWriter();
        StreamResult out = new StreamResult(outputXML);

        transformer.transform(in, out);

        LOGGER.info("The generated XML document is:\r\n{}", outputXML);

        Assert.assertTrue(outputXML.getBuffer().length() > 0);

        Document testDoc = XMLUnit.buildControlDocument(outputXML.toString());

        InputStream is = WizardLabelMigratorTest.class.getResourceAsStream("/xml-test/result/5340D75001236-macros.xml");
        final String xmlContent = IOUtils.toString(is, "UTF-8");
        Document controlDoc = XMLUnit.buildControlDocument(xmlContent);

        XMLAssert.assertXMLEqual(controlDoc, testDoc);
    }

    @Test
    public void migrationOldMacroLabelsTest() {
        URL url = WizardLabelMigratorTest.class.getResource(DATA5_XML);
        Assert.assertNotNull(url);

        String searchPath = url.getPath();
        // get the path only
        searchPath = FilenameUtils.getPath(searchPath);

        LOGGER.info("Prepared search path: {}", searchPath);

        Map<String, String> params = new HashMap<>();
        params.put("search_uniqueId", Long.toString(0x5340D75001236L));

        WizardLabelMigrator migrator = new WizardLabelMigrator();
        NodeLabels nodeLabels =
            migrator.performWizardLabelsMigration(params, "MacroLabels.labels", AbstractWizardLabelMigrator.INPUT2_XSL,
                searchPath);

        Assert.assertNotNull(nodeLabels);
        Assert.assertNotNull(nodeLabels.getNodeLabel());

        NodeLabel nodeLabel = nodeLabels.getNodeLabel();
        Assert.assertEquals(nodeLabel.getUniqueId(), 0x5340D75001236L);
        Assert.assertNull(nodeLabel.getUserName());
    }

    @Test
    public void findAllNodesInWizardLabelsTest() {
        URL url = WizardLabelMigratorTest.class.getResource(DATA3_XML);
        Assert.assertNotNull(url);

        String searchPath = url.getPath();
        // get the path only
        searchPath = FilenameUtils.getPath(searchPath);

        LOGGER.info("Prepared search path: {}", searchPath);

        WizardLabelMigrator migrator = new WizardLabelMigrator();
        Nodes nodes = migrator.findAllNodesInWizardLabels(searchPath);

        Assert.assertNotNull(nodes);
        Assert.assertNotNull(nodes.getNodeLabel());

        List<org.bidib.wizard.migration.schema.nodes.NodeLabel> nodeLabels = nodes.getNodeLabel();

        LOGGER.info("Current nodeLabels: {}", nodeLabels);
        Assert.assertEquals(nodeLabels.size(), 6);
    }

    @Test
    public void migrateSwitchPortForcedLabelsTest() {
        URL url = WizardLabelMigratorTest.class.getResource(DATA6_XML);
        Assert.assertNotNull(url);

        String searchPath = url.getPath();
        // get the path only
        searchPath = FilenameUtils.getPath(searchPath);

        LOGGER.info("Prepared search path: {}", searchPath);

        Map<String, String> params = new HashMap<>();
        params.put("search_uniqueId", Long.toString(0x5000D7500BAEAL));
        params.put("forced_porttype", "switch");

        WizardLabelMigrator migrator = new WizardLabelMigrator();
        NodeLabels nodeLabels =
            migrator.performWizardLabelsMigration(params, "SwitchPortLabels.labels",
                AbstractWizardLabelMigrator.INPUT2_XSL, searchPath);

        Assert.assertNotNull(nodeLabels);
        Assert.assertNotNull(nodeLabels.getNodeLabel());

        NodeLabel nodeLabel = nodeLabels.getNodeLabel();
        Assert.assertEquals(nodeLabel.getUniqueId(), 0x5000D7500BAEAL);
        Assert.assertNull(nodeLabel.getUserName());

        Assert.assertNotNull(nodeLabels.getPortLabels());

        PortLabels portLabels = nodeLabels.getPortLabels();

        Assert.assertEquals(portLabels.getPortLabel().size(), 36);

        for (PortLabel portLabel : portLabels.getPortLabel()) {

            Assert.assertEquals(portLabel.getType(), PortType.SWITCH);
        }
    }
}
