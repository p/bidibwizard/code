package org.bidib.wizard.migration.migrator;

import java.io.File;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.filefilter.FileFileFilter;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.bidib.jbidibc.core.schema.bidiblabels.LabelFactory;
import org.bidib.jbidibc.core.schema.bidiblabels.NodeLabels;
import org.bidib.wizard.common.context.ApplicationContext;
import org.bidib.wizard.migration.labels.Bidib2LabelMigrator;
import org.bidib.wizard.migration.labels.WizardLabelMigratorTest;
import org.bidib.wizard.migration.schema.nodes.Nodes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class WizardMigratorTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(WizardMigratorTest.class);

    private static final String DATA1_XML = "/xml-test/labels/bidib2/05000DD00043ED.xml";

    private static final String DATA1_COPY_XML = "target/xml-mig/bidib2/nodeLabels/05000DD00043ED.xml";

    private static final String DATA3_XML = "/xml-test/labels/wizard/NodeLabels.labels";

    private static final String OUTPUT_TARGET_DIR_BIDIB2 = "target/xml-mig/bidib2/nodeLabels";

    private static final String OUTPUT_TARGET_DIR_WIZARD = "target/xml-mig/wizard/nodeLabels";

    @BeforeClass
    public void prepareOutputDirectory() {
        LOGGER.info("Prepare the target directory for the migrated labels");
        try {
            File outputDir = new File(OUTPUT_TARGET_DIR_WIZARD);
            if (!outputDir.exists()) {
                outputDir.mkdirs();
            }
            else {
                FileUtils.cleanDirectory(outputDir);
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Create output directory failed: {}", OUTPUT_TARGET_DIR_WIZARD, ex);
        }

        try {
            File outputDir = new File(OUTPUT_TARGET_DIR_BIDIB2);
            if (!outputDir.exists()) {
                outputDir.mkdirs();
            }
            else {
                FileUtils.cleanDirectory(outputDir);
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Create output directory failed: {}", OUTPUT_TARGET_DIR_BIDIB2, ex);
        }

        try {
            URL url = WizardLabelMigratorTest.class.getResource(DATA1_XML);
            File source = new File(url.toURI());

            // Create a filter for ".txt" files
            IOFileFilter txtSuffixFilter = FileFilterUtils.suffixFileFilter(".xml");
            IOFileFilter txtFiles = FileFilterUtils.and(FileFileFilter.FILE, txtSuffixFilter);

            Path target = Paths.get(OUTPUT_TARGET_DIR_BIDIB2);
            LOGGER.info("Copy file: {}, to target: {}", source, target);

            // Copy using the filter
            FileUtils.copyDirectory(source.getParentFile(), target.toFile(), txtFiles);
        }
        catch (Exception ex) {
            LOGGER.warn("Copy files to migration dir failed.", ex);
        }
    }

    @Test
    public void migrateWizardLabels() {

        URL url = WizardLabelMigratorTest.class.getResource(DATA3_XML);
        Assert.assertNotNull(url);

        String searchPath = url.getPath();
        // get the path only
        searchPath = FilenameUtils.getPath(searchPath);

        String[] searchpaths = new String[] { searchPath };

        final Class<?>[] migratorClassHolder = new Class[1];

        WizardMigrator migrator = new WizardMigrator();
        Nodes nodes = migrator.findAllNodesInWizardLabels(migratorClassHolder, searchpaths);

        Assert.assertNotNull(nodes);
        Assert.assertNotNull(nodes.getSearchPath());

        LOGGER.info("Found nodes: {}", nodes);

        Assert.assertNotNull(nodes.getNodeLabel());
        Assert.assertEquals(nodes.getNodeLabel().size(), 6);

        String searchPathForLabels = nodes.getSearchPath();

        // create factory to store the migrated labels
        LabelFactory labelFactory = new LabelFactory();

        ApplicationContext context = new MigrationContext();

        for (org.bidib.wizard.migration.schema.nodes.NodeLabel nodeLabel : nodes.getNodeLabel()) {
            LOGGER.info("Collect labels for node: {}", nodeLabel);

            NodeLabels nodeLabels =
                migrator.migrateLabels(context, migratorClassHolder[0], nodeLabel.getUniqueId(), searchPathForLabels);

            Assert.assertNotNull(nodeLabels);

            LOGGER.info("Found nodeLabels: {}", nodeLabels);

            File file = new File(OUTPUT_TARGET_DIR_WIZARD, LabelFactory.prepareNodeFilename(nodeLabel.getUniqueId()));
            LOGGER.info("Prepared file: {}", file);

            labelFactory.saveNodeLabel(nodeLabels, file, false);
        }
    }

    @Test
    public void migrateBidib2Labels() {

        // URL url = WizardLabelMigratorTest.class.getResource(DATA1_COPY_XML);
        File source = new File(DATA1_COPY_XML);
        Assert.assertNotNull(source);

        LOGGER.info("Prepared source file: {}", source);

        String searchPath = source.toString();
        LOGGER.info("Prepared search path: {}", searchPath);
        // get the path only
        searchPath = FilenameUtils.getPath(searchPath);

        String[] searchpaths = new String[] { searchPath };

        final Class<?>[] migratorClassHolder = new Class[1];

        WizardMigrator migrator = new WizardMigrator();
        Nodes nodes = migrator.findAllNodesInWizardLabels(migratorClassHolder, searchpaths);

        Assert.assertNotNull(nodes);
        Assert.assertNotNull(nodes.getSearchPath());

        LOGGER.info("Found nodes: {}", nodes);

        Assert.assertNotNull(nodes.getNodeLabel());
        Assert.assertEquals(nodes.getNodeLabel().size(), 2);

        String searchPathForLabels = nodes.getSearchPath();

        ApplicationContext context = new MigrationContext();

        // create factory to store the migrated labels
        LabelFactory labelFactory = new LabelFactory();

        for (org.bidib.wizard.migration.schema.nodes.NodeLabel nodeLabel : nodes.getNodeLabel()) {
            LOGGER.info("Collect labels for node: {}", nodeLabel);

            NodeLabels nodeLabels =
                migrator.migrateLabels(context, migratorClassHolder[0], nodeLabel.getUniqueId(), searchPathForLabels);

            Assert.assertNotNull(nodeLabels);

            LOGGER.info("Found nodeLabels: {}", nodeLabels);

            File file = new File(OUTPUT_TARGET_DIR_BIDIB2, LabelFactory.prepareNodeFilename(nodeLabel.getUniqueId()));
            LOGGER.info("Prepared file: {}", file);

            labelFactory.saveNodeLabel(nodeLabels, file, false);

            if (Bidib2LabelMigrator.class.getName().equals(nodes.getMigratorClass())) {

                Bidib2LabelMigrator.moveSourceFileToBackupDir(nodes.getSearchPath(), nodeLabel.getUniqueId());
            }
        }
    }
}
