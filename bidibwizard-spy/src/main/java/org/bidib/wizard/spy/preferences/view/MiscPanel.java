package org.bidib.wizard.spy.preferences.view;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JTextField;

import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.spy.preferences.model.PreferencesModel;
import org.jdesktop.swingx.prompt.PromptSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.debug.FormDebugPanel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.validation.view.ValidationComponentUtils;

public class MiscPanel {

    private static final Logger LOGGER = LoggerFactory.getLogger(MiscPanel.class);

    private static final String ENCODED_DIALOG_COLUMN_SPECS = "pref, 3dlu, fill:50dlu:grow";

    private static final String ENCODED_DIALOG_ROW_SPECS = "pref, 3dlu, pref";

    private final PreferencesModel preferencesModel;

    private ValueModel interfacePortValueModel;

    private ValueModel proxyPortValueModel;

    public MiscPanel(final PreferencesModel model) {
        this.preferencesModel = model;
    }

    public JPanel createPanel() {

        DefaultFormBuilder dialogBuilder = null;
        boolean debugDialog = false;
        if (debugDialog) {
            JPanel panel = new FormDebugPanel();
            dialogBuilder =
                new DefaultFormBuilder(new FormLayout(ENCODED_DIALOG_COLUMN_SPECS, ENCODED_DIALOG_ROW_SPECS), panel);
        }
        else {
            JPanel panel = new JPanel(new BorderLayout());
            dialogBuilder =
                new DefaultFormBuilder(new FormLayout(ENCODED_DIALOG_COLUMN_SPECS, ENCODED_DIALOG_ROW_SPECS), panel);
        }
        dialogBuilder.border(Borders.TABBED_DIALOG);

        // prepare the panel content
        interfacePortValueModel =
            new PropertyAdapter<PreferencesModel>(preferencesModel, PreferencesModel.PROPERTY_INTERFACE_PORT, true);
        JTextField textInterfacePort = BasicComponentFactory.createTextField(interfacePortValueModel);
        PromptSupport.setPrompt(Resources.getString(getClass(), "interfacePort.prompt"), textInterfacePort);
        dialogBuilder.append(Resources.getString(getClass(), "interfacePort"), textInterfacePort);
        dialogBuilder.nextLine(2);

        ValidationComponentUtils.setMandatory(textInterfacePort, true);
        ValidationComponentUtils.setMessageKeys(textInterfacePort, "validation.interfaceport_key");

        proxyPortValueModel =
            new PropertyAdapter<PreferencesModel>(preferencesModel, PreferencesModel.PROPERTY_PROXY_PORT, true);
        JTextField textProxyPort = BasicComponentFactory.createTextField(proxyPortValueModel);
        PromptSupport.setPrompt(Resources.getString(getClass(), "proxyPort.prompt"), textProxyPort);
        dialogBuilder.append(Resources.getString(getClass(), "proxyPort"), textProxyPort);
        dialogBuilder.nextLine(2);

        ValidationComponentUtils.setMandatory(textProxyPort, true);
        ValidationComponentUtils.setMessageKeys(textProxyPort, "validation.proxyport_key");

        JPanel contentPanel = dialogBuilder.build();

        return contentPanel;
    }

}
