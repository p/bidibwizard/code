package org.bidib.wizard.spy.preferences.view;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.spy.preferences.model.PreferencesModel;
import org.bidib.wizard.spy.preferences.view.listener.PreferencesViewListener;

import com.jgoodies.forms.builder.ButtonBarBuilder;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.debug.FormDebugPanel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.FormLayout;

public class PreferencesView extends EscapeDialog {

    private static final long serialVersionUID = -1L;

    private static final String ENCODED_DIALOG_COLUMN_SPECS = "pref, 3dlu, fill:50dlu:grow";

    private final JTabbedPane tabbedPane;

    private final MiscPanel miscPanel;

    public PreferencesView(Frame parent, final PreferencesModel model, final PreferencesViewListener listener) {
        super(parent, Resources.getString(PreferencesView.class, "title"), false);
        // TODO Auto-generated constructor stub

        DefaultFormBuilder dialogBuilder = null;
        boolean debugDialog = false;
        if (debugDialog) {
            JPanel panel = new FormDebugPanel();
            dialogBuilder = new DefaultFormBuilder(new FormLayout(ENCODED_DIALOG_COLUMN_SPECS), panel);
        }
        else {
            JPanel panel = new JPanel(new BorderLayout());
            dialogBuilder = new DefaultFormBuilder(new FormLayout(ENCODED_DIALOG_COLUMN_SPECS), panel);
        }
        dialogBuilder.border(Borders.DIALOG);

        dialogBuilder.append(Resources.getString(getClass(), "preferences.title"));

        // create the tabbed pane for the preferences panels
        tabbedPane = new JTabbedPane();

        miscPanel = new MiscPanel(model);

        // add tabs
        tabbedPane.addTab(Resources.getString(getClass(), "tab-misc"), null/* icon */, miscPanel.createPanel(),
            Resources.getString(getClass(), "tab-misc.tooltip"));
        tabbedPane.setMnemonicAt(0, KeyEvent.VK_1);

        // add the tabbed pane to the dialog
        dialogBuilder.appendRow("3dlu");
        dialogBuilder.appendRow("fill:200px:grow");
        dialogBuilder.nextLine(2);
        dialogBuilder.append(tabbedPane, 3);
        dialogBuilder.nextLine();

        // prepare buttons
        JButton save = new JButton(Resources.getString(getClass(), "save"));
        save.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                fireSave(listener);
            }
        });

        JButton cancel = new JButton(Resources.getString(getClass(), "cancel"));
        cancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                fireCancel(listener);
            }
        });
        JPanel buttons = new ButtonBarBuilder().addGlue().addButton(save, cancel).border(Borders.DLU4).build();

        dialogBuilder.appendRow("p");
        dialogBuilder.nextLine(1);

        dialogBuilder.append(buttons, 3);

        JPanel contentPanel = dialogBuilder.build();

        getContentPane().add(contentPanel);

        pack();
        setLocationRelativeTo(parent);

        setMinimumSize(getSize());
        setVisible(true);
    }

    protected void fireCancel(final PreferencesViewListener listener) {
        listener.cancel();
    }

    private void fireSave(final PreferencesViewListener listener) {
        listener.save();
    }
}
