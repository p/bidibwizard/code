package org.bidib.wizard.spy.tray;

public interface TrayActionExecutor {

    /**
     * Exit the application.
     */
    void exit();

    /**
     * Show the preferences.
     */
    void showPreferences();

    /**
     * Connect the ports for the spy.
     */
    void connect();

    /**
     * Disconnect the ports for the spy.
     */
    void disconnect();

    boolean isConnectEnabled();

    boolean isDisconnectEnabled();
}
