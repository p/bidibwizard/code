package org.bidib.wizard.spy.preferences.model;

import com.jgoodies.binding.beans.Model;

public class PreferencesModel extends Model {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_INTERFACE_PORT = "interfacePort";

    public static final String PROPERTY_PROXY_PORT = "proxyPort";

    private String interfacePort;

    private String proxyPort;

    public void setInterfacePort(String interfacePort) {
        String oldValue = this.interfacePort;
        this.interfacePort = interfacePort;

        firePropertyChange(PROPERTY_INTERFACE_PORT, oldValue, interfacePort);
    }

    public String getInterfacePort() {
        return interfacePort;
    }

    public void setProxyPort(String proxyPort) {
        String oldValue = this.proxyPort;
        this.proxyPort = proxyPort;

        firePropertyChange(PROPERTY_PROXY_PORT, oldValue, proxyPort);
    }

    public String getProxyPort() {
        return proxyPort;
    }
}
