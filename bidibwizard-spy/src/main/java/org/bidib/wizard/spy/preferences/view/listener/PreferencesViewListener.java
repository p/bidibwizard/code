package org.bidib.wizard.spy.preferences.view.listener;

public interface PreferencesViewListener {

    /**
     * User pressed save.
     */
    void save();

    /**
     * User pressed cancel.
     */
    void cancel();
}
