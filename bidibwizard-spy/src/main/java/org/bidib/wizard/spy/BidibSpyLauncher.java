package org.bidib.wizard.spy;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import javax.swing.SwingUtilities;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;
import ch.qos.logback.core.util.StatusPrinter;

public class BidibSpyLauncher {

    private static final Logger LOGGER = LoggerFactory.getLogger(BidibSpyLauncher.class);

    /**
     * Reload the logger configuration to make sure the changed settings for logfile location are applied. Make sure the
     * Preferences are saved before calling this method.
     * 
     * @param preferencesPath
     *            the preferences path
     */
    public void reloadLoggerConfiguration(String preferencesPath) {

        // assume SLF4J is bound to logback in the current environment
        LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();

        try {
            JoranConfigurator configurator = new JoranConfigurator();
            configurator.setContext(context);
            // Call context.reset() to clear any previous configuration, e.g. default
            // configuration. For multi-step configuration, omit calling context.reset().
            context.reset();

            File logbackFile = new File(preferencesPath, "logback-proxy.xml");
            if (!logbackFile.exists()) {
                // fallback to installation directory
                logbackFile = new File("logback-proxy.xml");
            }
            LOGGER.info("logbackFile: {}", logbackFile.getAbsolutePath());
            InputStream logbackConfig = null;
            if (logbackFile.exists()) {
                try {
                    logbackConfig = new BufferedInputStream(new FileInputStream(logbackFile));
                }
                catch (FileNotFoundException ex) {
                    LOGGER.warn("Load user-defined logback configuration failed.", ex);
                }
            }
            if (logbackConfig == null) {
                LOGGER.info("Use default logback config.");
                logbackConfig = BidibSpyLauncher.class.getResourceAsStream("/config/logback-proxy-default.xml");
            }
            else {
                LOGGER.info("Using user-defined logback config.");
            }
            configurator.doConfigure(logbackConfig);
        }
        catch (JoranException je) {
            // StatusPrinter will handle this
        }

        StatusPrinter.printInCaseOfErrorsOrWarnings(context);
    }

    public static void main(final String[] args) {
        // open GUI
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                // get the preferences path from a jvm property (set with -Dbidib.preferencesPath=....
                String preferencesPath = System.getProperty("bidib.preferencesPath");

                // if the path is not set, use the value from the environment
                if (StringUtils.isBlank(preferencesPath)) {
                    preferencesPath = System.getenv("bidib.preferencesPath");
                    // if the path is not set use the user.home
                    if (StringUtils.isBlank(preferencesPath)) {
                        preferencesPath = System.getProperty("user.home");

                        System.setProperty("bidib.preferencesPath", preferencesPath);
                    }
                }

                new BidibSpyLauncher().reloadLoggerConfiguration(preferencesPath);
                new BidibSpy().startApp(args);
            }
        });
    }
}
