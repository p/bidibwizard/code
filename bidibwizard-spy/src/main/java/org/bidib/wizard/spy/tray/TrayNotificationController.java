package org.bidib.wizard.spy.tray;

import java.awt.TrayIcon.MessageType;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import javax.swing.SwingUtilities;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The {@code TrayNotificationController} handles the display of the notifications in the tray for the spy.
 */
public class TrayNotificationController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TrayNotificationController.class);

    private NotificationQueueWatcher watcher;

    private BlockingQueue<BalloonMessage> notificationQueue = new LinkedBlockingQueue<>();

    private final ScheduledExecutorService hotplugWorker = Executors.newScheduledThreadPool(1);

    private ScheduledFuture<?> watcherFuture;

    public static final class BalloonMessage {
        String caption;

        String message;

        MessageType type;

        public BalloonMessage(String caption, String message, MessageType type) {
            this.caption = caption;
            this.message = message;
            this.type = type;
        }

        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
        }
    }

    public void start(final Tray tray) {

        SwingUtilities.invokeLater(new Runnable() {
            public void run() {

                try {
                    LOGGER.info("Start the watcher for events.");

                    watcher = new NotificationQueueWatcher(tray);

                    watcherFuture = hotplugWorker.schedule(watcher, 0, TimeUnit.MILLISECONDS);
                    LOGGER.info("Create and start TrayNotificationEventWatcher passed.");
                }
                catch (Exception ex) {
                    LOGGER.warn("Start TrayNotificationEventWatcher failed.", ex);
                }
            }
        });
    }

    public void addInfoNotification(String caption, String message) {
        BalloonMessage alert = new BalloonMessage(caption, message, MessageType.INFO);
        notificationQueue.add(alert);
    }

    public void addErrorNotification(String caption, String message) {
        BalloonMessage alert = new BalloonMessage(caption, message, MessageType.ERROR);
        notificationQueue.add(alert);
    }

    public void stopWatcher() {
        if (watcher != null) {
            LOGGER.info("Stop the watcher.");

            try {
                watcher.stop();
            }
            catch (Exception ex) {
                LOGGER.warn("Stop watcher failed.", ex);
            }

            if (watcherFuture != null) {
                LOGGER.info("Wait for termination of watcher.");
                try {
                    watcherFuture.cancel(true);
                    watcherFuture.get();
                }
                catch (Exception ex) {
                    LOGGER.warn("Stop watcher failed.", ex);
                }

                watcherFuture = null;
                LOGGER.info("Watcher has terminated.");
            }
        }
    }

    private final class NotificationQueueWatcher implements Runnable {

        private final Tray tray;

        boolean leaveLoop = false;

        public NotificationQueueWatcher(final Tray tray) {
            this.tray = tray;
        }

        public void stop() {
            leaveLoop = true;
        }

        @Override
        public void run() {
            LOGGER.info("Start the notification queue watcher.");
            do {

                try {
                    BalloonMessage alert = notificationQueue.take();
                    if (alert != null) {
                        LOGGER.info("Show alert: {}", alert);
                        SwingUtilities.invokeLater(new Runnable() {
                            public void run() {
                                LOGGER.info("Show alert: {}", alert);

                                try {

                                    tray.showBalloonMessage(alert.caption, alert.message, alert.type);
                                }
                                catch (Exception ex) {
                                    LOGGER.warn("Show alert in tray failed.", ex);
                                }
                            }
                        });
                    }
                }
                catch (InterruptedException ex) {
                    LOGGER.info("Wait for notification was interrupted. Leave the loop.");
                    leaveLoop = true;
                }
                catch (Exception ex) {
                    LOGGER.info("Wait for notification caused an exception.", ex);
                }
            }
            while (!leaveLoop);

        }

    }
}
