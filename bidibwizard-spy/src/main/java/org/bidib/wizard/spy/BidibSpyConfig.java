package org.bidib.wizard.spy;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import org.apache.commons.configuration2.CombinedConfiguration;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.YAMLConfiguration;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.configuration2.io.FileBased;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BidibSpyConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(BidibSpyConfig.class);

    public static final String DEFAULT_CONFIG_FILENAME = "bidib-spy.yml";

    public static final String CONFIG_YAML_FILE = "yaml";

    private CombinedConfiguration config = new CombinedConfiguration();

    private final String configFileName;

    public BidibSpyConfig(String configFileName) {

        if (StringUtils.isBlank(configFileName)) {
            configFileName = DEFAULT_CONFIG_FILENAME;
        }
        this.configFileName = configFileName;
    }

    public String getConfigFileName() {
        return configFileName;
    }

    private String getPreferencesPath() {

        // get the preferences path from a jvm property (set with -Dbidib.preferencesPath=....
        String preferencesPath = System.getProperty("bidib.preferencesPath");

        // if the path is not set, use the value from the environment
        if (StringUtils.isBlank(preferencesPath)) {
            preferencesPath = System.getenv("bidib.preferencesPath");
            // if the path is not set use the user.home
            if (StringUtils.isBlank(preferencesPath)) {
                preferencesPath = System.getProperty("user.home");

                System.setProperty("bidib.preferencesPath", preferencesPath);
            }
        }

        LOGGER.info("Resolved current preferences path: {}", preferencesPath);
        return preferencesPath;
    }

    public void loadConfig() {

        YAMLConfiguration yamlConfig = new YAMLConfiguration();

        config.addConfiguration(yamlConfig, CONFIG_YAML_FILE);

        try {
            // Read data from this file
            File propertiesFile = new File(getPreferencesPath(), configFileName);

            yamlConfig.read(new FileReader(propertiesFile));
        }
        catch (ConfigurationException cex) {
            LOGGER.warn("Load configuration properties failed.", cex);

        }
        catch (FileNotFoundException ex) {
            LOGGER.warn("Load configuration properties failed because YAML file was not found.", ex);
        }
    }

    public CombinedConfiguration getConfig() {
        return config;
    }

    public void saveConfig(String name) {
        Configuration subConfig = config.getConfiguration(name);
        if (subConfig instanceof FileBased) {

            FileBased fileBased = (FileBased) subConfig;

            File propertiesFile = new File(getPreferencesPath(), configFileName);
            try (final Writer writer = new FileWriter(propertiesFile)) {
                fileBased.write(writer);
            }
            catch (IOException ex) {
                LOGGER.warn("Write preferences data to file failed.", ex);
            }
            catch (ConfigurationException ex) {
                LOGGER.warn("Write preferences data to file failed.", ex);
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("BidibProxyConfig, configFileName: ");
        sb.append(configFileName);
        return sb.toString();
    }
}
