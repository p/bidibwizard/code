package org.bidib.wizard.spy;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import org.apache.commons.lang3.StringUtils;
import org.bidib.jbidibc.com0com.BidibPortProvider;
import org.bidib.jbidibc.com0com.BidibPortStatusListener;
import org.bidib.wizard.spy.preferences.controller.PreferencesController;
import org.bidib.wizard.spy.tray.Tray;
import org.bidib.wizard.spy.tray.TrayActionExecutor;
import org.bidib.wizard.spy.tray.TrayNotificationController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BidibSpy implements TrayActionExecutor {

    private static final Logger LOGGER = LoggerFactory.getLogger(BidibSpy.class);

    private Tray tray;

    private String interfacePort;

    private String proxyPort;

    private BidibPortProvider provider;

    private BidibSpyConfig config = new BidibSpyConfig(null);

    private TrayNotificationController notificationController;

    public void startApp(String[] args) {

        config.loadConfig();

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch (Exception ex) {
            LOGGER.error("Change to use system look and feel failed.", ex);
        }

        // create the system tray icon
        tray = new Tray(this);
        tray.init();

        notificationController = new TrayNotificationController();
        notificationController.start(tray);

        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                LOGGER.info("Shutdown hook is executed.");

                exit();
            }
        });

        notificationController.addInfoNotification("BiDiB-Wizard-Spy",
            "The spy is started. Use the tray menu to connect.");
    }

    @Override
    public void exit() {

        disconnect();

        if (tray != null) {
            tray.cleanup();

            tray = null;
        }

        if (SwingUtilities.isEventDispatchThread()) {
            System.exit(0);
        }
    }

    @Override
    public void showPreferences() {
        // TODO Auto-generated method stub
        PreferencesController controller = new PreferencesController(JOptionPane.getFrameForComponent(null));
        controller.start(config);
    }

    @Override
    public void connect() {

        try {
            LOGGER.info("Create the provider the interface and proxy port.");

            provider = new BidibPortProvider();
            provider.addStatusListener(new BidibPortStatusListener() {

                @Override
                public void statusChanged(PortStatus portStatus) {
                    tray.setPortStatus(portStatus);
                }
            });

            provider.start(interfacePort, proxyPort);

            notificationController.addInfoNotification("BiDiB-Wizard-Spy",
                "Connect to interface and proxy port passed.");

            LOGGER.info("Start interface and proxy port passed.");
        }
        catch (Exception ex) {
            LOGGER.warn("Connect to interface and proxy port failed.", ex);

            notificationController.addErrorNotification("BiDiB-Wizard-Spy",
                "Connect to interface and proxy port failed.");

            stopProvider();
        }
    }

    @Override
    public void disconnect() {

        stopProvider();

        notificationController.addInfoNotification("BiDiB-Wizard-Spy",
            "Disconnect from interface and proxy port passed.");
    }

    private void stopProvider() {
        // TODO Auto-generated method stub
        try {
            if (provider != null) {
                LOGGER.info("Stop the provider.");
                provider.stop();

                provider = null;
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Stop provider failed.", ex);
        }

    }

    @Override
    public boolean isConnectEnabled() {
        return (provider == null && isSpyConfigValid());
    }

    @Override
    public boolean isDisconnectEnabled() {
        return (provider != null);
    }

    private boolean isSpyConfigValid() {
        interfacePort = config.getConfig().getString("spy.interfacePort");
        proxyPort = config.getConfig().getString("spy.proxyPort");

        LOGGER.info("Loaded properties, interfacePort: {}, proxyPort: {}", interfacePort, proxyPort);

        if (StringUtils.isBlank(interfacePort) || StringUtils.isBlank(proxyPort)) {
            LOGGER.error(
                "The configuration properties has missing valus for 'interfacePort' and/or 'proxyPort'. Add these values to properties file: {}",
                BidibSpyConfig.DEFAULT_CONFIG_FILENAME);

            return false;
        }

        return true;
    }
}
