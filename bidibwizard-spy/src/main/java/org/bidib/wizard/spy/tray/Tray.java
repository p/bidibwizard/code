package org.bidib.wizard.spy.tray;

import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.SystemTray;
import java.awt.TrayIcon.MessageType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.event.PopupMenuEvent;

import org.bidib.jbidibc.com0com.BidibPortStatusListener.PortStatus;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.common.utils.ImageUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Tray {

    private static final Logger LOGGER = LoggerFactory.getLogger(Tray.class);

    private JXTrayIcon trayIcon;

    private ImageIcon wizardTrayIcon;

    private ImageIcon wizardTrayIconConnected;

    private ImageIcon wizardTrayIconDisconnected;

    private final TrayActionExecutor trayActionExecutor;

    public Tray(final TrayActionExecutor trayActionExecutor) {
        this.trayActionExecutor = trayActionExecutor;
    }

    public void init() {
        SystemTray systemTray = SystemTray.getSystemTray();
        Dimension dimension = systemTray.getTrayIconSize();

        wizardTrayIcon =
            ImageUtils.createImageIcon(Tray.class, "/icons/wizard/logo.png", dimension.width, dimension.height);
        wizardTrayIconConnected =
            ImageUtils.createImageIcon(Tray.class, "/icons/wizard/proxy-connected.png", dimension.width,
                dimension.height);
        wizardTrayIconDisconnected =
            ImageUtils.createImageIcon(Tray.class, "/icons/wizard/proxy-disconnected.png", dimension.width,
                dimension.height);

        trayIcon = new JXTrayIcon(wizardTrayIconDisconnected.getImage()) {
            @Override
            protected void updatePopupMenu(PopupMenuEvent e) {
                super.updatePopupMenu(e);

                Tray.this.updatePopupMenu();
            }
        };
        trayIcon.setToolTip(Resources.getString(Tray.class, "application.name"));

        trayIcon.setJPopuMenu(createJPopupMenu());

        try {
            systemTray.add(trayIcon);
        }
        catch (AWTException e) {
            LOGGER.error("Tray icon failure", e);
        }

    }

    /**
     * Show a balloon message on the tray icon.
     * 
     * @param caption
     *            the caption of the message
     * @param message
     *            the message text
     * @param type
     *            the type
     */
    public void showBalloonMessage(String caption, String message, MessageType type) {
        trayIcon.displayMessage(caption, message, type);
    }

    public void cleanup() {
        LOGGER.info("Removing system tray icon");
        try {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    SystemTray tray = SystemTray.getSystemTray();
                    tray.remove(trayIcon);
                }
            });
            LOGGER.info("System tray icon removed");
        }
        catch (Exception ex) {
            LOGGER.warn("Remove system tray icon failed.", ex);
        }
        catch (Error ex) {
            LOGGER.warn("Remove system tray icon failed.", ex);
        }
    }

    private JMenuItem connectItem;

    private JMenuItem disconnectItem;

    private JPopupMenu createJPopupMenu() {

        JPopupMenu popup = new JPopupMenu();

        JMenuItem preferencesItem = new JMenuItem(Resources.getString(Tray.class, "preferencesMenuItemLabelText"));
        // preferencesItem.setEnabled(false);
        popup.add(preferencesItem);

        connectItem =
            new JMenuItem(Resources.getString(Tray.class, "connectMenuItemLabelText"), wizardTrayIconConnected);
        // enable only of the config is valid
        connectItem.setEnabled(trayActionExecutor.isConnectEnabled());
        popup.add(connectItem);

        disconnectItem =
            new JMenuItem(Resources.getString(Tray.class, "disconnectMenuItemLabelText"), wizardTrayIconDisconnected);
        disconnectItem.setEnabled(trayActionExecutor.isDisconnectEnabled());
        popup.add(disconnectItem);

        JMenuItem exitItem = new JMenuItem(Resources.getString(Tray.class, "exitMenuItemLabelText"));
        exitItem.setEnabled(true);
        popup.add(exitItem);

        exitItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                LOGGER.info("Exit action");

                trayActionExecutor.exit();
            }
        });

        preferencesItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                LOGGER.info("Preferences action");

                trayActionExecutor.showPreferences();
            }
        });

        connectItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                trayActionExecutor.connect();
            }
        });

        disconnectItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                trayActionExecutor.disconnect();
            }
        });

        return popup;
    }

    protected void updatePopupMenu() {
        if (connectItem != null) {
            connectItem.setEnabled(trayActionExecutor.isConnectEnabled());
        }
        if (disconnectItem != null) {
            disconnectItem.setEnabled(trayActionExecutor.isDisconnectEnabled());
        }
    }

    public void setPortStatus(final PortStatus portStatus) {

        SwingUtilities.invokeLater(() -> {
            switch (portStatus) {
                case CONNECTED:
                    trayIcon.setImage(wizardTrayIconConnected.getImage());
                    break;
                default:
                    trayIcon.setImage(wizardTrayIconDisconnected.getImage());
                    break;
            }
        });

    }
}
