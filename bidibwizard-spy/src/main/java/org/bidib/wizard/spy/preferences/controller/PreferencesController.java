package org.bidib.wizard.spy.preferences.controller;

import java.awt.Frame;

import org.apache.commons.configuration2.Configuration;
import org.bidib.wizard.spy.BidibSpyConfig;
import org.bidib.wizard.spy.preferences.model.PreferencesModel;
import org.bidib.wizard.spy.preferences.view.PreferencesView;
import org.bidib.wizard.spy.preferences.view.listener.PreferencesViewListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PreferencesController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PreferencesController.class);

    private final Frame parent;

    // private PreferencesModel model;

    public PreferencesController(Frame parent) {
        this.parent = parent;
    }

    public void start(final BidibSpyConfig config) {
        LOGGER.debug("Start the preferences controller.");
        final PreferencesModel model = createFromConfig(config);

        final PreferencesViewListener listener = new PreferencesViewListener() {

            @Override
            public void save() {
                // TODO Auto-generated method stub
                LOGGER.info("Save the configuration.");
                updateConfig(config, BidibSpyConfig.CONFIG_YAML_FILE, model);
                config.saveConfig(BidibSpyConfig.CONFIG_YAML_FILE);
            }

            @Override
            public void cancel() {
                // TODO Auto-generated method stub

            }
        };
        PreferencesView view = new PreferencesView(parent, model, listener);

        LOGGER.info("After show view ...");
    }

    protected PreferencesModel createFromConfig(final BidibSpyConfig config) {
        PreferencesModel preferencesModel = new PreferencesModel();

        String interfacePort = config.getConfig().getString("spy.interfacePort");
        String proxyPort = config.getConfig().getString("spy.proxyPort");

        preferencesModel.setInterfacePort(interfacePort);
        preferencesModel.setProxyPort(proxyPort);

        return preferencesModel;
    }

    protected void updateConfig(
        final BidibSpyConfig spyConfig, String configName, final PreferencesModel preferencesModel) {
        Configuration conf = spyConfig.getConfig().getConfiguration(configName);
        conf.setProperty("spy.interfacePort", preferencesModel.getInterfacePort());
        conf.setProperty("spy.proxyPort", preferencesModel.getProxyPort());
    }
}
