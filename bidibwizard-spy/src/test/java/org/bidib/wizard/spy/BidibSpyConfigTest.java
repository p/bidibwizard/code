package org.bidib.wizard.spy;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.commons.io.FileUtils;
import org.bidib.wizard.spy.BidibSpyConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class BidibSpyConfigTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(BidibSpyConfigTest.class);

    private static final String OUTPUT_TARGET_DIR = "target/proxy-config-test";

    @BeforeTest
    public void prepareOutputDirectory() {
        LOGGER.info("Make sure the output target directory exists: {}", OUTPUT_TARGET_DIR);

        try {
            File outputDir = new File(OUTPUT_TARGET_DIR);
            if (!outputDir.exists()) {
                outputDir.mkdirs();
            }
            else {
                FileUtils.cleanDirectory(outputDir);
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Create output directory failed: {}", OUTPUT_TARGET_DIR, ex);
        }
    }

    @Test
    public void writeProxyConfigTest() throws IOException, URISyntaxException {
        LOGGER.info("writeProxyConfigTest is started");

        System.setProperty("bidib.preferencesPath", OUTPUT_TARGET_DIR);

        BidibSpyConfig config = new BidibSpyConfig("proxy-config-test.yml");

        Assert.assertNotNull(config);
        Assert.assertNotNull(config.getConfigFileName());

        LOGGER.info("Current config: {}", config);

        config.loadConfig();

        config.getConfig().getConfiguration(BidibSpyConfig.CONFIG_YAML_FILE).setProperty("proxy.test.value1", "test");
        config.saveConfig(BidibSpyConfig.CONFIG_YAML_FILE);

        File file = new File(OUTPUT_TARGET_DIR, "proxy-config-test.yml");
        Assert.assertTrue(file.exists());

        LOGGER.info("writeProxyConfigTest has finished");
    }

    @Test
    public void loadProxyConfigTest() throws IOException, URISyntaxException {
        LOGGER.info("loadProxyConfigTest is started");

        System.setProperty("bidib.preferencesPath", "target/test-classes/config-test");

        BidibSpyConfig config = new BidibSpyConfig("proxy-config-test.yml");

        Assert.assertNotNull(config);
        Assert.assertNotNull(config.getConfigFileName());

        LOGGER.info("Current config: {}", config);

        config.loadConfig();

        String testValue = config.getConfig().getString("proxy.test.value1");
        Assert.assertNotNull(testValue);
        Assert.assertEquals(testValue, "test-read");

        LOGGER.info("loadProxyConfigTest has finished");
    }
}
