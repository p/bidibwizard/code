Preparation to build
====================

A working Maven 3 installation is required.

Build
=====

!!! Linux 64-bit !!!
If the build fails in the launch4j part:
Installing lib32z1 lib32ncurses5 lib32bz2-1.0 (has been ia32-libs in older Ubuntu versions) solves the issue. 
See https://github.com/lukaszlenart/launch4j-maven-plugin/issues/4


Run the maven build in the bibid directory: 'mvn install' (or to rebuild all: 'mvn clean install').

The installer artifact is located unter 'bidibwizard-installer/target'.

Local testing
=============

After the maven build has completed successfully the unpackaged installation is located under 'bidibwizard-installer/target/staging'. For testing 
purposes you can start the BiDiBWizard with the following command line: java -jar bidibwizard-<version>.jar


Eclipse
=======

Use latest Eclipse (Mars SR1 or newer, m2e tools are included).

Start Eclipse with clean workspace (m2e support added by default) if you don't use the same workspace for jbidibc! 

* Prepare:

Install TestNG Plugin
http://testng.org/doc/download.html

Configure Maven installation in Eclipse:
Window > Preferences > Maven > Installations > Add ... and select your local maven installation to use. 
Click Finish button in the dialog and check the new maven installation. Press Apply. 
Verify under Maven > User Settings that the correct user settings (settings.xml) and local maven repository location is used if you have 
changed the default settings.

* Import project:
! The maven installation steps must be executed before importing projects !

Import the maven project: File -> Import -> Maven > Existing Maven Projects -> 
Root directory > Select the bidib directory > Select the projects -> Finish

