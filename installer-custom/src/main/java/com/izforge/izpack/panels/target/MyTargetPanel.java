/*
 * IzPack - Copyright 2001-2008 Julien Ponge, All Rights Reserved.
 *
 * http://izpack.org/
 * http://izpack.codehaus.org/
 *
 * Copyright 2004 Klaus Bartz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.izforge.izpack.panels.target;

import java.io.File;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;

import com.izforge.izpack.api.data.Panel;
import com.izforge.izpack.api.resource.Resources;
import com.izforge.izpack.gui.log.Log;
import com.izforge.izpack.installer.data.GUIInstallData;
import com.izforge.izpack.installer.gui.InstallerFrame;
import com.izforge.izpack.util.OsVersion;

/**
 * The target directory selection panel.
 * 
 * @author Julien Ponge
 */
public class MyTargetPanel extends TargetPanel {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * The logger.
     */
    private static final Logger LOGGER = Logger.getLogger(MyTargetPanel.class.getName());

    /**
     * Constructs a <tt>TargetPanel</tt>.
     * 
     * @param panel
     *            the panel meta-data
     * @param parent
     *            the parent window
     * @param installData
     *            the installation data
     * @param resources
     *            the resources
     * @param log
     *            the log
     */
    public MyTargetPanel(Panel panel, InstallerFrame parent, GUIInstallData installData, Resources resources, Log log) {
        super(panel, parent, installData, resources, log);
    }

    @Override
    public void saveData() {
        String path = this.pathSelectionPanel.getPath();
        if (OsVersion.IS_OSX) {
            // check if the path ends with .app and add this postfix if not
            if (path != null && !path.endsWith(".app/Contents")) {
                LOGGER.info("Adding '.app' to path because of OSX.");
                this.installData.setInstallPath(path + ".app/Contents");
            }
            else {
                this.installData.setInstallPath(path);
            }
        }
        else {
            this.installData.setInstallPath(path);
        }
    }

    /**
     * Indicates whether the panel has been validated or not.
     * 
     * @return Whether the panel has been validated or not.
     */
    @Override
    public boolean isValidated() {
        boolean result = false;
        if (TargetPanelHelper.isIncompatibleInstallation(pathSelectionPanel.getPath(), true)) {
            LOGGER.info("Found incompatible installation. Remove all files.");
            result = FileUtils.deleteQuietly(new File(pathSelectionPanel.getPath()));
            if (!result) {
                String message = getString("TargetPanel.incompatibleInstallation");
                if (message != null && message.equals("TargetPanel.incompatibleInstallation")) {
                    message =
                        "Eine inkompatible Installation wurde erkannt. Deinstallieren sie diese oder w&auml;hlen Sie ein anderes Installations-Verzeichnis.";
                }
                emitError(getString("installer.error"), message);
            }
            else {
                installData.setInstallPath(pathSelectionPanel.getPath());
                result = true;
            }
        }
        else if (super.isValidated()) {
            installData.setInstallPath(pathSelectionPanel.getPath());
            result = true;
        }
        return result;
    }
}
