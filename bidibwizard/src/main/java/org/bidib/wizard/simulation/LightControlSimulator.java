package org.bidib.wizard.simulation;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.BidibPort;
import org.bidib.jbidibc.core.Feature;
import org.bidib.jbidibc.core.LcConfigX;
import org.bidib.jbidibc.core.LcMacro;
import org.bidib.jbidibc.core.enumeration.IoBehaviourSwitchEnum;
import org.bidib.jbidibc.core.enumeration.LcMacroOperationCode;
import org.bidib.jbidibc.core.enumeration.LcMacroState;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.LightPortEnum;
import org.bidib.jbidibc.core.enumeration.SwitchPortEnum;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.message.AccessoryGetMessage;
import org.bidib.jbidibc.core.message.AccessoryParaGetMessage;
import org.bidib.jbidibc.core.message.AccessoryParaResponse;
import org.bidib.jbidibc.core.message.AccessoryParaSetMessage;
import org.bidib.jbidibc.core.message.AccessorySetMessage;
import org.bidib.jbidibc.core.message.AccessoryStateResponse;
import org.bidib.jbidibc.core.message.BidibCommand;
import org.bidib.jbidibc.core.message.LcConfigGetMessage;
import org.bidib.jbidibc.core.message.LcConfigResponse;
import org.bidib.jbidibc.core.message.LcConfigSetMessage;
import org.bidib.jbidibc.core.message.LcConfigXGetAllMessage;
import org.bidib.jbidibc.core.message.LcConfigXGetMessage;
import org.bidib.jbidibc.core.message.LcConfigXResponse;
import org.bidib.jbidibc.core.message.LcConfigXSetMessage;
import org.bidib.jbidibc.core.message.LcKeyMessage;
import org.bidib.jbidibc.core.message.LcKeyResponse;
import org.bidib.jbidibc.core.message.LcMacroGetMessage;
import org.bidib.jbidibc.core.message.LcMacroHandleMessage;
import org.bidib.jbidibc.core.message.LcMacroParaGetMessage;
import org.bidib.jbidibc.core.message.LcMacroParaResponse;
import org.bidib.jbidibc.core.message.LcMacroParaSetMessage;
import org.bidib.jbidibc.core.message.LcMacroResponse;
import org.bidib.jbidibc.core.message.LcMacroSetMessage;
import org.bidib.jbidibc.core.message.LcMacroStateResponse;
import org.bidib.jbidibc.core.message.LcNotAvailableResponse;
import org.bidib.jbidibc.core.message.LcOutputMessage;
import org.bidib.jbidibc.core.message.LcOutputQueryMessage;
import org.bidib.jbidibc.core.message.LcPortQueryAllMessage;
import org.bidib.jbidibc.core.message.LcStatResponse;
import org.bidib.jbidibc.core.port.BytePortConfigValue;
import org.bidib.jbidibc.core.port.Int16PortConfigValue;
import org.bidib.jbidibc.core.port.PortConfigValue;
import org.bidib.jbidibc.core.port.RgbPortConfigValue;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.core.utils.MacroUtils;
import org.bidib.jbidibc.core.utils.MessageUtils;
import org.bidib.jbidibc.simulation.SwitchingFunctionsNode;
import org.bidib.jbidibc.simulation.net.SimulationBidibMessageProcessor;
import org.bidib.jbidibc.simulation.nodes.DefaultNodeSimulator;
import org.bidib.jbidibc.simulation.nodes.InputPortType;
import org.bidib.jbidibc.simulation.nodes.LightPortParamsType;
import org.bidib.jbidibc.simulation.nodes.LightPortType;
import org.bidib.jbidibc.simulation.nodes.PortType;
import org.bidib.jbidibc.simulation.nodes.ServoPortType;
import org.bidib.jbidibc.simulation.nodes.SwitchPortType;
import org.bidib.wizard.comm.InputPortStatus;
import org.bidib.wizard.comm.LightPortStatus;
import org.bidib.wizard.comm.ServoPortStatus;
import org.bidib.wizard.comm.SwitchPortStatus;
import org.bidib.wizard.mvc.main.model.InputPort;
import org.bidib.wizard.mvc.main.model.LightPort;
import org.bidib.wizard.mvc.main.model.Port;
import org.bidib.wizard.mvc.main.model.ServoPort;
import org.bidib.wizard.mvc.main.model.SwitchPort;
import org.bidib.wizard.simulation.events.InputPortSetStatusEvent;
import org.bidib.wizard.simulation.events.InputPortStatusEvent;
import org.bidib.wizard.simulation.events.LightPortStatusEvent;
import org.bidib.wizard.simulation.events.ServoPortStatusEvent;
import org.bidib.wizard.simulation.events.SwitchPortStatusEvent;
import org.bidib.wizard.simulation.macro.MacroContainer;
import org.bushe.swing.event.EventBus;
import org.bushe.swing.event.annotation.AnnotationProcessor;
import org.bushe.swing.event.annotation.EventSubscriber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LightControlSimulator extends DefaultNodeSimulator implements SwitchingFunctionsNode {
    private static final Logger LOGGER = LoggerFactory.getLogger(LightControlSimulator.class);

    private static final String SIMULATION_PANEL_CLASS = "org.bidib.wizard.mvc.simulation.view.panel.LightControlPanel";

    protected final Map<Integer, InputPort> inputPorts = new HashMap<Integer, InputPort>();

    protected final Map<Integer, LightPort> lightPorts = new HashMap<Integer, LightPort>();

    protected final Map<Integer, SwitchPort> switchPorts = new HashMap<Integer, SwitchPort>();

    protected final Map<Integer, ServoPort> servoPorts = new HashMap<Integer, ServoPort>();

    protected final Map<Integer, MacroContainer> macros = new HashMap<Integer, MacroContainer>();

    protected int inputPortCount;

    protected int inputPortOffset;

    protected int lightPortCount;

    protected int servoPortCount;

    protected int switchPortCount;

    public LightControlSimulator(byte[] nodeAddress, long uniqueId, boolean autoAddFeature,
        SimulationBidibMessageProcessor messageReceiver) {
        super(nodeAddress, uniqueId, autoAddFeature, messageReceiver);
    }

    @Override
    protected void prepareFeatures() {
        LOGGER.info("Prepare the features.");
        super.prepareFeatures();

        // features.add(new Feature(BidibLibrary.FEATURE_CTRL_INPUT_COUNT, inputPortCount));
        // features.add(new Feature(BidibLibrary.FEATURE_CTRL_LIGHT_COUNT, lightPortCount));
        // features.add(new Feature(BidibLibrary.FEATURE_CTRL_SERVO_COUNT, servoPortCount));
        // features.add(new Feature(BidibLibrary.FEATURE_CTRL_SWITCH_COUNT, switchPortCount));

        features.add(new Feature(BidibLibrary.FEATURE_CTRL_PORT_QUERY_AVAILABLE, 1));
        features.add(new Feature(BidibLibrary.FEATURE_SWITCH_CONFIG_AVAILABLE, 1));

        features.add(new Feature(BidibLibrary.FEATURE_CTRL_INPUT_NOTIFY, 1));

        features.add(new Feature(BidibLibrary.FEATURE_CTRL_MAC_COUNT, 40));
        features.add(new Feature(BidibLibrary.FEATURE_CTRL_MAC_SIZE, 38));
        features.add(new Feature(BidibLibrary.FEATURE_CTRL_MAC_SAVE, 40));
        features.add(new Feature(BidibLibrary.FEATURE_CTRL_MAC_LEVEL, 2));

        features.add(new Feature(BidibLibrary.FEATURE_ACCESSORY_COUNT, 20));
        features.add(new Feature(BidibLibrary.FEATURE_ACCESSORY_SURVEILLED, 0));
        features.add(new Feature(BidibLibrary.FEATURE_ACCESSORY_MACROMAPPED, 2));
    }

    @Override
    public void postConstruct() {
        super.postConstruct();

        features.add(new Feature(BidibLibrary.FEATURE_CTRL_INPUT_COUNT, inputPortCount));
        features.add(new Feature(BidibLibrary.FEATURE_CTRL_LIGHT_COUNT, lightPortCount));
        features.add(new Feature(BidibLibrary.FEATURE_CTRL_SERVO_COUNT, servoPortCount));
        features.add(new Feature(BidibLibrary.FEATURE_CTRL_SWITCH_COUNT, switchPortCount));
    }

    @Override
    protected void prepareCVs() {
        super.prepareCVs();

        configurationVariables.put("2", "0");
        configurationVariables.put("90", "191");
    }

    @Override
    public String getSimulationPanelClass() {
        return SIMULATION_PANEL_CLASS;
    }

    @Override
    public void start() {
        LOGGER.info("Start the simulator for address: {}", getAddress());

        AnnotationProcessor.process(this);

        // prepare the input ports
        setupInputPorts();

        // prepare the light ports
        setupLightPorts();

        // prepare the switch ports
        setupSwitchPorts();

        // prepare the servo ports
        setupServoPorts();

        super.start();
    }

    @Override
    public void stop() {
        AnnotationProcessor.unprocess(this);
        super.stop();
    }

    protected void setupInputPorts() {
        if (CollectionUtils.isNotEmpty(inputPorts.values())) {
            LOGGER.info("InputPorts are set externally already.");
            return;
        }

        for (int id = 0; id < inputPortCount; id++) {
            InputPort port = new InputPort();

            port.setId(id);
            port.setStatus(id % 2 == 0 ? InputPortStatus.ON : InputPortStatus.OFF);
            inputPorts.put(id, port);
        }
    }

    protected void setupLightPorts() {

        if (CollectionUtils.isNotEmpty(lightPorts.values())) {
            LOGGER.info("LightPorts are set externally already.");
            return;
        }

        for (int id = 0; id < lightPortCount; id++) {
            LightPort port = new LightPort();

            port.setId(id);
            port.setStatus(id % 3 == 0 ? LightPortStatus.ON : LightPortStatus.OFF);

            lightPorts.put(id, port);
        }
    }

    protected void setupSwitchPorts() {
        if (CollectionUtils.isNotEmpty(switchPorts.values())) {
            LOGGER.info("SwitchPorts are set externally already.");
            return;
        }

        for (int id = 0; id < switchPortCount; id++) {
            SwitchPort port = new SwitchPort();

            port.setId(id);
            port.setStatus(SwitchPortStatus.OFF);

            // set some default values
            port.setOutputBehaviour(IoBehaviourSwitchEnum.HIGH);
            port.setSwitchOffTime(15);

            switchPorts.put(id, port);
        }
    }

    protected void setupServoPorts() {
        if (CollectionUtils.isNotEmpty(servoPorts.values())) {
            LOGGER.info("ServoPorts are set externally already.");
            return;
        }

        LOGGER.info("Create servoPorts, count: {}", servoPortCount);
        for (int id = 0; id < servoPortCount; id++) {
            ServoPort port = new ServoPort();

            port.setId(id);
            port.setRelativeValue((id % 4) * 25);

            byte[] portConfig = new byte[] { 0x30, 0x7B, 0x08 };
            port.setPortConfig(portConfig);

            servoPorts.put(id, port);
        }
    }

    @EventSubscriber(eventClass = InputPortSetStatusEvent.class)
    public void inputPortSetStatus(InputPortSetStatusEvent setStatusEvent) {
        LOGGER.info("The change of the input port was requested.");
        String nodeAddress = setStatusEvent.getNodeAddr();

        // check if the node is addressed
        if (!isAddressEqual(nodeAddress)) {
            LOGGER.trace("Another node is addressed.");
            return;
        }

        int portNum = setStatusEvent.getPortNum();

        changeInputPortStatus(portNum);

    }

    protected void changeInputPortStatus(int portNum) {

        InputPort port = inputPorts.get(portNum);
        if (port != null) {
            switch (port.getStatus()) {
                case OFF:
                    port.setStatus(InputPortStatus.ON);
                    break;
                default:
                    port.setStatus(InputPortStatus.OFF);
                    break;
            }

            // prepare the request
            final LcKeyMessage lcKeyMessage = new LcKeyMessage(portNum) {
                @Override
                public byte[] getAddr() {
                    return getNodeAddress();
                }
            };
            processRequest(lcKeyMessage);
        }
        else {
            LOGGER.warn("The requested light port is not available: {}", portNum);
        }
    }

    @Override
    protected byte[] prepareResponse(BidibCommand bidibMessage) {

        byte[] response = null;
        switch (ByteUtils.getInt(bidibMessage.getType())) {
            case BidibLibrary.MSG_LC_OUTPUT:
                response = processLcOutputRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_LC_CONFIG_GET:
                response = processLcConfigGetRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_LC_CONFIG_SET:
                response = processLcConfigSetRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_LC_KEY_QUERY:
                response = processLcKeyQueryRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_LC_OUTPUT_QUERY:
                response = processLcOutputQueryRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_LC_CONFIGX_SET:
                response = processLcConfigXSetRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_LC_CONFIGX_GET:
                response = processLcConfigXGetRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_LC_CONFIGX_GET_ALL:
                processLcConfigXGetAllRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_LC_PORT_QUERY_ALL:
                processLcPortQueryAllRequest(bidibMessage);
                break;

            case BidibLibrary.MSG_LC_MACRO_HANDLE:
                response = processLcMacroHandleRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_LC_MACRO_PARA_GET:
                response = processLcMacroParaGetRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_LC_MACRO_PARA_SET:
                response = processLcMacroParaSetRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_LC_MACRO_GET:
                response = processLcMacroGetRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_LC_MACRO_SET:
                response = processLcMacroSetRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_ACCESSORY_SET:
                response = processAccessorySetRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_ACCESSORY_GET:
                response = processAccessoryGetRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_ACCESSORY_PARA_SET:
                response = processAccessoryParaSetRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_ACCESSORY_PARA_GET:
                response = processAccessoryParaGetRequest(bidibMessage);
                break;
            default:
                response = super.prepareResponse(bidibMessage);
                break;
        }
        return response;
    }

    protected byte[] processLcOutputRequest(BidibCommand bidibMessage) {
        LOGGER.info("Process the LcOutput request: {}", bidibMessage);
        byte[] response = null;
        try {
            LcOutputMessage lcOutputMessage = (LcOutputMessage) bidibMessage;
            LcOutputType outputType = lcOutputMessage.getOutputType(getPortModel());
            int outputNumber = lcOutputMessage.getOutputNum(getPortModel());
            byte outputStatus = lcOutputMessage.getOutputStatus();

            Port<?> port = null;
            switch (outputType) {
                case LIGHTPORT:
                    LightPort lightPort = lightPorts.get(Integer.valueOf(outputNumber));
                    lightPort.setStatus(LightPortStatus.valueOf(LightPortEnum.valueOf(outputStatus)));
                    port = lightPort;
                    break;
                case SWITCHPORT:
                    SwitchPort switchPort = switchPorts.get(Integer.valueOf(outputNumber));
                    switchPort.setStatus(SwitchPortStatus.valueOf(SwitchPortEnum.valueOf(outputStatus)));
                    port = switchPort;
                    break;
                case SERVOPORT:
                    ServoPort servoPort = servoPorts.get(Integer.valueOf(outputNumber));
                    servoPort.setValue(outputStatus);
                    port = servoPort;
                    break;
                default:
                    LOGGER.warn("LcOutput request for unsupported port type detected: {}", outputType);
                    break;
            }

            BidibPort bidibPort = prepareBidibPort(getPortModel(), outputType, outputNumber);

            if (port != null) {
                LcStatResponse lcStatResponse =
                    new LcStatResponse(bidibMessage.getAddr(), getNextSendNum(), bidibPort,
                        lcOutputMessage.getOutputStatus());
                response = lcStatResponse.getContent();
            }
            else {
                LcNotAvailableResponse lcNotAvailableResponse =
                    new LcNotAvailableResponse(bidibMessage.getAddr(), getNextSendNum(), bidibPort);
                response = lcNotAvailableResponse.getContent();
            }

            if (port != null) {
                switch (outputType) {
                    case LIGHTPORT:
                        publishLightPortChange(port);
                        break;
                    case SWITCHPORT:
                        publishSwitchPortChange(port);
                        break;
                    case SERVOPORT:
                        publishServoPortChange(port);
                        break;
                    default:
                        break;
                }
            }
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create LcStat response failed.", ex);
        }
        return response;
    }

    private void publishLightPortChange(Port<?> port) {
        LightPort lightPort = (LightPort) port;
        LightPortStatus status = lightPort.getStatus();

        LOGGER.info("The lightport status has changed, notify the listeners, nodeAddress: {}", nodeAddress);
        EventBus.publish(new LightPortStatusEvent(ByteUtils.bytesToHex(nodeAddress), lightPort.getId(), status));
    }

    private void publishSwitchPortChange(Port<?> port) {
        SwitchPort switchPort = (SwitchPort) port;
        SwitchPortStatus status = switchPort.getStatus();

        LOGGER.info("The switchport status has changed, notify the listeners, nodeAddress: {}", nodeAddress);
        EventBus.publish(new SwitchPortStatusEvent(ByteUtils.bytesToHex(nodeAddress), switchPort, status));
    }

    private void publishServoPortChange(Port<?> port) {
        ServoPort servoPort = (ServoPort) port;
        int value = servoPort.getValue();

        LOGGER.info("The servoport status has changed, notify the listeners, nodeAddress: {}", nodeAddress);
        EventBus.publish(new ServoPortStatusEvent(ByteUtils.bytesToHex(nodeAddress), servoPort, value));
    }

    protected byte[] processLcConfigGetRequest(BidibCommand bidibMessage) {
        LOGGER.info("Process the LcConfigGet request: {}", bidibMessage);
        byte[] response = null;

        try {
            LcConfigGetMessage lcConfigGetMessage = (LcConfigGetMessage) bidibMessage;
            LcOutputType outputType = lcConfigGetMessage.getPortType(getPortModel());
            int outputNumber = lcConfigGetMessage.getPortNumber(getPortModel());

            Port<?> port = null;
            switch (outputType) {
                case LIGHTPORT:
                    port = lightPorts.get(Integer.valueOf(outputNumber));
                    break;
                case SWITCHPORT:
                    port = switchPorts.get(Integer.valueOf(outputNumber));
                    break;
                case SERVOPORT:
                    // if (outputNumber != 1) { // testing
                    port = servoPorts.get(Integer.valueOf(outputNumber));
                    // }
                    break;
                default:
                    LOGGER.warn("LcConfigGet request for unsupported port type detected: {}", outputType);
                    break;
            }

            BidibPort bidibPort = prepareBidibPort(getPortModel(), outputType, outputNumber);
            if (port != null) {
                LcConfigResponse lcConfigResponse =
                    new LcConfigResponse(bidibMessage.getAddr(), getNextSendNum(), bidibPort, port.getPortConfig());
                response = lcConfigResponse.getContent();
            }
            else {
                LOGGER.warn("No port assigned!");
                LcNotAvailableResponse lcNotAvailableResponse =
                    new LcNotAvailableResponse(bidibMessage.getAddr(), getNextSendNum(), bidibPort);
                response = lcNotAvailableResponse.getContent();
            }
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create LcConfig response failed.", ex);
        }
        return response;
    }

    protected byte[] processLcConfigSetRequest(BidibCommand bidibMessage) {

        LOGGER.info("Process the LcConfigSet request: {}", bidibMessage);
        byte[] response = null;
        try {
            LcConfigSetMessage lcConfigSetMessage = (LcConfigSetMessage) bidibMessage;
            LcOutputType outputType = lcConfigSetMessage.getPortType(getPortModel());
            int outputNumber = lcConfigSetMessage.getPortNumber(getPortModel());

            Port<?> port = null;
            switch (outputType) {
                case LIGHTPORT:
                    LightPort lightPort = lightPorts.get(Integer.valueOf(outputNumber));
                    if (lightPort != null) {
                        lightPort.setPortConfig(lcConfigSetMessage.getPortConfig());
                        port = lightPort;
                    }
                    else {
                        LOGGER.warn("Lightport not available, outputNumber: {}", outputNumber);
                    }
                    break;
                case SWITCHPORT:
                    SwitchPort switchPort = switchPorts.get(Integer.valueOf(outputNumber));
                    if (switchPort != null) {
                        switchPort.setPortConfig(lcConfigSetMessage.getPortConfig());
                        port = switchPort;
                    }
                    else {
                        LOGGER.warn("Switchport not available, outputNumber: {}", outputNumber);
                    }
                    break;
                case SERVOPORT:
                    ServoPort servoPort = servoPorts.get(Integer.valueOf(outputNumber));
                    if (servoPort != null /* && outputNumber != 1 */) {
                        servoPort.setPortConfig(lcConfigSetMessage.getPortConfig());
                        port = servoPort;
                    }
                    else {
                        LOGGER.warn("Servoport not available, outputNumber: {}", outputNumber);
                    }
                    break;
                default:
                    LOGGER.warn("LcConfigSet request for unsupported port type detected: {}", outputType);
                    break;
            }

            BidibPort bidibPort = prepareBidibPort(getPortModel(), outputType, outputNumber);
            if (port != null) {
                LcConfigResponse lcConfigResponse =
                    new LcConfigResponse(bidibMessage.getAddr(), getNextSendNum(), bidibPort, port.getPortConfig());
                response = lcConfigResponse.getContent();
            }
            else {
                LOGGER.warn("No port assigned!");
                LcNotAvailableResponse lcNotAvailableResponse =
                    new LcNotAvailableResponse(bidibMessage.getAddr(), getNextSendNum(), bidibPort);
                response = lcNotAvailableResponse.getContent();
            }
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create LcStat response failed.", ex);
        }
        return response;
    }

    protected byte[] processLcConfigXSetRequest(BidibCommand bidibMessage) {
        LOGGER.info("Process the LcConfigXSet request: {}", bidibMessage);
        byte[] response = null;
        try {
            LcConfigXSetMessage lcConfigXSetMessage = (LcConfigXSetMessage) bidibMessage;
            LcOutputType outputType = lcConfigXSetMessage.getPortType(getPortModel());
            int outputNumber = lcConfigXSetMessage.getPortNumber(getPortModel());

            Port<?> port = null;
            switch (outputType) {
                case LIGHTPORT:
                    LightPort lightPort = lightPorts.get(Integer.valueOf(outputNumber));
                    if (lightPort != null) {
                        lightPort.setPortConfigX(lcConfigXSetMessage.getLcConfigX().getPortConfig());
                        port = lightPort;
                    }
                    else {
                        LOGGER.warn("Lightport not available, outputNumber: {}", outputNumber);
                    }
                    break;
                case SWITCHPORT:
                    SwitchPort switchPort = switchPorts.get(Integer.valueOf(outputNumber));
                    if (switchPort != null) {
                        switchPort.setPortConfigX(lcConfigXSetMessage.getLcConfigX().getPortConfig());
                        port = switchPort;
                    }
                    else {
                        LOGGER.warn("Switchport not available, outputNumber: {}", outputNumber);
                    }
                    break;
                case SERVOPORT:
                    ServoPort servoPort = servoPorts.get(Integer.valueOf(outputNumber));
                    if (servoPort != null) {
                        servoPort.setPortConfigX(lcConfigXSetMessage.getLcConfigX().getPortConfig());
                        port = servoPort;
                    }
                    else {
                        LOGGER.warn("Servoport not available, outputNumber: {}", outputNumber);
                    }
                    break;
                default:
                    LOGGER.warn("LcConfigSet request for unsupported port type detected: {}", outputType);
                    break;
            }

            BidibPort bidibPort = prepareBidibPort(getPortModel(), outputType, outputNumber);

            if (port != null) {
                LcConfigX lcConfigX = new LcConfigX(bidibPort, lcConfigXSetMessage.getLcConfigX().getPortConfig());

                LcConfigXResponse lcConfigXResponse =
                    new LcConfigXResponse(bidibMessage.getAddr(), getNextSendNum(),
                        MessageUtils.getCodedPortConfig(lcConfigX, getPortModel()));

                response = lcConfigXResponse.getContent();
            }
            else {
                LOGGER.warn("No port assigned!");
                LcNotAvailableResponse magicResponse =
                    new LcNotAvailableResponse(bidibMessage.getAddr(), getNextSendNum(), bidibPort);
                response = magicResponse.getContent();
            }
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create LcConfigX response failed.", ex);
        }
        return response;
    }

    protected byte[] processLcConfigXGetRequest(BidibCommand bidibMessage) {
        LOGGER.info("Process the LcConfigXGet request: {}", bidibMessage);
        byte[] response = null;

        try {
            LcConfigXGetMessage lcConfigXGetMessage = (LcConfigXGetMessage) bidibMessage;
            int outputNumber = lcConfigXGetMessage.getPortNumber(getPortModel());

            LcOutputType lcOutputType = lcConfigXGetMessage.getPortType(getPortModel());

            Port<?> port = null;
            // byte[] portConfig = null;
            Map<Byte, PortConfigValue<?>> values = new LinkedHashMap<>();
            switch (lcOutputType) {
                case LIGHTPORT:
                    port = lightPorts.get(Integer.valueOf(outputNumber));
                    // portConfig = port.getPortConfig();
                    LightPort lightPort = (LightPort) port;
                    values.put(BidibLibrary.BIDIB_PCFG_LEVEL_PORT_ON,
                        new BytePortConfigValue(ByteUtils.getLowByte(lightPort.getPwmMax())));
                    values.put(BidibLibrary.BIDIB_PCFG_LEVEL_PORT_OFF,
                        new BytePortConfigValue(ByteUtils.getLowByte(lightPort.getPwmMin())));
                    values.put(BidibLibrary.BIDIB_PCFG_DIMM_UP,
                        new BytePortConfigValue(ByteUtils.getLowByte(lightPort.getDimMax())));
                    values.put(BidibLibrary.BIDIB_PCFG_DIMM_DOWN,
                        new BytePortConfigValue(ByteUtils.getLowByte(lightPort.getDimMin())));
                    // RGB
                    if (lightPort.getRgbValue() != null) {
                        values.put(BidibLibrary.BIDIB_PCFG_RGB, new RgbPortConfigValue(lightPort.getRgbValue()));
                    }
                    // transition time
                    if (lightPort.getTransitionTime() != null) {
                        values.put(BidibLibrary.BIDIB_PCFG_TRANSITION_TIME,
                            new Int16PortConfigValue(lightPort.getTransitionTime()));
                    }

                    // TODO remove this test
                    if (outputNumber == 2) {
                        values.put(BidibLibrary.BIDIB_PCFG_NONE, new BytePortConfigValue((byte) 12));
                    }
                    break;

                case SWITCHPORT:
                    port = switchPorts.get(Integer.valueOf(outputNumber));
                    SwitchPort switchPort = (SwitchPort) port;
                    values.put(BidibLibrary.BIDIB_PCFG_SWITCH_CTRL,
                        new BytePortConfigValue(ByteUtils.getLowByte(switchPort.getOutputBehaviour().getType())));
                    values.put(BidibLibrary.BIDIB_PCFG_TICKS,
                        new BytePortConfigValue(ByteUtils.getLowByte(switchPort.getSwitchOffTime())));
                    break;

                case SERVOPORT:
                    port = servoPorts.get(Integer.valueOf(outputNumber));
                    ServoPort servoPort = (ServoPort) port;
                    values.put(BidibLibrary.BIDIB_PCFG_SERVO_SPEED,
                        new BytePortConfigValue(ByteUtils.getLowByte(servoPort.getSpeed())));
                    values.put(BidibLibrary.BIDIB_PCFG_SERVO_ADJ_L,
                        new BytePortConfigValue(ByteUtils.getLowByte(servoPort.getTrimDown())));
                    values.put(BidibLibrary.BIDIB_PCFG_SERVO_ADJ_H,
                        new BytePortConfigValue(ByteUtils.getLowByte(servoPort.getTrimUp())));

                    break;

                default:
                    LOGGER.warn("LcConfigGet request for unsupported port type detected: {}", lcOutputType);
                    break;
            }

            LOGGER.info("Return config of port: {}", port);
            BidibPort bidibPort = prepareBidibPort(getPortModel(), lcOutputType, outputNumber);
            LcConfigX lcConfigX = new LcConfigX(bidibPort, values);

            LcConfigXResponse lcConfigXResponse =
                new LcConfigXResponse(bidibMessage.getAddr(), getNextSendNum(),
                    MessageUtils.getCodedPortConfig(lcConfigX, getPortModel()));
            response = lcConfigXResponse.getContent();
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create LcConfigX response failed.", ex);
        }
        return response;
    }

    protected void processLcConfigXGetAllRequest(BidibCommand bidibMessage) {
        LOGGER.info("Process the LcConfigXGetAll request: {}", bidibMessage);
        byte[] response = null;

        try {
            LcConfigXGetAllMessage lcConfigXGetAllMessage = (LcConfigXGetAllMessage) bidibMessage;
            LcOutputType outputType = lcConfigXGetAllMessage.getPortTypeFrom(getPortModel());

            // TODO evaluate port type/range to

            Map<Byte, PortConfigValue<?>> values = new LinkedHashMap<>();

            if (outputType != null) {
                LOGGER.info("Get all ports for output type: {}", outputType);

                // TODO check the range

                switch (outputType) {
                    case SERVOPORT:
                        for (ServoPort servoPort : servoPorts.values()) {
                            values.clear();

                            values.put(BidibLibrary.BIDIB_PCFG_SERVO_SPEED,
                                new BytePortConfigValue(ByteUtils.getLowByte(servoPort.getSpeed())));
                            values.put(BidibLibrary.BIDIB_PCFG_SERVO_ADJ_L,
                                new BytePortConfigValue(ByteUtils.getLowByte(servoPort.getTrimDown())));
                            values.put(BidibLibrary.BIDIB_PCFG_SERVO_ADJ_H,
                                new BytePortConfigValue(ByteUtils.getLowByte(servoPort.getTrimUp())));

                            LOGGER.info("Return config of servo port: {}", servoPort);
                            BidibPort bidibPort = prepareBidibPort(getPortModel(), outputType, servoPort.getId());
                            LcConfigX lcConfigX = new LcConfigX(bidibPort, values);

                            LcConfigXResponse lcConfigXResponse =
                                new LcConfigXResponse(bidibMessage.getAddr(), getNextSendNum(),
                                    MessageUtils.getCodedPortConfig(lcConfigX, getPortModel()));
                            response = lcConfigXResponse.getContent();

                            LOGGER.info("Prepared lcConfigXResponse: {}", ByteUtils.bytesToHex(response));
                            sendSpontanousResponse(response);
                            response = null;
                        }
                        break;
                    case SWITCHPORT:
                        for (SwitchPort switchPort : switchPorts.values()) {
                            values.clear();

                            values.put(BidibLibrary.BIDIB_PCFG_SWITCH_CTRL, new BytePortConfigValue(
                                ByteUtils.getLowByte(switchPort.getOutputBehaviour().getType())));
                            values.put(BidibLibrary.BIDIB_PCFG_TICKS,
                                new BytePortConfigValue(ByteUtils.getLowByte(switchPort.getSwitchOffTime())));

                            LOGGER.info("Return config of switch port: {}", switchPort);
                            BidibPort bidibPort = prepareBidibPort(getPortModel(), outputType, switchPort.getId());
                            LcConfigX lcConfigX = new LcConfigX(bidibPort, values);

                            LcConfigXResponse lcConfigXResponse =
                                new LcConfigXResponse(bidibMessage.getAddr(), getNextSendNum(),
                                    MessageUtils.getCodedPortConfig(lcConfigX, getPortModel()));
                            response = lcConfigXResponse.getContent();

                            LOGGER.info("Prepared lcConfigXResponse: {}", ByteUtils.bytesToHex(response));
                            sendSpontanousResponse(response);
                            response = null;
                        }
                        break;
                    case LIGHTPORT:
                        for (LightPort lightPort : lightPorts.values()) {
                            values.clear();

                            values.put(BidibLibrary.BIDIB_PCFG_LEVEL_PORT_ON,
                                new BytePortConfigValue(ByteUtils.getLowByte(lightPort.getPwmMax())));

                            // TODO remove this test
                            if (lightPort.getId() != 3) {
                                values.put(BidibLibrary.BIDIB_PCFG_LEVEL_PORT_OFF,
                                    new BytePortConfigValue(ByteUtils.getLowByte(lightPort.getPwmMin())));
                            }
                            else {
                                LOGGER.warn("No BIDIB_PCFG_LEVEL_PORT_OFF for light port 3!");
                            }

                            values.put(BidibLibrary.BIDIB_PCFG_DIMM_UP,
                                new BytePortConfigValue(ByteUtils.getLowByte(lightPort.getDimMax())));

                            // TODO remove this test
                            if (lightPort.getId() != 3) {
                                values.put(BidibLibrary.BIDIB_PCFG_DIMM_DOWN,
                                    new BytePortConfigValue(ByteUtils.getLowByte(lightPort.getDimMin())));
                            }
                            else {
                                LOGGER.warn("No BIDIB_PCFG_DIMM_DOWN for light port 3!");
                            }

                            // return RGB if available
                            if (lightPort.getRgbValue() != null) {
                                values.put(BidibLibrary.BIDIB_PCFG_RGB,
                                    new RgbPortConfigValue(lightPort.getRgbValue()));
                            }

                            // return transition time if available
                            if (lightPort.getTransitionTime() != null) {
                                values.put(BidibLibrary.BIDIB_PCFG_TRANSITION_TIME,
                                    new Int16PortConfigValue(lightPort.getTransitionTime()));
                            }

                            LOGGER.info("Return config of light port: {}", lightPort);
                            BidibPort bidibPort = prepareBidibPort(getPortModel(), outputType, lightPort.getId());
                            LcConfigX lcConfigX = new LcConfigX(bidibPort, values);

                            LcConfigXResponse lcConfigXResponse =
                                new LcConfigXResponse(bidibMessage.getAddr(), getNextSendNum(),
                                    MessageUtils.getCodedPortConfig(lcConfigX, getPortModel()));
                            response = lcConfigXResponse.getContent();

                            LOGGER.info("Prepared lcConfigXResponse: {}", ByteUtils.bytesToHex(response));
                            sendSpontanousResponse(response);
                            response = null;
                        }
                        break;
                    case INPUTPORT:
                        for (InputPort inputPort : inputPorts.values()) {
                            values.clear();

                            LOGGER.info("Return config of input port: {}", inputPort);
                            BidibPort bidibPort = prepareBidibPort(getPortModel(), outputType, inputPort.getId());
                            LcConfigX lcConfigX = new LcConfigX(bidibPort, values);

                            LcConfigXResponse lcConfigXResponse =
                                new LcConfigXResponse(bidibMessage.getAddr(), getNextSendNum(),
                                    MessageUtils.getCodedPortConfig(lcConfigX, getPortModel()));
                            response = lcConfigXResponse.getContent();

                            LOGGER.info("Prepared lcConfigXResponse: {}", ByteUtils.bytesToHex(response));
                            sendSpontanousResponse(response);
                            response = null;
                        }
                        break;
                    default:
                        LOGGER.warn("Unsupported port type requested: {}", outputType);
                        break;
                }
            }
            else {
                // deliver servo ports
                for (ServoPort servoPort : servoPorts.values()) {
                    values.clear();

                    LOGGER.info("Return config of servo port: {}", servoPort);
                    BidibPort bidibPort = prepareBidibPort(getPortModel(), LcOutputType.SERVOPORT, servoPort.getId());
                    LcConfigX lcConfigX = new LcConfigX(bidibPort, values);

                    LcConfigXResponse lcConfigXResponse =
                        new LcConfigXResponse(bidibMessage.getAddr(), getNextSendNum(),
                            MessageUtils.getCodedPortConfig(lcConfigX, getPortModel()));
                    response = lcConfigXResponse.getContent();

                    LOGGER.info("Prepared lcConfigXResponse: {}", ByteUtils.bytesToHex(response));
                    sendSpontanousResponse(response);
                    response = null;
                }

                // deliver switch ports
                for (SwitchPort switchPort : switchPorts.values()) {
                    values.clear();

                    LOGGER.info("Return config of switch port: {}", switchPort);

                    values = switchPort.getPortConfigX();
                    BidibPort bidibPort = prepareBidibPort(getPortModel(), LcOutputType.SWITCHPORT, switchPort.getId());
                    LcConfigX lcConfigX = new LcConfigX(bidibPort, values);

                    LcConfigXResponse lcConfigXResponse =
                        new LcConfigXResponse(bidibMessage.getAddr(), getNextSendNum(),
                            MessageUtils.getCodedPortConfig(lcConfigX, getPortModel()));
                    response = lcConfigXResponse.getContent();

                    LOGGER.info("Prepared lcConfigXResponse: {}", ByteUtils.bytesToHex(response));
                    sendSpontanousResponse(response);
                    response = null;
                }

                // deliver input ports
                for (InputPort inputPort : inputPorts.values()) {
                    values.clear();

                    LOGGER.info("Return config of input port: {}", inputPort);
                    BidibPort bidibPort = prepareBidibPort(getPortModel(), LcOutputType.INPUTPORT, inputPort.getId());
                    LcConfigX lcConfigX = new LcConfigX(bidibPort, values);

                    LcConfigXResponse lcConfigXResponse =
                        new LcConfigXResponse(bidibMessage.getAddr(), getNextSendNum(),
                            MessageUtils.getCodedPortConfig(lcConfigX, getPortModel()));
                    response = lcConfigXResponse.getContent();

                    LOGGER.info("Prepared lcConfigXResponse: {}", ByteUtils.bytesToHex(response));
                    sendSpontanousResponse(response);
                    response = null;
                }

                // deliver light ports
                for (LightPort lightPort : lightPorts.values()) {
                    values.clear();

                    LOGGER.info("Return config of light port: {}", lightPort);
                    BidibPort bidibPort = prepareBidibPort(getPortModel(), LcOutputType.LIGHTPORT, lightPort.getId());
                    LcConfigX lcConfigX = new LcConfigX(bidibPort, values);

                    LcConfigXResponse lcConfigXResponse =
                        new LcConfigXResponse(bidibMessage.getAddr(), getNextSendNum(),
                            MessageUtils.getCodedPortConfig(lcConfigX, getPortModel()));
                    response = lcConfigXResponse.getContent();

                    LOGGER.info("Prepared lcConfigXResponse: {}", ByteUtils.bytesToHex(response));
                    sendSpontanousResponse(response);
                    response = null;
                }
            }
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create lcConfigXResponse response failed.", ex);
        }

    }

    protected byte[] processLcKeyQueryRequest(BidibCommand bidibMessage) {
        LOGGER.info("Process the LcKeyQuery request: {}", bidibMessage);
        byte[] response = null;

        byte keyState = 0;
        Port<?> port = null;
        try {
            LcKeyMessage lcKeyMessage = (LcKeyMessage) bidibMessage;
            int keyNumber = lcKeyMessage.getKeyNumber();
            port = inputPorts.get(keyNumber);
            keyState = port.getStatus().getType().getType();
            LcKeyResponse lcKeyResponse =
                new LcKeyResponse(bidibMessage.getAddr(), getNextSendNum(),
                    ByteUtils.getLowByte(lcKeyMessage.getKeyNumber()), keyState);
            response = lcKeyResponse.getContent();
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create LcKey response failed.", ex);
        }

        if (port != null) {
            publishInputPortChange(port);
        }

        return response;
    }

    private void publishInputPortChange(Port<?> port) {
        InputPort inputPort = (InputPort) port;
        InputPortStatus status = inputPort.getStatus();

        LOGGER.info("The inputport status has changed, notify the listeners, nodeAddress: {}", nodeAddress);
        EventBus.publish(new InputPortStatusEvent(ByteUtils.bytesToHex(nodeAddress), inputPort.getId(), status));
    }

    protected byte[] processLcOutputQueryRequest(BidibCommand bidibMessage) {
        LOGGER.info("Process the LcOutputQuery request: {}", bidibMessage);
        byte[] response = null;

        byte portState = 0;

        try {
            LcOutputQueryMessage lcOutputQueryMessage = (LcOutputQueryMessage) bidibMessage;
            LcOutputType outputType = lcOutputQueryMessage.getPortType(getPortModel());
            int portNumber = lcOutputQueryMessage.getPortNumber(getPortModel());

            switch (outputType) {
                case LIGHTPORT:
                    portState = lightPorts.get(portNumber).getStatus().getType().getType();
                    break;
                case SWITCHPORT:
                    portState = switchPorts.get(portNumber).getStatus().getType().getType();
                    break;
                case SERVOPORT:
                    portState = ByteUtils.getLowByte(servoPorts.get(portNumber).getValue());
                    break;
                default:
                    LOGGER.warn("LcOutputQuery for unsupported port type detected: {}", outputType);
                    break;
            }
            BidibPort bidibPort = prepareBidibPort(getPortModel(), outputType, portNumber);

            LcStatResponse lcStatResponse =
                new LcStatResponse(bidibMessage.getAddr(), getNextSendNum(), bidibPort, portState);
            response = lcStatResponse.getContent();
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create LcStat response failed.", ex);
        }
        return response;
    }

    protected byte[] processLcPortQueryAllRequest(BidibCommand bidibMessage) {
        LOGGER.info("Process the PortGetAll request: {}", bidibMessage);
        byte[] response = null;
        try {
            LcPortQueryAllMessage portQueryAllMessage = (LcPortQueryAllMessage) bidibMessage;
            int portRangeFrom = portQueryAllMessage.getPortRangeFrom(getPortModel());
            int portRangeTo = portQueryAllMessage.getPortRangeTo(getPortModel());

            LOGGER.info("Query all port states, portRangeFrom: {}, portRangeTo: {}, portModel: {}", portRangeFrom,
                portRangeTo, getPortModel());

            if (MapUtils.isNotEmpty(inputPorts)) {
                for (InputPort inputPort : inputPorts.values()) {
                    try {
                        InputPortStatus inputPortStatus = inputPort.getStatus();
                        byte portStatus = inputPortStatus.getType().getType();

                        publishPortState(bidibMessage.getAddr(), LcOutputType.INPUTPORT, inputPort.getId(), portStatus);
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Publish port state failed for port: {}", inputPort, ex);
                    }
                }
            }

            if (MapUtils.isNotEmpty(lightPorts)) {
                for (LightPort lightPort : lightPorts.values()) {
                    try {
                        LightPortStatus lightPortStatus = lightPort.getStatus();
                        byte portStatus = lightPortStatus.getType().getType();

                        publishPortState(bidibMessage.getAddr(), LcOutputType.LIGHTPORT, lightPort.getId(), portStatus);
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Publish port state failed for port: {}", lightPort, ex);
                    }
                }
            }

            if (MapUtils.isNotEmpty(switchPorts)) {
                for (SwitchPort switchPort : switchPorts.values()) {
                    try {
                        SwitchPortStatus switchPortStatus = switchPort.getStatus();
                        byte portStatus = switchPortStatus.getType().getType();

                        publishPortState(bidibMessage.getAddr(), LcOutputType.SWITCHPORT, switchPort.getId(),
                            portStatus);
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Publish port state failed for port: {}", switchPort, ex);
                    }
                }
            }

            if (MapUtils.isNotEmpty(servoPorts)) {
                for (ServoPort servoPort : servoPorts.values()) {
                    try {
                        ServoPortStatus servoPortStatus = servoPort.getStatus();
                        byte portStatus = servoPortStatus.getType().getType();

                        publishPortState(bidibMessage.getAddr(), LcOutputType.SERVOPORT, servoPort.getId(), portStatus);
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Publish port state failed for port: {}", servoPort, ex);
                    }
                }
            }

            LOGGER.info("Send the terminating LC_NA message.");
            publishLcNaResponse(bidibMessage.getAddr());

        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create LcStat response failed.", ex);
        }
        return response;
    }

    protected void publishPortState(byte[] address, LcOutputType outputType, int outputNumber, byte portStatus)
        throws ProtocolException {

        BidibPort bidibPort = prepareBidibPort(getPortModel(), outputType, outputNumber);

        LcStatResponse lcStatResponse = new LcStatResponse(address, getNextSendNum(), bidibPort, portStatus);

        LOGGER.info("Prepared LcStatResponse: {}", lcStatResponse);

        byte[] response = lcStatResponse.getContent();
        sendSpontanousResponse(response);
    }

    protected void publishLcNaResponse(byte[] address) throws ProtocolException {

        BidibPort bidibPort = new BidibPort(new byte[] { ByteUtils.getLowByte(0xFF), ByteUtils.getLowByte(0xFF) });

        LcNotAvailableResponse lcNotAvailableResponse =
            new LcNotAvailableResponse(address, getNextSendNum(), bidibPort);

        LOGGER.info("Prepared LcNotAvailableResponse: {}", lcNotAvailableResponse);

        byte[] response = lcNotAvailableResponse.getContent();
        sendSpontanousResponse(response);
    }

    protected byte[] processLcMacroParaGetRequest(BidibCommand bidibMessage) {
        LOGGER.info("Process the LcMacroParaGet request: {}", bidibMessage);
        byte[] response = null;

        try {
            LcMacroParaGetMessage lcMacroParaGetMessage = (LcMacroParaGetMessage) bidibMessage;
            int macroNumber = lcMacroParaGetMessage.getMacroNumber();
            int paramId = lcMacroParaGetMessage.getParameterIndex();
            LOGGER.info("Process macroNumber: {}, paramId: {}", macroNumber, paramId);

            // return the stored parameter value
            MacroContainer container = macros.get(macroNumber);
            if (container == null) {
                LOGGER.info("Create new MacroContainer for macro number: {}", macroNumber);
                // initialize a new macro container
                container = new MacroContainer(macroNumber);
                macros.put(macroNumber, container);
            }
            byte[] parameter = container.getMacroParameter(paramId);

            LcMacroParaResponse lcMacroParaResponse =
                new LcMacroParaResponse(bidibMessage.getAddr(), getNextSendNum(), (byte) macroNumber, (byte) paramId,
                    parameter);
            response = lcMacroParaResponse.getContent();
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create LcMacroPara response failed.", ex);
        }
        return response;
    }

    protected byte[] processLcMacroParaSetRequest(BidibCommand bidibMessage) {
        LOGGER.info("Process the LcMacroParaSet request: {}", bidibMessage);
        byte[] response = null;

        try {
            LcMacroParaSetMessage lcMacroParaSetMessage = (LcMacroParaSetMessage) bidibMessage;
            int macroNumber = lcMacroParaSetMessage.getMacroNumber();
            int paramId = lcMacroParaSetMessage.getParameterIndex();
            byte[] parameter = lcMacroParaSetMessage.getValue();

            // store the parameter value
            MacroContainer container = macros.get(macroNumber);
            if (container == null) {
                // initialize a new macro container
                container = new MacroContainer(macroNumber);
                macros.put(macroNumber, container);
            }
            container.setMacroParameter(paramId, parameter);

            LcMacroParaResponse lcMacroParaResponse =
                new LcMacroParaResponse(bidibMessage.getAddr(), getNextSendNum(), (byte) macroNumber, (byte) paramId,
                    parameter);
            response = lcMacroParaResponse.getContent();
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create LcMacroPara response failed.", ex);
        }
        return response;
    }

    protected byte[] processLcMacroHandleRequest(BidibCommand bidibMessage) {
        LOGGER.info("Process the LcMacroHandle request: {}", bidibMessage);
        byte[] response = null;

        try {
            LcMacroHandleMessage lcMacroHandleMessage = (LcMacroHandleMessage) bidibMessage;
            Integer macroNumber = lcMacroHandleMessage.getMacroNumber();
            LcMacroOperationCode lcMacroOperationCode = lcMacroHandleMessage.getMacroOperationCode();

            LOGGER.info("Handle macro request, macroNumber: {}, lcMacroOperationCode: {}", macroNumber,
                lcMacroOperationCode);

            // TODO store the macro state value
            LcMacroState macroState = null;
            switch (lcMacroOperationCode) {
                case START:
                    macroState = LcMacroState.RUNNING;
                    break;
                case DELETE:
                    macroState = LcMacroState.DELETE;
                    LOGGER.info("Remove macro with number: {}", macroNumber);
                    macros.remove(macroNumber);
                    break;
                case OFF:
                    macroState = LcMacroState.OFF;
                    break;
                case RESTORE:
                    macroState = LcMacroState.RESTORE;
                    break;
                case SAVE:
                    macroState = LcMacroState.SAVE;
                    break;
                default:
                    macroState = LcMacroState.NOTEXIST;
                    break;
            }

            LcMacroStateResponse lcMacroStateResponse =
                new LcMacroStateResponse(bidibMessage.getAddr(), getNextSendNum(), ByteUtils.getLowByte(macroNumber),
                    macroState);
            response = lcMacroStateResponse.getContent();
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create LcMacroState response failed.", ex);
        }
        return response;
    }

    protected byte[] processLcMacroGetRequest(BidibCommand bidibMessage) {
        LOGGER.info("Process the LcMacroGet request: {}", bidibMessage);
        byte[] response = null;

        try {
            LcMacroGetMessage lcMacroGetMessage = (LcMacroGetMessage) bidibMessage;
            Integer macroNumber = lcMacroGetMessage.getMacroNumber();
            int stepNumber = lcMacroGetMessage.getStep();

            // fetch the macro content and test if the macro is available
            MacroContainer container = macros.get(macroNumber);
            if (container == null) {
                // initialize a new macro container
                container = new MacroContainer(macroNumber);
                macros.put(macroNumber, container);
            }
            LcMacro macroStep = container.getMacroStep(stepNumber);

            LcMacro value = macroStep;

            LcMacroResponse lcMacroResponse =
                new LcMacroResponse(bidibMessage.getAddr(), getNextSendNum(), ByteUtils.getLowByte(macroNumber),
                    ByteUtils.getLowByte(stepNumber), value);
            response = lcMacroResponse.getContent();
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create LcMacro response failed.", ex);
        }
        return response;
    }

    protected byte[] processLcMacroSetRequest(BidibCommand bidibMessage) {
        LOGGER.info("Process the LcMacroSet request: {}", bidibMessage);
        byte[] response = null;

        try {
            LcMacroSetMessage lcMacroSetMessage = (LcMacroSetMessage) bidibMessage;
            int macroNumber = lcMacroSetMessage.getMacroNumber();
            int stepNumber = lcMacroSetMessage.getStep();

            LcMacro macroStep = MacroUtils.getMacro(lcMacroSetMessage.getData());
            LOGGER.info("Current macroNumber: {}, stepNumber: {}, macroStep: {}", macroNumber, stepNumber, macroStep);

            // fetch the macro content and test if the macro container is available
            MacroContainer container = macros.get(macroNumber);
            if (container == null) {
                container = new MacroContainer(macroNumber);
                macros.put(macroNumber, container);
            }
            container.setMacroStep(stepNumber, macroStep);
            try {
                LcMacroResponse lcMacroResponse =
                    new LcMacroResponse(bidibMessage.getAddr(), getNextSendNum(), ByteUtils.getLowByte(macroNumber),
                        ByteUtils.getLowByte(stepNumber), macroStep);
                response = lcMacroResponse.getContent();
            }
            catch (NullPointerException npe) {
                LOGGER.warn("create response failed.", npe);
            }
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create LcMacro response failed.", ex);
        }
        return response;
    }

    protected byte[] processAccessorySetRequest(BidibCommand bidibMessage) {
        LOGGER.info("Process the AccessorySet request: {}", bidibMessage);
        byte[] response = null;

        try {
            AccessorySetMessage accessorySetMessage = (AccessorySetMessage) bidibMessage;
            int accessoryNumber = accessorySetMessage.getAccessoryNumber();
            int aspect = accessorySetMessage.getAspect();

            byte[] value = new byte[] { 0, 0, 0 };

            AccessoryStateResponse accessoryStateResponse =
                new AccessoryStateResponse(bidibMessage.getAddr(), getNextSendNum(), (byte) accessoryNumber,
                    (byte) aspect, value);
            response = accessoryStateResponse.getContent();
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create AccessoryState response failed.", ex);
        }
        return response;
    }

    protected byte[] processAccessoryGetRequest(BidibCommand bidibMessage) {
        LOGGER.info("Process the AccessoryGet request: {}", bidibMessage);
        byte[] response = null;

        try {
            AccessoryGetMessage accessoryGetMessage = (AccessoryGetMessage) bidibMessage;
            int accessoryNumber = accessoryGetMessage.getAccessoryNumber();
            int aspect = 0;
            byte[] value = new byte[] { 0, 0, 0 };

            AccessoryStateResponse accessoryStateResponse =
                new AccessoryStateResponse(bidibMessage.getAddr(), getNextSendNum(), (byte) accessoryNumber,
                    (byte) aspect, value);
            response = accessoryStateResponse.getContent();
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create AccessoryState response failed.", ex);
        }
        return response;
    }

    protected byte[] processAccessoryParaSetRequest(BidibCommand bidibMessage) {
        LOGGER.info("Process the AccessoryParaSet request: {}", bidibMessage);
        byte[] response = null;

        try {
            AccessoryParaSetMessage accessoryParaSetMessage = (AccessoryParaSetMessage) bidibMessage;
            int accessoryNumber = accessoryParaSetMessage.getAccessoryNumber();
            int paraNumber = accessoryParaSetMessage.getParaNumber();

            byte[] value = accessoryParaSetMessage.getValue();

            AccessoryParaResponse accessoryParaResponse =
                new AccessoryParaResponse(bidibMessage.getAddr(), getNextSendNum(),
                    ByteUtils.getLowByte(accessoryNumber), ByteUtils.getLowByte(paraNumber), value);
            response = accessoryParaResponse.getContent();
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create AccessoryPara response failed.", ex);
        }
        return response;
    }

    protected byte[] processAccessoryParaGetRequest(BidibCommand bidibMessage) {
        LOGGER.info("Process the AccessoryParaGet request: {}", bidibMessage);
        byte[] response = null;

        try {
            AccessoryParaGetMessage accessoryParaGetMessage = (AccessoryParaGetMessage) bidibMessage;
            int accessoryNumber = accessoryParaGetMessage.getAccessoryNumber();
            int paraNumber = accessoryParaGetMessage.getParaNumber();

            // TODO provide the correct data here ...
            byte[] value = new byte[] { 0, 0, 0, 0 };

            if (paraNumber == BidibLibrary.BIDIB_ACCESSORY_SWITCH_TIME) {

                LOGGER.info("The param is not known currently!");

                value = new byte[] { ByteUtils.getLowByte(paraNumber) };
                paraNumber = BidibLibrary.BIDIB_ACCESSORY_PARA_NOTEXIST;
            }

            AccessoryParaResponse accessoryParaResponse =
                new AccessoryParaResponse(bidibMessage.getAddr(), getNextSendNum(),
                    ByteUtils.getLowByte(accessoryNumber), ByteUtils.getLowByte(paraNumber), value);
            response = accessoryParaResponse.getContent();
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create AccessoryPara response failed.", ex);
        }
        return response;
    }

    @Override
    public void queryStatus(Class<?> portClass) {
        if (InputPort.class.equals(portClass)) {
            for (InputPort inputPort : inputPorts.values()) {
                publishInputPortChange(inputPort);
            }
        }
        else if (LightPort.class.equals(portClass)) {
            for (LightPort lightPort : lightPorts.values()) {
                publishLightPortChange(lightPort);
            }
        }
        else if (SwitchPort.class.equals(portClass)) {
            for (SwitchPort switchPort : switchPorts.values()) {
                publishSwitchPortChange(switchPort);
            }
        }
        else if (ServoPort.class.equals(portClass)) {
            for (ServoPort servoPort : servoPorts.values()) {
                publishServoPortChange(servoPort);
            }
        }
    }

    @Override
    public void setPortsConfig(PortType portType) {
        if (portType == null) {
            return;
        }

        LOGGER.info("Set the ports config: {}", portType);

        if (portType instanceof LightPortType) {

            LightPortType lightPortType = (LightPortType) portType;
            lightPortCount = lightPortType.getCount();
            LOGGER.info("Total number of lightports: {}", lightPortCount);

            lightPorts.clear();

            // prepare initial lightports
            for (int portId = 0; portId < lightPortCount; portId++) {

                LightPort lightPort = new LightPort();
                lightPort.setId(portId);

                // add the unconfigured lightport
                lightPorts.put(portId, lightPort);
            }

            prepareLightPortCvValuesFromConfig(lightPortType);

        }
        else if (portType instanceof SwitchPortType) {
            switchPortCount = portType.getCount();

            LOGGER.info("Configured number of switch ports: {}", switchPortCount);
        }
        else if (portType instanceof InputPortType) {
            inputPortCount = portType.getCount();
            inputPortOffset = (portType.getOffset() != null ? portType.getOffset() : 0);

            LOGGER.info("Configured number of input ports: {}", inputPortCount);
        }
        else if (portType instanceof ServoPortType) {
            servoPortCount = portType.getCount();
        }
    }

    protected void prepareLightPortCvValuesFromConfig(final LightPortType lightPortType) {

        // overwrite configured ports
        if (CollectionUtils.isNotEmpty(lightPortType.getPort())) {
            final int cvBaseOffset = 215 /* offset */;
            // evaluate the configured ports
            for (LightPortParamsType portParams : lightPortType.getPort()) {
                LightPort lightPort = new LightPort();
                lightPort.setId(portParams.getPortId());
                lightPort.setDimMin(portParams.getDimSlopeDown());
                lightPort.setDimMax(portParams.getDimSlopeUp());
                lightPort.setPwmMax(portParams.getIntensityOn());
                lightPort.setPwmMin(portParams.getIntensityOff());
                if (portParams.getRgbValue() != null) {
                    try {
                        Integer rgbValue = Integer.parseInt(portParams.getRgbValue(), 16);
                        lightPort.setRgbValue(rgbValue);
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Parse RGB value failed: {}", portParams.getRgbValue(), ex);
                        lightPort.setRgbValue(null);
                    }
                }
                if (portParams.getTransitionTime() != null) {
                    lightPort.setTransitionTime(portParams.getTransitionTime());
                }

                lightPort.setStatus(LightPortStatus.OFF);

                LOGGER.info("Add configured port: {}", lightPort);

                // add the configured port
                lightPorts.put(lightPort.getId(), lightPort);

                // prepare the CVs
                int cvOffset = cvBaseOffset + portParams.getPortId() * 7 /* next */;

                configurationVariables.put(String.valueOf(cvOffset), String.valueOf(-1));

                configurationVariables.put(String.valueOf(cvOffset + 1), String.valueOf(lightPort.getPwmMin()));
                configurationVariables.put(String.valueOf(cvOffset + 2), String.valueOf(lightPort.getPwmMax()));
                configurationVariables.put(String.valueOf(cvOffset + 3),
                    String.valueOf(ByteUtils.getLowByte(lightPort.getDimMin())));
                configurationVariables.put(String.valueOf(cvOffset + 4),
                    String.valueOf(ByteUtils.getHighByte(lightPort.getDimMin())));
                configurationVariables.put(String.valueOf(cvOffset + 5),
                    String.valueOf(ByteUtils.getLowByte(lightPort.getDimMax())));
                configurationVariables.put(String.valueOf(cvOffset + 6),
                    String.valueOf(ByteUtils.getHighByte(lightPort.getDimMax())));
            }

            // use the number of configured ports
            lightPortCount = lightPorts.size();
        }
    }

    public int getInputPortOffset() {
        return inputPortOffset;
    }
}
