package org.bidib.wizard.simulation.events;

import org.bidib.wizard.comm.SwitchPortStatus;
import org.bidib.wizard.mvc.main.model.SwitchPort;

public class SwitchPortStatusEvent {

    private final String nodeAddr;

    private final SwitchPort port;

    private final SwitchPortStatus status;

    public SwitchPortStatusEvent(String nodeAddr, SwitchPort port, SwitchPortStatus status) {
        this.nodeAddr = nodeAddr;
        this.port = port;
        this.status = status;
    }

    public String getNodeAddr() {
        return nodeAddr;
    }

    public SwitchPort getPort() {
        return port;
    }

    public SwitchPortStatus getStatus() {
        return status;
    }
}
