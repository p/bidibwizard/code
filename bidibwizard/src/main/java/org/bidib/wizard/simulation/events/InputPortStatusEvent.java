package org.bidib.wizard.simulation.events;

import org.bidib.wizard.comm.InputPortStatus;

public class InputPortStatusEvent {

    private final String nodeAddr;

    private final int portNumber;

    private final InputPortStatus status;

    public InputPortStatusEvent(String nodeAddr, int portNumber, InputPortStatus status) {
        this.nodeAddr = nodeAddr;
        this.portNumber = portNumber;
        this.status = status;
    }

    public String getNodeAddr() {
        return nodeAddr;
    }

    public int getPortNumber() {
        return portNumber;
    }

    public InputPortStatus getStatus() {
        return status;
    }
}
