package org.bidib.wizard.simulation.events;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.bidib.wizard.comm.FeedbackPortStatus;
import org.bidib.wizard.mvc.main.model.FeedbackPort;

public class FeedbackPortStatusEvent {

    private final String nodeAddr;

    private final FeedbackPort port;

    private final FeedbackPortStatus status;

    public FeedbackPortStatusEvent(String nodeAddr, FeedbackPort port, FeedbackPortStatus status) {
        this.nodeAddr = nodeAddr;
        this.port = port;
        this.status = status;
    }

    public String getNodeAddr() {
        return nodeAddr;
    }

    public FeedbackPort getPort() {
        return port;
    }

    public FeedbackPortStatus getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
