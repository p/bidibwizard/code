package org.bidib.wizard.simulation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.swing.Timer;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.BidibPort;
import org.bidib.jbidibc.core.Feature;
import org.bidib.jbidibc.core.LcConfigX;
import org.bidib.jbidibc.core.enumeration.InputPortEnum;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.LightPortEnum;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.message.AccessoryParaGetMessage;
import org.bidib.jbidibc.core.message.AccessoryParaResponse;
import org.bidib.jbidibc.core.message.AccessorySetMessage;
import org.bidib.jbidibc.core.message.AccessoryStateResponse;
import org.bidib.jbidibc.core.message.BidibCommand;
import org.bidib.jbidibc.core.message.LcConfigXGetMessage;
import org.bidib.jbidibc.core.message.LcConfigXResponse;
import org.bidib.jbidibc.core.message.LcConfigXSetMessage;
import org.bidib.jbidibc.core.message.LcKeyMessage;
import org.bidib.jbidibc.core.message.LcKeyResponse;
import org.bidib.jbidibc.core.message.LcNotAvailableResponse;
import org.bidib.jbidibc.core.message.LcOutputMessage;
import org.bidib.jbidibc.core.message.LcOutputQueryMessage;
import org.bidib.jbidibc.core.message.LcStatResponse;
import org.bidib.jbidibc.core.port.BytePortConfigValue;
import org.bidib.jbidibc.core.port.PortConfigValue;
import org.bidib.jbidibc.core.port.ReconfigPortConfigValue;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.core.utils.MessageUtils;
import org.bidib.jbidibc.simulation.SwitchingFunctionsNode;
import org.bidib.jbidibc.simulation.net.SimulationBidibMessageProcessor;
import org.bidib.wizard.comm.InputPortStatus;
import org.bidib.wizard.comm.LightPortStatus;
import org.bidib.wizard.mvc.main.model.GenericPort;
import org.bidib.wizard.simulation.events.InputPortStatusEvent;
import org.bidib.wizard.simulation.events.LightPortStatusEvent;
import org.bidib.wizard.simulation.ports.GenericSimulationPort;
import org.bushe.swing.event.EventBus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LedIo24Simulator extends LightControlSimulator implements SwitchingFunctionsNode {

    private static final Logger LOGGER = LoggerFactory.getLogger(LedIo24Simulator.class);

    private static final String SIMULATION_PANEL_CLASS = "org.bidib.wizard.mvc.simulation.view.panel.LightControlPanel";

    private final Map<Integer, GenericSimulationPort> genericPorts = new HashMap<Integer, GenericSimulationPort>();

    public LedIo24Simulator(byte[] nodeAddress, long uniqueId, boolean autoAddFeature,
        SimulationBidibMessageProcessor messageReceiver) {
        super(nodeAddress, uniqueId, autoAddFeature, messageReceiver);
    }

    @Override
    protected void prepareFeatures() {
        LOGGER.info("Prepare the features.");
        super.prepareFeatures();

        // features.add(new Feature(BidibLibrary.FEATURE_CTRL_INPUT_COUNT, inputPortCount));
        // features.add(new Feature(BidibLibrary.FEATURE_CTRL_LIGHT_COUNT, lightPortCount));

        features.remove(new Feature(BidibLibrary.FEATURE_ACCESSORY_MACROMAPPED, 2));
        features.add(new Feature(BidibLibrary.FEATURE_ACCESSORY_MACROMAPPED, 8));

        Feature feature = getFeature(BidibLibrary.FEATURE_CTRL_PORT_FLAT_MODEL);
        if (feature != null) {
            int numPorts = feature.getValue();

            LOGGER.info("Create generic ports, numPorts: {}", numPorts);

            for (int portNumber = 0; portNumber < numPorts; portNumber++) {
                GenericSimulationPort port = new GenericSimulationPort(portNumber);

                genericPorts.put(portNumber, port);
            }
        }

        features.remove(new Feature(BidibLibrary.FEATURE_RELEVANT_PID_BITS, 16));
        features.add(new Feature(BidibLibrary.FEATURE_RELEVANT_PID_BITS, 8));

    }

    @Override
    protected void prepareCVs() {
        // TODO Auto-generated method stub
        super.prepareCVs();

        configurationVariables.put("String 8", "TEST result");

    }

    @Override
    public void start() {
        LOGGER.info("Start the simulator for address: {}", getAddress());

        super.start();

        if (!MapUtils.isEmpty(genericPorts)) {
            int index = 0;

            for (int portNum = 0; portNum < lightPortCount; portNum++) {
                GenericSimulationPort genericPort = genericPorts.get(portNum + index);
                genericPort.setCurrentPortType(LcOutputType.LIGHTPORT, 0x8002);

                if (portNum == 0) {
                    genericPort.setInactive(true);
                }

                Map<Byte, PortConfigValue<?>> values = new LinkedHashMap<>();
                values.put(BidibLibrary.BIDIB_PCFG_RECONFIG, new ReconfigPortConfigValue(Integer.valueOf(0x800201)));

                values.put(BidibLibrary.BIDIB_PCFG_LEVEL_PORT_OFF, new BytePortConfigValue(Byte.valueOf((byte) 0x00)));
                values.put(BidibLibrary.BIDIB_PCFG_LEVEL_PORT_ON, new BytePortConfigValue(Byte.valueOf((byte) 0xFF)));
                values.put(BidibLibrary.BIDIB_PCFG_DIMM_DOWN, new BytePortConfigValue(Byte.valueOf((byte) 0x0F)));
                values.put(BidibLibrary.BIDIB_PCFG_DIMM_UP, new BytePortConfigValue(Byte.valueOf((byte) 0x3F)));

                // TODO RGB
                // values.put(BidibLibrary.BIDIB_PCFG_RGB, new RgbPortConfigValue(Integer.valueOf(3)));

                genericPort.setPortConfig(values);
            }
            // index += switchPortCount;

            // TODO this is a hack for the OneControl
            index = lightPortCount;

            LOGGER.info("Set the port type for input ports, index: {}", index);
            for (int portNum = 0; portNum < inputPortCount; portNum++) {
                GenericSimulationPort genericPort = genericPorts.get(portNum + index);
                genericPort.setCurrentPortType(LcOutputType.INPUTPORT, 0x8002);

                // input ports do support remapping
                Map<Byte, PortConfigValue<?>> values = new LinkedHashMap<>();
                values.put(BidibLibrary.BIDIB_PCFG_RECONFIG, new ReconfigPortConfigValue(Integer.valueOf(0x80020F)));

                values.put(BidibLibrary.BIDIB_PCFG_INPUT_CTRL, new BytePortConfigValue(Byte.valueOf((byte) 0x00)));
                values.put(BidibLibrary.BIDIB_PCFG_TICKS, new BytePortConfigValue(Byte.valueOf((byte) 0x00)));

                genericPort.setPortConfig(values);
            }

        }
    }

    @Override
    public String getSimulationPanelClass() {
        return SIMULATION_PANEL_CLASS;
    }

    protected byte[] processLcConfigXSetRequest(BidibCommand bidibMessage) {

        LOGGER.info("Process the LcConfigXSet request: {}", bidibMessage);
        byte[] response = null;

        if (getFlatPortModelPortCount() > 0) {
            try {
                LcConfigXSetMessage lcConfigXSetMessage = (LcConfigXSetMessage) bidibMessage;
                int outputNumber = lcConfigXSetMessage.getPortNumber(getPortModel());
                LcOutputType outputType = lcConfigXSetMessage.getPortType(getPortModel());

                GenericPort port = null;
                port = genericPorts.get(Integer.valueOf(outputNumber));
                LOGGER.info("Set LcConfig for output number: {}, port: {}", outputNumber, port);

                BidibPort bidibPort = prepareBidibPort(getPortModel(), outputType, outputNumber);

                if (port != null) {

                    // the problem here is that the BIDIB_PCFG_RECONFIG overwrites the supported port types
                    LcConfigX lcConfigX = lcConfigXSetMessage.getLcConfigX();
                    ReconfigPortConfigValue reconfig =
                        (ReconfigPortConfigValue) lcConfigX.getPortConfig().get(BidibLibrary.BIDIB_PCFG_RECONFIG);
                    if (reconfig != null) {
                        // we must keep the supported port types
                        Integer supportedPortTypes = port.getSupportedPortTypes();
                        if (supportedPortTypes != null) {
                            ReconfigPortConfigValue newReconfig =
                                new ReconfigPortConfigValue(reconfig.getCurrentOutputType().getType(),
                                    supportedPortTypes);
                            LOGGER.info("Prepared BIDIB_PCFG_RECONFIG to replace: {}", newReconfig);
                            lcConfigX.getPortConfig().put(BidibLibrary.BIDIB_PCFG_RECONFIG, newReconfig);
                        }
                    }
                    // set the current port configuration
                    port.setPortConfig(lcConfigX.getPortConfig());

                    Map<Byte, PortConfigValue<?>> updatedValues = port.getPortConfig();
                    LOGGER.info("Return config of port: {}", port);
                    lcConfigX = new LcConfigX(bidibPort, updatedValues);

                    byte[] content = MessageUtils.getCodedPortConfig(lcConfigX, getPortModel());

                    // // TODO remove this test
                    // if (outputNumber == 5) {
                    // content = new byte[] { 0x00, 0x05, 0x00, 0x0C };
                    // }

                    LcConfigXResponse lcConfigXResponse =
                        new LcConfigXResponse(bidibMessage.getAddr(), getNextSendNum(), content);
                    response = lcConfigXResponse.getContent();
                }
                else {
                    LOGGER.warn("No port assigned!");
                    LcNotAvailableResponse magicResponse =
                        new LcNotAvailableResponse(bidibMessage.getAddr(), getNextSendNum(), bidibPort);
                    response = magicResponse.getContent();
                }
            }
            catch (ProtocolException ex) {
                LOGGER.warn("Create LcConfigX response failed.", ex);
            }
        }
        else {
            response = super.processLcConfigXSetRequest(bidibMessage);
        }
        return response;
    }

    protected byte[] processLcConfigXGetRequest(BidibCommand bidibMessage) {
        LOGGER.info("Process the LcConfigXGet request: {}", bidibMessage);
        byte[] response = null;

        if (getFlatPortModelPortCount() > 0) {

            try {
                LcConfigXGetMessage lcConfigXGetMessage = (LcConfigXGetMessage) bidibMessage;
                int outputNumber = lcConfigXGetMessage.getPortNumber(getPortModel());
                LcOutputType outputType = lcConfigXGetMessage.getPortType(getPortModel());

                GenericPort port = null;

                port = genericPorts.get(Integer.valueOf(outputNumber));

                BidibPort bidibPort = prepareBidibPort(getPortModel(), outputType, outputNumber);

                if (port != null) {
                    Map<Byte, PortConfigValue<?>> values = port.getPortConfig();

                    // TODO remove this test
                    // if (outputNumber == 2) {
                    // values.put(BidibLibrary.BIDIB_PCFG_NONE, new BytePortConfigValue((byte) 12));
                    // }

                    LOGGER.info("Return config of port: {}", port);
                    LcConfigX lcConfigX = new LcConfigX(bidibPort, values);

                    LcConfigXResponse lcConfigXResponse =
                        new LcConfigXResponse(bidibMessage.getAddr(), getNextSendNum(),
                            MessageUtils.getCodedPortConfig(lcConfigX, getPortModel()));
                    response = lcConfigXResponse.getContent();
                }
                else {
                    LOGGER.warn("No port available with port number: {}", outputNumber);
                    LcNotAvailableResponse notAvailableResponse =
                        new LcNotAvailableResponse(bidibMessage.getAddr(), getNextSendNum(), bidibPort);
                    response = notAvailableResponse.getContent();
                }
            }
            catch (ProtocolException ex) {
                LOGGER.warn("Create LcConfigX response failed.", ex);
            }
        }
        else {
            response = super.processLcConfigXGetRequest(bidibMessage);
        }
        return response;
    }

    protected void processLcConfigXGetAllRequest(BidibCommand bidibMessage) {
        LOGGER.info("Process the LcConfigXGetAll request: {}", bidibMessage);

        if (getFlatPortModelPortCount() > 0) {
            LcOutputType outputType = LcOutputType.SWITCHPORT;

            Map<Byte, PortConfigValue<?>> values = new LinkedHashMap<>();

            try {
                for (GenericPort port : genericPorts.values()) {
                    int portNumber = port.getPortNumber();
                    LOGGER.info("Prepare lcConfigXResponse for port number: {}", portNumber);
                    values.clear();

                    if (portNumber >= 0 && portNumber < lightPortCount) { // 24 light ports
                        values.put(BidibLibrary.BIDIB_PCFG_RECONFIG,
                            new ReconfigPortConfigValue(Integer.valueOf(0x800201)));

                        values.put(BidibLibrary.BIDIB_PCFG_LEVEL_PORT_OFF,
                            new BytePortConfigValue(Byte.valueOf((byte) 0x00)));
                        values.put(BidibLibrary.BIDIB_PCFG_LEVEL_PORT_ON,
                            new BytePortConfigValue(Byte.valueOf((byte) 0xFF)));
                        values.put(BidibLibrary.BIDIB_PCFG_DIMM_DOWN,
                            new BytePortConfigValue(Byte.valueOf((byte) 0x0F)));
                        values.put(BidibLibrary.BIDIB_PCFG_DIMM_UP, new BytePortConfigValue(Byte.valueOf((byte) 0x3F)));

                        // TODO RGB
                        // values.put(BidibLibrary.BIDIB_PCFG_RGB, new RgbPortConfigValue(Integer.valueOf(3)));

                    }
                    else { // input ports do not support remapping

                        int currentPortType = ByteUtils.getInt(port.getCurrentPortType().getType());

                        values.put(BidibLibrary.BIDIB_PCFG_RECONFIG,
                            // be careful, the constructor is different ...
                            new ReconfigPortConfigValue(currentPortType, 0x8000));

                        values.put(BidibLibrary.BIDIB_PCFG_INPUT_CTRL,
                            new BytePortConfigValue(Byte.valueOf((byte) 0x00)));
                        values.put(BidibLibrary.BIDIB_PCFG_TICKS, new BytePortConfigValue(Byte.valueOf((byte) 0x00)));
                    }

                    // TODO remove this test
                    if (portNumber == 6) {
                        values.put(BidibLibrary.BIDIB_PCFG_NONE, new BytePortConfigValue((byte) 12));
                    }

                    if (port.isInactive()) {
                        values.put(BidibLibrary.BIDIB_PCFG_ERR_PORT_INACTIVE,
                            new BytePortConfigValue(Byte.valueOf((byte) 0x00)));
                    }

                    LOGGER.info("Return config of port: {}", port);
                    BidibPort bidibPort = prepareBidibPort(getPortModel(), outputType, portNumber);
                    LcConfigX lcConfigX = new LcConfigX(bidibPort, values);

                    LcConfigXResponse lcConfigXResponse =
                        new LcConfigXResponse(bidibMessage.getAddr(), getNextSendNum(),
                            MessageUtils.getCodedPortConfig(lcConfigX, getPortModel()));
                    byte[] response = lcConfigXResponse.getContent();

                    LOGGER.info("Prepared lcConfigXResponse: {}", ByteUtils.bytesToHex(response));
                    sendSpontanousResponse(response);
                }
            }
            catch (ProtocolException ex) {
                LOGGER.warn("Create LcConfigXResponse response failed.", ex);
            }
        }
        else {
            super.processLcConfigXGetAllRequest(bidibMessage);
        }
    }

    protected byte[] processLcOutputQueryRequest(BidibCommand bidibMessage) {

        LOGGER.info("Process the LcOutputQuery request: {}", bidibMessage);
        byte[] response = null;

        if (getFlatPortModelPortCount() > 0) {
            byte portState = 0;

            try {
                LcOutputQueryMessage lcOutputQueryMessage = (LcOutputQueryMessage) bidibMessage;
                LcOutputType outputType = lcOutputQueryMessage.getPortType(getPortModel());
                int outputNumber = lcOutputQueryMessage.getPortNumber(getPortModel());

                GenericPort port = genericPorts.get(Integer.valueOf(outputNumber));

                BidibPort bidibPort = prepareBidibPort(getPortModel(), outputType, outputNumber);

                if (port != null) {
                    portState = ByteUtils.getLowByte(0xFF);
                }
                else {
                    LOGGER.warn("No port available with portNumber: {}", outputNumber);
                }
                LcStatResponse lcStatResponse =
                    new LcStatResponse(bidibMessage.getAddr(), getNextSendNum(), bidibPort, portState);
                response = lcStatResponse.getContent();
            }
            catch (ProtocolException ex) {
                LOGGER.warn("Create LcStat response failed.", ex);
            }
        }
        else {
            response = super.processLcOutputQueryRequest(bidibMessage);
        }
        return response;
    }

    protected byte[] processLcKeyQueryRequest(BidibCommand bidibMessage) {
        LOGGER.info("Process the LcKeyQuery request: {}", bidibMessage);
        byte[] response = null;

        if (getFlatPortModelPortCount() > 0) {
            byte keyState = 0;
            GenericSimulationPort port = null;
            try {
                LcKeyMessage lcKeyMessage = (LcKeyMessage) bidibMessage;
                int portNumber = lcKeyMessage.getKeyNumber();

                port = genericPorts.get(Integer.valueOf(portNumber));

                if (port != null) {
                    keyState = port.getPortStatus();
                }
                else {
                    LOGGER.warn("No port available with portNumber: {}", portNumber);
                }

                LcKeyResponse lcKeyResponse =
                    new LcKeyResponse(bidibMessage.getAddr(), getNextSendNum(), ByteUtils.getLowByte(portNumber),
                        keyState);
                response = lcKeyResponse.getContent();
            }
            catch (ProtocolException ex) {
                LOGGER.warn("Create LcKey response failed.", ex);
            }

            if (port != null) {
                publishInputPortChange(port);
            }
        }
        else {
            response = super.processLcKeyQueryRequest(bidibMessage);
        }
        return response;
    }

    protected byte[] processLcOutputRequest(BidibCommand bidibMessage) {
        LOGGER.info("Process the LcOutput request: {}", bidibMessage);
        byte[] response = null;

        if (getFlatPortModelPortCount() > 0) {

            GenericSimulationPort port = null;
            try {
                LcOutputMessage lcOutputMessage = (LcOutputMessage) bidibMessage;
                LcOutputType outputType = lcOutputMessage.getOutputType(getPortModel());
                int portNumber = lcOutputMessage.getOutputNum(getPortModel());
                byte outputStatus = lcOutputMessage.getOutputStatus();

                port = genericPorts.get(Integer.valueOf(portNumber));

                BidibPort bidibPort = prepareBidibPort(getPortModel(), outputType, portNumber);

                if (port != null) {
                    port.setPortStatus(outputStatus);

                    LcStatResponse lcStatResponse =
                        new LcStatResponse(bidibMessage.getAddr(), getNextSendNum(), bidibPort,
                            lcOutputMessage.getOutputStatus());
                    response = lcStatResponse.getContent();
                }
                else {
                    LcNotAvailableResponse lcNotAvailableResponse =
                        new LcNotAvailableResponse(bidibMessage.getAddr(), getNextSendNum(), bidibPort);
                    response = lcNotAvailableResponse.getContent();
                }

                if (port != null) {
                    switch (port.getCurrentPortType()) {
                        case LIGHTPORT:
                            publishLightPortChange(port);
                            break;
                        default:
                            break;
                    }
                }
            }
            catch (ProtocolException ex) {
                LOGGER.warn("Create LcStat response failed.", ex);
            }
        }
        return response;
    }

    private void publishLightPortChange(GenericSimulationPort port) {

        LightPortStatus status = LightPortStatus.valueOf(LightPortEnum.valueOf(port.getPortStatus()));

        LOGGER.info("The lightport status has changed, notify the listeners, nodeAddress: {}", nodeAddress);
        EventBus.publish(new LightPortStatusEvent(ByteUtils.bytesToHex(nodeAddress), port.getPortNumber(), status));
    }

    // //////

    private Timer simulationTimer;

    private List<String> accessoryResponseList = new ArrayList<>();

    protected byte[] processAccessorySetRequest(final BidibCommand bidibMessage) {
        LOGGER.info("Process the AccessorySet request: {}", bidibMessage);
        byte[] response = null;

        try {
            AccessorySetMessage accessorySetMessage = (AccessorySetMessage) bidibMessage;
            int accessoryNumber = accessorySetMessage.getAccessoryNumber();
            int aspect = accessorySetMessage.getAspect();

            byte[] value = new byte[] { 0, 0, 0 };
            // byte[] value = new byte[] { 2, 1, 20 };

            if (accessoryNumber == 1 && aspect == 1) {
                LOGGER.warn("Adding simulated error to accessory state response");

                value = new byte[] { 2, 1, 20 };

                // add the remaining response values
                accessoryResponseList.add(accessoryNumber + "," + aspect + ",2,128,7");
                accessoryResponseList.add(accessoryNumber + ",255,2,129,7");
                accessoryResponseList.add(accessoryNumber + ",255,2,0,0");

                simulationTimer = new Timer(1500, new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {

                        if (!accessoryResponseList.isEmpty()) {
                            String accessoryResponse = accessoryResponseList.remove(0);
                            if (StringUtils.isNotBlank(accessoryResponse)) {
                                LOGGER.info("Send response: {}", accessoryResponse);
                                // parse
                                String[] parts = accessoryResponse.split(",");
                                byte[] value = new byte[parts.length];
                                int index = 0;
                                for (String part : parts) {
                                    int val = Integer.parseInt(part);
                                    value[index] = ByteUtils.getLowByte(val);
                                    index++;
                                }

                                try {
                                    AccessoryStateResponse accessoryStateResponse =
                                        new AccessoryStateResponse(bidibMessage.getAddr(), getNextSendNum(),
                                            /* accessoryNumber */value[0], /* aspect */value[1],
                                            ByteUtils.subArray(value, 2));
                                    byte[] response = accessoryStateResponse.getContent();
                                    LOGGER.info("Prepared accessoryStateResponse: {}", accessoryStateResponse);
                                    sendSpontanousResponse(response);
                                }
                                catch (ProtocolException ex) {
                                    LOGGER.warn("Create AccessoryState response failed.", ex);
                                }
                            }
                        }

                        if (accessoryResponseList.isEmpty()) {
                            LOGGER.info("No more entries in accessoryResponseList. Stop and release timer.");
                            simulationTimer.stop();
                            simulationTimer = null;
                        }
                    }
                });
                simulationTimer.start();
            }

            // send accessory state response
            AccessoryStateResponse accessoryStateResponse =
                new AccessoryStateResponse(bidibMessage.getAddr(), getNextSendNum(),
                    ByteUtils.getLowByte(accessoryNumber), ByteUtils.getLowByte(aspect), value);
            response = accessoryStateResponse.getContent();

        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create AccessoryState response failed.", ex);
        }
        return response;
    }

    // //////
    @Override
    protected byte[] processAccessoryParaGetRequest(BidibCommand bidibMessage) {
        LOGGER.info("Process the AccessoryParaGet request: {}", bidibMessage);
        byte[] response = null;

        try {
            AccessoryParaGetMessage accessoryParaGetMessage = (AccessoryParaGetMessage) bidibMessage;
            int accessoryNumber = accessoryParaGetMessage.getAccessoryNumber();
            int paraNumber = accessoryParaGetMessage.getParaNumber();

            if (paraNumber == BidibLibrary.BIDIB_ACCESSORY_SWITCH_TIME) {

                LOGGER.info("The param BIDIB_ACCESSORY_SWITCH_TIME is known for accessory 0 and 1!");

                // TODO provide the correct data here ...
                byte[] value = null;

                switch (accessoryNumber) {
                    case 0:
                        value = new byte[] { ByteUtils.getLowByte(0x87) };
                        break;
                    case 1:
                        value = new byte[] { ByteUtils.getLowByte(0x77) };
                        break;
                    default:
                        value = new byte[] { ByteUtils.getLowByte(paraNumber) };
                        paraNumber = BidibLibrary.BIDIB_ACCESSORY_PARA_NOTEXIST;
                        break;
                }

                AccessoryParaResponse accessoryParaResponse =
                    new AccessoryParaResponse(bidibMessage.getAddr(), getNextSendNum(),
                        ByteUtils.getLowByte(accessoryNumber), ByteUtils.getLowByte(paraNumber), value);
                response = accessoryParaResponse.getContent();
            }
            else {
                response = super.processAccessoryParaGetRequest(bidibMessage);
            }

        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create AccessoryPara response failed.", ex);
        }
        return response;
    }

    //////////

    private void publishInputPortChange(GenericSimulationPort port) {

        InputPortStatus status = InputPortStatus.valueOf(InputPortEnum.valueOf(port.getPortStatus()));

        LOGGER.info("The inputport status has changed, notify the listeners, nodeAddress: {}", nodeAddress);
        EventBus.publish(new InputPortStatusEvent(ByteUtils.bytesToHex(nodeAddress), port.getPortNumber(), status));
    }

    protected void changeInputPortStatus(int portNum) {

        GenericSimulationPort port = genericPorts.get(portNum);
        if (port != null) {

            InputPortStatus portStatus = InputPortStatus.valueOf(InputPortEnum.valueOf(port.getPortStatus()));

            switch (portStatus) {
                case OFF:
                    port.setPortStatus(InputPortStatus.ON.getType().getType());
                    break;
                default:
                    port.setPortStatus(InputPortStatus.OFF.getType().getType());
                    break;
            }

            // prepare the request
            final LcKeyMessage lcKeyMessage = new LcKeyMessage(portNum) {
                @Override
                public byte[] getAddr() {
                    return getNodeAddress();
                }
            };
            processRequest(lcKeyMessage);
        }
        else {
            LOGGER.warn("The requested input port is not available: {}", portNum);
        }
    }
}
