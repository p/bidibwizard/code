package org.bidib.wizard.simulation.events;

import org.bidib.wizard.comm.LightPortStatus;

public class LightPortStatusEvent {

    private final String nodeAddr;

    private final int portNumber;

    private final LightPortStatus status;

    public LightPortStatusEvent(String nodeAddr, int portNumber, LightPortStatus status) {
        this.nodeAddr = nodeAddr;
        this.portNumber = portNumber;
        this.status = status;
    }

    public String getNodeAddr() {
        return nodeAddr;
    }

    public int getPortNumber() {
        return portNumber;
    }

    public LightPortStatus getStatus() {
        return status;
    }
}
