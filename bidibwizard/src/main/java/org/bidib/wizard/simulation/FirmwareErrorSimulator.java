package org.bidib.wizard.simulation;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.enumeration.FirmwareUpdateOperation;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.message.BidibCommand;
import org.bidib.jbidibc.core.message.FwUpdateOpMessage;
import org.bidib.jbidibc.core.message.FwUpdateStatResponse;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.simulation.SimulatorNode;
import org.bidib.jbidibc.simulation.SimulatorRegistry;
import org.bidib.jbidibc.simulation.events.NodeLostEvent;
import org.bidib.jbidibc.simulation.net.SimulationBidibMessageProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FirmwareErrorSimulator extends LightControlSimulator {
    private static final Logger LOGGER = LoggerFactory.getLogger(FirmwareErrorSimulator.class);

    private int dataBlocksReceived;

    public FirmwareErrorSimulator(byte[] nodeAddress, long uniqueId, boolean autoAddFeature,
        SimulationBidibMessageProcessor messageReceiver) {
        super(nodeAddress, uniqueId, autoAddFeature, messageReceiver);
    }

    protected byte[] processFwUpdateOpRequest(BidibCommand bidibMessage) {

        byte[] response = null;
        try {
            FwUpdateOpMessage fwUpdateOpMessage = (FwUpdateOpMessage) bidibMessage;
            FirmwareUpdateOperation operation = fwUpdateOpMessage.getOperation();

            LOGGER.info("processFwUpdateOpRequest, operation: {}", operation);

            int timeout = 0;
            int status = BidibLibrary.BIDIB_MSG_FW_UPDATE_STAT_ERROR;
            switch (operation) {
                case ENTER:
                    status = BidibLibrary.BIDIB_MSG_FW_UPDATE_STAT_READY;
                    LOGGER.warn("Send no answer on ENTER request. Send NODE_LOST instead.");
                    // get the master node
                    SimulatorNode master = SimulatorRegistry.getInstance().getSimulator("0");

                    // if (master instanceof GBMboostMasterSimulator) {
                    // NodeLostEvent nodeLostEvent = new NodeLostEvent(getNodeAddress(), getUniqueId());
                    // ((GBMboostMasterSimulator) master).nodeLostEvent(nodeLostEvent);
                    // return response;
                    // }
                    break;
                case DATA:
                    status = BidibLibrary.BIDIB_MSG_FW_UPDATE_STAT_DATA;
                    dataBlocksReceived++;

                    if (dataBlocksReceived > 0 && dataBlocksReceived < 200 && dataBlocksReceived % 15 == 0) {
                        LOGGER.warn("Return a timeout > 0");
                        timeout = 30;
                    }
                    break;
                case SETDEST:
                    status = BidibLibrary.BIDIB_MSG_FW_UPDATE_STAT_DATA;
                    break;
                case EXIT:
                    status = BidibLibrary.BIDIB_MSG_FW_UPDATE_STAT_EXIT;
                    LOGGER.warn("Send no answer on EXIT request.");
                    // return response;
                    break;
                default:
                    break;
            }
            FwUpdateStatResponse fwUpdateStatResponse =
                new FwUpdateStatResponse(bidibMessage.getAddr(), getNextSendNum(), ByteUtils.getLowByte(status),
                    ByteUtils.getLowByte(timeout));
            response = fwUpdateStatResponse.getContent();

            if (operation == FirmwareUpdateOperation.EXIT) {
                // get the master node
                SimulatorNode master = SimulatorRegistry.getInstance().getSimulator("0");

                if (master instanceof GBMboostMasterSimulator) {
                    NodeLostEvent nodeLostEvent = new NodeLostEvent(getNodeAddress(), getUniqueId());
                    ((GBMboostMasterSimulator) master).nodeLostEvent(nodeLostEvent);
                    return response;
                }
            }

        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create fwUpdateStat response failed.", ex);
        }
        return response;
    }
}
