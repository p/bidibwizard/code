package org.bidib.wizard.simulation.ports;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.port.ReconfigPortConfigValue;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.wizard.mvc.main.model.GenericPort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GenericSimulationPort extends GenericPort {
    private static final Logger LOGGER = LoggerFactory.getLogger(GenericSimulationPort.class);

    private byte portStatus;

    public GenericSimulationPort(Integer portNumber) {
        super(portNumber);
    }

    /**
     * @return the portStatus
     */
    public byte getPortStatus() {
        return portStatus;
    }

    /**
     * @param portStatus
     *            the portStatus to set
     */
    public void setPortStatus(byte portStatus) {
        this.portStatus = portStatus;
    }

    public void setCurrentPortType(LcOutputType portType, int portMap) {

        ReconfigPortConfigValue newReconfig =
            new ReconfigPortConfigValue(ByteUtils.getInt(portType.getType()), portMap);

        LOGGER.info("Set the new BIDIB_PCFG_RECONFIG: {}", newReconfig);
        setPortConfigValue(BidibLibrary.BIDIB_PCFG_RECONFIG, newReconfig);

        getKnownPortConfigKeys().add(BidibLibrary.BIDIB_PCFG_RECONFIG);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
