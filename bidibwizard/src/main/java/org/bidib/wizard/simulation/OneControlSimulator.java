package org.bidib.wizard.simulation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.Timer;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.bidib.jbidibc.core.AddressData;
import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.BidibPort;
import org.bidib.jbidibc.core.Feature;
import org.bidib.jbidibc.core.LcConfigX;
import org.bidib.jbidibc.core.enumeration.InputPortEnum;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.message.AccessorySetMessage;
import org.bidib.jbidibc.core.message.AccessoryStateResponse;
import org.bidib.jbidibc.core.message.BidibCommand;
import org.bidib.jbidibc.core.message.BidibMessage;
import org.bidib.jbidibc.core.message.FeedbackAddressResponse;
import org.bidib.jbidibc.core.message.FeedbackConfidenceResponse;
import org.bidib.jbidibc.core.message.FeedbackFreeResponse;
import org.bidib.jbidibc.core.message.FeedbackGetRangeMessage;
import org.bidib.jbidibc.core.message.FeedbackMultipleResponse;
import org.bidib.jbidibc.core.message.FeedbackOccupiedResponse;
import org.bidib.jbidibc.core.message.LcConfigXGetMessage;
import org.bidib.jbidibc.core.message.LcConfigXResponse;
import org.bidib.jbidibc.core.message.LcConfigXSetMessage;
import org.bidib.jbidibc.core.message.LcKeyMessage;
import org.bidib.jbidibc.core.message.LcKeyResponse;
import org.bidib.jbidibc.core.message.LcNotAvailableResponse;
import org.bidib.jbidibc.core.message.LcOutputQueryMessage;
import org.bidib.jbidibc.core.message.LcStatResponse;
import org.bidib.jbidibc.core.port.BytePortConfigValue;
import org.bidib.jbidibc.core.port.PortConfigValue;
import org.bidib.jbidibc.core.port.ReconfigPortConfigValue;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.core.utils.MessageUtils;
import org.bidib.jbidibc.core.utils.NodeUtils;
import org.bidib.jbidibc.simulation.SwitchingFunctionsNode;
import org.bidib.jbidibc.simulation.net.SimulationBidibMessageProcessor;
import org.bidib.wizard.comm.FeedbackPortStatus;
import org.bidib.wizard.comm.InputPortStatus;
import org.bidib.wizard.mvc.main.model.FeedbackAddressData;
import org.bidib.wizard.mvc.main.model.FeedbackPort;
import org.bidib.wizard.mvc.main.model.GenericPort;
import org.bidib.wizard.mvc.main.model.Port;
import org.bidib.wizard.simulation.events.FeedbackConfidenceSetEvent;
import org.bidib.wizard.simulation.events.FeedbackConfidenceStatusEvent;
import org.bidib.wizard.simulation.events.FeedbackPortSetStatusEvent;
import org.bidib.wizard.simulation.events.FeedbackPortStatusEvent;
import org.bidib.wizard.simulation.events.InputPortStatusEvent;
import org.bidib.wizard.simulation.ports.GenericSimulationPort;
import org.bushe.swing.event.EventBus;
import org.bushe.swing.event.annotation.EventSubscriber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OneControlSimulator extends LightControlSimulator implements SwitchingFunctionsNode {
    private static final Logger LOGGER = LoggerFactory.getLogger(OneControlSimulator.class);

    private static final String SIMULATION_PANEL_CLASS = "org.bidib.wizard.mvc.simulation.view.panel.OneControlPanel";

    protected static final int NUM_OF_FEEDBACK_PORTS = 20;

    private final Map<Integer, GenericSimulationPort> genericPorts = new HashMap<Integer, GenericSimulationPort>();

    private final Map<Integer, FeedbackPort> feedbackPorts = new HashMap<Integer, FeedbackPort>();

    private final AtomicBoolean statusFreeze = new AtomicBoolean();

    private final AtomicBoolean statusValid = new AtomicBoolean();

    private final AtomicBoolean statusSignal = new AtomicBoolean();

    public OneControlSimulator(byte[] nodeAddress, long uniqueId, boolean autoAddFeature,
        SimulationBidibMessageProcessor messageReceiver) {
        super(nodeAddress, uniqueId, autoAddFeature, messageReceiver);
    }

    @Override
    protected void prepareFeatures() {
        LOGGER.info("Prepare the features.");
        super.prepareFeatures();

        features.remove(new Feature(BidibLibrary.FEATURE_ACCESSORY_MACROMAPPED, 2));
        features.add(new Feature(BidibLibrary.FEATURE_ACCESSORY_MACROMAPPED, 8));

        if (MapUtils.isNotEmpty(feedbackPorts)) {
            features.add(new Feature(BidibLibrary.FEATURE_BM_SECACK_AVAILABLE, 1));
            features.add(new Feature(BidibLibrary.FEATURE_BM_SECACK_ON, 0));

            features.add(new Feature(BidibLibrary.FEATURE_BM_ON, 1));
            // features.add(new Feature(BidibLibrary.FEATURE_BM_SIZE, feedbackPorts.size()));
        }
    }

    @Override
    public void postConstruct() {
        super.postConstruct();

        features.add(new Feature(BidibLibrary.FEATURE_BM_SIZE, feedbackPorts.size()));

        Feature feature = getFeature(BidibLibrary.FEATURE_CTRL_PORT_FLAT_MODEL);
        if (feature != null) {
            LOGGER.info("The current simulator has the flat model configured. Prepare the generic ports.");
            int numPorts = feature.getValue();

            LOGGER.info("Create generic ports, numPorts: {}", numPorts);

            for (int portNumber = 0; portNumber < numPorts; portNumber++) {
                GenericSimulationPort port = new GenericSimulationPort(portNumber);

                genericPorts.put(portNumber, port);
            }
        }
    }

    @Override
    protected void prepareCVs() {
        super.prepareCVs();
    }

    @Override
    public String getSimulationPanelClass() {
        return SIMULATION_PANEL_CLASS;
    }

    @Override
    public void start() {
        LOGGER.info("Start the simulator for address: {}", getAddress());

        // prepare the feedback ports
        setupFeedbackPorts();

        super.start();

        if (!MapUtils.isEmpty(genericPorts)) {
            int index = 0;

            for (int portNum = 0; portNum < servoPortCount; portNum++) {
                GenericSimulationPort genericPort = genericPorts.get(portNum + index);
                genericPort.setCurrentPortType(LcOutputType.SERVOPORT, 0x0004);
            }
            index += servoPortCount;

            for (int portNum = 0; portNum < switchPortCount; portNum++) {
                GenericSimulationPort genericPort = genericPorts.get(portNum + index);

                // only even port can be a switchpair port
                int mask = (1 << BidibLibrary.BIDIB_PORTTYPE_SWITCH | 1 << BidibLibrary.BIDIB_PORTTYPE_SWITCHPAIR);
                if ((portNum & 0x01) == 0x01) {
                    mask = (1 << BidibLibrary.BIDIB_PORTTYPE_SWITCH);
                }
                genericPort.setCurrentPortType(LcOutputType.SWITCHPORT, mask /* 0x0001 */);

                Map<Byte, PortConfigValue<?>> configX = genericPort.getPortConfig();
                configX.put(BidibLibrary.BIDIB_PCFG_LOAD_TYPE, new BytePortConfigValue(Byte.valueOf((byte) 0x00)));

                genericPort.setPortConfig(configX);
            }

            // TODO this is a hack for the OneControl
            index = inputPortOffset;
            LOGGER.info("Set the port type for input ports, index: {}", index);
            for (int portNum = 0; portNum < inputPortCount; portNum++) {
                GenericSimulationPort genericPort = genericPorts.get(portNum + index);
                int mask =
                    (1 << BidibLibrary.BIDIB_PORTTYPE_SWITCH /* | 1 << BidibLibrary.BIDIB_PORTTYPE_SWITCHPAIR */
                        | 1 << BidibLibrary.BIDIB_PORTTYPE_INPUT);
                genericPort.setCurrentPortType(LcOutputType.INPUTPORT, mask /* 0x8001 */);
            }

            GenericSimulationPort simPort = null;
            LOGGER.info("Prepare CV for OneControl.");
            for (Entry<String, String> cv : configurationVariables.entrySet()) {
                switch (cv.getKey()) {
                    case "437":
                        simPort = genericPorts.get(inputPortOffset);
                        break;
                    case "440":
                        simPort = genericPorts.get(inputPortOffset + 1);
                        break;
                    case "443":
                        simPort = genericPorts.get(inputPortOffset + 2);
                        break;
                    case "446":
                        simPort = genericPorts.get(inputPortOffset + 3);
                        break;
                    case "449":
                        simPort = genericPorts.get(inputPortOffset + 4);
                        break;
                    case "452":
                        simPort = genericPorts.get(inputPortOffset + 5);
                        break;
                    case "455":
                        simPort = genericPorts.get(inputPortOffset + 6);
                        break;
                    case "458":
                        simPort = genericPorts.get(inputPortOffset + 7);
                        break;
                    case "461":
                        simPort = genericPorts.get(inputPortOffset + 8);
                        break;
                    case "464":
                        simPort = genericPorts.get(inputPortOffset + 9);
                        break;
                    case "467":
                        simPort = genericPorts.get(inputPortOffset + 10);
                        break;
                    case "470":
                        simPort = genericPorts.get(inputPortOffset + 11);
                        break;
                    case "473":
                        simPort = genericPorts.get(inputPortOffset + 12);
                        break;
                    case "476":
                        simPort = genericPorts.get(inputPortOffset + 13);
                        break;
                    case "479":
                        simPort = genericPorts.get(inputPortOffset + 14);
                        break;
                    case "482":
                        simPort = genericPorts.get(inputPortOffset + 15);
                        break;
                    default:
                        break;
                }

                if (simPort != null) {
                    switch (cv.getValue()) {
                        case "0":
                            simPort.setCurrentPortType(LcOutputType.SWITCHPORT, 0x8001);
                            break;
                        default:
                            simPort.setCurrentPortType(LcOutputType.INPUTPORT, 0x8001);
                            break;
                    }

                    LOGGER.info("Prepared port: {}", simPort);
                }

                simPort = null;
            }

        }
    }

    @Override
    public void stop() {

        super.stop();
    }

    private void setupFeedbackPorts() {
        for (int id = 0; id < NUM_OF_FEEDBACK_PORTS; id++) {
            FeedbackPort port = new FeedbackPort();

            port.setId(id);
            port.setStatus(id % 3 == 0 ? FeedbackPortStatus.FREE : FeedbackPortStatus.OCCUPIED);
            feedbackPorts.put(id, port);
        }

        uniqueId = NodeUtils.setHasFeedbackFunctions(uniqueId);
        LOGGER.info("Set the feeback ports flag in the class. New uniqueId: {}", NodeUtils.formatHexUniqueId(uniqueId));
    }

    @Override
    protected byte[] prepareResponse(BidibCommand bidibMessage) {

        byte[] response = null;
        switch (ByteUtils.getInt(bidibMessage.getType())) {
            case BidibLibrary.MSG_BM_GET_RANGE:
                response = processBmGetRangeRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_BM_MIRROR_MULTIPLE:
                processBmMirrorMultipleRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_BM_MIRROR_OCC:
                processBmMirrorOccupiedRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_BM_MIRROR_FREE:
                processBmMirrorFreeRequest(bidibMessage);
                break;

            case BidibLibrary.MSG_BM_ADDR_GET_RANGE:
                processBmAddrGetRangeRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_BM_GET_CONFIDENCE:
                response = processBmGetConfidenceRequest(bidibMessage);
                break;
            default:
                response = super.prepareResponse(bidibMessage);
                break;
        }
        return response;
    }

    protected byte[] processBmGetRangeRequest(BidibCommand bidibMessage) {
        LOGGER.info("Process the FeedbackGetRangeMessage: {}", bidibMessage);

        byte[] response = null;
        try {
            FeedbackGetRangeMessage feedbackGetRangeMessage = (FeedbackGetRangeMessage) bidibMessage;
            byte baseAddress = feedbackGetRangeMessage.getBegin();
            int end = feedbackGetRangeMessage.getEnd();
            byte feedbackSize =
                ByteUtils.getLowByte(feedbackGetRangeMessage.getEnd() - feedbackGetRangeMessage.getBegin());

            byte value = 0x00;
            int index = 0;

            byte[] feedbackMultiple = new byte[feedbackSize / 8];
            int position = feedbackMultiple.length;

            for (int portNum = end; portNum > baseAddress; portNum--) {
                value = (byte) ((value & 0xFF) << 1);
                FeedbackPort fbp = feedbackPorts.get(portNum - 1);
                byte status = 0;
                if (fbp != null) {
                    status = (byte) (fbp.getStatus().getType().getType() & 0x01);
                }

                value |= status;
                index++;
                if (index > 7) {
                    feedbackMultiple[position - 1] = value;
                    value = 0;
                    index = 0;
                    position--;
                }
            }

            LOGGER.info("Prepared feedback multiple: {}", ByteUtils.bytesToHex(feedbackMultiple));

            FeedbackMultipleResponse feedbackMultipleResponse =
                new FeedbackMultipleResponse(bidibMessage.getAddr(), getNextSendNum(), baseAddress, feedbackSize,
                    feedbackMultiple);
            response = feedbackMultipleResponse.getContent();
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create feedbackMultiple response failed.", ex);
        }
        catch (Exception ex) {
            LOGGER.warn("Create feedbackMultiple response failed.", ex);
        }
        return response;
    }

    protected void processBmMirrorMultipleRequest(BidibCommand bidibMessage) {
        LOGGER.info("Process the FeedbackMirrorMultipleMessage: {}, do nothing ...", bidibMessage);
    }

    protected void processBmMirrorOccupiedRequest(BidibCommand bidibMessage) {
        LOGGER.info("Process the processBmMirrorOccupiedRequest: {}, do nothing ...", bidibMessage);
    }

    protected void processBmMirrorFreeRequest(BidibCommand bidibMessage) {
        LOGGER.info("Process the processBmMirrorFreeRequest: {}, do nothing ...", bidibMessage);
    }

    protected void processBmAddrGetRangeRequest(BidibCommand bidibMessage) {

        try {
            for (FeedbackPort port : feedbackPorts.values()) {

                int detectorNumber = port.getId();
                List<AddressData> bidibAddresses = new ArrayList<>();
                List<FeedbackAddressData> addresses = port.getAddresses();
                for (FeedbackAddressData addressData : addresses) {
                    AddressData bidibAddress = new AddressData(addressData.getAddress(), addressData.getType());
                    bidibAddresses.add(bidibAddress);
                }
                FeedbackAddressResponse feedbackAddressResponse =
                    new FeedbackAddressResponse(bidibMessage.getAddr(), getNextSendNum(), detectorNumber,
                        bidibAddresses);
                byte[] response = feedbackAddressResponse.getContent();
                LOGGER.info("Prepare feedbackAddressResponse: {}", ByteUtils.bytesToHex(response));
                sendSpontanousResponse(response);
            }
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create feedbackAddress response failed.", ex);
        }
    }

    protected byte[] processBmGetConfidenceRequest(BidibCommand bidibMessage) {
        byte[] response = null;
        try {

            byte valid = (byte) (statusValid.get() ? 1 : 0);
            byte freeze = (byte) (statusFreeze.get() ? 1 : 0);
            byte signal = (byte) (statusSignal.get() ? 1 : 0);

            // TODO if more than a single GBM16T is attached we must set more bits? See 4.7.4. Uplink: Nachrichten für
            // Belegtmelder --> MSG_BM_CONFIDENCE
            // Test with real system: See MainMessageListener.confidence()

            FeedbackConfidenceResponse feedbackConfidenceResponse =
                new FeedbackConfidenceResponse(bidibMessage.getAddr(), getNextSendNum(), valid, freeze, signal);
            response = feedbackConfidenceResponse.getContent();
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create feedbackConfidence response failed.", ex);
        }
        return response;
    }

    protected byte[] processLcConfigXSetRequest(BidibCommand bidibMessage) {

        LOGGER.info("Process the LcConfigXSet request: {}", bidibMessage);
        byte[] response = null;

        if (getFlatPortModelPortCount() > 0) {
            try {
                LcConfigXSetMessage lcConfigXSetMessage = (LcConfigXSetMessage) bidibMessage;
                int outputNumber = lcConfigXSetMessage.getPortNumber(getPortModel());
                LcOutputType outputType = lcConfigXSetMessage.getPortType(getPortModel());

                GenericPort port = null;
                port = genericPorts.get(Integer.valueOf(outputNumber));
                LOGGER.info("Set LcConfig for output number: {}, port: {}", outputNumber, port);

                BidibPort bidibPort = prepareBidibPort(getPortModel(), outputType, outputNumber);

                if (port != null) {

                    // the problem here is that the BIDIB_PCFG_RECONFIG overwrites the supported port types
                    LcConfigX lcConfigX = lcConfigXSetMessage.getLcConfigX();
                    ReconfigPortConfigValue reconfig =
                        (ReconfigPortConfigValue) lcConfigX.getPortConfig().get(BidibLibrary.BIDIB_PCFG_RECONFIG);
                    if (reconfig != null) {
                        // we must keep the supported port types
                        Integer supportedPortTypes = port.getSupportedPortTypes();
                        if (supportedPortTypes != null) {
                            ReconfigPortConfigValue newReconfig =
                                new ReconfigPortConfigValue(reconfig.getCurrentOutputType().getType(),
                                    supportedPortTypes);
                            LOGGER.info("Prepared BIDIB_PCFG_RECONFIG to replace: {}", newReconfig);
                            lcConfigX.getPortConfig().put(BidibLibrary.BIDIB_PCFG_RECONFIG, newReconfig);
                        }
                    }
                    port.setPortConfig(lcConfigX.getPortConfig());

                    // TODO remove this test
                    if (outputNumber == 5) {
                        LOGGER.warn("Change the content to signal an error ...");
                        lcConfigX.getPortConfig().put(BidibLibrary.BIDIB_PCFG_NONE, new BytePortConfigValue((byte) 12));
                    }

                    byte[] content = MessageUtils.getCodedPortConfig(lcConfigX, getPortModel());
                    LOGGER.info("Prepared content: {}", ByteUtils.bytesToHex(content));

                    LcConfigXResponse lcConfigXResponse =
                        new LcConfigXResponse(bidibMessage.getAddr(), getNextSendNum(), content);
                    response = lcConfigXResponse.getContent();
                }
                else {
                    LOGGER.warn("No port assigned!");
                    LcNotAvailableResponse magicResponse =
                        new LcNotAvailableResponse(bidibMessage.getAddr(), getNextSendNum(), bidibPort);
                    response = magicResponse.getContent();
                }
            }
            catch (ProtocolException ex) {
                LOGGER.warn("Create LcConfigX response failed.", ex);
            }
        }
        else {
            response = super.processLcConfigXSetRequest(bidibMessage);
        }
        return response;
    }

    protected byte[] processLcConfigXGetRequest(BidibCommand bidibMessage) {
        LOGGER.info("Process the LcConfigXGet request: {}", bidibMessage);
        byte[] response = null;

        if (getFlatPortModelPortCount() > 0) {

            try {
                LcConfigXGetMessage lcConfigXGetMessage = (LcConfigXGetMessage) bidibMessage;
                int outputNumber = lcConfigXGetMessage.getPortNumber(getPortModel());
                LcOutputType outputType = lcConfigXGetMessage.getPortType(getPortModel());

                GenericPort port = null;
                Map<Byte, PortConfigValue<?>> values = new LinkedHashMap<>();

                port = genericPorts.get(Integer.valueOf(outputNumber));

                BidibPort bidibPort = prepareBidibPort(getPortModel(), outputType, outputNumber);

                if (port != null) {

                    if (outputNumber >= 0 && outputNumber < servoPortCount) { // 4 or 8 servo ports
                        values.put(BidibLibrary.BIDIB_PCFG_RECONFIG,
                            new ReconfigPortConfigValue(Integer.valueOf(0x000402)));
                    }
                    else if (outputNumber >= 4 && outputNumber < (servoPortCount + 16)) { // 16 power ports
                        values.putAll(port.getPortConfig());

                        // values.put(BidibLibrary.BIDIB_PCFG_RECONFIG,
                        // new ReconfigPortConfigValue(Integer.valueOf(0x000100)));
                        //
                        // values.put(BidibLibrary.BIDIB_PCFG_IO_CTRL, new BytePortConfigValue(Byte.valueOf((byte)
                        // 0x00)));
                        // values.put(BidibLibrary.BIDIB_PCFG_TICKS, new BytePortConfigValue(Byte.valueOf((byte)
                        // 0x0F)));
                    }
                    else { // GPIO are all input ports
                        values.put(BidibLibrary.BIDIB_PCFG_RECONFIG,
                            new ReconfigPortConfigValue(Integer.valueOf(0x80010F)));

                        values.put(BidibLibrary.BIDIB_PCFG_INPUT_CTRL,
                            new BytePortConfigValue(Byte.valueOf((byte) 0x00)));
                        values.put(BidibLibrary.BIDIB_PCFG_TICKS, new BytePortConfigValue(Byte.valueOf((byte) 0x00)));
                    }

                    // TODO remove this test
                    if (outputNumber == 2) {
                        values.put(BidibLibrary.BIDIB_PCFG_NONE, new BytePortConfigValue((byte) 12));
                    }

                    LOGGER.info("Return config of port: {}", port);
                    LcConfigX lcConfigX = new LcConfigX(bidibPort, values);

                    LcConfigXResponse lcConfigXResponse =
                        new LcConfigXResponse(bidibMessage.getAddr(), getNextSendNum(),
                            MessageUtils.getCodedPortConfig(lcConfigX, getPortModel()));
                    response = lcConfigXResponse.getContent();
                }
                else {
                    LOGGER.warn("No port available with port number: {}", outputNumber);
                    LcNotAvailableResponse notAvailableResponse =
                        new LcNotAvailableResponse(bidibMessage.getAddr(), getNextSendNum(), bidibPort);
                    response = notAvailableResponse.getContent();
                }
            }
            catch (ProtocolException ex) {
                LOGGER.warn("Create LcConfigX response failed.", ex);
            }
        }
        else {
            response = super.processLcConfigXGetRequest(bidibMessage);
        }
        return response;
    }

    protected void processLcConfigXGetAllRequest(BidibCommand bidibMessage) {
        LOGGER.info("Process the LcConfigXGetAll request: {}", bidibMessage);

        if (getFlatPortModelPortCount() > 0) {
            LcOutputType outputType = LcOutputType.SWITCHPORT;

            Map<Byte, PortConfigValue<?>> values = new LinkedHashMap<>();

            try {
                for (GenericPort port : genericPorts.values()) {
                    int portNumber = port.getPortNumber();
                    LOGGER.info("Prepare lcConfigXResponse for port number: {}", portNumber);
                    values.clear();

                    if (portNumber >= 0 && portNumber < servoPortCount) { // 4 servo ports
                        values.put(BidibLibrary.BIDIB_PCFG_RECONFIG,
                            new ReconfigPortConfigValue(Integer.valueOf(0x000402)));
                        values.put(BidibLibrary.BIDIB_PCFG_SERVO_ADJ_L,
                            new BytePortConfigValue(Byte.valueOf((byte) 0x00)));

                        if (portNumber != 4) {
                            values.put(BidibLibrary.BIDIB_PCFG_SERVO_ADJ_H,
                                new BytePortConfigValue(Byte.valueOf((byte) 0xFA)));
                            values.put(BidibLibrary.BIDIB_PCFG_SERVO_SPEED,
                                new BytePortConfigValue(Byte.valueOf((byte) 0x05)));
                        }
                        else {
                            LOGGER.warn(
                                "Port 4 has no BIDIB_PCFG_SERVO_ADJ_H and BIDIB_PCFG_SERVO_SPEED for testing purposes!");
                        }
                    }
                    else if (portNumber >= 4 && portNumber < (servoPortCount + 16)) { // 16 power ports

                        values.putAll(port.getPortConfig());

                        // values.put(BidibLibrary.BIDIB_PCFG_RECONFIG,
                        // new ReconfigPortConfigValue(Integer.valueOf(0x008100)));
                        //
                        // values.put(BidibLibrary.BIDIB_PCFG_IO_CTRL, new BytePortConfigValue(Byte.valueOf((byte)
                        // 0x00)));
                        // values.put(BidibLibrary.BIDIB_PCFG_TICKS, new BytePortConfigValue(Byte.valueOf((byte)
                        // 0x0F)));
                    }
                    else { // GPIO are all input ports

                        int currentPortType = ByteUtils.getInt(port.getCurrentPortType().getType());

                        values.put(BidibLibrary.BIDIB_PCFG_RECONFIG,
                            new ReconfigPortConfigValue(currentPortType, 0x8001));

                        values.put(BidibLibrary.BIDIB_PCFG_INPUT_CTRL,
                            new BytePortConfigValue(Byte.valueOf((byte) 0x00)));
                        values.put(BidibLibrary.BIDIB_PCFG_TICKS, new BytePortConfigValue(Byte.valueOf((byte) 0x00)));
                    }

                    // TODO remove this test
                    if (portNumber == 6) {
                        values.put(BidibLibrary.BIDIB_PCFG_NONE, new BytePortConfigValue((byte) 12));
                    }

                    LOGGER.info("Return config of port: {}", port);
                    BidibPort bidibPort = prepareBidibPort(getPortModel(), outputType, portNumber);
                    LcConfigX lcConfigX = new LcConfigX(bidibPort, values);

                    LcConfigXResponse lcConfigXResponse =
                        new LcConfigXResponse(bidibMessage.getAddr(), getNextSendNum(),
                            MessageUtils.getCodedPortConfig(lcConfigX, getPortModel()));
                    byte[] response = lcConfigXResponse.getContent();

                    LOGGER.info("Prepared lcConfigXResponse: {}", ByteUtils.bytesToHex(response));
                    sendSpontanousResponse(response);
                }
            }
            catch (ProtocolException ex) {
                LOGGER.warn("Create LcConfigXResponse response failed.", ex);
            }
        }
        else {
            super.processLcConfigXGetAllRequest(bidibMessage);
        }
    }

    protected byte[] processLcOutputQueryRequest(BidibCommand bidibMessage) {

        LOGGER.info("Process the LcOutputQuery request: {}", bidibMessage);
        byte[] response = null;

        if (isPortFlatModelAvailable()) {
            byte portState = 0;

            try {
                LcOutputQueryMessage lcOutputQueryMessage = (LcOutputQueryMessage) bidibMessage;
                LcOutputType outputType = lcOutputQueryMessage.getPortType(getPortModel());
                int outputNumber = lcOutputQueryMessage.getPortNumber(getPortModel());

                GenericPort port = genericPorts.get(Integer.valueOf(outputNumber));

                BidibPort bidibPort = prepareBidibPort(getPortModel(), outputType, outputNumber);

                if (port != null) {
                    portState = ByteUtils.getLowByte(0xFF);
                }
                else {
                    LOGGER.warn("No port available with portNumber: {}", outputNumber);
                }
                LcStatResponse lcStatResponse =
                    new LcStatResponse(bidibMessage.getAddr(), getNextSendNum(), bidibPort, portState);
                response = lcStatResponse.getContent();
            }
            catch (ProtocolException ex) {
                LOGGER.warn("Create LcStat response failed.", ex);
            }
        }
        else {
            response = super.processLcOutputQueryRequest(bidibMessage);
        }
        return response;
    }

    protected byte[] processLcKeyQueryRequest(BidibCommand bidibMessage) {
        LOGGER.info("Process the LcKeyQuery request: {}", bidibMessage);
        byte[] response = null;

        if (getFlatPortModelPortCount() > 0) {
            byte keyState = 0;
            GenericSimulationPort port = null;
            try {
                LcKeyMessage lcKeyMessage = (LcKeyMessage) bidibMessage;
                int portNumber = lcKeyMessage.getKeyNumber();

                port = genericPorts.get(Integer.valueOf(portNumber));

                if (port != null) {
                    keyState = port.getPortStatus();
                }
                else {
                    LOGGER.warn("No port available with portNumber: {}", portNumber);
                }

                LcKeyResponse lcKeyResponse =
                    new LcKeyResponse(bidibMessage.getAddr(), getNextSendNum(), ByteUtils.getLowByte(portNumber),
                        keyState);
                response = lcKeyResponse.getContent();
            }
            catch (ProtocolException ex) {
                LOGGER.warn("Create LcKey response failed.", ex);
            }

            if (port != null) {
                publishInputPortChange(port);
            }
        }
        else {
            response = super.processLcKeyQueryRequest(bidibMessage);
        }
        return response;
    }

    // //////

    private Timer simulationTimer;

    private List<String> accessoryResponseList = new ArrayList<>();

    protected byte[] processAccessorySetRequest(final BidibCommand bidibMessage) {
        LOGGER.info("Process the AccessorySet request: {}", bidibMessage);
        byte[] response = null;

        try {
            AccessorySetMessage accessorySetMessage = (AccessorySetMessage) bidibMessage;
            int accessoryNumber = accessorySetMessage.getAccessoryNumber();
            int aspect = accessorySetMessage.getAspect();

            byte[] value = new byte[] { 0, 0, 0 };
            // byte[] value = new byte[] { 2, 1, 20 };

            if (accessoryNumber == 1 && aspect == 1) {
                LOGGER.warn("Adding simulated error to accessory state response");

                value = new byte[] { 2, 1, 20 };

                // add the remaining response values
                accessoryResponseList.add(accessoryNumber + "," + aspect + ",2,128,7");
                accessoryResponseList.add(accessoryNumber + ",255,2,129,7");
                accessoryResponseList.add(accessoryNumber + ",255,2,0,0");

                simulationTimer = new Timer(1500, new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {

                        if (!accessoryResponseList.isEmpty()) {
                            String accessoryResponse = accessoryResponseList.remove(0);
                            if (StringUtils.isNotBlank(accessoryResponse)) {
                                LOGGER.info("Send response: {}", accessoryResponse);
                                // parse
                                String[] parts = accessoryResponse.split(",");
                                byte[] value = new byte[parts.length];
                                int index = 0;
                                for (String part : parts) {
                                    int val = Integer.parseInt(part);
                                    value[index] = ByteUtils.getLowByte(val);
                                    index++;
                                }

                                try {
                                    AccessoryStateResponse accessoryStateResponse =
                                        new AccessoryStateResponse(bidibMessage.getAddr(), getNextSendNum(),
                                            /* accessoryNumber */value[0], /* aspect */value[1],
                                            ByteUtils.subArray(value, 2));
                                    byte[] response = accessoryStateResponse.getContent();
                                    LOGGER.info("Prepared accessoryStateResponse: {}", accessoryStateResponse);
                                    sendSpontanousResponse(response);
                                }
                                catch (ProtocolException ex) {
                                    LOGGER.warn("Create AccessoryState response failed.", ex);
                                }
                            }
                        }

                        if (accessoryResponseList.isEmpty()) {
                            LOGGER.info("No more entries in accessoryResponseList. Stop and release timer.");
                            simulationTimer.stop();
                            simulationTimer = null;
                        }
                    }
                });
                simulationTimer.start();
            }

            // send accessory state response
            AccessoryStateResponse accessoryStateResponse =
                new AccessoryStateResponse(bidibMessage.getAddr(), getNextSendNum(),
                    ByteUtils.getLowByte(accessoryNumber), ByteUtils.getLowByte(aspect), value);
            response = accessoryStateResponse.getContent();

        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create AccessoryState response failed.", ex);
        }
        return response;
    }

    // //////

    private void publishInputPortChange(GenericSimulationPort port) {

        InputPortStatus status = InputPortStatus.valueOf(InputPortEnum.valueOf(port.getPortStatus()));

        LOGGER.info("The inputport status has changed, notify the listeners, nodeAddress: {}", nodeAddress);
        EventBus.publish(new InputPortStatusEvent(ByteUtils.bytesToHex(nodeAddress), port.getPortNumber(), status));
    }

    protected void changeInputPortStatus(int portNum) {

        GenericSimulationPort port = genericPorts.get(portNum);
        if (port != null) {

            InputPortStatus portStatus = InputPortStatus.valueOf(InputPortEnum.valueOf(port.getPortStatus()));

            switch (portStatus) {
                case OFF:
                    port.setPortStatus(InputPortStatus.ON.getType().getType());
                    break;
                default:
                    port.setPortStatus(InputPortStatus.OFF.getType().getType());
                    break;
            }

            // prepare the request
            final LcKeyMessage lcKeyMessage = new LcKeyMessage(portNum) {
                @Override
                public byte[] getAddr() {
                    return getNodeAddress();
                }
            };
            processRequest(lcKeyMessage);
        }
        else {
            LOGGER.warn("The requested input port is not available: {}", portNum);
        }
    }

    private void publishFeedbackPortChange(Port<?> port) {
        FeedbackPort feedbackPort = (FeedbackPort) port;
        FeedbackPortStatus status = feedbackPort.getStatus();

        LOGGER.info("The feedbackport status has changed, notify the listeners, nodeAddress: {}", nodeAddress);
        EventBus.publish(new FeedbackPortStatusEvent(ByteUtils.bytesToHex(nodeAddress), feedbackPort, status));
    }

    @Override
    public void queryStatus(Class<?> portClass) {
        if (FeedbackPort.class.equals(portClass)) {
            for (FeedbackPort feedbackPort : feedbackPorts.values()) {
                publishFeedbackPortChange(feedbackPort);
            }

            // publish the confidence
            publishFeedbackConfidenceStatusEvent(statusValid.get(), statusFreeze.get(), statusSignal.get());
        }
    }

    @EventSubscriber(eventClass = FeedbackConfidenceSetEvent.class)
    public void feedbackConfidenceSetEvent(FeedbackConfidenceSetEvent feedbackConfidenceEvent) {
        String nodeAddress = feedbackConfidenceEvent.getNodeAddr();
        LOGGER.info("The change of the feedback confidence was requested, nodeAddress: {}", nodeAddress);

        // check if the node is addressed
        if (!isAddressEqual(nodeAddress)) {
            LOGGER.trace("Another node is addressed.");
            return;
        }

        statusValid.set(feedbackConfidenceEvent.getValid());
        statusFreeze.set(feedbackConfidenceEvent.getFreeze());
        statusSignal.set(feedbackConfidenceEvent.getSignal());

        byte valid = (byte) (statusValid.get() ? 1 : 0);
        byte freeze = (byte) (statusFreeze.get() ? 1 : 0);
        byte signal = (byte) (statusSignal.get() ? 1 : 0);

        try {
            FeedbackConfidenceResponse feedbackConfidenceResponse =
                new FeedbackConfidenceResponse(this.nodeAddress, getNextSendNum(), valid, freeze, signal);
            LOGGER.info("Prepared feedbackConfidenceResponse: {}", feedbackConfidenceResponse);
            sendSpontanousResponse(feedbackConfidenceResponse.getContent());
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Send feedbackConfidenceResponse failed.", ex);
        }

        publishFeedbackConfidenceStatusEvent(statusValid.get(), statusFreeze.get(), statusSignal.get());
    }

    private void publishFeedbackConfidenceStatusEvent(boolean valid, boolean freeze, boolean signal) {

        LOGGER.info("The feedbackport confidence status has changed, notify the listeners, nodeAddress: {}",
            nodeAddress);
        EventBus.publish(new FeedbackConfidenceStatusEvent(ByteUtils.bytesToHex(nodeAddress), valid, freeze, signal));
    }

    @EventSubscriber(eventClass = FeedbackPortSetStatusEvent.class)
    public void feedbackPortSetStatus(FeedbackPortSetStatusEvent setStatusEvent) {
        LOGGER.info("The change of the feedback port was requested, setStatusEvent: {}", setStatusEvent);
        String nodeAddress = setStatusEvent.getNodeAddr();

        // check if the node is addressed
        if (!isAddressEqual(nodeAddress)) {
            LOGGER.trace("Another node is addressed.");
            return;
        }

        int portNum = setStatusEvent.getPortNum();
        try {
            changeFeedbackPortStatus(portNum);
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Publish feedback status failed.", ex);
        }
    }

    private int timestamp;

    private int getTimestamp() {
        timestamp += 10;
        if (timestamp > 65000) {
            timestamp = 0;
        }
        return timestamp;
    }

    private boolean hasTimestampFeature() {
        Feature feature = Feature.findFeature(features, BidibLibrary.FEATURE_BM_TIMESTAMP_ON);

        return (feature != null && feature.getValue() > 0);
    }

    private int decoderAddress = 10;

    private int getDecoderAddress() {
        decoderAddress++;

        if (decoderAddress > 15) {
            decoderAddress = 10;
        }

        return decoderAddress;
    }

    private int locationAddress = 120;

    private int getLocationAddress() {
        locationAddress++;

        if (locationAddress > 150) {
            locationAddress = 120;
        }

        return locationAddress;
    }

    protected void changeFeedbackPortStatus(int portNum) throws ProtocolException {

        FeedbackPort port = feedbackPorts.get(portNum);
        if (port != null) {
            BidibMessage response = null;

            // BidibMessage responsePosition = null;

            LOGGER.info("Publish status report for port: {}", port);

            switch (port.getStatus()) {
                case FREE:
                    port.setStatus(FeedbackPortStatus.OCCUPIED);
                    if (hasTimestampFeature()) {
                        response =
                            new FeedbackOccupiedResponse(getNodeAddress(), getNextSendNum(), portNum, getTimestamp());
                    }
                    else {
                        response = new FeedbackOccupiedResponse(getNodeAddress(), getNextSendNum(), portNum);
                    }

                    // int locationType = 0;
                    // responsePosition =
                    // new FeedbackPositionResponse(getNodeAddress(), getNextSendNum(), getDecoderAddress(),
                    // locationType, getLocationAddress());
                    break;
                default:
                    port.setStatus(FeedbackPortStatus.FREE);

                    response = new FeedbackFreeResponse(getNodeAddress(), getNextSendNum(), portNum);
                    break;
            }

            sendSpontanousResponse(response.getContent());

            // if (responsePosition != null) {
            // sendSpontanousResponse(responsePosition.getContent());
            // }

            publishFeedbackPortChange(port);
        }
        else {
            LOGGER.warn("The requested feedback port is not available: {}", portNum);
        }
    }
}
