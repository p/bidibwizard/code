package org.bidib.wizard.simulation;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.BidibPort;
import org.bidib.jbidibc.core.Feature;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.message.AccessoryGetMessage;
import org.bidib.jbidibc.core.message.AccessoryParaGetMessage;
import org.bidib.jbidibc.core.message.AccessoryParaResponse;
import org.bidib.jbidibc.core.message.AccessoryParaSetMessage;
import org.bidib.jbidibc.core.message.AccessorySetMessage;
import org.bidib.jbidibc.core.message.AccessoryStateResponse;
import org.bidib.jbidibc.core.message.BidibCommand;
import org.bidib.jbidibc.core.message.LcConfigGetMessage;
import org.bidib.jbidibc.core.message.LcConfigSetMessage;
import org.bidib.jbidibc.core.message.LcNotAvailableResponse;
import org.bidib.jbidibc.core.message.LcOutputMessage;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.simulation.net.SimulationBidibMessageProcessor;
import org.bidib.jbidibc.simulation.nodes.DefaultNodeSimulator;
import org.bushe.swing.event.annotation.AnnotationProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MultiDecoderSimulator extends DefaultNodeSimulator {

    private static final Logger LOGGER = LoggerFactory.getLogger(MultiDecoderSimulator.class);

    private byte totalAspects = 2;

    public MultiDecoderSimulator(byte[] nodeAddress, long uniqueId, boolean autoAddFeature,
        SimulationBidibMessageProcessor messageReceiver) {
        super(nodeAddress, uniqueId, autoAddFeature, messageReceiver);
    }

    @Override
    protected void prepareFeatures() {
        LOGGER.info("Prepare the features.");
        super.prepareFeatures();

        features.add(new Feature(BidibLibrary.FEATURE_CTRL_MAC_COUNT, 0));
        features.add(new Feature(BidibLibrary.FEATURE_CTRL_MAC_LEVEL, 0));

        features.add(new Feature(BidibLibrary.FEATURE_ACCESSORY_COUNT, 8));
        features.add(new Feature(BidibLibrary.FEATURE_ACCESSORY_MACROMAPPED, 0));
    }

    @Override
    public void start() {
        LOGGER.info("Start the simulator for address: {}", getAddress());

        AnnotationProcessor.process(this);

        super.start();
    }

    @Override
    public void stop() {
        AnnotationProcessor.unprocess(this);
        super.stop();
    }

    @Override
    protected byte[] prepareResponse(BidibCommand bidibMessage) {

        byte[] response = null;
        switch (ByteUtils.getInt(bidibMessage.getType())) {
            case BidibLibrary.MSG_LC_OUTPUT:
                response = processLcOutputRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_LC_CONFIG_GET:
                response = processLcConfigGetRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_LC_CONFIG_SET:
                response = processLcConfigSetRequest(bidibMessage);
                break;

            case BidibLibrary.MSG_ACCESSORY_SET:
                response = processAccessorySetRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_ACCESSORY_GET:
                response = processAccessoryGetRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_ACCESSORY_PARA_SET:
                response = processAccessoryParaSetRequest(bidibMessage);
                break;
            case BidibLibrary.MSG_ACCESSORY_PARA_GET:
                response = processAccessoryParaGetRequest(bidibMessage);
                break;
            default:
                response = super.prepareResponse(bidibMessage);
                break;
        }
        return response;
    }

    protected byte[] processLcOutputRequest(BidibCommand bidibMessage) {
        LOGGER.info("Process the LcOutput request: {}", bidibMessage);
        byte[] response = null;
        try {
            LcOutputMessage lcOutputMessage = (LcOutputMessage) bidibMessage;
            LcOutputType outputType = lcOutputMessage.getOutputType(getPortModel());
            int outputNumber = lcOutputMessage.getOutputNum(getPortModel());

            BidibPort bidibPort = prepareBidibPort(getPortModel(), outputType, outputNumber);

            LcNotAvailableResponse lcNotAvailableResponse =
                new LcNotAvailableResponse(bidibMessage.getAddr(), getNextSendNum(), bidibPort);
            response = lcNotAvailableResponse.getContent();
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create LcStat response failed.", ex);
        }
        return response;
    }

    protected byte[] processLcConfigGetRequest(BidibCommand bidibMessage) {
        LOGGER.info("Process the LcConfigGet request: {}", bidibMessage);
        byte[] response = null;

        try {
            LcConfigGetMessage lcConfigGetMessage = (LcConfigGetMessage) bidibMessage;

            LcOutputType outputType = lcConfigGetMessage.getPortType(getPortModel());
            int outputNumber = lcConfigGetMessage.getPortNumber(getPortModel());

            BidibPort bidibPort = prepareBidibPort(getPortModel(), outputType, outputNumber);

            LcNotAvailableResponse lcNotAvailableResponse =
                new LcNotAvailableResponse(bidibMessage.getAddr(), getNextSendNum(), bidibPort);
            response = lcNotAvailableResponse.getContent();
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create LcConfig response failed.", ex);
        }
        return response;
    }

    protected byte[] processLcConfigSetRequest(BidibCommand bidibMessage) {

        LOGGER.info("Process the LcConfigSet request: {}", bidibMessage);
        byte[] response = null;
        try {
            LcConfigSetMessage lcConfigSetMessage = (LcConfigSetMessage) bidibMessage;

            LcOutputType outputType = lcConfigSetMessage.getPortType(getPortModel());
            int outputNumber = lcConfigSetMessage.getPortNumber(getPortModel());

            BidibPort bidibPort = prepareBidibPort(getPortModel(), outputType, outputNumber);

            LOGGER.warn("No port assigned!");
            LcNotAvailableResponse magicResponse =
                new LcNotAvailableResponse(bidibMessage.getAddr(), getNextSendNum(), bidibPort);
            response = magicResponse.getContent();
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create LcStat response failed.", ex);
        }
        return response;
    }

    protected byte[] processAccessorySetRequest(BidibCommand bidibMessage) {
        LOGGER.info("Process the AccessorySet request: {}", bidibMessage);
        byte[] response = null;

        try {
            AccessorySetMessage accessorySetMessage = (AccessorySetMessage) bidibMessage;
            int accessoryNumber = accessorySetMessage.getAccessoryNumber();
            int aspect = accessorySetMessage.getAspect();

            byte[] value = new byte[] { totalAspects, 0, 0 };

            AccessoryStateResponse accessoryStateResponse =
                new AccessoryStateResponse(bidibMessage.getAddr(), getNextSendNum(), (byte) accessoryNumber,
                    (byte) aspect, value);
            response = accessoryStateResponse.getContent();
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create AccessoryState response failed.", ex);
        }
        return response;
    }

    protected byte[] processAccessoryGetRequest(BidibCommand bidibMessage) {
        LOGGER.info("Process the AccessoryGet request: {}", bidibMessage);
        byte[] response = null;

        try {
            AccessoryGetMessage accessoryGetMessage = (AccessoryGetMessage) bidibMessage;
            int accessoryNumber = accessoryGetMessage.getAccessoryNumber();
            int aspect = 1;
            byte[] value = new byte[] { totalAspects, 0, 0 };

            AccessoryStateResponse accessoryStateResponse =
                new AccessoryStateResponse(bidibMessage.getAddr(), getNextSendNum(), (byte) accessoryNumber,
                    (byte) aspect, value);
            response = accessoryStateResponse.getContent();

            LOGGER.info("Return accessoryStateResponse: {}", accessoryStateResponse.toExtendedString());
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create AccessoryState response failed.", ex);
        }
        return response;
    }

    protected byte[] processAccessoryParaSetRequest(BidibCommand bidibMessage) {
        LOGGER.info("Process the AccessoryParaSet request: {}", bidibMessage);
        byte[] response = null;

        try {
            AccessoryParaSetMessage accessoryParaSetMessage = (AccessoryParaSetMessage) bidibMessage;
            int accessoryNumber = accessoryParaSetMessage.getAccessoryNumber();
            int paraNumber = accessoryParaSetMessage.getParaNumber();

            byte[] value = accessoryParaSetMessage.getValue();

            AccessoryParaResponse accessoryParaResponse =
                new AccessoryParaResponse(bidibMessage.getAddr(), getNextSendNum(),
                    ByteUtils.getLowByte(accessoryNumber), ByteUtils.getLowByte(paraNumber), value);
            response = accessoryParaResponse.getContent();
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create AccessoryPara response failed.", ex);
        }
        return response;
    }

    protected byte[] processAccessoryParaGetRequest(BidibCommand bidibMessage) {
        LOGGER.info("Process the AccessoryParaGet request: {}", bidibMessage);
        byte[] response = null;

        try {
            AccessoryParaGetMessage accessoryParaGetMessage = (AccessoryParaGetMessage) bidibMessage;
            int accessoryNumber = accessoryParaGetMessage.getAccessoryNumber();
            int paraNumber = accessoryParaGetMessage.getParaNumber();

            if (paraNumber == BidibLibrary.BIDIB_ACCESSORY_SWITCH_TIME) {

                LOGGER.info("The param BIDIB_ACCESSORY_SWITCH_TIME is known for accessory 0 and 1!");

                // TODO provide the correct data here ...
                byte[] value = null;

                switch (accessoryNumber) {
                    case 0:
                        value = new byte[] { ByteUtils.getLowByte(0x87) };
                        break;
                    case 1:
                        value = new byte[] { ByteUtils.getLowByte(0x77) };
                        break;
                    default:
                        value = new byte[] { ByteUtils.getLowByte(paraNumber) };
                        paraNumber = BidibLibrary.BIDIB_ACCESSORY_PARA_NOTEXIST;
                        break;
                }

                AccessoryParaResponse accessoryParaResponse =
                    new AccessoryParaResponse(bidibMessage.getAddr(), getNextSendNum(),
                        ByteUtils.getLowByte(accessoryNumber), ByteUtils.getLowByte(paraNumber), value);
                response = accessoryParaResponse.getContent();
            }
            else {
                // TODO provide the correct data here ...
                byte[] value = new byte[] { 0, 0, 0, 0 };

                AccessoryParaResponse accessoryParaResponse =
                    new AccessoryParaResponse(bidibMessage.getAddr(), getNextSendNum(),
                        ByteUtils.getLowByte(accessoryNumber), ByteUtils.getLowByte(paraNumber), value);
                response = accessoryParaResponse.getContent();
            }

        }
        catch (ProtocolException ex) {
            LOGGER.warn("Create AccessoryPara response failed.", ex);
        }
        return response;
    }

}
