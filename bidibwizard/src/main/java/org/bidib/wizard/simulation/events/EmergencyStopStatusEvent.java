package org.bidib.wizard.simulation.events;

import org.apache.commons.lang.builder.ToStringBuilder;

public class EmergencyStopStatusEvent {

    private final String nodeAddr;

    private final boolean emergencyStop;

    public EmergencyStopStatusEvent(String nodeAddr, boolean emergencyStop) {
        this.nodeAddr = nodeAddr;
        this.emergencyStop = emergencyStop;
    }

    public String getNodeAddr() {
        return nodeAddr;
    }

    public boolean getEmergencyStop() {
        return emergencyStop;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
