package org.bidib.wizard.simulation.events;

import org.bidib.wizard.comm.InputPortStatus;

public class InputPortSetStatusEvent {

    private final String nodeAddr;

    private final int portNum;

    private final InputPortStatus status;

    public InputPortSetStatusEvent(String nodeAddr, int portNum, InputPortStatus status) {
        this.nodeAddr = nodeAddr;
        this.portNum = portNum;
        this.status = status;
    }

    public String getNodeAddr() {
        return nodeAddr;
    }

    public int getPortNum() {
        return portNum;
    }

    public InputPortStatus getStatus() {
        return status;
    }
}
