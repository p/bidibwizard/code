package org.bidib.wizard.simulation.events;

import org.bidib.wizard.mvc.main.model.ServoPort;

public class ServoPortStatusEvent {

    private final String nodeAddr;

    private final ServoPort port;

    private final int value;

    public ServoPortStatusEvent(String nodeAddr, ServoPort port, int value) {
        this.nodeAddr = nodeAddr;
        this.port = port;
        this.value = value;
    }

    public String getNodeAddr() {
        return nodeAddr;
    }

    public ServoPort getPort() {
        return port;
    }

    public int getValue() {
        return value;
    }
}
