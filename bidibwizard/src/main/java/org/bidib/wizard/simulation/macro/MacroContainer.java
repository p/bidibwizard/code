package org.bidib.wizard.simulation.macro;

import java.util.HashMap;
import java.util.Map;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.LcMacro;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.wizard.mvc.main.model.MacroUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MacroContainer {
    private static final Logger LOGGER = LoggerFactory.getLogger(MacroContainer.class);

    private int macroNumber;

    private Map<Integer, LcMacro> macroSteps = new HashMap<Integer, LcMacro>();

    private Map<Integer, byte[]> macroParameters = new HashMap<Integer, byte[]>();

    public MacroContainer(int macroNumber) {
        this.setMacroNumber(macroNumber);
        LOGGER.info("Create new MacroContainer for macroNumber: {}", macroNumber);

        // add LcOutputType.END_OF_MACRO as first step
        byte stepNumber = 0;
        LcMacro lcMacro =
            new LcMacro(ByteUtils.getLowByte(macroNumber), stepNumber, ByteUtils.getLowByte(255),
                LcOutputType.END_OF_MACRO.getType(), (byte) 0, (byte) 0);
        macroSteps.put(0, lcMacro);

        macroParameters.put(BidibLibrary.BIDIB_MACRO_PARA_START_CLK, MacroUtils.MACRO_START_OFF);
        macroParameters.put(BidibLibrary.BIDIB_MACRO_PARA_SLOWDOWN, new byte[] { 1 });
        macroParameters.put(BidibLibrary.BIDIB_MACRO_PARA_REPEAT, new byte[] { 1 });
    }

    /**
     * @return the macroNumber
     */
    public int getMacroNumber() {
        return macroNumber;
    }

    /**
     * @param macroNumber
     *            the macroNumber to set
     */
    public void setMacroNumber(int macroNumber) {
        this.macroNumber = macroNumber;
    }

    /**
     * @return the macro steps
     */
    public Map<Integer, LcMacro> getMacroSteps() {
        return macroSteps;
    }

    /**
     * @param macroSteps
     *            the macro steps to set
     */
    public void setMacroSteps(Map<Integer, LcMacro> macroSteps) {
        this.macroSteps = macroSteps;
    }

    /**
     * Get the macro step.
     * 
     * @param stepNumber
     *            the step number
     * @return the macro step
     */
    public LcMacro getMacroStep(Integer stepNumber) {
        return macroSteps.get(stepNumber);
    }

    /**
     * Set the macro step.
     * 
     * @param stepNumber
     *            the step number
     * @param macroStep
     *            the macro step
     */
    public void setMacroStep(Integer stepNumber, LcMacro macroStep) {
        macroSteps.put(stepNumber, macroStep);
    }

    /**
     * @return the macroParameters
     */
    public Map<Integer, byte[]> getMacroParameters() {
        return macroParameters;
    }

    /**
     * @param macroParameters
     *            the macroParameters to set
     */
    public void setMacroParameters(Map<Integer, byte[]> macroParameters) {
        this.macroParameters = macroParameters;
    }

    /**
     * Get a single macro parameter.
     * 
     * @param paramId
     *            the parameter id
     * @return the parameter value
     */
    public byte[] getMacroParameter(Integer paramId) {
        byte[] parameter = macroParameters.get(paramId);
        LOGGER.info("Get the macro parameterId: {}, params: {}", paramId, ByteUtils.bytesToHex(parameter));
        return parameter;
    }

    /**
     * Set a single macro parameter.
     * 
     * @param paramId
     *            the parameter id
     * @param parameter
     *            the parameter value
     */
    public void setMacroParameter(Integer paramId, byte[] parameter) {
        LOGGER.info("Set the macro parameterId: {}, params: {}", paramId, ByteUtils.bytesToHex(parameter));
        macroParameters.put(paramId, parameter);
    }
}
