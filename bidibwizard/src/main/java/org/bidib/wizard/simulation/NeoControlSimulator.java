package org.bidib.wizard.simulation;

import org.bidib.jbidibc.simulation.net.SimulationBidibMessageProcessor;
import org.bidib.jbidibc.simulation.nodes.LightPortType;

public class NeoControlSimulator extends LightControlSimulator {

    public NeoControlSimulator(byte[] nodeAddress, long uniqueId, boolean autoAddFeature,
        SimulationBidibMessageProcessor messageReceiver) {
        super(nodeAddress, uniqueId, autoAddFeature, messageReceiver);

    }

    protected void prepareLightPortCvValuesFromConfig(final LightPortType portType) {
        // do nothing here
    }

}
