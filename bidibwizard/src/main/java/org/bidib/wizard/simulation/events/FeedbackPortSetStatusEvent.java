package org.bidib.wizard.simulation.events;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.bidib.wizard.comm.FeedbackPortStatus;

public class FeedbackPortSetStatusEvent {

    private final String nodeAddr;

    private final int portNum;

    private final FeedbackPortStatus status;

    public FeedbackPortSetStatusEvent(String nodeAddr, int portNum, FeedbackPortStatus status) {
        this.nodeAddr = nodeAddr;
        this.portNum = portNum;
        this.status = status;
    }

    public String getNodeAddr() {
        return nodeAddr;
    }

    public int getPortNum() {
        return portNum;
    }

    public FeedbackPortStatus getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
