package org.bidib.wizard.simulation.events;

import org.bidib.wizard.comm.BacklightPortStatus;
import org.bidib.wizard.mvc.main.model.BacklightPort;

public class BacklightPortStatusEvent {

    private final String nodeAddr;

    private final BacklightPort port;

    private final BacklightPortStatus status;

    public BacklightPortStatusEvent(String nodeAddr, BacklightPort port, BacklightPortStatus status) {
        this.nodeAddr = nodeAddr;
        this.port = port;
        this.status = status;
    }

    public String getNodeAddr() {
        return nodeAddr;
    }

    public BacklightPort getPort() {
        return port;
    }

    public BacklightPortStatus getStatus() {
        return status;
    }
}
