package org.bidib.wizard.simulation.events;

import org.bidib.wizard.comm.SoundPortStatus;
import org.bidib.wizard.mvc.main.model.SoundPort;

public class SoundPortStatusEvent {

    private final String nodeAddr;

    private final SoundPort port;

    private final SoundPortStatus status;

    public SoundPortStatusEvent(String nodeAddr, SoundPort port, SoundPortStatus status) {
        this.nodeAddr = nodeAddr;
        this.port = port;
        this.status = status;
    }

    public String getNodeAddr() {
        return nodeAddr;
    }

    public SoundPort getPort() {
        return port;
    }

    public SoundPortStatus getStatus() {
        return status;
    }
}
