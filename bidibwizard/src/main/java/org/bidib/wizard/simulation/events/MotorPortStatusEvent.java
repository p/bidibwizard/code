package org.bidib.wizard.simulation.events;

import org.bidib.wizard.mvc.main.model.MotorPort;

public class MotorPortStatusEvent {

    private final String nodeAddr;

    private final MotorPort port;

    private final int value;

    public MotorPortStatusEvent(String nodeAddr, MotorPort port, int value) {
        this.nodeAddr = nodeAddr;
        this.port = port;
        this.value = value;
    }

    public String getNodeAddr() {
        return nodeAddr;
    }

    public MotorPort getPort() {
        return port;
    }

    public int getValue() {
        return value;
    }
}
