package org.bidib.wizard.simulation.events;

import org.apache.commons.lang.builder.ToStringBuilder;

public class FeedbackConfidenceStatusEvent {

    private final String nodeAddr;

    private final boolean valid;

    private final boolean freeze;

    private final boolean signal;

    public FeedbackConfidenceStatusEvent(String nodeAddr, boolean valid, boolean freeze, boolean signal) {
        this.nodeAddr = nodeAddr;
        this.valid = valid;
        this.freeze = freeze;
        this.signal = signal;
    }

    public String getNodeAddr() {
        return nodeAddr;
    }

    public boolean getValid() {
        return valid;
    }

    public boolean getFreeze() {
        return freeze;
    }

    public boolean getSignal() {
        return signal;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
