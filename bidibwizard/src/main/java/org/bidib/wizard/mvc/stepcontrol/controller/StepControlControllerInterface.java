package org.bidib.wizard.mvc.stepcontrol.controller;

import org.bidib.wizard.comm.SoundPortStatus;
import org.bidib.wizard.mvc.main.model.MotorPort;
import org.bidib.wizard.mvc.stepcontrol.model.StepControlAspect;

import com.jgoodies.common.collect.ArrayListModel;

public interface StepControlControllerInterface {
    /**
     * Trigger load the CV values of the step control.
     */
    void triggerLoadCvValues();

    ArrayListModel<StepControlAspect> getConfigureAspectsListModel();

    /**
     * Set the new value of the motor port.
     * 
     * @param motorPort
     *            the motor port
     */
    void setMotorPortValue(final MotorPort motorPort);

    /**
     * @param portId
     *            the sound port to trigger
     * @param soundPortStatus
     *            the sound port status
     */
    void triggerSoundPort(final int portId, final SoundPortStatus soundPortStatus);
}
