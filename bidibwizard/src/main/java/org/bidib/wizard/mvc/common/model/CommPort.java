package org.bidib.wizard.mvc.common.model;

public class CommPort {

    public static final String NONE = "<none>";

    private String name;

    public CommPort(String name) {
        this.name = name;
    }

    /**
     * @return the name of the commPort
     */
    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (o != null && o instanceof CommPort) {
            return toString().equals(o.toString());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    public String toString() {
        if (name != null) {
            return name;
        }
        return NONE;
    }
}
