package org.bidib.wizard.mvc.simulation.model;

import java.util.HashMap;
import java.util.Map;

import org.bidib.jbidibc.simulation.SimulatorNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimulationModel {
    private static final Logger LOGGER = LoggerFactory.getLogger(SimulationModel.class);

    private Map<String, SimulatorNode> simulatorMap = new HashMap<String, SimulatorNode>();

    public SimulationModel() {

    }

    public void addSimulator(String uuid, SimulatorNode simulator) {
        LOGGER.info("Add new simulator with uuid: {}, simulator: {}", uuid, simulator);
        if (simulatorMap.containsKey(uuid)) {
            LOGGER.warn("Simulator is already registered, uuid: {}", uuid);

            return;
        }
        simulatorMap.put(uuid, simulator);
    }
}
