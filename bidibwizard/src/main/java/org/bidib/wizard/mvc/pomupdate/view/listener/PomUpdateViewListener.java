package org.bidib.wizard.mvc.pomupdate.view.listener;

import java.util.List;
import java.util.Map;

import org.bidib.jbidibc.core.message.CommandStationPomMessage;
import org.bidib.wizard.mvc.pomupdate.model.Decoder;

public interface PomUpdateViewListener {
    /**
     * Close the dialog.
     */
    void close();

    /**
     * @param prepareUpdateMap
     *            the map with all decoders to prepare for pom update and the related POM messages
     */
    void prepareUpdate(
        final Map<Decoder, List<CommandStationPomMessage>> prepareUpdateMap,
        final PomUpdateStatusListener statusListener);

    /**
     * @param updateMessages
     *            the list with the update POM messages
     * @param statusListener
     *            the status listener
     */
    void performUpdate(
        final List<CommandStationPomMessage> updateMessages, final PomUpdatePerformStatusListener statusListener);

    /**
     * @param decodersToLoadInfo
     *            the list of decoders to get the info
     * @param statusListener
     *            the status listener
     */
    void performLoadDecoderInfo(final List<Decoder> decodersToLoadInfo, final DecoderInfoStatusListener statusListener);

}
