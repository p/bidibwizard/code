package org.bidib.wizard.mvc.error.model.listener;

public interface ErrorListener {
    void errorChanged(String errorMessage, String stackTrace);
}
