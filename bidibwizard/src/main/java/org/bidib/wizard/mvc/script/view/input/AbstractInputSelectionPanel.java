package org.bidib.wizard.mvc.script.view.input;

import javax.swing.JComponent;

import org.bidib.wizard.common.context.ApplicationContext;

import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.validation.ValidationResult;

public abstract class AbstractInputSelectionPanel {

    protected JComponent content;

    protected String variableName;

    protected String defaultValue;

    public abstract void initialize(
        final ApplicationContext context, final DefaultFormBuilder dialogBuilder, ValidationSupport validationSupport,
        String variableName, String variableType, String caption, String defaultValue, String prevValue);

    public JComponent getComponent() {
        return content;
    }

    public void validate(final ValidationResult validationResult) {
    }

    public abstract Object getSelectedValue();

    protected void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getDefaultValue() {
        return defaultValue;
    }
}
