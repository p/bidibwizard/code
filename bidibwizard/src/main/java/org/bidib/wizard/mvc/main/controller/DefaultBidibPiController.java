package org.bidib.wizard.mvc.main.controller;

import org.bidib.jbidib.pi.BiDiBPiConnector;
import org.bidib.wizard.common.context.ApplicationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultBidibPiController implements BidibPiController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultBidibPiController.class);

    private BiDiBPiConnector connector;

    public void start(final ApplicationContext applicationContext) {
        LOGGER.info("Start the DefaultBidibPiController.");

        try {
            BiDiBPiConnector.checkPlatform();

            connector = new BiDiBPiConnector();
            connector.connect();

            // register
            applicationContext.register(ApplicationContext.KEY_BIDIB_PI_CONTROLLER, this);

            // LOGGER.info("Reset the BiDiB Pi.");
            // connector.resetBidibPi();
        }
        catch (Exception ex) {
            LOGGER.warn("Create and initialize the connector to the Pi failed.", ex);

            if (connector != null) {
                LOGGER.info("Disconnect and free the pi connector.");
                connector.disconnect();
                connector = null;
            }

            throw new IllegalArgumentException("Init the pi connector failed.");
        }
        catch (Error ex) {
            LOGGER.warn("Create and initialize the connector to the Pi failed.", ex);

            if (connector != null) {
                LOGGER.info("Disconnect and free the pi connector.");
                connector.disconnect();
                connector = null;
            }

            throw new IllegalArgumentException("Init the pi connector failed.");
        }
    }

    @Override
    public void shutdown() {

        try {
            if (connector != null) {
                connector.disconnect();
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Disconnect the connector to the Pi failed.", ex);
        }

    }

    @Override
    public void reset() {
        try {
            if (connector != null) {
                connector.resetBidibPi();
            }
            else {
                LOGGER.warn("No BiDiB-Pi to reset connected.");
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Reset the BiDiB-Pi failed.", ex);
        }

    }
}
