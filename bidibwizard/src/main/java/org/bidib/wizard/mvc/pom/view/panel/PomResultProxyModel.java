package org.bidib.wizard.mvc.pom.view.panel;

import org.bidib.jbidibc.core.enumeration.PomProgState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.beans.Model;

public class PomResultProxyModel extends Model {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(PomResultProxyModel.class);

    public static final String PROPERTYNAME_POMPROGSTATE = "pomProgState";

    public static final String PROPERTYNAME_CVNUMBER = "cvNumber";

    public static final String PROPERTYNAME_CVVALUE = "cvValue";

    private int cvNumber = 1;

    private Integer cvValue;

    private boolean isAllowUpdate;

    private PomProgState pomProgState;

    public void setAllowUpdate(boolean allowUpdate) {
        this.isAllowUpdate = allowUpdate;
    }

    public boolean isAllowUpdate() {
        return isAllowUpdate;
    }

    /**
     * @return the CV number
     */
    public int getCvNumber() {
        return cvNumber;
    }

    /**
     * @param cvNumber
     *            the CV number to set
     */
    public void setCvNumber(int cvNumber) {
        if (!isAllowUpdate()) {
            return;
        }

        int oldNumber = this.cvNumber;
        this.cvNumber = cvNumber;
        firePropertyChange(PROPERTYNAME_CVNUMBER, oldNumber, cvNumber);
    }

    public Integer getCvValue() {
        return cvValue;
    }

    public void setCvValue(Integer value) {
        if (!isAllowUpdate()) {
            return;
        }

        LOGGER.debug("Set the CV value: {}", value);
        Integer oldValue = cvValue;
        this.cvValue = value;
        firePropertyChange(PROPERTYNAME_CVVALUE, oldValue, cvValue);
    }

    public void clearCvValue() {
        if (!isAllowUpdate()) {
            return;
        }

        LOGGER.debug("Clear the CV value.");
        Integer oldValue = cvValue;
        cvValue = null;
        firePropertyChange(PROPERTYNAME_CVVALUE, oldValue, cvValue);
    }

    /**
     * @return the pomProgState
     */
    public PomProgState getPomProgState() {
        return pomProgState;
    }

    /**
     * @param pomProgState
     *            the pomProgState to set
     */
    public void setPomProgState(PomProgState pomProgState) {
        if (!isAllowUpdate()) {
            return;
        }

        LOGGER.info("Set the new POM prog state: {}", pomProgState);
        PomProgState oldPomProgState = this.pomProgState;

        this.pomProgState = pomProgState;

        firePropertyChange(PROPERTYNAME_POMPROGSTATE, oldPomProgState, pomProgState);
    }
}
