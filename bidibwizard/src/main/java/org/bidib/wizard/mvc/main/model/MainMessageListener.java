package org.bidib.wizard.mvc.main.model;

import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.ref.WeakReference;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import javax.swing.SwingUtilities;

import org.apache.commons.collections4.CollectionUtils;
import org.bidib.jbidibc.core.AccessoryState;
import org.bidib.jbidibc.core.AccessoryStateOptions;
import org.bidib.jbidibc.core.AddressData;
import org.bidib.jbidibc.core.BidibPort;
import org.bidib.jbidibc.core.DecoderIdAddressData;
import org.bidib.jbidibc.core.DecoderUniqueIdData;
import org.bidib.jbidibc.core.DriveState;
import org.bidib.jbidibc.core.LcConfig;
import org.bidib.jbidibc.core.LcConfigX;
import org.bidib.jbidibc.core.MessageListener;
import org.bidib.jbidibc.core.RcPlusDecoderAnswerData;
import org.bidib.jbidibc.core.RcPlusFeedbackBindData;
import org.bidib.jbidibc.core.TidData;
import org.bidib.jbidibc.core.enumeration.AccessoryAcknowledge;
import org.bidib.jbidibc.core.enumeration.ActivateCoilEnum;
import org.bidib.jbidibc.core.enumeration.BoosterState;
import org.bidib.jbidibc.core.enumeration.CommandStationProgState;
import org.bidib.jbidibc.core.enumeration.CommandStationState;
import org.bidib.jbidibc.core.enumeration.ErrorCodeEnum;
import org.bidib.jbidibc.core.enumeration.IdentifyState;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.PortConfigStatus;
import org.bidib.jbidibc.core.enumeration.PortModelEnum;
import org.bidib.jbidibc.core.enumeration.RcPlusAcknowledge;
import org.bidib.jbidibc.core.enumeration.RcPlusDecoderType;
import org.bidib.jbidibc.core.enumeration.RcPlusPhase;
import org.bidib.jbidibc.core.enumeration.SysErrorEnum;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.core.utils.NodeUtils;
import org.bidib.wizard.comm.BoosterStatus;
import org.bidib.wizard.comm.FeedbackPortStatus;
import org.bidib.wizard.mvc.console.controller.ConsoleController;
import org.bidib.wizard.mvc.console.model.ConsoleModel;
import org.bidib.wizard.utils.PortListUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The MainMessageListener is the message listener for the MainModel instance.
 */
public class MainMessageListener implements MessageListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(MainMessageListener.class);

    private static final Logger LOGGER_DYN_STATE = LoggerFactory.getLogger("DYNSTATE");

    private MainModel model;

    public MainMessageListener(MainModel model) {
        this.model = model;
    }

    private Node findNode(byte[] address) {
        Node result = null;

        if (address != null) {
            int addressValue = NodeUtils.convertAddress(address);

            for (Node node : model.getNodes()) {
                if (addressValue == NodeUtils.convertAddress(node.getNode().getAddr())) {
                    result = node;
                    break;
                }
            }
        }
        return result;
    }

    @Override
    public void boosterDiag(byte[] address, int current, int voltage, int temperature) {
        Node node = findNode(address);

        if (model.getSelectedNode() != null && model.getSelectedNode().equals(node)) {
            LOGGER.trace("boosterDiag, address: {}, current: {}mA, voltage: {}, temperature: {}", address, current,
                voltage, temperature);

            model.setBoosterCurrent(current);
            model.setBoosterVoltage(voltage);
            model.setBoosterTemperature(temperature);
        }
    }

    @Override
    public void boosterState(byte[] address, BoosterState boosterState) {
        Node node = findNode(address);

        if (model.getSelectedNode() != null && model.getSelectedNode().equals(node)) {
            LOGGER.trace("Booster state has changed, address: {}, state: {}", address, boosterState);
            model.setBoosterStatus(BoosterStatus.valueOf(boosterState));
        }
    }

    @Override
    public void identity(byte[] address, IdentifyState identifyState) {
        Node node = findNode(address);

        // the MainMenuBar is only added to the selected node, so it will/should not receive changes of other nodes
        model.setIdentifyState(node, identifyState);
    }

    @Override
    public void pong(byte[] address, int marker) {
        // TODO implement
    };

    @Override
    public void key(byte[] address, int keyNumber, int keyState) {
        Node node = findNode(address);

        // only signal the status change of the selected node
        Node selectedNode = model.getSelectedNode();
        if (selectedNode != null && selectedNode.equals(node)) {
            model.setInputPortStatus(keyNumber, keyState);
        }
        else {
            LOGGER.debug("Node not found or node is not the selected node.");
        }
    }

    @Override
    public void confidence(final byte[] address, final int valid, final int freeze, final int signal) {
        final Node node = findNode(address);
        LOGGER.info("Confidence has changed on node: {}, valid: {}, freeze: {}, signal: {}", node, valid, freeze,
            signal);

        // if the dcc signal is not available the following values are received for 32 BM: 00 02 03
        // this is evaluated to:
        // 00 -> VOID = 0
        // 02 -> FREEZE = 0000 0010 <- the 2nd detector is freeze, ok: no external power
        // 03 -> SIGNAL = 0000 0011 <- the 1st and 2nd detector have an invalid signal but occupancy detection is
        // possible

        // only signal the status change of the selected node
        final Node selectedNode = model.getSelectedNode();
        if (selectedNode != null && selectedNode.equals(node)) {

            // TODO this relies on the assumption that always 16 ports are on one detector
            final List<FeedbackPort> ports = node.getFeedbackPorts();
            int totalPorts = ports.size();
            int detectors = totalPorts / 16;

            LOGGER.info("Change confidence on feedback ports: {}, num of detectors: {}", totalPorts, detectors);

            // iterate over the ports
            for (int portNumber = 0; portNumber < totalPorts; portNumber++) {
                int detector = portNumber / 16;
                node.setFeedbackPortConfidence(portNumber, (freeze >> detector) == 1, (signal >> detector) == 0,
                    (valid >> detector) == 0);
            }
        }
        else {
            LOGGER.debug("Node not found or node is not the selected node.");
        }
    }

    @Override
    public void free(final byte[] address, final int detectorNumber, final Long timestamp) {
        LOGGER.debug("free, address: {}, detectorNumber: {}, timestamp: {}", address, detectorNumber, timestamp);

        // TODO remove this test output
        if (timestamp != null) {
            LOGGER.info("free, address: {}, detectorNumber: {}, timestamp: {}", address, detectorNumber, timestamp);
        }

        final Node node = findNode(address);

        // only signal the status change of the selected node
        Node selectedNode = model.getSelectedNode();
        if (selectedNode != null && selectedNode.equals(node)) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    node.setFeedbackPortStatus(detectorNumber, FeedbackPortStatus.FREE, null);
                }
            });
        }
        else {
            LOGGER.debug("Node not found or node is not the selected node.");
        }
    }

    @Override
    public void occupied(final byte[] address, final int detectorNumber, final Long timestamp) {
        LOGGER.debug("occupied, address: {}, detectorNumber: {}, timestamp: {}", address, detectorNumber, timestamp);

        // TODO remove this test output
        if (timestamp != null) {
            LOGGER.info("occupied, address: {}, detectorNumber: {}, timestamp: {}", address, detectorNumber, timestamp);
        }

        final Node node = findNode(address);

        // only signal the status change of the selected node
        Node selectedNode = model.getSelectedNode();
        if (selectedNode != null && selectedNode.equals(node)) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    node.setFeedbackPortStatus(detectorNumber, FeedbackPortStatus.OCCUPIED, timestamp);
                }
            });
        }
        else {
            LOGGER.debug("Node not found or node is not the selected node.");
        }
    }

    @Override
    public void position(byte[] address, int decoderAddress, int locationType, int locationAddress) {
        LOGGER.info("position, address: {}, decoderAddress: {}, locationType: {}, locationAddress: {}", address,
            decoderAddress, locationType, locationAddress);

        // find the node for this position message
        final Node node = findNode(address);

        // only signal the status change of the selected node
        Node selectedNode = model.getSelectedNode();
        if (selectedNode != null && selectedNode.equals(node)) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    node.setFeedbackPosition(decoderAddress, locationType, locationAddress);
                }
            });
        }
        else {
            LOGGER.debug("Node not found or node is not the selected node.");
        }
    }

    @Override
    public void address(
        byte[] address, final int detectorNumber, final List<org.bidib.jbidibc.core.AddressData> addressDatas) {
        LOGGER.debug("address message, address: {}, detectorNumber: {}, addressDatas: {}", address, detectorNumber,
            addressDatas);
        final Node node = findNode(address);

        if (model.getSelectedNode() != null && model.getSelectedNode().equals(node)) {

            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    List<FeedbackAddressData> addresses = new LinkedList<FeedbackAddressData>();

                    for (org.bidib.jbidibc.core.AddressData addressData : addressDatas) {
                        addresses.add(new FeedbackAddressData(addressData.getAddress(), addressData.getType()));
                    }
                    node.setFeedbackPortAddresses(detectorNumber, addresses);
                }
            });
        }
    }

    @Override
    public void speed(final byte[] address, final AddressData addressData, final int speed) {
        final Node node = findNode(address);

        // only signal the status change of the selected node
        final Node selectedNode = model.getSelectedNode();
        if (selectedNode != null && selectedNode.equals(node)) {

            SwingUtilities.invokeLater(new Runnable() {
                public void run() {

                    final List<FeedbackPort> ports = node.getFeedbackPorts();
                    // iterate over all detectors of the current node and set the speed value (if a matching address is
                    // found)

                    for (int detectorNumber = 0; detectorNumber < ports.size(); detectorNumber++) {
                        node.setFeedbackPortSpeed(detectorNumber, addressData.getAddress(), speed);
                    }
                }
            });

        }
        else {
            LOGGER.debug("Node not found or node is not the selected node.");
        }
    }

    @Override
    public void dynState(
        byte[] address, final int detectorNumber, final AddressData decoderAddress, final int dynNumber,
        final int dynValue) {
        LOGGER.debug(
            "Dynamic state is signaled from feedback device, address: {} , detectorNumber: {}, decoderAddress: {}, dynNumber: {}, dynValue: {}",
            address, detectorNumber, decoderAddress, dynNumber, dynValue);

        LOGGER_DYN_STATE.info("addr: {}, detector: {}, decoderAddress: {}, number: {}, value: {}", address,
            detectorNumber, decoderAddress, dynNumber, dynValue);

        final Node node = findNode(address);

        if (model.getSelectedNode() != null && model.getSelectedNode().equals(node)) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {

                    List<FeedbackDynStateData> dynStates = new LinkedList<FeedbackDynStateData>();
                    dynStates.add(new FeedbackDynStateData(decoderAddress.getAddress(), dynNumber, dynValue));
                    node.setFeedbackPortDynStates(detectorNumber, dynStates);
                }
            });
        }
    }

    @Override
    public void error(byte[] address, int errorCode, byte[] reasonData) {
        SysErrorEnum sysError = SysErrorEnum.valueOf(ByteUtils.getLowByte(errorCode));
        LOGGER.warn("An error was detected for address: {}, sysError: {}, reasonData: {}", address, sysError,
            reasonData);

        Node node = findNode(address);

        if (node != null) {
            // the MainMenuBar is only added to the selected node, so it will/should not receive changes of other nodes
            model.setErrorState(node, sysError, reasonData);
        }
        else {
            LOGGER.error("No node found for address: {}", new Object[] { address });
        }
    }

    @Override
    public void stall(byte[] address, boolean stall) {
        LOGGER.warn("The node has sent the stall message for address: {}, stall: {}", address, stall);
        Node node = findNode(address);

        if (node != null) {
            node.setNodeIsStall(stall);
        }
        else {
            LOGGER.error("No node found for address: {}", new Object[] { address });
        }
    }

    @Override
    public void accessoryState(
        byte[] address, final AccessoryState accessoryState, final AccessoryStateOptions accessoryStateOptions) {
        // forward the accessory state
        LOGGER.info("Accessory state is signaled, address: {}, accessoryState: {}, accessoryStateOptions: {}", address,
            accessoryState, accessoryStateOptions);

        final Node node = findNode(address);

        // only signal the status change of the selected node
        Node selectedNode = model.getSelectedNode();
        if (selectedNode != null && selectedNode.equals(node)) {
            int accessoryNumber = ByteUtils.getInt(accessoryState.getAccessoryNumber());
            // the current node is the selected node
            for (Accessory accessory : model.getAccessories()) {
                if (accessory.getId() == accessoryNumber) {
                    LOGGER.info("The execution state of the accessory has changed: {}", accessoryState);

                    accessory.setAccessoryState(accessoryState, accessoryStateOptions);
                    break;
                }
            }
        }

        if (accessoryState.hasError()) {
            LOGGER.warn("The node has signalled an error: {}, node: {}", accessoryState, node);

            // notify the console
            SwingUtilities.invokeLater(new Runnable() {

                @Override
                public void run() {
                    // ensure console is visible
                    ConsoleController.ensureConsoleVisible();

                    // add line
                    ConsoleModel.getConsoleModel().addConsoleLine(Color.red, String
                        .format("The node %s has signalled an accessory error: %s", node, accessoryState.toString()));
                }
            });
        }
    }

    @Override
    public void lcStat(byte[] address, BidibPort bidibPort, int portStatus) {
        LOGGER.info("LC state is signaled, address: {}, bidibPort: {}, portStatus: {}", address, bidibPort, portStatus);

        Node node = findNode(address);

        PortModelEnum portModel = PortModelEnum.getPortModel(node.getNode());
        int portNumber = bidibPort.getPortNumber(portModel);

        if (model.getSelectedNode() != null && model.getSelectedNode().equals(node)) {

            if (bidibPort.getHighValue() == ByteUtils.getLowByte(0xFF)
                && bidibPort.getLowValue() == ByteUtils.getLowByte(0xFF)) {
                LOGGER.info("End of port status delivered.");
                return;
            }

            LcOutputType portType = null;
            if (portModel == PortModelEnum.type) {
                // type port model
                portType = bidibPort.getPortType(portModel);
            }
            else {
                // flat port model
                try {
                    // TODO must handle the new End-of-ports message with port number 255
                    GenericPort port = node.getGenericPorts().get(portNumber);
                    if (port != null) {
                        portType = port.getCurrentPortType();
                    }
                }
                catch (IndexOutOfBoundsException ex) {
                    LOGGER.error("Get port type failed for port number: {}", portNumber, ex);
                }
            }
            if (portType != null) {
                switch (portType) {
                    case SERVOPORT:
                        model.setServoPortValue(portNumber, portStatus);
                        break;
                    case MOTORPORT:
                        if (portStatus == 0) {
                            LOGGER.info("No speed: {}", portStatus);
                        }
                        else if ((portStatus & 0x80) == 0x00) {
                            portStatus = -(portStatus) + 1;
                            LOGGER.info("Negative speed: {}", portStatus);
                        }
                        else {
                            portStatus = (portStatus & 0x7F) - 1;
                            LOGGER.info("Positive speed: {}", portStatus);
                        }
                        LOGGER.info("received motor speed: {}", portStatus);
                        // TODO enable set the motor port value
                        // model.setMotorPortValue(portNumber, portStatus);
                        break;
                    case BACKLIGHTPORT:
                        model.setBacklightPortValue(portNumber, portStatus);
                        break;
                    case SWITCHPORT:
                        model.setSwitchPortStatus(portNumber, portStatus);
                        break;
                    case SWITCHPAIRPORT:
                        model.setSwitchPairPortStatus(portNumber, portStatus);
                        break;
                    case SOUNDPORT:
                        model.setSoundPortStatus(portNumber, portStatus);
                        break;
                    case LIGHTPORT:
                        model.setLightPortStatus(portNumber, portStatus);
                        break;
                    case INPUTPORT:
                        model.setInputPortStatus(portNumber, portStatus);
                        break;
                    // add support for more port types
                    default:
                        break;
                }
            }
        }
        else {
            LOGGER.info("LcStat message is not for the selected node.");
        }
    }

    @Override
    public void lcWait(byte[] address, BidibPort bidibPort, int predRotationTime) {
        LOGGER.info("LC wait is signaled, address: {}, bidibPort: {}, predRotationTime: {}", address, bidibPort,
            predRotationTime);

    }

    @Override
    public void lcNa(byte[] address, BidibPort bidibPort, Integer errorCode) {
        LOGGER.info("LC NA is signaled, address: {}, bidibPort: {}, errorCode: {}", address, bidibPort,
            ErrorCodeEnum.formatErrorCode(errorCode));

        Node node = findNode(address);

        if (node == null) {
            LOGGER.info("No node to process LcNa message is not available.");
            return;
        }

        if (bidibPort.getHighValue() == ByteUtils.getLowByte(0xFF)
            && bidibPort.getLowValue() == ByteUtils.getLowByte(0xFF)) {
            LOGGER.info("End of port status delivered.");

            node.signalInitialLoadFinished();

            return;
        }

        PortModelEnum portModel = PortModelEnum.getPortModel(node.getNode());
        int portNumber = bidibPort.getPortNumber(portModel);

        if (node.getNode().isPortFlatModelAvailable()) {
            LOGGER.info("The current node supports the flat port model, update the generic port.");

            try {
                LOGGER.info("Set the port config for port id: {}", portNumber);

                GenericPort port = node.getGenericPorts().get(portNumber);
                port.setConfigStatus(PortConfigStatus.CONFIG_NA);
                port.setInactive(true);
            }
            catch (Exception ex) {
                LOGGER.warn("Set config status to CONFIG_NA on generic port failed, portNumber: {}", portNumber, ex);
            }
        }
        else {
            // the node supports the port-type oriented model

            if (model.getSelectedNode() != null && model.getSelectedNode().equals(node)) {
                LcOutputType portType = bidibPort.getPortType(portModel);

                LOGGER.info("Disable port, porttype: {}, portNumber: {}", portType, portNumber);

                List<? extends Port<?>> ports = null;
                switch (portType) {
                    case SERVOPORT:
                        ports = model.getServoPorts();
                        break;
                    case SWITCHPORT:
                        ports = model.getSwitchPorts();
                        break;
                    case SWITCHPAIRPORT:
                        ports = model.getSwitchPairPorts();
                        break;
                    case INPUTPORT:
                        ports = model.getInputPorts();
                        break;
                    case LIGHTPORT:
                        ports = model.getLightPorts();
                        break;
                    case BACKLIGHTPORT:
                        ports = model.getBacklightPorts();
                        break;
                    case MOTORPORT:
                        ports = model.getMotorPorts();
                        break;
                    case SOUNDPORT:
                        ports = model.getSoundPorts();
                        break;
                    case ANALOGPORT:
                        ports = model.getAnalogPorts();
                        break;
                    default:
                        break;
                }

                if (CollectionUtils.isNotEmpty(ports)) {
                    try {
                        LOGGER.info("Set the port config for port id: {}", portNumber);
                        Port<?> port = ports.get(portNumber);
                        port.setConfigStatus(PortConfigStatus.CONFIG_NA);
                        port.setInactive(true);
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Set config status to CONFIG_NA on generic port failed, portNumber: {}", portNumber,
                            ex);
                    }
                }
                else {
                    LOGGER.warn("No ports available of type: {}", portType);
                }
            }
            else {
                LOGGER.info("lcNa message is not for the selected node.");
            }
        }

    }

    @Override
    public void lcConfig(byte[] address, LcConfig lcConfig) {
        LOGGER.info("LC_CONFIG is signaled, address: {}, lcConfig: {}", address, lcConfig);

        Node node = findNode(address);

        // TODO implementation missing
        // the node supports the port-type oriented model
        Node selectedNode = model.getSelectedNode();
        if (selectedNode != null && selectedNode.equals(node)) {
            PortModelEnum portModel = PortModelEnum.getPortModel(node.getNode());
            LcOutputType portType = lcConfig.getOutputType(portModel);
            int portNumber = lcConfig.getOutputNumber(portModel);
            LOGGER.info("LcConfig, porttype: {}, portNumber: {}", portType, portNumber);

            switch (portType) {
                case BACKLIGHTPORT:
                    BacklightPort backlightPort = model.getBacklightPorts().get(portNumber);
                    backlightPort.setPortConfig(lcConfig.getPortConfig());
                    backlightPort.setConfigStatus(PortConfigStatus.CONFIG_PASSED);
                    break;
                case LIGHTPORT:
                    LightPort lightPort = model.getLightPorts().get(portNumber);
                    lightPort.setPortConfig(lcConfig.getPortConfig());
                    lightPort.setConfigStatus(PortConfigStatus.CONFIG_PASSED);
                    break;
                case SERVOPORT:
                    ServoPort servoPort = node.getServoPorts().get(portNumber);
                    servoPort.setPortConfig(lcConfig.getPortConfig());
                    servoPort.setConfigStatus(PortConfigStatus.CONFIG_PASSED);
                    break;
                case SWITCHPORT:
                    SwitchPort switchPort = model.getSwitchPorts().get(portNumber);
                    switchPort.setPortConfig(lcConfig.getPortConfig());
                    switchPort.setConfigStatus(PortConfigStatus.CONFIG_PASSED);
                    break;
                case MOTORPORT:
                    MotorPort motorPort = node.getMotorPorts().get(portNumber);
                    motorPort.setPortConfig(lcConfig.getPortConfig());
                    motorPort.setConfigStatus(PortConfigStatus.CONFIG_PASSED);
                    break;

                default:
                    break;
            }

        }
    }

    @Override
    public void lcConfigX(byte[] address, LcConfigX lcConfigX) {
        LOGGER.info("LC_CONFIGX is signaled, address: {}, lcConfigX: {}", address, lcConfigX);

        final Node node = findNode(address);
        AtomicBoolean configStatusChanged = new AtomicBoolean();
        AtomicInteger portTypeChanged = new AtomicInteger(-1);
        if (node.getNode().isPortFlatModelAvailable()) {
            LOGGER.info("The current node supports the flat port model, update the generic port.");

            final PropertyChangeListener pcl = new PropertyChangeListener() {

                @Override
                public void propertyChange(PropertyChangeEvent evt) {

                    if (GenericPort.PROPERTY_CONFIG_LOADED.equals(evt.getPropertyName())) {
                        LOGGER.info("The port config status has changed.");

                        configStatusChanged.set(true);
                    }
                    else if (GenericPort.PROPERTY_PORT_TYPE_CHANGED.equals(evt.getPropertyName())) {
                        LOGGER.info("The port type value has changed, evt: {}", evt);

                        // this should only trigger refresh the port
                        configStatusChanged.set(true);

                        if (evt.getSource() instanceof GenericPort) {
                            GenericPort port = (GenericPort) evt.getSource();
                            portTypeChanged.set(port.getPortNumber());

                            // this is a hack but cannot solve it better
                            // if a SwitchPair port was changed we must change the paired switch port to make it
                            // unavailable
                            if (port.isMatchingPortType(LcOutputType.SWITCHPAIRPORT)) {
                                // the port was switched to SWITCHPAIR port and we must dirty the paired port
                                LOGGER.info(
                                    "The current port is a SWITCHPAIR port. Try to find the paired port instance.");
                                try {
                                    GenericPort pairedPort =
                                        PortListUtils.findGenericPortByPortNumber(node.getGenericPorts(),
                                            port.getPortNumber() + 1);
                                    if (pairedPort != null) {
                                        WeakReference<GenericPort> refMaster = new WeakReference<GenericPort>(port);
                                        pairedPort.setPairedPortMaster(refMaster);
                                    }
                                    else {
                                        LOGGER.warn("Paired port instance was not found for port: {}", port);
                                    }
                                }
                                catch (Exception ex) {
                                    LOGGER.warn("Inactivate paired port failed.", ex);
                                }
                            }
                            else if (LcOutputType.SWITCHPAIRPORT.equals(evt.getOldValue())
                                && LcOutputType.SWITCHPORT.equals(evt.getNewValue())) {
                                // port was switched from SWITCHPAIR to SWITCH, release the paired master
                                try {
                                    GenericPort pairedPort =
                                        PortListUtils.findGenericPortByPortNumber(node.getGenericPorts(),
                                            port.getPortNumber() + 1);
                                    if (pairedPort != null) {
                                        pairedPort.setPairedPortMaster(null);
                                    }
                                    else {
                                        LOGGER.warn("Paired port instance to release was not found for port: {}", port);
                                    }
                                }
                                catch (Exception ex) {
                                    LOGGER.warn("Release paired port failed.", ex);
                                }
                            }
                        }
                        else {
                            LOGGER.warn("Event source is not a GenericPort: {}", evt.getSource());
                        }

                    }
                    else if (GenericPort.PROPERTY_PORT_CONFIG_CHANGED.equals(evt.getPropertyName())) {
                        LOGGER.info("The port config value have changed.");

                        // this should only trigger refresh the port

                        if (evt.getSource() instanceof GenericPort) {
                            GenericPort port = (GenericPort) evt.getSource();
                            portTypeChanged.set(port.getPortNumber());
                        }
                        else {
                            LOGGER.warn("Event source is not a GenericPort: {}", evt.getSource());
                        }
                    }

                }
            };

            // ignore the delivered port type
            GenericPort port = null;
            try {
                int portNumber = lcConfigX.getOutputNumber(PortModelEnum.getPortModel(node.getNode()));
                LOGGER.info("Set the port config for port id: {}", portNumber);

                port = node.getGenericPorts().get(portNumber);

                port.addPropertyChangeListener(pcl);

                port.setPortConfig(lcConfigX.getPortConfig());

            }
            catch (Exception ex) {
                LOGGER.warn("Set port config on generic port failed.", ex);
            }
            finally {
                if (port != null) {
                    port.removePropertyChangeListener(pcl);
                }
            }

            // check if all generic ports have been configured and update the list panels
            try {
                boolean configPendingFound = false;
                for (GenericPort currentPort : node.getGenericPorts()) {
                    if (PortConfigStatus.CONFIG_PENDING.equals(currentPort.getConfigStatus())) {
                        // found a port with config pending status
                        LOGGER.info("Found a port with config pending status: {}", currentPort);

                        configPendingFound = true;
                        break;
                    }
                }

                if (!configPendingFound) {
                    if (configStatusChanged.get()) {
                        LOGGER.info(
                            "No config pending status found in generic ports, clear the port cache on the node and update the port lists.");

                        node.clearPortCache();

                        model.updatePortLists();
                    }
                    else if (portTypeChanged.get() > -1) {
                        LOGGER.info(
                            "No config pending status found in generic ports, publish the port configuration change of port: {}",
                            portTypeChanged);

                        model.publishPortConfigChanged(port.getCurrentPortType(), port.getPortNumber());
                    }
                    else {
                        LOGGER.info("The config of a port has changed, but no port type change: {}", port);
                    }
                }
                else {
                    LOGGER.info("Found pending config or config status has not changed, do not update ports list.");
                }
            }
            catch (Exception ex) {
                LOGGER.warn("Check if all generic ports have been configured failed.", ex);
            }

        }
        else {
            // the node supports the port-type oriented model
            Node selectedNode = model.getSelectedNode();
            if (selectedNode != null && selectedNode.equals(node)) {
                PortModelEnum portModel = PortModelEnum.getPortModel(node.getNode());
                LcOutputType portType = lcConfigX.getOutputType(portModel);
                int portNumber = lcConfigX.getOutputNumber(portModel);
                LOGGER.info("LcConfigX, porttype: {}, portNumber: {}", portType, portNumber);

                switch (portType) {
                    case SERVOPORT:
                        selectedNode.setServoPortConfig(portNumber, lcConfigX.getPortConfig());
                        break;
                    case MOTORPORT:
                        selectedNode.setMotorPortConfig(portNumber, lcConfigX.getPortConfig());
                        break;
                    case BACKLIGHTPORT:
                        model.setBacklightPortConfig(portNumber, lcConfigX.getPortConfig());
                        break;
                    case SWITCHPORT:
                        model.setSwitchPortConfig(portNumber, lcConfigX.getPortConfig());
                        break;
                    case SWITCHPAIRPORT:
                        model.setSwitchPairPortConfig(portNumber, lcConfigX.getPortConfig());
                        break;
                    case SOUNDPORT:
                        model.setSoundPortConfig(portNumber, lcConfigX.getPortConfig());
                        break;
                    case LIGHTPORT:
                        model.setLightPortConfig(portNumber, lcConfigX.getPortConfig());
                        break;
                    case INPUTPORT:
                        model.setInputPortConfig(portNumber, lcConfigX.getPortConfig());
                        break;
                    // add support for more port types
                    default:
                        break;
                }
            }
            else {
                LOGGER.info("lcConfigX message is not for the selected node.");
            }
        }
    }

    @Override
    public void feedbackAccessory(byte[] address, int detectorNumber, int accessoryAddress) {
        LOGGER.info("Accessory address is signaled, address: {}, detectorNumber: {}, accessoryAddress: {}", address,
            detectorNumber, accessoryAddress);

    }

    @Override
    public void feedbackCv(byte[] address, AddressData decoderAddress, int cvNumber, int dat) {
        LOGGER.info("CV is signaled from feedback device, address: {} , decoderAddress: {}, cvNumber: {}, dat: {}",
            address, decoderAddress, cvNumber, dat);

    }

    @Override
    public void feedbackXPom(byte[] address, AddressData decoderAddress, int cvNumber, int[] data) {
        LOGGER.info("XPOM is signaled from feedback device, address: {} , decoderAddress: {}, cvNumber: {}, data: {}",
            address, decoderAddress, cvNumber, data);

    }

    @Override
    public void feedbackXPom(byte[] address, DecoderIdAddressData did, int cvNumber, int[] data) {
        LOGGER.info("XPOM is signaled from feedback device, address: {} , did: {}, cvNumber: {}, data: {}", address,
            did, cvNumber, data);

    }

    @Override
    public void csProgState(
        byte[] address, CommandStationProgState commandStationProgState, int remainingTime, int cvNumber, int cvData) {
        LOGGER.info(
            "CS prog state is signaled, address: {} , progState: {}, remainingTime: {}, cvNumber: {}, cvData: {}",
            address, commandStationProgState, remainingTime, cvNumber, cvData);

    }

    @Override
    public void csState(byte[] address, CommandStationState commandStationState) {
        LOGGER.info("CS state is signaled, address: {} , csState: {}", address, commandStationState);

        Node node = findNode(address);

        // if (model.getSelectedNode() != null && model.getSelectedNode().equals(node)) {
        LOGGER.trace("The command station state has changed, address: {}, state: {}", address, commandStationState);
        model.setCommandStationState(node, commandStationState);
        // }
    }

    @Override
    public void csDriveManual(byte[] address, DriveState driveState) {
        LOGGER.info("CS drive manual is signaled, address: {} , driveState: {}", address, driveState);

    }

    @Override
    public void csDriveState(byte[] address, DriveState driveState) {
        LOGGER.info("CS drive state is signaled, address: {} , driveState: {}", address, driveState);

    }

    @Override
    public void csAccessoryManual(byte[] address, AddressData decoderAddress, ActivateCoilEnum activate, int aspect) {
        LOGGER.info("CS accessory manual is signaled, address: {}, decoderAddress: {}, activate: {}, aspect: {}",
            address, decoderAddress, activate, aspect);

    }

    @Override
    public void csAccessoryAcknowledge(byte[] address, int decoderAddress, AccessoryAcknowledge acknowledge) {
        LOGGER.info("CS accessory acknowledge is signaled, address: {}, decoder address: {} , acknowledge: {}", address,
            decoderAddress, acknowledge);

    }

    @Override
    public void csRcPlusTid(byte[] address, TidData tidData) {
        LOGGER.info("CS rcPlusTid was signaled, address: {}, tidData: {}", address, tidData);

    }

    @Override
    public void csRcPlusPingAcknState(byte[] address, RcPlusPhase phase, RcPlusAcknowledge acknState) {
        // no implementation
    }

    @Override
    public void csRcPlusBindAnswer(byte[] address, RcPlusDecoderAnswerData decoderAnswer) {
        // no implementation
    }

    @Override
    public void csRcPlusFindAnswer(byte[] address, RcPlusPhase phase, RcPlusDecoderAnswerData decoderAnswer) {
        // no implementation
    }

    @Override
    public void feedbackRcPlusBindAccepted(
        byte[] address, int detectorNum, RcPlusDecoderType decoderType, RcPlusFeedbackBindData rcPlusBindAccepted) {
        // no implementation
    }

    @Override
    public void feedbackRcPlusPingCollision(byte[] address, int detectorNum, RcPlusPhase phase) {
        // no implementation
    }

    @Override
    public void feedbackRcPlusFindCollision(
        byte[] address, int detectorNum, RcPlusPhase phase, DecoderUniqueIdData uniqueId) {
        // no implementation
    }

    @Override
    public void feedbackRcPlusPongOkay(
        byte[] address, int detectorNum, RcPlusPhase phase, RcPlusDecoderType decoderType,
        DecoderUniqueIdData uniqueId) {
        // no implementation
    }

    @Override
    public void feedbackRcPlusPongNew(
        byte[] address, int detectorNum, RcPlusPhase phase, RcPlusDecoderType decoderType,
        DecoderUniqueIdData uniqueId) {
        // no implementation
    }

    @Override
    public void nodeLost(byte[] address, org.bidib.jbidibc.core.Node node) {
        // no implementation
    }

    @Override
    public void nodeNew(byte[] address, org.bidib.jbidibc.core.Node node) {
        // no implementation
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof MainMessageListener) {
            MainMessageListener other = (MainMessageListener) obj;
            if (model.equals(other.model)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        return model.hashCode();
    }
}
