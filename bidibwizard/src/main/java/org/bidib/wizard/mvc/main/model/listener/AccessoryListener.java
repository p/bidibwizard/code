package org.bidib.wizard.mvc.main.model.listener;

public interface AccessoryListener {
    /**
     * The label of the accessory has changed.
     * 
     * @param label
     *            the new label
     */
    void labelChanged(String label);

    /**
     * The macros of an accessory have changed.
     */
    void macrosChanged();

    /**
     * The execution state of an accessory has been changed.
     * 
     * @param accessoryId
     *            the accessory id
     * @param aspect
     *            the aspect number
     */
    void accessoryStateChanged(int accessoryId, int aspect);
}
