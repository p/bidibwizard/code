package org.bidib.wizard.mvc.pom.view.panel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.util.DefaultValidationResultModel;

public class PomValidationResultModel extends DefaultValidationResultModel {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(PomValidationResultModel.class);

    public static final String PROPERTY_VALID_STATE = "validState";

    public static final String PROPERTY_VALID_STATE_NO_WARN_OR_ERRORS = "validStateNoWarnOrErrors";

    private boolean validState;

    private boolean validStateNoWarnOrErrors;

    private PomValidationResultModel parentValidationModel;

    public PomValidationResultModel() {
        this(null);
    }

    public PomValidationResultModel(final PomValidationResultModel parentValidationModel) {
        this.parentValidationModel = parentValidationModel;
    }

    @Override
    public void setResult(ValidationResult newResult) {

        boolean oldValidState = getResult().isEmpty();
        boolean oldValidStateNoWarnErrors = !(getResult().hasWarnings() || getResult().hasErrors());

        // we must add the result of the parent
        if (parentValidationModel != null) {
            LOGGER.info("Add the messages of the parent.");
            ValidationResult parentResult = parentValidationModel.getResult();
            newResult.addAll(parentResult.getMessages());
        }
        super.setResult(newResult);

        boolean newValidState = newResult.isEmpty();
        boolean newValidStateNoWarnErrors = !(newResult.hasWarnings() || newResult.hasErrors());

        LOGGER
            .info(
                "Set the newValidState: {}, oldValidState: {}, newValidStateNoWarnErrors: {}, oldValidStateNoWarnErrors: {}",
                newValidState, oldValidState, newValidStateNoWarnErrors, oldValidStateNoWarnErrors);

        validState = newValidState;
        validStateNoWarnOrErrors = newValidStateNoWarnErrors;

        firePropertyChange(PROPERTY_VALID_STATE, oldValidState, newValidState);
        firePropertyChange(PROPERTY_VALID_STATE_NO_WARN_OR_ERRORS, oldValidStateNoWarnErrors, newValidStateNoWarnErrors);
    }

    public boolean getValidState() {
        return validState;
    }

    public boolean getValidStateNoWarnOrErrors() {
        return validStateNoWarnOrErrors;
    }
};
