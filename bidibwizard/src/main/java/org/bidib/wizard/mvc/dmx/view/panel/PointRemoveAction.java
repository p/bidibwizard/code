package org.bidib.wizard.mvc.dmx.view.panel;

import java.awt.event.ActionEvent;

import org.bidib.wizard.mvc.dmx.model.DmxChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PointRemoveAction extends LocationAwareAction<DmxChannel> {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(PointRemoveAction.class);

    private DmxDataItem originalDataItem;

    private String text;

    public PointRemoveAction(String text, DmxDataItem dmxDataItem, DmxChannel dmxChannel, DmxChartPanel dmxChartPanel) {
        super(dmxDataItem.toString(), dmxChannel, dmxChartPanel);
        this.originalDataItem = dmxDataItem;
        this.text = text;
    }

    @Override
    public void actionPerformed(ActionEvent event) {

        LOGGER.info("Remove dmxDataItem: {}", originalDataItem);

        String seriesKey = Integer.toString(getActionObject().getChannelId());

        dmxChartPanel.removeDataItem(seriesKey, originalDataItem);

    }

    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        return text;
    }
}
