package org.bidib.wizard.mvc.main.model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.PortConfigStatus;
import org.bidib.jbidibc.core.port.PortConfigUtils;
import org.bidib.jbidibc.core.port.PortConfigValue;
import org.bidib.jbidibc.core.port.PortMapUtils;
import org.bidib.jbidibc.core.port.ReconfigPortConfigValue;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GenericPort {
    private static final Logger LOGGER = LoggerFactory.getLogger(GenericPort.class);

    public static final String PROPERTY_CONFIG_LOADED = "configLoaded";

    public static final String PROPERTY_PORT_CONFIG_ERRORCODE = "portConfigErrorCode";

    public static final String PROPERTY_PORT_INACTIVE = "isInactive";

    public static final String PROPERTY_PORT_TYPE_CHANGED = "portTypeChanged";

    public static final String PROPERTY_PORT_CONFIG_CHANGED = "portConfigChanged";

    private PropertyChangeSupport pcs;

    private Integer portNumber;

    private Map<Byte, PortConfigValue<?>> portConfig = new LinkedHashMap<>();

    private transient Set<Byte> knownPortConfigKeys = new LinkedHashSet<>();

    private PortConfigStatus configStatus = PortConfigStatus.CONFIG_PENDING;

    private Integer portConfigErrorCode;

    private byte portStatus;

    // TODO maybe better use a map to store portStatus and portValue
    private Integer portValue;

    private boolean isInactive;

    private transient WeakReference<GenericPort> pairedPortMaster;

    public GenericPort(Integer portNumber) {
        this.portNumber = portNumber;
        pcs = new PropertyChangeSupport(this);
    }

    /**
     * @return the portNumber
     */
    public Integer getPortNumber() {
        return portNumber;
    }

    /**
     * @param portNumber
     *            the portNumber to set
     */
    public void setPortNumber(Integer portNumber) {
        this.portNumber = portNumber;
    }

    /**
     * @return the currentPortType
     */
    public LcOutputType getCurrentPortType() {

        ReconfigPortConfigValue reconfig = getPortConfig(BidibLibrary.BIDIB_PCFG_RECONFIG);
        if (reconfig != null) {
            return reconfig.getCurrentOutputType();
        }
        return null;
    }

    /**
     * Check if the current port type is matching the required port type.
     * 
     * @param requiredPortType
     *            the required port type
     * @return port type matches
     */
    public boolean isMatchingPortType(LcOutputType requiredPortType) {
        LcOutputType currentPortType = getCurrentPortType();
        LOGGER.trace("isMatchingPortType for requiredPortType: {}, currentPortType: {}, portNumber: {}",
            requiredPortType, currentPortType, portNumber);
        if (currentPortType == null) {
            // if the port type is not available we assume the port is matching
            return true;
        }

        boolean matchingTypeSelf = requiredPortType.equals(currentPortType);
        if (matchingTypeSelf && pairedPortMaster != null && pairedPortMaster.get() != null) {
            matchingTypeSelf = pairedPortMaster.get().isMatchingPortType(requiredPortType);
        }
        return matchingTypeSelf;
    }

    /**
     * @return the port supports remapping. This means the port can be configured to more than one port type.
     */
    public boolean isRemappingEnabled() {

        ReconfigPortConfigValue reconfig = getPortConfig(BidibLibrary.BIDIB_PCFG_RECONFIG);
        if (reconfig != null) {
            return PortMapUtils.supportsPortRemapping(reconfig);
        }
        return false;
    }

    /**
     * @return the configLoaded
     */
    public PortConfigStatus getConfigStatus() {
        return configStatus;
    }

    /**
     * @param configStatus
     *            the configStatus to set
     */
    public void setConfigStatus(PortConfigStatus configStatus) {
        PortConfigStatus oldValue = this.configStatus;
        this.configStatus = configStatus;
        LOGGER.info("New port configStatus: {}", configStatus);

        pcs.firePropertyChange(PROPERTY_CONFIG_LOADED, new ConfigEvent(portNumber, oldValue),
            new ConfigEvent(portNumber, this.configStatus));
    }

    /**
     * @return the portConfigErrorCode
     */
    public Integer getPortConfigErrorCode() {
        return portConfigErrorCode;
    }

    /**
     * @param portConfigErrorCode
     *            the portConfigErrorCode to set
     */
    public void setPortConfigErrorCode(Integer portConfigErrorCode) {
        Integer oldValue = this.portConfigErrorCode;

        LOGGER.info("Set the portConfigErrorCode: {}, port number: {}", portConfigErrorCode, portNumber);
        this.portConfigErrorCode = portConfigErrorCode;

        pcs.firePropertyChange(PROPERTY_PORT_CONFIG_ERRORCODE, new ConfigEvent(portNumber, oldValue),
            new ConfigEvent(portNumber, this.portConfigErrorCode));
    }

    public void clearPortConfig() {
        // remove all existing data
        this.portConfig.clear();
    }

    public void setPortConfig(Map<Byte, PortConfigValue<?>> portConfig) {
        LOGGER.info("Set the port config: {}", portConfig);

        // keep the received config keys because we must only send the known keys to the node
        Collections.addAll(getKnownPortConfigKeys(), portConfig.keySet().toArray(new Byte[0]));

        // and set the new config
        boolean changedPortTypeDetected = false;

        LcOutputType currentPortType = null;
        LcOutputType newPortType = null;

        ReconfigPortConfigValue reconfig = (ReconfigPortConfigValue) portConfig.get(BidibLibrary.BIDIB_PCFG_RECONFIG);
        if (reconfig != null) {

            currentPortType = getCurrentPortType();
            newPortType = reconfig.getCurrentOutputType();

            if (!Objects.equals(currentPortType, newPortType)) {
                LOGGER.info("The port type has changed from: {}, to: {}", currentPortType, newPortType);
                changedPortTypeDetected = true;

                // reset the value and the status
                portStatus = ByteUtils.getLowByte(0);
                portValue = null;
            }
        }

        updatePortConfigValues(portConfig);

        // signal that the port type has changed
        if (changedPortTypeDetected) {
            pcs.firePropertyChange(PROPERTY_PORT_TYPE_CHANGED, currentPortType, newPortType);
        }

        // an additional message will follow
        boolean moreToContinue = portConfig.containsKey(BidibLibrary.BIDIB_PCFG_CONTINUE);
        if (moreToContinue) {
            LOGGER.info("An additional message with more config will follow.");
            setConfigStatus(PortConfigStatus.CONFIG_PENDING);
        }
        else {
            setConfigStatus(PortConfigStatus.CONFIG_PASSED);
        }

        // no parameters available / error code
        Number noneErrorCode = getPortConfigValue(BidibLibrary.BIDIB_PCFG_NONE, portConfig);
        if (noneErrorCode != null) {
            int errorCode = ByteUtils.getInt(noneErrorCode.byteValue());
            LOGGER.error("The returned port config has signaled an error: {}", errorCode);
            setPortConfigErrorCode(errorCode);
            setConfigStatus(PortConfigStatus.CONFIG_ERROR);
        }

        // the port is inactive
        boolean isInactive = portConfig.containsKey(BidibLibrary.BIDIB_PCFG_ERR_PORT_INACTIVE);
        if (isInactive) {
            LOGGER.info("The port is signalled as inactive.");
            setInactive(true);
        }
    }

    private void updatePortConfigValues(final Map<Byte, PortConfigValue<?>> portConfig) {

        // iterate over the entries of portConfig and check if the values have changed
        boolean valuesChanged = false;
        for (Entry<Byte, PortConfigValue<?>> entry : portConfig.entrySet()) {
            PortConfigValue<?> value = this.portConfig.get(entry.getKey());
            if (value != null && !value.equals(entry.getValue())) {
                // the port config value has changed
                LOGGER.debug("Changed port config value detected, old: {}, new: {}", value, entry.getValue());
                valuesChanged = true;
            }
        }

        // TODO why don't we signal the change of a port value ?
        this.portConfig.putAll(portConfig);

        if (valuesChanged) {
            LOGGER.info("Notify the changed port config values.");

            pcs.firePropertyChange(PROPERTY_PORT_CONFIG_CHANGED, false, true);
        }
    }

    /**
     * @return the current port configuation
     */
    public Map<Byte, PortConfigValue<?>> getPortConfig() {
        Map<Byte, PortConfigValue<?>> values = new LinkedHashMap<>();
        values.putAll(portConfig);
        return values;
    }

    /**
     * @return the knownPortConfigKeys
     */
    public Set<Byte> getKnownPortConfigKeys() {
        return knownPortConfigKeys;
    }

    /**
     * @param knownPortConfigKeys
     *            the knownPortConfigKeys to set
     */
    public void setKnownPortConfigKeys(Set<Byte> knownPortConfigKeys) {

        // clear the current keys
        this.knownPortConfigKeys.clear();

        // add all new keys
        this.knownPortConfigKeys.addAll(knownPortConfigKeys);
    }

    /**
     * @param key
     *            the key to search
     * @return {@code true}: the key is supported by the node, {@code false}: the key is not supported by the node
     */
    public boolean isPortConfigKeySupported(Byte key) {
        return knownPortConfigKeys.contains(key);
    }

    protected void setPortConfigValue(Byte key, PortConfigValue<?> value) {
        try {
            PortConfigValue<?> portConfigValue = portConfig.get(key);
            if (portConfigValue != null) {
                LOGGER.info("Replace old portConfigValue: {} for key: {} with new value: {}", portConfigValue,
                    ByteUtils.byteToHex(key), value);
            }
            portConfig.put(key, value);
        }
        catch (ClassCastException ex) {
            LOGGER.warn("Cast value of key: {} to target type failed.", key, ex);
        }
    }

    protected <T> T getPortConfigValue(Byte key) {
        try {
            PortConfigValue<?> portConfigValue = portConfig.get(key);
            if (portConfigValue != null) {
                return (T) portConfig.get(key).getValue();
            }
        }
        catch (ClassCastException ex) {
            LOGGER.warn("Cast value of key: {} to target type failed.", key, ex);
        }
        return null;
    }

    protected <T> T getPortConfigValue(Byte key, Map<Byte, PortConfigValue<?>> portConfig) {
        try {
            PortConfigValue<?> portConfigValue = portConfig.get(key);
            if (portConfigValue != null) {
                return (T) portConfig.get(key).getValue();
            }
        }
        catch (ClassCastException ex) {
            LOGGER.warn("Cast value of key: {} to target type failed.", key, ex);
        }
        return null;
    }

    protected <T> T getPortConfig(Byte key) {
        try {
            PortConfigValue<?> portConfigValue = portConfig.get(key);
            if (portConfigValue != null) {
                return (T) portConfig.get(key);
            }
        }
        catch (ClassCastException ex) {
            LOGGER.warn("Cast value of key: {} to target type failed.", key, ex);
        }
        return null;
    }

    /**
     * @return the port has port config available
     */
    public boolean isHasPortConfig(Byte key) {
        boolean hasPortConfig = portConfig.containsKey(key);
        LOGGER.info("The generic port has port config for key '{}': {}", key.intValue(), hasPortConfig);
        return hasPortConfig;
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        this.pcs.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        this.pcs.removePropertyChangeListener(listener);
    }

    public boolean isSupportsSwitchPort() {
        boolean isSupportsSwitchPort = PortConfigUtils.isSupportsSwitchPort(portConfig);
        return isSupportsSwitchPort;
    }

    public boolean isSupportsSwitchPairPort() {
        boolean isSupportsSwitchPairPort = PortConfigUtils.isSupportsSwitchPairPort(portConfig);
        return isSupportsSwitchPairPort;
    }

    public boolean isSupportsInputPort() {
        boolean isSupportsInputPort = PortConfigUtils.isSupportsInputPort(portConfig);
        return isSupportsInputPort;
    }

    public boolean isSupportsServoPort() {
        boolean isSupportsServoPort = PortConfigUtils.isSupportsServoPort(portConfig);
        return isSupportsServoPort;
    }

    public boolean isSupportsAnalogPort() {
        boolean isSupportsAnalogPort = PortConfigUtils.isSupportsAnalogPort(portConfig);
        return isSupportsAnalogPort;
    }

    public boolean isSupportsLightPort() {
        boolean isSupportsLightPort = PortConfigUtils.isSupportsLightPort(portConfig);
        return isSupportsLightPort;
    }

    public boolean isSupportsBacklightPort() {
        boolean isSupportsBacklightPort = PortConfigUtils.isSupportsBacklightPort(portConfig);
        return isSupportsBacklightPort;
    }

    public boolean isSupportsMotorPort() {
        boolean isSupportsMotorPort = PortConfigUtils.isSupportsMotorPort(portConfig);
        return isSupportsMotorPort;
    }

    public boolean isSupportsSoundPort() {
        boolean isSupportsMotorPort = PortConfigUtils.isSupportsSoundPort(portConfig);
        return isSupportsMotorPort;
    }

    public Integer getSupportedPortTypes() {
        ReconfigPortConfigValue reconfig = getPortConfig(BidibLibrary.BIDIB_PCFG_RECONFIG);
        if (reconfig != null) {

            return reconfig.getPortMap();
        }
        return null;
    }

    /**
     * @return the portStatus
     */
    public byte getPortStatus() {
        return portStatus;
    }

    /**
     * @param portStatus
     *            the portStatus to set
     */
    public void setPortStatus(byte portStatus) {
        LOGGER.info("Set portStatus: {}, port: {}", portStatus, this);
        this.portStatus = portStatus;
    }

    /**
     * @return the portValue
     */
    public Integer getPortValue() {
        return portValue;
    }

    /**
     * @param portValue
     *            the portValue to set
     */
    public void setPortValue(Integer portValue) {
        this.portValue = portValue;
    }

    /**
     * @return the isInactive flag
     */
    public boolean isInactive() {
        return isInactive;
    }

    /**
     * The port can be inactive after the port type was changed.
     * 
     * @param isInactive
     *            the isInactive flag to set
     */
    public void setInactive(boolean isInactive) {
        boolean oldValue = this.isInactive;
        this.isInactive = isInactive;

        pcs.firePropertyChange(PROPERTY_PORT_INACTIVE, oldValue, isInactive);
    }

    public WeakReference<GenericPort> getPairedPortMaster() {
        return pairedPortMaster;
    }

    public void setPairedPortMaster(WeakReference<GenericPort> pairedPortMaster) {
        LOGGER.info("Set the paired port master: {}", pairedPortMaster);
        this.pairedPortMaster = pairedPortMaster;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
