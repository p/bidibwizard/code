package org.bidib.wizard.mvc.main.model.function;

import org.bidib.wizard.mvc.main.model.Port;

public interface PortAware<P extends Port<?>> {

    /**
     * @return the port
     */
    P getPort();

    /**
     * @param port
     *            the port
     */
    void setPort(P port);
}
