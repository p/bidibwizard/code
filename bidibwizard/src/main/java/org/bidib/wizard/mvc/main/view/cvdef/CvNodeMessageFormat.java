package org.bidib.wizard.mvc.main.view.cvdef;

import java.text.FieldPosition;
import java.text.Format;
import java.text.MessageFormat;
import java.text.ParsePosition;

import org.bidib.wizard.utils.XmlLocaleUtils;

public class CvNodeMessageFormat extends Format {
    private static final long serialVersionUID = 1L;

    private MessageFormat messageFormat;

    private String lang;

    public CvNodeMessageFormat(String pattern) {

        messageFormat = new MessageFormat(pattern);
        lang = XmlLocaleUtils.getXmlLocaleVendorCV();
    }

    @Override
    public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
        Object[] args = null;
        if (obj instanceof Object[]) {
            args = (Object[]) obj;
        }
        else if (obj instanceof CvNode) {
            CvNode cvNode = (CvNode) obj;
            args =
                new Object[] { CvNode.getDescription(cvNode.getCV().getDescription(), lang), cvNode.getCV().getNumber() };
        }
        else {
            args = new Object[] { obj };
        }
        return messageFormat.format(args, toAppendTo, pos);
    }

    @Override
    public Object parseObject(String source, ParsePosition pos) {
        return messageFormat.parseObject(source, pos);
    }
}