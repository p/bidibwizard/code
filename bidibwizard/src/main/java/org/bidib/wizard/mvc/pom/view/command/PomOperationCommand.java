package org.bidib.wizard.mvc.pom.view.command;

import org.bidib.jbidibc.core.AddressData;
import org.bidib.jbidibc.core.enumeration.PomOperation;
import org.bidib.jbidibc.core.enumeration.PomProgState;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.wizard.mvc.pom.view.panel.ProgCommandAwareBeanModel;

public class PomOperationCommand<M extends ProgCommandAwareBeanModel> {
    private AddressData decoderAddress;

    private PomOperation pomOperation;

    private int cvNumber;

    private int cvValue;

    private Integer cvValueResult;

    private PomProgState progStateResult;

    public PomOperationCommand(AddressData decoderAddress, PomOperation pomOperation, int cvNumber, int cvValue) {
        this.decoderAddress = decoderAddress;
        this.pomOperation = pomOperation;
        this.cvNumber = cvNumber;
        this.cvValue = cvValue;
    }

    /**
     * @return the decoderAddress
     */
    public AddressData getDecoderAddress() {
        return decoderAddress;
    }

    /**
     * @param decoderAddress
     *            the decoderAddress to set
     */
    public void setDecoderAddress(AddressData decoderAddress) {
        this.decoderAddress = decoderAddress;
    }

    /**
     * @return the pomOperation
     */
    public PomOperation getPomOperation() {
        return pomOperation;
    }

    /**
     * @param pomOperation
     *            the pomOperation to set
     */
    public void setPomOperation(PomOperation pomOperation) {
        this.pomOperation = pomOperation;
    }

    /**
     * @return the cvNumber
     */
    public int getCvNumber() {
        return cvNumber;
    }

    /**
     * @param cvNumber
     *            the cvNumber to set
     */
    public void setCvNumber(int cvNumber) {
        this.cvNumber = cvNumber;
    }

    /**
     * @return the cvValue
     */
    public int getCvValue() {
        return cvValue;
    }

    /**
     * @param cvValue
     *            the cvValue to set
     */
    public void setCvValue(int cvValue) {
        this.cvValue = cvValue;
    }

    /**
     * @return the cvValueResult
     */
    public Integer getCvValueResult() {
        return cvValueResult;
    }

    /**
     * @param cvValueResult
     *            the cvValueResult to set
     */
    public void setCvValueResult(Integer cvValueResult) {
        this.cvValueResult = cvValueResult;
    }

    /**
     * @return the progStateResult
     */
    public PomProgState getProgStateResult() {
        return progStateResult;
    }

    /**
     * @param progStateResult
     *            the progStateResult to set
     */
    public void setProgStateResult(PomProgState progStateResult) {
        this.progStateResult = progStateResult;
    }

    public void postExecute(final M progComamndAwareBeanModel) {
        // do nothing
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("decoderAddress=");
        sb.append(decoderAddress).append("ptOperation=");
        sb.append(pomOperation).append(",cvNumber=").append(cvNumber).append(",cvValue=0x")
            .append(ByteUtils.byteToHex(cvValue));
        if (cvValueResult != null) {
            sb.append(",cvValueResult=").append(ByteUtils.byteToHex(cvValueResult));
        }
        if (progStateResult != null) {
            sb.append(",progStateResult=").append(progStateResult);
        }
        return sb.toString();
    }
}
