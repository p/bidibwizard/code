package org.bidib.wizard.mvc.common.view.converter;

import java.text.Format;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.value.BindingConverter;
import com.jgoodies.common.base.Preconditions;

/**
 * Converts Values to Strings and vice-versa using a given Format.
 */
public final class StringToFloatConverter implements BindingConverter<Float, String> {
    private static final Logger LOGGER = LoggerFactory.getLogger(StringToFloatConverter.class);

    /**
     * Holds the {@code Format} used to format and parse.
     */
    private final Format format;

    // Instance Creation **************************************************

    /**
     * Constructs a {@code StringConverter} on the given subject using the specified {@code Format}.
     * 
     * @param format
     *            the {@code Format} used to format and parse
     * @throws NullPointerException
     *             if the subject or the format is null.
     */
    public StringToFloatConverter(Format format) {
        this.format = Preconditions.checkNotNull(format, "The format must not be null.");
    }

    // Implementing Abstract Behavior *************************************

    /**
     * Formats the source value and returns a String representation.
     * 
     * @param sourceValue
     *            the source value
     * @return the formatted sourceValue
     */
    @Override
    public String targetValue(Float sourceValue) {
        LOGGER.info("Convert source value: {}", sourceValue);
        String returnValue = null;

        if (sourceValue != null) {

            float value = ((Number) sourceValue).floatValue();
            returnValue = format.format(value);
        }
        LOGGER.info("Converted return value: {}", returnValue);
        return returnValue;
    }

    /**
     * Parses the given String encoding and sets it as the subject's new value. Silently catches {@code ParseException}.
     * 
     * @param targetValue
     *            the value to be converted and set as new subject value
     */
    @Override
    public Float sourceValue(String targetValue) {
        LOGGER.info("Convert targetValue: {}", targetValue);
        Float returnValue = null;
        try {
            if (StringUtils.isNotBlank(targetValue)) {
                // we must replace all ',' with '.' to let the float conversion pass
                String val = targetValue.replaceAll(",", ".");
                returnValue = Float.valueOf(val);
            }
        }
        catch (NumberFormatException e) {
            LOGGER.warn("Cannot parse the target value {}", targetValue);
        }
        LOGGER.info("Converted return value number: {}", returnValue);
        return returnValue;
    }
}
