package org.bidib.wizard.mvc.pt.view.panel;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.bidib.jbidibc.core.enumeration.AddressMode;

public class AddressProgBeanModel extends ProgCommandAwareBeanModel {
    private static final long serialVersionUID = 1L;

    public static final String PROPERTYNAME_ADDRESS_MODE = "addressMode";

    public static final String PROPERTYNAME_ADDRESS = "address";

    private AddressMode mode = AddressMode.SHORT;

    private Integer address;

    public AddressProgBeanModel() {
    }

    /**
     * @return the mode
     */
    public AddressMode getAddressMode() {
        return mode;
    }

    /**
     * @param mode
     *            the mode to set
     */
    public void setAddressMode(AddressMode mode) {
        AddressMode oldMode = this.mode;
        this.mode = mode;
        firePropertyChange(PROPERTYNAME_ADDRESS_MODE, oldMode, mode);
    }

    /**
     * @return the address
     */
    public Integer getAddress() {
        return address;
    }

    /**
     * @param address
     *            the address to set
     */
    public void setAddress(Integer address) {
        Integer oldAddress = this.address;
        this.address = address;
        firePropertyChange(PROPERTYNAME_ADDRESS, oldAddress, address);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}