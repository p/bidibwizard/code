package org.bidib.wizard.mvc.features.view.listener;

import java.util.Collection;

import org.bidib.jbidibc.core.Feature;

public interface FeaturesViewListener {

    /**
     * Close the view.
     */
    void close();

    /**
     * Write provided features.
     * 
     * @param features
     *            the features to write
     */
    void write(Collection<Feature> features);
}
