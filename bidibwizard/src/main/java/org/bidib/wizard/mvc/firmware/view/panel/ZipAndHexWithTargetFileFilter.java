package org.bidib.wizard.mvc.firmware.view.panel;

import java.io.File;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import javax.swing.filechooser.FileFilter;

import org.apache.commons.io.FilenameUtils;
import org.bidib.wizard.common.locale.Resources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ZipAndHexWithTargetFileFilter extends FileFilter {

    private static final Logger LOGGER = LoggerFactory.getLogger(ZipAndHexWithTargetFileFilter.class);

    public static final String SUFFIX_ZIP = "zip";

    public static final String SUFFIX_HEX = "hex";

    @Override
    public boolean accept(File file) {
        boolean result = false;

        if (file != null) {
            if (file.isDirectory()) {
                result = true;
            }
            else if (file.toString() != null) {
                String extension = FilenameUtils.getExtension(file.toString());
                if (SUFFIX_ZIP.equalsIgnoreCase(extension)) {
                    // load ZIP
                    // have a look inside
                    ZipFile zipFile = null;
                    try {
                        zipFile = new ZipFile(file);
                        ZipEntry zipEntry = zipFile.getEntry("firmware.xml");
                        if (zipEntry != null) {
                            result = true;
                        }
                    }
                    catch (ZipException e) {
                        LOGGER.warn("Check for firmware.xml inside ZIP failed.");
                    }
                    catch (Exception e) {
                        LOGGER.warn("Check for firmware.xml inside ZIP failed.", e);
                    }
                    finally {
                        if (zipFile != null) {
                            try {
                                zipFile.close();
                            }
                            catch (Exception e) {
                                LOGGER.warn("Close zip failed.", e);
                            }
                        }
                    }
                }
                else {
                    String[] parts = file.toString().split("\\.");

                    if (parts != null && parts.length >= 1 && parts[parts.length - 1].toLowerCase().equals(SUFFIX_HEX)
                    /* && parts[parts.length - 2].matches("\\d{3}") */) {
                        // try {
                        // // allow all integer values
                        // int destIdentifier = Integer.parseInt(parts[parts.length - 2]);
                        // if (destIdentifier > -1 && destIdentifier < 128) {
                        result = true;
                        // }
                        // }
                        // catch (Exception e) {
                        // }
                    }
                }
            }
        }
        return result;
    }

    @Override
    public String getDescription() {
        return Resources.getString(FirmwareUpdatePanel.class, "filter") + " (*." + SUFFIX_HEX + ", *." + SUFFIX_ZIP
            + ")";
    }

}
