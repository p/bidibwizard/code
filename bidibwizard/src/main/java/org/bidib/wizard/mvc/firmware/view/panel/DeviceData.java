package org.bidib.wizard.mvc.firmware.view.panel;

import javax.swing.Icon;

import org.bidib.wizard.mvc.common.view.checkboxtree.IconData;

public class DeviceData extends IconData {

    public DeviceData(Icon icon, Icon expandedIcon, Object data) {
        super(icon, expandedIcon, data);
    }

    public DeviceData(Icon icon, Object data) {
        this(icon, null, data);
    }
}
