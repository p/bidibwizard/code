package org.bidib.wizard.mvc.pom.view.listener;

import org.bidib.jbidibc.core.AddressData;
import org.bidib.jbidibc.core.enumeration.PomOperation;

public interface PomProgrammerViewListener {
    /**
     * Close the dialog.
     */
    void close();

    /**
     * Send the POM request.
     * 
     * @param decoderAddress
     *            the decoder address
     * @param operation
     *            the operation
     * @param cvNumber
     *            the CV number
     * @param cvValue
     *            the CV value
     */
    void sendRequest(AddressData decoderAddress, PomOperation operation, int cvNumber, int cvValue);

    /**
     * Send the command station state request.
     * 
     * @param activate
     *            activate the command station (set to start mode)
     * @return activation command was sent
     */
    boolean sendCommandStationStateRequest(boolean activate);
}
