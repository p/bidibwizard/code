package org.bidib.wizard.mvc.firmware.view.panel.listener;

import java.io.File;

@Deprecated
public interface FirmwareChooseListener {
    @Deprecated
    void chooseFirmware(File file, int destIdentifier);
}
