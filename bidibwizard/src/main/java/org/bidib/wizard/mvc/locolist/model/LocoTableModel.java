package org.bidib.wizard.mvc.locolist.model;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.collections4.Predicate;
import org.bidib.jbidibc.core.DriveState;
import org.bidib.jbidibc.core.enumeration.DirectionEnum;
import org.bidib.wizard.comm.Direction;
import org.bidib.wizard.comm.SpeedSteps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.beans.Model;
import com.jgoodies.common.collect.ArrayListModel;

public class LocoTableModel extends Model {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(LocoTableModel.class);

    public static final String PROPERTY_LOCOS = "locos";

    public static final String PROPERTY_CS_NODE_SELECTED = "csNodeSelected";

    private ArrayListModel<LocoModel> locoList = new ArrayListModel<>();

    private boolean csNodeSelected;

    private final PropertyChangeListener locoChangeListener;

    public LocoTableModel() {

        // create the propertyChangeListener to refresh the booster list
        locoChangeListener = new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (LocoModel.PROPERTY_LOCO_ADDRESS.equals(evt.getPropertyName())) {
                    LOGGER.debug("The loco address has been changed.");
                    LocoModel booster = (LocoModel) evt.getSource();
                    int index = locoList.indexOf(booster);
                    locoList.fireContentsChanged(index);
                }
            }
        };

    }

    public void addLoco(final DriveState driveState) {
        synchronized (locoList) {
            int locoAddress = driveState.getAddress();
            final LocoModel loco = new LocoModel(locoAddress, locoChangeListener);
            if (!locoList.contains(loco)) {
                LOGGER.info("Add loco to loco list: {}, driveState: {}", loco, driveState);
                toLocoModel(driveState, loco);

                List<LocoModel> oldValue = new LinkedList<>(locoList);
                locoList.add(loco);

                firePropertyChange(PROPERTY_LOCOS, oldValue, locoList);
            }
            else {
                LOGGER.warn("Loco is already in loco list: {}", loco);
                LocoModel existing = IterableUtils.find(locoList, new Predicate<LocoModel>() {

                    @Override
                    public boolean evaluate(LocoModel model) {
                        return model.equals(loco);
                    }

                });
                toLocoModel(driveState, existing);
            }
        }
    }

    private void toLocoModel(final DriveState driveState, final LocoModel loco) {
        // update members of loco
        loco.setSpeed(driveState.getSpeed() & 0x7F);
        // loco.setDirection((driveState.getSpeed() & 0x80) == 0x80 ? Direction.FORWARD : Direction.BACKWARD);
        loco.setDirection(
            (driveState.getDirection() == DirectionEnum.FORWARD ? Direction.FORWARD : Direction.BACKWARD));

        loco.setSpeedSteps(SpeedSteps.fromBidibFormat(driveState.getFormat()));

        loco.setFunctions(driveState.getFunctions());
    }

    public void removeLoco(int locoAddress) {
        synchronized (locoList) {
            LOGGER.info("Remove loco from loco list: {}", locoAddress);

            List<LocoModel> oldValue = new LinkedList<>(locoList);
            int index = locoList.indexOf(new LocoModel(locoAddress, null));
            if (index > -1) {
                locoList.remove(index);

                firePropertyChange(PROPERTY_LOCOS, oldValue, locoList);
            }
        }
    }

    public void removeAllLocos() {
        synchronized (locoList) {
            LOGGER.info("Remove all locos from loco list.");

            List<LocoModel> oldValue = new LinkedList<>(locoList);
            locoList.clear();

            firePropertyChange(PROPERTY_LOCOS, oldValue, locoList);
        }
    }

    public void setDriveState(byte[] address, DriveState driveState) {
        LOGGER.info("Drive state was delivered: {}", driveState);

        if (driveState.getOutputActive() > 0) {
            addLoco(driveState);
        }
        else {
            removeLoco(driveState.getAddress());
        }

    }

    public ArrayListModel<LocoModel> getLocoListModel() {
        return locoList;
    }

    public List<LocoModel> getLocos() {
        return Collections.unmodifiableList(locoList);
    }

    public boolean isCsNodeSelected() {
        return csNodeSelected;
    }

    public void setCsNodeSelected(boolean csNodeSelected) {
        boolean oldValue = this.csNodeSelected;
        this.csNodeSelected = csNodeSelected;

        firePropertyChange(PROPERTY_CS_NODE_SELECTED, oldValue, csNodeSelected);
    }
}
