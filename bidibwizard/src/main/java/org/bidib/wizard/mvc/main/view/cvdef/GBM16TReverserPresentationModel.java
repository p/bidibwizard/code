package org.bidib.wizard.mvc.main.view.cvdef;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.bidib.wizard.mvc.common.view.validation.PropertyValidationI18NSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.validation.Validatable;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.ValidationResultModel;
import com.jgoodies.validation.util.PropertyValidationSupport;

public abstract class GBM16TReverserPresentationModel extends PresentationModel<GBM16TReverserValueBean>
    implements Validatable {
    private static final Logger LOGGER = LoggerFactory.getLogger(GBM16TReverserPresentationModel.class);

    private static final long serialVersionUID = 1L;

    private PropertyChangeListener handler;

    private ValidationResultModel validationModel;

    private GBM16TReverserValueBean gbm16TReverserValueBean;

    public GBM16TReverserPresentationModel(GBM16TReverserValueBean gbm16TReverserValueBean, ValueModel triggerChannel) {
        super(gbm16TReverserValueBean, triggerChannel);
        this.gbm16TReverserValueBean = gbm16TReverserValueBean;
        initEventHandling();
    }

    private void initEventHandling() {
        handler = new ValueUpdateHandler();

        getBufferedModel("reverserModeValue").addValueChangeListener(handler);
        getBufferedModel("reverserOnValue").addValueChangeListener(handler);
        getBufferedModel("reverserOffValue").addValueChangeListener(handler);
        getBufferedModel("reverserPrioValue").addValueChangeListener(handler);

        addBeanPropertyChangeListener(handler);
        addPropertyChangeListener("validState", handler);
        getBeanChannel().addValueChangeListener(handler);
    }

    public void setValidationResultModel(ValidationResultModel validationModel) {
        this.validationModel = validationModel;
    }

    public PropertyChangeListener getChangeHandler() {
        return handler;
    }

    public boolean isValidState() {
        return !validationModel.hasErrors();
    }

    public class ValueUpdateHandler implements PropertyChangeListener {
        public void propertyChange(PropertyChangeEvent evt) {
            LOGGER.debug("prop changed, evt: {}", (evt != null ? evt.getPropertyName() : "---"));

            if (evt != null && "signalValidState".equals(evt.getPropertyName())) {
                signalValidState(Boolean.FALSE, Boolean.TRUE);
            }
            else {
                updateComponents();
            }
        }

        private void updateComponents() {
            LOGGER.debug("Value was updated.");
            boolean oldValue = validationModel.hasErrors();
            ValidationResult result = validate();
            validationModel.setResult(result);

            signalValidState(!oldValue, !result.hasErrors());
        }
    }

    private void signalValidState(boolean oldValue, boolean newValue) {
        LOGGER.debug("Signal valid state, old: {}, new: {}", oldValue, newValue);

        firePropertyChange("validState", oldValue, newValue);
    }

    protected ValidationResult validateModel(boolean writeEnabled) {
        PropertyValidationSupport support = new PropertyValidationI18NSupport(gbm16TReverserValueBean, "validation");

        if (writeEnabled) {

            if (getBufferedModel("reverserModeValue").getValue() == null) {
                LOGGER.debug("Reverser mode value is not set.");
                support.addError("reverserModeValue_key", "not_empty");
            }
        }

        return support.getResult();
    }
}
