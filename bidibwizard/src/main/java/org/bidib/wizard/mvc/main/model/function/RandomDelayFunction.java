package org.bidib.wizard.mvc.main.model.function;

import org.bidib.jbidibc.exchange.lcmacro.LcMacroPointType;
import org.bidib.jbidibc.exchange.lcmacro.RandomDelayPoint;
import org.bidib.wizard.comm.BidibStatus;

public class RandomDelayFunction extends SystemFunction<BidibStatus> implements Delayable {
    private int maximumValue = 255;

    public RandomDelayFunction() {
        super(null, KEY_RANDOM_DELAY);
    }

    public int getMaximumValue() {
        return maximumValue;
    }

    public void setMaximumValue(int maximumValue) {
        this.maximumValue = maximumValue;
    }

    public String getDebugString() {
        return "@" + getMaximumValue() + " RandomDelay";
    }

    @Override
    public LcMacroPointType toLcMacroPoint() {
        RandomDelayPoint delayPoint = new RandomDelayPoint();
        delayPoint.setRandomDelayActionType(getMaximumValue());
        return delayPoint;
    }

    @Override
    public int getDelay() {
        return getMaximumValue();
    }

    @Override
    public void setDelay(int delay) {
        setMaximumValue(delay);
    }
}
