package org.bidib.wizard.mvc.dmx.view.scenery;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

import org.bidib.wizard.mvc.dmx.model.DmxChannel;

public class DmxChannelNodeTreeRenderer extends DefaultTreeCellRenderer {
    private static final long serialVersionUID = 1L;

    public Component getTreeCellRendererComponent(
        JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {

        super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);

        if (value instanceof DmxChannelNode) {
            DmxChannelNode dmxChannelTreeNode = (DmxChannelNode) value;
            DmxChannel dmxChannel = (DmxChannel) dmxChannelTreeNode.getUserObject();

            Color lineColor = dmxChannel.getLineColor();
            if (lineColor == null) {
                // set default color
                lineColor = Color.MAGENTA;
            }
            BufferedImage bufImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
            Graphics2D g2 = bufImg.createGraphics();
            g2.setColor(lineColor);
            g2.fillOval(2, 2, 12, 12);
            // etc.
            g2.dispose();

            ImageIcon imic = new ImageIcon(bufImg);
            setIcon(imic);
        }
        else {
            setIcon(null);
        }
        return this;
    }
}
