package org.bidib.wizard.mvc.main.view.panel.glazed;

import javax.swing.JList;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.bidib.wizard.mvc.main.model.FeedbackPosition;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.UniqueList;
import ca.odell.glazedlists.matchers.AbstractMatcherEditor;
import ca.odell.glazedlists.matchers.Matcher;
import ca.odell.glazedlists.swing.DefaultEventListModel;
import ca.odell.glazedlists.swing.DefaultEventSelectionModel;

public class DecodersSelect extends AbstractMatcherEditor<FeedbackPosition> implements ListSelectionListener {

    private UniqueList<String> decodersEventList;

    private EventList<String> decodersSelectedList;

    private JList<String> decodersJList;

    public DecodersSelect(EventList<FeedbackPosition> source) {
        // derive the decoders list from the feedback positions list
        EventList<String> decodersNonUnique = new FeedbackPositionsToDecoderList(source);
        decodersEventList = new UniqueList<>(decodersNonUnique);

        // create a JList that contains decoders
        DefaultEventListModel<String> decodersListModel = new DefaultEventListModel<>(decodersEventList);
        decodersJList = new JList<>(decodersListModel);

        // create an EventList containing the JList's selection
        DefaultEventSelectionModel<String> decoderSelectionModel = new DefaultEventSelectionModel<>(decodersEventList);
        decodersJList.setSelectionModel(decoderSelectionModel);
        decodersSelectedList = decoderSelectionModel.getSelected();

        // handle changes to the list's selection
        decodersJList.addListSelectionListener(this);
    }

    /**
     * Get the widget for selecting decoders.
     */
    public JList<String> getJList() {
        return decodersJList;
    }

    /**
     * When the JList selection changes, create a new Matcher and fire an event.
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {

        Matcher<FeedbackPosition> newMatcher = new FeedbackPositionForDecodersMatcher(decodersSelectedList);
        fireChanged(newMatcher);
    }

}
