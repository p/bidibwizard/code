package org.bidib.wizard.mvc.pom.view;

import org.bidib.jbidibc.core.enumeration.AccessoryAddressingEnum;
import org.bidib.jbidibc.core.enumeration.AddressTypeEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.beans.Model;

public class CurrentAddressBeanModel extends Model {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(CurrentAddressBeanModel.class);

    public static final String PROPERTYNAME_DCC_ADDRESS = "dccAddress";

    public static final String PROPERTYNAME_ADDRESS_TYPE = "addressType";

    public static final String PROPERTYNAME_ADDRESS_CHANGED = "addressChanged";

    private Integer dccAddress;

    private Boolean addressChanged = Boolean.FALSE;

    private AddressTypeEnum addressType = AddressTypeEnum.LOCOMOTIVE_FORWARD;

    private AccessoryAddressingEnum accessoryAddressing = AccessoryAddressingEnum.RCN_213;

    public CurrentAddressBeanModel() {

    }

    /**
     * @return the dccAddress
     */
    public Integer getDccAddress() {
        return dccAddress;
    }

    /**
     * @param dccAddress
     *            the dccAddress to set
     */
    public void setDccAddress(Integer dccAddress) {
        LOGGER.info("Set the new dccAddress: {}", dccAddress);
        Integer oldAddress = this.dccAddress;
        this.dccAddress = dccAddress;
        firePropertyChange(PROPERTYNAME_DCC_ADDRESS, oldAddress, dccAddress);
    }

    /**
     * @return the addressChanged
     */
    public Boolean getAddressChanged() {
        return addressChanged;
    }

    /**
     * @param addressChanged
     *            the addressChanged to set
     */
    public void setAddressChanged(Boolean addressChanged) {
        LOGGER.info("Set the address changed flag: {}", addressChanged);
        Boolean oldValue = this.addressChanged;
        this.addressChanged = addressChanged;
        firePropertyChange(PROPERTYNAME_ADDRESS_CHANGED, oldValue, addressChanged);
    }

    /**
     * @return the addressType
     */
    public AddressTypeEnum getAddressType() {
        return addressType;
    }

    /**
     * @param addressType
     *            the addressType to set
     */
    public void setAddressType(AddressTypeEnum addressType) {
        LOGGER.info("Set the new address type: {}", addressType);
        AddressTypeEnum oldValue = this.addressType;

        this.addressType = addressType;
        firePropertyChange(PROPERTYNAME_ADDRESS_TYPE, oldValue, addressType);
    }

    /**
     * @return the accessoryAddressing
     */
    public AccessoryAddressingEnum getAccessoryAddressing() {
        return accessoryAddressing;
    }

    /**
     * @param accessoryAddressing
     *            the accessoryAddressing to set
     */
    public void setAccessoryAddressing(AccessoryAddressingEnum accessoryAddressing) {
        this.accessoryAddressing = accessoryAddressing;
    }
}