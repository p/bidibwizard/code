package org.bidib.wizard.mvc.features.view.panel;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import org.bidib.jbidibc.core.Feature;

public class FeatureAwareRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = 1L;

    public FeatureAwareRenderer() {
    }

    public Component getTableCellRendererComponent(
        JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        setToolTipText(null);

        // Feature feature = (Feature) table.getModel().getValueAt(row, featureColumnIndex);
        // if (feature != null) {
        if (value instanceof Feature) {
            Feature feature = (Feature) value;
            int featureValue = feature.getValue();

            setText(Integer.toString(featureValue));
            setToolTipText("This port is inactive");
        }
        else {
            setText(null);
        }

        return this;
    }
}
