package org.bidib.wizard.mvc.stepcontrol.model;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.jgoodies.binding.beans.Model;

public class StepControlAspect extends Model {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTYNAME_ASPECT_NUMBER = "aspectNumber";

    public static final String PROPERTYNAME_POSITION = "position";

    public static final String PROPERTYNAME_POLARITY = "polarity";

    public static final String PROPERTYNAME_STATUS = "status";

    public enum Polarity {
        normal(0), inverted(1);

        int cvValue;

        Polarity(int cvValue) {
            this.cvValue = cvValue;
        }

        public int getCvValue() {
            return cvValue;
        }

        public static Polarity valueOf(int cvValue) {
            Polarity polarity = null;
            for (Polarity e : values()) {
                if (e.getCvValue() == cvValue) {
                    polarity = e;
                    break;
                }
            }
            if (polarity == null) {
                throw new IllegalArgumentException("Cannot map " + cvValue + " to a polarity.");
            }
            return polarity;
        }
    }

    private Integer aspectNumber;

    private long position;

    private Polarity polarity;

    public StepControlAspect(Integer aspectNumber, long position, Polarity polarity) {
        this.aspectNumber = aspectNumber;
        this.position = position;
        this.polarity = polarity;
    }

    /**
     * @return the aspectNumber
     */
    public Integer getAspectNumber() {
        return aspectNumber;
    }

    /**
     * @param aspectNumber
     *            the aspectNumber to set
     */
    public void setAspectNumber(Integer aspectNumber) {
        Integer oldValue = this.aspectNumber;
        this.aspectNumber = aspectNumber;

        firePropertyChange(PROPERTYNAME_ASPECT_NUMBER, oldValue, aspectNumber);
    }

    /**
     * @return the position
     */
    public long getPosition() {
        return position;
    }

    /**
     * @param position
     *            the position to set
     */
    public void setPosition(long position) {
        long oldValue = this.position;
        this.position = position;

        firePropertyChange(PROPERTYNAME_POSITION, oldValue, position);
    }

    /**
     * @return the polarity
     */
    public Polarity getPolarity() {
        return polarity;
    }

    /**
     * @param polarity
     *            the polarity to set
     */
    public void setPolarity(Polarity polarity) {
        Polarity oldValue = this.polarity;
        this.polarity = polarity;

        firePropertyChange(PROPERTYNAME_POLARITY, oldValue, polarity);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
