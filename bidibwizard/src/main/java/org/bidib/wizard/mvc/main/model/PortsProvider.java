package org.bidib.wizard.mvc.main.model;

import java.util.List;

public interface PortsProvider {

    /**
     * @return returns the list of analog ports
     */
    List<AnalogPort> getAnalogPorts();

    /**
     * @return returns the list of feedback ports
     */
    List<FeedbackPort> getFeedbackPorts();

    /**
     * @return returns the list of flags
     */
    List<Flag> getFlags();

    /**
     * @return returns the list of input ports
     */
    List<InputPort> getInputPorts();

    /**
     * @return returns the list of enabled input ports
     */
    List<InputPort> getEnabledInputPorts();

    /**
     * @return returns the list of light ports
     */
    List<LightPort> getLightPorts();

    /**
     * @return returns the list of backlight ports
     */
    List<BacklightPort> getBacklightPorts();

    /**
     * @return returns the list of motor ports
     */
    List<MotorPort> getMotorPorts();

    /**
     * @return returns the list of servo ports
     */
    List<ServoPort> getServoPorts();

    /**
     * @return returns the list of sound ports
     */
    List<SoundPort> getSoundPorts();

    /**
     * @return returns the list of switch ports
     */
    List<SwitchPort> getSwitchPorts();

    /**
     * @return returns the list of enabled switch ports
     */
    List<SwitchPort> getEnabledSwitchPorts();

    /**
     * @return returns the list of switchPair ports
     */
    List<SwitchPairPort> getSwitchPairPorts();

    /**
     * @return returns the list of enabled switchPair ports
     */
    List<SwitchPairPort> getEnabledSwitchPairPorts();

    /**
     * @return returns the list of macros
     */
    List<Macro> getMacros();

    /**
     * @return the list of accessories
     */
    List<Accessory> getAccessories();

    /**
     * @return the node supports the flat port model
     */
    boolean isFlatPortModel();
}
