package org.bidib.wizard.mvc.locolist.controller.listener;

import org.bidib.jbidibc.core.enumeration.CsQueryTypeEnum;

public interface LocoTableControllerListener {

    /**
     * Query the loco list from the command station.
     * 
     * @param csQueryType
     *            the query type
     * @param locoAddress
     *            the loco address to search
     */
    void queryCommandStationList(CsQueryTypeEnum csQueryType, Integer locoAddress);
}
