package org.bidib.wizard.mvc.simulation.view.panel;

import java.awt.Component;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.simulation.events.SimulatorStatusEvent;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.simulation.view.SimulationNodePanel;
import org.bushe.swing.event.annotation.EventSubscriber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vlsolutions.swing.docking.event.DockableStateChangeEvent;

public abstract class AbstractSimulatorNodePanel implements SimulationNodePanel {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractSimulatorNodePanel.class);

    private final Node node;

    protected JPanel contentPanel;

    private JScrollPane scrollContent;

    public AbstractSimulatorNodePanel(final Node node) {
        this.node = node;
    }

    protected boolean isMatchingAddress(String address) {

        if (ByteUtils.bytesToHex(node.getNode().getAddr()).equals(address)) {
            return true;
        }

        return false;
    }

    @Override
    public Component getComponent() {
        if (scrollContent == null) {
            scrollContent = new JScrollPane(contentPanel);
        }
        return scrollContent;
    }

    @Override
    public Node getNode() {
        return node;
    }

    @EventSubscriber(eventClass = SimulatorStatusEvent.class)
    public void simulatortStatusChanged(SimulatorStatusEvent statusEvent) {
        LOGGER.info("The inputport status has changed, nodeAddr: {}, status: {}", statusEvent.getNodeAddr(),
            statusEvent.getStatus());

        // check if the node address matches
        if (!isMatchingAddress(statusEvent.getNodeAddr())) {
            return;
        }

        if (statusEvent.getStatus().equals(SimulatorStatusEvent.Status.stopped)) {
            LOGGER.info("The simulator was stopped. Close the simulator node panel.");
            stop();
        }
    }

    public abstract void stop();

    @Override
    public void dockableStateChanged(DockableStateChangeEvent event) {
        LOGGER.debug("The dockable state has changed: {}", event);
    }
}
