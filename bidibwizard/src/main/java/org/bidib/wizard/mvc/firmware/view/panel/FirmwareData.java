package org.bidib.wizard.mvc.firmware.view.panel;

import javax.swing.Icon;

import org.bidib.jbidibc.exchange.firmware.FirmwareNode;
import org.bidib.wizard.mvc.common.view.checkboxtree.IconData;

public class FirmwareData extends IconData {

    private FirmwareNode firmwareNode;

    public FirmwareData(Icon icon, Icon expandedIcon, Object data, FirmwareNode firmwareNode) {
        super(icon, expandedIcon, data);

        this.firmwareNode = firmwareNode;
    }

    public FirmwareData(Icon icon, Object data, FirmwareNode firmwareNode) {
        this(icon, null, data, firmwareNode);
    }

    /**
     * @return the firmwareNode
     */
    public FirmwareNode getFirmwareNode() {
        return firmwareNode;
    }

    /**
     * @param firmwareNode
     *            the firmwareNode to set
     */
    public void setFirmwareNode(FirmwareNode firmwareNode) {
        this.firmwareNode = firmwareNode;
    }

}
