package org.bidib.wizard.mvc.accessory.view.panel;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JRadioButton;

import org.bidib.jbidibc.core.AddressData;
import org.bidib.jbidibc.core.enumeration.AccessoryAddressingEnum;
import org.bidib.jbidibc.core.enumeration.AddressTypeEnum;
import org.bidib.jbidibc.core.enumeration.TimeBaseUnitEnum;
import org.bidib.jbidibc.core.enumeration.TimingControlEnum;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.accessory.model.AccessoryBeanModel;
import org.bidib.wizard.mvc.accessory.model.AccessoryModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.validation.util.PropertyValidationSupport;

public class DccAccessoryPanel extends AbstractAccessoryPanel<AccessoryBeanModel> {
    private static final Logger LOGGER = LoggerFactory.getLogger(DccAccessoryPanel.class);

    private static final String ENCODED_LOCAL_COLUMN_SPECS = "max(50dlu;pref), 3dlu, max(50dlu;pref), 3dlu, pref:grow";

    private ValueModel timingControlModel;

    private final AccessoryBeanModel accessoryBeanModel;

    private JComponent[] timingControlButtons;

    private JButton aspectButtons[];

    public DccAccessoryPanel(final AccessoryModel accessoryModel) {
        super(accessoryModel);
        accessoryBeanModel = new AccessoryBeanModel();
    }

    @Override
    protected AccessoryBeanModel getAccessoryBeanModel() {
        return accessoryBeanModel;
    }

    @Override
    protected void addSpecificComponents(final DefaultFormBuilder builder) {
        // goto next line
        builder.nextLine();

        // timing control
        builder.append(Resources.getString(AbstractAccessoryPanel.class, "timingControl"));
        timingControlModel =
            new PropertyAdapter<AccessoryBeanModel>(accessoryBeanModel, AccessoryBeanModel.PROPERTYNAME_TIMING_CONTROL,
                true);
        timingControlButtons = new JComponent[TimingControlEnum.values().length];
        int index = 0;

        DefaultFormBuilder timingControlBuilder = new DefaultFormBuilder(new FormLayout("pref, 3dlu, pref"));

        for (TimingControlEnum timingControl : TimingControlEnum.values()) {

            JRadioButton radio =
                BasicComponentFactory.createRadioButton(timingControlModel, timingControl,
                    Resources.getString(TimingControlEnum.class, timingControl.getKey()));
            timingControlButtons[index++] = radio;

            // add radio button
            timingControlBuilder.append(radio);
        }
        builder.append(timingControlBuilder.build(), 9);

        builder.nextLine();

        DefaultFormBuilder localBuilder = new DefaultFormBuilder(new FormLayout(ENCODED_LOCAL_COLUMN_SPECS));
        // add the aspects
        aspectButtons = new JButton[2];
        for (int aspect = 0; aspect < 2; aspect++) {

            JButton aspectButton = new JButton(new AbstractAction(
                Resources.getString(DccAccessoryPanel.class, "aspect-" + aspect)) {
                private static final long serialVersionUID = 1L;

                @Override
                public void actionPerformed(ActionEvent e) {
                    // set aspect button was pressed
                    Integer aspect = (Integer) ((JButton) e.getSource()).getClientProperty("aspect");
                    LOGGER.info("Pressed button: {}, aspect: {}", e.getActionCommand(), aspect);

                    int directDccAddress =
                        accessoryBeanModel.getDccAddress()
                            + (accessoryBeanModel.getAccessoryAddressing() == AccessoryAddressingEnum.RCN_213 ? 3 : 0);

                    addLogText("Send DCC accessory request, address: {}, aspect: {}, switch time: {}, time base: {}",
                        directDccAddress, aspect, accessoryBeanModel.getSwitchTime(),
                        Resources.getString(TimeBaseUnitEnum.class, accessoryBeanModel.getTimeBaseUnit().getKey()));

                    // set the aspect triggers the property change listener
                    accessoryBeanModel.setAcknowledge(null);
                    accessoryBeanModel.setAspect(aspect);
                    // send the request
                    AddressData addressData = new AddressData(directDccAddress, AddressTypeEnum.ACCESSORY);
                    sendRequest(addressData, accessoryBeanModel.getAspect(), accessoryBeanModel.getSwitchTime(),
                        accessoryBeanModel.getTimeBaseUnit(), accessoryBeanModel.getTimingControl());
                }
            });
            aspectButton.putClientProperty("aspect", Integer.valueOf(aspect));
            aspectButton.setEnabled(false);
            aspectButtons[aspect] = aspectButton;
            localBuilder.append(aspectButton);
        }
        builder.append(localBuilder.build(), 9);
    }

    @Override
    protected void addSpecificValidation() {

        // if the model is valid, the aspect buttons are enabled.
        for (int aspect = aspectButtons.length - 1; aspect > -1; aspect--) {
            PropertyConnector.connect(accessoryValidationModel, AccessoryValidationResultModel.PROPERTY_VALID_STATE,
                aspectButtons[aspect], "enabled");
        }
    }

    @Override
    protected void validateSpecificPanel(PropertyValidationSupport support) {

    }

    protected void disableInputElements() {
        // TODO use a new different property here
        accessoryValidationModel.setValidState(false);
    }

    protected void enableInputElements() {
        // TODO use a new different property here
        accessoryValidationModel.setValidState(true);
    }

    private static final int MAX_ADDRESS = 2047;

    @Override
    protected int getMaxAddress() {
        return MAX_ADDRESS;
    }
}
