package org.bidib.wizard.mvc.simulation.controller;

import org.bidib.jbidibc.core.utils.NodeUtils;
import org.bidib.jbidibc.simulation.SimulatorNode;
import org.bidib.jbidibc.simulation.SimulatorRegistry;
import org.bidib.wizard.mvc.common.view.DockKeys;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.simulation.view.SimulationView;
import org.bidib.wizard.mvc.simulation.view.SimulatorProvider;
import org.bidib.wizard.utils.DockUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vlsolutions.swing.docking.Dockable;
import com.vlsolutions.swing.docking.DockableState;
import com.vlsolutions.swing.docking.DockingConstants;
import com.vlsolutions.swing.docking.DockingDesktop;
import com.vlsolutions.swing.docking.DockingUtilities;
import com.vlsolutions.swing.docking.RelativeDockablePosition;
import com.vlsolutions.swing.docking.TabbedDockableContainer;

public class SimulationController implements SimulatorProvider {
    private static final Logger LOGGER = LoggerFactory.getLogger(SimulationController.class);

    // private final JFrame parent;

    private final DockingDesktop desktop;

    private SimulationView simulationView;

    // private MainModel mainModel;

    public SimulationController(/* final JFrame parent, */ final DockingDesktop desktop) {
        // this.parent = parent;
        this.desktop = desktop;
    }

    public void start(MainModel mainModel) {
        // check if the debug interface view is already opened
        String searchKey = DockKeys.SIMULATION_VIEW;
        LOGGER.info("Search for view with key: {}", searchKey);
        Dockable view = (SimulationView) desktop.getContext().getDockableByKey(searchKey);
        if (view != null) {
            LOGGER.info("Select the existing simulation view instead of open a new one: {}", view);
            simulationView = (SimulationView) view;
            DockUtils.selectWindow(view);
            return;
        }

        LOGGER.info("Create new instance of simulation view.");
        // this.mainModel = mainModel;

        simulationView = new SimulationView(this, desktop);

        DockableState[] dockables = desktop.getDockables();
        LOGGER.info("Current dockables: {}", new Object[] { dockables });
        if (dockables.length > 1) {

            DockableState tabPanelNodeDetails = null;
            // search the node details tab panel
            for (DockableState dockable : dockables) {

                if (DockKeys.DOCKKEY_TAB_PANEL.equals(dockable.getDockable().getDockKey())) {
                    LOGGER.info("Found the tab panel dockable.");
                    tabPanelNodeDetails = dockable;

                    break;
                }
            }

            Dockable dock = desktop.getDockables()[1].getDockable();
            if (tabPanelNodeDetails != null) {
                LOGGER.info("Add the simulation view next to the node details panel.");
                dock = tabPanelNodeDetails.getDockable();

                TabbedDockableContainer container = DockingUtilities.findTabbedDockableContainer(dock);
                int order = 0;
                if (container != null) {
                    order = container.getTabCount();
                }
                LOGGER.info("Add new simulationView at order: {}", order);

                desktop.createTab(dock, simulationView, order, true);
            }
            else {
                desktop.split(dock, simulationView, DockingConstants.SPLIT_RIGHT);
            }
        }
        else {
            desktop.addDockable(simulationView, RelativeDockablePosition.RIGHT);
        }

        restoreSimulationView();
    }

    @Override
    public void restoreSimulationView() {
        // Dockable tabPanel = getTabPanel(desktop);
        // if (tabPanel != null) {
        // LOGGER.info("Split the tabPanel to add the SimulationView.");
        // desktop.split(tabPanel, view, DockingConstants.SPLIT_BOTTOM);
        // }

        // TODO enable?
        // desktop.addDockable(view);
    }

    private Dockable getTabPanel(DockingDesktop desktop) {
        Dockable tabPanel = null;
        DockableState[] dockables = desktop.getDockables();
        for (DockableState dockable : dockables) {

            if (dockable.getDockable().getDockKey().getKey().equals("tabPanel")) {
                LOGGER.info("Found tabPanel.");
                tabPanel = dockable.getDockable();
                break;
            }
        }
        return tabPanel;
    }

    public void activate(Node node) {
        if (simulationView != null) {
            simulationView.activate(node, desktop);
        }
    }

    @Override
    public SimulatorNode getSimulator(Node node) {
        String nodeAddress = NodeUtils.formatAddress(node.getNode().getAddr());
        return SimulatorRegistry.getInstance().getSimulator(nodeAddress);
    }
}
