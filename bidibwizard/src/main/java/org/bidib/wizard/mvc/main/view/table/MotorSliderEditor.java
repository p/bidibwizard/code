package org.bidib.wizard.mvc.main.view.table;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;

import javax.swing.BoundedRangeModel;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JSlider;

import org.bidib.wizard.mvc.main.model.MotorPort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MotorSliderEditor extends SliderEditor {
    private static final Logger LOGGER = LoggerFactory.getLogger(MotorSliderEditor.class);

    private static final long serialVersionUID = 1L;

    private final DecimalFormat format = new DecimalFormat("##0");

    private int rowNumber;

    private MotorPort port;

    private JButton stopButton;

    public MotorSliderEditor(MotorPort motorPort, int minValue, int maxValue) {
        super(minValue, maxValue, null);
        this.port = motorPort;
    }

    protected MotorPort getPort() {
        return port;
    }

    @Override
    public void createComponent(int initialValue) {

        super.createComponent(initialValue);

        Dimension dim = sliderValue.getMinimumSize();
        dim = new Dimension(dim.width + 10, dim.height);
        sliderValue.setPreferredSize(dim);
        sliderValue.setMaximumSize(dim);
        sliderValue.setMinimumSize(dim);

        slider.setMajorTickSpacing(slider.getMaximum());
        slider.setPaintTicks(true);

        stopButton = new JButton("STOP");
        stopButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                if (slider instanceof JZeroSlider) {
                    ((JZeroSlider) slider).stop();
                }

                // slider.setValue(0);
            }
        });
        panel.add(stopButton);
        panel.add(Box.createRigidArea(new Dimension(3, 0)));

        panel.validate();

    }

    private static final class JZeroSlider extends JSlider {
        private static final long serialVersionUID = 1L;

        boolean ignoreNewValue;

        public JZeroSlider(int min, int max, int value) {
            super(min, max, value);
        }

        @Override
        public void setValue(int value) {

            BoundedRangeModel m = getModel();
            int oldValue = m.getValue();

            boolean isAdjusting = getValueIsAdjusting();

            LOGGER.info("Set the new slider value: {}, oldValue: {}, ignoreNewValue: {}, isAdjusting: {}", value,
                oldValue, ignoreNewValue, isAdjusting);

            if (oldValue > 0 && value <= 0 && isAdjusting) {
                LOGGER.info("Must stop at value 0.");
                ignoreNewValue = true;
                value = 0;
            }
            else if (oldValue < 0 && value >= 0 && isAdjusting) {
                LOGGER.info("Must stop at value 0.");
                ignoreNewValue = true;
                value = 0;
            }
            else if (ignoreNewValue) {
                value = 0;
            }
            super.setValue(value);
        }

        public void stop() {
            LOGGER.info("Stop is called.");
            setValue(0);
            ignoreNewValue = false;
        }

        @Override
        public void setValueIsAdjusting(boolean isAdjusting) {
            LOGGER.info("isAdjusting: {}", isAdjusting);
            super.setValueIsAdjusting(isAdjusting);

            if (ignoreNewValue && !isAdjusting) {
                LOGGER.info("Reset the ignoreNewValue flag.");
                ignoreNewValue = false;
            }
        }
    }

    @Override
    protected void createJSlider(int minValue, int maxValue, int initialValue) {

        slider = new JZeroSlider(minValue, maxValue, initialValue);
    }

    public void setRowNumber(int rowNumber) {
        this.rowNumber = rowNumber;
    }

    public int getRowNumber() {
        return rowNumber;
    }

    @Override
    protected void prepareSliderTextLabel(int maxValue, int initialValue) {
        super.prepareSliderTextLabel(maxValue, initialValue);
        // the slider must not be opaque
        slider.setOpaque(false);
    }

    @Override
    protected void updateSliderTextValue(int value, boolean isSelected) {
        LOGGER.trace("updateSliderTextValue, value: {}, isSelected: {}", value, isSelected);

        if (port == null) {
            return;
        }
        String newValue = format.format(value);
        LOGGER.trace("Set the new value: {}", newValue);

        sliderValue.setText(newValue);
    }

    public void stop() {
        slider.setValue(0);
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        stopButton.setEnabled(enabled);
    }

    public void setPort(final MotorPort port) {
        this.port = port;
    }
}