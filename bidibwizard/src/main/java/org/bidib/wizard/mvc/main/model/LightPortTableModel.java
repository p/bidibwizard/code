package org.bidib.wizard.mvc.main.model;

import java.awt.Color;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.PortConfigKeys;
import org.bidib.wizard.comm.LightPortStatus;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.main.model.listener.LightPortListener;
import org.bidib.wizard.mvc.main.model.listener.PortListener;
import org.bidib.wizard.mvc.main.view.table.listener.ButtonListener;
import org.bidib.wizard.utils.PortUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LightPortTableModel
    extends SimplePortTableModel<LightPortStatus, LightPort, LightPortListener<LightPortStatus>>
    implements ButtonListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(LightPortTableModel.class);

    private static final long serialVersionUID = 1L;

    public static final int COLUMN_LABEL = 0;

    public static final int COLUMN_PWM_MIN = 1;

    public static final int COLUMN_PWM_MAX = 2;

    public static final int COLUMN_DIM_MAX = 3;

    public static final int COLUMN_DIM_MIN = 4;

    public static final int COLUMN_RGB = 5;

    public static final int COLUMN_TRANSITION_TIME = 6;

    public static final int COLUMN_PORT_IDENTIFIER = 7;

    public static final int COLUMN_STATUS = 8;

    public static final int COLUMN_TEST = 9;

    public static final int COLUMN_PORT_INSTANCE = 10;

    public LightPortTableModel(MainModel model) {
        super();

        model.addLightPortListener(new PortListener<LightPortStatus>() {
            @Override
            public void labelChanged(Port<LightPortStatus> port, String label) {
                LOGGER.info("The label has changed, port: {}, label: {}", port, label);
            }

            @Override
            public void statusChanged(Port<LightPortStatus> port, LightPortStatus status) {
                LOGGER.debug("The port status has changed: {}, port: {}, port.status: {}", status, port,
                    port.getStatus());
                updatePortStatus(port);
            }

            @Override
            public void configChanged(Port<LightPortStatus> port) {
                LOGGER.debug("The port config has been changed, port: {}", port);
                updatePortConfig((LightPort) port);
            }
        });
    }

    @Override
    protected int getColumnPortInstance() {
        return COLUMN_PORT_INSTANCE;
    }

    private void updatePortStatus(Port<LightPortStatus> port) {
        LOGGER.info("Update the port status: {}", port);
        for (int row = 0; row < getRowCount(); row++) {
            if (port.equals(getValueAt(row, COLUMN_PORT_INSTANCE))) {

                LOGGER.debug("The port state has changed: {}", port.getStatus());
                super.setValueAt(port.getStatus(), row, COLUMN_STATUS);
                LightPortStatus oppositeStatus = (LightPortStatus) PortUtils.getOppositeStatus(port.getStatus());
                LOGGER.info("Set the opposite state: {}", oppositeStatus);

                setValueAt(oppositeStatus, row, COLUMN_TEST);
                break;
            }
        }
    }

    private void updatePortConfig(LightPort port) {
        for (int row = 0; row < getRowCount(); row++) {
            if (port.equals(getValueAt(row, COLUMN_PORT_INSTANCE))) {
                LOGGER.debug("Found row to update: {}", row);
                fireTableRowsUpdated(row, row);
                break;
            }
        }
    }

    @Override
    protected void initialize() {
        columnNames =
            new String[] { Resources.getString(getClass(), "label"), Resources.getString(getClass(), "pwmMin"),
                Resources.getString(getClass(), "pwmMax"), Resources.getString(getClass(), "dimMin"),
                Resources.getString(getClass(), "dimMax"), Resources.getString(getClass(), "rgb"),
                Resources.getString(getClass(), "transitionTime"), Resources.getString(getClass(), "portIdentifier"),
                Resources.getString(getClass(), "status"), Resources.getString(getClass(), "test"), null };
    }

    public void addRow(LightPort port) {
        if (port != null) {
            Object[] rowData = new Object[columnNames.length];

            rowData[COLUMN_LABEL] = port.toString();
            rowData[COLUMN_PWM_MIN] = port.getPwmMin();
            rowData[COLUMN_PWM_MAX] = port.getPwmMax();
            rowData[COLUMN_DIM_MIN] = port.getDimMin();
            rowData[COLUMN_DIM_MAX] = port.getDimMax();
            rowData[COLUMN_RGB] = (port.getRgbValue() != null ? new Color(port.getRgbValue()) : null);
            rowData[COLUMN_TRANSITION_TIME] = port.getTransitionTime();
            rowData[COLUMN_PORT_IDENTIFIER] = port.getPortIdentifier();
            rowData[COLUMN_STATUS] = port.getStatus();

            LightPortStatus oppositeStatus = (LightPortStatus) PortUtils.getOppositeStatus(port.getStatus());
            rowData[COLUMN_TEST] = oppositeStatus;
            rowData[COLUMN_PORT_INSTANCE] = port;

            addRow(rowData);
        }
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        boolean isEditable = false;
        LightPort lightPort = (LightPort) getValueAt(row, COLUMN_PORT_INSTANCE);
        switch (column) {
            case COLUMN_LABEL:
                // the label can always be changed.
                return true;
            case COLUMN_STATUS:
                // the status can never be changed.
                return false;
            case COLUMN_PWM_MIN:
                // the config can always be changed
                if (lightPort.isEnabled()
                    && lightPort.isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_LEVEL_PORT_OFF)) {
                    isEditable = true;
                }
                break;
            case COLUMN_PWM_MAX:
                // the config can always be changed
                if (lightPort.isEnabled()
                    && lightPort.isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_LEVEL_PORT_ON)) {
                    isEditable = true;
                }
                break;
            case COLUMN_DIM_MIN:
                // the config can always be changed
                if (lightPort.isEnabled() && lightPort.isPortConfigKeySupported(
                    new byte[] { BidibLibrary.BIDIB_PCFG_DIMM_DOWN, BidibLibrary.BIDIB_PCFG_DIMM_DOWN_8_8 })) {
                    isEditable = true;
                }
                break;
            case COLUMN_DIM_MAX:
                // the config can always be changed
                if (lightPort.isEnabled() && lightPort.isPortConfigKeySupported(
                    new byte[] { BidibLibrary.BIDIB_PCFG_DIMM_UP, BidibLibrary.BIDIB_PCFG_DIMM_UP_8_8 })) {
                    isEditable = true;
                }
                break;
            case COLUMN_RGB:
                // the config can always be changed
                if (lightPort.isEnabled() && lightPort.isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_RGB)) {
                    isEditable = true;
                }
                break;
            case COLUMN_TRANSITION_TIME:
                // the config can always be changed
                if (lightPort.isEnabled()
                    && lightPort.isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_TRANSITION_TIME)) {
                    isEditable = true;
                }
                break;
            case COLUMN_TEST:
                // the test can be changed.
                if (lightPort.isEnabled()) {
                    isEditable = true;
                }
                break;
            default:
                break;
        }
        return isEditable;
    }

    @Override
    public Class<?> getColumnClass(int column) {
        switch (column) {
            case COLUMN_LABEL:
                return String.class;
            case COLUMN_RGB:
                return Color.class;
            case COLUMN_STATUS:
                return Object.class;
            case COLUMN_TEST:
                return Object.class;
            case COLUMN_PORT_INSTANCE:
            case COLUMN_PORT_IDENTIFIER:
                return Object.class;
            default:
                return Integer.class;
        }
    }

    @Override
    public void setValueAt(Object value, int row, int column) {
        final LightPort port = (LightPort) super.getValueAt(row, LightPortTableModel.COLUMN_PORT_INSTANCE);

        switch (column) {
            case COLUMN_LABEL:
                port.setLabel((String) value);
                super.setValueAt(port.toString(), row, column);
                fireLabelChanged(port, port.getLabel());
                break;
            case COLUMN_PWM_MIN:
                port.setPwmMin((Integer) value);
                super.setValueAt(value, row, column);
                fireValuesChanged(port, PortConfigKeys.BIDIB_PCFG_LEVEL_PORT_OFF);
                break;
            case COLUMN_PWM_MAX:
                port.setPwmMax((Integer) value);
                super.setValueAt(value, row, column);
                fireValuesChanged(port, PortConfigKeys.BIDIB_PCFG_LEVEL_PORT_ON);
                break;
            case COLUMN_DIM_MIN:
                try {
                    port.setDimMin(Integer.parseInt(value.toString()));
                    super.setValueAt(value, row, column);
                    fireValuesChanged(port, PortConfigKeys.BIDIB_PCFG_DIMM_DOWN);
                }
                catch (NumberFormatException e) {
                }
                break;
            case COLUMN_DIM_MAX:
                try {
                    port.setDimMax(Integer.parseInt(value.toString()));
                    super.setValueAt(value, row, column);
                    fireValuesChanged(port, PortConfigKeys.BIDIB_PCFG_DIMM_UP);
                }
                catch (NumberFormatException e) {
                }
                break;
            case COLUMN_RGB:
                Integer col = null;
                if (value instanceof Color) {
                    col = ((Color) value).getRGB();
                }
                else if (value instanceof Integer) {
                    col = ((Integer) value).intValue();
                }
                port.setRgbValue(col);
                super.setValueAt(value, row, column);
                fireValuesChanged(port, PortConfigKeys.BIDIB_PCFG_RGB);
                break;
            case COLUMN_TRANSITION_TIME:
                try {
                    port.setTransitionTime(Integer.parseInt(value.toString()));
                    super.setValueAt(value, row, column);
                    fireValuesChanged(port, PortConfigKeys.BIDIB_PCFG_TRANSITION_TIME);
                }
                catch (NumberFormatException e) {
                }
                break;
            case COLUMN_STATUS:
                port.setStatus((LightPortStatus) value);
                super.setValueAt(value, row, column);
                break;
            case COLUMN_TEST:

                LightPortStatus oppositeStatus = (LightPortStatus) value;

                port.setStatus(oppositeStatus);
                super.setValueAt(oppositeStatus, row, column);
                break;
        }
    }

    @Override
    public Object getValueAt(int row, int column) {
        final LightPort port = (LightPort) super.getValueAt(row, LightPortTableModel.COLUMN_PORT_INSTANCE);

        switch (column) {
            case COLUMN_PORT_IDENTIFIER:
            case COLUMN_LABEL:
            case COLUMN_TEST:
                column = COLUMN_PORT_INSTANCE;
                break;
            case COLUMN_DIM_MAX:
                return port.getDimMax();
            case COLUMN_DIM_MIN:
                return port.getDimMin();
            case COLUMN_PWM_MIN:
                return port.getPwmMin();
            case COLUMN_PWM_MAX:
                return port.getPwmMax();
            case COLUMN_RGB:
                return (port.getRgbValue() != null ? new Color(port.getRgbValue()) : null);
            case COLUMN_TRANSITION_TIME:
                return port.getTransitionTime();
            default:

                break;
        }
        return super.getValueAt(row, column);
    }

    private void fireValuesChanged(LightPort port, PortConfigKeys... portConfigKeys) {
        LOGGER.info("The values of the port have changed: {}", port);
        for (LightPortListener<LightPortStatus> l : portListeners) {
            l.valuesChanged(port, portConfigKeys);
        }
    }

    @Override
    public void buttonPressed(int row, int column) {
        LOGGER.info("The button was pressed, row: {}, column: {}", row, column);

        final Object portInstance = getValueAt(row, COLUMN_PORT_INSTANCE);
        if (portInstance instanceof LightPort) {
            final LightPort port = (LightPort) portInstance;
            fireTestButtonPressed(port);
        }
        else {
            LOGGER.warn("The current portInstance is not a LightPort: {}", portInstance);
        }
    }

    /**
     * Change the port type.
     * 
     * @param portType
     *            the new port type
     * @param port
     *            the port
     */
    public void changePortType(LcOutputType portType, LightPort port) {

        for (LightPortListener<LightPortStatus> l : portListeners) {
            l.changePortType(portType, port);
        }
    }
}
