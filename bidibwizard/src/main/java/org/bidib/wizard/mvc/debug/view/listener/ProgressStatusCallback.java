package org.bidib.wizard.mvc.debug.view.listener;

public interface ProgressStatusCallback {

    void statusChanged(int progressValue);

    void transferFinished();
}
