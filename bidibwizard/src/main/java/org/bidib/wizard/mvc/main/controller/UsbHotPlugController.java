package org.bidib.wizard.mvc.main.controller;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GradientPaint;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import org.apache.commons.collections4.collection.SynchronizedCollection;
import org.apache.commons.lang3.StringUtils;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.scm.HotPlugEventListener;
import org.bidib.jbidibc.scm.HotPlugEventWatcher;
import org.bidib.jbidibc.scm.UsbDevice;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.common.utils.ImageUtils;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.bidib.wizard.mvc.common.model.PreferencesPortType;
import org.bidib.wizard.mvc.common.model.PreferencesPortType.ConnectionPortType;
import org.bidib.wizard.mvc.main.controller.listener.AlertListener;
import org.bidib.wizard.mvc.main.controller.listener.AlertListener.AlertAction;
import org.bidib.wizard.mvc.main.view.menu.listener.MainMenuListener;
import org.bidib.wizard.mvc.main.view.statusbar.StatusBar;
import org.bidib.wizard.mvc.preferences.model.Preferences;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jidesoft.alert.Alert;
import com.jidesoft.alert.AlertGroup;
import com.jidesoft.animation.CustomAnimation;
import com.jidesoft.swing.JideButton;
import com.jidesoft.swing.JideSwingUtilities;
import com.jidesoft.swing.PaintPanel;

public class UsbHotPlugController {
    private static final Logger LOGGER = LoggerFactory.getLogger(UsbHotPlugController.class);

    private HotPlugEventWatcher watcher;

    private final ScheduledExecutorService hotplugWorker = Executors.newScheduledThreadPool(1);

    private final static class BidibAlertGroup extends AlertGroup {

        private Collection<Alert> listAlerts = SynchronizedCollection.synchronizedCollection(new LinkedList<Alert>());

        @Override
        public void add(Alert alert) {
            LOGGER.info("Add alert to group: {}", alert);
            try {

                super.add(alert);

                listAlerts.add(alert);
            }
            catch (Exception ex) {
                LOGGER.warn("Add alert to group failed.", ex);
            }
        }

        @Override
        public void remove(Alert alert) {
            LOGGER.info("Remove alert from group: {}", alert);

            try {
                super.remove(alert);

                listAlerts.remove(alert);
            }
            catch (Exception ex) {
                LOGGER.warn("Remove alert from group failed.", ex);
            }

        }

        public Collection<Alert> getAlerts() {
            return Collections.unmodifiableCollection(listAlerts);
        }
    }

    private BidibAlertGroup alertGroup = new BidibAlertGroup();

    public void start() {

        SwingUtilities.invokeLater(new Runnable() {
            public void run() {

                try {
                    LOGGER.info("Create and start HotPlugEventWatcher.");
                    watcher = new HotPlugEventWatcher();

                    final JFrame frame =
                        DefaultApplicationContext.getInstance().get(DefaultApplicationContext.KEY_MAIN_FRAME,
                            JFrame.class);

                    watcher.addEventListener(new HotPlugEventListener() {

                        @Override
                        public void usbDeviceRemoved(final UsbDevice usbDevice) {

                            SwingUtilities.invokeLater(new Runnable() {
                                public void run() {

                                    try {
                                        final String text =
                                            "<HTML><B>USB device removed</B><BR>" + "<FONT COLOR=BLUE>VID: 0x"
                                                + ByteUtils.intToHex(usbDevice.getUsbDevice().getVendorID())
                                                + "&nbsp;PID: 0x"
                                                + ByteUtils.intToHex(usbDevice.getUsbDevice().getProductID()) + "<BR>"
                                                + "Serial number: " + usbDevice.getUsbDevice().getSerialNumber()
                                                + "<BR>Product: " + usbDevice.getUsbDevice().getProductString()
                                                + "<BR>COM Port: " + usbDevice.getComPorts()[0] + "</FONT></HTML>";
                                        final String highlightText =
                                            "<HTML><U><B>USB device removed</B><BR>" + "<FONT COLOR=BLUE>VID: 0x"
                                                + ByteUtils.intToHex(usbDevice.getUsbDevice().getVendorID())
                                                + "&nbsp;PID: 0x"
                                                + ByteUtils.intToHex(usbDevice.getUsbDevice().getProductID()) + "<BR>"
                                                + "Serial number: " + usbDevice.getUsbDevice().getSerialNumber()
                                                + "<BR>Product: " + usbDevice.getUsbDevice().getProductString()
                                                + "<BR>COM Port: " + usbDevice.getComPorts()[0] + "</FONT><U></HTML>";

                                        // create the alert
                                        final Alert alert = createAlert(null, text, highlightText, usbDevice);

                                        notifyAlertAddedListeners(alert, AlertAction.DEVICE_REMOVED);

                                        showAlert(alert, frame);
                                    }
                                    catch (Exception ex) {
                                        LOGGER.warn("Show device removed alert failed.", ex);
                                    }
                                }
                            });
                        }

                        @Override
                        public void usbDeviceAdded(final UsbDevice usbDevice) {

                            SwingUtilities.invokeLater(new Runnable() {
                                public void run() {

                                    try {
                                        final String text =
                                            "<HTML><B>USB device detected</B><BR>" + "<FONT COLOR=BLUE>VID: 0x"
                                                + ByteUtils.intToHex(usbDevice.getUsbDevice().getVendorID())
                                                + "&nbsp;PID: 0x"
                                                + ByteUtils.intToHex(usbDevice.getUsbDevice().getProductID()) + "<BR>"
                                                + "Serial number: " + usbDevice.getUsbDevice().getSerialNumber()
                                                + "<BR>Product: " + usbDevice.getUsbDevice().getProductString()
                                                + "<BR>COM Port: " + usbDevice.getComPorts()[0] + "</FONT></HTML>";
                                        final String highlightText =
                                            "<HTML><U><B>USB device detected</B><BR>" + "<FONT COLOR=BLUE>VID: 0x"
                                                + ByteUtils.intToHex(usbDevice.getUsbDevice().getVendorID())
                                                + "&nbsp;PID: 0x"
                                                + ByteUtils.intToHex(usbDevice.getUsbDevice().getProductID()) + "<BR>"
                                                + "Serial number: " + usbDevice.getUsbDevice().getSerialNumber()
                                                + "<BR>Product: " + usbDevice.getUsbDevice().getProductString()
                                                + "<BR>COM Port: " + usbDevice.getComPorts()[0] + "</FONT><U></HTML>";

                                        String comPort = usbDevice.getComPorts()[0];
                                        // create the alert
                                        final Alert alert = createAlert(comPort, text, highlightText, usbDevice);
                                        LOGGER.info("Created alert: {}", alert);

                                        notifyAlertAddedListeners(alert, AlertAction.DEVICE_ADDED);

                                        showAlert(alert, frame);
                                    }
                                    catch (Exception ex) {
                                        LOGGER.warn("Show device added alert failed.", ex);
                                    }
                                }

                            });
                        }
                    });

                    watcherFuture = hotplugWorker.schedule(watcher, 0, TimeUnit.MILLISECONDS);
                    LOGGER.info("Create and start HotPlugEventWatcher passed.");
                }
                catch (Exception ex) {
                    LOGGER.warn("Start HotPlug EventListener for USB devices failed.", ex);
                }
            }
        });
    }

    private ScheduledFuture<?> watcherFuture;

    public void stopWatcher() {
        if (watcher != null) {
            LOGGER.info("Stop the watcher.");

            try {
                watcher.stop();
            }
            catch (Exception ex) {
                LOGGER.warn("Stop watcher failed.", ex);
            }

            if (watcherFuture != null) {
                LOGGER.info("Wait for termination of watcher.");
                try {
                    watcherFuture.get();
                }
                catch (Exception ex) {
                    LOGGER.warn("Stop watcher failed.", ex);
                }

                watcherFuture = null;
                LOGGER.info("Watcher has terminated.");
            }
        }
    }

    /**
     * Show the provided alert.
     * 
     * @param alert
     *            the alert
     * @param owner
     *            the owner component
     */
    public void showAlert(final Alert alert, final Component owner) {
        alertGroup.add(alert);

        alert.setAlertGroup(alertGroup);

        alertGroup.add(alert);
        alert.showPopup(SwingConstants.SOUTH_EAST, owner);
    }

    private static class BidibAlert extends Alert {

        private static final long serialVersionUID = 1L;

        private final String comPort;

        private final UsbDevice usbDevice;

        public BidibAlert(final String comPort, final UsbDevice usbDevice) {
            this.comPort = comPort;
            this.usbDevice = usbDevice;
        }

        /**
         * @return the com port
         */
        public String getComPort() {
            return comPort;
        }

        /**
         * @return the usbDevice
         */
        public UsbDevice getUsbDevice() {
            return usbDevice;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof BidibAlert) {
                return ((BidibAlert) obj).usbDevice.equals(usbDevice);
            }
            return false;
        }

        @Override
        public int hashCode() {
            return usbDevice.hashCode();
        }
    }

    private Alert createAlert(final String comPort, String text, String highlightText, final UsbDevice usbDevice) {

        // create the alert
        final BidibAlert alert = new BidibAlert(comPort, usbDevice);
        alert.getContentPane().setLayout(new BorderLayout());

        ActionListener flagAction = null;

        if (StringUtils.isNotBlank(comPort)) {
            flagAction = new ActionListener() {
                public void actionPerformed(ActionEvent e) {

                    LOGGER.info("Use the COM port to connect: {}", comPort);

                    PreferencesPortType portType = PreferencesPortType.getValue(ConnectionPortType.SerialPort.name());
                    portType.setConnectionName(comPort);

                    final Preferences preferences = Preferences.getInstance();
                    preferences.setSelectedPortType(portType);
                    preferences.save(null);

                    StatusBar statusBar = DefaultApplicationContext.getInstance().get("statusBar", StatusBar.class);
                    if (statusBar != null) {
                        statusBar.setStatus(
                            Resources.getString(UsbHotPlugController.class, "message.comport-selected", comPort));
                    }

                    AlertGroup alertGroup = alert.getAlertGroup();
                    alert.hidePopupImmediately();

                    if (alertGroup instanceof BidibAlertGroup) {
                        BidibAlertGroup bidibAlertGroup = (BidibAlertGroup) alertGroup;
                        bidibAlertGroup.remove(alert);

                        Collection<Alert> alerts = new LinkedList<>(bidibAlertGroup.getAlerts());
                        LOGGER.info("Current alerts in AlertGroup: {}", alerts);

                        for (Alert current : alerts) {
                            try {
                                LOGGER.info("Hide alert: {}", current);
                                current.hidePopupImmediately();
                                bidibAlertGroup.remove(current);
                            }
                            catch (Exception ex) {
                                LOGGER.warn("Hide alert failed.", ex);
                            }

                            notifyAlertRemoveListeners(alert);
                        }
                    }
                    else {
                        LOGGER.info("No AlertGroup assigned to alert: {}", alert);
                    }

                    // trigger connect
                    try {
                        MainMenuListener mainMenuListener =
                            DefaultApplicationContext.getInstance().get(DefaultApplicationContext.KEY_MAINMENU_LISTENER,
                                MainMenuListener.class);

                        LOGGER.info("Try to connect to comPort:{}", comPort);
                        mainMenuListener.connect();
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Connect to com port failed: {}", comPort, ex);
                    }
                }
            };
        }

        alert.getContentPane().add(createUSBSerialAlert(flagAction, new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                try {
                    alert.hidePopupImmediately();
                }
                catch (Exception ex) {
                    LOGGER.warn("Hide popup immediately failed.", ex);
                }

                notifyAlertRemoveListeners(alert);
            }
        }, text, highlightText));
        alert.setResizable(true);
        alert.setMovable(true);
        // alert.setTimeout(15000);

        // Sets the transient attribute. If a popup is transient, it will hide automatically when mouse is clicked
        // outside the popup. Otherwise, it will stay visible until timeout or hidePopup() is called.
        alert.setTransient(true);

        CustomAnimation animation =
            new CustomAnimation(CustomAnimation.TYPE_ENTRANCE, CustomAnimation.EFFECT_FADE,
                CustomAnimation.SMOOTHNESS_SMOOTH, CustomAnimation.SPEED_VERY_FAST) {
                private static final long serialVersionUID = 1L;

                @Override
                public void start(Component source) {

                    try {
                        super.start(source);
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Start show animation failed.", ex);
                    }
                }
            };
        animation.setFunctionFade(CustomAnimation.FUNC_BOUNCE);
        alert.setShowAnimation(animation);
        CustomAnimation animationExit =
            new CustomAnimation(CustomAnimation.TYPE_EXIT, CustomAnimation.EFFECT_FADE,
                CustomAnimation.SMOOTHNESS_SMOOTH, CustomAnimation.SPEED_MEDIUM) {
                private static final long serialVersionUID = 1L;

                @Override
                public void start(Component source) {

                    try {
                        super.start(source);
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Start exit animation failed.", ex);
                    }
                }
            };
        animationExit.setFunctionFade(CustomAnimation.FUNC_POW_HALF);
        alert.setHideAnimation(animationExit);

        // add listener when the alert gets invisible
        alert.addPropertyChangeListener("visible", new PropertyChangeListener() {
            public void propertyChange(PropertyChangeEvent evt) {
                LOGGER.info("Received pce: {}", evt);
                if (Boolean.FALSE.equals(evt.getNewValue())) {

                    LOGGER.info("Remove alert from list: {}", alert);

                    try {
                        AlertGroup alertGroup = alert.getAlertGroup();
                        if (alertGroup instanceof BidibAlertGroup) {
                            BidibAlertGroup bidibAlertGroup = (BidibAlertGroup) alertGroup;
                            bidibAlertGroup.remove(alert);
                        }
                        else {
                            LOGGER.info("No AlertGroup assigned to alert: {}", alert);
                        }
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Remove alert from group failed.", ex);
                    }

                    notifyAlertRemoveListeners(alert);
                }
            }
        });

        alert.setPopupBorder(BorderFactory.createLineBorder(new Color(10, 30, 106)));

        return alert;
    }

    private JideButton createButton(Icon icon) {
        JideButton button = new JideButton(icon);
        return button;
    }

    private JComponent createUSBSerialAlert(
        final ActionListener flagAction, ActionListener closeAction, final String text, final String highlightText) {
        JPanel bottomPanel = new JPanel(new GridLayout(1, 2, 0, 0));
        if (flagAction != null) {
            JideButton flagButton = createButton(ImageUtils.createImageIcon(getClass(), "/icons/16x16/connect.png"));
            flagButton.addActionListener(flagAction);
            bottomPanel.add(flagButton);
        }
        JideButton deleteButton = createButton(ImageUtils.createImageIcon(getClass(), "/icons/alert/delete.png"));
        deleteButton.addActionListener(closeAction);
        bottomPanel.add(deleteButton);

        JPanel leftPanel = new JPanel(new BorderLayout(6, 6));
        leftPanel.add(new JLabel(ImageUtils.createImageIcon(getClass(), "/icons/alert/usb_plug.png")));
        leftPanel.add(bottomPanel, BorderLayout.AFTER_LAST_LINE);

        JPanel rightPanel = new JPanel(new GridLayout(1, 2, 0, 0));
        rightPanel.add(createButton(ImageUtils.createImageIcon(getClass(), "/icons/alert/option.png")));
        JideButton closeButton = createButton(ImageUtils.createImageIcon(getClass(), "/icons/alert/close.png"));
        closeButton.addActionListener(closeAction);
        rightPanel.add(closeButton);

        final JLabel message = new JLabel(text);

        message.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                message.setText(highlightText);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                message.setText(text);
            }
        });

        PaintPanel panel = new PaintPanel(new BorderLayout(6, 6));
        panel.setBorder(BorderFactory.createEmptyBorder(6, 7, 7, 7));
        panel.add(message, BorderLayout.CENTER);
        JPanel topPanel = JideSwingUtilities.createTopPanel(rightPanel);
        panel.add(topPanel, BorderLayout.AFTER_LINE_ENDS);
        panel.add(leftPanel, BorderLayout.BEFORE_LINE_BEGINS);
        for (int i = 0; i < panel.getComponentCount(); i++) {
            JideSwingUtilities.setOpaqueRecursively(panel.getComponent(i), false);
        }
        panel.setOpaque(true);
        panel.setBackgroundPaint(new GradientPaint(0, 0, new Color(231, 229, 224), 0, panel.getPreferredSize().height,
            new Color(212, 208, 200)));
        return panel;
    }

    private final List<AlertListener> alertListeners = new LinkedList<>();

    public void addAlertListener(final AlertListener alertListener) {
        alertListeners.add(alertListener);
    }

    public void removeAlertListener(final AlertListener alertListener) {
        alertListeners.remove(alertListener);
    }

    private void notifyAlertAddedListeners(final Alert alert, AlertAction alertAction) {
        LOGGER.info("Notify the alertListeners about added, alertAction: {}, alert: {}", alertAction, alert);

        for (AlertListener listener : alertListeners) {
            listener.alertAdded(alert, alertAction);
        }
    }

    private void notifyAlertRemoveListeners(final Alert alert) {
        LOGGER.info("Notify the alertListeners about remove, alert: {}", alert);
        for (AlertListener listener : alertListeners) {
            listener.alertRemoved(alert);
        }
    }

}
