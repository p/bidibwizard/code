package org.bidib.wizard.mvc.main.view.menu;

import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

public class BasicPopupMenu extends JPopupMenu {
    private static final long serialVersionUID = 1L;

    @Override
    public void addSeparator() {
        add(new Separator());
    }

    @Override
    public JMenuItem add(JMenuItem menuItem) {
        menuItem.addMouseListener(new MouseListener() {
            @Override
            public void mouseEntered(MouseEvent e) {
                ((JMenuItem) e.getSource()).setArmed(true);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                ((JMenuItem) e.getSource()).setArmed(false);
            }

            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }
        });
        return super.add(menuItem);
    }

    public static void addMenuItem(Object menu, JMenuItem menuItem) {
        if (menu instanceof JMenu) {
            ((JMenu) menu).add(menuItem);
        }
        else if (menu instanceof JPopupMenu) {
            ((JPopupMenu) menu).add(menuItem);
        }
    }

    public static final class Separator extends JPopupMenu.Separator {
        private static final long serialVersionUID = 1L;

        public Dimension getPreferredSize() {
            Dimension d = super.getPreferredSize();

            if (d.height == 0) {
                d.height = 4;
            }
            return d;
        }
    }
}
