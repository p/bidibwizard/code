package org.bidib.wizard.mvc.main.model;

import java.util.HashMap;

@Deprecated
public class NodeLabels extends Labels<Long, String> {
    private static final long serialVersionUID = 1L;

    private class NodeMap extends HashMap<Long, String> {
        private static final long serialVersionUID = 1L;
    }

    public NodeLabels() {
        labelMap = new NodeMap();
    }

    public String getLabel(long uuid) {
        return labelMap.get(uuid);
    }

    public void removeLabel(long uuid) {
        String oldValue = labelMap.remove(uuid);
        modified = true;

        fireLabelsChanged(oldValue, null);
    }

    public void setLabel(long uuid, String value) {
        String oldValue = labelMap.put(uuid, value);
        modified = true;

        fireLabelsChanged(oldValue, value);
    }
}
