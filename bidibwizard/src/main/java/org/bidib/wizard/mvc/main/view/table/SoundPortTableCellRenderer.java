package org.bidib.wizard.mvc.main.view.table;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import org.bidib.wizard.mvc.main.model.SoundPort;
import org.bidib.wizard.mvc.main.model.SoundPortTableModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SoundPortTableCellRenderer extends DefaultTableCellRenderer {
    private static final Logger LOGGER = LoggerFactory.getLogger(SoundPortTableCellRenderer.class);

    private static final long serialVersionUID = 1L;

    private final int forColumn;

    public SoundPortTableCellRenderer(int forColumn) {
        this.forColumn = forColumn;
    }

    public Component getTableCellRendererComponent(
        JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

        // call super to set the correct color if selected
        super.getTableCellRendererComponent(table, null, isSelected, hasFocus, row, column);

        // renderer only handles SoundPorts
        if (value instanceof SoundPort) {
            SoundPort soundPort = (SoundPort) value;
            boolean enabled = soundPort.isEnabled();
            setEnabled(enabled);
            setIcon(null);

            switch (forColumn) {
                case SoundPortTableModel.COLUMN_PULSE_TIME:
                    setText(Integer.toString(soundPort.getPulseTime()));
                    break;
                default:
                    // should never happen
                    LOGGER.warn("Invalid renderer configuration detected, column: {}", column);
                    break;
            }
        }
        else {
            setEnabled(false);
            setText(null);
        }

        return this;
    }
}