package org.bidib.wizard.mvc.main.view.panel.listener;

import org.bidib.wizard.mvc.main.model.Macro;

public interface MacroListListener extends LabelChangedListener<Macro> {
    /**
     * Export the macro to an external file.
     * 
     * @param macro
     *            the macro
     */
    void exportMacro(Macro macro);

    /**
     * Import the macro from an external file.
     */
    void importMacro();

    /**
     * Reload macro from node.
     * 
     * @param macro
     *            the macro
     */
    void reloadMacro(Macro macro);

    /**
     * Write the macro to the node into persistent memory.
     * 
     * @param macro
     *            the macro
     */
    void saveMacro(Macro macro);

    /**
     * Start the macro.
     * 
     * @param macro
     *            the macro
     * @param transferBeforeStart
     *            transfer macro to node before start
     */
    void startMacro(Macro macro, boolean transferBeforeStart);

    /**
     * Stop the macro.
     * 
     * @param macro
     *            the macro
     */
    void stopMacro(Macro macro);

    /**
     * Clear the entries of the macro and set default values for cycles.
     * 
     * @param macro
     *            the macro
     */
    void initializeMacro(Macro macro);

    /**
     * Write the macro to the node into transient memory.
     * 
     * @param macro
     *            the macro
     */
    void transferMacro(Macro macro);
}
