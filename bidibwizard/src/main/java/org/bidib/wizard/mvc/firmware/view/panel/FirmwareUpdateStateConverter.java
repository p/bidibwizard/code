package org.bidib.wizard.mvc.firmware.view.panel;

import org.apache.commons.lang.StringUtils;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.firmware.model.UpdateStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.value.BindingConverter;

/**
 * Converts Values to Strings and vice-versa using a given Format.
 */
public final class FirmwareUpdateStateConverter implements BindingConverter<UpdateStatus, String> {
    private static final Logger LOGGER = LoggerFactory.getLogger(FirmwareUpdateStateConverter.class);

    // Implementing Abstract Behavior *************************************

    /**
     * Formats the source value and returns a String representation.
     * 
     * @param sourceValue
     *            the source value
     * @return the formatted sourceValue
     */
    @Override
    public String targetValue(UpdateStatus sourceValue) {
        if (sourceValue instanceof UpdateStatus) {
            UpdateStatus updateState = (UpdateStatus) sourceValue;
            return Resources.getString(UpdateStatus.class, updateState.getCode());
        }
        return null;
    }

    /**
     * Parses the given String encoding and sets it as the subject's new value. Silently catches {@code ParseException}.
     * 
     * @param targetValue
     *            the value to be converted and set as new subject value
     */
    @Override
    public UpdateStatus sourceValue(String targetValue) {
        try {
            if (StringUtils.isNotBlank((String) targetValue)) {
                return UpdateStatus.valueOf((String) targetValue);
            }
        }
        catch (Exception e) {
            LOGGER.warn("Cannot convert the target value to UpdateStatus: {}", targetValue);
        }
        return null;
    }
}
