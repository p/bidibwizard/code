package org.bidib.wizard.mvc.main.model;

import java.util.Collection;
import java.util.LinkedList;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.wizard.comm.ServoPortStatus;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.main.model.listener.ServoPortListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServoPortTableModel
    extends SimplePortTableModel<ServoPortStatus, ServoPort, ServoPortListener<ServoPortStatus>> {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ServoPortTableModel.class);

    private final Collection<ServoPortListener<ServoPortStatus>> servoPortListeners =
        new LinkedList<ServoPortListener<ServoPortStatus>>();

    private final MainModel mainModel;

    public static final int COLUMN_LABEL = 0;

    public static final int COLUMN_SPEED = 1;

    public static final int COLUMN_TRIM_DOWN = 2;

    public static final int COLUMN_TRIM_UP = 3;

    public static final int COLUMN_VALUE = 4;

    public static final int COLUMN_PORT_INSTANCE = 5;

    private static final String[] COLUMN_NAMES =
        { Resources.getString(ServoPortTableModel.class, "label"),
            Resources.getString(ServoPortTableModel.class, "speed"),
            Resources.getString(ServoPortTableModel.class, "trimDown"),
            Resources.getString(ServoPortTableModel.class, "trimUp"),
            Resources.getString(ServoPortTableModel.class, "value"), null };

    public ServoPortTableModel(final MainModel model) {
        this.mainModel = model;
        setColumnIdentifiers(COLUMN_NAMES);
    }

    @Override
    protected int getColumnPortInstance() {
        return COLUMN_PORT_INSTANCE;
    }

    public MainModel getMainModel() {
        return mainModel;
    }

    public void addServoPortListener(ServoPortListener<ServoPortStatus> l) {
        servoPortListeners.add(l);
    }

    public void addRow(ServoPort servoPort) {
        if (servoPort != null) {
            Object[] rowData = new Object[COLUMN_NAMES.length];

            rowData[COLUMN_LABEL] = servoPort.toString();
            rowData[COLUMN_SPEED] = servoPort.getSpeed();
            rowData[COLUMN_TRIM_DOWN] = servoPort.getTrimDown();
            rowData[COLUMN_TRIM_UP] = servoPort.getTrimUp();
            rowData[COLUMN_VALUE] = servoPort.getValue();
            rowData[COLUMN_PORT_INSTANCE] = servoPort;
            addRow(rowData);
        }
        else {
            LOGGER.warn("Performed addRow without servo port instance.");
        }
    }

    private void fireLabelChanged(ServoPort servoPort, String label) {
        for (ServoPortListener<ServoPortStatus> l : servoPortListeners) {
            l.labelChanged(servoPort, label);
        }
    }

    private void fireConfigValuesChanged(ServoPort servoPort) {
        for (ServoPortListener<ServoPortStatus> l : servoPortListeners) {
            l.valuesChanged(servoPort);
        }
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        boolean isEditable = false;
        ServoPort servoPort = (ServoPort) getValueAt(row, COLUMN_PORT_INSTANCE);
        switch (column) {
            case COLUMN_SPEED:
                if (servoPort.isEnabled() && servoPort.isPortConfigEnabled()
                    && servoPort.isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_SERVO_SPEED)) {
                    isEditable = true;
                }
                break;
            case COLUMN_TRIM_DOWN:
                if (servoPort.isEnabled() && servoPort.isPortConfigEnabled()
                    && servoPort.isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_SERVO_ADJ_L)) {
                    isEditable = true;
                }
                break;
            case COLUMN_TRIM_UP:
                if (servoPort.isEnabled() && servoPort.isPortConfigEnabled()
                    && servoPort.isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_SERVO_ADJ_H)) {
                    isEditable = true;
                }
                break;
            default:
                if (servoPort.isEnabled()) {
                    isEditable = true;
                }
                break;
        }
        return isEditable;
    }

    @Override
    public Class<?> getColumnClass(int column) {
        switch (column) {
            case COLUMN_LABEL:
                return String.class;
            default:
                return Integer.class;
        }
    }

    @Override
    public Object getValueAt(int row, int column) {
        Object result = null;
        switch (column) {
            case COLUMN_LABEL:
                column = COLUMN_PORT_INSTANCE;
                break;
            case COLUMN_VALUE:
                ServoPort port = (ServoPort) super.getValueAt(row, COLUMN_PORT_INSTANCE);

                if (port != null) {
                    // return the value for the target position
                    result = port.getRelativeValue();
                }
                break;
            default:
                column = COLUMN_PORT_INSTANCE;
                break;
        }
        if (result == null) {
            result = super.getValueAt(row, column);
        }
        return result;
    }

    @Override
    public void setValueAt(Object value, int row, int column) {
        final ServoPort port = (ServoPort) getValueAt(row, COLUMN_PORT_INSTANCE);

        switch (column) {
            case COLUMN_LABEL:
                port.setLabel((String) value);
                super.setValueAt(port.toString(), row, column);
                fireLabelChanged(port, port.getLabel());
                break;
            case COLUMN_SPEED:
                try {
                    port.setSpeed(Integer.parseInt(value.toString()));
                    super.setValueAt(value, row, column);
                    fireConfigValuesChanged(port);
                }
                catch (NumberFormatException e) {
                }
                break;
            case COLUMN_TRIM_DOWN:
                if (port.getTrimDown() != (Integer) value) {
                    port.setTrimDown((Integer) value);
                    super.setValueAt(value, row, column);
                    fireConfigValuesChanged(port);
                    // this is used to update the value slider
                    // super.setValueAt(port.getValue(), row, COLUMN_VALUE);
                }
                break;
            case COLUMN_TRIM_UP:
                if (port.getTrimUp() != (Integer) value) {
                    port.setTrimUp((Integer) value);
                    super.setValueAt(value, row, column);
                    fireConfigValuesChanged(port);
                    // this is used to update the value slider
                    // super.setValueAt(port.getValue(), row, COLUMN_VALUE);
                }
                break;
            case COLUMN_VALUE:
                // set the value for the target position
                int intValue = port.getAbsoluteValue(Integer.parseInt(value.toString()));

                if (port.getValue() != intValue) {
                    port.setValue(intValue);
                    super.setValueAt(intValue, row, column);
                }
                break;
        }
    }
}
