package org.bidib.wizard.mvc.simulation.model;

import org.bidib.jbidibc.core.message.BidibMessage;

@Deprecated
public interface Simulator {

    void start();

    void stop();

    void processRequest(final BidibMessage bidibMessage);

    String getSimulationPanelClass();

    void queryStatus(Class<?> portClass);
}
