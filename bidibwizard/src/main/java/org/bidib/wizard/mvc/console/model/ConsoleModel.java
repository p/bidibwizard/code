package org.bidib.wizard.mvc.console.model;

import java.awt.Color;
import java.util.LinkedList;
import java.util.List;

import org.bidib.wizard.main.DefaultApplicationContext;

import com.jgoodies.binding.beans.Model;

public class ConsoleModel extends Model {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_CONSOLE_CONTENT = "consoleContent";

    public static final String PROPERTY_CONSOLE_CONTENT_SIZE = "consoleContentSize";

    private List<ConsoleLine> consoleMessages = new LinkedList<>();

    public static final class ConsoleLine {
        private final Color color;

        private final String message;

        public ConsoleLine(Color color, String message) {
            this.color = color;
            this.message = message;
        }

        /**
         * @return the color
         */
        public Color getColor() {
            return color;
        }

        /**
         * @return the message
         */
        public String getMessage() {
            return message;
        }
    }

    public void addConsoleLine(Color color, String line) {
        ConsoleLine consoleLine = new ConsoleLine(color, line);

        // only keep 250 in memory
        if (consoleMessages.size() > 250) {
            consoleMessages.remove(0);
        }

        consoleMessages.add(consoleLine);

        int index = consoleMessages.size() - 1;

        fireIndexedPropertyChange(PROPERTY_CONSOLE_CONTENT, index, null, consoleLine);
    }

    public void clear() {
        int oldSize = consoleMessages.size();

        consoleMessages.clear();

        firePropertyChange(PROPERTY_CONSOLE_CONTENT_SIZE, oldSize, consoleMessages.size());
    }

    public static synchronized ConsoleModel getConsoleModel() {
        ConsoleModel consoleModel =
            DefaultApplicationContext
                .getInstance().get(DefaultApplicationContext.KEY_CONSOLE_MODEL, ConsoleModel.class);
        if (consoleModel == null) {
            consoleModel = new ConsoleModel();
            DefaultApplicationContext.getInstance().register(DefaultApplicationContext.KEY_CONSOLE_MODEL, consoleModel);
        }

        return consoleModel;
    }
}
