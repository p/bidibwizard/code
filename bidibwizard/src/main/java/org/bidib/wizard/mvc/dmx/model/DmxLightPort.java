package org.bidib.wizard.mvc.dmx.model;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.LightPortEnum;
import org.bidib.wizard.comm.LightPortStatus;
import org.bidib.wizard.mvc.main.model.LightPort;
import org.bidib.wizard.mvc.main.model.Port;

/**
 * Wrapper for LightPort that adds DMX specific attributes.
 * 
 */
public class DmxLightPort extends Port<LightPortStatus> {
    private static final long serialVersionUID = 1L;

    private final LightPort lightPort;

    private DmxChannel dmxTargetChannel;

    private final PropertyChangeListener listener;

    public DmxLightPort(final LightPort lightPort) {
        super(null);

        this.lightPort = lightPort;

        listener = new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                firePropertyChange(evt.getPropertyName(), evt.getOldValue(), evt.getNewValue());
            }
        };

        this.lightPort.addPropertyChangeListener(listener);
    }

    public void freeLightPort() {
        this.lightPort.removePropertyChangeListener(listener);
    }

    /**
     * @return the lightPort
     */
    public LightPort getLightPort() {
        return lightPort;
    }

    /**
     * @return the dmxTargetChannel
     */
    public DmxChannel getDmxTargetChannel() {
        return dmxTargetChannel;
    }

    /**
     * @param dmxTargetChannel
     *            the dmxTargetChannel to set
     */
    public void setDmxTargetChannel(DmxChannel dmxTargetChannel) {
        this.dmxTargetChannel = dmxTargetChannel;
    }

    public int getId() {
        return lightPort.getId();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof DmxLightPort) {
            return ((DmxLightPort) obj).getId() == getId();
        }
        // return super.equals(obj);
        return false;
    }

    @Override
    public int hashCode() {
        return getId();
    }

    @Override
    public byte[] getPortConfig() {
        return lightPort.getPortConfig();
    }

    @Override
    public String getLabel() {
        return lightPort.getLabel();
    }

    @Override
    public LightPortStatus getStatus() {
        return lightPort.getStatus();
    }

    @Override
    public void setStatus(LightPortStatus status) {
        lightPort.setStatus(status);
    }

    @Override
    public String toString() {
        return lightPort.toString();
    }

    @Override
    protected LcOutputType getPortType() {
        return LcOutputType.LIGHTPORT;
    }

    @Override
    protected LightPortStatus internalGetStatus() {
        return LightPortStatus.valueOf(LightPortEnum.valueOf(genericPort.getPortStatus()));
    }
}
