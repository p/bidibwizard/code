package org.bidib.wizard.mvc.script.model;

public class NodeScriptData {
    private final String application;

    private final String instruction;

    private final String filePath;

    private final String hyperlink;

    private final boolean disabled;

    public NodeScriptData(String application, String instruction, String hyperlink, String filePath, boolean disabled) {
        this.application = application;
        this.instruction = instruction;
        this.filePath = filePath;
        this.hyperlink = hyperlink;
        this.disabled = disabled;
    }

    /**
     * @return the instruction
     */
    public String getInstruction() {
        return instruction;
    }

    /**
     * @return the hyperlink
     */
    public String getHyperlink() {
        return hyperlink;
    }

    /**
     * @return the filePath
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * @return the item is disabled
     */
    public boolean isDisabled() {
        return disabled;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("<html>");
        sb.append(application).append("</html>");
        return sb.toString();
    }
}
