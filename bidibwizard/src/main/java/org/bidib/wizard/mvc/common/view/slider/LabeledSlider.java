package org.bidib.wizard.mvc.common.view.slider;

import java.awt.Dimension;

import javax.swing.BoundedRangeModel;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.CellEditor;
import javax.swing.DefaultBoundedRangeModel;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.bidib.jbidibc.ui.LogarithmicJSlider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LabeledSlider extends JPanel {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(LabeledSlider.class);

    protected JSlider slider;

    protected SliderValueLabel sliderValue;

    private SliderValueChangeListener sliderValueChangeListener;

    protected class SliderValueLabel extends JLabel {
        private static final long serialVersionUID = 1L;

        @Override
        public void setText(String text) {
            LOGGER.debug("Set the text of slider value: {}", text);
            super.setText(text);
        }
    }

    /**
     * Creates a new slider editor instance with the initial value set.
     * 
     * @param initialValue
     *            the initial value
     * @param minValue
     *            the minimum value
     * @param maxValue
     *            the maximum value
     * @param changeListener
     *            the change listener
     * @param useLogarithmicSlider
     *            use logarithmic slider
     */
    public LabeledSlider(int initialValue, int minValue, int maxValue, final SliderValueChangeListener changeListener,
        boolean useLogarithmicSlider) {
        this(new DefaultBoundedRangeModel(initialValue, 0, minValue, maxValue), changeListener, useLogarithmicSlider);
    }

    public LabeledSlider(BoundedRangeModel brm, final SliderValueChangeListener changeListener,
        boolean useLogarithmicSlider) {
        setFocusCycleRoot(true);
        sliderValueChangeListener = changeListener;

        if (useLogarithmicSlider) {
            slider = new LogarithmicJSlider(brm);
        }
        else {
            slider = new JSlider(brm);
        }
    }

    /*
     * Create the component and initialize the labels. This method must be called before the component can be used.
     * 
     * @param initialValue the initial value
     */
    public void createComponent() {

        sliderValue = new SliderValueLabel();

        slider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                JSlider source = (JSlider) e.getSource();

                updateSliderTextValue(source.getValue());

                if (!source.getValueIsAdjusting()) {

                    if (getParent() instanceof CellEditor) {
                        ((CellEditor) getParent()).stopCellEditing();
                    }

                    if (sliderValueChangeListener != null) {
                        // forward event
                        sliderValueChangeListener.stateChanged(e, false, source.getValue());
                    }
                }
                else {
                    if (sliderValueChangeListener != null) {
                        // forward event
                        sliderValueChangeListener.stateChanged(e, true, source.getValue());
                    }
                }

            }
        });

        prepareSliderTextLabel(slider.getMaximum(), slider.getValue());

        setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
        add(slider);

        add(Box.createRigidArea(new Dimension(5, 0)));
        add(sliderValue);
        add(Box.createRigidArea(new Dimension(3, 0)));

        validate();
    }

    protected void prepareSliderTextLabel(int maxValue, int initialValue) {
        sliderValue.setHorizontalAlignment(SwingConstants.TRAILING);

        sliderValue.setText(Integer.toString(maxValue));

        Dimension d = sliderValue.getMinimumSize();
        LOGGER.debug("Set the initial value: {}", initialValue);
        sliderValue.setText(Integer.toString(initialValue));

        LOGGER.trace("Current dimension: {}", d);

        sliderValue.setPreferredSize(d);
        sliderValue.setMaximumSize(d);
        sliderValue.setMinimumSize(d);
    }

    /**
     * @param value
     *            the relative value to set
     */
    protected void updateSliderTextValue(int value) {

        String newValue = Integer.toString(value);
        LOGGER.trace("Set the new value: {}", newValue);
        sliderValue.setText(newValue);
    }

    public void setSliderValueChangeListener(SliderValueChangeListener sliderValueChangeListener) {
        this.sliderValueChangeListener = sliderValueChangeListener;
    }

    public void setDown() {
        LOGGER.trace("set slider value down: {}", slider.getValue());
        slider.setValue(slider.getValue() - 1);
    }

    public void setUp() {
        LOGGER.trace("set slider value up: {}", slider.getValue());
        slider.setValue(slider.getValue() + 1);
    }

    public void setValue(int value) {
        LOGGER.debug("Set the new value: {}", value);
        slider.getModel().setValueIsAdjusting(true);
        slider.setValue(value);
        sliderValue.setText(Integer.toString(value));
        slider.getModel().setValueIsAdjusting(false);
    }

    @Override
    public void setEnabled(boolean enabled) {
        slider.setEnabled(enabled);
        sliderValue.setEnabled(enabled);
    }
}
