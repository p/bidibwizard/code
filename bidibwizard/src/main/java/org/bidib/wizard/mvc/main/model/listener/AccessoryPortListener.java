package org.bidib.wizard.mvc.main.model.listener;

public interface AccessoryPortListener {
    /**
     * The test button was pressed.
     * 
     * @param aspectId
     *            the aspect number
     */
    void testButtonPressed(int aspectId);

    /**
     * The label of the aspect was changed.
     * 
     * @param aspectId
     *            the aspect number
     */
    void labelChanged(int aspectId);
}
