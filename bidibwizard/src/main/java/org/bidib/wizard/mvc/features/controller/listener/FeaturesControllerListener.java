package org.bidib.wizard.mvc.features.controller.listener;

import org.bidib.jbidibc.core.Feature;
import org.bidib.wizard.mvc.main.model.Node;

public interface FeaturesControllerListener {

    /**
     * Read all features of the specified node.
     * 
     * @param node
     *            the node
     */
    void readAll(Node node);

    void write(Node node, Feature feature);

    void close();
}
