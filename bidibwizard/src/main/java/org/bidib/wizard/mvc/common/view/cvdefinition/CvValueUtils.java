package org.bidib.wizard.mvc.common.view.cvdefinition;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.bidib.jbidibc.core.enumeration.SysErrorEnum;
import org.bidib.jbidibc.core.node.ConfigurationVariable;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.view.cvdef.CvNode;
import org.bidib.wizard.mvc.main.view.cvdef.LongCvNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jidesoft.grid.DefaultExpandableRow;

public class CvValueUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(CvValueUtils.class);

    public static void writeCvValues(
        final Node selectedNode, final DefaultExpandableRow treeNode, final Map<String, CvNode> cvNumberToNodeMap,
        final CvDefintionPanelProvider cvDefintionPanelProvider) {
        LOGGER.info("Write the CV values.");
        List<ConfigurationVariable> cvList = new LinkedList<ConfigurationVariable>();

        collectNewValues(treeNode, cvList);

        writeCvValues(selectedNode, cvList, cvNumberToNodeMap, cvDefintionPanelProvider);
    }

    /**
     * Write the CV values provided in {@code cvList} parameter via the {@code cvDefinitionPanelProvider} to the node.
     * 
     * @param selectedNode
     *            the selected node
     * @param cvList
     *            the list of new CV values
     * @param cvNumberToNodeMap
     *            the map
     * @param cvDefinitionPanelProvider
     *            the CV definition panel provider
     */
    public static void writeCvValues(
        final Node selectedNode, final List<ConfigurationVariable> cvList, final Map<String, CvNode> cvNumberToNodeMap,
        final CvDefintionPanelProvider cvDefinitionPanelProvider) {
        LOGGER.debug("Write the provided CV values.");

        if (cvList.size() > 0) {

            cvDefinitionPanelProvider.writeConfigVariables(cvList);

            boolean needsReboot = false;
            // reset the new values in the CV nodes ...
            for (ConfigurationVariable cv : cvList) {
                CvNode cvNode = cvNumberToNodeMap.get(cv.getName());
                if (cvNode != null) {
                    cvNode.resetNewValue();

                    Boolean isRebootNeeded = cvNode.getCV().isRebootneeded();

                    if (isRebootNeeded != null && isRebootNeeded.booleanValue()) {
                        needsReboot = true;
                        LOGGER.info("Needs reboot detected, current CV: {}", cvNode.getCV());
                    }
                }
            }

            cvDefinitionPanelProvider.refreshDisplayedValues();

            cvDefinitionPanelProvider.checkPendingChanges();

            if (needsReboot) {
                LOGGER.info("A CV was changed that needs a reboot of the node!");
                if (selectedNode != null) {
                    String reason = "A CV was changed that needs a reboot of the node!";
                    selectedNode.setErrorState(SysErrorEnum.BIDIB_ERR_RESET_REQUIRED, reason.getBytes());
                }
            }
        }
        else {
            LOGGER.warn("No changed configuration variables available.");
        }
    }

    /**
     * Iterate over the children of {@code node} and collect the new CV values in the {@code cvList}.
     * 
     * @param node
     * @param cvList
     */
    private static void collectNewValues(DefaultExpandableRow node, List<ConfigurationVariable> cvList) {

        for (int childIndex = 0; childIndex < node.getChildrenCount(); childIndex++) {
            DefaultExpandableRow child = (DefaultExpandableRow) node.getChildAt(childIndex);
            if (child instanceof CvNode) {
                CvNode cvNode = (CvNode) child;
                addNewValueToList(cvNode, cvList);
            }
            else {
                collectNewValues(child, cvList);
            }
        }
    }

    /**
     * Add a new {@link ConfigurationVariable} item to the list if the new value is available.
     * 
     * @param cvNode
     *            the cvNode
     * @param cvList
     *            the list
     */
    public static void addNewValueToList(CvNode cvNode, List<ConfigurationVariable> cvList) {

        if (cvNode.getNewValue() != null) {
            // we have a new value to write to the node.
            String cvNumber = cvNode.getConfigVar().getName();
            String cvValue = Objects.toString(cvNode.getNewValue(), null);

            ConfigurationVariable cv = new ConfigurationVariable(cvNumber, cvValue);
            cvList.add(cv);
        }
    }

    /**
     * Add a new {@link ConfigurationVariable} item to the list if the new value is available.
     * 
     * @param cvNode
     *            the cvNode
     * @param cvList
     *            the list
     */
    public static void compareAndAddNewValue(
        CvNode cvNode, String newValue, List<ConfigurationVariable> cvList,
        final Map<String, CvNode> cvNumberToNodeMap) {

        if (cvNode != null) {
            switch (cvNode.getCV().getType()) {
                case INT:
                    // process integer values
                    int val = Integer.parseInt(newValue);
                    int cvNumber = Integer.parseInt(cvNode.getCV().getNumber());

                    // an integer contains 2 CV values
                    int highCvNum = Integer.parseInt(cvNode.getCV().getHigh());
                    if (highCvNum == cvNumber) {
                        byte highValue = ByteUtils.getHighByte(val);
                        boolean changeDetected =
                            compareAndAddNewValueAtomic(cvNode, ByteUtils.toString(highValue), cvList, false);
                        // search the low CV
                        CvNode lowCvNode = cvNumberToNodeMap.get(cvNode.getCV().getLow());
                        byte lowValue = ByteUtils.getLowByte(val);
                        boolean changeDetectedLow =
                            compareAndAddNewValueAtomic(lowCvNode, ByteUtils.toString(lowValue), cvList,
                                changeDetected);
                        if (changeDetectedLow != changeDetected) {
                            compareAndAddNewValueAtomic(cvNode, ByteUtils.toString(highValue), cvList, true);
                        }
                    }
                    else {
                        byte lowValue = ByteUtils.getLowByte(val);
                        boolean changeDetected =
                            compareAndAddNewValueAtomic(cvNode, ByteUtils.toString(lowValue), cvList, false);
                        // search the high CV
                        CvNode highCvNode = cvNumberToNodeMap.get(cvNode.getCV().getHigh());
                        byte highValue = ByteUtils.getHighByte(val);
                        boolean changeDetectedLow =
                            compareAndAddNewValueAtomic(highCvNode, ByteUtils.toString(highValue), cvList,
                                changeDetected);
                        if (changeDetectedLow != changeDetected) {
                            compareAndAddNewValueAtomic(cvNode, ByteUtils.toString(highValue), cvList, true);
                        }
                    }
                    break;
                case LONG:
                    LOGGER.info("Long value: {}", cvNode);
                    // process long values
                    long longVal = Long.parseLong(newValue);

                    LongCvNode positionNode = (LongCvNode) cvNode;

                    cvNumber = Integer.parseInt(positionNode.getCV().getNumber());

                    byte[] bytes = new byte[4];
                    bytes[0] = ByteUtils.getLowByte((int) longVal);
                    bytes[1] = ByteUtils.getHighByte((int) longVal);

                    bytes[2] = ByteUtils.getHighWordLowByte((int) longVal);
                    bytes[3] = ByteUtils.getHighWordHighByte((int) longVal);

                    LOGGER.debug("Current master CV#: {}, value: {}", positionNode.getCV().getNumber(),
                        ByteUtils.getInt(bytes[0]));

                    // write the master value
                    boolean changeDetected =
                        compareAndAddNewValueAtomic(positionNode, Integer.toString(ByteUtils.getInt(bytes[0])), cvList,
                            false);

                    // add the slaves
                    int idx = 1;
                    for (CvNode slaveNode : positionNode.getSlaveNodes()) {
                        LOGGER.debug("Current slave CV#: {}, value: {}", slaveNode.getCV().getNumber(),
                            ByteUtils.getInt(bytes[idx]));
                        boolean changeDetectedSlave =
                            compareAndAddNewValueAtomic(slaveNode, ByteUtils.toString(bytes[idx]), cvList,
                                changeDetected);

                        if (changeDetectedSlave != changeDetected) {
                            LOGGER.info("Change detected!");
                            // master node
                            compareAndAddNewValueAtomic(positionNode, Integer.toString(ByteUtils.getInt(bytes[0])),
                                cvList, true);
                            // previous slave nodes
                            for (int i = 1; i < idx; i++) {
                                CvNode tempSlaveNode = positionNode.getSlaveNodes().get(i);
                                compareAndAddNewValueAtomic(tempSlaveNode, ByteUtils.toString(bytes[i]), cvList, true);
                            }

                            changeDetected = true;
                        }

                        idx++;
                    }

                    break;
                default:
                    // process 'atomic' nodes directly: BYTE, BIT, etc. everything that has not additional CV numbers to
                    // be updated
                    compareAndAddNewValueAtomic(cvNode, newValue, cvList, false);
                    break;
            }
        }
    }

    /**
     * Add a new {@link ConfigurationVariable} item to the list if the new value is available.
     * 
     * @param cvNode
     *            the cvNode
     * @param newValue
     *            the new value
     * @param cvList
     *            the list
     * @param force
     *            force write all values
     */
    private static boolean compareAndAddNewValueAtomic(
        CvNode cvNode, String newValue, List<ConfigurationVariable> cvList, boolean force) {

        boolean changeDetected = false;
        if (cvNode.getNewValue() == null) {
            // compare with old value
            if (force || !Objects.equals(cvNode.getConfigVar().getValue(), newValue)) {
                cvNode.setNewValue(newValue);
            }
        }
        else {
            // compare with new value
            if (force || !Objects.equals(cvNode.getNewValue(), newValue)) {
                cvNode.setNewValue(newValue);
            }
        }

        if (cvNode.getNewValue() != null) {
            // we have a new value to write to the node.
            String cvNumber = cvNode.getConfigVar().getName();
            String cvValue = Objects.toString(cvNode.getNewValue(), null);

            ConfigurationVariable cv = new ConfigurationVariable(cvNumber, cvValue);
            cvList.add(cv);
            changeDetected = true;
        }
        return changeDetected;
    }

    /**
     * Add a new {@link ConfigurationVariable} item to the list if the new value is available.
     * 
     * @param cvNode
     *            the cvNode
     * @param cvList
     *            the list
     */
    public static void compareAndAddNewValue(
        LongCvNode cvNode, Integer newValue, final List<ConfigurationVariable> cvList) {

        // prepare the master value
        LOGGER.info("The new long value is: {}", newValue);
        byte[] bytes = new byte[4];
        bytes[0] = ByteUtils.getLowByte(newValue);
        bytes[1] = ByteUtils.getHighByte(newValue);

        bytes[2] = ByteUtils.getHighWordLowByte(newValue);
        bytes[3] = ByteUtils.getHighWordHighByte(newValue);

        LOGGER.debug("Current master CV#: {}, value: {}", cvNode.getCV().getNumber(), ByteUtils.getInt(bytes[0]));
        boolean changeDetected =
            CvValueUtils.compareAndAddNewValueAtomic(cvNode, Integer.toString(ByteUtils.getInt(bytes[0])), cvList,
                false);
        // add the slaves
        int idx = 1;
        for (CvNode slaveNode : cvNode.getSlaveNodes()) {
            LOGGER.debug("Current slave CV#: {}, value: {}", slaveNode.getCV().getNumber(),
                ByteUtils.getInt(bytes[idx]));

            // TODO
            boolean changeDetectedSlave =
                CvValueUtils.compareAndAddNewValueAtomic(slaveNode, Integer.toString(ByteUtils.getInt(bytes[idx])),
                    cvList, changeDetected);

            if (changeDetectedSlave != changeDetected) {
                // force the previous bytes to be written
                CvValueUtils.compareAndAddNewValueAtomic(cvNode, Integer.toString(ByteUtils.getInt(bytes[0])), cvList,
                    true);
                for (int i = 1; i < idx; i++) {
                    CvValueUtils.compareAndAddNewValueAtomic(cvNode.getSlaveNodes().get(i),
                        Integer.toString(ByteUtils.getInt(bytes[i])), cvList, true);
                }

                changeDetected = true;
            }

            idx++;
        }
    }

    /**
     * Return the byte value in the CV as integer value. If no CV is assigned the value <code>0</code> is returned.
     * 
     * @param cvNode
     *            the cv node
     * @param cvNumberToNodeMap
     *            the map of cv number to node
     * @return the value
     */
    public static Integer getConfigVarByteValue(final CvNode cvNode, final Map<String, CvNode> cvNumberToNodeMap) {
        Integer value = 0;
        if (cvNode.getCV() != null) {
            switch (cvNode.getCV().getType()) {
                case BYTE:
                    // process BYTE values
                    value = Integer.parseInt(cvNode.getConfigVar().getValue());
                    break;
                default:
                    value = Integer.parseInt(cvNode.getConfigVar().getValue());
                    break;
            }
        }
        return value;
    }

    public static Integer getConfigVarIntValue(final CvNode cvNode, final Map<String, CvNode> cvNumberToNodeMap) {
        Integer value = null;
        if (cvNode != null) {
            try {
                switch (cvNode.getCV().getType()) {
                    case INT:
                        // process integer values
                        value = Integer.parseInt(cvNode.getConfigVar().getValue());
                        int highCvNum = Integer.parseInt(cvNode.getCV().getHigh());
                        int cvNumber = Integer.parseInt(cvNode.getCV().getNumber());

                        if (highCvNum == cvNumber) {
                            // search the low CV
                            CvNode lowCvNode = cvNumberToNodeMap.get(cvNode.getCV().getLow());
                            value = (value << 8) | Integer.parseInt(lowCvNode.getConfigVar().getValue());
                        }
                        else {
                            // search the high CV
                            CvNode highCvNode = cvNumberToNodeMap.get(cvNode.getCV().getHigh());
                            value = (Integer.parseInt(highCvNode.getConfigVar().getValue()) << 8) | value;
                        }
                        break;
                    default:
                        value = Integer.parseInt(cvNode.getConfigVar().getValue());
                        break;
                }
            }
            catch (NumberFormatException ex) {
                LOGGER.warn("Get the integer value from CV node failed.", ex);
            }
        }
        return value;
    }

    public static Long getConfigVarLongValue(final CvNode cvNode) {
        Long value = null;
        if (cvNode != null) {
            switch (cvNode.getCV().getType()) {
                case LONG:
                    // process long values
                    LongCvNode masterNode = ((LongCvNode) cvNode).getMasterNode();
                    long longValue = 0;
                    CvNode[] slaveNodes = masterNode.getSlaveNodes().toArray(new CvNode[0]);
                    for (int index = 2; index > -1; index--) {
                        CvNode slaveNode = slaveNodes[index];
                        longValue = (longValue << 8) | Integer.parseInt(slaveNode.getConfigVar().getValue());
                    }
                    longValue = (longValue << 8) | Integer.parseInt(masterNode.getConfigVar().getValue());
                    value = ((long) longValue) & 0xffffffffL;
                    break;
                default:
                    break;
            }
        }
        return value;
    }

    public static String getConfigVarStringValue(final CvNode cvNode) {
        String value = null;
        if (cvNode != null) {
            switch (cvNode.getCV().getType()) {
                case STRING:
                    // process string values
                    value = cvNode.getConfigVar().getValue();
                    break;
                default:
                    break;
            }
        }
        return value;
    }
}
