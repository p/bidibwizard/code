package org.bidib.wizard.mvc.pom.view.command;

import org.bidib.jbidibc.core.AddressData;
import org.bidib.jbidibc.core.enumeration.PomOperation;
import org.bidib.jbidibc.core.enumeration.PomProgState;
import org.bidib.wizard.mvc.pom.view.CurrentAddressBeanModel;
import org.bidib.wizard.mvc.pom.view.panel.AddressProgBeanModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PomAddressPanelChangeAddressCommand extends PomAddressValueCommand
    implements PomOperationContinueAfterTimeoutCommand {

    private static final Logger LOGGER = LoggerFactory.getLogger(PomAddressPanelChangeAddressCommand.class);

    private final CurrentAddressBeanModel currentAddressBeanModel;

    public PomAddressPanelChangeAddressCommand(AddressData decoderAddress, PomOperation pomOperation, int cvNumber,
        int cvValue, ValueType valueType, CurrentAddressBeanModel currentAddressBeanModel) {
        super(decoderAddress, pomOperation, cvNumber, cvValue, valueType);
        this.currentAddressBeanModel = currentAddressBeanModel;
    }

    @Override
    public void postExecute(AddressProgBeanModel addressProgBeanModel) {
        super.postExecute(addressProgBeanModel);
        LOGGER.info("postExecute, progStateResult: {}", getProgStateResult());

        if (PomProgState.POM_PROG_OKAY.equals(getProgStateResult())) {
            LOGGER.info("The POM operation was executed successfully.");

            if (currentAddressBeanModel != null) {
                Integer addressValue = addressProgBeanModel.getAddress();
                LOGGER.info("Update the address value in the dialog: {}", addressValue);

                currentAddressBeanModel.setDccAddress(addressValue);

                // TODO signal the change of the address to the user
                currentAddressBeanModel.setAddressChanged(Boolean.TRUE);
            }
        }
    }

}
