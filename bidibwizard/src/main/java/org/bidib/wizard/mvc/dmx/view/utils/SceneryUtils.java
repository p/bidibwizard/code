package org.bidib.wizard.mvc.dmx.view.utils;

import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SceneryUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(SceneryUtils.class);

    public static int calculateSlowdown(TreeSet<Integer> delaySet) {

        // *-----*----------*---------*---*

        Integer prevDelay = null;
        int maximalSlowdown = 255;
        int maximalDeviation = 0;
        do {
            for (Integer currentDelay : delaySet) {

                if (currentDelay.intValue() == 0) {
                    LOGGER.info("Skip the first point.");
                    prevDelay = currentDelay;
                    continue;
                }

                if (prevDelay != null) {
                    int currentSlowdown = (currentDelay.intValue() - prevDelay.intValue()) / 20 /* ms */;
                    if (currentSlowdown < 0) {
                        currentSlowdown = -currentSlowdown;
                    }
                    LOGGER.debug("Current slowdown: {}, maximalSlowdown: {}", currentSlowdown, maximalSlowdown);
                    if (currentSlowdown < maximalSlowdown) {
                        maximalSlowdown = currentSlowdown;
                        LOGGER.debug("Set new maximalSlowdown: {}", maximalSlowdown);
                    }
                }
                else {
                    int currentSlowdown = (currentDelay.intValue()) / 20 /* ms */;
                    if (currentSlowdown < 0) {
                        currentSlowdown = -currentSlowdown;
                    }
                    LOGGER.debug("Current slowdown: {}, maximalSlowdown: {}", currentSlowdown, maximalSlowdown);
                    if (currentSlowdown < maximalSlowdown) {
                        maximalSlowdown = currentSlowdown;
                        LOGGER.info("Set new maximalSlowdown: {}", maximalSlowdown);
                    }
                }
                prevDelay = currentDelay;
            }

            LOGGER.info("Calculated maximalSlowdown: {}", maximalSlowdown);

            for (Integer currentDelay : delaySet) {
                if (maximalSlowdown == 0) {
                    LOGGER
                        .warn("The calculated maximal slowdown is 0! Stop further optimization and return slowdown = 1.");
                    maximalSlowdown = 1;
                    break;
                }

                int calculatedDelay = currentDelay / (maximalSlowdown * 20 /* ms */);
                calculatedDelay *= (maximalSlowdown * 20 /* ms */);

                LOGGER.info("currentDelay: {}, calculatedDelay: {}", currentDelay, calculatedDelay);

                maximalDeviation = currentDelay - calculatedDelay;
                if (maximalDeviation > 1000) {
                    LOGGER.info("The maximal deviation has been exceeded, half the maximalSlowdown and try again.");
                    maximalSlowdown = maximalSlowdown / 2;

                    break;
                }
            }
        }
        while (maximalDeviation > 1000);

        return maximalSlowdown;
    }
}
