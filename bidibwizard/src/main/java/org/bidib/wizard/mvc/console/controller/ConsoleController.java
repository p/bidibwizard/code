package org.bidib.wizard.mvc.console.controller;

import org.bidib.wizard.main.DefaultApplicationContext;
import org.bidib.wizard.mvc.common.view.DockKeys;
import org.bidib.wizard.mvc.console.view.ConsoleView;
import org.bidib.wizard.utils.DockUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vlsolutions.swing.docking.Dockable;
import com.vlsolutions.swing.docking.DockableState;
import com.vlsolutions.swing.docking.DockingConstants;
import com.vlsolutions.swing.docking.DockingDesktop;
import com.vlsolutions.swing.docking.DockingUtilities;
import com.vlsolutions.swing.docking.RelativeDockablePosition;
import com.vlsolutions.swing.docking.TabbedDockableContainer;
import com.vlsolutions.swing.docking.event.DockableStateChangeEvent;
import com.vlsolutions.swing.docking.event.DockableStateChangeListener;

public class ConsoleController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConsoleController.class);

    private ConsoleView view;

    public void start(final DockingDesktop desktop) {

        // check if the console is already opened
        String searchKey = "ConsoleView";
        LOGGER.info("Search for view with key: {}", searchKey);
        Dockable consoleView = desktop.getContext().getDockableByKey(searchKey);
        if (consoleView != null) {
            LOGGER.info("Select the existing console view.");
            DockUtils.selectWindow(consoleView);
            return;
        }

        LOGGER.info("Create new ConsoleView.");

        view = new ConsoleView();

        // add the log panel next to the booster panel
        DockableState[] dockables = desktop.getDockables();
        LOGGER.info("Current dockables: {}", new Object[] { dockables });
        if (dockables.length > 1) {

            DockableState boosterTableView = null;
            // search the booster table view
            for (DockableState dockable : dockables) {

                if (DockKeys.DOCKKEY_BOOSTER_TABLE_VIEW.equals(dockable.getDockable().getDockKey())) {
                    LOGGER.info("Found the booster table view dockable.");
                    boosterTableView = dockable;

                    break;
                }
            }

            Dockable dock = desktop.getDockables()[1].getDockable();
            if (boosterTableView != null) {
                LOGGER.info("Add the console view to the booster table view panel.");
                dock = boosterTableView.getDockable();

                TabbedDockableContainer container = DockingUtilities.findTabbedDockableContainer(dock);
                int order = 0;
                if (container != null) {
                    order = container.getTabCount();
                }
                LOGGER.info("Add new log panel at order: {}", order);

                desktop.createTab(dock, view, order, true);
            }
            else {
                desktop.split(dock, view, DockingConstants.SPLIT_RIGHT);
            }
        }
        else {
            desktop.addDockable(view, RelativeDockablePosition.RIGHT);
        }

        desktop.addDockableStateChangeListener(new DockableStateChangeListener() {

            @Override
            public void dockableStateChanged(DockableStateChangeEvent event) {
                if (event.getNewState().getDockable().equals(view) && event.getNewState().isClosed()) {
                    LOGGER.info("ConsoleView was closed, free resources.");

                    view.close();
                }
            }
        });
    }

    public static synchronized void ensureConsoleVisible() {
        // check if the console is already opened
        String searchKey = "ConsoleView";
        LOGGER.info("Search for view with key: {}", searchKey);

        DockingDesktop desktop =
            DefaultApplicationContext.getInstance().get(DefaultApplicationContext.KEY_DESKTOP, DockingDesktop.class);

        Dockable consoleView = desktop.getContext().getDockableByKey(searchKey);
        if (consoleView != null) {
            LOGGER.info("Select the existing console view.");
            DockUtils.selectWindow(consoleView);
            return;
        }
        else {
            LOGGER.info("Create new controller to open the console.");
            new ConsoleController().start(desktop);
        }
    }
}
