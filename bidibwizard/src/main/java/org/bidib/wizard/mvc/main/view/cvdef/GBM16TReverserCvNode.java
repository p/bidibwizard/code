package org.bidib.wizard.mvc.main.view.cvdef;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.bidib.jbidibc.core.node.ConfigurationVariable;
import org.bidib.jbidibc.exchange.vendorcv.CVType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GBM16TReverserCvNode extends CvNode {
    private static final Logger LOGGER = LoggerFactory.getLogger(GBM16TReverserCvNode.class);

    private List<GBM16TReverserCvNode> slaveNodes = new LinkedList<GBM16TReverserCvNode>();

    private GBM16TReverserCvNode masterNode;

    final private int nodeIndex;

    public GBM16TReverserCvNode(CVType cv, ConfigurationVariable configVar) {
        super(cv, configVar);
        int cvNumber = Integer.parseInt(cv.getNumber());
        this.nodeIndex = (cvNumber / 10000) - 1;
        LOGGER.trace("Create GBM16TReverserCvNode with nodeIndex: {}", nodeIndex);
    }

    public void addSlaveNode(GBM16TReverserCvNode slaveNode) {
        LOGGER.trace("Add new slave node: {}", slaveNode);
        slaveNodes.add(slaveNode);
    }

    public void setMasterNode(GBM16TReverserCvNode masterNode) {
        LOGGER.trace("Set the master node: {}", masterNode);
        this.masterNode = masterNode;
    }

    public GBM16TReverserCvNode getMasterNode() {
        return masterNode;
    }

    public List<GBM16TReverserCvNode> getSlaveNodes() {
        return Collections.unmodifiableList(slaveNodes);
    }

    /**
     * @return the nodeIndex
     */
    public int getNodeIndex() {
        return nodeIndex;
    }
}
