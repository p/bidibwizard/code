package org.bidib.wizard.mvc.dmx.view.panel;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.lang.reflect.Field;

import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.mvc.main.model.Port;
import org.bidib.wizard.mvc.main.model.function.MacroFunction;
import org.jfree.data.xy.XYDataItem;
import org.jfree.util.ObjectUtilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DmxDataItem extends XYDataItem {

    private static final Logger LOGGER = LoggerFactory.getLogger(DmxDataItem.class);

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_TIME_OFFSET = "timeOffset";

    public static final String PROPERTY_BRIGHTNESS = "brightness";

    public static final String PROPERTY_PORT = "port";

    public static final String PROPERTY_MACRO = "macro";

    public static final String PROPERTY_ACTION = "action";

    private long uniqueId;

    private Port<?> port;

    private MacroFunction macro;

    private BidibStatus action;

    private transient PropertyChangeSupport changeSupport;

    /**
     * Creates a new DmxDataItem instance.
     * 
     * @param timeOffset
     *            the time offset of the point
     * @param brightness
     *            the brightness value of the point
     * @param uniqueId
     *            the uniqueId
     * @param port
     *            the assigned port (maybe null)
     */
    public DmxDataItem(Number timeOffset, Number brightness, long uniqueId, Port<?> port) {
        super(timeOffset, brightness);
        this.port = port;
        this.uniqueId = uniqueId;
    }

    /**
     * Creates a new DmxDataItem instance.
     * 
     * @param timeOffset
     *            the time offset of the point
     * @param brightness
     *            the brightness value of the point
     * @param uniqueId
     *            the uniqueId
     * @param macro
     *            the assigned macro (maybe null)
     */
    public DmxDataItem(Number timeOffset, Number brightness, long uniqueId, MacroFunction macro) {
        super(timeOffset, brightness);
        this.macro = macro;
        this.uniqueId = uniqueId;
    }

    /**
     * @return the uniqueId of this item
     */
    public long getUniqueId() {
        return uniqueId;
    }

    public Port<?> getPort() {
        return port;
    }

    public void setPort(Port<?> port) {
        Port<?> oldPortValue = this.port;
        this.port = port;

        MacroFunction oldMacroValue = this.macro;
        this.macro = null;

        firePropertyChange(PROPERTY_PORT, oldPortValue, port);
        firePropertyChange(PROPERTY_MACRO, oldMacroValue, macro);
    }

    public MacroFunction getMacro() {
        return macro;
    }

    public void setMacro(MacroFunction macro) {
        Port<?> oldPortValue = this.port;
        this.port = null;

        MacroFunction oldMacroValue = this.macro;
        this.macro = macro;

        BidibStatus oldActionValue = this.action;
        this.action = null;

        firePropertyChange(PROPERTY_PORT, oldPortValue, port);
        firePropertyChange(PROPERTY_MACRO, oldMacroValue, macro);
        firePropertyChange(PROPERTY_ACTION, oldActionValue, action);
    }

    public int getTimeOffset() {
        return Double.valueOf(getXValue()).intValue();
    }

    public int getBrightness() {
        return Double.valueOf(getYValue()).intValue();
    }

    public void setTimeOffset(int timeOffset) {
        LOGGER.debug("Set the new timeoffset value: {}", timeOffset);
        try {
            Field fieldX = XYDataItem.class.getDeclaredField("x");
            fieldX.setAccessible(true);

            fieldX.set(this, new Double(timeOffset));
        }
        catch (Exception ex) {
            LOGGER.warn("Set the timeoffset value failed.", ex);
        }
    }

    public void setBrightness(int brightness) {
        LOGGER.debug("Set the new brightness value: {}", brightness);
        try {
            // Field fieldX = XYDataItem.class.getDeclaredField("y");
            // fieldX.setAccessible(true);
            //
            // fieldX.set(this, new Double(brightness));

            setY(Double.valueOf(brightness));
        }
        catch (Exception ex) {
            LOGGER.warn("Set the brightness value failed.", ex);
        }
    }

    /**
     * @return the action
     */
    public BidibStatus getAction() {
        return action;
    }

    /**
     * @param action
     *            the action to set
     */
    public void setAction(BidibStatus action) {
        BidibStatus oldValue = this.action;
        this.action = action;

        firePropertyChange(PROPERTY_ACTION, oldValue, action);
    }

    /**
     * Adds a PropertyChangeListener to the listener list.
     * 
     * @param listener
     *            the PropertyChangeListener to be added
     */
    public final synchronized void addPropertyChangeListener(PropertyChangeListener listener) {
        if (listener == null) {
            return;
        }
        if (changeSupport == null) {
            changeSupport = createPropertyChangeSupport(this);
        }
        changeSupport.addPropertyChangeListener(listener);
    }

    /**
     * Removes a PropertyChangeListener from the listener list.
     * 
     * @param listener
     *            the PropertyChangeListener to be removed.
     */
    public final synchronized void removeChangeListener(PropertyChangeListener listener) {
        if (listener == null || changeSupport == null) {
            return;
        }
        changeSupport.removePropertyChangeListener(listener);
    }

    /**
     * Adds a PropertyChangeListener to the listener list for a specific property. The specified property may be
     * user-defined.
     * <p>
     * 
     * Note that if this Model is inheriting a bound property, then no event will be fired in response to a change in
     * the inherited property.
     * <p>
     * 
     * If listener is {@code null}, no exception is thrown and no action is performed.
     * 
     * @param propertyName
     *            one of the property names listed above
     * @param listener
     *            the PropertyChangeListener to be added
     */
    public final synchronized void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if (listener == null) {
            return;
        }
        if (changeSupport == null) {
            changeSupport = createPropertyChangeSupport(this);
        }
        changeSupport.addPropertyChangeListener(propertyName, listener);
    }

    /**
     * Removes a PropertyChangeListener from the listener list for a specific property. This method should be used to
     * remove PropertyChangeListeners that were registered for a specific bound property.
     * <p>
     * 
     * If listener is {@code null}, no exception is thrown and no action is performed.
     * 
     * @param propertyName
     *            a valid property name
     * @param listener
     *            the PropertyChangeListener to be removed
     */
    public final synchronized void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if (listener == null || changeSupport == null) {
            return;
        }
        changeSupport.removePropertyChangeListener(propertyName, listener);
    }

    protected PropertyChangeSupport createPropertyChangeSupport(final Object bean) {
        return new PropertyChangeSupport(bean);
    }

    /**
     * Notifies all registered listeners that the annotation has changed.
     */
    protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        PropertyChangeSupport aChangeSupport = this.changeSupport;
        if (aChangeSupport == null) {
            return;
        }
        aChangeSupport.firePropertyChange(propertyName, oldValue, newValue);
    }

    @Override
    public Object clone() {
        if (port != null) {
            DmxDataItem clone = new DmxDataItem(getX(), getY(), uniqueId, port);
            clone.setAction(getAction());
            return clone;
        }
        DmxDataItem clone = new DmxDataItem(getX(), getY(), uniqueId, macro);
        return clone;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer(getClass().getSimpleName());
        sb
            .append(", x: ").append(getXValue()).append(", y: ").append(getYValue()).append(", uniqueId: ")
            .append(uniqueId).append(", port: ").append(port).append(", action: ").append(action).append(", macro: ")
            .append(macro);
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof DmxDataItem)) {
            return false;
        }

        DmxDataItem that = (DmxDataItem) obj;
        if (!this.getX().equals(that.getX())) {
            return false;
        }
        if (!ObjectUtilities.equal(this.getY(), that.getY())) {
            return false;
        }
        if (!ObjectUtilities.equal(this.getUniqueId(), that.getUniqueId())) {
            return false;
        }
        if (!ObjectUtilities.equal(this.action, that.action)) {
            return false;
        }
        if (!ObjectUtilities.equal(this.port, that.port)) {
            return false;
        }
        if (!ObjectUtilities.equal(this.macro, that.macro)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = super.hashCode();
        hash += uniqueId;
        if (action != null) {
            hash += action.hashCode();
        }
        if (port != null) {
            hash += port.hashCode();
        }
        if (macro != null) {
            hash += macro.hashCode();
        }
        return hash;
    }

    public int compareTo(Object o1) {
        int result;
        if (o1 instanceof DmxDataItem) {
            DmxDataItem dataItem = (DmxDataItem) o1;
            // double compare = x.doubleValue() - dataItem.getX().doubleValue();
            int compare = getTimeOffset() - dataItem.getTimeOffset();
            // if(compare > 0.0D)
            if (compare > 0)
                result = 1;
            else {
                // if(compare < 0.0D)
                if (compare < 0) {
                    result = -1;
                }
                else {
                    LOGGER.info("The timeoffset is equal, compare the uniqueId: {}, other: {}", getUniqueId(),
                        dataItem.getUniqueId());
                    if (getUniqueId() - dataItem.getUniqueId() > 0) {
                        result = 1;
                    }
                    else if (getUniqueId() - dataItem.getUniqueId() < 0) {
                        result = -1;
                    }
                    else {
                        result = 0;
                    }
                }
            }
        }
        else {
            result = 1;
        }
        return result;
    }

}
