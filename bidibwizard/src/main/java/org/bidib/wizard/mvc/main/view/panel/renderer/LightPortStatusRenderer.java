package org.bidib.wizard.mvc.main.view.panel.renderer;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import org.bidib.jbidibc.core.enumeration.LightPortEnum;
import org.bidib.wizard.comm.LightPortStatus;

public class LightPortStatusRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = 1L;

    @Override
    public Component getTableCellRendererComponent(
        JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

        Component renderer = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        if (value instanceof LightPortStatus) {
            LightPortStatus lightPortStatus = (LightPortStatus) value;
            LightPortEnum type = lightPortStatus.getType();
            LightPortStatusLabel label = LightPortStatusLabel.valueOf(type);

            setText(label.toString());
        }

        return renderer;
    }
}
