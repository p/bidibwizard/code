package org.bidib.wizard.mvc.main.model.listener;

public interface StatusListener {
    /**
     * The connected status has changed.
     * 
     * @param cd
     *            the new connected status
     */
    void cdChanged(boolean cd);

    /**
     * The running status has changed.
     * 
     * @param running
     *            the new running status
     */
    void runningChanged(boolean running);

    /**
     * The RX status has changed.
     * 
     * @param rx
     *            the new RX status
     */
    void rxChanged(boolean rx);

    /**
     * The TX status has changed.
     * 
     * @param tx
     *            the new TX status
     */
    void txChanged(boolean tx);

    /**
     * The CTS status has changed.
     * 
     * @param cts
     *            the new CTS status
     */
    void ctsChanged(boolean cts);
}
