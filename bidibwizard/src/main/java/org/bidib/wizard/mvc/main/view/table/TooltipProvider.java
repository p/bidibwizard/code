package org.bidib.wizard.mvc.main.view.table;

public interface TooltipProvider {

    String getToolTip();
}
