package org.bidib.wizard.mvc.main.model;

public class FeedbackPosition {

    private int decoderAddress;

    private int locationType;

    private int locationId;

    private long lastSeenTimestamp;

    public FeedbackPosition(int decoderAddress, int locationType, int locationId, long lastSeenTimestamp) {
        this.decoderAddress = decoderAddress;
        this.locationType = locationType;
        this.locationId = locationId;
        this.lastSeenTimestamp = lastSeenTimestamp;
    }

    /**
     * @return the decoderAddress
     */
    public int getDecoderAddress() {
        return decoderAddress;
    }

    /**
     * @param decoderAddress
     *            the decoderAddress to set
     */
    public void setDecoderAddress(int decoderAddress) {
        this.decoderAddress = decoderAddress;
    }

    /**
     * @return the locationType
     */
    public int getLocationType() {
        return locationType;
    }

    /**
     * @param locationType
     *            the locationType to set
     */
    public void setLocationType(int locationType) {
        this.locationType = locationType;
    }

    /**
     * @return the locationId
     */
    public int getLocationId() {
        return locationId;
    }

    /**
     * @param locationId
     *            the locationId to set
     */
    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    /**
     * @return the lastSeenTimestamp
     */
    public long getLastSeenTimestamp() {
        return lastSeenTimestamp;
    }

    /**
     * @param lastSeenTimestamp
     *            the lastSeenTimestamp to set
     */
    public void setLastSeenTimestamp(long lastSeenTimestamp) {
        this.lastSeenTimestamp = lastSeenTimestamp;
    }
}
