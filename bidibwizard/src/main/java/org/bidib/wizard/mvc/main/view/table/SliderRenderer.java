package org.bidib.wizard.mvc.main.view.table;

import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JSlider;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

public class SliderRenderer extends JSlider implements TableCellRenderer {
    private static final long serialVersionUID = 1L;

    public SliderRenderer(int orientation, int min, int max, int value) {
        super(orientation, min, max, value);

        setOpaque(false);
    }

    public Component getTableCellRendererComponent(
        JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        if (isSelected) {
            setForeground(table.getSelectionForeground());
            setBackground(table.getSelectionBackground());
        }
        else {
            setForeground(table.getForeground());
            setBackground(table.getBackground());
        }

        TableColumnModel columnModel = table.getColumnModel();
        TableColumn selectedColumn = columnModel.getColumn(column);
        int columnWidth = selectedColumn.getWidth();
        int columnHeight = table.getRowHeight();
        setSize(new Dimension(columnWidth, columnHeight));

        setValue(((Integer) value).intValue());
        updateUI();
        return this;
    }

}
