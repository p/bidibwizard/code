package org.bidib.wizard.mvc.preferences.model;

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Point;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;

import org.apache.commons.lang3.StringUtils;
import org.bidib.wizard.mvc.common.model.CommPort;
import org.bidib.wizard.mvc.common.model.PreferencesPortType;
import org.bidib.wizard.mvc.preferences.model.listener.PreferencesModelListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.beans.Model;
import com.jgoodies.common.collect.ArrayListModel;

public class PreferencesModel extends Model {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(PreferencesModel.class);

    private final Collection<PreferencesModelListener> listeners = new LinkedList<PreferencesModelListener>();

    public static final String PROPERTY_SELECTED_SERIALPORT_PROVIDER = "selectedSerialPortProvider";

    private final Collection<CommPort> commPorts = new HashSet<CommPort>();

    private boolean serialUseHardwareFlowControl;

    private boolean serialEnabled;

    private boolean udpEnabled;

    private boolean tcpEnabled;

    private boolean plainTcpEnabled;

    private PreferencesPortType selectedPortType;

    private String previousSelectedSerialSymLink;

    private String previousSelectedComPort;

    private String previousSelectedUdpHost;

    private String previousSelectedTcpHost;

    private String selectedSerialPortProvider;

    private ArrayListModel<String> serialProviderList = new ArrayListModel<>();

    private Date startTime;

    private int timeFactor;

    private String logFilePath;

    @Deprecated
    private String labelPath;

    private String labelV2Path;

    private boolean powerUser;

    private boolean showBoosterTable;

    private boolean alwaysShowProductNameInTree;

    private boolean ignoreWaitTimeout;

    private boolean ignoreWrongReceiveMessageNumber;

    private boolean ignoreFlowControl;

    private int firmwarePacketTimeout;

    private Dimension windowSize = new Dimension(1024, 600);

    private Point windowPosition = new Point(0, 0);

    private int extendedState = Frame.NORMAL;

    private String railcomPlusVendorIds;

    private int pomUpdateInitialDelay;

    private int pomUpdateDelayBetweenPackets;

    private int pomUpdateResendPacketCount;

    private boolean useHotPlugController;

    private boolean logFileAppend;

    private boolean csQueryEnabled;

    private boolean showActionInLastTab;

    private boolean showTipOfDay;

    private boolean allBoosterOnDoNotConfirmSwitch;

    private boolean m4SupportEnabled;

    public PreferencesModel() {
        serialProviderList.add("RXTX");
        serialProviderList.add("SCM");
        serialProviderList.add("SPSW");
    }

    public void addPreferencesModelListener(PreferencesModelListener l) {
        listeners.add(l);
    }

    private void fireCommPortsChanged() {
        for (PreferencesModelListener l : listeners) {
            l.commPortsChanged();
        }
    }

    public Collection<CommPort> getCommPorts() {
        return commPorts;
    }

    /**
     * Set the commPorts and notify the listeners
     * 
     * @param commPorts
     *            the commPorts to set
     */
    public void setCommPorts(Collection<CommPort> commPorts) {
        this.commPorts.clear();
        this.commPorts.addAll(commPorts);
        fireCommPortsChanged();
    }

    /**
     * @return the serialUseHardwareFlowControl
     */
    public boolean isSerialUseHardwareFlowControl() {
        return serialUseHardwareFlowControl;
    }

    /**
     * @param serialUseHardwareFlowControl
     *            the serialUseHardwareFlowControl to set
     */
    public void setSerialUseHardwareFlowControl(boolean serialUseHardwareFlowControl) {
        this.serialUseHardwareFlowControl = serialUseHardwareFlowControl;
    }

    /**
     * @return the serialEnabled
     */
    public boolean isSerialEnabled() {
        return serialEnabled;
    }

    /**
     * @param serialEnabled
     *            the serialEnabled to set
     */
    public void setSerialEnabled(boolean serialEnabled) {
        this.serialEnabled = serialEnabled;
    }

    /**
     * @return the udpEnabled
     */
    public boolean isUdpEnabled() {
        return udpEnabled;
    }

    /**
     * @param udpEnabled
     *            the udpEnabled to set
     */
    public void setUdpEnabled(boolean udpEnabled) {
        this.udpEnabled = udpEnabled;
    }

    /**
     * @return the tcpEnabled
     */
    public boolean isTcpEnabled() {
        return tcpEnabled;
    }

    /**
     * @param tcpEnabled
     *            the tcpEnabled to set
     */
    public void setTcpEnabled(boolean tcpEnabled) {
        this.tcpEnabled = tcpEnabled;
    }

    /**
     * @return the plainTcpEnabled
     */
    public boolean isPlainTcpEnabled() {
        return plainTcpEnabled;
    }

    /**
     * @param plainTcpEnabled
     *            the tcpEnabled to set
     */
    public void setPlainTcpEnabled(boolean plainTcpEnabled) {
        this.plainTcpEnabled = plainTcpEnabled;
    }

    /**
     * @return the selected portType
     */
    public PreferencesPortType getSelectedPortType() {
        LOGGER.info("Return the selected port type: {}", selectedPortType);
        return selectedPortType;
    }

    /**
     * @param selectedPortType
     *            the selected portType to set
     */
    public void setSelectedPortType(PreferencesPortType selectedPortType) {
        this.selectedPortType = selectedPortType;
    }

    /**
     * @return the previousSelectedComPort
     */
    public String getPreviousSelectedComPort() {
        return previousSelectedComPort;
    }

    /**
     * @param previousSelectedComPort
     *            the previousSelectedComPort to set
     */
    public void setPreviousSelectedComPort(String previousSelectedComPort) {
        this.previousSelectedComPort = previousSelectedComPort;
    }

    /**
     * @return the previousSelectedSerialSymLink
     */
    public String getPreviousSelectedSerialSymLink() {
        return previousSelectedSerialSymLink;
    }

    /**
     * @param previousSelectedSerialSymLink
     *            the previousSelectedSerialSymLink to set
     */
    public void setPreviousSelectedSerialSymLink(String previousSelectedSerialSymLink) {
        this.previousSelectedSerialSymLink = previousSelectedSerialSymLink;
    }

    /**
     * @return the previousSelectedUdpHost
     */
    public String getPreviousSelectedUdpHost() {
        return previousSelectedUdpHost;
    }

    /**
     * @param previousSelectedUdpHost
     *            the previousSelectedUdpHost to set
     */
    public void setPreviousSelectedUdpHost(String previousSelectedUdpHost) {
        this.previousSelectedUdpHost = previousSelectedUdpHost;
    }

    /**
     * @return the previousSelectedTcpHost
     */
    public String getPreviousSelectedTcpHost() {
        return previousSelectedTcpHost;
    }

    /**
     * @param previousSelectedTcpHost
     *            the previousSelectedTcpHost to set
     */
    public void setPreviousSelectedTcpHost(String previousSelectedTcpHost) {
        this.previousSelectedTcpHost = previousSelectedTcpHost;
    }

    /**
     * @return the startTime
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * @param startTime
     *            the startTime to set
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * @return the timeFactor
     */
    public int getTimeFactor() {
        return timeFactor;
    }

    /**
     * @param timeFactor
     *            the timeFactor to set
     */
    public void setTimeFactor(int timeFactor) {
        this.timeFactor = timeFactor;
    }

    /**
     * @return the firmwarePacketTimeout
     */
    public int getFirmwarePacketTimeout() {
        return firmwarePacketTimeout;
    }

    /**
     * @param firmwarePacketTimeout
     *            the firmwarePacketTimeout to set
     */
    public void setFirmwarePacketTimeout(int firmwarePacketTimeout) {
        this.firmwarePacketTimeout = firmwarePacketTimeout;
    }

    /**
     * @return the logFilePath
     */
    public String getLogFilePath() {
        return logFilePath;
    }

    /**
     * @param logFilePath
     *            the logFilePath to set
     */
    public void setLogFilePath(String logFilePath) {
        this.logFilePath = logFilePath;
    }

    /**
     * @return the labelPath
     */
    @Deprecated
    public String getLabelPath() {
        return labelPath;
    }

    /**
     * @param labelPath
     *            the labelPath to set
     */
    @Deprecated
    public void setLabelPath(String labelPath) {
        this.labelPath = labelPath;
    }

    /**
     * @return the labelPath
     */
    public String getLabelV2Path() {
        return labelV2Path;
    }

    /**
     * @param labelV2Path
     *            the labelPath to set
     */
    public void setLabelV2Path(String labelV2Path) {
        this.labelV2Path = labelV2Path;
    }

    /**
     * @return the powerUser
     */
    public boolean isPowerUser() {
        return powerUser;
    }

    /**
     * @param powerUser
     *            the powerUser to set
     */
    public void setPowerUser(boolean powerUser) {
        this.powerUser = powerUser;
    }

    /**
     * @return the showBoosterTable
     */
    public boolean isShowBoosterTable() {
        return showBoosterTable;
    }

    /**
     * @param showBoosterTable
     *            the showBoosterTable to set
     */
    public void setShowBoosterTable(boolean showBoosterTable) {
        this.showBoosterTable = showBoosterTable;
    }

    /**
     * @return the alwaysShowProductNameInTree
     */
    public boolean isAlwaysShowProductNameInTree() {
        return alwaysShowProductNameInTree;
    }

    /**
     * @param alwaysShowProductNameInTree
     *            the alwaysShowProductNameInTree to set
     */
    public void setAlwaysShowProductNameInTree(boolean alwaysShowProductNameInTree) {
        this.alwaysShowProductNameInTree = alwaysShowProductNameInTree;
    }

    /**
     * @return the ignoreWaitTimeout
     */
    public boolean isIgnoreWaitTimeout() {
        return ignoreWaitTimeout;
    }

    /**
     * @param ignoreWaitTimeout
     *            the ignoreWaitTimeout to set
     */
    public void setIgnoreWaitTimeout(boolean ignoreWaitTimeout) {
        this.ignoreWaitTimeout = ignoreWaitTimeout;
    }

    /**
     * @return the ignoreWrongReceiveMessageNumber
     */
    public boolean isIgnoreWrongReceiveMessageNumber() {
        return ignoreWrongReceiveMessageNumber;
    }

    /**
     * @param ignoreWrongReceiveMessageNumber
     *            the ignoreWrongReceiveMessageNumber to set
     */
    public void setIgnoreWrongReceiveMessageNumber(boolean ignoreWrongReceiveMessageNumber) {
        this.ignoreWrongReceiveMessageNumber = ignoreWrongReceiveMessageNumber;
    }

    /**
     * @return the ignoreFlowControl flag
     */
    public boolean isIgnoreFlowControl() {
        return ignoreFlowControl;
    }

    /**
     * @param ignoreFlowControl
     *            the ignoreFlowControl flag to set
     */
    public void setIgnoreFlowControl(boolean ignoreFlowControl) {
        this.ignoreFlowControl = ignoreFlowControl;
    }

    public void setWindowPosition(Point position, Dimension windowSize, int extendedState) {
        LOGGER.info("Set the new window position: {}, size: {}, extendedState: {}", position, windowSize,
            extendedState);
        this.windowPosition = position;
        this.windowSize = windowSize;
        this.extendedState = extendedState;
    }

    public void setEncodedWindowPosition(String encodedWindowPosition) {
        try {
            if (StringUtils.isNotBlank(encodedWindowPosition)) {
                Point pos = Preferences.getWindowPos(encodedWindowPosition);
                Dimension size = Preferences.getWindowSize(encodedWindowPosition);
                int state = Preferences.getExtendedState(encodedWindowPosition);
                setWindowPosition(pos, size, state);
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Set encoded window position failed: {}", encodedWindowPosition, ex);
        }
    }

    public String getEncodedWindowPosition() {
        String encodedWindowPosition = Preferences.encodedWindowPosition(windowPosition, windowSize, extendedState);
        return encodedWindowPosition;
    }

    public void setRailcomPlusVendorIds(String vendorIds) {
        this.railcomPlusVendorIds = vendorIds;
    }

    public String getRailcomPlusVendorIds() {
        return railcomPlusVendorIds;
    }

    /**
     * @return the pomUpdateInitialDelay
     */
    public int getPomUpdateInitialDelay() {
        return pomUpdateInitialDelay;
    }

    /**
     * @param pomUpdateInitialDelay
     *            the pomUpdateInitialDelay to set
     */
    public void setPomUpdateInitialDelay(int pomUpdateInitialDelay) {
        this.pomUpdateInitialDelay = pomUpdateInitialDelay;
    }

    /**
     * @return the pomUpdateDelayBetweenPackets
     */
    public int getPomUpdateDelayBetweenPackets() {
        return pomUpdateDelayBetweenPackets;
    }

    /**
     * @param pomUpdateDelayBetweenPackets
     *            the pomUpdateDelayBetweenPackets to set
     */
    public void setPomUpdateDelayBetweenPackets(int pomUpdateDelayBetweenPackets) {
        this.pomUpdateDelayBetweenPackets = pomUpdateDelayBetweenPackets;
    }

    /**
     * @param pomUpdateResendPacketCount
     *            the pomUpdateResendPacketCount to set
     */
    public void setPomUpdateResendPacketCount(int pomUpdateResendPacketCount) {
        this.pomUpdateResendPacketCount = pomUpdateResendPacketCount;
    }

    /**
     * @return the pomUpdateResendPacketCount
     */
    public int getPomUpdateResendPacketCount() {
        return pomUpdateResendPacketCount;
    }

    /**
     * @return the serial provider list
     */
    public ArrayListModel<String> getSerialProviderList() {
        return serialProviderList;
    }

    /**
     * @return the selectedSerialPortProvider
     */
    public String getSelectedSerialPortProvider() {
        return selectedSerialPortProvider;
    }

    /**
     * @param selectedSerialPortProvider
     *            the selectedSerialPortProvider to set
     */
    public void setSelectedSerialPortProvider(String selectedSerialPortProvider) {
        String oldValue = this.selectedSerialPortProvider;
        this.selectedSerialPortProvider = selectedSerialPortProvider;

        firePropertyChange(PROPERTY_SELECTED_SERIALPORT_PROVIDER, oldValue, selectedSerialPortProvider);
    }

    public boolean isUseHotPlugController() {
        return useHotPlugController;
    }

    /**
     * @param useHotPlugController
     *            the useHotPlugController flag to set
     */
    public void setUseHotPlugController(boolean useHotPlugController) {
        this.useHotPlugController = useHotPlugController;
    }

    /**
     * @return the logFileAppend
     */
    public boolean isLogFileAppend() {
        return logFileAppend;
    }

    /**
     * @param logFileAppend
     *            the logFileAppend to set
     */
    public void setLogFileAppend(boolean logFileAppend) {
        this.logFileAppend = logFileAppend;
    }

    /**
     * @return the show tip of day flag
     */
    public boolean isShowTipOfDay() {
        return showTipOfDay;
    }

    /**
     * @param showTipOfDay
     *            the showTipOfDay flag to set
     */
    public void setShowTipOfDay(boolean showTipOfDay) {
        this.showTipOfDay = showTipOfDay;
    }

    /**
     * @return the csQueryEnabled flag
     */
    public boolean isCsQueryEnabled() {
        return csQueryEnabled;
    }

    /**
     * @param csQueryEnabled
     *            the csQueryEnabled flag to set
     */
    public void setCsQueryEnabled(boolean csQueryEnabled) {
        this.csQueryEnabled = csQueryEnabled;
    }

    /**
     * @return the showActionInLastTab flag
     */
    public boolean isShowActionInLastTab() {
        return showActionInLastTab;
    }

    /**
     * @param showActionInLastTab
     *            the showActionInLastTab flag to set
     */
    public void setShowActionInLastTab(boolean showActionInLastTab) {
        this.showActionInLastTab = showActionInLastTab;
    }

    /**
     * @return the allBoosterOnDoNotConfirmSwitch
     */
    public boolean isAllBoosterOnDoNotConfirmSwitch() {
        return allBoosterOnDoNotConfirmSwitch;
    }

    /**
     * @param allBoosterOnDoNotConfirmSwitch
     *            the allBoosterOnDoNotConfirmSwitch to set
     */
    public void setAllBoosterOnDoNotConfirmSwitch(boolean allBoosterOnDoNotConfirmSwitch) {
        this.allBoosterOnDoNotConfirmSwitch = allBoosterOnDoNotConfirmSwitch;
    }

    /**
     * @return the m4SupportEnabled flag
     */
    public boolean isM4SupportEnabled() {
        return m4SupportEnabled;
    }

    /**
     * @param m4SupportEnabled
     *            the m4SupportEnabled flag to set
     */
    public void setM4SupportEnabled(boolean m4SupportEnabled) {
        this.m4SupportEnabled = m4SupportEnabled;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("PreferencesModel, ");
        sb.append(", selected portType: ");
        sb.append(getSelectedPortType()).append(", serialEnabled: ");
        sb.append(isSerialEnabled()).append(", previousSelectedSerialSymLink: ");
        sb.append(getPreviousSelectedSerialSymLink()).append(", previousSelectedUdpHost: ");
        sb.append(getPreviousSelectedUdpHost()).append(", udpEnabled: ");
        sb.append(isUdpEnabled()).append(", startTime: ");
        sb.append(getStartTime()).append(", timeFactor: ");
        sb.append(getTimeFactor()).append(", logFilePath: ");
        sb.append(getLogFilePath()).append(", labelPath: ");
        sb.append(getLabelPath()).append(", powerUser: ");
        sb.append(isPowerUser()).append(", showBoosterTable: ").append(isShowBoosterTable());
        sb.append(", alwaysShowProductNameInTree: ").append(isAlwaysShowProductNameInTree());
        sb.append(", ignoreWaitTimeout: ").append(isIgnoreWaitTimeout());
        sb.append(", ignoreWrongReceiveMessageNumber: ").append(isIgnoreWrongReceiveMessageNumber());
        sb.append(", ignoreFlowControl: ").append(isIgnoreFlowControl());
        sb.append(", firmwarePacketTimeout: ").append(getFirmwarePacketTimeout());
        sb.append(", selectedSerialPortProvider: ").append(getSelectedSerialPortProvider());
        sb.append(", railcomPlusVendorIds: ").append(railcomPlusVendorIds);
        sb.append(", pomUpdateInitialDelay: ").append(getPomUpdateInitialDelay());
        sb.append(", pomUpdateDelayBetweenPackets: ").append(getPomUpdateDelayBetweenPackets());
        sb.append(", pomUpdateResendPacketCount: ").append(getPomUpdateResendPacketCount());
        sb.append(", useHotPlugController: ").append(isUseHotPlugController());
        sb.append(", logFileAppend: ").append(isLogFileAppend()).append(", serialUseHardwareFlowControl: ");
        sb.append(isSerialUseHardwareFlowControl()).append(", showTipOfDay: ");
        sb.append(isShowTipOfDay()).append(", csQueryEnabled: ");
        sb.append(isCsQueryEnabled()).append(", showActionInLastTab: ");
        sb.append(isShowActionInLastTab()).append(", allBoosterOnDoNotConfirmSwitch: ");
        sb.append(isAllBoosterOnDoNotConfirmSwitch()).append(", m4SupportEnabled: ").append(m4SupportEnabled);

        return sb.toString();
    }
}
