package org.bidib.wizard.mvc.main.view.cvdef;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.ParameterizedType;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.apache.commons.lang.StringUtils;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.wizard.mvc.common.view.validation.IconFeedbackPanel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.value.BufferedValueModel;
import com.jgoodies.binding.value.Trigger;
import com.jgoodies.binding.value.ValueHolder;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.ValidationResultModel;
import com.jgoodies.validation.util.DefaultValidationResultModel;

public abstract class CvValueNumberEditor<T> extends JPanel implements CvValueEditor {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(CvValueNumberEditor.class);

    protected CvValueBean<T> cvValueBean;

    protected CvValuePresentationModel<T, CvValueBean<T>> cvAdapter;

    protected ValidationResultModel cvValidationModel;

    private JComponent cvIconPanel;

    protected ValueModel cvNumberModel = new ValueHolder();

    private String valueLabelPrefix;

    protected BufferedValueModel cvValueModel;

    protected ValueModel valueConverterModel;

    protected ValueModel needsRebootModel = new ValueHolder();

    private ValueModel saveButtonEnabledModel = new ValueHolder(false);

    private Trigger trigger;

    private Class<T> valueClazz;

    public CvValueNumberEditor() {

        valueClazz = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    public CvValuePresentationModel<T, CvValueBean<T>> getCvAdapter() {
        return cvAdapter;
    }

    public ValueModel getSaveButtonEnabledModel() {
        return saveButtonEnabledModel;
    }

    public void setValueLabelPrefix(String valueLabelPrefix) {
        this.valueLabelPrefix = valueLabelPrefix;
    }

    public String getValueLabelPrefix() {
        return valueLabelPrefix;
    }

    @Override
    public void setValue(CvNode cvNode, Map<String, CvNode> cvNumberToNodeMap) {
        LOGGER.debug("Set the new cvNode: {}", cvNode);

        // keep the node reference
        cvValueBean.setCvNode(cvNode);

        // cvValueBean.setCvValue(null);

        // update the value in the value models
        setValueInternally(cvNode, false);

        // trigger.triggerFlush();

        cvNumberModel.setValue(cvNode);

        // check if a reboot is needed
        checkNeedsReboot();
    }

    protected void checkNeedsReboot() {
        CvNode cvNode = cvValueBean.getCvNode();
        if (cvNode != null && cvNode.getCV() != null && cvNode.getCV().isRebootneeded() != null) {
            needsRebootModel.setValue(cvNode.getCV().isRebootneeded());
        }
        else {
            needsRebootModel.setValue(Boolean.FALSE);
        }
    }

    protected void setValueInternally(CvNode cvNode, boolean fromSource) {

        trigger.triggerFlush();

        Number value = null;
        // this must be updated in the text field ...
        if (!fromSource && cvNode.getNewValue() instanceof Number) {
            value = (Number) cvNode.getNewValue();
        }
        else if (StringUtils.isNotBlank(cvNode.getConfigVar().getValue())) {
            String strValue = cvNode.getConfigVar().getValue();
            try {
                if (valueClazz.isAssignableFrom(Byte.class)) {
                    // must parse the value as integer first and convert to byte value
                    // because Byte.valueOf() does not work correct for values > 128
                    Integer integerVal = Integer.valueOf(strValue);
                    byte byteValue = ByteUtils.getLowByte(integerVal.intValue());

                    value = Byte.valueOf(byteValue);
                }
                else {
                    // value = Integer.valueOf(strValue);
                    value = parseValue(strValue);
                }
            }
            catch (Exception ex) {
                LOGGER.warn("Parse value failed: {}, reason: {}", strValue, ex.getMessage());
            }
        }

        LOGGER.debug("Set the new value: {}", value);
        cvValueBean.setCvValue((T) value);

        // trigger.triggerFlush();
        cvAdapter.resetChanged();
    }

    protected Number parseValue(String strValue) {
        Number value = Integer.valueOf(strValue);

        return value;
    }

    protected abstract ValidationResult validateModel(
        final PresentationModel<CvValueBean<T>> model, CvValueBean<T> cvValueBean);

    @Override
    public Trigger getTrigger() {
        return trigger;
    }

    public JPanel create(Trigger trigger) {
        this.trigger = trigger;
        FormLayout layout = new FormLayout("p:g");

        DefaultFormBuilder builder = null;
        builder = new DefaultFormBuilder(layout, this);

        this.cvValidationModel = new DefaultValidationResultModel();

        cvValueBean = new CvValueBean<T>();
        cvAdapter = new CvValuePresentationModel<T, CvValueBean<T>>(cvValueBean, this.trigger) {
            private static final long serialVersionUID = 1L;

            @Override
            public ValidationResult validate() {
                ValidationResult validationResult = validateModel(this, cvValueBean);

                // update the save button
                boolean hasErrors = cvValidationModel.hasErrors();
                updateSaveButtonEnabled(cvValueModel.isBuffering(), hasErrors);

                return validationResult;
            }
        };
        cvAdapter.setValidationResultModel(cvValidationModel);

        // create the content panel
        DefaultFormBuilder formBuilder = prepareFormPanel();
        formBuilder.appendRow("5dlu");

        // Padding for overlay icon
        this.cvIconPanel = new IconFeedbackPanel(this.cvValidationModel, formBuilder.getPanel());

        builder.appendRow("p");
        builder.add(cvIconPanel);
        builder.border(new EmptyBorder(10, 0, 0, 0));

        builder.build();

        // listen on changes of buffering
        cvValueModel.addPropertyChangeListener(BufferedValueModel.PROPERTY_BUFFERING, new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                // update the save button
                updateSaveButtonEnabled(cvValueModel.isBuffering(), cvValidationModel.hasErrors());
            }
        });

        triggerValidation(null);

        return this;
    }

    private void updateSaveButtonEnabled(boolean isBuffering, boolean validationHasErrors) {
        LOGGER.debug("Update the save button, isBuffering: {}, validationHasErrors: {}", isBuffering,
            validationHasErrors);

        if (!validationHasErrors) {
            saveButtonEnabledModel.setValue(isBuffering);
        }
        else {
            saveButtonEnabledModel.setValue(false);
        }
    }

    protected void triggerValidation(String propertyName) {
        PropertyChangeEvent evt = null;

        LOGGER.debug("Validation is triggered, propertyname: {}", propertyName);
        if (propertyName != null) {
            evt = new PropertyChangeEvent(this, propertyName, Boolean.FALSE, Boolean.TRUE);
        }

        // force validation
        cvAdapter.getChangeHandler().propertyChange(evt);
    }

    protected abstract DefaultFormBuilder prepareFormPanel();

    @Override
    public void refreshValue() {
        CvNode cvNode = cvValueBean.getCvNode();
        LOGGER.debug("Refresh values from cvNode: {}", cvNode);
        if (cvNode != null) {
            setValueInternally(cvNode, true);
        }
    }

    @Override
    public void refreshValueFromSource() {
        CvNode cvNode = cvValueBean.getCvNode();
        LOGGER.info("Refresh values from source of cvNode: {}", cvNode);
        if (cvNode != null) {
            setValueInternally(cvNode, true);
        }
    }

    @Override
    public CvNode getCvNode() {
        return cvValueBean.getCvNode();
    }

    /**
     * Prepare the allowed values from the provided values. Ranges are supported with e.g. '0-15' and single values that
     * are separated by ';'.
     * 
     * @param values
     *            the values
     * @return the allowed values
     */
    protected String[] prepareAllowedValues(String values) {

        if (StringUtils.isNotBlank(values) && !("-".equals(values))) {
            Set<String> valueSet = new LinkedHashSet<>();
            // we have allowed values defined
            String[] splitedValues = values.split(";");

            // check for ranges
            for (String value : splitedValues) {
                if (value.indexOf("-") > -1) {
                    String[] splitedRange = value.split("-");
                    if (splitedRange.length == 2) {
                        try {
                            int startRange = Integer.parseInt(splitedRange[0]);
                            int endRange = Integer.parseInt(splitedRange[1]);

                            for (int val = startRange; val <= endRange; val++) {
                                valueSet.add(String.valueOf(val));
                            }
                        }
                        catch (Exception ex) {
                            LOGGER.warn("Prepare valid values from range was not possible for provided value: {}",
                                value);
                        }
                    }
                    else {
                        LOGGER.warn("Split range was not possible for provided value: {}", value);
                    }
                }
                else {
                    valueSet.add(value);
                }
            }
            return valueSet.toArray(new String[0]);
        }

        return null;
    }
}
