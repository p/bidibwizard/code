package org.bidib.wizard.mvc.main.view.cvdef;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.validation.Validatable;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.ValidationResultModel;

public abstract class CvValuePresentationModel<T, B extends CvValueBean<T>> extends PresentationModel<B>
    implements Validatable {
    private static final Logger LOGGER = LoggerFactory.getLogger(CvValuePresentationModel.class);

    private static final long serialVersionUID = 1L;

    private PropertyChangeListener handler;

    private ValidationResultModel validationModel;

    public CvValuePresentationModel(B bean, ValueModel triggerChannel) {
        super(bean, triggerChannel);
        initEventHandling();
    }

    private void initEventHandling() {
        handler = new ValueUpdateHandler();
        getBufferedModel("cvValue").addValueChangeListener(handler);

        addBeanPropertyChangeListener(handler);
        addPropertyChangeListener("validState", handler);
        getBeanChannel().addValueChangeListener(handler);
    }

    public void setValidationResultModel(ValidationResultModel validationModel) {
        this.validationModel = validationModel;
    }

    public PropertyChangeListener getChangeHandler() {
        return handler;
    }

    public boolean isValidState() {
        return !validationModel.hasErrors();
    }

    public class ValueUpdateHandler implements PropertyChangeListener {
        public void propertyChange(PropertyChangeEvent evt) {
            LOGGER.debug("prop changed, evt: {}", (evt != null ? evt.getPropertyName() : "---"));

            if (evt != null && "signalValidState".equals(evt.getPropertyName())) {
                signalValidState(Boolean.FALSE, Boolean.TRUE);
            }
            else {
                updateComponents();
            }
        }

        private void updateComponents() {
            LOGGER.debug("Value was updated.");
            boolean oldValue = validationModel.hasErrors();
            ValidationResult result = validate();
            validationModel.setResult(result);

            signalValidState(!oldValue, !result.hasErrors());
        }
    }

    private void signalValidState(boolean oldValue, boolean newValue) {
        LOGGER.debug("Signal valid state, old: {}, new: {}", oldValue, newValue);

        firePropertyChange("validState", oldValue, newValue);
    }
}
