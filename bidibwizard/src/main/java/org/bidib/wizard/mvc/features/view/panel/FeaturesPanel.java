package org.bidib.wizard.mvc.features.view.panel;

import java.awt.Color;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.swing.JPanel;
import javax.swing.JTable;

import org.bidib.jbidibc.core.Feature;
import org.bidib.wizard.mvc.features.model.FeaturesModel;
import org.bidib.wizard.mvc.features.model.listener.FeatureListener;
import org.bidib.wizard.mvc.features.view.panel.listener.FeaturesWriteListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jidesoft.grid.RowStripeTableStyleProvider;
import com.jidesoft.grid.SortableTable;

public class FeaturesPanel extends JPanel {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(FeaturesPanel.class);

    private final Set<FeaturesWriteListener> listeners = new LinkedHashSet<FeaturesWriteListener>();

    private FeatureTableModel featureTableModel;

    private SortableTable table;

    private final FeaturesModel model;

    int featuresAdded = 0;

    public FeaturesPanel(final FeaturesModel model) {
        this.model = model;
        featureTableModel = new FeatureTableModel();
        table = new SortableTable(featureTableModel);

        table
            .getColumnModel().getColumn(FeatureTableModel.COLUMN_FEATURE_VALUE).setCellEditor(new FeatureAwareEditor());
        table.getColumnModel().getColumn(FeatureTableModel.COLUMN_FEATURE_VALUE).setCellRenderer(
            new FeatureAwareRenderer());

        RowStripeTableStyleProvider tableStyleProvider =
            new RowStripeTableStyleProvider(Color.WHITE, new Color(209, 224, 239));
        table.setTableStyleProvider(tableStyleProvider);

        // do not allow drag columns to other position
        table.getTableHeader().setReorderingAllowed(false);

        table.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);

        table.setRowHeight(table.getRowHeight() + 4);
        add(table);

        model.addFeatureListener(new FeatureListener() {

            @Override
            public void featureChanged(Feature feature) {
                LOGGER.debug("The following feature has changed: {}", feature);
                showFeature(feature);
            }

            @Override
            public void featuresChanged() {
                LOGGER.info("The features have changed.");
            }
        });

        table.sortColumn(FeatureTableModel.COLUMN_FEATURE_NAME);
        // table.setColumnAutoResizable(true);

        table.getColumnModel().getColumn(FeatureTableModel.COLUMN_FEATURE_NAME).setPreferredWidth(400);
        table.getColumnModel().getColumn(FeatureTableModel.COLUMN_FEATURE_ID).setPreferredWidth(250);

        showFeatures(model.getFeatures());
    }

    public void addFeaturesWriteListener(FeaturesWriteListener listener) {
        listeners.add(listener);
    }

    public void showFeatures(Collection<Feature> features) {
        LOGGER.debug("Show features: {}", features);
        featureTableModel.clear();

        for (Feature feature : features) {
            LOGGER.debug("Add feature: {}", feature);
            showFeature(feature);
        }
    }

    public JTable getTable() {
        return table;
    }

    private void showFeature(Feature feature) {
        LOGGER.debug("Show feature: {}", feature);

        featureTableModel.updateRow(feature);
    }

    /**
     * Write the features to the model and notify the listener of the changed features. If no exception is thrown the
     * model is updated with the new feature values.
     */
    public void writeFeatures() {
        if (table.getCellEditor() != null) {
            table.getCellEditor().stopCellEditing();
        }

        List<Feature> features = featureTableModel.getFeatures();

        // check the changed data
        Collection<Feature> featuresToWrite = model.prepareUpdateFeatures(features);

        // write the changed features to the node
        fireWrite(featuresToWrite);

        // update the changed features in the model
        model.setFeatures(featuresToWrite);
    }

    private void fireWrite(Collection<Feature> features) {
        for (FeaturesWriteListener l : listeners) {
            l.write(features);
        }
    }

}
