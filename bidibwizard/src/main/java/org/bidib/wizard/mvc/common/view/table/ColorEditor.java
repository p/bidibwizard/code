package org.bidib.wizard.mvc.common.view.table;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.colorchooser.AbstractColorChooserPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.TableCellEditor;

import org.apache.commons.collections4.CollectionUtils;
import org.bidib.wizard.common.locale.Resources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ColorEditor extends AbstractCellEditor implements TableCellEditor, ActionListener {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ColorEditor.class);

    private Color currentColor;

    private JCheckBox checkBox;

    JColorChooser colorChooser;

    JDialog dialog;

    protected static final String EDIT = "edit";

    protected static final String OK = "ok";

    public static interface ColorChooserValueUpdate {
        void apply(Color newColor);
    }

    public ColorEditor() {

        checkBox = new JCheckBox();

        checkBox.setUI(new ColorCheckBoxUI());
        checkBox.setActionCommand(EDIT);
        checkBox.addActionListener(this);

        // Set up the dialog that the button brings up.
        colorChooser = new JColorChooser();
        colorChooser.setPreviewPanel(new JPanel());
        List<AbstractColorChooserPanel> chooserPanelsToRemove = new ArrayList<>();
        for (AbstractColorChooserPanel chooserPanel : colorChooser.getChooserPanels()) {

            if (chooserPanel.getClass().getName().contains("SwatchChooserPanel")) {
                LOGGER.info("Found the SwatchChooserPanel to remove.");
                chooserPanelsToRemove.add(chooserPanel);
            }
        }

        if (CollectionUtils.isNotEmpty(chooserPanelsToRemove)) {
            for (AbstractColorChooserPanel chooserPanel : chooserPanelsToRemove) {
                LOGGER.info("Remove chooserPanel: {}", chooserPanel);
                colorChooser.removeChooserPanel(chooserPanel);
            }
        }

        dialog =
            JColorChooser.createDialog(checkBox, Resources.getString(ColorEditor.class, "title"), true, // modal
                colorChooser, this, // OK button handler
                this); // no CANCEL button handler

        colorChooser.getSelectionModel().addChangeListener(new ChangeListener() {

            @Override
            public void stateChanged(ChangeEvent e) {
                LOGGER.info("State changed, e: {}", e);
                if (chooserValueUpdate != null) {
                    chooserValueUpdate.apply(colorChooser.getColor());
                }
            }
        });
    }

    private ColorChooserValueUpdate chooserValueUpdate;

    public void setColorChooserValueUpdate(final ColorChooserValueUpdate chooserValueUpdate) {
        this.chooserValueUpdate = chooserValueUpdate;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (EDIT.equals(e.getActionCommand())) {
            // The user has clicked the cell, so
            Container container = null;
            if (e.getSource() instanceof JCheckBox) {
                container = ((JCheckBox) e.getSource()).getParent();
            }
            // bring up the dialog.
            checkBox.setBackground(currentColor);

            colorChooser.setColor(currentColor);
            colorChooser.setForeground(currentColor);
            dialog.setVisible(true);

            if (container != null) {
                // request the focus in the parent to restore navigation with keys
                container.requestFocusInWindow();
            }
        }
        else if (OK.equalsIgnoreCase(e.getActionCommand())) {
            // User pressed dialog's "OK" button.
            currentColor = colorChooser.getColor();

            LOGGER.info("User pressed ok.");
            stopCellEditing();
        }
        else {
            LOGGER.info("User pressed cancel.");
            stopCellEditing();
        }
    }

    /**
     * Reduce a 32-bit RGB color to a 8-bit 3-3-2 color.
     * 
     * @param color
     *            the 32-bit RGB color
     * @return the 8-bit 3-3-2 color
     */
    public byte reduceColor(Color color) {

        int r = (color.getRGB() & 0x00ff0000) >> 16;
        int g = (color.getRGB() & 0x0000ff00) >> 8;
        int b = (color.getRGB() & 0x000000ff);

        byte rgb = (byte) ((r & 0xE0) | (g & 0xE0) >> 3 | ((b & 0xC0) >> 6));
        return rgb;
    }

    @Override
    public Object getCellEditorValue() {
        return currentColor;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {

        if (value instanceof Color) {
            currentColor = (Color) value;
        }
        else if (value instanceof Integer) {
            currentColor = new Color(((Integer) value).intValue());
        }

        checkBox.setBackground(currentColor);

        return checkBox;
    }
}
