package org.bidib.wizard.mvc.main.view.menu;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import com.vlsolutions.swing.docking.DockKey;
import com.vlsolutions.swing.docking.Dockable;

public class DockingFramesRegistry {

    private Map<DockKey, Dockable> registeredDockables = new LinkedHashMap<DockKey, Dockable>();

    public Dockable getDockable(DockKey dockKey) {
        return registeredDockables.get(dockKey);
    }

    public void addDockable(DockKey dockKey, Dockable dockable) {
        registeredDockables.put(dockKey, dockable);
    }

    public void removeDockable(DockKey dockKey) {
        registeredDockables.remove(dockKey);
    }

    public Collection<Dockable> getDockables() {
        return Collections.unmodifiableCollection(registeredDockables.values());
    }
}
