package org.bidib.wizard.mvc.main.view.panel;

import java.awt.event.MouseEvent;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.swing.JOptionPane;

import org.bidib.wizard.comm.FeedbackPortStatus;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.dialog.LabelDialog;
import org.bidib.wizard.labels.FeedbackPortLabelFactory;
import org.bidib.wizard.labels.Labels;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.bidib.wizard.mvc.main.controller.FeedbackPortPanelController;
import org.bidib.wizard.mvc.main.model.FeedbackAddressData;
import org.bidib.wizard.mvc.main.model.FeedbackConfidenceData;
import org.bidib.wizard.mvc.main.model.FeedbackDynStateData;
import org.bidib.wizard.mvc.main.model.FeedbackPort;
import org.bidib.wizard.mvc.main.model.FeedbackPortModel;
import org.bidib.wizard.mvc.main.model.FeedbackPortTableModel;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.model.Port;
import org.bidib.wizard.mvc.main.model.SimplePortTableModel;
import org.bidib.wizard.mvc.main.model.listener.DefaultFeedbackPortListener;
import org.bidib.wizard.mvc.main.model.listener.FeedbackPortListener;
import org.bidib.wizard.mvc.main.model.listener.PortListener;
import org.bidib.wizard.mvc.main.view.menu.PortListMenu;
import org.bidib.wizard.mvc.main.view.menu.listener.PortListMenuListener;
import org.bidib.wizard.mvc.main.view.panel.listener.TabVisibilityListener;
import org.bidib.wizard.mvc.main.view.panel.listener.TabVisibilityProvider;
import org.bidib.wizard.mvc.main.view.table.DefaultPortListMenuListener;
import org.bidib.wizard.mvc.main.view.table.FeedbackPortTableCellRenderer;
import org.bidib.wizard.mvc.main.view.table.PortTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FeedbackPortListPanel
    extends SimplePortListPanel<FeedbackPortStatus, FeedbackPort, PortListener<FeedbackPortStatus>>
    implements TabVisibilityProvider {
    private static final Logger LOGGER = LoggerFactory.getLogger(FeedbackPortListPanel.class);

    private static final long serialVersionUID = 1L;

    private final FeedbackPortPanelController controller;

    private final FeedbackPortListener<FeedbackPortStatus> feedbackPortListener;

    private final FeedbackPortTableModel feedbackPortTableModel;

    private final FeedbackPortModel feedbackPortModel;

    private final TabVisibilityListener tabVisibilityListener;

    public FeedbackPortListPanel(final FeedbackPortPanelController controller,
        final FeedbackPortModel feedbackPortModel, MainModel model, final TabVisibilityListener tabVisibilityListener) {
        super(new FeedbackPortTableModel(), model.getFeedbackPorts(),
            Resources.getString(FeedbackPortListPanel.class, "emptyTable"));

        this.controller = controller;
        this.feedbackPortModel = feedbackPortModel;
        this.tabVisibilityListener = tabVisibilityListener;
        LOGGER.debug("Create new FeedbackPortListPanel.");

        table.setDefaultCellRenderer(new FeedbackPortTableCellRenderer());
        // for (int index = 0; index < table.getColumnCount(); index++) {
        // table.getColumnModel().getColumn(index).setCellRenderer(new FeedbackPortTableCellRenderer());
        // }

        table.setRowHeight(100);
        table.setTableHeader(null);

        feedbackPortTableModel = (FeedbackPortTableModel) tableModel;

        // add the feedback port listener
        feedbackPortListener = new DefaultFeedbackPortListener() {
            @Override
            public void addressesChanged(FeedbackPort port, Collection<FeedbackAddressData> addresses) {
                feedbackPortTableModel.updatePort(port);
            }

            @Override
            public void confidenceChanged(FeedbackPort port, FeedbackConfidenceData confidence) {
                LOGGER.debug("Confidence has changed for port: {}, confidence: {}", port, confidence);
                feedbackPortTableModel.updatePort(port);
            }

            @Override
            public void labelChanged(Port<FeedbackPortStatus> port, String label) {

                Labels feedbackPortLabels =
                    DefaultApplicationContext.getInstance().get(DefaultApplicationContext.KEY_FEEDBACKPORT_LABELS,
                        Labels.class);

                LOGGER.info("The label has changed, port: {}, label: {}", port, label);

                port.setLabel(label);

                // update the stored labels
                try {
                    FeedbackPortLabelFactory factory = new FeedbackPortLabelFactory();
                    long uniqueId = controller.getSelectedNode().getUniqueId();
                    factory.replaceLabel(feedbackPortLabels, uniqueId, port.getId(), label);

                    factory.saveLabels(uniqueId, feedbackPortLabels);
                }
                catch (Exception ex) {
                    LOGGER.warn("Save feedback labels failed.", ex);

                    String labelPath = ex.getMessage();
                    JOptionPane.showMessageDialog(JOptionPane.getFrameForComponent(null),
                        Resources.getString(Labels.class, "labelfileerror.message", new Object[] { labelPath }),
                        Resources.getString(Labels.class, "labelfileerror.title"), JOptionPane.ERROR_MESSAGE);

                    throw new RuntimeException(ex);
                }
            }

            @Override
            public void speedChanged(FeedbackPort port, int address, int speed) {
                feedbackPortTableModel.updatePort(port);
            }

            @Override
            public void statusChanged(Port<FeedbackPortStatus> port, FeedbackPortStatus status) {
                feedbackPortTableModel.updatePort(port);
            }

            @Override
            public void dynStatesChanged(FeedbackPort port, Collection<FeedbackDynStateData> dynStates) {
                feedbackPortTableModel.updatePort(port);
            }
        };

        feedbackPortModel.addFeedbackPortListener(feedbackPortListener);

        feedbackPortModel.addPortListListener(this);

        // get notified of changes of label
        addPortListener(feedbackPortListener);
    }

    @Override
    protected PortTable createPortTable(
        final SimplePortTableModel<FeedbackPortStatus, FeedbackPort, PortListener<FeedbackPortStatus>> tableModel,
        String emptyTableText) {
        return new PortTable(tableModel, emptyTableText) {
            private static final long serialVersionUID = 1L;

            @Override
            public void clearTable() {
            }

            @Override
            protected PortListMenuListener createMenuListener() {

                // for feedback ports the port mapping is not visible
                setPortMappingVisible(false);

                // create the port list menu
                LOGGER.info("Create the menu listener.");
                return new DefaultPortListMenuListener() {
                    @Override
                    public void editLabel() {
                        final int row = getRow(popupEvent.getPoint());
                        final int column = getColumn(popupEvent.getPoint());
                        LOGGER.info("Edit label on row: {}, column: {}", row, column);
                        if (row > -1) {
                            Object val = getValueAt(row, column);
                            if (val instanceof Port<?>) {
                                val = ((Port<?>) val).toString();
                            }
                            final Object value = val;
                            if (value instanceof String) {
                                // show the port name editor
                                new LabelDialog((String) value, popupEvent.getXOnScreen(), popupEvent.getYOnScreen()) {
                                    @Override
                                    public void labelChanged(String label) {
                                        LOGGER.info("Set the new label for row: {}, column: {}, label: {}", row, column,
                                            label);
                                        setValueAt(label, row, column);
                                    }
                                };
                            }
                        }
                        else {
                            LOGGER.warn("The row is not available!");
                        }
                    }
                };
            }

            protected void showPortListMenu(MouseEvent e, PortListMenu portListMenu, int row, int column) {

                Object value = getValueAt(row, column);
                if (row >= 0 && column >= 0 && (value instanceof Port<?> || value instanceof String)) {
                    if (row >= 0 && getSelectedRowCount() == 0) {
                        setRowSelectionInterval(row, row);
                    }

                    if (value instanceof Port<?>) {
                        // if the port has mapping support enabled the activate the menu
                        if (!((Port<?>) value).isEnabled()) {
                            portListMenu.setMapPortEnabled(((Port<?>) value).isRemappingEnabled(),
                                isPortMappingVisible());
                        }
                        else {
                            portListMenu.setMapPortEnabled(false, isPortMappingVisible());
                        }
                    }
                    else {
                        portListMenu.setMapPortEnabled(false, false);
                    }
                    portListMenu.setInsertPortsEnabled(false, false);

                    grabFocus();
                    portListMenu.show(e.getComponent(), e.getX(), e.getY());
                }
            }

        };
    }

    @Override
    public void listChanged() {
        LOGGER.info("List has changed, remove all rows and add rows again.");
        tableModel.setRowCount(0);

        List<FeedbackPort> ports = getPorts();

        ((FeedbackPortTableModel) tableModel).addRows(ports);

        tabVisibilityListener.setTabVisible(this, isTabVisible());

    }

    @Override
    protected boolean isPackLastColumn() {
        return false;
    }

    @Override
    protected List<FeedbackPort> getPorts() {
        Node node = feedbackPortModel.getSelectedNode();
        if (node != null) {
            return node.getFeedbackPorts();
        }
        return Collections.emptyList();
    }

    @Override
    public boolean isTabVisible() {
        Node node = feedbackPortModel.getSelectedNode();
        if (node != null) {
            boolean isTabVisible = node.hasFeedbackPorts();
            LOGGER.info("Check if tab is visible: {}", isTabVisible);
            return isTabVisible;
        }
        return false;
    }
}
