package org.bidib.wizard.mvc.main.view.cvdef;

import java.util.Map;
import java.util.regex.Pattern;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import org.apache.commons.lang.StringUtils;
import org.bidib.jbidibc.exchange.vendorcv.ModeType;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.common.utils.ImageUtils;
import org.bidib.wizard.mvc.common.view.cvdefinition.CvDefinitionTreeTableModel;
import org.bidib.wizard.mvc.common.view.validation.PropertyValidationI18NSupport;
import org.bidib.wizard.mvc.main.view.panel.NodeTree;
import org.bidib.wizard.utils.InputValidationDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.value.ValueHolder;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.util.PropertyValidationSupport;
import com.jgoodies.validation.view.ValidationComponentUtils;

public class CvValueStringEditor extends CvValueNumberEditor<String> {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(CvValueStringEditor.class);

    private int minLength;

    private int maxLength;

    private ValueModel writeEnabled;

    private String[] allowedValues;

    private Pattern validationPattern;

    public CvValueStringEditor() {
    }

    protected ValidationResult validateModel(
        PresentationModel<CvValueBean<String>> model, CvValueBean<String> cvValueBean) {
        PropertyValidationSupport support = new PropertyValidationI18NSupport(cvValueBean, "validation");

        LOGGER.info("Validate the model, cvValueModel.buffering: {}", cvValueModel.isBuffering());
        if (Boolean.TRUE.equals(writeEnabled.getValue())) {

            if (validationPattern != null) {
                Object current = valueConverterModel.getValue();
                if (current != null) {
                    if (validationPattern.matcher(current.toString()).matches()) {
                        return support.getResult();
                    }
                    support.addError("cvvalue_key", "invalid_value");
                }
                else {
                    support.addError("cvvalue_key", "not_empty");
                }
            }
            else if (allowedValues != null) {
                // only allowed values are checked
                Object current = valueConverterModel.getValue();

                for (String allowed : allowedValues) {
                    if (allowed.equals(current)) {
                        return support.getResult();
                    }
                }
                support.addError("cvvalue_key", "invalid_value");
            }
            else {
                Object val = model.getBufferedModel("cvValue").getValue();
                if (val instanceof String) {
                    String value = (String) val;
                    if (value.length() < minLength || value.length() > maxLength) {
                        support.addError("cvvalue_key", "invalid_value_length;min=" + minLength + ",max=" + maxLength);
                    }
                }
                else if (val == null) {
                    support.addError("cvvalue_key", "not_empty;min=" + minLength + ",max=" + maxLength);
                }
            }
        }
        else {
            LOGGER.debug("Validation is disabled!");
        }
        return support.getResult();
    }

    protected DefaultFormBuilder prepareFormPanel() {

        writeEnabled = new ValueHolder(false);

        // Get buffered model objects.
        cvValueModel = cvAdapter.getBufferedModel("cvValue");
        // valueConverterModel = new ConverterValueModel(cvValueModel, new StringConverter(new DecimalFormat("#")));

        DefaultFormBuilder formBuilder =
            new DefaultFormBuilder(new FormLayout("3dlu, max(20dlu;pref), 3dlu, 30dlu:grow"));
        JTextField textCvValue = BasicComponentFactory.createTextField(cvValueModel, false);
        textCvValue.setDocument(new InputValidationDocument(InputValidationDocument.ALPHANUM_AND_SPACE));
        textCvValue.setEditable(false);

        // the needs reboot Icon is invisible by default
        ImageIcon warnIcon = ImageUtils.createImageIcon(NodeTree.class, "/icons/warn.png");
        JLabel needsRebootIcon =
            new JLabel(Resources.getString(getClass(), "rebootrequired"), warnIcon, SwingConstants.LEADING);
        needsRebootIcon.setVisible(false);

        // init the components
        ValidationComponentUtils.setMandatory(textCvValue, true);
        ValidationComponentUtils.setMessageKey(textCvValue, "validation.cvvalue_key");
        formBuilder.leadingColumnOffset(1);
        formBuilder.nextLine(0);
        formBuilder.append(BasicComponentFactory.createLabel(cvNumberModel, new CvNodeMessageFormat("{0} (CV{1})")), 3);
        formBuilder.nextLine();
        formBuilder.append(textCvValue, 3);
        formBuilder.append(needsRebootIcon);

        // add bindings for enable/disable the textfield
        PropertyConnector.connect(writeEnabled, "value", textCvValue, "editable");
        PropertyConnector.connect(needsRebootModel, "value", needsRebootIcon, "visible");

        return formBuilder;
    }

    @Override
    public void setValue(CvNode cvNode, Map<String, CvNode> cvNumberToNodeMap) {

        boolean timeout = cvNode.getConfigVar().isTimeout();

        // set textfield editable before the value is set because the validation is triggered
        writeEnabled.setValue(!(ModeType.RO.equals(cvNode.getCV().getMode()) || timeout));

        // set the value ... triggers validation
        super.setValue(cvNode, cvNumberToNodeMap);

        // check if 'values' is defined
        validationPattern = null;
        allowedValues = null;

        String regex = cvValueBean.getCvNode().getCV().getRegex();
        if (StringUtils.isNotBlank(regex)) {
            LOGGER.info("Found validationRegex: {}", regex);

            try {
                validationPattern = Pattern.compile(regex);
            }
            catch (Exception ex) {
                LOGGER.warn("Compile validation pattern from regex failed: {}", regex);
            }
        }

        if (validationPattern == null) {
            String values = cvValueBean.getCvNode().getCV().getValues();
            allowedValues = prepareAllowedValues(values);
        }

        String min = cvValueBean.getCvNode().getCV().getMin();
        if (min != null) {
            try {
                minLength = Integer.parseInt(min);
            }
            catch (NumberFormatException ex) {
                LOGGER.debug("Parse min length failed: {}", min);
                minLength = 0;
            }
        }
        else {
            minLength = 0;
        }
        String max = cvValueBean.getCvNode().getCV().getMax();
        if (max != null) {
            try {
                maxLength = Integer.parseInt(max);
            }
            catch (NumberFormatException ex) {
                LOGGER.debug("Parse max length failed: {}", max);
                maxLength = 255;
            }
        }
        else {
            maxLength = 255;
        }

        triggerValidation(null);
    }

    protected void setValueInternally(CvNode cvNode, boolean fromSource) {

        getTrigger().triggerFlush();

        String value = null;
        // this must be updated in the text field ...
        if (!fromSource && cvNode.getNewValue() instanceof String) {
            value = (String) cvNode.getNewValue();
        }
        else if (StringUtils.isNotBlank(cvNode.getConfigVar().getValue())) {
            value = cvNode.getConfigVar().getValue();
        }

        LOGGER.debug("Set the new value: {}", value);
        cvValueBean.setCvValue(value);

        // trigger.triggerFlush();
        cvAdapter.resetChanged();
    }

    @Override
    public void updateCvValues(Map<String, CvNode> cvNumberToNodeMap, CvDefinitionTreeTableModel treeModel) {
        CvNode cvNode = (CvNode) cvNumberModel.getValue();
        cvNode.setValueAt(cvValueBean.getCvValue(), CvNode.COLUMN_NEW_VALUE);
    }
}
