package org.bidib.wizard.mvc.dmx.model;

/**
 * The DmxModel is used by the DmxModelerPanel to get access to the DMX scenery that is edited in the panel.
 */
public class DmxModel {

    private final DmxScenery dmxScenery;

    private final DmxSceneryModel dmxSceneryModel;

    /**
     * @param dmxScenery
     *            the dmx scenery
     */
    public DmxModel(final DmxScenery dmxScenery, final DmxSceneryModel dmxSceneryModel) {
        this.dmxScenery = dmxScenery;
        this.dmxSceneryModel = dmxSceneryModel;
    }

    /**
     * @return the dmx scenery
     */
    public DmxScenery getDmxScenery() {
        return dmxScenery;
    }

    /**
     * @return the dmxSceneryModel
     */
    public DmxSceneryModel getDmxSceneryModel() {
        return dmxSceneryModel;
    }
}
