package org.bidib.wizard.mvc.main.view.panel;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;

import org.apache.commons.collections4.CollectionUtils;
import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.Feature;
import org.bidib.jbidibc.core.enumeration.IoBehaviourSwitchEnum;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.LoadTypeEnum;
import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.comm.SwitchPortStatus;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.dialog.LabelDialog;
import org.bidib.wizard.mvc.common.view.renderer.PortIdentifierTableCellRenderer;
import org.bidib.wizard.mvc.main.controller.SwitchPortPanelController;
import org.bidib.wizard.mvc.main.model.ConfigurablePort;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.model.Port;
import org.bidib.wizard.mvc.main.model.SimplePortTableModel;
import org.bidib.wizard.mvc.main.model.SwitchPort;
import org.bidib.wizard.mvc.main.model.SwitchPortTableModel;
import org.bidib.wizard.mvc.main.model.listener.SwitchPortListener;
import org.bidib.wizard.mvc.main.view.menu.listener.PortListMenuListener;
import org.bidib.wizard.mvc.main.view.table.ConfigurablePortComboBoxEditor;
import org.bidib.wizard.mvc.main.view.table.ConfigurablePortComboBoxRenderer;
import org.bidib.wizard.mvc.main.view.table.DefaultPortListMenuListener;
import org.bidib.wizard.mvc.main.view.table.PortComboBoxWithButtonEditor;
import org.bidib.wizard.mvc.main.view.table.PortComboBoxWithButtonRenderer;
import org.bidib.wizard.mvc.main.view.table.PortConfigErrorAwareEditor;
import org.bidib.wizard.mvc.main.view.table.PortConfigErrorAwareRenderer;
import org.bidib.wizard.mvc.main.view.table.PortTable;
import org.bidib.wizard.mvc.main.view.table.PortTicksEditor;
import org.bidib.wizard.mvc.main.view.table.PortTicksRenderer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jidesoft.grid.TableColumnChooser;

public class SwitchPortListPanel
    extends SimplePortListPanel<SwitchPortStatus, SwitchPort, SwitchPortListener<SwitchPortStatus>> {
    private static final Logger LOGGER = LoggerFactory.getLogger(SwitchPortListPanel.class);

    private static final long serialVersionUID = 1L;

    private final MainModel mainModel;

    private final class IoBehaviourComboBoxRenderer extends ConfigurablePortComboBoxRenderer<IoBehaviourSwitchEnum> {
        private static final long serialVersionUID = 1L;

        public IoBehaviourComboBoxRenderer(IoBehaviourSwitchEnum[] items, byte... pcfgType) {
            super(SwitchPortTableModel.COLUMN_PORT_INSTANCE, items, pcfgType);
        }

        @Override
        protected Object getCurrentValue(ConfigurablePort<?> port) {
            IoBehaviourSwitchEnum value = ((SwitchPort) port).getOutputBehaviour();
            if (IoBehaviourSwitchEnum.UNKNOWN == value) {
                value = null;
            }
            return value;
        }
    }

    private final class LoadTypeComboBoxRenderer extends ConfigurablePortComboBoxRenderer<LoadTypeEnum> {
        private static final long serialVersionUID = 1L;

        public LoadTypeComboBoxRenderer(LoadTypeEnum[] items, byte... pcfgType) {
            super(SwitchPortTableModel.COLUMN_PORT_INSTANCE, items, pcfgType);
        }

        @Override
        protected Object getCurrentValue(ConfigurablePort<?> port) {
            LoadTypeEnum value = ((SwitchPort) port).getLoadType();
            if (LoadTypeEnum.UNKNOWN == value) {
                value = null;
            }
            return value;
        }
    }

    public SwitchPortListPanel(final SwitchPortPanelController controller, MainModel model) {
        super(new SwitchPortTableModel(model), model.getSwitchPorts(),
            Resources.getString(SwitchPortListPanel.class, "emptyTable"));
        mainModel = model;

        mainModel.addSwitchPortListListener(this);

        TableColumn tc = table.getColumnModel().getColumn(SwitchPortTableModel.COLUMN_LABEL);
        tc.setCellRenderer(new PortConfigErrorAwareRenderer(SwitchPortTableModel.COLUMN_LABEL));
        tc.setCellEditor(new PortConfigErrorAwareEditor(SwitchPortTableModel.COLUMN_PORT_INSTANCE));
        tc.setIdentifier(Integer.valueOf(SwitchPortTableModel.COLUMN_LABEL));

        IoBehaviourComboBoxRenderer tableCellRenderer =
            new IoBehaviourComboBoxRenderer(IoBehaviourSwitchEnum.getValues(), BidibLibrary.BIDIB_PCFG_SWITCH_CTRL);

        TableCellEditor tableCellEditor =
            new ConfigurablePortComboBoxEditor<IoBehaviourSwitchEnum>(SwitchPortTableModel.COLUMN_PORT_INSTANCE,
                IoBehaviourSwitchEnum.getValues(), IoBehaviourSwitchEnum.getValues());

        tc = table.getColumnModel().getColumn(SwitchPortTableModel.COLUMN_IO_BEHAVIOUR);
        tc.setCellRenderer(tableCellRenderer);
        tc.setCellEditor(tableCellEditor);
        tc.setIdentifier(Integer.valueOf(SwitchPortTableModel.COLUMN_IO_BEHAVIOUR));

        tc = table.getColumnModel().getColumn(SwitchPortTableModel.COLUMN_SWITCH_OFF_TIME);
        tc.setCellRenderer(new PortTicksRenderer());
        tc.setCellEditor(new PortTicksEditor(0, 255));
        tc.setIdentifier(Integer.valueOf(SwitchPortTableModel.COLUMN_SWITCH_OFF_TIME));

        LoadTypeComboBoxRenderer tableCellRendererLoadType =
            new LoadTypeComboBoxRenderer(LoadTypeEnum.getValues(), BidibLibrary.BIDIB_PCFG_LOAD_TYPE);

        LoadTypeEnum[] loadTypes = Arrays.copyOf(LoadTypeEnum.getValues(), LoadTypeEnum.getValues().length - 1);
        TableCellEditor tableCellEditorLoadType =
            new ConfigurablePortComboBoxEditor<LoadTypeEnum>(SwitchPortTableModel.COLUMN_PORT_INSTANCE,
                LoadTypeEnum.getValues(), loadTypes);

        tc = table.getColumnModel().getColumn(SwitchPortTableModel.COLUMN_LOAD_TYPE);
        tc.setCellRenderer(tableCellRendererLoadType);
        tc.setCellEditor(tableCellEditorLoadType);
        tc.setIdentifier(Integer.valueOf(SwitchPortTableModel.COLUMN_LOAD_TYPE));

        tc = table.getColumnModel().getColumn(SwitchPortTableModel.COLUMN_PORT_IDENTIFIER);
        tc.setCellRenderer(new PortIdentifierTableCellRenderer());
        tc.setMaxWidth(80);
        tc.setIdentifier(Integer.valueOf(SwitchPortTableModel.COLUMN_PORT_IDENTIFIER));

        // Set the status renderer
        tc = table.getColumnModel().getColumn(SwitchPortTableModel.COLUMN_STATUS);
        tc.setIdentifier(Integer.valueOf(SwitchPortTableModel.COLUMN_STATUS));
        tc.setCellRenderer(new DefaultTableCellRenderer());
        tc.setMaxWidth(80);

        TableColumn buttonColumn = table.getColumnModel().getColumn(SwitchPortTableModel.COLUMN_TEST);
        buttonColumn.setIdentifier(Integer.valueOf(SwitchPortTableModel.COLUMN_TEST));

        buttonColumn.setCellRenderer(new PortComboBoxWithButtonRenderer<BidibStatus, SwitchPortStatus>(
            table.getActions(SwitchPortStatus.ON), ">") {
            private static final long serialVersionUID = 1L;

            @Override
            protected void setSelectedValue(Port<?> port) {
                SwitchPortStatus oppositeStatus = null;
                if (port != null) {
                    oppositeStatus = (SwitchPortStatus) port.getStatus();
                }
                comboBox.setSelectedItem(oppositeStatus);
            }
        });
        PortComboBoxWithButtonEditor editor =
            new PortComboBoxWithButtonEditor(table.getActions(SwitchPortStatus.ON), ">") {
                private static final long serialVersionUID = 1L;

                @Override
                protected void setSelectedValue(Port<?> port) {
                    SwitchPortStatus oppositeStatus = null;
                    if (port != null) {
                        oppositeStatus = (SwitchPortStatus) port.getStatus();
                    }
                    comboBox.setSelectedItem(oppositeStatus);
                }
            };
        editor.addButtonListener((SwitchPortTableModel) tableModel);
        buttonColumn.setCellEditor(editor);

        TableColumnChooser.hideColumn(table, SwitchPortTableModel.COLUMN_PORT_INSTANCE);
    }

    @Override
    protected PortTable createPortTable(
        final SimplePortTableModel<SwitchPortStatus, SwitchPort, SwitchPortListener<SwitchPortStatus>> tableModel,
        String emptyTableText) {

        PortTable portTable = new PortTable(tableModel, emptyTableText) {
            private static final long serialVersionUID = 1L;

            @Override
            public void clearTable() {
            }

            @Override
            protected PortListMenuListener createMenuListener() {
                // create the port list menu
                return new DefaultPortListMenuListener() {
                    @Override
                    public void editLabel() {
                        final int row = getRow(popupEvent.getPoint());
                        if (row > -1) {
                            Object val = getValueAt(row, 0);
                            if (val instanceof Port<?>) {
                                val = ((Port<?>) val).toString();
                            }
                            final Object value = val;
                            if (value instanceof String) {
                                // show the port name editor
                                new LabelDialog((String) value, popupEvent.getXOnScreen(), popupEvent.getYOnScreen()) {
                                    @Override
                                    public void labelChanged(String label) {
                                        setValueAt(label, row, 0);
                                    }
                                };
                            }
                        }
                        else {
                            LOGGER.warn("The row is not available!");
                        }
                    }

                    @Override
                    public void mapPort() {

                        final int row = getRow(popupEvent.getPoint());
                        if (row > -1) {
                            Object val = getValueAt(row, 0);
                            if (val instanceof SwitchPort) {
                                SwitchPort switchPort = (SwitchPort) val;
                                LOGGER.info("Change mapping for port: {}", switchPort);

                                // confirm switch to switch port
                                int result =
                                    JOptionPane.showConfirmDialog(
                                        JOptionPane.getFrameForComponent(SwitchPortListPanel.this),
                                        Resources.getString(SwitchPortListPanel.class, "switch-port-confirm"),
                                        Resources.getString(SwitchPortListPanel.class, "switch-port-title"),
                                        JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
                                if (result == JOptionPane.OK_OPTION) {
                                    LOGGER.info("Change the port to an switch port.");

                                    SwitchPortTableModel switchPortTableModel = (SwitchPortTableModel) getModel();
                                    switchPortTableModel.changePortType(LcOutputType.SWITCHPORT, switchPort);
                                }
                            }
                        }
                    }
                };
            }

        };

        return portTable;
    }

    @Override
    public void listChanged() {
        LOGGER.info("The port list has changed.");

        super.listChanged();

        boolean hasPortIdentifiers = false;
        List<SwitchPort> ports = new LinkedList<>();
        ports.addAll(getPorts());
        synchronized (ports) {
            for (SwitchPort port : ports) {
                if (port.isRemappingEnabled()) {
                    hasPortIdentifiers = true;
                    break;
                }
            }
        }

        if (mainModel.getSelectedNode() != null) {
            LOGGER.info("A node is selected.");
            boolean hasSwitchPortConfigIo = false;
            boolean hasSwitchPortConfigTicks = false;
            boolean hasSwitchPortConfigLoadType = false;
            Node node = mainModel.getSelectedNode();
            if (node.getNode().isPortFlatModelAvailable() && CollectionUtils.isNotEmpty(node.getEnabledSwitchPorts())) {
                LOGGER.info("Check if at least one switch port has the switch port config available.");
                for (SwitchPort port : node.getEnabledSwitchPorts()) {
                    if (!hasSwitchPortConfigIo) {
                        hasSwitchPortConfigIo = port.isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_SWITCH_CTRL);
                    }
                    if (!hasSwitchPortConfigTicks) {
                        hasSwitchPortConfigTicks = port.isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_TICKS);
                    }
                    if (!hasSwitchPortConfigLoadType) {
                        hasSwitchPortConfigLoadType = port.isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_LOAD_TYPE);
                    }

                    if (hasSwitchPortConfigIo && hasSwitchPortConfigTicks && hasSwitchPortConfigLoadType) {
                        // found one -> show the column
                        break;
                    }
                }
            }
            else {
                Feature switchPortConfigAvailable =
                    Feature.findFeature(node.getNode().getFeatures(), BidibLibrary.FEATURE_SWITCH_CONFIG_AVAILABLE);
                if (switchPortConfigAvailable != null) {
                    hasSwitchPortConfigIo = (switchPortConfigAvailable.getValue() > 0);
                    hasSwitchPortConfigTicks = hasSwitchPortConfigIo;
                }
            }
            LOGGER.info(
                "List has changed, hasPortIdentifiers: {}, hasSwitchPortConfigIo: {}, hasSwitchPortConfigTicks: {}",
                hasPortIdentifiers, hasSwitchPortConfigIo, hasSwitchPortConfigTicks);

            // keep the column index of the column to insert in the view
            int viewColumnIndex = SwitchPortTableModel.COLUMN_IO_BEHAVIOUR;

            // show/hide the IO behaviour column
            viewColumnIndex =
                table.setColumnVisible(SwitchPortTableModel.COLUMN_IO_BEHAVIOUR, viewColumnIndex,
                    hasSwitchPortConfigIo);

            // show/hide the ticks column
            viewColumnIndex =
                table.setColumnVisible(SwitchPortTableModel.COLUMN_SWITCH_OFF_TIME, viewColumnIndex,
                    hasSwitchPortConfigTicks);

            // show/hide the load type column
            viewColumnIndex =
                table.setColumnVisible(SwitchPortTableModel.COLUMN_LOAD_TYPE, viewColumnIndex,
                    hasSwitchPortConfigLoadType);

            // show/hide the port identifier column
            viewColumnIndex =
                table.setColumnVisible(SwitchPortTableModel.COLUMN_PORT_IDENTIFIER, viewColumnIndex,
                    hasPortIdentifiers);
        }
    }

    @Override
    protected void refreshPorts() {
        LOGGER.info("refresh the ports.");

        Node node = mainModel.getSelectedNode();
        if (node != null) {
            if (node.getNode().isPortFlatModelAvailable()) {
                if (CollectionUtils.isNotEmpty(node.getGenericPorts())) {
                    mainModel.getSwitchPorts();
                }
                else {
                    LOGGER.info(
                        "The node supports flat port model but no generic ports are available. Skip get switch ports.");
                }
            }
            else {
                mainModel.getSwitchPorts();
            }
        }
        LOGGER.info("refresh the ports has finished.");
    }
}
