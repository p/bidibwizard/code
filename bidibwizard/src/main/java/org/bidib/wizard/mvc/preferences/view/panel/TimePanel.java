package org.bidib.wizard.mvc.preferences.view.panel;

import java.awt.BorderLayout;
import java.text.NumberFormat;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;

import javax.swing.JFormattedTextField;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.preferences.model.PreferencesModel;
import org.bidib.wizard.mvc.preferences.view.listener.PreferencesViewListener;

import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.debug.FormDebugPanel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.FormLayout;

/**
 * This panel displays the time preferences.
 * 
 */
public class TimePanel {
    private final Collection<PreferencesViewListener> listeners = new LinkedList<PreferencesViewListener>();

    private static final String ENCODED_DIALOG_COLUMN_SPECS = "pref, 3dlu, pref, 3dlu, fill:50dlu:grow";

    private final PreferencesModel preferencesModel;

    public TimePanel(final PreferencesModel model) {
        this.preferencesModel = model;
    }

    public JPanel createPanel() {

        DefaultFormBuilder dialogBuilder = null;
        boolean debugDialog = false;
        if (debugDialog) {
            JPanel panel = new FormDebugPanel();
            dialogBuilder = new DefaultFormBuilder(new FormLayout(ENCODED_DIALOG_COLUMN_SPECS), panel);
        }
        else {
            JPanel panel = new JPanel(new BorderLayout());
            dialogBuilder = new DefaultFormBuilder(new FormLayout(ENCODED_DIALOG_COLUMN_SPECS), panel);
        }
        dialogBuilder.border(Borders.TABBED_DIALOG);

        final JTextField timeFactor = new JFormattedTextField(NumberFormat.getNumberInstance());

        timeFactor.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void changedUpdate(DocumentEvent e) {
                valueChanged();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                valueChanged();
            }

            @Override
            public void insertUpdate(DocumentEvent e) {
                valueChanged();
            }

            private void valueChanged() {
                if (!timeFactor.getText().isEmpty()) {
                    try {
                        fireTimeFactorChanged(Integer.parseInt(timeFactor.getText()));
                    }
                    catch (NumberFormatException e) {
                    }
                }
            }
        });
        timeFactor.setText(String.valueOf(preferencesModel.getTimeFactor()));

        dialogBuilder.append(Resources.getString(getClass(), "timeFactor"), timeFactor);
        dialogBuilder.nextLine();

        final JSpinner startTime = new JSpinner(new SpinnerDateModel());

        startTime.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                JSpinner spinner = (JSpinner) e.getSource();

                fireStartTimeChanged((Date) spinner.getValue());
            }
        });
        startTime.setEditor(new JSpinner.DateEditor(startTime, "HH:mm"));
        startTime.setValue(preferencesModel.getStartTime());

        dialogBuilder.append(Resources.getString(getClass(), "startTime"), startTime);

        return dialogBuilder.build();
    }

    public void addPreferencesViewListener(PreferencesViewListener l) {
        listeners.add(l);
    }

    private void fireStartTimeChanged(Date startTime) {
        for (PreferencesViewListener l : listeners) {
            l.startTimeChanged(startTime);
        }
    }

    private void fireTimeFactorChanged(int timeFactor) {
        for (PreferencesViewListener l : listeners) {
            l.timeFactorChanged(timeFactor);
        }
    }
}
