package org.bidib.wizard.mvc.main.view.table;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.Collection;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.bidib.wizard.comm.FeedbackPortStatus;
import org.bidib.wizard.mvc.common.view.panel.DisabledPanel;
import org.bidib.wizard.mvc.main.model.FeedbackAddressData;
import org.bidib.wizard.mvc.main.model.FeedbackConfidenceData;
import org.bidib.wizard.mvc.main.model.FeedbackDynStateData;
import org.bidib.wizard.mvc.main.model.FeedbackPort;
import org.bidib.wizard.mvc.main.model.FeedbackTimestampData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FeedbackPortTableCellRenderer implements TableCellRenderer {
    private static final Logger LOGGER = LoggerFactory.getLogger(FeedbackPortTableCellRenderer.class);

    private JPanel panel;

    public FeedbackPortTableCellRenderer() {
        panel = new JPanel(new GridBagLayout());
        panel.setOpaque(true);

    }

    public Component getTableCellRendererComponent(
        JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

        // JPanel result = new JPanel(new GridBagLayout());
        // clear the panel
        panel.removeAll();

        if (value instanceof FeedbackPort) {
            FeedbackPort port = (FeedbackPort) value;

            if (port != null) {
                JPanel innerPanel = new JPanel(new GridBagLayout());
                FeedbackPortStatus status = (FeedbackPortStatus) port.getStatus();

                Collection<FeedbackAddressData> addresses = port.getAddresses();
                Collection<FeedbackDynStateData> dynStates = port.getDynStates();

                GridBagConstraints c = new GridBagConstraints();

                c.anchor = GridBagConstraints.CENTER;
                c.gridx = 0;
                c.gridy = 0;
                c.weightx = 1;

                // prepare the port label
                String label = null;

                if (StringUtils.isNotBlank(port.getLabel())) {
                    label = String.format("%1$02d : %2$s", port.getId(), port.getLabel());
                }
                else if (port.getId() > -1) {
                    label = String.format("%1$02d", port.getId());
                }
                else {
                    label = " ";
                }

                JLabel portLabel = new JLabel(label);
                Font f = portLabel.getFont();

                portLabel.setFont(f.deriveFont(f.getStyle() | Font.BOLD));
                innerPanel.add(portLabel, c);

                final FeedbackConfidenceData confidence = port.getConfidence();
                if (confidence != null) {
                    c.gridy++;
                    JLabel confidenceLabel =
                        new JLabel((confidence.isVoid() ? "1" : "0") + "," + (confidence.isFreeze() ? "1" : "0") + ","
                            + (confidence.isNoSignal() ? "1" : "0"));

                    StringBuilder sb = new StringBuilder("<html><b>CONFIDENCE</b><br>VOID: ");
                    sb.append(confidence.isVoid() ? "true" : "false").append("<br>");
                    sb.append("FREEZE: ").append(confidence.isFreeze() ? "true" : "false").append("<br>");
                    sb.append("NOSIGNAL: ").append(confidence.isNoSignal() ? "true" : "false").append("</html>");
                    // LOGGER.info("Set the tooltip text: {}", sb);
                    panel.setToolTipText(sb.toString());
                    innerPanel.add(confidenceLabel, c);
                }

                if (addresses != null) {
                    for (FeedbackAddressData address : addresses) {
                        c.gridy++;
                        innerPanel.add(new JLabel(getAddressData(address, dynStates)), c);
                    }
                }

                FeedbackTimestampData timestamp = port.getTimestamp();
                if (timestamp != null) {
                    c.gridy++;
                    innerPanel.add(new JLabel(timestamp.toString()), c);
                }

                c.fill = GridBagConstraints.BOTH;
                c.gridy++;
                c.weighty = 1;
                innerPanel.add(Box.createVerticalGlue(), c);

                // background color
                if (status == FeedbackPortStatus.OCCUPIED) {
                    innerPanel.setBackground(Color.RED);
                }
                else {
                    innerPanel.setBackground(Color.GREEN);
                }

                // gray out depending on confidence
                DisabledPanel disabledInnerPanel = new DisabledPanel(innerPanel);

                c.gridy = 0;
                panel.add(disabledInnerPanel, c);
                boolean enabled = false;
                if (confidence != null) {
                    enabled = !confidence.isFreeze() && confidence.isSignal() && confidence.isValid();
                }
                else {
                    LOGGER.warn("No confidence available: {}", port);
                }

                disabledInnerPanel.setEnabled(enabled);
            }
        }
        return panel;
    }

    private String getAddressData(FeedbackAddressData address, final Collection<FeedbackDynStateData> dynStates) {
        StringBuffer result = new StringBuffer();

        if (address != null) {
            if (result.length() > 0) {
                result.append(',');
            }
            int addr = address.getAddress();
            result.append(addr);
            switch (address.getType()) {
                case LOCOMOTIVE_FORWARD:
                    result.append(">>");
                    break;
                case LOCOMOTIVE_BACKWARD:
                    result.append("<<");
                    break;
                default:
                    break;
            }
            result.append('(');
            result.append(address.getSpeed());
            result.append(')');

            if (CollectionUtils.isNotEmpty(dynStates)) {
                // search dyn state values
                for (FeedbackDynStateData dynState : dynStates) {
                    // append the dynstate
                    if (dynState.getAddress() == addr) {
                        dynState.appendDynState(result);
                        break;
                    }
                }
            }
        }
        else {
            result.append("[]");
        }
        return result.toString();
    }
}
