package org.bidib.wizard.mvc.common.view.converter;

import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.stepcontrol.model.TurnTableType;

import com.jgoodies.binding.value.BindingConverter;

public class TurnTableTypeConverter implements BindingConverter<TurnTableType, String> {

    @Override
    public String targetValue(TurnTableType sourceValue) {
        if (sourceValue != null) {
            String name = sourceValue.name();

            return Resources.getString(TurnTableType.class, name);
        }
        return null;
    }

    @Override
    public TurnTableType sourceValue(String targetValue) {
        // no implementation
        return null;
    }

}
