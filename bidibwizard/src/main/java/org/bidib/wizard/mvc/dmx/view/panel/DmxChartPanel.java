package org.bidib.wizard.mvc.dmx.view.panel;

import java.awt.Color;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.bidib.wizard.comm.LightPortStatus;
import org.bidib.wizard.comm.MacroStatus;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.dmx.model.DmxChannel;
import org.bidib.wizard.mvc.dmx.model.DmxLightPort;
import org.bidib.wizard.mvc.dmx.model.DmxModel;
import org.bidib.wizard.mvc.dmx.model.DmxScenery;
import org.bidib.wizard.mvc.dmx.model.DmxSceneryPoint;
import org.bidib.wizard.mvc.dmx.view.panel.renderer.DmxLineRenderer;
import org.bidib.wizard.mvc.dmx.view.panel.renderer.SceneryPointArrowAnnotation;
import org.bidib.wizard.mvc.main.model.BacklightPort;
import org.bidib.wizard.mvc.main.model.LightPort;
import org.bidib.wizard.mvc.main.model.Macro;
import org.bidib.wizard.mvc.main.model.Port;
import org.bidib.wizard.mvc.main.model.function.MacroFunction;
import org.bidib.wizard.utils.PortUtils;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.entity.ChartEntity;
import org.jfree.chart.entity.EntityCollection;
import org.jfree.chart.entity.XYItemEntity;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.AbstractXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.UnknownKeyException;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.RectangleEdge;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.value.ValueHolder;
import com.jgoodies.binding.value.ValueModel;

public class DmxChartPanel extends ChartPanel {

    static final Logger LOGGER = LoggerFactory.getLogger(DmxChartPanel.class);

    private static final long serialVersionUID = 1L;

    private ChartEntity entityToDragLine;

    private DmxDataItem orgDmxDataItemToDragLine;

    private JMenu subMenuAddPoint;

    private JMenu subMenuAddMacro;

    private JMenu subMenuSetPort;

    private JMenu subMenuSetAction;

    private JPopupMenu.Separator setPortSeparator;

    private Point currentMousePoint = new Point();

    private final DmxModel dmxModel;

    private JMenuItem removePointMenuItem;

    private ValueModel dimmStretch;

    private PropertyChangeListener lightPortListener;

    private PropertyChangeListener backlightPortListener;

    public DmxChartPanel(JFreeChart chart, final DmxModel dmxModel) {
        super(chart);
        this.dmxModel = dmxModel;

        setPopupMenu(createPopupMenu(false, false, false, true));

        dimmStretch = new ValueHolder(dmxModel.getDmxSceneryModel().getDimmStretch());

        // add change listener for dimmStretch
        this.dmxModel.getDmxSceneryModel().addPropertyChangeListener(DmxScenery.PROPERTY_DIMM_STRETCH,
            new PropertyChangeListener() {

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    LOGGER.info("The dimmStretch value has been changed: {}", evt.getNewValue());
                    // update the scenery models
                    if (evt.getNewValue() instanceof Number) {
                        int newDimmStretch = ((Number) evt.getNewValue()).intValue();

                        LOGGER.info("Set the new dimmStretch value: {}", newDimmStretch);
                        dimmStretch.setValue(newDimmStretch);

                        List<DmxSeries> dmxSeriesList = getDataSet().getSeries();
                        for (DmxSeries dmxSeries : dmxSeriesList) {
                            refreshSceneryPointAnnotations(getChart().getXYPlot(), dmxSeries);
                        }
                    }
                }
            });

        // add listener to react on configuration changes of light and backlight port
        lightPortListener = new PropertyChangeListener() {
            @Override
            public void propertyChange(final PropertyChangeEvent evt) {
                LOGGER.info("Lightport has changed, current property name: {}", evt.getPropertyName());

                if (SwingUtilities.isEventDispatchThread()) {
                    switch (evt.getPropertyName()) {
                        case LightPort.PROPERTY_DIM_MAX:
                        case LightPort.PROPERTY_DIM_MIN:
                            List<DmxSeries> dmxSeriesList = getDataSet().getSeries();
                            for (DmxSeries dmxSeries : dmxSeriesList) {
                                refreshSceneryPointAnnotations(getChart().getXYPlot(), dmxSeries);
                            }
                            break;
                        default:
                            break;
                    }
                }
                else {
                    SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            switch (evt.getPropertyName()) {
                                case LightPort.PROPERTY_DIM_MAX:
                                case LightPort.PROPERTY_DIM_MIN:
                                    List<DmxSeries> dmxSeriesList = getDataSet().getSeries();
                                    for (DmxSeries dmxSeries : dmxSeriesList) {
                                        refreshSceneryPointAnnotations(getChart().getXYPlot(), dmxSeries);
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }
                    });
                }

            }
        };
        List<DmxLightPort> lightPorts = this.dmxModel.getDmxSceneryModel().getLightPorts();
        if (CollectionUtils.isNotEmpty(lightPorts)) {
            for (DmxLightPort lightPort : lightPorts) {
                lightPort.addPropertyChangeListener(lightPortListener);
            }
        }

        backlightPortListener = new PropertyChangeListener() {
            @Override
            public void propertyChange(final PropertyChangeEvent evt) {
                LOGGER.info("Backlightport has changed, current property name: {}", evt.getPropertyName());

                if (SwingUtilities.isEventDispatchThread()) {
                    switch (evt.getPropertyName()) {
                        case BacklightPort.PROPERTY_DIM_SLOPE_UP:
                        case BacklightPort.PROPERTY_DIM_SLOPE_DOWN:
                            List<DmxSeries> dmxSeriesList = getDataSet().getSeries();
                            for (DmxSeries dmxSeries : dmxSeriesList) {
                                refreshSceneryPointAnnotations(getChart().getXYPlot(), dmxSeries);
                            }
                            break;
                        default:
                            break;
                    }
                }
                else {
                    SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            switch (evt.getPropertyName()) {
                                case BacklightPort.PROPERTY_DIM_SLOPE_UP:
                                case BacklightPort.PROPERTY_DIM_SLOPE_DOWN:
                                    List<DmxSeries> dmxSeriesList = getDataSet().getSeries();
                                    for (DmxSeries dmxSeries : dmxSeriesList) {
                                        refreshSceneryPointAnnotations(getChart().getXYPlot(), dmxSeries);
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }
                    });
                }
            }
        };
        List<BacklightPort> backlightPorts = this.dmxModel.getDmxSceneryModel().getBacklightPorts();
        if (CollectionUtils.isNotEmpty(backlightPorts)) {
            for (BacklightPort backlightPort : backlightPorts) {
                backlightPort.addPropertyChangeListener(backlightPortListener);
            }
        }
    }

    public void cleanup() {
        LOGGER.info("Cleanup the listeners.");
        // remove property change listener from BacklightPorts and DmxLightPorts
        if (lightPortListener != null) {
            List<DmxLightPort> lightPorts = this.dmxModel.getDmxSceneryModel().getLightPorts();
            if (CollectionUtils.isNotEmpty(lightPorts)) {
                for (DmxLightPort lightPort : lightPorts) {
                    lightPort.removePropertyChangeListener(lightPortListener);
                }
            }
            lightPortListener = null;
        }

        if (backlightPortListener != null) {
            List<BacklightPort> backlightPorts = this.dmxModel.getDmxSceneryModel().getBacklightPorts();
            if (CollectionUtils.isNotEmpty(backlightPorts)) {
                for (BacklightPort backlightPort : backlightPorts) {
                    backlightPort.removePropertyChangeListener(backlightPortListener);
                }
            }
            backlightPortListener = null;
        }
    }

    protected Point getCurrentMousePoint() {

        // TODO the mouse point is not valid after the legend has grown ...
        return currentMousePoint;
    }

    private XYSeriesCollection getDataSet() {
        return (XYSeriesCollection) getChart().getXYPlot().getDataset();
    }

    private DmxSeries getDmxSeries(Comparable<String> seriesKey) {
        return (DmxSeries) getDataSet().getSeries(seriesKey);
    }

    @Override
    protected void displayPopupMenu(int x, int y) {

        LOGGER.info("Display popupMenu, x: {}, y: {}", x, y);

        Rectangle2D plotArea = getScreenDataArea();
        XYPlot plot = (XYPlot) getChart().getPlot(); // your plot

        // calculate the position
        double chartX = plot.getDomainAxis().java2DToValue(x, plotArea, RectangleEdge.BOTTOM);
        double chartY = plot.getRangeAxis().java2DToValue(y, plotArea, RectangleEdge.LEFT);

        LOGGER.info("Current chartX: {}, chartY: {}", chartX, chartY);

        currentMousePoint.setLocation(chartX, chartY);

        updatePopupMenu();

        super.displayPopupMenu(x, y);
    }

    @Override
    protected JPopupMenu createPopupMenu(boolean properties, boolean copy, boolean save, boolean print, boolean zoom) {
        JPopupMenu defaultMenu = super.createPopupMenu(properties, copy, save, print, zoom);
        defaultMenu.setLabel(localizationResources.getString("Chart") + ":");

        return defaultMenu;
    }

    private void updatePopupMenu() {
        if (dmxModel != null && dmxModel.getDmxScenery() != null) {

            DmxScenery dmxScenery = dmxModel.getDmxScenery();
            if (!CollectionUtils.isNotEmpty(dmxScenery.getUsedChannels())) {
                // no channels defined
                return;
            }

            DmxChannel selectedDmxChannel = null;

            PointRemoveAction pointRemoveAction = null;
            if (this.getChart() != null) {
                int selectedDmxChannelId = -1;

                DmxLineRenderer dmxLineRenderer = (DmxLineRenderer) ((XYPlot) this.getChart().getPlot()).getRenderer();

                boolean isHighlighted = dmxLineRenderer.isHighlighted();
                LOGGER.info("isHighlighted: {}, row: {}, column: {}", isHighlighted, dmxLineRenderer.getRow(),
                    dmxLineRenderer.getColumn());

                if (isHighlighted) {
                    if (subMenuSetPort == null) {
                        // add submenu for adding points
                        JPopupMenu defaultMenu = getPopupMenu();
                        subMenuSetPort = new JMenu(Resources.getString(DmxChartPanel.class, "set-port"));
                        defaultMenu.insert(subMenuSetPort, 0);

                        subMenuAddMacro = new JMenu(Resources.getString(DmxChartPanel.class, "add-macro"));
                        defaultMenu.insert(subMenuAddMacro, 1);

                        subMenuSetAction = new JMenu(Resources.getString(DmxChartPanel.class, "set-action"));
                        defaultMenu.insert(subMenuSetAction, 2);
                        // TODO add the macros that are available

                        setPortSeparator = new JPopupMenu.Separator();
                        defaultMenu.insert(setPortSeparator, 3);
                    }

                    subMenuSetPort.setVisible(isHighlighted);
                    subMenuSetPort.removeAll();

                    subMenuAddMacro.setVisible(isHighlighted);
                    subMenuSetAction.setVisible(false);

                    setPortSeparator.setVisible(isHighlighted);

                    if (getChart().getPlot() instanceof XYPlot) {

                        try {
                            DmxDataItem item =
                                (DmxDataItem) getDataSet()
                                    .getSeries(dmxLineRenderer.getRow()).getDataItem(dmxLineRenderer.getColumn());
                            LOGGER.info("Fetched selected item: {}", item);

                            if (item != null) {
                                String seriesKey = (String) getDataSet().getSeriesKey(dmxLineRenderer.getRow());
                                LOGGER.info("Currently no port assigned, seriesKey: {}, row: {}", seriesKey,
                                    dmxLineRenderer.getRow());

                                selectedDmxChannelId = Integer.parseInt(seriesKey);
                                // we must keep the coordinates of the item to overwrite the item later ...

                                // add the ports assigned to the selected channel
                                for (DmxChannel dmxChannel : dmxScenery.getUsedChannels()) {
                                    LOGGER.debug("Current channel: {}", dmxChannel);

                                    if (selectedDmxChannelId == dmxChannel.getChannelId()) {

                                        // prepare the remove point action
                                        pointRemoveAction =
                                            new PointRemoveAction(
                                                Resources.getString(DmxChartPanel.class, "remove-point"), item,
                                                dmxChannel, this);

                                        if (CollectionUtils.isNotEmpty(dmxChannel.getAssignedPorts())) {
                                            // add the assigned ports
                                            for (Port<?> port : dmxChannel.getAssignedPorts()) {
                                                subMenuSetPort.add(new PortAction(port, dmxChannel, this, item));
                                            }
                                            subMenuSetPort.setEnabled(true);
                                        }
                                        else {
                                            subMenuSetPort.setEnabled(false);
                                        }

                                        selectedDmxChannel = dmxChannel;
                                        break;
                                    }
                                }

                                if (subMenuAddMacro != null) {
                                    subMenuAddMacro.removeAll();

                                    // add the macros
                                    for (Macro macro : dmxModel.getDmxSceneryModel().getMacros()) {
                                        if (macro == null) {
                                            continue;
                                        }

                                        subMenuAddMacro.add(new MacroAction(macro, selectedDmxChannel, this, item));
                                    }
                                }

                                if (subMenuSetAction != null) {
                                    subMenuSetAction.removeAll();
                                    if (item.getPort() instanceof DmxLightPort) {
                                        subMenuSetAction.add(
                                            new LightPortAction(LightPortStatus.UP, selectedDmxChannel, this, item));
                                        subMenuSetAction.add(
                                            new LightPortAction(LightPortStatus.DOWN, selectedDmxChannel, this, item));
                                        subMenuSetAction.add(
                                            new LightPortAction(LightPortStatus.ON, selectedDmxChannel, this, item));
                                        subMenuSetAction.add(
                                            new LightPortAction(LightPortStatus.OFF, selectedDmxChannel, this, item));
                                        subMenuSetAction.setVisible(true);
                                    }
                                    else if (item.getMacro() != null) {
                                        subMenuSetAction.add(
                                            new MacroStatusAction(MacroStatus.START, selectedDmxChannel, this, item));
                                        subMenuSetAction.add(
                                            new MacroStatusAction(MacroStatus.STOP, selectedDmxChannel, this, item));
                                        subMenuSetAction.setVisible(true);
                                    }

                                }
                            }
                        }
                        catch (Exception ex) {
                            LOGGER.warn("Get dataItem failed.", ex);
                        }
                    }
                }
                else {
                    if (subMenuSetPort != null) {
                        subMenuSetPort.setVisible(isHighlighted);
                        subMenuSetPort.removeAll();
                    }
                    if (subMenuAddMacro != null) {
                        subMenuAddMacro.setVisible(isHighlighted);
                        subMenuAddMacro.removeAll();
                    }
                    if (subMenuSetAction != null) {
                        subMenuSetAction.setVisible(isHighlighted);
                        subMenuSetAction.removeAll();
                    }
                    if (setPortSeparator != null) {
                        setPortSeparator.setVisible(isHighlighted);
                    }
                }
            }

            if (subMenuAddPoint == null) {

                // add submenu for adding points
                JPopupMenu defaultMenu = getPopupMenu();
                subMenuAddPoint = new JMenu(Resources.getString(DmxChartPanel.class, "add-point"));
                defaultMenu.insert(subMenuAddPoint, 0);

                defaultMenu.insert(new JPopupMenu.Separator(), 1);
            }

            if (pointRemoveAction != null) {
                LOGGER.info("Add point remove action.");
                JPopupMenu defaultMenu = getPopupMenu();
                // remove existing remove point menu item before adding the new one
                if (removePointMenuItem != null) {
                    defaultMenu.remove(removePointMenuItem);
                    removePointMenuItem = null;
                }
                removePointMenuItem = new JMenuItem(pointRemoveAction);
                removePointMenuItem.setText(pointRemoveAction.getText());
                int insertIndexAfter = defaultMenu.getComponentIndex(subMenuAddMacro);

                if (insertIndexAfter > -1) {
                    LOGGER.info("Insert remvocePointMenuItem after index: {}", insertIndexAfter);
                }
                else {
                    insertIndexAfter = 0;
                }
                defaultMenu.insert(removePointMenuItem, insertIndexAfter + 1);
            }
            else {
                JPopupMenu defaultMenu = getPopupMenu();
                // add remove point item
                if (removePointMenuItem != null) {
                    defaultMenu.remove(removePointMenuItem);
                    removePointMenuItem = null;
                }
            }

            if (subMenuAddPoint != null) {
                subMenuAddPoint.removeAll();

                // add the channels
                for (DmxChannel dmxChannel : dmxScenery.getUsedChannels()) {
                    subMenuAddPoint.add(new DmxChannelAction(dmxChannel, this));
                }
            }

        }
    }

    private boolean updateLocation(DmxSeries dmxSeries, int chartX, int chartY, final DmxDataItem item) {

        // TODO current issue here is that if we drag the point we think it's another point and skip update the position

        // we allow to update both coordinates
        int index = dmxSeries.indexOf(Double.valueOf(chartX), item.getUniqueId());
        if (index > -1) {
            DmxDataItem storedDataItem = (DmxDataItem) dmxSeries.getDataItem(index);
            LOGGER.info("Current storedDataItem: {}, dragged item: {}", storedDataItem, item);
            if (storedDataItem.equals(item)) {
                LOGGER.debug("We found the dragged item. Let's update the position.");
            }
            else {
                // a point is already stored at this position -> skip update to this position
                LOGGER.warn("A point is already stored at this position -> skip update to this position");
                return false;
            }
        }

        // keep the old values because we need them to search the corresponding point in the sceneryPoints later
        final long oldUniqueId = item.getUniqueId();

        dmxSeries.moveItem(item, chartX, chartY);

        String seriesKey = (String) dmxSeries.getKey();

        // TODO we must make sure we get the correct point with the correct uniqueId

        // find the existing point
        final int dmxChannelNumber = Integer.valueOf(seriesKey);
        DmxSceneryPoint dmxSceneryPoint =
            CollectionUtils.find(dmxModel.getDmxScenery().getSceneryPoints(), new Predicate<DmxSceneryPoint>() {

                @Override
                public boolean evaluate(DmxSceneryPoint currentPoint) {
                    if (currentPoint.getDmxChannelNumber() == dmxChannelNumber
                        && currentPoint.getUniqueId() == oldUniqueId) {
                        LOGGER.info("Found matching dmxSceneryPoint: {}", currentPoint);
                        return true;
                    }
                    return false;
                }

            });
        if (dmxSceneryPoint != null) {
            // update point in scenery
            LOGGER.info("Update existing scenery point: {}", dmxSceneryPoint);
            dmxSceneryPoint.setTimeOffset(chartX);
            dmxSceneryPoint.setBrightness(chartY);
        }

        return true;
    }

    /**
     * Set the action on the selected lightport.
     * 
     * @param seriesKey
     *            the series key
     * @param dmxDataItem
     *            the data item
     * @param action
     *            the action
     */
    public void setLightPortAction(String seriesKey, DmxDataItem dmxDataItem, LightPortStatus action) {
        try {
            // get the series
            DmxSeries dmxSeries = getDmxSeries(seriesKey);

            // get the point and remove from series
            int index = dmxSeries.indexOf(Double.valueOf(dmxDataItem.getTimeOffset()), dmxDataItem.getUniqueId());
            if (index > -1) {

                // an existing data item is already stored
                final DmxDataItem prevDmxDataItem = (DmxDataItem) dmxSeries.remove(index);

                Port<?> port = prevDmxDataItem.getPort();
                if (port instanceof DmxLightPort) {
                    // DmxLightPort dmxLightPort = (DmxLightPort) port;
                    // dmxLightPort.setStatus(action);
                    LOGGER.info("Set the new action: {} on the lightport: {}", action, port);
                    prevDmxDataItem.setAction(action);
                }
                else {
                    LOGGER.warn("The current port is not a lightport.");
                }
                // add again
                dmxSeries.add(prevDmxDataItem);

                // find the existing point
                final int dmxChannelNumber = Integer.valueOf(seriesKey);
                DmxSceneryPoint dmxSceneryPoint =
                    CollectionUtils.find(dmxModel.getDmxScenery().getSceneryPoints(), new Predicate<DmxSceneryPoint>() {

                        @Override
                        public boolean evaluate(DmxSceneryPoint currentPoint) {
                            if (currentPoint.getDmxChannelNumber() == dmxChannelNumber
                                && currentPoint.getUniqueId() == prevDmxDataItem.getUniqueId()) {
                                LOGGER.info("Found dmxSceneryPoint: {}", currentPoint);
                                return true;
                            }
                            return false;
                        }

                    });
                if (dmxSceneryPoint != null) {
                    // update point in scenery
                    LOGGER.info("Update existing scenery point: {}", dmxSceneryPoint);
                    dmxSceneryPoint.setAction(action);
                }

            }

            // refresh the annotations
            refreshSceneryPointAnnotations(getChart().getXYPlot(), dmxSeries);
        }
        catch (Exception ex) {
            LOGGER.warn("Set action on lightport failed.", ex);
        }
    }

    /**
     * Set the action on the selected macro.
     * 
     * @param seriesKey
     *            the series key
     * @param dmxDataItem
     *            the data item
     * @param action
     *            the action
     */
    public void setMacroStatusAction(String seriesKey, DmxDataItem dmxDataItem, MacroStatus action) {
        try {
            // get the series
            DmxSeries dmxSeries = getDmxSeries(seriesKey);

            // get the point and remove from series
            int index = dmxSeries.indexOf(Double.valueOf(dmxDataItem.getTimeOffset()), dmxDataItem.getUniqueId());
            if (index > -1) {

                // an existing data item is already stored
                final DmxDataItem prevDmxDataItem = (DmxDataItem) dmxSeries.remove(index);

                MacroFunction macro = prevDmxDataItem.getMacro();
                if (macro != null) {
                    macro.setAction(action);
                }
                else {
                    LOGGER.warn("The current port is not a lightport.");
                }

                dmxSeries.add(prevDmxDataItem);

                // find the existing point
                final int dmxChannelNumber = Integer.valueOf(seriesKey);
                DmxSceneryPoint dmxSceneryPoint =
                    CollectionUtils.find(dmxModel.getDmxScenery().getSceneryPoints(), new Predicate<DmxSceneryPoint>() {

                        @Override
                        public boolean evaluate(DmxSceneryPoint currentPoint) {
                            if (currentPoint.getDmxChannelNumber() == dmxChannelNumber
                                && currentPoint.getUniqueId() == prevDmxDataItem.getUniqueId()) {
                                LOGGER.info("Found dmxSceneryPoint: {}", currentPoint);
                                return true;
                            }
                            return false;
                        }

                    });
                if (dmxSceneryPoint != null) {
                    // update point in scenery
                    LOGGER.info("Update existing scenery point: {}", dmxSceneryPoint);
                    dmxSceneryPoint.setMacro(macro);
                }
            }

            // refresh the annotations
            refreshSceneryPointAnnotations(getChart().getXYPlot(), dmxSeries);
        }
        catch (Exception ex) {
            LOGGER.warn("Set action on lightport failed.", ex);
        }
    }

    /**
     * Remove a point from the series and the scenery.
     * 
     * @param seriesKey
     *            the series key
     * @param dmxDataItem
     *            the data item
     */
    public void removeDataItem(String seriesKey, DmxDataItem dmxDataItem) {
        try {
            // get the series
            DmxSeries dmxSeries = getDmxSeries(seriesKey);

            // get the point and remove from series
            int index = dmxSeries.indexOf(Double.valueOf(dmxDataItem.getTimeOffset()), dmxDataItem.getUniqueId());
            if (index > -1) {

                // an existing data item is already stored
                final DmxDataItem prevDmxDataItem = (DmxDataItem) dmxSeries.getDataItem(index);

                // remove point from series
                dmxSeries.remove(index);

                // find the existing point
                final int dmxChannelNumber = Integer.valueOf(seriesKey);
                DmxSceneryPoint dmxSceneryPoint =
                    CollectionUtils.find(dmxModel.getDmxScenery().getSceneryPoints(), new Predicate<DmxSceneryPoint>() {

                        @Override
                        public boolean evaluate(DmxSceneryPoint currentPoint) {
                            if (currentPoint.getDmxChannelNumber() == dmxChannelNumber
                                && currentPoint.getUniqueId() == prevDmxDataItem.getUniqueId()) {
                                LOGGER.info("Found dmxSceneryPoint: {}", currentPoint);
                                return true;
                            }
                            return false;
                        }

                    });
                // remove point from scenery
                LOGGER.info("Remove existing scenery point: {}", dmxSceneryPoint);

                // boolean removed = dmxModel.getDmxScenery().getSceneryPoints().remove(dmxSceneryPoint);
                //
                // if (!removed) {
                // LOGGER.warn("The prevSceneryPoint was not removed: {}", dmxSceneryPoint);
                // }
                final long uniqueId = dmxSceneryPoint.getUniqueId();

                DmxSceneryPoint existingSceneryPoint =
                    CollectionUtils.find(dmxModel.getDmxScenery().getSceneryPoints(), new Predicate<DmxSceneryPoint>() {

                        @Override
                        public boolean evaluate(DmxSceneryPoint sceneryPoint) {
                            LOGGER.info("Search matching sceneryPoint: {}", sceneryPoint);

                            if (sceneryPoint.getUniqueId() == uniqueId) {
                                return true;
                            }
                            return false;
                        }
                    });

                if (existingSceneryPoint != null) {
                    boolean removed = dmxModel.getDmxScenery().getSceneryPoints().remove(existingSceneryPoint);
                    if (!removed) {
                        LOGGER.warn("The dmxSceneryPoint was not removed: {}", dmxSceneryPoint);
                    }
                }
                else {
                    LOGGER.warn("No scenery point to remove available.");
                }
            }
            else {
                LOGGER.warn("No matching point to remove found.");
            }

            // refresh the annotations
            refreshSceneryPointAnnotations(getChart().getXYPlot(), dmxSeries);
        }
        catch (Exception ex) {
            LOGGER.warn("Remove data item failed.", ex);
        }
    }

    /**
     * Create a data item with the assigned port.
     * 
     * @param seriesKey
     *            the series key
     * @param timeOffset
     *            the X position
     * @param brightness
     *            the Y position
     * @param port
     *            the port
     * @param dmxChannel
     *            the dmxChannel
     */
    public void createDataItem(
        String seriesKey, int timeOffset, int brightness, Port<?> port, DmxChannel dmxChannel,
        DmxDataItem originalDataItem) {

        try {
            //
            DmxSeries dmxSeries = getDmxSeries(seriesKey);

            DmxSceneryPoint prevSceneryPoint = null;

            // get an existing point at the timeOffset to remove later
            int index = -1;
            if (originalDataItem != null) {
                index = dmxSeries.indexOf(Double.valueOf(timeOffset), originalDataItem.getUniqueId());
            }
            else {
                index = dmxSeries.indexOf(Double.valueOf(timeOffset));
            }

            if (index > -1) {
                // an existing data item is already stored
                DmxDataItem prevDmxDataItem = (DmxDataItem) dmxSeries.getDataItem(index);
                prevSceneryPoint =
                    new DmxSceneryPoint()
                        .withBrightness(brightness).withTimeOffset(timeOffset)
                        .withUniqueId(prevDmxDataItem.getUniqueId()).withMacro(prevDmxDataItem.getMacro())
                        .withDmxChannelNumber(dmxChannel.getChannelId());

                if (prevDmxDataItem.getPort() != null) {
                    prevSceneryPoint.withPort(prevDmxDataItem.getPort());
                }

                if (prevDmxDataItem.getAction() != null) {
                    prevSceneryPoint.withAction(prevDmxDataItem.getAction());
                }

                prevDmxDataItem.setMacro(null);

                if (prevDmxDataItem.getPort() != null && !prevDmxDataItem.getPort().equals(port)) {
                    // reset the action if the type of port has been changed
                    prevDmxDataItem.setAction(null);
                }

                // Set the new port as last action
                prevDmxDataItem.setPort(port);

                dmxSeries.moveItem(prevDmxDataItem, timeOffset, brightness);
            }
            else {
                // create new item
                originalDataItem = dmxSeries.add(timeOffset, brightness, port);
            }

            // add the new point
            DmxSceneryPoint dmxSceneryPoint =
                new DmxSceneryPoint()
                    .withBrightness(brightness).withTimeOffset(timeOffset).withPort(port)
                    .withDmxChannelNumber(dmxChannel.getChannelId());

            if (originalDataItem != null) {
                if (originalDataItem.getPort() != null && !originalDataItem.getPort().equals(port)) {
                    // reset the action if the type of port has been changed
                    originalDataItem.setAction(null);
                }

                dmxSceneryPoint =
                    dmxSceneryPoint
                        .withAction(originalDataItem.getAction()).withUniqueId(originalDataItem.getUniqueId());
            }

            if (prevSceneryPoint != null) {
                LOGGER.info("Remove existing scenery point: {}", prevSceneryPoint);

                final long uniqueId = prevSceneryPoint.getUniqueId();

                DmxSceneryPoint existingSceneryPoint =
                    CollectionUtils.find(dmxModel.getDmxScenery().getSceneryPoints(), new Predicate<DmxSceneryPoint>() {

                        @Override
                        public boolean evaluate(DmxSceneryPoint sceneryPoint) {
                            LOGGER.info("Search matching sceneryPoint: {}", sceneryPoint);

                            if (sceneryPoint.getUniqueId() == uniqueId) {
                                return true;
                            }
                            return false;
                        }
                    });

                if (existingSceneryPoint != null) {
                    boolean removed = dmxModel.getDmxScenery().getSceneryPoints().remove(existingSceneryPoint);
                    if (!removed) {
                        LOGGER.warn("The prevSceneryPoint was not removed: {}", prevSceneryPoint);
                    }
                }
                else {
                    LOGGER.warn("No scenery point to remove available.");
                }
            }
            dmxModel.getDmxScenery().getSceneryPoints().add(dmxSceneryPoint);

            refreshSceneryPointAnnotations(getChart().getXYPlot(), dmxSeries);
        }
        catch (UnknownKeyException ex) {
            LOGGER.debug("Add new point failed because no series in dataset for key: {}", seriesKey);
            try {
                DmxSeries dmxSeries = new DmxSeries(seriesKey, true);
                originalDataItem = dmxSeries.add(timeOffset, brightness, port);
                getDataSet().addSeries(dmxSeries);

                // add the new point
                DmxSceneryPoint dmxSceneryPoint =
                    new DmxSceneryPoint()
                        .withBrightness(brightness).withTimeOffset(timeOffset).withPort(port)
                        .withDmxChannelNumber(dmxChannel.getChannelId());

                if (originalDataItem != null) {
                    dmxSceneryPoint =
                        dmxSceneryPoint
                            .withAction(originalDataItem.getAction()).withUniqueId(originalDataItem.getUniqueId());

                    if (originalDataItem.getMacro() != null) {
                        LOGGER.info("Restore the macro function: {}", originalDataItem.getMacro());
                        dmxSceneryPoint = dmxSceneryPoint.withMacro(originalDataItem.getMacro());
                    }
                }
                dmxModel.getDmxScenery().getSceneryPoints().add(dmxSceneryPoint);

                XYItemRenderer renderer = getChart().getXYPlot().getRenderer();
                if (renderer != null && dmxChannel.getLineColor() != null) {
                    renderer.setSeriesPaint(getDataSet().indexOf(dmxSeries), dmxChannel.getLineColor());
                }

                refreshSceneryPointAnnotations(getChart().getXYPlot(), dmxSeries);
            }
            catch (IllegalArgumentException ex1) {
                LOGGER.warn("Add new point failed.", ex1);
                // show an error to the user
                JOptionPane.showMessageDialog(this, Resources.getString(DmxChartPanel.class, "add-point-failed"),
                    Resources.getString(DmxChartPanel.class, "add-point"), JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    /**
     * Create a data item with the assigned port.
     * 
     * @param seriesKey
     *            the series key
     * @param timeOffset
     *            the X position
     * @param brightness
     *            the Y position
     * @param macro
     *            the macro
     * @param dmxChannel
     *            the dmxChannel
     */
    public void createDataItem(
        String seriesKey, int timeOffset, int brightness, MacroFunction macro, DmxChannel dmxChannel,
        DmxDataItem originalDataItem) {

        try {
            // get the DMX series from the series key
            DmxSeries dmxSeries = getDmxSeries(seriesKey);

            DmxSceneryPoint prevSceneryPoint = null;

            // get an existing point at the timeOffset to remove later
            int index = -1;
            if (originalDataItem != null) {
                index = dmxSeries.indexOf(Double.valueOf(timeOffset), originalDataItem.getUniqueId());
            }
            else {
                index = dmxSeries.indexOf(Double.valueOf(timeOffset));
            }
            if (index > -1) {
                // an existing data item is already stored
                DmxDataItem prevDmxDataItem = (DmxDataItem) dmxSeries.getDataItem(index);
                prevSceneryPoint =
                    new DmxSceneryPoint()
                        .withBrightness(brightness).withTimeOffset(timeOffset)
                        .withUniqueId(prevDmxDataItem.getUniqueId()).withPort(prevDmxDataItem.getPort())
                        .withAction(prevDmxDataItem.getAction()).withDmxChannelNumber(dmxChannel.getChannelId());

                if (prevDmxDataItem.getMacro() != null) {
                    prevSceneryPoint.withMacro(prevDmxDataItem.getMacro());
                }

                prevDmxDataItem.setPort(null);
                prevDmxDataItem.setAction(null);

                // change user object from port to macro as last action
                prevDmxDataItem.setMacro(macro);

                dmxSeries.moveItem(prevDmxDataItem, timeOffset, brightness);
            }
            else {
                // create new item
                dmxSeries.add(timeOffset, brightness, macro);
            }

            // add the new point
            DmxSceneryPoint dmxSceneryPoint =
                new DmxSceneryPoint()
                    .withBrightness(brightness).withTimeOffset(timeOffset).withMacro(macro)
                    .withDmxChannelNumber(dmxChannel.getChannelId());

            if (prevSceneryPoint != null) {
                LOGGER.info("Remove existing scenery point: {}", prevSceneryPoint);
                // boolean removed = dmxModel.getDmxScenery().getSceneryPoints().remove(prevSceneryPoint);
                //
                // if (!removed) {
                // LOGGER.warn("The prevSceneryPoint was not removed: {}", prevSceneryPoint);
                // }
                final long uniqueId = prevSceneryPoint.getUniqueId();

                DmxSceneryPoint existingSceneryPoint =
                    CollectionUtils.find(dmxModel.getDmxScenery().getSceneryPoints(), new Predicate<DmxSceneryPoint>() {

                        @Override
                        public boolean evaluate(DmxSceneryPoint sceneryPoint) {
                            LOGGER.info("Search matching sceneryPoint: {}", sceneryPoint);

                            if (sceneryPoint.getUniqueId() == uniqueId) {
                                return true;
                            }
                            return false;
                        }
                    });

                if (existingSceneryPoint != null) {
                    boolean removed = dmxModel.getDmxScenery().getSceneryPoints().remove(existingSceneryPoint);
                    if (!removed) {
                        LOGGER.warn("The prevSceneryPoint was not removed: {}", prevSceneryPoint);
                    }
                }
                else {
                    LOGGER.warn("No scenery point to remove available.");
                }
            }
            dmxModel.getDmxScenery().getSceneryPoints().add(dmxSceneryPoint);

            refreshSceneryPointAnnotations(getChart().getXYPlot(), dmxSeries);
        }
        catch (UnknownKeyException ex) {
            LOGGER.debug("Add new point failed because no series in dataset for key: {}", seriesKey);
            try {
                DmxSeries dmxSeries = new DmxSeries(seriesKey, true);
                dmxSeries.add(timeOffset, brightness, macro);
                getDataSet().addSeries(dmxSeries);

                XYItemRenderer renderer = getChart().getXYPlot().getRenderer();
                if (renderer != null && dmxChannel.getLineColor() != null) {
                    renderer.setSeriesPaint(getDataSet().indexOf(dmxSeries), dmxChannel.getLineColor());
                }

                refreshSceneryPointAnnotations(getChart().getXYPlot(), dmxSeries);
            }
            catch (IllegalArgumentException ex1) {
                LOGGER.warn("Add new point failed.", ex1);

                // show an error to the user
                JOptionPane.showMessageDialog(this, Resources.getString(DmxChartPanel.class, "add-point-failed"),
                    Resources.getString(DmxChartPanel.class, "add-point"), JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent event) {

        // super.mousePressed(event);

        // MB1 and control is used for panning
        if (event.getButton() == MouseEvent.BUTTON1 && !event.isControlDown() && getChart() != null) {
            Insets insets = getInsets();
            int x = (int) ((event.getX() - insets.left) / getScaleX());
            int y = (int) ((event.getY() - insets.top) / getScaleY());

            ChartEntity entity = null;
            if (getChartRenderingInfo() != null) {
                EntityCollection entities = getChartRenderingInfo().getEntityCollection();
                if (entities != null) {
                    entity = entities.getEntity(x, y);
                }
            }

            if (entity instanceof XYItemEntity) {
                LOGGER.trace("Set the entity to drag line: {}", entity);
                entityToDragLine = entity;
                // clone the entity to have the original values
                try {
                    XYItemEntity xyie = (XYItemEntity) entityToDragLine;
                    DmxDataItem item =
                        (DmxDataItem) ((XYSeriesCollection) getChart().getXYPlot().getDataset())
                            .getSeries(xyie.getSeriesIndex()).getDataItem(xyie.getItem());
                    orgDmxDataItemToDragLine = (DmxDataItem) item.clone();
                }
                catch (Exception ex) {
                    LOGGER.warn("Clone dragged DmxDataItem failed.", ex);
                }
            }
            else {
                super.mousePressed(event);
            }
        }
        else {
            super.mousePressed(event);
        }
    }

    @Override
    public void mouseDragged(MouseEvent event) {

        if (entityToDragLine != null) {
            Insets insets = getInsets();
            int x = event.getX() - insets.left;
            int y = event.getY() - insets.top;

            if (getChart() == null) {
                return;
            }

            LOGGER.trace("Move the entity to drag: {}", entityToDragLine);
            if (entityToDragLine instanceof XYItemEntity) {
                LOGGER.info("released, entityToDrag: {}", entityToDragLine);
                XYItemEntity xyie = (XYItemEntity) entityToDragLine;

                int seriesIndex = xyie.getSeriesIndex();
                XYDataset dataset = xyie.getDataset();

                final XYPlot plot = getChart().getXYPlot();
                final ValueAxis rangeAxis = plot.getRangeAxis();
                final ValueAxis domainAxis = plot.getDomainAxis();
                final Rectangle2D plotRectangle = getScreenDataArea();

                int chartY = (int) rangeAxis.java2DToValue(y, plotRectangle, plot.getRangeAxisEdge());
                int chartX = (int) domainAxis.java2DToValue(x, plotRectangle, plot.getDomainAxisEdge());

                String seriesKey = (String) dataset.getSeriesKey(seriesIndex);

                LOGGER.trace("seriesKey: {}, seriesIndex: {}, y: {}, x: {}", seriesKey, seriesIndex, chartY, chartX);

                if (chartX < 0) {
                    LOGGER.warn("No X values < 0 allowed.");
                    chartX = 0;
                }
                if (chartY < 0) {
                    LOGGER.warn("No Y values < 0 allowed.");
                    chartY = 0;
                }

                DmxSeries dmxSeries = (DmxSeries) ((XYSeriesCollection) plot.getDataset()).getSeries(seriesKey);

                DmxDataItem item = orgDmxDataItemToDragLine;

                if (event.isShiftDown()) {
                    LOGGER.trace("SHIFT is pressed.");
                    // we only allow to update the Y coordinate
                    dmxSeries.addOrUpdate(item.getX(), chartY, item);
                }
                else {
                    // TODO this causes problems if the point is dragged over an exisiting point ...

                    // we allow to update both coordinates
                    updateLocation(dmxSeries, chartX, chartY, item);
                }
            }
            LOGGER.trace("+++ dragged, x: {}, y: {}, entity: {}.", x, y, entityToDragLine);
        }
        else {
            super.mouseDragged(event);
        }
    }

    @Override
    public void mouseReleased(MouseEvent event) {

        if (entityToDragLine != null && event.getButton() == MouseEvent.BUTTON1 && !event.isControlDown()) {

            if (getChart() == null) {
                return;
            }

            Insets insets = getInsets();
            int x = event.getX() - insets.left;
            int y = event.getY() - insets.top;

            final XYPlot plot = getChart().getXYPlot();

            DmxSeries dmxSeries = null;
            LOGGER.trace("Move the entity to drag: {}", entityToDragLine);
            if (entityToDragLine instanceof XYItemEntity) {
                LOGGER.trace("released, entityToDragLine: {}", entityToDragLine);
                XYItemEntity xyie = (XYItemEntity) entityToDragLine;
                //
                int seriesIndex = xyie.getSeriesIndex();
                XYDataset dataset = xyie.getDataset();
                //
                // final ValueAxis rangeAxis = plot.getRangeAxis();
                // final ValueAxis domainAxis = plot.getDomainAxis();
                // final Rectangle2D plotRectangle = getScreenDataArea();
                //
                // int chartY = (int) rangeAxis.java2DToValue(y, plotRectangle, plot.getRangeAxisEdge());
                // int chartX = (int) domainAxis.java2DToValue(x, plotRectangle, plot.getDomainAxisEdge());
                //
                String seriesKey = (String) dataset.getSeriesKey(seriesIndex);

                dmxSeries = getDmxSeries(seriesKey);
                //
                // LOGGER.info("seriesKey: {}, seriesIndex: {}, y: {}, x: {}", seriesKey, seriesIndex, chartY, chartX);
                //
                // if (chartX < 0) {
                // LOGGER.warn("No X values < 0 allowed.");
                // chartX = 0;
                // }
                // if (chartY < 0) {
                // LOGGER.warn("No Y values < 0 allowed.");
                // chartY = 0;
                // }
                //
                // // TODO getDataItem(index) ... get the correct index. If we change the order of the items by moving
                // this
                // // is no longer valid.
                //
                // DmxDataItem item =
                // (DmxDataItem) ((XYSeriesCollection) plot.getDataset()).getSeries(seriesKey).getDataItem(
                // xyie.getItem());
                // LOGGER.info("Found data item: {}", item);
                //
                // dmxSeries = (DmxSeries) ((XYSeriesCollection) plot.getDataset()).getSeries(seriesKey);
                //
                // if (event.isShiftDown()) {
                // LOGGER.trace("SHIFT is pressed.");
                // // the X value stays
                // chartX = (int) item.getXValue();
                // // we only allow to update the Y coordinate
                // ((DmxSeries) ((XYSeriesCollection) plot.getDataset()).getSeries(seriesKey)).addOrUpdate(chartX,
                // chartY, item);
                // }
                // else {
                // // TODO this causes problems if the point is dragged over an exisiting point ...
                // // we allow to update both coordinates
                // LOGGER.info("mouseReleased, update location for item: {}", item);
                // updateLocation(dmxSeries, chartX, chartY, item);
                // }
            }
            LOGGER.info("Reset the entity to drag line.");
            entityToDragLine = null;
            orgDmxDataItemToDragLine = null;

            if (dmxSeries != null) {
                refreshSceneryPointAnnotations(plot, dmxSeries);
            }
        }
        else {
            super.mouseReleased(event);
        }
    }

    private void refreshSceneryPointAnnotations(XYPlot plot, DmxSeries dmxSeries) {
        LOGGER.info("refreshSceneryPointAnnotations");

        XYItemRenderer renderer = plot.getRenderer();
        if (renderer instanceof AbstractXYItemRenderer) {
            AbstractXYItemRenderer abstractXYItemRenderer = ((AbstractXYItemRenderer) renderer);

            for (Object annotation : abstractXYItemRenderer.getAnnotations()) {
                if (annotation instanceof SceneryPointArrowAnnotation) {
                    SceneryPointArrowAnnotation sceneryPointArrowAnnotation = (SceneryPointArrowAnnotation) annotation;
                    if (dmxSeries.getKey().equals(sceneryPointArrowAnnotation.getSeriesKey())) {
                        abstractXYItemRenderer.removeAnnotation(sceneryPointArrowAnnotation);
                    }
                }
            }
        }

        String seriesKey = (String) dmxSeries.getKey();

        for (Object item : dmxSeries.getItems()) {
            DmxDataItem dmxDataItem = (DmxDataItem) item;

            Integer dimmSlopeUp = null;
            Integer dimmSlopeDown = null;

            Port<?> currentPort = dmxDataItem.getPort();
            if (currentPort instanceof BacklightPort) {
                BacklightPort backlightPort = (BacklightPort) currentPort;
                dimmSlopeUp = backlightPort.getDimSlopeUp();
                dimmSlopeDown = backlightPort.getDimSlopeDown();
            }
            else if (currentPort instanceof DmxLightPort) {
                DmxLightPort dmxLightPort = (DmxLightPort) currentPort;
                dimmSlopeUp = dmxLightPort.getLightPort().getDimMax();
                dimmSlopeDown = dmxLightPort.getLightPort().getDimMin();
            }

            LOGGER.info("Current dimmSlopeUp: {}, dimmSlopeDown: {}", dimmSlopeUp, dimmSlopeDown);
            if (dimmSlopeUp != null && dimmSlopeDown != null) {

                try {
                    int volumeDeltaUp = 100 - dmxDataItem.getBrightness();
                    int durationUp = PortUtils.calculateDimmDuration((volumeDeltaUp * 255) / 100, dimmSlopeUp);

                    int volumeDeltaDown = dmxDataItem.getBrightness();
                    int durationDown = PortUtils.calculateDimmDuration((volumeDeltaDown * 255) / 100, dimmSlopeDown);

                    LOGGER.info(
                        "Current port: {}, volumeDeltaUp: {}, durationUp: {}, volumeDeltaDown: {}, durationDown: {}",
                        currentPort, volumeDeltaUp, durationUp, volumeDeltaDown, durationDown);

                    addSceneryPointAnnotation(renderer, seriesKey, dmxDataItem.getTimeOffset(),
                        dmxDataItem.getBrightness(), durationUp, volumeDeltaUp, Color.red);

                    addSceneryPointAnnotation(renderer, seriesKey, dmxDataItem.getTimeOffset(),
                        dmxDataItem.getBrightness(), durationDown, -volumeDeltaDown, Color.green.darker());
                }
                catch (Exception ex) {
                    LOGGER.warn("Prepare scenery point annotations failed.", ex);
                }
            }
        }
    }

    private void addSceneryPointAnnotation(
        XYItemRenderer renderer, Comparable<String> seriesKey, int chartX, int chartY, int duration, int volumeDelta,
        Color color) {
        SceneryPointArrowAnnotation annotation =
            new SceneryPointArrowAnnotation(seriesKey, chartX, chartY, duration, volumeDelta);
        annotation.setBaseRadius(0);
        annotation.setArrowPaint(color);
        renderer.addAnnotation(annotation);
    }

    public void loadSceneryPoints() {
        LOGGER.info("Load scenery points.");

        final XYPlot plot = getChart().getXYPlot();

        // clear all data from the series collection
        XYSeriesCollection dataSet = ((XYSeriesCollection) plot.getDataset());
        dataSet.removeAllSeries();

        // add the points from the dmx
        if (dmxModel != null && dmxModel.getDmxScenery() != null) {

            DmxScenery dmxScenery = dmxModel.getDmxScenery();

            if (!CollectionUtils.isNotEmpty(dmxScenery.getSceneryPoints())) {
                LOGGER.info("No stored scenery points available.");
                return;
            }

            if (!CollectionUtils.isNotEmpty(dmxScenery.getUsedChannels())) {
                // no channels defined
                return;
            }

            XYItemRenderer renderer = plot.getRenderer();

            // sort the points by time
            Collections.sort(dmxScenery.getSceneryPoints(), new Comparator<DmxSceneryPoint>() {
                @Override
                public int compare(DmxSceneryPoint p1, DmxSceneryPoint p2) {
                    return p1.getTimeOffset() - p2.getTimeOffset();
                }
            });

            // load the points
            for (DmxSceneryPoint dmxSceneryPoint : dmxScenery.getSceneryPoints()) {
                LOGGER.info("Current dmxSceneryPoint: {}", dmxSceneryPoint);

                int dmxChannelNumber = dmxSceneryPoint.getDmxChannelNumber();
                String seriesKey = Integer.toString(dmxChannelNumber);

                double chartX = dmxSceneryPoint.getTimeOffset();
                double chartY = dmxSceneryPoint.getBrightness();

                LOGGER.info("Prepared chartX: {}, chartY: {}, seriesKey: {}", chartX, chartY, seriesKey);

                // add the new point to the series
                try {
                    DmxSeries dmxSeries = getDmxSeries(seriesKey);
                    // TODO we must add the DmxDataItem
                    DmxDataItem item = dmxSeries.getNewDmxDataItem(chartX, chartY);

                    // the data item has a new uniqueId -> update the uniqueId in the scenery point that is stored in
                    // the model
                    dmxSceneryPoint.setUniqueId(item.getUniqueId());

                    if (dmxSceneryPoint.getPort() != null) {
                        item.setPort(dmxSceneryPoint.getPort());
                        item.setAction(dmxSceneryPoint.getAction());
                    }
                    else {
                        item.setMacro(dmxSceneryPoint.getMacro());
                    }
                    dmxSeries.add(item, false /* no notify */);
                    // ((DmxSeries) dataSet.getSeries(seriesKey)).add(chartX, chartY, dmxSceneryPoint.getPort());
                }
                catch (UnknownKeyException ex) {
                    LOGGER.debug("Add new point failed because no series in dataset for key: {}", seriesKey);

                    LOGGER.info("Create new DmxSeries for seriesKey: {}", seriesKey);
                    DmxSeries dmxSeries = new DmxSeries(seriesKey, true);
                    dataSet.addSeries(dmxSeries);

                    DmxDataItem item = dmxSeries.getNewDmxDataItem(chartX, chartY);

                    // the data item has a new uniqueId -> update the uniqueId in the scenery point that is stored in
                    // the model
                    dmxSceneryPoint.setUniqueId(item.getUniqueId());

                    if (dmxSceneryPoint.getPort() != null) {
                        item.setPort(dmxSceneryPoint.getPort());
                        item.setAction(dmxSceneryPoint.getAction());
                    }
                    else {
                        item.setMacro(dmxSceneryPoint.getMacro());
                    }
                    dmxSeries.add(item, false /* no notify */);

                    // dmxSeries.add(chartX, chartY, dmxSceneryPoint.getPort());

                    int index = dmxScenery.getUsedChannels().indexOf(new DmxChannel(dmxChannelNumber));
                    DmxChannel dmxChannel = dmxScenery.getUsedChannels().get(index);
                    if (renderer != null && dmxChannel.getLineColor() != null) {
                        renderer.setSeriesPaint(dataSet.indexOf(dmxSeries), dmxChannel.getLineColor());
                    }
                }
            }

            // add annotations to all series
            for (Object series : dataSet.getSeries()) {
                DmxSeries dmxSeries = (DmxSeries) series;

                if (dmxSeries != null) {
                    dmxSeries.fireSeriesChanged();

                    refreshSceneryPointAnnotations(plot, dmxSeries);
                }
            }
        }
    }

    public void storeSceneryPoints() {
        LOGGER.info("Store scenery points.");

        final XYPlot plot = getChart().getXYPlot();

        // clear all data from the series collection
        XYSeriesCollection dataSet = ((XYSeriesCollection) plot.getDataset());

        List<DmxSceneryPoint> dmxSceneryPoints = new LinkedList<>();

        for (Object series : dataSet.getSeries()) {
            if (series instanceof DmxSeries) {
                DmxSeries dmxSeries = (DmxSeries) series;
                LOGGER.info("Current series: {}", dmxSeries);

                String seriesKey = (String) dmxSeries.getKey();

                DmxChannel dmxChannel = new DmxChannel(Integer.parseInt(seriesKey));

                List<DmxDataItem> dmxDataItems = dmxSeries.getItems();

                for (DmxDataItem dmxDataItem : dmxDataItems) {
                    int brightness = dmxDataItem.getY().intValue();
                    int timeOffset = dmxDataItem.getX().intValue();
                    Port<?> port = dmxDataItem.getPort();

                    DmxSceneryPoint dmxSceneryPoint =
                        new DmxSceneryPoint()
                            .withBrightness(brightness).withTimeOffset(timeOffset)
                            .withUniqueId(dmxDataItem.getUniqueId()).withPort(port)
                            .withDmxChannelNumber(dmxChannel.getChannelId());

                    dmxSceneryPoints.add(dmxSceneryPoint);
                }
            }
        }

        LOGGER.info("Set scenery points: {}", dmxSceneryPoints);

        dmxModel.getDmxScenery().setSceneryPoints(dmxSceneryPoints);
    }
}
