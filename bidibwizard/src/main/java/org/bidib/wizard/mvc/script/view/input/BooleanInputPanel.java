package org.bidib.wizard.mvc.script.view.input;

import javax.swing.JCheckBox;

import org.apache.commons.lang3.StringUtils;
import org.bidib.wizard.common.context.ApplicationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.validation.ValidationResult;

public class BooleanInputPanel extends AbstractInputSelectionPanel {
    private static final Logger LOGGER = LoggerFactory.getLogger(BooleanInputPanel.class);

    private ValueModel booleanValueModel;

    private ItemSelectionModel<Boolean> booleanSelectionModel;

    @Override
    public void initialize(
        final ApplicationContext context, final DefaultFormBuilder dialogBuilder, ValidationSupport validationSupport,
        String variableName, String variableType, String caption, String defaultValue, String prevValue) {
        this.variableName = variableName;
        setDefaultValue(defaultValue);

        booleanSelectionModel = new ItemSelectionModel<>();

        booleanValueModel =
            new PropertyAdapter<ItemSelectionModel<Boolean>>(booleanSelectionModel,
                ItemSelectionModel.PROPERTY_SELECTED_ITEM, true);

        JCheckBox checkBox = BasicComponentFactory.createCheckBox(booleanValueModel, null);

        caption = "<html>" + caption + "</html>";

        dialogBuilder.append(caption, checkBox);

        String initialValue = defaultValue;
        if (StringUtils.isNotBlank(prevValue)) {
            initialValue = prevValue;
        }

        if (StringUtils.isNotBlank(initialValue)) {
            try {
                Boolean def = Boolean.valueOf(initialValue);
                LOGGER.info("Set the initial value: {}", def);
                booleanSelectionModel.setSelectedItem(def);
            }
            catch (Exception ex) {
                LOGGER.warn("Parse initial value to boolean failed: {}", initialValue, ex);
            }
        }

        content = dialogBuilder.build();
    }

    public void validate(final ValidationResult validationResult) {

    }

    @Override
    public Object getSelectedValue() {
        if (booleanValueModel.getValue() != null) {
            return booleanValueModel.getValue();
        }
        return null;
    }
}
