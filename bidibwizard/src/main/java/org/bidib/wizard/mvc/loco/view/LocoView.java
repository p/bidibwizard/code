package org.bidib.wizard.mvc.loco.view;

import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.bidib.wizard.comm.Direction;
import org.bidib.wizard.comm.SpeedSteps;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.common.utils.ImageUtils;
import org.bidib.wizard.mvc.common.view.ViewCloseListener;
import org.bidib.wizard.mvc.common.view.converter.StringConverter;
import org.bidib.wizard.mvc.common.view.graph.LedBarGraph;
import org.bidib.wizard.mvc.loco.model.LocoModel;
import org.bidib.wizard.mvc.loco.model.listener.LocoModelListener;
import org.bidib.wizard.mvc.preferences.model.Preferences;
import org.bidib.wizard.utils.InputValidationDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.binding.value.ConverterValueModel;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.builder.ButtonBarBuilder;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.FormLayout;
import com.jidesoft.swing.JideButton;
import com.jidesoft.swing.JideToggleButton;

public class LocoView extends JDialog implements LocoViewScripting {
    private static final Logger LOGGER = LoggerFactory.getLogger(LocoView.class);

    private static final long serialVersionUID = 1L;

    private LocoModel locoModel;

    private JComboBox<SpeedSteps> speedStepsCombo;

    private JTextField address;

    private JLabel speed;

    private JSlider speedSlider;

    private JLabel reportedSpeed;

    private JButton stopButton;

    private JButton stopEmergencyButton;

    private Map<String, JideToggleButton> functionButtonMap = new HashMap<>();

    private final List<LocoViewListener> locoViewListeners = new LinkedList<LocoViewListener>();

    private final List<ViewCloseListener> viewCloseListeners = new LinkedList<ViewCloseListener>();

    private LocoModelListener locoModelListener;

    private ScriptPanel scriptPanel;

    private LedBarGraph ledBarGraph;

    public LocoView(JFrame parent, final LocoModel model, int x, int y) {
        super(parent);
        this.locoModel = model;

        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        setLocation(x, y);
        setTitle(Resources.getString(getClass(), "title"));

        DefaultFormBuilder formBuilder = new DefaultFormBuilder(new FormLayout("pref:grow"));

        formBuilder.border(Borders.DIALOG);

        try {
            formBuilder.append(createLocoAddressPanel());
        }
        catch (Exception ex) {
            LOGGER.warn("Create loco address panel failed.", ex);
        }

        try {
            formBuilder.append(createDirectionPanel());
        }
        catch (Exception ex) {
            LOGGER.warn("Create direction panel failed.", ex);
        }

        final List<JideToggleButton> functionButtons = new LinkedList<JideToggleButton>();

        try {
            formBuilder.append(createLightAndStopButtonPanel(functionButtons));
        }
        catch (Exception ex) {
            LOGGER.warn("Create light and buttons panel failed.", ex);
        }

        // c.gridy++;
        try {
            formBuilder.appendParagraphGapRow();
            formBuilder.appendRow("pref");
            formBuilder.nextLine(2);
            formBuilder.append(createFunctionButtonPanel(functionButtons));
        }
        catch (Exception ex) {
            LOGGER.warn("Create function buttons panel failed.", ex);
        }

        // prepare the script panel
        scriptPanel = new ScriptPanel(this);
        addLocoViewListener(scriptPanel);
        JPanel panel = scriptPanel.createPanel();

        panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
            Resources.getString(getClass(), "script") + ":"));

        formBuilder.append(panel);

        // add loco model listener
        locoModelListener = new LocoModelListener() {
            @Override
            public void addressChanged(int address) {
                // LOGGER.info(">>>> Address changed: {}", address);
                locoModel.setReportedSpeed(0);
            }

            @Override
            public void directionChanged(Direction direction) {
            }

            @Override
            public void functionChanged(int index, boolean value) {
                functionButtons.get(index).setSelected(value);
            }

            @Override
            public void speedStepsChanged(SpeedSteps speedSteps) {
                speedSlider.setMaximum(speedSteps.getSteps());
                speedSlider.setMinimum(-speedSteps.getSteps());
                speedSlider.setMajorTickSpacing(speedSlider.getMaximum());
            }

            @Override
            public void dynStateEnergyChanged(final int dynStateEnergy) {

                LOGGER.info("The dynStateEnergy has changed, dynStateEnergy: {}", dynStateEnergy);

                SwingUtilities.invokeLater(new Runnable() {

                    @Override
                    public void run() {

                        if (ledBarGraph != null) {
                            ledBarGraph.setValue(dynStateEnergy);
                        }
                    }
                });

            }
        };
        locoModel.addLocoModelListener(locoModelListener);

        locoModel.addPropertyChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                switch (evt.getPropertyName()) {
                    case LocoModel.PROPERTYNAME_SPEED:
                        try {
                            int speed = (Integer) evt.getNewValue();
                            if (speed == 0 || speed == 1) {
                                speedSlider.setValue(0);
                            }
                        }
                        catch (Exception ex) {
                            LOGGER.warn("Set speed failed.", ex);
                        }
                        break;
                    default:
                        break;
                }
            }
        });

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                LOGGER.info("The window is closing: {}", e.getWindow());

                cleanup();

                super.windowClosing(e);
            }

            @Override
            public void windowClosed(WindowEvent e) {
                LOGGER.info("The window is closed: {}", e.getWindow());

                super.windowClosed(e);
            }
        });

        getContentPane().setLayout(new BorderLayout());
        getContentPane().add(formBuilder.build(), BorderLayout.CENTER);

        pack();

        setVisible(true);
    }

    private JPanel createLocoAddressPanel() {
        DefaultFormBuilder formBuilder =
            new DefaultFormBuilder(new FormLayout("pref, 3dlu, 60dlu, 10dlu, pref, 3dlu, 30dlu, 3dlu:grow"));

        address = new JTextField();
        address.setDocument(new InputValidationDocument(InputValidationDocument.NUMERIC));

        // set the default address
        address.setText("3");

        locoModel.setAddress(3);

        address.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void changedUpdate(DocumentEvent e) {
                valueChanged();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                valueChanged();
            }

            @Override
            public void insertUpdate(DocumentEvent e) {
                valueChanged();
            }

            private void valueChanged() {
                int currentAddress = 0;

                if (!address.getText().isEmpty()) {
                    // LOGGER.info("Get the address from the textfield.");
                    try {
                        currentAddress = Integer.parseInt(address.getText());
                    }
                    catch (NumberFormatException e) {
                        LOGGER.warn("Parse decoder address failed.");
                    }
                }
                if (locoModel != null) {
                    LOGGER.info("Set the new address: {}", currentAddress);
                    locoModel.setAddress(currentAddress);
                }
            }
        });

        formBuilder.append(Resources.getString(getClass(), "address"), address);

        boolean m4SupportEnabled = Preferences.getInstance().isM4SupportEnabled();

        if (m4SupportEnabled) {
            speedStepsCombo = new JComboBox<SpeedSteps>(SpeedSteps.values());
        }
        else {
            // filter the M4 speed steps

            List<SpeedSteps> speedSteps = new ArrayList<>();
            speedSteps.addAll(Arrays.asList(SpeedSteps.values()));
            CollectionUtils.filter(speedSteps, new Predicate<SpeedSteps>() {

                @Override
                public boolean evaluate(SpeedSteps speedStep) {
                    return (speedStep.ordinal() <= SpeedSteps.DCC128.ordinal());
                }
            });

            speedStepsCombo = new JComboBox<SpeedSteps>(speedSteps.toArray(new SpeedSteps[0]));
        }

        speedStepsCombo.setSelectedItem(locoModel.getSpeedSteps());

        speedStepsCombo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                LOGGER.info("Speed steps combo has changed, update loco model.");
                locoModel.setSpeedSteps((SpeedSteps) ((JComboBox<SpeedSteps>) e.getSource()).getSelectedItem());
            }
        });

        formBuilder.append(Resources.getString(getClass(), "speedSteps"), speedStepsCombo);

        return formBuilder.build();
    }

    private JPanel createDirectionPanel() {
        DefaultFormBuilder formBuilder = new DefaultFormBuilder(new FormLayout("pref, 3dlu, 60dlu:grow, 3dlu, pref"));

        formBuilder.border(new EmptyBorder(10, 0, 10, 0));

        ValueModel speedModel = new PropertyAdapter<LocoModel>(locoModel, LocoModel.PROPERTYNAME_SPEED, true);
        final ValueModel speedConverterModel =
            new ConverterValueModel(speedModel, new StringConverter(new DecimalFormat("#")));

        speed = BasicComponentFactory.createLabel(speedConverterModel);
        formBuilder.append(Resources.getString(getClass(), "speed"), speed);
        formBuilder.nextLine();

        ValueModel reportedSpeedModel =
            new PropertyAdapter<LocoModel>(locoModel, LocoModel.PROPERTYNAME_REPORTED_SPEED, true);
        final ValueModel reportedSpeedConverterModel =
            new ConverterValueModel(reportedSpeedModel, new StringConverter(new DecimalFormat("#")));

        reportedSpeed = BasicComponentFactory.createLabel(reportedSpeedConverterModel);
        formBuilder.append(Resources.getString(getClass(), "reportedSpeed"), reportedSpeed);
        formBuilder.nextLine();

        speedSlider = new JSlider(JSlider.HORIZONTAL);

        speedSlider.setMaximum(locoModel.getSpeedSteps().getSteps());
        speedSlider.setMinimum(-locoModel.getSpeedSteps().getSteps());
        speedSlider.setMajorTickSpacing(speedSlider.getMaximum());
        speedSlider.setPaintTicks(true);

        // init value before add the change listener because otherwise stops the loco ...
        speedSlider.setValue(0);

        speedSlider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                int value = (int) speedSlider.getValue();

                if (value > 0) {
                    locoModel.setDirection(Direction.FORWARD);
                    value++;
                }
                else if (value < 0) {
                    locoModel.setDirection(Direction.BACKWARD);
                    value--;
                }
                value = Math.abs(value);
                LOGGER.debug("Set the speed value: {}", value);
                locoModel.setSpeed(value);
            }
        });

        formBuilder.append(speedSlider, 5);

        formBuilder.append(Resources.getString(getClass(), "backwards"));
        formBuilder.nextColumn(2);
        formBuilder.append(Resources.getString(getClass(), "forwards"));

        formBuilder.nextLine();

        JPanel helperPanel = new JPanel(new BorderLayout());

        ledBarGraph = new LedBarGraph(10);

        helperPanel.add(formBuilder.build(), BorderLayout.CENTER);
        helperPanel.add(ledBarGraph, BorderLayout.EAST);

        return helperPanel;
    }

    private JPanel createLightAndStopButtonPanel(final List<JideToggleButton> functionButtons) {

        DefaultFormBuilder formBuilder = new DefaultFormBuilder(new FormLayout("pref, 3dlu, 60dlu:grow, 3dlu, pref"));

        // add the light icons
        ImageIcon lightOnIcon = ImageUtils.createImageIcon(LocoView.class, "/icons/16x16/lightbulb.png");
        ImageIcon lightOffIcon = ImageUtils.createImageIcon(LocoView.class, "/icons/16x16/lightbulb_off.png");

        final JideToggleButton lightButton = new JideToggleButton(lightOffIcon);
        lightButton.setSelectedIcon(lightOnIcon);
        lightButton.setButtonStyle(JideButton.TOOLBOX_STYLE);

        lightButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                locoModel.setFunction(0, lightButton.isSelected());
            }
        });
        functionButtons.add(lightButton);
        functionButtonMap.put("F0", lightButton);

        formBuilder.append(lightButton);

        JPanel stopPanel = new JPanel(new GridBagLayout());

        stopPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
            Resources.getString(getClass(), "stopLoco") + ":"));

        stopButton = new JButton(Resources.getString(getClass(), "stop"));

        stopButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireStop();
            }
        });

        stopEmergencyButton = new JButton(Resources.getString(getClass(), "emergencyStop"));

        stopEmergencyButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireEmergencyStop();
            }
        });

        JPanel buttonPanel = new ButtonBarBuilder().addGlue().addButton(stopButton, stopEmergencyButton).build();
        formBuilder.add(buttonPanel);

        return formBuilder.build();
    }

    private JPanel createFunctionButtonPanel(final List<JideToggleButton> functionButtons) {

        DefaultFormBuilder formBuilder =
            new DefaultFormBuilder(new FormLayout(
                "pref, 8dlu, pref, 8dlu, pref, 8dlu, pref, 8dlu, pref, 8dlu, pref, 8dlu, pref, 8dlu, pref, 8dlu, pref, 8dlu, pref"));

        formBuilder.appendSeparator(Resources.getString(getClass(), "additionalFunctions"));

        final int columns = 10;

        for (int row = 0; row < 4; row++) {

            for (int column = 0; column < columns; column++) {
                final int functionIndex = row * columns + (column + 1);
                final String buttonText = "F" + functionIndex;
                final JideToggleButton functionButton = new JideToggleButton(buttonText);
                functionButton.setButtonStyle(JideButton.TOOLBOX_STYLE);

                functionButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        LOGGER.info("Set function with index: {}", functionIndex);
                        locoModel.setFunction(functionIndex, functionButton.isSelected());
                    }
                });
                functionButtons.add(functionButton);

                functionButtonMap.put(buttonText, functionButton);

                formBuilder.append(functionButton);
            }
        }

        return formBuilder.build();
    }

    private void addLocoViewListener(LocoViewListener listener) {
        locoViewListeners.add(listener);
    }

    public void addViewCloseListener(ViewCloseListener listener) {
        viewCloseListeners.add(listener);
    }

    private void fireEmergencyStop() {
        locoModel.setSpeed(1);

        for (LocoViewListener listener : locoViewListeners) {
            listener.emergencyStop();
        }
    }

    private void fireStop() {
        locoModel.setSpeed(0);

        for (LocoViewListener listener : locoViewListeners) {
            listener.stop();
        }
    }

    @Override
    public void selectDecoderAddress(int dccAddress) {
        LOGGER.info("Select the decoder address: {}", dccAddress);
        address.setText(String.valueOf(dccAddress));
    }

    @Override
    public void setSpeedSteps(SpeedSteps speedSteps) {
        LOGGER.info("Set the speed steps: {}", speedSteps);
        speedStepsCombo.setSelectedItem(speedSteps);
    }

    @Override
    public void setSpeed(int speed) {
        LOGGER.info("Set the speed: {}", speed);
        speedSlider.setValue(speed);
    }

    @Override
    public void setFunction(int function) {
        LOGGER.info("Set the function: {}", function);

        JideToggleButton functionButton = functionButtonMap.get("F" + function);
        LOGGER.warn("Fetched functionButton: {}", functionButton);

        functionButton.doClick();
    }

    @Override
    public void setStop() {
        LOGGER.info("Set stop.");

        stopButton.doClick();
    }

    @Override
    public void setStopEmergency() {
        LOGGER.info("Set stop emergency.");

        stopEmergencyButton.doClick();
    }

    public void close() {
        LOGGER.info("The LocoView should be closed.");

        cleanup();

        LOGGER.info("Cleanup has passed, set visible false.");
        setVisible(false);
    }

    private void cleanup() {
        LOGGER.info("The LocoView is disposed.");

        for (ViewCloseListener closeListener : viewCloseListeners) {
            try {
                closeListener.close();
            }
            catch (Exception ex) {
                LOGGER.warn("Notify view close listener failed.", ex);
            }
        }

        viewCloseListeners.clear();

        locoViewListeners.clear();

        if (locoModelListener != null) {
            locoModel.removeLocoModelListener(locoModelListener);
            locoModelListener = null;
        }

        if (scriptPanel != null) {
            scriptPanel.close();
            scriptPanel = null;
        }

        if (locoModel != null) {
            locoModel = null;
        }

        functionButtonMap.clear();
    }
}
