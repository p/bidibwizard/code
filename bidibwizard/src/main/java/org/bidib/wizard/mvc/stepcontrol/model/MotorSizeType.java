package org.bidib.wizard.mvc.stepcontrol.model;

import org.bidib.jbidibc.core.enumeration.BidibEnum;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.wizard.common.locale.Resources;

public enum MotorSizeType implements BidibEnum {
    nema8("nema8", 8, 200, 50), nema11("nema11", 11, 500, 100), nema13("nema13", 13, 500, 100), nema17("nema17", 17,
        800, 200), nema23("nema23", 23, 1200, 300);

    private final byte type;

    private final String key;

    private final int currentMoving;

    private final int currentStopped;

    MotorSizeType(String key, int type, int currentMoving, int currentStopped) {
        this.key = key;
        this.type = ByteUtils.getLowByte(type);
        this.currentMoving = currentMoving;
        this.currentStopped = currentStopped;
    }

    @Override
    public byte getType() {
        return type;
    }

    public String getKey() {
        return key;
    }

    public int getCvValue() {
        return ByteUtils.getInt(type);
    }

    /**
     * @return the currentMoving in mA
     */
    public int getCurrentMoving() {
        return currentMoving;
    }

    /**
     * @return the currentStopped in mA
     */
    public int getCurrentStopped() {
        return currentStopped;
    }

    @Override
    public String toString() {
        return Resources.getString(getClass(), key);
    }

    public static MotorSizeType fromValue(byte type) {
        MotorSizeType result = null;
        for (MotorSizeType e : values()) {
            if (e.type == type) {
                result = e;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("cannot map " + type + " to a MotorSizeType enum");
        }
        return result;
    }
}
