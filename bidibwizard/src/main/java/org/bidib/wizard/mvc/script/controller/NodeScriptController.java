package org.bidib.wizard.mvc.script.controller;

import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.StringWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.exception.MethodInvocationException;
import org.apache.velocity.exception.ParseErrorException;
import org.bidib.jbidibc.core.Feature;
import org.bidib.jbidibc.core.enumeration.FeatureEnum;
import org.bidib.jbidibc.core.utils.NodeUtils;
import org.bidib.wizard.common.context.ApplicationContext;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.common.utils.ImageUtils;
import org.bidib.wizard.labels.Labels;
import org.bidib.wizard.labels.PortLabelUtils;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.bidib.wizard.mvc.main.controller.MainControllerInterface;
import org.bidib.wizard.mvc.main.model.LightPort;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.model.listener.DefaultNodeListListener;
import org.bidib.wizard.mvc.main.model.listener.NodeListListener;
import org.bidib.wizard.mvc.preferences.model.Preferences;
import org.bidib.wizard.mvc.script.controller.exception.UserDefinedVelocityException;
import org.bidib.wizard.mvc.script.controller.exception.VelocityExceptionThrower;
import org.bidib.wizard.mvc.script.controller.listener.ExecuteScriptListener;
import org.bidib.wizard.mvc.script.model.NodeScriptModel;
import org.bidib.wizard.mvc.script.view.NodeScriptPreView;
import org.bidib.wizard.mvc.script.view.NodeScriptView;
import org.bidib.wizard.mvc.script.view.NodeScripting;
import org.bidib.wizard.mvc.script.view.ScriptParser;
import org.bidib.wizard.mvc.script.view.input.CancelledByUserException;
import org.bidib.wizard.mvc.script.view.utils.NodeRequirementException;
import org.bidib.wizard.mvc.script.view.utils.NodeScriptUtils;
import org.bidib.wizard.mvc.script.view.wizard.NodeScriptWizard;
import org.bidib.wizard.script.DefaultScriptContext;
import org.bidib.wizard.script.ScriptCommand;
import org.bidib.wizard.script.ScriptEngineListener;
import org.bidib.wizard.script.engine.ScriptEngine;
import org.bidib.wizard.script.engine.ScriptEngine.ScriptStatus;
import org.bidib.wizard.script.node.NodeScriptCommandList;
import org.bidib.wizard.script.node.NodeScriptCommandList.ExecutionStatus;
import org.bidib.wizard.utils.DockUtils;
import org.oxbow.swingbits.dialog.task.TaskDialog;
import org.oxbow.swingbits.dialog.task.TaskDialogs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vlsolutions.swing.docking.Dockable;
import com.vlsolutions.swing.docking.DockingDesktop;
import com.vlsolutions.swing.docking.RelativeDockablePosition;
import com.vlsolutions.swing.docking.event.DockableStateChangeEvent;
import com.vlsolutions.swing.docking.event.DockableStateChangeListener;

public class NodeScriptController implements ExecuteScriptListener, NodeScriptProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(NodeScriptController.class);

    private NodeScriptView nodeScriptView;

    private NodeScriptPreView nodeScriptPreView;

    private NodeScriptModel nodeScriptModel;

    private NodeListListener nodeListListener;

    private ScriptEngine<NodeScripting> scriptEngine;

    private MainModel mainModel;

    private DockingDesktop desktop;

    private PropertyChangeListener executionStatusListener;

    private ImageIcon successfulIcon;

    private DefaultScriptContext scriptContext;

    private NodeScriptController() {
    }

    public static synchronized NodeScriptController getInstance() {

        ApplicationContext applicationContext = DefaultApplicationContext.getInstance();
        NodeScriptController instance = applicationContext.get("NodeScriptController", NodeScriptController.class);
        if (instance == null) {
            LOGGER.info("Create and register the NodeScriptController.");
            instance = new NodeScriptController();
            applicationContext.register("NodeScriptController", instance);
        }

        return instance;
    }

    private void start(
        final DockingDesktop desktop, final MainModel mainModel, final MainControllerInterface mainController) {
        LOGGER.info("Start the NodeScriptController.");
        this.desktop = desktop;

        LOGGER.info("Create new NodeScriptView.");
        this.mainModel = mainModel;

        successfulIcon = ImageUtils.createImageIcon(NodeScriptController.class, "/icons/successful_32x32.png");

        nodeScriptModel = new NodeScriptModel();

        nodeScriptModel.addPropertyChangeListener(NodeScriptModel.PROPERTYNAME_NODE_SCRIPT_COMMAND_LIST,
            new PropertyChangeListener() {

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    LOGGER.info("The node script commands have changed. Start the engine ...");

                    final NodeScriptCommandList nodeScriptCommandList = nodeScriptModel.getNodeScriptCommandList();

                    if (!ExecutionStatus.pending.equals(nodeScriptCommandList.getExecutionStatus())) {
                        LOGGER.info("The execution status is not pending, skip processing.");
                        return;
                    }

                    if (executionStatusListener == null) {
                        executionStatusListener = new PropertyChangeListener() {

                            @Override
                            public void propertyChange(PropertyChangeEvent evt) {

                                SwingUtilities.invokeLater(new Runnable() {
                                    public void run() {
                                        final ExecutionStatus executionStatus =
                                            nodeScriptCommandList.getExecutionStatus();
                                        switch (executionStatus) {
                                            case finished:
                                            case finishedWithErrors:
                                                LOGGER.info(
                                                    "The execution of the script has finished with executionStatus: {}",
                                                    executionStatus);

                                                // remove as property change listener
                                                nodeScriptCommandList.removePropertyChangeListener(
                                                    NodeScriptCommandList.PROPERTYNAME_EXECUTION_STATUS,
                                                    executionStatusListener);

                                                executionStatusListener = null;

                                                if (scriptContext != null) {

                                                    if (executionStatus == ExecutionStatus.finished) {
                                                        TaskDialogs
                                                            .build(
                                                                JOptionPane.getFrameForComponent(
                                                                    nodeScriptView.getComponent()),
                                                                Resources.getString(NodeScriptController.class,
                                                                    "execute-script-finished.instruction"),
                                                                Resources.getString(NodeScriptController.class,
                                                                    "execute-script-finished"))
                                                            .title(Resources.getString(NodeScriptController.class,
                                                                "execute-script.title"))
                                                            .icon(successfulIcon).inform();
                                                    }
                                                    else {
                                                        // finished with errors

                                                        final Map<String, Object> context =
                                                            nodeScriptModel.getContext();
                                                        List<String> scriptErrors =
                                                            (List<String>) context.get(ScriptParser.KEY_SCRIPT_ERRORS);

                                                        StringBuilder sb =
                                                            new StringBuilder(
                                                                Resources.getString(NodeScriptController.class,
                                                                    "execute-script-finished-with-errors"));
                                                        if (CollectionUtils.isNotEmpty(scriptErrors)) {
                                                            sb.append("<br/><list>");
                                                            for (String error : scriptErrors) {
                                                                sb.append("<li>").append(error).append("</li>");
                                                            }
                                                            sb.append("</list>");
                                                        }

                                                        TaskDialogs
                                                            .build(
                                                                JOptionPane.getFrameForComponent(
                                                                    nodeScriptView.getComponent()),
                                                                Resources.getString(NodeScriptController.class,
                                                                    "execute-script-finished-with-errors.instruction"),
                                                                sb.toString())
                                                            .title(Resources.getString(NodeScriptController.class,
                                                                "execute-script.title"))
                                                            .icon(TaskDialog.StandardIcon.WARNING).inform();

                                                        // remove old errors
                                                        scriptErrors.clear();

                                                    }
                                                }
                                                else {
                                                    LOGGER.info("No scriptContext available.");
                                                }
                                                break;
                                            case aborted:
                                                LOGGER.info("The execution of the script was aborted.");

                                                // remove as property change listener
                                                nodeScriptCommandList.removePropertyChangeListener(
                                                    NodeScriptCommandList.PROPERTYNAME_EXECUTION_STATUS,
                                                    executionStatusListener);

                                                executionStatusListener = null;

                                                if (scriptContext != null) {
                                                    TaskDialogs
                                                        .build(
                                                            JOptionPane
                                                                .getFrameForComponent(nodeScriptView.getComponent()),
                                                            Resources.getString(NodeScriptController.class,
                                                                "execute-script-aborted.instruction"),
                                                            Resources.getString(NodeScriptController.class,
                                                                "execute-script-aborted"))
                                                        .title(Resources.getString(NodeScriptController.class,
                                                            "execute-script.title"))
                                                        .error();
                                                }
                                                else {
                                                    LOGGER.info("No scriptContext available.");
                                                }
                                                break;
                                            default:
                                                break;
                                        }

                                    }
                                });
                            }
                        };
                    }

                    if (executionStatusListener != null) {
                        LOGGER.info("Add executionStatusListener to nodeScriptCommandList.");
                        nodeScriptCommandList.addPropertyChangeListener(
                            NodeScriptCommandList.PROPERTYNAME_EXECUTION_STATUS, executionStatusListener);
                    }

                    List<ScriptCommand<NodeScripting>> commands = nodeScriptCommandList.getNodeScriptCommands();
                    if (CollectionUtils.isNotEmpty(commands)) {
                        LOGGER.info("Process the node script commands.");
                        nodeScriptCommandList.setExecutionStatus(ExecutionStatus.pending);

                        final Map<String, Object> context = nodeScriptModel.getContext();

                        // remove the script error marker
                        context.remove(ScriptParser.KEY_SCRIPT_ERRORS);

                        // create the script engine
                        scriptEngine = new ScriptEngine<NodeScripting>(mainController.getNodeScripting(), context);
                        scriptEngine.addScriptEngineListener(new ScriptEngineListener<NodeScripting>() {

                            @Override
                            public void scriptStatusChanged(ScriptStatus scriptStatus) {
                                LOGGER.info("The script status has changed: {}", scriptStatus);

                                switch (scriptStatus) {
                                    case STOPPED:
                                    case FINISHED:
                                        nodeScriptCommandList.setExecutionStatus(ExecutionStatus.finished);
                                        break;
                                    case FINISHED_WITH_ERRORS:
                                        nodeScriptCommandList.setExecutionStatus(ExecutionStatus.finishedWithErrors);
                                        break;
                                    case ABORTED:
                                        nodeScriptCommandList.setExecutionStatus(ExecutionStatus.aborted);
                                        break;
                                    case RUNNING:
                                        nodeScriptCommandList.setExecutionStatus(ExecutionStatus.running);
                                        break;
                                    default:
                                        break;
                                }
                            }

                            @Override
                            public void currentCommandChanged(ScriptCommand<NodeScripting> command) {
                                LOGGER.info("The current command has changed: {}", command);
                            }
                        });
                        scriptEngine.setScriptCommands(commands);

                        LOGGER.info("Start the execution of the script in the scriptEngine.");
                        scriptEngine.startScript();
                    }
                    else {
                        final Map<String, Object> context = nodeScriptModel.getContext();

                        // TODO add I18N
                        String errorDescription = "No steps available!";
                        addError(context, errorDescription);
                        nodeScriptCommandList.setExecutionStatus(ExecutionStatus.finishedWithErrors);
                    }

                    LOGGER.info("Reset the commands to execute.");
                }
            });

        nodeListListener = new DefaultNodeListListener() {

            @Override
            public void nodeChanged() {
                Node selectedNode = mainModel.getSelectedNode();
                LOGGER.info("The selected node has changed: {}", selectedNode);
                nodeScriptModel.setSelectedNode(selectedNode);
            }
        };
        mainModel.addNodeListListener(nodeListListener);

        // initialize
        nodeListListener.nodeChanged();
    }

    protected void addError(final Map<String, Object> context, String errorDescription) {

        List<String> scriptErrors = (List<String>) context.get(ScriptParser.KEY_SCRIPT_ERRORS);
        if (scriptErrors == null) {
            scriptErrors = new LinkedList<>();
            context.put(ScriptParser.KEY_SCRIPT_ERRORS, scriptErrors);
        }

        // TODO add the error

        scriptErrors.add(errorDescription);
    }

    public void showView(
        final DockingDesktop desktop, final MainModel mainModel, final MainControllerInterface mainController) {

        // check if the nodeScript view is already opened
        String searchKey = NodeScriptView.NODE_SCRIPT_VIEW;
        LOGGER.info("Search for view with key: {}", searchKey);
        Dockable view = desktop.getContext().getDockableByKey(searchKey);
        if (view != null) {
            LOGGER.info("Select the existing nodeScript view.");
            DockUtils.selectWindow(view);
            return;
        }

        start(desktop, mainModel, mainController);

        nodeScriptView = new NodeScriptView(nodeScriptModel);
        nodeScriptView.setExecuteScriptListener(this);

        if (desktop.getDockables().length > 1) {
            Dockable dock = desktop.getDockables()[1].getDockable();

            desktop.createTab(dock, nodeScriptView, 1, true);
        }
        else {
            desktop.addDockable(nodeScriptView, RelativeDockablePosition.RIGHT);
        }

        desktop.addDockableStateChangeListener(new DockableStateChangeListener() {

            @Override
            public void dockableStateChanged(DockableStateChangeEvent event) {
                if (event.getNewState().getDockable().equals(nodeScriptView) && event.getNewState().isClosed()) {
                    LOGGER.info("NodeScriptView was closed, free resources.");

                    if (nodeListListener != null) {
                        LOGGER.info("Remove nodeListListener: {}", nodeListListener);
                        mainModel.removeNodeListListener(nodeListListener);
                        nodeListListener = null;
                    }
                }
            }
        });

        // initialize
        nodeListListener.nodeChanged();

    }

    public void showWizard(
        final DockingDesktop desktop, final MainModel mainModel, final MainControllerInterface mainController) {
        LOGGER.info("Show the node script wizard.");

        Node selectedNode = mainModel.getSelectedNode();

        if (mainModel.getSelectedNode() == null) {
            LOGGER.warn("Cannot open wizard if no node is selected.");
            return;
        }

        if (this.mainModel == null) {
            start(desktop, mainModel, mainController);
        }

        // clear the scriptContext
        scriptContext = null;

        NodeScriptWizard nodeScriptWizard = new NodeScriptWizard(this);
        nodeScriptWizard.showWizard(desktop, selectedNode.getNode());

        LOGGER.info("The node script wizard has finished.");
    }

    @Override
    public boolean prepareVelocityContext(String script, final ApplicationContext scriptContext) {
        LOGGER.info("Prepare the velocity context from the script.");

        ApplicationContext applicationContext = DefaultApplicationContext.getInstance();

        Labels nodeLabels = (Labels) applicationContext.get(DefaultApplicationContext.KEY_NODE_LABELS, Labels.class);

        // prepare the script context
        if (scriptContext != null) {
            LOGGER.info("Remove some keys from the context.");
            scriptContext.unregister(ScriptContextKeys.KEY_DEFINE_LINES);
            scriptContext.unregister(ScriptContextKeys.KEY_INPUT_LINES);
            scriptContext.unregister(ScriptContextKeys.KEY_INSTRUCTION_LINES);
            scriptContext.unregister(ScriptContextKeys.KEY_APPLICATION_LINES);

            scriptContext.unregister(ScriptContextKeys.KEY_VELOCITY_CONTEXT);
        }
        else {
            throw new IllegalArgumentException("The scriptContext must no be null.");
        }

        Node selectedNode = nodeScriptModel.getSelectedNode();

        scriptContext.register(ScriptParser.KEY_SELECTED_NODE, selectedNode);
        scriptContext.register(ScriptParser.KEY_NODE_LABELS, nodeLabels);
        scriptContext.register(ScriptParser.KEY_MAIN_MODEL, mainModel);

        long uniqueId = selectedNode.getUniqueId();
        Labels macroLabels = applicationContext.get(DefaultApplicationContext.KEY_MACRO_LABELS, Labels.class);
        scriptContext.register(ScriptParser.KEY_MACRO_LABELS, PortLabelUtils.getCopyOfMap(macroLabels, uniqueId));

        Labels flagLabels = applicationContext.get(DefaultApplicationContext.KEY_FLAG_LABELS, Labels.class);
        scriptContext.register(ScriptParser.KEY_FLAG_LABELS, PortLabelUtils.getCopyOfMap(flagLabels, uniqueId));

        Labels accessoryLabels = applicationContext.get(DefaultApplicationContext.KEY_ACCESSORY_LABELS, Labels.class);
        scriptContext.register(ScriptParser.KEY_ACCESSORY_LABELS,
            PortLabelUtils.getCopyOfMap(accessoryLabels, uniqueId));

        Labels analogLabels = applicationContext.get(DefaultApplicationContext.KEY_ANALOGPORT_LABELS, Labels.class);
        scriptContext.register(ScriptParser.KEY_ANALOG_LABELS, PortLabelUtils.getCopyOfMap(analogLabels, uniqueId));

        Labels backlightLabels =
            applicationContext.get(DefaultApplicationContext.KEY_BACKLIGHTPORT_LABELS, Labels.class);
        scriptContext.register(ScriptParser.KEY_BACKLIGHT_LABELS,
            PortLabelUtils.getCopyOfMap(backlightLabels, uniqueId));

        Labels inputLabels = applicationContext.get(DefaultApplicationContext.KEY_INPUTPORT_LABELS, Labels.class);
        scriptContext.register(ScriptParser.KEY_INPUT_LABELS, PortLabelUtils.getCopyOfMap(inputLabels, uniqueId));

        Labels lightLabels = applicationContext.get(DefaultApplicationContext.KEY_LIGHTPORT_LABELS, Labels.class);
        scriptContext.register(ScriptParser.KEY_LIGHT_LABELS, PortLabelUtils.getCopyOfMap(lightLabels, uniqueId));

        Labels motorLabels = applicationContext.get(DefaultApplicationContext.KEY_MOTORPORT_LABELS, Labels.class);
        scriptContext.register(ScriptParser.KEY_MOTOR_LABELS, PortLabelUtils.getCopyOfMap(motorLabels, uniqueId));

        Labels servoLabels = applicationContext.get(DefaultApplicationContext.KEY_SERVOPORT_LABELS, Labels.class);
        scriptContext.register(ScriptParser.KEY_SERVO_LABELS, PortLabelUtils.getCopyOfMap(servoLabels, uniqueId));

        Labels soundLabels = applicationContext.get(DefaultApplicationContext.KEY_SOUNDPORT_LABELS, Labels.class);
        scriptContext.register(ScriptParser.KEY_SOUND_LABELS, PortLabelUtils.getCopyOfMap(soundLabels, uniqueId));

        Labels switchLabels = applicationContext.get(DefaultApplicationContext.KEY_SWITCHPORT_LABELS, Labels.class);
        scriptContext.register(ScriptParser.KEY_SWITCH_LABELS, PortLabelUtils.getCopyOfMap(switchLabels, uniqueId));

        Labels switchPairLabels =
            applicationContext.get(DefaultApplicationContext.KEY_SWITCHPAIRPORT_LABELS, Labels.class);
        scriptContext.register(ScriptParser.KEY_SWITCHPAIR_LABELS,
            PortLabelUtils.getCopyOfMap(switchPairLabels, uniqueId));

        // initialize velocity to be strict
        Velocity.setProperty("runtime.references.strict", true);
        Velocity.init();

        // preprocessing of script
        final VelocityContext velocityContext = new VelocityContext();
        velocityContext.put("exceptionThrower", new VelocityExceptionThrower());

        scriptContext.register(ScriptContextKeys.KEY_VELOCITY_CONTEXT, velocityContext);

        // add the reserved keys
        velocityContext.put(NodeScriptReservedKeys.KEY_UNIQUEID, uniqueId);
        velocityContext.put(NodeScriptReservedKeys.KEY_NODE_VID, NodeUtils.getVendorId(uniqueId));
        velocityContext.put(NodeScriptReservedKeys.KEY_NODE_PID,
            NodeUtils.getPid(uniqueId, selectedNode.getNode().getRelevantPidBits()));
        velocityContext.put(NodeScriptReservedKeys.KEY_NODE_FIRMWARE_VERSION,
            selectedNode.getNode().getSoftwareVersion());
        velocityContext.put(NodeScriptReservedKeys.KEY_POWER_USER, Preferences.getInstance().isPowerUser());

        if (NodeUtils.hasAccessoryFunctions(uniqueId)) {
            Feature accessoryCount =
                Feature.findFeature(selectedNode.getNode().getFeatures(),
                    FeatureEnum.FEATURE_ACCESSORY_COUNT.getNumber());
            if (accessoryCount != null) {
                velocityContext.put(NodeScriptReservedKeys.KEY_ACCESSORY_COUNT, accessoryCount.getValue());
            }

            Feature accessoryMacroMapped =
                Feature.findFeature(selectedNode.getNode().getFeatures(),
                    FeatureEnum.FEATURE_ACCESSORY_MACROMAPPED.getNumber());
            if (accessoryMacroMapped != null) {
                velocityContext.put(NodeScriptReservedKeys.KEY_ACCESSORY_MACRO_MAPPED, accessoryMacroMapped.getValue());
            }

            Feature macroCount =
                Feature.findFeature(selectedNode.getNode().getFeatures(),
                    FeatureEnum.FEATURE_CTRL_MAC_COUNT.getNumber());
            if (macroCount != null) {
                velocityContext.put(NodeScriptReservedKeys.KEY_MACRO_COUNT, macroCount.getValue());
            }

            Feature macroSize =
                Feature.findFeature(selectedNode.getNode().getFeatures(),
                    FeatureEnum.FEATURE_CTRL_MAC_SIZE.getNumber());
            if (macroSize != null) {
                velocityContext.put(NodeScriptReservedKeys.KEY_MACRO_SIZE, macroSize.getValue());
            }
        }

        if (NodeUtils.hasSwitchFunctions(uniqueId)) {
            // get the dimm stretch value if the node has light ports
            try {
                List<LightPort> lightPorts = mainModel.getLightPorts();
                if (CollectionUtils.isNotEmpty(lightPorts)) {
                    LightPort lightPort = lightPorts.get(0);
                    int dimmStretchMin = lightPort.getDimStretchMin();
                    int dimmStretchMax = lightPort.getDimStretchMax();

                    int dimmRange = 8;
                    if (dimmStretchMax == 65535) {
                        dimmRange = 16;
                    }

                    LOGGER.info("Set the values for dimmStretchMin: {}, dimmStretchMax: {}, dimmRange: {}",
                        dimmStretchMin, dimmStretchMax, dimmRange);

                    velocityContext.put(NodeScriptReservedKeys.KEY_DIMMRANGE, dimmRange);
                }
            }
            catch (Exception ex) {
                LOGGER.warn("Get the dimm stretch values failed.", ex);
            }

            Feature backlightCount =
                Feature.findFeature(selectedNode.getNode().getFeatures(),
                    FeatureEnum.FEATURE_CTRL_BACKLIGHT_COUNT.getNumber());
            if (backlightCount != null) {
                velocityContext.put(NodeScriptReservedKeys.KEY_BACKLIGHT_COUNT, backlightCount.getValue());
            }

            Feature inputCount =
                Feature.findFeature(selectedNode.getNode().getFeatures(),
                    FeatureEnum.FEATURE_CTRL_INPUT_COUNT.getNumber());
            if (inputCount != null) {
                velocityContext.put(NodeScriptReservedKeys.KEY_INPUT_COUNT, inputCount.getValue());
            }

            Feature lightCount =
                Feature.findFeature(selectedNode.getNode().getFeatures(),
                    FeatureEnum.FEATURE_CTRL_LIGHT_COUNT.getNumber());
            if (lightCount != null) {
                velocityContext.put(NodeScriptReservedKeys.KEY_LIGHT_COUNT, lightCount.getValue());
            }

            Feature servoCount =
                Feature.findFeature(selectedNode.getNode().getFeatures(),
                    FeatureEnum.FEATURE_CTRL_SERVO_COUNT.getNumber());
            if (servoCount != null) {
                velocityContext.put(NodeScriptReservedKeys.KEY_SERVO_COUNT, servoCount.getValue());
            }

            Feature switchCount =
                Feature.findFeature(selectedNode.getNode().getFeatures(),
                    FeatureEnum.FEATURE_CTRL_SWITCH_COUNT.getNumber());
            if (switchCount != null) {
                velocityContext.put(NodeScriptReservedKeys.KEY_SWITCH_COUNT, switchCount.getValue());
            }
        }

        List<String> defineLines = new LinkedList<>();

        scriptContext.register(ScriptContextKeys.KEY_DEFINE_LINES, defineLines);

        try {
            // scan the script for input parameters
            // ##input(my_accessory, text:de="Eingabe Nummer des zu erzeugenden Accessory", default=3)

            List<String> inputLines = new LinkedList<>();
            scriptContext.register(ScriptContextKeys.KEY_INPUT_LINES, inputLines);
            List<String> instructionLines = new LinkedList<>();
            scriptContext.register(ScriptContextKeys.KEY_INSTRUCTION_LINES, instructionLines);
            List<String> applicationLines = new LinkedList<>();
            scriptContext.register(ScriptContextKeys.KEY_APPLICATION_LINES, applicationLines);

            StringTokenizer tokenizer = new StringTokenizer(script, System.lineSeparator());
            while (tokenizer.hasMoreTokens()) {
                String line = tokenizer.nextToken();
                LOGGER.info("Current line: {}", line);

                if (line.startsWith(NodeScriptKeywords.KEYWORD_INPUT)) {
                    LOGGER.info("Found input line: {}", line);

                    inputLines.add(line);
                }
                else if (line.startsWith(NodeScriptKeywords.KEYWORD_INSTRUCTION)) {
                    LOGGER.info("Found instruction line: {}", line);

                    instructionLines.add(line);
                }
                else if (line.startsWith(NodeScriptKeywords.KEYWORD_DEFINE)) {
                    LOGGER.info("Found define line: {}", line);

                    defineLines.add(line);
                }
                else if (line.startsWith(NodeScriptKeywords.KEYWORD_APPLICATION)) {
                    LOGGER.info("Found application line: {}", line);

                    applicationLines.add(line);
                }
                else if (line.startsWith(NodeScriptKeywords.KEYWORD_SCRIPTVERSION)) {
                    LOGGER.info("Found scriptVersion line: {}", line);
                    String scriptVersion = line;

                    scriptContext.register(ScriptContextKeys.KEY_SCRIPT_VERSION, scriptVersion);
                }
                else if (line.startsWith(NodeScriptKeywords.KEYWORD_REQUIRE)) {
                    LOGGER.info("Found require line: {}", line);
                    String require = line;

                    NodeScriptUtils.checkMatchingNodeVidPid(selectedNode.getNode(), require);
                }
            }
        }
        catch (NodeRequirementException ex) {
            // TODO: handle exception
            LOGGER.warn("Preprocessing of script is cancelled because the node requirements are not fulfilled.", ex);

            TaskDialogs
                .build(JOptionPane.getFrameForComponent(getFeedbackComponent(scriptContext)),
                    Resources.getString(NodeScriptController.class, "prepare-script-cancelled.instruction"),
                    Resources.getString(NodeScriptController.class, "prepare-script-cancelled.node-requirements-error"))
                .title(Resources.getString(NodeScriptController.class, "prepare-script.title")).showException(ex);
            return false;
        }
        catch (Exception ex) {
            LOGGER.warn("Preprocessing of script failed.", ex);

            TaskDialogs
                .build(JOptionPane.getFrameForComponent(getFeedbackComponent(scriptContext)),
                    Resources.getString(NodeScriptController.class, "prepare-script-failed.instruction"),
                    Resources.getString(NodeScriptController.class, "prepare-script-failed.input-params-error"))
                .title(Resources.getString(NodeScriptController.class, "prepare-script.title")).showException(ex);
            return false;
        }

        return true;
    }

    private Component getFeedbackComponent(final ApplicationContext scriptContext) {
        Component feedbackComponent = scriptContext.get(ScriptContextKeys.KEY_SCRIPT_PANEL, Component.class);
        if (feedbackComponent == null) {
            feedbackComponent = nodeScriptView.getComponent();
        }
        return feedbackComponent;
    }

    /**
     * Query the input parameters.
     * 
     * @param scriptContext
     *            the script context
     * @return {@code true}: continue processing, {@code false}: abort processing
     */
    public boolean queryInputValues(final DefaultScriptContext scriptContext) {

        try {
            final VelocityContext velocityContext =
                scriptContext.get(ScriptContextKeys.KEY_VELOCITY_CONTEXT, VelocityContext.class);
            final List<String> inputLines = scriptContext.get(ScriptContextKeys.KEY_INPUT_LINES, List.class);
            final List<String> defineLines = scriptContext.get(ScriptContextKeys.KEY_DEFINE_LINES, List.class);
            final List<String> instructionLines =
                scriptContext.get(ScriptContextKeys.KEY_INSTRUCTION_LINES, List.class);

            if (CollectionUtils.isNotEmpty(inputLines)) {
                // query the input parameters from the user
                nodeScriptView.queryInputValues(scriptContext, velocityContext, inputLines, instructionLines,
                    defineLines);
            }

        }
        catch (CancelledByUserException ex) {
            LOGGER.warn("Preprocessing of script was cancelled by user.");
            return false;

        }
        catch (Exception ex) {
            LOGGER.warn("Preprocessing of script failed.", ex);
            TaskDialogs
                .build(JOptionPane.getFrameForComponent(getFeedbackComponent(scriptContext)),
                    Resources.getString(NodeScriptController.class, "prepare-script-failed.instruction"),
                    Resources.getString(NodeScriptController.class, "prepare-script-failed.input-params-error"))
                .title(Resources.getString(NodeScriptController.class, "prepare-script.title")).showException(ex);
            return false;
        }
        return true;
    }

    @Override
    public void substituteVariables(StringBuilder script, final ApplicationContext scriptContext) {

        final VelocityContext velocityContext =
            scriptContext.get(ScriptContextKeys.KEY_VELOCITY_CONTEXT, VelocityContext.class);

        StringWriter output = null;
        try {
            Locale locale = Locale.getDefault();
            String lang = locale.getLanguage();
            LOGGER.info("Current locale: {}, current lang: {}", locale, lang);

            String logTag = "[velocity]";
            output = new StringWriter();

            final List<String> defineLines = scriptContext.get(ScriptContextKeys.KEY_DEFINE_LINES, List.class);

            for (String defineLine : defineLines) {
                // String in = "Weiche${my_accessory}_Spule0";
                // parse the line
                DefineLineParams params = scanDefineLine(lang, defineLine);

                Velocity.evaluate(velocityContext, output, logTag, params.caption);
                LOGGER.info("Evaluate define, variable: {}, value: {}", params.variableName, output.toString());
                velocityContext.put(params.variableName, output.toString());

                output.getBuffer().setLength(0);
            }
        }
        catch (ParseErrorException ex) {
            LOGGER.warn("Substitute variables with template engine failed because of a parse error.", ex);
            TaskDialogs
                .build(JOptionPane.getFrameForComponent(getFeedbackComponent(scriptContext)),
                    Resources.getString(NodeScriptController.class, "substitue-variable-failed.instruction"),
                    Resources.getString(NodeScriptController.class, "substitue-variable-failed.parse-error"))
                .title(Resources.getString(NodeScriptController.class, "render-script.title")).showException(ex);

            // TODO throw an exception
            return;
        }
        catch (Exception ex) {
            LOGGER.warn("Substitute variables with template engine failed.", ex);

            TaskDialogs
                .build(JOptionPane.getFrameForComponent(getFeedbackComponent(scriptContext)),
                    Resources.getString(NodeScriptController.class, "substitue-variable-failed.instruction"),
                    Resources.getString(NodeScriptController.class, "substitue-variable-failed"))
                .title(Resources.getString(NodeScriptController.class, "render-script.title")).showException(ex);

            // TODO throw an exception
            return;
        }
        finally {
            if (output != null) {
                try {
                    output.close();
                }
                catch (Exception ex1) {
                    LOGGER.warn("Close output writer failed.", ex1);
                }
            }
            output = null;
        }

        // one more preprocessing step: line comment at the end of line with text before must add a new line
        try {
            StringBuilder sb = new StringBuilder();
            String[] lines = script.toString().split("\n");

            // search for ## marker after text
            for (String line : lines) {
                int index = line.indexOf("##");
                if (index > 0) {
                    sb.append(line.substring(0, index));
                }
                else {
                    sb.append(line);
                }
                sb.append(System.lineSeparator());
            }

            LOGGER.debug("Replaced script: {}", sb);
            script.setLength(0);
            script.append(sb/* .toString() */);
        }
        catch (Exception ex) {
            LOGGER.warn("Replace trailing line comment before rendering failed.", ex);
            // TODO throw an exception
        }

        // return script;
    }

    @Override
    public String renderScript(StringBuilder script, final ApplicationContext scriptContext) {
        String rendered = null;
        StringWriter output = null;

        try {
            String logTag = "[velocity]";

            output = new StringWriter();

            final VelocityContext velocityContext =
                scriptContext.get(ScriptContextKeys.KEY_VELOCITY_CONTEXT, VelocityContext.class);
            Velocity.evaluate(velocityContext, output, logTag, script.toString());

            rendered = output.toString();

            nodeScriptModel.setPreProcessedScript(rendered);
        }
        catch (ParseErrorException ex) {
            LOGGER.warn("Render script with template engine failed because of a parse error.", ex);

            TaskDialogs
                .build(JOptionPane.getFrameForComponent(getFeedbackComponent(scriptContext)),
                    Resources.getString(NodeScriptController.class, "render-script-failed.instruction"),
                    Resources.getString(NodeScriptController.class, "render-script-failed.parse-error"))
                .title(Resources.getString(NodeScriptController.class, "render-script.title")).showException(ex);

            throw ex;
        }
        catch (MethodInvocationException ex) {

            if (ex.getCause() instanceof UserDefinedVelocityException) {
                UserDefinedVelocityException udvEx = (UserDefinedVelocityException) ex.getCause();
                LOGGER.warn("Render script with template engine failed because of a user defined error.", udvEx);
                TaskDialogs
                    .build(JOptionPane.getFrameForComponent(getFeedbackComponent(scriptContext)),
                        Resources.getString(NodeScriptController.class, "render-script-failed.instruction"),
                        udvEx.getMessage())
                    .title(Resources.getString(NodeScriptController.class, "render-script.title")).showException(udvEx);

                throw udvEx;
            }

            LOGGER.warn("Render script with template engine failed.", ex);

            TaskDialogs
                .build(JOptionPane.getFrameForComponent(getFeedbackComponent(scriptContext)),
                    Resources.getString(NodeScriptController.class, "render-script-failed.instruction"),
                    Resources.getString(NodeScriptController.class, "render-script-failed"))
                .title(Resources.getString(NodeScriptController.class, "render-script.title")).showException(ex);

            throw ex;
        }
        catch (Exception ex) {
            LOGGER.warn("Render script with template engine failed.", ex);

            TaskDialogs
                .build(JOptionPane.getFrameForComponent(getFeedbackComponent(scriptContext)),
                    Resources.getString(NodeScriptController.class, "render-script-failed.instruction"),
                    Resources.getString(NodeScriptController.class, "render-script-failed"))
                .title(Resources.getString(NodeScriptController.class, "render-script.title")).showException(ex);

            throw ex;
        }
        finally {
            if (output != null) {
                try {
                    output.close();
                }
                catch (Exception ex1) {
                    LOGGER.warn("Close output writer failed.", ex1);
                }
            }
        }

        FileOutputStream fos = null;
        try {

            String logfilePath = Preferences.getInstance().getLogFilePath();

            fos = new FileOutputStream(new File(logfilePath, "NodeScript.log"));
            IOUtils.write(rendered, fos);

            fos.flush();
        }
        catch (Exception ex) {
            LOGGER.warn("Write NodeScript.log failed.", ex);
        }
        finally {
            if (fos != null) {
                try {
                    fos.close();
                }
                catch (Exception ex1) {
                    LOGGER.warn("Close file output stream failed.", ex1);
                }
            }
        }
        return rendered;
    }

    @Override
    public void executeScript(String script, boolean previewOnly) {

        LOGGER.info("Execute the script on node.");

        if (scriptContext == null) {
            LOGGER.info("Create new ScriptContext.");
            scriptContext = new DefaultScriptContext();
        }

        boolean continueProcessing = prepareVelocityContext(script, scriptContext);
        if (!continueProcessing) {
            LOGGER.info("The processing of the script is aborted.");
            return;
        }

        continueProcessing = queryInputValues(scriptContext);
        if (!continueProcessing) {
            LOGGER.info("The processing of the script was cancelled by user.");
            return;
        }

        LOGGER.info("Substitute variables with template engine.");
        StringBuilder sbScript = new StringBuilder(script);
        substituteVariables(sbScript, scriptContext);

        LOGGER.info("Render script with template engine.");

        try {
            String rendered = renderScript(sbScript, scriptContext);

            if (!previewOnly) {
                LOGGER.info("Rendered script: {}", rendered);

                parseAndApplyScript(rendered, scriptContext);
            }
            else {
                // preview the script
                showPreview();
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Prepare rendered script failed.");

            TaskDialogs
                .build(JOptionPane.getFrameForComponent(getFeedbackComponent(scriptContext)),
                    Resources.getString(NodeScriptController.class, "execute-script-aborted.instruction"),
                    Resources.getString(NodeScriptController.class, "execute-script-aborted"))
                .title(Resources.getString(NodeScriptController.class, "execute-script.title")).error();

        }
    }

    @Override
    public void parseAndApplyScript(String renderedScript, final ApplicationContext scriptContext) {
        NodeScriptCommandList nodeScriptCommandList = new NodeScriptCommandList();
        List<ScriptCommand<NodeScripting>> nodeScriptCommands = new LinkedList<>();
        nodeScriptCommandList.setNodeScriptCommands(nodeScriptCommands);
        nodeScriptCommandList.setExecutionStatus(ExecutionStatus.pending);

        // let the parser parse the script
        try {
            ScriptParser scriptParser = new ScriptParser();

            DefaultScriptContext defaultScriptContext = (DefaultScriptContext) scriptContext;
            scriptParser.parseScript(renderedScript, nodeScriptCommands, defaultScriptContext.getRegistry());

            LOGGER.info("Prepared nodeScriptCommands: {}", nodeScriptCommands);

            // set the nodeScriptCommands in the model
            nodeScriptModel.setNodeScriptCommandList(nodeScriptCommandList, defaultScriptContext.getRegistry());
        }
        catch (Exception ex) {
            LOGGER.warn("Parse script failed.", ex);

            TaskDialogs
                .build(JOptionPane.getFrameForComponent(getFeedbackComponent(scriptContext)),
                    Resources.getString(NodeScriptController.class, "execute-script-aborted.instruction"),
                    Resources.getString(NodeScriptController.class, "execute-script-aborted"))
                .title(Resources.getString(NodeScriptController.class, "execute-script.title")).exception(ex);
        }
    }

    private void showPreview() {

        // check if the nodeScript preView is already opened
        String searchKey = NodeScriptPreView.NODE_SCRIPT_PREVIEW;
        LOGGER.info("Search for view with key: {}", searchKey);
        Dockable view = desktop.getContext().getDockableByKey(searchKey);
        if (view != null) {
            LOGGER.info("Select the existing nodeScriptPreView view.");
            DockUtils.selectWindow(view);
            return;
        }

        LOGGER.info("Create new NodeScriptPreView.");
        nodeScriptPreView = new NodeScriptPreView(nodeScriptModel);

        if (desktop.getDockables().length > 1) {
            Dockable dock = desktop.getDockables()[1].getDockable();

            desktop.createTab(dock, nodeScriptPreView, 2, true);
        }
        else {
            desktop.addDockable(nodeScriptPreView, RelativeDockablePosition.RIGHT);
        }

        desktop.addDockableStateChangeListener(new DockableStateChangeListener() {

            @Override
            public void dockableStateChanged(DockableStateChangeEvent event) {
                if (event.getNewState().getDockable().equals(nodeScriptPreView) && event.getNewState().isClosed()) {
                    LOGGER.info("NodeScriptPreView was closed, free resources.");

                }
            }
        });
    }

    protected static final class DefineLineParams {

        String variableName;

        String variableType;

        String caption;
    }

    protected static final String REGEX_INPUT = "text\\:[a-z]{2}=\"([^\"]*)\"|(\\S+)";

    protected DefineLineParams scanDefineLine(String lang, String line) {

        DefineLineParams defineLineParams = new DefineLineParams();

        // parse the line
        String caption = line.substring(line.indexOf("(") + 1, line.lastIndexOf(")"));

        Matcher m = Pattern.compile(REGEX_INPUT).matcher(caption);
        while (m.find()) {
            if (m.groupCount() > 0 && m.group(1) != null) {
                String text = m.group(0);
                LOGGER.info("1. Found text: [{}]", text);
                if (text.startsWith("text:" + lang)) {
                    defineLineParams.caption = m.group(1);
                }
            }
            else {
                String text = m.group(0);
                LOGGER.debug("Plain [{}]", text);
                text = StringUtils.strip(text, ",").trim();

                if (StringUtils.isNotBlank(text)) {
                    LOGGER.info("3. Found text: [{}]", text);
                    if (StringUtils.isBlank(defineLineParams.variableName)) {
                        if (text.indexOf(":") > -1) {
                            // type provided
                            defineLineParams.variableName = text.split(":")[0];
                            defineLineParams.variableType = text.split(":")[1];
                        }
                        else {
                            defineLineParams.variableName = text.trim();
                            defineLineParams.variableType = "string";
                        }

                        defineLineParams.variableName = StringUtils.strip(defineLineParams.variableName, "$");

                        LOGGER.info("Found variable with name: {}, type: {}", defineLineParams.variableName,
                            defineLineParams.variableType);
                    }
                }
            }
        }

        LOGGER.info("Prepared defineLineParams: {}",
            ToStringBuilder.reflectionToString(defineLineParams, ToStringStyle.SHORT_PREFIX_STYLE));
        return defineLineParams;
    }

    @Override
    public void addExecutionStatusListener(PropertyChangeListener executionStatusListener) {
        LOGGER.info("Add the execution status listener.");
        this.executionStatusListener = executionStatusListener;
    }

    @Override
    public void removeExecutionStatusListener(PropertyChangeListener executionStatusListener) {
        LOGGER.info("Remove the execution status listener.");
        this.executionStatusListener = null;
    }

}
