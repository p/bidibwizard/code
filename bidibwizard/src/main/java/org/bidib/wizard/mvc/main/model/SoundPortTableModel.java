package org.bidib.wizard.mvc.main.model;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.PortConfigKeys;
import org.bidib.wizard.comm.SoundPortStatus;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.main.model.listener.PortListener;
import org.bidib.wizard.mvc.main.model.listener.SoundPortListener;
import org.bidib.wizard.mvc.main.view.table.listener.ButtonListener;
import org.bidib.wizard.utils.PortUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SoundPortTableModel
    extends SimplePortTableModel<SoundPortStatus, SoundPort, SoundPortListener<SoundPortStatus>>
    implements ButtonListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(SoundPortTableModel.class);

    private static final long serialVersionUID = 1L;

    public static final int COLUMN_LABEL = 0;

    public static final int COLUMN_PULSE_TIME = 1;

    public static final int COLUMN_PORT_IDENTIFIER = 2;

    public static final int COLUMN_STATUS = 3;

    public static final int COLUMN_TEST = 4;

    public static final int COLUMN_PORT_INSTANCE = 5;

    public SoundPortTableModel(final MainModel model) {
        super();

        model.addSoundPortListener(new PortListener<SoundPortStatus>() {
            @Override
            public void labelChanged(Port<SoundPortStatus> port, String label) {
            }

            @Override
            public void statusChanged(Port<SoundPortStatus> port, SoundPortStatus status) {
                LOGGER.info("The port status has changed: {}, port: {}, port.status: {}", status, port,
                    port.getStatus());
                updatePortStatus(port);
            }

            @Override
            public void configChanged(Port<SoundPortStatus> port) {
            }
        });
    }

    @Override
    protected int getColumnPortInstance() {
        return COLUMN_PORT_INSTANCE;
    }

    private void updatePortStatus(Port<SoundPortStatus> port) {
        // the port status is signaled from the node
        for (int row = 0; row < getRowCount(); row++) {
            if (port.equals(getValueAt(row, COLUMN_PORT_INSTANCE))) {

                LOGGER.debug("The port state has changed: {}", port.getStatus());
                super.setValueAt(port.getStatus(), row, COLUMN_STATUS);

                // get the opposite status and set it
                SoundPortStatus oppositeStatus = PortUtils.getOppositeStatus(port.getStatus());

                LOGGER.info("Set the port status, oppositeStatus: {}", oppositeStatus);

                setValueAt(oppositeStatus, row, COLUMN_TEST);
                break;
            }
        }
    }

    @Override
    protected void initialize() {
        columnNames =
            new String[] { Resources.getString(getClass(), "label"), Resources.getString(getClass(), "pulseTime"),
                Resources.getString(getClass(), "portIdentifier"), Resources.getString(getClass(), "status"),
                Resources.getString(getClass(), "test"), null };
    }

    public void addRow(SoundPort port) {
        if (port != null) {
            Object[] rowData = new Object[columnNames.length];

            rowData[COLUMN_LABEL] = port.toString();
            rowData[COLUMN_PULSE_TIME] = port.getPulseTime();
            rowData[COLUMN_PORT_IDENTIFIER] = port.getPortIdentifier();
            rowData[COLUMN_STATUS] = port.getStatus();

            SoundPortStatus oppositeStatus = (SoundPortStatus) PortUtils.getOppositeStatus(port.getStatus());
            rowData[COLUMN_TEST] = oppositeStatus;
            rowData[COLUMN_PORT_INSTANCE] = port;
            addRow(rowData);
        }
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        boolean isEditable = false;
        SoundPort soundPort = (SoundPort) getValueAt(row, COLUMN_PORT_INSTANCE);
        switch (column) {
            case COLUMN_LABEL:
                isEditable = true;
                break;
            case COLUMN_PULSE_TIME:
                if (soundPort.isEnabled() && soundPort.isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_TICKS)) {
                    isEditable = true;
                }
                break;
            case COLUMN_STATUS:
                // the status can never be changed.
                isEditable = false;
            case COLUMN_TEST:
                if (soundPort.isEnabled()) {
                    isEditable = true;
                }
                break;
            default:
                break;
        }
        return isEditable;
    }

    @Override
    public Class<?> getColumnClass(int column) {
        switch (column) {
            case COLUMN_LABEL:
                return String.class;
            case COLUMN_PORT_INSTANCE:
            case COLUMN_PORT_IDENTIFIER:
                return Object.class;
            case COLUMN_STATUS:
                return Object.class;
            case COLUMN_TEST:
                return Object.class;

            default:
                return Object.class;
        }
    }

    @Override
    public void setValueAt(Object value, int row, int column) {
        final Object o = getValueAt(row, COLUMN_PORT_INSTANCE);

        if (o instanceof SoundPort) {
            final SoundPort port = (SoundPort) o;

            switch (column) {
                case COLUMN_LABEL:
                    // if (value instanceof String) {
                    port.setLabel((String) value);
                    super.setValueAt(port.toString(), row, column);
                    fireLabelChanged(port, port.getLabel());
                    // }
                    // else {
                    // super.setValueAt(value, row, column);
                    // }
                    break;
                case COLUMN_PULSE_TIME:
                    int pulseTime = (Integer) value;
                    if (port.getPulseTime() != pulseTime) {
                        port.setPulseTime(pulseTime);
                        super.setValueAt(value, row, column);
                        fireValuesChanged(port, PortConfigKeys.BIDIB_PCFG_TICKS);
                    }
                    else {
                        LOGGER.debug("The pulse time has not been changed.");
                    }
                    break;
                case COLUMN_STATUS:
                    port.setStatus((SoundPortStatus) value);
                    super.setValueAt(value, row, column);
                    break;
                case COLUMN_TEST:
                    LOGGER.debug("Status of sound port is updated: {}, port: {}", value, port);
                    if (value instanceof SoundPortStatus) {
                        SoundPortStatus currentStatus = (SoundPortStatus) value;

                        port.setStatus(currentStatus);
                        super.setValueAt(currentStatus, row, column);
                    }
                    else {
                        LOGGER.warn("Set an invalid value: {}", value);
                        super.setValueAt(value, row, column);
                    }
                    break;
                default:
                    super.setValueAt(value, row, column);
                    break;
            }
        }
        else {
            super.setValueAt(value, row, column);
        }
    }

    @Override
    public Object getValueAt(int row, int column) {
        switch (column) {
            case COLUMN_PORT_IDENTIFIER:
            case COLUMN_LABEL:
            case COLUMN_PULSE_TIME:
            case COLUMN_TEST:
                column = COLUMN_PORT_INSTANCE;
                break;
            default:
                break;
        }
        return super.getValueAt(row, column);
    }

    private void fireValuesChanged(SoundPort port, PortConfigKeys... portConfigKeys) {

        LOGGER.info("The values of the port have changed: {}", port);
        for (SoundPortListener<SoundPortStatus> l : portListeners) {
            l.valuesChanged(port, portConfigKeys);
        }
    }

    @Override
    public void buttonPressed(int row, int column) {
        LOGGER.info("The button was pressed, row: {}, column: {}", row, column);

        // be careful: if we check the column we must evaluate if the SPORT configuration columns are displayed or not
        // ...
        final Object portInstance = getValueAt(row, COLUMN_PORT_INSTANCE);
        if (portInstance instanceof SoundPort) {
            final SoundPort port = (SoundPort) portInstance;
            fireTestButtonPressed(port);
        }
        else {
            LOGGER.warn("The current portInstance is not a SoundPort: {}", portInstance);
        }
    }

    /**
     * Change the port type.
     * 
     * @param portType
     *            the new port type
     * @param port
     *            the port
     */
    public void changePortType(LcOutputType portType, SoundPort port) {

        for (SoundPortListener<SoundPortStatus> l : portListeners) {
            l.changePortType(portType, port);
        }
    }
}
