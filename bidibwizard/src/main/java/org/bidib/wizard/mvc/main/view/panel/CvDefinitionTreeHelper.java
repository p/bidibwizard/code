package org.bidib.wizard.mvc.main.view.panel;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.lang.StringUtils;
import org.bidib.jbidibc.core.node.ConfigurationVariable;
import org.bidib.jbidibc.exchange.vendorcv.CVType;
import org.bidib.jbidibc.exchange.vendorcv.DataType;
import org.bidib.jbidibc.exchange.vendorcv.DescriptionType;
import org.bidib.jbidibc.exchange.vendorcv.ModeType;
import org.bidib.jbidibc.exchange.vendorcv.NodeType;
import org.bidib.jbidibc.exchange.vendorcv.NodetextType;
import org.bidib.jbidibc.exchange.vendorcv.TemplateType;
import org.bidib.jbidibc.exchange.vendorcv.VendorCVUtils;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.view.cvdef.CvNode;
import org.bidib.wizard.mvc.main.view.cvdef.GBM16TReverserCvNode;
import org.bidib.wizard.mvc.main.view.cvdef.LongCvNode;
import org.bidib.wizard.mvc.main.view.cvdef.LongNodeNode;
import org.bidib.wizard.mvc.main.view.cvdef.NodeNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jidesoft.grid.DefaultExpandableRow;
import com.jidesoft.grid.TreeTableModel;

public class CvDefinitionTreeHelper {
    private static final Logger LOGGER = LoggerFactory.getLogger(CvDefinitionTreeHelper.class);

    public List<ConfigurationVariable> addSubNodes(
        DefaultExpandableRow parentNode, List<NodeType> nodeTypes, TreeTableModel<DefaultExpandableRow> treeModel,
        Map<String, TemplateType> templatesMap, Integer parentOffset, Integer parentStep, boolean skipOnTimeout,
        String lang, Map<String, CvNode> cvNumberToNodeMap) {

        LOGGER.debug("Add subnodes, parentOffset: {}, parentStep: {}", parentOffset, parentStep);

        List<ConfigurationVariable> configVariables = new LinkedList<ConfigurationVariable>();

        if (nodeTypes != null) {

            for (NodeType nodeType : nodeTypes) {
                LOGGER.trace("Add new node: {}", nodeType);

                String templateId = nodeType.getTemplate();
                LOGGER.debug("Current templateId: {}", templateId);

                NodeNode node = null;

                if (StringUtils.isNotBlank(templateId)) {
                    // process the template
                    TemplateType template = templatesMap.get(templateId);
                    LOGGER.trace("Fetched template: {}", template);

                    Integer count = nodeType.getCount();
                    Integer step = nodeType.getNext();
                    Integer offset = nodeType.getOffset();
                    LOGGER.trace("The parameters of the current template, count: {}, step: {}, offset: {}", count, step,
                        offset);

                    if (count == null) {
                        count = Integer.valueOf(1);
                    }

                    if (offset == null) {
                        offset = (parentOffset != null ? parentOffset : Integer.valueOf(0));
                    }
                    else {
                        offset += (parentOffset != null ? parentOffset : Integer.valueOf(0));
                    }

                    if (step == null) {
                        step = (parentStep != null ? parentStep : Integer.valueOf(1));
                    }
                    else {
                        step += (parentStep != null ? parentStep : Integer.valueOf(0));
                    }

                    for (int index = 0; index < count; index++) {
                        LOGGER.trace("Create node with index: {}, node: {}", index, nodeType);
                        // create the node with a clone of the nodeType
                        NodeType clone =
                            new NodeType()
                                .withNodetext(VendorCVUtils.getNodetextOfLanguage(nodeType, lang))
                                .withNodeOrCVOrRepeater(nodeType.getNodeOrCVOrRepeater()).withCount(nodeType.getCount())
                                .withNext(nodeType.getNext()).withOffset(nodeType.getOffset());

                        node = new NodeNode(clone, index, lang);

                        treeModel.addRow(parentNode, node);

                        // apply template
                        if (template != null) {
                            // this is the support for nodes in template
                            if (!CollectionUtils.isEmpty(template.getCVOrNodeOrRepeater())) {
                                LOGGER.debug("Add nodes of template, offset: {}, count: {}, step: {}, index: {}",
                                    offset, count, step, index);

                                Boolean skipOnTimeoutOnTemplate = template.isSkipOnTimeout();
                                if (skipOnTimeoutOnTemplate == null) {
                                    skipOnTimeoutOnTemplate = Boolean.FALSE;
                                }
                                else {
                                    LOGGER.debug(
                                        "The skipOnTimeout flag is set, skipOnTimeoutOnTemplate: {}, offset: {}, step: {}",
                                        skipOnTimeoutOnTemplate, offset, step);
                                }

                                List<Object> items = template.getCVOrNodeOrRepeater();
                                List<NodeType> nodes = new LinkedList<>();
                                for (Object item : items) {
                                    if (item instanceof NodeType) {
                                        nodes.add((NodeType) item);
                                    }
                                }

                                if (!CollectionUtils.isEmpty(nodes)) {
                                    List<ConfigurationVariable> subConfigVariables =
                                        addSubNodes(node, nodes, treeModel, templatesMap, offset * (index + 1), step,
                                            skipOnTimeoutOnTemplate, lang, cvNumberToNodeMap);
                                    configVariables.addAll(subConfigVariables);
                                }
                            }

                            if (!CollectionUtils.isEmpty(template.getCVOrNodeOrRepeater())) {
                                boolean firstCv = true;
                                GBM16TReverserCvNode masterNode = null;

                                NodeNode savedNode = node;

                                // prepare the cv types
                                List<CVType> cvTypes = VendorCVUtils.getCVs(template);

                                if (!CollectionUtils.isEmpty(cvTypes)) {
                                    for (CVType cv : cvTypes) {

                                        // ----------------------
                                        if (!ModeType.H.equals(cv.getMode())) {
                                            // add the CVs defined by the template

                                            CVType cvClone = null;

                                            // LONG needs special processing
                                            if (DataType.LONG.equals(cv.getType())) {

                                                LOGGER.trace("Start processing for LONG type, cvNumber: {}",
                                                    cv.getNumber());

                                                NodeType longNodeType = new NodeType();

                                                for (DescriptionType dt : cv.getDescription()) {

                                                    NodetextType nodetext = new NodetextType();
                                                    nodetext.setLang(dt.getLang());
                                                    nodetext.setText(dt.getText());
                                                    longNodeType.getNodetext().add(nodetext);
                                                }

                                                LongNodeNode longNode = new LongNodeNode(longNodeType, index, lang);
                                                node = longNode;
                                                treeModel.addRow(savedNode, node);

                                                CvNode cvNode = null;

                                                int indexOffset = (index * step);
                                                String keyword = cv.getKeyword();

                                                // Create 4 CV values
                                                LongCvNode masterNodeLong = null;
                                                for (int indexLong = 0; indexLong < 4; indexLong++) {

                                                    cvClone = prepareCV(cv, indexLong + indexOffset, offset, 1);

                                                    ConfigurationVariable configVar =
                                                        prepareConfigVar(configVariables, cvClone);

                                                    if (firstCv) {
                                                        // set some extra info
                                                        configVar.setSkipOnTimeout(skipOnTimeout);
                                                        configVar.setMinCvNumber((index * step) + offset);
                                                        configVar.setMaxCvNumber(((index + 1) * step) + offset - 1);
                                                        firstCv = false;
                                                    }
                                                    // add to list of CVs of this node
                                                    configVariables.add(configVar);

                                                    LongCvNode newNode = new LongCvNode(cvClone, configVar);

                                                    if (masterNodeLong == null) {
                                                        masterNodeLong = (LongCvNode) newNode;

                                                        // prepare the keyword for the master
                                                        cvClone = VendorCVUtils.processKeyword(cvClone, keyword, index);
                                                    }
                                                    else {
                                                        masterNodeLong.addSlaveNode(newNode);
                                                        newNode.setMasterNode(masterNodeLong);
                                                        // clear the keyword because we only want the keyword on the
                                                        // master
                                                        cvClone.setKeyword(null);
                                                    }
                                                    cvNode = newNode;

                                                    // store new node in map
                                                    cvNumberToNodeMap.put(cvClone.getNumber(), cvNode);
                                                    LOGGER.trace("Add new CV node: {}", cvNode);
                                                    treeModel.addRow(node, cvNode);
                                                }

                                                // set the master node as reference
                                                longNode.setSubCvMasterNode(masterNodeLong);

                                                // restore the current node
                                                node = savedNode;
                                            }
                                            else {
                                                cvClone = prepareCV(cv, index, offset, step);
                                                ConfigurationVariable configVar =
                                                    prepareConfigVar(configVariables, cvClone);

                                                if (firstCv) {
                                                    // set some extra info
                                                    configVar.setSkipOnTimeout(skipOnTimeout);
                                                    configVar.setMinCvNumber((index * step) + offset);
                                                    configVar.setMaxCvNumber(((index + 1) * step) + offset - 1);
                                                    firstCv = false;
                                                }
                                                // add to list of CVs of this node
                                                configVariables.add(configVar);

                                                CvNode cvNode = null;
                                                // gbm16t reverser needs special processing
                                                if (DataType.GBM_16_T_REVERSER.equals(cvClone.getType())) {
                                                    GBM16TReverserCvNode newNode =
                                                        new GBM16TReverserCvNode(cvClone, configVar);
                                                    if (masterNode == null) {
                                                        masterNode = (GBM16TReverserCvNode) newNode;
                                                    }
                                                    else {
                                                        masterNode.addSlaveNode(newNode);
                                                        newNode.setMasterNode(masterNode);
                                                    }
                                                    cvNode = newNode;
                                                }
                                                else {
                                                    cvNode = new CvNode(cvClone, configVar);
                                                    // current node is not a GBM16TReverserCvNode -> reset the master
                                                    // node reference
                                                    masterNode = null;
                                                }
                                                // store new node in map
                                                cvNumberToNodeMap.put(cvClone.getNumber(), cvNode);
                                                LOGGER.trace("Add new CV node: {}", cvNode);
                                                treeModel.addRow(node, cvNode);
                                            }
                                        }
                                        else {
                                            LOGGER.debug("Current CV is hidden.");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    LOGGER.debug("Current node has no template assigned.");
                    node = new NodeNode(nodeType, 0, lang);
                    treeModel.addRow(parentNode, node);
                }

                // check if the new node has sub elements
                if (node != null) {
                    LOGGER.trace("Check if we must add sub elements.");
                    if (nodeType.withNodeOrCVOrRepeater() != null) {

                        NodeNode savedNode = node;

                        List<CVType> cvTypes = VendorCVUtils.getCVs(nodeType);
                        for (CVType cv : cvTypes) {

                            if (!ModeType.H.equals(cv.getMode())) {
                                CvNode cvNode = null;
                                CVType cvClone = null;

                                Integer offset = nodeType.getOffset();
                                if (offset == null) {
                                    offset = (parentOffset != null ? parentOffset : Integer.valueOf(0));
                                }
                                else {
                                    offset += (parentOffset != null ? parentOffset : Integer.valueOf(0));
                                }

                                // LONG needs special processing
                                if (DataType.LONG.equals(cv.getType())) {

                                    LOGGER.info("Start processing for LONG type, cvNumber: {}", cv.getNumber());

                                    NodeType longNodeType = new NodeType();

                                    for (DescriptionType dt : cv.getDescription()) {

                                        NodetextType nodetext = new NodetextType();
                                        nodetext.setLang(dt.getLang());
                                        nodetext.setText(dt.getText());
                                        longNodeType.getNodetext().add(nodetext);
                                    }

                                    LongNodeNode longNode = new LongNodeNode(longNodeType, 0, lang);
                                    node = longNode;
                                    treeModel.addRow(savedNode, node);

                                    // Create 4 CV values
                                    LongCvNode masterNode = null;
                                    for (int index = 0; index < 4; index++) {

                                        cvClone = prepareCV(cv, index, offset, 1);

                                        ConfigurationVariable configVar = prepareConfigVar(configVariables, cvClone);
                                        // add to list of CVs of this node
                                        configVariables.add(configVar);

                                        LongCvNode newNode = new LongCvNode(cvClone, configVar);

                                        if (masterNode == null) {
                                            masterNode = (LongCvNode) newNode;
                                        }
                                        else {
                                            masterNode.addSlaveNode(newNode);
                                            newNode.setMasterNode(masterNode);
                                        }
                                        cvNode = newNode;

                                        // store new node in map
                                        cvNumberToNodeMap.put(cvClone.getNumber(), cvNode);
                                        LOGGER.trace("Add new CV node: {}", cvNode);

                                        treeModel.addRow(node, cvNode);
                                    }
                                    // set the master node as reference
                                    longNode.setSubCvMasterNode(masterNode);

                                    cvNode = null;
                                    cvClone = null;

                                    node = savedNode;
                                }
                                else {
                                    cvClone = prepareCV(cv, 0, offset, 1);
                                    ConfigurationVariable configVar = prepareConfigVar(configVariables, cvClone);
                                    // add to list of CVs of this node
                                    configVariables.add(configVar);

                                    cvNode = new CvNode(cvClone, configVar);

                                    // store new node in map
                                    cvNumberToNodeMap.put(cvClone.getNumber(), cvNode);
                                    LOGGER.trace("Add new CV node: {}", cvNode);

                                    treeModel.addRow(node, cvNode);
                                }
                            }
                            else {
                                LOGGER.debug("Current CV is hidden.");
                            }
                        }
                    }

                    if (VendorCVUtils.getSubNodes(nodeType) != null) {
                        List<ConfigurationVariable> subConfigVariables =
                            addSubNodes(node, VendorCVUtils.getSubNodes(nodeType), treeModel, templatesMap,
                                parentOffset, parentStep, skipOnTimeout, lang, cvNumberToNodeMap);
                        configVariables.addAll(subConfigVariables);
                    }
                    else {
                        LOGGER.debug("Node has no subnodes.");
                    }
                }

            }
        }
        else {
            LOGGER.debug("No sub nodes available.");
        }

        return configVariables;
    }

    private CVType prepareCV(CVType cv, int index, int offset, int step) {
        LOGGER.trace("Create new CV, index: {}, offset: {}, step: {}", index, offset, step);
        // correct the high and low CV
        String highCv = cv.getHigh();
        if (StringUtils.isNotBlank(highCv) && StringUtils.isNumeric(highCv)) {
            int highCvVal = Integer.parseInt(highCv);
            highCvVal += (index * step) + offset;
            highCv = String.valueOf(highCvVal);
        }
        else {
            highCv = null;
        }
        String lowCv = cv.getLow();
        if (StringUtils.isNotBlank(lowCv) && StringUtils.isNumeric(lowCv)) {
            int lowCvVal = Integer.parseInt(lowCv);
            lowCvVal += (index * step) + offset;
            lowCv = String.valueOf(lowCvVal);
        }
        else {
            lowCv = null;
        }

        // TODO Use VendorCvUtils.prepareCVClone() ?

        CVType clone = null;
        if (StringUtils.isNumeric(cv.getNumber())) {
            int cvNumber = Integer.parseInt(cv.getNumber());
            clone =
                new CVType()
                    .withNumber(String.valueOf(cvNumber + (index * step) + offset)).withDescription(cv.getDescription())
                    .withHigh(highCv).withLow(lowCv).withMin(cv.getMin()).withMax(cv.getMax())
                    .withValues(cv.getValues()).withMode(cv.getMode()).withType(cv.getType())
                    .withBitdescription(cv.getBitdescription()).withRebootneeded(cv.isRebootneeded())
                    .withLowbits(cv.getLowbits()).withHighbits(cv.getHighbits()).withRadiobits(cv.getRadiobits())
                    .withRadiovalues(cv.getRadiovalues()).withRadioGroups(cv.getRadioGroups()).withRegex(cv.getRegex());
        }
        else {
            clone =
                new CVType()
                    .withNumber(cv.getNumber()).withDescription(cv.getDescription()).withHigh(highCv).withLow(lowCv)
                    .withMin(cv.getMin()).withMax(cv.getMax()).withValues(cv.getValues()).withMode(cv.getMode())
                    .withType(cv.getType()).withBitdescription(cv.getBitdescription())
                    .withRebootneeded(cv.isRebootneeded()).withLowbits(cv.getLowbits()).withHighbits(cv.getHighbits())
                    .withRadiobits(cv.getRadiobits()).withRadiovalues(cv.getRadiovalues())
                    .withRadioGroups(cv.getRadioGroups()).withRegex(cv.getRegex());
        }

        clone = VendorCVUtils.processKeyword(clone, cv.getKeyword(), index);

        return clone;
    }

    private ConfigurationVariable prepareConfigVar(final List<ConfigurationVariable> configVariables, CVType cv) {

        // TODO we must keep a list of the configuration variables and if a ConfigurationVariable with the same name is
        // already in the list must return this instance instead of create a new one
        ConfigurationVariable configVar = IterableUtils.find(configVariables, new Predicate<ConfigurationVariable>() {

            @Override
            public boolean evaluate(ConfigurationVariable other) {
                if (other.getName().equals(cv.getNumber())) {
                    LOGGER.info("Found existing configuration variable: {}", other);
                    return true;
                }
                return false;
            }
        });

        if (configVar == null) {
            configVar = new ConfigurationVariable(cv.getNumber(), null /* "?" */);
        }
        return configVar;
    }

    public Map<String, ConfigurationVariable> getNodes(Node node, String nodeIdentifier) {
        return getNodes(node, nodeIdentifier, 0, false, null);
    }

    public Map<String, ConfigurationVariable> getNodes(
        Node node, String nodeIdentifier, int keyIndex, boolean useStartsWith, DefaultExpandableRow parent) {
        LOGGER.debug("Get the nodes with identifier: {}", nodeIdentifier);

        if (parent == null) {
            parent = (DefaultExpandableRow) node.getCvDefinitionTreeTableModel().getRoot();
        }
        Map<String, ConfigurationVariable> cvMap = new LinkedHashMap<String, ConfigurationVariable>();

        // check if we must search for a subnode
        String[] searchPath = nodeIdentifier.split("\\\\");
        LOGGER.info("Current searchPath: {}", searchPath[0]);

        if (searchPath.length > 1) {
            LOGGER.info("Search for subnode.");
        }

        int childCount = parent.getChildrenCount();
        for (int index = 0; index < childCount; index++) {
            DefaultExpandableRow treeNode = (DefaultExpandableRow) parent.getChildAt(index);
            LOGGER.trace("Current node: {}", treeNode);
            if (treeNode instanceof NodeNode) {
                String id = (String) treeNode.getValueAt(keyIndex);
                if (useStartsWith && StringUtils.isNotBlank(id) && id.startsWith(searchPath[0])) {
                    LOGGER.debug("found treeNode: {}", treeNode);

                    if (searchPath.length > 1) {
                        LOGGER.info("Fetch map from subpath.");
                        cvMap =
                            getNodes(node, nodeIdentifier.substring(searchPath[0].length() + 1), keyIndex,
                                useStartsWith, treeNode);
                    }
                    else {
                        getCv(treeNode, cvMap);
                    }
                    return cvMap;
                }
                else if (searchPath[0].equals(id)) {
                    LOGGER.debug("found treeNode: {}", treeNode);

                    if (searchPath.length > 1) {
                        LOGGER.info("Fetch map from subpath.");
                        cvMap =
                            getNodes(node, nodeIdentifier.substring(searchPath[0].length() + 1), keyIndex,
                                useStartsWith, treeNode);
                    }
                    else {
                        getCv(treeNode, cvMap);
                    }
                    return cvMap;
                }

            }
        }
        return null;
    }

    private void getCv(DefaultExpandableRow parent, Map<String, ConfigurationVariable> cvMap) {
        int childCount = parent.getChildrenCount();
        for (int index = 0; index < childCount; index++) {
            DefaultExpandableRow node = (DefaultExpandableRow) parent.getChildAt(index);

            if (node.getChildAt(0) instanceof CvNode) {
                String nodeDescription = (String) node.getValueAt(0);

                ConfigurationVariable var = (ConfigurationVariable) ((CvNode) node.getChildAt(0)).getConfigVar();

                cvMap.put(nodeDescription, var);
            }
        }
    }

    public static boolean hasPendingChanges(TreeTableModel<DefaultExpandableRow> treeModel) {
        if (treeModel != null) {
            DefaultExpandableRow root = (DefaultExpandableRow) treeModel.getRoot();
            try {
                findPendingChanges(root);
            }
            catch (PendingChangesException ex) {
                LOGGER.warn("Pending CV defintion values detected.");
                return true;
            }
        }
        return false;
    }

    private static void findPendingChanges(DefaultExpandableRow node) {
        if (node == null) {
            return;
        }
        for (int childIndex = 0; childIndex < node.getChildrenCount(); childIndex++) {
            DefaultExpandableRow child = (DefaultExpandableRow) node.getChildAt(childIndex);
            if (child instanceof CvNode) {
                CvNode cvNode = (CvNode) child;
                if (cvNode.getNewValue() != null) {
                    throw new PendingChangesException();
                }
            }
            else {
                findPendingChanges(child);
            }
        }
    }

    public static class PendingChangesException extends RuntimeException {
        private static final long serialVersionUID = 1L;
    }

}
