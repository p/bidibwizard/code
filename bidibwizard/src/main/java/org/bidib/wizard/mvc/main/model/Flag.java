package org.bidib.wizard.mvc.main.model;

import org.apache.commons.lang.StringUtils;
import org.bidib.wizard.common.locale.Resources;

public class Flag implements IdAware {

    private int id;

    private String label;

    public Flag() {
    }

    public Flag(int id) {
        this(id, null);
    }

    public Flag(int id, String label) {
        this.id = id;
        this.label = label;
    }

    @Override
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Flag) {
            Flag other = (Flag) obj;
            if (id == other.id) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        return id;
    }

    public String toString() {
        String result = null;

        if (!StringUtils.isBlank(label)) {
            result = label;
        }
        else {
            result = Resources.getString(getClass(), "label") + "_" + id;
        }
        return result;
    }
}
