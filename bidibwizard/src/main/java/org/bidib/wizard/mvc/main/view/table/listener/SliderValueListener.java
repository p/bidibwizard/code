package org.bidib.wizard.mvc.main.view.table.listener;

public interface SliderValueListener<P> {
    /**
     * The slider value for the specified port has changed.
     * 
     * @param port
     *            the port
     */
    void sliderValueChanged(P port);
}
