package org.bidib.wizard.mvc.locolist.view;

import org.bidib.jbidibc.core.DriveState;
import org.bidib.wizard.comm.Direction;
import org.bidib.wizard.comm.SpeedSteps;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.locolist.model.LocoModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.adapter.AbstractTableAdapter;
import com.jgoodies.binding.list.SelectionInList;

public class LocoTableTableModel extends AbstractTableAdapter<LocoModel> {
    private static final Logger LOGGER = LoggerFactory.getLogger(LocoTableTableModel.class);

    private static final long serialVersionUID = 1L;

    public static final int COLUMN_ADDRESS = 0;

    public static final int COLUMN_SPEED = 1;

    public static final int COLUMN_DIRECTION = 2;

    public static final int COLUMN_SPEEDSTEPS = 3;

    public static final int COLUMN_FUNCTIONS_LIGHT = 4;

    public static final int COLUMN_FUNCTIONS_1_TO_4 = 5;

    public static final int COLUMN_FUNCTIONS_5_TO_12 = 6;

    public static final int COLUMN_FUNCTIONS_13_TO_20 = 7;

    public static final int COLUMN_FUNCTIONS_21_TO_28 = 8;

    public static final int COLUMN_PORT_INSTANCE = 9;

    private static final String[] COLUMNNAMES =
        new String[] { Resources.getString(LocoTableTableModel.class, "address"),
            Resources.getString(LocoTableTableModel.class, "speed"),
            Resources.getString(LocoTableTableModel.class, "direction"),
            Resources.getString(LocoTableTableModel.class, "speedSteps"),
            Resources.getString(LocoTableTableModel.class, "functionsLight"),
            Resources.getString(LocoTableTableModel.class, "functions1To4"),
            Resources.getString(LocoTableTableModel.class, "functions5To12"),
            Resources.getString(LocoTableTableModel.class, "functions13To20"),
            Resources.getString(LocoTableTableModel.class, "functions21To28") };

    public LocoTableTableModel(SelectionInList<LocoModel> boosterList) {
        super(boosterList, COLUMNNAMES);

        LOGGER.info("Current listModel: {}", getListModel());
    }

    @Override
    public Class<?> getColumnClass(int column) {
        switch (column) {
            case COLUMN_ADDRESS:
                return Integer.class;
            case COLUMN_SPEED:
                return Integer.class;
            case COLUMN_DIRECTION:
                // return Direction.class;
                return Boolean.class;
            case COLUMN_SPEEDSTEPS:
                return SpeedSteps.class;
            case COLUMN_FUNCTIONS_LIGHT:
            case COLUMN_FUNCTIONS_1_TO_4:
            case COLUMN_FUNCTIONS_5_TO_12:
            case COLUMN_FUNCTIONS_13_TO_20:
            case COLUMN_FUNCTIONS_21_TO_28:
                return Integer.class;
            default:
                return Object.class;
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        LocoModel loco = (LocoModel) getRow(rowIndex);
        switch (columnIndex) {
            case COLUMN_ADDRESS:
                return loco.getLocoAddress();
            case COLUMN_SPEED:
                return loco.getSpeed() * (loco.getDirection() == Direction.FORWARD ? 1 : -1);
            case COLUMN_DIRECTION:
                // return loco.getDirection();
                return (loco.getDirection() == Direction.FORWARD ? Boolean.TRUE : Boolean.FALSE);
            case COLUMN_SPEEDSTEPS:
                return loco.getSpeedSteps();
            case COLUMN_FUNCTIONS_LIGHT:
                return Integer.valueOf(loco.getFunctions(DriveState.FUNCTIONS_INDEX_F0_F4) & 0x10);
            case COLUMN_FUNCTIONS_1_TO_4:
                return Integer.valueOf(loco.getFunctions(DriveState.FUNCTIONS_INDEX_F0_F4) & 0x0F);
            case COLUMN_FUNCTIONS_5_TO_12:
                return Integer.valueOf(loco.getFunctions(DriveState.FUNCTIONS_INDEX_F5_F12) & 0xFF);
            case COLUMN_FUNCTIONS_13_TO_20:
                return Integer.valueOf(loco.getFunctions(DriveState.FUNCTIONS_INDEX_F13_F20) & 0xFF);
            case COLUMN_FUNCTIONS_21_TO_28:
                return Integer.valueOf(loco.getFunctions(DriveState.FUNCTIONS_INDEX_F21_F28) & 0xFF);
            default:
                return null;
        }
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }
}
