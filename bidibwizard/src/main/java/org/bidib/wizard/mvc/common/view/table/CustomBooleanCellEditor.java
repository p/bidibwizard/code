package org.bidib.wizard.mvc.common.view.table;

import java.awt.Component;

import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

public class CustomBooleanCellEditor extends AbstractCellEditor implements TableCellEditor {
    private static final long serialVersionUID = 1L;

    private CustomCheckBox editor;

    public CustomBooleanCellEditor(String pathSelectedIcon, String pathUnselectedIcon) {
        editor = new CustomCheckBox(pathSelectedIcon, pathUnselectedIcon);
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        if (value instanceof Boolean) {
            boolean selected = (boolean) value;
            editor.setSelected(!selected);
        }
        if (isSelected) {
            editor.setForeground(table.getSelectionForeground());
            editor.setBackground(table.getSelectionBackground());
        }
        else {
            editor.setForeground(table.getForeground());
            editor.setBackground(table.getBackground());
        }
        return editor;
    }

    @Override
    public Object getCellEditorValue() {
        return editor.isSelected();
    }
}
