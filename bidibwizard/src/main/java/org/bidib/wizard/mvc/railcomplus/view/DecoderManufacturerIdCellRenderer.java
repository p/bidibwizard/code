package org.bidib.wizard.mvc.railcomplus.view;

import java.awt.Component;
import java.util.List;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import org.apache.commons.lang3.StringUtils;
import org.bidib.jbidibc.core.schema.DecoderVendorFactory;
import org.bidib.jbidibc.core.schema.decodervendor.VendorType;

public class DecoderManufacturerIdCellRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = 1L;

    private List<VendorType> vendors;

    public DecoderManufacturerIdCellRenderer(final List<VendorType> vendors) {
        this.vendors = vendors;
    }

    public Component getTableCellRendererComponent(
        JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        if (value instanceof Integer) {
            int vendorId = ((Integer) value).intValue();

            String vendorName = DecoderVendorFactory.getDecoderVendorName(vendors, vendorId);
            if (StringUtils.isNotBlank(vendorName)) {
                setText(String.format("%s (%2d : 0x%02X)", vendorName, vendorId, vendorId));
            }
            else {
                setText(String.format("%2d (0x%02X)", vendorId, vendorId));
            }
        }
        else {
            setText(null);
        }

        return this;
    }
}