package org.bidib.wizard.mvc.main.view.table;

import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.table.TableModel;

import org.bidib.wizard.dialog.LabelDialog;
import org.bidib.wizard.mvc.main.model.Port;
import org.bidib.wizard.mvc.main.view.menu.PortListMenu;
import org.bidib.wizard.mvc.main.view.menu.listener.PortListMenuListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class PortTable extends AbstractEmptyTable {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(PortTable.class);

    private final PortListMenu menu;

    private boolean isPortMappingVisible = true;

    public PortTable(TableModel tableModel, String emptyTableText) {
        super(tableModel, emptyTableText);

        menu = createMenu();

        addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                handleMouseEvent(e, menu);
            }

            public void mouseReleased(MouseEvent e) {
                handleMouseEvent(e, menu);
            }
        });

        addMenuListener(menu);

        // set the correct row height
        org.bidib.wizard.mvc.common.view.slider.SliderRenderer sliderEditor =
            new org.bidib.wizard.mvc.common.view.slider.SliderRenderer(0, 255, 10);
        // sliderEditor.createComponent(0);

        int rowHeight =
            sliderEditor.getTableCellRendererComponent(this, 1, false, false, 0, 0).getPreferredSize().height + 4;
        LOGGER.info("Set row height: {}", rowHeight);

        setRowHeight(rowHeight);
    }

    protected PortListMenu createMenu() {
        // create the port list menu
        return new PortListMenu();
    }

    protected PortListMenuListener createMenuListener() {
        // create the port list menu
        return new DefaultPortListMenuListener() {
            @Override
            public void editLabel() {
                final int row = getRow(popupEvent.getPoint());
                if (row > -1) {
                    Object val = getValueAt(row, 0);
                    if (val instanceof Port<?>) {
                        val = ((Port<?>) val).toString();
                    }
                    final Object value = val;
                    if (value instanceof String) {
                        // show the port name editor
                        new LabelDialog((String) value, popupEvent.getXOnScreen(), popupEvent.getYOnScreen()) {
                            @Override
                            public void labelChanged(String label) {
                                setValueAt(label, row, 0);
                            }
                        };
                    }
                }
                else {
                    LOGGER.warn("The row is not available!");
                }
            }
        };
    }

    protected void addMenuListener(PortListMenu portListMenu) {
        portListMenu.addMenuListener(createMenuListener());
    }

    private void handleMouseEvent(MouseEvent e, PortListMenu portListMenu) {
        if (e.isPopupTrigger() && getRowCount() > 0) {
            popupEvent = e;

            int row = getRow(e.getPoint());
            int column = getColumnFromPoint(e.getPoint());
            showPortListMenu(e, portListMenu, row, column);
        }
    }

    private int getColumnFromPoint(Point point) {
        return columnAtPoint(point);
    }

    protected boolean isInsertPortsVisible() {
        return false;
    }

    /**
     * Show the port list menu.
     * 
     * @param e
     *            the mouse event
     * @param portListMenu
     *            the portListMenu to use
     * @param row
     *            the row
     * @param column
     *            the column
     */
    protected void showPortListMenu(MouseEvent e, PortListMenu portListMenu, int row, int column) {

        // always get the value of column 0
        column = 0;
        Object value = getValueAt(row, column);
        if (row >= 0 && column >= 0 && (value instanceof Port<?> || value instanceof String)) {
            if (row >= 0 && getSelectedRowCount() == 0) {
                setRowSelectionInterval(row, row);
            }

            if (value instanceof Port<?>) {
                // if the port has mapping support enabled the activate the menu
                if (!((Port<?>) value).isEnabled()) {
                    portListMenu.setMapPortEnabled(((Port<?>) value).isRemappingEnabled(), isPortMappingVisible);
                    portListMenu.setInsertPortsEnabled(false, false);
                }
                else {
                    portListMenu.setMapPortEnabled(false, isPortMappingVisible);
                    portListMenu.setInsertPortsEnabled(isInsertPortsVisible(), isInsertPortsVisible());
                }
            }
            else {
                portListMenu.setMapPortEnabled(false, false);
                portListMenu.setInsertPortsEnabled(false, false);
            }

            grabFocus();
            portListMenu.show(e.getComponent(), e.getX(), e.getY());
        }
    }

    public abstract void clearTable();

    /**
     * @return the isPortMappingVisible
     */
    public boolean isPortMappingVisible() {
        return isPortMappingVisible;
    }

    /**
     * @param isPortMappingVisible
     *            the isPortMappingVisible to set
     */
    public void setPortMappingVisible(boolean isPortMappingVisible) {
        this.isPortMappingVisible = isPortMappingVisible;
    }
}
