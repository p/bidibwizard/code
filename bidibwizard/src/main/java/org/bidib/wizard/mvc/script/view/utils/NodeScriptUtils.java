package org.bidib.wizard.mvc.script.view.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.bidib.jbidibc.core.Node;
import org.mozilla.universalchardet.UniversalDetector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NodeScriptUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(NodeScriptUtils.class);

    /**
     * Extract the localized instruction form the instruction line.
     * 
     * @param line
     *            the instruction line
     * @return the localized instruction
     */
    public static String extractInstruction(String line) {
        // ##instruction(text:de="Konfiguration eines Lichtsignal der DB", text:en="Configuration of a light signal
        // of DB")
        Locale locale = Locale.getDefault();
        String lang = locale.getLanguage();
        String instruction = null;
        try {
            // String line = instructionLines.get(0);
            // parse the line
            String caption = line.substring(line.indexOf("(") + 1, line.lastIndexOf(")"));

            final String regex = "text\\:[a-z]{2}=\"([^\"]*)\"|(\\S+)";

            Matcher m = Pattern.compile(regex).matcher(caption);
            while (m.find()) {
                if (m.group(1) != null) {
                    LOGGER.info("Found text: [" + m.group(0) + "]");
                    String identifier = m.group(0);
                    String currentLang = identifier.trim().substring(5, 7);
                    if (lang.equalsIgnoreCase(currentLang)) {
                        instruction = identifier.trim().substring(8);
                        instruction = StringUtils.strip(instruction, "\"");
                        LOGGER.info("Found instruction: {}", instruction);

                        break;
                    }
                }
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Get instruction text failed.", ex);
        }

        return instruction;
    }

    /**
     * Extract the hyperlink form the instruction line.
     * 
     * @param line
     *            the instruction line
     * @return the hyperlink
     */
    public static String extractInstructionHyperlink(String line) {
        // ##instruction(text:de="Konfiguration eines Lichtsignal der DB", text:en="Configuration of a light signal
        // of DB", link="http://www.bidib.org")

        String hyperlink = null;
        try {
            // parse the line
            String caption = line.substring(line.indexOf("(") + 1, line.lastIndexOf(")"));

            final String regex = "link=\"([^\"]*)\"|(\\S+)";

            Matcher m = Pattern.compile(regex).matcher(caption);
            while (m.find()) {
                if (m.group(1) != null) {
                    LOGGER.info("Found hyperlink: [" + m.group(0) + "]");
                    String link = m.group(0);
                    hyperlink = link.substring(5);
                    hyperlink = StringUtils.strip(hyperlink, "\"");
                    LOGGER.info("Found hyperlink: {}", hyperlink);
                    break;
                }
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Get instruction hyperlink failed.", ex);
        }

        return hyperlink;
    }

    public static NodeRequirement extractRequire(String line) {
        // ##require(vid="", pid="")
        // String require = null;

        line = StringUtils.replaceAll(line, " ", "");

        NodeRequirement nodeRequirement = null;

        try {
            // String line = instructionLines.get(0);
            // parse the line
            String caption = line.substring(line.indexOf("(") + 1, line.lastIndexOf(")"));

            final String regex = "vid=\"([^\"]*)\",pid=\"([^\"]*)\"";

            Matcher m = Pattern.compile(regex).matcher(caption);
            while (m.find()) {
                LOGGER.info("m: {}, count: {}", m.group(), m.groupCount());
                if (m.group(0) != null) {
                    LOGGER.info("Found text: [" + m.group(0) + "]");
                    String identifier = m.group(0);

                    // vid="13", pid="125"
                    final String regexContent = "vid=\"([^\"]*)\"|pid=\"([^\"]*)\"";
                    Matcher m1 = Pattern.compile(regexContent).matcher(identifier);

                    String vid = null;
                    String pid = null;
                    int index = 0;
                    while (m1.find()) {
                        LOGGER.info("m1: {}, count: {}", m1.group(), m1.groupCount());
                        if (m1.group(0) != null) {
                            LOGGER.info("Found text: [" + m1.group(0) + "]");

                            if (index == 0) {
                                vid = m1.group(0).substring(4);
                                vid = StringUtils.strip(vid, "\"");
                            }
                            else if (index == 1) {
                                pid = m1.group(0).substring(4);
                                pid = StringUtils.strip(pid, "\"");
                            }

                            index++;
                        }
                    }

                    nodeRequirement = new NodeRequirement(vid, pid);
                }
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Get instruction text failed.", ex);
        }

        return nodeRequirement;
    }

    /**
     * @param node
     *            the node
     * @param require
     *            the require string
     */
    public static void checkMatchingNodeVidPid(final Node node, String require) {
        LOGGER.info("Check if the current node matches the provide VID/PID restrictions: {}", require);
        NodeRequirement requirement = NodeScriptUtils.extractRequire(require);

        boolean matches = requirement.matches(node);
        LOGGER.info("Requirement matches: {}", matches);

        if (!matches) {
            throw new NodeRequirementException("The required node does not match the script requirements.");
        }
    }

    /**
     * @param filePath
     *            the file path
     * @return the encoding of the file content
     */
    public static String detectFileEncoding(String filePath) {
        return detectFileEncoding(new File(filePath));
    }

    /**
     * @param file
     *            the file
     * @return the encoding of the file content
     */
    public static String detectFileEncoding(File file) {

        byte[] buf = new byte[4096];
        UniversalDetector detector = new UniversalDetector(null);

        int nread;
        try (FileInputStream fis = new FileInputStream(file)) {
            while ((nread = fis.read(buf)) > 0 && !detector.isDone()) {
                detector.handleData(buf, 0, nread);
            }
        }
        catch (FileNotFoundException ex) {
            LOGGER.warn("The file for detection of charset was not found.", ex);
        }
        catch (IOException ex) {
            LOGGER.warn("Get the content of the file for detection of charset failed.", ex);
        }

        // (3)
        detector.dataEnd();

        // (4)
        String encoding = detector.getDetectedCharset();
        if (encoding != null) {
            LOGGER.info("Detected encoding: {}", encoding);
        }
        else {
            LOGGER.info("No encoding detected. Use default ISO-8859-1.");
            encoding = "ISO-8859-1";
        }

        // (5)
        detector.reset();

        return encoding;
    }

    /**
     * @param is
     *            the inputstream
     * @return the encoding of the file content
     */
    public static String detectFileEncoding(InputStream is) {

        byte[] buf = new byte[4096];
        UniversalDetector detector = new UniversalDetector(null);

        int nread;
        try {
            while ((nread = is.read(buf)) > 0 && !detector.isDone()) {
                detector.handleData(buf, 0, nread);
            }
        }
        catch (FileNotFoundException ex) {
            LOGGER.warn("The file for detection of charset was not found.", ex);
        }
        catch (IOException ex) {
            LOGGER.warn("Get the content of the file for detection of charset failed.", ex);
        }

        // (3)
        detector.dataEnd();

        // (4)
        String encoding = detector.getDetectedCharset();
        if (encoding != null) {
            LOGGER.info("Detected encoding: {}", encoding);
        }
        else {
            LOGGER.info("No encoding detected. Use default ISO-8859-1.");
            encoding = "ISO-8859-1";
        }

        // (5)
        detector.reset();

        return encoding;
    }
}
