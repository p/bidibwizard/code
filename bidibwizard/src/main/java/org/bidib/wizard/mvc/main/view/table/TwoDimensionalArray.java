package org.bidib.wizard.mvc.main.view.table;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TwoDimensionalArray<E> {
    private static final Logger LOGGER = LoggerFactory.getLogger(TwoDimensionalArray.class);

    private final List<ArrayList<E>> array;

    public TwoDimensionalArray() {
        this(10);
    }

    public TwoDimensionalArray(int initialCapacity) {
        array = new ArrayList<ArrayList<E>>(initialCapacity);
    }

    public E get(int row, int column) {
        E result = null;
        LOGGER.debug("Get for row: {}, column: {}, size: {}", row, column, array.size());
        if (row >= 0 && row < array.size()) {
            ArrayList<E> columnArray = array.get(row);
            LOGGER.debug("Get for column: {}, size: {}", column, columnArray.size());
            resize(columnArray, column + 1);
            if (column >= 0 && column <= columnArray.size()) {
                result = columnArray.get(column);
            }
        }
        return result;
    }

    private void resize(List<?> l, int size) {
        LOGGER.debug("Resize, list.size: {}, size: {}", l.size(), size);
        // fill the list with empty items
        for (int index = 1; index <= size; index++) {
            if (l.size() < index) {
                l.add(null);
            }
        }
    }

    public void set(E value, int row, int column) {
        ArrayList<E> columnArray = null;
        LOGGER.debug("set value: {}, row: {}, column: {}", value, row, column);
        if (row >= 0 && row < array.size()) {
            columnArray = array.get(row);
        }
        else {
            columnArray = new ArrayList<E>();
            LOGGER.debug("Created new columnArray.");
            resize(array, row);
            array.add(row, columnArray);
        }
        resize(columnArray, column);
        columnArray.add(column, value);
    }

    /**
     * Clears all entries from the array.
     */
    public void clear() {
        LOGGER.debug("Clear is called.");
        // process the rows
        for (ArrayList<E> row : array) {
            // clear the columns
            if (row != null) {
                row.clear();
            }
            else {
                LOGGER.debug("Row is not available.");
            }
        }
        // clear the rows
        array.clear();
    }
}
