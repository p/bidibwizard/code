package org.bidib.wizard.mvc.dmx.view.panel;

import java.awt.Point;
import java.awt.event.ActionEvent;

import org.bidib.wizard.comm.LightPortStatus;
import org.bidib.wizard.mvc.dmx.model.DmxChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The <code>LightPortAction</code> sets the action that is performed on the assigned port.
 */
public class LightPortAction extends LocationAwareAction<DmxChannel> {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(LightPortAction.class);

    private LightPortStatus lightPortStatus;

    private DmxChannel dmxChannel;

    private DmxDataItem originalDataItem;

    public LightPortAction(LightPortStatus lightPortStatus, DmxChannel dmxChannel, DmxChartPanel dmxChartPanel,
        DmxDataItem originalDataItem) {
        super(lightPortStatus.toString(), dmxChannel, dmxChartPanel);
        this.dmxChannel = dmxChannel;
        this.lightPortStatus = lightPortStatus;
        this.originalDataItem = originalDataItem;
    }

    @Override
    public void actionPerformed(ActionEvent event) {

        Point currentMousePoint = dmxChartPanel.getCurrentMousePoint();

        String seriesKey = Integer.toString(dmxChannel.getChannelId());
        LOGGER.info("Selected key: {}, currentMousePoint: {}", seriesKey, currentMousePoint);

        if (originalDataItem != null) {
            LOGGER.info("Set action on port for item: {}, lightPortStatus: {}", originalDataItem, lightPortStatus);

            dmxChartPanel.setLightPortAction(seriesKey, originalDataItem, lightPortStatus);
        }
    }
}
