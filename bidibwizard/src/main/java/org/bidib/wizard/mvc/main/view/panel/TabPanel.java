package org.bidib.wizard.mvc.main.view.panel;

import java.awt.BorderLayout;
import java.awt.Component;
import java.io.File;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionListener;

import org.apache.commons.collections4.CollectionUtils;
import org.bidib.jbidibc.core.enumeration.IdentifyState;
import org.bidib.jbidibc.core.enumeration.SysErrorEnum;
import org.bidib.jbidibc.core.schema.BidibProductsFactory;
import org.bidib.jbidibc.core.schema.bidib.products.ProductType;
import org.bidib.jbidibc.core.utils.NodeUtils;
import org.bidib.jbidibc.core.utils.ProductUtils;
import org.bidib.wizard.comm.AnalogPortStatus;
import org.bidib.wizard.comm.ServoPortStatus;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.common.utils.ImageUtils;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.bidib.wizard.mvc.common.view.DockKeys;
import org.bidib.wizard.mvc.main.controller.AccessoryPanelController;
import org.bidib.wizard.mvc.main.controller.BoosterPanelController;
import org.bidib.wizard.mvc.main.controller.FeedbackPortPanelController;
import org.bidib.wizard.mvc.main.controller.FeedbackPositionPanelController;
import org.bidib.wizard.mvc.main.controller.FlagPanelController;
import org.bidib.wizard.mvc.main.controller.LightPortPanelController;
import org.bidib.wizard.mvc.main.controller.MacroListController;
import org.bidib.wizard.mvc.main.controller.MotorPortPanelController;
import org.bidib.wizard.mvc.main.controller.SoundPortPanelController;
import org.bidib.wizard.mvc.main.controller.SwitchPairPortPanelController;
import org.bidib.wizard.mvc.main.controller.SwitchPortPanelController;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.MotorPort;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.model.ServoPort;
import org.bidib.wizard.mvc.main.model.listener.BacklightPortListener;
import org.bidib.wizard.mvc.main.model.listener.CvDefinitionListener;
import org.bidib.wizard.mvc.main.model.listener.CvDefinitionRequestListener;
import org.bidib.wizard.mvc.main.model.listener.NodeListListener;
import org.bidib.wizard.mvc.main.model.listener.NodeListener;
import org.bidib.wizard.mvc.main.model.listener.OutputListener;
import org.bidib.wizard.mvc.main.model.listener.PortListListener;
import org.bidib.wizard.mvc.main.model.listener.ServoPortListener;
import org.bidib.wizard.mvc.main.view.panel.listener.AccessoryListListener;
import org.bidib.wizard.mvc.main.view.panel.listener.AccessoryTableListener;
import org.bidib.wizard.mvc.main.view.panel.listener.MacroListListener;
import org.bidib.wizard.mvc.main.view.panel.listener.MacroTableListener;
import org.bidib.wizard.mvc.main.view.panel.listener.TabSelectionListener;
import org.bidib.wizard.mvc.main.view.panel.listener.TabStatusListener;
import org.bidib.wizard.mvc.main.view.panel.listener.TabVisibilityListener;
import org.bidib.wizard.mvc.main.view.panel.listener.TabVisibilityProvider;
import org.bidib.wizard.mvc.main.view.panel.renderer.BidibNodeNameUtils;
import org.bidib.wizard.mvc.preferences.model.Preferences;
import org.bidib.wizard.mvc.script.view.listener.NodeLabelScriptingListener;
import org.bidib.wizard.mvc.stepcontrol.controller.StepControlController;
import org.bidib.wizard.mvc.stepcontrol.view.StepControlPanel;
import org.bidib.wizard.script.node.types.ScriptingTargetType;
import org.bidib.wizard.script.node.types.TargetType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vlsolutions.swing.docking.DockKey;
import com.vlsolutions.swing.docking.Dockable;

public class TabPanel extends JPanel
    implements NodeListener, NodeListListener, NodeLabelScriptingListener, Dockable, TabVisibilityListener {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(TabPanel.class);

    private String emptyLabel;

    private final MainModel model;

    private TitledBorder border;

    private final InfoPanel infoPanel;

    private final BasicOperationsPanel basicOperationsPanel;

    private final AccessoryListPanel accessoryListPanel;

    private final AnalogPortListPanel analogPortListPanel;

    private final BoosterPanel boosterPanel;

    private final FeedbackPortListPanel feedbackPortListPanel;

    private final FeedbackPositionListPanel feedbackPositionListPanel;

    private final InputPortListPanel inputPortListPanel;

    private final LightPortListPanel lightPortListPanel;

    private final BacklightPortListPanel backlightPortListPanel;

    private final MacroListPanel macroListPanel;

    private final MotorPortListPanel motorPortListPanel;

    private final ServoPortListPanel servoPortListPanel;

    private final SoundPortListPanel soundPortListPanel;

    private final SwitchPortListPanel switchPortListPanel;

    private final SwitchPairPortListPanel switchPairPortListPanel;

    private final FlagListPanel flagListPanel;

    private final StepControlPanel stepControlPanel;

    private final JPanel cvDefinitionPanel;

    private final JTabbedPane tabbedPane;

    private Node displayedNode;

    private final List<JPanel> tabs = new LinkedList<JPanel>();

    private final Map<ScriptingTargetType, ChangeLabelSupport> scriptingSupportMap = new LinkedHashMap<>();

    private EmptyPanel emptyPanel;

    private TabStatusListener tabStatusListener;

    private AccessoryPanelController accessoryPanelController;

    private final BoosterPanelController boosterPanelController;

    private FeedbackPortPanelController feedbackPortPanelController;

    private FeedbackPositionPanelController feedbackPositionPanelController;

    private SoundPortPanelController soundPortPanelController;

    private MotorPortPanelController motorPortPanelController;

    private SwitchPortPanelController switchPortPanelController;

    private SwitchPairPortPanelController switchPairPortPanelController;

    private LightPortPanelController lightPortPanelController;

    private StepControlController stepControlController;

    private FlagPanelController flagPanelController;

    private MacroListController macroListController;

    public TabPanel(final MainModel model) {
        this.model = model;
        emptyLabel = Resources.getString(getClass(), "emptyLabel");
        border = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), emptyLabel);

        DockKeys.DOCKKEY_TAB_PANEL.setName(Resources.getString(getClass(), "title"));
        // turn off autohide and close features
        DockKeys.DOCKKEY_TAB_PANEL.setCloseEnabled(false);
        DockKeys.DOCKKEY_TAB_PANEL.setAutoHideEnabled(false);
        DockKeys.DOCKKEY_TAB_PANEL.setFloatEnabled(true);

        final ImageIcon pendingChangesIcon =
            ImageUtils.createImageIcon(TabPanel.class, "/icons/16x16/save-to-node-12x15.png");

        tabbedPane = new JTabbedPane();
        emptyPanel = new EmptyPanel();

        tabStatusListener = new TabStatusListener() {

            @Override
            public void updatePendingChanges(JPanel source, boolean hasPendingChanges) {
                Icon icon = null;
                if (hasPendingChanges) {
                    icon = pendingChangesIcon;
                }
                int componentIndex = tabbedPane.indexOfComponent(source);
                LOGGER.debug("updatePendingChanges, componentIndex: {}, component: {}", componentIndex, source);
                if (componentIndex > -1) {
                    tabbedPane.setIconAt(componentIndex, icon);
                }
            }
        };

        setBorder(border);
        setLayout(new BorderLayout());

        accessoryPanelController = new AccessoryPanelController(model);
        accessoryListPanel = accessoryPanelController.createAccessoryListPanel();

        analogPortListPanel = new AnalogPortListPanel(model);

        boosterPanelController = new BoosterPanelController(model);
        model.addNodeListListener(boosterPanelController);
        boosterPanel = boosterPanelController.createPanel(this);

        feedbackPortPanelController = new FeedbackPortPanelController(model, this);
        feedbackPortListPanel = feedbackPortPanelController.createFeedbackPortListPanel();

        feedbackPositionPanelController = new FeedbackPositionPanelController(model, this);
        feedbackPositionListPanel = feedbackPositionPanelController.createFeedbackPositionListPanel();

        inputPortListPanel = new InputPortListPanel(model);

        lightPortPanelController = new LightPortPanelController(model);
        lightPortListPanel = lightPortPanelController.createPanel();

        backlightPortListPanel = new BacklightPortListPanel(model);

        macroListController = new MacroListController(model);
        macroListPanel = macroListController.createPanel();
        DefaultApplicationContext.getInstance().register(DefaultApplicationContext.KEY_MACROLIST_CONTROLLER,
            macroListController);

        motorPortPanelController = new MotorPortPanelController(model);
        motorPortListPanel = motorPortPanelController.createPanel();

        servoPortListPanel = new ServoPortListPanel(model);

        soundPortPanelController = new SoundPortPanelController(model);
        soundPortListPanel = soundPortPanelController.createPanel();

        switchPortPanelController = new SwitchPortPanelController(model);
        switchPortListPanel = switchPortPanelController.createPanel();

        switchPairPortPanelController = new SwitchPairPortPanelController(model);
        switchPairPortListPanel = switchPairPortPanelController.createPanel();

        flagPanelController = new FlagPanelController(model);
        flagListPanel = flagPanelController.createFlagListPanel();

        infoPanel = new InfoPanel(model);
        basicOperationsPanel = new BasicOperationsPanel(model);

        // create the CV definition panel
        cvDefinitionPanel = new CvDefinitionPanel(model, tabStatusListener);

        // create the step control panel
        stepControlController = new StepControlController();
        DefaultApplicationContext.getInstance().register(DefaultApplicationContext.KEY_STEPCONTROL_CONTROLLER,
            stepControlController);
        stepControlController.start(model, tabStatusListener);

        stepControlPanel = stepControlController.getComponent();

        boolean showActionInLastTab = Preferences.getInstance().isShowActionInLastTab();

        if (!showActionInLastTab) {
            tabs.add(basicOperationsPanel.getComponent());
        }
        tabs.add(infoPanel.getComponent());
        tabs.add(macroListPanel.getComponent());
        tabs.add(accessoryListPanel.getComponent());
        tabs.add(inputPortListPanel);
        tabs.add(analogPortListPanel);
        tabs.add(lightPortListPanel);
        tabs.add(backlightPortListPanel);
        tabs.add(motorPortListPanel);
        tabs.add(switchPortListPanel);
        tabs.add(switchPairPortListPanel);
        tabs.add(servoPortListPanel);
        tabs.add(soundPortListPanel);
        tabs.add(flagListPanel.getComponent());
        tabs.add(boosterPanel);
        tabs.add(feedbackPortListPanel);
        tabs.add(feedbackPositionListPanel.getComponent());
        tabs.add(cvDefinitionPanel);
        tabs.add(stepControlPanel.getComponent());
        if (showActionInLastTab) {
            tabs.add(basicOperationsPanel.getComponent());
        }

        // register the supported port types
        scriptingSupportMap.put(ScriptingTargetType.ANALOGPORT, analogPortListPanel);
        scriptingSupportMap.put(ScriptingTargetType.BACKLIGHTPORT, backlightPortListPanel);
        scriptingSupportMap.put(ScriptingTargetType.FEEDBACKPORT, feedbackPortListPanel);
        // scriptingSupportMap.put(ScriptingTargetType.FEEDBACKPOSITION, feedbackPositionListPanel);
        scriptingSupportMap.put(ScriptingTargetType.INPUTPORT, inputPortListPanel);
        scriptingSupportMap.put(ScriptingTargetType.LIGHTPORT, lightPortListPanel);
        scriptingSupportMap.put(ScriptingTargetType.MOTORPORT, motorPortListPanel);
        scriptingSupportMap.put(ScriptingTargetType.SERVOPORT, servoPortListPanel);
        scriptingSupportMap.put(ScriptingTargetType.SOUNDPORT, soundPortListPanel);
        scriptingSupportMap.put(ScriptingTargetType.SWITCHPORT, switchPortListPanel);
        scriptingSupportMap.put(ScriptingTargetType.SWITCHPAIRPORT, switchPairPortListPanel);
        scriptingSupportMap.put(ScriptingTargetType.FLAG, flagListPanel);

        scriptingSupportMap.put(ScriptingTargetType.MACRO, macroListPanel);
        scriptingSupportMap.put(ScriptingTargetType.ACCESSORY, accessoryListPanel);

        showInnerComponent(displayedNode == null);

        model.addNodeListListener(this);

        model.addAccessoryListListener(new org.bidib.wizard.mvc.main.model.listener.AccessoryListListener() {
            @Override
            public void listChanged() {
                enableTab(accessoryListPanel.getComponent(), !model.getAccessories().isEmpty());
            }

            @Override
            public void accessoryChanged(int accessoryId) {
            }

            @Override
            public void pendingChangesChanged() {
            }
        });
        model.addAnalogPortListListener(new PortListListener() {
            @Override
            public void listChanged() {
                enableTab(analogPortListPanel, !model.getAnalogPorts().isEmpty());
            }
        });

        model.addInputPortListListener(new PortListListener() {
            @Override
            public void listChanged() {
                enableTab(inputPortListPanel, !model.getInputPorts().isEmpty());
            }
        });
        model.addLightPortListListener(new PortListListener() {
            @Override
            public void listChanged() {
                LOGGER.debug("listChanged in LightPortListener, model.lightPorts: {}", model.getLightPorts());
                enableTab(lightPortListPanel, !model.getLightPorts().isEmpty());
            }
        });
        model.addBacklightPortListListener(new PortListListener() {
            @Override
            public void listChanged() {
                LOGGER.debug("listChanged in BacklightPortListener, model.backlightPorts: {}",
                    model.getBacklightPorts());
                enableTab(backlightPortListPanel, !model.getBacklightPorts().isEmpty());
            }
        });
        model.addMacroListListener(new org.bidib.wizard.mvc.main.model.listener.MacroListListener() {
            @Override
            public void listChanged() {
                enableTab(macroListPanel.getComponent(), !model.getMacros().isEmpty());
                enableTab(flagListPanel.getComponent(), !model.getMacros().isEmpty());
            }

            @Override
            public void macroChanged() {
            }

            @Override
            public void pendingChangesChanged() {
            }
        });

        model.addSoundPortListListener(new PortListListener() {
            @Override
            public void listChanged() {
                enableTab(soundPortListPanel, CollectionUtils.isNotEmpty(model.getSoundPorts()));
            }
        });
        model.addSwitchPortListListener(new PortListListener() {
            @Override
            public void listChanged() {
                LOGGER.info("The switch port list has changed.");
                enableTab(switchPortListPanel, CollectionUtils.isNotEmpty(model.getSwitchPorts()));
            }
        });
        model.addSwitchPairPortListListener(new PortListListener() {
            @Override
            public void listChanged() {
                LOGGER.info("The switchPair port list has changed.");
                enableTab(switchPairPortListPanel, CollectionUtils.isNotEmpty(model.getSwitchPairPorts()));
            }
        });

        model.addCvDefinitionListener(new CvDefinitionListener() {
            @Override
            public void cvDefinitionChanged() {
                LOGGER.info(
                    "The CV defintion has changed. Enable/disable the cvDefintionPanel and the stepControlPanel.");
                enableTab(cvDefinitionPanel, model.isCvDefinitionAvailable());

                // step control has special panel
                enableTab(stepControlPanel.getComponent(),
                    displayedNode != null && ProductUtils.isStepControl(displayedNode.getNode().getUniqueId()));

            }

            @Override
            public void cvDefinitionValuesChanged(final boolean read) {
            }
        });

        tabbedPane.addChangeListener(new ChangeListener() {

            @Override
            public void stateChanged(ChangeEvent e) {
                Component selectedComponent = tabbedPane.getSelectedComponent();
                LOGGER.debug("Currently selected component in tabbedPane: {}", selectedComponent);

                for (Component comp : tabs) {
                    if (comp instanceof TabSelectionListener) {
                        LOGGER.info("Select tab, current component: {}", comp);
                        ((TabSelectionListener) comp).tabSelected(comp.equals(selectedComponent));
                    }
                }
            }
        });

    }

    private void showInnerComponent(boolean empty) {

        if (empty) {
            remove(tabbedPane);
            add(emptyPanel.getContent());
        }
        else {
            remove(emptyPanel.getContent());
            add(tabbedPane);
        }
    }

    public void addAccessoryListListener(AccessoryListListener l) {
        accessoryListPanel.addAccessoryListListener(l);
    }

    public void addAccessoryListSelectionListener(ListSelectionListener l) {
        accessoryListPanel.addListSelectionListener(l);
    }

    public void addAccessoryTableListener(AccessoryTableListener l) {
        accessoryListPanel.addAccessoryTableListener(l);
    }

    public void addAnalogPortListener(OutputListener<AnalogPortStatus> l) {
        analogPortListPanel.addPortListener(l);
    }

    public void addBacklightPortListener(BacklightPortListener l) {
        backlightPortListPanel.addPortListener(l);
    }

    public void addMacroListListener(MacroListListener l) {
        macroListPanel.addMacroListListener(l);
    }

    public void addMacroListSelectionListener(ListSelectionListener l) {
        macroListPanel.addListSelectionListener(l);
    }

    public void addMacroTableListener(MacroTableListener l) {
        macroListPanel.addMacroTableListener(l);
    }

    public void addServoPortListener(ServoPortListener<ServoPortStatus> l) {
        servoPortListPanel.addServoPortListener(l);
    }

    public void addCvDefinitionRequestListener(CvDefinitionRequestListener l) {
        ((CvDefinitionRequestListenerAware) cvDefinitionPanel).addCvDefinitionRequestListener(l);
        stepControlPanel.addCvDefinitionRequestListener(l);
        lightPortPanelController.addCvDefinitionRequestListener(l);
    }

    @Override
    public void addressMessagesEnabledChanged(Boolean isAddressMessagesEnabled) {
    }

    @Override
    public void dccStartEnabledChanged(Boolean isDccStartEnabled) {
    }

    @Override
    public void railComPlusAvailableChanged(Boolean railComPlusAvailable) {
    }

    @Override
    public void pomUpdateAvailableChanged(Boolean pomUpdateAvailable) {
    }

    @Override
    public void externalStartEnabledChanged(Boolean isExternalStartEnabled) {
    }

    @Override
    public void feedbackMessagesEnabledChanged(Boolean isFeedbackMessagesEnabled) {
    }

    @Override
    public void feedbackMirrorDisabledChanged(Boolean isFeedbackMirrorDisabled) {
    }

    @Override
    public void identifyStateChanged(IdentifyState identifyState) {
    }

    @Override
    public void keyMessagesEnabledChanged(Boolean isKeyMessagesEnabled) {
    }

    @Override
    public void labelChanged(String label) {
        setBorderLabel(label);
    }

    private void setBorderLabel(final String tabTitle) {

        LOGGER.debug("Change the tab label: {}", tabTitle);
        if (SwingUtilities.isEventDispatchThread()) {
            border.setTitle(tabTitle);
            repaint();
        }
        else {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    border.setTitle(tabTitle);
                    repaint();
                }
            });
        }
    }

    @Override
    public void listChanged() {
        LOGGER.debug("list has changed.");
    }

    @Override
    public void nodeChanged() {
        LOGGER.debug("The node has changed: {}", displayedNode);
        if (SwingUtilities.isEventDispatchThread()) {
            internalNodeChanged();
        }
        else {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    internalNodeChanged();
                }
            });
        }
    }

    // TODO remove later
    private final PortListListener servoPortListListener = new PortListListener() {
        @Override
        public void listChanged() {
            LOGGER.info("List of servo ports has changed.");
            enableTab(servoPortListPanel, !model.getServoPorts().isEmpty());
        }
    };

    private final PortListListener motorPortListListener = new PortListListener() {
        @Override
        public void listChanged() {
            LOGGER.info("List of motor ports has changed.");
            enableTab(motorPortListPanel, !model.getMotorPorts().isEmpty());
        }
    };

    private void internalNodeChanged() {
        LOGGER.debug("handle node has changed: {}", displayedNode);

        if (displayedNode != null && displayedNode.equals(model.getSelectedNode())) {
            LOGGER.debug("The node has not changed.");
            return;
        }

        if (displayedNode != null) {
            displayedNode.removeNodeListener(this);
        }
        // remove all tabs because otherwise while loading features from LC the booster panel is still displayed
        tabbedPane.removeAll();

        // TODO
        if (displayedNode != null) {
            displayedNode.removePortListListener(ServoPort.class, servoPortListListener);
            displayedNode.removePortListListener(MotorPort.class, motorPortListListener);
        }

        displayedNode = model.getSelectedNode();
        showInnerComponent(displayedNode == null);

        for (Component tab : tabs) {
            if (tab instanceof TabVisibilityProvider) {
                LOGGER.info("Set visibility of tab: {}", tab);
                enableTab(tab, ((TabVisibilityProvider) tab).isTabVisible());
            }
        }

        if (displayedNode != null) {
            displayedNode.addPortListListener(ServoPort.class, servoPortListListener);
            displayedNode.addPortListListener(MotorPort.class, motorPortListListener);
        }

        // TODO change this
        servoPortListPanel.listChanged();
        motorPortListPanel.listChanged();

        // change visibility of info panel
        enableTab(infoPanel.getComponent(), displayedNode != null);

        // change visibility of basic operations panel
        enableTab(basicOperationsPanel.getComponent(), displayedNode != null);

        // the booster panel needs special handling because it has no list of ports
        enableTab(boosterPanel,
            displayedNode != null && (NodeUtils.hasBoosterFunctions(displayedNode.getNode().getUniqueId())
                || NodeUtils.hasCommandStationFunctions(displayedNode.getNode().getUniqueId())));

        if (displayedNode != null) {
            displayedNode.addNodeListener(this);

            File file = new File("");
            file = new File(file.getAbsoluteFile(), "data/BiDiBProducts");

            ProductType product =
                BidibProductsFactory.getProduct(displayedNode.getNode(), file.getAbsolutePath(),
                    "classpath:/xml/BiDiBProducts");

            String nodeName = BidibNodeNameUtils.getNodeName(displayedNode);
            if (product != null) {
                labelChanged(nodeName + " - " + product.getName());
            }
            else {
                labelChanged(nodeName);
            }
        }
        else {
            labelChanged(emptyLabel);
        }

        flagListPanel.nodeChanged();
        infoPanel.nodeChanged();
        basicOperationsPanel.nodeChanged();
    }

    private void enableTab(final Component component, final boolean enable) {
        if (SwingUtilities.isEventDispatchThread()) {
            innerEnableTab(component, enable);
        }
        else {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    innerEnableTab(component, enable);
                }
            });
        }
    }

    private void innerEnableTab(final Component component, final boolean enable) {

        int componentIndex = tabbedPane.indexOfComponent(component);
        LOGGER.info("enableTab, enable: {}, componentIndex: {}, component: {}", enable, componentIndex, component);
        if (enable && componentIndex == -1) {

            // count all visible components in front of me
            int index = 0;

            for (Component tab : tabs) {
                if (tab == component) {
                    break;
                }
                if (tabbedPane.indexOfComponent(tab) >= 0) {
                    index++;
                }
            }
            tabbedPane.add(component, index);
            if (index == 0) {
                tabbedPane.setSelectedIndex(0);
            }
        }
        else if (!enable && componentIndex >= 0) {
            tabbedPane.remove(component);
        }
        LOGGER.debug("number of components in tabbed pane: {}", tabbedPane.getComponentCount());
    }

    @Override
    public void nodeStateChanged() {
    }

    @Override
    public Component getComponent() {
        return this;
    }

    @Override
    public DockKey getDockKey() {
        return DockKeys.DOCKKEY_TAB_PANEL;
    }

    @Override
    public void sysErrorChanged(SysErrorEnum sysError) {
    }

    @Override
    public void listNodeAdded(Node node) {
    }

    @Override
    public void listNodeRemoved(Node node) {
    }

    @Override
    public void setLabel(Long uuid, TargetType targetType) {
        LOGGER.info("Set label, uuid: {}, targetType: {}", uuid, targetType);

        ChangeLabelSupport changeLabelSupport = scriptingSupportMap.get(targetType.getScriptingTargetType());
        if (changeLabelSupport != null) {
            changeLabelSupport.changeLabel(targetType);
        }
        else {
            LOGGER.warn("No changeLabelSupport available for targetType: {}", targetType);
        }
    }

    public void setSelectedNode(Node node) {
        LOGGER.info("Set the selected node: {}", node);

    }

    @Override
    public void nodeWillChange() {
    }

    @Override
    public void setTabVisible(Component component, boolean visible) {
        LOGGER.info("Set the component visible, component: {}, visible: {}", component, visible);

        innerEnableTab(component, visible);
    }
}
