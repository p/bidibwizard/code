package org.bidib.wizard.mvc.logger.view;

import java.util.Observable;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.AppenderBase;

public class BidibLogsAppender extends AppenderBase<ILoggingEvent> {

    private BidibLogsObservable bidibLogsObservable = new BidibLogsObservable();

    public static final String APPENDER_NAME = "BidibLogsAppender";

    /**
     * Receives a log from Logback, and sends it to the {@code LogsPane} object.
     * 
     * @param loggingEvent
     *            a Logback {@code ILoggingEvent} event.
     */
    @Override
    protected void append(ILoggingEvent loggingEvent) {
        bidibLogsObservable.notifyObservers(loggingEvent);
    }

    /**
     * Returns the log observable object.
     * 
     * @return the log observable object.
     */
    public Observable getObservable() {
        return bidibLogsObservable;
    }
}
