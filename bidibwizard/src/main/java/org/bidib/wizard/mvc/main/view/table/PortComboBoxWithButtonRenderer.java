package org.bidib.wizard.mvc.main.view.table;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.mvc.main.model.ConfigurablePort;
import org.bidib.wizard.mvc.main.model.Port;

public class PortComboBoxWithButtonRenderer<E, S extends BidibStatus> extends JPanel implements TableCellRenderer {
    private static final long serialVersionUID = 1L;

    protected final JComboBox<E> comboBox;

    private final JButton button;

    public PortComboBoxWithButtonRenderer(E[] items, String buttonText) {
        comboBox = new JComboBox<E>(items.clone());
        button = new JButton(buttonText);
        setLayout(new GridBagLayout());

        GridBagConstraints c = new GridBagConstraints();

        c.anchor = GridBagConstraints.FIRST_LINE_START;
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0;
        c.insets = new Insets(0, 0, 2, 2);
        c.weightx = 1;
        add(comboBox, c);
        c.anchor = GridBagConstraints.FIRST_LINE_END;
        c.gridx++;
        c.weightx = 0;
        add(button, c);
    }

    public Component getTableCellRendererComponent(
        JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

        if (isSelected) {
            setForeground(table.getSelectionForeground());
            setBackground(table.getSelectionBackground());
        }
        else {
            setForeground(table.getForeground());
            setBackground(table.getBackground());
        }

        if (value instanceof ConfigurablePort) {
            ConfigurablePort<?> port = (ConfigurablePort<?>) value;
            setEnabled(port.isEnabled());
            comboBox.setEnabled(port.isEnabled());
            button.setEnabled(port.isEnabled());

            setSelectedValue((Port<? extends BidibStatus>) port);
        }
        else {
            setEnabled(true);
            comboBox.setEnabled(true);
            button.setEnabled(true);

            setSelectedValue((Port<? extends BidibStatus>) value);
        }
        return this;
    }

    protected void setSelectedValue(Port<? extends BidibStatus> port) {
        comboBox.setSelectedItem(port != null ? port.getStatus() : null);
    }
}
