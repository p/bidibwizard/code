package org.bidib.wizard.mvc.main.model;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.bidib.jbidibc.core.enumeration.FeedbackPortEnum;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.wizard.comm.FeedbackPortStatus;

public class FeedbackPort extends Port<FeedbackPortStatus> {
    private static final long serialVersionUID = 1L;

    private List<FeedbackAddressData> addresses;

    private final Set<FeedbackDynStateData> dynStates;

    private FeedbackTimestampData timestamp;

    private FeedbackConfidenceData confidence;

    public FeedbackPort() {
        super(null);
        addresses = new LinkedList<FeedbackAddressData>();
        dynStates = new TreeSet<FeedbackDynStateData>();

        setStatus(FeedbackPortStatus.FREE);
    }

    public List<FeedbackAddressData> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<FeedbackAddressData> addresses) {
        this.addresses = addresses;
    }

    /**
     * @return the confidence
     */
    public FeedbackConfidenceData getConfidence() {
        return confidence;
    }

    /**
     * @param confidence
     *            the confidence to set
     */
    public void setConfidence(FeedbackConfidenceData confidence) {
        this.confidence = confidence;
    }

    /**
     * @return the dynStates
     */
    public Set<FeedbackDynStateData> getDynStates() {
        return Collections.unmodifiableSet(dynStates);
    }

    /**
     * @param dynStates
     *            the dynStates to set
     */
    public void addDynStates(List<FeedbackDynStateData> dynStates) {

        for (FeedbackDynStateData data : dynStates) {
            this.dynStates.remove(data);
            this.dynStates.add(data);
        }

        // this.dynStates.addAll(dynStates);
    }

    /**
     * Clear the dyn states
     */
    public void clearDynStates() {
        dynStates.clear();
    }

    /**
     * @return the timestamp
     */
    public FeedbackTimestampData getTimestamp() {
        return timestamp;
    }

    /**
     * @param timestamp
     *            the timestamp to set
     */
    public void setTimestamp(FeedbackTimestampData timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public void setStatus(FeedbackPortStatus status) {
        super.setStatus(status);

        // remove the dynStates
        if (FeedbackPortStatus.FREE.equals(status)) {
            clearDynStates();
            setAddresses(null);
            setTimestamp(null);
        }
    }

    @Override
    public byte[] getPortConfig() {
        return new byte[] { 0, 0, 0, 0 };
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof FeedbackPort) {
            return ((FeedbackPort) obj).getId() == getId();
        }
        return false;
    }

    @Override
    public int hashCode() {
        return getId();
    }

    @Override
    protected LcOutputType getPortType() {
        return LcOutputType.FEEDBACKPORT;
    }

    @Override
    protected FeedbackPortStatus internalGetStatus() {
        return FeedbackPortStatus.valueOf(FeedbackPortEnum.valueOf(genericPort.getPortStatus()));
    }
}
