package org.bidib.wizard.mvc.main.view.panel.listener;

public interface StatusListener {
    /**
     * Switch booster off.
     */
    void switchedOff();

    /**
     * Switch booster on.
     */
    void switchedOn();

    /**
     * Query the booster state.
     */
    void queryBoosterState();

    /**
     * Switch the command station to go.
     * 
     * @param ignoreWatchDog
     *            ignore watchdog flag
     */
    void switchedCommandStationOn(boolean ignoreWatchDog);

    /**
     * Switch the command station to stop.
     */
    void switchedCommandStationStop();

    /**
     * Switch the command station to soft stop.
     */
    void switchedCommandStationSoftStop();

    /**
     * Switch the command station to off.
     */
    void switchedCommandStationOff();
}
