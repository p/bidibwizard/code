package org.bidib.wizard.mvc.pt.view.command;

import java.util.LinkedList;
import java.util.List;

import org.bidib.jbidibc.core.enumeration.PtOperation;
import org.bidib.wizard.mvc.pt.view.panel.ProgCommandAwareBeanModel;

public abstract class PtOperationIfElseCommand<M extends ProgCommandAwareBeanModel> extends PtOperationCommand<M> {

    private List<PtOperationCommand<M>> progCommandsSuccess = new LinkedList<PtOperationCommand<M>>();

    private List<PtOperationCommand<M>> progCommandsFailure = new LinkedList<PtOperationCommand<M>>();

    public PtOperationIfElseCommand(PtOperation ptOperation, int cvNumber, int cvValue) {
        super(ptOperation, cvNumber, cvValue);
    }

    /**
     * @return the progCommandsSuccess
     */
    public List<PtOperationCommand<M>> getProgCommandsSuccess() {
        return progCommandsSuccess;
    }

    /**
     * @param progCommandsSuccess
     *            the progCommandsSuccess to set
     */
    public void setProgCommandsSuccess(List<PtOperationCommand<M>> progCommandsSuccess) {
        this.progCommandsSuccess = progCommandsSuccess;
    }

    /**
     * @param progCommandSuccess
     *            the progCommandSuccess to add
     */
    public void addProgCommandSuccess(PtOperationCommand<M> progCommandSuccess) {
        if (progCommandsSuccess == null) {
            progCommandsSuccess = new LinkedList<>();
        }
        progCommandsSuccess.add(progCommandSuccess);
    }

    /**
     * @return the progCommandsFailure
     */
    public List<PtOperationCommand<M>> getProgCommandsFailure() {
        return progCommandsFailure;
    }

    /**
     * @param progCommandsFailure
     *            the progCommandsFailure to set
     */
    public void setProgCommandsFailure(List<PtOperationCommand<M>> progCommandsFailure) {
        this.progCommandsFailure = progCommandsFailure;
    }

    /**
     * Verify the result.
     * 
     * @return true: expected result, false: result is different from expected result
     */
    public abstract boolean isExpectedResult();

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer(super.toString());
        if (progCommandsSuccess != null) {
            sb.append(",progCommandsSuccess=").append(progCommandsSuccess);
        }
        if (progCommandsFailure != null) {
            sb.append(",progCommandsFailure=").append(progCommandsFailure);
        }
        return sb.toString();
    }
}
