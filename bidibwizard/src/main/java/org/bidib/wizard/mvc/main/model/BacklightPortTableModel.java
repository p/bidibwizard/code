package org.bidib.wizard.mvc.main.model;

import java.util.Collection;
import java.util.LinkedList;

import javax.swing.SwingUtilities;

import org.bidib.wizard.comm.BacklightPortStatus;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.main.model.listener.BacklightPortListener;
import org.bidib.wizard.mvc.main.model.listener.PortListener;
import org.bidib.wizard.mvc.main.view.table.listener.SliderValueListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BacklightPortTableModel
    extends SimplePortTableModel<BacklightPortStatus, BacklightPort, BacklightPortListener> {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(BacklightPortTableModel.class);

    private final Collection<SliderValueListener<BacklightPort>> sliderValueListeners =
        new LinkedList<SliderValueListener<BacklightPort>>();

    public static final int COLUMN_LABEL = 0;

    public static final int COLUMN_DIM_SLOPE_UP = 1;

    public static final int COLUMN_DIM_SLOPE_DOWN = 2;

    public static final int COLUMN_DMX_MAPPING = 3;

    public static final int COLUMN_VALUE = 4;

    public static final int COLUMN_PORT_INSTANCE = 5;

    public BacklightPortTableModel(MainModel model) {
        super();

        model.addBacklightPortListener(new PortListener<BacklightPortStatus>() {
            @Override
            public void labelChanged(Port<BacklightPortStatus> port, String label) {
            }

            @Override
            public void statusChanged(Port<BacklightPortStatus> port, BacklightPortStatus status) {
                LOGGER.debug("The port status has changed: {}, port: {}, port.status: {}", status, port,
                    port.getStatus());
                // updatePortStatus(port);
            }

            @Override
            public void configChanged(Port<BacklightPortStatus> port) {
                LOGGER.info("The port config has been changed, port: {}", port);
                updatePortConfig((BacklightPort) port);
            }
        });
    }

    private void updatePortConfig(BacklightPort port) {
        for (int row = 0; row < getRowCount(); row++) {
            if (port.equals(getValueAt(row, COLUMN_PORT_INSTANCE))) {
                super.setValueAt(port.getDimSlopeDown(), row, COLUMN_DIM_SLOPE_DOWN);
                super.setValueAt(port.getDimSlopeUp(), row, COLUMN_DIM_SLOPE_UP);
                super.setValueAt(port.getDmxMapping(), row, COLUMN_DMX_MAPPING);
                break;
            }
        }
    }

    @Override
    protected void initialize() {
        super.setPortValuesProcessing(true);
        columnNames =
            new String[] { Resources.getString(BacklightPortTableModel.class, "label"),
                Resources.getString(BacklightPortTableModel.class, "dimSlopeDown"),
                Resources.getString(BacklightPortTableModel.class, "dimSlopeUp"),
                Resources.getString(BacklightPortTableModel.class, "dmxMapping"),
                Resources.getString(BacklightPortTableModel.class, "test"), null };
    }

    @Override
    protected int getColumnPortInstance() {
        return COLUMN_PORT_INSTANCE;
    }

    public void addSliderValueListener(SliderValueListener<BacklightPort> l) {
        sliderValueListeners.add(l);
    }

    public void addRow(BacklightPort port) {
        if (port != null) {
            Object[] rowData = new Object[columnNames.length];

            rowData[COLUMN_LABEL] = port.toString();
            rowData[COLUMN_DIM_SLOPE_DOWN] = port.getDimSlopeDown();
            rowData[COLUMN_DIM_SLOPE_UP] = port.getDimSlopeUp();
            rowData[COLUMN_DMX_MAPPING] = port.getDmxMapping();
            rowData[COLUMN_VALUE] = port.getValue();
            rowData[COLUMN_PORT_INSTANCE] = port;

            addRow(rowData);
        }
    }

    private void fireLabelChanged(BacklightPort port, String label) {
        for (BacklightPortListener l : portListeners) {
            l.labelChanged(port, label);
        }
    }

    private void fireConfigValuesChanged(BacklightPort port) {
        for (BacklightPortListener l : portListeners) {
            l.valuesChanged(port);
        }
    }

    private void fireTestValuesChanged(BacklightPort port) {
        // the test values have changed
        for (SliderValueListener<BacklightPort> l : sliderValueListeners) {
            l.sliderValueChanged(port);
        }
    }

    @Override
    public Class<?> getColumnClass(int column) {
        switch (column) {
            case COLUMN_LABEL:
                return String.class;
            case COLUMN_PORT_INSTANCE:
                return Object.class;
            default:
                return Integer.class;
        }
    }

    @Override
    public void setValueAt(Object value, int row, int column) {
        final BacklightPort port = (BacklightPort) getValueAt(row, COLUMN_PORT_INSTANCE);
        LOGGER.trace("Set value, row: {}, column: {}, value: {}, port: {}", row, column, value, port);

        if (!SwingUtilities.isEventDispatchThread()) {
            LOGGER.warn("setValueAt was called from non-AWT thread!");
        }

        switch (column) {
            case COLUMN_LABEL:
                port.setLabel((String) value);
                super.setValueAt(port.toString(), row, column);
                fireLabelChanged(port, port.getLabel());
                break;
            case COLUMN_DIM_SLOPE_DOWN:
                port.setDimSlopeDown((Integer) value);
                super.setValueAt(value, row, column);
                fireConfigValuesChanged(port);
                break;
            case COLUMN_DIM_SLOPE_UP:
                port.setDimSlopeUp((Integer) value);
                super.setValueAt(value, row, column);
                fireConfigValuesChanged(port);
                break;
            case COLUMN_DMX_MAPPING:
                try {
                    port.setDmxMapping(Integer.parseInt(value.toString()));
                    super.setValueAt(value, row, column);
                    fireConfigValuesChanged(port);
                }
                catch (NumberFormatException e) {
                }
                break;
            case COLUMN_VALUE:
                try {
                    // set the value for the brightness
                    int intValue = port.getAbsoluteValue(Integer.parseInt(value.toString()));
                    LOGGER.debug("Current value: {}, absolute value: {}", value, intValue);

                    if (port.getValue() != intValue) {
                        port.setValue(intValue, true);
                        super.setValueAt(intValue, row, column);
                        fireTestValuesChanged(port);
                    }
                }
                catch (NumberFormatException e) {
                }
                break;
        }
    }

    @Override
    public Object getValueAt(int row, int column) {
        Object result = super.getValueAt(row, column);

        if (result != null && column == COLUMN_VALUE) {
            BacklightPort port = (BacklightPort) getValueAt(row, COLUMN_PORT_INSTANCE);

            if (port != null) {
                // return the value for the brightness
                result = port.getRelativeValue();
                LOGGER.trace("Return relative brightness value: {}", result);
            }
        }
        return result;
    }

}
