package org.bidib.wizard.mvc.main.view.cvdef;

import java.awt.Color;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.apache.commons.lang.StringUtils;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.exchange.vendorcv.ModeType;
import org.bidib.wizard.comm.FeedbackPortStatus;
import org.bidib.wizard.common.utils.ImageUtils;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.bidib.wizard.mvc.common.view.cvdefinition.CvDefinitionTreeTableModel;
import org.bidib.wizard.mvc.common.view.validation.IconFeedbackPanel;
import org.bidib.wizard.mvc.main.controller.FeedbackPortStatusChangeProvider;
import org.bidib.wizard.mvc.main.model.FeedbackPort;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.model.Port;
import org.bidib.wizard.mvc.main.model.listener.DefaultFeedbackPortListener;
import org.bidib.wizard.mvc.main.model.listener.PortListListener;
import org.bidib.wizard.mvc.main.view.panel.NodeTree;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.adapter.ComboBoxAdapter;
import com.jgoodies.binding.beans.BeanAdapter;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.value.BindingConverter;
import com.jgoodies.binding.value.BufferedValueModel;
import com.jgoodies.binding.value.ConverterValueModel;
import com.jgoodies.binding.value.Trigger;
import com.jgoodies.binding.value.ValueHolder;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.ValidationResultModel;
import com.jgoodies.validation.util.DefaultValidationResultModel;
import com.jgoodies.validation.view.ValidationComponentUtils;

public class GBM16TReverserEditor extends JPanel implements CvValueEditor {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(GBM16TReverserEditor.class);

    private MainModel mainModel;

    private int minFeedbackPort;

    private int maxFeedbackPort;

    private GBM16TReverserValueBean gbm16TReverserValueBean;

    private GBM16TReverserPresentationModel cvAdapter;

    private ValidationResultModel cvValidationModel;

    private BufferedValueModel reverserModeValueModel;

    private ValueModel converterReverserModeValueModel;

    private BufferedValueModel reverserOnValueModel;

    private BufferedValueModel reverserOffValueModel;

    private BufferedValueModel reverserPrioValueModel;

    private ValueModel[] checkReverserOnValueModel;

    private CheckBoxStateBean[] checkBoxStateBeanOn;

    private CheckBoxStateBean[] checkBoxStateBeanOff;

    private JCheckBox[] checkReverserOn;

    private ValueModel[] checkReverserOffValueModel;

    private JCheckBox[] checkReverserOff;

    private ValueModel[] checkReverserPrioValueModel;

    private JCheckBox[] checkReverserPrio;

    private WordBitfieldBean bitfieldBeanReverserOn = new WordBitfieldBean();

    private WordBitfieldBean bitfieldBeanReverserOff = new WordBitfieldBean();

    private WordBitfieldBean bitfieldBeanReverserPrio = new WordBitfieldBean();

    private BeanAdapter<WordBitfieldBean> bitfieldBeanReverserOnAdapter;

    private PortListListener portListListener;

    private JComponent cvIconPanel;

    private ValueModel saveButtonEnabledModel = new ValueHolder(false);

    private boolean setInProgress;

    private ValueModel writeEnabled = new ValueHolder(true);

    // private PropertyChangeListener pclFeedbackPorts;

    private final FeedbackPortStatusChangeProvider feedbackPortStatusChangeProvider;

    private Node selectedNode;

    private static enum ReverserMode {
        OFF((byte) 0, "OFF"), DCC((byte) 1, "DCC"), TRACK((byte) 2, "TRACK"), TRACKDCC((byte) 3, "TRACK & DCC");

        private final byte type;

        private final String text;

        ReverserMode(byte type, String text) {
            this.type = type;
            this.text = text;
        }

        public byte getType() {
            return type;
        }

        public String toString() {
            return text;
        }

        public static ReverserMode valueOf(byte type) {
            ReverserMode result = null;

            for (ReverserMode e : values()) {
                if (e.type == type) {
                    result = e;
                    break;
                }
            }
            if (result == null) {
                throw new IllegalArgumentException("cannot map " + type + " to a ReverserMode");
            }
            return result;
        }

    }

    private static class ReverserModeConverter implements BindingConverter<Number, ReverserMode> {

        @Override
        public ReverserMode targetValue(Number sourceValue) {
            LOGGER.debug("Convert to target, sourceValue: {}", sourceValue);
            if (sourceValue != null) {
                try {
                    return ReverserMode.valueOf(ByteUtils.getLowByte(((Number) sourceValue).intValue()));
                }
                catch (IllegalArgumentException ex) {
                    LOGGER.warn("Convert value to ReverserMode failed, return ReverserMode.OFF as default value.", ex);
                    return ReverserMode.OFF;
                }
            }
            return null;
        }

        @Override
        public Number sourceValue(ReverserMode targetValue) {
            LOGGER.debug("Convert to source, targetValue: {}", targetValue);
            if (targetValue != null) {
                return ((ReverserMode) targetValue).getType();
            }
            return null;
        }
    }

    public GBM16TReverserEditor(MainModel mainModel) {
        this.mainModel = mainModel;

        feedbackPortStatusChangeProvider =
            DefaultApplicationContext.getInstance().get(DefaultApplicationContext.KEY_FEEDBACKPORTSTATUSCHANGEPROVIDER,
                FeedbackPortStatusChangeProvider.class);

        LOGGER.info("Use changeProvider: {}", feedbackPortStatusChangeProvider);
    }

    public GBM16TReverserPresentationModel getCvAdapter() {
        return cvAdapter;
    }

    public ValueModel getSaveButtonEnabledModel() {
        return saveButtonEnabledModel;
    }

    private Trigger trigger;

    @Override
    public Trigger getTrigger() {
        return trigger;
    }

    public JPanel create(final Trigger trigger) {
        this.trigger = trigger;

        StringBuffer columnSpec = new StringBuffer("20dlu, 2dlu");
        for (int col = 0; col < 16; col++) {
            columnSpec.append(", 15dlu, 2dlu");
        }
        FormLayout layout = new FormLayout(columnSpec.toString(), "");

        DefaultFormBuilder formBuilder = null;
        formBuilder = new DefaultFormBuilder(layout/* , this */);

        this.cvValidationModel = new DefaultValidationResultModel();

        gbm16TReverserValueBean = new GBM16TReverserValueBean();
        cvAdapter = new GBM16TReverserPresentationModel(
            gbm16TReverserValueBean, this.trigger) {
            private static final long serialVersionUID = 1L;

            @Override
            public ValidationResult validate() {
                LOGGER.debug("Validate is called.");
                boolean writeEnabledFlag = Boolean.TRUE.equals(writeEnabled.getValue());
                ValidationResult validationResult = validateModel(writeEnabledFlag);

                // update the save button
                boolean hasErrors = cvValidationModel.hasErrors();

                updateSaveButtonEnabled(cvAdapter.isBuffering(), hasErrors);

                return validationResult;
            };
        };

        cvAdapter.setValidationResultModel(cvValidationModel);

        reverserModeValueModel = cvAdapter.getBufferedModel("reverserModeValue");
        reverserOnValueModel = cvAdapter.getBufferedModel("reverserOnValue");
        reverserOffValueModel = cvAdapter.getBufferedModel("reverserOffValue");
        reverserPrioValueModel = cvAdapter.getBufferedModel("reverserPrioValue");

        // create a lable with an icon
        final ImageIcon gbm16TReverserIcon = ImageUtils.createImageIcon(NodeTree.class, "/icons/GBM16TReverser.png");
        JLabel label = new JLabel(
            gbm16TReverserIcon) {
            private static final long serialVersionUID = 1L;

            @Override
            public Dimension getPreferredSize() {
                return new Dimension(gbm16TReverserIcon.getIconWidth(), gbm16TReverserIcon.getIconHeight());
            }
        };
        formBuilder.appendRow("p");
        CellConstraints cc = new CellConstraints();
        formBuilder.add(label, cc.xyw(1, 1, 33));

        // create the list of reverser modes
        List<ReverserMode> reverserModes = new ArrayList<ReverserMode>();
        reverserModes.add(ReverserMode.OFF);
        reverserModes.add(ReverserMode.DCC);
        reverserModes.add(ReverserMode.TRACK);
        reverserModes.add(ReverserMode.TRACKDCC);

        // wire our new combobox up to that property adapter.
        converterReverserModeValueModel = new ConverterValueModel(reverserModeValueModel, new ReverserModeConverter());
        JComboBox<ReverserMode> reverserModeComboBox =
            new JComboBox<ReverserMode>(new ComboBoxAdapter<ReverserMode>(reverserModes,
                converterReverserModeValueModel));

        formBuilder.appendRow("3dlu");
        formBuilder.nextLine();
        formBuilder.appendRow("p");
        formBuilder.nextLine();
        formBuilder.append("Mode");
        formBuilder.add(reverserModeComboBox, cc.xyw(3, 3, 7));

        formBuilder.appendRow("3dlu");
        formBuilder.nextLine();
        formBuilder.appendRow("p");
        formBuilder.nextLine();

        /* final BeanAdapter<WordBitfieldBean> */bitfieldBeanReverserOnAdapter =
            new BeanAdapter<WordBitfieldBean>(bitfieldBeanReverserOn, true);
        final BeanAdapter<WordBitfieldBean> bitfieldBeanReverserOffAdapter =
            new BeanAdapter<WordBitfieldBean>(bitfieldBeanReverserOff, true);
        final BeanAdapter<WordBitfieldBean> bitfieldBeanReverserPrioAdapter =
            new BeanAdapter<WordBitfieldBean>(bitfieldBeanReverserPrio, true);

        // reverser on
        checkReverserOn = new JCheckBox[16];
        checkReverserOnValueModel = new ValueModel[16];

        checkBoxStateBeanOn = new CheckBoxStateBean[16];
        for (int index = 0; index < 16; index++) {
            LOGGER.trace("Prepare reverser on checkbox for bit: {}", index);
            checkReverserOnValueModel[index] =
                bitfieldBeanReverserOnAdapter.getValueModel(BitfieldBean.BIT_PROPERTY + (index));
            checkReverserOn[index] = BasicComponentFactory.createCheckBox(checkReverserOnValueModel[index], null);
            // checkReverserOn[index].setBackground(Color.GREEN);
            checkReverserOn[index].setContentAreaFilled(true);
        }

        // reverser off
        checkReverserOff = new JCheckBox[16];
        checkReverserOffValueModel = new ValueModel[16];
        checkBoxStateBeanOff = new CheckBoxStateBean[16];
        for (int index = 0; index < 16; index++) {
            LOGGER.trace("Prepare reverser off checkbox for bit: {}", index);

            checkReverserOffValueModel[index] =
                bitfieldBeanReverserOffAdapter.getValueModel(BitfieldBean.BIT_PROPERTY + (index));

            checkReverserOff[index] = BasicComponentFactory.createCheckBox(checkReverserOffValueModel[index], null);
            // checkReverserOff[index].setBackground(Color.RED);
            checkReverserOff[index].setContentAreaFilled(true);
        }

        // reverser prio
        checkReverserPrio = new JCheckBox[16];
        checkReverserPrioValueModel = new ValueModel[16];
        for (int index = 0; index < 16; index++) {
            LOGGER.trace("Prepare reverser prio checkbox for bit: {}", index);
            checkReverserPrioValueModel[index] =
                bitfieldBeanReverserPrioAdapter.getValueModel(BitfieldBean.BIT_PROPERTY + (index));
            checkReverserPrio[index] = BasicComponentFactory.createCheckBox(checkReverserPrioValueModel[index], null);
            checkReverserPrio[index].setContentAreaFilled(true);
        }

        formBuilder.nextLine();
        formBuilder.append("");
        for (int index = 16; index > 0; index--) {
            JLabel bitNumberLabel = new JLabel(Integer.toString(index - 1), JLabel.CENTER);
            formBuilder.append(bitNumberLabel);
        }

        formBuilder.nextLine();
        formBuilder.append("ON");
        for (int index = 16; index > 0; index--) {
            formBuilder.append(checkReverserOn[index - 1]);
        }

        formBuilder.nextLine();
        formBuilder.append("OFF");
        for (int index = 16; index > 0; index--) {
            formBuilder.append(checkReverserOff[index - 1]);
        }

        formBuilder.nextLine();
        formBuilder.append("PRIO");
        for (int index = 16; index > 0; index--) {
            formBuilder.append(checkReverserPrio[index - 1]);
        }

        // add bindings for enable/disable the checkbox
        for (int index = 0; index < 16; index++) {

            checkBoxStateBeanOn[index] = new CheckBoxStateBean();
            checkBoxStateBeanOff[index] = new CheckBoxStateBean();

            // control the bits in the checkBoxStateBean
            PropertyConnector.connect(bitfieldBeanReverserOff, WordBitfieldBean.BIT_PROPERTY + index,
                checkBoxStateBeanOn[index], "bitSet");

            PropertyConnector.connect(bitfieldBeanReverserOn, WordBitfieldBean.BIT_PROPERTY + index,
                checkBoxStateBeanOff[index], "bitSet");

            // control the enabled state of the checkboxes
            PropertyConnector.connect(checkBoxStateBeanOn[index], CheckBoxStateBean.PROPERTY_CHECKBOX_ENABLED,
                checkReverserOn[index], "enabled");

            PropertyConnector.connect(checkBoxStateBeanOff[index], CheckBoxStateBean.PROPERTY_CHECKBOX_ENABLED,
                checkReverserOff[index], "enabled");
        }

        PropertyChangeListener localChangeListenerReverserOn = new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (evt.getPropertyName().startsWith("inverted")) {
                    return;
                }

                LOGGER.debug("The reverserOn bitfield bean value has changed, new value: {}, propName: {}",
                    evt.getNewValue(), evt.getPropertyName());

                Integer bitfieldValue = bitfieldBeanReverserOn.getBitfield();
                reverserOnValueModel.setValue(bitfieldValue);

                if (!setInProgress) {
                    triggerValidation(null);
                }
            }
        };

        PropertyChangeListener localChangeListenerReverserOff = new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                LOGGER.debug("The reverserOff bitfield bean value has changed: {}", evt.getNewValue());

                if (evt.getPropertyName().startsWith("inverted")) {
                    return;
                }

                Integer bitfieldValue = bitfieldBeanReverserOff.getBitfield();
                reverserOffValueModel.setValue(bitfieldValue);

                if (!setInProgress) {
                    triggerValidation(null);
                }
            }
        };

        PropertyChangeListener localChangeListenerReverserPrio = new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                LOGGER.debug("The bitfield bean value has changed: {}", evt.getNewValue());

                if (evt.getPropertyName().startsWith("inverted")) {
                    return;
                }

                Integer bitfieldValue = bitfieldBeanReverserPrio.getBitfield();
                reverserPrioValueModel.setValue(bitfieldValue);

                if (!setInProgress) {
                    triggerValidation(null);
                }
            }
        };

        bitfieldBeanReverserOn.addPropertyChangeListener(localChangeListenerReverserOn);
        bitfieldBeanReverserOff.addPropertyChangeListener(localChangeListenerReverserOff);
        bitfieldBeanReverserPrio.addPropertyChangeListener(localChangeListenerReverserPrio);

        // TODO this causes the buffering
        reverserOnValueModel.addValueChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                LOGGER.debug("Converter model has changed, evt.propertyName: {}", evt.getPropertyName());
                Integer value = null;
                if (evt.getNewValue() instanceof Number) {
                    Number number = (Number) evt.getNewValue();
                    value = number.intValue();
                }

                bitfieldBeanReverserOn.setBitfield(value);

                updateSaveButtonEnabled(cvAdapter.isBuffering(), cvValidationModel.hasErrors());
            }
        });

        reverserOffValueModel.addValueChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                LOGGER.debug("Converter model has changed, evt: {}", evt);
                Integer value = null;
                if (evt.getNewValue() instanceof Number) {
                    Number number = (Number) evt.getNewValue();
                    value = number.intValue();
                }
                // LOGGER.debug("Set the reverser off bitfield value: {}", value);
                bitfieldBeanReverserOff.setBitfield(value);

                updateSaveButtonEnabled(cvAdapter.isBuffering(), cvValidationModel.hasErrors());
            }
        });

        reverserPrioValueModel.addValueChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                LOGGER.debug("Converter model has changed, evt: {}", evt);
                Integer value = null;
                if (evt.getNewValue() instanceof Number) {
                    Number number = (Number) evt.getNewValue();
                    value = number.intValue();
                }
                // LOGGER.debug("Set the reverser prio bitfield value: {}", value);
                bitfieldBeanReverserPrio.setBitfield(value);

                updateSaveButtonEnabled(cvAdapter.isBuffering(), cvValidationModel.hasErrors());
            }
        });

        formBuilder.appendRow("5dlu");

        ValidationComponentUtils.setMandatory(reverserModeComboBox, true);
        ValidationComponentUtils.setMessageKey(reverserModeComboBox, "validation.reverserModeValue_key");

        this.cvIconPanel = new IconFeedbackPanel(this.cvValidationModel, formBuilder.getPanel());

        DefaultFormBuilder builder = null;
        builder = new DefaultFormBuilder(new FormLayout("p:g"), this);

        builder.appendRow("p");
        builder.add(cvIconPanel);
        builder.border(new EmptyBorder(10, 0, 0, 0));

        builder.build();

        // add bindings for enable/disable the textfield
        PropertyConnector.connect(writeEnabled, "value", reverserModeComboBox, "enabled");

        for (int index = 0; index < 16; index++) {
            LOGGER.trace("Prepare reverser on checkbox for bit: {}", index);
            PropertyConnector.connect(writeEnabled, "value", checkBoxStateBeanOn[index], "enabledState");

            // PropertyConnector.connect(checkBoxStateBeanOn[index], "checkBoxEnabled", checkReverserOn[index],
            // "enabled");
            // }
            // for (int index = 0; index < 16; index++) {
            LOGGER.trace("Prepare reverser off checkbox for bit: {}", index);
            PropertyConnector.connect(writeEnabled, "value", checkBoxStateBeanOff[index], "enabledState");

            // PropertyConnector.connect(checkBoxStateBeanOff[index], "checkBoxEnabled", checkReverserOff[index],
            // "enabled");
        }
        for (int index = 0; index < 16; index++) {
            LOGGER.trace("Prepare reverser prio checkbox for bit: {}", index);
            PropertyConnector.connect(writeEnabled, "value", checkReverserPrio[index], "enabled");
        }

        // listen on changes of buffering
        cvAdapter.addPropertyChangeListener(BufferedValueModel.PROPERTY_BUFFERING, new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                LOGGER.debug("Buffering has changed, evt.newValue: {}", evt.getNewValue());
                // update the save button
                boolean isBuffering =
                    reverserModeValueModel.isBuffering() || reverserOnValueModel.isBuffering()
                        || reverserOffValueModel.isBuffering() || reverserPrioValueModel.isBuffering();

                updateSaveButtonEnabled(cvAdapter.isBuffering() /* isBuffering */, cvValidationModel.hasErrors());
            }
        });

        reverserModeValueModel.addValueChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                // TODO Auto-generated method stub

            }
        });

        triggerValidation(null);

        feedbackPortStatusChangeProvider.addFeedbackPortListener(new DefaultFeedbackPortListener() {

            @Override
            public void statusChanged(Port<FeedbackPortStatus> port, FeedbackPortStatus status) {
                LOGGER.info("Port status has changed.");
                // the changed port is only signaled from the selected node
                updateFeedbackPortStatus(port, status);
                // force repaint
                repaint();
            }
        });

        portListListener = new PortListListener() {

            @Override
            public void listChanged() {
                // the changed port is only signaled from the selected node
                LOGGER.debug("Feedback port list has changed.");
                selectedNode = feedbackPortStatusChangeProvider.getSelectedNode();

                initFeedbackPortStatus();
                if (selectedNode != null) {
                    for (FeedbackPort port : selectedNode.getFeedbackPorts()) {
                        updateFeedbackPortStatus(port, port.getStatus());
                    }
                }
                else {
                    LOGGER.warn("The selected node is not available.");
                }
                // force repaint
                repaint();
            }
        };
        feedbackPortStatusChangeProvider.addPortListListener(portListListener);

        return this;
    }

    private void initFeedbackPortStatus() {
        // remove all colored backgrounds
        for (int portNumber = 0; portNumber < 16; portNumber++) {
            Color bgColor = getBackground();
            checkReverserOn[portNumber].setBackground(bgColor);
            checkReverserOff[portNumber].setBackground(bgColor);
            checkReverserPrio[portNumber].setBackground(bgColor);
        }
    }

    private void updateFeedbackPortStatus(Port<FeedbackPortStatus> port, FeedbackPortStatus status) {
        int portNumber = port.getId();
        LOGGER.debug("Port status has changed, port num: {}, status: {}", portNumber, status);
        if (portNumber >= minFeedbackPort && portNumber <= maxFeedbackPort) {
            Color bgColor = (status == FeedbackPortStatus.FREE ? Color.GREEN : Color.RED);
            checkReverserOn[portNumber - minFeedbackPort].setBackground(bgColor);
            checkReverserOff[portNumber - minFeedbackPort].setBackground(bgColor);
            checkReverserPrio[portNumber - minFeedbackPort].setBackground(bgColor);
        }
        else {
            LOGGER.debug("The port number is out of the displayed range: {}", portNumber);
        }
    }

    protected void updateSaveButtonEnabled(boolean isBuffering, boolean validationHasErrors) {
        LOGGER.debug("Update the save button, isBuffering: {}, validationHasErrors: {}", isBuffering,
            validationHasErrors);

        if (!validationHasErrors) {
            saveButtonEnabledModel.setValue(isBuffering);
        }
        else {
            saveButtonEnabledModel.setValue(false);
        }
    }

    protected void triggerValidation(String propertyName) {
        PropertyChangeEvent evt = null;
        LOGGER.debug("trigger validation, propertyName: {}", propertyName);

        if (propertyName != null) {
            evt = new PropertyChangeEvent(this, propertyName, Boolean.FALSE, Boolean.TRUE);
        }

        // force validation
        cvAdapter.getChangeHandler().propertyChange(evt);
    }

    @Override
    public void setValue(CvNode cvNode, Map<String, CvNode> cvNumberToNodeMap) {
        LOGGER.info("Set the new cvNode: {}", cvNode);
        setInProgress = true;

        trigger.triggerFlush();
        trigger.triggerFlush();

        // writeEnabled.setValue(true);

        boolean timeout = cvNode.getConfigVar().isTimeout();
        // set textfield editable before the value is set because the validation is triggered
        writeEnabled.setValue(!(ModeType.RO.equals(cvNode.getCV().getMode()) || timeout));

        if (cvNode instanceof GBM16TReverserCvNode) {
            // check if this is the master node
            GBM16TReverserCvNode gbm16TNode = (GBM16TReverserCvNode) cvNode;
            if (gbm16TNode.getMasterNode() != null) {
                // this is a slaveNode -> get the master
                gbm16TNode = gbm16TNode.getMasterNode();
            }
            LOGGER.info("Fetched the master node: {}", gbm16TNode);

            gbm16TReverserValueBean.setCvNodeMaster(gbm16TNode);

            // set the value in the gbm16TReverserValueBean
            setValueInternally(gbm16TNode, false);
        }
        else {
            LOGGER.error("Invalid node type passed to GBM16TReverserEditor: {}", cvNode);
        }

        setInProgress = false;

        triggerValidation(null);
    }

    @Override
    public void updateCvValues(Map<String, CvNode> cvNumberToNodeMap, CvDefinitionTreeTableModel treeModel) {
        LOGGER.info("Update CV values.");

        GBM16TReverserCvNode cvNodeMaster = gbm16TReverserValueBean.getCvNodeMaster();
        cvNodeMaster.setValueAt(reverserModeValueModel.getValue(), CvNode.COLUMN_NEW_VALUE);

        List<GBM16TReverserCvNode> slaveNodes = cvNodeMaster.getSlaveNodes();

        GBM16TReverserCvNode reverserOnLow = slaveNodes.get(0);
        reverserOnLow.setValueAt(ByteUtils.getLowByte(bitfieldBeanReverserOn.getBitfield()), CvNode.COLUMN_NEW_VALUE);
        GBM16TReverserCvNode reverserOnHigh = slaveNodes.get(1);
        reverserOnHigh.setValueAt(ByteUtils.getHighByte(bitfieldBeanReverserOn.getBitfield()), CvNode.COLUMN_NEW_VALUE);

        GBM16TReverserCvNode reverserOffLow = slaveNodes.get(2);
        reverserOffLow.setValueAt(ByteUtils.getLowByte(bitfieldBeanReverserOff.getBitfield()), CvNode.COLUMN_NEW_VALUE);
        GBM16TReverserCvNode reverserOffHigh = slaveNodes.get(3);
        reverserOffHigh.setValueAt(ByteUtils.getHighByte(bitfieldBeanReverserOff.getBitfield()),
            CvNode.COLUMN_NEW_VALUE);

        GBM16TReverserCvNode reverserPrioLow = slaveNodes.get(4);
        reverserPrioLow.setValueAt(ByteUtils.getLowByte(bitfieldBeanReverserPrio.getBitfield()),
            CvNode.COLUMN_NEW_VALUE);
        GBM16TReverserCvNode reverserPrioHigh = slaveNodes.get(5);
        reverserPrioHigh.setValueAt(ByteUtils.getHighByte(bitfieldBeanReverserPrio.getBitfield()),
            CvNode.COLUMN_NEW_VALUE);

        // treeModel.refreshTreeTable(cvNodeMaster);
        // treeModel.refreshTreeTable(reverserOnLow);
        // treeModel.refreshTreeTable(reverserOnHigh);
        // treeModel.refreshTreeTable(reverserOffLow);
        // treeModel.refreshTreeTable(reverserOffHigh);
        // treeModel.refreshTreeTable(reverserPrioLow);
        // treeModel.refreshTreeTable(reverserPrioHigh);
    }

    @Override
    public void refreshValue() {
        LOGGER.info("Refresh CV values.");

        GBM16TReverserCvNode cvNodeMaster = gbm16TReverserValueBean.getCvNodeMaster();
        LOGGER.info("Refresh values form cvNodeMaster: {}", cvNodeMaster);
        if (cvNodeMaster != null) {
            setValueInternally(cvNodeMaster, false);
        }
    }

    @Override
    public void refreshValueFromSource() {
        GBM16TReverserCvNode cvNodeMaster = gbm16TReverserValueBean.getCvNodeMaster();
        LOGGER.info("Refresh values form cvNodeMaster: {}", cvNodeMaster);
        if (cvNodeMaster != null) {
            setValueInternally(cvNodeMaster, true);
        }
    }

    protected void setValueInternally(GBM16TReverserCvNode cvNodeMaster, boolean fromSource) {

        // trigger.triggerFlush();

        Number value = getValue(cvNodeMaster, fromSource);

        LOGGER.info("setValueInternally, value: {}", value);

        reverserModeValueModel.setValue(value);
        gbm16TReverserValueBean.setReverserModeValue((value != null ? ByteUtils.getLowByte(value.intValue()) : null));

        List<GBM16TReverserCvNode> slaveNodes = cvNodeMaster.getSlaveNodes();
        GBM16TReverserCvNode reverserOnLow = slaveNodes.get(0);
        Integer reverserOnLowValue = getValue(reverserOnLow, fromSource);
        GBM16TReverserCvNode reverserOnHigh = slaveNodes.get(1);
        Integer reverserOnHighValue = getValue(reverserOnHigh, fromSource);

        Integer reverserOnValue =
            ByteUtils.getInteger(ByteUtils.getLowByte(reverserOnLowValue), ByteUtils.getLowByte(reverserOnHighValue));
        gbm16TReverserValueBean.setReverserOnValue(reverserOnValue);
        bitfieldBeanReverserOn.setBitfield(reverserOnValue);

        GBM16TReverserCvNode reverserOffLow = slaveNodes.get(2);
        Integer reverserOffLowValue = getValue(reverserOffLow, fromSource);
        GBM16TReverserCvNode reverserOffHigh = slaveNodes.get(3);
        Integer reverserOffHighValue = getValue(reverserOffHigh, fromSource);

        Integer reverserOffValue =
            ByteUtils.getInteger(ByteUtils.getLowByte(reverserOffLowValue), ByteUtils.getLowByte(reverserOffHighValue));
        gbm16TReverserValueBean.setReverserOffValue(reverserOffValue);
        LOGGER.info("Set the reverser off value: {}", reverserOffValue);
        bitfieldBeanReverserOff.setBitfield(reverserOffValue);

        GBM16TReverserCvNode reverserPrioLow = slaveNodes.get(4);
        Integer reverserPrioLowValue = getValue(reverserPrioLow, fromSource);
        GBM16TReverserCvNode reverserPrioHigh = slaveNodes.get(5);
        Integer reverserPrioHighValue = getValue(reverserPrioHigh, fromSource);

        Integer reverserPrioValue =
            ByteUtils.getInteger(ByteUtils.getLowByte(reverserPrioLowValue),
                ByteUtils.getLowByte(reverserPrioHighValue));
        gbm16TReverserValueBean.setReverserPrioValue(reverserPrioValue);
        bitfieldBeanReverserPrio.setBitfield(reverserPrioValue);

        // get the node index to calculate the range of feedback ports
        int nodeIndex = cvNodeMaster.getNodeIndex();
        minFeedbackPort = nodeIndex * 16;
        maxFeedbackPort = (nodeIndex + 1) * 16 - 1;
        LOGGER.debug("Prepared minFeedbackPort: {}, maxFeedbackPort: {}", minFeedbackPort, maxFeedbackPort);

        // force refresh of port status
        portListListener.listChanged();

        LOGGER.info("Reset the changes in the presentation model.");
        trigger.triggerFlush();

        cvAdapter.resetChanged();
    }

    private Integer getValue(CvNode cvNode, boolean fromSource) {
        Integer value = null;
        // this must be updated in the text field ...
        if (!fromSource && cvNode.getNewValue() instanceof Number) {
            value = ((Number) cvNode.getNewValue()).intValue();
        }
        else if (StringUtils.isNotBlank(cvNode.getConfigVar().getValue())) {
            String strValue = cvNode.getConfigVar().getValue();
            try {
                value = Integer.valueOf(strValue);
            }
            catch (NumberFormatException ex) {
                LOGGER.warn("Parse value failed: {}", strValue);
            }
        }
        return value;
    }

    @Override
    public CvNode getCvNode() {
        return gbm16TReverserValueBean.getCvNodeMaster();
    }
}
