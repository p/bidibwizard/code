package org.bidib.wizard.mvc.main.view.panel.glazed;

import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.main.model.FeedbackPosition;

import ca.odell.glazedlists.gui.TableFormat;

public class FeedbackPositionTableFormat implements TableFormat<FeedbackPosition> {

    private final String[] columnNames =
        { Resources.getString(FeedbackPositionTableFormat.class, "column.decoder"),
            Resources.getString(FeedbackPositionTableFormat.class, "column.location"),
            Resources.getString(FeedbackPositionTableFormat.class, "column.lastseen") };

    public int getColumnCount() {
        return 3;
    }

    public String getColumnName(int column) {

        switch (column) {
            case 0:
            case 1:
            case 2:
                return columnNames[column];
            default:
                throw new IllegalStateException();
        }
    }

    public Object getColumnValue(FeedbackPosition feedbackPosition, int column) {

        switch (column) {
            case 0:
                return feedbackPosition.getDecoderAddress();
            case 1:
                return feedbackPosition.getLocationId();
            case 2:
                return feedbackPosition.getLastSeenTimestamp();
            default:
                throw new IllegalStateException();
        }
    }
}
