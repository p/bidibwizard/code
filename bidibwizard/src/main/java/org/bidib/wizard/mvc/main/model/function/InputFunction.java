package org.bidib.wizard.mvc.main.model.function;

import org.bidib.jbidibc.exchange.lcmacro.InputPortActionType;
import org.bidib.jbidibc.exchange.lcmacro.InputPortPoint;
import org.bidib.jbidibc.exchange.lcmacro.LcMacroPointType;
import org.bidib.wizard.comm.InputStatus;
import org.bidib.wizard.mvc.main.model.InputPort;

public class InputFunction extends SystemFunction<InputStatus> {
    private InputPort input;

    public InputFunction() {
        this(InputStatus.QUERY0);
    }

    public InputFunction(InputStatus action) {
        this(action, null);
    }

    public InputFunction(InputStatus action, InputPort input) {
        super(action, KEY_INPUT);
        this.input = input;
    }

    public InputPort getInput() {
        return input;
    }

    public void setInput(InputPort input) {
        this.input = input;
    }

    public String getDebugString() {
        int id = 0;

        if (getInput() != null) {
            id = getInput().getId();
        }
        return "Input=" + getAction().name().substring(5) + "? Port=" + id;
    }

    @Override
    public LcMacroPointType toLcMacroPoint() {
        InputPortPoint inputPortPoint = new InputPortPoint();
        inputPortPoint.setInputNumber(getInput().getId());
        inputPortPoint.setInputPortActionType(InputPortActionType.fromValue(getAction().name()));
        return inputPortPoint;
    }
}
