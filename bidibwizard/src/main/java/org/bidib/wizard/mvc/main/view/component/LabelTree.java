package org.bidib.wizard.mvc.main.view.component;

import java.awt.Point;
import java.beans.PropertyVetoException;
import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.swing.ListCellRenderer;
import javax.swing.ToolTipManager;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import org.jdesktop.swingx.JXTree;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LabelTree<T extends Serializable> extends JXTree implements LabeledDisplayItems<T> {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(LabelTree.class);

    private final Set<ListSelectionListener> listSelectionListeners = new LinkedHashSet<ListSelectionListener>();

    protected static class LabelTreeModel extends DefaultTreeModel {
        private static final long serialVersionUID = 1L;

        public LabelTreeModel(TreeNode root) {
            super(root);
        }
    }

    public LabelTree() {
        this(new LabelTreeModel(new DefaultMutableTreeNode()));
    }

    public LabelTree(TreeModel model) {
        super(model);

        // add the tree selection listener that reacts on selection of tree items
        getSelectionModel().addTreeSelectionListener(new TreeSelectionListener() {

            @Override
            public void valueChanged(TreeSelectionEvent e) {
                LOGGER.debug("Tree selection has changed: {}", e);

                // for (ListSelectionListener listSelListener : listSelectionListeners) {
                //
                // listSelListener.valueChanged(new ListSelectionEvent(LabelTree.this, getRowForPath(e
                // .getNewLeadSelectionPath()), getRowForPath(e.getOldLeadSelectionPath()), false));
                // }
            }
        });

        ToolTipManager.sharedInstance().registerComponent(this);
    }

    @Override
    public void addListSelectionListener(ListSelectionListener listener) {
        listSelectionListeners.add(listener);
    }

    private int prevSelectedIndex = -1;

    @Override
    public void selectedValueChanged(int index) {

        int currentlySelectedIndex = prevSelectedIndex;

        try {
            fireVetoableChange("selectedNode", currentlySelectedIndex, index);

            LOGGER.info("The change of the selected node was not vetoed, perform the change.");
            for (ListSelectionListener listSelListener : listSelectionListeners) {
                listSelListener.valueChanged(new ListSelectionEvent(LabelTree.this, index, index, false));
            }

            prevSelectedIndex = index;
        }
        catch (PropertyVetoException ex) {
            LOGGER.warn("The selectedNode change was vetoed.", ex);
        }
    }

    @Override
    public T getElementAt(int index) {
        TreePath path = getPathForRow(index);
        if (path != null) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) path.getLastPathComponent();
            return (T) node.getUserObject();
        }
        return null;
    }

    @Override
    public T getElementAt(int x, int y) {
        return getElementAt(getRowForLocation(x, y));
    }

    @Override
    public int getIndex(Point point) {
        return getClosestRowForLocation(point.x, point.y);
    }

    @Override
    public int getItemSize() {
        return getRowCount();
    }

    @Override
    public int getSelectedIndex() {
        if (getSelectionPath() != null) {
            return getRowForPath(getSelectionPath());
        }
        return 0;
    }

    @Override
    public T getSelectedItem() {
        if (getSelectionPath() != null) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) getSelectionPath().getLastPathComponent();
            return (T) node.getUserObject();
        }
        return null;
    }

    @Override
    public Point indexToLocation(int index) {
        return getRowBounds(index).getLocation();
    }

    @Override
    public void refreshView() {
        ((DefaultTreeModel) getModel()).nodeChanged((TreeNode) getModel().getRoot());

        updateUI();
    }

    @Override
    public T selectElement(Point point) {
        setSelectionRow(getIndex(point));
        T selectedItem = getSelectedItem();
        grabFocus();

        return selectedItem;
    }

    @Override
    public void setSelectionMode(int selectionMode) {
    }

    @Override
    public void setItems(T[] items) {
        LOGGER.info("Set the items: {}", (Object[]) items);

        LabelTreeModel model = (LabelTreeModel) getModel();

        Object root = model.getRoot();
        if (root instanceof MutableTreeNode) {
            MutableTreeNode node = (MutableTreeNode) root;

            for (int i = node.getChildCount(); i > 0; i--) {
                LOGGER.info("remove child at index: {}", i - 1);
                node.remove(i - 1);
            }

            if (items == null) {
                LOGGER.info("reset the prev selected index");
                prevSelectedIndex = -1;
            }

            if (items != null) {
                for (int index = 0; index < items.length; index++) {
                    node.insert(new DefaultMutableTreeNode(items[index]), index);
                }
            }
        }

        setRootVisible(true);
        // expand root and make it invisible
        expandPath(getPathForRow(0));
        setRootVisible(false);

        for (int i = 0; i < getRowCount(); i++) {
            LOGGER.debug("Expanding row: {}", i);
            expandRow(i);
        }

        if (getParent() != null) {
            getParent().invalidate();
        }
    }

    @Override
    public String toString() {
        return getClass().getName();
    }

    @Override
    public void setCellRenderer(ListCellRenderer<? super T> cellRenderer) {

    }

    @Override
    public void setSelectedItem(T item) {
    }
}