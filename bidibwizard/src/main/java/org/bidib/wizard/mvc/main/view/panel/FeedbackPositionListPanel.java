package org.bidib.wizard.mvc.main.view.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.apache.commons.lang3.StringUtils;
import org.bidib.jbidibc.core.Feature;
import org.bidib.jbidibc.core.enumeration.FeatureEnum;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.main.controller.FeedbackPositionPanelController;
import org.bidib.wizard.mvc.main.model.FeedbackPosition;
import org.bidib.wizard.mvc.main.model.FeedbackPositionModel;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.view.panel.glazed.DecodersSelect;
import org.bidib.wizard.mvc.main.view.panel.glazed.FeedbackPositionComparator;
import org.bidib.wizard.mvc.main.view.panel.glazed.FeedbackPositionTableFormat;
import org.bidib.wizard.mvc.main.view.panel.listener.TabVisibilityListener;
import org.bidib.wizard.mvc.main.view.panel.listener.TabVisibilityProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.debug.FormDebugPanel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.jidesoft.grid.CategorizedTable;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.FilterList;
import ca.odell.glazedlists.SortedList;
import ca.odell.glazedlists.matchers.ThreadedMatcherEditor;
import ca.odell.glazedlists.swing.AdvancedTableModel;
import ca.odell.glazedlists.swing.GlazedListsSwing;
import ca.odell.glazedlists.swing.TableComparatorChooser;

public class FeedbackPositionListPanel implements TabVisibilityProvider {
    private static final Logger LOGGER = LoggerFactory.getLogger(FeedbackPositionListPanel.class);

    private static final long serialVersionUID = 1L;

    private final FeedbackPositionPanelController controller;

    private final FeedbackPositionModel feedbackPositionModel;

    private final TabVisibilityListener tabVisibilityListener;

    private DecodersSelect decodersSelect;

    private static final String ENCODED_FILTER_PANEL_COLUMN_SPECS = "70dlu:grow";

    private static final String ENCODED_FILTER_PANEL_ROW_SPECS = "pref, $lg, fill:50dlu:grow, $lg, pref, $lg, pref";

    private static final String ENCODED_DIALOG_COLUMN_SPECS = "pref, 3dlu, fill:50dlu:grow";

    private static final String ENCODED_DIALOG_ROW_SPECS = "fill:50dlu:grow";

    private JPanel contentPanel;

    public FeedbackPositionListPanel(final FeedbackPositionPanelController controller,
        final FeedbackPositionModel feedbackPositionModel, MainModel model,
        final TabVisibilityListener tabVisibilityListener) {

        this.controller = controller;
        this.feedbackPositionModel = feedbackPositionModel;
        this.tabVisibilityListener = tabVisibilityListener;
        LOGGER.debug("Create new FeedbackPositionListPanel.");

        DefaultFormBuilder dialogBuilderFilter = null;
        boolean debugDialog = false;
        if (debugDialog) {
            JPanel panel = new FormDebugPanel();
            dialogBuilderFilter =
                new DefaultFormBuilder(
                    new FormLayout(ENCODED_FILTER_PANEL_COLUMN_SPECS, ENCODED_FILTER_PANEL_ROW_SPECS), panel);
        }
        else {
            JPanel panel = new JPanel(new BorderLayout());
            dialogBuilderFilter =
                new DefaultFormBuilder(
                    new FormLayout(ENCODED_FILTER_PANEL_COLUMN_SPECS, ENCODED_FILTER_PANEL_ROW_SPECS), panel);
        }

        DefaultFormBuilder dialogBuilder = null;
        // boolean debugDialog = false;
        if (debugDialog) {
            JPanel panel = new FormDebugPanel();
            dialogBuilder =
                new DefaultFormBuilder(new FormLayout(ENCODED_DIALOG_COLUMN_SPECS, ENCODED_DIALOG_ROW_SPECS), panel);
        }
        else {
            JPanel panel = new JPanel(new BorderLayout());
            dialogBuilder =
                new DefaultFormBuilder(new FormLayout(ENCODED_DIALOG_COLUMN_SPECS, ENCODED_DIALOG_ROW_SPECS), panel);
        }
        dialogBuilder.border(Borders.TABBED_DIALOG);

        EventList<FeedbackPosition> feedbackPositionEventList = feedbackPositionModel.getPositionsEventList();
        SortedList<FeedbackPosition> sortedFeedbackPositions =
            new SortedList<FeedbackPosition>(feedbackPositionEventList, new FeedbackPositionComparator());

        decodersSelect = new DecodersSelect(sortedFeedbackPositions);

        // create the filter list
        FilterList<FeedbackPosition> filteredFeedbackPositions =
            new FilterList<FeedbackPosition>(sortedFeedbackPositions,
                new ThreadedMatcherEditor<FeedbackPosition>(decodersSelect));

        AdvancedTableModel<FeedbackPosition> positionsTableModel =
            GlazedListsSwing.eventTableModelWithThreadProxyList(filteredFeedbackPositions,
                new FeedbackPositionTableFormat());

        CategorizedTable positionsJTable = new CategorizedTable(positionsTableModel) {
            private static final long serialVersionUID = 1L;

            protected void paintComponent(Graphics g) {
                super.paintComponent(g);

                if (positionsTableModel.getRowCount() == 0) {
                    if (StringUtils.isNotBlank(getEmptyTableText())) {
                        Graphics2D g2d = (Graphics2D) g;

                        g2d.setColor(Color.BLACK);
                        g2d.drawString(getEmptyTableText(), 10, 20);
                    }
                }
            }

            public Dimension getPreferredScrollableViewportSize() {
                return getPreferredSize();
            }

            public boolean getScrollableTracksViewportHeight() {
                return getPreferredSize().height < getParent().getHeight();
            }

        };

        // do not remove sorter, otherwise sorting will no longer work
        TableComparatorChooser<FeedbackPosition> tableSorter =
            TableComparatorChooser.install(positionsJTable, sortedFeedbackPositions,
                TableComparatorChooser.MULTIPLE_COLUMN_MOUSE);
        // set the initial sorting
        tableSorter.fromString("column 2 reversed");

        JScrollPane positionsTableScrollPane = new JScrollPane(positionsJTable);

        // create the decoders list
        JList<String> decodersJList = decodersSelect.getJList();
        JScrollPane decodersListScrollPane = new JScrollPane(decodersJList);

        final JButton clearButton = new JButton(Resources.getString(FeedbackPositionListPanel.class, "label.clear"));
        clearButton.setToolTipText(Resources.getString(FeedbackPositionListPanel.class, "tooltip.clear"));
        clearButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                decodersJList.clearSelection();
            }
        });

        dialogBuilderFilter.add(new JLabel(Resources.getString(FeedbackPositionListPanel.class, "label.decoder")),
            new CellConstraints(1, 1));
        dialogBuilderFilter.add(decodersListScrollPane, new CellConstraints(1, 3));
        dialogBuilderFilter.add(clearButton, new CellConstraints(1, 5));

        dialogBuilder.append(dialogBuilderFilter.build());

        dialogBuilder.append(positionsTableScrollPane);

        contentPanel = dialogBuilder.build();
        // the name is displayed on the tab
        contentPanel.setName(getName());
    }

    public JPanel getComponent() {
        return contentPanel;
    }

    private String getEmptyTableText() {
        return Resources.getString(FeedbackPositionListPanel.class, "emptyTable");
    }

    public String getName() {
        return Resources.getString(getClass(), "name");
    }

    @Override
    public boolean isTabVisible() {
        Node node = feedbackPositionModel.getSelectedNode();
        if (node != null) {
            boolean isTabVisible = hasFeedbackPositions(node.getNode().getFeatures());
            LOGGER.info("Check if tab is visible: {}", isTabVisible);
            return isTabVisible;
        }
        return false;
    }

    public void listChanged() {
        LOGGER.info("List has changed, remove all rows and add rows again.");

        tabVisibilityListener.setTabVisible(contentPanel, isTabVisible());
    }

    private boolean hasFeedbackPositions(Collection<Feature> features) {

        Feature feedbackPositions = Feature.findFeature(features, FeatureEnum.FEATURE_BM_POSITION_ON.getNumber());
        return (feedbackPositions != null && feedbackPositions.getValue() > 0);
    }
}
