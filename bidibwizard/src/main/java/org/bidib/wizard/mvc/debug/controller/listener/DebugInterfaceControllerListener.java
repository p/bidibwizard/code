package org.bidib.wizard.mvc.debug.controller.listener;

public interface DebugInterfaceControllerListener {
    /**
     * Close the view.
     */
    void viewClosed();

}
