package org.bidib.wizard.mvc.main.view.table;

import javax.swing.table.AbstractTableModel;

import com.jidesoft.grid.TreeTable;

public class CvDefinitionJideTreeTable extends TreeTable {
    private static final long serialVersionUID = 1L;

    /**
     * Constructs a CvDefinitionJideTreeTable.
     */
    public CvDefinitionJideTreeTable() {
        super();
    }

    /**
     * Constructs a Jide TreeTable.
     * 
     * @param treeModel
     *            model for the TreeTable
     */
    public CvDefinitionJideTreeTable(AbstractTableModel treeModel) {
        super(treeModel);
    }
}
