package org.bidib.wizard.mvc.main.model;

import java.util.Collection;
import java.util.LinkedList;

import javax.swing.table.DefaultTableModel;

import org.apache.commons.lang3.StringUtils;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.main.model.listener.AccessoryPortListener;
import org.bidib.wizard.mvc.main.view.table.listener.ButtonListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is the table model for the accessory aspects table.
 * 
 */
public class AccessoryTableModel extends DefaultTableModel implements ButtonListener {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(AccessoryTableModel.class);

    public static final int COLUMN_LABEL = 0;

    public static final int COLUMN_MACRO = 1;

    public static final int COLUMN_ACCESSORY_ASPECT_INSTANCE = 2;

    private static final String[] COLUMN_NAMES =
        new String[] { Resources.getString(AccessoryTableModel.class, "aspect"),
            Resources.getString(AccessoryTableModel.class, "macro"),
            Resources.getString(AccessoryTableModel.class, "test") };

    private final Collection<AccessoryPortListener> portListeners = new LinkedList<>();

    private final MainModel model;

    private Accessory selectedAccessory;

    public AccessoryTableModel(final MainModel model, final AccessoryStartupAspectModel accessoryStartupAspectModel) {
        this.model = model;

        setColumnIdentifiers(COLUMN_NAMES);
    }

    public void addPortListener(AccessoryPortListener l) {
        portListeners.add(l);
    }

    public void setSelectedAccessory(Accessory selectedAccessory) {
        this.selectedAccessory = selectedAccessory;
    }

    public Accessory getSelectedAccessory() {
        return selectedAccessory;
    }

    public void addRow(AccessoryAspect accessoryAspect) {

        if (accessoryAspect != null) {
            Object[] rowData = new Object[COLUMN_NAMES.length];

            rowData[COLUMN_LABEL] = accessoryAspect.toString();
            if (accessoryAspect instanceof AccessoryAspectMacro) {
                rowData[COLUMN_MACRO] = ((AccessoryAspectMacro) accessoryAspect).getMacro();
            }
            rowData[COLUMN_ACCESSORY_ASPECT_INSTANCE] = accessoryAspect;

            LOGGER.info("Add new row: {}", rowData);
            addRow(rowData);
        }

    }

    private void fireTestButtonPressed(int aspectId) {
        for (AccessoryPortListener l : portListeners) {
            l.testButtonPressed(aspectId);
        }
    }

    private void fireAspectLabelChanged(int aspectId) {
        for (AccessoryPortListener l : portListeners) {
            l.labelChanged(aspectId);
        }
    }

    private Object getRowData(int row, int column, Macro macro) {
        Object result = null;

        switch (column) {
            case COLUMN_LABEL:
                result = Resources.getString(getClass(), "aspect") + "_" + row;
                break;
            case COLUMN_MACRO:
                result = macro;
                break;
            case COLUMN_ACCESSORY_ASPECT_INSTANCE:
                result = super.getValueAt(row, column);
                break;
        }
        return result;
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        boolean isEditable = false;
        switch (column) {
            case COLUMN_MACRO /* macro */:
                Accessory accessory = selectedAccessory;
                if (accessory != null) {
                    isEditable = accessory.isMacroMapped();

                    if (isEditable) {
                        try {
                            AccessoryAspect aspect =
                                (AccessoryAspect) getRowData(row, COLUMN_ACCESSORY_ASPECT_INSTANCE, null);
                            if (aspect instanceof AccessoryAspectMacro) {
                                isEditable = !((AccessoryAspectMacro) aspect).isImmutableAccessory();
                                LOGGER.info("The current aspect is immutable: {}", !isEditable);
                            }
                        }
                        catch (Exception ex) {
                            LOGGER.warn("Check if aspect is immutable failed.", ex);
                        }
                    }
                }
                break;
            case COLUMN_ACCESSORY_ASPECT_INSTANCE:
                isEditable = true;
                break;
            default:
                isEditable = true;
                break;
        }

        return isEditable;
    }

    @Override
    public void setValueAt(Object value, int row, int column) {
        switch (column) {
            case COLUMN_LABEL:

                if (value != null && StringUtils.isBlank(value.toString())) {
                    value = null;
                }

                super.setValueAt(value, row, column);

                // keep the label in the AccessoryAspectsLabels
                fireAspectLabelChanged(row);

                break;
            case COLUMN_MACRO:
                if (value instanceof Macro) {
                    // set the new macro directly in the selected accessory of the main model
                    model.getSelectedAccessory().setMacro(row, ((Macro) value).getId());
                }
                super.setValueAt(value, row, column);
                break;
            case COLUMN_ACCESSORY_ASPECT_INSTANCE:
                // fireTestButtonPressed(row);
                break;
            default:
                super.setValueAt(value, row, column);
        }
    }

    @Override
    public void buttonPressed(int row, int column) {
        LOGGER.info("The button was pressed, row: {}, column: {}", row, column);
        if (column == COLUMN_ACCESSORY_ASPECT_INSTANCE) {
            fireTestButtonPressed(row);
        }
    }
}
