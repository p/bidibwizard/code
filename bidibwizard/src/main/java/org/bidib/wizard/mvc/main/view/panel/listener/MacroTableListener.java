package org.bidib.wizard.mvc.main.view.panel.listener;

import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.mvc.main.model.function.Function;

public interface MacroTableListener {
    void copy(Function<? extends BidibStatus>[] functions);

    void cut(int[] rows, Function<? extends BidibStatus>[] functions);

    void delete(int[] rows);

    void insertEmptyAfter(int row);

    void insertEmptyBefore(int row);

    void pasteAfter(int row);

    void pasteBefore(int row);

    void pasteInvertedAfter(int row);
}
