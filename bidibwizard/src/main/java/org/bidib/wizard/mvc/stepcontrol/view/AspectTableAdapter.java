package org.bidib.wizard.mvc.stepcontrol.view;

import javax.swing.ListModel;

import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.stepcontrol.model.StepControlAspect;
import org.bidib.wizard.mvc.stepcontrol.model.StepControlAspect.Polarity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.adapter.AbstractTableAdapter;

public class AspectTableAdapter extends AbstractTableAdapter<StepControlAspect> {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(AspectTableAdapter.class);

    // public static final int COLUMN_ASPECT = 0;

    public static final int COLUMN_POSITION = 0;

    public static final int COLUMN_ANGLE = 1;

    public static final int COLUMN_POLARITY = 2;

    public static final int COLUMN_PERFORM_ASPECT = 3;

    private String activateLabel = "Activate";

    public AspectTableAdapter(ListModel<StepControlAspect> listModel, String[] columnNames) {
        super(listModel, columnNames);

        activateLabel = Resources.getString(AspectTableAdapter.class, "activate");
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        StepControlAspect stepControlAspect = (StepControlAspect) getRow(rowIndex);
        switch (columnIndex) {
            // case COLUMN_ASPECT:
            // // return stepControlAspect.getAspectNumber();
            // return "-";
            case COLUMN_POSITION:
            case COLUMN_ANGLE:
                return ((long) stepControlAspect.getPosition()) & 0xffffffffL;
            case COLUMN_POLARITY:
                return (stepControlAspect.getPolarity() == Polarity.normal ? true : false);
            case COLUMN_PERFORM_ASPECT:
                return activateLabel;
            default:
                break;
        }
        return stepControlAspect;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case COLUMN_POSITION:
                return Long.class;
            case COLUMN_ANGLE:
                return Float.class;
            default:
                break;
        }
        return super.getColumnClass(columnIndex);
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        if (columnIndex == COLUMN_ANGLE) {
            return false;
        }
        return true;
    }

    @Override
    public void setValueAt(Object value, int rowIndex, int columnIndex) {
        LOGGER.info("Set the value: {}, rowIndex: {}", value, rowIndex);
        StepControlAspect stepControlAspect = (StepControlAspect) getRow(rowIndex);
        boolean notifyChange = false;
        switch (columnIndex) {
            // case COLUMN_ASPECT:
            // if (value instanceof String) {
            // // stepControlAspect.setAspectNumber(aspectNumber);
            // }
            // break;
            case COLUMN_POSITION:
                if (value instanceof Integer) {
                    Integer val = (Integer) value;
                    stepControlAspect.setPosition(val.intValue());

                    notifyChange = true;
                }
                break;
            case COLUMN_POLARITY:
                if (value instanceof Boolean) {
                    Boolean val = (Boolean) value;
                    stepControlAspect.setPolarity(val.booleanValue() ? Polarity.normal : Polarity.inverted);

                    notifyChange = true;
                }
                break;
            default:
                break;
        }

        super.setValueAt(value, rowIndex, columnIndex);

        if (notifyChange) {
            fireTableDataChanged();
        }
    }
}
