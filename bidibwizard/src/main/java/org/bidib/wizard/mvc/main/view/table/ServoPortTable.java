package org.bidib.wizard.mvc.main.view.table;

import java.awt.event.MouseEvent;

import javax.swing.event.ChangeEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;

import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.common.view.slider.SliderValueChangeListener;
import org.bidib.wizard.mvc.main.model.ServoPort;
import org.bidib.wizard.mvc.main.model.ServoPortTableModel;
import org.bidib.wizard.mvc.main.view.panel.ServoPortListPanel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServoPortTable extends PortTable {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ServoPortTable.class);

    private final TwoDimensionalArray<ServoSliderEditor> sliderEditors = new TwoDimensionalArray<ServoSliderEditor>();

    public ServoPortTable(final TableModel servoPortTableModel, String emptyTableText) {
        super(servoPortTableModel, emptyTableText);
    }

    @Override
    public String getToolTipText(MouseEvent e) {
        String tip = null;
        java.awt.Point p = e.getPoint();
        int rowIndex = rowAtPoint(p);
        int colIndex = columnAtPoint(p);

        if (rowIndex == -1 || colIndex == -1) {
            return tip;
        }

        int realColumnIndex = convertColumnIndexToModel(colIndex);

        if (realColumnIndex == ServoPortTableModel.COLUMN_VALUE) {

            ServoPort port = (ServoPort) getModel().getValueAt(rowIndex, ServoPortTableModel.COLUMN_PORT_INSTANCE);
            if (port.isEnabled()) {
                tip = prepareTooltip(port, port.getRelativeValue());
            }
            else {
                tip = super.getToolTipText();
            }

        }
        else {
            tip = super.getToolTipText();
        }
        return tip;
    }

    private String prepareTooltip(ServoPort port, int value) {
        LOGGER.trace("Set the new value: {}", value);
        int range = port.getTrimUp() - port.getTrimDown();
        float factor = (float) range / 100;
        float targetValue = (factor * value) + port.getTrimDown();
        LOGGER.trace("Calculated range: {}, factor: {}, targetValue: {}", range, factor, targetValue);
        int targetAbsolute = Math.round(targetValue);

        StringBuffer tip = new StringBuffer(Resources.getString(ServoPortListPanel.class, "absolutevalue"));
        tip.append(" ").append(targetAbsolute);
        tip.append(", relative value: ").append(value);
        return tip.toString();
    }

    public TableCellEditor getCellEditor(int row, int column) {
        TableCellEditor result = null;

        switch (column) {
            case ServoPortTableModel.COLUMN_SPEED:
                result = new ServoPortTableCellEditor(0, 255);
                break;
            case ServoPortTableModel.COLUMN_TRIM_DOWN:
            case ServoPortTableModel.COLUMN_TRIM_UP:
                result = new ServoPortTableCellEditor(0, 255);
                break;
            case ServoPortTableModel.COLUMN_VALUE:
                result = sliderEditors.get(row, column);
                break;
            default:
                result = super.getCellEditor(row, column);
                break;
        }
        return result;
    }

    public TableCellRenderer getCellRenderer(final int row, int column) {
        TableCellRenderer result = null;

        if (column == ServoPortTableModel.COLUMN_VALUE) {
            LOGGER.debug("Get cell renderer for row: {}", row);
            result = sliderEditors.get(row, column);

            ServoPort servoPort = (ServoPort) getModel().getValueAt(row, ServoPortTableModel.COLUMN_PORT_INSTANCE);

            if (result == null) {
                LOGGER.debug("Prepare new sliderEditor with initial value: {}, servoPort: {}",
                    servoPort.getRelativeValue(), servoPort);
                final ServoSliderEditor sliderEditor = new ServoSliderEditor(servoPort, 0, 100);
                sliderEditor.setRowNumber(row);
                sliderEditor.createComponent(servoPort.getRelativeValue());

                final SliderValueChangeListener sliderValueChangeListener = new SliderValueChangeListener() {

                    @Override
                    public void stateChanged(ChangeEvent e, boolean isAdjusting, int value) {
                        // only handle if not adjusting
                        if (!isAdjusting) {
                            ServoPort servoPort =
                                (ServoPort) getModel().getValueAt(sliderEditor.getRowNumber(),
                                    getModel().getColumnCount() - 1);
                            LOGGER.debug("Update servo port: {}, row: {}", servoPort.getDebugString(),
                                sliderEditor.getRowNumber());

                            sliderStateChanged(servoPort);
                        }
                    }
                };
                sliderEditor.setSliderValueChangeListener(sliderValueChangeListener);

                LOGGER.debug("Store new sliderEditor: {}, row: {}, column: {}", sliderEditor, row, column);
                sliderEditors.set(sliderEditor, row, column);
                result = sliderEditor;
            }
            if (result != null) {
                ServoSliderEditor sliderEditor = (ServoSliderEditor) result;
                sliderEditor.setEnabled(servoPort.isEnabled());
            }
            LOGGER.debug("Return sliderEditor: {}, row: {}", result, row);
        }
        else {
            result = super.getCellRenderer(row, column);
        }
        return result;
    }

    @Override
    public void clearTable() {
        LOGGER.debug("clearTable, remove all rows and remove all slider editors");
        // remove all rows and remove all slider editors
        for (int row = 0; row < getModel().getRowCount(); row++) {
            SliderEditor current = sliderEditors.get(row, ServoPortTableModel.COLUMN_VALUE);
            if (current != null) {
                current.setSliderValueChangeListener(null);
            }
        }

        ((DefaultTableModel) getModel()).setRowCount(0);
        sliderEditors.clear();
    }

    public void updateSliderPosition(int row, int column, int value) {
        SliderEditor current = sliderEditors.get(row, column);
        if (current != null) {
            LOGGER.info("updateSliderPosition, set the new slider value: {}", value);
            current.setValue(value);
        }
    }

    protected void sliderStateChanged(ServoPort servoPort) {
    }

}
