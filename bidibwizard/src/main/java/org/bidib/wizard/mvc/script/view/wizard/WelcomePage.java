package org.bidib.wizard.mvc.script.view.wizard;

import org.bidib.wizard.common.locale.Resources;

import com.jidesoft.wizard.WelcomeWizardPage;

public class WelcomePage extends WelcomeWizardPage {
    private static final long serialVersionUID = 1L;

    public WelcomePage() {
        super(Resources.getString(WelcomePage.class, "title"), Resources.getString(WelcomePage.class, "description"));
    }

    @Override
    protected void initContentPane() {
        super.initContentPane();
        addText(Resources.getString(WelcomePage.class, "content.text"));
        addSpace();
        addText(Resources.getString(WelcomePage.class, "content.continue"));
    }
}