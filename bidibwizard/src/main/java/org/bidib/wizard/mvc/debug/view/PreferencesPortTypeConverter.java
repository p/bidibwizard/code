package org.bidib.wizard.mvc.debug.view;

import org.bidib.wizard.mvc.common.model.PreferencesPortType;

import com.jgoodies.binding.value.BindingConverter;

public class PreferencesPortTypeConverter implements BindingConverter<PreferencesPortType, String> {

    @Override
    public String targetValue(PreferencesPortType sourceValue) {
        if (sourceValue != null) {
            return sourceValue.getConnectionName();
        }
        return null;
    }

    @Override
    public PreferencesPortType sourceValue(String targetValue) {
        return null;
    }
}
