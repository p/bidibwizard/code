package org.bidib.wizard.mvc.dmx.view.panel;

import org.bidib.wizard.mvc.dmx.view.panel.axis.LabeledNumberTickUnit;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.event.AxisChangeEvent;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.ValueAxisPlot;
import org.jfree.data.Range;
import org.jfree.data.RangeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AutoRangeLimitedValueAxis extends NumberAxis {
    private static final Logger LOGGER = LoggerFactory.getLogger(AutoRangeLimitedValueAxis.class);

    private static final long serialVersionUID = 1L;

    private boolean isFixedZoomAxis;

    public AutoRangeLimitedValueAxis() {
        super();
    }

    public AutoRangeLimitedValueAxis(String label) {
        super(label);
    }

    /**
     * @return the isFixedZoomAxis
     */
    public boolean isFixedZoomAxis() {
        return isFixedZoomAxis;
    }

    /**
     * @param isFixedZoomAxis
     *            the isFixedZoomAxis to set
     */
    public void setFixedZoomAxis(boolean isFixedZoomAxis) {
        this.isFixedZoomAxis = isFixedZoomAxis;
    }

    protected Range calcAutoRange() {
        LOGGER.info("calcAutoRange, label: {}, range: {}", this.getLabel(), this.getRange());

        Plot plot = getPlot();
        if (plot == null) {
            return getDefaultAutoRange(); // no plot, no data
        }

        // if (!isAutoRange()) {
        // LOGGER.info("No auto-range allowed.");
        // return getRange();
        // }

        if (plot instanceof ValueAxisPlot) {
            ValueAxisPlot vap = (ValueAxisPlot) plot;

            // get the range from the current data points
            Range r = vap.getDataRange(this);
            LOGGER.info("Fetched dataRange: {}", r);
            if (r == null) {
                r = getDefaultAutoRange();
            }

            double upper = r.getUpperBound();
            double lower = r.getLowerBound();
            if (getRangeType() == RangeType.POSITIVE) {
                lower = Math.max(0.0, lower);
                upper = Math.max(0.0, upper);
            }
            else if (getRangeType() == RangeType.NEGATIVE) {
                lower = Math.min(0.0, lower);
                upper = Math.min(0.0, upper);
            }

            if (getAutoRangeIncludesZero()) {
                lower = Math.min(lower, 0.0);
                upper = Math.max(upper, 0.0);
            }
            double range = upper - lower;

            // if fixed auto range, then derive lower bound...
            double fixedAutoRange = getFixedAutoRange();
            if (fixedAutoRange < 0.0) {
                lower = upper + fixedAutoRange;
                // if (fixedAutoRange > 0.0) {
                // lower = upper - fixedAutoRange;
                LOGGER.info("fixedAutoRange: {}, lower: {}, upper: {}", fixedAutoRange, lower, upper);
            }
            else {
                // ensure the autorange is at least <minRange> in size...
                double minRange = getAutoRangeMinimumSize();
                LOGGER.info("Calculated minRange: {}, range: {}", minRange, range);
                if (range < minRange) {
                    double expand = (minRange - range) / 2;
                    upper = upper + expand;
                    lower = lower - expand;
                    if (lower == upper) { // see bug report 1549218
                        double adjust = Math.abs(lower) / 10.0;
                        lower = lower - adjust;
                        upper = upper + adjust;
                    }
                    if (getRangeType() == RangeType.POSITIVE) {
                        if (lower < 0.0) {
                            upper = upper - lower;
                            lower = 0.0;
                        }
                    }
                    else if (getRangeType() == RangeType.NEGATIVE) {
                        if (upper > 0.0) {
                            lower = lower - upper;
                            upper = 0.0;
                        }
                    }
                }

                if (getAutoRangeStickyZero()) {
                    if (upper <= 0.0) {
                        upper = Math.min(0.0, upper + getUpperMargin() * range);
                    }
                    else {
                        upper = upper + getUpperMargin() * range;
                    }
                    if (lower >= 0.0) {
                        lower = Math.max(0.0, lower - getLowerMargin() * range);
                    }
                    else {
                        lower = lower - getLowerMargin() * range;
                    }
                }
                else {
                    upper = upper + getUpperMargin() * range;
                    lower = lower - getLowerMargin() * range;
                }
            }

            LOGGER.info("calcAutoRange, return lower: {}, upper: {}", lower, upper);

            return new Range(lower, upper);
        }
        return getDefaultAutoRange();
    }

    @Override
    public void zoomRange(double lowerPercent, double upperPercent) {
        LOGGER.info("zoomRange, lowerPercent: {}, upperPercent: {}", lowerPercent, upperPercent);

        if (isFixedZoomAxis()) {
            LOGGER.info("Current axis has fixed zoom, no calculation is needed.");
            return;
        }

        Range r = getRange();
        // Range autoRange = calcAutoRange();

        double start = r.getLowerBound();
        double length = r.getLength();

        double lower;
        double upper;

        if (isInverted()) {
            lower = start + (length * (1 - upperPercent));
            upper = start + (length * (1 - lowerPercent));
        }
        else {
            lower = start + length * lowerPercent;
            upper = start + length * upperPercent;
        }

        LOGGER.info("zoomRange, set new range, calculated lower: {}, upper: {}", lower, upper);

        setRange(lower, upper);
        //
        // double corrlower = autoRange.constrain(lower);
        // double corrupper = autoRange.constrain(upper);
        // LOGGER.info("zoomRange, calculated corrlower: {}, corrupper: {}", corrlower, corrupper);
        // if (corrlower >= corrupper) {
        // corrupper = corrlower + getAutoRangeMinimumSize();
        // LOGGER.info("zoomRange, calculated new corrupper: {}, autoRangeMinimumSize: {}", corrupper,
        // getAutoRangeMinimumSize());
        // }
        //
        // LOGGER.info("Set new range, lower: {}, upper: {}", corrlower, corrupper);
        // setRange(corrlower, corrupper);
    }

    /**
     * Increases or decreases the axis range by the specified percentage about the specified anchor value and sends an
     * {@link AxisChangeEvent} to all registered listeners.
     * <P>
     * To double the length of the axis range, use 200% (2.0). To halve the length of the axis range, use 50% (0.5).
     * 
     * @param percent
     *            the resize factor.
     * @param anchorValue
     *            the new central value after the resize.
     * @see #resizeRange(double)
     */
    @Override
    public void resizeRange(double percent, double anchorValue) {
        LOGGER.info("resizeRange, label: {}, percent: {}, anchorValue: {}", getLabel(), percent, anchorValue);

        if (isFixedZoomAxis()) {
            LOGGER.info("Current axis has fixed zoom, no calculation is needed.");
            return;
        }

        if (percent > 0.0) {
            double halfLength = getRange().getLength() * percent / 2;
            Range adjusted = new Range(anchorValue - halfLength, anchorValue + halfLength);
            if (adjusted.getLowerBound() < 0) {
                LOGGER.debug("Lower bound is below zero, ignore resize below zero.");
                adjusted = new Range(0, 2 * halfLength);
                // return;
            }
            setRange(adjusted.getLowerBound(), adjusted.getUpperBound());

            // Range autoRange = calcAutoRange();
            // double corrlower = autoRange.constrain(adjusted.getLowerBound());
            // double corrupper = autoRange.constrain(adjusted.getUpperBound());
            // if (corrlower >= corrupper) {
            // corrupper = corrlower + getAutoRangeMinimumSize();
            // }
            //
            // setRange(corrlower, corrupper);
        }
        else if (isAutoRange()) {
            setAutoRange(true);
        }
        else {
            // TODO check if this is really correct
            setRange(new Range(0.0, -1 * getFixedAutoRange()));
        }
    }

    // /**
    // * Increases or decreases the axis range by the specified percentage about the specified anchor value and sends an
    // * {@link AxisChangeEvent} to all registered listeners.
    // * <P>
    // * To double the length of the axis range, use 200% (2.0). To halve the length of the axis range, use 50% (0.5).
    // *
    // * @param percent
    // * the resize factor.
    // * @param anchorValue
    // * the new central value after the resize.
    // * @see #resizeRange(double)
    // * @since 1.0.13
    // */
    // @Override
    // public void resizeRange2(double percent, double anchorValue) {
    // LOGGER.info("resizeRange2, percent: {}, anchorValue: {}", percent, anchorValue);
    //
    // if (percent > 0.0) {
    // double left = anchorValue - getLowerBound();
    // double right = getUpperBound() - anchorValue;
    // Range adjusted = new Range(anchorValue - left * percent, anchorValue + right * percent);
    // setRange(adjusted.getLowerBound(), adjusted.getUpperBound());
    //
    // // Range autoRange = calcAutoRange();
    // // double corrlower = autoRange.constrain(adjusted.getLowerBound());
    // // double corrupper = autoRange.constrain(adjusted.getUpperBound());
    // // if (corrlower >= corrupper) {
    // // corrupper = corrlower + getAutoRangeMinimumSize();
    // // }
    // //
    // // setRange(corrlower, corrupper);
    // }
    // else {
    // setAutoRange(true);
    // }
    // }

    @Override
    public void pan(double percent) {
        LOGGER.debug("pan, percent: {}, label: {}, range: {}", percent, this.getLabel(), this.getRange());

        if (percent == 0.0d) {
            return;
        }

        Range r = getRange();
        // Range autoRange = calcAutoRange();

        // LOGGER.debug("pan, r: {}, autoRange: {}", r, autoRange);

        double length = r.getLength();
        double adj = length * percent;
        double lower = r.getLowerBound() + adj;
        double upper = r.getUpperBound() + adj;

        LOGGER.debug("pan 1, lower: {}, upper: {}, length: {}, adj: {}", lower, upper, length, adj);
        if (lower < 0) {
            lower = 0;
            upper = lower + length;
        }
        // if (lower < autoRange.getLowerBound()) {
        // // calc the max percentage that is possible to pan
        // double diff = autoRange.getLowerBound() - r.getLowerBound();
        // double perc = diff / r.getLength();
        // if (perc == 0.0) {
        // return;
        // }
        //
        // lower = autoRange.getLowerBound();
        // upper = r.getUpperBound() + r.getLength() * perc;
        // }
        // else if (upper > autoRange.getUpperBound()) {
        // // calc the max percentage that is possible to pan
        // double diff = r.getUpperBound() - autoRange.getUpperBound();
        // double perc = diff / r.getLength();
        // if (perc == 0.0) {
        // return;
        // }
        //
        // lower = r.getLowerBound() + r.getLength() * perc;
        // upper = autoRange.getUpperBound();
        // }

        LOGGER.debug("pan 2, lower: {}, upper: {}", lower, upper);

        // if (lower >= upper) {
        // // upper = lower + getAutoRangeMinimumSize();
        // upper = lower - getAutoRangeMinimumSize();
        // }
        //
        // LOGGER.debug("pan 3, set new range, lower: {}, upper: {}", lower, upper);
        //
        // if (lower < 0) {
        // lower = 0;
        // upper = autoRange.getUpperBound();
        // LOGGER.debug("lower is < 0, autofix upper: {}", upper);
        // }

        setRange(lower, upper);
    }

    @Override
    public String getLabel() {
        NumberTickUnit unit = getTickUnit();
        if (unit instanceof LabeledNumberTickUnit) {
            return ((LabeledNumberTickUnit) unit).getAxisLabel();
        }
        return super.getLabel();
    }
}
