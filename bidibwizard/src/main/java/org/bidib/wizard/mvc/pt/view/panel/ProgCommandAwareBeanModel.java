package org.bidib.wizard.mvc.pt.view.panel;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.bidib.jbidibc.core.enumeration.PtOperation;
import org.bidib.wizard.mvc.pt.view.command.PtOperationCommand;

import com.jgoodies.binding.beans.Model;

public class ProgCommandAwareBeanModel extends Model {
    private static final long serialVersionUID = 1L;

    public static final String PROPERTYNAME_CURRENT_OPERATION = "currentOperation";

    public static final String PROPERTYNAME_EXECUTION = "execution";

    public static final String PROPERTYNAME_EXECUTING_PROG_COMMAND = "executingProgCommand";

    public static final String PROPERTYNAME_PROG_COMMANDS = "progCommands";

    public static final String PROPERTYNAME_EXECUTED_PROG_COMMANDS = "executedProgCommands";

    private PtOperation currentOperation;

    private ExecutionType execution;

    private PtOperationCommand<? extends ProgCommandAwareBeanModel> executingProgCommand;

    private List<PtOperationCommand<? extends ProgCommandAwareBeanModel>> progCommands =
        new LinkedList<PtOperationCommand<? extends ProgCommandAwareBeanModel>>();

    private List<PtOperationCommand<? extends ProgCommandAwareBeanModel>> executedProgCommands =
        new LinkedList<PtOperationCommand<? extends ProgCommandAwareBeanModel>>();

    public enum ExecutionType {
        READ, WRITE;
    }

    public ProgCommandAwareBeanModel() {
    }

    /**
     * @return the currentOperation
     */
    public PtOperation getCurrentOperation() {
        return currentOperation;
    }

    /**
     * @param currentOperation
     *            the currentOperation to set
     */
    public void setCurrentOperation(PtOperation currentOperation) {
        PtOperation oldCurrentOperation = this.currentOperation;
        this.currentOperation = currentOperation;
        firePropertyChange(PROPERTYNAME_CURRENT_OPERATION, oldCurrentOperation, currentOperation);
    }

    /**
     * @return the execution
     */
    public ExecutionType getExecution() {
        return execution;
    }

    /**
     * @param execution
     *            the execution to set
     */
    public void setExecution(ExecutionType execution) {
        ExecutionType oldExecution = this.execution;
        this.execution = execution;
        firePropertyChange(PROPERTYNAME_EXECUTION, oldExecution, execution);
    }

    /**
     * @return the executingProgCommand
     */
    public PtOperationCommand<? extends ProgCommandAwareBeanModel> getExecutingProgCommand() {
        return executingProgCommand;
    }

    /**
     * @param executingProgCommand
     *            the executingProgCommand to set
     */
    public void setExecutingProgCommand(PtOperationCommand<? extends ProgCommandAwareBeanModel> executingProgCommand) {
        PtOperationCommand<? extends ProgCommandAwareBeanModel> oldExecutingProgCommand = this.executingProgCommand;
        this.executingProgCommand = executingProgCommand;
        firePropertyChange(PROPERTYNAME_EXECUTING_PROG_COMMAND, oldExecutingProgCommand, executingProgCommand);
    }

    /**
     * @return the progCommands
     */
    public List<PtOperationCommand<? extends ProgCommandAwareBeanModel>> getProgCommands() {
        return progCommands;
    }

    /**
     * @param progCommands
     *            the progCommands to set
     */
    public void setProgCommands(List<PtOperationCommand<? extends ProgCommandAwareBeanModel>> progCommands) {
        List<PtOperationCommand<? extends ProgCommandAwareBeanModel>> oldProgCommands = this.progCommands;
        this.progCommands = progCommands;
        firePropertyChange(PROPERTYNAME_PROG_COMMANDS, oldProgCommands, progCommands);
    }

    /**
     * @return the executedProgCommands
     */
    public List<PtOperationCommand<? extends ProgCommandAwareBeanModel>> getExecutedProgCommands() {
        return executedProgCommands;
    }

    /**
     * @param executedProgCommands
     *            the executedProgCommands to set
     */
    public void setExecutedProgCommands(
        List<PtOperationCommand<? extends ProgCommandAwareBeanModel>> executedProgCommands) {
        List<PtOperationCommand<? extends ProgCommandAwareBeanModel>> oldExecutedProgCommands =
            this.executedProgCommands;
        this.executedProgCommands = executedProgCommands;
        firePropertyChange(PROPERTYNAME_PROG_COMMANDS, oldExecutedProgCommands, executedProgCommands);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}