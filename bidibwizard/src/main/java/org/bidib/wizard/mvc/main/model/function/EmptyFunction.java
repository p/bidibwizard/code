package org.bidib.wizard.mvc.main.model.function;

import org.bidib.jbidibc.exchange.lcmacro.LcMacroPointType;
import org.bidib.wizard.comm.BidibStatus;

public class EmptyFunction extends Function<BidibStatus> {

    public EmptyFunction() {
        super(null, "empty");
    }

    @Override
    public String getDebugString() {
        return "EmptyFunction";
    }

    @Override
    public LcMacroPointType toLcMacroPoint() {
        return null;
    }

}
