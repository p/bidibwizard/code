package org.bidib.wizard.mvc.debug.model;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bidib.jbidibc.debug.LineEndingEnum;
import org.bidib.wizard.mvc.common.model.CommPort;

import com.jgoodies.binding.beans.Model;
import com.jgoodies.common.collect.ArrayListModel;

public class DebugInterfaceModel extends Model {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_COM_PORTS = "comPorts";

    public static final String PROPERTY_SELECTED_PORT = "selectedPort";

    public static final String PROPERTY_SEND_TEXT = "sendText";

    public static final String PROPERTY_BAUDRATE = "baudRate";

    public static final String PROPERTY_SEND_FILE = "sendFile";

    public static final String PROPERTY_CONNECTED = "connected";

    public static final String PROPERTY_DISCONNECTED = "disconnected";

    public static final String PROPERTY_TRANSFER_IN_PROGRESS = "transferInProgress";

    public static final String PROPERTY_TRANSMIT_ENABLED = "transmitEnabled";

    public static final String PROPERTY_LINE_ENDING = "lineEnding";

    public static final String PROPERTY_LOG_TO_FILE = "logToFile";

    public static final String PROPERTY_LOGFILE_NAME = "logFileName";

    private ArrayListModel<CommPort> commPorts = new ArrayListModel<>();

    private CommPort selectedPort;

    private Integer baudRate;

    private String sendText;

    private File sendFile;

    private boolean connected;

    private boolean transferInProgress;

    private boolean logToFile;

    private String logFileName;

    private LineEndingEnum lineEnding = LineEndingEnum.CRLF;

    public DebugInterfaceModel() {

    }

    public ArrayListModel<CommPort> getCommPortsListModel() {
        return commPorts;
    }

    public Collection<CommPort> getCommPorts() {
        return commPorts;
    }

    /**
     * Set the commPorts and notify the listeners
     * 
     * @param commPorts
     *            the commPorts to set
     */
    public void setCommPorts(List<CommPort> commPorts) {
        List<CommPort> oldValue = new ArrayList<>(this.commPorts);

        this.commPorts.clear();
        this.commPorts.addAll(commPorts);
        firePropertyChange(PROPERTY_COM_PORTS, oldValue, commPorts);
    }

    /**
     * @return the selectedPort
     */
    public CommPort getSelectedPort() {
        return selectedPort;
    }

    /**
     * @param selectedPort
     *            the selectedPort to set
     */
    public void setSelectedPort(CommPort selectedPort) {
        CommPort oldValue = this.selectedPort;

        this.selectedPort = selectedPort;
        firePropertyChange(PROPERTY_SELECTED_PORT, oldValue, selectedPort);
    }

    /**
     * @return the baudRate
     */
    public Integer getBaudRate() {
        return baudRate;
    }

    /**
     * @param baudRate
     *            the baudRate to set
     */
    public void setBaudRate(Integer baudRate) {
        Integer oldValue = this.baudRate;
        this.baudRate = baudRate;
        firePropertyChange(PROPERTY_BAUDRATE, oldValue, baudRate);
    }

    /**
     * @return the sendText
     */
    public String getSendText() {
        return sendText;
    }

    /**
     * @param sendText
     *            the sendText to set
     */
    public void setSendText(String sendText) {
        String oldValue = this.sendText;
        this.sendText = sendText;
        firePropertyChange(PROPERTY_SEND_TEXT, oldValue, sendText);
    }

    /**
     * @return the sendFile
     */
    public File getSendFile() {
        return sendFile;
    }

    /**
     * @param sendFile
     *            the sendFile to set
     */
    public void setSendFile(File sendFile) {
        File oldValue = this.sendFile;
        this.sendFile = sendFile;
        firePropertyChange(PROPERTY_SEND_FILE, oldValue, sendFile);
    }

    /**
     * @return the connected
     */
    public boolean isConnected() {
        return connected;
    }

    /**
     * @param connected
     *            the connected to set
     */
    public void setConnected(boolean connected) {
        boolean oldTransmitEnabled = isTransmitEnabled();
        boolean oldValue = this.connected;
        this.connected = connected;
        firePropertyChange(PROPERTY_CONNECTED, oldValue, connected);
        firePropertyChange(PROPERTY_DISCONNECTED, !oldValue, !connected);
        firePropertyChange(PROPERTY_TRANSMIT_ENABLED, oldTransmitEnabled, isTransmitEnabled());
    }

    /**
     * @return the disconnected state
     */
    public boolean isDisconnected() {
        return !connected;
    }

    /**
     * @param disconnected
     *            the connected to set
     */
    public void setDisconnected(boolean disconnected) {
        setConnected(!disconnected);
    }

    /**
     * @return the transferInProgress
     */
    public boolean isTransferInProgress() {
        return transferInProgress;
    }

    /**
     * @param transferInProgress
     *            the transferInProgress to set
     */
    public void setTransferInProgress(boolean transferInProgress) {
        boolean oldTransmitEnabled = isTransmitEnabled();
        boolean oldValue = this.transferInProgress;
        this.transferInProgress = transferInProgress;
        firePropertyChange(PROPERTY_TRANSFER_IN_PROGRESS, oldValue, transferInProgress);

        firePropertyChange(PROPERTY_TRANSMIT_ENABLED, oldTransmitEnabled, isTransmitEnabled());
    }

    public boolean isTransmitEnabled() {
        return isConnected() && !isTransferInProgress();
    }

    /**
     * @return the lineEnding
     */
    public LineEndingEnum getLineEnding() {
        return lineEnding;
    }

    /**
     * @param lineEnding
     *            the lineEnding to set
     */
    public void setLineEnding(LineEndingEnum lineEnding) {
        LineEndingEnum oldValue = this.lineEnding;
        this.lineEnding = lineEnding;
        firePropertyChange(PROPERTY_LINE_ENDING, oldValue, lineEnding);
    }

    /**
     * @return the logToFile
     */
    public boolean isLogToFile() {
        return logToFile;
    }

    /**
     * @param logToFile
     *            the logToFile to set
     */
    public void setLogToFile(boolean logToFile) {
        boolean oldValue = this.logToFile;
        this.logToFile = logToFile;
        firePropertyChange(PROPERTY_LOG_TO_FILE, oldValue, logToFile);
    }

    /**
     * @return the logFileName
     */
    public String getLogFileName() {
        return logFileName;
    }

    /**
     * @param logFileName
     *            the logFileName to set
     */
    public void setLogFileName(String logFileName) {
        String oldValue = this.logFileName;
        this.logFileName = logFileName;
        firePropertyChange(PROPERTY_LOGFILE_NAME, oldValue, logFileName);
    }
}
