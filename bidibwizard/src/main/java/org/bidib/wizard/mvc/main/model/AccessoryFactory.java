package org.bidib.wizard.mvc.main.model;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.lang3.StringUtils;
import org.bidib.jbidibc.core.enumeration.TimeBaseUnitEnum;
import org.bidib.jbidibc.exchange.lcmacro.LcAccessoryExporter;
import org.bidib.jbidibc.exchange.lcmacro.LcAccessorySwitchTime;
import org.bidib.jbidibc.exchange.lcmacro.LcAccessoryType;
import org.bidib.jbidibc.exchange.lcmacro.LcAspectPointType;
import org.bidib.jbidibc.exchange.lcmacro.TimeBaseUnitType;
import org.bidib.wizard.common.context.ApplicationContext;
import org.bidib.wizard.labels.AccessoryLabelUtils;
import org.bidib.wizard.labels.LabelType;
import org.bidib.wizard.labels.Labels;
import org.bidib.wizard.mvc.main.model.MacroFactory.ExportFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AccessoryFactory {
    private static final Logger LOGGER = LoggerFactory.getLogger(AccessoryFactory.class);

    private AccessoryFactory() {
    }

    public static void saveAccessory(
        String fileName, Accessory accessory, ExportFormat exportFormat, final ApplicationContext context)
        throws FileNotFoundException, IOException {
        saveAccessory(fileName, accessory, exportFormat, true, context);
    }

    public static void saveAccessory(
        String fileName, Accessory accessory, ExportFormat exportFormat, boolean gzip, final ApplicationContext context)
        throws FileNotFoundException, IOException {

        switch (exportFormat) {
            case jaxb:
                new AccessoryFactory().saveAccessoryWithJaxb(fileName, accessory, /* gzip */false, context);
                break;
            default:
                new AccessoryFactory().saveAccessoryWithXmlEncoder(fileName, accessory, gzip);
                break;
        }
    }

    public static Accessory loadAccessory(
        String fileName, ExportFormat exportFormat, PortsProvider portsProvider, final ApplicationContext context) {

        Accessory accessory = null;
        switch (exportFormat) {
            case jaxb:
                accessory = new AccessoryFactory().loadAccessoryWithJaxb(fileName, portsProvider, context);
                break;
            default:
                accessory = new AccessoryFactory().loadAccessoryWithXmlDecoder(fileName);
                break;
        }
        return accessory;
    }

    protected void saveAccessoryWithXmlEncoder(String fileName, Accessory accessory, boolean gzip)
        throws FileNotFoundException, IOException {
        OutputStream os = new BufferedOutputStream(new FileOutputStream(fileName));
        if (gzip) {
            LOGGER.debug("Use gzip to compress accessory.");
            os = new GZIPOutputStream(os);
        }
        XMLEncoder e = new XMLEncoder(os);

        e.writeObject(accessory);
        e.close();
    }

    protected void saveAccessoryWithJaxb(
        String fileName, Accessory accessory, boolean gzip, final ApplicationContext context) {

        LcAccessoryType lcAccessory = new LcAccessoryType();
        int accessoryId = accessory.getId();
        lcAccessory.setAccessoryId(accessoryId);
        lcAccessory.setAccessoryName(accessory.toString());

        Labels accessoryLabels = context.get("labels", Labels.class);
        Long uniqueId = context.get("uniqueId", Long.class);
        LabelType accessoryAspectLabel = AccessoryLabelUtils.getAccessoryLabel(accessoryLabels, uniqueId, accessoryId);

        lcAccessory.setStartupState(accessory.getStartupState());

        Integer switchTime = accessory.getSwitchTime();
        if (switchTime != null) {
            LcAccessorySwitchTime lcAccessorySwitchTime = new LcAccessorySwitchTime();
            lcAccessorySwitchTime.setSwitchTime(switchTime);
            TimeBaseUnitEnum timeBaseUnit = accessory.getTimeBaseUnit();
            switch (timeBaseUnit) {
                case UNIT_100MS:
                    lcAccessorySwitchTime.setTimeBaseUnit(TimeBaseUnitType.UNIT_100_MS);
                    break;
                case UNIT_1S:
                    lcAccessorySwitchTime.setTimeBaseUnit(TimeBaseUnitType.UNIT_1_S);
                    break;
                default:
                    LOGGER.warn("Unsupported timebase unit detected for conversion to xml: {}", timeBaseUnit);
                    throw new RuntimeException(
                        "Unsupported timebase unit detected for conversion to xml: " + timeBaseUnit);
            }
        }

        int aspectIndex = 0;
        for (MacroRef aspect : accessory.getAspects()) {
            Integer macroId = aspect.getId();
            LcAspectPointType lcAspectPoint = new LcAspectPointType();

            LabelType aspectLabel = AccessoryLabelUtils.getAccessoryAspectLabel(accessoryAspectLabel, aspectIndex);

            if (aspectLabel != null && StringUtils.isNotBlank(aspectLabel.getLabelString())) {

                lcAspectPoint.setAspectName(aspectLabel.getLabelString());
            }

            lcAspectPoint.setMacroId(macroId);
            lcAccessory.getLcAspectPoint().add(lcAspectPoint);
            aspectIndex++;
        }

        new LcAccessoryExporter().saveAccessory(lcAccessory, fileName, gzip);
    }

    protected Accessory loadAccessoryWithXmlDecoder(String fileName) {
        Accessory accessory = null;
        XMLDecoder d = null;

        InputStream is = null;
        try {
            try {
                is = new FileInputStream(fileName);
                d = new XMLDecoder(XmlLoader.changePackage(new InputStreamReader(new GZIPInputStream(is))));
                accessory = (Accessory) d.readObject();
            }
            catch (IOException e) {
                if (is != null) {
                    try {
                        is.close();
                    }
                    catch (IOException e1) {
                        LOGGER.warn("Close inputstream failed.", e1);
                    }
                    is = null;
                }

                d = new XMLDecoder(XmlLoader.changePackage(new FileReader(fileName)));
                accessory = (Accessory) d.readObject();
            }
        }
        catch (FileNotFoundException e) {
            LOGGER.trace("Read accessory from file failed because the file is not available.", e);
        }
        catch (UnsupportedEncodingException e) {
            LOGGER.warn("The selected encoding is not supported.", e);
        }
        finally {
            if (d != null) {
                d.close();
            }
        }
        return accessory;
    }

    protected Accessory loadAccessoryWithJaxb(
        String fileName, PortsProvider portsProvider, final ApplicationContext context) {
        // load accessories from file
        LcAccessoryType lcAccessory = new LcAccessoryExporter().loadAccessory(fileName);
        Accessory accessory = null;
        if (lcAccessory != null) {
            accessory = new Accessory();

            int accessoryId = lcAccessory.getAccessoryId();
            accessory.setId(accessoryId);

            Labels accessoryLabels = context.get("labels", Labels.class);
            Long uniqueId = context.get("uniqueId", Long.class);

            // import name of accessory

            String label = lcAccessory.getAccessoryName();
            accessory.setLabel(label);

            AccessoryLabelUtils.replaceLabel(accessoryLabels, uniqueId, accessory.getId(), label);

            // startup state
            accessory.setStartupState(lcAccessory.getStartupState());
            if (lcAccessory.getAccessorySwitchTime() != null) {
                accessory.setSwitchTime(lcAccessory.getAccessorySwitchTime().getSwitchTime());
                switch (lcAccessory.getAccessorySwitchTime().getTimeBaseUnit()) {
                    case UNIT_100_MS:
                        accessory.setTimeBaseUnit(TimeBaseUnitEnum.UNIT_100MS);
                        break;
                    case UNIT_1_S:
                        accessory.setTimeBaseUnit(TimeBaseUnitEnum.UNIT_1S);
                        break;
                    default:
                        break;
                }
            }

            List<MacroRef> macroIds = new ArrayList<>();
            int aspectId = 0;
            for (LcAspectPointType lcAspectPoint : lcAccessory.getLcAspectPoint()) {
                Macro macro = null;
                // get the assigned macro
                for (Macro currentMacro : portsProvider.getMacros()) {
                    if (currentMacro.getId() == lcAspectPoint.getMacroId()) {
                        macro = currentMacro;

                        // update the label
                        String aspectName = lcAspectPoint.getAspectName();
                        // if (StringUtils.isNotBlank(aspectName)) {
                        LOGGER.info("Found new aspect name to set: {}", aspectName);

                        AccessoryLabelUtils.replaceLabel(accessoryLabels, uniqueId, accessoryId, aspectId, aspectName);
                        // }
                        break;
                    }
                }
                if (macro != null) {
                    LOGGER.info("Add current macro to accessory: {}", macro);

                    macroIds.add(new MacroRef(macro.getId()));
                }
                else {
                    LOGGER.warn("No macro available to add to accessory with id: {}", lcAspectPoint.getMacroId());
                }

                // next aspect
                aspectId++;
            }
            accessory.setAspects(macroIds);
            accessory.setTotalAspects(macroIds.size());

        }
        LOGGER.info("return accessory: {}", accessory);
        return accessory;
    }
}
