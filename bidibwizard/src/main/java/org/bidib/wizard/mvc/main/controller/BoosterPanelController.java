package org.bidib.wizard.mvc.main.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.SwingUtilities;
import javax.swing.Timer;

import org.bidib.jbidibc.core.enumeration.CommandStationState;
import org.bidib.jbidibc.core.utils.NodeUtils;
import org.bidib.wizard.comm.BoosterStatus;
import org.bidib.wizard.comm.CommunicationFactory;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.model.listener.BoosterStatusListener;
import org.bidib.wizard.mvc.main.model.listener.CommandStationStatusListener;
import org.bidib.wizard.mvc.main.model.listener.DefaultNodeListListener;
import org.bidib.wizard.mvc.main.view.panel.BoosterPanel;
import org.bidib.wizard.mvc.main.view.panel.listener.StatusListener;
import org.bidib.wizard.mvc.main.view.panel.listener.TabVisibilityListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BoosterPanelController extends DefaultNodeListListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(BoosterPanelController.class);

    private final MainModel mainModel;

    private BoosterPanel boosterPanel;

    private Timer boosterCurrentTimer;

    private Node selectedNode;

    private final BoosterStatusListener boosterStatusListener;

    private final CommandStationStatusListener commandStationStatusListener;

    private static final long CURRENT_UPDATE_TIMEOUT = 3000;

    public BoosterPanelController(final MainModel mainModel) {
        this.mainModel = mainModel;

        boosterStatusListener = new BoosterStatusListener() {
            @Override
            public void currentChanged(final int current) {
                if (SwingUtilities.isEventDispatchThread()) {
                    internalCurrentChanged(current);
                }
                else {
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            internalCurrentChanged(current);
                        }
                    });
                }
            }

            private void internalCurrentChanged(int current) {
                if (boosterPanel != null) {
                    boosterPanel.boosterCurrentChanged(current);
                }
            }

            @Override
            public void maximumCurrentChanged(final int maximumCurrent) {

                if (SwingUtilities.isEventDispatchThread()) {
                    internalMaximumCurrentChanged(maximumCurrent);
                }
                else {
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            internalMaximumCurrentChanged(maximumCurrent);
                        }
                    });
                }
            }

            private void internalMaximumCurrentChanged(int maximumCurrent) {
                if (boosterPanel != null) {
                    boosterPanel.boosterMaximumCurrentChanged(maximumCurrent);
                }
            }

            @Override
            public void stateChanged(final BoosterStatus status) {
                LOGGER.info("The booster state has changed: {}", status);
                if (SwingUtilities.isEventDispatchThread()) {
                    internalStateChanged(status);
                }
                else {
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            internalStateChanged(status);
                        }
                    });
                }
            }

            private void internalStateChanged(BoosterStatus status) {
                LOGGER.debug("Status of booster has changed: {}", status);
                if (boosterPanel != null) {
                    boosterPanel.boosterStateChanged(status);
                }
            }

            @Override
            public void temperatureChanged(final int temperature) {
                if (SwingUtilities.isEventDispatchThread()) {
                    internalTemperatureChanged(temperature);
                }
                else {
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            internalTemperatureChanged(temperature);
                        }
                    });
                }
            }

            private void internalTemperatureChanged(int temperature) {
                if (boosterPanel != null) {
                    boosterPanel.temperatureChanged(temperature);
                }
            }

            @Override
            public void voltageChanged(final int voltage) {
                if (SwingUtilities.isEventDispatchThread()) {
                    internalVoltageChanged(voltage);
                }
                else {
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            internalVoltageChanged(voltage);
                        }
                    });
                }
            }

            private void internalVoltageChanged(int voltage) {
                if (boosterPanel != null) {
                    boosterPanel.voltageChanged(voltage);
                }
            }
        };
        mainModel.addBoosterStatusListener(boosterStatusListener);

        // // prepare the status listener for command station status messages
        commandStationStatusListener = new CommandStationStatusListener() {

            @Override
            public void stateChanged(final Node node, final CommandStationState status) {
                LOGGER.info("The command station status has changed, node: {}, status: {}", node, status);
                if (SwingUtilities.isEventDispatchThread()) {
                    internalStateChanged(node, status);
                }
                else {
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            internalStateChanged(node, status);
                        }
                    });
                }
            }

            private void internalStateChanged(Node node, final CommandStationState status) {
                LOGGER.info("Status of command station has changed: {}", status);

                if (boosterPanel != null) {
                    boosterPanel.commandStationStateChanged(status);
                }
            }
        };
        mainModel.addCommandStationStatusListener(commandStationStatusListener);
    }

    public BoosterPanel createPanel(final TabVisibilityListener tabVisibilityListener) {
        LOGGER.info("Create new booster panel.");

        final CommandStationService commandStationService =
            DefaultApplicationContext.getInstance().get(DefaultApplicationContext.KEY_COMMAND_STATION_SERVICE,
                CommandStationService.class);

        final BoosterPanel boosterPanel = new BoosterPanel(mainModel);

        boosterPanel.addStatusListener(new StatusListener() {
            @Override
            public void switchedOff() {
                CommunicationFactory.getInstance().boosterOff(mainModel.getSelectedNode().getNode());
            }

            @Override
            public void switchedOn() {
                CommunicationFactory.getInstance().boosterOn(mainModel.getSelectedNode().getNode());
            }

            @Override
            public void queryBoosterState() {
                // trigger the booster state
                CommunicationFactory.getInstance().queryBoosterState(mainModel.getSelectedNode().getNode());
            }

            @Override
            public void switchedCommandStationOn(boolean ignoreWatchDog) {

                final Node node = mainModel.getSelectedNode();

                LOGGER.info("Switch command station on, node: {}, ignoreWatchDog: {}", node, ignoreWatchDog);

                commandStationService.switchOn(node, ignoreWatchDog);
            }

            @Override
            public void switchedCommandStationStop() {
                LOGGER.info("Switch command station to stop.");
                final Node node = mainModel.getSelectedNode();

                commandStationService.switchStop(node, true);
            }

            @Override
            public void switchedCommandStationSoftStop() {
                LOGGER.info("Switch command station to softStop.");
                final Node node = mainModel.getSelectedNode();

                commandStationService.switchStop(node, true);
            }

            @Override
            public void switchedCommandStationOff() {
                LOGGER.info("Switch command station off.");
                final Node node = mainModel.getSelectedNode();

                commandStationService.switchOff(node);
            }
        });

        try {
            // start the booster current timer
            boosterCurrentTimer = new Timer(1000, new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    LOGGER.trace("The booster current timer has elapsed.");

                    long now = System.currentTimeMillis();

                    final Node selectedNode = mainModel.getSelectedNode();
                    if (selectedNode != null && selectedNode.isBooster()) {

                        if (mainModel.getBoosterCurrent() >= 0
                            && mainModel.getLastCurrentUpdate() < (now - CURRENT_UPDATE_TIMEOUT)) {
                            // the current value is outdated -> clear the value
                            LOGGER.info("the current value is outdated -> clear the value, node: {}", selectedNode);

                            mainModel.setBoosterCurrent(-1);
                        }
                    }
                }
            });
            boosterCurrentTimer.setCoalesce(true);
            boosterCurrentTimer.start();
        }
        catch (Exception ex) {
            LOGGER.warn("Start the booster current timer failed.", ex);
        }

        this.boosterPanel = boosterPanel;

        return boosterPanel;
    }

    @Override
    public void nodeChanged() {
        LOGGER.info("The selected node has changed.");

        updateComponentState();
    }

    private void updateComponentState() {

        Node node = mainModel.getSelectedNode();

        if (selectedNode != null && selectedNode.equals(node)) {
            LOGGER.info("Node is selected already: {}", node);
            return;
        }

        // release the selected node
        selectedNode = null;

        boolean isBooster = false;
        if (node != null && NodeUtils.hasBoosterFunctions(node.getUniqueId())) {
            isBooster = true;
        }
        boolean isCommandStation = false;
        if (node != null && NodeUtils.hasCommandStationFunctions(node.getUniqueId())) {
            isCommandStation = true;
        }

        if (isBooster || isCommandStation) {
            selectedNode = node;
        }
        LOGGER.info("The selected node has booster functions: {}, has command station functions: {}, node: {}",
            isBooster, isCommandStation, node);

        if (boosterPanel != null) {
            boosterPanel.nodeChanged();
        }
    }

}
