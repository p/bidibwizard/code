package org.bidib.wizard.mvc.main.model.function;

import org.bidib.wizard.comm.MacroStatus;
import org.bidib.wizard.mvc.main.model.Flag;
import org.bidib.wizard.mvc.main.model.InputPort;
import org.bidib.wizard.mvc.main.model.Port;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FunctionUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(FunctionUtils.class);

    /**
     * Returns the output number of the function.
     * 
     * @param function
     *            the function
     * @return the output number
     */
    public static int getOutputNumber(Function<?> function) {
        int outputNumber = 0;
        if (function instanceof DelayFunction) {
            outputNumber = ((DelayFunction) function).getDelay();
        }
        else if (function instanceof AccessoryOkayFunction) {
            InputPort port = ((AccessoryOkayFunction) function).getInput();

            if (port != null && port.getId() > -1) {
                outputNumber = port.getId();
            }
        }
        else if (function instanceof FlagFunction) {
            Flag flag = ((FlagFunction) function).getFlag();

            if (flag != null) {
                outputNumber = flag.getId();
            }
        }
        else if (function instanceof InputFunction) {
            InputPort port = ((InputFunction) function).getInput();

            if (port != null) {
                outputNumber = port.getId();
            }
        }
        else if (function instanceof MacroFunction) {
            MacroFunction macroFunction = (MacroFunction) function;
            if (MacroStatus.END.equals(macroFunction.getAction())) {
                // do not deliver macro id if action is 'END'
                outputNumber = 0;
            }
            else {
                outputNumber = ((MacroFunction) function).getMacroId();
            }
        }
        else if (function instanceof PortAware) {
            Port<?> port = ((PortAware<?>) function).getPort();

            if (port != null && port.getId() > -1) {
                outputNumber = port.getId();
            }

        }
        else if (function instanceof RandomDelayFunction) {
            outputNumber = ((RandomDelayFunction) function).getMaximumValue();
        }
        return outputNumber;
    }

    /**
     * Returns the delay of the port functions. For other function types the value 255 is returned.
     * 
     * @param function
     *            the function
     * @return the delay value
     */
    public static byte getDelay(Function<?> function) {
        byte delay = (byte) 255;
        if (function instanceof PortAction) {
            delay = (byte) ((PortAction<?, ?>) function).getDelay();
        }
        return delay;
    }

    /**
     * Returns the value of the function if available.
     * 
     * @param function
     *            the function
     * @return the value
     */
    public static Integer getValue(Function<?> function) {
        Integer value = null;
        if (function instanceof BacklightPortAction) {
            // value = ByteUtils.getLowByte(((BacklightPortAction) function).getValue());
            value = ((BacklightPortAction) function).getValue();
            LOGGER.debug("The current value of the backlight port is: {}", value);
        }
        else if (function instanceof ServoPortAction) {
            // value = ByteUtils.getLowByte(((ServoPortAction) function).getValue());
            value = ((ServoPortAction) function).getValue();
        }
        return value;
    }
}
