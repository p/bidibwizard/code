package org.bidib.wizard.mvc.main.view.panel;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import org.apache.commons.collections4.CollectionUtils;
import org.bidib.jbidibc.core.AccessoryState;
import org.bidib.jbidibc.core.enumeration.TimeBaseUnitEnum;
import org.bidib.jbidibc.core.utils.AccessoryStateUtils.ErrorAccessoryState.AccessoryExecutionState;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.core.utils.ProductUtils;
import org.bidib.wizard.comm.AccessoryAspectStatus;
import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.common.utils.ImageUtils;
import org.bidib.wizard.labels.LabelType;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.bidib.wizard.mvc.accessory.model.AccessoryModel;
import org.bidib.wizard.mvc.accessory.view.panel.AbstractAccessoryPanel;
import org.bidib.wizard.mvc.common.view.converter.StringConverter;
import org.bidib.wizard.mvc.features.view.panel.IntegerInputValidationDocument;
import org.bidib.wizard.mvc.features.view.panel.RangeValidationCallback;
import org.bidib.wizard.mvc.main.controller.AccessoryPanelController;
import org.bidib.wizard.mvc.main.model.Accessory;
import org.bidib.wizard.mvc.main.model.AccessoryAspect;
import org.bidib.wizard.mvc.main.model.AccessoryAspectMacro;
import org.bidib.wizard.mvc.main.model.AccessoryAspectParam;
import org.bidib.wizard.mvc.main.model.AccessoryStartupAspectModel;
import org.bidib.wizard.mvc.main.model.AccessorySwitchTimeModel;
import org.bidib.wizard.mvc.main.model.AccessoryTableModel;
import org.bidib.wizard.mvc.main.model.Macro;
import org.bidib.wizard.mvc.main.model.MacroRef;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.SelectedAccessoryModel;
import org.bidib.wizard.mvc.main.model.listener.AccessoryExecutionListener;
import org.bidib.wizard.mvc.main.model.listener.AccessoryListListener;
import org.bidib.wizard.mvc.main.model.listener.AccessoryListener;
import org.bidib.wizard.mvc.main.model.listener.AccessoryPortListener;
import org.bidib.wizard.mvc.main.view.menu.AccessoryTableMenu;
import org.bidib.wizard.mvc.main.view.menu.listener.AccessoryTableMenuListener;
import org.bidib.wizard.mvc.main.view.panel.listener.AccessoryTableListener;
import org.bidib.wizard.mvc.main.view.table.AbstractEmptyTable;
import org.bidib.wizard.mvc.main.view.table.AccessoryAspectRenderer;
import org.bidib.wizard.mvc.main.view.table.ComboBoxEditor;
import org.bidib.wizard.mvc.main.view.table.ComboBoxRenderer;
import org.bidib.wizard.mvc.main.view.table.ComboBoxWithButtonEditor;
import org.bidib.wizard.mvc.main.view.table.ComboBoxWithButtonRenderer;
import org.bidib.wizard.mvc.stepcontrol.controller.StepControlControllerInterface;
import org.bidib.wizard.mvc.stepcontrol.model.StepControlAspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.adapter.ComboBoxAdapter;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.binding.value.BindingConverter;
import com.jgoodies.binding.value.ConverterValueModel;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.common.collect.ArrayListModel;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.validation.view.ValidationComponentUtils;

public class AccessoryPanel extends JPanel implements AccessoryExecutionListener, AccessoryListener {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(AccessoryPanel.class);

    private static final String ENCODED_SWITCHTIME_COLUMN_SPECS =
        "pref, 3dlu, pref, 10dlu, pref, 3dlu, pref, 3dlu, pref, 3dlu, pref, 3dlu, pref:grow";

    private final Collection<AccessoryTableListener> tableListeners = new LinkedList<AccessoryTableListener>();

    private final AbstractEmptyTable table;

    private final AccessoryTableModel tableModel;

    private final AccessoryTableMenu accessoryTableMenu;

    private JLabel executionStateIconLabel = new JLabel();

    private ImageIcon accessoryErrorIcon;

    private ImageIcon accessorySuccessfulIcon;

    private ImageIcon accessoryWaitIcon;

    private ImageIcon accessoryUnknownIcon;

    private SelectionInList<AccessoryAspect> startupAspectSelection;

    private ValueModel selectionHolderStartupAspect;

    private final AccessoryStartupAspectModel accessoryStartupAspectModel;

    private final AccessorySwitchTimeModel accessorySwitchTimeModel;

    private final AccessoryPanelController accessoryPanelController;

    private final SelectedAccessoryModel selectedAccessoryModel;

    private final PropertyChangeListener labelChangedListener;

    private final ValueModel selectedAccessoryValueModel;

    private ValueModel switchTimeValueModel;

    private JLabel labelSwitchTime;

    private JTextField switchTimeText;

    private JComponent[] baseUnitButtons;

    private final PropertyChangeListener startupAspectChangeListener;

    private final PropertyChangeListener switchTimeChangeListener;

    private final JLabel labelInitialState;

    private final JComboBox<Accessory> comboStartupAspect;

    private final static class AccessoryConverter implements BindingConverter<Accessory, String> {

        @Override
        public String targetValue(Accessory sourceValue) {
            return Objects.toString(sourceValue, null);
        }

        @Override
        public Accessory sourceValue(String targetValue) {
            return null;
        }
    }

    public AccessoryPanel(final AccessoryPanelController accessoryPanelController, final MainModel model,
        final AccessoryStartupAspectModel accessoryStartupAspectModel,
        final AccessorySwitchTimeModel accessorySwitchTimeModel) {

        this.accessoryPanelController = accessoryPanelController;
        this.accessoryStartupAspectModel = accessoryStartupAspectModel;
        this.accessorySwitchTimeModel = accessorySwitchTimeModel;

        selectedAccessoryModel = new SelectedAccessoryModel();

        labelChangedListener = new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                LOGGER.info("The label has changed.");

                selectedAccessoryModel.triggerLabelChanged();
            }
        };

        initialize();

        setLayout(new BorderLayout());

        accessoryTableMenu = new AccessoryTableMenu(model);
        accessoryTableMenu.addMenuListener(new AccessoryTableMenuListener() {
            @Override
            public void delete() {
                fireDelete(table.getSelectedRows());
            }

            @Override
            public void insertEmptyAfter() {
                fireInsertEmptyAfter(table.getSelectedRow());
            }

            @Override
            public void insertEmptyBefore() {
                fireInsertEmptyBefore(table.getSelectedRow());
            }

            @Override
            public void selectAll() {
                int rowCount = table.getRowCount();

                if (rowCount > 0) {
                    table.setRowSelectionInterval(0, rowCount - 1);
                }
            }

            @Override
            public void copy() {
                fireCopy(table.getSelectedRows());
            }

            @Override
            public void cut() {
                fireCut(table.getSelectedRows());
            }

            @Override
            public void pasteAfter() {
                firePasteAfter(table.getSelectedRow());
            }
        });

        tableModel = new AccessoryTableModel(model, accessoryStartupAspectModel);

        // DefaultFormBuilder formBuilder =
        // new DefaultFormBuilder(new FormLayout("max(20dlu;pref), 3dlu, max(30dlu;pref), 3dlu, 0dlu:grow"));
        DefaultFormBuilder formBuilder = new DefaultFormBuilder(new FormLayout(ENCODED_SWITCHTIME_COLUMN_SPECS));

        formBuilder.border(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        selectedAccessoryValueModel =
            new PropertyAdapter<SelectedAccessoryModel>(selectedAccessoryModel,
                SelectedAccessoryModel.PROPERTY_SELECTED_ACCESSORY, true);

        ValueModel valueConverterModel = new ConverterValueModel(selectedAccessoryValueModel, new AccessoryConverter());

        JLabel selectedAccessoryLabel = BasicComponentFactory.createLabel(valueConverterModel);

        formBuilder.append(Resources.getString(getClass(), "accessoryName"), selectedAccessoryLabel, 5);
        formBuilder.nextLine();

        // this must be an aspect and not an accessory
        startupAspectSelection =
            new SelectionInList<AccessoryAspect>(
                (ListModel<AccessoryAspect>) accessoryStartupAspectModel.getAccessoryAspectList());

        selectionHolderStartupAspect =
            new PropertyAdapter<AccessoryStartupAspectModel>(accessoryStartupAspectModel,
                AccessoryStartupAspectModel.PROPERTY_SELECTED_STARTUP_ASPECT, true);

        ComboBoxAdapter<Accessory> comboBoxAdapterStartupAspect =
            new ComboBoxAdapter<Accessory>(startupAspectSelection, selectionHolderStartupAspect);
        comboStartupAspect = new JComboBox<>();
        comboStartupAspect.setModel(comboBoxAdapterStartupAspect);

        labelInitialState = formBuilder.append(Resources.getString(getClass(), "initialState"), comboStartupAspect);
        // formBuilder.nextLine();

        switchTimeValueModel =
            new PropertyAdapter<>(accessorySwitchTimeModel, AccessoryModel.PROPERTYNAME_SWITCH_TIME, true);

        final ValueModel switchTimeConverterModel =
            new ConverterValueModel(switchTimeValueModel, new StringConverter(new DecimalFormat("#")));

        switchTimeText = new JTextField();
        IntegerInputValidationDocument switchTimeDocument =
            new IntegerInputValidationDocument(3, IntegerInputValidationDocument.NUMERIC);
        switchTimeDocument.setRangeValidationCallback(new RangeValidationCallback() {

            @Override
            public int getMinValue() {
                return 0;
            }

            @Override
            public int getMaxValue() {
                return 127;
            }
        });
        switchTimeText.setDocument(switchTimeDocument);
        switchTimeText.setColumns(3);

        // bind manually because we changed the document of the textfield
        Bindings.bind(switchTimeText, switchTimeConverterModel, false);

        labelSwitchTime =
            formBuilder.append(Resources.getString(AbstractAccessoryPanel.class, "switchTime"), switchTimeText);

        ValidationComponentUtils.setMandatory(switchTimeText, true);
        ValidationComponentUtils.setMessageKeys(switchTimeText, "validation.switchtime_key");

        ValueModel modeModel =
            new PropertyAdapter<>(accessorySwitchTimeModel, AccessorySwitchTimeModel.PROPERTYNAME_TIME_BASE_UNIT, true);
        baseUnitButtons = new JComponent[TimeBaseUnitEnum.values().length];

        int index = 0;
        for (TimeBaseUnitEnum baseUnit : TimeBaseUnitEnum.values()) {

            JRadioButton radio =
                BasicComponentFactory.createRadioButton(modeModel, baseUnit,
                    Resources.getString(TimeBaseUnitEnum.class, baseUnit.getKey()));
            baseUnitButtons[index++] = radio;

            // add radio button
            formBuilder.append(radio);
        }

        formBuilder.nextLine();

        for (JComponent comp : baseUnitButtons) {
            comp.setEnabled(false);
        }
        switchTimeText.setEnabled(false);
        labelSwitchTime.setEnabled(false);

        // support for startup aspect
        labelInitialState.setEnabled(false);
        comboStartupAspect.setEnabled(false);

        startupAspectChangeListener = new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {

                AccessoryAspect accessoryAspect = accessoryStartupAspectModel.getSelectedStartupAspect();
                boolean startupAspectEnabled = (accessoryAspect != null);
                LOGGER.info("The startup aspect has changed, startupAspectEnabled: {}, selected startup aspect: {}",
                    startupAspectEnabled, accessoryAspect);

                labelInitialState.setEnabled(startupAspectEnabled);
                comboStartupAspect.setEnabled(startupAspectEnabled);

                // update the accessory to trigger pending changes
                if (accessoryAspect != null) {
                    Accessory selectedAccessory = tableModel.getSelectedAccessory();
                    if (selectedAccessory != null) {

                        if (accessoryAspect instanceof AccessoryAspectMacro) {
                            // aspect is selected
                            Integer startupState =
                                ((AccessoryAspectMacro) accessoryStartupAspectModel.getSelectedStartupAspect())
                                    .getIndex();
                            LOGGER.info("Set the startup state: {}", startupState);
                            selectedAccessory.setStartupState(startupState);
                        }
                        else if (accessoryAspect instanceof AccessoryAspectParam) {
                            // param is selected
                            Integer startupState =
                                ByteUtils.getInt(
                                    ((AccessoryAspectParam) accessoryStartupAspectModel.getSelectedStartupAspect())
                                        .getParam());
                            LOGGER.info("Set the startup state: {}", startupState);
                            selectedAccessory.setStartupState(startupState);
                        }
                        else {
                            LOGGER.info("The startup state is not an macro aspect or a param: {}",
                                accessoryStartupAspectModel.getSelectedStartupAspect());
                        }
                    }
                }
            }
        };
        this.accessoryStartupAspectModel.addPropertyChangeListener(
            AccessoryStartupAspectModel.PROPERTY_SELECTED_STARTUP_ASPECT, startupAspectChangeListener);

        switchTimeChangeListener = new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                LOGGER.info("The switch time has changed, evt: {}", evt);

                Accessory selectedAccessory = tableModel.getSelectedAccessory();

                if (selectedAccessory != null) {
                    switch (evt.getPropertyName()) {
                        case AccessorySwitchTimeModel.PROPERTYNAME_SWITCH_TIME:
                            Integer switchTime = accessorySwitchTimeModel.getSwitchTime();
                            LOGGER.info("Set the switchTime: {}", switchTime);
                            selectedAccessory.setSwitchTime(switchTime);
                            break;
                        case AccessorySwitchTimeModel.PROPERTYNAME_TIME_BASE_UNIT:
                            TimeBaseUnitEnum timeBaseUnit = accessorySwitchTimeModel.getTimeBaseUnit();
                            LOGGER.info("Set the timeBaseUnit: {}", timeBaseUnit);
                            selectedAccessory.setTimeBaseUnit(timeBaseUnit);
                            break;
                        default:
                            break;
                    }
                }
            }
        };
        this.accessorySwitchTimeModel.addPropertyChangeListener(switchTimeChangeListener);

        JPanel executionStatePanel = new JPanel();
        executionStatePanel.setLayout(new BoxLayout(executionStatePanel, BoxLayout.LINE_AXIS));
        JLabel executionStateTextLabel = new JLabel("Execution state: ");
        executionStateTextLabel.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 0));
        executionStatePanel.add(executionStateTextLabel);
        executionStateIconLabel = new JLabel();
        executionStatePanel.add(executionStateIconLabel);

        formBuilder.append(executionStatePanel, 9);

        table = new AbstractEmptyTable(tableModel, Resources.getString(getClass(), "emptyTable")) {
            private static final long serialVersionUID = 1L;

            public TableCellEditor getCellEditor(int row, int column) {
                TableCellEditor result = super.getCellEditor(row, column);

                switch (column) {
                    case AccessoryTableModel.COLUMN_MACRO:
                        result = new ComboBoxEditor<Macro>(model.getMacros().toArray(new Macro[0])) {
                            private static final long serialVersionUID = 1L;

                            public Component getTableCellEditorComponent(
                                JTable table, Object value, boolean isSelected, int row, int column) {

                                Component comp =
                                    super.getTableCellEditorComponent(table, value, isSelected, row, column);
                                if (value instanceof MacroRef) {
                                    MacroRef macroRef = (MacroRef) value;
                                    JComboBox<Macro> combo = ((JComboBox<Macro>) editorComponent);
                                    for (int index = 0; index < combo.getItemCount(); index++) {

                                        Macro macro = combo.getItemAt(index);
                                        if (macro.getId() == macroRef.getId()) {
                                            combo.setSelectedItem(macro);
                                            break;
                                        }
                                    }
                                }
                                else {
                                    LOGGER.debug("Current value is not a MacroRef: {}", value);
                                }

                                return comp;
                            };
                        };
                        break;
                    case AccessoryTableModel.COLUMN_ACCESSORY_ASPECT_INSTANCE:
                        ComboBoxWithButtonEditor editor =
                            new ComboBoxWithButtonEditor(table.getActions(AccessoryAspectStatus.START), ">");

                        // TODO initialize the correct value

                        editor.addButtonListener(tableModel);
                        result = editor;
                        break;
                    default:
                        break;
                }
                return result;
            }

            public TableCellRenderer getCellRenderer(int row, int column) {
                TableCellRenderer result = super.getCellRenderer(row, column);

                switch (column) {
                    case AccessoryTableModel.COLUMN_MACRO:
                        result = new ComboBoxRenderer<Macro>(model.getMacros().toArray(new Macro[0])) {
                            private static final long serialVersionUID = 1L;

                            public Component getTableCellRendererComponent(
                                JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                                Component comp =
                                    super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row,
                                        column);

                                if (value instanceof MacroRef) {
                                    MacroRef macroRef = (MacroRef) value;
                                    for (int index = 0; index < getItemCount(); index++) {

                                        Macro macro = getItemAt(index);
                                        if (macro.getId() == macroRef.getId()) {
                                            setSelectedItem(macro);
                                            break;
                                        }
                                    }
                                }
                                else {
                                    LOGGER.debug("Current value is not a MacroRef: {}", value);
                                }

                                comp.setEnabled(true);
                                Object val = table.getModel().getValueAt(row, 2 /* accessory */);
                                if (val instanceof AccessoryAspectMacro) {
                                    AccessoryAspectMacro accessory = (AccessoryAspectMacro) val;
                                    comp.setEnabled(!accessory.isImmutableAccessory());
                                }

                                return comp;
                            };
                        };
                        break;
                    case AccessoryTableModel.COLUMN_ACCESSORY_ASPECT_INSTANCE:
                        result =
                            new ComboBoxWithButtonRenderer<BidibStatus>(table.getActions(AccessoryAspectStatus.START),
                                ">");
                        break;
                    default:
                        break;
                }
                return result;
            }
        };
        TableColumn tc = table.getColumnModel().getColumn(AccessoryTableModel.COLUMN_LABEL);
        tc.setCellRenderer(new AccessoryAspectRenderer(Resources.getString(AccessoryTableModel.class, "aspect") + "_"));

        table.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                handleMouseEvent(e, accessoryTableMenu);
            }

            public void mouseReleased(MouseEvent e) {
                handleMouseEvent(e, accessoryTableMenu);
            }
        });

        table.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);

        table.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(AccessoryTableMenu.KEYSTROKE_DELETE,
            "delete");
        table.getActionMap().put("delete", new AbstractAction() {
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {
                accessoryTableMenu.fireDelete();
            }
        });
        tableModel.addPortListener(new AccessoryPortListener() {
            @Override
            public void testButtonPressed(int aspectId) {
                fireTestButtonPressed(aspectId);
            }

            @Override
            public void labelChanged(int aspectId) {
                fireAspectLabelChanged(aspectId);
            }
        });

        formBuilder.appendRow("3dlu");
        formBuilder.appendRow("fill:300px:grow");
        formBuilder.nextLine(2);
        formBuilder.append(new JScrollPane(table), 13);

        add(formBuilder.build(), BorderLayout.CENTER);

        model.addAccessoryListListener(new AccessoryListListener() {
            @Override
            public void accessoryChanged(int accessoryId) {
                LOGGER.info("The selected accessory has changed.");

                // the selected accessory has changed
                Accessory selectedAccessory = tableModel.getSelectedAccessory();
                if (selectedAccessory != null) {
                    selectedAccessory.removeAccessoryListener(AccessoryPanel.this);
                }

                // get the new selected accessory
                final Accessory accessory = model.getSelectedAccessory();

                if (accessory != null) {
                    accessory.addAccessoryListener(AccessoryPanel.this);
                    accessory.addPropertyChangeListener(Accessory.PROPERTY_LABEL, labelChangedListener);
                }
                else {
                    LOGGER.info("No accessory selected.");
                }

                // set the new selected accessory
                tableModel.setSelectedAccessory(accessory);
                selectedAccessoryModel.setSelectedAccessory(accessory);

                // signal that the macros of the selected accessories have been changed
                macrosChanged();

                // clear the accessory execution state
                if (accessory != null) {
                    accessoryStateChanged(accessory.getId(), -1);
                }
                else {
                    accessoryStateChanged(-1, -1);
                }
            }

            @Override
            public void listChanged() {
            }

            @Override
            public void pendingChangesChanged() {
            }
        });

    }

    /**
     * Initialize the component
     */
    protected void initialize() {
        // Set the icon for leaf nodes.
        accessoryErrorIcon = ImageUtils.createImageIcon(NodeTree.class, "/icons/accessory-error.png");
        accessorySuccessfulIcon = ImageUtils.createImageIcon(NodeTree.class, "/icons/accessory-successful.png");
        accessoryWaitIcon = ImageUtils.createImageIcon(NodeTree.class, "/icons/accessory-wait.png");
        accessoryUnknownIcon = ImageUtils.createImageIcon(NodeTree.class, "/icons/accessory-unknown.png");
    }

    public void addTableListener(AccessoryTableListener l) {
        tableListeners.add(l);
    }

    private void fireDelete(int[] rows) {
        for (AccessoryTableListener l : tableListeners) {
            l.delete(rows);
        }
    }

    private void fireInsertEmptyAfter(int row) {
        for (AccessoryTableListener l : tableListeners) {
            l.insertEmptyAfter(row);
        }
    }

    private void fireInsertEmptyBefore(int row) {
        for (AccessoryTableListener l : tableListeners) {
            l.insertEmptyBefore(row);
        }
    }

    private void fireCopy(int[] rows) {
        for (AccessoryTableListener l : tableListeners) {
            l.copy(getMacros(rows));
        }
    }

    private void fireCut(int[] rows) {
        for (AccessoryTableListener l : tableListeners) {
            l.cut(rows, getMacros(rows));
        }
    }

    private void firePasteAfter(int row) {
        for (AccessoryTableListener l : tableListeners) {
            l.pasteAfter(row);
        }
    }

    private void fireTestButtonPressed(int aspect) {
        for (AccessoryTableListener l : tableListeners) {
            l.testButtonPressed(aspect);
        }
    }

    private int getRow(Point point) {
        return table.rowAtPoint(point);
    }

    private void handleMouseEvent(MouseEvent e, AccessoryTableMenu popupMenu) {
        if (e.isPopupTrigger()) {
            int row = getRow(e.getPoint());
            // stop table editing before the menu is displayed
            if (table.getCellEditor() != null) {
                table.getCellEditor().cancelCellEditing();
            }

            if (table.getSelectedRowCount() == 0 && table.getRowCount() > 0 && row >= 0 && row < table.getRowCount()) {
                table.setRowSelectionInterval(row, row);
            }

            int selectedRow = table.getSelectedRow();
            if (selectedRow > -1) {
                try {
                    Object value = tableModel.getValueAt(selectedRow, 2);
                    if (value instanceof AccessoryAspectMacro) {
                        boolean immutable = ((AccessoryAspectMacro) value).isImmutableAccessory();
                        if (immutable) {
                            popupMenu.updateImmutableMenuItems();
                        }
                    }
                }
                catch (Exception ex) {
                    LOGGER.warn("Update menu items for immutable aspect failed.", ex);
                }
            }

            table.requestFocusInWindow();
            // table.grabFocus();

            popupMenu.show(e.getComponent(), e.getX(), e.getY());
        }
    }

    @Override
    public void executionStateChanged(
        AccessoryExecutionState executionState, int accessoryId, int aspect, AccessoryState accessoryState) {
        LOGGER.info("The execution state has changed: {}, accessoryId: {}, aspect: {}", executionState, accessoryId,
            aspect);
        if (aspect == -1 || executionState == null) {
            executionState = AccessoryExecutionState.IDLE;
        }

        executionStateIconLabel.setToolTipText(null);
        switch (executionState) {
            case ERROR:
                executionStateIconLabel.setIcon(accessoryErrorIcon);
                if (accessoryState != null) {
                    executionStateIconLabel.setToolTipText(accessoryState.getErrorInformation());
                }
                break;
            case RUNNING:
                executionStateIconLabel.setIcon(accessoryWaitIcon);
                break;
            case SUCCESSFUL:
                executionStateIconLabel.setIcon(accessorySuccessfulIcon);
                break;
            case UNKNOWN:
                executionStateIconLabel.setIcon(accessoryUnknownIcon);
                break;
            default:
                executionStateIconLabel.setIcon(null);
                break;
        }

    }

    @Override
    public void labelChanged(String label) {
    }

    private ArrayListModel<StepControlAspect> stepControlAspects;

    private ListDataListener stepControlAspectsListener;

    private void loadStepControlMovingAspects(final LabelType accessoryLabel) {
        LOGGER.info("Load the StepControl moving aspects.");

        // remove all rows
        tableModel.setRowCount(0);
        accessoryStartupAspectModel.clearAccessoryAspects();

        int aspectNumber = 0;
        for (StepControlAspect stepControlAspect : stepControlAspects) {
            LOGGER.info("Create accessory aspect for stepControlAspect: {}", stepControlAspect);

            AccessoryAspectMacro aspectTurntable = new AccessoryAspectMacro(aspectNumber, null);
            aspectTurntable.setImmutableAccessory(true);

            // try to get the user-defined label
            aspectTurntable.setLabel(getAccessoryAspectLabel(accessoryLabel, aspectNumber));

            tableModel.addRow(aspectTurntable);
            accessoryStartupAspectModel.addAccessoryAspect(aspectTurntable);

            aspectNumber++;
        }

    }

    @Override
    public void macrosChanged() {
        LOGGER.info("The macros have changed.");

        // remove all rows
        tableModel.setRowCount(0);

        accessoryStartupAspectModel.clearAccessoryAspects();

        // release the stepControlAspects
        if (stepControlAspects != null) {
            if (stepControlAspectsListener != null) {
                stepControlAspects.removeListDataListener(stepControlAspectsListener);
            }
            stepControlAspects = null;
        }

        // get the selected accessory and prepare the aspects
        final Accessory selectedAccessory = tableModel.getSelectedAccessory();
        if (selectedAccessory != null) {

            long uniqueId = accessoryPanelController.getSelectedNode().getUniqueId();
            boolean isStepControl = ProductUtils.isStepControl(uniqueId);

            // in case of the Step Control we have the first 3 accessories fix
            if (isStepControl && selectedAccessory.getId() < 3) {
                switch (selectedAccessory.getId()) {
                    case 0:
                        // get the configured moving aspects
                        try {
                            // get the saved accessory labels
                            final LabelType accessoryLabel =
                                accessoryPanelController.getAccessoryAspectsLabels(selectedAccessory.getId());

                            StepControlControllerInterface controller =
                                DefaultApplicationContext.getInstance().get(
                                    DefaultApplicationContext.KEY_STEPCONTROL_CONTROLLER,
                                    StepControlControllerInterface.class);

                            stepControlAspects = controller.getConfigureAspectsListModel();

                            if (stepControlAspectsListener == null) {
                                LOGGER.info("Create new stepControlAspectsListener.");
                                stepControlAspectsListener = new ListDataListener() {

                                    @Override
                                    public void intervalRemoved(ListDataEvent e) {
                                        LOGGER.info("intervalRemoved.");

                                        loadStepControlMovingAspects(accessoryLabel);
                                    }

                                    @Override
                                    public void intervalAdded(ListDataEvent e) {
                                        LOGGER.info("intervalAdded.");

                                        loadStepControlMovingAspects(accessoryLabel);
                                    }

                                    @Override
                                    public void contentsChanged(ListDataEvent e) {
                                        LOGGER.info("The contents of the stepControl aspects have changed.");
                                    }
                                };
                                LOGGER.info("Created new instance of stepControlAspectsListener: {}",
                                    stepControlAspectsListener);
                            }
                            stepControlAspects.addListDataListener(stepControlAspectsListener);

                            loadStepControlMovingAspects(accessoryLabel);
                        }
                        catch (Exception ex) {
                            LOGGER.warn("Load configured moving aspects failed", ex);
                        }
                        break;
                    case 1:
                        // the homing aspect
                        // AccessoryAspectMacro aspectEmergencyStop = new AccessoryAspectMacro(0, null);
                        // aspectEmergencyStop.setImmutableAccessory(true);
                        // aspectEmergencyStop.setLabel("Emergency Stop");
                        // tableModel.addRow(aspectEmergencyStop);
                        // accessoryStartupAspectModel.addAccessoryAspect(aspectEmergencyStop);

                        AccessoryAspectMacro aspectOperating = new AccessoryAspectMacro(0, null);
                        aspectOperating.setImmutableAccessory(true);
                        aspectOperating.setLabel("Operating");
                        tableModel.addRow(aspectOperating);
                        accessoryStartupAspectModel.addAccessoryAspect(aspectOperating);
                        break;
                    default:
                        // the sound aspect
                        AccessoryAspectMacro aspectSoundOn = new AccessoryAspectMacro(0, null);
                        aspectSoundOn.setImmutableAccessory(true);
                        aspectSoundOn.setLabel(Resources.getString(AccessoryPanel.class, "stepControl.soundOff"));
                        tableModel.addRow(aspectSoundOn);
                        accessoryStartupAspectModel.addAccessoryAspect(aspectSoundOn);

                        AccessoryAspectMacro aspectSoundOff = new AccessoryAspectMacro(1, null);
                        aspectSoundOff.setImmutableAccessory(true);
                        aspectSoundOff.setLabel(Resources.getString(AccessoryPanel.class, "stepControl.soundOn"));
                        tableModel.addRow(aspectSoundOff);
                        accessoryStartupAspectModel.addAccessoryAspect(aspectSoundOff);

                        AccessoryAspectMacro aspectSoundOnOnlyFunctions = new AccessoryAspectMacro(2, null);
                        aspectSoundOnOnlyFunctions.setImmutableAccessory(true);
                        aspectSoundOnOnlyFunctions
                            .setLabel(Resources.getString(AccessoryPanel.class, "stepControl.soundOnOnlyFunctions"));
                        tableModel.addRow(aspectSoundOnOnlyFunctions);
                        accessoryStartupAspectModel.addAccessoryAspect(aspectSoundOnOnlyFunctions);
                        break;
                }
            }
            else {
                // processing of non-StepControl nodes
                LabelType accessoryLabel =
                    accessoryPanelController.getAccessoryAspectsLabels(selectedAccessory.getId());

                if (CollectionUtils.isNotEmpty(selectedAccessory.getAspects())) {

                    // prepare the aspects of the selected accessory
                    List<Macro> macros = accessoryPanelController.getMacros();

                    MacroRef[] aspects = selectedAccessory.getAspects().toArray(new MacroRef[0]);
                    for (int row = 0; row < aspects.length; row++) {
                        // get the number of the macro of the selected aspect

                        MacroRef currentMacro = aspects[row];
                        if (currentMacro != null && currentMacro.getId() != null) {
                            final int macroNumber = currentMacro.getId();

                            LOGGER.info("Adding new aspect, row: {}, macroNumber: {}", row, macroNumber);

                            if (macroNumber > -1 && macroNumber < macros.size()) {
                                AccessoryAspectMacro accessoryAspect = new AccessoryAspectMacro(row, currentMacro);

                                // try to get the user-defined label
                                accessoryAspect.setLabel(getAccessoryAspectLabel(accessoryLabel, row));

                                // tableModel.addRow(row, macros.get(macroNumber));
                                tableModel.addRow(accessoryAspect);

                                accessoryStartupAspectModel.addAccessoryAspect(accessoryAspect);
                            }
                        }
                    }
                }
                else if (!selectedAccessory.isMacroMapped()) {

                    int totalAspects = selectedAccessory.getTotalAspects();

                    if (totalAspects > 0) {
                        for (int row = 0; row < totalAspects; row++) {
                            AccessoryAspectMacro accessoryAspect = new AccessoryAspectMacro(row, null);

                            // try to get the user-defined label
                            accessoryAspect.setLabel(getAccessoryAspectLabel(accessoryLabel, row));

                            // TODO prepare AccessoryAspects for non macro mapped nodes
                            accessoryAspect.setImmutableAccessory(true);

                            tableModel.addRow(accessoryAspect);

                        }
                    }
                }
            }

            // evaluate the startup aspect
            try {
                // remove the property change listener
                this.accessoryStartupAspectModel.removePropertyChangeListener(
                    AccessoryStartupAspectModel.PROPERTY_SELECTED_STARTUP_ASPECT, startupAspectChangeListener);

                if (selectedAccessory.getStartupState() != null) {
                    AccessoryAspect startupAspect =
                        accessoryStartupAspectModel.getAssignedAspect(selectedAccessory.getStartupState());
                    accessoryStartupAspectModel.setSelectedStartupAspect(startupAspect);
                }
                else {
                    LOGGER.info("No startup aspect available.");
                    accessoryStartupAspectModel.setSelectedStartupAspect(null);
                }

                AccessoryAspect accessoryAspect = accessoryStartupAspectModel.getSelectedStartupAspect();
                boolean startupAspectEnabled = (accessoryAspect != null);
                LOGGER.info("The startup aspect has changed, startupAspectEnabled: {}, selected startup aspect: {}",
                    startupAspectEnabled, accessoryAspect);

                labelInitialState.setEnabled(startupAspectEnabled);
                comboStartupAspect.setEnabled(startupAspectEnabled);

            }
            finally {
                // add the property change listener
                this.accessoryStartupAspectModel.addPropertyChangeListener(
                    AccessoryStartupAspectModel.PROPERTY_SELECTED_STARTUP_ASPECT, startupAspectChangeListener);
            }

            // evaluate the switch time
            try {
                // remove the property change listener
                this.accessorySwitchTimeModel.removePropertyChangeListener(switchTimeChangeListener);

                boolean switchTimeEnabled = (selectedAccessory.getSwitchTime() != null);

                LOGGER.info("The accessory switch time has changed, startupAspectEnabled: {}", switchTimeEnabled);

                for (JComponent comp : baseUnitButtons) {
                    comp.setEnabled(switchTimeEnabled);
                }
                switchTimeText.setEnabled(switchTimeEnabled);
                labelSwitchTime.setEnabled(switchTimeEnabled);

                if (switchTimeEnabled) {
                    Integer switchTime = selectedAccessory.getSwitchTime();
                    TimeBaseUnitEnum timeBaseUnit = selectedAccessory.getTimeBaseUnit();

                    accessorySwitchTimeModel.setSwitchTime(switchTime);
                    accessorySwitchTimeModel.setTimeBaseUnit(timeBaseUnit);
                }
                else {
                    accessorySwitchTimeModel.setSwitchTime(null);
                    accessorySwitchTimeModel.setTimeBaseUnit(null);
                }
            }
            finally {
                // add the property change listener
                this.accessorySwitchTimeModel.addPropertyChangeListener(switchTimeChangeListener);
            }

        }
    }

    private String getAccessoryAspectLabel(LabelType labels, int aspectIndex) {
        if (labels != null && labels.getChildLabels() != null
            && CollectionUtils.isNotEmpty(labels.getChildLabels().getLabel())) {

            for (LabelType label : labels.getChildLabels().getLabel()) {
                if (label.getIndex() == aspectIndex) {
                    String labelString = label.getLabelString();
                    LOGGER.info("Found the aspect label: {}", labelString);
                    return labelString;
                }
            }
        }
        return null;
    }

    @Override
    public void accessoryStateChanged(int accessoryId, int aspect) {
        LOGGER.info("The accessory state has changed for accessory id: {}, aspect: {}", accessoryId, aspect);

        final Accessory selectedAccessory = tableModel.getSelectedAccessory();
        if (selectedAccessory != null && selectedAccessory.getId() == accessoryId) {
            AccessoryExecutionState executionState = selectedAccessory.getAccessoryExecutionState();
            AccessoryState accessoryState = selectedAccessory.getAccessoryState();

            executionStateChanged(executionState, accessoryId, aspect, accessoryState);
        }
        else {
            executionStateChanged(null, accessoryId, aspect, null);
        }
    }

    private void fireAspectLabelChanged(int aspectId) {

        String label = (String) tableModel.getValueAt(aspectId, AccessoryTableModel.COLUMN_LABEL);

        accessoryPanelController.setAccessoryAspectLabel(aspectId, label);
    }

    private MacroRef[] getMacros(int[] rows) {
        MacroRef[] result = new MacroRef[rows.length];

        try {
            final Accessory selectedAccessory = tableModel.getSelectedAccessory();
            MacroRef[] aspects = selectedAccessory.getAspects().toArray(new MacroRef[0]);
            for (int index = 0; index < rows.length; index++) {
                result[index] = aspects[rows[index]];
            }

            LOGGER.info("Prepared macros: {}", new Object[] { result });
        }
        catch (Exception ex) {
            LOGGER.warn("Get selected macros failed.", ex);
        }
        return result;
    }

    public void saveAccessory(final Accessory selectedAccessory) {
        LOGGER.info("Save the values to the selected accessory: {}", selectedAccessory);

        if (accessoryStartupAspectModel.getSelectedStartupAspect() instanceof AccessoryAspectMacro) {
            // aspect is selected
            Integer startupState =
                ((AccessoryAspectMacro) accessoryStartupAspectModel.getSelectedStartupAspect()).getIndex();
            LOGGER.info("Set the startup state: {}", startupState);
            selectedAccessory.setStartupState(startupState);
        }
        else if (accessoryStartupAspectModel.getSelectedStartupAspect() instanceof AccessoryAspectParam) {
            // param is selected
            Integer startupState =
                ByteUtils
                    .getInt(((AccessoryAspectParam) accessoryStartupAspectModel.getSelectedStartupAspect()).getParam());
            LOGGER.info("Set the startup state: {}", startupState);
            selectedAccessory.setStartupState(startupState);
        }
        else {
            LOGGER.info("The startup state is not an macro aspect or a param: {}",
                accessoryStartupAspectModel.getSelectedStartupAspect());
        }
    }
}
