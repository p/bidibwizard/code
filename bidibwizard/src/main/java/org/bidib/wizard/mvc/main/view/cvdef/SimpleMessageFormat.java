package org.bidib.wizard.mvc.main.view.cvdef;

import java.text.FieldPosition;
import java.text.Format;
import java.text.MessageFormat;
import java.text.ParsePosition;

public class SimpleMessageFormat extends Format {
    private static final long serialVersionUID = 1L;

    private MessageFormat messageFormat;

    public SimpleMessageFormat(String pattern) {

        messageFormat = new MessageFormat(pattern);
    }

    @Override
    public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
        Object[] args = null;
        if (obj instanceof Object[]) {
            args = (Object[]) obj;
        }
        else {
            args = new Object[] { obj };
        }
        return messageFormat.format(args, toAppendTo, pos);
    }

    @Override
    public Object parseObject(String source, ParsePosition pos) {
        return messageFormat.parseObject(source, pos);
    }

}