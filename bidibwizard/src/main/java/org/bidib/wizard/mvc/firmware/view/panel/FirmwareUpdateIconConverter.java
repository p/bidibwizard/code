package org.bidib.wizard.mvc.firmware.view.panel;

import javax.swing.ImageIcon;

import org.bidib.wizard.common.utils.ImageUtils;
import org.bidib.wizard.mvc.firmware.model.UpdateStatus;
import org.bidib.wizard.mvc.main.view.panel.NodeTree;

import com.jgoodies.binding.value.BindingConverter;

/**
 * Converts Values to Strings and vice-versa using a given Format.
 */
public final class FirmwareUpdateIconConverter implements BindingConverter<UpdateStatus, ImageIcon> {

    private ImageIcon updateOperationErrorIcon;

    private ImageIcon updateOperationSuccessfulIcon;

    private ImageIcon updateOperationWaitIcon;

    private ImageIcon updateOperationUnknownIcon;

    private ImageIcon updateOperationEmptyIcon;

    public FirmwareUpdateIconConverter() {
        initializeIcons();
    }

    /**
     * Initialize the icons
     */
    private void initializeIcons() {
        // Load the icons
        updateOperationErrorIcon = ImageUtils.createImageIcon(NodeTree.class, "/icons/accessory-error.png");
        updateOperationSuccessfulIcon = ImageUtils.createImageIcon(NodeTree.class, "/icons/accessory-successful.png");
        updateOperationWaitIcon = ImageUtils.createImageIcon(NodeTree.class, "/icons/accessory-wait.png");
        updateOperationUnknownIcon = ImageUtils.createImageIcon(NodeTree.class, "/icons/information.png");
        updateOperationEmptyIcon = ImageUtils.createImageIcon(NodeTree.class, "/icons/empty.png");
    }

    // Implementing Abstract Behavior *************************************

    /**
     * Formats the source value and returns a String representation.
     * 
     * @param sourceValue
     *            the source value
     * @return the formatted sourceValue
     */
    @Override
    public ImageIcon targetValue(UpdateStatus sourceValue) {
        UpdateStatus updateState = (UpdateStatus) sourceValue;
        ImageIcon icon = null;
        switch (updateState) {
            case PREPARE:
                icon = updateOperationUnknownIcon;
                break;
            case ENTRY_PASSED:
            case DATA_TRANSFER:
                icon = updateOperationWaitIcon;
                break;
            case DATA_TRANSFER_PASSED:
                icon = updateOperationSuccessfulIcon;
                break;
            case PREPARE_FAILED:
            case DATA_TRANSFER_FAILED:
            case NODE_LOST:
                icon = updateOperationErrorIcon;
                break;
            default:
                icon = updateOperationEmptyIcon;
                break;
        }
        return icon;
    }

    /**
     * Parses the given String encoding and sets it as the subject's new value. Silently catches {@code ParseException}.
     * 
     * @param targetValue
     *            the value to be converted and set as new subject value
     */
    @Override
    public UpdateStatus sourceValue(ImageIcon targetValue) {

        return null;
    }
}
