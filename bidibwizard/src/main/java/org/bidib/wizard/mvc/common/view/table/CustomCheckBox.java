package org.bidib.wizard.mvc.common.view.table;

import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.SwingConstants;

import org.bidib.wizard.common.utils.ImageUtils;

public class CustomCheckBox extends JCheckBox {
    private static final long serialVersionUID = 1L;

    private ImageIcon unselectedIcon;

    private ImageIcon selectedIcon;

    public CustomCheckBox(String pathSelectedIcon, String pathUnselectedIcon) {
        // prepare the icons
        selectedIcon = ImageUtils.createImageIcon(CustomCheckBox.class, pathSelectedIcon);
        unselectedIcon = ImageUtils.createImageIcon(CustomCheckBox.class, pathUnselectedIcon);

        setHorizontalAlignment(SwingConstants.CENTER);
    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);
        if (selected) {
            setIcon(selectedIcon);
        }
        else {
            setIcon(unselectedIcon);
        }
    }

}
