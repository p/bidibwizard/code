package org.bidib.wizard.mvc.script.view.wizard;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.velocity.VelocityContext;
import org.bidib.wizard.common.context.ApplicationContext;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.script.controller.NodeScriptProcessor;
import org.bidib.wizard.mvc.script.controller.ScriptContextKeys;
import org.bidib.wizard.mvc.script.view.input.AbstractInputSelectionPanel;
import org.bidib.wizard.mvc.script.view.input.InputParametersDialog;
import org.bidib.wizard.mvc.script.view.utils.NodeScriptUtils;
import org.bidib.wizard.script.node.NodeScriptCommandList.ExecutionStatus;
import org.bidib.wizard.script.node.NodeScriptContextKeys;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jidesoft.dialog.ButtonEvent;
import com.jidesoft.dialog.ButtonNames;
import com.jidesoft.dialog.PageEvent;
import com.jidesoft.dialog.PageListener;
import com.jidesoft.wizard.DefaultWizardPage;

public class ScriptInputParametersPage extends DefaultWizardPage {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ScriptInputParametersPage.class);

    private final ApplicationContext scriptContext;

    private NodeScriptProcessor nodeScriptProcessor;

    private JScrollPane scrollPane;

    private JPanel contentPanel;

    private JPanel parameterPanel;

    private StringBuilder sbScript;

    private final Map<String, AbstractInputSelectionPanel> inputPanelMap = new HashMap<>();

    public ScriptInputParametersPage(Icon icon, final ApplicationContext scriptContext) {
        super(Resources.getString(ScriptInputParametersPage.class, "title"),
            Resources.getString(ScriptInputParametersPage.class, "description"), icon);

        this.scriptContext = scriptContext;
        contentPanel = new JPanel(new BorderLayout()) {
            private static final long serialVersionUID = 1L;

            @Override
            public Dimension getPreferredSize() {
                return new Dimension(400, 600);
            }
        };

        parameterPanel = new JPanel();

        nodeScriptProcessor = scriptContext.get("nodeScriptProcessor", NodeScriptProcessor.class);

    }

    @Override
    public int getSelectedStepIndex() {
        return 3;
    }

    private PropertyChangeListener executionStatusListener;

    private AtomicBoolean scriptFinishedLock = new AtomicBoolean();

    @Override
    protected void initContentPane() {
        super.initContentPane();

        scrollPane = new JScrollPane(parameterPanel);
        contentPanel.add(scrollPane);
        addComponent(contentPanel, true);

        scriptContext.register(ScriptContextKeys.KEY_SCRIPT_PANEL, contentPanel);

        addPageListener(new PageListener() {

            @Override
            public void pageEventFired(PageEvent e) {

                switch (e.getID()) {
                    case PageEvent.PAGE_CLOSING:

                        if (e.getSource() instanceof JButton) {
                            JButton button = (JButton) e.getSource();
                            if (ButtonNames.NEXT.equals(button.getName())) {
                                LOGGER.info("The next button was pressed.");

                                synchronized (scriptFinishedLock) {
                                    scriptFinishedLock.set(false);
                                }
                                scriptContext.unregister(NodeScriptContextKeys.KEY_SCRIPTEXECUTIONSTATUS);

                                LOGGER.info("Add the user input values to velocity context.");
                                final VelocityContext velocityContext =
                                    scriptContext.get(ScriptContextKeys.KEY_VELOCITY_CONTEXT, VelocityContext.class);
                                for (Entry<String, AbstractInputSelectionPanel> entry : inputPanelMap.entrySet()) {
                                    String variableName = entry.getKey();
                                    Object value = entry.getValue().getSelectedValue();
                                    LOGGER.info("Add to velocity context, variable name: {}, value: {}", variableName,
                                        value);
                                    velocityContext.put(variableName, value);
                                }

                                executionStatusListener = new PropertyChangeListener() {

                                    @Override
                                    public void propertyChange(PropertyChangeEvent evt) {

                                        LOGGER.info("Received change event: {}", evt);
                                        Object value = evt.getNewValue();
                                        if (value != null) {
                                            final ExecutionStatus executionStatus = (ExecutionStatus) value;
                                            SwingUtilities.invokeLater(new Runnable() {
                                                public void run() {
                                                    handleExecutionStatusChange(executionStatus,
                                                        executionStatusListener);
                                                }
                                            });
                                        }
                                        else {
                                            LOGGER.warn("No value available.");
                                        }
                                    }
                                };

                                try {
                                    scriptContext.register(NodeScriptContextKeys.KEY_SCRIPTEXECUTIONSTATUSLOCK,
                                        scriptFinishedLock);

                                    nodeScriptProcessor.addExecutionStatusListener(executionStatusListener);

                                    nodeScriptProcessor.substituteVariables(sbScript, scriptContext);
                                    LOGGER.info("Script after substitution of variables: {}", sbScript);

                                    String rendered = nodeScriptProcessor.renderScript(sbScript, scriptContext);
                                    LOGGER.info("Rendered script: {}", rendered);

                                    nodeScriptProcessor.parseAndApplyScript(rendered, scriptContext);
                                    LOGGER.info("Apply script on node has finished.");
                                }
                                catch (Exception ex) {
                                    LOGGER.warn("Apply script on node failed.", ex);

                                    // executionStatus = ExecutionStatus.aborted;
                                    handleExecutionStatusChange(ExecutionStatus.aborted, executionStatusListener);

                                    // synchronized (scriptFinishedLock) {
                                    //
                                    // scriptFinishedLock.set(true);
                                    // scriptFinishedLock.notifyAll();
                                    // }
                                }
                                finally {
                                    scriptContext.unregister(ScriptContextKeys.KEY_SCRIPT_PANEL);
                                }
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
        });
    }

    private void handleExecutionStatusChange(
        final ExecutionStatus executionStatus, final PropertyChangeListener executionStatusListener) {

        LOGGER.info("The execution of the script has changed: {}", executionStatus);

        switch (executionStatus) {
            case finished:
            case finishedWithErrors:
                LOGGER.info("The execution of the script has finished.");
                nodeScriptProcessor.removeExecutionStatusListener(executionStatusListener);

                // this.executionStatus = executionStatus;

                scriptContext.register(NodeScriptContextKeys.KEY_SCRIPTEXECUTIONSTATUS, executionStatus);

                synchronized (scriptFinishedLock) {
                    scriptFinishedLock.set(true);
                    scriptFinishedLock.notifyAll();
                }
                break;
            case aborted:
                LOGGER.info("The execution of the script was aborted.");
                nodeScriptProcessor.removeExecutionStatusListener(executionStatusListener);

                // this.executionStatus = executionStatus;
                scriptContext.register(NodeScriptContextKeys.KEY_SCRIPTEXECUTIONSTATUS, executionStatus);

                synchronized (scriptFinishedLock) {
                    scriptFinishedLock.set(true);
                    scriptFinishedLock.notifyAll();
                }
                break;
            default:
                LOGGER.info("The execution of the script was signalled: {}", executionStatus);
                scriptContext.register(NodeScriptContextKeys.KEY_SCRIPTEXECUTIONSTATUS, executionStatus);
                break;
        }
    }

    @Override
    public void setupWizardButtons() {
        LOGGER.info("Setup the wizard buttons.");

        fireButtonEvent(ButtonEvent.ENABLE_BUTTON, ButtonNames.BACK);
        fireButtonEvent(ButtonEvent.ENABLE_BUTTON, ButtonNames.NEXT);
        fireButtonEvent(ButtonEvent.SET_DEFAULT_BUTTON, ButtonNames.NEXT);
        fireButtonEvent(ButtonEvent.HIDE_BUTTON, ButtonNames.FINISH);

        String selectedPath = scriptContext.get(NodeScriptContextKeys.KEY_SELECTED_NODESCRIPT_PATH, String.class);
        LOGGER.info("Fetched the selected path: {}", selectedPath);

        if (StringUtils.isNotBlank(selectedPath)) {

            String encoding = NodeScriptUtils.detectFileEncoding(selectedPath);

            sbScript = new StringBuilder();

            // load the script content
            try (BufferedReader br =
                new BufferedReader(new InputStreamReader(new FileInputStream(new File(selectedPath)), encoding))) {
                for (String line; (line = br.readLine()) != null;) {
                    // process the line.
                    sbScript.append(line);
                    sbScript.append(System.lineSeparator());
                }
                // line is not visible here.
            }
            catch (FileNotFoundException ex) {
                LOGGER.warn("The selected nodescript file was not found.", ex);
            }
            catch (IOException ex) {
                LOGGER.warn("Get the content of the nodescript file failed.", ex);
            }

            scriptContext.register(ScriptContextKeys.KEY_SCRIPT_PANEL, contentPanel);

            boolean velocityPrepared = nodeScriptProcessor.prepareVelocityContext(sbScript.toString(), scriptContext);
            if (velocityPrepared) {
                fireButtonEvent(ButtonEvent.ENABLE_BUTTON, ButtonNames.NEXT);
            }
            else {
                fireButtonEvent(ButtonEvent.DISABLE_BUTTON, ButtonNames.NEXT);
            }
            try {
                final VelocityContext velocityContext =
                    scriptContext.get(ScriptContextKeys.KEY_VELOCITY_CONTEXT, VelocityContext.class);
                final List<String> inputLines = scriptContext.get(ScriptContextKeys.KEY_INPUT_LINES, List.class);
                final List<String> defineLines = scriptContext.get(ScriptContextKeys.KEY_DEFINE_LINES, List.class);
                final List<String> instructionLines =
                    scriptContext.get(ScriptContextKeys.KEY_INSTRUCTION_LINES, List.class);

                if (CollectionUtils.isNotEmpty(inputLines)) {

                    Locale locale = Locale.getDefault();
                    String lang = locale.getLanguage();
                    LOGGER.info("Current locale: {}, current lang: {}", locale, lang);

                    // query the input parameters from the user
                    InputParametersDialog tempDialog = new InputParametersDialog();
                    JPanel contentPanel =
                        tempDialog.prepareContentPanel(scriptContext, velocityContext, inputLines, instructionLines,
                            defineLines, lang, inputPanelMap);

                    this.parameterPanel = contentPanel;

                    LOGGER.info("Set the new viewport view");
                    this.scrollPane.setViewportView(contentPanel);
                    this.scrollPane.setPreferredSize(contentPanel.getPreferredSize());
                }

            }
            catch (Exception ex) {
                LOGGER.warn("Prepare the input parameters failed.", ex);
            }
        }
        else {
            fireButtonEvent(ButtonEvent.HIDE_BUTTON, ButtonNames.NEXT);
        }
    }
}
