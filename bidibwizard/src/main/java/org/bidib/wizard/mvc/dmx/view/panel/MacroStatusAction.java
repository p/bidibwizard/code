package org.bidib.wizard.mvc.dmx.view.panel;

import java.awt.Point;
import java.awt.event.ActionEvent;

import org.bidib.wizard.comm.MacroStatus;
import org.bidib.wizard.mvc.dmx.model.DmxChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The <code>MacroStatusAction</code> sets the action that is performed on the assigned macro.
 */
public class MacroStatusAction extends LocationAwareAction<DmxChannel> {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(MacroStatusAction.class);

    private MacroStatus macroStatus;

    private DmxChannel dmxChannel;

    private DmxDataItem originalDataItem;

    public MacroStatusAction(MacroStatus macroStatus, DmxChannel dmxChannel, DmxChartPanel dmxChartPanel,
        DmxDataItem originalDataItem) {
        super(macroStatus.toString(), dmxChannel, dmxChartPanel);
        this.dmxChannel = dmxChannel;
        this.macroStatus = macroStatus;
        this.originalDataItem = originalDataItem;
    }

    @Override
    public void actionPerformed(ActionEvent event) {

        Point currentMousePoint = dmxChartPanel.getCurrentMousePoint();

        String seriesKey = Integer.toString(dmxChannel.getChannelId());
        LOGGER.info("Selected key: {}, currentMousePoint: {}", seriesKey, currentMousePoint);

        if (originalDataItem != null) {
            LOGGER.info("Set action on port for item: {}, lightPortStatus: {}", originalDataItem, macroStatus);

            dmxChartPanel.setMacroStatusAction(seriesKey, originalDataItem, macroStatus);
        }
    }
}
