package org.bidib.wizard.mvc.main.view.table;

import java.awt.Component;

import javax.swing.JTable;

import org.bidib.wizard.mvc.main.model.TicksAware;
import org.bidib.wizard.utils.IntegerEditor;

public class PortTicksEditor extends IntegerEditor {

    private static final long serialVersionUID = 1L;

    public PortTicksEditor(int min, int max) {
        super(min, max);
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        if (value instanceof TicksAware) {
            TicksAware ticksAware = (TicksAware) value;
            value = ticksAware.getTicks();
        }

        return super.getTableCellEditorComponent(table, value, isSelected, row, column);
    }
}
