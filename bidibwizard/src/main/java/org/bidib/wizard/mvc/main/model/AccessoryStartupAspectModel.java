package org.bidib.wizard.mvc.main.model;

import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.collections4.Predicate;
import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.wizard.common.locale.Resources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.beans.Model;
import com.jgoodies.common.collect.ArrayListModel;

public class AccessoryStartupAspectModel extends Model {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(AccessoryStartupAspectModel.class);

    public static final String PROPERTY_SELECTED_STARTUP_ASPECT = "selectedStartupAspect";

    // public static final int ASPECT_PARAM_UNCHANGED = 255;
    //
    // public static final int ASPECT_PARAM_RESTORE = 254;

    private AccessoryAspect selectedStartupAspect;

    private ArrayListModel<AccessoryAspect> accessoryAspectList = new ArrayListModel<>();

    public ArrayListModel<AccessoryAspect> getAccessoryAspectList() {
        return accessoryAspectList;
    }

    public void clearAccessoryAspects() {
        accessoryAspectList.clear();

        // add the default values
        accessoryAspectList.add(new AccessoryAspectParam(ByteUtils.getLowByte(BidibLibrary.ASPECT_PARAM_UNCHANGED),
            Resources.getString(getClass(), "aspectParam-unchanged")));
        accessoryAspectList.add(new AccessoryAspectParam(ByteUtils.getLowByte(BidibLibrary.ASPECT_PARAM_RESTORE),
            Resources.getString(getClass(), "aspectParam-restore")));
    }

    public void addAccessoryAspect(AccessoryAspect accessoryAspect) {
        accessoryAspectList.add(accessoryAspect);
    }

    public void setSelectedStartupAspect(AccessoryAspect selectedStartupAspect) {
        LOGGER.info("Set the selected startup aspect: {}", selectedStartupAspect);

        AccessoryAspect oldValue = this.selectedStartupAspect;
        this.selectedStartupAspect = selectedStartupAspect;

        firePropertyChange(PROPERTY_SELECTED_STARTUP_ASPECT, oldValue, selectedStartupAspect);
    }

    public AccessoryAspect getSelectedStartupAspect() {
        return selectedStartupAspect;
    }

    public AccessoryAspect getAssignedAspect(final int value) {
        AccessoryAspect accessoryAspect = null;
        switch (value) {
            case BidibLibrary.ASPECT_PARAM_UNCHANGED:
                accessoryAspect = accessoryAspectList.get(0);
                break;
            case BidibLibrary.ASPECT_PARAM_RESTORE:
                accessoryAspect = accessoryAspectList.get(1);
                break;
            default:
                // lookup the macro aspects
                accessoryAspect = IterableUtils.find(accessoryAspectList, new Predicate<AccessoryAspect>() {

                    @Override
                    public boolean evaluate(AccessoryAspect aspect) {

                        if (aspect instanceof AccessoryAspectMacro) {
                            AccessoryAspectMacro aspectMacro = (AccessoryAspectMacro) aspect;
                            if (aspectMacro.getMacro() != null && value == aspectMacro.getIndex()) {

                                return true;
                            }
                        }
                        return false;
                    }
                });

                if (accessoryAspect == null) {
                    // we have a value but no matching aspect -> return 'unchanged' aspect
                    accessoryAspect = accessoryAspectList.get(0);
                }
                break;
        }

        return accessoryAspect;
    }
}
