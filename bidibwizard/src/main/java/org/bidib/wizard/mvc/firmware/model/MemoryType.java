package org.bidib.wizard.mvc.firmware.model;

@Deprecated
public enum MemoryType {
    FLASH(0), EEPROM(1);

    private final int type;

    MemoryType(int type) {
        this.type = type;
    }

    public byte getType() {
        return (byte) type;
    }

    /**
     * Create a memory type.
     * 
     * @param type
     *            numeric value of the memory type
     * 
     * @return MemoryType
     */
    public static MemoryType valueOf(int type) {
        MemoryType result = null;

        for (MemoryType e : values()) {
            if (e.type == type) {
                result = e;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("cannot map " + type + " to a memory type");
        }
        return result;
    }
}
