package org.bidib.wizard.mvc.main.view.cvdef;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import org.apache.commons.lang.StringUtils;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.exchange.vendorcv.ModeType;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.common.utils.ImageUtils;
import org.bidib.wizard.mvc.common.view.converter.StringConverter;
import org.bidib.wizard.mvc.common.view.cvdefinition.CvDefinitionTreeTableModel;
import org.bidib.wizard.mvc.common.view.validation.PropertyValidationI18NSupport;
import org.bidib.wizard.mvc.main.view.panel.NodeTree;
import org.bidib.wizard.utils.InputValidationDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.adapter.RadioButtonAdapter;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.value.ConverterValueModel;
import com.jgoodies.binding.value.ValueHolder;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.util.PropertyValidationSupport;
import com.jgoodies.validation.view.ValidationComponentUtils;

public class DccAddrRGEditor extends CvValueNumberEditor<Integer> {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(DccAddrRGEditor.class);

    private ValueModel cvHighValueModel = new ValueHolder();

    private ValueModel cvLowValueModel = new ValueHolder();

    private ValueModel converterHighValueModel;

    private ValueModel converterLowValueModel;

    private int minValue;

    private int maxValue;

    private ValueModel cvNumberHighModel = new ValueHolder();

    private ValueModel cvNumberLowModel = new ValueHolder();

    private ValueModel writeEnabled;

    private ValueHolder selectionModelInhibitTriggerMode = new ValueHolder();

    private PropertyChangeListener inhibitChangeHandler;

    private static enum InhibitTriggerMode {
        R((byte) 0), G((byte) 1);
        private final byte type;

        InhibitTriggerMode(byte type) {
            this.type = type;
        }

        public static InhibitTriggerMode valueOf(byte type) {
            InhibitTriggerMode result = null;

            for (InhibitTriggerMode e : values()) {
                if (e.type == type) {
                    result = e;
                    break;
                }
            }
            if (result == null) {
                throw new IllegalArgumentException("cannot map " + type + " to a InhibitMode");
            }
            return result;
        }

    }

    @Override
    protected ValidationResult validateModel(
        PresentationModel<CvValueBean<Integer>> model, CvValueBean<Integer> cvValueBean) {
        PropertyValidationSupport support = new PropertyValidationI18NSupport(cvValueBean, "validation");

        if (Boolean.TRUE.equals(writeEnabled.getValue())) {
            Object val = model.getBufferedModel("cvValue").getValue();
            if (val instanceof Number) {
                int value = ((Number) val).intValue();
                if (value < minValue || value > maxValue) {
                    support.addError("cvvalue_key", "invalid_value;min=" + minValue + ",max=" + maxValue);
                }
            }
            else if (val == null) {
                support.addError("cvvalue_key", "not_empty");
            }

            if (selectionModelInhibitTriggerMode.getValue() == null) {
                support.addError("cvvalue_key", "not_empty");
            }
        }
        return support.getResult();
    }

    @Override
    protected DefaultFormBuilder prepareFormPanel() {

        writeEnabled = new ValueHolder(false);

        // Get buffered model objects.
        cvValueModel = cvAdapter.getBufferedModel("cvValue");
        valueConverterModel = new ConverterValueModel(cvValueModel, new StringConverter(new DecimalFormat("#")));

        DefaultFormBuilder formBuilder =
            new DefaultFormBuilder(
                new FormLayout("3dlu, max(40dlu;pref), 30dlu, max(20dlu;pref), 3dlu, max(20dlu;pref), 0dlu:grow"));
        JTextField textCvValue = BasicComponentFactory.createTextField(valueConverterModel, false);
        textCvValue.setDocument(new InputValidationDocument(InputValidationDocument.NUMERIC));
        textCvValue.setEditable(false);

        // the needs reboot Icon is invisible by default
        ImageIcon warnIcon = ImageUtils.createImageIcon(NodeTree.class, "/icons/warn.png");
        JLabel needsRebootIcon =
            new JLabel(Resources.getString(getClass(), "rebootrequired"), warnIcon, SwingConstants.LEADING);
        needsRebootIcon.setVisible(false);

        // init the components
        ValidationComponentUtils.setMandatory(textCvValue, true);
        ValidationComponentUtils.setMessageKey(textCvValue, "validation.cvvalue_key");
        formBuilder.leadingColumnOffset(1);
        formBuilder.nextLine(0);
        formBuilder.append(BasicComponentFactory.createLabel(new ValueHolder("DCC Addr.")), 1);
        formBuilder.nextLine();
        formBuilder.append(textCvValue, 1);

        JRadioButton radioButtonRed = new JRadioButton("R");
        RadioButtonAdapter radioButtonRedAdapter =
            new RadioButtonAdapter(selectionModelInhibitTriggerMode, InhibitTriggerMode.R);
        radioButtonRed.setModel(radioButtonRedAdapter);

        JRadioButton radioButtonGreen = new JRadioButton("G");
        RadioButtonAdapter radioButtonGreenAdapter =
            new RadioButtonAdapter(selectionModelInhibitTriggerMode, InhibitTriggerMode.G);
        radioButtonGreen.setModel(radioButtonGreenAdapter);

        CellConstraints cc = new CellConstraints();
        formBuilder.add(BasicComponentFactory.createLabel(new ValueHolder("   R/G On")), cc.xyw(4, 1, 3));
        formBuilder.add(radioButtonRed, cc.xy(4, 3));
        formBuilder.add(radioButtonGreen, cc.xy(6, 3));

        // Add the reboot icon
        formBuilder.nextLine();
        formBuilder.append(needsRebootIcon, 6);

        // the 'byte' fields
        converterHighValueModel =
            new ConverterValueModel(cvHighValueModel, new StringConverter(new DecimalFormat("#")));
        converterLowValueModel = new ConverterValueModel(cvLowValueModel, new StringConverter(new DecimalFormat("#")));

        JTextField textCvHighValue = BasicComponentFactory.createTextField(converterHighValueModel, false);
        textCvHighValue.setDocument(new InputValidationDocument(InputValidationDocument.NUMERIC));
        textCvHighValue.setEditable(false);
        ValidationComponentUtils.setMessageKey(textCvHighValue, "validation.cvhighvalue_key");

        formBuilder.nextLine();
        formBuilder.append(BasicComponentFactory.createLabel(cvNumberHighModel, new CvNodeMessageFormat("{0} (CV{1})")),
            6);
        formBuilder.nextLine();
        formBuilder.append(textCvHighValue);

        JTextField textCvLowValue = BasicComponentFactory.createTextField(converterLowValueModel, false);
        textCvLowValue.setDocument(new InputValidationDocument(InputValidationDocument.NUMERIC));
        textCvLowValue.setEditable(false);
        ValidationComponentUtils.setMessageKey(textCvLowValue, "validation.cvlowvalue_key");

        formBuilder.nextLine();
        formBuilder.append(BasicComponentFactory.createLabel(cvNumberLowModel, new CvNodeMessageFormat("{0} (CV{1})")),
            6);
        formBuilder.nextLine();
        formBuilder.append(textCvLowValue);

        // add bindings for enable/disable the textfields
        PropertyConnector.connect(writeEnabled, "value", textCvValue, "editable");
        PropertyConnector.connect(needsRebootModel, "value", needsRebootIcon, "visible");

        selectionModelInhibitTriggerMode.addPropertyChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                InhibitTriggerMode inhibitTriggerMode = (InhibitTriggerMode) evt.getNewValue();
                LOGGER.debug("InhibitTriggerMode has changed: {}", inhibitTriggerMode);

                Object obj = cvLowValueModel.getValue();
                if (obj instanceof Number) {
                    int val = ((Number) obj).intValue();
                    byte low = ByteUtils.getLowByte(val);

                    low = ByteUtils.setBit(low, InhibitTriggerMode.G.equals(inhibitTriggerMode), 0);
                    LOGGER.debug("new low value: 0x{}", ByteUtils.byteToHex(low));
                    cvLowValueModel.setValue(ByteUtils.getInt(low));
                }
                else {
                    byte low = 0;
                    low = ByteUtils.setBit(low, InhibitTriggerMode.G.equals(inhibitTriggerMode), 0);
                    LOGGER.debug("new low value: 0x{}", ByteUtils.byteToHex(low));
                    cvLowValueModel.setValue(ByteUtils.getInt(low));
                }

                // triggerValidation(null);
            }
        });

        cvValueModel.addValueChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                LOGGER.debug("The cvValueModel has changed: {}", evt.getNewValue());

                if (evt.getNewValue() instanceof Number) {
                    Number num = (Number) evt.getNewValue();
                    int val = num.intValue();
                    val = val << 1;
                    LOGGER.debug("The new value is: {}", val);
                    byte highByte = ByteUtils.getHighByte(val);
                    byte lowByte = ByteUtils.getLowByte(val);

                    InhibitTriggerMode inhibitTriggerMode =
                        (InhibitTriggerMode) selectionModelInhibitTriggerMode.getValue();
                    lowByte = ByteUtils.setBit(lowByte, InhibitTriggerMode.G.equals(inhibitTriggerMode), 0);

                    cvHighValueModel.setValue(ByteUtils.getInt(highByte));
                    cvLowValueModel.setValue(ByteUtils.getInt(lowByte));
                }
                else {
                    selectionModelInhibitTriggerMode.setValue(InhibitTriggerMode.R);

                    cvHighValueModel.setValue(null);
                    cvLowValueModel.setValue(null);
                }
                triggerValidation(null);
            }
        });

        inhibitChangeHandler = new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                LOGGER.info("selectionModelInhibitTriggerMode has changed: {}", evt);

                cvValueModel.setValue(cvValueModel.getValue());
            }
        };

        selectionModelInhibitTriggerMode.addValueChangeListener(inhibitChangeHandler);

        return formBuilder;
    }

    @Override
    public void setValue(CvNode cvNode, Map<String, CvNode> cvNumberToNodeMap) {
        // set textfield editable before the value is set because the validation is triggered
        writeEnabled.setValue(!ModeType.RO.equals(cvNode.getCV().getMode()));

        LOGGER.debug("Set the value from cvNode: {}", cvNode);
        // clear the value models
        converterHighValueModel.setValue(null);
        converterLowValueModel.setValue(null);
        selectionModelInhibitTriggerMode.setValue(InhibitTriggerMode.R);

        // set the value ... triggers validation
        // set the value in the cvValueBean
        super.setValue(cvNode, cvNumberToNodeMap);

        String highCvNumber = cvValueBean.getCvNode().getCV().getHigh();
        String lowCvNumber = cvValueBean.getCvNode().getCV().getLow();

        // get the low and high value nodes
        CvNode lowNode = cvNumberToNodeMap.get(lowCvNumber);
        CvNode highNode = cvNumberToNodeMap.get(highCvNumber);

        LOGGER.debug("Prepare form panel, high cv: {}, low cv: {}", highCvNumber, lowCvNumber);
        cvNumberHighModel.setValue(highNode);
        cvNumberLowModel.setValue(lowNode);

        // prepare the integer value
        Integer lowValue = null;
        // this must be updated in the text field ...
        if (lowNode.getNewValue() instanceof Number) {
            lowValue = ((Number) lowNode.getNewValue()).intValue();
        }
        else if (StringUtils.isNotBlank(lowNode.getConfigVar().getValue())) {
            String value = lowNode.getConfigVar().getValue();
            try {
                lowValue = Integer.valueOf(value);
            }
            catch (NumberFormatException ex) {
                LOGGER.warn("Parse value failed: {}", value);
            }
        }
        cvLowValueModel.setValue(lowValue);

        Integer highValue = null;
        // this must be updated in the text field ...
        if (highNode.getNewValue() instanceof Number) {
            highValue = ((Number) highNode.getNewValue()).intValue();
        }
        else if (StringUtils.isNotBlank(highNode.getConfigVar().getValue())) {
            String value = highNode.getConfigVar().getValue();
            try {
                highValue = Integer.valueOf(value);
            }
            catch (NumberFormatException ex) {
                LOGGER.warn("Parse value failed: {}", value);
            }
        }
        cvHighValueModel.setValue(highValue);

        // set the inhibit trigger mode
        if (lowValue != null) {
            byte inhibitTriggerMode = ByteUtils.getLowByte(lowValue.intValue());
            inhibitTriggerMode = (byte) (inhibitTriggerMode & 0x01);
            LOGGER.info("New inhibitTriggerMode: {}", inhibitTriggerMode);

            // remove the listener because this would enable the save button
            selectionModelInhibitTriggerMode.removeValueChangeListener(inhibitChangeHandler);

            selectionModelInhibitTriggerMode.setValue(InhibitTriggerMode.valueOf(inhibitTriggerMode));

            // set the value in the textfield
            int correctedValue = lowValue.intValue();
            correctedValue = correctedValue >> 1;

            cvValueBean.setCvValue(correctedValue);

            selectionModelInhibitTriggerMode.addValueChangeListener(inhibitChangeHandler);
        }

        // prepare the min and max value
        minValue = 0;
        maxValue = 32767;

        checkNeedsReboot();

        triggerValidation(null);
    }

    protected void checkNeedsReboot() {
        CvNode highNode = (CvNode) cvNumberHighModel.getValue();
        CvNode lowNode = (CvNode) cvNumberLowModel.getValue();
        Boolean needsReboot = Boolean.FALSE;
        if (highNode != null && highNode.getCV() != null && highNode.getCV().isRebootneeded() != null) {
            needsReboot = highNode.getCV().isRebootneeded();
        }
        if (needsReboot.equals(Boolean.FALSE) && lowNode != null && lowNode.getCV() != null
            && lowNode.getCV().isRebootneeded() != null) {
            needsReboot = lowNode.getCV().isRebootneeded();
        }

        needsRebootModel.setValue(needsReboot);
    }

    @Override
    public void updateCvValues(Map<String, CvNode> cvNumberToNodeMap, CvDefinitionTreeTableModel treeModel) {
        CvNode cvNodeHigh = (CvNode) cvNumberHighModel.getValue();
        cvNodeHigh.setValueAt(cvHighValueModel.getValue(), CvNode.COLUMN_NEW_VALUE);

        CvNode cvNodeLow = (CvNode) cvNumberLowModel.getValue();
        cvNodeLow.setValueAt(cvLowValueModel.getValue(), CvNode.COLUMN_NEW_VALUE);

        // treeModel.refreshTreeTable(cvNodeLow);
        // treeModel.refreshTreeTable(cvNodeHigh);
    }

    @Override
    public void refreshValue() {
        CvNode cvNodeHigh = (CvNode) cvNumberHighModel.getValue();
        LOGGER.debug("Refresh values form cvNodeHigh: {}", cvNodeHigh);
        if (cvNodeHigh != null) {
            setValueInternally(cvNodeHigh, false);
        }
        CvNode cvNodeLow = (CvNode) cvNumberLowModel.getValue();
        LOGGER.debug("Refresh values form cvNodeLow: {}", cvNodeLow);
        if (cvNodeLow != null) {
            setValueInternally(cvNodeLow, false);
        }
    }
}
