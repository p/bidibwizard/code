package org.bidib.wizard.mvc.main.view.component;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.UIManager;

import org.bidib.jbidibc.core.enumeration.CommandStationState;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.dialog.EscapeDialog;
import org.bidib.wizard.mvc.preferences.model.Preferences;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.beans.Model;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.binding.value.ValueHolder;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.builder.ButtonBarBuilder;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

public class BoosterOnConfirmDialog extends EscapeDialog {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(BoosterOnConfirmDialog.class);

    private static final String ENCODED_DIALOG_COLUMN_SPECS = "pref, 10dlu, min(300dlu;pref)";

    public static final int RESULT_CONTINUE_BOOSTER_AND_COMMANDSTATION = 3;

    private int result = JOptionPane.CANCEL_OPTION;

    private ValueModel checkDoNotAskInFutureValueModel;

    private final CommandStationStartModel commandStationStartModel;

    private JComponent[] modeButtons;

    public BoosterOnConfirmDialog(Frame frame, boolean modal) {
        super(frame, Resources.getString(BoosterOnConfirmDialog.class, "title"), modal);

        commandStationStartModel = new CommandStationStartModel();

        getContentPane().setLayout(new BorderLayout());

        DefaultFormBuilder builder =
            new DefaultFormBuilder(new FormLayout(ENCODED_DIALOG_COLUMN_SPECS,
                "pref, 10dlu, top:35dlu, 5dlu, pref, 5dlu, pref, 30dlu, pref"));
        builder.border(Borders.DIALOG);

        JLabel iconLabel = new JLabel(UIManager.getIcon("OptionPane.warningIcon"));

        int row = 1;
        CellConstraints cc = new CellConstraints();
        builder.add(iconLabel, cc.xywh(1, row, 1, 2));

        JLabel messageLabel = new JLabel(Resources.getString(getClass(), "message-warn"));
        Font font = messageLabel.getFont();
        font = font.deriveFont(16.0f);
        messageLabel.setFont(font);
        messageLabel.setForeground(Color.RED);

        builder.add(messageLabel, cc.xy(3, row));
        row += 2;

        JLabel messageInfoLabel = new JLabel(Resources.getString(getClass(), "message"));
        builder.add(messageInfoLabel, cc.xy(3, row));
        row += 2;

        checkDoNotAskInFutureValueModel = new ValueHolder(Preferences.getInstance().isAllBoosterOnDoNotConfirmSwitch());
        if (!((Boolean) checkDoNotAskInFutureValueModel.getValue()).booleanValue()) {

            JCheckBox checkDoNotAskInFuture =
                BasicComponentFactory.createCheckBox(checkDoNotAskInFutureValueModel,
                    Resources.getString(getClass(), "do-not-ask"));
            builder.add(checkDoNotAskInFuture, cc.xy(3, row));

            row += 2;
        }
        else {
            // we do not use it
            checkDoNotAskInFutureValueModel = null;
        }

        ButtonBarBuilder startModeButtons = new ButtonBarBuilder();
        startModeButtons.addFixed(new JLabel("CS state"));
        startModeButtons.addRelatedGap();
        ValueModel modeModel =
            new PropertyAdapter<CommandStationStartModel>(commandStationStartModel,
                CommandStationStartModel.PROPERTYNAME_START_MODE, true);
        modeButtons = new JComponent[CommandStationStartMode.values().length];
        int index = 0;
        for (CommandStationStartMode mode : CommandStationStartMode.values()) {

            JRadioButton radio =
                BasicComponentFactory.createRadioButton(modeModel, mode,
                    Resources.getString(CommandStationStartMode.class, mode.getKey()));
            modeButtons[index++] = radio;

            // add radio button
            startModeButtons.addButton(radio);
        }

        builder.add(startModeButtons.build(), cc.xy(3, row));
        row++;

        // buttons
        JButton continueButton = new JButton(Resources.getString(getClass(), "continue"));

        continueButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                fireContinue();
            }
        });

        JButton continueBoosterAndCommandStationButton =
            new JButton(Resources.getString(getClass(), "continueBoosterAndCommandStation"));

        continueBoosterAndCommandStationButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                fireContinueBoosterAndCommandStation();
            }
        });

        JButton cancel = new JButton(Resources.getString(getClass(), "cancel"));

        cancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                fireCancel();
            }
        });

        JPanel buttons =
            new ButtonBarBuilder()
                /* .addGlue() */.addButton(continueButton, continueBoosterAndCommandStationButton, cancel).build();

        builder.add(buttons, cc.xy(3, row));

        getContentPane().add(builder.build());

        commandStationStartModel.setStartMode(
            CommandStationStartMode.valueOf(Preferences.getInstance().getAllBoosterOnRequestedCommandStationState()));

        pack();

        setLocationRelativeTo(frame);
        setMinimumSize(getSize());
        setVisible(true);
    }

    private void fireContinue() {
        LOGGER.info("Continue operation.");

        result = JOptionPane.YES_OPTION;

        Preferences prefs = Preferences.getInstance();
        if (checkDoNotAskInFutureValueModel != null
            && ((Boolean) checkDoNotAskInFutureValueModel.getValue()).booleanValue()) {
            LOGGER.info("Do not show confirm dialog in the future.");

            prefs.setAllBoosterOnDoNotConfirmSwitch(true, result);
        }
        prefs.setAllBoosterOnRequestedCommandStationState(getCommandStationState());
        prefs.save(null);
    }

    private void fireContinueBoosterAndCommandStation() {
        LOGGER.info("Continue operation with booster and command station.");

        result = RESULT_CONTINUE_BOOSTER_AND_COMMANDSTATION;

        Preferences prefs = Preferences.getInstance();
        if (checkDoNotAskInFutureValueModel != null
            && ((Boolean) checkDoNotAskInFutureValueModel.getValue()).booleanValue()) {
            LOGGER.info("Do not show confirm dialog in the future.");

            prefs.setAllBoosterOnDoNotConfirmSwitch(true, result);
        }
        prefs.setAllBoosterOnRequestedCommandStationState(getCommandStationState());
        prefs.save(null);
    }

    private void fireCancel() {

    }

    public int getResult() {
        return result;
    }

    /**
     * @return the selected start mode for the command station
     */
    public CommandStationState getCommandStationState() {
        return commandStationStartModel.getStartMode().getValue();
    }

    public enum CommandStationStartMode {
        WATCHDOG("watchdog", CommandStationState.GO), IGN_WATCHDOG("ign-watchdog", CommandStationState.GO_IGN_WD);

        private final String key;

        private final CommandStationState value;

        private CommandStationStartMode(String key, CommandStationState value) {
            this.key = key;
            this.value = value;
        }

        public String getKey() {
            return key;
        }

        public CommandStationState getValue() {
            return value;
        }

        public static CommandStationStartMode valueOf(CommandStationState value) {
            CommandStationStartMode result = null;

            for (CommandStationStartMode e : values()) {
                if (e.value == value) {
                    result = e;
                    break;
                }
            }
            if (result == null) {
                throw new IllegalArgumentException("cannot map " + value + " to a command station state");
            }
            return result;
        }

    }

    public static final class CommandStationStartModel extends Model {
        private static final long serialVersionUID = 1L;

        public static final String PROPERTYNAME_START_MODE = "startMode";

        private CommandStationStartMode startMode = CommandStationStartMode.WATCHDOG;

        /**
         * @return the startMode
         */
        public CommandStationStartMode getStartMode() {
            return startMode;
        }

        /**
         * @param startMode
         *            the startMode to set
         */
        public void setStartMode(CommandStationStartMode startMode) {
            CommandStationStartMode oldMode = this.startMode;
            this.startMode = startMode;

            firePropertyChange(PROPERTYNAME_START_MODE, oldMode, startMode);
        }
    }
}
