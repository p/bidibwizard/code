package org.bidib.wizard.mvc.main.model.listener;

import java.util.Collection;

import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.mvc.main.model.FeedbackAddressData;
import org.bidib.wizard.mvc.main.model.FeedbackConfidenceData;
import org.bidib.wizard.mvc.main.model.FeedbackDynStateData;
import org.bidib.wizard.mvc.main.model.FeedbackPort;

public interface FeedbackPortListener<S extends BidibStatus> extends PortListener<S> {
    /**
     * The reported address data on the port has changed.
     * 
     * @param port
     *            the port
     * @param addresses
     *            the address data
     */
    void addressesChanged(FeedbackPort port, Collection<FeedbackAddressData> addresses);

    /**
     * The reported dynamic state values on the port have changed.
     * 
     * @param port
     *            the port
     * @param dynStates
     *            the dynState values
     */
    void dynStatesChanged(FeedbackPort port, Collection<FeedbackDynStateData> dynStates);

    /**
     * The reported confidence on the feedback port has changed.
     * 
     * @param port
     *            the port
     * @param confidence
     *            the confidence data
     */
    void confidenceChanged(FeedbackPort port, FeedbackConfidenceData confidence);

    /**
     * The reported speed on the feedback port has changed.
     * 
     * @param port
     *            the port
     * @param address
     *            the decoder address
     * @param speed
     *            the new speed value
     */
    void speedChanged(FeedbackPort port, int address, int speed);
}
