package org.bidib.wizard.mvc.main.view;

import java.awt.Component;
import java.awt.Point;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.bidib.jbidibc.core.AddressData;
import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.Feature;
import org.bidib.jbidibc.core.StringData;
import org.bidib.jbidibc.core.enumeration.AccessoryAcknowledge;
import org.bidib.jbidibc.core.enumeration.CommandStationPom;
import org.bidib.jbidibc.core.enumeration.CommandStationPt;
import org.bidib.jbidibc.core.enumeration.CommandStationState;
import org.bidib.jbidibc.core.enumeration.PomAcknowledge;
import org.bidib.jbidibc.core.enumeration.TimeBaseUnitEnum;
import org.bidib.jbidibc.core.enumeration.TimingControlEnum;
import org.bidib.jbidibc.core.exception.InvalidConfigurationException;
import org.bidib.jbidibc.core.node.ConfigurationVariable;
import org.bidib.jbidibc.core.utils.NodeUtils;
import org.bidib.jbidibc.core.utils.ProductUtils;
import org.bidib.wizard.comm.CommunicationFactory;
import org.bidib.wizard.common.context.ApplicationContext;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.dialog.FileDialog;
import org.bidib.wizard.dialog.NodeDetailsDialog;
import org.bidib.wizard.labels.AnalogPortLabelFactory;
import org.bidib.wizard.labels.BacklightPortLabelFactory;
import org.bidib.wizard.labels.FeedbackPortLabelFactory;
import org.bidib.wizard.labels.InputPortLabelFactory;
import org.bidib.wizard.labels.Labels;
import org.bidib.wizard.labels.LightPortLabelFactory;
import org.bidib.wizard.labels.MotorPortLabelFactory;
import org.bidib.wizard.labels.NodeLabelFactory;
import org.bidib.wizard.labels.PortLabelUtils;
import org.bidib.wizard.labels.ServoPortLabelFactory;
import org.bidib.wizard.labels.SoundPortLabelFactory;
import org.bidib.wizard.labels.SwitchPortLabelFactory;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.bidib.wizard.mvc.accessory.controller.AccessoryController;
import org.bidib.wizard.mvc.accessory.controller.listener.AccessoryControllerListener;
import org.bidib.wizard.mvc.common.view.cvdefinition.CvDefinitionTreeTableModel;
import org.bidib.wizard.mvc.common.view.cvdefinition.CvDefintionPanelProvider;
import org.bidib.wizard.mvc.common.view.cvdefinition.CvValueUtils;
import org.bidib.wizard.mvc.dmx.controller.DmxModelerController;
import org.bidib.wizard.mvc.features.controller.FeaturesController;
import org.bidib.wizard.mvc.features.controller.listener.FeaturesControllerListener;
import org.bidib.wizard.mvc.firmware.controller.FirmwareController;
import org.bidib.wizard.mvc.firmware.controller.listener.FirmwareControllerListener;
import org.bidib.wizard.mvc.loco.controller.LocoController;
import org.bidib.wizard.mvc.locolist.controller.LocoTableController;
import org.bidib.wizard.mvc.main.controller.CommandStationService;
import org.bidib.wizard.mvc.main.controller.MainController;
import org.bidib.wizard.mvc.main.controller.MainControllerInterface;
import org.bidib.wizard.mvc.main.model.Accessory;
import org.bidib.wizard.mvc.main.model.BacklightPort;
import org.bidib.wizard.mvc.main.model.LightPort;
import org.bidib.wizard.mvc.main.model.Macro;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.model.NodeState;
import org.bidib.wizard.mvc.main.model.ServoPort;
import org.bidib.wizard.mvc.main.model.SwitchPort;
import org.bidib.wizard.mvc.main.view.component.NodeErrorsDialog;
import org.bidib.wizard.mvc.main.view.component.SaveNodeConfigurationDialog;
import org.bidib.wizard.mvc.main.view.panel.CvDefinitionTreeHelper;
import org.bidib.wizard.mvc.main.view.panel.listener.NodeListActionListener;
import org.bidib.wizard.mvc.main.view.statusbar.StatusBar;
import org.bidib.wizard.mvc.pom.controller.PomProgrammerController;
import org.bidib.wizard.mvc.pom.controller.listener.PomProgrammerControllerListener;
import org.bidib.wizard.mvc.preferences.model.Preferences;
import org.bidib.wizard.mvc.pt.controller.PtProgrammerController;
import org.bidib.wizard.mvc.pt.controller.listener.PtProgrammerControllerListener;
import org.bidib.wizard.mvc.pt.view.PtConfirmDialog;
import org.bidib.wizard.utils.NodeStateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jidesoft.grid.DefaultExpandableRow;

public class MainNodeListActionListener implements NodeListActionListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(MainNodeListActionListener.class);

    private static final String NODE_EXTENSION = "node";

    // description, suffix for node files
    private static String nodeDescription;

    private static FileFilter nodeFilter;

    private final MainView view;

    private final MainModel model;

    public MainNodeListActionListener(final MainView view, final MainModel model) {
        this.view = view;
        this.model = model;

        nodeDescription = Resources.getString(MainController.class, "nodeDescription");
        nodeFilter = new FileNameExtensionFilter(nodeDescription, NODE_EXTENSION);
    }

    private void setWaitCursor() {
        view.setBusy(true);
    }

    private void setDefaultCursor() {
        view.setBusy(false);
    }

    @Override
    public void enableAddressMessages(Node node) {
        CommunicationFactory.getInstance().setAddressMessagesEnabled(node.getNode(), node.isAddressMessagesEnabled());
    }

    @Override
    public void enableDccStart(Node node) {
        CommunicationFactory.getInstance().setDccStartEnabled(node.getNode(), node.isDccStartEnabled());
    }

    @Override
    public void enableExternalStart(Node node) {
        CommunicationFactory.getInstance().setExternalStartMacroEnabled(node.getNode(), node.isExternalStartEnabled());
    }

    @Override
    public void enableFeedbackMessages(Node node) {
        CommunicationFactory.getInstance().setFeedbackMessagesEnabled(node.getNode(), node.isFeedbackMessagesEnabled());
    }

    @Override
    public void disableFeedbackMirror(Node node, boolean disable) {
        CommunicationFactory.getInstance().setFeedbackMirrorDisabled(node.getNode(), disable);
    }

    @Override
    public void enableKeyMessages(Node node) {
        CommunicationFactory.getInstance().setKeyMessagesEnabled(node.getNode(), node.isKeyMessagesEnabled());
    }

    @Override
    public void features(Node node, int x, int y) {
        setWaitCursor();

        final FeaturesController featuresController = new FeaturesController(view, model, node, x, y);

        featuresController.addFeaturesControllerListener(new FeaturesControllerListener() {
            @Override
            public void close() {
                setDefaultCursor();
            }

            @Override
            public void readAll(Node node) {
                StopWatch sw = new StopWatch();
                sw.start();

                List<Feature> features = CommunicationFactory.getInstance().getFeatures(node.getNode(), true);

                // keep the features in the node
                List<Feature> featureList = new LinkedList<>();
                featureList.addAll(features);
                node.getNode().setFeatures(featureList);

                // set the features in the controller
                featuresController.setFeatures(features);
                sw.stop();

                LOGGER.info("Load features has finished! Total loading duration: {}", sw);
            }

            @Override
            public void write(Node node, Feature feature) {
                LOGGER.debug("Write feature to node: {}, feature: {}", node, feature);
                CommunicationFactory.getInstance().writeFeature(node.getNode(), feature);

                // update the main model
                // model.updateFeature(feature);
            }
        });
        featuresController.start();
    }

    @Override
    public void exportNode(final Node node) {

        LOGGER.info("export node: {}", node);

        String nodeLabel = org.bidib.wizard.utils.NodeUtils.prepareLabel(node);
        LOGGER.info("export node, prepared nodeLabel: {}", nodeLabel);

        // export node data
        FileDialog dialog = new FileDialog(view, FileDialog.SAVE, nodeLabel + "." + NODE_EXTENSION, nodeFilter) {
            private JCheckBox checkLoadMacroContent;

            private JCheckBox checkLoadFeatures;

            private JCheckBox checkLoadCVs;

            @Override
            public void approve(String fileName) {

                try {
                    setWaitCursor();
                    LOGGER.info("Save node state for node: {}, fileName: {}", node, fileName);

                    NodeState nodeState =
                        NodeStateUtils.prepareNodeState(model, CommunicationFactory.getInstance(), node,
                            (checkLoadMacroContent != null ? checkLoadMacroContent.isSelected() : false),
                            checkLoadFeatures.isSelected(), checkLoadCVs.isSelected());
                    nodeState.save(fileName);

                    LOGGER.info("Save node state passed, fileName: {}", fileName);

                    view.setStatusText(
                        String.format(Resources.getString(MainController.class, "exportedNodeState"), fileName),
                        StatusBar.DISPLAY_NORMAL);
                }
                catch (IOException e) {
                    LOGGER.warn("Save node state failed.", e);
                    throw new RuntimeException(e);
                }
                finally {
                    setDefaultCursor();
                }
            }

            @Override
            protected Component getAdditionalPanel() {
                // prepare a panel with checkboxes for loading macro content before export
                JPanel additionalPanel = new JPanel();
                additionalPanel.setLayout(new BoxLayout(additionalPanel, BoxLayout.PAGE_AXIS));

                if (NodeUtils.hasSwitchFunctions(node.getUniqueId())) {
                    checkLoadMacroContent =
                        new JCheckBox(Resources.getString(MainController.class, "checkLoadMacroContent"), true);
                    additionalPanel.add(checkLoadMacroContent);
                }
                checkLoadFeatures = new JCheckBox(Resources.getString(MainController.class, "checkLoadFeatures"), true);
                additionalPanel.add(checkLoadFeatures);
                checkLoadCVs = new JCheckBox(Resources.getString(MainController.class, "checkLoadCVs"), true);
                additionalPanel.add(checkLoadCVs);
                return additionalPanel;
            }
        };
        dialog.showDialog();
    }

    @Override
    public void importNode(final Node node) {
        // import node data
        final Map<String, Object> importParams = new HashMap<String, Object>();

        FileDialog dialog = new FileDialog(view, FileDialog.OPEN, null, nodeFilter) {
            private JCheckBox checkRestoreMacroContent;

            private JCheckBox checkRestoreNodeString;

            private JCheckBox checkRestoreFeatures;

            private JCheckBox checkRestoreCVs;

            @Override
            public void approve(String fileName) {
                try {
                    setWaitCursor();
                    LOGGER.info("Start importing node, fileName: {}", fileName);

                    ApplicationContext applicationContext = DefaultApplicationContext.getInstance();

                    // restore the node state into memory
                    NodeStateUtils.loadNodeState(view, fileName, model, node, importParams, applicationContext,
                        (checkRestoreNodeString != null ? checkRestoreNodeString.isSelected() : null),
                        (checkRestoreMacroContent != null ? checkRestoreMacroContent.isSelected() : null),
                        (checkRestoreFeatures != null ? checkRestoreFeatures.isSelected() : null),
                        (checkRestoreCVs != null ? checkRestoreCVs.isSelected() : null));

                    // save the node labels ....
                    LOGGER.info("Save the node labels.");
                    try {
                        long uniqueId = node.getUniqueId();

                        // save the port labels locally
                        new AnalogPortLabelFactory().saveLabels(uniqueId,
                            applicationContext.get(DefaultApplicationContext.KEY_ANALOGPORT_LABELS, Labels.class));
                        new LightPortLabelFactory().saveLabels(uniqueId,
                            applicationContext.get(DefaultApplicationContext.KEY_LIGHTPORT_LABELS, Labels.class));
                        new ServoPortLabelFactory().saveLabels(uniqueId,
                            applicationContext.get(DefaultApplicationContext.KEY_SERVOPORT_LABELS, Labels.class));
                        new BacklightPortLabelFactory().saveLabels(uniqueId,
                            applicationContext.get(DefaultApplicationContext.KEY_BACKLIGHTPORT_LABELS, Labels.class));
                        new FeedbackPortLabelFactory().saveLabels(uniqueId,
                            applicationContext.get(DefaultApplicationContext.KEY_FEEDBACKPORT_LABELS, Labels.class));
                        new InputPortLabelFactory().saveLabels(uniqueId,
                            applicationContext.get(DefaultApplicationContext.KEY_INPUTPORT_LABELS, Labels.class));
                        new SoundPortLabelFactory().saveLabels(uniqueId,
                            applicationContext.get(DefaultApplicationContext.KEY_SOUNDPORT_LABELS, Labels.class));
                        new SwitchPortLabelFactory().saveLabels(uniqueId,
                            applicationContext.get(DefaultApplicationContext.KEY_SWITCHPORT_LABELS, Labels.class));
                        new MotorPortLabelFactory().saveLabels(uniqueId,
                            applicationContext.get(DefaultApplicationContext.KEY_MOTORPORT_LABELS, Labels.class));
                    }
                    catch (InvalidConfigurationException ex) {
                        LOGGER.warn("Save port labels failed.", ex);

                        String labelPath = ex.getReason();
                        JOptionPane.showMessageDialog(JOptionPane.getFrameForComponent(null),
                            Resources.getString(Labels.class, "labelfileerror.message", new Object[] { labelPath }),
                            Resources.getString(Labels.class, "labelfileerror.title"), JOptionPane.ERROR_MESSAGE);
                    }

                    view.setStatusText(
                        String.format(Resources.getString(MainController.class, "importedNodeState"), fileName),
                        StatusBar.DISPLAY_NORMAL);

                    // show a dialog with some checkboxes to allow the user to select all options (port config,
                    // macros, accessories) and move this to saveNode ...
                    SaveNodeConfigurationDialog saveNodeConfigurationDialog =
                        new SaveNodeConfigurationDialog(view, null, null, node, importParams, true) {
                            private static final long serialVersionUID = 1L;

                            @Override
                            protected void fireContinue(Node node) {

                                LOGGER.info("User wants to save the macros to node.");
                                Map<String, Object> params = new HashMap<String, Object>();

                                params.put(SAVE_MACROS, isSaveMacros());
                                params.put(SAVE_ACCESSORIES, isSaveAccessories());
                                params.put(SAVE_BACKLIGHTPORTS, isSaveBacklightPorts());
                                params.put(SAVE_LIGHTPORTS, isSaveLightPorts());
                                params.put(SAVE_SERVOPORTS, isSaveServoPorts());
                                params.put(SAVE_SWITCHPORTS, isSaveSwitchPorts());
                                params.put(SAVE_FEATURES, isSaveFeatures());
                                params.put(SAVE_CVS, isSaveConfigurationVariables());

                                saveNode(node, params);

                                if (params.containsKey(SAVE_ERRORS)) {
                                    LOGGER.warn("The save on node operation has finished with errors!!!");
                                    // show an error dialog with the list of save errors during update node
                                    NodeErrorsDialog nodeErrorsDialog = new NodeErrorsDialog(view, null, true);

                                    // set the error information
                                    nodeErrorsDialog.setErrors(
                                        Resources.getString(MainController.class, "save-values-on-node-failed"),
                                        (List<String>) params.get(SAVE_ERRORS));
                                    nodeErrorsDialog.showDialog();
                                }
                            }

                            @Override
                            protected void fireCancel(Node node) {
                            }
                        };

                    saveNodeConfigurationDialog.setSaveFeaturesEnabled(checkRestoreFeatures.isSelected());
                    saveNodeConfigurationDialog.setSaveCVsEnabled(checkRestoreCVs.isSelected());

                    saveNodeConfigurationDialog.showDialog();
                }
                catch (InvalidConfigurationException ex) {
                    LOGGER.warn("Load node configuration failed.", ex);

                    // show a message to the user
                    throw new RuntimeException(Resources.getString(NodeState.class, ex.getReason()));
                }
                catch (Exception ex) {
                    LOGGER.warn("Load node configuration failed.", ex);

                    // show a message to the user
                    throw new RuntimeException("Load node configuration failed.");
                }
                finally {
                    setDefaultCursor();
                }
            }

            @Override
            protected Component getAdditionalPanel() {

                boolean defaultImportCVOnly = false;
                // check if the current node is the OneControl with GPIO
                if (ProductUtils.isOneControl(node.getUniqueId()) || ProductUtils.isOneDriveTurn(node.getUniqueId())) {
                    int result =
                        JOptionPane.showConfirmDialog(JOptionPane.getFrameForComponent(null),
                            Resources.getString(MainNodeListActionListener.class, "importOneControl.message"),
                            Resources.getString(MainNodeListActionListener.class, "importOneControl.title"),
                            JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE);

                    if (result == JOptionPane.OK_OPTION) {
                        LOGGER.info("User selected OK to uncheck all options expect the import of CV values.");
                        defaultImportCVOnly = true;
                        importParams.put(SaveNodeConfigurationDialog.DEFAULT_IMPORT_CV_ONLY, defaultImportCVOnly);
                    }
                }

                // prepare a panel with checkboxes for loading macro content before export
                JPanel additionalPanel = new JPanel();
                additionalPanel.setLayout(new BoxLayout(additionalPanel, BoxLayout.PAGE_AXIS));

                checkRestoreNodeString =
                    new JCheckBox(Resources.getString(MainController.class, "checkRestoreNodeString"), true);
                additionalPanel.add(checkRestoreNodeString);

                if (NodeUtils.hasAccessoryFunctions(node.getUniqueId())) {
                    // TODO check if this is really necessary ... user can use store permanently manually
                    checkRestoreMacroContent =
                        new JCheckBox(Resources.getString(MainController.class, "checkRestoreMacroContent"),
                            !defaultImportCVOnly);
                    additionalPanel.add(checkRestoreMacroContent);
                    if (defaultImportCVOnly) {
                        checkRestoreMacroContent.setEnabled(false);
                    }
                }
                checkRestoreFeatures =
                    new JCheckBox(Resources.getString(MainController.class, "checkRestoreFeatures"), false);
                additionalPanel.add(checkRestoreFeatures);
                if (defaultImportCVOnly) {
                    checkRestoreFeatures.setEnabled(false);
                }
                checkRestoreCVs =
                    new JCheckBox(Resources.getString(MainController.class, "checkRestoreCVs"), defaultImportCVOnly);
                additionalPanel.add(checkRestoreCVs);
                if (defaultImportCVOnly) {
                    checkRestoreCVs.setEnabled(false);
                }

                return additionalPanel;
            }
        };
        dialog.showDialog();
    }

    @Override
    public void firmwareUpdate(Node node, int x, int y) {
        setWaitCursor();

        FirmwareController firmwareController = new FirmwareController(view, node, x, y, model);

        firmwareController.addFirmwareControllerListener(new FirmwareControllerListener() {
            @Override
            public void close() {
                LOGGER.info("The firmware update view was closed.");
                setDefaultCursor();
            }
        });
        firmwareController.start();
    }

    @Override
    public void identify(Node node) {
        CommunicationFactory.getInstance().identify(node.getNode(), node.getIdentifyState());
    }

    @Override
    public void ping(Node node, byte data) {
        CommunicationFactory.getInstance().ping(node.getNode(), data);
    }

    @Override
    public Long readUniqueId(Node node) {
        return CommunicationFactory.getInstance().readUniqueId(node.getNode());
    }

    @Override
    public void labelChanged(final Node node, String label) {
        // final Node node = (Node) object;
        LOGGER.info("The label has changed, node: {}, label: {}", node, label);

        // write label to node if feature is available
        int stringSize = node.getNode().getStringSize();
        if (stringSize > 0) {
            String nodeString = label;
            if (label.length() > stringSize) {
                nodeString = label.substring(0, stringSize);
                LOGGER.info("Shrinked node string from '{}' to:' {}'", label, nodeString);
            }
            String returnedLabel =
                CommunicationFactory.getInstance().setString(node.getNode(), StringData.NAMESPACE_NODE,
                    StringData.INDEX_USERNAME, nodeString);
            LOGGER.info("Set the new string on node returned: {}", returnedLabel);
        }

        try {
            long uniqueId = node.getNode().getUniqueId();
            Labels nodeLabels =
                DefaultApplicationContext.getInstance().get(DefaultApplicationContext.KEY_NODE_LABELS, Labels.class);
            PortLabelUtils.replaceLabel(nodeLabels, uniqueId, 0, label, NodeLabelFactory.DEFAULT_LABELTYPE);
            new NodeLabelFactory().saveLabels(uniqueId, nodeLabels);
        }
        catch (InvalidConfigurationException ex) {
            LOGGER.warn("Save node labels failed.", ex);

            String labelPath = ex.getReason();
            JOptionPane.showMessageDialog(JOptionPane.getFrameForComponent(null),
                Resources.getString(Labels.class, "labelfileerror.message", new Object[] { labelPath }),
                Resources.getString(Labels.class, "labelfileerror.title"), JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public void loco(final Node node, int x, int y) {
        LocoController locoController = new LocoController(node, view, x, y);

        locoController.start();
    }

    @Override
    public void dccAccessory(Node node, int x, int y) {
        final AccessoryController accessoryController = new AccessoryController(node, view, x, y);
        accessoryController.addAccessoryControllerListener(new AccessoryControllerListener() {

            @Override
            public AccessoryAcknowledge sendAccessoryRequest(
                Node node, AddressData dccAddress, Integer aspect, Integer switchTime, TimeBaseUnitEnum timeBaseUnit,
                TimingControlEnum timingControl) {
                LOGGER.info("send accessory request, node: {}", node);
                AccessoryAcknowledge acknowledgde =
                    CommunicationFactory.getInstance().setDccAccessory(node.getNode(), dccAddress, aspect, switchTime,
                        timeBaseUnit, timingControl);

                return acknowledgde;
            }
        });

        accessoryController.start();
    }

    @Override
    public void locoCv(Node node, int x, int y) {
        // setWaitCursor();

        final PomProgrammerController pomProgrammerController = new PomProgrammerController(node, view, x, y);
        pomProgrammerController.addPomProgrammerControllerListener(new PomProgrammerControllerListener() {

            @Override
            public void sendRequest(
                Node node, AddressData locoAddress, CommandStationPom opCode, int cvNumber, int cvValue) {
                LOGGER.info("Send POM request.");

                PomAcknowledge pomAck =
                    CommunicationFactory.getInstance().sendCvPomRequest(node.getNode(), locoAddress, opCode, cvNumber,
                        cvValue);
                LOGGER.info("Received pomAck: {}", pomAck);
            }

            @Override
            public void close() {
                // setDefaultCursor();
            }
        });

        pomProgrammerController.start(view.getDesktop());
    }

    @Override
    public void locoCvPt(Node node, int x, int y) {
        setWaitCursor();

        if (!Preferences.getInstance().isPtModeDoNotConfirmSwitch()) {
            PtConfirmDialog ptConfirmDialog = new PtConfirmDialog(node, true, new Point(x, y));
            if (JOptionPane.CANCEL_OPTION == ptConfirmDialog.getResult()) {
                LOGGER.info("User cancelled ptConfirmDialog.");
                setDefaultCursor();
                return;
            }
        }
        else {
            LOGGER.info("Switch to PT programming is disabled by user!");
        }
        setDefaultCursor();

        final PtProgrammerController ptProgrammerController = new PtProgrammerController(node, view, x, y);

        ptProgrammerController.addPtProgrammerControllerListener(new PtProgrammerControllerListener() {
            @Override
            public void close() {
                // setDefaultCursor();
            }

            @Override
            public void sendRequest(Node node, CommandStationPt opCode, int cvNumber, int cvValue) {
                LOGGER.info("Send PT request, opCode: {}, cvNumber: {}, cvValue: {}", opCode, cvNumber, cvValue);
                CommunicationFactory.getInstance().sendCvPtRequest(node.getNode(), opCode, cvNumber, cvValue);
            }

            @Override
            public void sendCommandStationStateRequest(Node node, CommandStationState commandStationState) {
                LOGGER.info("Send the commandStationState: {}", commandStationState);

                // TODO must start the watchdog

                // CommunicationFactory.getInstance().setCommandStationState(node.getNode(), commandStationState);

                CommandStationService commandStationService =
                    DefaultApplicationContext.getInstance().get(DefaultApplicationContext.KEY_COMMAND_STATION_SERVICE,
                        CommandStationService.class);
                commandStationService.setCommandStationState(node, commandStationState);
            }

            @Override
            public CommandStationState getCurrentCommandStationState(Node node) {
                LOGGER.info("Query the command station state.");
                return CommunicationFactory.getInstance().queryCommandStationState(node.getNode());
            }
        });
        ptProgrammerController.start(view.getDesktop());
    }

    @Override
    public void nodeDetails(Node node, int x, int y) {
        // show the node details dialog
        new NodeDetailsDialog(node, CommunicationFactory.getInstance(), x, y);

    }

    @Override
    public void saveNode(Node node, Map<String, Object> params) {
        LOGGER.info("Save the configuration to the node: {}", node);
        if (params == null) {
            // no params provided, save all configuration values
            params = new HashMap<String, Object>();
            params.put(SaveNodeConfigurationDialog.SAVE_MACROS, Boolean.TRUE);
            params.put(SaveNodeConfigurationDialog.SAVE_ACCESSORIES, Boolean.TRUE);
            params.put(SaveNodeConfigurationDialog.SAVE_BACKLIGHTPORTS, Boolean.TRUE);
            params.put(SaveNodeConfigurationDialog.SAVE_LIGHTPORTS, Boolean.TRUE);
            params.put(SaveNodeConfigurationDialog.SAVE_SERVOPORTS, Boolean.TRUE);
            params.put(SaveNodeConfigurationDialog.SAVE_SWITCHPORTS, Boolean.TRUE);
            params.put(SaveNodeConfigurationDialog.SAVE_FEATURES, Boolean.FALSE);
            params.put(SaveNodeConfigurationDialog.SAVE_CVS, Boolean.FALSE);
        }

        if (params.get(SaveNodeConfigurationDialog.SAVE_FEATURES) != null
            && ((Boolean) params.get(SaveNodeConfigurationDialog.SAVE_FEATURES)).booleanValue()) {

            LOGGER.info("Save the features on the node.");
            List<Feature> features = node.getNode().getFeatures();

            if (CollectionUtils.isNotEmpty(features)) {
                // use new list to prevent ConcurrentModificationException
                features = new LinkedList<>(features);
                for (Feature feature : features) {
                    try {
                        CommunicationFactory.getInstance().writeFeature(node.getNode(), feature);
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Write feature to node failed: {}", feature, ex);
                        StringBuffer sb =
                            new StringBuffer("Save Feature ")
                                .append(feature.getFeatureName()).append(" on node failed, value: ")
                                .append(feature.getValue());
                        addError(params, sb.toString());
                    }
                }
            }
            else {
                LOGGER.warn("No features to transfer available.");
            }
        }

        if (params.get(SaveNodeConfigurationDialog.SAVE_CVS) != null
            && ((Boolean) params.get(SaveNodeConfigurationDialog.SAVE_CVS)).booleanValue()) {
            LOGGER.info("Save the CVs on the node.");

            CvDefinitionTreeTableModel treeModel = node.getCvDefinitionTreeTableModel();
            if (treeModel != null) {

                boolean hasPendingCVChanges = CvDefinitionTreeHelper.hasPendingChanges(treeModel);
                LOGGER.info("Before transfer CV values to node, hasPendingCVChanges: {}", hasPendingCVChanges);

                // write the values to the node
                final CvDefintionPanelProvider provider = new CvDefintionPanelProvider() {

                    @Override
                    public void writeConfigVariables(List<ConfigurationVariable> cvList) {
                        LOGGER.info("Write cv variables to node.");

                        // write the cv values to the node
                        List<ConfigurationVariable> configVars =
                            CommunicationFactory.getInstance().writeConfigurationVariables(node.getNode(), cvList);

                        // iterate over the collection of stored variables in the model and update the values.
                        // After that notify the tree and delete the new values that are now stored in the node
                        model.updateConfigurationVariableValues(configVars, false);
                    }

                    @Override
                    public void refreshDisplayedValues() {
                    }

                    @Override
                    public void checkPendingChanges() {
                    }
                };
                DefaultExpandableRow root = (DefaultExpandableRow) treeModel.getRoot();
                CvValueUtils.writeCvValues(node, root, node.getCvNumberToJideNodeMap(), provider);

            }
        }

        if (params.get(SaveNodeConfigurationDialog.SAVE_LIGHTPORTS) != null
            && ((Boolean) params.get(SaveNodeConfigurationDialog.SAVE_LIGHTPORTS)).booleanValue()) {
            for (LightPort lightPort : model.getLightPorts()) {
                try {
                    CommunicationFactory.getInstance().setLightPortParameters(node.getNode(), lightPort,
                        lightPort.getPwmMin(), lightPort.getPwmMax(), lightPort.getDimMin(), lightPort.getDimMax(),
                        lightPort.getRgbValue());
                }
                catch (Exception ex) {
                    LOGGER.warn("Save lightport configuration on node failed: {}", lightPort, ex);
                    addError(params, "Save lightport configuration on node failed: " + lightPort);
                }
            }
        }
        else {
            LOGGER.info("Don't save lightports.");
        }

        if (params.get(SaveNodeConfigurationDialog.SAVE_BACKLIGHTPORTS) != null
            && ((Boolean) params.get(SaveNodeConfigurationDialog.SAVE_BACKLIGHTPORTS)).booleanValue()) {
            for (BacklightPort backlightPort : model.getBacklightPorts()) {
                try {
                    CommunicationFactory.getInstance().setBacklightPortParameters(node.getNode(), backlightPort,
                        backlightPort.getDimSlopeDown(), backlightPort.getDimSlopeUp(), backlightPort.getDmxMapping());
                }
                catch (Exception ex) {
                    LOGGER.warn("Save backlightport configuration on node failed: {}", backlightPort, ex);
                    addError(params, "Save backlightport configuration on node failed: " + backlightPort);
                }
            }
        }
        else {
            LOGGER.info("Don't save backlightports.");
        }

        if (params.get(SaveNodeConfigurationDialog.SAVE_SERVOPORTS) != null
            && ((Boolean) params.get(SaveNodeConfigurationDialog.SAVE_SERVOPORTS)).booleanValue()) {
            LOGGER.info("Set the servo port configuration on the node.");
            for (ServoPort servoPort : model.getServoPorts()) {
                try {
                    CommunicationFactory.getInstance().setServoPortParameters(node.getNode(), servoPort,
                        servoPort.getTrimDown(), servoPort.getTrimUp(), servoPort.getSpeed());
                }
                catch (Exception ex) {
                    LOGGER.warn("Save servoport configuration on node failed: {}", servoPort, ex);
                    addError(params, "Save servoport configuration on node failed: " + servoPort);
                }
            }
        }
        else {
            LOGGER.info("Don't save servoports.");
        }

        boolean hasSwitchPortConfig = false;
        Feature switchPortConfigAvailable =
            Feature.findFeature(node.getNode().getFeatures(), BidibLibrary.FEATURE_SWITCH_CONFIG_AVAILABLE);
        if (switchPortConfigAvailable != null) {
            hasSwitchPortConfig = (switchPortConfigAvailable.getValue() > 0);
        }

        if (hasSwitchPortConfig) {
            if (params.get(SaveNodeConfigurationDialog.SAVE_SWITCHPORTS) != null
                && ((Boolean) params.get(SaveNodeConfigurationDialog.SAVE_SWITCHPORTS)).booleanValue()) {
                LOGGER.info("Set the switch port configuration on the node.");
                for (SwitchPort switchPort : model.getSwitchPorts()) {
                    try {
                        if (switchPort.isEnabled()) {
                            CommunicationFactory.getInstance().setSwitchPortParameters(node.getNode(), switchPort,
                                switchPort.getOutputBehaviour(), switchPort.getSwitchOffTime());
                        }
                        else {
                            LOGGER.info("The current switchport is not enabled: {}", switchPort);
                        }
                        // if (switchPort.getId() == 5) {
                        // throw new RuntimeException("Test ");
                        // }
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Save switchport configuration on node failed: {}", switchPort, ex);
                        addError(params, "Save switchport configuration on node failed: " + switchPort);
                    }
                }
            }
            else {
                LOGGER.info("Don't save switchports.");
            }
        }
        else {
            LOGGER.info("Node does not support switch port configuration.");
        }

        // save the node string username
        if (node.getNode().getStringSize() > 0) {
            LOGGER.info("Transfer the node strings.");

            String userName = node.getNode().getStoredString(StringData.INDEX_USERNAME);
            LOGGER.info("Transfer the node string username: {}", userName);

            try {
                CommunicationFactory.getInstance().setString(node.getNode(), StringData.NAMESPACE_NODE,
                    StringData.INDEX_USERNAME, userName);
            }
            catch (Exception ex) {
                LOGGER.warn("Save userName on node failed: {}", userName, ex);
                addError(params, "Save userName on node failed: " + userName);
            }
        }
        else {
            LOGGER.info("The node does not support store node string values.");
        }

        // save the macros on the node
        if (params.get(SaveNodeConfigurationDialog.SAVE_MACROS) != null
            && ((Boolean) params.get(SaveNodeConfigurationDialog.SAVE_MACROS)).booleanValue()) {
            LOGGER.info("Save macros on the node.");
            for (Macro macro : model.getMacros()) {
                try {
                    CommunicationFactory.getInstance().saveMacro(node, macro);
                }
                catch (Exception ex) {
                    LOGGER.warn("Save macro on node failed: {}", macro, ex);
                    addError(params, "Save macro on node failed: " + macro);
                }
            }
        }
        else {
            LOGGER.info("Don't save macros.");
        }

        // save the accessories on the node
        if (params.get(SaveNodeConfigurationDialog.SAVE_ACCESSORIES) != null
            && ((Boolean) params.get(SaveNodeConfigurationDialog.SAVE_ACCESSORIES)).booleanValue()) {
            LOGGER.info("Save accessories on the node.");
            for (Accessory accessory : model.getAccessories()) {
                try {
                    CommunicationFactory.getInstance().saveAccessory(node.getNode(), accessory);
                }
                catch (Exception ex) {
                    LOGGER.warn("Save accessory on node failed: {}", accessory, ex);
                    addError(params, "Save accessory on node failed: " + accessory);
                }
            }
        }
        else {
            LOGGER.info("Don't save accessories.");
        }
    }

    private void addError(Map<String, Object> params, String message) {
        List<String> saveErrors = (List<String>) params.get(SaveNodeConfigurationDialog.SAVE_ERRORS);
        if (saveErrors == null) {
            saveErrors = new LinkedList<String>();
            params.put(SaveNodeConfigurationDialog.SAVE_ERRORS, saveErrors);
        }
        saveErrors.add(message);
    }

    @Override
    public void dmxModeler(Node node) {
        setWaitCursor();
        LOGGER.info("Open the DMX modeler for node: {}", node);
        try {
            DmxModelerController dmxModelerController = new DmxModelerController(view, view.getDesktop(), node);

            dmxModelerController.start(model);
        }
        catch (Exception ex) {
            LOGGER.warn("Open DMX modeler failed.", ex);
            // show dialog for user
            JOptionPane.showMessageDialog(view, "Open DMX modeler failed.", "Open DMX modeler",
                JOptionPane.ERROR_MESSAGE);
        }
        finally {
            setDefaultCursor();
        }
    }

    @Override
    public void locoList(final Node node) {
        setWaitCursor();
        LOGGER.info("Open the loco table for node: {}", node);
        try {
            final MainControllerInterface mainController =
                DefaultApplicationContext.getInstance().get(DefaultApplicationContext.KEY_MAIN_CONTROLLER,
                    MainControllerInterface.class);

            LocoTableController locoTableController = new LocoTableController();
            locoTableController.start(view.getDesktop(), mainController);
            locoTableController.setSelectedNode(node);

        }
        catch (Exception ex) {
            LOGGER.warn("Open loco table failed.", ex);
            // show dialog for user
            JOptionPane.showMessageDialog(view, "Open loco table failed.", "Open loco table",
                JOptionPane.ERROR_MESSAGE);
        }
        finally {
            setDefaultCursor();
        }
    }

}
