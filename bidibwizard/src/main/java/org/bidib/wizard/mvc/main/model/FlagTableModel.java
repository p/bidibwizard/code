package org.bidib.wizard.mvc.main.model;

import java.util.Collection;
import java.util.LinkedList;

import javax.swing.table.DefaultTableModel;

import org.apache.commons.lang3.StringUtils;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.main.model.listener.FlagListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FlagTableModel extends DefaultTableModel {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(FlagTableModel.class);

    public static final int COLUMN_LABEL = 0;

    private static final String[] COLUMN_NAMES = new String[] { Resources.getString(FlagTableModel.class, "flag") };

    private final MainModel model;

    private final Collection<FlagListener> flagListeners = new LinkedList<>();

    public FlagTableModel(final MainModel model) {
        this.model = model;

        setColumnIdentifiers(COLUMN_NAMES);
    }

    public void addFlagListener(FlagListener l) {
        flagListeners.add(l);
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return true;
    }

    public void addRow(Flag flag) {
        if (flag != null) {
            Object[] rowData = new Object[COLUMN_NAMES.length];
            rowData[COLUMN_LABEL] = flag;
            LOGGER.info("Add new row: {}", rowData);
            addRow(rowData);
        }
    }

    @Override
    public void setValueAt(Object value, int row, int column) {
        switch (column) {
            case COLUMN_LABEL:

                if (value != null && StringUtils.isBlank(value.toString())) {
                    value = null;
                    LOGGER.info("Set the null value instead of empty string.");
                }

                Flag flag = (Flag) super.getValueAt(row, column);
                flag.setLabel(value != null ? value.toString() : null);
                super.setValueAt(flag, row, column);

                // keep the label in the FlagLabels
                fireFlagLabelChanged(row);
            default:
                break;
        }
    }

    private void fireFlagLabelChanged(int aspectId) {
        for (FlagListener l : flagListeners) {
            l.labelChanged(aspectId);
        }
    }

}
