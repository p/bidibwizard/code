package org.bidib.wizard.mvc.accessory.view.listener;

import org.bidib.jbidibc.core.AddressData;
import org.bidib.jbidibc.core.enumeration.TimeBaseUnitEnum;
import org.bidib.jbidibc.core.enumeration.TimingControlEnum;

public interface AccessoryViewListener {
    /**
     * Close the view.
     */
    void close();

    /**
     * Send the accessory request.
     * 
     * @param dccAddress
     *            the DCC address to switch
     * @param aspect
     *            the aspect to set
     * @param switchTime
     *            the switch time
     * @param timeBaseUnit
     *            the time base unit (100ms or 1s)
     * @param timingControl
     *            the timing control
     */
    void sendAccessoryRequest(
        AddressData dccAddress, Integer aspect, Integer switchTime, TimeBaseUnitEnum timeBaseUnit,
        TimingControlEnum timingControl);
}
