package org.bidib.wizard.mvc.main.view.menu.listener;

public interface MacroListMenuListener extends LabelListMenuListener {
    void exportMacro();

    void importMacro();

    void startMacro();

    void stopMacro();

    void initializeMacro();
}
