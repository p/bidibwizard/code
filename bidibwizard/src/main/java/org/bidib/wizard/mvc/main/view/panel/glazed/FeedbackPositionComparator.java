package org.bidib.wizard.mvc.main.view.panel.glazed;

import java.util.Comparator;

import org.bidib.wizard.mvc.main.model.FeedbackPosition;

public class FeedbackPositionComparator implements Comparator<FeedbackPosition> {

    @Override
    public int compare(FeedbackPosition pos1, FeedbackPosition pos2) {
        return pos1.getDecoderAddress() - pos2.getDecoderAddress();
    }

}
