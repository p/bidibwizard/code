package org.bidib.wizard.mvc.main.view.table;

import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.TableCellRenderer;

public class NumberCellRenderer implements TableCellRenderer {
    private final JPanel panel = new JPanel();

    private final JTextField textField = new JTextField(3);

    public NumberCellRenderer() {
        textField.setHorizontalAlignment(SwingConstants.RIGHT);
        panel.setLayout(new BorderLayout());
        panel.add(textField, BorderLayout.CENTER);
    }

    public Component getTableCellRendererComponent(
        JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        if (isSelected) {
            panel.setForeground(table.getSelectionForeground());
            panel.setBackground(table.getSelectionBackground());
        }
        else {
            panel.setForeground(table.getForeground());
            panel.setBackground(table.getBackground());
        }
        textField.setText(value != null ? value.toString() : "");
        return panel;
    }
}
