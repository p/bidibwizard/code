package org.bidib.wizard.mvc.ping.view;

import org.bidib.jbidibc.core.utils.NodeUtils;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.ping.model.NodePingModel;
import org.bidib.wizard.mvc.ping.model.NodePingState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.adapter.AbstractTableAdapter;
import com.jgoodies.binding.list.SelectionInList;

public class PingTableTableModel extends AbstractTableAdapter<NodePingModel> {
    private static final Logger LOGGER = LoggerFactory.getLogger(PingTableTableModel.class);

    private static final long serialVersionUID = 1L;

    public static final int COLUMN_UNIQUE_ID = 0;

    public static final int COLUMN_DESCRIPTION = 1;

    public static final int COLUMN_STATUS = 2;

    public static final int COLUMN_PING_INTERVAL = 3;

    public static final int COLUMN_VOLTAGE = 4;

    public static final int COLUMN_MAX_CURRENT = 5;

    public static final int COLUMN_CURRENT = 6;

    public static final int COLUMN_PORT_INSTANCE = 7;

    private static final String[] COLUMNNAMES = new String[] {
        Resources.getString(PingTableTableModel.class, "uniqueId"),
        Resources.getString(PingTableTableModel.class, "description"),
        Resources.getString(PingTableTableModel.class, "status"),
        Resources.getString(PingTableTableModel.class, "interval") };

    public PingTableTableModel(SelectionInList<NodePingModel> nodeList) {
        super(nodeList, COLUMNNAMES);

        LOGGER.info("Current listModel: {}", getListModel());
    }

    @Override
    public Class<?> getColumnClass(int column) {
        switch (column) {
            case COLUMN_UNIQUE_ID:
                return String.class;
            case COLUMN_STATUS:
                // return NodePingState.class;
            default:
                return Object.class;
        }
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        switch (column) {
            case COLUMN_UNIQUE_ID:
                break;
            case COLUMN_STATUS:
                return true;
            default:
                break;
        }
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        NodePingModel nodePingModel = (NodePingModel) getRow(rowIndex);
        switch (columnIndex) {
            case COLUMN_UNIQUE_ID:
                return NodeUtils.getUniqueIdAsString(nodePingModel.getNode().getUniqueId());
            case COLUMN_DESCRIPTION:
                return nodePingModel.getNodeLabel();
            case COLUMN_STATUS:
                return (nodePingModel.getNodePingState() == NodePingState.OFF ? false : true);
            case COLUMN_PING_INTERVAL:
                return nodePingModel.getPingInterval();
            default:
                return null;
        }
    }

    @Override
    public void setValueAt(Object value, int rowIndex, int columnIndex) {
        LOGGER.info("Set the value: {}, rowIndex: {}", value, rowIndex);

        NodePingModel nodePingModel = (NodePingModel) getRow(rowIndex);
        switch (columnIndex) {
            case COLUMN_STATUS:
                if (value instanceof Boolean) {
                    Boolean val = (Boolean) value;
                    nodePingModel.setNodePingState(val.booleanValue() ? NodePingState.ON : NodePingState.OFF);
                }
                break;
            default:
                break;
        }
    }
}
