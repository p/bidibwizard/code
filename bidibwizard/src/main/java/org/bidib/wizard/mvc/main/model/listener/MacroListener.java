package org.bidib.wizard.mvc.main.model.listener;

import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.mvc.main.model.function.Function;

public interface MacroListener {
    /**
     * Functions were added.
     * 
     * @param row
     *            the row index
     * @param functions
     *            the functions
     */
    void functionsAdded(int row, Function<? extends BidibStatus>[] functions);

    /**
     * Function was removed.
     * 
     * @param row
     *            the removed row index
     */
    void functionRemoved(int row);

    /**
     * Functions were moved.
     * 
     * @param fromIndex
     *            the from index
     * @param toIndex
     *            the to index
     * @param fromFunction
     *            the from function
     */
    void functionMoved(int fromIndex, int toIndex, Function<? extends BidibStatus> fromFunction);

    /**
     * Functions were removed.
     */
    void functionsRemoved();

    /**
     * The label of the macro was changed.
     * 
     * @param label
     *            the new label
     */
    void labelChanged(String label);

    /**
     * The start condition of the macro has been changed.
     */
    void startConditionChanged();

    /**
     * The slowdown factor has changed.
     */
    void slowdownFactorChanged();

    /**
     * The cycles have changed.
     */
    void cyclesChanged();
}
