package org.bidib.wizard.mvc.pom.view.panel;

public interface LogAreaAware {

    /**
     * Clears the log area.
     */
    void clearLogArea();
}
