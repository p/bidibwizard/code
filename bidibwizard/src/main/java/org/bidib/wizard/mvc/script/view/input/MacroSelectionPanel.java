package org.bidib.wizard.mvc.script.view.input;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JComboBox;

import org.apache.commons.lang3.StringUtils;
import org.bidib.wizard.common.context.ApplicationContext;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.common.view.renderer.MacroRenderer;
import org.bidib.wizard.mvc.main.model.Macro;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.script.view.ScriptParser;
import org.bidib.wizard.utils.MacroListUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.adapter.ComboBoxAdapter;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.view.ValidationComponentUtils;

public class MacroSelectionPanel extends AbstractInputSelectionPanel {
    private static final Logger LOGGER = LoggerFactory.getLogger(MacroSelectionPanel.class);

    private ValueModel selectionHolderMacro;

    private ItemSelectionModel<Macro> macroSelectionModel;

    private SelectionInList<Macro> macroSelection;

    @Override
    public void initialize(
        final ApplicationContext context, final DefaultFormBuilder dialogBuilder, ValidationSupport validationSupport,
        String variableName, String variableType, String caption, String defaultValue, String prevValue) {
        this.variableName = variableName;
        setDefaultValue(defaultValue);

        macroSelectionModel = new ItemSelectionModel<>();
        macroSelectionModel.addPropertyChangeListener(ItemSelectionModel.PROPERTY_SELECTED_ITEM,
            new PropertyChangeListener() {

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    // TODO Auto-generated method stub
                    LOGGER.debug("Selected item has changed: {}", macroSelectionModel.getSelectedItem());
                    validationSupport.triggerValidation();
                }
            });

        MainModel mainModel = context.get(ScriptParser.KEY_MAIN_MODEL, MainModel.class);

        List<Macro> macros = new LinkedList<>(mainModel.getMacros());
        macros.add(0, Macro.NONE);

        macroSelection = new SelectionInList<Macro>(macros);

        selectionHolderMacro =
            new PropertyAdapter<ItemSelectionModel>(macroSelectionModel, ItemSelectionModel.PROPERTY_SELECTED_ITEM,
                true);

        ComboBoxAdapter<Macro> comboBoxAdapterMacros = new ComboBoxAdapter<Macro>(macroSelection, selectionHolderMacro);
        JComboBox<Macro> comboMacros = new JComboBox<>();
        comboMacros.setModel(comboBoxAdapterMacros);
        comboMacros.setRenderer(new MacroRenderer());

        caption = "<html>" + caption + "</html>";

        dialogBuilder.append(caption, comboMacros);

        ValidationComponentUtils.setMandatory(comboMacros, true);
        ValidationComponentUtils.setMessageKeys(comboMacros, "validation." + variableName);

        String initialValue = defaultValue;
        if (StringUtils.isNotBlank(prevValue)) {
            initialValue = prevValue;
        }

        if (StringUtils.isNotBlank(initialValue)) {
            try {
                Integer def = Integer.valueOf(initialValue);
                LOGGER.info("Set the initial value: {}", def);

                Macro macro = MacroListUtils.findMacroByMacroNumber(macroSelection.getList(), def.intValue());

                macroSelectionModel.setSelectedItem(macro);
            }
            catch (Exception ex) {
                LOGGER.warn("Parse initial value to macro number failed: {}", initialValue, ex);
            }
        }
        else {
            macroSelectionModel.setSelectedItem(Macro.NONE);
        }

        content = dialogBuilder.build();
    }

    public void validate(final ValidationResult validationResult) {

        if (selectionHolderMacro.getValue() == null || Macro.NONE.equals(selectionHolderMacro.getValue())) {
            validationResult.addError(Resources.getString((Class<?>) null, "validation.select_macro"),
                "validation." + variableName);
        }
    }

    @Override
    public Object getSelectedValue() {
        // TODO Auto-generated method stub
        if (selectionHolderMacro.getValue() != null) {
            return Integer.valueOf(((Macro) selectionHolderMacro.getValue()).getId());
        }
        return null;
    }
}
