package org.bidib.wizard.mvc.main.view.component;

import javax.swing.JPanel;

import org.bidib.wizard.mvc.main.view.panel.listener.TabSelectionListener;

public abstract class DefaultTabSelectionPanel extends JPanel implements TabSelectionListener {
    private static final long serialVersionUID = 1L;
}
