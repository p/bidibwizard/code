package org.bidib.wizard.mvc.script.view.input;

import java.awt.BorderLayout;
import java.awt.Window;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.velocity.context.Context;
import org.bidib.wizard.common.context.ApplicationContext;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.common.view.validation.IconFeedbackPanel;
import org.bidib.wizard.mvc.common.view.validation.ValidStateResultModel;
import org.oxbow.swingbits.dialog.task.TaskDialog;
import org.oxbow.swingbits.dialog.task.TaskDialog.Command;
import org.oxbow.swingbits.dialog.task.TaskDialog.StandardCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.debug.FormDebugPanel;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.validation.ValidationResult;

public class InputParametersDialog implements ValidationSupport {

    private static final Logger LOGGER = LoggerFactory.getLogger(InputParametersDialog.class);

    private static final String ENCODED_INPUT_ARGS_COLUMN_SPECS = "pref, 3dlu, fill:130dlu:grow";

    protected static final String REGEX_INPUT =
        "text\\:[a-z]{2}=\"([^\"]*)\"|default=(\"([^\"]*)\"|([0-9a-zA-Z]*))|(\\S+)";

    final ValidStateResultModel validationResultModel = new ValidStateResultModel();

    public InputParametersDialog() {

    }

    /**
     * Show a dialog to query the input parameters.
     * 
     * @param context
     *            the application context
     * @param velocityContext
     *            the velocity context
     * @param inputLines
     *            the lines with {@code ##input}
     * @param instructionLines
     *            the lines with {@code ##instruction}
     * @param defineLines
     *            the lines with {@code ##define}
     */
    public void queryInputParameters(
        final ApplicationContext context, final Context velocityContext, final List<String> inputLines,
        final List<String> instructionLines, final List<String> defineLines) {

        Locale locale = Locale.getDefault();
        String lang = locale.getLanguage();
        LOGGER.info("Current locale: {}, current lang: {}", locale, lang);

        final Map<String, AbstractInputSelectionPanel> inputPanelMap = new HashMap<>();

        JPanel dialogContent =
            prepareContentPanel(context, velocityContext, inputLines, instructionLines, defineLines, lang,
                inputPanelMap);

        String instruction = Resources.getString(getClass(), "input-args.instruction");
        String text = Resources.getString(getClass(), "input-args.text");

        if (CollectionUtils.isNotEmpty(instructionLines)) {

            // ##instruction(text:de="Konfiguration eines Lichtsignal der DB", text:en="Configuration of a licht signal
            // of DB")
            try {
                String line = instructionLines.get(0);
                // parse the line
                String caption = line.substring(line.indexOf("(") + 1, line.lastIndexOf(")"));

                final String regex = "text\\:[a-z]{2}=\"([^\"]*)\"|(\\S+)";

                Matcher m = Pattern.compile(regex).matcher(caption);
                while (m.find()) {
                    if (m.group(1) != null) {
                        LOGGER.info("Found text: [" + m.group(0) + "]");
                        String identifier = m.group(0);
                        String currentLang = identifier.trim().substring(5, 7);
                        if (lang.equalsIgnoreCase(currentLang)) {
                            instruction = identifier.trim().substring(8);
                            instruction = StringUtils.strip(instruction, "\"");
                            LOGGER.info("Found instruction: {}", instruction);

                            break;
                        }
                    }
                }
            }
            catch (Exception ex) {
                LOGGER.warn("Get instruction text failed.", ex);
            }
        }

        // prepare the dialog
        TaskDialog dlg =
            questionDialog(JOptionPane.getFrameForComponent(null), Resources.getString(getClass(), "input-args.title"),
                null, instruction, text);

        dlg.setIcon(TaskDialog.StandardIcon.QUESTION);
        dlg.setFixedComponent(dialogContent);

        // if the model is valid, the write button is enabled.
        Command okCommand = dlg.getCommands().toArray(new Command[0])[0];

        PropertyConnector.connect(validationResultModel, ValidStateResultModel.PROPERTY_VALID_STATE, okCommand,
            "enabled");

        boolean result = dlg.show().equals(StandardCommand.OK);
        if (!result) {
            LOGGER.warn("The processing of input parameters was not not successful.");
            throw new CancelledByUserException("User canceled processing.");
        }

        Map<String, Object> scriptUserSelectionValues = context.get("ScriptUserSelection", Map.class);

        if (scriptUserSelectionValues == null) {
            LOGGER.info(
                "Create a map to store the user selection input params in the application context to reuse them later.");

            scriptUserSelectionValues = new HashMap<>();

            context.register("ScriptUserSelection", scriptUserSelectionValues);
        }

        // clear the cache
        scriptUserSelectionValues.clear();

        LOGGER.info("Add the user input values to velocity context.");
        for (Entry<String, AbstractInputSelectionPanel> entry : inputPanelMap.entrySet()) {
            String variableName = entry.getKey();
            Object value = entry.getValue().getSelectedValue();
            String defaultValue = entry.getValue().getDefaultValue();
            LOGGER.info("Add to velocity context, variable name: {}, value: {}, defaultValue: {}", variableName, value,
                defaultValue);

            if (value != null) {
                velocityContext.put(variableName, value);

                // if the selected value does not match the default value we must keep the selected value
                if (!value.toString().equals(defaultValue)) {
                    LOGGER.info("Keep selected value as user selected value: {}", value);
                    scriptUserSelectionValues.put(variableName, value);
                }
                else {
                    LOGGER.info("The selected value equals the default value and is not stored for later usage.");
                }
            }
        }
    }

    public JPanel prepareContentPanel(
        final ApplicationContext context, final Context velocityContext, final List<String> inputLines,
        final List<String> instructionLines, final List<String> defineLines, String lang,
        final Map<String, AbstractInputSelectionPanel> inputPanelMap) {

        JPanel dialogContent = null;

        DefaultFormBuilder dialogBuilder = null;
        boolean debugDialog = false;
        if (debugDialog) {
            JPanel panel = new FormDebugPanel();
            dialogBuilder = new DefaultFormBuilder(new FormLayout(ENCODED_INPUT_ARGS_COLUMN_SPECS), panel);
        }
        else {
            JPanel panel = new JPanel(new BorderLayout());
            dialogBuilder = new DefaultFormBuilder(new FormLayout(ENCODED_INPUT_ARGS_COLUMN_SPECS), panel);
        }

        Map<String, Object> scriptUserSelectionValues = context.get("ScriptUserSelection", Map.class);

        // scan the script for input parameters
        // ##input($my_accessory, text:de="Eingabe Nummer des zu erzeugenden Accessory", default=3)
        // ##input($my_accessory:accessory, text:de="Auswahl des zu erzeugenden Accessory", default=3)

        // final Map<String, AbstractInputSelectionPanel> inputPanelMap = new HashMap<>();

        for (String line : inputLines) {

            // parse the line
            InputLineParams params = scanInputLine(lang, line, scriptUserSelectionValues);

            // prepare the panel for the dialog
            if (StringUtils.isBlank(params.variableType)) {
                params.variableType = "string";
            }
            if (StringUtils.isNotBlank(params.variableType)) {
                // prepare the panel based on the type of the variable
                AbstractInputSelectionPanel inputArgPanel = null;
                switch (params.variableType) {
                    case "accessory":
                        inputArgPanel = new AccessorySelectionPanel();
                        inputArgPanel.initialize(context, dialogBuilder, this, params.variableName, params.variableType,
                            params.caption, params.defaultValue, params.prevValue);
                        addValidationComponent(inputArgPanel);
                        break;
                    case "macro":
                        inputArgPanel = new MacroSelectionPanel();
                        inputArgPanel.initialize(context, dialogBuilder, this, params.variableName, params.variableType,
                            params.caption, params.defaultValue, params.prevValue);
                        addValidationComponent(inputArgPanel);
                        break;
                    case "backlight":
                    case "input":
                    case "light":
                    case "motor":
                    case "servo":
                    case "sound":
                    case "switch":
                    case "switchpair":
                        inputArgPanel = new PortSelectionPanel();
                        inputArgPanel.initialize(context, dialogBuilder, this, params.variableName, params.variableType,
                            params.caption, params.defaultValue, params.prevValue);
                        addValidationComponent(inputArgPanel);
                        break;
                    case "boolean":
                        inputArgPanel = new BooleanInputPanel();
                        inputArgPanel.initialize(context, dialogBuilder, this, params.variableName, params.variableType,
                            params.caption, params.defaultValue, params.prevValue);
                        addValidationComponent(inputArgPanel);
                        break;
                    case "int":
                        inputArgPanel = new IntegerInputPanel();
                        inputArgPanel.initialize(context, dialogBuilder, this, params.variableName, params.variableType,
                            params.caption, params.defaultValue, params.prevValue);
                        addValidationComponent(inputArgPanel);
                        break;
                    default:
                        inputArgPanel = new StringInputPanel();
                        inputArgPanel.initialize(context, dialogBuilder, this, params.variableName, params.variableType,
                            params.caption, params.defaultValue, params.prevValue);
                        addValidationComponent(inputArgPanel);
                        break;
                }
                inputPanelMap.put(params.variableName, inputArgPanel);
            }
        }

        // add empty label to have the validation marker displayed correct
        dialogBuilder.append(new JLabel(""));

        dialogContent = dialogBuilder.build();

        // check if we have validation enabled

        LOGGER.info("Create iconfeedback panel.");
        JComponent cvIconPanel = new IconFeedbackPanel(validationResultModel, dialogContent);
        DefaultFormBuilder feedbackBuilder = null;
        feedbackBuilder = new DefaultFormBuilder(new FormLayout("p:g"));

        feedbackBuilder.appendRow("fill:p:grow");
        feedbackBuilder.add(cvIconPanel);

        dialogContent = feedbackBuilder.build();
        triggerValidation();

        return dialogContent;
    }

    protected ValidationResult validate() {
        ValidationResult validationResult = new ValidationResult();

        for (AbstractInputSelectionPanel panel : validationComponents) {

            panel.validate(validationResult);
        }

        return validationResult;
    }

    @Override
    public void triggerValidation() {

        ValidationResult validationResult = validate();
        validationResultModel.setResult(validationResult);
    }

    private List<AbstractInputSelectionPanel> validationComponents = new LinkedList<>();

    @Override
    public void addValidationComponent(AbstractInputSelectionPanel panel) {

        validationComponents.add(panel);
    }

    @Override
    public void removeValidationComponent(AbstractInputSelectionPanel panel) {

        validationComponents.remove(panel);
    }

    private static TaskDialog questionDialog(Window parent, String title, Icon icon, String instruction, String text) {

        TaskDialog dlg = new TaskDialog(parent, title);
        dlg.setInstruction(instruction);
        dlg.setText(text);
        dlg.setIcon(icon);
        dlg.setCommands(StandardCommand.OK.derive(TaskDialog.makeKey("Ok")),
            StandardCommand.CANCEL.derive(TaskDialog.makeKey("Cancel")));
        return dlg;
    }

    protected static final class InputLineParams {

        String variableName;

        String variableType;

        String caption;

        String defaultValue;

        String prevValue;
    }

    protected InputLineParams scanInputLine(
        String lang, String line, final Map<String, Object> scriptUserSelectionValues) {

        InputLineParams inputLineParams = new InputLineParams();

        // parse the line
        String caption = line.substring(line.indexOf("(") + 1, line.lastIndexOf(")"));

        Matcher m = Pattern.compile(InputParametersDialog.REGEX_INPUT).matcher(caption);
        while (m.find()) {
            if (m.group(1) != null) {
                String text = m.group(0);
                LOGGER.info("1. Found text: [{}]", text);
                if (text.startsWith("text:" + lang)) {
                    inputLineParams.caption = m.group(1);
                }
            }
            else if (m.group(2) != null) {
                String text = m.group(0);
                LOGGER.info("2. Found text: [{}]", text);
                if (text.startsWith("default=")) {
                    // strip leading and trailing quotation marks
                    inputLineParams.defaultValue = StringUtils.strip(m.group(2), "\"");
                }
            }
            else {
                String text = m.group(0);
                LOGGER.debug("Plain [{}]", text);
                text = StringUtils.strip(text, ",").trim();

                if (StringUtils.isNotBlank(text)) {
                    LOGGER.info("3. Found text: [{}]", text);
                    if (StringUtils.isBlank(inputLineParams.variableName)) {
                        if (text.indexOf(":") > -1) {
                            // type provided
                            inputLineParams.variableName = text.split(":")[0];
                            inputLineParams.variableType = text.split(":")[1];
                        }
                        else {
                            inputLineParams.variableName = text.trim();
                            inputLineParams.variableType = "string";
                        }

                        inputLineParams.variableName = StringUtils.strip(inputLineParams.variableName, "$");

                        LOGGER.info("Found variable with name: {}, type: {}", inputLineParams.variableName,
                            inputLineParams.variableType);
                    }
                }
            }
        }

        if (scriptUserSelectionValues != null) {

            // check if an previous value is available and override the default value
            Object preDefinedValue = scriptUserSelectionValues.get(inputLineParams.variableName);
            if (preDefinedValue != null) {
                inputLineParams.prevValue = preDefinedValue.toString();
                LOGGER.info("Use predefined value from previous selection: {}", inputLineParams.prevValue);
            }
        }

        LOGGER.info("Prepared inputLineParams: {}",
            ToStringBuilder.reflectionToString(inputLineParams, ToStringStyle.SHORT_PREFIX_STYLE));
        return inputLineParams;
    }
}
