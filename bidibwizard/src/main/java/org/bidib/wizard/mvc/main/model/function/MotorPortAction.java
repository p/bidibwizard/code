package org.bidib.wizard.mvc.main.model.function;

import org.bidib.jbidibc.exchange.lcmacro.LcMacroPointType;
import org.bidib.jbidibc.exchange.lcmacro.MotorActionType;
import org.bidib.jbidibc.exchange.lcmacro.MotorPortActionType;
import org.bidib.jbidibc.exchange.lcmacro.MotorPortPoint;
import org.bidib.wizard.comm.MotorPortStatus;
import org.bidib.wizard.mvc.main.model.MotorPort;

public class MotorPortAction extends SimplePortAction<MotorPortStatus, MotorPort> {
    public MotorPortAction() {
        this(MotorPortStatus.FORWARD);
    }

    public MotorPortAction(MotorPortStatus action) {
        this(MotorPortStatus.FORWARD, null, 0, 0);
    }

    public MotorPortAction(MotorPortStatus action, MotorPort port, int delay, int value) {
        super(action, KEY_MOTOR, port, delay, value);
    }

    public String getDebugString() {
        int id = 0;

        if (getPort() != null) {
            id = getPort().getId();
        }
        return "@" + getDelay() + " Motor:" + String.format("%02d", id) + "->" + getValue();
    }

    @Override
    public LcMacroPointType toLcMacroPoint() {
        MotorPortPoint motorPortPoint = new MotorPortPoint();
        motorPortPoint.setDelay(getDelay());
        motorPortPoint.setOutputNumber(getPort().getId());
        MotorPortActionType motorPortActionType = new MotorPortActionType();
        motorPortActionType.setAction(MotorActionType.fromValue(getAction().name()));
        motorPortActionType.setValue(getValue());
        motorPortPoint.setMotorPortActionType(motorPortActionType);
        return motorPortPoint;
    }
}
