package org.bidib.wizard.mvc.features.model;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.bidib.jbidibc.core.Feature;
import org.bidib.wizard.mvc.features.model.listener.FeatureListener;
import org.bidib.wizard.mvc.main.model.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FeaturesModel {
    private static final Logger LOGGER = LoggerFactory.getLogger(FeaturesModel.class);

    private final Collection<FeatureListener> listeners = new LinkedHashSet<FeatureListener>();

    private Map<Integer, Feature> features = new LinkedHashMap<Integer, Feature>();

    private final Node node;

    public FeaturesModel(final Node node) {
        this.node = node;
    }

    public Node getNode() {
        return node;
    }

    public void addFeatureListener(FeatureListener listener) {
        listeners.add(listener);
    }

    public void removeFeatureListener(FeatureListener listener) {
        listeners.remove(listener);
    }

    public List<Feature> getFeatures() {
        return Collections.unmodifiableList(new LinkedList<Feature>(features.values()));
    }

    /**
     * Set the new feature values and notify the listener if a value has changed.
     * 
     * @param features
     *            the feature values
     */
    public void setFeatures(Collection<Feature> features) {
        LOGGER.debug("Set the new features: {}", features);
        for (Feature newFeature : features) {
            // check if the feature is in the features map
            Feature feature = this.features.get(newFeature.getType());
            boolean fireUpdate = false;
            if (feature != null) {
                // feature already in features map, check if value changed
                if (feature.getValue() != newFeature.getValue()) {
                    fireUpdate = true;
                    feature.setValue(newFeature.getValue());
                }
            }
            else {
                fireUpdate = true;
                feature = newFeature;
                this.features.put(feature.getType(), feature);
            }

            if (fireUpdate) {
                fireFeatureChanged(feature);
            }
        }
    }

    /**
     * Prepare a collection of the changed features that can be written to the node.
     * 
     * @param features
     *            all features
     * @return the changed features that must be written to the node
     */
    public Collection<Feature> prepareUpdateFeatures(final Collection<Feature> features) {
        LOGGER.debug("Write the new feature values: {}", features);
        Collection<Feature> featuresToUpdate = new LinkedList<Feature>();
        for (Feature newFeature : features) {
            // check if the feature is in the features map
            Feature feature = this.features.get(newFeature.getType());
            boolean fireValueUpdate = false;
            if (feature != null) {
                // feature already in features map, check if value changed
                if (feature.getValue() != newFeature.getValue()) {
                    fireValueUpdate = true;
                }
            }
            else {
                fireValueUpdate = true;
            }

            if (fireValueUpdate) {
                // TODO not sure if this is correct ... must be update from returned data
                // this.features.put(newFeature.getType(), newFeature);
                LOGGER.info("Adding feature to update list: {}", newFeature);
                featuresToUpdate.add(newFeature);
            }
        }

        return featuresToUpdate;
    }

    private void fireFeatureChanged(Feature feature) {
        LOGGER.debug("The feature has changed: {}", feature);
        for (FeatureListener listener : listeners) {
            listener.featureChanged(feature);
        }
    }
}
