package org.bidib.wizard.mvc.main.view.table;

import java.awt.BorderLayout;
import java.awt.Component;
import java.text.DecimalFormat;

import javax.swing.AbstractCellEditor;
import javax.swing.JFormattedTextField;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.TableCellEditor;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NumberCellEditor extends AbstractCellEditor implements TableCellEditor {
    private static final Logger LOGGER = LoggerFactory.getLogger(NumberCellEditor.class);

    private static final long serialVersionUID = 1L;

    private final JPanel panel = new JPanel();

    private final JFormattedTextField textField = new JFormattedTextField(3);

    private final NumberFormatter formatter = new NumberFormatter(
        new DecimalFormat("#")) {
        private static final long serialVersionUID = 1L;

        {
            // setAllowsInvalid(false);
            setMaximum(255);
            setMinimum(0);
        }
    };

    public NumberCellEditor() {
        this(255);
    }

    public NumberCellEditor(int maximum) {
        formatter.setMaximum(maximum);
        textField.setFormatterFactory(new DefaultFormatterFactory(formatter, formatter, formatter));
        textField.setHorizontalAlignment(SwingConstants.RIGHT);
        panel.setFocusCycleRoot(true);
        panel.setLayout(new BorderLayout());
        panel.add(textField, BorderLayout.CENTER);
    }

    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        if (isSelected) {
            panel.setForeground(table.getSelectionForeground());
            panel.setBackground(table.getSelectionBackground());
        }
        else {
            panel.setForeground(table.getForeground());
            panel.setBackground(table.getBackground());
        }
        textField.setText(value != null ? value.toString() : "");
        return panel;
    }

    @Override
    public Object getCellEditorValue() {
        try {
            if (StringUtils.isNotBlank(textField.getText())) {
                return Integer.parseInt(textField.getText());
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Parse textfield value failed.", ex);
        }
        LOGGER.debug("Allow empty string in text field. Return minimum value: {}", formatter.getMinimum());
        Comparable<?> minValue = formatter.getMinimum();
        if (minValue instanceof Number) {
            Number numMinValue = (Number) minValue;

            return Integer.valueOf(numMinValue.intValue());
        }
        return Integer.valueOf(0);
    }

    @Override
    public boolean stopCellEditing() {
        try {
            if (StringUtils.isNotBlank(textField.getText())) {
                int value = Integer.valueOf(textField.getText());
                if (value < 0) {
                    LOGGER.warn("Invalid value detected: {}", value);
                    throw new NumberFormatException("Invalid value detected: " + value);
                }
            }
            else {
                LOGGER.info("Allow empty string in text field.");
            }
        }
        catch (NumberFormatException ex) {
            return false;
        }
        return super.stopCellEditing();
    }
}
