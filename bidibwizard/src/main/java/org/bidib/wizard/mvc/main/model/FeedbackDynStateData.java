package org.bidib.wizard.mvc.main.model;

public class FeedbackDynStateData implements Comparable<FeedbackDynStateData> {
    private final int address;

    private int dynNumber;

    private int dynValue;

    public FeedbackDynStateData(int address, int dynNumber, int dynValue) {
        this.address = address;
        this.dynNumber = dynNumber;
        this.dynValue = dynValue;
    }

    public int getAddress() {
        return address;
    }

    public int getDynNumber() {
        return dynNumber;
    }

    public int getDynValue() {
        return dynValue;
    }

    /**
     * Append the dyn state value to the string buffer
     * 
     * @param sb
     *            the string buffer instance
     */
    public void appendDynState(StringBuffer sb) {
        sb.append("[").append(dynNumber).append("=").append(dynValue).append("]");
    }

    public String toString() {
        StringBuffer result = new StringBuffer();

        result.append("[").append(address);
        result.append(":").append(dynNumber).append("=").append(dynValue).append("]");
        return result.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + address;
        result = prime * result + dynNumber;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof FeedbackDynStateData)) {
            return false;
        }
        FeedbackDynStateData other = (FeedbackDynStateData) obj;
        if (address != other.address) {
            return false;
        }
        if (dynNumber != other.dynNumber) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(FeedbackDynStateData other) {
        return 0;
    }

}
