package org.bidib.wizard.mvc.script.controller.exception;

public class UserDefinedVelocityException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public UserDefinedVelocityException(String message) {
        super(message);
    }

}
