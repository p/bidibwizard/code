package org.bidib.wizard.mvc.main.model.listener;

public interface PortTableLayoutListener {

    /**
     * The port table layout has been changed.
     */
    void portTableLayoutChanged();

}
