package org.bidib.wizard.mvc.script.controller;

public interface ScriptContextKeys {

    static final String KEY_VELOCITY_CONTEXT = "velocityContext";

    static final String KEY_INPUT_LINES = "inputLines";

    static final String KEY_DEFINE_LINES = "defineLines";

    static final String KEY_INSTRUCTION_LINES = "instructionLines";

    static final String KEY_APPLICATION_LINES = "applicationLines";

    static final String KEY_DEFINE_LANG = "language";

    static final String KEY_SCRIPT_VERSION = "scriptVersion";

    static final String KEY_SCRIPT_PANEL = "scriptPanel";
}
