package org.bidib.wizard.mvc.main.model.function;

import org.bidib.jbidibc.exchange.lcmacro.LcMacroPointType;
import org.bidib.jbidibc.exchange.lcmacro.SwitchPortActionType;
import org.bidib.jbidibc.exchange.lcmacro.SwitchPortPoint;
import org.bidib.wizard.comm.SwitchPortStatus;
import org.bidib.wizard.mvc.main.model.SwitchPort;

public class SwitchPortAction extends PortAction<SwitchPortStatus, SwitchPort> {
    public SwitchPortAction() {
        this(SwitchPortStatus.ON);
    }

    public SwitchPortAction(SwitchPortStatus action) {
        this(action, null, 0);
    }

    public SwitchPortAction(SwitchPort port, int delay) {
        this(SwitchPortStatus.ON, port, delay);
    }

    public SwitchPortAction(SwitchPortStatus action, SwitchPort port, int delay) {
        super(action, KEY_SWITCH, port, delay);
    }

    public String getDebugString() {
        int id = 0;

        if (getPort() != null) {
            id = getPort().getId();
        }
        return "@" + getDelay() + " S-Port:" + String.format("%02d", id) + "->" + getAction().name().toLowerCase();
    }

    @Override
    public LcMacroPointType toLcMacroPoint() {
        SwitchPortPoint switchPortPoint = new SwitchPortPoint();
        switchPortPoint.setDelay(getDelay());
        switchPortPoint.setOutputNumber(getPort().getId());
        switchPortPoint.setSwitchPortActionType(SwitchPortActionType.fromValue(getAction().name()));
        return switchPortPoint;
    }
}
