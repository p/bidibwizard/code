package org.bidib.wizard.mvc.pomupdate.model;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.swing.SwingUtilities;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.collections4.Predicate;
import org.bidib.jbidibc.core.AddressData;
import org.bidib.jbidibc.core.schema.DecoderVendorFactory;
import org.bidib.jbidibc.core.schema.decodervendor.VendorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.beans.Model;
import com.jgoodies.common.collect.ArrayListModel;

public class PomUpdateModel extends Model {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(PomUpdateModel.class);

    public static final String PROPERTY_DECODERS = "decoders";

    public static final String PROPERTY_DECODER_FILE = "decoderFile";

    public static final String PROPERTY_HAS_FIRMWARE_AVAILABLE = "hasFirmwareAvailable";

    public static final String PROPERTY_FIRMWARE_CONTENT = "firmwareContent";

    public static final String PROPERTY_READY_FOR_PREPARE_UPDATE = "readyForPrepareUpdate";

    public static final String PROPERTY_READY_FOR_PERFORM_UPDATE = "readyForPerformUpdate";

    public static final String PROPERTY_UPDATE_IN_PROGRESS = "updateInProgress";

    private List<VendorType> vendors;

    private ArrayListModel<Decoder> decoderList = new ArrayListModel<>();

    private String decoderFile;

    private boolean hasFirmwareAvailable;

    private boolean readyForPrepareUpdate;

    private boolean readyForPerformUpdate;

    private boolean updateInProgress;

    private List<String> firmwareContent = new ArrayList<>();

    public PomUpdateModel() {

        vendors = DecoderVendorFactory.getDecoderVendors();
    }

    /**
     * @return the decoder addresses list
     */
    public ArrayListModel<Decoder> getDecoderListModel() {
        return decoderList;
    }

    public void addDecoder(Decoder decoder) {
        synchronized (decoderList) {
            if (!decoderList.contains(decoder)) {
                LOGGER.info("Add decoder to decoder list: {}", decoder);
                List<Decoder> oldValue = new LinkedList<>(decoderList);

                decoderList.add(decoder);

                firePropertyChange(PROPERTY_DECODERS, oldValue, decoderList);
            }
            else {
                LOGGER.warn("Decoder is already in decoder list: {}", decoder);
            }

            if (!decoderList.isEmpty() && isHasFirmwareAvailable()) {
                setReadyForPrepareUpdate(true);
                setReadyForPerformUpdate(false);
            }
            else {
                setReadyForPrepareUpdate(false);
                setReadyForPerformUpdate(false);
            }
        }
    }

    public void removeDecoder(Decoder decoder) {
        synchronized (decoderList) {
            LOGGER.info("Remove decoder from decoder list: {}", decoder);

            List<Decoder> oldValue = new LinkedList<>(decoderList);
            boolean removed = decoderList.remove(decoder);

            if (removed) {
                firePropertyChange(PROPERTY_DECODERS, oldValue, decoderList);
            }
            else {
                LOGGER.warn("Decoder was not removed from decoder list: {}", decoder);
            }

            if (!decoderList.isEmpty() && isHasFirmwareAvailable()) {
                setReadyForPrepareUpdate(true);
            }
            else {
                setReadyForPrepareUpdate(false);
            }
        }
    }

    public void updatePomFeedback(AddressData decoderAddress, int cvNumber, int cvData) {
        synchronized (decoderList) {

            for (Decoder decoder : decoderList) {
                if (decoder.getAddress() == decoderAddress.getAddress()) {
                    LOGGER.info("Found decoder to update: {}", decoder);
                    switch (cvNumber) {
                        case 7: // decoder version
                            decoder.setDecoderVersionAndModel(Integer.toString(cvData));
                            break;
                        case 8: // manufacturer

                            String vendorName = DecoderVendorFactory.getDecoderVendorName(vendors, cvData);
                            decoder.setVendorId(vendorName);
                            break;
                        case 803: // decoder identification
                            String decoderId = decoder.getDecoderVersionAndModel();
                            decoderId += "(LD-G-" + Integer.toHexString(cvData) + ")";
                            decoder.setDecoderVersionAndModel(decoderId);
                        default:
                            break;
                    }
                    break;
                }
            }
        }

        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                LOGGER.info("Fire decoder CV changed.");
                fireMultiplePropertiesChanged();
            }
        });
    }

    /**
     * @return the decoderFile
     */
    public String getDecoderFile() {
        return decoderFile;
    }

    /**
     * @param decoderFile
     *            the decoderFile to set
     */
    public void setDecoderFile(String decoderFile) {
        String oldValue = this.decoderFile;
        this.decoderFile = decoderFile;

        firePropertyChange(PROPERTY_DECODER_FILE, oldValue, decoderFile);

        if (this.decoderFile == null) {
            synchronized (decoderList) {
                for (Decoder decoder : decoderList) {
                    decoder.setPrepareProgress(0);
                    decoder.setPerformProgress(0);
                }
            }
        }
        setReadyForPerformUpdate(false);
    }

    /**
     * @return the hasFirmwareAvailable
     */
    public boolean isHasFirmwareAvailable() {
        return hasFirmwareAvailable;
    }

    /**
     * @param hasFirmwareAvailable
     *            the hasFirmwareAvailable to set
     */
    public void setHasFirmwareAvailable(boolean hasFirmwareAvailable) {
        boolean oldValue = this.hasFirmwareAvailable;
        this.hasFirmwareAvailable = hasFirmwareAvailable;

        firePropertyChange(PROPERTY_HAS_FIRMWARE_AVAILABLE, oldValue, hasFirmwareAvailable);

        synchronized (decoderList) {
            if (!decoderList.isEmpty() && isHasFirmwareAvailable()) {
                setReadyForPrepareUpdate(true);
            }
            else {
                setReadyForPrepareUpdate(false);
            }
        }
    }

    /**
     * @return the firmwareContent
     */
    public List<String> getFirmwareContent() {
        return firmwareContent;
    }

    /**
     * @param firmwareContent
     *            the firmwareContent to set
     */
    public void setFirmwareContent(List<String> firmwareContent) {
        List<String> oldValue = new LinkedList<>(this.firmwareContent);

        this.firmwareContent.clear();
        if (CollectionUtils.isNotEmpty(firmwareContent)) {
            this.firmwareContent.addAll(firmwareContent);
        }

        firePropertyChange(PROPERTY_FIRMWARE_CONTENT, oldValue, this.firmwareContent);

        if (this.firmwareContent.isEmpty()) {
            setHasFirmwareAvailable(false);
        }
        else {
            setHasFirmwareAvailable(true);
        }
    }

    /**
     * @return the readyForPrepareUpdate
     */
    public boolean isReadyForPrepareUpdate() {
        return readyForPrepareUpdate;
    }

    /**
     * @param readyForPrepareUpdate
     *            the readyForPrepareUpdate to set
     */
    public void setReadyForPrepareUpdate(boolean readyForPrepareUpdate) {
        boolean oldValue = this.readyForPrepareUpdate;

        // check if the new value is true but update is in progress
        if (readyForPrepareUpdate && updateInProgress) {
            LOGGER.debug("Skip set the readyForPrepareUpdate enabled during update is in progress.");
        }
        else {
            this.readyForPrepareUpdate = readyForPrepareUpdate;
        }

        firePropertyChange(PROPERTY_READY_FOR_PREPARE_UPDATE, oldValue, readyForPrepareUpdate);
    }

    /**
     * @return the readyForPerformUpdate
     */
    public boolean isReadyForPerformUpdate() {
        return readyForPerformUpdate;
    }

    /**
     * @param readyForPerformUpdate
     *            the readyForPerformUpdate to set
     */
    public void setReadyForPerformUpdate(boolean readyForPerformUpdate) {
        boolean oldValue = this.readyForPerformUpdate;

        // check if the new value is true but update is in progress
        if (readyForPerformUpdate && updateInProgress) {
            LOGGER.debug("Skip set the readyForPerformUpdate enabled during update is in progress.");
        }
        else {
            this.readyForPerformUpdate = readyForPerformUpdate;
        }
        firePropertyChange(PROPERTY_READY_FOR_PERFORM_UPDATE, oldValue, readyForPerformUpdate);
    }

    /**
     * @return the updateInProgress
     */
    public boolean isUpdateInProgress() {
        return updateInProgress;
    }

    /**
     * @param updateInProgress
     *            the updateInProgress to set
     */
    public void setUpdateInProgress(boolean updateInProgress) {
        boolean oldValue = this.updateInProgress;
        this.updateInProgress = updateInProgress;

        firePropertyChange(PROPERTY_UPDATE_IN_PROGRESS, oldValue, updateInProgress);

        if (updateInProgress) {
            setReadyForPerformUpdate(false);
            setReadyForPrepareUpdate(false);
        }
        else {
            // the firmware update is no longer in progress
            setHasFirmwareAvailable(hasFirmwareAvailable);
        }
    }

    public void checkPendingPrepare() {
        LOGGER.debug("checkPendingPrepare is called.");
        synchronized (decoderList) {
            // find the first decoder that is not done with prepare for update
            Decoder decoder = IterableUtils.find(decoderList, new Predicate<Decoder>() {
                @Override
                public boolean evaluate(Decoder decoder) {
                    boolean done = decoder.isPrepareUpdateDone();
                    LOGGER.debug("Current decoder: {}, done: {}", decoder, done);
                    return !done;
                }
            });

            LOGGER.debug("Current found decoder: {}", decoder);
            setReadyForPerformUpdate(decoder == null);
        }
    }
}
