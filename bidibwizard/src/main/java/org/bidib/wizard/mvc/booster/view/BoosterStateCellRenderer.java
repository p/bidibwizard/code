package org.bidib.wizard.mvc.booster.view;

import java.awt.Component;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

import org.bidib.jbidibc.core.enumeration.BoosterState;
import org.bidib.wizard.common.utils.ImageUtils;

public class BoosterStateCellRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = 1L;

    Map<BoosterState, ImageIcon> boosterStateIcons;

    public BoosterStateCellRenderer() {

        boosterStateIcons = new LinkedHashMap<>();
        boosterStateIcons.put(BoosterState.OFF,
            ImageUtils.createImageIcon(BoosterStateCellRenderer.class, "/icons/booster/boosterOff_00.png"));
        boosterStateIcons.put(BoosterState.OFF_SHORT,
            ImageUtils.createImageIcon(BoosterStateCellRenderer.class, "/icons/booster/boosterOff_01_by_short.png"));
        boosterStateIcons.put(BoosterState.OFF_HOT,
            ImageUtils.createImageIcon(BoosterStateCellRenderer.class, "/icons/booster/boosterOff_02_by_hot.png"));
        boosterStateIcons.put(BoosterState.OFF_NO_POWER,
            ImageUtils.createImageIcon(BoosterStateCellRenderer.class, "/icons/booster/boosterOff_03_by_novcc.png"));
        boosterStateIcons.put(BoosterState.OFF_GO_REQ,
            ImageUtils.createImageIcon(BoosterStateCellRenderer.class, "/icons/booster/boosterOff_04_by_askforon.png"));
        boosterStateIcons.put(BoosterState.OFF_HERE,
            ImageUtils.createImageIcon(BoosterStateCellRenderer.class, "/icons/booster/boosterOff_05_by_localkey.png"));
        boosterStateIcons.put(BoosterState.OFF_NO_DCC,
            ImageUtils.createImageIcon(BoosterStateCellRenderer.class, "/icons/booster/boosterOff_06_by_nodcc.png"));
        boosterStateIcons.put(BoosterState.ON,
            ImageUtils.createImageIcon(BoosterStateCellRenderer.class, "/icons/booster/boosterOn_80.png"));
        boosterStateIcons.put(BoosterState.ON_LIMIT,
            ImageUtils.createImageIcon(BoosterStateCellRenderer.class, "/icons/booster/boosterOn_81_cur_limit.png"));
        boosterStateIcons.put(BoosterState.ON_HOT,
            ImageUtils.createImageIcon(BoosterStateCellRenderer.class, "/icons/booster/boosterOn_82_hot.png"));
        boosterStateIcons.put(BoosterState.ON_STOP_REQ,
            ImageUtils.createImageIcon(BoosterStateCellRenderer.class, "/icons/booster/boosterOn_83_askforstop.png"));
        boosterStateIcons.put(BoosterState.ON_HERE,
            ImageUtils.createImageIcon(BoosterStateCellRenderer.class, "/icons/booster/boosterOn_84_by_localkey.png"));

        setHorizontalAlignment(SwingConstants.CENTER);
    }

    public Component getTableCellRendererComponent(
        JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        // provide value == null because default processing is setText(value.toString())
        super.getTableCellRendererComponent(table, null, isSelected, hasFocus, row, column);

        if (value instanceof BoosterState) {
            BoosterState boosterState = (BoosterState) value;

            Icon icon = boosterStateIcons.get(boosterState);
            setIcon(icon);

            setToolTipText(boosterState.toString());
        }
        else {
            setIcon(null);
            setToolTipText(null);
        }

        return this;
    }
}