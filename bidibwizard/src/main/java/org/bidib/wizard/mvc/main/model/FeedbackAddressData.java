package org.bidib.wizard.mvc.main.model;

import org.bidib.jbidibc.core.enumeration.AddressTypeEnum;

public class FeedbackAddressData {
    private final int address;

    private int speed = 0;

    private final AddressTypeEnum type;

    public FeedbackAddressData(int address, AddressTypeEnum type) {
        this.address = address;
        this.type = type;
    }

    public int getAddress() {
        return address;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public AddressTypeEnum getType() {
        return type;
    }

    public String toString() {
        StringBuffer result = new StringBuffer();

        result.append(address);
        switch (type) {
            case ACCESSORY:
                result.append("A");
                break;
            case EXTENDED_ACCESSORY:
                result.append("X");
                break;
            case LOCOMOTIVE_BACKWARD:
                // result.append("\u25c0");
                result.append("<");
                break;
            case LOCOMOTIVE_FORWARD:
                // result.append("\u25b6");
                result.append(">");
                break;
        }
        return result.toString();
    }
}
