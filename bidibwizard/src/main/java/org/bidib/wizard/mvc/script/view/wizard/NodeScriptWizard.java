package org.bidib.wizard.mvc.script.view.wizard;

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

import org.apache.commons.collections4.MapUtils;
import org.bidib.jbidibc.core.Node;
import org.bidib.schema.nodescriptsources.NodeScriptSources;
import org.bidib.wizard.common.context.ApplicationContext;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.common.utils.ImageUtils;
import org.bidib.wizard.mvc.preferences.model.Preferences;
import org.bidib.wizard.mvc.script.controller.NodeScriptProcessor;
import org.bidib.wizard.mvc.script.model.NodeScriptSource;
import org.bidib.wizard.mvc.script.model.NodeScriptSource.ScriptSource;
import org.bidib.wizard.script.DefaultScriptContext;
import org.bidib.wizard.script.node.NodeScriptContextKeys;
import org.bidib.wizard.utils.NodeScriptSourcesFactory;
import org.oxbow.swingbits.dialog.task.TaskDialogs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jidesoft.dialog.ButtonNames;
import com.jidesoft.dialog.ButtonPanel;
import com.jidesoft.dialog.PageList;
import com.jidesoft.swing.JideSwingUtilities;
import com.jidesoft.wizard.AbstractWizardPage;
import com.jidesoft.wizard.WizardDialog;
import com.jidesoft.wizard.WizardStyle;
import com.vlsolutions.swing.docking.DockingDesktop;

public class NodeScriptWizard {
    private static final Logger LOGGER = LoggerFactory.getLogger(NodeScriptWizard.class);

    private final NodeScriptProcessor nodeScriptProcessor;

    public NodeScriptWizard(final NodeScriptProcessor nodeScriptProcessor) {

        this.nodeScriptProcessor = nodeScriptProcessor;
    }

    private Map<NodeScriptSource, NodeScriptSources> loadNodeScriptSources() {

        File file = new File("");
        file = new File(file.getAbsoluteFile(), "data/nodescript/nodeScriptSources.xml");
        String searchPathInstallation = file.getPath();

        String labelPath = Preferences.getInstance().getLabelV2Path();
        String searchPathUser = "/data/nodescript/nodeScriptSources.xml";
        File fileUser = new File(labelPath, searchPathUser);
        searchPathUser = fileUser.getPath();

        // the internal path is used as fallback location only
        String searchPathInternal = "classpath:xml/nodescript/nodeScriptSources.xml";

        LOGGER.info("Prepared nodeScriptSources search path user: {}, app: {}, internal: {}", searchPathUser,
            searchPathInstallation, searchPathInternal);

        Map<NodeScriptSource, NodeScriptSources> mapNodeScriptSources = new HashMap<>();

        // load user defined nodeScripts
        try {
            NodeScriptSources userScripts = NodeScriptSourcesFactory.loadNodeScriptSources(searchPathUser);
            if (userScripts == null && fileUser.getParentFile().exists()) {
                LOGGER.info(
                    "The user directory contains a valid nodescript directory. Prepare a default userScript sources instance to allow scanning of user defined scripts.");
                userScripts = NodeScriptSourcesFactory.loadNodeScriptSources(searchPathInternal);
            }
            if (userScripts != null) {
                mapNodeScriptSources.put(new NodeScriptSource(ScriptSource.user, searchPathUser), userScripts);
            }
        }
        catch (FileNotFoundException ex) {
            LOGGER.warn("Load nodeScript sources failed.", ex);
        }

        // load installation defined nodeScripts
        try {
            NodeScriptSources installationScripts =
                NodeScriptSourcesFactory.loadNodeScriptSources(searchPathInstallation);
            if (installationScripts != null) {
                mapNodeScriptSources.put(new NodeScriptSource(ScriptSource.installation, searchPathInstallation),
                    installationScripts);
            }
        }
        catch (FileNotFoundException ex) {
            LOGGER.warn("Load nodeScript sources failed.", ex);
        }

        return mapNodeScriptSources;
    }

    public void showWizard(final DockingDesktop desktop, final Node selectedNode) {
        LOGGER.info("Show the NodeScriptWizard.");

        int style = WizardStyle.WIZARD97_STYLE;

        Icon logoIcon = ImageUtils.createImageIcon(NodeScriptWizard.class, "/icons/wizard/nodescripting.png");

        WizardStyle.setStyle(style);

        Map<NodeScriptSource, NodeScriptSources> mapNodeScriptSources = loadNodeScriptSources();
        if (MapUtils.isEmpty(mapNodeScriptSources)) {
            LOGGER.error("Load nodeScriptSources was not successful. Abort operation.");

            TaskDialogs
                .build(JOptionPane.getFrameForComponent(desktop),
                    Resources.getString(NodeScriptWizard.class, "load-nodescriptresources-failed.instruction"),
                    Resources.getString(NodeScriptWizard.class, "load-nodescriptresources-failed.text"))
                .error();

            return;
        }

        Frame frame = null;

        final ApplicationContext scriptContext = new DefaultScriptContext();

        Locale locale = Locale.getDefault();
        String lang = locale.getLanguage();
        LOGGER.info("Current locale: {}, current lang: {}", locale, lang);

        scriptContext.register(NodeScriptContextKeys.KEY_LANG, lang);
        scriptContext.register(NodeScriptContextKeys.KEY_NODESCRIPTPROCESSOR, nodeScriptProcessor);
        scriptContext.register(NodeScriptContextKeys.KEY_SELECTED_CORE_NODE, selectedNode);

        final WizardDialog wizard = new WizardDialog(frame, Resources.getString(NodeScriptWizard.class, "title")) {
            @Override
            public ButtonPanel createButtonPanel() {

                ButtonPanel bp = super.createButtonPanel();
                ((JButton) bp.getButtonByName(NEXT)).setText(Resources.getString(NodeScriptWizard.class, "next"));
                ((JButton) bp.getButtonByName(BACK)).setText(Resources.getString(NodeScriptWizard.class, "back"));
                return bp;
            }
        };
        wizard.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        wizard.setDefaultGraphic(
            ImageUtils.createImageIcon(NodeScriptWizard.class, "/icons/wizard/nodeScriptWizard.png").getImage());

        wizard.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                wizard.dispose();
            }
        });

        // set new size
        wizard.setPreferredSize(new Dimension(600, 500));

        PageList model = new PageList();
        AbstractWizardPage page1 = new WelcomePage();

        AbstractWizardPage page4 = new SelectCategoryPage(logoIcon, scriptContext, mapNodeScriptSources);
        AbstractWizardPage page5 = new SelectApplicationPage(logoIcon, scriptContext, mapNodeScriptSources);
        AbstractWizardPage page6 = new ScriptInputParametersPage(logoIcon, scriptContext);
        AbstractWizardPage page7 = new CompletionPage(scriptContext);
        model.append(page1);
        model.append(page4);
        model.append(page5);
        model.append(page6);
        model.append(page7);

        wizard.setPageList(model);
        wizard.setStepsPaneNavigable(true);

        // the code below is to show you how to customize the finish and cancel action. You don't have to call these two
        // methods.
        wizard.setFinishAction(new AbstractAction(Resources.getString(NodeScriptWizard.class, "finish")) {
            public void actionPerformed(ActionEvent e) {
                if (wizard.closeCurrentPage(wizard.getButtonPanel().getButtonByName(ButtonNames.FINISH))) {
                    wizard.dispose();
                }
            }
        });
        wizard.setCancelAction(new AbstractAction(Resources.getString(NodeScriptWizard.class, "cancel")) {
            public void actionPerformed(ActionEvent e) {
                if (wizard.closeCurrentPage(wizard.getButtonPanel().getButtonByName(ButtonNames.CANCEL))) {
                    wizard.dispose();
                }
            }
        });

        wizard.pack();
        wizard.setResizable(false); // for wizard, it's better to make it not resizable.
        JideSwingUtilities.globalCenterWindow(wizard);
        wizard.setVisible(true);
    }
}
