package org.bidib.wizard.mvc.main.model.listener;

import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.mvc.main.model.ServoPort;

public interface ServoPortListener<S extends BidibStatus> extends OutputListener<S> {
    void valuesChanged(ServoPort servoPort);
}
