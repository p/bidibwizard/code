package org.bidib.wizard.mvc.dmx.view.scenery;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.bidib.jbidibc.exchange.dmxscenery.DmxSceneries;
import org.bidib.jbidibc.exchange.dmxscenery.DmxSceneriesExchange;
import org.bidib.jbidibc.exchange.dmxscenery.DmxSceneryType;
import org.bidib.wizard.comm.BacklightPortStatus;
import org.bidib.wizard.comm.LightPortStatus;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.dialog.FileDialog;
import org.bidib.wizard.mvc.common.view.slider.LabeledSlider;
import org.bidib.wizard.mvc.common.view.validation.IconFeedbackPanel;
import org.bidib.wizard.mvc.common.view.validation.PropertyValidationI18NSupport;
import org.bidib.wizard.mvc.dmx.controller.listener.DmxModelerControllerListener;
import org.bidib.wizard.mvc.dmx.model.DmxChannel;
import org.bidib.wizard.mvc.dmx.model.DmxLightPort;
import org.bidib.wizard.mvc.dmx.model.DmxScenery;
import org.bidib.wizard.mvc.dmx.model.DmxSceneryModel;
import org.bidib.wizard.mvc.dmx.model.DmxSceneryPoint;
import org.bidib.wizard.mvc.dmx.view.utils.SceneryUtils;
import org.bidib.wizard.mvc.main.model.BacklightPort;
import org.bidib.wizard.mvc.main.model.Macro;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.model.Port;
import org.bidib.wizard.mvc.main.model.function.BacklightPortAction;
import org.bidib.wizard.mvc.main.model.function.DelayFunction;
import org.bidib.wizard.mvc.main.model.function.Function;
import org.bidib.wizard.mvc.main.model.function.LightPortAction;
import org.bidib.wizard.mvc.main.model.function.MacroFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.adapter.BoundedRangeAdapter;
import com.jgoodies.binding.adapter.ComboBoxAdapter;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.binding.value.BindingConverter;
import com.jgoodies.binding.value.BufferedValueModel;
import com.jgoodies.binding.value.ConverterValueModel;
import com.jgoodies.binding.value.ValueHolder;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.builder.ButtonBarBuilder;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.debug.FormDebugPanel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.ValidationResultModel;
import com.jgoodies.validation.util.PropertyValidationSupport;
import com.jgoodies.validation.view.ValidationComponentUtils;

public class SceneryPanel {
    private static final Logger LOGGER = LoggerFactory.getLogger(SceneryPanel.class);

    private static final String ENCODED_COLUMN_SPECS_TOP = "pref, 3dlu, fill:30dlu:grow";

    private static final String ENCODED_COLUMN_SPECS = "pref, 3dlu, fill:50dlu:grow";

    private static final String SCENERIES_EXTENSION = "sceneries";

    // description, suffix for node files
    private static String sceneriesDescription;

    private static FileFilter sceneriesFilter;

    private final DmxSceneryModel dmxSceneryModel;

    private final Node node;

    private DmxModelerControllerListener dmxModelerControllerListener;

    private JList<DmxScenery> sceneryList;

    private SelectionInList<DmxScenery> scenerySelection;

    private DmxScenery editedScenery;

    private PresentationModel<DmxScenery> detailsModel;

    private JPanel bottomDetailsPanel;

    private JTextField sceneryName;

    private JTree usedChannelsTree;

    private JButton applyButton;

    private JButton resetButton;

    private JPopupMenu sceneryMenu;

    private BufferedValueModel sceneryNameModel;

    private DefaultTreeModel usedChannelsTreeModel;

    private DefaultMutableTreeNode root;

    private SceneryValidationResultModel validationResultModel;

    private ApplyButtonEnabledModel applyButtonEnabled;

    private JMenuItem openViewer;

    private JMenuItem addDmxChannel;

    private ValueModel dimmStretchUpModel;

    private ValueModel dimmStretchDownModel;

    public SceneryPanel(final Node node, final DmxSceneryModel dmxSceneryModel) {
        this.dmxSceneryModel = dmxSceneryModel;
        this.node = node;

        sceneriesDescription = Resources.getString(SceneryPanel.class, "sceneriesDescription");
        sceneriesFilter = new FileNameExtensionFilter(sceneriesDescription, SCENERIES_EXTENSION);
    }

    /**
     * @param dmxModelerControllerListener
     *            the dmxModelerControllerListener to set
     */
    public void setDmxModelerControllerListener(DmxModelerControllerListener dmxModelerControllerListener) {
        this.dmxModelerControllerListener = dmxModelerControllerListener;
    }

    public JComponent createPanel() {

        detailsModel = new PresentationModel<DmxScenery>(new ValueHolder(null, true));

        // prepare the dimmStretchModel
        dimmStretchUpModel = new ValueHolder(Integer.valueOf(1));
        dimmStretchDownModel = new ValueHolder(Integer.valueOf(1));

        applyButtonEnabled = new ApplyButtonEnabledModel();

        sceneryList = new JList<DmxScenery>();
        sceneryList.addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (e.getValueIsAdjusting())
                    return;

                // Now set the current selection as edited scenery.
                editedScenery = (DmxScenery) sceneryList.getSelectedValue();

                // Then copy the scenery data to the component values.
                updateView(editedScenery);
            }
        });

        // add action to popup menu
        sceneryMenu = new JPopupMenu(Resources.getString(SceneryPanel.class, "scenery"));
        openViewer = sceneryMenu.add(new AbstractAction(
            Resources.getString(SceneryPanel.class, "open-viewer")) {
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {
                DmxScenery dmxScenery = sceneryList.getSelectedValue();
                if (dmxScenery != null) {
                    LOGGER.info("Show selected scenery: {}", dmxScenery);
                    dmxModelerControllerListener.openView(dmxScenery);
                }
            }
        });

        sceneryMenu.add(new AbstractAction(
            Resources.getString(SceneryPanel.class, "new-scenery")) {
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {

                addNewScenery();
            }
        });
        sceneryMenu.add(new AbstractAction(
            Resources.getString(SceneryPanel.class, "import-scenery")) {
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {

                loadScenery();

                // select the first
                if (sceneryList.getSelectedValue() == null && scenerySelection.getSize() > 0) {
                    LOGGER.debug("No scenery selected, select the first.");

                    sceneryList.setSelectedIndex(0);
                }
            }
        });
        sceneryMenu.add(new AbstractAction(
            Resources.getString(SceneryPanel.class, "export-scenery")) {
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {

                saveScenery();
            }
        });
        sceneryMenu.add(new AbstractAction(
            Resources.getString(SceneryPanel.class, "export-all-sceneries")) {
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {

                saveAllSceneries();
            }
        });

        sceneryMenu.addSeparator();

        sceneryMenu.add(new AbstractAction(
            Resources.getString(SceneryPanel.class, "transform-scenery")) {
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {

                transformScenery();
            }
        });

        sceneryMenu.addSeparator();

        addDmxChannel = sceneryMenu.add(new AbstractAction(
            Resources.getString(SceneryPanel.class, "menu.add-dmx-channel")) {
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {
                DmxScenery dmxScenery = sceneryList.getSelectedValue();
                if (dmxScenery != null) {
                    LOGGER.info("Add DMX channel to selected scenery: {}", dmxScenery);

                    // add DMX channel
                    if (CollectionUtils.isNotEmpty(dmxSceneryModel.getDmxChannels())) {

                        // show a dialog to select the new channel
                        DmxChannelSelectionPanel dmxChannelSelectionPanel = new DmxChannelSelectionPanel();

                        // prepare a copy of the list with all dmx channels
                        List<DmxChannel> availableChannels = new LinkedList<>(dmxSceneryModel.getDmxChannels());
                        // remove the already assigned channels
                        availableChannels.removeAll(dmxScenery.getUsedChannels());

                        List<DmxChannel> selectedDmxChannels =
                            dmxChannelSelectionPanel.showDialog(JOptionPane.getFrameForComponent(sceneryList),
                                availableChannels);

                        if (CollectionUtils.isNotEmpty(selectedDmxChannels)) {
                            LOGGER.info("Add the new DMX channels.");

                            dmxScenery.addUsedChannels(selectedDmxChannels);
                        }
                    }
                }
            }
        });

        sceneryList.addMouseListener(new PopupListener());
        sceneryList.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {

                if (e.getButton() == MouseEvent.BUTTON1 && e.getClickCount() == 2) {
                    // open the viewer
                    LOGGER.info("Open the viewer.");
                    DmxScenery selectedScenery = sceneryList.getSelectedValue();
                    if (selectedScenery != null) {
                        e.consume();
                        LOGGER.info("Show selected scenery: {}", selectedScenery);
                        dmxModelerControllerListener.openView(selectedScenery);
                    }
                }
                super.mouseClicked(e);
            }
        });

        DefaultFormBuilder builderTop = null;
        boolean debugTop = false;
        if (debugTop) {
            builderTop = new DefaultFormBuilder(new FormLayout(ENCODED_COLUMN_SPECS_TOP), new FormDebugPanel());
        }
        else {
            builderTop = new DefaultFormBuilder(new FormLayout(ENCODED_COLUMN_SPECS_TOP), new JPanel());
        }
        builderTop.border(Borders.TABBED_DIALOG);

        // add components
        scenerySelection = new SelectionInList<DmxScenery>(dmxSceneryModel.getSceneries());

        Bindings.bind(sceneryList, scenerySelection);

        builderTop.append(new JScrollPane(sceneryList), 3);

        JPanel topPanel = builderTop.build();

        // lower part

        DefaultFormBuilder builder = null;
        boolean debug = false;
        if (debug) {
            builder = new DefaultFormBuilder(new FormLayout(ENCODED_COLUMN_SPECS), new FormDebugPanel());
        }
        else {
            builder = new DefaultFormBuilder(new FormLayout(ENCODED_COLUMN_SPECS), new JPanel());
        }
        builder.border(Borders.TABBED_DIALOG);

        sceneryNameModel = detailsModel.getBufferedModel(DmxScenery.PROPERTY_NAME);

        // scenery name
        sceneryName = BasicComponentFactory.createTextField(sceneryNameModel, false);
        sceneryNameModel.addPropertyChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                LOGGER.info("Scenery name model has changed: {}", evt);
                triggerValidation();
            }
        });
        builder.append(Resources.getString(getClass(), "scenery-name"), sceneryName);

        // macro number combo
        //
        ValueModel sceneryMacroModel = detailsModel.getBufferedModel(DmxScenery.PROPERTY_MACRO_NUMBER);
        // wire our new combobox up to that property adapter.
        ValueModel converterMacroValueModel = new ConverterValueModel(sceneryMacroModel, new MacroNumberConverter());
        @SuppressWarnings("unchecked")
        JComboBox<Macro> macroNumberComboBox =
            new JComboBox<Macro>(new ComboBoxAdapter<Macro>(dmxSceneryModel.getMacros(), converterMacroValueModel));
        builder.append(Resources.getString(getClass(), "macro-number"), macroNumberComboBox);

        builder.appendRow("5dlu");
        builder.nextLine(2);

        ValidationComponentUtils.setMandatory(sceneryName, true);
        ValidationComponentUtils.setMessageKey(sceneryName, "validation.sceneryname_key");

        validationResultModel = new SceneryValidationResultModel();

        // ------------
        root = new DefaultMutableTreeNode();
        usedChannelsTreeModel = new DefaultTreeModel(root);
        usedChannelsTree = new JTree(usedChannelsTreeModel);
        usedChannelsTree.setCellRenderer(new DmxChannelNodeTreeRenderer());
        usedChannelsTree.setRootVisible(false);

        BufferedValueModel channelModel = detailsModel.getBufferedModel(DmxScenery.PROPERTY_USED_CHANNELS);
        channelModel.addPropertyChangeListener(ValueModel.PROPERTY_VALUE, new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                LOGGER.info("The selected scenery has been changed, new DMX channel list: {}", evt.getNewValue());

                if (evt.getNewValue() instanceof List<?>) {
                    @SuppressWarnings("unchecked")
                    List<DmxChannel> dmxChannels = (List<DmxChannel>) evt.getNewValue();
                    setUsedDmxChannels(dmxChannels);
                }
                else {
                    // clear and refresh the tree
                    root.removeAllChildren();
                    usedChannelsTreeModel.reload();
                }
            }
        });

        usedChannelsTree.addMouseListener(new MouseAdapter() {
            public void mouseReleased(MouseEvent e) {
                potentiallyShowPopup(e);
            }

            public void mousePressed(MouseEvent e) {
                potentiallyShowPopup(e);
            }
        });

        // -------------

        builder.appendRow("fill:200px:grow");
        JScrollPane scrollChannels = new JScrollPane(usedChannelsTree);
        scrollChannels.setPreferredSize(new Dimension(100, 100));
        builder.append(scrollChannels, 3);

        // dimm stretch
        final LabeledSlider dimmStretchUpSlider =
            new LabeledSlider(new BoundedRangeAdapter(dimmStretchUpModel, 0, 1, 65535), null, true);
        dimmStretchUpSlider.createComponent();
        builder.append(Resources.getString(getClass(), "dimm-stretch-up"), dimmStretchUpSlider);

        dimmStretchUpModel.addValueChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {

                Integer dimmSlopeUp = (Integer) dimmStretchUpModel.getValue();

                // change the port config and transfer to the node

                Port<?> port = getSelectedPort();

                LOGGER.info("The dimm stretch up model has been changed: {}, port: {}", dimmSlopeUp, port);

                if (port instanceof DmxLightPort) {
                    ((DmxLightPort) port).getLightPort().setDimMax(dimmSlopeUp);

                    dmxModelerControllerListener.portConfigChanged(port);
                }
                else if (port instanceof BacklightPort) {
                    ((BacklightPort) port).setDimSlopeUp(dimmSlopeUp);

                    dmxModelerControllerListener.portConfigChanged(port);
                }
                else {

                }
            }
        });

        builder.nextLine();

        final LabeledSlider dimmStretchDownSlider =
            new LabeledSlider(new BoundedRangeAdapter(dimmStretchDownModel, 0, 1, 65535), null, true);
        dimmStretchDownSlider.createComponent();
        builder.append(Resources.getString(getClass(), "dimm-stretch-down"), dimmStretchDownSlider);

        dimmStretchDownModel.addValueChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                Integer dimmSlopeDown = (Integer) dimmStretchDownModel.getValue();

                Port<?> port = getSelectedPort();

                LOGGER.info("The dimm stretch down model has been changed: {}, port: {}", dimmSlopeDown, port);

                if (port instanceof DmxLightPort) {
                    ((DmxLightPort) port).getLightPort().setDimMin(dimmSlopeDown);

                    dmxModelerControllerListener.portConfigChanged(port);
                }
                else if (port instanceof BacklightPort) {
                    ((BacklightPort) port).setDimSlopeDown(dimmSlopeDown);

                    dmxModelerControllerListener.portConfigChanged(port);
                }
                else {

                }
            }
        });

        builder.nextLine();

        applyButton = new JButton(new ApplyAction());
        resetButton = new JButton(new ResetAction());

        applyButton.setEnabled(false);
        resetButton.setEnabled(false);

        builder.append(buildButtonBar(), 3);

        // add bindings for enable/disable the write button
        PropertyConnector.connect(detailsModel, PresentationModel.PROPERTY_BUFFERING, applyButtonEnabled,
            ApplyButtonEnabledModel.PROPERTY_MODEL_BUFFERING);
        PropertyConnector.connect(detailsModel, PresentationModel.PROPERTY_BUFFERING, resetButton, "enabled");

        PropertyConnector.connect(applyButtonEnabled, ApplyButtonEnabledModel.PROPERTY_BUTTON_ENABLED, applyButton,
            "enabled");

        // build the panel

        // check if we have validation enabled
        if (getValidationResultModel() != null) {
            LOGGER.info("Create iconfeedback panel.");
            JComponent cvIconPanel = new IconFeedbackPanel(getValidationResultModel(), builder.build());
            DefaultFormBuilder feedbackBuilder = null;
            feedbackBuilder = new DefaultFormBuilder(new FormLayout("p:g"));

            feedbackBuilder.appendRow("fill:p:grow");
            feedbackBuilder.add(cvIconPanel);

            bottomDetailsPanel = feedbackBuilder.build();

            // validation is triggered
            triggerValidation();
        }
        else {
            bottomDetailsPanel = builder.build();
        }

        JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, topPanel, new JScrollPane(bottomDetailsPanel));
        splitPane.setDividerLocation(125);

        // disable elements of the bottomDetailsPanel
        disableInputElements();
        // the bottomDetailsPanel is enabled if a scenery is selected
        detailsModel.getBeanChannel().addValueChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {

                if (detailsModel.getBean() != null) {
                    enableInputElements();
                }
                else {
                    disableInputElements();
                }
            }
        });

        usedChannelsTree.addTreeSelectionListener(new TreeSelectionListener() {

            @Override
            public void valueChanged(TreeSelectionEvent e) {

                Integer dimmSlopeUpValue = null;
                Integer dimmSlopeDownValue = null;

                Port<?> port = getSelectedPort();

                if (port instanceof DmxLightPort) {
                    int dimmSlopeUp = ((DmxLightPort) port).getLightPort().getDimMax();
                    int dimmSlopeDown = ((DmxLightPort) port).getLightPort().getDimMin();

                    dimmSlopeUpValue = Integer.valueOf(dimmSlopeUp);
                    dimmSlopeDownValue = Integer.valueOf(dimmSlopeDown);
                }
                else if (port instanceof BacklightPort) {
                    int dimmSlopeUp = ((BacklightPort) port).getDimSlopeUp();
                    int dimmSlopeDown = ((BacklightPort) port).getDimSlopeDown();

                    dimmSlopeUpValue = Integer.valueOf(dimmSlopeUp);
                    dimmSlopeDownValue = Integer.valueOf(dimmSlopeDown);
                }
                else {

                }

                if (dimmSlopeUpValue != null) {
                    dimmStretchUpSlider.setEnabled(true);
                    dimmStretchUpModel.setValue(dimmSlopeUpValue);
                }
                else {
                    dimmStretchUpSlider.setEnabled(false);
                    dimmStretchUpModel.setValue(1);
                }

                if (dimmSlopeDownValue != null) {
                    dimmStretchDownSlider.setEnabled(true);
                    dimmStretchDownModel.setValue(dimmSlopeDownValue);
                }
                else {
                    dimmStretchDownSlider.setEnabled(false);
                    dimmStretchDownModel.setValue(1);
                }

            }
        });

        return splitPane;
    }

    private Port<?> getSelectedPort() {
        Port<?> selectedPort = null;
        TreePath path = usedChannelsTree.getSelectionPath();
        if (path != null) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) path.getLastPathComponent();

            LOGGER.info("Currently selected node: {}", node);
            if (node instanceof PortNode) {
                PortNode portNode = (PortNode) node;
                selectedPort = portNode.getPort();
            }
        }
        return selectedPort;
    }

    private final class MacroNumberConverter implements BindingConverter<Number, Macro> {

        @Override
        public Macro targetValue(Number sourceValue) {
            LOGGER.trace("Convert to target, sourceValue: {}", sourceValue);
            if (sourceValue != null) {
                for (Macro macro : dmxSceneryModel.getMacros()) {
                    if (macro != null && macro.getId() == sourceValue.intValue()) {
                        return macro;
                    }
                }
            }
            return null;
        }

        @Override
        public Number sourceValue(Macro targetValue) {
            LOGGER.trace("Convert to source, targetValue: {}", targetValue);
            if (targetValue != null) {
                return targetValue.getId();
            }
            return null;
        }

    }

    public void enableComponents(Container container, boolean enable) {
        Component[] components = container.getComponents();
        for (Component component : components) {
            if (component instanceof JComboBox<?>) {
                LOGGER.info("Current comp is a JComboBox: {}, enable: {}", component, enable);
                component.setEnabled(enable);
            }
            else if (component instanceof JSlider) {
                // do nothing
            }
            else if (!(component instanceof JButton) && !(component instanceof Container)) {
                component.setEnabled(enable);
            }
            if (component instanceof Container) {
                enableComponents((Container) component, enable);
            }

        }
    }

    private void disableInputElements() {
        LOGGER.info("+++ disableInputElements");

        enableComponents(bottomDetailsPanel, false);
    }

    private void enableInputElements() {
        LOGGER.info("+++ enableInputElements");

        enableComponents(bottomDetailsPanel, true);

        // check the validation model ...
        triggerValidation();
    }

    private void expandAllNodes(JTree tree, DefaultMutableTreeNode treeTop, int maxLevel) {
        DefaultMutableTreeNode currentNode = treeTop.getNextNode();
        while (currentNode != null) {
            if (currentNode.getLevel() <= maxLevel) {
                tree.expandPath(new TreePath(currentNode.getPath()));
            }
            currentNode = currentNode.getNextNode();
        }
    }

    private void expandNode(JTree tree, DefaultMutableTreeNode currentNode) {
        tree.expandPath(new TreePath(currentNode.getPath()));
    }

    private void addNewScenery() {
        DmxScenery dmxScenery = new DmxScenery("Scenery-" + System.currentTimeMillis());
        dmxScenery.setName(dmxScenery.getId());

        LOGGER.info("Add new DMX scenery: {}", dmxScenery);
        dmxSceneryModel.addScenery(dmxScenery);
        scenerySelection.setList(dmxSceneryModel.getSceneries());
    }

    private void loadScenery() {

        FileDialog dialog = new FileDialog(
            bottomDetailsPanel, FileDialog.OPEN, null, sceneriesFilter) {

            @Override
            public void approve(String fileName) {
                try {
                    setWaitCursor();
                    LOGGER.info("Start importing sceneries, fileName: {}", fileName);

                    DmxSceneries dmxSceneries = new DmxSceneriesExchange().loadDmxSceneries(fileName);

                    if (dmxSceneries != null && CollectionUtils.isNotEmpty(dmxSceneries.getDmxScenery())) {

                        for (DmxSceneryType dmxSceneryType : dmxSceneries.getDmxScenery()) {

                            final DmxScenery dmxScenery =
                                new DmxScenery(dmxSceneryType.getSceneryName()).withDmxScenery(dmxSceneryModel,
                                    dmxSceneryType);
                            LOGGER.info("Add new DMX scenery: {}", dmxScenery);

                            DmxScenery existingScenery =
                                CollectionUtils.find(dmxSceneryModel.getSceneries(), new Predicate<DmxScenery>() {

                                    @Override
                                    public boolean evaluate(DmxScenery currentScenery) {
                                        if (currentScenery.getName().equals(dmxScenery.getName())) {
                                            LOGGER.info("Found scenery to replace: {}", currentScenery);
                                            return true;
                                        }
                                        return false;
                                    }
                                });
                            if (existingScenery != null) {
                                // remove existing scenery
                                dmxSceneryModel.getSceneries().remove(existingScenery);
                            }

                            dmxSceneryModel.addScenery(dmxScenery);
                        }
                    }
                    scenerySelection.setList(dmxSceneryModel.getSceneries());

                    LOGGER.debug("Current dmxSceneries: {}", dmxSceneryModel.getSceneries());
                }
                finally {
                    setDefaultCursor();
                }
            }
        };
        dialog.showDialog();

    }

    private void setWaitCursor() {
        // TODO find a way to get the frame
        // view.setBusy(true);
    }

    private void setDefaultCursor() {
        // TODO find a way to get the frame
        // view.setBusy(false);
    }

    private void saveScenery() {

        final DmxScenery dmxScenery = sceneryList.getSelectedValue();

        // export node data
        FileDialog dialog = new FileDialog(
            bottomDetailsPanel, FileDialog.SAVE, node.toString() /* + "." + SCENERIES_EXTENSION */, sceneriesFilter) {

            @Override
            public void approve(String fileName) {
                LOGGER.info("Save sceneries to file: {}", fileName);

                try {
                    setWaitCursor();

                    // add extension if missing
                    if (!fileName.endsWith("." + SCENERIES_EXTENSION)) {
                        fileName += "." + SCENERIES_EXTENSION;
                    }

                    DmxSceneries dmxSceneries = new DmxSceneries();

                    for (DmxScenery currentDmxScenery : dmxSceneryModel.getSceneries()) {

                        if (currentDmxScenery.getName().equals(dmxScenery.getName())) {

                            LOGGER.info("Store points of scenery: {}", currentDmxScenery);
                            DmxSceneryType dmxSceneryType = currentDmxScenery.fromDmxScenery();
                            dmxSceneries.getDmxScenery().add(dmxSceneryType);

                            break;
                        }
                    }

                    if (CollectionUtils.isNotEmpty(dmxSceneries.getDmxScenery())) {
                        new DmxSceneriesExchange().saveDmxSceneries(dmxSceneries, fileName, false);
                    }
                    else {
                        LOGGER.warn("No scenery to export available.");
                        JOptionPane.showMessageDialog(JOptionPane.getFrameForComponent(bottomDetailsPanel),
                            Resources.getString(SceneryPanel.class, "dialog.no_scenery_selected"),
                            Resources.getString(SceneryPanel.class, "dialog.export_error"), JOptionPane.ERROR_MESSAGE);
                    }
                }
                finally {
                    setDefaultCursor();
                }
            }
        };

        if (dmxScenery != null) {
            String sceneryName = dmxScenery.getName();
            dialog.updateFileFilter(sceneriesFilter, sceneryName + "." + SCENERIES_EXTENSION);
        }

        dialog.showDialog();
    }

    private void saveAllSceneries() {

        // export node data
        FileDialog dialog = new FileDialog(
            bottomDetailsPanel, FileDialog.SAVE, null, null) {

            @Override
            public void approve(String fileName) {
                LOGGER.info("Save sceneries to file: {}", fileName);

                try {
                    setWaitCursor();

                    DmxSceneries dmxSceneries = new DmxSceneries();

                    for (DmxScenery currentDmxScenery : dmxSceneryModel.getSceneries()) {
                        dmxSceneries.getDmxScenery().clear();

                        LOGGER.info("Store points of scenery: {}", currentDmxScenery);
                        DmxSceneryType dmxSceneryType = currentDmxScenery.fromDmxScenery();
                        dmxSceneries.getDmxScenery().add(dmxSceneryType);

                        String sceneryFileName =
                            FilenameUtils.concat(fileName, currentDmxScenery.getName() + "." + SCENERIES_EXTENSION);
                        LOGGER.info("Save scenery to file: {}", sceneryFileName);

                        boolean override = super.checkOverrideExisting(new File(sceneryFileName));
                        if (override) {
                            if (CollectionUtils.isNotEmpty(dmxSceneries.getDmxScenery())) {
                                try {
                                    new DmxSceneriesExchange().saveDmxSceneries(dmxSceneries, sceneryFileName, false);
                                }
                                catch (Exception ex) {
                                    LOGGER.warn("Save dmxScenery failed to file: {}", sceneryFileName, ex);
                                    JOptionPane.showMessageDialog(JOptionPane.getFrameForComponent(bottomDetailsPanel),
                                        Resources.getString(SceneryPanel.class, "dialog.export_scenery_failed",
                                            new Object[] { currentDmxScenery.getName() }), Resources.getString(
                                            SceneryPanel.class, "dialog.export_error"), JOptionPane.ERROR_MESSAGE);
                                }
                            }
                            else {
                                LOGGER.warn("No scenery to export available.");
                                JOptionPane.showMessageDialog(JOptionPane.getFrameForComponent(bottomDetailsPanel),
                                    Resources.getString(SceneryPanel.class, "dialog.no_scenery_selected"),
                                    Resources.getString(SceneryPanel.class, "dialog.export_error"),
                                    JOptionPane.ERROR_MESSAGE);
                            }
                        }
                    }

                }
                finally {
                    setDefaultCursor();
                }
            }

            @Override
            protected boolean checkOverrideExisting(File file) {
                return true;
            }
        };
        dialog.setApproveButtonText(Resources.getString(SceneryPanel.class, "save_to_directory"));
        dialog.showDialog();
    }

    private void transformScenery() {

        // if the bufferedModel is dirty we should give the user a hint
        if (detailsModel.isBuffering()) {
            int result =
                JOptionPane.showConfirmDialog(JOptionPane.getFrameForComponent(bottomDetailsPanel),
                    Resources.getString(SceneryPanel.class, "dialog.pending_changes"),
                    Resources.getString(SceneryPanel.class, "dialog.warn"), JOptionPane.YES_NO_OPTION,
                    JOptionPane.WARNING_MESSAGE);

            if (result == JOptionPane.YES_OPTION) {
                LOGGER.info("Save the scenery");
                detailsModel.triggerCommit();
            }
        }

        // transform scenery to a macro
        DmxScenery dmxScenery = sceneryList.getSelectedValue();
        if (dmxScenery == null || dmxScenery.getSceneryPoints().size() == 0) {
            JOptionPane.showMessageDialog(JOptionPane.getFrameForComponent(bottomDetailsPanel),
                Resources.getString(SceneryPanel.class, "dialog.no_scenery_or_points"),
                Resources.getString(SceneryPanel.class, "dialog.export_error"), JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (dmxScenery.getMacroNumber() == null) {
            JOptionPane.showMessageDialog(JOptionPane.getFrameForComponent(bottomDetailsPanel),
                Resources.getString(SceneryPanel.class, "dialog.no_scenery_selected"),
                Resources.getString(SceneryPanel.class, "dialog.export_error"), JOptionPane.ERROR_MESSAGE);
            return;
        }

        LOGGER.info("Transform selected scenery: {}", dmxScenery);

        Macro selectedMacro = null;
        for (Macro currentMacro : dmxSceneryModel.getMacros()) {
            if (currentMacro == null) {
                // the first is null ...
                continue;
            }
            if (currentMacro.getId() == dmxScenery.getMacroNumber()) {
                selectedMacro = currentMacro;
                break;
            }
        }

        if (selectedMacro == null) {
            // no macro is selected
            JOptionPane.showMessageDialog(JOptionPane.getFrameForComponent(bottomDetailsPanel),
                Resources.getString(SceneryPanel.class, "dialog.selected_macro_not_available",
                    new Object[] { dmxScenery.getMacroNumber() }), Resources.getString(SceneryPanel.class,
                    "dialog.export_error"), JOptionPane.ERROR_MESSAGE);
            return;
        }

        // initialize the selected macro
        selectedMacro.initialize();

        // sort the points by timeOffset
        Collections.sort(dmxScenery.getSceneryPoints(), new Comparator<DmxSceneryPoint>() {
            @Override
            public int compare(DmxSceneryPoint p1, DmxSceneryPoint p2) {
                return p1.getTimeOffset() - p2.getTimeOffset();
            }
        });

        // int index = 0;
        int currentDelay = 0;
        int prevTimeOffset = 0;

        List<String> pointsWithErrors = new ArrayList<>();
        String errorToAdd = null;
        // iterate over the scenery points to find the delays between the points
        List<Function<?>> functions = new ArrayList<Function<?>>();
        TreeSet<Integer> delaySet = new TreeSet<>();

        DmxSceneryPoint prevDmxSceneryPoint = null;
        for (DmxSceneryPoint dmxSceneryPoint : dmxScenery.getSceneryPoints()) {
            LOGGER.debug("Current point: {}", dmxSceneryPoint);

            if (prevDmxSceneryPoint != null && prevDmxSceneryPoint.getPort() != null) {

                if (prevDmxSceneryPoint.getPort() instanceof DmxLightPort) {
                    // the previous point was a lightport
                    if (prevDmxSceneryPoint.getAction() == null
                        || prevDmxSceneryPoint.getAction().equals(LightPortStatus.UP)
                        || prevDmxSceneryPoint.getAction().equals(LightPortStatus.DOWN)) {

                        LOGGER.info("Adjust the action for lightport: {}", prevDmxSceneryPoint.getPort());

                        if (dmxSceneryPoint.getBrightness() >= prevDmxSceneryPoint.getBrightness()) {
                            // we must dimm up
                            prevDmxSceneryPoint.setAction(LightPortStatus.UP);
                        }
                        else {
                            // we must dimm down
                            prevDmxSceneryPoint.setAction(LightPortStatus.DOWN);
                        }

                    }
                }
                else if (prevDmxSceneryPoint.getPort() instanceof BacklightPort) {
                    // the previous point was a backlight port
                    if (prevDmxSceneryPoint.getAction() == null) {
                        prevDmxSceneryPoint.setAction(BacklightPortStatus.START);
                    }
                }
            }

            prevDmxSceneryPoint = dmxSceneryPoint;
        }

        int currentIndex = 0;
        for (DmxSceneryPoint dmxSceneryPoint : dmxScenery.getSceneryPoints()) {
            LOGGER.debug("Current point: {}", dmxSceneryPoint);

            // the last point does not need to have a port or macro assigned
            // and this is the way to not get adding the error for the last point
            if (errorToAdd != null) {
                pointsWithErrors.add(errorToAdd);
                errorToAdd = null;
            }

            if (dmxSceneryPoint.getPort() != null && dmxSceneryPoint.getAction() == null) {

                if (dmxSceneryPoint.getPort() instanceof BacklightPort
                    && currentIndex == dmxScenery.getSceneryPoints().size() - 1) {
                    LOGGER
                        .info("The last point has a backlight port assigned but no action. Set the start action and ignore this error.");
                    dmxSceneryPoint.setAction(BacklightPortStatus.START);
                }
                else {
                    LOGGER.warn("The scenery point has no action assigned. Remove port or set an action, point: {}",
                        dmxSceneryPoint);
                    pointsWithErrors.add(Resources.getString(SceneryPanel.class, "error-port-without-action",
                        new Object[] { dmxSceneryPoint.getTimeOffset(), dmxSceneryPoint.getDmxChannelNumber() }));
                }
            }
            else if (dmxSceneryPoint.getMacro() == null && dmxSceneryPoint.getPort() == null) {
                LOGGER.warn("The scenery point has no port and no macro assigned: {}", dmxSceneryPoint);
                errorToAdd =
                    Resources.getString(SceneryPanel.class, "error-port-without-port-or-macro", new Object[] {
                        dmxSceneryPoint.getTimeOffset(), dmxSceneryPoint.getDmxChannelNumber() });
            }

            int timeOffset = dmxSceneryPoint.getTimeOffset();
            if (timeOffset > 0) {
                // get the delay between the current point and the previous point
                currentDelay = timeOffset - prevTimeOffset;
                // and add the delay to the set
                LOGGER.info("Add a delay: {}", currentDelay);

                delaySet.add(currentDelay);
            }
            prevTimeOffset = timeOffset;

            currentIndex++;
        }

        if (CollectionUtils.isNotEmpty(pointsWithErrors)) {
            StringBuffer sb = new StringBuffer(Resources.getString(SceneryPanel.class, "message.errors_detected"));
            for (String errorMessage : pointsWithErrors) {
                sb.append(errorMessage).append("\n");
            }

            JOptionPane.showMessageDialog(JOptionPane.getFrameForComponent(bottomDetailsPanel), sb.toString(),
                Resources.getString(SceneryPanel.class, "dialog.export_error"), JOptionPane.ERROR_MESSAGE);
            return;
        }

        Integer minDelay = delaySet.first();
        Integer maxDelay = delaySet.last();

        int slowdown = SceneryUtils.calculateSlowdown(delaySet);
        LOGGER.info("minDelay: {}, maxDelay: {}, slowdown: {}", minDelay, maxDelay, slowdown);

        currentDelay = 0;
        prevTimeOffset = 0;
        int delay = 0;

        // we must try to guess the actions if the action is missing on a port point
        // check the next point and use dimmUp or dimmDown as default actions

        for (final DmxSceneryPoint dmxSceneryPoint : dmxScenery.getSceneryPoints()) {
            LOGGER.info("Current point: {}", dmxSceneryPoint);

            int timeOffset = dmxSceneryPoint.getTimeOffset();
            if (timeOffset > 0) {
                currentDelay = timeOffset - prevTimeOffset;
                // add a delay
                LOGGER.info("Current timeoffset: {}, prevTimeOffset: {}, current delay: {}, slowdown: {}", timeOffset,
                    prevTimeOffset, currentDelay, slowdown);

                delay = currentDelay / (slowdown * 20 /* ms */);

                int remaining = currentDelay % (slowdown * 20 /* ms */);
                LOGGER.info("Current delay: {}, calculated delay: {}, remaining: {}", currentDelay, delay, remaining);

                while (delay > 255) {
                    LOGGER.info("Add a current delay: {}, with slowdown corrected: {}", currentDelay, delay);

                    // create all functions and set them later to the macro
                    functions.add(DelayFunction.DelayFunctionBuilder
                        .delayFunction().withDelay((delay > 255 ? 255 : delay)).build());

                    delay = delay - 255;
                }
            }
            prevTimeOffset = timeOffset;

            // check if a port is assigned
            if (dmxSceneryPoint.getPort() != null) {
                Port<?> port = dmxSceneryPoint.getPort();
                LOGGER.info("Current port: {}, remaining delay: {}, brightness: {}", port, delay,
                    dmxSceneryPoint.getBrightness());

                if (port instanceof DmxLightPort) {
                    // TODO the lightport don't have a target brightness ... check how to handle this ...
                    DmxLightPort dmxLightPort = (DmxLightPort) port;

                    LightPortAction action =
                        LightPortAction.LightPortActionBuilder
                            .lightPortAction().withAction(dmxSceneryPoint.getAction()).withDelay(delay)
                            .withPort(dmxLightPort.getLightPort()).build();
                    functions.add(action);
                }
                else if (port instanceof BacklightPort) {
                    // the target brightness is the brightness of the next point in the series

                    List<DmxSceneryPoint> seriesPoints =
                        (List<DmxSceneryPoint>) CollectionUtils.select(dmxScenery.getSceneryPoints(),
                            new Predicate<DmxSceneryPoint>() {

                                @Override
                                public boolean evaluate(DmxSceneryPoint point) {
                                    return point.getDmxChannelNumber() == dmxSceneryPoint.getDmxChannelNumber()
                                        && point.getTimeOffset() >= dmxSceneryPoint.getTimeOffset()
                                        && point != dmxSceneryPoint;
                                }

                            });

                    Collections.sort(seriesPoints, new Comparator<DmxSceneryPoint>() {
                        @Override
                        public int compare(DmxSceneryPoint p1, DmxSceneryPoint p2) {
                            return p1.getTimeOffset() - p2.getTimeOffset();
                        }
                    });

                    int targetBrightness = 100;
                    if (seriesPoints.size() > 0) {
                        DmxSceneryPoint nextDmxSceneryPoint = seriesPoints.get(0);
                        targetBrightness = nextDmxSceneryPoint.getBrightness();
                    }
                    else {
                        LOGGER.info("No further scenery points available, use the brightness of the current point.");
                        targetBrightness = dmxSceneryPoint.getBrightness();
                    }

                    BacklightPortAction action =
                        BacklightPortAction.BacklightPortActionBuilder
                            .backlightPortAction().withAction(BacklightPortStatus.START).withDelay(delay)
                            .withPort((BacklightPort) port).withBrightness(targetBrightness).build();
                    functions.add(action);
                }

            }
            // check if a macro is assigned
            else if (dmxSceneryPoint.getMacro() != null) {
                MacroFunction macro = dmxSceneryPoint.getMacro();
                LOGGER.info("Current point contains a macro: {}", macro);

                functions.add(macro);
            }

        }
        selectedMacro.setSpeed(slowdown);
        selectedMacro.setFunctions(functions);

    }

    private void potentiallyShowPopup(MouseEvent e) {
        JPopupMenu popupMenu = null;

        if (e.isPopupTrigger()) {
            TreePath selPath = usedChannelsTree.getPathForLocation(e.getX(), e.getY());

            // If we clicked on a node...
            if (selPath != null) {
                // ...make sure we also add it to the selection
                usedChannelsTree.getSelectionModel().addSelectionPath(selPath);
            }

            Object selectedComponent = null;
            if (usedChannelsTree.getSelectionModel().getLeadSelectionPath() != null) {
                selectedComponent = usedChannelsTree.getSelectionModel().getLeadSelectionPath().getLastPathComponent();
            }
            popupMenu = buildPopup(selectedComponent);

            // Only show popup if we have any menu items in it
            if (popupMenu.getComponentCount() > 0)
                popupMenu.show((Component) e.getSource(), e.getX(), e.getY());
        }
    }

    private JPopupMenu buildPopup(final Object selectedComponent) {
        JPopupMenu pm = new JPopupMenu();
        // Based on selection context, install actions...
        if (selectedComponent instanceof DmxChannelNode) {
            LOGGER.info("A DmxChannelNode is selected: {}", selectedComponent);
            // add remove menu item
            JMenuItem removeChannel =
                new JMenuItem(new RemoveChannelAction(Resources.getString(getClass(), "menu.remove-dmx-channel"),
                    (DmxChannelNode) selectedComponent));

            if (detailsModel.isBuffering()) {
                // buffering values must be saved first
                removeChannel.setEnabled(false);
            }
            pm.add(removeChannel);

            pm.add(new AbstractAction(
                Resources.getString(SceneryPanel.class, "menu.add-port")) {
                private static final long serialVersionUID = 1L;

                @Override
                public void actionPerformed(ActionEvent e) {
                    DmxScenery dmxScenery = sceneryList.getSelectedValue();
                    if (dmxScenery != null) {
                        LOGGER.info("Add port to selected scenery: {}", dmxScenery);

                        // add DMX channel
                        if (CollectionUtils.isNotEmpty(dmxSceneryModel.getDmxChannels())) {

                            DmxChannelNode dmxChannelNode = (DmxChannelNode) selectedComponent;
                            DmxChannel selectedDmxChannel = dmxChannelNode.getDmxChannel();
                            int selectedDmxChannelId = selectedDmxChannel.getChannelId();

                            // prepare a copy of the list with all ports assigned to the selected DMX channel
                            List<Port<?>> availablePorts = new LinkedList<>();
                            for (DmxLightPort dmxLightPort : dmxSceneryModel.getLightPorts()) {
                                if (dmxLightPort.getDmxTargetChannel() != null
                                    && dmxLightPort.getDmxTargetChannel().getChannelId() == selectedDmxChannelId) {
                                    availablePorts.add(dmxLightPort);
                                }
                            }
                            for (BacklightPort backlightPort : dmxSceneryModel.getBacklightPorts()) {
                                if (backlightPort.getDmxMapping() == selectedDmxChannelId) {
                                    availablePorts.add(backlightPort);
                                }
                            }
                            // remove the already assigned ports
                            availablePorts.removeAll(selectedDmxChannel.getAssignedPorts());

                            // show a dialog to select the new ports
                            PortSelectionPanel dmxPortSelectionPanel = new PortSelectionPanel();

                            List<Port<?>> selectedDmxPorts =
                                dmxPortSelectionPanel.showDialog(JOptionPane.getFrameForComponent(sceneryList),
                                    availablePorts);

                            if (CollectionUtils.isNotEmpty(selectedDmxPorts)) {
                                LOGGER.info("Add the new DMX ports.");

                                selectedDmxChannel.addAssignedPorts(selectedDmxPorts);

                                // reload the ports of the selected DMX channel in the tree
                                refreshPortsOfDmxChannel(selectedDmxChannel);
                            }
                        }
                    }
                }
            });
            pm.add(new AbstractAction(
                Resources.getString(SceneryPanel.class, "menu.change-color")) {
                private static final long serialVersionUID = 1L;

                @Override
                public void actionPerformed(ActionEvent e) {
                    DmxScenery dmxScenery = sceneryList.getSelectedValue();
                    if (dmxScenery != null) {
                        LOGGER.info("Change color of selected scenery: {}", dmxScenery);

                        DmxChannelNode dmxChannelNode = (DmxChannelNode) selectedComponent;
                        DmxChannel selectedDmxChannel = dmxChannelNode.getDmxChannel();

                        Color initialColor = selectedDmxChannel.getLineColor();
                        Color newLineColor =
                            JColorChooser.showDialog(bottomDetailsPanel, "Choose line color", initialColor);

                        if (newLineColor != null) {
                            selectedDmxChannel.setLineColor(newLineColor);
                        }
                    }
                }
            });
            pm.addSeparator();
            pm.add(new AbstractAction(
                Resources.getString(SceneryPanel.class, "menu.add-dmx-channel")) {
                private static final long serialVersionUID = 1L;

                @Override
                public void actionPerformed(ActionEvent e) {
                    DmxScenery dmxScenery = sceneryList.getSelectedValue();
                    if (dmxScenery != null) {
                        LOGGER.info("Add DMX channel to selected scenery: {}", dmxScenery);

                        // add DMX channel
                        if (CollectionUtils.isNotEmpty(dmxSceneryModel.getDmxChannels())) {

                            // show a dialog to select the new channel
                            DmxChannelSelectionPanel dmxChannelSelectionPanel = new DmxChannelSelectionPanel();

                            // prepare a copy of the list with all dmx channels
                            List<DmxChannel> availableChannels = new LinkedList<>(dmxSceneryModel.getDmxChannels());
                            // remove the already assigned channels
                            availableChannels.removeAll(dmxScenery.getUsedChannels());

                            List<DmxChannel> selectedDmxChannels =
                                dmxChannelSelectionPanel.showDialog(JOptionPane.getFrameForComponent(sceneryList),
                                    availableChannels);

                            if (CollectionUtils.isNotEmpty(selectedDmxChannels)) {
                                LOGGER.info("Add the new DMX channels.");

                                dmxScenery.addUsedChannels(selectedDmxChannels);
                            }
                        }
                    }
                }
            });
        }
        else if (selectedComponent instanceof PortNode) {
            LOGGER.info("A PortNode is selected: {}", selectedComponent);
            final PortNode portNode = (PortNode) selectedComponent;

            pm.add(new AbstractAction(
                Resources.getString(SceneryPanel.class, "menu.remove-port")) {
                private static final long serialVersionUID = 1L;

                @Override
                public void actionPerformed(ActionEvent e) {

                    // check if the port is assigned in a scenery and show a hint to the user
                    for (DmxScenery dmxScenery : dmxSceneryModel.getSceneries()) {

                        for (DmxSceneryPoint dmxSceneryPoint : dmxScenery.getSceneryPoints()) {

                            if (portNode.getPort().equals(dmxSceneryPoint.getPort())) {
                                // the port is assigned
                                LOGGER.info("The port is still assigned in the scenery: {}", dmxScenery);
                                JOptionPane.showMessageDialog(JOptionPane.getFrameForComponent(bottomDetailsPanel),
                                    Resources.getString(SceneryPanel.class, "dialog.remove-port.message",
                                        new Object[] { dmxScenery }), Resources.getString(SceneryPanel.class,
                                        "dialog.remove-port.title"), JOptionPane.ERROR_MESSAGE);
                                return;
                            }
                        }

                    }

                    TreeNode parentNode = portNode.getParent();
                    if (parentNode instanceof DmxChannelNode) {
                        DmxChannelNode dmxChannelNode = (DmxChannelNode) parentNode;

                        DmxChannel selectedDmxChannel = dmxChannelNode.getDmxChannel();
                        // remove the current port
                        selectedDmxChannel.getAssignedPorts().remove(portNode.getPort());

                        // reload the ports of the selected DMX channel in the tree
                        refreshPortsOfDmxChannel(selectedDmxChannel);
                    }
                }
            });

        }
        else if (selectedComponent == null && detailsModel.getBean() != null) {
            // if a scenery is selected then show the add DMX channels menu item
            pm.add(new AbstractAction(
                Resources.getString(SceneryPanel.class, "menu.add-dmx-channel")) {
                private static final long serialVersionUID = 1L;

                @Override
                public void actionPerformed(ActionEvent e) {
                    DmxScenery dmxScenery = sceneryList.getSelectedValue();
                    if (dmxScenery != null) {
                        LOGGER.info("Add DMX channel to selected scenery: {}", dmxScenery);

                        // add DMX channel
                        if (CollectionUtils.isNotEmpty(dmxSceneryModel.getDmxChannels())) {

                            // show a dialog to select the new channel
                            DmxChannelSelectionPanel dmxChannelSelectionPanel = new DmxChannelSelectionPanel();

                            // prepare a copy of the list with all dmx channels
                            List<DmxChannel> availableChannels = new LinkedList<>(dmxSceneryModel.getDmxChannels());
                            // remove the already assigned channels
                            availableChannels.removeAll(dmxScenery.getUsedChannels());

                            List<DmxChannel> selectedDmxChannels =
                                dmxChannelSelectionPanel.showDialog(JOptionPane.getFrameForComponent(sceneryList),
                                    availableChannels);

                            if (CollectionUtils.isNotEmpty(selectedDmxChannels)) {
                                LOGGER.info("Add the new DMX channels.");

                                dmxScenery.addUsedChannels(selectedDmxChannels);
                            }
                        }
                    }
                }
            });
        }

        return pm;
    }

    private JComponent buildButtonBar() {
        return new ButtonBarBuilder().addGlue().addButton(applyButton, resetButton).build();
    }

    private ValidationResult validate() {
        PropertyValidationSupport support = new PropertyValidationI18NSupport(detailsModel, "validation");

        // add some validation steps
        LOGGER.info("Current name: '{}'", sceneryNameModel.getString());

        if (StringUtils.isBlank(sceneryNameModel.getString())) {
            support.addError("sceneryname_key", "not_empty");
        }

        ValidationResult validationResult = support.getResult();
        LOGGER.info("Prepared validationResult: {}", validationResult);
        return validationResult;
    }

    protected void triggerValidation() {
        if (detailsModel.getBean() == null) {
            // no detail bean available
            validationResultModel.setResult(new ValidationResult());
        }
        else {
            ValidationResult validationResult = validate();
            validationResultModel.setResult(validationResult);

            // enable or disable the buttons
            LOGGER.debug("Set the writeButtonEnabled, validStateNoWarnOrErrors: {}, validState: {}",
                validationResultModel.getValidStateNoWarnOrErrors(), validationResultModel.getValidState());
        }
        // apply button must be enabled if valid validation and buffering
        applyButtonEnabled.setModelValid(validationResultModel.getValidStateNoWarnOrErrors());
    }

    protected ValidationResultModel getValidationResultModel() {
        return validationResultModel;
    }

    private void updateView(DmxScenery dmxScenery) {
        LOGGER.info("Set the dmxScenery: {}", dmxScenery);

        if (detailsModel.isBuffering()) {
            LOGGER.warn("The details model is buffering.");
        }
        detailsModel.triggerFlush();

        detailsModel.setBean(dmxScenery);

        detailsModel.resetChanged();
    }

    private void setUsedDmxChannels(List<DmxChannel> dmxChannels) {
        // clear the tree
        root.removeAllChildren();

        for (DmxChannel dmxChannel : dmxChannels) {
            LOGGER.info("Add new dmxChannel as node: {}", dmxChannel);

            DmxChannelNode channelNode = new DmxChannelNode(dmxChannel);
            // find the place to insert
            boolean inserted = false;
            for (int index = 0; index < root.getChildCount(); index++) {
                DmxChannelNode childNode = (DmxChannelNode) root.getChildAt(index);
                DmxChannel currentChannel = childNode.getDmxChannel();
                if (currentChannel.getChannelId() > dmxChannel.getChannelId()) {
                    // must insert node here
                    LOGGER.info("Found index to insert new node: {}", index);
                    root.insert(channelNode, index);
                    inserted = true;
                    break;
                }
            }
            if (!inserted) {
                // add to last position
                root.add(channelNode);
            }

            if (CollectionUtils.isNotEmpty(dmxChannel.getAssignedPorts())) {
                // prepare port nodes
                List<Port<?>> sortedPorts = new LinkedList<>(dmxChannel.getAssignedPorts());
                Collections.sort(sortedPorts, new Comparator<Port<?>>() {

                    @Override
                    public int compare(Port<?> port1, Port<?> port2) {
                        return port1.toString().compareTo(port2.toString());
                    }
                });
                for (Port<?> port : sortedPorts) {
                    LOGGER.info("Add new port as node: {}", port);

                    PortNode portNode = new PortNode(port);
                    channelNode.add(portNode);
                }
            }
        }

        // refresh the tree
        usedChannelsTreeModel.reload();
        expandAllNodes(usedChannelsTree, root, 2);
    }

    private void refreshPortsOfDmxChannel(DmxChannel dmxChannel) {
        LOGGER.info("Refresh ports of DMX channel: {}", dmxChannel);

        DmxChannelNode nodeToRefresh = null;
        for (int index = 0; index < root.getChildCount(); index++) {
            DmxChannelNode childNode = (DmxChannelNode) root.getChildAt(index);
            DmxChannel currentChannel = childNode.getDmxChannel();
            LOGGER.debug("Check current dmx channel: {}", currentChannel);
            if (currentChannel.getChannelId() == dmxChannel.getChannelId()) {
                // found node to refresh
                LOGGER.info("Found node to refresh: {}", childNode);
                nodeToRefresh = childNode;

                childNode.removeAllChildren();

                if (CollectionUtils.isNotEmpty(dmxChannel.getAssignedPorts())) {
                    // prepare port nodes
                    List<Port<?>> sortedPorts = new LinkedList<>(dmxChannel.getAssignedPorts());
                    Collections.sort(sortedPorts, new Comparator<Port<?>>() {

                        @Override
                        public int compare(Port<?> port1, Port<?> port2) {
                            return port1.toString().compareTo(port2.toString());
                        }
                    });
                    for (Port<?> port : sortedPorts) {
                        LOGGER.info("Add new port as node: {}", port);

                        PortNode portNode = new PortNode(port);
                        childNode.add(portNode);
                    }
                }
                break;
            }
        }
        // refresh the tree
        usedChannelsTreeModel.reload(nodeToRefresh);
        expandNode(usedChannelsTree, nodeToRefresh);
    }

    private class RemoveChannelAction extends AbstractAction {
        private static final long serialVersionUID = 1L;

        private DmxChannelNode dmxChannelNode;

        public RemoveChannelAction(String name, final DmxChannelNode dmxChannelNode) {
            super(name);
            this.dmxChannelNode = dmxChannelNode;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            LOGGER.info("Remove dmxChannelNode from tree: {}", dmxChannelNode);

            // TODO check if changing to used the buffered model is better?
            // user could remove channels and then commit the changes

            // remove the channel from the bean saved in the details model, binding handles the update of the tree
            DmxScenery dmxScenery = detailsModel.getBean();
            dmxScenery.removeUsedChannel(dmxChannelNode.getDmxChannel());
        }
    }

    // Actions ****************************************************************

    /**
     * Commits the Trigger used to buffer the editor contents.
     */
    private final class ApplyAction extends AbstractAction {
        private static final long serialVersionUID = 1L;

        private ApplyAction() {
            super(Resources.getString(SceneryPanel.class, "apply"));
        }

        public void actionPerformed(ActionEvent e) {
            detailsModel.triggerCommit();

            // update the scenery list
            scenerySelection.fireSelectedContentsChanged();
        }
    }

    /**
     * Flushed the Trigger used to buffer the editor contents.
     */
    private final class ResetAction extends AbstractAction {
        private static final long serialVersionUID = 1L;

        private ResetAction() {
            super(Resources.getString(SceneryPanel.class, "reset"));
        }

        public void actionPerformed(ActionEvent e) {
            detailsModel.triggerFlush();
        }
    }

    private final class PopupListener extends MouseAdapter {
        public void mousePressed(MouseEvent e) {
            maybeShowPopup(e);
        }

        public void mouseReleased(MouseEvent e) {
            maybeShowPopup(e);
        }

        private void maybeShowPopup(MouseEvent e) {
            if (e.isPopupTrigger()) {

                sceneryList.setSelectedIndex(sceneryList.locationToIndex(e.getPoint())); // select the item
                boolean scenerySelected = sceneryList.getSelectedIndex() > -1;
                openViewer.setEnabled(scenerySelected);
                addDmxChannel.setEnabled(scenerySelected);
                sceneryMenu.show(sceneryList, e.getX(), e.getY()); // and show the menu
            }
        }
    }
}
