package org.bidib.wizard.mvc.script.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.filechooser.FileFilter;

import org.apache.commons.io.FilenameUtils;
import org.apache.velocity.context.Context;
import org.bidib.wizard.common.context.ApplicationContext;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.dialog.FileDialog;
import org.bidib.wizard.mvc.script.controller.listener.ExecuteScriptListener;
import org.bidib.wizard.mvc.script.model.NodeScriptModel;
import org.bidib.wizard.mvc.script.view.input.InputParametersDialog;
import org.bidib.wizard.mvc.script.view.utils.NodeScriptUtils;
import org.fife.ui.autocomplete.AutoCompletion;
import org.fife.ui.autocomplete.BasicCompletion;
import org.fife.ui.autocomplete.CompletionProvider;
import org.fife.ui.autocomplete.DefaultCompletionProvider;
import org.fife.ui.autocomplete.ShorthandCompletion;
import org.fife.ui.rsyntaxtextarea.AbstractTokenMakerFactory;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.TokenMakerFactory;
import org.fife.ui.rtextarea.RTextScrollPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.value.ConverterValueModel;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.builder.ButtonBarBuilder;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.debug.FormDebugPanel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.FormLayout;
import com.vlsolutions.swing.docking.DockKey;
import com.vlsolutions.swing.docking.Dockable;

public class NodeScriptView implements Dockable {
    private static final Logger LOGGER = LoggerFactory.getLogger(NodeScriptView.class);

    public static final String NODE_SCRIPT_VIEW = "NodeScriptView";

    private static final String SUFFIX_NODESCRIPT = "nodescript";

    public final DockKey DOCKKEY = new DockKey(NODE_SCRIPT_VIEW);

    private final JPanel contentPanel;

    private static final String ENCODED_DIALOG_COLUMN_SPECS = "pref, 3dlu, fill:50dlu:grow, 3dlu, pref";

    private ValueModel selectedFileValueModel;

    private ValueModel selectedNodeValueModel;

    private final JButton selectNodeScriptButton = new JButton(Resources.getString(getClass(), "select-nodescript"));

    private final JButton saveNodeScriptButton = new JButton(Resources.getString(getClass(), "save-nodescript"));

    private final JButton applyOnNodeButton = new JButton(Resources.getString(getClass(), "apply-on-node"));

    private final JButton previewScriptButton = new JButton(Resources.getString(getClass(), "preview-script"));

    private RSyntaxTextArea textArea;

    private final NodeScriptModel nodeScriptModel;

    private ExecuteScriptListener executeScriptListener;

    private final FileFilter ff = new FileFilter() {

        @Override
        public boolean accept(File file) {
            boolean result = false;

            if (file != null) {
                if (file.isDirectory()) {
                    result = true;
                }
                else if (FilenameUtils.wildcardMatch(file.getName(), "*." + SUFFIX_NODESCRIPT)) {
                    result = true;
                }
            }
            return result;
        }

        @Override
        public String getDescription() {
            return Resources.getString(NodeScriptView.class, "filter") + " (*." + SUFFIX_NODESCRIPT + ")";
        }
    };

    /**
     * @param nodeScriptModel
     *            the nodeScript model
     */
    public NodeScriptView(final NodeScriptModel nodeScriptModel) {
        this.nodeScriptModel = nodeScriptModel;

        DOCKKEY.setName(Resources.getString(getClass(), "title"));
        DOCKKEY.setFloatEnabled(true);
        DOCKKEY.setAutoHideEnabled(false);

        LOGGER.info("Create new NodeScriptView");

        DefaultFormBuilder dialogBuilder = null;
        boolean debugDialog = false;
        if (debugDialog) {
            JPanel panel = new FormDebugPanel();
            dialogBuilder = new DefaultFormBuilder(new FormLayout(ENCODED_DIALOG_COLUMN_SPECS), panel);
        }
        else {
            JPanel panel = new JPanel(new BorderLayout());
            dialogBuilder = new DefaultFormBuilder(new FormLayout(ENCODED_DIALOG_COLUMN_SPECS), panel);
        }
        dialogBuilder.border(Borders.DIALOG);

        selectedFileValueModel =
            new PropertyAdapter<NodeScriptModel>(nodeScriptModel, NodeScriptModel.PROPERTYNAME_NODE_SCRIPT_NAME, true);
        JTextField selectedFileText = BasicComponentFactory.createTextField(selectedFileValueModel, true);
        selectedFileText.setEditable(false);
        dialogBuilder.append(Resources.getString(getClass(), "scriptFile"), selectedFileText);

        // prepare the select and save button
        JPanel nodeScriptActionButtons =
            new ButtonBarBuilder().addButton(selectNodeScriptButton).addGlue().addButton(saveNodeScriptButton).build();
        dialogBuilder.append(nodeScriptActionButtons);

        selectNodeScriptButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                selectNodeScript();
            }
        });

        // saveNodeScriptButton.setMnemonic(KeyEvent.VK_S);
        saveNodeScriptButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                saveNodeScript();
            }
        });

        textArea = new RSyntaxTextArea(20, 60);

        AbstractTokenMakerFactory atmf = (AbstractTokenMakerFactory) TokenMakerFactory.getDefaultInstance();
        atmf.putMapping("text/bidibscript", "org.bidib.wizard.mvc.script.view.modes.BidibTokenMaker");

        textArea.setSyntaxEditingStyle("text/bidibscript");

        // textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVASCRIPT);
        textArea.setCodeFoldingEnabled(true);

        // A CompletionProvider is what knows of all possible completions, and
        // analyzes the contents of the text area at the caret position to
        // determine what completion choices should be presented. Most instances
        // of CompletionProvider (such as DefaultCompletionProvider) are designed
        // so that they can be shared among multiple text components.
        CompletionProvider provider = createCompletionProvider();

        // An AutoCompletion acts as a "middle-man" between a text component
        // and a CompletionProvider. It manages any options associated with
        // the auto-completion (the popup trigger key, whether to display a
        // documentation window along with completion choices, etc.). Unlike
        // CompletionProviders, instances of AutoCompletion cannot be shared
        // among multiple text components.
        AutoCompletion ac = new AutoCompletion(provider);
        ac.install(textArea);

        RTextScrollPane sp = new RTextScrollPane(textArea);

        dialogBuilder.appendRow("3dlu");
        dialogBuilder.appendRow("fill:300px:grow");
        dialogBuilder.nextLine(2);

        dialogBuilder.append(sp, 5);

        dialogBuilder.appendRow("3dlu");
        dialogBuilder.appendRow("p");
        dialogBuilder.nextLine(2);

        selectedNodeValueModel =
            new PropertyAdapter<NodeScriptModel>(nodeScriptModel, NodeScriptModel.PROPERTYNAME_SELECTED_NODE, true);
        ValueModel valueConverterModel = new ConverterValueModel(selectedNodeValueModel, new NodeConverter());

        JTextField selectedNodeText = BasicComponentFactory.createTextField(valueConverterModel, true);
        selectedFileText.setEditable(false);
        dialogBuilder.append(Resources.getString(getClass(), "selectedNode"), selectedNodeText);

        // prepare the apply on node button
        applyOnNodeButton.setEnabled(false);

        // applyOnNodeButton.setMnemonic(KeyEvent.VK_A);
        applyOnNodeButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                applyOnNode();
            }
        });

        // prepare the preview script button
        previewScriptButton.setEnabled(false);
        JPanel nodeActionButtons = new ButtonBarBuilder().addButton(previewScriptButton, applyOnNodeButton).build();
        dialogBuilder.append(nodeActionButtons);

        // previewScriptButton.setMnemonic(KeyEvent.VK_A);
        previewScriptButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                previewScript();
            }
        });

        // create the content panel
        contentPanel = new JPanel();
        contentPanel.setLayout(new BorderLayout());

        JScrollPane scrollPane = new JScrollPane(dialogBuilder.build());
        contentPanel.add(scrollPane, BorderLayout.CENTER);

        // add bindings for enable/disable the write button
        PropertyConnector.connect(nodeScriptModel, NodeScriptModel.PROPERTYNAME_CAN_EXECUTE_SCRIPT, applyOnNodeButton,
            "enabled");
        PropertyConnector.connect(nodeScriptModel, NodeScriptModel.PROPERTYNAME_CAN_EXECUTE_SCRIPT, previewScriptButton,
            "enabled");

        final Action selectNodeScriptAction = new AbstractAction() {
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {
                LOGGER.warn("Abstract selectNodeScriptAction was performed.");
                selectNodeScript();
            }
        };

        contentPanel.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(
            KeyStroke.getKeyStroke(KeyEvent.VK_O, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()),
            "selectNodeScript");
        contentPanel.getActionMap().put("selectNodeScript", selectNodeScriptAction);

        // TODO register keys
        // KeyStroke OPEN = KeyStroke.getKeyStroke(KeyEvent.VK_O, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask());
        // getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(OPEN, "OPEN");
        // getRootPane().getActionMap().put("OPEN", selectNodeScriptAction);
        // table.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(OPEN, "OPEN");
        // table.getActionMap().put("OPEN", selectNodeScriptAction);

    }

    /**
     * @param executeScriptListener
     *            the executeScriptListener to set
     */
    public void setExecuteScriptListener(ExecuteScriptListener executeScriptListener) {
        this.executeScriptListener = executeScriptListener;
    }

    @Override
    public DockKey getDockKey() {
        return DOCKKEY;
    }

    @Override
    public Component getComponent() {
        return contentPanel;
    }

    private void selectNodeScript() {

        final FileDialog dialog = new FileDialog(contentPanel, FileDialog.OPEN, null, ff) {
            @Override
            public void approve(String selectedFile) {
                File file = new File(selectedFile);

                selectedFile = file.getName();

                try {
                    nodeScriptModel.setNodeScriptName(null);
                    loadScript(file);
                    nodeScriptModel.setNodeScriptName(file.toString());
                }
                catch (IOException ex) {
                    LOGGER.warn("Load nodeScript from file failed.", ex);

                    JOptionPane.showMessageDialog(contentPanel,
                        Resources.getString(NodeScriptView.class, "load-nodescript-failed"),
                        Resources.getString(NodeScriptView.class, "load-nodescript"), JOptionPane.ERROR_MESSAGE);
                }
            }
        };
        dialog.showDialog();
    }

    private void loadScript(File nodeScript) throws IOException {
        LOGGER.info("Load script from file: {}", nodeScript);

        String encoding = NodeScriptUtils.detectFileEncoding(nodeScript);

        try (BufferedReader reader =
            new BufferedReader(new InputStreamReader(new FileInputStream(nodeScript), encoding))) {

            textArea.read(reader, null);
        }
        catch (FileNotFoundException ex) {
            LOGGER.warn("The selected nodescript file was not found.", ex);
        }
        catch (IOException ex) {
            LOGGER.warn("Get the content of the nodescript file failed.", ex);
        }

        textArea.setCaretPosition(0);
    }

    private void saveNodeScript() {

        final FileDialog dialog = new FileDialog(contentPanel, FileDialog.SAVE, null, ff) {
            @Override
            public void approve(String selectedFile) {

                if (!FilenameUtils.isExtension(selectedFile, SUFFIX_NODESCRIPT)) {
                    LOGGER.info("Add default extension 'nodescript'.");
                    selectedFile = selectedFile + "." + SUFFIX_NODESCRIPT;
                }

                File file = new File(selectedFile);

                selectedFile = file.getName();

                nodeScriptModel.setNodeScriptName(file.toString());

                try {
                    saveScript(file);
                }
                catch (IOException ex) {
                    LOGGER.warn("Save nodeScript to file failed.", ex);

                    JOptionPane.showMessageDialog(contentPanel,
                        Resources.getString(NodeScriptView.class, "save-nodescript-failed"),
                        Resources.getString(NodeScriptView.class, "save-nodescript"), JOptionPane.ERROR_MESSAGE);
                }
            }
        };
        dialog.showDialog();
    }

    private void saveScript(File nodeScript) throws IOException {
        LOGGER.info("Save script to file: {}", nodeScript);

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(nodeScript))) {

            textArea.write(writer);
        }

    }

    private void applyOnNode() {
        LOGGER.info("Apply the script on node.");

        if (executeScriptListener != null) {
            executeScriptListener.executeScript(textArea.getText(), false);
        }
    }

    private void previewScript() {
        LOGGER.info("Preview the script.");

        if (executeScriptListener != null) {
            executeScriptListener.executeScript(textArea.getText(), true);
        }
    }

    public void queryInputValues(
        final ApplicationContext context, final Context velocityContext, final List<String> inputLines,
        final List<String> instructionLines, final List<String> defineLines) {

        InputParametersDialog dialog = new InputParametersDialog();
        dialog.queryInputParameters(context, velocityContext, inputLines, instructionLines, defineLines);

    }

    /**
     * Create a simple provider that adds some Java-related completions.
     */
    private CompletionProvider createCompletionProvider() {

        // A DefaultCompletionProvider is the simplest concrete implementation
        // of CompletionProvider. This provider has no understanding of
        // language semantics. It simply checks the text entered up to the
        // caret position for a match against known completions. This is all
        // that is needed in the majority of cases.
        DefaultCompletionProvider provider = new DefaultCompletionProvider();

        // Add completions for all Java keywords. A BasicCompletion is just
        // a straightforward word completion.
        provider.addCompletion(new BasicCompletion(provider, "add"));
        provider.addCompletion(new BasicCompletion(provider, "config"));
        provider.addCompletion(new BasicCompletion(provider, "define("));
        provider.addCompletion(new BasicCompletion(provider, "input("));
        provider.addCompletion(new BasicCompletion(provider, "set("));
        // ... etc ...
        provider.addCompletion(new BasicCompletion(provider, "accessory"));
        provider.addCompletion(new BasicCompletion(provider, "aspect"));
        provider.addCompletion(new BasicCompletion(provider, "port"));
        provider.addCompletion(new BasicCompletion(provider, "macro"));
        provider.addCompletion(new BasicCompletion(provider, "backlight"));

        // Add a couple of "shorthand" completions. These completions don't
        // require the input text to be the same thing as the replacement text.
        provider.addCompletion(new ShorthandCompletion(provider, "prevent",
            "##input($prevent_replace_labels:boolean, text:de=\"Keine Namen f\u00fcr Accessory, Makros, Ports ersetzen\", text:en=\"Prevent replace labels for accessory, macro and ports\", default=false)",
            "Prevent replace labels"));
        // provider
        // .addCompletion(new ShorthandCompletion(provider, "syserr", "System.err.println(", "System.err.println("));

        return provider;

    }
}
