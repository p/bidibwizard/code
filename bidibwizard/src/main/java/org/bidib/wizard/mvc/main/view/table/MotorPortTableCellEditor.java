package org.bidib.wizard.mvc.main.view.table;

import java.awt.Component;

import javax.swing.JTable;

import org.bidib.wizard.mvc.main.model.MotorPort;
import org.bidib.wizard.mvc.main.model.MotorPortTableModel;
import org.bidib.wizard.utils.IntegerEditor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MotorPortTableCellEditor extends IntegerEditor {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(MotorPortTableCellEditor.class);

    public MotorPortTableCellEditor(int min, int max) {
        super(min, max);
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {

        // renderer only handles MotorPorts
        if (value instanceof MotorPort) {
            MotorPort motorPort = (MotorPort) value;

            switch (column) {
                case MotorPortTableModel.COLUMN_LABEL:
                    value = motorPort.toString();
                    break;
                default:
                    // should never happen
                    LOGGER.warn("Invalid renderer configuration detected, column: {}", column);
                    break;
            }
        }

        Component component = super.getTableCellEditorComponent(table, value, isSelected, row, column);

        return component;
    }

}
