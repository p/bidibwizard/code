package org.bidib.wizard.mvc.pom.view.command;

import org.bidib.jbidibc.core.AddressData;
import org.bidib.jbidibc.core.enumeration.PomOperation;
import org.bidib.wizard.mvc.pom.view.panel.DirectAccessProgBeanModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PomDirectAccessCommand extends PomOperationCommand<DirectAccessProgBeanModel> {
    private static final Logger LOGGER = LoggerFactory.getLogger(PomDirectAccessCommand.class);

    public PomDirectAccessCommand(AddressData decoderAddress, PomOperation pomOperation, int cvNumber, int cvValue) {
        super(decoderAddress, pomOperation, cvNumber, cvValue);
    }

    @Override
    public void postExecute(final DirectAccessProgBeanModel directAccessProgBeanModel) {
        super.postExecute(directAccessProgBeanModel);

        // TODO check if the address and CV number is correct
        // update the railcom config
        if (getCvValueResult() != null) {
            LOGGER.debug("Set the cvValueResult: {}", getCvValueResult());
            directAccessProgBeanModel.setCvValue(getCvValueResult());
        }
    }
}
