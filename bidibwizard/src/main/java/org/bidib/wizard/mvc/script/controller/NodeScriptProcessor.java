package org.bidib.wizard.mvc.script.controller;

import java.beans.PropertyChangeListener;

import org.bidib.wizard.common.context.ApplicationContext;

public interface NodeScriptProcessor {

    /**
     * Prepare the velocity context.
     * 
     * @param script
     *            the script content
     * @param scriptContext
     *            the script context
     * @return {@code true}: continue processing, {@code false}: abort processing
     */
    boolean prepareVelocityContext(String script, final ApplicationContext scriptContext);

    void substituteVariables(StringBuilder script, final ApplicationContext scriptContext);

    String renderScript(StringBuilder script, final ApplicationContext scriptContext);

    void parseAndApplyScript(String renderedScript, final ApplicationContext scriptContext);

    void addExecutionStatusListener(PropertyChangeListener executionStatusListener);

    void removeExecutionStatusListener(PropertyChangeListener executionStatusListener);
}
