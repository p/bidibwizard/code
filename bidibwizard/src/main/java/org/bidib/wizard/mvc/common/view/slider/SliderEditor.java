package org.bidib.wizard.mvc.common.view.slider;

import java.awt.Component;
import java.awt.Dimension;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTable;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.bidib.jbidibc.ui.LogarithmicJSlider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SliderEditor extends DefaultCellEditor {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(SliderEditor.class);

    protected JSlider slider;

    private JLabel sliderValue;

    private JPanel panel;

    private SliderValueChangeListener sliderValueChangeListener;

    public SliderEditor(int min, int max, int value) {
        this(min, max, value, false);
    }

    public SliderEditor(int min, int max, int value, boolean useLogarithmicSlider) {
        super(new JCheckBox());

        panel = new JPanel();

        JLabel tempLabel = new JLabel(Integer.toString(65535/* max */));
        final Dimension prefSize = tempLabel.getPreferredSize();
        sliderValue = new JLabel();
        sliderValue.setHorizontalAlignment(JLabel.RIGHT);
        sliderValue.setPreferredSize(prefSize);
        sliderValue.setMinimumSize(prefSize);

        if (!useLogarithmicSlider) {
            slider = new JSlider(min, max, value);
        }
        else {
            slider = new LogarithmicJSlider(min, max, value);
        }

        slider.setOpaque(false);
        slider.setMinimumSize(prefSize);

        slider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                JSlider source = (JSlider) e.getSource();

                updateSliderTextValue(source.getValue(), true);

                if (!source.getValueIsAdjusting()) {
                    stopCellEditing();
                    if (sliderValueChangeListener != null) {
                        // forward event
                        sliderValueChangeListener.stateChanged(e, false, source.getValue());
                    }
                }
                else {
                    if (sliderValueChangeListener != null) {
                        // forward event
                        sliderValueChangeListener.stateChanged(e, true, source.getValue());
                    }
                }
            }
        });

        // panel.add(slider, BorderLayout.CENTER);
        // panel.add(sliderValue, BorderLayout.EAST);
        panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
        panel.add(slider);
        panel.add(Box.createRigidArea(new Dimension(0, 5)));
        panel.add(sliderValue);
    }

    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        if (isSelected) {
            slider.setForeground(table.getSelectionForeground());
            slider.setBackground(table.getSelectionBackground());
        }
        else {
            slider.setForeground(table.getForeground());
            slider.setBackground(table.getBackground());
        }
        slider.setValue(((Integer) value).intValue());

        updateSliderTextValue(slider.getValue(), isSelected);

        slider.updateUI();

        return panel;
    }

    public Object getCellEditorValue() {
        return new Integer(slider.getValue());
    }

    public boolean stopCellEditing() {
        return super.stopCellEditing();
    }

    protected void fireEditingStopped() {
        super.fireEditingStopped();
    }

    public void setMaxValue(int maxValue) {
        if (slider != null) {
            slider.setMaximum(maxValue);
        }
    }

    public void setMinValue(int minValue) {
        if (slider != null) {
            slider.setMinimum(minValue);
        }
    }

    public void stepDown() {
        LOGGER.trace("set slider value down: {}", slider.getValue());
        slider.setValue(slider.getValue() - 1);
    }

    public void stepUp() {
        LOGGER.trace("set slider value up: {}", slider.getValue());
        slider.setValue(slider.getValue() + 1);
    }

    /**
     * @param value
     *            the relative value to set
     */
    protected void updateSliderTextValue(int value, boolean isSelected) {

        String newValue = Integer.toString(value);
        LOGGER.trace("Set the new value: {}", newValue);
        sliderValue.setText(newValue);
    }

    public void setSliderValueChangeListener(SliderValueChangeListener sliderValueChangeListener) {
        this.sliderValueChangeListener = sliderValueChangeListener;
    }

}
