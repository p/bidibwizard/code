package org.bidib.wizard.mvc.main.model.listener;

import java.util.List;

import org.bidib.jbidibc.core.node.ConfigurationVariable;

public interface CvDefinitionRequestListener {
    /**
     * Load the values of the provided CV from the node.
     * 
     * @param configVariables
     *            the configuration variables
     */
    void loadCvValues(List<ConfigurationVariable> configVariables);

    /**
     * Write the values of the provided CV to the node.
     * 
     * @param cvList
     *            the list of CV
     */
    void writeCvValues(List<ConfigurationVariable> cvList);

    /**
     * Activate the aspect.
     * 
     * @param aspectNumber
     *            the aspect number
     */
    void activateAspect(int aspectNumber);
}
