package org.bidib.wizard.mvc.pomupdate.view.listener;

import org.bidib.wizard.mvc.pomupdate.model.Decoder;

public interface PomUpdateStatusListener {

    /**
     * Status update notification.
     * 
     * @param decoder
     *            the decoder
     * @param progress
     *            the new progress value
     */
    void updateStatus(Decoder decoder, int progress);
}
