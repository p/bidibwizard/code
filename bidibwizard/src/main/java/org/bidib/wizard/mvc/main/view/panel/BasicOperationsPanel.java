package org.bidib.wizard.mvc.main.view.panel;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.common.utils.ImageUtils;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.view.menu.listener.MainMenuListener;
import org.bidib.wizard.mvc.main.view.panel.listener.NodeListActionListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.debug.FormDebugPanel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.ComponentFactory;
import com.jgoodies.forms.factories.DefaultComponentFactory;
import com.jgoodies.forms.layout.FormLayout;

public class BasicOperationsPanel {

    private static final Logger LOGGER = LoggerFactory.getLogger(BasicOperationsPanel.class);

    private static final String ENCODED_DIALOG_COLUMN_SPECS = "fill:pref:grow, 5dlu, 200dlu";

    private final MainModel mainModel;

    private final PropertyChangeListener nodeListener;

    private Node selectedNode;

    private final JPanel contentPanel;

    private final JButton btnFirmwareUpdate;

    private final JButton btnNodeConfigurator;

    private ImageIcon firmwareUpdateIcon;

    private ImageIcon nodeConfiguratorIcon;

    public BasicOperationsPanel(final MainModel model) {
        this.mainModel = model;

        nodeListener = new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                // if (org.bidib.jbidibc.core.Node.PROPERTY_FEATURES == evt.getPropertyName()) {
                // // the features have changed
                // updateNodeInfo(selectedNode);
                // }

            }
        };

        // create form builder
        DefaultFormBuilder dialogBuilder = null;
        boolean debugDialog = false;
        if (debugDialog) {
            JPanel panel = new FormDebugPanel() {
                private static final long serialVersionUID = 1L;

                @Override
                public String getName() {
                    // this is used as tab title
                    return Resources.getString(BasicOperationsPanel.class, "name");
                }
            };
            dialogBuilder = new DefaultFormBuilder(new FormLayout(ENCODED_DIALOG_COLUMN_SPECS), panel);
        }
        else {
            JPanel panel = new JPanel(new BorderLayout()) {
                private static final long serialVersionUID = 1L;

                @Override
                public String getName() {
                    // this is used as tab title
                    return Resources.getString(BasicOperationsPanel.class, "name");
                }
            };
            dialogBuilder = new DefaultFormBuilder(new FormLayout(ENCODED_DIALOG_COLUMN_SPECS), panel);
        }
        dialogBuilder.border(Borders.TABBED_DIALOG);

        final ComponentFactory componentFactory = DefaultComponentFactory.getInstance();

        firmwareUpdateIcon = ImageUtils.createImageIcon(BasicOperationsPanel.class, "/icons/64x64/firmware-update.png");

        btnFirmwareUpdate =
            componentFactory.createButton(
                new AbstractAction(Resources.getString("mvc.firmware.view.FirmwareView", "title"), firmwareUpdateIcon) {

                    private static final long serialVersionUID = 1L;

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        fireFirmwareUpdate();
                    }

                    @Override
                    public boolean isEnabled() {
                        return true;
                    }
                });

        dialogBuilder.append(btnFirmwareUpdate);
        dialogBuilder.nextLine();

        nodeConfiguratorIcon =
            ImageUtils.createImageIcon(BasicOperationsPanel.class, "/icons/64x64/nodescript-wizard.png");

        btnNodeConfigurator =
            componentFactory.createButton(new AbstractAction(
                Resources.getString("mvc.script.view.wizard.NodeScriptWizard", "title"), nodeConfiguratorIcon) {

                private static final long serialVersionUID = 1L;

                @Override
                public void actionPerformed(ActionEvent e) {
                    fireNodeScriptWizard();
                }

                @Override
                public boolean isEnabled() {
                    return true;
                }
            });
        dialogBuilder.append(btnNodeConfigurator);

        contentPanel = dialogBuilder.build();
    }

    public JPanel getComponent() {
        return contentPanel;
    }

    public void nodeChanged() {
        LOGGER.info("The selected node has changed.");

        updateComponentState();
    }

    private void updateComponentState() {

        Node node = mainModel.getSelectedNode();

        if (selectedNode != null && !selectedNode.equals(node)) {
            // the selected node has changed.
            selectedNode.getNode().removePropertyChangeListener(nodeListener);
        }

        if (node != null) {
            updateNodeInfo(node);

            selectedNode = node;
            // register the feature listener
            selectedNode.getNode().addPropertyChangeListener(nodeListener);
        }
        else {
            selectedNode = null;
        }
    }

    private void updateNodeInfo(final Node node) {
        if (node == null) {
            // no node available
            return;
        }
        LOGGER.info("Update the basic operations panel for node: {}", node);

        btnFirmwareUpdate.setEnabled(node.isUpdatable());
    }

    private void fireFirmwareUpdate() {
        LOGGER.info("Trigger the firmware update.");

        NodeListActionListener nodeListActionListener =
            DefaultApplicationContext.getInstance().get(DefaultApplicationContext.KEY_MAINNODELISTLISTENER,
                NodeListActionListener.class);

        if (nodeListActionListener != null) {
            nodeListActionListener.firmwareUpdate(mainModel.getSelectedNode(), -1, -1);
        }
        else {
            LOGGER.warn("No nodeListAction listener available.");
        }
    }

    private void fireNodeScriptWizard() {
        LOGGER.info("Trigger the node script wizard.");

        MainMenuListener mainMenuListener =
            DefaultApplicationContext.getInstance().get(DefaultApplicationContext.KEY_MAINMENU_LISTENER,
                MainMenuListener.class);

        if (mainMenuListener != null) {
            mainMenuListener.showNodeScriptWizard(mainModel);
        }
        else {
            LOGGER.warn("No main menu listener available.");
        }

    }

}
