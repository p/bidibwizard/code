package org.bidib.wizard.mvc.main.model;

import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.SwingUtilities;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.bidib.jbidibc.core.enumeration.IdentifyState;
import org.bidib.jbidibc.core.enumeration.SysErrorEnum;
import org.bidib.jbidibc.core.node.ConfigurationVariable;
import org.bidib.jbidibc.core.port.PortConfigValue;
import org.bidib.jbidibc.core.utils.MessageUtils;
import org.bidib.jbidibc.core.utils.NodeUtils;
import org.bidib.jbidibc.exchange.vendorcv.NodeType;
import org.bidib.jbidibc.exchange.vendorcv.TemplateType;
import org.bidib.jbidibc.exchange.vendorcv.VendorCvData;
import org.bidib.wizard.comm.FeedbackPortStatus;
import org.bidib.wizard.labels.PortLabelUtils;
import org.bidib.wizard.labels.ServoPortLabelFactory;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.bidib.wizard.mvc.common.view.cvdefinition.CvDefinitionTreeTableModel;
import org.bidib.wizard.mvc.console.controller.ConsoleController;
import org.bidib.wizard.mvc.console.model.ConsoleModel;
import org.bidib.wizard.mvc.main.model.listener.NodeListener;
import org.bidib.wizard.mvc.main.model.listener.PortListListener;
import org.bidib.wizard.mvc.main.view.cvdef.CvNode;
import org.bidib.wizard.mvc.main.view.panel.CvDefinitionTreeHelper;
import org.bidib.wizard.utils.XmlLocaleUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.beans.Model;
import com.jidesoft.grid.DefaultExpandableRow;

public class Node extends Model implements LabelAware, PropertyChangeListener {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(Node.class);

    public static final String PROPERTY_FEEDBACKPORTS = "feedbackPorts";

    public static final String PROPERTY_FEEDBACKPOSITIONS = "feedbackPositions";

    public static final String PROPERTY_FEEDBACKPORT_STATUS = "feedbackPortStatus";

    public static final String PROPERTY_FEEDBACKPORT_ADDRESSES = "feedbackPortAddresses";

    public static final String PROPERTY_FEEDBACKPORT_CONFIDENCE = "feedbackPortConfidence";

    public static final String PROPERTY_FEEDBACKPORT_DYNSTATES = "feedbackPortDynStates";

    public static final String PROPERTY_LABEL = "label";

    public static final String PROPERTY_INITIAL_LOAD_FINISHED = "initialLoadFinished";

    private final Collection<NodeListener> listeners = new LinkedList<NodeListener>();

    private Boolean addressMessagesEnabled;

    private Boolean commandStation = Boolean.FALSE;

    private Boolean railComPlusAvailable = Boolean.FALSE;

    private Boolean pomUpdateAvailable = Boolean.FALSE;

    private Boolean booster = Boolean.FALSE;

    private Boolean dccStartEnabled;

    private Boolean externalStartEnabled;

    private Boolean feedbackMessagesEnabled;

    private Boolean feedbackMirrorDisabled;

    private IdentifyState identifyState;

    private SysErrorEnum sysError;

    private byte[] reasonData;

    private boolean isStall;

    private Boolean keyMessagesEnabled;

    private String label;

    private final org.bidib.jbidibc.core.Node node;

    private int storableMacroCount;

    private int maxMacroSteps;

    private boolean updatable;

    private boolean bootloaderNode;

    private boolean nodeHasError;

    private VendorCvData vendorCV;

    private CvDefinitionTreeTableModel cvTreeModel;

    private List<ConfigurationVariable> configVariables;

    private Map<String, CvNode> cvNumberToJideNodeMap;

    private List<FeedbackPort> feedbackPorts = new LinkedList<>();

    private List<GenericPort> genericPorts = new LinkedList<>();

    private List<AnalogPort> analogPorts = new LinkedList<>();

    private List<BacklightPort> backlightPorts = new LinkedList<>();

    private List<InputPort> inputPorts = new LinkedList<>();

    private List<LightPort> lightPorts = new LinkedList<>();

    private List<MotorPort> motorPorts = new LinkedList<>();

    private List<ServoPort> servoPorts = new LinkedList<>();

    private List<SoundPort> soundPorts = new LinkedList<>();

    private List<SwitchPort> switchPorts = new LinkedList<>();

    private List<SwitchPairPort> switchPairPorts = new LinkedList<>();

    private final List<Flag> flags = new LinkedList<Flag>();

    private Map<Class<?>, List<PortListListener>> portListListeners = new LinkedHashMap<>();

    private AtomicBoolean initialLoadFinished = new AtomicBoolean(false);

    /**
     * Create a new node with the associated bidib node.
     * 
     * @param node
     *            the bidib node is mandatory
     * @throws IllegalArgumentException
     *             if the bidib node is null
     */
    public Node(org.bidib.jbidibc.core.Node node) {
        if (node == null) {
            throw new IllegalArgumentException("The node must not be null!");
        }
        this.node = node;
    }

    public long getUniqueId() {
        return node.getUniqueId();
    }

    public void addNodeListener(NodeListener l) {
        listeners.add(l);
    }

    public void removeNodeListener(NodeListener l) {
        listeners.remove(l);
    }

    public void addPortListListener(Class<?> portClazz, PortListListener listener) {
        List<PortListListener> listeners = portListListeners.get(portClazz);
        if (listeners == null) {
            listeners = new LinkedList<>();
            portListListeners.put(portClazz, listeners);
        }
        listeners.add(listener);
    }

    public void removePortListListener(Class<?> portClazz, PortListListener listener) {
        List<PortListListener> listeners = portListListeners.get(portClazz);
        if (listeners != null) {
            listeners.remove(listener);
        }
    }

    private void firePortListChanged(Class<?> portClazz) {
        LOGGER.info("The port list has changed for class: {}", portClazz);
        List<PortListListener> listeners = portListListeners.get(portClazz);
        if (listeners != null) {
            notifyPortListListeners(listeners);
        }
    }

    private void notifyPortListListeners(final Collection<PortListListener> listeners) {
        if (SwingUtilities.isEventDispatchThread()) {
            for (PortListListener l : listeners) {
                l.listChanged();
            }
        }
        else {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    for (PortListListener l : listeners) {
                        l.listChanged();
                    }
                }
            });
        }
    }

    public void setFlags(List<Flag> flags) {
        LOGGER.info("Set the flags.");
        this.flags.clear();
        if (CollectionUtils.isNotEmpty(flags)) {
            this.flags.addAll(flags);
        }
    }

    public List<Flag> getFlags() {
        synchronized (flags) {
            return Collections.unmodifiableList(flags);
        }
    }

    public Boolean isAddressMessagesEnabled() {
        return addressMessagesEnabled;
    }

    public void setAddressMessagesEnabled(Boolean addressMessagesEnabled) {
        this.addressMessagesEnabled = addressMessagesEnabled;
        fireAddressMessagesEnabledChanged(addressMessagesEnabled);
    }

    public Boolean isBooster() {
        return booster;
    }

    public void setBooster(Boolean booster) {
        this.booster = booster;
    }

    public Boolean isCommandStation() {
        return commandStation;
    }

    public void setCommandStation(Boolean commandStation) {
        this.commandStation = commandStation;
    }

    public Boolean isRailComPlusAvailable() {
        return railComPlusAvailable;
    }

    public void setRailComPlusAvailable(Boolean railComPlusAvailable) {
        LOGGER.info("Set railComPlusAvailable: {}", railComPlusAvailable);
        this.railComPlusAvailable = railComPlusAvailable;
        fireRailComPlusAvailableChanged(railComPlusAvailable);
    }

    public Boolean isPomUpdateAvailable() {
        return pomUpdateAvailable;
    }

    public void setPomUpdateAvailable(Boolean pomUpdateAvailable) {
        LOGGER.info("Set pomUpdateAvailable: {}", pomUpdateAvailable);
        this.pomUpdateAvailable = pomUpdateAvailable;
        firePomUpdateAvailableChanged(pomUpdateAvailable);
    }

    public Boolean isDccStartEnabled() {
        return dccStartEnabled;
    }

    public void setDccStartEnabled(Boolean dccStartEnabled) {
        this.dccStartEnabled = dccStartEnabled;
        fireDccStartEnabledChanged(dccStartEnabled);
    }

    public Boolean isExternalStartEnabled() {
        return externalStartEnabled;
    }

    public void setExternalStartEnabled(Boolean externalStartEnabled) {
        this.externalStartEnabled = externalStartEnabled;
        fireExternalStartEnabledChanged(externalStartEnabled);
    }

    public Boolean isFeedbackMessagesEnabled() {
        return feedbackMessagesEnabled;
    }

    public void setFeedbackMessagesEnabled(Boolean feedbackMessagesEnabled) {
        this.feedbackMessagesEnabled = feedbackMessagesEnabled;
        fireFeedbackMessagesEnabledChanged(feedbackMessagesEnabled);
    }

    public Boolean isFeedbackMirrorDisabled() {
        return feedbackMirrorDisabled;
    }

    public void setFeedbackMirrorDisabled(Boolean feedbackMirrorDisabled) {
        LOGGER.info("Set the feedback mirror messages disabled: {}", feedbackMirrorDisabled);
        this.feedbackMirrorDisabled = feedbackMirrorDisabled;
        fireFeedbackMirrorDisabledChanged(feedbackMirrorDisabled);
    }

    public IdentifyState getIdentifyState() {
        return identifyState;
    }

    public void setIdentifyState(IdentifyState identifyState) {
        this.identifyState = identifyState;
        fireIdentifyStateChanged(identifyState);
    }

    public void setErrorState(final SysErrorEnum sysError, final byte[] reasonData) {
        this.sysError = sysError;
        this.reasonData = reasonData;
        fireSysErrorChanged(sysError);

        // notify the console
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                // ensure console is visible
                ConsoleController.ensureConsoleVisible();
                try {
                    String reason = MessageUtils.formatSysError(getNode().getAddr(), sysError, reasonData);
                    // add line
                    ConsoleModel.getConsoleModel().addConsoleLine(Color.red,
                        String.format("The node %s is set to error state: %s", Node.this.toString(), reason));
                }
                catch (Exception ex) {
                    LOGGER.warn("Add new error line to console failed, sysError: {}, reasonData: {}", sysError,
                        reasonData, ex);
                }
            }
        });
    }

    public SysErrorEnum getErrorState() {
        return sysError;
    }

    public byte[] getReasonData() {
        return reasonData;
    }

    public void setReasonData(final byte[] reasonData) {
        this.reasonData = reasonData;

        // notify the console
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                // ensure console is visible
                ConsoleController.ensureConsoleVisible();
                try {
                    String reason = null;
                    if (sysError != null) {
                        reason = MessageUtils.formatSysError(getNode().getAddr(), sysError, reasonData);
                    }
                    else {
                        reason = new String(reasonData, Charset.forName("ISO-8859-1"));
                    }
                    // add line
                    ConsoleModel.getConsoleModel().addConsoleLine(Color.red,
                        String.format("The node %s is set to error state: %s", Node.this.toString(), reason));
                }
                catch (Exception ex) {
                    LOGGER.warn("Add new error line to console failed, sysError: {}, reasonData: {}", sysError,
                        reasonData, ex);
                }
            }
        });
    }

    /**
     * @return the node has the reset required flag set
     */
    public boolean isNodeHasRestartPendingError() {
        return SysErrorEnum.BIDIB_ERR_RESET_REQUIRED.equals(sysError);
    }

    public Boolean isKeyMessagesEnabled() {
        return keyMessagesEnabled;
    }

    public void setKeyMessagesEnabled(Boolean keyMessagesEnabled) {
        this.keyMessagesEnabled = keyMessagesEnabled;
        fireKeyMessagesEnabledChanged(keyMessagesEnabled);
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        LOGGER.info("Set the label: {}", label);
        String oldValue = this.label;

        this.label = label;

        firePropertyChange(PROPERTY_LABEL, oldValue, label);

        fireLabelChanged(label);
    }

    public org.bidib.jbidibc.core.Node getNode() {
        return node;
    }

    public int getStorableMacroCount() {
        return storableMacroCount;
    }

    public void setStorableMacroCount(int storableMacroCount) {
        this.storableMacroCount = storableMacroCount;
    }

    /**
     * @return the maximum supported steps per macro
     */
    public int getMaxMacroSteps() {
        return maxMacroSteps;
    }

    /**
     * @param maxMacroSteps
     *            the maximum supported steps per macro
     */
    public void setMaxMacroSteps(int maxMacroSteps) {
        this.maxMacroSteps = maxMacroSteps;
    }

    /**
     * @return the genericPorts
     */
    public List<GenericPort> getGenericPorts() {
        return Collections.unmodifiableList(genericPorts);
    }

    /**
     * @param genericPorts
     *            the genericPorts to set
     */
    public void setGenericPorts(List<GenericPort> genericPorts) {

        synchronized (this.genericPorts) {
            LOGGER.info("Set the generic ports on the node: {}", this);

            // remove property change listener
            for (GenericPort port : this.genericPorts) {
                port.removePropertyChangeListener(this);
            }

            // set the new generic ports
            this.genericPorts.clear();
            this.genericPorts.addAll(genericPorts);

            // add property change listener
            for (GenericPort port : this.genericPorts) {
                port.addPropertyChangeListener(this);
            }

            clearPortCache();
        }
    }

    public void clearPortCache() {
        LOGGER.info("Clear the port cache on node: {}", this);

        synchronized (genericPorts) {
            // clear all cached ports
            switchPorts.clear();
            switchPairPorts.clear();
            servoPorts.clear();
            inputPorts.clear();
            analogPorts.clear();
            lightPorts.clear();
            backlightPorts.clear();
            motorPorts.clear();
            soundPorts.clear();
        }
    }

    public List<AnalogPort> getAnalogPorts() {

        synchronized (genericPorts) {

            if (CollectionUtils.isEmpty(analogPorts)) {
                LOGGER.debug("Prepare the analog ports.");
                for (GenericPort genericPort : genericPorts) {
                    // fetch the values from the generic port
                    if (genericPort.isSupportsAnalogPort()) {
                        LOGGER.trace("The current port supports analog port: {}", genericPort);
                        AnalogPort analogPort = new AnalogPort(genericPort);
                        analogPort.setId(genericPort.getPortNumber());
                        analogPorts.add(analogPort);
                    }
                    else {
                        LOGGER.trace("The current port does not support analog port: {}", genericPort);
                    }
                }
                LOGGER.debug("Return the prepared analog ports: {}", analogPorts.size());
            }
            else {
                LOGGER.debug("Return the cached analog ports, size: {}", analogPorts.size());
            }

            return Collections.unmodifiableList(analogPorts);
        }
    }

    public List<SwitchPort> getSwitchPorts() {

        synchronized (genericPorts) {

            if (CollectionUtils.isEmpty(switchPorts)) {
                LOGGER.debug("Prepare the switch ports.");
                for (GenericPort genericPort : genericPorts) {
                    // fetch the values from the generic port
                    if (genericPort.isSupportsSwitchPort()) {
                        LOGGER.trace("The current port supports switch port: {}", genericPort);
                        SwitchPort switchPort = new SwitchPort(genericPort);
                        switchPort.setId(genericPort.getPortNumber());
                        switchPorts.add(switchPort);
                    }
                    else {
                        LOGGER.trace("The current port does not support switch port: {}", genericPort);
                    }
                }
                LOGGER.debug("Return the prepared switch ports: {}", switchPorts.size());
            }
            else {
                LOGGER.debug("Return the cached switch ports, size: {}", switchPorts.size());
            }

            return Collections.unmodifiableList(switchPorts);
        }
    }

    public List<SwitchPort> getEnabledSwitchPorts() {
        synchronized (switchPorts) {
            List<SwitchPort> enabledPorts = new LinkedList<>();
            List<SwitchPort> switchPorts = getSwitchPorts();
            for (SwitchPort port : switchPorts) {
                if (port.isEnabled()) {
                    enabledPorts.add(port);
                }
            }
            return Collections.unmodifiableList(enabledPorts);
        }
    }

    public List<SwitchPairPort> getSwitchPairPorts() {

        synchronized (genericPorts) {

            if (CollectionUtils.isEmpty(switchPairPorts)) {
                LOGGER.info("Prepare the switchPair ports.");
                for (GenericPort genericPort : genericPorts) {
                    // fetch the values from the generic port
                    if (genericPort.isSupportsSwitchPairPort()) {
                        LOGGER.info("The current port supports switchPair port: {}", genericPort);
                        SwitchPairPort switchPairPort = new SwitchPairPort(genericPort);
                        switchPairPort.setId(genericPort.getPortNumber());
                        switchPairPorts.add(switchPairPort);
                    }
                    else {
                        LOGGER.trace("The current port does not support switchPair port: {}", genericPort);
                    }
                }
                LOGGER.debug("Return the prepared switchPair ports: {}", switchPairPorts.size());
            }
            else {
                LOGGER.debug("Return the cached switchPair ports, size: {}", switchPairPorts.size());
            }

            return Collections.unmodifiableList(switchPairPorts);
        }
    }

    public List<SwitchPairPort> getEnabledSwitchPairPorts() {
        synchronized (switchPairPorts) {
            List<SwitchPairPort> enabledPorts = new LinkedList<>();
            List<SwitchPairPort> switchPairPorts = getSwitchPairPorts();
            for (SwitchPairPort port : switchPairPorts) {
                if (port.isEnabled()) {
                    enabledPorts.add(port);
                }
            }
            return Collections.unmodifiableList(enabledPorts);
        }
    }

    public List<LightPort> getLightPorts() {

        synchronized (genericPorts) {

            if (CollectionUtils.isEmpty(lightPorts)) {
                LOGGER.debug("Prepare the light ports.");
                for (GenericPort genericPort : genericPorts) {
                    // fetch the values from the generic port
                    if (genericPort.isSupportsLightPort()) {
                        LOGGER.trace("The current port supports light port: {}", genericPort);
                        LightPort lightPort = new LightPort(genericPort);
                        lightPort.setId(genericPort.getPortNumber());
                        lightPorts.add(lightPort);
                    }
                    else {
                        LOGGER.trace("The current port does not support light port: {}", genericPort);
                    }
                }
                LOGGER.debug("Return the prepared light ports: {}", lightPorts.size());
            }
            else {
                LOGGER.debug("Return the cached light ports, size: {}", lightPorts.size());
            }

            return Collections.unmodifiableList(lightPorts);
        }
    }

    public List<LightPort> getEnabledLightPorts() {
        synchronized (lightPorts) {
            List<LightPort> enabledPorts = new LinkedList<>();
            List<LightPort> lightPorts = getLightPorts();
            for (LightPort port : lightPorts) {
                if (port.isEnabled()) {
                    enabledPorts.add(port);
                }
            }
            return Collections.unmodifiableList(enabledPorts);
        }
    }

    public List<BacklightPort> getBacklightPorts() {

        synchronized (genericPorts) {

            if (CollectionUtils.isEmpty(backlightPorts)) {
                LOGGER.debug("Prepare the backlight ports.");
                for (GenericPort genericPort : genericPorts) {
                    // fetch the values from the generic port
                    if (genericPort.isSupportsBacklightPort()) {
                        LOGGER.trace("The current port supports backlight port: {}", genericPort);
                        BacklightPort backlightPort = new BacklightPort(genericPort);
                        backlightPort.setId(genericPort.getPortNumber());
                        backlightPorts.add(backlightPort);
                    }
                    else {
                        LOGGER.trace("The current port does not support backlight port: {}", genericPort);
                    }
                }
                LOGGER.debug("Return the prepared backlight ports: {}", backlightPorts.size());
            }
            else {
                LOGGER.debug("Return the cached backlight ports, size: {}", backlightPorts.size());
            }

            return Collections.unmodifiableList(backlightPorts);
        }
    }

    public List<InputPort> getInputPorts() {
        synchronized (genericPorts) {
            if (CollectionUtils.isEmpty(inputPorts)) {
                LOGGER.debug("Prepare the input ports.");
                for (GenericPort genericPort : genericPorts) {
                    // fetch the values from the generic port
                    if (genericPort.isSupportsInputPort()) {
                        LOGGER.trace("The current port supports input port: {}", genericPort);
                        InputPort inputPort = new InputPort(genericPort);
                        inputPort.setId(genericPort.getPortNumber());
                        inputPorts.add(inputPort);
                    }
                    else {
                        LOGGER.trace("The current port does not support input port: {}", genericPort);
                    }
                }
            }
            else {
                LOGGER.debug("Return the cached input ports, size: {}", inputPorts.size());
            }

            return Collections.unmodifiableList(inputPorts);
        }
    }

    public List<InputPort> getEnabledInputPorts() {
        synchronized (inputPorts) {
            List<InputPort> enabledPorts = new LinkedList<>();
            List<InputPort> inputPorts = getInputPorts();
            for (InputPort port : inputPorts) {
                if (port.isEnabled()) {
                    enabledPorts.add(port);
                }
            }
            return Collections.unmodifiableList(enabledPorts);
        }
    }

    /**
     * Set the servo ports
     * 
     * @param servoPorts
     *            the new servo ports.
     */
    public void setServoPorts(List<ServoPort> servoPorts) {
        LOGGER.info("Set the servo ports on the node: {}", servoPorts);

        synchronized (servoPorts) {
            this.servoPorts.clear();
            this.servoPorts.addAll(servoPorts);
        }

        firePortListChanged(ServoPort.class);
    }

    public boolean hasServoPorts() {
        LOGGER.debug("Check if servo ports are available.");
        synchronized (servoPorts) {
            return CollectionUtils.isNotEmpty(servoPorts);
        }
    }

    public List<ServoPort> getServoPorts() {

        synchronized (genericPorts) {
            if (CollectionUtils.isEmpty(servoPorts)) {
                LOGGER.debug("Prepare the servo ports.");
                for (GenericPort genericPort : genericPorts) {
                    // fetch the values from the generic port
                    if (genericPort.isSupportsServoPort()) {
                        LOGGER.trace("The current port supports servo port: {}", genericPort);
                        ServoPort servoPort = new ServoPort(genericPort);
                        servoPort.setId(genericPort.getPortNumber());

                        servoPorts.add(servoPort);
                    }
                    else {
                        LOGGER.trace("The current port does not support servo port: {}", genericPort);
                    }
                }
                // TODO move this to the node
                // set the port labels
                PortLabelUtils.applyPortLabels(this, DefaultApplicationContext.KEY_SERVOPORT_LABELS,
                    ServoPortLabelFactory.DEFAULT_LABELTYPE, servoPorts);

            }
            else {
                LOGGER.debug("Return the cached servo ports.");
            }
            return Collections.unmodifiableList(servoPorts);
        }
    }

    public void setServoPortConfig(int portNumber, Map<Byte, PortConfigValue<?>> portConfig) {
        // synchronize because the servoPorts can be accessed from different threads
        try {
            List<ServoPort> servoPorts = getServoPorts();
            if (CollectionUtils.isNotEmpty(servoPorts)) {
                servoPorts.get(portNumber).setPortConfigX(portConfig);
            }

            // synchronized (this.servoPorts) {
            // if (servoPorts != null) {
            // this.servoPorts.get(portNumber).setPortConfigX(portConfig);
            // }
            // }
            firePortListChanged(ServoPort.class);
        }
        catch (Exception ex) {
            LOGGER.warn("Set servoport config failed for portNumber: {}", portNumber, ex);
        }
    }

    /**
     * Set the motor ports
     * 
     * @param motorPorts
     *            the new motor ports.
     */
    public void setMotorPorts(List<MotorPort> motorPorts) {
        LOGGER.info("Set the motor ports on the node: {}", motorPorts);

        synchronized (motorPorts) {
            this.motorPorts.clear();
            this.motorPorts.addAll(motorPorts);
        }

        firePortListChanged(MotorPort.class);
    }

    public boolean hasMotorPorts() {
        LOGGER.debug("Check if motor ports are available.");
        synchronized (motorPorts) {
            return CollectionUtils.isNotEmpty(motorPorts);
        }
    }

    public List<MotorPort> getMotorPorts() {

        synchronized (genericPorts) {

            if (CollectionUtils.isEmpty(motorPorts)) {
                LOGGER.debug("Prepare the motor ports.");
                for (GenericPort genericPort : genericPorts) {
                    // fetch the values from the generic port
                    if (genericPort.isSupportsMotorPort()) {
                        LOGGER.trace("The current port supports motor port: {}", genericPort);
                        MotorPort motorPort = new MotorPort(genericPort);
                        motorPort.setId(genericPort.getPortNumber());
                        motorPorts.add(motorPort);
                    }
                    else {
                        LOGGER.trace("The current port does not support motor port: {}", genericPort);
                    }
                }
                LOGGER.debug("Return the prepared motor ports: {}", motorPorts.size());
            }
            else {
                LOGGER.debug("Return the cached motor ports, size: {}", motorPorts.size());
            }

            return Collections.unmodifiableList(motorPorts);
        }
    }

    public void setMotorPortConfig(int portNumber, Map<Byte, PortConfigValue<?>> portConfig) {
        // synchronize because the servoPorts can be accessed from different threads
        try {
            List<MotorPort> motorPorts = getMotorPorts();
            if (CollectionUtils.isNotEmpty(motorPorts)) {
                motorPorts.get(portNumber).setPortConfigX(portConfig);
            }

            firePortListChanged(MotorPort.class);
        }
        catch (Exception ex) {
            LOGGER.warn("Set motorport config failed for portNumber: {}", portNumber, ex);
        }
    }

    public List<SoundPort> getSoundPorts() {

        synchronized (genericPorts) {

            if (CollectionUtils.isEmpty(soundPorts)) {
                LOGGER.debug("Prepare the sound ports.");
                for (GenericPort genericPort : genericPorts) {
                    // fetch the values from the generic port
                    if (genericPort.isSupportsSoundPort()) {
                        LOGGER.trace("The current port supports sound port: {}", genericPort);
                        SoundPort soundPort = new SoundPort(genericPort);
                        soundPort.setId(genericPort.getPortNumber());
                        soundPorts.add(soundPort);
                    }
                    else {
                        LOGGER.trace("The current port does not support sound port: {}", genericPort);
                    }
                }
                LOGGER.debug("Return the prepared sound ports: {}", soundPorts.size());
            }
            else {
                LOGGER.debug("Return the cached sound ports, size: {}", soundPorts.size());
            }

            return Collections.unmodifiableList(soundPorts);
        }
    }

    public List<SoundPort> getEnabledSoundPorts() {
        synchronized (soundPorts) {
            List<SoundPort> enabledPorts = new LinkedList<>();
            List<SoundPort> soundPorts = getSoundPorts();
            for (SoundPort port : soundPorts) {
                if (port.isEnabled()) {
                    enabledPorts.add(port);
                }
            }
            return Collections.unmodifiableList(enabledPorts);
        }
    }

    public boolean isUpdatable() {
        return updatable;
    }

    public void setUpdatable(boolean updatable) {
        this.updatable = updatable;
    }

    /**
     * @return the bootloaderNode
     */
    public boolean isBootloaderNode() {
        return bootloaderNode;
    }

    /**
     * @param bootloaderNode
     *            the bootloaderNode to set
     */
    public void setBootloaderNode(boolean bootloaderNode) {
        this.bootloaderNode = bootloaderNode;
    }

    /**
     * @return the nodeHasError
     */
    public boolean isNodeHasError() {
        return nodeHasError || (sysError != null && !SysErrorEnum.BIDIB_ERR_NONE.equals(sysError));
    }

    /**
     * @param ignoreSysError
     *            flag to allow ignore sys errors
     * @return node has error flag set
     */
    public boolean isNodeHasError(boolean ignoreSysError) {
        if (ignoreSysError) {
            return nodeHasError;
        }
        return isNodeHasError();
    }

    /**
     * @return the nodeHasSysError
     */
    public boolean isNodeHasSysError() {
        return (sysError != null && !SysErrorEnum.BIDIB_ERR_NONE.equals(sysError));
    }

    /**
     * @param nodeHasError
     *            the nodeHasError to set
     */
    public void setNodeHasError(boolean nodeHasError) {
        LOGGER.info("The node has an error: {}", nodeHasError);
        this.nodeHasError = nodeHasError;
        if (!nodeHasError) {
            setErrorState(null, null);
        }
    }

    /**
     * @param stall
     *            the stall flag to set
     */
    public void setNodeIsStall(final boolean stall) {
        LOGGER.warn("The node {} is stall: {}", node, stall);
        this.isStall = stall;

        // notify the console
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                // ensure console is visible
                ConsoleController.ensureConsoleVisible();

                // add line
                ConsoleModel.getConsoleModel().addConsoleLine(Color.black,
                    String.format("The node %s is %s", Node.this.toString(), stall ? "stall" : "not stall"));
            }
        });
    }

    /**
     * @return the stall flag
     */
    public boolean isNodeStall() {
        return isStall;
    }

    /**
     * @return the vendorCV
     */
    public VendorCvData getVendorCV() {
        return vendorCV;
    }

    /**
     * @param vendorCV
     *            the vendorCV to set
     */
    public void setVendorCV(VendorCvData vendorCV) {
        LOGGER.debug("Set the vendorCV: {}", vendorCV);
        this.vendorCV = vendorCV;
    }

    public CvDefinitionTreeTableModel getCvDefinitionTreeTableModel() {
        if (cvTreeModel == null) {
            prepareVendorCVTree();
        }
        return cvTreeModel;
    }

    public List<ConfigurationVariable> getConfigVariables() {
        return configVariables;
    }

    public Map<String, CvNode> getCvNumberToJideNodeMap() {
        return cvNumberToJideNodeMap;
    }

    /**
     * Prepare the vendor CV tree.
     */
    public void prepareVendorCVTree() {
        if (vendorCV == null) {
            LOGGER.info("No vendorCV definition available for node: {}", node);
            return;
        }
        LOGGER.info("Prepare the vendorCV tree for node: {}", node);

        cvTreeModel = new CvDefinitionTreeTableModel();

        Object root = cvTreeModel.getRoot();
        DefaultExpandableRow rootNode = (DefaultExpandableRow) root;

        cvNumberToJideNodeMap = new LinkedHashMap<String, CvNode>();

        List<NodeType> nodes = vendorCV.getVendorCV().getCVDefinition().getNode();

        Map<String, TemplateType> templatesMap = new HashMap<String, TemplateType>();
        if (vendorCV.getVendorCV().getTemplates() != null) {
            for (TemplateType template : vendorCV.getVendorCV().getTemplates().getTemplate()) {
                LOGGER.trace("Add template: {}", template.getName());
                templatesMap.put(template.getName(), template);
            }
            LOGGER.trace("Prepared templatesMap: {}", templatesMap.keySet());
        }
        else {
            LOGGER.info("No templates available.");
        }

        String lang = XmlLocaleUtils.getXmlLocaleVendorCV();
        configVariables =
            new CvDefinitionTreeHelper().addSubNodes(rootNode, nodes, cvTreeModel, templatesMap, null, null, false,
                lang, cvNumberToJideNodeMap);

    }

    private void fireAddressMessagesEnabledChanged(Boolean isAddressMessagesEnabled) {
        for (NodeListener l : listeners) {
            l.addressMessagesEnabledChanged(isAddressMessagesEnabled);
        }
    }

    private void fireDccStartEnabledChanged(Boolean isDccStartEnabled) {
        for (NodeListener l : listeners) {
            l.dccStartEnabledChanged(isDccStartEnabled);
        }
    }

    private void fireRailComPlusAvailableChanged(Boolean railComPlusAvailable) {
        for (NodeListener l : listeners) {
            l.railComPlusAvailableChanged(railComPlusAvailable);
        }
    }

    private void firePomUpdateAvailableChanged(Boolean pomUpdateAvailable) {
        for (NodeListener l : listeners) {
            l.pomUpdateAvailableChanged(pomUpdateAvailable);
        }
    }

    private void fireExternalStartEnabledChanged(Boolean isExternalStartEnabled) {
        for (NodeListener l : listeners) {
            l.externalStartEnabledChanged(isExternalStartEnabled);
        }
    }

    private void fireFeedbackMessagesEnabledChanged(Boolean isFeedbackMessagesEnabled) {
        for (NodeListener l : listeners) {
            l.feedbackMessagesEnabledChanged(isFeedbackMessagesEnabled);
        }
    }

    private void fireFeedbackMirrorDisabledChanged(Boolean feedbackMirrorDisabled) {
        for (NodeListener l : listeners) {
            l.feedbackMirrorDisabledChanged(feedbackMirrorDisabled);
        }
    }

    private void fireIdentifyStateChanged(IdentifyState identifyState) {
        for (NodeListener l : listeners) {
            l.identifyStateChanged(identifyState);
        }
    }

    private void fireSysErrorChanged(SysErrorEnum sysError) {
        for (NodeListener l : listeners) {
            l.sysErrorChanged(sysError);
        }
    }

    private void fireKeyMessagesEnabledChanged(Boolean isKeyMessagesEnabled) {
        for (NodeListener l : listeners) {
            l.keyMessagesEnabledChanged(isKeyMessagesEnabled);
        }
    }

    private void fireLabelChanged(String label) {
        for (NodeListener l : listeners) {
            l.labelChanged(label);
        }
    }

    public boolean equals(Object other) {
        LOGGER.debug("equals, other: {}", other);
        if (other != null) {
            return ((Node) other).hashCode() == hashCode();
        }
        return false;
    }

    public int hashCode() {
        if (node != null) {
            return Long.valueOf(getNode().getUniqueId()).hashCode();
        }
        return super.hashCode();
    }

    public String toString() {
        String result = null;

        if (StringUtils.isNotBlank(label)) {
            result = label;
        }
        else {
            result = NodeUtils.getUniqueIdAsString(node.getUniqueId());
        }
        return result;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        LOGGER.info("The property has been changed, name: {}, new value: {}", evt.getPropertyName(), evt.getNewValue());

        // TODO signal changes and error to UI
        switch (evt.getPropertyName()) {
            case GenericPort.PROPERTY_PORT_CONFIG_ERRORCODE:
                break;
            case GenericPort.PROPERTY_PORT_CONFIG_CHANGED:
                break;
            default:
                break;
        }
    }

    public List<FeedbackPort> getFeedbackPorts() {
        LOGGER.debug("Get the feedback ports.");
        synchronized (feedbackPorts) {
            return Collections.unmodifiableList(feedbackPorts);
        }
    }

    public void setFeedbackPorts(List<FeedbackPort> feedbackPorts) {
        LOGGER.info("Set the feedback ports: {}", feedbackPorts);

        List<FeedbackPort> oldValue = null;
        synchronized (this.feedbackPorts) {
            oldValue = new LinkedList<>(this.feedbackPorts);

            this.feedbackPorts.clear();
            this.feedbackPorts.addAll(feedbackPorts);
        }

        firePropertyChange(PROPERTY_FEEDBACKPORTS, oldValue, this.feedbackPorts);
    }

    public boolean hasFeedbackPorts() {
        LOGGER.debug("Check if  feedback ports are available.");
        synchronized (feedbackPorts) {
            return CollectionUtils.isNotEmpty(feedbackPorts);
        }
    }

    /**
     * Set the feedback port status.
     * 
     * @param detectorNumber
     *            the detector number
     * @param status
     *            the new port status
     */
    public void setFeedbackPortStatus(int detectorNumber, FeedbackPortStatus status, final Long timestamp) {
        LOGGER.debug("feedback port status, detector number: {}, status: {}", detectorNumber, status);

        synchronized (feedbackPorts) {
            if (detectorNumber < feedbackPorts.size()) {
                FeedbackPort port = feedbackPorts.get(detectorNumber);

                FeedbackPortStatus oldValue = port.getStatus();

                port.setStatus(status);

                if (timestamp != null && FeedbackPortStatus.OCCUPIED == status) {
                    port.setTimestamp(new FeedbackTimestampData(timestamp));
                }

                fireIndexedPropertyChange(PROPERTY_FEEDBACKPORT_STATUS, port.getId(), oldValue, status);
            }
            else {
                LOGGER.warn("Status update is skipped because detector number is out of range: {}", detectorNumber);
            }
        }
    }

    /**
     * Set the detected addresses for the feedback port.
     * 
     * @param detectorNumber
     *            the detector number
     * @param addresses
     *            the detected addresses
     */
    public void setFeedbackPortAddresses(int detectorNumber, List<FeedbackAddressData> addresses) {
        LOGGER.info("feedback port addresses, detector number: {}, addresses: {}", detectorNumber, addresses);

        synchronized (feedbackPorts) {
            if (detectorNumber < feedbackPorts.size()) {
                FeedbackPort port = feedbackPorts.get(detectorNumber);

                port.setAddresses(addresses);

                fireIndexedPropertyChange(PROPERTY_FEEDBACKPORT_ADDRESSES, port.getId(), null, addresses);
            }
            else {
                LOGGER.warn("Address update is skipped because detector number is out of range: {}", detectorNumber);
            }
        }
    }

    /**
     * Confidence is signaled from the node.
     * 
     * @param portNumber
     *            the port number (not the detector number)
     * @param freeze
     *            the freeze flag of the detector
     * @param signal
     *            the signal is valid flag of the detector
     * @param valid
     *            the occupancy detection is detected from the track flag of the detector
     */
    public void setFeedbackPortConfidence(int portNumber, boolean freeze, boolean signal, boolean valid) {
        LOGGER.info("feedback port confidence, port number: {}, freeze: {}, signal: {}, valid: {}", portNumber, freeze,
            signal, valid);

        synchronized (feedbackPorts) {
            int totalPorts = feedbackPorts.size();
            if (portNumber < totalPorts) {
                final FeedbackPort port = feedbackPorts.get(portNumber);

                final FeedbackConfidenceData oldValue = port.getConfidence();
                final FeedbackConfidenceData confidence = new FeedbackConfidenceData(freeze, signal, valid);
                port.setConfidence(confidence);

                // signal the change from the EDT
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        fireIndexedPropertyChange(PROPERTY_FEEDBACKPORT_CONFIDENCE, port.getId(), oldValue, confidence);
                    }
                });
            }
            else {
                LOGGER.warn("Confidence update is skipped because detector number is out of range: {}", portNumber);
            }
        }
    }

    /**
     * Set the speed on the current detector if the address matches.
     * 
     * @param detectorNumber
     *            the detector number
     * @param address
     *            the address to search
     * @param speed
     *            the speed value
     */
    public void setFeedbackPortSpeed(int detectorNumber, int address, int speed) {
        LOGGER.info("feedback port speed, detector number: {}, address: {}, speed: {}", detectorNumber, address, speed);

        synchronized (feedbackPorts) {
            if (detectorNumber < feedbackPorts.size()) {
                FeedbackPort port = feedbackPorts.get(detectorNumber);

                List<FeedbackAddressData> addresses = port.getAddresses();
                // check if the current detector has addresses and set the speed if the address matches
                if (addresses != null) {
                    for (FeedbackAddressData addressData : addresses) {
                        if (address == addressData.getAddress()) {
                            addressData.setSpeed(speed);
                        }
                    }
                }

                port.setAddresses(addresses);

                fireIndexedPropertyChange(PROPERTY_FEEDBACKPORT_ADDRESSES, port.getId(), null, addresses);
            }
            else {
                LOGGER.warn("Speed update is skipped because detector number is out of range: {}", detectorNumber);
            }
        }
    }

    public void setFeedbackPortDynStates(int detectorNumber, List<FeedbackDynStateData> dynStates) {
        synchronized (feedbackPorts) {
            if (detectorNumber < feedbackPorts.size()) {
                FeedbackPort port = feedbackPorts.get(detectorNumber);

                port.addDynStates(dynStates);

                fireIndexedPropertyChange(PROPERTY_FEEDBACKPORT_DYNSTATES, port.getId(), null, port.getDynStates());
            }
            else {
                LOGGER.warn("DynState update is skipped because detector number is out of range: {}", detectorNumber);
            }

        }
    }

    /**
     * Update all port lists with the values from the model
     */
    public void updatePortLists() {
        LOGGER.info("updatePortLists.");

        // TODO clear the caches

        // notify the listeners
        // fireAnalogPortListChanged();
        // fireBacklightPortListChanged();
        // fireInputPortListChanged();
        // fireLightPortListChanged();
        // fireMotorPortListChanged();
        firePortListChanged(ServoPort.class);
        // fireSoundPortListChanged();
        // fireSwitchPortListChanged();
    }

    public void setFeedbackPosition(int decoderAddress, int locationType, int locationAddress) {

        FeedbackPosition feedbackPosition =
            new FeedbackPosition(decoderAddress, locationType, locationAddress, System.currentTimeMillis());
        firePropertyChange(PROPERTY_FEEDBACKPOSITIONS, null, feedbackPosition);
    }

    public void signalInitialLoadFinished() {
        LOGGER.info("The initial load has finished.");
        initialLoadFinished.set(true);

        synchronized (initialLoadFinished) {
            initialLoadFinished.notifyAll();
        }

        firePropertyChange(PROPERTY_INITIAL_LOAD_FINISHED, false, true);
    }

    public void signalResetInitialLoadFinished() {
        LOGGER.info("The initial load is reset.");
        boolean oldValue = initialLoadFinished.get();
        initialLoadFinished.set(false);

        synchronized (initialLoadFinished) {
            initialLoadFinished.notifyAll();
        }

        firePropertyChange(PROPERTY_INITIAL_LOAD_FINISHED, oldValue, initialLoadFinished.get());
    }

    public boolean isInitialLoadFinished() {
        return initialLoadFinished.get();
    }

    public Object getInitialLoadFinishedLock() {
        return initialLoadFinished;
    }

}
