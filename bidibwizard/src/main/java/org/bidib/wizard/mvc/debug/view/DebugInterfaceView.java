package org.bidib.wizard.mvc.debug.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.BadLocationException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.bidib.jbidibc.core.exception.InvalidLibraryException;
import org.bidib.jbidibc.debug.LineEndingEnum;
import org.bidib.jbidibc.rxtx.PortIdentifierUtils;
import org.bidib.jbidibc.scm.ScmPortIdentifierUtils;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.common.utils.ImageUtils;
import org.bidib.wizard.dialog.FileDialog;
import org.bidib.wizard.highlight.TextLineNumber;
import org.bidib.wizard.mvc.common.model.CommPort;
import org.bidib.wizard.mvc.common.view.DockKeys;
import org.bidib.wizard.mvc.common.view.text.HistoryModel;
import org.bidib.wizard.mvc.common.view.text.HistoryTextField;
import org.bidib.wizard.mvc.debug.controller.listener.DebugInterfaceControllerListener;
import org.bidib.wizard.mvc.debug.model.DebugInterfaceModel;
import org.bidib.wizard.mvc.debug.view.listener.DebugInterfaceViewListener;
import org.bidib.wizard.mvc.main.view.menu.BasicPopupMenu;
import org.bidib.wizard.mvc.preferences.model.Preferences;
import org.jdesktop.swingx.prompt.PromptSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.adapter.ComboBoxAdapter;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.binding.value.ConverterValueModel;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.builder.ButtonBarBuilder;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.debug.FormDebugPanel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.FormLayout;
import com.jidesoft.swing.JideScrollPane;
import com.vlsolutions.swing.docking.DockKey;
import com.vlsolutions.swing.docking.Dockable;
import com.vlsolutions.swing.docking.DockableState;
import com.vlsolutions.swing.docking.DockingDesktop;
import com.vlsolutions.swing.docking.event.DockableStateChangeEvent;
import com.vlsolutions.swing.docking.event.DockableStateChangeListener;

public class DebugInterfaceView implements Dockable {
    private static final Logger LOGGER = LoggerFactory.getLogger(DebugInterfaceView.class);

    private static final String ENCODED_DIALOG_COLUMN_SPECS =
        "pref, 3dlu, max(100dlu;pref), 3dlu, 20dlu, 3dlu, pref, 3dlu, pref, 3dlu, pref, 3dlu, pref, 3dlu, fill:50dlu:grow, 3dlu, pref, 3dlu, pref";

    private final Collection<DebugInterfaceViewListener> listeners = new LinkedList<>();

    private final DebugInterfaceModel debugInterfaceModel;

    private final DockableStateChangeListener dockableStateChangeListener;

    private final JComponent contentPanel;

    private ValueModel selectionHolderComPort;

    private ValueModel sendTextValueModel;

    private ValueModel selectionHolderBaudRate;

    private ValueModel sendFileValueModel;

    private JTextField sendText;

    private JTextField sendFile;

    private final JButton refreshButton = new JButton(Resources.getString(getClass(), "refresh"));

    private final JButton connectButton = new JButton(Resources.getString(getClass(), "connect"));

    private final JButton disconnectButton = new JButton(Resources.getString(getClass(), "disconnect"));

    private final JButton transmitButton = new JButton(Resources.getString(getClass(), "transmit"));

    private final JButton transmitFileButton = new JButton(Resources.getString(getClass(), "transmit"));

    private final JButton selectFileButton = new JButton(Resources.getString(getClass(), "selectFile"));

    private final JTextArea logsArea = new JTextArea();

    private TextLineNumber tln;

    private SelectionInList<CommPort> commPortSelection;

    private boolean timestampsEnabled;

    private JCheckBox addTimeStamps;

    private JCheckBox logToFile;

    private ValueModel selectedLogFileValueModel;

    private final JButton selectLogFileButton = new JButton(Resources.getString(getClass(), "select-logfile"));

    private JCheckBox tc9SpeedSupport;

    private boolean tc9SpeedSupportEnabled;

    public DebugInterfaceView(final DockingDesktop desktop, final DebugInterfaceControllerListener listener,
        final DebugInterfaceModel debugInterfaceModel) {

        DockKeys.DOCKKEY_DEBUG_INTERFACE_VIEW.setName(Resources.getString(getClass(), "title"));
        DockKeys.DOCKKEY_DEBUG_INTERFACE_VIEW.setFloatEnabled(true);
        DockKeys.DOCKKEY_DEBUG_INTERFACE_VIEW.setAutoHideEnabled(false);

        dockableStateChangeListener = new DockableStateChangeListener() {

            @Override
            public void dockableStateChanged(DockableStateChangeEvent event) {
                LOGGER.info("The state has changed, newState: {}, prevState: {}", event.getNewState(),
                    event.getPreviousState());

                DockableState newState = event.getNewState();
                if (newState.getDockable().equals(DebugInterfaceView.this) && newState.isClosed()) {
                    LOGGER.info("The DebugInterfaceView is closed.");
                    // we are closed
                    desktop.removeDockableStateChangeListener(dockableStateChangeListener);

                    if (listener != null) {
                        LOGGER.info("Close the view.");

                        listener.viewClosed();
                    }
                }

            }
        };
        desktop.addDockableStateChangeListener(dockableStateChangeListener);

        LOGGER.info("Create new DebugInterfaceView");
        this.debugInterfaceModel = debugInterfaceModel;

        loadPortIdentifiers(debugInterfaceModel);

        // create form builder
        DefaultFormBuilder dialogBuilder = null;
        boolean debugDialog = false;
        if (debugDialog) {
            JPanel panel = new FormDebugPanel();
            dialogBuilder = new DefaultFormBuilder(new FormLayout(ENCODED_DIALOG_COLUMN_SPECS), panel);
        }
        else {
            JPanel panel = new JPanel(new BorderLayout());
            dialogBuilder = new DefaultFormBuilder(new FormLayout(ENCODED_DIALOG_COLUMN_SPECS), panel);
        }
        dialogBuilder.border(Borders.DIALOG);

        // add some components
        commPortSelection =
            new SelectionInList<CommPort>((ListModel<CommPort>) debugInterfaceModel.getCommPortsListModel());

        selectionHolderComPort =
            new PropertyAdapter<DebugInterfaceModel>(debugInterfaceModel, DebugInterfaceModel.PROPERTY_SELECTED_PORT,
                true);

        ComboBoxAdapter<CommPort> comboBoxAdapterCommPorts =
            new ComboBoxAdapter<CommPort>(commPortSelection, selectionHolderComPort);
        JComboBox<CommPort> comboCommPorts = new JComboBox<CommPort>();
        comboCommPorts.setModel(comboBoxAdapterCommPorts);

        dialogBuilder.append(Resources.getString(getClass(), "selectedPort"), comboCommPorts);

        refreshButton.setBorder(new EmptyBorder(5, 5, 5, 5));
        dialogBuilder.append(refreshButton);

        List<Integer> baudRates = new ArrayList<>();
        baudRates.add(Integer.valueOf(19200));
        baudRates.add(Integer.valueOf(115200));
        selectionHolderBaudRate =
            new PropertyAdapter<DebugInterfaceModel>(debugInterfaceModel, DebugInterfaceModel.PROPERTY_BAUDRATE, true);
        ComboBoxAdapter<Integer> comboBoxAdapter = new ComboBoxAdapter<Integer>(baudRates, selectionHolderBaudRate);

        JComboBox<Integer> comboBaudRate = new JComboBox<>();
        comboBaudRate.setModel(comboBoxAdapter);
        comboBaudRate.setSelectedIndex(1);

        dialogBuilder.append(Resources.getString(getClass(), "selectedBaudRate"), comboBaudRate);

        refreshButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                fireRefreshComPorts();
            }
        });

        connectButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                fireConnect();
            }
        });
        disconnectButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                fireDisconnect();
            }
        });
        disconnectButton.setEnabled(false);

        List<LineEndingEnum> lineEndings = new ArrayList<>();
        for (LineEndingEnum lineEnding : LineEndingEnum.values()) {
            lineEndings.add(lineEnding);
        }
        SelectionInList<LineEndingEnum> lineEndingSelection = new SelectionInList<>(lineEndings);
        ValueModel selectionHolderLineEnding =
            new PropertyAdapter<DebugInterfaceModel>(debugInterfaceModel, DebugInterfaceModel.PROPERTY_LINE_ENDING,
                true);
        ComboBoxAdapter<LineEndingEnum> comboAdapterLineEnding =
            new ComboBoxAdapter<LineEndingEnum>(lineEndings, selectionHolderLineEnding);

        JComboBox<Integer> comboLineEnding = new JComboBox<>();
        comboLineEnding.setModel(comboAdapterLineEnding);
        comboLineEnding.setRenderer(new LineEndingCellRenderer());

        // prepare the connect and disconnect button
        JPanel debugInterfaceActionButtons =
            new ButtonBarBuilder().addButton(connectButton).addRelatedGap().addButton(disconnectButton).build();
        dialogBuilder.append(debugInterfaceActionButtons);

        addTimeStamps = new JCheckBox(Resources.getString(getClass(), "addTimestamps"));
        dialogBuilder.append(addTimeStamps);

        dialogBuilder.nextLine();

        logToFile = new JCheckBox(Resources.getString(getClass(), "logToFile"));
        dialogBuilder.append(logToFile);

        selectedLogFileValueModel =
            new PropertyAdapter<DebugInterfaceModel>(debugInterfaceModel, DebugInterfaceModel.PROPERTY_LOGFILE_NAME,
                true);
        JTextField selectedLogFileText = BasicComponentFactory.createTextField(selectedLogFileValueModel, true);
        selectedLogFileText.setEditable(false);
        // dialogBuilder.append(Resources.getString(getClass(), "logFile"), selectedLogFileText);
        dialogBuilder.append(selectedLogFileText);

        // prepare the select and save button

        selectLogFileButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                selectLogFile();
            }
        });
        dialogBuilder.append(selectLogFileButton);

        dialogBuilder.append(Resources.getString(getClass(), "lineEnding"), comboLineEnding);

        // add the TC9 speed checkbox
        // dialogBuilder.nextLine();

        tc9SpeedSupport = new JCheckBox(Resources.getString(getClass(), "tc9SpeedSupport"));
        dialogBuilder.append(tc9SpeedSupport);

        // add the log area
        dialogBuilder.appendRow("3dlu");
        dialogBuilder.appendRow("fill:200dlu:grow");
        dialogBuilder.nextLine(2);

        logsArea.setEditable(false);
        logsArea.setFont(new Font("Monospaced", Font.PLAIN, 13));

        JScrollPane logsPane = new JScrollPane(logsArea);

        tln = new TextLineNumber(logsArea);
        logsPane.setRowHeaderView(tln);

        logsPane.setAutoscrolls(true);
        dialogBuilder.append(logsPane, 19);

        dialogBuilder.appendRow("3dlu");
        dialogBuilder.appendRow("pref");
        dialogBuilder.nextLine(2);

        // create the textfield for send message to debug
        sendTextValueModel =
            new PropertyAdapter<DebugInterfaceModel>(debugInterfaceModel, DebugInterfaceModel.PROPERTY_SEND_TEXT, true);
        // sendText = BasicComponentFactory.createTextField(sendTextValueModel, false);

        // set default 20 items in history
        HistoryModel.setMax(20);
        sendText = new HistoryTextField("sendText", false, true);
        Bindings.bind(sendText, sendTextValueModel, false);

        sendText.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                fireTransmit();
            }
        });

        PromptSupport.init(Resources.getString(getClass(), "transmitText.prompt"), null, null, sendText);
        dialogBuilder.append(Resources.getString(getClass(), "transmitText"), sendText, 15);

        dialogBuilder.append(transmitButton);
        transmitButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                fireTransmit();
            }
        });
        transmitButton.setEnabled(false);

        dialogBuilder.appendRow("3dlu");
        dialogBuilder.appendRow("pref");
        dialogBuilder.nextLine(2);

        // create the textfield for filename
        sendFileValueModel =
            new PropertyAdapter<DebugInterfaceModel>(debugInterfaceModel, DebugInterfaceModel.PROPERTY_SEND_FILE, true);
        final ValueModel sendFileConverterModel =
            new ConverterValueModel(sendFileValueModel, new FileStringConverter());
        sendFile = BasicComponentFactory.createTextField(sendFileConverterModel, true);
        sendFile.setEditable(false);
        PromptSupport.init(Resources.getString(getClass(), "transmitFile.prompt"), null, null, sendFile);
        dialogBuilder.append(Resources.getString(getClass(), "transmitFile"), sendFile, 13);

        dialogBuilder.append(selectFileButton);
        selectFileButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                fireSelectFile();
            }
        });

        dialogBuilder.append(transmitFileButton);
        transmitFileButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                fireTransmitFile();
            }
        });
        transmitFileButton.setEnabled(false);

        // add bindings for enable/disable the send button
        PropertyConnector.connect(debugInterfaceModel, DebugInterfaceModel.PROPERTY_TRANSMIT_ENABLED, transmitButton,
            "enabled");
        PropertyConnector.connect(debugInterfaceModel, DebugInterfaceModel.PROPERTY_TRANSMIT_ENABLED,
            transmitFileButton, "enabled");

        PropertyConnector.connect(debugInterfaceModel, DebugInterfaceModel.PROPERTY_DISCONNECTED, connectButton,
            "enabled");
        PropertyConnector.connect(debugInterfaceModel, DebugInterfaceModel.PROPERTY_CONNECTED, disconnectButton,
            "enabled");

        // contentPanel = dialogBuilder.build();
        JPanel contentPanelTemp = dialogBuilder.build();

        JideScrollPane scrollPane = new JideScrollPane(contentPanelTemp);
        contentPanel = scrollPane;

        JPopupMenu popupMenu = new BasicPopupMenu();
        JMenuItem clearConsole = new JMenuItem(Resources.getString(getClass(), "clear_console"));
        clearConsole.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fireClearConsole();
            }
        });
        popupMenu.add(clearConsole);

        JMenuItem saveToFile =
            new JMenuItem(Resources.getString(getClass(), "save_to_file"),
                ImageUtils.createImageIcon(getClass(), "/icons/savetofile.png"));
        saveToFile.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fireSaveToFile(false);
            }
        });
        popupMenu.add(saveToFile);

        JMenuItem saveSelectedToFile =
            new JMenuItem(Resources.getString(getClass(), "save_selected_to_file"),
                ImageUtils.createImageIcon(getClass(), "/icons/saveselectedtofile.png"));
        saveSelectedToFile.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fireSaveToFile(true);
            }
        });
        popupMenu.add(saveSelectedToFile);

        logsArea.setComponentPopupMenu(popupMenu);

        addTimeStamps.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                final boolean timestampsEnabled = e.getStateChange() == ItemEvent.SELECTED;

                fireTimestampsEnabledChanged(timestampsEnabled);
            }
        });

        logToFile.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                final boolean logToFileEnabled = e.getStateChange() == ItemEvent.SELECTED;

                debugInterfaceModel.setLogToFile(logToFileEnabled);
            }
        });

        tc9SpeedSupport.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                final boolean tc9SpeedSupportEnabled = e.getStateChange() == ItemEvent.SELECTED;

                fireTc9SpeedSupportEnabledChanged(tc9SpeedSupportEnabled);
            }
        });
    }

    private void fireTimestampsEnabledChanged(boolean timestampsEnabled) {
        this.timestampsEnabled = timestampsEnabled;
    }

    private void fireTc9SpeedSupportEnabledChanged(boolean tc9SpeedSupportEnabled) {
        this.tc9SpeedSupportEnabled = tc9SpeedSupportEnabled;
    }

    private void fireClearConsole() {
        LOGGER.info("clear the console.");

        logsArea.setText(null);
    }

    private static FileFilter logfileFilter;

    private static final String LOGFILE_EXTENSION = "log";

    // description, suffix for node files
    private String savedLogFilesDescription;

    private void fireSaveToFile(final boolean selectedOnly) {
        LOGGER.info("Save the console content to file.");

        savedLogFilesDescription = Resources.getString(getClass(), "savedLogFilesDescription");
        logfileFilter = new FileNameExtensionFilter(savedLogFilesDescription, LOGFILE_EXTENSION);

        FileDialog dialog = new FileDialog(logsArea, FileDialog.SAVE, null, logfileFilter) {

            @Override
            public void approve(String fileName) {
                try {
                    // setWaitCursor();
                    LOGGER.info("Start saving logfile, fileName: {}", fileName);
                    File file = new File(fileName);
                    if (selectedOnly) {
                        FileUtils.write(file, logsArea.getSelectedText(), Charset.forName("UTF-8"));
                    }
                    else {
                        FileUtils.write(file, logsArea.getText(), Charset.forName("UTF-8"));
                    }
                }
                catch (Exception ex) {
                    LOGGER.warn("Save logfile failed.", ex);

                    throw new RuntimeException("Save logfile failed.");
                }
                finally {
                    // setDefaultCursor();
                }
            }
        };
        dialog.showDialog();
    }

    private void fireRefreshComPorts() {
        // Reload the com port identifiers

        loadPortIdentifiers(debugInterfaceModel);
    }

    private void fireConnect() {
        for (DebugInterfaceViewListener listener : listeners) {
            listener.openConnection();
        }
    }

    private void fireDisconnect() {
        for (DebugInterfaceViewListener listener : listeners) {
            listener.closeConnection();
        }
    }

    private void fireTransmit() {

        for (DebugInterfaceViewListener listener : listeners) {
            listener.transmit();
        }
        // clear the text field
        sendText.setText(null);
    }

    private void fireTransmitFile() {

        boolean modal = true;
        // open the progress dialog
        FileTransferProgressDialog progressDialog = new FileTransferProgressDialog(contentPanel, modal, listeners);

    }

    private void fireSelectFile() {
        final FileDialog dialog = new FileDialog(contentPanel, FileDialog.OPEN, null, ff) {
            @Override
            public void approve(String selectedFile) {
                File file = new File(selectedFile);

                selectedFile = file.getName();

                debugInterfaceModel.setSendFile(file);
            }
        };
        dialog.showDialog();
    }

    @Override
    public DockKey getDockKey() {
        return DockKeys.DOCKKEY_DEBUG_INTERFACE_VIEW;
    }

    @Override
    public Component getComponent() {
        return contentPanel;
    }

    public void addDebugInterfaceViewListener(DebugInterfaceViewListener l) {
        listeners.add(l);
    }

    private int currentLineLen;

    public void addLog(final String logMessage) {

        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {

                addLogMessage(logMessage);
            }
        });
    }

    private final SimpleDateFormat sdf = new SimpleDateFormat("mm:ss.SSS");

    public static final String LOGGER_PANE_NAME = "DebugInterfacePane";

    private static final Logger LOGGER_PANE = LoggerFactory.getLogger(LOGGER_PANE_NAME);

    StringBuilder sbLogger = new StringBuilder();

    // int lenOfTimestamp = 0;

    private void addLogMessage(final String logMessage) {

        try {
            int lines = logsArea.getLineCount();
            if (lines > 500) {
                // remove the first 50 lines
                int end = logsArea.getLineEndOffset(/* lines - */50);
                logsArea.getDocument().remove(0, end);
            }
        }
        catch (BadLocationException ex) {
            LOGGER.warn("Remove some lines from logsArea failed.", ex);
        }

        // ...........\n........
        int beginIndex = 0;
        int index = logMessage.indexOf('\n');
        int lenOfTimestamp = 0;
        if (index > -1) {
            // found line terminator
            while (index > -1) {
                // print the whole line
                String part = logMessage.substring(beginIndex, index);
                // add text to scroll pane
                if (timestampsEnabled && currentLineLen == 0) {
                    // add the timestamp to the line
                    sbLogger.append(sdf.format(new Date()));
                    sbLogger.append(" - ");

                    lenOfTimestamp = 12;

                }
                sbLogger.append(part);

                LOGGER.debug("1. Added part: {}, currentLineLen: {}, sbLogger: {}", part, currentLineLen, sbLogger);

                // the line is complete
                logsArea.append(sbLogger.substring(currentLineLen));
                logsArea.append("\n"/* System.lineSeparator() */);

                LOGGER_PANE.info(sbLogger.toString());

                // complete line added to logs pane, check if we must copy to clipboard for TC9 speed support
                if (tc9SpeedSupportEnabled) {
                    String speed = SpeedSupport.parseSpeed(sbLogger, currentLineLen + lenOfTimestamp);
                    if (StringUtils.isNotBlank(speed)) {
                        LOGGER.info("Copy speed message to clipboard: {}", speed);
                        StringSelection stringSelection = new StringSelection(speed);
                        Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
                        clpbrd.setContents(stringSelection, null);
                    }
                }

                lenOfTimestamp = 0;
                sbLogger.setLength(0);
                currentLineLen = 0;

                beginIndex = index + 1;

                index = logMessage.indexOf('\n', beginIndex);
            }

            lenOfTimestamp = 0;

            if (beginIndex < logMessage.length()) {
                // add text to scroll pane
                String part = logMessage.substring(beginIndex);
                if (timestampsEnabled) {
                    sbLogger.append(sdf.format(new Date()));
                    sbLogger.append(" - ");

                    lenOfTimestamp = 12;
                }
                sbLogger.append(part);

                LOGGER.debug("2. Added part: {}, currentLineLen: {}, sbLogger: {}", part, currentLineLen, sbLogger);

                logsArea.append(sbLogger.substring(currentLineLen));

                currentLineLen = part.length() + lenOfTimestamp;

                lenOfTimestamp = 0;

                LOGGER.debug("Added last part: {}, currentLineLen: {}", part, currentLineLen);
            }
        }
        else {
            // add text to scroll pane
            if (timestampsEnabled && currentLineLen == 0) {
                LOGGER.debug("Add timestamp, currentLineLen: {}", currentLineLen);
                sbLogger.append(sdf.format(new Date()));
                sbLogger.append(" - ");

                lenOfTimestamp = 12;
                // currentLineLen += lenOfTimestamp;
            }

            String toLog = logMessage;
            // split the received data if necessary
            if (logMessage.length() > (120 - currentLineLen)) {
                int start = 0;
                int end = 0;
                String part = null;
                toLog = null;

                end = start + (120 - currentLineLen);

                while (end < logMessage.length()) {
                    // end = start + (120 - currentLineLen);
                    part = StringUtils.substring(logMessage, start, end);
                    LOGGER.debug("Fetched part, currentLineLen {}, part: '{}'", currentLineLen, part);

                    sbLogger.append(part);

                    logsArea.append(sbLogger.substring(currentLineLen));
                    logsArea.append("\n");

                    currentLineLen = 120;

                    LOGGER.debug("Added logMessage: {}, currentLineLen: {}", logMessage, currentLineLen);

                    currentLineLen = 0;
                    sbLogger.setLength(0);

                    start += 120;
                    end = start + (120 - currentLineLen);
                }

                if (start < logMessage.length()) {
                    LOGGER.debug("Keep the remaining part from logMessage, currentLineLen: {}", currentLineLen, start);
                    toLog = StringUtils.substring(logMessage, start);
                }
            }

            if (StringUtils.isNotBlank(toLog)) {
                sbLogger.append(toLog);

                LOGGER.debug("3. Added logMessage: {}, currentLineLen: {}, sbLogger: {}", toLog, currentLineLen,
                    sbLogger);

                logsArea.append(sbLogger.substring(currentLineLen));

                currentLineLen += toLog.length() + lenOfTimestamp;

                LOGGER.debug("Added logMessage: {}, currentLineLen: {}", logMessage, currentLineLen);
            }
        }

        // Update and scroll pane to the bottom

        if (currentLineLen > 120) {
            LOGGER.debug("Append new line to logsArea, currentLineLen: {}", currentLineLen);

            // break the line
            // logsArea.append(sb.toString());
            logsArea.append("\n" /* System.lineSeparator() */);

            LOGGER_PANE.info(sbLogger.toString());

            lenOfTimestamp = 0;
            sbLogger.setLength(0);
            currentLineLen = 0;
        }
        logsArea.invalidate();
    }

    private static final String SUFFIX_HEX = "hex";

    private static final String SUFFIX_EEP = "eep";

    private final FileFilter ff = new FileFilter() {

        @Override
        public boolean accept(File file) {
            boolean result = false;

            if (file != null) {
                if (file.isDirectory()) {
                    result = true;
                }
                else if (FilenameUtils.wildcardMatch(file.getName(), "*." + SUFFIX_HEX)) {
                    result = true;
                }
                else if (FilenameUtils.wildcardMatch(file.getName(), "*." + SUFFIX_EEP)) {
                    result = true;
                }
            }
            return result;
        }

        @Override
        public String getDescription() {
            return Resources.getString(DebugInterfaceView.class, "filter") + " (*." + SUFFIX_HEX + ",*." + SUFFIX_EEP
                + ")";
        }
    };

    private void loadPortIdentifiers(DebugInterfaceModel model) {
        LOGGER.info("Load the comm ports, model: {}", model);
        Set<CommPort> commPorts = new HashSet<CommPort>();

        try {
            // use PortIdentifierUtils because we must load the RXTX libraries
            List<String> portIdentifiers = null;
            if ("SCM".equals(Preferences.getInstance().getSerialPortProvider())) {
                portIdentifiers = ScmPortIdentifierUtils.getPortIdentifiers();
            }
            else {
                portIdentifiers = PortIdentifierUtils.getPortIdentifiers();
            }

            if (portIdentifiers != null) {
                for (String id : portIdentifiers) {
                    LOGGER.info("Add new CommPort with id: {}", id);
                    commPorts.add(new CommPort(id));
                }
            }
        }
        catch (InvalidLibraryException ex) {
            LOGGER.warn(
                "Fetch port identifiers failed. This can be caused because the ext/lib directory of the Java installation contains an old RXTXComm.jar!",
                ex);

            JOptionPane.showMessageDialog(contentPanel,
                Resources.getString(DebugInterfaceView.class, "fetch-port-identifiers-failed",
                    new Object[] { new File(SystemUtils.getJavaHome(), "lib/ext").getPath() }),
                Resources.getString(DebugInterfaceView.class, "title-error"), JOptionPane.ERROR_MESSAGE);
        }
        catch (Exception ex) {
            LOGGER.warn("Fetch port identifiers failed.", ex);
        }
        model.setCommPorts(Arrays.asList(commPorts.toArray(new CommPort[0])));
    }

    private class LineEndingCellRenderer extends DefaultListCellRenderer {
        private static final long serialVersionUID = 1L;

        private Map<String, String> labelMap = new HashMap<>();

        public LineEndingCellRenderer() {
            for (LineEndingEnum lineEndingEnum : LineEndingEnum.values()) {
                String label = Resources.getString(LineEndingEnum.class, lineEndingEnum.getKey());
                labelMap.put(lineEndingEnum.getKey(), label);
            }
        }

        public Component getListCellRendererComponent(
            JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {

            JLabel renderer = (JLabel) super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);

            if (value instanceof LineEndingEnum) {
                LineEndingEnum lineEndingEnum = (LineEndingEnum) value;
                renderer.setText(labelMap.get(lineEndingEnum.getKey()));
            }
            else {
                renderer.setText(null);
            }

            return renderer;
        }
    }

    private static final String SUFFIX_LOG = "log";

    private static final String SUFFIX_TXT = "txt";

    private final FileFilter ffLogFile = new FileFilter() {

        @Override
        public boolean accept(File file) {
            boolean result = false;

            if (file != null) {
                if (file.isDirectory()) {
                    result = true;
                }
                else if (FilenameUtils.wildcardMatch(file.getName(), "*." + SUFFIX_LOG)) {
                    result = true;
                }
                else if (FilenameUtils.wildcardMatch(file.getName(), "*." + SUFFIX_TXT)) {
                    result = true;
                }
            }
            return result;
        }

        @Override
        public String getDescription() {
            return Resources.getString(DebugInterfaceView.class, "filterLogFile") + " (*." + SUFFIX_LOG + ",*."
                + SUFFIX_TXT + ")";
        }
    };

    private void selectLogFile() {

        final FileDialog dialog = new FileDialog(contentPanel, FileDialog.SAVE, null, ffLogFile) {
            @Override
            public void approve(String selectedFile) {
                File file = new File(selectedFile);

                selectedFile = file.getName();

                debugInterfaceModel.setLogFileName(file.toString());
            }
        };
        dialog.showDialog();
    }

}
