package org.bidib.wizard.mvc.dmx.model;

import java.util.LinkedList;
import java.util.List;

import org.bidib.wizard.mvc.main.model.BacklightPort;
import org.bidib.wizard.mvc.main.model.Macro;

import com.jgoodies.binding.beans.Model;

/**
 * The <code>DmxSceneryModel</code> holds all information of the selected node that is needed for DMX modeling. It has a
 * list of the <code>DmxScenery</code> instances.
 */
public class DmxSceneryModel extends Model implements DmxEnvironmentProvider {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_SCENERIES = "sceneries";

    public static final String PROPERTY_DMX_CHANNELS = "dmxChannels";

    public static final String PROPERTY_LIGHTPORTS = "lightPorts";

    public static final String PROPERTY_BACKLIGHTPORTS = "backlightPorts";

    public static final String PROPERTY_MACROS = "macros";

    public static final String PROPERTY_DIMM_STRETCH = "dimmStretch";

    private List<BacklightPort> backlightPorts = new LinkedList<>();

    private List<DmxLightPort> lightPorts = new LinkedList<>();

    private List<DmxScenery> sceneries = new LinkedList<>();

    private List<DmxChannel> dmxChannels = new LinkedList<>();

    private List<Macro> macros = new LinkedList<>();

    private Integer dimmStretch;

    /**
     * @return the backlightPorts
     */
    @Override
    public List<BacklightPort> getBacklightPorts() {
        return backlightPorts;
    }

    /**
     * @param backlightPorts
     *            the backlightPorts to set
     */
    public void setBacklightPorts(List<BacklightPort> backlightPorts) {
        List<BacklightPort> oldValue = new LinkedList<>(this.backlightPorts);

        this.backlightPorts = backlightPorts;
        firePropertyChange(PROPERTY_BACKLIGHTPORTS, oldValue, backlightPorts);
    }

    /**
     * @return the lightPorts
     */
    @Override
    public List<DmxLightPort> getLightPorts() {
        return lightPorts;
    }

    /**
     * @param lightPorts
     *            the lightPorts to set
     */
    public void setLightPorts(List<DmxLightPort> lightPorts) {
        List<DmxLightPort> oldValue = new LinkedList<>(this.lightPorts);

        this.lightPorts = lightPorts;
        firePropertyChange(PROPERTY_LIGHTPORTS, oldValue, lightPorts);
    }

    /**
     * @return the sceneries
     */
    public List<DmxScenery> getSceneries() {
        return sceneries;
    }

    /**
     * @param scenery
     *            the scenery to add
     */
    public void addScenery(DmxScenery scenery) {
        List<DmxScenery> oldValue = this.sceneries;

        List<DmxScenery> sceneries = new LinkedList<>(this.sceneries);
        sceneries.add(scenery);
        this.sceneries = sceneries;

        firePropertyChange(PROPERTY_SCENERIES, oldValue, sceneries);
    }

    /**
     * @param sceneries
     *            the sceneries to set
     */
    public void setSceneries(List<DmxScenery> sceneries) {
        List<DmxScenery> oldValue = new LinkedList<>(this.sceneries);

        this.sceneries = sceneries;

        firePropertyChange(PROPERTY_SCENERIES, oldValue, sceneries);
    }

    /**
     * @return the dmxChannels
     */
    @Override
    public List<DmxChannel> getDmxChannels() {
        return dmxChannels;
    }

    /**
     * @param dmxChannels
     *            the dmxChannels to set
     */
    public void setDmxChannels(List<DmxChannel> dmxChannels) {
        List<DmxChannel> oldValue = new LinkedList<>(this.dmxChannels);

        this.dmxChannels = dmxChannels;

        firePropertyChange(PROPERTY_DMX_CHANNELS, oldValue, dmxChannels);
    }

    /**
     * @return the macros
     */
    public List<Macro> getMacros() {
        return macros;
    }

    /**
     * @param macros
     *            the macros to set
     */
    public void setMacros(List<Macro> macros) {
        List<Macro> oldValue = new LinkedList<>(this.macros);

        this.macros = macros;

        firePropertyChange(PROPERTY_MACROS, oldValue, macros);
    }

    /**
     * @return the dimmStretch
     */
    public Integer getDimmStretch() {
        return dimmStretch;
    }

    /**
     * @param dimmStretch
     *            the dimmStretch to set
     */
    public void setDimmStretch(Integer dimmStretch) {
        Integer oldValue = this.dimmStretch;

        this.dimmStretch = dimmStretch;

        firePropertyChange(PROPERTY_DIMM_STRETCH, oldValue, dimmStretch);
    }
}
