package org.bidib.wizard.mvc.main.model;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.swing.table.DefaultTableModel;

import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.main.model.listener.OutputListener;
import org.bidib.wizard.mvc.main.model.listener.PortListener;
import org.bidib.wizard.mvc.main.model.listener.PortTableLayoutListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class SimplePortTableModel<S extends BidibStatus, P extends Port<S>, L extends PortListener<S>>
    extends DefaultTableModel {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(SimplePortTableModel.class);

    protected final List<L> portListeners = new LinkedList<L>();

    public static final int COLUMN_LABEL = 0;

    public static final int COLUMN_STATUS = 1;

    public static final int COLUMN_PORT_INSTANCE = 2;

    protected String[] columnNames;

    protected boolean isSetPortValuesDisabled;

    private List<PortTableLayoutListener> portTableLayoutListeners = new LinkedList<>();

    public SimplePortTableModel() {
        initialize();
        setColumnIdentifiers(columnNames);
    }

    protected void initialize() {
        columnNames =
            new String[] { Resources.getString(getClass(), "label"), Resources.getString(getClass(), "test"), null };
    }

    protected abstract int getColumnPortInstance();

    protected void setPortValuesProcessing(boolean skip) {
        isSetPortValuesDisabled = skip;
    }

    public void addPortTableLayoutListener(PortTableLayoutListener listener) {
        portTableLayoutListeners.add(listener);
    }

    protected void firePortTableLayoutChanged() {
        LOGGER.info("The port table layout has been changed.");
        for (PortTableLayoutListener listener : portTableLayoutListeners) {

            listener.portTableLayoutChanged();
        }
    }

    public void addPortListener(L listener) {
        portListeners.add(listener);
    }

    public List<L> getPortListeners() {
        return Collections.unmodifiableList(portListeners);
    }

    public void addRow(P port) {
        if (port != null) {
            Object[] rowData = new Object[getColumnCount()];

            rowData[COLUMN_LABEL] = port.toString();
            rowData[COLUMN_STATUS] = port.getStatus();
            rowData[COLUMN_PORT_INSTANCE] = port;
            addRow(rowData);
        }
    }

    protected void fireLabelChanged(Port<S> port, String value) {
        LOGGER.info("fire label changed: {}", value);
        for (PortListener<S> l : portListeners) {
            l.labelChanged(port, value);
        }
    }

    protected void fireTestButtonPressed(Port<S> port) {
        LOGGER.warn("Test button pressed, port: {}", port);
        for (PortListener<S> l : portListeners) {
            if (l instanceof OutputListener) {
                OutputListener<S> listener = (OutputListener<S>) l;
                listener.testButtonPressed(port);
            }
        }
    }

    @Override
    public Class<?> getColumnClass(int column) {
        switch (column) {
            case COLUMN_LABEL:
                return String.class;
            default:
                return Object.class;
        }
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        if (column == COLUMN_LABEL) {
            // the label can always be changed.
            return true;
        }
        return super.isCellEditable(row, column);
    }

    @Override
    public void setValueAt(Object value, int row, int column) {
        // final Object o = getValueAt(row, 2);
        //
        // if (!isSetPortValuesDisabled && o instanceof Port) {
        // final P port = (P) o;
        //
        // switch (column) {
        // case COLUMN_LABEL:
        // if (value instanceof String) {
        // port.setLabel((String) value);
        // super.setValueAt(port.toString(), row, column);
        // fireLabelChanged(port, port.getLabel());
        // }
        // else {
        // super.setValueAt(value, row, column);
        // }
        // break;
        // case COLUMN_STATUS:
        // if (value instanceof BidibStatus) {
        // port.setStatus((S) value);
        // super.setValueAt(value, row, column);
        // fireTestButtonPressed(port);
        // }
        // else {
        // super.setValueAt(value, row, column);
        // }
        // break;
        // default:
        // super.setValueAt(value, row, column);
        // }
        // }
        // else {
        super.setValueAt(value, row, column);
        // }
    }
}
