package org.bidib.wizard.mvc.preferences.view.panel;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.common.model.CommPort;
import org.bidib.wizard.mvc.common.model.PreferencesPortType;
import org.bidib.wizard.mvc.common.model.PreferencesPortType.ConnectionPortType;
import org.bidib.wizard.mvc.preferences.model.PreferencesModel;
import org.bidib.wizard.mvc.preferences.model.listener.PreferencesModelListener;
import org.bidib.wizard.mvc.preferences.view.listener.PreferencesViewListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.adapter.ComboBoxAdapter;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.debug.FormDebugPanel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.FormLayout;

public class ConnectionPanel {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConnectionPanel.class);

    private static final String ENCODED_DIALOG_COLUMN_SPECS = "pref, 3dlu, fill:50dlu:grow";

    private static final String ENCODED_DIALOG_ROW_SPECS = "top:pref, 8dlu, top:pref, 8dlu, top:pref";

    private final Collection<PreferencesViewListener> listeners = new LinkedList<PreferencesViewListener>();

    private final PreferencesModel model;

    private CommPortRadioPanel commPortRadioPanel;

    private UdpPortRadioPanel udpPortRadioPanel;

    private TcpPortRadioPanel tcpPortRadioPanel;

    private SerialOverTcpPortRadioPanel plainTcpPortRadioPanel;

    private JCheckBox checkSerial;

    private JCheckBox checkUdp;

    private JCheckBox checkTcp;

    private JCheckBox checkSerialOverTcp;

    private JPanel contentPanel;

    public ConnectionPanel(final PreferencesModel model) {
        this.model = model;
    }

    public JPanel createPanel() {

        checkSerial = new JCheckBox(Resources.getString(MiscPanel.class, "serialPort") + ":", model.isSerialEnabled());
        checkSerial.setContentAreaFilled(false);
        checkUdp = new JCheckBox(Resources.getString(MiscPanel.class, "udpPort") + ":", model.isUdpEnabled());
        checkUdp.setContentAreaFilled(false);
        checkTcp = new JCheckBox(Resources.getString(MiscPanel.class, "tcpPort") + ":", model.isTcpEnabled());
        checkTcp.setContentAreaFilled(false);

        checkSerialOverTcp =
            new JCheckBox(Resources.getString(MiscPanel.class, "serialOverTcpPort") + ":", model.isPlainTcpEnabled());
        checkSerialOverTcp.setContentAreaFilled(false);

        DefaultFormBuilder dialogBuilder = null;
        boolean debugDialog = false;
        if (debugDialog) {
            JPanel panel = new FormDebugPanel();
            dialogBuilder =
                new DefaultFormBuilder(new FormLayout(ENCODED_DIALOG_COLUMN_SPECS, ENCODED_DIALOG_ROW_SPECS), panel);
        }
        else {
            JPanel panel = new JPanel(new BorderLayout());
            dialogBuilder =
                new DefaultFormBuilder(new FormLayout(ENCODED_DIALOG_COLUMN_SPECS, ENCODED_DIALOG_ROW_SPECS), panel);
        }
        dialogBuilder.border(Borders.TABBED_DIALOG);

        dialogBuilder.append(checkSerial);

        final JComboBox<CommPort> commPorts = new JComboBox<CommPort>();

        ButtonGroup group = new ButtonGroup();
        commPortRadioPanel = new CommPortRadioPanel(group, commPorts);
        dialogBuilder.append(commPortRadioPanel);
        dialogBuilder.nextLine(2);

        dialogBuilder.append(checkUdp);

        udpPortRadioPanel = new UdpPortRadioPanel(group);
        dialogBuilder.append(udpPortRadioPanel);
        dialogBuilder.nextLine(2);

        // tcp panel
        dialogBuilder.append(checkTcp);

        tcpPortRadioPanel = new TcpPortRadioPanel(group);
        dialogBuilder.append(tcpPortRadioPanel);

        // plain tcp panel
        dialogBuilder.append(checkSerialOverTcp);

        plainTcpPortRadioPanel = new SerialOverTcpPortRadioPanel(group);
        dialogBuilder.append(plainTcpPortRadioPanel);

        udpPortRadioPanel.addPeer(commPortRadioPanel);
        udpPortRadioPanel.addPeer(tcpPortRadioPanel);
        udpPortRadioPanel.addPeer(plainTcpPortRadioPanel);

        commPortRadioPanel.addPeer(tcpPortRadioPanel);
        commPortRadioPanel.addPeer(udpPortRadioPanel);
        commPortRadioPanel.addPeer(plainTcpPortRadioPanel);

        tcpPortRadioPanel.addPeer(commPortRadioPanel);
        tcpPortRadioPanel.addPeer(udpPortRadioPanel);
        tcpPortRadioPanel.addPeer(plainTcpPortRadioPanel);

        plainTcpPortRadioPanel.addPeer(commPortRadioPanel);
        plainTcpPortRadioPanel.addPeer(udpPortRadioPanel);
        plainTcpPortRadioPanel.addPeer(tcpPortRadioPanel);

        // set the comm ports after creation of the radio panel
        setCommPorts(commPorts);
        commPortRadioPanel.setEnabled(model.isSerialEnabled());
        udpPortRadioPanel.setEnabled(model.isUdpEnabled());
        tcpPortRadioPanel.setEnabled(model.isTcpEnabled());
        plainTcpPortRadioPanel.setEnabled(model.isPlainTcpEnabled());

        commPortRadioPanel.setInitialValues(model);
        udpPortRadioPanel.setInitialValues(model);
        tcpPortRadioPanel.setInitialValues(model);
        plainTcpPortRadioPanel.setInitialValues(model);

        // select the radio button
        PreferencesPortType selectedPortType = model.getSelectedPortType();
        if (selectedPortType != null) {
            switch (selectedPortType.getConnectionPortType()) {
                case SerialPort:
                case SerialSymLink:
                case SerialSimulation:
                    commPortRadioPanel.select(model.getSelectedPortType());
                    break;
                case TcpPort:
                case TcpSimulation:
                    tcpPortRadioPanel.select(model.getSelectedPortType());
                    break;
                case SerialOverTcpPort:
                case SerialOverTcpSimulation:
                    plainTcpPortRadioPanel.select(model.getSelectedPortType());
                    break;
                default:
                    udpPortRadioPanel.select(model.getSelectedPortType());
                    break;
            }
        }

        SelectionInList<String> serialProviderSelection =
            new SelectionInList<String>((ListModel<String>) model.getSerialProviderList());

        ValueModel selectionHolderSerialProvider =
            new PropertyAdapter<PreferencesModel>(model, PreferencesModel.PROPERTY_SELECTED_SERIALPORT_PROVIDER, true);

        ComboBoxAdapter<String> comboBoxAdapterSerialProvider =
            new ComboBoxAdapter<String>(serialProviderSelection, selectionHolderSerialProvider);
        final JComboBox<String> comboSerialProvider = new JComboBox<>();
        comboSerialProvider.setModel(comboBoxAdapterSerialProvider);

        dialogBuilder.append(Resources.getString(MiscPanel.class, "serialPortProvider") + ":", comboSerialProvider);
        dialogBuilder.nextLine();

        checkSerial.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                final boolean serialEnabled = e.getStateChange() == ItemEvent.SELECTED;

                commPortRadioPanel.setEnabled(serialEnabled);
                fireSerialEnabledChanged(serialEnabled);
            }
        });
        checkUdp.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                final boolean udpEnabled = e.getStateChange() == ItemEvent.SELECTED;

                udpPortRadioPanel.setEnabled(udpEnabled);
                fireUdpEnabledChanged(udpEnabled);
            }
        });
        checkTcp.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                final boolean tcpEnabled = e.getStateChange() == ItemEvent.SELECTED;

                tcpPortRadioPanel.setEnabled(tcpEnabled);
                fireTcpEnabledChanged(tcpEnabled);
            }
        });
        checkSerialOverTcp.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                final boolean plainTcpEnabled = e.getStateChange() == ItemEvent.SELECTED;

                plainTcpPortRadioPanel.setEnabled(plainTcpEnabled);
                firePlainTcpEnabledChanged(plainTcpEnabled);
            }
        });

        // add preferences listener
        model.addPreferencesModelListener(new PreferencesModelListener() {
            @Override
            public void commPortsChanged() {
                LOGGER.debug("Preferences model listener, commPortsChanged.");
                setCommPorts(commPorts);
            }
        });

        contentPanel = dialogBuilder.build();
        return contentPanel;
    }

    public void addPreferencesViewListener(PreferencesViewListener l) {
        listeners.add(l);
    }

    private void fireSelectedPortTypeChanged(PreferencesPortType portType) {
        LOGGER.debug("Selected port type has changed: {}", portType);
        for (PreferencesViewListener l : listeners) {
            l.selectedPortTypeChanged(portType);
        }
    }

    private void firePrevSelectedUdpHostChanged(String udpHost) {
        LOGGER.debug("Selected udpHost has changed: {}", udpHost);
        for (PreferencesViewListener l : listeners) {
            l.selectedUdpHostChanged(udpHost);
        }
    }

    private void firePrevSelectedTcpHostChanged(String tcpHost) {
        LOGGER.debug("Selected tcpHost has changed: {}", tcpHost);
        for (PreferencesViewListener l : listeners) {
            l.selectedTcpHostChanged(tcpHost);
        }
    }

    private void firePrevSelectedSerialSymLinkChanged(String serialSymLink) {
        LOGGER.debug("Selected serialSymLink has changed: {}", serialSymLink);
        for (PreferencesViewListener l : listeners) {
            l.selectedSerialSymLinkChanged(serialSymLink);
        }
    }

    private void firePrevSelectedComPortChanged(String comPort) {
        LOGGER.debug("Selected COM port has changed: {}", comPort);
        for (PreferencesViewListener l : listeners) {
            l.selectedComPortChanged(comPort);
        }
    }

    private void fireSerialEnabledChanged(boolean serialEnabled) {
        for (PreferencesViewListener l : listeners) {
            l.serialEnabledChanged(serialEnabled);
        }
    }

    private void fireUdpEnabledChanged(boolean udpEnabled) {
        for (PreferencesViewListener l : listeners) {
            l.udpEnabledChanged(udpEnabled);
        }
    }

    private void fireTcpEnabledChanged(boolean tcpEnabled) {
        for (PreferencesViewListener l : listeners) {
            l.tcpEnabledChanged(tcpEnabled);
        }
    }

    private void firePlainTcpEnabledChanged(boolean plainTcpEnabled) {
        for (PreferencesViewListener l : listeners) {
            l.plainTcpEnabledChanged(plainTcpEnabled);
        }
    }

    private void setCommPorts(JComboBox<CommPort> commPortsCombo) {
        Set<CommPort> commPorts = new HashSet<CommPort>();
        CommPort activeCommPort = null;
        String activeCommPortName = null;

        PreferencesPortType portType = model.getSelectedPortType();
        if (portType != null && ConnectionPortType.SerialPort.equals(portType.getConnectionPortType())) {
            activeCommPortName = portType.getConnectionName();
            if ("null".equals(activeCommPortName)) {
                activeCommPortName = null;
            }
            LOGGER.debug("The activeCommPortName: {}", activeCommPortName);
        }

        for (CommPort commPort : model.getCommPorts()) {
            commPorts.add(commPort);
            if (activeCommPortName != null && activeCommPortName.equals(commPort.getName())) {
                activeCommPort = commPort;
            }
        }

        LOGGER.debug("Add empty item if someone wants to release the port.");
        CommPort commPortNone = new CommPort(CommPort.NONE);
        commPorts.add(commPortNone);

        commPortsCombo.setModel(new DefaultComboBoxModel<CommPort>(commPorts.toArray(new CommPort[0])));

        if (activeCommPort != null) {
            LOGGER.debug("Select the active commport: {}", activeCommPort);
            commPortRadioPanel.setSelectedPort(activeCommPort);
        }
        else {
            // on linux and mac we must support symlinks ...
            if (StringUtils.isNotBlank(activeCommPortName) && !CommPort.NONE.equals(activeCommPortName)) {
                LOGGER.debug("Set the active symlink: {}", activeCommPortName);
                commPortRadioPanel.setSymlink(activeCommPortName);
            }
            else {
                LOGGER.debug("Select <none> item because no default active port is set.");
                commPortRadioPanel.setSelectedPort(commPortNone);
            }
        }

        // do not call fireCommPortChanged
    }

    private interface PortRadioPanel extends ActionListener {
        void selectDefault();
    }

    private class UdpPortRadioPanel extends JPanel implements PortRadioPanel {
        private static final long serialVersionUID = 1L;

        private JTextField textField;

        private JLabel labelUdpMock;

        private JRadioButton textFieldButton;

        private JRadioButton udpMockButton;

        private List<PortRadioPanel> peer = new ArrayList<>();

        public void addPeer(PortRadioPanel peer) {
            this.peer.add(peer);
        }

        public UdpPortRadioPanel(ButtonGroup group) {
            super(new GridBagLayout());
            setOpaque(false);

            textFieldButton = new JRadioButton("");
            textFieldButton.setContentAreaFilled(false);
            udpMockButton = new JRadioButton("");
            udpMockButton.setContentAreaFilled(false);

            // Group the radio buttons.
            group.add(textFieldButton);
            group.add(udpMockButton);

            textField = new JTextField(20);
            textField.getDocument().addDocumentListener(new DocumentListener() {

                @Override
                public void removeUpdate(DocumentEvent e) {
                    setText();
                }

                @Override
                public void insertUpdate(DocumentEvent e) {
                    setText();
                }

                @Override
                public void changedUpdate(DocumentEvent e) {
                    setText();
                }

                private void setText() {
                    if (textFieldButton.isSelected()) {
                        PreferencesPortType portType = PreferencesPortType.getValue(ConnectionPortType.UdpPort.name());
                        portType.setConnectionName(textField.getText());
                        fireSelectedPortTypeChanged(portType);

                    }
                    firePrevSelectedUdpHostChanged(textField.getText());
                }
            });

            labelUdpMock = new JLabel(Resources.getString(MiscPanel.class, "udpMock"));
            labelUdpMock.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    udpMockButton.setSelected(true);

                    // set selected does not fire the action event and therefore we must fire the change
                    PreferencesPortType portType =
                        PreferencesPortType.getValue(ConnectionPortType.UdpSimulation.name());
                    fireSelectedPortTypeChanged(portType);
                }
            });

            textFieldButton.addActionListener(UdpPortRadioPanel.this);
            udpMockButton.addActionListener(UdpPortRadioPanel.this);

            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridy = 0;
            gbc.insets = new Insets(0, 0, 5, 0);
            add(textFieldButton, gbc);

            gbc.gridx = 1;
            gbc.fill = GridBagConstraints.HORIZONTAL;
            gbc.weightx = 1.0;
            add(textField, gbc);

            gbc.gridx = 2;
            gbc.fill = GridBagConstraints.HORIZONTAL;
            gbc.weightx = 0.0;
            add(new JLabel(":62875"), gbc);

            gbc = new GridBagConstraints();
            gbc.gridy = 1;
            add(udpMockButton, gbc);

            gbc.gridx = 1;
            gbc.fill = GridBagConstraints.HORIZONTAL;
            gbc.gridwidth = 2;
            gbc.weightx = 1.0;
            add(labelUdpMock, gbc);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            labelUdpMock.setEnabled(udpMockButton.isEnabled());
            textField.setEnabled(textFieldButton.isEnabled());

            if (e != null && (e.getSource() == textFieldButton || e.getSource() == udpMockButton)) {
                if (CollectionUtils.isNotEmpty(peer)) {
                    LOGGER.trace("Update peers from udpPort.");
                    for (PortRadioPanel p : peer) {
                        p.actionPerformed(e);
                    }
                }
            }

            if (e != null) {
                if (e.getSource() == udpMockButton) {
                    PreferencesPortType portType =
                        PreferencesPortType.getValue(ConnectionPortType.UdpSimulation.name());
                    fireSelectedPortTypeChanged(portType);
                }
                else if (e.getSource() == textFieldButton) {
                    PreferencesPortType portType = PreferencesPortType.getValue(ConnectionPortType.UdpPort.name());
                    portType.setConnectionName(textField.getText());
                    fireSelectedPortTypeChanged(portType);
                }
            }
        }

        @Override
        public void setEnabled(boolean enabled) {
            boolean old = isEnabled();
            if (old != enabled) {
                super.setEnabled(enabled);

                textFieldButton.setEnabled(enabled);
                udpMockButton.setEnabled(enabled);
                actionPerformed(null);

                if (!enabled) {
                    // check if we we have a selected option on this panel, if yes, remove it by selecting the default
                    // option of the peer
                    if (udpMockButton.isSelected() || textFieldButton.isSelected()) {
                        if (CollectionUtils.isNotEmpty(peer)) {
                            for (PortRadioPanel p : peer) {
                                p.selectDefault();
                            }
                        }
                    }
                }
            }
        }

        public void selectDefault() {
            udpMockButton.setSelected(true);
            PreferencesPortType portType = PreferencesPortType.getValue(ConnectionPortType.UdpSimulation.name());
            fireSelectedPortTypeChanged(portType);
        }

        public void setInitialValues(PreferencesModel model) {
            textField.setText(model.getPreviousSelectedUdpHost());
        }

        public void select(PreferencesPortType portType) {
            switch (portType.getConnectionPortType()) {
                case UdpPort:
                    textFieldButton.setSelected(true);
                    textField.setText(portType.getConnectionName());
                    break;
                default:
                    udpMockButton.setSelected(true);
                    break;
            }
        }
    }

    private class TcpPortRadioPanel extends JPanel implements PortRadioPanel {
        private static final long serialVersionUID = 1L;

        public static final String DEFAULT_TCP_PORT = "62875";

        public static final String DEFAULT_TCP_HOST = "localhost";

        private JTextField textField;

        private JTextField portTextField;

        private JLabel labelTcpMock;

        private JRadioButton textFieldButton;

        private JRadioButton tcpMockButton;

        private List<PortRadioPanel> peer = new ArrayList<>();

        public void addPeer(PortRadioPanel peer) {
            this.peer.add(peer);
        }

        public TcpPortRadioPanel(ButtonGroup group) {
            super(new GridBagLayout());
            setOpaque(false);

            textFieldButton = new JRadioButton("");
            textFieldButton.setContentAreaFilled(false);
            tcpMockButton = new JRadioButton("");
            tcpMockButton.setContentAreaFilled(false);

            // Group the radio buttons.
            group.add(textFieldButton);
            group.add(tcpMockButton);

            textField = new JTextField(20);
            portTextField = new JTextField(5);
            portTextField.setText(DEFAULT_TCP_PORT);
            final DocumentListener documentListener = new DocumentListener() {

                @Override
                public void removeUpdate(DocumentEvent e) {
                    setText();
                }

                @Override
                public void insertUpdate(DocumentEvent e) {
                    setText();
                }

                @Override
                public void changedUpdate(DocumentEvent e) {
                    setText();
                }

                private void setText() {
                    if (textFieldButton.isSelected()) {
                        PreferencesPortType portType = PreferencesPortType.getValue(ConnectionPortType.TcpPort.name());

                        String host =
                            (StringUtils.isNotBlank(textField.getText()) ? textField.getText() : DEFAULT_TCP_HOST);
                        String port =
                            (StringUtils.isNotBlank(portTextField.getText()) ? portTextField.getText()
                                : DEFAULT_TCP_PORT);
                        String connectionName = String.format("%1$s:%2$s", host, port);
                        portType.setConnectionName(connectionName);
                        fireSelectedPortTypeChanged(portType);

                    }
                    firePrevSelectedTcpHostChanged(textField.getText());
                }
            };
            textField.getDocument().addDocumentListener(documentListener);
            portTextField.getDocument().addDocumentListener(documentListener);

            labelTcpMock = new JLabel(Resources.getString(MiscPanel.class, "tcpMock"));
            labelTcpMock.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    tcpMockButton.setSelected(true);

                    // set selected does not fire the action event and therefore we must fire the change
                    PreferencesPortType portType =
                        PreferencesPortType.getValue(ConnectionPortType.TcpSimulation.name());
                    fireSelectedPortTypeChanged(portType);
                }
            });

            textFieldButton.addActionListener(TcpPortRadioPanel.this);
            tcpMockButton.addActionListener(TcpPortRadioPanel.this);

            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridy = 0;
            gbc.insets = new Insets(0, 0, 5, 0);
            add(textFieldButton, gbc);

            gbc.gridx = 1;
            gbc.fill = GridBagConstraints.HORIZONTAL;
            gbc.weightx = 1.0;
            add(textField, gbc);

            gbc.gridx = 2;
            gbc.weightx = 0.0;
            add(new JLabel(":"), gbc);

            gbc.gridx = 3;
            gbc.fill = GridBagConstraints.HORIZONTAL;
            gbc.weightx = 1.0;
            add(portTextField, gbc);

            gbc = new GridBagConstraints();
            gbc.gridy = 1;
            add(tcpMockButton, gbc);

            gbc.gridx = 1;
            gbc.fill = GridBagConstraints.HORIZONTAL;
            gbc.gridwidth = 2;
            gbc.weightx = 1.0;
            add(labelTcpMock, gbc);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            labelTcpMock.setEnabled(tcpMockButton.isEnabled());
            textField.setEnabled(textFieldButton.isEnabled());
            portTextField.setEnabled(textFieldButton.isEnabled());

            if (e != null && (e.getSource() == textFieldButton || e.getSource() == tcpMockButton)) {
                if (peer != null) {
                    if (CollectionUtils.isNotEmpty(peer)) {
                        LOGGER.trace("Update peers from tcpPort.");
                        for (PortRadioPanel p : peer) {
                            p.actionPerformed(e);
                        }
                    }
                }
            }

            if (e != null) {
                if (e.getSource() == tcpMockButton) {
                    PreferencesPortType portType =
                        PreferencesPortType.getValue(ConnectionPortType.TcpSimulation.name());
                    fireSelectedPortTypeChanged(portType);
                }
                else if (e.getSource() == textFieldButton) {
                    PreferencesPortType portType = PreferencesPortType.getValue(ConnectionPortType.TcpPort.name());
                    String connectionName = String.format("%1$s:%2$s", textField.getText(), portTextField.getText());
                    portType.setConnectionName(connectionName);
                    fireSelectedPortTypeChanged(portType);
                }
            }
        }

        @Override
        public void setEnabled(boolean enabled) {
            boolean old = isEnabled();
            if (old != enabled) {
                super.setEnabled(enabled);

                textFieldButton.setEnabled(enabled);
                tcpMockButton.setEnabled(enabled);
                actionPerformed(null);

                if (!enabled) {
                    // check if we we have a selected option on this panel, if yes, remove it by selecting the default
                    // option of the peer
                    if (tcpMockButton.isSelected() || textFieldButton.isSelected()) {
                        if (CollectionUtils.isNotEmpty(peer)) {
                            for (PortRadioPanel p : peer) {
                                p.selectDefault();
                            }
                        }
                    }
                }
            }
        }

        public void selectDefault() {
            tcpMockButton.setSelected(true);
            PreferencesPortType portType = PreferencesPortType.getValue(ConnectionPortType.TcpSimulation.name());
            fireSelectedPortTypeChanged(portType);
        }

        public void setInitialValues(PreferencesModel model) {

            String prevSelectedHost = model.getPreviousSelectedTcpHost();
            if (StringUtils.isNotEmpty(prevSelectedHost)) {
                String[] splited = prevSelectedHost.split(":");

                textField.setText(splited[0]);
                if (splited.length > 1) {
                    portTextField.setText(splited[1]);
                }
            }
        }

        public void select(PreferencesPortType portType) {
            switch (portType.getConnectionPortType()) {
                case TcpPort:
                    textFieldButton.setSelected(true);

                    String prevSelectedHost = portType.getConnectionName();
                    if (StringUtils.isNotEmpty(prevSelectedHost)) {
                        String[] splited = prevSelectedHost.split(":");

                        textField.setText(splited[0]);
                        if (splited.length > 1) {
                            portTextField.setText(splited[1]);
                        }
                    }
                    break;
                default:
                    tcpMockButton.setSelected(true);
                    break;
            }
        }
    }

    private class SerialOverTcpPortRadioPanel extends JPanel implements PortRadioPanel {
        private static final long serialVersionUID = 1L;

        private JTextField textField;

        private JLabel labelSerialOverTcpMock;

        private JRadioButton textFieldButton;

        private JRadioButton serialOverTcpMockButton;

        private List<PortRadioPanel> peer = new ArrayList<>();

        public void addPeer(PortRadioPanel peer) {
            this.peer.add(peer);
        }

        public SerialOverTcpPortRadioPanel(ButtonGroup group) {
            super(new GridBagLayout());
            setOpaque(false);

            textFieldButton = new JRadioButton("");
            textFieldButton.setContentAreaFilled(false);
            serialOverTcpMockButton = new JRadioButton("");
            serialOverTcpMockButton.setContentAreaFilled(false);

            // Group the radio buttons.
            group.add(textFieldButton);
            group.add(serialOverTcpMockButton);

            textField = new JTextField(20);
            textField.getDocument().addDocumentListener(new DocumentListener() {

                @Override
                public void removeUpdate(DocumentEvent e) {
                    setText();
                }

                @Override
                public void insertUpdate(DocumentEvent e) {
                    setText();
                }

                @Override
                public void changedUpdate(DocumentEvent e) {
                    setText();
                }

                private void setText() {
                    if (textFieldButton.isSelected()) {
                        PreferencesPortType portType =
                            PreferencesPortType.getValue(ConnectionPortType.SerialOverTcpPort.name());
                        portType.setConnectionName(textField.getText());
                        fireSelectedPortTypeChanged(portType);

                    }
                    firePrevSelectedTcpHostChanged(textField.getText());
                }
            });

            labelSerialOverTcpMock = new JLabel(Resources.getString(MiscPanel.class, "serialOverTcpMock"));
            labelSerialOverTcpMock.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    serialOverTcpMockButton.setSelected(true);

                    // set selected does not fire the action event and therefore we must fire the change
                    PreferencesPortType portType =
                        PreferencesPortType.getValue(ConnectionPortType.SerialOverTcpSimulation.name());
                    fireSelectedPortTypeChanged(portType);
                }
            });

            textFieldButton.addActionListener(SerialOverTcpPortRadioPanel.this);
            serialOverTcpMockButton.addActionListener(SerialOverTcpPortRadioPanel.this);

            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridy = 0;
            gbc.insets = new Insets(0, 0, 5, 0);
            add(textFieldButton, gbc);

            gbc.gridx = 1;
            gbc.fill = GridBagConstraints.HORIZONTAL;
            gbc.weightx = 1.0;
            add(textField, gbc);

            gbc.gridx = 2;
            gbc.fill = GridBagConstraints.HORIZONTAL;
            gbc.weightx = 0.0;
            add(new JLabel(":62875"), gbc);

            gbc = new GridBagConstraints();
            gbc.gridy = 1;
            add(serialOverTcpMockButton, gbc);

            gbc.gridx = 1;
            gbc.fill = GridBagConstraints.HORIZONTAL;
            gbc.gridwidth = 2;
            gbc.weightx = 1.0;
            add(labelSerialOverTcpMock, gbc);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            labelSerialOverTcpMock.setEnabled(serialOverTcpMockButton.isEnabled());
            textField.setEnabled(textFieldButton.isEnabled());

            if (e != null && (e.getSource() == textFieldButton || e.getSource() == serialOverTcpMockButton)) {
                if (peer != null) {
                    if (CollectionUtils.isNotEmpty(peer)) {
                        LOGGER.trace("Update peers from plainTcpPort.");
                        for (PortRadioPanel p : peer) {
                            p.actionPerformed(e);
                        }
                    }
                }
            }

            if (e != null) {
                if (e.getSource() == serialOverTcpMockButton) {
                    PreferencesPortType portType =
                        PreferencesPortType.getValue(ConnectionPortType.SerialOverTcpSimulation.name());
                    fireSelectedPortTypeChanged(portType);
                }
                else if (e.getSource() == textFieldButton) {
                    PreferencesPortType portType =
                        PreferencesPortType.getValue(ConnectionPortType.SerialOverTcpPort.name());
                    portType.setConnectionName(textField.getText());
                    fireSelectedPortTypeChanged(portType);
                }
            }
        }

        @Override
        public void setEnabled(boolean enabled) {
            boolean old = isEnabled();
            if (old != enabled) {
                super.setEnabled(enabled);

                textFieldButton.setEnabled(enabled);
                serialOverTcpMockButton.setEnabled(enabled);
                actionPerformed(null);

                if (!enabled) {
                    // check if we we have a selected option on this panel, if yes, remove it by selecting the default
                    // option of the peer
                    if (serialOverTcpMockButton.isSelected() || textFieldButton.isSelected()) {
                        if (CollectionUtils.isNotEmpty(peer)) {
                            for (PortRadioPanel p : peer) {
                                p.selectDefault();
                            }
                        }
                    }
                }
            }
        }

        public void selectDefault() {
            serialOverTcpMockButton.setSelected(true);
            PreferencesPortType portType =
                PreferencesPortType.getValue(ConnectionPortType.SerialOverTcpSimulation.name());
            fireSelectedPortTypeChanged(portType);
        }

        public void setInitialValues(PreferencesModel model) {
            textField.setText(model.getPreviousSelectedTcpHost());
        }

        public void select(PreferencesPortType portType) {
            switch (portType.getConnectionPortType()) {
                case SerialOverTcpPort:
                    textFieldButton.setSelected(true);
                    textField.setText(portType.getConnectionName());
                    break;
                default:
                    serialOverTcpMockButton.setSelected(true);
                    break;
            }
        }
    }

    private class CommPortRadioPanel extends JPanel implements PortRadioPanel {
        private static final long serialVersionUID = 1L;

        private JComboBox<CommPort> commPorts;

        private JTextField textFieldSerialSymLink;

        private JLabel labelSerialMock;

        private JRadioButton comboButton;

        private JRadioButton textFieldButton;

        private JRadioButton serialMockButton;

        private List<PortRadioPanel> peer = new ArrayList<>();

        public void addPeer(PortRadioPanel peer) {
            this.peer.add(peer);
        }

        public CommPortRadioPanel(ButtonGroup group, JComboBox<CommPort> commPorts) {
            super(new GridBagLayout());
            setOpaque(false);

            this.commPorts = commPorts;

            comboButton = new JRadioButton("");
            comboButton.setContentAreaFilled(false);
            textFieldButton = new JRadioButton("");
            textFieldButton.setContentAreaFilled(false);
            serialMockButton = new JRadioButton("");
            serialMockButton.setContentAreaFilled(false);

            // Group the radio buttons.
            group.add(comboButton);
            group.add(textFieldButton);
            group.add(serialMockButton);

            textFieldSerialSymLink = new JTextField(20);
            textFieldSerialSymLink.getDocument().addDocumentListener(new DocumentListener() {

                @Override
                public void removeUpdate(DocumentEvent e) {
                    setText();
                }

                @Override
                public void insertUpdate(DocumentEvent e) {
                    setText();
                }

                @Override
                public void changedUpdate(DocumentEvent e) {
                    setText();
                }

                private void setText() {
                    if (textFieldButton.isSelected()) {
                        PreferencesPortType portType =
                            PreferencesPortType.getValue(ConnectionPortType.SerialSymLink.name());
                        portType.setConnectionName(textFieldSerialSymLink.getText());
                        fireSelectedPortTypeChanged(portType);
                    }

                    firePrevSelectedSerialSymLinkChanged(textFieldSerialSymLink.getText());
                }
            });

            this.commPorts.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    CommPort commPort = (CommPort) CommPortRadioPanel.this.commPorts.getSelectedItem();
                    LOGGER.debug("The selection of the comm port has changed: {}", commPort);

                    if (commPort != null && !CommPort.NONE.equals(commPort.getName())) {
                        LOGGER.info("The COM port was selected, auto select the COM port radio button.");
                        comboButton.setSelected(true);
                    }

                    if (comboButton.isSelected()) {

                        PreferencesPortType portType =
                            PreferencesPortType.getValue(ConnectionPortType.SerialPort.name());
                        portType.setConnectionName(commPort != null ? commPort.getName() : "");
                        LOGGER.info("Selectced port: {}", portType);
                        fireSelectedPortTypeChanged(portType);
                    }
                    firePrevSelectedComPortChanged(commPort != null ? commPort.getName() : "");
                }
            });

            labelSerialMock = new JLabel(Resources.getString(MiscPanel.class, "serialMock"));
            labelSerialMock.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    serialMockButton.setSelected(true);

                    // set selected does not fire the action event and therefore we must fire the change
                    PreferencesPortType portType =
                        PreferencesPortType.getValue(ConnectionPortType.SerialSimulation.name());
                    fireSelectedPortTypeChanged(portType);
                }
            });

            comboButton.addActionListener(CommPortRadioPanel.this);
            textFieldButton.addActionListener(CommPortRadioPanel.this);
            serialMockButton.addActionListener(CommPortRadioPanel.this);

            GridBagConstraints gbc = new GridBagConstraints();
            gbc.insets = new Insets(0, 0, 5, 0);
            add(comboButton, gbc);

            gbc.gridx = 1;
            gbc.fill = GridBagConstraints.HORIZONTAL;
            gbc.weightx = 1.0;
            gbc.gridwidth = 2;
            add(commPorts, gbc);

            gbc.gridwidth = 1;
            gbc = new GridBagConstraints();
            gbc.gridy = 1;
            gbc.insets = new Insets(0, 0, 5, 0);
            add(textFieldButton, gbc);

            gbc.gridx = 1;
            gbc.fill = GridBagConstraints.HORIZONTAL;
            gbc.weightx = 0.0;
            gbc.insets = new Insets(0, 0, 0, 5);
            add(new JLabel("Symlink"), gbc);

            gbc.insets = new Insets(0, 0, 0, 0);
            gbc.gridx = 2;
            gbc.fill = GridBagConstraints.HORIZONTAL;
            gbc.weightx = 1.0;
            add(textFieldSerialSymLink, gbc);

            gbc = new GridBagConstraints();
            gbc.gridy = 2;
            add(serialMockButton, gbc);

            gbc.gridwidth = 2;

            gbc.gridx = 1;
            gbc.fill = GridBagConstraints.HORIZONTAL;
            gbc.weightx = 1.0;
            add(labelSerialMock, gbc);
        }

        private void setSelectedPort(CommPort commPort) {
            commPorts.setSelectedItem(commPort);
            actionPerformed(null);
        }

        private void setSymlink(String symlink) {
            textFieldSerialSymLink.setText(symlink);
            actionPerformed(null);
        }

        @Override
        public void actionPerformed(ActionEvent e) {

            commPorts.setEnabled(comboButton.isEnabled());
            textFieldSerialSymLink.setEnabled(textFieldButton.isEnabled());
            labelSerialMock.setEnabled(serialMockButton.isEnabled());

            if (e != null && (e.getSource() == comboButton || e.getSource() == textFieldButton
                || e.getSource() == serialMockButton)) {
                if (peer != null) {
                    if (CollectionUtils.isNotEmpty(peer)) {
                        LOGGER.trace("Update peers from commPort.");
                        for (PortRadioPanel p : peer) {
                            p.actionPerformed(e);
                        }
                    }
                }
            }

            if (e != null) {
                if (e.getSource() == serialMockButton) {
                    PreferencesPortType portType =
                        PreferencesPortType.getValue(ConnectionPortType.SerialSimulation.name());
                    fireSelectedPortTypeChanged(portType);
                }
                else if (e.getSource() == comboButton) {
                    PreferencesPortType portType = PreferencesPortType.getValue(ConnectionPortType.SerialPort.name());
                    // get the selected port
                    portType.setConnectionName(
                        (commPorts.getSelectedItem() != null ? commPorts.getSelectedItem().toString() : null));
                    fireSelectedPortTypeChanged(portType);
                }
                else if (e.getSource() == textFieldButton) {
                    PreferencesPortType portType =
                        PreferencesPortType.getValue(ConnectionPortType.SerialSymLink.name());
                    portType.setConnectionName(textFieldSerialSymLink.getText());
                    fireSelectedPortTypeChanged(portType);
                }
            }
        }

        @Override
        public void setEnabled(boolean enabled) {
            boolean old = isEnabled();
            if (old != enabled) {
                super.setEnabled(enabled);

                comboButton.setEnabled(enabled);
                textFieldButton.setEnabled(enabled);
                serialMockButton.setEnabled(enabled);
                actionPerformed(null);

                if (!enabled) {
                    // check if we we have a selected option on this panel, if yes, remove it by selecting the default
                    // option of the peer
                    if (serialMockButton.isSelected() || textFieldButton.isSelected() || comboButton.isSelected()) {
                        if (CollectionUtils.isNotEmpty(peer)) {
                            for (PortRadioPanel p : peer) {
                                p.selectDefault();
                            }
                        }
                    }
                }
            }
        }

        public void selectDefault() {
            serialMockButton.setSelected(true);
            PreferencesPortType portType = PreferencesPortType.getValue(ConnectionPortType.SerialSimulation.name());
            fireSelectedPortTypeChanged(portType);
        }

        public void setInitialValues(PreferencesModel model) {
            textFieldSerialSymLink.setText(model.getPreviousSelectedSerialSymLink());
        }

        public void select(PreferencesPortType portType) {
            LOGGER.info("Selected the portType: {}", portType);

            switch (portType.getConnectionPortType()) {
                case SerialPort:
                    comboButton.setSelected(true);
                    break;
                case SerialSymLink:
                    textFieldButton.setSelected(true);
                    textFieldSerialSymLink.setText(portType.getConnectionName());
                    break;
                default:
                    serialMockButton.setSelected(true);
                    break;
            }
        }
    }
}
