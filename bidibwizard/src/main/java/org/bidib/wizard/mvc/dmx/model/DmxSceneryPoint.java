package org.bidib.wizard.mvc.dmx.model;

import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.mvc.main.model.Port;
import org.bidib.wizard.mvc.main.model.function.MacroFunction;
import org.jfree.util.ObjectUtilities;

import com.jgoodies.binding.beans.Model;

public class DmxSceneryPoint extends Model {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_PORT = "port";

    public static final String PROPERTY_MACRO = "macro";

    public static final String PROPERTY_DMX_CHANNEL_NUMBER = "dmxChannelNumber";

    public static final String PROPERTY_TIME_OFFSET = "timeOffset";

    public static final String PROPERTY_BRIGHTNESS = "brightness";

    public static final String PROPERTY_UNIQUEID = "uniqueId";

    public static final String PROPERTY_ACTION = "action";

    private Port<?> port;

    private MacroFunction macro;

    // DMX channel number
    private int dmxChannelNumber;

    private int timeOffset;

    private int brightness;

    private long uniqueId;

    private BidibStatus action;

    public DmxSceneryPoint() {

    }

    /**
     * @return the port
     */
    public Port<?> getPort() {
        return port;
    }

    /**
     * @param port
     *            the port to set
     */
    public void setPort(Port<?> port) {
        Port<?> oldPortValue = this.port;
        this.port = port;

        MacroFunction oldMacroValue = this.macro;
        this.macro = null;

        firePropertyChange(PROPERTY_PORT, oldPortValue, port);
        firePropertyChange(PROPERTY_MACRO, oldMacroValue, macro);
    }

    /**
     * @return the macro
     */
    public MacroFunction getMacro() {
        return macro;
    }

    /**
     * @param macro
     *            the macro to set
     */
    public void setMacro(MacroFunction macro) {
        MacroFunction oldMacroValue = this.macro;
        this.macro = macro;

        Port<?> oldPortValue = this.port;
        this.port = null;

        firePropertyChange(PROPERTY_MACRO, oldMacroValue, macro);
        firePropertyChange(PROPERTY_PORT, oldPortValue, port);
    }

    /**
     * @return the dmxChannelNumber
     */
    public int getDmxChannelNumber() {
        return dmxChannelNumber;
    }

    /**
     * @param dmxChannelNumber
     *            the dmxChannelNumber to set
     */
    public void setDmxChannelNumber(int dmxChannelNumber) {
        int oldValue = this.dmxChannelNumber;
        this.dmxChannelNumber = dmxChannelNumber;

        firePropertyChange(PROPERTY_DMX_CHANNEL_NUMBER, oldValue, dmxChannelNumber);
    }

    /**
     * @return the timeOffset
     */
    public int getTimeOffset() {
        return timeOffset;
    }

    /**
     * @param timeOffset
     *            the timeOffset to set
     */
    public void setTimeOffset(int timeOffset) {
        int oldValue = this.timeOffset;
        this.timeOffset = timeOffset;

        firePropertyChange(PROPERTY_TIME_OFFSET, oldValue, timeOffset);
    }

    /**
     * @return the brightness
     */
    public int getBrightness() {
        return brightness;
    }

    /**
     * @param brightness
     *            the brightness to set
     */
    public void setBrightness(int brightness) {
        int oldValue = this.brightness;
        this.brightness = brightness;

        firePropertyChange(PROPERTY_BRIGHTNESS, oldValue, brightness);
    }

    /**
     * @return the uniqueId
     */
    public long getUniqueId() {
        return uniqueId;
    }

    /**
     * @param uniqueId
     *            the uniqueId to set
     */
    public void setUniqueId(long uniqueId) {
        long oldValue = this.uniqueId;
        this.uniqueId = uniqueId;

        firePropertyChange(PROPERTY_UNIQUEID, oldValue, uniqueId);
    }

    /**
     * @return the action
     */
    public BidibStatus getAction() {
        return action;
    }

    /**
     * @param action
     *            the action to set
     */
    public void setAction(BidibStatus action) {
        BidibStatus oldValue = this.action;
        this.action = action;

        firePropertyChange(PROPERTY_ACTION, oldValue, action);
    }

    public DmxSceneryPoint withTimeOffset(int timeOffset) {
        setTimeOffset(timeOffset);
        return this;
    }

    public DmxSceneryPoint withPort(Port<?> port) {
        setPort(port);
        return this;
    }

    public DmxSceneryPoint withMacro(MacroFunction macro) {
        setMacro(macro);
        return this;
    }

    public DmxSceneryPoint withDmxChannelNumber(int dmxChannelNumber) {
        setDmxChannelNumber(dmxChannelNumber);
        return this;
    }

    public DmxSceneryPoint withBrightness(int brightness) {
        setBrightness(brightness);
        return this;
    }

    public DmxSceneryPoint withUniqueId(long uniqueId) {
        setUniqueId(uniqueId);
        return this;
    }

    public DmxSceneryPoint withAction(BidibStatus action) {
        setAction(action);
        return this;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof DmxSceneryPoint)) {
            return false;
        }

        DmxSceneryPoint that = (DmxSceneryPoint) obj;
        if (this.dmxChannelNumber != that.dmxChannelNumber) {
            return false;
        }
        if (this.timeOffset != that.timeOffset) {
            return false;
        }
        if (this.brightness != that.brightness) {
            return false;
        }
        if (this.uniqueId != that.uniqueId) {
            return false;
        }
        if (this.action != that.action) {
            return false;
        }
        if (!ObjectUtilities.equal(this.port, that.port)) {
            return false;
        }
        if (!ObjectUtilities.equal(this.macro, that.macro)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int result;
        result = this.timeOffset;
        result = 29 * result + this.brightness;
        result = (int) (29 * result + this.uniqueId);
        result = 29 * result + this.dmxChannelNumber;
        result = 29 * result + (this.action != null ? this.action.hashCode() : 0);
        result = 29 * result + (this.port != null ? this.port.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("DmxSceneryPoint, timeOffset: ");
        sb.append(timeOffset).append(" ms, brightness: ").append(brightness).append(", uniqueId: ").append(uniqueId)
            .append(", dmxChannelNumber: ").append(dmxChannelNumber).append(", port: ").append(port)
            .append(", action: ").append(action).append(", macro: ").append(macro);
        return sb.toString();
    }
}
