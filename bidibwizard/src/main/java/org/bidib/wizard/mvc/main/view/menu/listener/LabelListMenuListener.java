package org.bidib.wizard.mvc.main.view.menu.listener;

public interface LabelListMenuListener {
    /**
     * Edit the label.
     */
    void editLabel();
}
