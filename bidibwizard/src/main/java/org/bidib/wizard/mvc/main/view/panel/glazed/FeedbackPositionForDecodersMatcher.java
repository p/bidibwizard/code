package org.bidib.wizard.mvc.main.view.panel.glazed;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.bidib.wizard.mvc.main.model.FeedbackPosition;

import ca.odell.glazedlists.matchers.Matcher;

public class FeedbackPositionForDecodersMatcher implements Matcher<FeedbackPosition> {

    /** the decoders to match */
    private Set<String> decoders = new HashSet<String>();

    /**
     * Create a new {@link FeedbackPositionForDecodersMatcher} that matches only {@link FeedbackPosition}s that have one
     * or more user in the specified list.
     */
    public FeedbackPositionForDecodersMatcher(Collection<String> decoders) {
        // defensive copy all the users
        this.decoders.addAll(decoders);
    }

    /**
     * Test whether to include or not include the specified feedback position based on whether or not their decoder is
     * selected.
     */
    @Override
    public boolean matches(FeedbackPosition position) {
        if (position == null) {
            return false;
        }
        if (decoders.isEmpty()) {
            return true;
        }

        String address = Integer.toString(position.getDecoderAddress());
        return decoders.contains(address);
    }

}
