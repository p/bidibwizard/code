package org.bidib.wizard.mvc.main.model;

public enum MacroSaveState {
    NOT_LOADED_FROM_NODE, PENDING_CHANGES, SAVED_ON_NODE, PERMANENTLY_STORED_ON_NODE;
}
