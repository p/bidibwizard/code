package org.bidib.wizard.mvc.main.view.panel;

import java.awt.BorderLayout;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.collections4.Predicate;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.main.controller.FlagPanelController;
import org.bidib.wizard.mvc.main.model.Flag;
import org.bidib.wizard.mvc.main.model.FlagTableModel;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.model.listener.FlagListener;
import org.bidib.wizard.mvc.main.view.table.AbstractEmptyTable;
import org.bidib.wizard.mvc.main.view.table.FlagRenderer;
import org.bidib.wizard.script.node.types.TargetType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.debug.FormDebugPanel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.FormLayout;

public class FlagListPanel implements ChangeLabelSupport {

    private static final Logger LOGGER = LoggerFactory.getLogger(FlagListPanel.class);

    private static final String ENCODED_DIALOG_COLUMN_SPECS = "fill:pref:grow, 5dlu, 200dlu";

    private final MainModel mainModel;

    private final JPanel contentPanel;

    private final AbstractEmptyTable table;

    private final FlagTableModel tableModel;

    private final FlagPanelController flagsPanelController;

    public FlagListPanel(final FlagPanelController flagsPanelController, final MainModel model) {
        this.mainModel = model;
        this.flagsPanelController = flagsPanelController;

        // create form builder
        DefaultFormBuilder dialogBuilder = null;
        boolean debugDialog = false;
        if (debugDialog) {
            JPanel panel = new FormDebugPanel() {
                private static final long serialVersionUID = 1L;

                @Override
                public String getName() {
                    // this is used as tab title
                    return Resources.getString(FlagListPanel.class, "name");
                }
            };
            dialogBuilder = new DefaultFormBuilder(new FormLayout(ENCODED_DIALOG_COLUMN_SPECS), panel);
        }
        else {
            JPanel panel = new JPanel(new BorderLayout()) {
                private static final long serialVersionUID = 1L;

                @Override
                public String getName() {
                    // this is used as tab title
                    return Resources.getString(FlagListPanel.class, "name");
                }
            };
            dialogBuilder = new DefaultFormBuilder(new FormLayout(ENCODED_DIALOG_COLUMN_SPECS), panel);
        }
        dialogBuilder.border(Borders.TABBED_DIALOG);

        tableModel = new FlagTableModel(model);

        table = new AbstractEmptyTable(tableModel, Resources.getString(getClass(), "emptyTable")) {
            private static final long serialVersionUID = 1L;

            public TableCellRenderer getCellRenderer(int row, int column) {
                TableCellRenderer result = super.getCellRenderer(row, column);
                return result;
            }
        };

        table.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        // do not allow drag columns to other position
        table.getTableHeader().setReorderingAllowed(false);
        // disabled sorting
        table.setSortable(false);

        // set the correct row height
        org.bidib.wizard.mvc.common.view.slider.SliderRenderer sliderEditor =
            new org.bidib.wizard.mvc.common.view.slider.SliderRenderer(0, 255, 10);

        int rowHeight =
            sliderEditor.getTableCellRendererComponent(table, 1, false, false, 0, 0).getPreferredSize().height + 4;
        LOGGER.info("Set row height: {}", rowHeight);
        table.setRowHeight(rowHeight);

        TableColumn tc = table.getColumnModel().getColumn(FlagTableModel.COLUMN_LABEL);
        tc.setCellRenderer(new FlagRenderer(Resources.getString(FlagTableModel.class, "flag") + "_"));

        tableModel.addFlagListener(new FlagListener() {

            @Override
            public void labelChanged(int flagId) {
                fireFlagLabelChanged(flagId);
            }
        });

        dialogBuilder.appendRow("fill:200dlu:grow");
        dialogBuilder.append(new JScrollPane(table), 3);

        contentPanel = dialogBuilder.build();
    }

    public JPanel getComponent() {
        return contentPanel;
    }

    @Override
    public void changeLabel(TargetType portType) {
        // TODO Auto-generated method stub

        LOGGER.info("Change the label of the flag: {}", portType);

        final int flagNum = portType.getPortNum();
        String label = portType.getLabel();

        Node node = mainModel.getSelectedNode();
        if (node != null) {
            List<Flag> flags = node.getFlags();

            Flag flag = IterableUtils.find(flags, new Predicate<Flag>() {

                @Override
                public boolean evaluate(Flag object) {
                    return object.getId() == flagNum;
                }
            });
            LOGGER.info("Change label of flag: {}", flag);

            if (flag != null) {
                flag.setLabel(label);

                fireFlagLabelChanged(flag.getId());
            }
        }

    }

    private void fireFlagLabelChanged(int flagId) {

        Flag flag = (Flag) tableModel.getValueAt(flagId, FlagTableModel.COLUMN_LABEL);

        flagsPanelController.setFlagLabel(flagId, flag.getLabel());
    }

    public void nodeChanged() {
        LOGGER.info("The selected node has changed.");

        // remove all rows
        tableModel.setRowCount(0);

        refreshFlags();
    }

    protected void refreshFlags() {
        LOGGER.info("refresh the flags.");

        Node node = mainModel.getSelectedNode();
        if (node != null) {
            List<Flag> flags = node.getFlags();

            for (Flag flag : flags) {

                LOGGER.debug("Adding row for flag: {}", flag);
                tableModel.addRow(flag);
            }
        }
    }
}
