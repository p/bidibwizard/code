package org.bidib.wizard.mvc.main.view.menu.listener;

public interface AccessoryListMenuListener extends LabelListMenuListener {
    void exportAccessory();

    void importAccessory();

    void initializeAccessory();
}
