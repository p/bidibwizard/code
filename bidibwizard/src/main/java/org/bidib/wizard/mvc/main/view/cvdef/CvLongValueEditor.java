package org.bidib.wizard.mvc.main.view.cvdef;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import org.apache.commons.lang.StringUtils;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.exchange.vendorcv.ModeType;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.common.utils.ImageUtils;
import org.bidib.wizard.mvc.common.view.converter.StringConverter;
import org.bidib.wizard.mvc.common.view.converter.StringToUnsignedLongConverter;
import org.bidib.wizard.mvc.common.view.cvdefinition.CvDefinitionTreeTableModel;
import org.bidib.wizard.mvc.common.view.validation.PropertyValidationI18NSupport;
import org.bidib.wizard.mvc.main.view.panel.NodeTree;
import org.bidib.wizard.utils.InputValidationDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.value.ConverterValueModel;
import com.jgoodies.binding.value.ValueHolder;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.util.PropertyValidationSupport;
import com.jgoodies.validation.view.ValidationComponentUtils;

public class CvLongValueEditor extends CvValueNumberEditor<Long> {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(CvLongValueEditor.class);

    private static final int MAX_INDEX = 4;

    private ValueModel[] cvByteValueModel = new ValueHolder[MAX_INDEX];

    private ValueModel converterByte3ValueModel;

    private ValueModel converterByte2ValueModel;

    private ValueModel converterByte1ValueModel;

    private ValueModel converterByte0ValueModel;

    private boolean adjustingInProgress;

    private long minValue;

    private long maxValue;

    private ValueModel cvNumberByte3Model = new ValueHolder();

    private ValueModel cvNumberByte2Model = new ValueHolder();

    private ValueModel cvNumberByte1Model = new ValueHolder();

    private ValueModel cvNumberByte0Model = new ValueHolder();

    private ValueModel writeEnabled;

    // if the range of the high value is limited, this value is the maximum
    private Integer highValueMax;

    public CvLongValueEditor() {

        // initialize the value holders
        for (int index = 0; index < MAX_INDEX; index++) {
            cvByteValueModel[index] = new ValueHolder();
        }
    }

    @Override
    protected ValidationResult validateModel(
        final PresentationModel<CvValueBean<Long>> model, CvValueBean<Long> cvValueBean) {

        PropertyValidationSupport support = new PropertyValidationI18NSupport(cvValueBean, "validation");

        if (Boolean.TRUE.equals(writeEnabled.getValue())) {
            Object val = model.getBufferedModel("cvValue").getValue();
            if (val instanceof Number) {
                int value = ((Number) val).intValue();
                long lValue = ((long) value) & 0xffffffffL;
                if (lValue < minValue || lValue > maxValue) {
                    support.addError("cvvalue_key", "invalid_value;min=" + minValue + ",max=" + maxValue);
                }
            }
            else if (val == null) {
                support.addError("cvvalue_key", "not_empty");
            }

            // validation of byte values
            for (int index = 0; index < MAX_INDEX; index++) {
                Object valHigh = cvByteValueModel[index].getValue();
                if (valHigh instanceof Number) {
                    int value = ((Number) valHigh).intValue();

                    int maxHighValue = 255;
                    if (highValueMax != null) {
                        maxHighValue = highValueMax;
                    }

                    if (value < 0 || value > maxHighValue) {
                        support.addError("cvb" + index + "value_key", "invalid_value;min=0,max=" + maxHighValue);
                    }
                }
                else if (valHigh == null) {
                    support.addError("cvb" + index + "value_key", "not_empty");
                }
            }
        }
        else {
            LOGGER.debug("Validation is disabled!");
        }

        return support.getResult();
    }

    private boolean isAdjusting() {
        return adjustingInProgress;
    }

    private void setAdjusting(boolean adjusting) {
        this.adjustingInProgress = adjusting;
    }

    @Override
    protected DefaultFormBuilder prepareFormPanel() {

        writeEnabled = new ValueHolder(false);

        // Get buffered model objects.
        cvValueModel = cvAdapter.getBufferedModel("cvValue");
        valueConverterModel =
            new ConverterValueModel(cvValueModel, new StringToUnsignedLongConverter(new DecimalFormat("#")));

        DefaultFormBuilder formBuilder =
            new DefaultFormBuilder(new FormLayout("3dlu, max(50dlu;pref), 3dlu, max(30dlu;pref), 0dlu:grow"));
        JTextField textCvValue = BasicComponentFactory.createTextField(valueConverterModel, false);
        textCvValue.setDocument(new InputValidationDocument(InputValidationDocument.NUMERIC));
        textCvValue.setEditable(false);

        // the needs reboot Icon is invisible by default
        ImageIcon warnIcon = ImageUtils.createImageIcon(NodeTree.class, "/icons/warn.png");
        JLabel needsRebootIcon =
            new JLabel(Resources.getString(getClass(), "rebootrequired"), warnIcon, SwingConstants.LEADING);
        needsRebootIcon.setVisible(false);

        // init the components
        ValidationComponentUtils.setMandatory(textCvValue, true);
        ValidationComponentUtils.setMessageKey(textCvValue, "validation.cvvalue_key");
        formBuilder.leadingColumnOffset(1);
        formBuilder.nextLine(0);

        formBuilder.append(BasicComponentFactory.createLabel(new ValueHolder(getValueLabelPrefix() + "wert")), 4);
        formBuilder.nextLine();
        formBuilder.append(textCvValue, 1);
        formBuilder.append(needsRebootIcon);

        // the 'byte' fields
        converterByte3ValueModel =
            new ConverterValueModel(cvByteValueModel[3], new StringConverter(new DecimalFormat("#")));
        converterByte2ValueModel =
            new ConverterValueModel(cvByteValueModel[2], new StringConverter(new DecimalFormat("#")));
        converterByte1ValueModel =
            new ConverterValueModel(cvByteValueModel[1], new StringConverter(new DecimalFormat("#")));
        converterByte0ValueModel =
            new ConverterValueModel(cvByteValueModel[0], new StringConverter(new DecimalFormat("#")));

        JTextField textCvByte3Value = BasicComponentFactory.createTextField(converterByte3ValueModel, false);
        textCvByte3Value.setDocument(new InputValidationDocument(InputValidationDocument.NUMERIC));
        textCvByte3Value.setEditable(false);
        ValidationComponentUtils.setMessageKey(textCvByte3Value, "validation.cvb3value_key");

        formBuilder.nextLine();
        formBuilder
            .append(BasicComponentFactory.createLabel(cvNumberByte3Model, new CvNodeMessageFormat("{0} (CV{1})")), 4);
        formBuilder.nextLine();
        formBuilder.append(textCvByte3Value);

        // --------------------------
        JTextField textCvByte2Value = BasicComponentFactory.createTextField(converterByte2ValueModel, false);
        textCvByte2Value.setDocument(new InputValidationDocument(InputValidationDocument.NUMERIC));
        textCvByte2Value.setEditable(false);
        ValidationComponentUtils.setMessageKey(textCvByte2Value, "validation.cvb2value_key");

        formBuilder.nextLine();
        formBuilder
            .append(BasicComponentFactory.createLabel(cvNumberByte2Model, new CvNodeMessageFormat("{0} (CV{1})")), 4);
        formBuilder.nextLine();
        formBuilder.append(textCvByte2Value);

        // ---------------------------
        JTextField textCvByte1Value = BasicComponentFactory.createTextField(converterByte1ValueModel, false);
        textCvByte1Value.setDocument(new InputValidationDocument(InputValidationDocument.NUMERIC));
        textCvByte1Value.setEditable(false);
        ValidationComponentUtils.setMessageKey(textCvByte1Value, "validation.cvb1value_key");

        formBuilder.nextLine();
        formBuilder
            .append(BasicComponentFactory.createLabel(cvNumberByte1Model, new CvNodeMessageFormat("{0} (CV{1})")), 4);
        formBuilder.nextLine();
        formBuilder.append(textCvByte1Value);

        // ---------------------------
        JTextField textCvByte0Value = BasicComponentFactory.createTextField(converterByte0ValueModel, false);
        textCvByte0Value.setDocument(new InputValidationDocument(InputValidationDocument.NUMERIC));
        textCvByte0Value.setEditable(false);
        ValidationComponentUtils.setMessageKey(textCvByte0Value, "validation.cvb0value_key");

        formBuilder.nextLine();
        formBuilder
            .append(BasicComponentFactory.createLabel(cvNumberByte0Model, new CvNodeMessageFormat("{0} (CV{1})")), 4);
        formBuilder.nextLine();
        formBuilder.append(textCvByte0Value);

        // add bindings for enable/disable the textfields
        PropertyConnector.connect(writeEnabled, "value", textCvValue, "editable");
        PropertyConnector.connect(writeEnabled, "value", textCvByte3Value, "editable");
        PropertyConnector.connect(writeEnabled, "value", textCvByte2Value, "editable");
        PropertyConnector.connect(writeEnabled, "value", textCvByte1Value, "editable");
        PropertyConnector.connect(writeEnabled, "value", textCvByte0Value, "editable");
        PropertyConnector.connect(needsRebootModel, "value", needsRebootIcon, "visible");

        final PropertyChangeListener pcl = new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                LOGGER.debug("high value has changed: {}", evt.getNewValue());
                if (isAdjusting()) {
                    LOGGER.debug("updateChangeInProgress, skip processing.");
                    return;
                }

                try {
                    setAdjusting(true);

                    int dword = 0;
                    for (int index = 0; index < MAX_INDEX; index++) {

                        Object lowVal = cvByteValueModel[MAX_INDEX - index - 1].getValue();
                        byte lowValByte = 0;
                        if (lowVal instanceof Number) {
                            lowValByte = ((Number) lowVal).byteValue();
                        }
                        dword = (dword << 8) | (lowValByte & 0xFF);
                    }
                    valueConverterModel.setValue(String.valueOf(dword));

                }
                finally {
                    setAdjusting(false);
                }
                triggerValidation(null);
            }
        };

        cvByteValueModel[3].addValueChangeListener(pcl);
        cvByteValueModel[2].addValueChangeListener(pcl);
        cvByteValueModel[1].addValueChangeListener(pcl);
        cvByteValueModel[0].addValueChangeListener(pcl);

        // value change listener for long value
        cvValueModel.addValueChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                LOGGER.debug("The value has changed: {}", evt.getNewValue());

                if (isAdjusting()) {
                    LOGGER.debug("updateChangeInProgress, skip processing.");
                    return;
                }

                try {
                    setAdjusting(true);

                    if (evt.getNewValue() instanceof Number) {
                        Number num = (Number) evt.getNewValue();
                        int val = num.intValue();
                        LOGGER.debug("The new value is: {}", val);
                        byte byte0 = ByteUtils.getLowByte(val);
                        byte byte1 = ByteUtils.getHighByte(val);

                        byte byte2 = ByteUtils.getHighWordLowByte(val);
                        byte byte3 = ByteUtils.getHighWordHighByte(val);

                        converterByte3ValueModel.setValue(String.valueOf(ByteUtils.getInt(byte3)));
                        converterByte2ValueModel.setValue(String.valueOf(ByteUtils.getInt(byte2)));
                        converterByte1ValueModel.setValue(String.valueOf(ByteUtils.getInt(byte1)));
                        converterByte0ValueModel.setValue(String.valueOf(ByteUtils.getInt(byte0)));
                    }
                    else {
                        converterByte3ValueModel.setValue(null);
                        converterByte2ValueModel.setValue(null);
                        converterByte1ValueModel.setValue(null);
                        converterByte0ValueModel.setValue(null);
                    }

                }
                finally {
                    setAdjusting(false);
                }
                triggerValidation(null);
            }
        });
        return formBuilder;
    }

    @Override
    public void setValue(CvNode cvNode, Map<String, CvNode> cvNumberToNodeMap) {

        getTrigger().triggerFlush();
        getTrigger().triggerFlush();

        boolean timeout = cvNode.getConfigVar().isTimeout();

        // set textfield editable before the value is set because the validation is triggered
        writeEnabled.setValue(!(ModeType.RO.equals(cvNode.getCV().getMode()) || timeout));

        if (cvNode instanceof LongCvNode) {
            LongCvNode longCvNode = (LongCvNode) cvNode;
            if (longCvNode.getMasterNode() != null) {
                // this is a slaveNode -> get the master
                longCvNode = longCvNode.getMasterNode();
            }
            LOGGER.info("Fetched the master node: {}", longCvNode);

            // keep the node reference
            cvValueBean.setCvNode(longCvNode);

            // set the value in the longValueBean
            setValueInternally(longCvNode, false);

            // get the labels for the byte values
            cvNumberByte3Model.setValue(longCvNode.getSlaveNodes().get(2));
            cvNumberByte2Model.setValue(longCvNode.getSlaveNodes().get(1));
            cvNumberByte1Model.setValue(longCvNode.getSlaveNodes().get(0));
            cvNumberByte0Model.setValue(longCvNode);
        }
        else {
            LOGGER.error("Invalid node type passed to GBM16TReverserEditor: {}", cvNode);
        }

        // prepare the min and max value
        String min = cvNode.getCV().getMin();
        if (min != null) {
            try {
                minValue = Long.parseLong(min);
            }
            catch (NumberFormatException ex) {
                LOGGER.debug("Parse min value failed: {}", min);
                minValue = 0L;
            }
        }
        else {
            minValue = 0L;
        }

        String max = cvNode.getCV().getMax();
        if (max != null) {
            try {
                maxValue = Long.parseLong(max);
            }
            catch (NumberFormatException ex) {
                LOGGER.debug("Parse max value low failed: {}", max);
                maxValue = Long.MAX_VALUE;
            }
        }
        else {
            maxValue = Long.MAX_VALUE;
        }
        LOGGER.info("Prepared minValue: {}, maxValue: {}", minValue, maxValue);

        checkNeedsReboot();

        triggerValidation(null);
    }

    protected void checkNeedsReboot() {
        CvNode highNode = (CvNode) cvNumberByte1Model.getValue();
        CvNode lowNode = (CvNode) cvNumberByte0Model.getValue();
        Boolean needsReboot = Boolean.FALSE;
        if (highNode != null && highNode.getCV() != null && highNode.getCV().isRebootneeded() != null) {
            needsReboot = highNode.getCV().isRebootneeded();
        }
        if (needsReboot.equals(Boolean.FALSE) && lowNode != null && lowNode.getCV() != null
            && lowNode.getCV().isRebootneeded() != null) {
            needsReboot = lowNode.getCV().isRebootneeded();
        }

        needsRebootModel.setValue(needsReboot);
    }

    @Override
    public void updateCvValues(Map<String, CvNode> cvNumberToNodeMap, CvDefinitionTreeTableModel treeModel) {

        LongCvNode cvNodeMaster = (LongCvNode) cvValueBean.getCvNode();
        cvNodeMaster.setValueAt(cvByteValueModel[0].getValue(), CvNode.COLUMN_NEW_VALUE);

        LongCvNode[] childNodes = cvNodeMaster.getSlaveNodes().toArray(new LongCvNode[0]);
        childNodes[0].setValueAt(cvByteValueModel[1].getValue(), CvNode.COLUMN_NEW_VALUE);
        childNodes[1].setValueAt(cvByteValueModel[2].getValue(), CvNode.COLUMN_NEW_VALUE);
        childNodes[2].setValueAt(cvByteValueModel[3].getValue(), CvNode.COLUMN_NEW_VALUE);

        LOGGER.info("New values: {}", new Object[] { childNodes });

        // treeModel.fireTableDataChanged();

        // treeModel.refreshTreeTable(cvNodeMaster);
        // treeModel.refreshTreeTable(childNodes[0]);
        // treeModel.refreshTreeTable(childNodes[1]);
        // treeModel.refreshTreeTable(childNodes[2]);
        // treeModel.refresh();
    }

    /**
     * Refresh the displayed value from the node.
     */
    public void refreshValue() {

        LongCvNode cvNodeMaster = (LongCvNode) cvValueBean.getCvNode();
        LOGGER.info("Refresh values form cvNodeMaster: {}", cvNodeMaster);
        if (cvNodeMaster != null) {
            setValueInternally(cvNodeMaster, false);
        }
    }

    @Override
    public void refreshValueFromSource() {

        LongCvNode cvNodeMaster = (LongCvNode) cvValueBean.getCvNode();
        LOGGER.info("Refresh values form cvNodeMaster: {}", cvNodeMaster);
        if (cvNodeMaster != null) {
            setValueInternally(cvNodeMaster, true);
        }
    }

    protected Number parseValue(String strValue) {
        Long value = Long.parseLong(strValue);

        return value;
    }

    protected void setValueInternally(LongCvNode cvNodeMaster, boolean fromSource) {

        Number value = getValue(cvNodeMaster, fromSource);

        LOGGER.info("setValueInternally, value: {}", value);

        cvValueModel.setValue(value);
    }

    private Long getValue(LongCvNode cvNode, boolean fromSource) {
        long value = 0;
        // iterate over all involved cvNodes
        LongCvNode[] longCvNodes = new LongCvNode[4];
        longCvNodes[3] = cvNode; // master
        int index = 2;
        for (LongCvNode slave : cvNode.getSlaveNodes()) {
            longCvNodes[index] = slave;
            index--;
        }

        for (LongCvNode node : longCvNodes) {

            // this must be updated in the text field ...
            if (!fromSource && node.getNewValue() instanceof Number) {
                long val = ((Number) node.getNewValue()).longValue();
                value = (value << 8) | (val & 0xFF);
            }
            else if (StringUtils.isNotBlank(node.getConfigVar().getValue())) {
                String strValue = node.getConfigVar().getValue();
                try {
                    long val = Long.valueOf(strValue);

                    value = (value << 8) | (val & 0xFF);
                }
                catch (NumberFormatException ex) {
                    LOGGER.warn("Parse value failed: {}", strValue);
                }
            }
        }

        return value;
    }
}
