package org.bidib.wizard.mvc.main.model.function;

import org.bidib.jbidibc.exchange.lcmacro.CriticalSectionActionType;
import org.bidib.jbidibc.exchange.lcmacro.CriticalSectionPoint;
import org.bidib.jbidibc.exchange.lcmacro.LcMacroPointType;
import org.bidib.wizard.comm.CriticalFunctionStatus;

public class CriticalFunction extends SystemFunction<CriticalFunctionStatus> {
    public CriticalFunction() {
        this(CriticalFunctionStatus.BEGIN);
    }

    public CriticalFunction(CriticalFunctionStatus action) {
        super(action, KEY_CRITICAL);
    }

    public String getDebugString() {
        return getAction().name().substring(0, 1) + getAction().name().substring(1).toLowerCase() + "Critical";
    }

    @Override
    public LcMacroPointType toLcMacroPoint() {
        CriticalSectionPoint criticalSectionPoint = new CriticalSectionPoint();
        criticalSectionPoint.setCriticalSectionActionType(CriticalSectionActionType.fromValue(getAction().name()));
        return criticalSectionPoint;
    }
}
