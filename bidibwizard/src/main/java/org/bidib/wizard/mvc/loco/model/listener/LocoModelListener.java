package org.bidib.wizard.mvc.loco.model.listener;

import org.bidib.wizard.comm.Direction;
import org.bidib.wizard.comm.SpeedSteps;

public interface LocoModelListener {
    void addressChanged(int address);

    void directionChanged(Direction direction);

    void functionChanged(int index, boolean value);

    void speedStepsChanged(SpeedSteps speedSteps);

    void dynStateEnergyChanged(int dynStateEnergy);
}
