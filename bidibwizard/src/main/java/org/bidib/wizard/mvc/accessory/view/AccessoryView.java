package org.bidib.wizard.mvc.accessory.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;

import org.bidib.jbidibc.core.AddressData;
import org.bidib.jbidibc.core.enumeration.AccessoryAcknowledge;
import org.bidib.jbidibc.core.enumeration.TimeBaseUnitEnum;
import org.bidib.jbidibc.core.enumeration.TimingControlEnum;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.dialog.EscapeDialog;
import org.bidib.wizard.mvc.accessory.model.AccessoryModel;
import org.bidib.wizard.mvc.accessory.view.listener.AccessoryViewListener;
import org.bidib.wizard.mvc.accessory.view.panel.DccAccessoryPanel;
import org.bidib.wizard.mvc.accessory.view.panel.DccExtAccessoryPanel;
import org.bidib.wizard.mvc.accessory.view.panel.LogAreaAwarePanel;
import org.bidib.wizard.mvc.accessory.view.panel.listener.AccessoryRequestListener;
import org.bidib.wizard.mvc.accessory.view.panel.listener.AccessoryResultListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.forms.builder.ButtonBarBuilder;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.debug.FormDebugPanel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.FormLayout;

/**
 * This is the view for DCC accessories.
 */
public class AccessoryView extends EscapeDialog {
    private static final Logger LOGGER = LoggerFactory.getLogger(AccessoryView.class);

    private static final long serialVersionUID = 1L;

    private final Collection<AccessoryViewListener> listeners = new LinkedList<AccessoryViewListener>();

    private final AccessoryModel accessoryModel;

    private static final String ENCODED_DIALOG_COLUMN_SPECS = "pref, fill:50dlu:grow";

    private final JTabbedPane tabbedPane;

    private final JPanel contentPanel;

    private final JButton clearButton = new JButton(Resources.getString(getClass(), "clearLogArea"));

    private final JButton closeButton = new JButton(Resources.getString(getClass(), "close"));

    private List<AccessoryResultListener> accessoryResultListeners = new LinkedList<AccessoryResultListener>();

    public AccessoryView(JFrame parent, final AccessoryModel model, int x, int y) {
        super(parent, Resources.getString(AccessoryView.class, "title"), false);
        this.accessoryModel = model;

        setLayout(new GridBagLayout());
        setLocation(x, y);

        tabbedPane = new JTabbedPane();

        AccessoryRequestListener accessoryRequestListener = new AccessoryRequestListener() {

            @Override
            public void sendRequest(
                AccessoryResultListener accessoryResultListener, AddressData dccAddress, Integer aspect,
                Integer switchTime, TimeBaseUnitEnum timeBaseUnit, TimingControlEnum timingControl) {

                LOGGER.info("Send request, accessoryResultListener: {}", accessoryResultListener);

                for (AccessoryResultListener resultListener : accessoryResultListeners) {
                    resultListener.setActive(resultListener.equals(accessoryResultListener));
                }

                // disable the other tabs
                int selectedIndex = tabbedPane.getSelectedIndex();
                LOGGER.info("Disable the unselected tabs, selectedIndex: {}", selectedIndex);
                for (int index = 0; index < tabbedPane.getTabCount(); index++) {
                    tabbedPane.setEnabledAt(index, index == selectedIndex);
                }

                // send the request
                for (AccessoryViewListener l : listeners) {
                    l.sendAccessoryRequest(dccAddress, aspect, switchTime, timeBaseUnit, timingControl);
                }
            }
        };

        // add the content to the tabbed pane
        final DccAccessoryPanel dccAccessoryPanel = new DccAccessoryPanel(accessoryModel);
        dccAccessoryPanel.addAccessoryRequestListener(accessoryRequestListener);
        accessoryResultListeners.add(dccAccessoryPanel);

        final DccExtAccessoryPanel dccExtAccessoryPanel = new DccExtAccessoryPanel(accessoryModel);
        dccExtAccessoryPanel.addAccessoryRequestListener(accessoryRequestListener);
        accessoryResultListeners.add(dccExtAccessoryPanel);

        tabbedPane.addTab(Resources.getString(getClass(), "tab-normal"), null/* icon */,
            dccAccessoryPanel.createPanel(), Resources.getString(getClass(), "tab-normal.tooltip"));
        tabbedPane.setMnemonicAt(0, KeyEvent.VK_1);
        tabbedPane.addTab(Resources.getString(getClass(), "tab-extended"), null/* icon */,
            dccExtAccessoryPanel.createPanel(), Resources.getString(getClass(), "tab-extended.tooltip"));
        tabbedPane.setMnemonicAt(1, KeyEvent.VK_2);

        DefaultFormBuilder dialogBuilder = null;
        boolean debugDialog = false;
        if (debugDialog) {
            JPanel panel = new FormDebugPanel();
            dialogBuilder = new DefaultFormBuilder(new FormLayout(ENCODED_DIALOG_COLUMN_SPECS), panel);
        }
        else {
            JPanel panel = new JPanel(new BorderLayout());
            dialogBuilder = new DefaultFormBuilder(new FormLayout(ENCODED_DIALOG_COLUMN_SPECS), panel);
        }
        dialogBuilder.border(Borders.DIALOG);

        dialogBuilder.appendRow("fill:p:grow");
        dialogBuilder.append(tabbedPane, 2);
        dialogBuilder.nextLine();

        dialogBuilder.appendRow("3dlu");
        dialogBuilder.nextLine();

        // prepare the close button
        JPanel buttons = new ButtonBarBuilder().addButton(clearButton).addGlue().addButton(closeButton).build();

        dialogBuilder.appendRow("p");
        // builder.nextLine(2);
        dialogBuilder.append(buttons, 2);

        clearButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                // clear the logger area of the selected tab
                Component comp = tabbedPane.getSelectedComponent();
                if (comp instanceof LogAreaAwarePanel) {
                    ((LogAreaAwarePanel) comp).clearLogArea();
                }
            }
        });

        contentPanel = dialogBuilder.build();
        getContentPane().setLayout(new BorderLayout());
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        pack();

        setMinimumSize(getPreferredSize());

        closeButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                tryCloseDialog();
            }
        });

        // add accessory model listener
        // accessoryModel.addPropertyChangeListener(AccessoryModel.PROPERTYNAME_ASPECT, new PropertyChangeListener() {
        //
        // @Override
        // public void propertyChange(PropertyChangeEvent evt) {
        // // prepare the data for the accessory request
        // Integer aspect = accessoryModel.getAspect();
        // Integer dccAddress = accessoryModel.getDccAddress();
        //
        // Integer switchTime = accessoryModel.getSwitchTime();
        // TimeBaseUnitEnum timeBaseUnit = accessoryModel.getTimeBaseUnit();
        // TimingControlEnum timingControl = accessoryModel.getTimingControl();
        // LOGGER.info("Set the new aspect: {}, dccAddress: {}, timeingControl: {}", aspect, dccAddress,
        // timingControl);
        //
        // // send to bidib
        // for (AccessoryViewListener listener : listeners) {
        // listener.sendAccessoryRequest(dccAddress, aspect, switchTime, timeBaseUnit);
        // }
        // }
        // });

        accessoryModel.addPropertyChangeListener(AccessoryModel.PROPERTYNAME_ACKNOWLEDGE, new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                final AccessoryAcknowledge acknowledge = accessoryModel.getAcknowledge();
                LOGGER.info("Accessory acknowledge has changed: {}", acknowledge);
                // if (acknowledge != null) {
                // addLogText("Received acknowledge: {}", acknowledge);
                // }
                if (SwingUtilities.isEventDispatchThread()) {
                    signalAcknowledgeChanged(acknowledge);
                }
                else {
                    SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            signalAcknowledgeChanged(acknowledge);
                        }
                    });
                }
            }
        });

        pack();
        setMinimumSize(getSize());
        setVisible(true);
    }

    @Override
    protected void performCancelAction(KeyEvent e) {
        tryCloseDialog();
    }

    private void tryCloseDialog() {
        // if (!firmwareModel.isInProgress()) {
        setVisible(false);
        fireClose();
        // }
        // else {
        // LOGGER.warn("Firmware update is in progress! Dialog is not closed!");
        // }
    }

    private void fireClose() {
        for (AccessoryViewListener l : listeners) {
            l.close();
        }
    }

    public void addAccessoryViewListener(AccessoryViewListener l) {
        listeners.add(l);
    }

    private void signalAcknowledgeChanged(AccessoryAcknowledge acknowledge) {

        // enable the tabs before the result is passed to the listeners
        if (acknowledge != null) {
            // enable all tabs
            LOGGER.info("Enable the tabs");
            for (int index = 0; index < tabbedPane.getTabCount(); index++) {
                tabbedPane.setEnabledAt(index, true);
            }
        }

        for (AccessoryResultListener listener : accessoryResultListeners) {
            listener.signalAcknowledgeChanged(acknowledge);
        }
    }

}
