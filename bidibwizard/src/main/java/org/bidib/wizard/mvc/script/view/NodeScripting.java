package org.bidib.wizard.mvc.script.view;

import java.util.Map;

import org.bidib.jbidibc.core.port.PortConfigValue;
import org.bidib.wizard.mvc.main.model.Accessory;
import org.bidib.wizard.mvc.main.model.Macro;
import org.bidib.wizard.script.Scripting;
import org.bidib.wizard.script.node.types.CvType;
import org.bidib.wizard.script.node.types.TargetType;

public interface NodeScripting extends Scripting {

    /**
     * Set a CV value.
     * 
     * @param uuid
     *            the uuid of the node
     * @param cvType
     *            the CV
     */
    void setCv(Long uuid, CvType... cvType);

    /**
     * Set the label.
     * 
     * @param uuid
     *            the uuid of the node
     * @param portType
     *            the port label info
     */
    void setLabel(Long uuid, TargetType portType);

    void setMacro(Long uuid, Macro macro);

    void setAccessory(Long uuid, Accessory accessory);

    void resetNode(Long uuid);

    void reselectNode(Long uuid);

    void setPortConfig(Long uuid, TargetType portType, final Map<Byte, PortConfigValue<?>> portConfig);
}
