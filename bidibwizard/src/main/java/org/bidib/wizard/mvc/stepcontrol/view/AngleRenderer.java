package org.bidib.wizard.mvc.stepcontrol.view;

import java.text.DecimalFormat;

import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AngleRenderer extends DefaultTableCellRenderer {

    private static final Logger LOGGER = LoggerFactory.getLogger(AngleRenderer.class);

    private static final long serialVersionUID = 1L;

    private DecimalFormat df;

    private long totalSteps = 1;

    public AngleRenderer(DecimalFormat df) {
        this.df = df;
        this.setHorizontalAlignment(SwingConstants.RIGHT);
    }

    @Override
    protected void setValue(Object value) {
        setText((value == null) ? "" : df.format(getPositionAsAngle(value)));
    }

    /**
     * @return the position
     */
    private float getPositionAsAngle(Object pos) {
        Long position = (Long) pos;
        return (position * 360.0f) / totalSteps;
    }

    /**
     * @return the totalSteps
     */
    public long getTotalSteps() {
        return totalSteps;
    }

    /**
     * @param totalSteps
     *            the totalSteps to set
     */
    public void setTotalSteps(long totalSteps) {
        if (totalSteps == 0) {
            // prevent NPE during rendering
            totalSteps = 1;
        }
        LOGGER.info("Set the number of total steps: {}", totalSteps);

        this.totalSteps = totalSteps;
    }

}
