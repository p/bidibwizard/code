package org.bidib.wizard.mvc.dmx.view.scenery;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.dialog.EscapeDialog;
import org.bidib.wizard.mvc.common.view.binding.MultiListSelectionAdapter;
import org.bidib.wizard.mvc.common.view.binding.MultiSelectionInList;
import org.bidib.wizard.mvc.dmx.model.DmxChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.value.ValueHolder;
import com.jgoodies.common.collect.ObservableList;
import com.jgoodies.forms.builder.ButtonBarBuilder;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.debug.FormDebugPanel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.FormLayout;

public class DmxChannelSelectionPanel {
    private static final Logger LOGGER = LoggerFactory.getLogger(DmxChannelSelectionPanel.class);

    private static final String ENCODED_COLUMN_SPECS = "pref, 3dlu, fill:200dlu:grow";

    private final JButton cancelButton = new JButton(Resources.getString(getClass(), "cancel"));

    private final JButton applyButton = new JButton(Resources.getString(getClass(), "apply"));

    private JList<DmxChannel> channelList;

    private MultiSelectionInList<DmxChannel> channelSelection;

    private ObservableList<DmxChannel> selectionHolder;

    public DmxChannelSelectionPanel() {

    }

    private JPanel createPanel(List<DmxChannel> dmxChannels) {
        DefaultFormBuilder builder = null;
        boolean debug = false;
        if (debug) {
            builder = new DefaultFormBuilder(new FormLayout(ENCODED_COLUMN_SPECS), new FormDebugPanel());
        }
        else {
            builder = new DefaultFormBuilder(new FormLayout(ENCODED_COLUMN_SPECS), new JPanel());
        }
        builder.border(Borders.TABBED_DIALOG);

        channelSelection = new MultiSelectionInList<DmxChannel>(dmxChannels.toArray(new DmxChannel[0]));
        selectionHolder = channelSelection.getSelection();

        channelList = new JList<DmxChannel>();
        channelList.setModel(channelSelection.getList());
        channelList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        channelList.setSelectionModel(new MultiListSelectionAdapter(channelSelection));

        builder.append(new JScrollPane(channelList), 3);

        // prepare the close button
        JPanel buttons = new ButtonBarBuilder().addGlue().addButton(cancelButton, applyButton).build();
        builder.append(buttons, 3);

        return builder.build();
    }

    public List<DmxChannel> showDialog(Frame frame, final List<DmxChannel> dmxChannels) {
        JPanel dialogPanel = createPanel(dmxChannels);

        final DmxChannelSelectionDialog dialog =
            new DmxChannelSelectionDialog(frame, Resources.getString(getClass(), "title"), dialogPanel);

        final ValueHolder selectedDmxChannels = new ValueHolder();

        applyButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                // get the selected channels
                LOGGER.info("selectedDmxChannel: {}", selectionHolder);

                List<DmxChannel> selectedChannels = Collections.unmodifiableList(selectionHolder);

                selectedDmxChannels.setValue(selectedChannels);

                dialog.close();
            }
        });
        cancelButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                // close the dialog
                dialog.close();
            }
        });

        dialog.showDialog();

        if (selectedDmxChannels.getValue() == null) {
            selectedDmxChannels.setValue(Collections.emptyList());
        }

        LOGGER.info("Selected DMX channels: {}", selectedDmxChannels.getValue());

        return (List<DmxChannel>) selectedDmxChannels.getValue();
    }

    private static final class DmxChannelSelectionDialog extends EscapeDialog {
        private static final long serialVersionUID = 1L;

        public DmxChannelSelectionDialog(Frame frame, String title, JPanel dialogPanel) {
            super(frame, title, true);

            Container contentPane = getContentPane();
            contentPane.setLayout(new BorderLayout());
            contentPane.add(dialogPanel, BorderLayout.CENTER);
        }

        private void close() {
            setVisible(false);

            dispose();
        }

        public void showDialog() {

            pack();
            setLocationRelativeTo(null);
            setVisible(true);
        }
    }
}
