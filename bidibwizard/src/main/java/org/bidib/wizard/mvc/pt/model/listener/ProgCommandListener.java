package org.bidib.wizard.mvc.pt.model.listener;

import org.bidib.jbidibc.core.enumeration.CommandStationProgState;

public interface ProgCommandListener {

    /**
     * The programming command has finished.
     * 
     * @param commandStationProgState
     *            the new programming state
     */
    void progCommandStationFinished(CommandStationProgState commandStationProgState);
}
