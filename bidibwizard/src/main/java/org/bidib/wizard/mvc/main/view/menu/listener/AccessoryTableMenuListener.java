package org.bidib.wizard.mvc.main.view.menu.listener;

public interface AccessoryTableMenuListener {
    void delete();

    void insertEmptyAfter();

    void insertEmptyBefore();

    void selectAll();

    void copy();

    void cut();

    void pasteAfter();
}
