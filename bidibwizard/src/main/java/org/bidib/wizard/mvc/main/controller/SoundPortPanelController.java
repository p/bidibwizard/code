package org.bidib.wizard.mvc.main.controller;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.JOptionPane;

import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.PortConfigKeys;
import org.bidib.jbidibc.core.exception.InvalidConfigurationException;
import org.bidib.jbidibc.core.port.PortConfigValue;
import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.comm.CommunicationFactory;
import org.bidib.wizard.comm.SoundPortStatus;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.labels.Labels;
import org.bidib.wizard.labels.SoundPortLabelFactory;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.bidib.wizard.mvc.main.controller.wrapper.NodePortWrapper;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.model.Port;
import org.bidib.wizard.mvc.main.model.SoundPort;
import org.bidib.wizard.mvc.main.model.listener.DefaultNodeListListener;
import org.bidib.wizard.mvc.main.model.listener.SoundPortListener;
import org.bidib.wizard.mvc.main.view.panel.SoundPortListPanel;
import org.bidib.wizard.mvc.script.view.ScriptParser;
import org.bidib.wizard.script.ScriptCommand;
import org.bidib.wizard.script.engine.ScriptEngine;
import org.bidib.wizard.script.switching.SoundPortCommand;
import org.bidib.wizard.script.switching.WaitCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SoundPortPanelController implements PortScripting {

    private static final Logger LOGGER = LoggerFactory.getLogger(SoundPortPanelController.class);

    private final MainModel mainModel;

    private final Map<Node, NodePortWrapper> testToggleRegistry = new LinkedHashMap<>();

    public SoundPortPanelController(final MainModel mainModel) {
        this.mainModel = mainModel;
    }

    public SoundPortListPanel createPanel() {
        final Labels switchPortLabels =
            DefaultApplicationContext.getInstance().get(DefaultApplicationContext.KEY_SOUNDPORT_LABELS, Labels.class);

        final SoundPortListPanel soundPortListPanel = new SoundPortListPanel(this, mainModel);

        soundPortListPanel.addPortListener(new SoundPortListener<SoundPortStatus>() {
            @Override
            public void labelChanged(Port<SoundPortStatus> port, String label) {
                port.setLabel(label);

                try {
                    SoundPortLabelFactory factory = new SoundPortLabelFactory();
                    long uniqueId = mainModel.getSelectedNode().getNode().getUniqueId();
                    factory.replaceLabel(switchPortLabels, uniqueId, port.getId(), label);

                    factory.saveLabels(uniqueId, switchPortLabels);
                }
                catch (InvalidConfigurationException ex) {
                    LOGGER.warn("Save switch port labels failed.", ex);

                    String labelPath = ex.getReason();
                    JOptionPane.showMessageDialog(JOptionPane.getFrameForComponent(null),
                        Resources.getString(Labels.class, "labelfileerror.message", new Object[] { labelPath }),
                        Resources.getString(Labels.class, "labelfileerror.title"), JOptionPane.ERROR_MESSAGE);
                }

                soundPortListPanel.repaint();
            }

            @Override
            public void statusChanged(Port<SoundPortStatus> port, SoundPortStatus status) {
                LOGGER.debug("Status of sound port has changed, port: {}", port);
            }

            @Override
            public void valuesChanged(SoundPort port, PortConfigKeys... portConfigKeys) {

                LOGGER.info("The port value are changed for port: {}", port);

                Map<Byte, PortConfigValue<?>> values = new LinkedHashMap<>();

                for (PortConfigKeys key : portConfigKeys) {
                    switch (key) {
                        default:
                            LOGGER.warn("Unsupported port config key detected: {}", key);
                            break;
                    }
                }

                // don't set the port type param to not send BIDIB_PCFG_RECONFIG
                CommunicationFactory.getInstance().setPortParameters(mainModel.getSelectedNode().getNode(), port, null,
                    values);
            }

            @Override
            public void testButtonPressed(Port<SoundPortStatus> port) {
                LOGGER.info("The test button was pressed for port: {}", port);

                Node node = mainModel.getSelectedNode();
                if (SoundPortStatus.TEST != port.getStatus()) {
                    stopTestToggleTask(node, port);

                    CommunicationFactory.getInstance().activateSoundPort(node.getNode(), port.getId(),
                        (SoundPortStatus) port.getStatus());
                }
                else {
                    addTestToggleTask(node, port);
                }
            }

            @Override
            public void configChanged(Port<SoundPortStatus> port) {
            }

            @Override
            public void changePortType(LcOutputType portType, SoundPort port) {
                LOGGER.info("The port type will change to: {}, port: {}", portType, port);

                Map<Byte, PortConfigValue<?>> values = new LinkedHashMap<>();

                CommunicationFactory.getInstance().setPortParameters(mainModel.getSelectedNode().getNode(), port,
                    portType, values);
            }
        });

        mainModel.addNodeListListener(new DefaultNodeListListener() {
            @Override
            public void nodeWillChange() {
                LOGGER.info("The selected node will change!");
                try {
                    List<Node> nodes = new LinkedList<>();
                    for (Node node : testToggleRegistry.keySet()) {
                        nodes.add(node);
                    }
                    LOGGER.info("Found nodes to stop the test toggle task: {}", nodes);
                    for (Node node : nodes) {
                        stopTestToggleTask(node, null);
                    }
                    LOGGER.info("Stop the test toggle task passed for nodes: {}", nodes);
                }
                catch (Exception ex) {
                    LOGGER.warn("Stop test toggle tasks failed.", ex);
                }
            }
        });

        return soundPortListPanel;
    }

    public void addTestToggleTask(final Node node, final Port<?> port) {
        LOGGER.info("Add test toggle task for node: {}, port: {}", node, port);

        NodePortWrapper nodePortWrapper = testToggleRegistry.remove(node);
        ScriptEngine<PortScripting> scriptEngine = null;
        if (nodePortWrapper != null) {
            scriptEngine = nodePortWrapper.removePort(port);
        }

        if (scriptEngine != null) {
            LOGGER.info("Found a node scripting engine in the registry: {}", scriptEngine);
            try {
                scriptEngine.stopScript(Long.valueOf(2000));
            }
            catch (Exception ex) {
                LOGGER.warn("Stop script failed.", ex);
            }
        }

        HashMap<String, Object> context = new LinkedHashMap<>();
        context.put(ScriptParser.KEY_SELECTED_NODE, node);
        context.put(ScriptParser.KEY_MAIN_MODEL, mainModel);

        scriptEngine = new ScriptEngine<PortScripting>(this, context);

        List<ScriptCommand<PortScripting>> scriptCommands = new LinkedList<ScriptCommand<PortScripting>>();
        SoundPortCommand<PortScripting> spc = new SoundPortCommand<>();
        spc.parse("SOUND " + port.getId() + " PLAY");
        scriptCommands.add(spc);
        WaitCommand<PortScripting> wc = new WaitCommand<>();
        wc.parse("WAIT 2000");
        scriptCommands.add(wc);
        spc = new SoundPortCommand<>();
        spc.parse("SOUND " + port.getId() + " STOP");
        scriptCommands.add(spc);
        wc = new WaitCommand<>();
        wc.parse("WAIT 2000");
        scriptCommands.add(wc);

        LOGGER.info("Prepared list of commands: {}", scriptCommands);

        scriptEngine.setScriptCommands(scriptCommands);
        // repeating
        scriptEngine.setScriptRepeating(true);

        if (nodePortWrapper == null) {
            LOGGER.info("Create new NodePortWrapper for node: {}", node);
            nodePortWrapper = new NodePortWrapper(node);
        }

        LOGGER.info("Put script engine in registry for node: {}", node);
        nodePortWrapper.addPort(port, scriptEngine);

        testToggleRegistry.put(node, nodePortWrapper);

        scriptEngine.startScript();
    }

    public void stopTestToggleTask(final Node node, final Port<?> port) {
        LOGGER.info("Stop test toggle task for node: {}, port: {}", node, port);

        NodePortWrapper nodePortWrapper = testToggleRegistry.get(node);

        if (nodePortWrapper != null) {
            Set<Port<?>> toRemove = new HashSet<>();
            if (port != null) {
                toRemove.add(port);
            }
            else {
                toRemove.addAll(nodePortWrapper.getKeySet());
            }

            for (Port<?> removePort : toRemove) {
                ScriptEngine<PortScripting> engine = nodePortWrapper.removePort(removePort);

                if (engine != null) {
                    LOGGER.info("Found a node scripting engine in the registry: {}", engine);
                    try {
                        engine.stopScript(Long.valueOf(2000));
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Stop script failed.", ex);
                    }
                }
                else {
                    LOGGER.warn("No scripting engine found for node: {}", node);
                }
            }

            if (nodePortWrapper.isEmpty()) {
                LOGGER.info("No more ports registered for node: {}", node);
                testToggleRegistry.remove(node);
            }
        }
    }

    @Override
    public void sendPortStatusAction(int port, BidibStatus portStatus) {

        LOGGER.info("Sound action on the port: {}, portStatus: {}", port, portStatus);
        try {
            Node node = mainModel.getSelectedNode();
            SoundPortStatus soundPortStatus = (SoundPortStatus) portStatus;
            CommunicationFactory.getInstance().activateSoundPort(node.getNode(), port, soundPortStatus);
        }
        catch (Exception ex) {
            LOGGER.warn("Activate sound port failed.", ex);
        }
    }

    @Override
    public void sendPortValueAction(int port, int portValue) {

    }
}
