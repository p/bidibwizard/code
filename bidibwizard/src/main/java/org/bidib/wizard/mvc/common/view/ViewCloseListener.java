package org.bidib.wizard.mvc.common.view;

public interface ViewCloseListener {

    void close();
}
