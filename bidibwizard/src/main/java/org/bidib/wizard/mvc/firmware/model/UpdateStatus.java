package org.bidib.wizard.mvc.firmware.model;

public enum UpdateStatus {
    NONE("none"), PREPARE("prepare"), ENTRY_PASSED("entry-passed"), DATA_TRANSFER("data-transfer"), DATA_TRANSFER_PASSED(
        "data-transfer-passed"), EXIT_PASSED("exit-passed"), PREPARE_FAILED("prepare-failed"), DATA_TRANSFER_FAILED(
        "data-transfer-failed"), NODE_LOST("node-lost");
    private String code;

    UpdateStatus(String code) {
        this.code = code;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }
}