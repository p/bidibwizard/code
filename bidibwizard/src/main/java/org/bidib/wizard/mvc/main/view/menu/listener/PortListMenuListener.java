package org.bidib.wizard.mvc.main.view.menu.listener;

public interface PortListMenuListener extends LabelListMenuListener {
    /**
     * Map the port as activated.
     */
    void mapPort();

    /**
     * Insert ports.
     */
    void insertPorts();
}
