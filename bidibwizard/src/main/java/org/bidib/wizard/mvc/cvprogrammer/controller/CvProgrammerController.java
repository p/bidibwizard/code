package org.bidib.wizard.mvc.cvprogrammer.controller;

import java.util.Collection;
import java.util.LinkedList;

import javax.swing.JFrame;

import org.bidib.jbidibc.core.AddressData;
import org.bidib.jbidibc.core.DefaultMessageListener;
import org.bidib.jbidibc.core.MessageListener;
import org.bidib.jbidibc.core.enumeration.AddressTypeEnum;
import org.bidib.jbidibc.core.enumeration.CommandStationPom;
import org.bidib.jbidibc.core.enumeration.PomAddressing;
import org.bidib.jbidibc.core.enumeration.PomDecoder;
import org.bidib.jbidibc.core.enumeration.PomOperation;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.wizard.comm.Communication;
import org.bidib.wizard.comm.CommunicationFactory;
import org.bidib.wizard.mvc.cvprogrammer.controller.listener.CvProgrammerControllerListener;
import org.bidib.wizard.mvc.cvprogrammer.model.CvProgrammerModel;
import org.bidib.wizard.mvc.cvprogrammer.view.CvProgrammerView;
import org.bidib.wizard.mvc.cvprogrammer.view.listener.CvProgrammerViewListener;
import org.bidib.wizard.mvc.main.model.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CvProgrammerController {
    private static final Logger LOGGER = LoggerFactory.getLogger(CvProgrammerController.class);

    private final Collection<CvProgrammerControllerListener> listeners =
        new LinkedList<CvProgrammerControllerListener>();

    private final JFrame parent;

    private final Node node;

    private final int x;

    private final int y;

    private final CvProgrammerModel model = new CvProgrammerModel();

    private CvProgrammerView view;

    private MessageListener messageListener;

    public CvProgrammerController(Node node, JFrame parent, int x, int y) {
        this.parent = parent;
        this.node = node;
        this.x = x;
        this.y = y;
    }

    public void addCvProgrammerControllerListener(CvProgrammerControllerListener l) {
        listeners.add(l);
    }

    private void fireClose() {
        for (CvProgrammerControllerListener l : listeners) {
            l.close();
        }
    }

    private void fireSendRequest(
        PomDecoder action, int decoderAddress, PomOperation operation, PomAddressing addressing, int cvNumber,
        int cvValue) {

        LOGGER.info(
            "Send CV request, action: {}, decoder address: {}, operation: {}, addressing: {}, cvNumber: {}, value: {}",
            action, decoderAddress, operation, addressing, cvNumber, ByteUtils.byteToHex(cvValue));

        AddressTypeEnum addressType = null;
        switch (action) {
            case LOCO:
                addressType = AddressTypeEnum.LOCOMOTIVE_FORWARD;
                break;
            case ACCESSORY:
                addressType = AddressTypeEnum.ACCESSORY;
                break;
            case EXT_ACCESSORY:
                addressType = AddressTypeEnum.EXTENDED_ACCESSORY;
                break;
            default:
                break;
        }
        AddressData address = new AddressData(decoderAddress, addressType);

        CommandStationPom opCode =
            CommandStationPom.valueOf(ByteUtils.getLowByte(operation.getType() | addressing.getType()));

        LOGGER.info("Prepared opCode: {}", opCode);

        for (CvProgrammerControllerListener l : listeners) {
            l.sendRequest(node, address, opCode, cvNumber, cvValue);
        }
    }

    public void start() {
        final Communication communication = CommunicationFactory.getInstance();

        view = new CvProgrammerView(parent, model, x, y);
        view.addCvProgrammerViewListener(new CvProgrammerViewListener() {
            @Override
            public void close() {
                if (messageListener != null) {
                    LOGGER.info("Remove the message listener.");
                    communication.removeMessageListener(messageListener);

                    messageListener = null;
                }
                fireClose();
            }

            @Override
            public void sendRequest(
                PomDecoder action, int address, PomOperation operation, PomAddressing addressing, int cvNumber,
                int cvValue) {
                fireSendRequest(action, address, operation, addressing, cvNumber, cvValue);
            }
        });

        messageListener = new DefaultMessageListener() {
            @Override
            public void feedbackCv(byte[] address, AddressData decoderAddress, int cvNumber, int dat) {
                LOGGER.info("CV was received, node addr: {}, decoder address: {}, cvNumber: {}, dat: {}", address,
                    decoderAddress, cvNumber, dat);
                // TODO compare the decoder address ...
                AddressData addressData = model.getSelectedDecoderAddress();
                // if (addressData != null && addressData.getAddress() == decoderAddress) {
                if (addressData != null && addressData.equals(decoderAddress)) {
                    // update the value
                    writeCvValue(dat);
                }
                else {
                    LOGGER.info("Received CV feedback for another decoder.");
                }
            }
        };

        communication.addMessageListener(messageListener);

    }

    private void writeCvValue(int cvValue) {
        model.setValue(cvValue);
    }
}
