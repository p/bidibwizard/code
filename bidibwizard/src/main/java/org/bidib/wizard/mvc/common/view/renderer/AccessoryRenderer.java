package org.bidib.wizard.mvc.common.view.renderer;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import org.apache.commons.lang.StringUtils;
import org.bidib.wizard.mvc.main.model.Accessory;

public class AccessoryRenderer extends JLabel implements ListCellRenderer<Accessory> {

    private static final long serialVersionUID = 1L;

    public AccessoryRenderer() {
        setOpaque(true);
    }

    public Component getListCellRendererComponent(
        JList<? extends Accessory> list, Accessory value, int index, boolean isSelected, boolean cellHasFocus) {

        if (isSelected) {
            setBackground(list.getSelectionBackground());
            setForeground(list.getSelectionForeground());
        }
        else {
            setBackground(list.getBackground());
            setForeground(list.getForeground());
        }

        if (value != null) {
            String text = null;
            if (StringUtils.isNotBlank(value.getLabel())) {
                text = String.format("%1$02d : %2$s", value.getId(), value.getLabel());
            }
            else if (value.getId() > -1) {
                text = String.format("%1$02d : ", value.getId());
            }
            else {
                text = " ";
            }
            setText(text);
        }
        else {
            setText(null);
        }
        return this;
    }
}
