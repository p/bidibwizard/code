package org.bidib.wizard.mvc.common.view.cvdefinition;

import java.util.List;

import org.bidib.jbidibc.core.node.ConfigurationVariable;

public interface CvDefintionPanelProvider {

    /**
     * Check if the panel has pending changes.
     */
    void checkPendingChanges();

    /**
     * Write the config variables.
     * 
     * @param cvList
     *            the list of config variables
     */
    void writeConfigVariables(final List<ConfigurationVariable> cvList);

    /**
     * Refresh the displayed values.
     */
    void refreshDisplayedValues();
}
