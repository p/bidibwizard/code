package org.bidib.wizard.mvc.script.view.input;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;

import javax.swing.JTextField;

import org.apache.commons.lang3.StringUtils;
import org.bidib.wizard.common.context.ApplicationContext;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.common.view.converter.StringConverter;
import org.bidib.wizard.utils.InputValidationDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.binding.value.ConverterValueModel;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.view.ValidationComponentUtils;

public class IntegerInputPanel extends AbstractInputSelectionPanel {
    private static final Logger LOGGER = LoggerFactory.getLogger(IntegerInputPanel.class);

    private ValueModel integerValueModel;

    private ItemSelectionModel<Integer> integerSelectionModel;

    @Override
    public void initialize(
        final ApplicationContext context, final DefaultFormBuilder dialogBuilder, ValidationSupport validationSupport,
        String variableName, String variableType, String caption, String defaultValue, String prevValue) {
        this.variableName = variableName;
        setDefaultValue(defaultValue);

        integerSelectionModel = new ItemSelectionModel<>();

        integerValueModel =
            new PropertyAdapter<ItemSelectionModel<Integer>>(integerSelectionModel,
                ItemSelectionModel.PROPERTY_SELECTED_ITEM, true);

        final ValueModel integerValueConverterModel =
            new ConverterValueModel(integerValueModel, new StringConverter(new DecimalFormat("#")));

        JTextField textInput = BasicComponentFactory.createTextField(integerValueConverterModel, false);
        textInput.setDocument(new InputValidationDocument(4, InputValidationDocument.NUMERIC));

        caption = "<html>" + caption + "</html>";

        dialogBuilder.append(caption, textInput);

        ValidationComponentUtils.setMandatory(textInput, true);
        ValidationComponentUtils.setMessageKeys(textInput, "validation." + variableName);

        String initialValue = defaultValue;
        if (StringUtils.isNotBlank(prevValue)) {
            initialValue = prevValue;
        }

        if (StringUtils.isNotBlank(initialValue)) {
            try {
                Integer def = Integer.valueOf(initialValue);
                LOGGER.info("Set the initial value: {}", def);
                integerSelectionModel.setSelectedItem(def);
            }
            catch (Exception ex) {
                LOGGER.warn("Parse initial value to integer failed: {}", initialValue, ex);
            }
        }

        content = dialogBuilder.build();

        // validate on change
        integerSelectionModel.addPropertyChangeListener(ItemSelectionModel.PROPERTY_SELECTED_ITEM,
            new PropertyChangeListener() {
                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    validationSupport.triggerValidation();
                }
            });
    }

    public void validate(final ValidationResult validationResult) {
        if (integerValueModel.getValue() == null) {
            validationResult.addError(variableName + " " + Resources.getString((Class<?>) null, "not_empty"),
                "validation." + variableName);
        }
    }

    @Override
    public Object getSelectedValue() {
        if (integerValueModel.getValue() != null) {
            return integerValueModel.getValue();
        }
        return null;
    }
}
