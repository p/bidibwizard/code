package org.bidib.wizard.mvc.main.view.statusbar;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

import org.apache.commons.lang.StringUtils;
import org.bidib.wizard.comm.BoosterStatus;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.darrylbu.util.SwingUtils;
import org.bidib.wizard.mvc.common.model.PreferencesPortType;
import org.bidib.wizard.mvc.common.view.panel.DisabledPanel;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.listener.BoosterStatusListener;
import org.bidib.wizard.mvc.main.model.listener.DefaultStatusListener;
import org.bidib.wizard.mvc.main.view.component.DigitalClock;
import org.bidib.wizard.mvc.main.view.panel.LedPanel;
import org.bidib.wizard.mvc.main.view.panel.LedPanel.Axis;
import org.bidib.wizard.mvc.main.view.panel.StatusButtonPanel;
import org.bidib.wizard.mvc.main.view.panel.listener.StatusListener;
import org.bidib.wizard.mvc.preferences.model.Preferences;
import org.bidib.wizard.mvc.preferences.model.listener.PreferencesAdapter;
import org.jdesktop.swingx.JXStatusBar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StatusBar extends JXStatusBar {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(StatusBar.class);

    public static final int DISPLAY_ERROR = -2;

    public static final int DISPLAY_NORMAL = -1;

    private JLabel statusLabel;

    private DigitalClock modelClock;

    private final DisabledPanel disabledLedPanel;

    private StatusButtonPanel statusButtonPanel;

    /**
     * Constructor.
     */
    public StatusBar(final MainModel model) {

        statusLabel = new JLabel(" ");
        JXStatusBar.Constraint c1 = new JXStatusBar.Constraint(JXStatusBar.Constraint.ResizeBehavior.FILL); // Fill with
                                                                                                            // no
                                                                                                            // inserts
        add(statusLabel, c1);

        final Preferences preferences = Preferences.getInstance();

        boolean simulationMode = PreferencesPortType.isSimulation(preferences.getSelectedPortType());

        JLabel simulationLabel = new JLabel("Simulation", SwingConstants.CENTER);
        Font oldFont = simulationLabel.getFont();
        simulationLabel.setFont(oldFont.deriveFont(Font.BOLD, oldFont.getSize2D() + 2));
        simulationLabel.setForeground(new Color(190, 15, 20));

        LedPanel ledPanel = new LedPanel(model.getStatusModel(), Axis.lineAxis);
        disabledLedPanel = new DisabledPanel(ledPanel);
        disabledLedPanel.setDisabledOverlayComponent(simulationLabel);
        disabledLedPanel.setEnabled(!simulationMode);

        JXStatusBar.Constraint c2 = new JXStatusBar.Constraint(170); // Fixed width of 170 with no inserts
        add(disabledLedPanel, c2);

        JXStatusBar.Constraint c5 = new JXStatusBar.Constraint(50); // Fixed width of 50 with no inserts
        String serialPortProvider = preferences.getSerialPortProvider();
        final JLabel serialProviderLabel = new JLabel(serialPortProvider);
        add(serialProviderLabel, c5);

        statusButtonPanel = new StatusButtonPanel(model.getStatusModel(), StatusButtonPanel.Axis.lineAxis);
        statusButtonPanel.setButtonHeight(/* statusLabel.getHeight() */20);
        JXStatusBar.Constraint c4 = new JXStatusBar.Constraint(60); // Fixed width of 100 with no inserts
        add(statusButtonPanel.getButton(), c4);

        LOGGER.info("Create the digital clock.");
        modelClock = new DigitalClock();
        SwingUtils.setFontBold(modelClock, true);
        JXStatusBar.Constraint c3 = new JXStatusBar.Constraint(100); // Fixed width of 100 with no inserts
        add(modelClock, c3);
        modelClock.setToolTipText(Resources.getString(getClass(), "modeltime"));

        model.getStatusModel().addModelClockStatusListener(modelClock);

        model.addBoosterStatusListener(new BoosterStatusListener() {

            @Override
            public void voltageChanged(int voltage) {
            }

            @Override
            public void temperatureChanged(int temperature) {
            }

            @Override
            public void stateChanged(BoosterStatus status) {
            }

            @Override
            public void maximumCurrentChanged(int maximumCurrent) {
            }

            @Override
            public void currentChanged(final int current) {
                if (SwingUtilities.isEventDispatchThread()) {
                    internalCurrentChanged(current);
                }
                else {
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            internalCurrentChanged(current);
                        }
                    });
                }
            }

            private void internalCurrentChanged(final int current) {
                LOGGER.debug("Current has changed: {}", current);
                if (current > 4000) {
                    StringBuffer sb = new StringBuffer("Received high current with value: ");
                    sb.append(current);
                    sb.append("mA");
                    LOGGER.warn(sb.toString());
                    setStatus(sb.toString(), 30);
                }
            }
        });

        preferences.addPropertyChangeListener(Preferences.PROPERTY_SELECTED_PORTTYPE, new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                try {
                    boolean simulationMode = PreferencesPortType.isSimulation(preferences.getSelectedPortType());
                    disabledLedPanel.setEnabled(!simulationMode);
                }
                catch (Exception ex) {
                    LOGGER.warn("Change enabled state of led panel failed.", ex);
                }
            }
        });

        preferences.addPropertyChangeListener(Preferences.PROPERTY_SERIAL_PORT_PROVIDER, new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                try {
                    String serialPortProvider = preferences.getSerialPortProvider();
                    serialProviderLabel.setText(serialPortProvider);
                }
                catch (Exception ex) {
                    LOGGER.warn("Change display serial port provider failed.", ex);
                }
            }
        });

        model.getStatusModel().addStatusListener(new DefaultStatusListener() {
            @Override
            public void runningChanged(boolean running) {
                if (running) {
                    modelClock.start();
                }
                else {
                    modelClock.stop();
                }
            }
        });

        preferences.addPreferencesListener(new PreferencesAdapter() {
            @Override
            public void startTimeChanged(Date startTime) {
                modelClock.setStartTime(startTime);
            }

            @Override
            public void timeFactorChanged(int timeFactor) {
                modelClock.setTimeFactor(timeFactor);
            }
        });

    }

    /**
     * Display a message on the status bar.
     * 
     * @param message
     *            The message to be displayed.
     */
    public void setStatus(String message) {
        LOGGER.debug("Set status message: {}", message);

        setStatus(message, -1);
    }

    /**
     * Display a message on the status bar then clear it after specified time.
     * 
     * @param message
     *            The message to be displayed.
     * @param seconds
     *            Time wait for clearing the message (in seconds). Any value lesser than 1 disable this functionality.
     */
    public synchronized void setStatus(final String message, final int seconds) {
        LOGGER.info("Set the status text: {}, seconds: {}", message, seconds);

        // stop the clean timer if any assigned
        if (cleanTimer != null) {
            LOGGER.info("Stop the clean timer.");
            cleanTimer.stop();
            cleanTimer = null;
        }

        if (SwingUtilities.isEventDispatchThread()) {
            updateStatusText(message, seconds);
        }
        else {
            try {
                SwingUtilities.invokeLater(new Runnable() {

                    @Override
                    public void run() {
                        updateStatusText(message, seconds);
                    }
                });
            }
            catch (Exception ex) {
                LOGGER.warn("Update status text failed.", ex);
            }
        }
    }

    private void updateStatusText(String message, int seconds) {
        switch (seconds) {
            case DISPLAY_ERROR:
                statusLabel.setForeground(Color.RED);
                break;
            case DISPLAY_NORMAL:
                statusLabel.setForeground(Color.BLACK);
                break;
            default:
                statusLabel.setForeground(Color.RED);
                break;
        }
        if (StringUtils.isNotBlank(message)) {
            statusLabel.setText(" " + message);
        }
        else {
            statusLabel.setText(" ");
        }
        if (seconds > 0) {
            // start the clean timer
            cleanTimer = new Timer(seconds * 1000, new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    LOGGER.info("Reset the statusbar.");
                    statusLabel.setForeground(Color.BLACK);
                    statusLabel.setText(" ");
                }
            });
            cleanTimer.setRepeats(false);
            cleanTimer.start();
        }
    }

    private Timer cleanTimer;

    public void addStatusListener(StatusListener l) {
        // listen to changes of the status model
        statusButtonPanel.addStatusListener(l);
    }

}
