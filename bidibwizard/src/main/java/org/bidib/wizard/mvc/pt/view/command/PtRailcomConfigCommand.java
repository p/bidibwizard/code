package org.bidib.wizard.mvc.pt.view.command;

import org.bidib.jbidibc.core.enumeration.PtOperation;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.wizard.mvc.pt.view.panel.RailcomProgBeanModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PtRailcomConfigCommand extends PtOperationCommand<RailcomProgBeanModel> {
    private static final Logger LOGGER = LoggerFactory.getLogger(PtRailcomConfigCommand.class);

    private Boolean channel1;

    private Boolean channel2;

    private Boolean channelUsage;

    private Boolean railcomPlus;

    public PtRailcomConfigCommand(PtOperation ptOperation, int cvNumber, int cvValue) {
        super(ptOperation, cvNumber, cvValue);
    }

    /**
     * @param cvValueResult
     *            the cvValueResult to set
     */
    public void setCvValueResult(Integer cvValueResult) {
        super.setCvValueResult(cvValueResult);

        if (cvValueResult != null) {
            // set the railcom config
            setChannel1(ByteUtils.getBit(cvValueResult.intValue(), 0) == 0 ? Boolean.FALSE : Boolean.TRUE);
            setChannel2(ByteUtils.getBit(cvValueResult.intValue(), 1) == 0 ? Boolean.FALSE : Boolean.TRUE);
            setChannelUsage(ByteUtils.getBit(cvValueResult.intValue(), 2) == 0 ? Boolean.FALSE : Boolean.TRUE);
            setRailcomPlus(ByteUtils.getBit(cvValueResult.intValue(), 7) == 0 ? Boolean.FALSE : Boolean.TRUE);
        }
    }

    public void setChannel1(Boolean channel1) {
        this.channel1 = channel1;
    }

    public Boolean getChannel1() {
        return channel1;
    }

    public void setChannel2(Boolean channel2) {
        this.channel2 = channel2;
    }

    public Boolean getChannel2() {
        return channel2;
    }

    /**
     * @return the channelUsage
     */
    public Boolean getChannelUsage() {
        return channelUsage;
    }

    /**
     * @param channelUsage
     *            the channelUsage to set
     */
    public void setChannelUsage(Boolean channelUsage) {
        this.channelUsage = channelUsage;
    }

    /**
     * @return the railcomPlus
     */
    public Boolean getRailcomPlus() {
        return railcomPlus;
    }

    /**
     * @param railcomPlus
     *            the railcomPlus to set
     */
    public void setRailcomPlus(Boolean railcomPlus) {
        this.railcomPlus = railcomPlus;
    }

    @Override
    public void postExecute(final RailcomProgBeanModel railcomProgBeanModel) {
        super.postExecute(railcomProgBeanModel);

        // update the railcom config
        if (channel1 != null) {
            LOGGER.debug("Set the channel1: {}", channel1);
            railcomProgBeanModel.setChannel1(channel1);
        }
        if (channel2 != null) {
            LOGGER.debug("Set the channel2: {}", channel2);
            railcomProgBeanModel.setChannel2(channel2);
        }
        if (channelUsage != null) {
            LOGGER.info("Set the channelUsage: {}", channelUsage);
            railcomProgBeanModel.setChannelUsage(channelUsage);
        }
        if (railcomPlus != null) {
            LOGGER.debug("Set the railcomPlus: {}", railcomPlus);
            railcomProgBeanModel.setRailcomPlus(railcomPlus);
        }
    }
}
