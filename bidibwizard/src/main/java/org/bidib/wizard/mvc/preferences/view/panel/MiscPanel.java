package org.bidib.wizard.mvc.preferences.view.panel;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Collection;
import java.util.LinkedList;

import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.bidib.jbidibc.core.BidibInterface;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.preferences.model.PreferencesModel;
import org.bidib.wizard.mvc.preferences.view.listener.PreferencesViewListener;
import org.bidib.wizard.utils.InputValidationDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.debug.FormDebugPanel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.FormLayout;

/**
 * This panel displays miscellaneous preferences like selected comm port, path to log files or label files.
 * 
 */
public class MiscPanel {
    private static final Logger LOGGER = LoggerFactory.getLogger(MiscPanel.class);

    private static final String ENCODED_DIALOG_COLUMN_SPECS = "pref, 3dlu, fill:50dlu:grow";

    private final Collection<PreferencesViewListener> listeners = new LinkedList<PreferencesViewListener>();

    private final PreferencesModel model;

    private JPanel contentPanel;

    public MiscPanel(final PreferencesModel model) {
        this.model = model;
    }

    public JPanel createPanel() {

        DefaultFormBuilder dialogBuilder = null;
        boolean debugDialog = false;
        if (debugDialog) {
            JPanel panel = new FormDebugPanel();
            dialogBuilder = new DefaultFormBuilder(new FormLayout(ENCODED_DIALOG_COLUMN_SPECS), panel);
        }
        else {
            JPanel panel = new JPanel(new BorderLayout());
            dialogBuilder = new DefaultFormBuilder(new FormLayout(ENCODED_DIALOG_COLUMN_SPECS), panel);
        }
        dialogBuilder.border(Borders.TABBED_DIALOG);

        dialogBuilder.append(Resources.getString(getClass(), "labelPath") + ":");

        final JTextField labelPath = new JTextField();

        labelPath.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                setText();
            }

            public void removeUpdate(DocumentEvent e) {
                setText();
            }

            public void insertUpdate(DocumentEvent e) {
                setText();
            }

            private void setText() {
                model.setLabelV2Path(labelPath.getText());
            }
        });
        labelPath.setText(model.getLabelV2Path());
        dialogBuilder.append(labelPath);

        dialogBuilder.append(Resources.getString(getClass(), "logFilePath") + ":");

        final JTextField logFilePath = new JTextField();

        logFilePath.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                setText();
            }

            public void removeUpdate(DocumentEvent e) {
                setText();
            }

            public void insertUpdate(DocumentEvent e) {
                setText();
            }

            private void setText() {
                model.setLogFilePath(logFilePath.getText());
            }
        });
        logFilePath.setText(model.getLogFilePath());
        logFilePath.setPreferredSize(new Dimension(300, logFilePath.getPreferredSize().height));
        dialogBuilder.append(logFilePath);

        // logfile append
        JCheckBox logFileAppend =
            new JCheckBox(Resources.getString(getClass(), "logFileAppend"), model.isLogFileAppend());
        logFileAppend.setToolTipText(Resources.getString(getClass(), "logFileAppend.tooltip"));
        logFileAppend.setContentAreaFilled(false);
        logFileAppend.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                final boolean logFileAppend = e.getStateChange() == ItemEvent.SELECTED;
                fireLogFileAppendChanged(logFileAppend);
            }
        });
        dialogBuilder.append(logFileAppend, 3);

        JCheckBox powerUser = new JCheckBox(Resources.getString(getClass(), "powerUser"), model.isPowerUser());
        powerUser.setContentAreaFilled(false);
        powerUser.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                final boolean powerUser = e.getStateChange() == ItemEvent.SELECTED;
                firePowerUserChanged(powerUser);
            }
        });
        dialogBuilder.append(powerUser, 3);

        JCheckBox showBoosterTable =
            new JCheckBox(Resources.getString(getClass(), "showBoosterTable"), model.isShowBoosterTable());
        showBoosterTable.setContentAreaFilled(false);
        showBoosterTable.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                final boolean showBoosterTable = e.getStateChange() == ItemEvent.SELECTED;
                fireShowBoosterTableChanged(showBoosterTable);
            }
        });
        dialogBuilder.append(showBoosterTable, 3);

        JCheckBox alwaysShowProductNameInTree =
            new JCheckBox(Resources.getString(getClass(), "alwaysShowProductNameInTree"),
                model.isAlwaysShowProductNameInTree());
        alwaysShowProductNameInTree.setContentAreaFilled(false);
        alwaysShowProductNameInTree.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                final boolean alwaysShowProductNameInTree = e.getStateChange() == ItemEvent.SELECTED;
                fireAlwaysShowProductNameInTreeChanged(alwaysShowProductNameInTree);
            }
        });
        dialogBuilder.append(alwaysShowProductNameInTree, 3);

        //
        JCheckBox ignoreWaitTimeout =
            new JCheckBox(Resources.getString(getClass(), "ignoreWaitTimeout"), model.isIgnoreWaitTimeout());
        ignoreWaitTimeout.setContentAreaFilled(false);
        ignoreWaitTimeout.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                final boolean ignoreWaitTimeout = e.getStateChange() == ItemEvent.SELECTED;
                fireIgnoreWaitTimeoutChanged(ignoreWaitTimeout);
            }
        });
        dialogBuilder.append(ignoreWaitTimeout, 3);

        //
        JCheckBox ignoreWrongReceiveMessageNumber =
            new JCheckBox(Resources.getString(getClass(), "ignoreWrongReceiveMessageNumber"),
                model.isIgnoreWrongReceiveMessageNumber());
        ignoreWrongReceiveMessageNumber.setContentAreaFilled(false);
        ignoreWrongReceiveMessageNumber.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                final boolean ignoreWrongReceiveMessageNumber = e.getStateChange() == ItemEvent.SELECTED;
                fireIgnoreWrongReceiveMessageNumberChanged(ignoreWrongReceiveMessageNumber);
            }
        });
        dialogBuilder.append(ignoreWrongReceiveMessageNumber, 3);

        dialogBuilder.append(Resources.getString(getClass(), "firmwarePacketTimeout") + ":");
        final JTextField firmwarePacketTimeout = new JTextField();
        firmwarePacketTimeout.setDocument(new InputValidationDocument(4, InputValidationDocument.NUMERIC));

        firmwarePacketTimeout.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                setText();
            }

            public void removeUpdate(DocumentEvent e) {
                setText();
            }

            public void insertUpdate(DocumentEvent e) {
                setText();
            }

            private void setText() {
                try {
                    int timeout = Integer.parseInt(firmwarePacketTimeout.getText());
                    if (timeout < BidibInterface.DEFAULT_TIMEOUT) {
                        LOGGER.warn("The timeout value is too small, must be at least {}ms.",
                            BidibInterface.DEFAULT_TIMEOUT);
                    }
                    else {
                        model.setFirmwarePacketTimeout(timeout);
                    }
                }
                catch (Exception ex) {
                    LOGGER.warn("Parse value for firmwarePaketTimeout failed.", ex);
                }
            }
        });
        firmwarePacketTimeout.setText(String.valueOf(model.getFirmwarePacketTimeout()));
        dialogBuilder.append(firmwarePacketTimeout);

        // USB hotplug controller
        JCheckBox useHotPlugController =
            new JCheckBox(Resources.getString(getClass(), "useHotPlugController"), model.isUseHotPlugController());
        useHotPlugController.setContentAreaFilled(false);
        useHotPlugController.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                final boolean useHotPlugController = e.getStateChange() == ItemEvent.SELECTED;
                fireUseHotPlugControllerChanged(useHotPlugController);
            }
        });
        dialogBuilder.append(useHotPlugController, 3);

        // serial user hardware flow control
        JCheckBox serialUseHardwareFlowControl =
            new JCheckBox(Resources.getString(getClass(), "serialUseHardwareFlowControl"),
                model.isSerialUseHardwareFlowControl());
        serialUseHardwareFlowControl.setContentAreaFilled(false);
        serialUseHardwareFlowControl.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                final boolean serialUseHardwareFlowControl = e.getStateChange() == ItemEvent.SELECTED;
                fireSerialUseHardwareFlowControlChanged(serialUseHardwareFlowControl);
            }
        });
        dialogBuilder.append(serialUseHardwareFlowControl, 3);

        contentPanel = dialogBuilder.build();
        return contentPanel;
    }

    public void addPreferencesViewListener(PreferencesViewListener l) {
        listeners.add(l);
    }

    private void firePowerUserChanged(boolean powerUser) {
        for (PreferencesViewListener l : listeners) {
            l.powerUserChanged(powerUser);
        }
    }

    private void fireShowBoosterTableChanged(boolean showBoosterTable) {
        for (PreferencesViewListener l : listeners) {
            l.showBoosterTableChanged(showBoosterTable);
        }
    }

    private void fireAlwaysShowProductNameInTreeChanged(boolean alwaysShowProductNameInTree) {
        for (PreferencesViewListener l : listeners) {
            l.alwaysShowProductNameInTreeChanged(alwaysShowProductNameInTree);
        }
    }

    private void fireIgnoreWaitTimeoutChanged(boolean ignoreWaitTimeout) {
        for (PreferencesViewListener l : listeners) {
            l.ignoreWaitTimeoutChanged(ignoreWaitTimeout);
        }
    }

    private void fireIgnoreWrongReceiveMessageNumberChanged(boolean ignoreWrongReceiveMessageNumber) {
        for (PreferencesViewListener l : listeners) {
            l.ignoreWrongReceiveMessageNumberChanged(ignoreWrongReceiveMessageNumber);
        }
    }

    private void fireUseHotPlugControllerChanged(boolean useHotPlugController) {
        for (PreferencesViewListener l : listeners) {
            l.useHotPlugControllerChanged(useHotPlugController);
        }
    }

    private void fireSerialUseHardwareFlowControlChanged(boolean serialUseHardwareFlowControl) {
        for (PreferencesViewListener l : listeners) {
            l.serialUseHardwareFlowControlChanged(serialUseHardwareFlowControl);
        }
    }

    private void fireLogFileAppendChanged(boolean logFileAppend) {
        for (PreferencesViewListener l : listeners) {
            l.logFileAppendChanged(logFileAppend);
        }
    }
}
