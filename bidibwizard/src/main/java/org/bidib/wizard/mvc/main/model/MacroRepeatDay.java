package org.bidib.wizard.mvc.main.model;

import java.util.Calendar;

import org.bidib.wizard.common.locale.Resources;

public enum MacroRepeatDay {
    // @formatter:off
    ALL(Resources.getString(MacroRepeatDay.class, "everyDay")), SUNDAY(getWeekdayName(Calendar.SUNDAY)), MONDAY(
        getWeekdayName(Calendar.MONDAY)), TUESDAY(getWeekdayName(Calendar.TUESDAY)), WEDNESDAY(
        getWeekdayName(Calendar.WEDNESDAY)), THURSDAY(getWeekdayName(Calendar.THURSDAY)), FRIDAY(
        getWeekdayName(Calendar.FRIDAY)), SATURDAY(getWeekdayName(Calendar.SATURDAY));
    // @formatter:on

    private final String label;

    MacroRepeatDay(String label) {
        this.label = label;
    }

    private static String getWeekdayName(int weekday) {
        return new java.text.DateFormatSymbols().getWeekdays()[weekday];
    }

    @Override
    public String toString() {
        return label;
    }
}
