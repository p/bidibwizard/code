package org.bidib.wizard.mvc.stepcontrol.controller;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections4.CollectionUtils;
import org.bidib.jbidibc.core.AccessoryState;
import org.bidib.jbidibc.core.AccessoryStateOptions;
import org.bidib.jbidibc.core.AccessoryStateOptions.Options;
import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.accessory.ByteOptionsValue;
import org.bidib.jbidibc.core.accessory.OptionsValue;
import org.bidib.jbidibc.core.enumeration.AccessoryStateOptionsKeys;
import org.bidib.jbidibc.core.utils.AccessoryStateUtils.ErrorAccessoryState.AccessoryExecutionState;
import org.bidib.jbidibc.core.utils.ProductUtils;
import org.bidib.wizard.comm.CommunicationFactory;
import org.bidib.wizard.comm.InputPortStatus;
import org.bidib.wizard.comm.SoundPortStatus;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.bidib.wizard.mvc.main.controller.FeedbackPortStatusChangeProvider;
import org.bidib.wizard.mvc.main.model.Accessory;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.MotorPort;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.model.Port;
import org.bidib.wizard.mvc.main.model.PortsProvider;
import org.bidib.wizard.mvc.main.model.SoundPort;
import org.bidib.wizard.mvc.main.model.listener.AccessoryListListener;
import org.bidib.wizard.mvc.main.model.listener.AccessoryListener;
import org.bidib.wizard.mvc.main.model.listener.CvDefinitionListener;
import org.bidib.wizard.mvc.main.model.listener.DefaultNodeListListener;
import org.bidib.wizard.mvc.main.model.listener.PortListListener;
import org.bidib.wizard.mvc.main.model.listener.PortListener;
import org.bidib.wizard.mvc.main.view.panel.listener.TabStatusListener;
import org.bidib.wizard.mvc.stepcontrol.model.StepControlAspect;
import org.bidib.wizard.mvc.stepcontrol.model.StepControlModel;
import org.bidib.wizard.mvc.stepcontrol.view.StepControlPanel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.common.collect.ArrayListModel;

public class StepControlController implements CvDefinitionListener, StepControlControllerInterface {
    private static final Logger LOGGER = LoggerFactory.getLogger(StepControlController.class);

    private StepControlPanel stepControlPanel;

    private final StepControlModel stepControlModel;

    private final AccessoryListener accessoryListener;

    private Accessory controlledAccessory;

    private Accessory operationalAccessory;

    private Node controlledNode;

    private final ScheduledExecutorService accessoryStateWorkers = Executors.newScheduledThreadPool(1);

    public StepControlController() {

        stepControlModel = new StepControlModel();

        accessoryListener = new AccessoryListener() {

            @Override
            public void macrosChanged() {
                // TODO Auto-generated method stub

            }

            @Override
            public void labelChanged(String label) {
                // TODO Auto-generated method stub

            }

            @Override
            public void accessoryStateChanged(int accessoryId, int aspect) {

                LOGGER.info("The accessory state has changed, accessory: {}, aspect: {}", accessoryId, aspect);

                // emergency stop must be signaled specially
                if (accessoryId == 0) {
                    boolean emergencyStop = false;
                    if (aspect == BidibLibrary.BIDIB_ACCESSORY_ASPECT_ESTOP) {
                        LOGGER.warn("Emergency stop was signalled!");

                        emergencyStop = true;
                        if (stepControlModel != null) {
                            stepControlModel.setOperationalMode(false);
                        }

                        // shutdown all tasks
                        cancelScheduleAccessoryStateCheck();
                    }
                    else if (aspect == BidibLibrary.BIDIB_ACCESSORY_ASPECT_UNKNOWN) {
                        LOGGER.warn("Illegal aspect was signalled!");
                    }
                    // else {
                    // if (stepControlModel != null && !stepControlModel.getOperationalMode()
                    // && !executionState == AccessoryExecutionState.ERROR) {
                    // LOGGER.warn("Emergency stop was reset!");
                    // stepControlModel.setOperationalMode(true);
                    // }
                    // }
                    // TODO other aspects must trigger the animation

                    if (controlledAccessory != null) {
                        AccessoryState state = controlledAccessory.getAccessoryState();
                        AccessoryStateOptions options = controlledAccessory.getAccessoryStateOptions();
                        AccessoryExecutionState executionState = controlledAccessory.getAccessoryExecutionState();

                        LOGGER.info("Current accessory state: {},  options: {}, executionState: {}", state, options,
                            executionState);

                        if (stepControlModel != null && !stepControlModel.getOperationalMode() && !emergencyStop
                        /*
                         * && (executionState != AccessoryExecutionState.ERROR || executionState !=
                         * AccessoryExecutionState.UNKNOWN)
                         */) {
                            LOGGER.warn("Emergency stop was reset!");
                            stepControlModel.setOperationalMode(true);
                        }

                        // notify the execution state to the panel
                        stepControlPanel.executionStateChanged(executionState, accessoryId, aspect, state);

                        if (options != null) {
                            Options stateOptions = options.getOptions();

                            if (stateOptions != null) {
                                try {

                                    Integer currentAngle = null;

                                    OptionsValue<?> optionsValue =
                                        stateOptions
                                            .getOptionsValue(AccessoryStateOptionsKeys.STATE_OPTION_CURRENT_ANGLE);
                                    if (optionsValue != null) {
                                        currentAngle = ByteOptionsValue.getIntValue(optionsValue);

                                        double currentDegrees = 1.5d * currentAngle;
                                        LOGGER.info("Current angle: {}, degrees: {}", currentAngle, currentDegrees);

                                        if (stepControlModel != null) {
                                            stepControlModel.setTurntableCurrentDegrees(currentDegrees);
                                        }

                                        stepControlPanel.setTurntableDegrees(currentDegrees);
                                    }

                                    optionsValue =
                                        stateOptions
                                            .getOptionsValue(AccessoryStateOptionsKeys.STATE_OPTION_TARGET_ANGLE);
                                    if (optionsValue != null) {
                                        int targetAngle = ByteOptionsValue.getIntValue(optionsValue);

                                        double targetDegrees = 1.5d * targetAngle;
                                        LOGGER.info(
                                            "Target angle: {}, degrees: {}, currentAngle: {}, emergencyStop: {}",
                                            targetAngle, targetDegrees, currentAngle, emergencyStop);

                                        if (stepControlModel != null) {
                                            stepControlModel.setTurntableTargetDegrees(targetDegrees);
                                        }

                                        if (currentAngle != null && !emergencyStop
                                            && executionState == AccessoryExecutionState.RUNNING) {
                                            // check if the current angle and the target angle are different
                                            if (targetAngle != currentAngle) {
                                                LOGGER.info("targetAngle != currentAngle, start the scheduler.");
                                                scheduleAccessoryStateCheck();
                                                // TODO change the duration
                                                // animator.restart();
                                            }
                                            else if (executionState == AccessoryExecutionState.RUNNING) {
                                                LOGGER.info("executionState is running, start the scheduler.");
                                                scheduleAccessoryStateCheck();

                                            }
                                        }
                                        else {
                                            LOGGER.info(
                                                "The execution state is no longer running! Current executionState: {}",
                                                executionState);

                                            cancelScheduleAccessoryStateCheck();
                                        }
                                    }
                                }
                                catch (Exception ex) {
                                    LOGGER.warn("Get the current turntable angle failed.", ex);
                                }
                            }
                        }
                    }

                }
                else if (accessoryId == 1) {
                    // operational accessory
                    LOGGER.info("Received accessory id 1: operational accessory, aspect: {}", aspect);
                    if (aspect == 0) {
                        if (stepControlModel != null) {
                            LOGGER.info("Set the step control operational mode.");
                            stepControlModel.setOperationalMode(true);
                        }
                    }
                }
                else {
                    LOGGER.info("Received accessory id > 0");
                }
            }
        };
    }

    public void start(final MainModel mainModel, final TabStatusListener tabStatusListener) {

        FeedbackPortStatusChangeProvider changeProvider =
            DefaultApplicationContext.getInstance().get(DefaultApplicationContext.KEY_FEEDBACKPORTSTATUSCHANGEPROVIDER,
                FeedbackPortStatusChangeProvider.class);

        LOGGER.info("Use changeProvider: {}", changeProvider);

        stepControlPanel = new StepControlPanel(mainModel, stepControlModel, tabStatusListener, changeProvider, this);
        stepControlPanel.createComponent();

        // add the cv definition listener
        mainModel.addCvDefinitionListener(this);

        final PortsProvider portsProvider = mainModel;

        // add listener for accessories
        mainModel.addAccessoryListListener(new AccessoryListListener() {

            @Override
            public void pendingChangesChanged() {

            }

            @Override
            public void listChanged() {
                LOGGER.info("The accessory list has changed.");

                Node selectedNode = mainModel.getSelectedNode();
                // TODO check if the node has really changed

                if (controlledAccessory != null) {
                    // free the listener
                    controlledAccessory.removeAccessoryListener(accessoryListener);
                    controlledAccessory = null;
                }

                if (operationalAccessory != null) {
                    // free the listener
                    operationalAccessory.removeAccessoryListener(accessoryListener);
                    operationalAccessory = null;
                }

                // check if the node is a StepControl and register listener for accessory 0
                if (selectedNode != null && ProductUtils.isStepControl(selectedNode.getUniqueId())) {
                    LOGGER.info("The currently selected node is a StepControl.");

                    // ensure console is visible
                    CvConsoleController.ensureConsoleVisible();

                    if (CollectionUtils.isNotEmpty(mainModel.getAccessories())) {
                        // the first accessory is the worker accessory
                        Accessory accessory = mainModel.getAccessories().get(0);

                        LOGGER.info("Add listener to controlled accessory: {}", accessory);

                        controlledAccessory = accessory;
                        controlledAccessory.addAccessoryListener(accessoryListener);

                        operationalAccessory = mainModel.getAccessories().get(1);
                        operationalAccessory.addAccessoryListener(accessoryListener);

                        LOGGER.info("Query the accessory state of the operational and execution accessory.");

                        // read the current state of accessory 0 and 1
                        CommunicationFactory.getInstance().queryAccessoryState(selectedNode.getNode(),
                            operationalAccessory, controlledAccessory);
                    }

                }
            }

            @Override
            public void accessoryChanged(int accessoryId) {

            }
        });

        final PortListListener motorPortListListener = new PortListListener() {

            @Override
            public void listChanged() {

                if (controlledNode != null) {
                    List<MotorPort> motorPorts = new LinkedList<>();
                    motorPorts.addAll(controlledNode.getMotorPorts());

                    if (CollectionUtils.isNotEmpty(motorPorts)) {
                        // get the first motor port
                        MotorPort motorPort = motorPorts.get(0);
                        LOGGER.info("Fetched first motor port: {}", motorPort);
                        stepControlModel.setMotorPort(motorPort);
                    }
                    else {
                        stepControlModel.setMotorPort(null);
                    }
                }
                else {
                    stepControlModel.setMotorPort(null);
                }
            }
        };

        final PortListListener soundPortListListener = new PortListListener() {

            @Override
            public void listChanged() {
                LOGGER.info("The list of sound port has changed.");
                List<SoundPort> soundPorts = new LinkedList<>();
                if (controlledNode != null) {
                    // soundPorts.addAll(controlledNode.getSoundPorts());
                    soundPorts.addAll(portsProvider.getSoundPorts());
                    LOGGER.info("Fetched sound ports: {}", soundPorts);
                }
                stepControlModel.setSoundPorts(soundPorts);
            }
        };

        mainModel.addNodeListListener(new DefaultNodeListListener() {

            @Override
            public void nodeChanged() {

                Node selectedNode = mainModel.getSelectedNode();

                // TODO check if the node has really changed
                LOGGER.info("The selected node has changed, current controlledAccessory: {}", controlledAccessory);
                if (controlledAccessory != null) {
                    // free the listener
                    controlledAccessory.removeAccessoryListener(accessoryListener);

                    controlledAccessory = null;
                }

                if (controlledNode != null) {

                    LOGGER.info("Remove the StepControlController as portListListener for MotorPorts.");
                    controlledNode.removePortListListener(MotorPort.class, motorPortListListener);
                    stepControlModel.setMotorPort(null);

                    // LOGGER.info("Remove the StepControlController as portListListener for InputPorts.");
                    // controlledNode.removePortListListener(InputPort.class, StepControlController.this);

                    LOGGER.info("Remove the StepControlController as portListListener for SoundPorts.");
                    controlledNode.removePortListListener(SoundPort.class, soundPortListListener);

                    stepControlModel.setSoundPorts(null);

                    mainModel.removeSoundPortListListener(soundPortListListener);
                }

                // release the controlled node
                controlledNode = null;

                // check if the node is a StepControl and register listener for accessory 0
                if (selectedNode != null && ProductUtils.isStepControl(selectedNode.getUniqueId())) {
                    LOGGER.info("The currently selected node is a StepControl.");

                    controlledNode = selectedNode;

                    LOGGER.info("Add the StepControlController as portListListener for MotorPorts.");
                    controlledNode.addPortListListener(MotorPort.class, motorPortListListener);

                    // LOGGER.info("Add the StepControlController as portListListener for InputPorts.");
                    // controlledNode.addPortListListener(InputPort.class, StepControlController.this);

                    LOGGER.info("Add the StepControlController as portListListener for SoundPorts.");
                    controlledNode.addPortListListener(SoundPort.class, soundPortListListener);

                    mainModel.addSoundPortListListener(soundPortListListener);
                }

            }
        });

        mainModel.addInputPortListener(new PortListener<InputPortStatus>() {
            @Override
            public void labelChanged(Port<InputPortStatus> port, String label) {
            }

            @Override
            public void statusChanged(Port<InputPortStatus> port, InputPortStatus status) {
                LOGGER.info("The port status has changed: {}, port: {}", status, port);
                if (port.getId() == 2) {
                    // this is the homing input
                }
            }

            @Override
            public void configChanged(Port<InputPortStatus> port) {
            }
        });

    }

    public StepControlPanel getComponent() {
        return stepControlPanel;
    }

    @Override
    public void triggerLoadCvValues() {
        LOGGER.info("Load the CV for the StepControl.");

        if (stepControlPanel != null) {
            LOGGER.info("Let the stepControlPanel initiate loading the CV values: {}", stepControlPanel);

            stepControlPanel.triggerLoadCvValues();
        }
    }

    @Override
    public void cvDefinitionChanged() {
        LOGGER.info("The CV definition has changed.");

        if (stepControlPanel != null) {
            LOGGER.info("Notify the stepControl panel: {}", stepControlPanel);

            stepControlPanel.cvDefinitionChanged();
        }
    }

    @Override
    public void cvDefinitionValuesChanged(final boolean read) {
        LOGGER.info("The CV definition values have changed, read: {}", read);

        if (stepControlPanel != null) {
            LOGGER.info("Notify the stepControl panel of the changed CV defintion values: {}", stepControlPanel);

            stepControlPanel.cvDefinitionValuesChanged(read);
        }
    }

    @Override
    public ArrayListModel<StepControlAspect> getConfigureAspectsListModel() {
        return stepControlModel.getStepControlAspectsListModel();
    }

    private ScheduledFuture<?> taskFuture;

    public void scheduleAccessoryStateCheck() {

        // check if the push interval value is active
        Integer pushInterval = stepControlModel.getPushInterval();
        if (pushInterval != null && pushInterval > 0) {

            LOGGER.info("The updates of the accessory state are delivered via push notification.");
            return;
        }

        // add a task to the worker to check the accessory state
        taskFuture = accessoryStateWorkers.schedule(new Runnable() {

            @Override
            public void run() {
                try {
                    LOGGER.info("Check the state of the controlledAccessory: {}", controlledAccessory);

                    // release the task future
                    taskFuture = null;

                    if (controlledNode != null) {

                        // read the current state of accessory 0
                        CommunicationFactory.getInstance().queryAccessoryState(controlledNode.getNode(),
                            controlledAccessory);
                    }
                }
                catch (Exception ex) {
                    LOGGER.warn("Check the state of the controlledAccessory failed for bidibNode: {}", controlledNode,
                        ex);
                }
            }
        }, 250, TimeUnit.MILLISECONDS);
    }

    private void cancelScheduleAccessoryStateCheck() {
        LOGGER.info("Cancel the accessory state check.");
        // shutdown all tasks
        try {
            if (taskFuture != null) {
                taskFuture.cancel(true);

                taskFuture = null;
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Stop scheduled task failed.", ex);
        }

    }

    @Override
    public void setMotorPortValue(final MotorPort motorPort) {
        LOGGER.info("Set the motor port value: {}", motorPort);
        CommunicationFactory.getInstance().activateMotorPort(controlledNode.getNode(), motorPort.getId(),
            motorPort.getValue());
    }

    @Override
    public void triggerSoundPort(int portId, final SoundPortStatus soundPortStatus) {
        LOGGER.info("Activate the sound port value: {}, soundPortStatus: {}", portId, soundPortStatus);

        CommunicationFactory.getInstance().activateSoundPort(controlledNode.getNode(), portId, soundPortStatus);
    }
}
