package org.bidib.wizard.mvc.main.view.table;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseEvent;

import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

import org.apache.commons.lang3.StringUtils;
import org.bidib.wizard.comm.BidibStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jidesoft.grid.RowStripeTableStyleProvider;
import com.jidesoft.grid.SortableTable;
import com.jidesoft.grid.TableColumnChooser;
import com.jidesoft.grid.TableStyleProvider;

public abstract class AbstractEmptyTable extends SortableTable {
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractEmptyTable.class);

    private static final long serialVersionUID = 1L;

    private final String emptyTableText;

    protected MouseEvent popupEvent;

    private boolean skipPackColumn;

    public AbstractEmptyTable(TableModel tableModel, String emptyTableText) {
        super(tableModel);
        this.emptyTableText = emptyTableText;

        setRowHeight(getRowHeight() + 4);

        // RowStripeTableStyleProvider tableStyleProvider =
        // new RowStripeTableStyleProvider(Color.WHITE, new Color(209, 224, 239));
        // setTableStyleProvider(tableStyleProvider);
    }

    @Override
    public TableStyleProvider getTableStyleProvider() {
        return new RowStripeTableStyleProvider(Color.WHITE, new Color(209, 224, 239));
    }

    protected String getEmptyTableText() {
        return emptyTableText;
    }

    /**
     * @return the skipPackColumn
     */
    public boolean isSkipPackColumn() {
        return skipPackColumn;
    }

    /**
     * @param skipPackColumn
     *            the skipPackColumn to set
     */
    public void setSkipPackColumn(boolean skipPackColumn) {
        this.skipPackColumn = skipPackColumn;
    }

    public BidibStatus[] getActions(BidibStatus status) {
        if (status != null) {
            BidibStatus[] actions = status.getValues();
            return actions;
        }
        else {
            LOGGER.debug("No BidibStatus available.");
        }
        return new BidibStatus[0];
    }

    protected int getRow(Point point) {
        return rowAtPoint(point);
    }

    protected int getColumn(Point point) {
        return columnAtPoint(point);
    }

    public Dimension getPreferredScrollableViewportSize() {
        return getPreferredSize();
    }

    public boolean getScrollableTracksViewportHeight() {
        return getPreferredSize().height < getParent().getHeight();
    }

    public void packColumn(int vColIndex, int margin) {

        if (isSkipPackColumn()) {
            // TODO packColumn is commented out
            // super.packColumn(vColIndex, margin);
            return;
        }

        DefaultTableColumnModel colModel = (DefaultTableColumnModel) getColumnModel();
        TableColumn col = colModel.getColumn(vColIndex);
        TableCellRenderer renderer = col.getHeaderRenderer();
        int width = 0;

        if (renderer == null && getTableHeader() != null) {
            renderer = getTableHeader().getDefaultRenderer();
        }
        if (renderer != null) {
            Component comp = renderer.getTableCellRendererComponent(this, col.getHeaderValue(), false, false, 0, 0);

            width = comp.getPreferredSize().width;
        }

        for (int r = 0; r < getRowCount(); r++) {
            renderer = getCellRenderer(r, vColIndex);
            Component comp =
                renderer.getTableCellRendererComponent(this, getValueAt(r, vColIndex), false, false, r, vColIndex);

            width = Math.max(width, comp.getPreferredSize().width);
        }
        width += 2 * margin;
        col.setMaxWidth(width);
        col.setPreferredWidth(width);
    }

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        if (getRowCount() == 0) {
            if (StringUtils.isNotBlank(getErrorText())) {
                Graphics2D g2d = (Graphics2D) g;

                g2d.setColor(Color.BLACK);
                g2d.drawString(getErrorText(), 10, 20);
            }
            else if (StringUtils.isNotBlank(getEmptyTableText())) {
                Graphics2D g2d = (Graphics2D) g;

                g2d.setColor(Color.BLACK);
                g2d.drawString(getEmptyTableText(), 10, 20);
            }
        }
    }

    protected String getErrorText() {
        return null;
    }

    /**
     * Set the column visible or hidden. If the column is set visible the columnIndex is incremented and returned.
     * 
     * @param modelIndex
     *            the model index
     * @param viewIndex
     *            the view index
     * @param visible
     *            column visible flag
     * @return the next column number
     */
    public int setColumnVisible(int modelIndex, int viewIndex, boolean visible) {

        if (visible) {
            if (!TableColumnChooser.isVisibleColumn(this, modelIndex)) {
                TableColumnChooser.showColumn(this, modelIndex, viewIndex);
            }
            viewIndex++;
        }
        else {
            if (TableColumnChooser.isVisibleColumn(this, modelIndex)) {
                TableColumnChooser.hideColumn(this, modelIndex);
            }
        }

        return viewIndex;
    }

}
