package org.bidib.wizard.mvc.main.model;

import org.bidib.jbidibc.core.node.listener.TransferListener;

public class DefaultTransferListener implements TransferListener {
    private MainModel model;

    public DefaultTransferListener(MainModel model) {
        this.model = model;
    }

    @Override
    public void receiveStarted() {
        model.getStatusModel().setRx(true);
    }

    @Override
    public void receiveStopped() {
        model.getStatusModel().setRx(false);
    }

    @Override
    public void sendStarted() {
        model.getStatusModel().setTx(true);
    }

    @Override
    public void sendStopped() {
        model.getStatusModel().setTx(false);
    }

    @Override
    public void ctsChanged(boolean cts) {
        model.getStatusModel().setCts(cts);
    }
}
