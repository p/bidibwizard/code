package org.bidib.wizard.mvc.common.view.slider;

import javax.swing.event.ChangeEvent;

public interface SliderValueChangeListener {
    /**
     * The slider state has changed.
     * 
     * @param e
     *            the change event
     * @param isAdjusting
     *            the slider is adjusting
     * @param value
     *            the transient value
     */
    void stateChanged(ChangeEvent e, boolean isAdjusting, int value);
}
