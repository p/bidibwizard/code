package org.bidib.wizard.mvc.dmx.view.panel.axis;

import java.text.NumberFormat;

import org.jfree.chart.axis.NumberTickUnit;

public class LabeledNumberTickUnit extends NumberTickUnit {
    private static final long serialVersionUID = 1L;

    private final String axisLabel;

    private final int divisor;

    public LabeledNumberTickUnit(double size, NumberFormat formatter, int minorTickCount, String axisLabel, int divisor) {
        super(size, formatter, minorTickCount);

        this.axisLabel = axisLabel;
        this.divisor = divisor;
    }

    /**
     * @return the axisLabel
     */
    public String getAxisLabel() {
        return axisLabel;
    }

    /**
     * @return the divisor
     */
    public int getDivisor() {
        return divisor;
    }

    @Override
    public String valueToString(double value) {
        return super.valueToString(value / divisor);
    }
}
