package org.bidib.wizard.mvc.logger.view;

import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.JPanel;

import org.bidib.wizard.common.locale.Resources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vlsolutions.swing.docking.DockKey;
import com.vlsolutions.swing.docking.Dockable;

public class LogPanelView implements Dockable {
    private static final Logger LOGGER = LoggerFactory.getLogger(LogPanelView.class);

    private final DockKey DOCKKEY = new DockKey("LogPanelView");

    private final JPanel contentPanel;

    private final LogsPane logsPane = new LogsPane();

    public LogPanelView() {
        DOCKKEY.setName(Resources.getString(getClass(), "title"));
        DOCKKEY.setFloatEnabled(true);
        DOCKKEY.setAutoHideEnabled(false);

        LOGGER.info("Create new LogPanelView");
        contentPanel = new JPanel();
        contentPanel.setLayout(new BorderLayout());

        contentPanel.add(logsPane.get());
    }

    @Override
    public Component getComponent() {
        return contentPanel;
    }

    @Override
    public DockKey getDockKey() {
        return DOCKKEY;
    }

    public void close() {
        if (logsPane != null) {
            logsPane.close();
        }
    }
}
