package org.bidib.wizard.mvc.stepcontrol.model;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.stepcontrol.view.wizard.SummaryPanel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.beans.Model;

public class ConfigurationWizardModel extends Model {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigurationWizardModel.class);

    public enum WizardStatus {
        started, finished, aborted;
    }

    public static final String PROPERTYNAME_TURNTABLE_TYPE = "turnTableType";

    public static final String PROPERTYNAME_MOTORSIZE_TYPE = "motorSizeType";

    public static final String PROPERTYNAME_STEPCOUNT = "stepCount";

    public static final String PROPERTYNAME_GEARING = "gearing";

    public static final String PROPERTYNAME_MICROSTEPPING = "microStepping";

    public static final String PROPERTYNAME_TOTALSTEPCOUNT = "totalStepCount";

    public static final String PROPERTYNAME_WIZARDSTATUS = "wizardStatus";

    public static final String PROPERTYNAME_HTMLCONTENT = "htmlContent";

    private TurnTableType turnTableType;

    private MotorSizeType motorSizeType;

    private Integer stepCount;

    private Gearing gearing;

    private Integer microStepping = 64;

    private WizardStatus wizardStatus = WizardStatus.started;

    /**
     * @return the turnTableType
     */
    public TurnTableType getTurnTableType() {
        return turnTableType;
    }

    /**
     * @param turnTableType
     *            the turnTableType to set
     */
    public void setTurnTableType(TurnTableType turnTableType) {
        LOGGER.info("Set the turnTableType: {}", turnTableType);

        TurnTableType oldValue = this.turnTableType;
        this.turnTableType = turnTableType;

        firePropertyChange(PROPERTYNAME_TURNTABLE_TYPE, oldValue, turnTableType);
    }

    /**
     * @return the motorSizeType
     */
    public MotorSizeType getMotorSizeType() {
        return motorSizeType;
    }

    /**
     * @param motorSizeType
     *            the motorSizeType to set
     */
    public void setMotorSizeType(MotorSizeType motorSizeType) {
        LOGGER.info("Set the motorSizeType: {}", motorSizeType);
        MotorSizeType oldValue = this.motorSizeType;

        this.motorSizeType = motorSizeType;

        firePropertyChange(PROPERTYNAME_MOTORSIZE_TYPE, oldValue, motorSizeType);
    }

    /**
     * @return the stepCount
     */
    public Integer getStepCount() {
        return stepCount;
    }

    /**
     * @param stepCount
     *            the stepCount to set
     */
    public void setStepCount(Integer stepCount) {
        Integer oldValue = this.stepCount;
        Integer oldTotalStepCount = getTotalStepCount();

        this.stepCount = stepCount;

        firePropertyChange(PROPERTYNAME_STEPCOUNT, oldValue, stepCount);
        firePropertyChange(PROPERTYNAME_TOTALSTEPCOUNT, oldTotalStepCount, getTotalStepCount());
    }

    /**
     * @return the gearing
     */
    public Gearing getGearing() {
        if (gearing == null) {
            gearing = new Gearing("no");
        }
        return gearing;
    }

    /**
     * @param gearing
     *            the gearing to set
     */
    public void setGearing(Gearing gearing) {
        Gearing oldValue = this.gearing;
        Integer oldTotalStepCount = getTotalStepCount();

        this.gearing = gearing;

        firePropertyChange(PROPERTYNAME_GEARING, oldValue, gearing);
        firePropertyChange(PROPERTYNAME_TOTALSTEPCOUNT, oldTotalStepCount, getTotalStepCount());
    }

    /**
     * @return the microStepping
     */
    public Integer getMicroStepping() {
        return microStepping;
    }

    /**
     * @param microStepping
     *            the microStepping to set
     */
    public void setMicroStepping(Integer microStepping) {
        LOGGER.info("Set the new microstepping value: {}", microStepping);
        Integer oldValue = this.microStepping;
        Integer oldTotalStepCount = getTotalStepCount();

        this.microStepping = microStepping;

        firePropertyChange(PROPERTYNAME_MICROSTEPPING, oldValue, microStepping);
        firePropertyChange(PROPERTYNAME_TOTALSTEPCOUNT, oldTotalStepCount, getTotalStepCount());
    }

    /**
     * @return the total step count
     */
    public Integer getTotalStepCount() {
        try {
            if (gearing != null && Gearing.YES.equals(gearing.getKey())) {
                // the calculation of total steps with gearRatio primary and secondary
                int totalStepCount =
                    stepCount * microStepping * gearing.getGearRatioSecondary() / gearing.getGearRatioPrimary();
                return totalStepCount;
            }
            return stepCount * microStepping;
        }
        catch (Exception ex) {
            LOGGER.warn("Calculate total step count failed.");
        }
        return null;
    }

    public void triggerUpdateTotalSteps() {
        firePropertyChange(PROPERTYNAME_TOTALSTEPCOUNT, null, getTotalStepCount());
    }

    /**
     * @return the wizardStatus
     */
    public WizardStatus getWizardStatus() {
        return wizardStatus;
    }

    /**
     * @param wizardStatus
     *            the wizardStatus to set
     */
    public void setWizardStatus(WizardStatus wizardStatus) {
        WizardStatus oldValue = this.wizardStatus;
        this.wizardStatus = wizardStatus;

        firePropertyChange(PROPERTYNAME_WIZARDSTATUS, oldValue, wizardStatus);
    }

    public String getHtmlContent() {
        StringBuilder sb = new StringBuilder("<html>");
        sb
            .append(Resources.getString(SummaryPanel.class, "turntableType",
                Resources.getString(TurnTableType.class, turnTableType.name())))
            .append("<br/>");
        sb
            .append(Resources.getString(SummaryPanel.class, "motorSize",
                Resources.getString(MotorSizeType.class, motorSizeType.name())))
            .append("<br/>");
        sb
            .append("&nbsp;&nbsp;").append(Resources.getString(SummaryPanel.class, "current",
                motorSizeType.getCurrentMoving(), motorSizeType.getCurrentStopped()))
            .append("<br/>");
        // gearing
        if (gearing != null && !Gearing.NO.equals(gearing.getKey())) {
            sb
                .append(Resources.getString(SummaryPanel.class, "gearing", gearing.getGearRatioPrimary(),
                    gearing.getGearRatioSecondary(), gearing.getBackLash()))
                .append("<br/>");
        }
        else {
            sb.append(Resources.getString(SummaryPanel.class, "gearingNone")).append("<br/>");
        }
        sb.append(Resources.getString(SummaryPanel.class, "stepCount", stepCount)).append("<br/>");
        sb.append(Resources.getString(SummaryPanel.class, "microStepping", microStepping)).append("<br/>");
        sb.append(Resources.getString(SummaryPanel.class, "totalStepCount", getTotalStepCount())).append("<br/>");
        sb.append("</html>");
        return sb.toString();
    }

    public void triggerUpdateHtmlContent() {
        firePropertyChange(PROPERTYNAME_HTMLCONTENT, null, getHtmlContent());
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
