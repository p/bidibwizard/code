package org.bidib.wizard.mvc.features.view.panel;

public interface RangeValidationCallback {

    /**
     * @return the maxValue
     */
    int getMaxValue();

    /**
     * @return the minValue
     */
    int getMinValue();
}
