package org.bidib.wizard.mvc.firmware.view.panel.listener;

public interface FirmwareProcessListener {
    /**
     * Start the firmware update process.
     */
    void updateFirmware();
}
