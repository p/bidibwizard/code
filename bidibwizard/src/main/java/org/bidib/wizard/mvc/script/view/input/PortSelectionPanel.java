package org.bidib.wizard.mvc.script.view.input;

import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.UIManager;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

import org.apache.commons.lang3.StringUtils;
import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.common.context.ApplicationContext;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.common.view.renderer.PortRenderer;
import org.bidib.wizard.mvc.main.model.NullPort;
import org.bidib.wizard.mvc.main.model.Port;
import org.bidib.wizard.mvc.main.model.PortsProvider;
import org.bidib.wizard.mvc.script.view.ScriptParser;
import org.bidib.wizard.utils.PortListUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.adapter.ComboBoxAdapter;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.view.ValidationComponentUtils;

public class PortSelectionPanel extends AbstractInputSelectionPanel {
    private static final Logger LOGGER = LoggerFactory.getLogger(PortSelectionPanel.class);

    private ValueModel selectionHolderPort;

    private ItemSelectionModel<Port<?>> portSelectionModel;

    private SelectionInList<? extends Port<?>> portSelection;

    @Override
    public void initialize(
        final ApplicationContext context, final DefaultFormBuilder dialogBuilder, ValidationSupport validationSupport,
        String variableName, String variableType, String caption, String defaultValue, String prevValue) {
        this.variableName = variableName;
        setDefaultValue(defaultValue);

        portSelectionModel = new ItemSelectionModel<>();
        portSelectionModel.addPropertyChangeListener(ItemSelectionModel.PROPERTY_SELECTED_ITEM,
            new PropertyChangeListener() {

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    LOGGER.debug("Selected item has changed: {}", portSelectionModel.getSelectedItem());
                    validationSupport.triggerValidation();
                }
            });

        PortsProvider portsProvider = context.get(ScriptParser.KEY_MAIN_MODEL, PortsProvider.class);

        List<Port<? extends BidibStatus>> ports = new LinkedList<>();
        ports.add(NullPort.NONE);
        switch (variableType) {
            case "backlight":
                ports.addAll(portsProvider.getBacklightPorts());
                break;
            case "input":
                ports.addAll(portsProvider.getInputPorts());
                break;
            case "light":
                ports.addAll(portsProvider.getLightPorts());
                break;
            case "motor":
                ports.addAll(portsProvider.getMotorPorts());
                break;
            case "servo":
                ports.addAll(portsProvider.getServoPorts());
                break;
            case "sound":
                ports.addAll(portsProvider.getSoundPorts());
                break;
            case "switch":
                ports.addAll(portsProvider.getSwitchPorts());
                break;
            case "switchpair":
                ports.addAll(portsProvider.getSwitchPairPorts());
                break;
            default:
                // TODO unsupported type
                LOGGER.warn("Unsupported variable type detected: {}, variableName: {}", variableType, variableName);
                throw new IllegalArgumentException(
                    "Unsupported variable type detected: " + variableType + ", variableName: " + variableName);
        }

        portSelection = new SelectionInList<>(ports);

        selectionHolderPort =
            new PropertyAdapter<ItemSelectionModel<Port<?>>>(portSelectionModel,
                ItemSelectionModel.PROPERTY_SELECTED_ITEM, true);

        ComboBoxAdapter<Port<?>> comboBoxAdapterPorts =
            new ComboBoxAdapter<Port<?>>(portSelection, selectionHolderPort);
        final JComboBox<Port<?>> comboPorts = new JComboBox<>();
        comboPorts.setModel(comboBoxAdapterPorts);
        comboPorts.setRenderer(new PortRenderer());

        comboPorts.setPrototypeDisplayValue(ports.get(0));

        // change the editor to show disabled ports in different color
        comboBoxAdapterPorts.addListDataListener(new ListDataListener() {

            @Override
            public void intervalRemoved(ListDataEvent e) {
            }

            @Override
            public void intervalAdded(ListDataEvent e) {
            }

            @Override
            public void contentsChanged(ListDataEvent e) {
                LOGGER.info("Contents have changed.");

                Object item = comboPorts.getSelectedItem();
                if (item != null && item instanceof Port) {
                    Port<?> p = (Port<?>) item;
                    if (p.isInactive() || !p.isEnabled()) {
                        comboPorts.setBackground(Color.red.darker());
                    }
                    else {
                        comboPorts.setBackground(UIManager.getColor("ComboBox.background"));
                    }
                }
                else {
                    comboPorts.setBackground(UIManager.getColor("ComboBox.background"));
                }
            }
        });

        caption = "<html>" + caption + "</html>";

        dialogBuilder.append(caption, comboPorts);

        ValidationComponentUtils.setMandatory(comboPorts, true);
        ValidationComponentUtils.setMessageKeys(comboPorts, "validation." + variableName);

        String initialValue = defaultValue;
        if (StringUtils.isNotBlank(prevValue)) {
            initialValue = prevValue;
        }

        if (StringUtils.isNotBlank(initialValue)) {
            try {
                Integer def = Integer.valueOf(initialValue);
                LOGGER.info("Set the initial value: {}", def);

                Port<?> port = PortListUtils.findPortByPortNumber(portSelection.getList(), def.intValue());

                portSelectionModel.setSelectedItem(port);
            }
            catch (Exception ex) {
                LOGGER.warn("Parse initial value to boolean failed: {}", initialValue, ex);
            }
        }
        else {
            portSelectionModel.setSelectedItem(NullPort.NONE);
        }

        content = dialogBuilder.build();
    }

    public void validate(final ValidationResult validationResult) {

        if (selectionHolderPort.getValue() == null || NullPort.NONE.equals(selectionHolderPort.getValue())) {
            validationResult.addError(Resources.getString((Class<?>) null, "validation.select_port"),
                "validation." + variableName);
        }
    }

    @Override
    public Object getSelectedValue() {

        if (selectionHolderPort.getValue() != null) {
            return Integer.valueOf(((Port<?>) selectionHolderPort.getValue()).getId());
        }
        return null;
    }
}
