package org.bidib.wizard.mvc.pom.view.command;

import org.bidib.jbidibc.core.AddressData;
import org.bidib.jbidibc.core.enumeration.AddressMode;
import org.bidib.jbidibc.core.enumeration.PomOperation;
import org.bidib.wizard.mvc.pom.view.panel.AddressProgBeanModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PomAddressModeCommand extends PomOperationIfElseCommand<AddressProgBeanModel> {
    private static final Logger LOGGER = LoggerFactory.getLogger(PomAddressModeCommand.class);

    private int bitPosition;

    private AddressMode addressMode;

    public PomAddressModeCommand(AddressData decoderAddress, PomOperation pomOperation, int cvNumber, int bitPosition,
        int cvValue) {
        super(decoderAddress, pomOperation, cvNumber, cvValue);

        this.bitPosition = bitPosition;
    }

    public PomAddressModeCommand(AddressData decoderAddress, PomOperation pomOperation, int cvNumber, int cvValue) {
        super(decoderAddress, pomOperation, cvNumber, cvValue);
    }

    /**
     * @param cvValueResult
     *            the cvValueResult to set
     */
    public void setCvValueResult(Integer cvValueResult) {
        LOGGER.info("Address mode, result: {}", cvValueResult);
        if (cvValueResult != null) {
            // set the address mode
            int bitMask = prepareCompareBitMask(bitPosition);
            int cvValue = cvValueResult.intValue();

            int val = (cvValue & bitMask) >> bitPosition;
            LOGGER.info("Set address mode from value: {}", val);

            setAddressMode(AddressMode.valueOf(val));

            // replace the cvValueResult
            cvValueResult = val;
        }

        super.setCvValueResult(cvValueResult);
    }

    public void setAddressMode(AddressMode addressMode) {
        LOGGER.info("Set the addressMode: {}", addressMode);
        this.addressMode = addressMode;
    }

    public AddressMode getAddressMode() {
        return addressMode;
    }

    @Override
    public void postExecute(final AddressProgBeanModel addressProgBeanModel) {
        super.postExecute(addressProgBeanModel);

        // update the address mode
        if (addressMode != null) {
            LOGGER.debug("Set the addressMode: {}", addressMode);
            addressProgBeanModel.setAddressMode(addressMode);
        }
    }

    protected static int prepareCompareBitMask(int bitNumber) {
        int compareValue = (1 << bitNumber);
        return compareValue;
    }

    @Override
    public boolean isExpectedResult() {
        if (PomOperation.WR_BIT.equals(getPomOperation()) || PomOperation.WR_BYTE.equals(getPomOperation())) {
            LOGGER.info("Don't compare expected for write operations.");
            return true;
        }

        try {
            LOGGER.info("isExpectedResult, compare values, expected: {}, received: {}", getCvValue(),
                getCvValueResult());
            return (getCvValue() == getCvValueResult());
        }
        catch (Exception ex) {
            LOGGER.warn("Compare expected and result cv value failed.", ex);
        }
        return false;
    }
}
