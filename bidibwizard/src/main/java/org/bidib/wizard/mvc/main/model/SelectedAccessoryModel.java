package org.bidib.wizard.mvc.main.model;

import com.jgoodies.binding.beans.Model;

public class SelectedAccessoryModel extends Model {
    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_SELECTED_ACCESSORY = "selectedAccessory";

    private Accessory selectedAccessory;

    public void setSelectedAccessory(Accessory selectedAccessory) {
        Accessory oldValue = this.selectedAccessory;

        this.selectedAccessory = selectedAccessory;

        firePropertyChange(PROPERTY_SELECTED_ACCESSORY, oldValue, selectedAccessory);
    }

    public Accessory getSelectedAccessory() {
        return selectedAccessory;
    }

    public void triggerLabelChanged() {
        firePropertyChange(PROPERTY_SELECTED_ACCESSORY, null, selectedAccessory);
    }

}
