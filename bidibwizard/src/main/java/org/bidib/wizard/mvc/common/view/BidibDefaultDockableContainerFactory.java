package org.bidib.wizard.mvc.common.view;

import java.awt.Window;

import com.vlsolutions.swing.docking.DefaultDockableContainerFactory;
import com.vlsolutions.swing.docking.FloatingDialog;
import com.vlsolutions.swing.docking.FloatingDockableContainer;

public class BidibDefaultDockableContainerFactory extends DefaultDockableContainerFactory {

    @Override
    public FloatingDockableContainer createFloatingDockableContainer(Window owner) {

        // we want a button in the taskbar for every floating window
        return new FloatingDialog() {
            private static final long serialVersionUID = 1L;

            @Override
            public void installDecoration() {
                super.installDecoration();
                // setUndecorated(false);
            }
        };
    }
}
