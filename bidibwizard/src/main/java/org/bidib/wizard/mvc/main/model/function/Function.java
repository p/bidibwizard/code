package org.bidib.wizard.mvc.main.model.function;

import org.bidib.jbidibc.exchange.lcmacro.LcMacroPointType;
import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.common.locale.Resources;

public abstract class Function<T extends BidibStatus> implements Cloneable {
    public static final String KEY_ANALOG = "analog";

    public static final String KEY_BACKLIGHT = "backlight";

    public static final String KEY_FEEDBACK = "feedback";

    public static final String KEY_INPUT = "input";

    public static final String KEY_LIGHT = "light";

    public static final String KEY_MOTOR = "motor";

    public static final String KEY_SERVO = "servo";

    public static final String KEY_SOUND = "sound";

    public static final String KEY_SWITCH = "switch";

    public static final String KEY_SWITCHPAIR = "switchPair";

    public static final String KEY_MACRO = "macro";

    public static final String KEY_ACCESSORY_OKAY = "accessoryOkay";

    public static final String KEY_CRITICAL = "critical";

    public static final String KEY_DELAY = "delay";

    public static final String KEY_FLAG = "flag";

    public static final String KEY_RANDOM_DELAY = "randomDelay";

    public static final String KEY_SERVO_MOVE_QUERY = "moveServoQuery";

    private T action;

    private String key;

    public Function(T action, String key) {
        this.action = action;
        this.key = key;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && toString().equals(obj.toString());
    }

    public T getAction() {
        return action;
    }

    public void setAction(T action) {
        this.action = action;
    }

    public String getKey() {
        return key;
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    public abstract String getDebugString();

    @Override
    public String toString() {
        return Resources.getString(getClass(), "title");
    }

    /**
     * Converts the function to lcMacroPoint.
     * 
     * @return the lcMacroPoint equivalent of the current function
     */
    public abstract LcMacroPointType toLcMacroPoint();

    public static String getDebugString(Function<?> function) {
        if (function != null) {
            return function.getDebugString();
        }
        return null;
    }
}
