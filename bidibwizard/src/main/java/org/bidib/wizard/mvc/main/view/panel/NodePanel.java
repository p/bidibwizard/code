package org.bidib.wizard.mvc.main.view.panel;

import javax.swing.event.ListSelectionListener;

import org.bidib.wizard.comm.AnalogPortStatus;
import org.bidib.wizard.comm.ServoPortStatus;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.listener.BacklightPortListener;
import org.bidib.wizard.mvc.main.model.listener.CvDefinitionRequestListener;
import org.bidib.wizard.mvc.main.model.listener.OutputListener;
import org.bidib.wizard.mvc.main.model.listener.ServoPortListener;
import org.bidib.wizard.mvc.main.view.panel.listener.AccessoryListListener;
import org.bidib.wizard.mvc.main.view.panel.listener.AccessoryTableListener;
import org.bidib.wizard.mvc.main.view.panel.listener.MacroListListener;
import org.bidib.wizard.mvc.main.view.panel.listener.MacroTableListener;
import org.bidib.wizard.mvc.main.view.panel.listener.NodeListActionListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vlsolutions.swing.docking.DockingConstants;
import com.vlsolutions.swing.docking.DockingDesktop;

public class NodePanel {
    private static final Logger LOGGER = LoggerFactory.getLogger(NodePanel.class);

    private final NodeListPanel nodeListPanel;

    private final TabPanel tabPanel;

    public NodePanel(MainModel model) {
        nodeListPanel = new NodeListPanel(model);
        tabPanel = new TabPanel(model);

        LOGGER.info("Register tabPanel and nodeListPanel in applicationContext.");
        DefaultApplicationContext.getInstance().register("tabPanel", tabPanel);
        DefaultApplicationContext.getInstance().register("nodeListPanel", nodeListPanel);
    }

    public void create(DockingDesktop desk) {

        desk.addDockable(nodeListPanel);
        desk.split(nodeListPanel, tabPanel, DockingConstants.SPLIT_RIGHT);
        desk.setDockableWidth(nodeListPanel, 0.2d);

        LOGGER.info("Added node list and tab panel to desktop.");
    }

    public void addAccessoryListListener(AccessoryListListener l) {
        tabPanel.addAccessoryListListener(l);
    }

    public void addAccessoryListSelectionListener(ListSelectionListener l) {
        tabPanel.addAccessoryListSelectionListener(l);
    }

    public void addAccessoryTableListener(AccessoryTableListener l) {
        tabPanel.addAccessoryTableListener(l);
    }

    public void addAnalogPortListener(OutputListener<AnalogPortStatus> l) {
        tabPanel.addAnalogPortListener(l);
    }

    public void addBacklightPortListener(BacklightPortListener l) {
        tabPanel.addBacklightPortListener(l);
    }

    public void addMacroListListener(MacroListListener l) {
        tabPanel.addMacroListListener(l);
    }

    public void addMacroListSelectionListener(ListSelectionListener l) {
        tabPanel.addMacroListSelectionListener(l);
    }

    public void addMacroTableListener(MacroTableListener l) {
        tabPanel.addMacroTableListener(l);
    }

    // public void addMotorPortListener(MotorPortListener<MotorPortStatus> l) {
    // tabPanel.addMotorPortListener(l);
    // }

    public void addNodeListListener(NodeListActionListener l) {
        nodeListPanel.addNodeListListener(l);
    }

    public void addNodeListSelectionListener(ListSelectionListener l) {
        nodeListPanel.addListSelectionListener(l);
    }

    public void addServoPortListener(ServoPortListener<ServoPortStatus> l) {
        tabPanel.addServoPortListener(l);
    }

    // public void addSoundPortListener(OutputListener<SoundPortStatus> l) {
    // tabPanel.addSoundPortListener(l);
    // }

    public void addCvDefinitionRequestListener(CvDefinitionRequestListener l) {
        tabPanel.addCvDefinitionRequestListener(l);
    }
}
