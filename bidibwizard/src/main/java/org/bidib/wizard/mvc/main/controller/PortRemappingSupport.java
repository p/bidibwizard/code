package org.bidib.wizard.mvc.main.controller;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.collections4.CollectionUtils;
import org.bidib.jbidibc.core.ProtocolVersion;
import org.bidib.jbidibc.core.node.ConfigurationVariable;
import org.bidib.jbidibc.core.utils.ProductUtils;
import org.bidib.wizard.comm.CommunicationFactory;
import org.bidib.wizard.mvc.main.model.InputPort;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.model.ServoPort;
import org.bidib.wizard.mvc.main.model.SwitchPort;
import org.bidib.wizard.mvc.main.view.panel.CvDefinitionTreeHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Deprecated
public class PortRemappingSupport {

    private static final Logger LOGGER = LoggerFactory.getLogger(PortRemappingSupport.class);

    public void processPortRemapping(
        Node node, List<SwitchPort> switchPorts, List<InputPort> inputPorts, List<ServoPort> servoPorts) {

        if (node.getNode().getProtocolVersion().isLowerThan(ProtocolVersion.VERSION_0_6)) {
            processLegacyPortRemapping(node, switchPorts, inputPorts, servoPorts);
        }
        else {
            LOGGER.warn("The port mapping for protocol version 0.6 and higher is supported with configX messages!");
        }
    }

    private void processLegacyPortRemapping(
        Node node, List<SwitchPort> switchPorts, List<InputPort> inputPorts, List<ServoPort> servoPorts) {
        // check if we have GPIO ports avaialable
        Map<String, ConfigurationVariable> gpioMap = null;
        // count the GPIO ports
        if (ProductUtils.isOneControl(node.getUniqueId()) || ProductUtils.isOneDriveTurn(node.getUniqueId())) {
            // OneControl has no dedicated input ports
            LOGGER
                .info("The current node is a OneControl or a OneDriveTurn. Process port remapping based on known CV structure.");
            gpioMap = new CvDefinitionTreeHelper().getNodes(node, "GPIO");

            LOGGER.info("returned gpioMap: {}", gpioMap);
            if (gpioMap != null && gpioMap.size() > 0) {
                List<ConfigurationVariable> cvList = new LinkedList<ConfigurationVariable>(gpioMap.values());

                cvList = CommunicationFactory.getInstance().getConfigurationVariables(node.getNode(), cvList);

                LOGGER.info("Fetched cvList: {}", cvList);
                LOGGER.info("Fetched gpioMap: {}", gpioMap);
            }
        }

        // handle the "normal" input and switch ports and "configured" GPIO ports
        if (gpioMap != null && gpioMap.size() > 0) {
            boolean hasSwitchPorts = CollectionUtils.isNotEmpty(switchPorts);
            boolean hasInputPorts = CollectionUtils.isNotEmpty(inputPorts);

            // The number of input and output ports contains the configured GPIO ports.
            // We must detect the number of input and output ports and remove them from
            // the list of input and output ports and add the GPIO ports.

            List<InputPort> gpioInputPorts = new LinkedList<InputPort>();
            List<SwitchPort> gpioSwitchPorts = new LinkedList<SwitchPort>();

            try {
                for (Entry<String, ConfigurationVariable> entry : gpioMap.entrySet()) {
                    String portId = entry.getKey();

                    int portConfig = Integer.parseInt(entry.getValue().getValue());
                    LOGGER.info("Current portId: {}, portConfig: {}", portId, portConfig);
                    InputPort inputPort = new InputPort();
                    inputPort.setPortIdentifier(portId);

                    SwitchPort switchPort = new SwitchPort();
                    switchPort.setPortIdentifier(portId);

                    if ((portConfig & 0x01) == 0x01) {
                        inputPort.setEnabled(true);
                        switchPort.setEnabled(false);
                        LOGGER.info("InputPort is enabled, SwitchPort is disabled");
                    }
                    else {
                        inputPort.setEnabled(false);
                        switchPort.setEnabled(true);
                        LOGGER.info("InputPort is disabled, SwitchPort is enabled");
                    }

                    if (hasInputPorts) {
                        LOGGER.info("Add new input port from GPIO: {}", inputPort);
                        gpioInputPorts.add(inputPort);
                    }
                    else {
                        LOGGER.info("No input ports available, don't add GPIO input: {}", inputPort);
                    }
                    if (hasSwitchPorts) {
                        LOGGER.info("Add new switch port from GPIO: {}", switchPort);
                        gpioSwitchPorts.add(switchPort);
                    }
                    else {
                        LOGGER.info("No switch ports available, don't add GPIO switch port: {}", switchPort);
                    }
                }
            }
            catch (Exception ex) {
                LOGGER.warn("Prepare configuration for GPIO ports failed.", ex);
                node.setNodeHasError(true);
            }

            // remove the spurious input and switch ports and replace with GPIO ports
            // get the number of input ports in the list
            int gpioInputPortsCount = gpioInputPorts.size();
            int inputPortsCount = inputPorts.size();
            int portsToKeep = inputPortsCount - gpioInputPortsCount;

            LOGGER.info("Processing input ports, number of GPIO input ports: {}, portsToKeep: {}", gpioInputPortsCount,
                portsToKeep);

            if (portsToKeep > -1) {
                for (InputPort gpioInputPort : gpioInputPorts) {
                    String portId = gpioInputPort.getPortIdentifier();

                    // get the input port and set the port identifier
                    int portNum = Integer.parseInt(portId.substring(5));
                    LOGGER.info("Current GPIO input port number: {}", portNum);
                    int portIndex = portNum + portsToKeep;

                    if ((portIndex > -1) && (portIndex < inputPorts.size())) {
                        InputPort inputPort = inputPorts.get(portNum + portsToKeep);
                        inputPort.setPortIdentifier(portId);
                        inputPort.setEnabled(gpioInputPort.isEnabled());
                        inputPort.setRemappingEnabled(true);
                    }
                    else {
                        LOGGER.warn("Skip processing of input port because index is out of range: {}, total: {}",
                            portIndex, inputPorts.size());
                    }
                }
            }
            else {
                LOGGER.info("Skipped processing of inputPorts.");
            }

            // process the switch ports
            int gpioSwitchPortsCount = gpioSwitchPorts.size();
            int switchPortsCount = switchPorts.size();
            portsToKeep = switchPortsCount - gpioSwitchPortsCount;

            LOGGER.info("Processing switch ports, number of GPIO switch ports: {}, portsToKeep: {}",
                gpioSwitchPortsCount, portsToKeep);

            if (portsToKeep > -1) {
                for (SwitchPort gpioSwitchPort : gpioSwitchPorts) {
                    String portId = gpioSwitchPort.getPortIdentifier();

                    // get the input port and set the port identifier
                    int portNum = Integer.parseInt(portId.substring(5));
                    LOGGER.info("Current GPIO output port number: {}", portNum);
                    int portIndex = portNum + portsToKeep;

                    if ((portIndex > -1) && (portIndex < switchPorts.size())) {
                        SwitchPort switchPort = switchPorts.get(portIndex);
                        switchPort.setPortIdentifier(portId);
                        switchPort.setEnabled(gpioSwitchPort.isEnabled());
                        switchPort.setRemappingEnabled(true);
                    }
                    else {
                        LOGGER.warn("Skip processing of switch port because index is out of range: {}, total: {}",
                            portIndex, switchPorts.size());
                    }
                }
            }
            else {
                LOGGER.info("Skipped processing of switchPorts.");
            }
        }

    }
}
