package org.bidib.wizard.mvc.postprocess.dccdevices;

import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.commons.collections4.CollectionUtils;
import org.bidib.jbidibc.core.utils.ProductUtils;
import org.bidib.wizard.comm.Communication;
import org.bidib.wizard.comm.CommunicationFactory;
import org.bidib.wizard.comm.Direction;
import org.bidib.wizard.comm.SpeedSteps;
import org.bidib.wizard.dccdevices.DecoderType;
import org.bidib.wizard.mvc.main.model.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class LocoDecoderInitializer {
    private static final Logger LOGGER = LoggerFactory.getLogger(LocoDecoderInitializer.class);

    private static LocoDecoderInitializer INSTANCE;

    public abstract void start(final Set<Node> commandStations);

    public abstract AtomicBoolean getInitializerFinishedLock();

    public synchronized static LocoDecoderInitializer getDefault() {

        if (INSTANCE == null) {
            INSTANCE = new BidibLocoDecoderInitializer();
        }

        return INSTANCE;
    }

    private static class BidibLocoDecoderInitializer extends LocoDecoderInitializer {

        private AtomicBoolean initializerFinishedLock = new AtomicBoolean();

        @Override
        public void start(final Set<Node> commandStations) {

            LOGGER.info("Start the BidibLocoDecoderInitializer, commandStations: {}", commandStations);

            Communication communication = CommunicationFactory.getInstance();
            if (communication.isOpened()) {
                // init the loco decoders
                initializeDecoders(communication, commandStations);
            }
            else {
                LOGGER.warn("Initialize the loco decoders is skipped because communication is not open.");

                synchronized (initializerFinishedLock) {
                    initializerFinishedLock.set(true);
                }

            }
        }

        private void initializeDecoders(Communication communication, final Set<Node> commandStations) {

            MonitorDccDevicesReader reader = new MonitorDccDevicesReader();
            List<DecoderType> decoders = reader.getLocoDecoders();
            if (CollectionUtils.isNotEmpty(decoders)) {
                for (DecoderType decoder : decoders) {
                    LOGGER.info("Initialize decoder: {}", decoder);
                    // get the command station node and check if the command station is started
                    for (Node commandStation : commandStations) {

                        if (ProductUtils.isRFBasisNode(commandStation.getUniqueId())) {
                            LOGGER.info("Do not initialize the decoders on the RF Basis Node.");
                            continue;
                        }

                        // TODO check if the command station is started

                        LOGGER.info("Send setSpeed to command station: {}, decoder: {}", commandStation, decoder);
                        communication.setSpeed(commandStation.getNode(), decoder.getPomAddress(),
                            SpeedSteps.valueOf(decoder.getSpeedsteps()), 0, Direction.FORWARD, null, null);
                    }
                }
            }

            synchronized (initializerFinishedLock) {
                initializerFinishedLock.set(true);
            }
        }

        @Override
        public AtomicBoolean getInitializerFinishedLock() {
            return initializerFinishedLock;
        }

    }

}
