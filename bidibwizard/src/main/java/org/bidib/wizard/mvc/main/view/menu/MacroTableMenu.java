package org.bidib.wizard.mvc.main.view.menu;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.Collection;
import java.util.LinkedList;

import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.main.model.Macro;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.function.Function;
import org.bidib.wizard.mvc.main.model.listener.MacroListListener;
import org.bidib.wizard.mvc.main.model.listener.MacroListener;
import org.bidib.wizard.mvc.main.view.menu.listener.MacroTableMenuListener;

public class MacroTableMenu extends BasicPopupMenu implements MacroListener {
    private static final long serialVersionUID = 1L;

    public static final KeyStroke KEYSTROKE_CUT =
        KeyStroke.getKeyStroke(KeyEvent.VK_X, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask());

    public static final KeyStroke KEYSTROKE_COPY =
        KeyStroke.getKeyStroke(KeyEvent.VK_C, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask());

    public static final KeyStroke KEYSTROKE_PASTE =
        KeyStroke.getKeyStroke(KeyEvent.VK_V, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask());

    public static final KeyStroke KEYSTROKE_DELETE = KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0);

    private final Collection<MacroTableMenuListener> menuListeners = new LinkedList<MacroTableMenuListener>();

    private final JMenuItem cut = new JMenuItem(Resources.getString(getClass(), "cut"));

    private final JMenuItem copy = new JMenuItem(Resources.getString(getClass(), "copy"));

    private final JMenuItem pasteBefore = new JMenuItem(Resources.getString(getClass(), "pasteBefore"));

    private final JMenuItem pasteAfter = new JMenuItem(Resources.getString(getClass(), "pasteAfter"));

    private final JMenuItem pasteInvertedAfter = new JMenuItem(Resources.getString(getClass(), "pasteInvertedAfter"));

    private final JMenuItem insertEmptyBefore = new JMenuItem(Resources.getString(getClass(), "insertEmptyBefore"));

    private final JMenuItem insertEmptyAfter = new JMenuItem(Resources.getString(getClass(), "insertEmptyAfter"));

    private final JMenuItem delete = new JMenuItem(Resources.getString(getClass(), "delete"));

    private final JMenuItem selectAll = new JMenuItem(Resources.getString(getClass(), "selectAll"));

    private Macro macro = null;

    public MacroTableMenu(final MainModel model) {
        cut.setAccelerator(KEYSTROKE_CUT);
        cut.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireCut();
            }
        });
        add(cut);

        copy.setAccelerator(KEYSTROKE_COPY);
        copy.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireCopy();
            }
        });
        add(copy);

        pasteBefore.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                firePasteBefore();
            }
        });
        add(pasteBefore);

        pasteAfter.setAccelerator(KEYSTROKE_PASTE);
        pasteAfter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                firePasteAfter();
            }
        });
        add(pasteAfter);

        pasteInvertedAfter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                firePasteInvertedAfter();
            }
        });
        add(pasteInvertedAfter);

        insertEmptyBefore.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireInsertEmptyBefore();
            }
        });
        add(insertEmptyBefore);

        insertEmptyAfter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireInsertEmptyAfter();
            }
        });
        add(insertEmptyAfter);

        delete.setAccelerator(KEYSTROKE_DELETE);
        delete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireDelete();
            }
        });
        add(delete);

        addSeparator();

        selectAll.setAccelerator(
            KeyStroke.getKeyStroke(KeyEvent.VK_A, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
        selectAll.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireSelectAll();
            }
        });
        selectAll.setEnabled(false);
        add(selectAll);

        model.addMacroListListener(new MacroListListener() {
            @Override
            public void listChanged() {
            }

            @Override
            public void macroChanged() {

                if (getMacro() != null) {
                    getMacro().removeMacroListener(MacroTableMenu.this);
                }

                Macro newMacro = model.getSelectedMacro();

                enableMenuItems(newMacro);
                if (newMacro != null) {
                    newMacro.addMacroListener(MacroTableMenu.this);
                }
                setMacro(newMacro);
            }

            @Override
            public void pendingChangesChanged() {
            }
        });
    }

    public void addMenuListener(MacroTableMenuListener l) {
        menuListeners.add(l);
    }

    private void enableMenuItems(Macro macro) {
        if (macro != null) {
            boolean macroEmpty = macro.getFunctionCount() == 0;
            boolean macroFull = macro.getFunctionCount() >= macro.getFunctionSize();

            cut.setEnabled(!macroEmpty);
            copy.setEnabled(!macroEmpty);
            pasteBefore.setEnabled(!macroFull);
            pasteAfter.setEnabled(!macroFull);
            insertEmptyBefore.setEnabled(!macroFull);
            insertEmptyAfter.setEnabled(!macroFull);
            delete.setEnabled(!macroEmpty);
            selectAll.setEnabled(!macroEmpty);
        }
        else {
            cut.setEnabled(false);
            copy.setEnabled(false);
            pasteBefore.setEnabled(true);
            pasteAfter.setEnabled(true);
            insertEmptyBefore.setEnabled(true);
            insertEmptyAfter.setEnabled(true);
            delete.setEnabled(false);
            selectAll.setEnabled(false);
        }
    }

    public void fireCopy() {
        for (MacroTableMenuListener l : menuListeners) {
            l.copy();
        }
    }

    public void fireCut() {
        for (MacroTableMenuListener l : menuListeners) {
            l.cut();
        }
    }

    public void fireDelete() {
        for (MacroTableMenuListener l : menuListeners) {
            l.delete();
        }
    }

    private void fireInsertEmptyAfter() {
        for (MacroTableMenuListener l : menuListeners) {
            l.insertEmptyAfter();
        }
    }

    private void fireInsertEmptyBefore() {
        for (MacroTableMenuListener l : menuListeners) {
            l.insertEmptyBefore();
        }
    }

    public void firePasteAfter() {
        for (MacroTableMenuListener l : menuListeners) {
            l.pasteAfter();
        }
    }

    public void firePasteInvertedAfter() {
        for (MacroTableMenuListener l : menuListeners) {
            l.pasteInvertedAfter();
        }
    }

    private void firePasteBefore() {
        for (MacroTableMenuListener l : menuListeners) {
            l.pasteBefore();
        }
    }

    private void fireSelectAll() {
        for (MacroTableMenuListener l : menuListeners) {
            l.selectAll();
        }
    }

    private Macro getMacro() {
        return macro;
    }

    private void setMacro(Macro macro) {
        this.macro = macro;
    }

    @Override
    public void functionsAdded(int row, Function<? extends BidibStatus>[] functions) {
        enableMenuItems(macro);
    }

    @Override
    public void functionRemoved(int row) {
        enableMenuItems(macro);
    }

    @Override
    public void functionsRemoved() {
        enableMenuItems(macro);
    }

    @Override
    public void labelChanged(String label) {
    }

    @Override
    public void functionMoved(int fromIndex, int toIndex, Function<? extends BidibStatus> fromFunction) {
    }

    @Override
    public void startConditionChanged() {
    }

    @Override
    public void slowdownFactorChanged() {
    }

    @Override
    public void cyclesChanged() {
    }
}
