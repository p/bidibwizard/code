package org.bidib.wizard.mvc.dmx.view.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.util.List;
import java.util.Locale;

import javax.swing.JComponent;
import javax.swing.JPanel;

import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.dmx.model.DmxChannel;
import org.bidib.wizard.mvc.dmx.model.DmxModel;
import org.bidib.wizard.mvc.dmx.view.panel.axis.LabeledNumberTickUnit;
import org.bidib.wizard.mvc.dmx.view.panel.renderer.DmxDrawingSupplier;
import org.bidib.wizard.mvc.dmx.view.panel.renderer.DmxLineRenderer;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.ChartTheme;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.axis.TickUnitSource;
import org.jfree.chart.axis.TickUnits;
import org.jfree.chart.entity.ChartEntity;
import org.jfree.chart.entity.XYItemEntity;
import org.jfree.chart.labels.StandardXYToolTipGenerator;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.RangeType;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.RectangleInsets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DmxModelerPanel extends JPanel implements ChartMouseListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(DmxModelerPanel.class);

    private static final long serialVersionUID = 1L;

    private XYDataset channelData;

    private DmxLineRenderer dmxLineRenderer;

    private JFreeChart lineChart;

    private DmxChartPanel dmxChartPanel;

    List<JFreeChart> charts;

    private final DmxModel dmxModel;

    /**
     * Creates a new instance.
     */
    public DmxModelerPanel(final DmxModel dmxModel) {
        super(new BorderLayout());
        this.dmxModel = dmxModel;

        // create the list for the charts
        this.charts = new java.util.ArrayList<JFreeChart>();

        this.channelData = new XYSeriesCollection();
        add(createContent());
    }

    public void cleanup() {
        if (dmxChartPanel != null) {
            dmxChartPanel.cleanup();
        }
    }

    /**
     * Records a chart as belonging to this panel. It will subsequently be returned by the getCharts() method.
     * 
     * @param chart
     *            the chart.
     */
    public void addChart(JFreeChart chart) {
        this.charts.add(chart);
    }

    /**
     * Returns an array containing the charts within this panel.
     * 
     * @return The charts.
     */
    public JFreeChart[] getCharts() {
        int chartCount = this.charts.size();
        JFreeChart[] charts = new JFreeChart[chartCount];
        for (int i = 0; i < chartCount; i++) {
            charts[i] = (JFreeChart) this.charts.get(i);
        }
        return charts;
    }

    /**
     * Creates a tabbed pane for displaying sample charts.
     * 
     * @return the tabbed pane.
     */
    private JComponent createContent() {
        dmxChartPanel = createChartPanel();
        return dmxChartPanel;
    }

    private static final int MAX_MINUTES = 10;

    /**
     * Creates a chart based on the second dataset, with a fitted power regression line.
     * 
     * @return the chart panel.
     */
    private DmxChartPanel createChartPanel() {

        // create subplot 1...
        final NumberAxis xAxis =
            new AutoRangeLimitedValueAxis(Resources.getString(DmxModelerPanel.class, "time-seconds"));
        // NumberAxis xAxis = new NumberAxis("Time [ms]");
        // xAxis.setAutoRange(false);
        xAxis.setFixedAutoRange(-1 * 60 * 1000.0 * MAX_MINUTES);
        // xAxis.setAutoRangeIncludesZero(true);
        // xAxis.setDefaultAutoRange(new Range(0.0, 255.0));
        // xAxis.setLowerMargin(0.02); // reduce the default margins
        // xAxis.setUpperMargin(0.02);
        xAxis.setRange(0.0, 60 * 1000.0 * MAX_MINUTES);
        xAxis.setRangeType(RangeType.POSITIVE);
        // xAxis.setTickUnit(new NumberTickUnit(1000.0));
        xAxis.setStandardTickUnits(createIntegerTickUnits(getLocale()));

        AutoRangeLimitedValueAxis yAxis =
            new AutoRangeLimitedValueAxis(Resources.getString(DmxModelerPanel.class, "brightness"));
        // NumberAxis yAxis = new NumberAxis("Brightness (%)");
        // yAxis.setAutoRangeIncludesZero(false);
        yAxis.setFixedZoomAxis(true);
        yAxis.setAutoRange(false);
        yAxis.setFixedAutoRange(-1 * 100.0);
        // yAxis.setDefaultAutoRange(new Range(0.0, 255.0));
        // yAxis.setLowerBound(0);
        // yAxis.setUpperBound(100);
        yAxis.setRange(DmxSeries.BRIGHTNESS_MIN, DmxSeries.BRIGHTNESS_MAX);
        yAxis.setRangeType(RangeType.POSITIVE);
        // yAxis.setUpperMargin(255);
        // yAxis.setRange(0, 100);

        yAxis.setTickUnit(new NumberTickUnit(25.0));
        yAxis.setMinorTickCount(5);

        dmxLineRenderer = new DmxLineRenderer();
        dmxLineRenderer.setDrawOutlines(true);
        dmxLineRenderer.setUseOutlinePaint(true);
        dmxLineRenderer.setBaseToolTipGenerator(new StandardXYToolTipGenerator() {

            private static final long serialVersionUID = 1L;

            /** The default tooltip format. */
            public static final String DEFAULT_PORT_TOOL_TIP_FORMAT = "{3}-port: {2}-action: {4}: ({0}ms, {1}%)";

            public static final String DEFAULT_MACRO_TOOL_TIP_FORMAT = "{3}-{2}: ({0}ms, {1}%)";

            @Override
            public String generateLabelString(XYDataset dataset, int series, int item) {
                Object[] items = createItemArray(dataset, series, item);
                DmxSeries dmxSeries = (DmxSeries) ((XYSeriesCollection) dataset).getSeries(series);
                DmxDataItem dmxDataItem = (DmxDataItem) dmxSeries.getDataItem(item);

                String seriesKey = (String) dmxSeries.getKey();
                DmxChannel currentDmxChannel = null;
                int channelId = Integer.parseInt(seriesKey);
                for (DmxChannel dmxChannel : dmxModel.getDmxScenery().getUsedChannels()) {
                    if (dmxChannel.getChannelId() == channelId) {
                        currentDmxChannel = dmxChannel;
                        break;
                    }
                }

                // TODO i18N

                String result = null;
                String portName = "<no port>";
                if (dmxDataItem.getPort() != null) {
                    portName = String.valueOf(dmxDataItem.getPort());
                    result =
                        MessageFormat.format(DEFAULT_PORT_TOOL_TIP_FORMAT, items[1], items[2], portName,
                            currentDmxChannel, (dmxDataItem.getAction() != null ? dmxDataItem.getAction() : "none"));
                }
                else if (dmxDataItem.getMacro() != null) {
                    // TODO i18N
                    portName =
                        "Macro #" + dmxDataItem.getMacro().getMacroId() + ", Action: "
                            + dmxDataItem.getMacro().getAction();
                    result =
                        MessageFormat.format(DEFAULT_MACRO_TOOL_TIP_FORMAT, items[1], items[2], portName,
                            currentDmxChannel);
                }
                else {
                    result = MessageFormat.format(DEFAULT_TOOL_TIP_FORMAT, items[1], items[2], currentDmxChannel);
                }

                return result;
            }
        });

        final XYPlot plot = new XYPlot(this.channelData, xAxis, yAxis, dmxLineRenderer);
        plot.setBackgroundPaint(Color.lightGray);
        plot.setDomainGridlinePaint(Color.white);
        plot.setRangeGridlinePaint(Color.white);
        plot.setAxisOffset(new RectangleInsets(4, 4, 4, 4));

        plot.setDomainPannable(true);
        // do not pan range axis
        plot.setRangePannable(false);

        // show crosshair
        plot.setDomainCrosshairVisible(true);
        plot.setRangeCrosshairVisible(true);

        // create and return the chart panel...
        lineChart =
            new JFreeChart(null/* "DMX Channel Values" */, null/* JFreeChart.DEFAULT_TITLE_FONT */, plot, false /* true */);
        // lineChart.getLegend().setPosition(RectangleEdge.RIGHT);

        addChart(lineChart);

        // change the drawing supplier, we need bigger item shapes
        ChartTheme chartTheme = ChartFactory.getChartTheme();
        if (chartTheme instanceof StandardChartTheme) {
            StandardChartTheme standardChartTheme = (StandardChartTheme) chartTheme;
            standardChartTheme.setDrawingSupplier(new DmxDrawingSupplier());
        }
        ChartUtilities.applyCurrentTheme(lineChart);

        // create the chart panel
        final DmxChartPanel chartPanel = new DmxChartPanel(lineChart, dmxModel);
        chartPanel.addChartMouseListener(this);

        // Marker m3 = new ValueMarker(100);
        // m3.setStroke(new BasicStroke(2));
        // m3.setPaint(Color.RED);
        // plot.addDomainMarker(m3);
        //
        // Marker m4 = new ValueMarker(200);
        // m4.setStroke(new BasicStroke(2));
        // m4.setPaint(Color.RED);
        // plot.addDomainMarker(m4);

        return chartPanel;
    }

    public static TickUnitSource createIntegerTickUnits(Locale locale) {
        TickUnits units = new TickUnits();

        DecimalFormat numberFormat = new DecimalFormat("####0");
        DecimalFormat numberFormat2 = new DecimalFormat("####0.0");
        // NumberFormat numberFormat = NumberFormat.getNumberInstance(locale);

        final String timeSeconds = Resources.getString(DmxModelerPanel.class, "time-seconds");
        final String timeMilliSeconds = Resources.getString(DmxModelerPanel.class, "time-milliseconds");

        units.add(new LabeledNumberTickUnit(1, numberFormat, 2, timeMilliSeconds, 1));
        units.add(new LabeledNumberTickUnit(2, numberFormat, 2, timeMilliSeconds, 1));
        units.add(new LabeledNumberTickUnit(5, numberFormat, 5, timeMilliSeconds, 1));
        units.add(new LabeledNumberTickUnit(10, numberFormat, 2, timeMilliSeconds, 1));
        units.add(new LabeledNumberTickUnit(20, numberFormat, 2, timeMilliSeconds, 1));
        units.add(new LabeledNumberTickUnit(50, numberFormat, 5, timeMilliSeconds, 1));
        units.add(new LabeledNumberTickUnit(100, numberFormat, 2, timeMilliSeconds, 1));
        units.add(new LabeledNumberTickUnit(200, numberFormat, 2, timeMilliSeconds, 1));
        units.add(new LabeledNumberTickUnit(500, numberFormat2, 5, timeSeconds, 1000));
        units.add(new LabeledNumberTickUnit(1000, numberFormat, 2, timeSeconds, 1000));
        units.add(new LabeledNumberTickUnit(2000, numberFormat, 2, timeSeconds, 1000));
        units.add(new LabeledNumberTickUnit(5000, numberFormat, 5, timeSeconds, 1000));
        units.add(new LabeledNumberTickUnit(10000, numberFormat, 2, timeSeconds, 1000));
        units.add(new LabeledNumberTickUnit(20000, numberFormat, 2, timeSeconds, 1000));
        units.add(new LabeledNumberTickUnit(50000, numberFormat, 5, timeSeconds, 1000));
        units.add(new LabeledNumberTickUnit(100000, numberFormat, 2, timeSeconds, 1000));
        units.add(new LabeledNumberTickUnit(200000, numberFormat, 2, timeSeconds, 1000));
        units.add(new LabeledNumberTickUnit(500000, numberFormat, 5, timeSeconds, 1000));
        units.add(new LabeledNumberTickUnit(1000000, numberFormat, 2, timeSeconds, 1000));
        // units.add(new NumberTickUnit(2000000, numberFormat, 2));
        // units.add(new NumberTickUnit(5000000, numberFormat, 5));
        // units.add(new NumberTickUnit(10000000, numberFormat, 2));
        // units.add(new NumberTickUnit(20000000, numberFormat, 2));
        // units.add(new NumberTickUnit(50000000, numberFormat, 5));
        // units.add(new NumberTickUnit(100000000, numberFormat, 2));
        // units.add(new NumberTickUnit(200000000, numberFormat, 2));
        // units.add(new NumberTickUnit(500000000, numberFormat, 5));
        // units.add(new NumberTickUnit(1000000000, numberFormat, 2));
        // units.add(new NumberTickUnit(2000000000, numberFormat, 2));
        // units.add(new NumberTickUnit(5000000000.0, numberFormat, 5));
        // units.add(new NumberTickUnit(10000000000.0, numberFormat, 2));
        return units;
    }

    @Override
    public void chartMouseClicked(ChartMouseEvent event) {

    }

    @Override
    public void chartMouseMoved(ChartMouseEvent event) {
        // This is used to 'select' a point
        ChartEntity entity = event.getEntity();
        JFreeChart chart = event.getChart();
        if (entity instanceof XYItemEntity) {
            LOGGER.trace("moved, entity: {}", entity);
            XYItemEntity xyie = (XYItemEntity) entity;
            int series = xyie.getSeriesIndex();
            XYDataset dataset = xyie.getDataset();

            LOGGER.trace("seriesKey: {}", dataset.getSeriesKey(series));

            if (chart.equals(lineChart)) {
                this.dmxLineRenderer.setHighlightedItem(xyie.getSeriesIndex(), xyie.getItem());
            }
        }
        else {
            if (chart.equals(lineChart)) {
                this.dmxLineRenderer.setHighlightedItem(-1, -1);
            }
        }
    }

    public void loadSceneryPoints() {
        if (dmxChartPanel != null) {
            dmxChartPanel.loadSceneryPoints();
        }
    }

    public void storeSceneryPoints() {
        if (dmxChartPanel != null) {
            dmxChartPanel.storeSceneryPoints();
        }
    }
}