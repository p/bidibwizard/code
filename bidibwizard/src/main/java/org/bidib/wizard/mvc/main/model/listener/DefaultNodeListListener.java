package org.bidib.wizard.mvc.main.model.listener;

import org.bidib.wizard.mvc.main.model.Node;

/**
 * The default implementation of the NodeListListener.
 * 
 */
public class DefaultNodeListListener implements NodeListListener {

    @Override
    public void listChanged() {
    }

    @Override
    public void nodeChanged() {
    }

    @Override
    public void nodeStateChanged() {
    }

    @Override
    public void listNodeAdded(Node node) {
    }

    @Override
    public void listNodeRemoved(Node node) {
    }

    @Override
    public void nodeWillChange() {
    }
}
