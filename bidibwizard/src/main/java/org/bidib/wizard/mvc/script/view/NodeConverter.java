package org.bidib.wizard.mvc.script.view;

import org.apache.commons.lang.StringUtils;
import org.bidib.wizard.mvc.main.model.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.value.BindingConverter;

/**
 * Converts Values to Strings and vice-versa using a given Format.
 */
public final class NodeConverter implements BindingConverter<Node, String> {
    private static final Logger LOGGER = LoggerFactory.getLogger(NodeConverter.class);

    // Implementing Abstract Behavior *************************************

    /**
     * Formats the source value and returns a String representation.
     * 
     * @param sourceValue
     *            the source value
     * @return the formatted sourceValue
     */
    @Override
    public String targetValue(Node sourceValue) {
        if (sourceValue instanceof Node) {
            Node updateState = (Node) sourceValue;
            return updateState.toString();
        }
        return null;
    }

    /**
     * Parses the given String encoding and sets it as the subject's new value. Silently catches {@code ParseException}.
     * 
     * @param targetValue
     *            the value to be converted and set as new subject value
     */
    @Override
    public Node sourceValue(String targetValue) {
        try {
            if (StringUtils.isNotBlank((String) targetValue)) {

            }
        }
        catch (Exception e) {
            LOGGER.warn("Cannot convert the target value to Node: {}", targetValue);
        }
        return null;
    }
}
