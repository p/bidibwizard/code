package org.bidib.wizard.mvc.main.model.function;

import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.mvc.main.model.Port;

public abstract class PortAction<T extends BidibStatus, P extends Port<?>> extends Function<T>
    implements Delayable, PortAware<P> {
    private int delay;

    private P port;

    public PortAction(T action, String key, P port, int delay) {
        super(action, key);
        this.port = port;
        this.delay = delay;
    }

    @Override
    public int getDelay() {
        return delay;
    }

    @Override
    public void setDelay(int delay) {
        this.delay = delay;
    }

    public P getPort() {
        return port;
    }

    public void setPort(P port) {
        this.port = port;
    }
}
