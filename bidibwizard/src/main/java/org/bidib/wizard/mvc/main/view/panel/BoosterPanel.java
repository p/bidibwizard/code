package org.bidib.wizard.mvc.main.view.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.LinkedList;

import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.JToggleButton;

import org.bidib.jbidibc.core.enumeration.CommandStationState;
import org.bidib.jbidibc.core.utils.NodeUtils;
import org.bidib.wizard.comm.BoosterStatus;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.view.menu.BasicPopupMenu;
import org.bidib.wizard.mvc.main.view.panel.listener.StatusListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alexandriasoftware.swing.JSplitButton;
import com.alexandriasoftware.swing.action.ButtonClickedActionListener;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.debug.FormDebugPanel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.FormLayout;

import eu.hansolo.steelseries.gauges.Radial1Vertical;
import eu.hansolo.steelseries.tools.Section;

public class BoosterPanel extends JPanel {
    private static final Logger LOGGER = LoggerFactory.getLogger(BoosterPanel.class);

    private static final long serialVersionUID = 1L;

    private static final String ENCODED_COLUMN_SPECS =
        "pref, 3dlu, pref, 3dlu, pref, 3dlu, pref, 3dlu, pref, 3dlu, pref, 3dlu, fill:50dlu:grow";

    private Collection<StatusListener> statusListeners = new LinkedList<StatusListener>();

    private final MainModel mainModel;

    private final Radial1Vertical ampereMeter;

    private final Radial1Vertical thermoMeter;

    private final Radial1Vertical voltMeter;

    private final JToggleButton boosterStatusButton;

    private final JTextField boosterStatusText;

    private final JSplitButton commandStationStatusButton;

    private final JTextField commandStationStatusText;

    private JPopupMenu popupMenuSplit;

    public BoosterPanel(final MainModel model) {
        this.mainModel = model;
        setLayout(new BorderLayout());

        boosterStatusButton = createBoosterStatusButton();
        boosterStatusText = addStatusText();

        commandStationStatusButton = createCommandStationStatusButton();

        setStatusButtonState(commandStationStatusButton, CommandStationState.OFF);
        commandStationStatusText = new JTextField(20);
        commandStationStatusText.setEditable(false);
        setStatusText(commandStationStatusText, mainModel.getCommandStationState());

        ampereMeter = addAmpereMeter();
        JPanel eastPanel = new JPanel(new GridLayout(2, 1));
        thermoMeter = addThermoMeter();
        eastPanel.add(thermoMeter);
        voltMeter = addVoltMeter();
        eastPanel.add(voltMeter);

        // prepare the form
        DefaultFormBuilder dialogBuilder = null;
        boolean debugDialog = false;
        if (debugDialog) {
            JPanel panel = new FormDebugPanel();
            dialogBuilder = new DefaultFormBuilder(new FormLayout(ENCODED_COLUMN_SPECS), panel);
        }
        else {
            JPanel panel = new JPanel(new BorderLayout());
            dialogBuilder = new DefaultFormBuilder(new FormLayout(ENCODED_COLUMN_SPECS), panel);
        }
        dialogBuilder.border(Borders.DIALOG);

        dialogBuilder.append("Booster", boosterStatusButton, boosterStatusText);
        dialogBuilder.append("Command Station", commandStationStatusButton, commandStationStatusText);
        dialogBuilder.nextLine();

        dialogBuilder.appendRow("5dlu");
        dialogBuilder.nextLine();
        dialogBuilder.appendRow("fill:100dlu:grow");
        dialogBuilder.append(ampereMeter, 7);
        dialogBuilder.append(eastPanel, 3);

        // add the form
        add(dialogBuilder.build(), BorderLayout.CENTER);

        // update the state of the components
        updateComponentState();
    }

    public void boosterCurrentChanged(final int current) {
        LOGGER.debug("Booster current has changed: {}", current);

        if (current >= 0) {
            ampereMeter.setEnabled(true);
            ampereMeter.setValueAnimated(current);
        }
        else {
            LOGGER.info("The current value is no longer valid.");
            ampereMeter.setValue(0);
            ampereMeter.setEnabled(false);
        }
    }

    public void boosterMaximumCurrentChanged(int maximumCurrent) {
        LOGGER.debug("Booster maximum current has changed: {}", maximumCurrent);

        if (maximumCurrent > 0) {
            ampereMeter.setMaxValue(maximumCurrent);

            Section[] sections = new Section[3];

            sections[0] = new Section(0, maximumCurrent / 2, Color.GREEN);
            sections[1] = new Section(maximumCurrent / 2, 3 * maximumCurrent / 4, Color.ORANGE);
            sections[2] = new Section(3 * maximumCurrent / 4, maximumCurrent, Color.RED);
            ampereMeter.setSections(sections);
            ampereMeter.setValue(mainModel.getBoosterCurrent());
        }
    }

    public void boosterStateChanged(BoosterStatus status) {
        LOGGER.info("Status of command station has changed: {}", status);

        setStatusButtonState(boosterStatusButton, status);
        setStatusText(boosterStatusText, status);
    }

    public void commandStationStateChanged(CommandStationState status) {
        LOGGER.info("Status of command station has changed: {}", status);

        setStatusButtonState(commandStationStatusButton, status);
        setStatusText(commandStationStatusText, status);
    }

    public void temperatureChanged(int temperature) {
        thermoMeter.setValueAnimated(temperature);
    }

    public void voltageChanged(int voltage) {
        voltMeter.setValueAnimated(voltage / 10d);
    }

    private JToggleButton createBoosterStatusButton() {
        final JToggleButton statusButton = new JToggleButton(Resources.getString(getClass(), "off"));
        // set the preferred size to prevent change of width if the button text is changed
        Dimension dim = statusButton.getPreferredSize();
        statusButton.setPreferredSize(dim);

        statusButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JToggleButton button = (JToggleButton) e.getSource();

                if (button.isSelected()) {
                    fireSwitchedOn();
                }
                else {
                    fireSwitchedOff();
                }
            }
        });
        setStatusButtonState(statusButton, BoosterStatus.OFF);

        return statusButton;
    }

    private JSplitButton createCommandStationStatusButton() {
        final JSplitButton statusButton = new JSplitButton(Resources.getString(getClass(), "cs-go"));
        // set the preferred size to prevent change of width if the button text is changed
        Dimension dim = statusButton.getPreferredSize();
        statusButton.setPreferredSize(new Dimension(dim.width + statusButton.getSplitWidth(), dim.height));

        statusButton.addButtonClickedActionListener(new ButtonClickedActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                String statusButtonText = statusButton.getText();
                LOGGER.info("The command station status button was clicked, current statusButtonText: {}",
                    statusButtonText);

                if (statusButtonText.equals(Resources.getString(BoosterPanel.class, "cs-go"))) {
                    fireSwitchedCommandStationOn(false);
                }
                else if (statusButtonText.equals(Resources.getString(BoosterPanel.class, "cs-goIgnWd"))) {
                    fireSwitchedCommandStationOn(true);
                }
                else if (statusButtonText.equals(Resources.getString(BoosterPanel.class, "cs-stop"))) {
                    fireSwitchedCommandStationStop();
                }
                else if (statusButtonText.equals(Resources.getString(BoosterPanel.class, "cs-softStop"))) {
                    fireSwitchedCommandStationSoftStop();
                }
                else if (statusButtonText.equals(Resources.getString(BoosterPanel.class, "cs-off"))) {
                    fireSwitchedCommandStationOff();
                }
            }
        });

        // prepare the popup menu for the split button
        popupMenuSplit = new BasicPopupMenu() {
            private static final long serialVersionUID = 1L;
        };
        JMenuItem csGoItem = new JMenuItem(Resources.getString(getClass(), "cs-go"));

        csGoItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // prepareUpdate(true);
                fireSwitchedCommandStationOn(false);
            }
        });
        BasicPopupMenu.addMenuItem(popupMenuSplit, csGoItem);

        JMenuItem csGoIgnWdItem = new JMenuItem(Resources.getString(getClass(), "cs-goIgnWd"));

        csGoIgnWdItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fireSwitchedCommandStationOn(true);
            }
        });
        BasicPopupMenu.addMenuItem(popupMenuSplit, csGoIgnWdItem);

        JMenuItem csSoftStopItem = new JMenuItem(Resources.getString(getClass(), "cs-softStop"));

        csSoftStopItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fireSwitchedCommandStationSoftStop();
            }
        });
        BasicPopupMenu.addMenuItem(popupMenuSplit, csSoftStopItem);

        JMenuItem csOffItem = new JMenuItem(Resources.getString(getClass(), "cs-off"));

        csOffItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fireSwitchedCommandStationOff();
            }
        });
        BasicPopupMenu.addMenuItem(popupMenuSplit, csOffItem);

        // add the popup menu to the split button
        statusButton.setPopupMenu(popupMenuSplit);

        statusButton.setEnabled(false);
        setStatusButtonState(statusButton, CommandStationState.OFF);

        return statusButton;
    }

    private JTextField addStatusText() {
        JTextField statusText = new JTextField();

        statusText.setEditable(false);
        statusText.setColumns(20);

        setStatusText(statusText, mainModel.getBoosterStatus());

        JPopupMenu popup = new JPopupMenu("Reload status");
        JMenuItem mi = new JMenuItem(Resources.getString(getClass(), "booster-query-status"));
        mi.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                LOGGER.info("Query the booster state");
                fireQueryBoosterState();
            }
        });
        popup.add(mi);
        statusText.add(popup);
        statusText.setComponentPopupMenu(popup);

        return statusText;
    }

    public void addStatusListener(StatusListener l) {
        statusListeners.add(l);
    }

    private Radial1Vertical addAmpereMeter() {
        Radial1Vertical result = new Radial1Vertical();
        result.setMinimumSize(new Dimension(220, 220));
        result.setPreferredSize(new Dimension(440, 400));

        result.setFrameVisible(true);
        result.setLcdDecimals(0);
        result.setLcdUnitStringVisible(false);
        result.setLedVisible(false);
        result.setSectionsVisible(true);
        result.setTitle(Resources.getString(getClass(), "current"));
        result.setTrackVisible(false);
        result.setUnitString("mA");

        return result;
    }

    private Radial1Vertical addThermoMeter() {
        Radial1Vertical result = new Radial1Vertical();
        result.setPreferredSize(new Dimension(210, 210));

        result.setFrameVisible(true);
        result.setLcdDecimals(0);
        result.setLcdUnitStringVisible(false);
        result.setLedVisible(false);
        result.setMaxValue(100);
        result.setSectionsVisible(true);
        result.setTitle(Resources.getString(getClass(), "temperature"));
        result.setTrackVisible(false);
        result.setUnitString("°C");

        Section[] sections = new Section[3];

        sections[0] = new Section(0, 60, Color.GREEN);
        sections[1] = new Section(60, 85, Color.ORANGE);
        sections[2] = new Section(85, result.getMaxValue(), Color.RED);
        result.setSections(sections);

        return result;
    }

    private Radial1Vertical addVoltMeter() {
        Radial1Vertical result = new Radial1Vertical();
        result.setPreferredSize(new Dimension(210, 210));

        result.setFrameVisible(true);
        result.setLcdDecimals(0);
        result.setLcdUnitStringVisible(false);
        result.setLedVisible(false);
        result.setMaxValue(30);
        result.setSectionsVisible(true);
        result.setTitle(Resources.getString(getClass(), "voltage"));
        result.setTrackVisible(false);
        result.setUnitString("V");

        Section[] sections = new Section[5];

        sections[0] = new Section(0, 10, Color.RED);
        sections[1] = new Section(10, 12, Color.ORANGE);
        sections[2] = new Section(12, 17, Color.GREEN);
        sections[3] = new Section(17, 20, Color.ORANGE);
        sections[4] = new Section(20, result.getMaxValue(), Color.RED);
        result.setSections(sections);

        return result;
    }

    private void fireSwitchedOn() {
        for (StatusListener l : statusListeners) {
            l.switchedOn();
        }
    }

    private void fireSwitchedOff() {
        for (StatusListener l : statusListeners) {
            l.switchedOff();
        }
    }

    private void fireQueryBoosterState() {
        for (StatusListener l : statusListeners) {
            l.queryBoosterState();
        }
    }

    private void fireSwitchedCommandStationOn(boolean ignoreWatchDog) {
        LOGGER.info("Set the command station ON.");
        for (StatusListener l : statusListeners) {
            l.switchedCommandStationOn(ignoreWatchDog);
        }
    }

    private void fireSwitchedCommandStationStop() {
        LOGGER.info("Set the command station STOP.");
        for (StatusListener l : statusListeners) {
            l.switchedCommandStationStop();
        }
    }

    private void fireSwitchedCommandStationSoftStop() {
        LOGGER.info("Set the command station SOFT STOP.");
        for (StatusListener l : statusListeners) {
            l.switchedCommandStationSoftStop();
        }
    }

    private void fireSwitchedCommandStationOff() {
        LOGGER.info("Set the command station OFF.");
        for (StatusListener l : statusListeners) {
            l.switchedCommandStationOff();
        }
    }

    public String getName() {
        return Resources.getString(getClass(), "name");
    }

    private void setStatusButtonState(JToggleButton statusButton, BoosterStatus status) {
        Node selectedNode = mainModel.getSelectedNode();
        if (status != null) {

            boolean wasEnabled = statusButton.isEnabled();
            if (selectedNode != null) {
                wasEnabled = wasEnabled && NodeUtils.hasBoosterFunctions(selectedNode.getUniqueId());
            }

            if (BoosterStatus.isOffState(status)) {
                // the booster is off

                statusButton.setSelected(false);
                statusButton.setText(Resources.getString(getClass(), "on"));
            }
            else {
                // the booster is on
                statusButton.setSelected(true);
                statusButton.setText(Resources.getString(getClass(), "off"));
            }

            // restore the enabled state because setSelected() enables the button
            LOGGER.info("Set boosterStatus button enabled: {}", wasEnabled);
            statusButton.setEnabled(wasEnabled);
        }
        else {
            if (selectedNode != null && NodeUtils.hasBoosterFunctions(selectedNode.getUniqueId())) {
                statusButton.setEnabled(true);
            }
            else {
                statusButton.setSelected(false);
                statusButton.setEnabled(false);
            }
        }
    }

    private void setStatusButtonState(JSplitButton statusButton, CommandStationState status) {
        Node selectedNode = mainModel.getSelectedNode();
        if (status != null) {

            boolean wasEnabled = statusButton.isEnabled();
            if (selectedNode != null) {
                wasEnabled = wasEnabled && NodeUtils.hasCommandStationFunctions(selectedNode.getUniqueId());
            }

            if (wasEnabled) {
                if (CommandStationState.isOffState(status)) {
                    // the command station is off
                    statusButton.setSelected(false);
                    statusButton.setText(Resources.getString(getClass(), "cs-go"));
                }
                else {
                    // the command station is on
                    statusButton.setSelected(true);
                    statusButton.setText(Resources.getString(getClass(), "cs-stop"));
                }
            }

            // restore the enabled state because setSelected() enables the button
            LOGGER.debug("Set the status button enabled: {}", wasEnabled);
            statusButton.setEnabled(wasEnabled);
        }
        else {
            if (selectedNode != null && NodeUtils.hasCommandStationFunctions(selectedNode.getUniqueId())) {
                LOGGER.debug("Set the status button enabled because the node has command station functions.");
                statusButton.setEnabled(true);
            }
            else {
                LOGGER.debug("Set the status button disabled because the node has no command station functions.");
                statusButton.setSelected(false);
                statusButton.setEnabled(false);
            }
        }
    }

    private void setStatusText(JTextField statusText, BoosterStatus status) {
        statusText.setText(status != null ? status.toString() : "");
    }

    private void setStatusText(JTextField statusText, CommandStationState status) {
        LOGGER.info("Set the new status of the command station: {}", status);
        statusText.setText(status != null ? status.toString() : "");
    }

    public void nodeChanged() {
        LOGGER.info("The selected node has changed.");

        updateComponentState();
    }

    private void updateComponentState() {

        Node node = mainModel.getSelectedNode();
        boolean isBooster = false;
        if (node != null && NodeUtils.hasBoosterFunctions(node.getUniqueId())) {
            isBooster = true;
        }
        LOGGER.info("The selected node has booster functions: {}, node: {}", isBooster, node);

        boosterStatusButton.setEnabled(isBooster);
        ampereMeter.setEnabled(isBooster);
        voltMeter.setEnabled(isBooster);
        thermoMeter.setEnabled(isBooster);

        boolean isCommandStation = false;
        if (node != null && NodeUtils.hasCommandStationFunctions(node.getUniqueId())) {
            isCommandStation = true;
        }
        LOGGER.info("The selected node has command station functions: {}, node: {}", isCommandStation, node);

        commandStationStatusButton.setEnabled(isCommandStation);
    }

}
