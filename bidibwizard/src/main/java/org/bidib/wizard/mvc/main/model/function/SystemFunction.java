package org.bidib.wizard.mvc.main.model.function;

import org.bidib.wizard.comm.BidibStatus;

public abstract class SystemFunction<T extends BidibStatus> extends Function<T> {
    public static final Integer SYSTEM_FUNCION = 255;

    public SystemFunction(T action, String key) {
        super(action, key);
    }
}
