package org.bidib.wizard.mvc.main.model;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.bidib.wizard.comm.FeedbackPortStatus;
import org.bidib.wizard.mvc.main.model.listener.PortListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FeedbackPortTableModel
    extends SimplePortTableModel<FeedbackPortStatus, FeedbackPort, PortListener<FeedbackPortStatus>> {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(FeedbackPortTableModel.class);

    private static final int COLUMNS = 8;

    // map from port to table position
    private final Map<Port<?>, Integer> portIndexes = new HashMap<Port<?>, Integer>();

    public FeedbackPortTableModel() {

        setColumnIdentifiers(new String[COLUMNS /* + 1 */]);
    }

    @Override
    protected int getColumnPortInstance() {
        return COLUMN_PORT_INSTANCE;
    }

    @Override
    public void setRowCount(int rowCount) {
        if (rowCount == 0) {
            portIndexes.clear();
        }
        super.setRowCount(rowCount);
    }

    /**
     * Add ports to table model.
     * 
     * @param ports
     *            the ports to add to the table model
     */
    public void addRows(List<FeedbackPort> ports) {
        if (ports != null) {
            List<Object> rowData = new LinkedList<Object>();
            int index = 0;

            for (Port<?> port : ports) {
                portIndexes.put(port, index++);
                rowData.add(port);
                if (index > 0 && index % COLUMNS == 0) {
                    // dummy column which will be removed from SimplePortListPanel
                    rowData.add("");
                    addRow(rowData.toArray());
                    rowData.clear();
                }
            }

            // check if a row is not fully filled ...
            if (rowData.size() > 0) {
                for (index = rowData.size(); index < COLUMNS + 1; index++) {
                    if (index > 0 && index % COLUMNS == 0) {
                        // dummy column which will be removed from SimplePortListPanel
                        rowData.add("");
                        addRow(rowData.toArray());
                        rowData.clear();

                        break;
                    }
                    rowData.add(null);
                }
            }
        }
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return true;
    }

    @Override
    public void setValueAt(Object value, int row, int column) {
        final Object o = getValueAt(row, column);

        if (o instanceof FeedbackPort) {
            final FeedbackPort port = (FeedbackPort) o;
            LOGGER.debug("Set value, current port: {}, value: {}", port, value);

            if (value instanceof String) {
                port.setLabel((String) value);
                super.setValueAt(port, row, column);
                fireLabelChanged(port, port.getLabel());
            }
            else if (value instanceof FeedbackPort) {
                super.setValueAt(value, row, column);
            }

        }
    }

    public void updatePort(Port<?> port) {
        if (port != null) {
            Integer index = portIndexes.get(port);
            if (index != null) {
                int row = index / COLUMNS;
                int column = index % COLUMNS;

                setValueAt(port, row, column);
            }
            else {
                LOGGER.warn("Feedback port is not registered: {}", port);
            }
        }
    }
}
