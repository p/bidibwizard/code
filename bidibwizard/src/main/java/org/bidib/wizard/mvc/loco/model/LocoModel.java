package org.bidib.wizard.mvc.loco.model;

import java.util.BitSet;
import java.util.Collection;
import java.util.LinkedList;

import org.bidib.wizard.comm.Direction;
import org.bidib.wizard.comm.SpeedSteps;
import org.bidib.wizard.mvc.loco.model.listener.LocoModelListener;

import com.jgoodies.common.bean.Bean;

public class LocoModel extends Bean {
    private static final long serialVersionUID = 1L;

    private final Collection<LocoModelListener> listeners = new LinkedList<LocoModelListener>();

    public static final String PROPERTYNAME_SPEED = "speed";

    public static final String PROPERTYNAME_REPORTED_SPEED = "reportedSpeed";

    private int address;

    private int reportedSpeed;

    private int dynStateEnergy;

    // default direction is forward
    private Direction direction = Direction.FORWARD;

    private int speed = 0;

    private SpeedSteps speedSteps = SpeedSteps.DCC128;

    private BitSet functions = new BitSet(29);

    public void addLocoModelListener(LocoModelListener l) {
        listeners.add(l);
    }

    public void removeLocoModelListener(LocoModelListener l) {
        listeners.remove(l);
    }

    public int getAddress() {
        return address;
    }

    public void setAddress(int address) {
        if (this.address != address) {
            this.address = address;

            // address change will reset energy state
            setDynStateEnergy(0);

            fireAddressChanged(address);
        }
    }

    public int getReportedSpeed() {
        return reportedSpeed;
    }

    public void setReportedSpeed(int reportedSpeed) {
        int oldValue = this.reportedSpeed;

        // if (this.reportedSpeed != reportedSpeed) {
        this.reportedSpeed = reportedSpeed;
        // fireReportedSpeedChanged(reportedSpeed);
        // }

        firePropertyChange(PROPERTYNAME_REPORTED_SPEED, oldValue, reportedSpeed);
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        if (this.direction != direction) {
            this.direction = direction;
            fireDirectionChanged(direction);
        }
    }

    public boolean getFunction(int index) {
        return functions.get(index);
    }

    public BitSet getFunctions() {
        return (BitSet) functions.clone();
    }

    public void setFunction(int index, boolean value) {
        if (getFunction(index) != value) {
            functions.set(index, value);
            fireFunctionChanged(index, value);
        }
    }

    public int getDynStateEnergy() {
        return dynStateEnergy;
    }

    public void setDynStateEnergy(int dynStateEnergy) {
        if (this.dynStateEnergy != dynStateEnergy || dynStateEnergy == 0 || dynStateEnergy == 1) {
            this.dynStateEnergy = dynStateEnergy;

            fireDynStateEnergyChanged(dynStateEnergy);
        }
    }

    public SpeedSteps getSpeedSteps() {
        return speedSteps;
    }

    public void setSpeedSteps(SpeedSteps speedSteps) {
        if (this.speedSteps != speedSteps) {
            this.speedSteps = speedSteps;
            fireSpeedStepsChanged(speedSteps);
        }
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        int oldValue = this.speed;

        // if (this.speed != speed || speed == 0 || speed == 1) {
        this.speed = speed;
        // fireSpeedChanged(speed);
        // }

        firePropertyChange(PROPERTYNAME_SPEED, oldValue, speed);
    }

    private void fireAddressChanged(int address) {
        for (LocoModelListener l : listeners) {
            l.addressChanged(address);
        }
    }

    private void fireDirectionChanged(Direction direction) {
        for (LocoModelListener l : listeners) {
            l.directionChanged(direction);
        }
    }

    private void fireFunctionChanged(int index, boolean value) {
        for (LocoModelListener l : listeners) {
            l.functionChanged(index, value);
        }
    }

    private void fireSpeedStepsChanged(SpeedSteps speedSteps) {
        for (LocoModelListener l : listeners) {
            l.speedStepsChanged(speedSteps);
        }
    }

    private void fireDynStateEnergyChanged(int dynStateEnergy) {
        for (LocoModelListener l : listeners) {
            l.dynStateEnergyChanged(dynStateEnergy);
        }
    }

}
