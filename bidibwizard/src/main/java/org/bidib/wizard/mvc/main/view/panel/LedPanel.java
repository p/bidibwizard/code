package org.bidib.wizard.mvc.main.view.panel;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.dk.nullesoft.Airlog.LED;
import org.bidib.wizard.mvc.main.model.StatusModel;
import org.bidib.wizard.mvc.main.model.listener.StatusListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LedPanel extends JPanel {
    private static final Logger LOGGER = LoggerFactory.getLogger(LedPanel.class);

    private static final long serialVersionUID = 1L;

    public enum Axis {
        lineAxis, pageAxis
    }

    public LedPanel(StatusModel model, Axis axis) {

        final LED connectLed = new LED();
        connectLed.setName("CD");

        setLedStatus(connectLed, model.isCd());

        final LED receiveLed = new LED();
        receiveLed.setName("RX");

        setLedStatus(receiveLed, model.isRx());

        final LED sendLed = new LED();
        sendLed.setName("TX");

        setLedStatus(sendLed, model.isTx());

        final LED ctsLed = new LED();
        ctsLed.setName("CTS");

        setLedStatus(ctsLed, model.isCd());

        if (Axis.pageAxis.equals(axis)) {
            setLayout(new GridBagLayout());

            GridBagConstraints c = new GridBagConstraints();

            c.anchor = GridBagConstraints.LINE_START;
            c.fill = GridBagConstraints.NONE;
            c.gridx = 0;
            c.gridy = 0;
            c.insets = new Insets(5, 3, 5, 3);
            c.weightx = 0;
            c.weighty = 0;
            add(connectLed, c);

            c.gridx++;
            add(receiveLed, c);

            c.gridx++;
            add(sendLed, c);

            c.gridx++;
            add(ctsLed, c);

            c.gridx = 0;
            c.gridy++;
            add(new JLabel("CD"), c);

            c.gridx++;
            add(new JLabel("RX"), c);

            c.gridx++;
            add(new JLabel("TX"), c);

            c.gridx++;
            add(new JLabel("CTS"), c);
        }
        else {
            connectLed.setBorder(new EmptyBorder(2, 4, 2, 4));
            receiveLed.setBorder(new EmptyBorder(2, 4, 2, 4));
            sendLed.setBorder(new EmptyBorder(2, 4, 2, 4));
            ctsLed.setBorder(new EmptyBorder(2, 4, 2, 4));

            setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
            add(Box.createHorizontalGlue());
            add(new JLabel("CD"));
            add(Box.createRigidArea(new Dimension(3, 0)));
            add(connectLed);
            add(Box.createRigidArea(new Dimension(3, 0)));
            add(new JLabel("RX"));
            add(Box.createRigidArea(new Dimension(3, 0)));
            add(receiveLed);
            add(Box.createRigidArea(new Dimension(3, 0)));
            add(new JLabel("TX"));
            add(Box.createRigidArea(new Dimension(3, 0)));
            add(sendLed);
            add(Box.createRigidArea(new Dimension(3, 0)));
            add(new JLabel("CTS"));
            add(Box.createRigidArea(new Dimension(3, 0)));
            add(ctsLed);
            add(Box.createHorizontalGlue());
        }

        model.addStatusListener(new StatusListener() {
            @Override
            public void cdChanged(boolean cd) {
                setLedStatus(connectLed, cd);
            }

            @Override
            public void runningChanged(boolean running) {
            }

            @Override
            public void rxChanged(boolean rx) {
                setLedStatus(receiveLed, rx);
            }

            @Override
            public void txChanged(boolean tx) {
                setLedStatus(sendLed, tx);
            }

            @Override
            public void ctsChanged(boolean cts) {
                setLedStatus(ctsLed, cts);
            }
        });
        setMinimumSize(getPreferredSize());
    }

    public void setTitleBorder() {
        setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
            Resources.getString(getClass(), "title") + ":"));

    }

    /**
     * Sets the led status. As this will call the swing components we must be sure that the AWT-Thread is used to
     * perform the update.
     * 
     * @param led
     *            the led
     * @param on
     *            the led status to set
     */
    private void setLedStatus(final LED led, final boolean on) {
        LOGGER.trace("setLedStatus led: {}, on: {}", led.getName(), on);
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                if (on) {
                    led.usePrimary();
                }
                else {
                    led.useSecondary();
                }
            }
        });
    }
}
