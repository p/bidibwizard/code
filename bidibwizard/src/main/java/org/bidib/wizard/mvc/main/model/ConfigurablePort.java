package org.bidib.wizard.mvc.main.model;

import org.bidib.wizard.comm.BidibStatus;

public interface ConfigurablePort<T extends BidibStatus> {

    /**
     * @return the enabled state
     */
    boolean isEnabled();

    /**
     * @return the port status
     */
    T getStatus();

    /**
     * @return <code>true</code> if the port supports remapping, <code>false</code> otherwise
     */
    boolean isRemappingEnabled();

    /**
     * @param keys
     *            the keys to search
     * @return {@code true}: at least one key is supported by the node, {@code false}: none of the provided keys is not
     *         supported by the node
     */
    public boolean isPortConfigKeySupported(byte[] keys);
}
