package org.bidib.wizard.mvc.main.view.cvdef;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.ListModel;
import javax.swing.SwingConstants;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.exchange.vendorcv.BitdescriptionType;
import org.bidib.jbidibc.exchange.vendorcv.CVType;
import org.bidib.jbidibc.exchange.vendorcv.ModeType;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.common.utils.ImageUtils;
import org.bidib.wizard.mvc.common.view.converter.StringConverter;
import org.bidib.wizard.mvc.common.view.cvdefinition.CvDefinitionTreeTableModel;
import org.bidib.wizard.mvc.common.view.validation.PropertyValidationI18NSupport;
import org.bidib.wizard.mvc.main.view.panel.NodeTree;
import org.bidib.wizard.utils.InputValidationDocument;
import org.bidib.wizard.utils.XmlLocaleUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.adapter.ComboBoxAdapter;
import com.jgoodies.binding.beans.BeanAdapter;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.binding.value.ConverterValueModel;
import com.jgoodies.binding.value.ValueHolder;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.common.collect.ArrayListModel;
import com.jgoodies.common.swing.MnemonicUtils;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.util.PropertyValidationSupport;
import com.jgoodies.validation.view.ValidationComponentUtils;

public class CvRadioValueEditor extends CvValueNumberEditor<Byte> {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(CvRadioValueEditor.class);

    private static final int NUM_OPTIONS = 16;

    private Byte[] radioValues;

    private ValueModel[] bitValueModels = new ValueModel[NUM_OPTIONS];

    private JToggleButton[] radioButtonBits = new JToggleButton[NUM_OPTIONS];

    private BitfieldExclusiveBean bitfieldBean = new BitfieldExclusiveBean(NUM_OPTIONS);

    private BitfieldBean16 allowedValuesBean = new BitfieldBean16();

    private ValueModel writeEnabled;

    private SelectionInList<OffsetValue> offsetCvSelection;

    private ArrayListModel<OffsetValue> offsetCvList = new ArrayListModel<>();

    private boolean setInProgress;

    private Integer upperPartBitValues;

    @Override
    protected ValidationResult validateModel(
        PresentationModel<CvValueBean<Byte>> model, CvValueBean<Byte> cvValueBean) {
        PropertyValidationSupport support = new PropertyValidationI18NSupport(cvValueBean, "validation");

        return support.getResult();
    }

    @Override
    protected DefaultFormBuilder prepareFormPanel() {

        writeEnabled = new ValueHolder(false);

        // Get buffered model objects.
        cvValueModel = cvAdapter.getBufferedModel("cvValue");
        valueConverterModel = new ConverterValueModel(cvValueModel, new StringConverter(new DecimalFormat("#")));

        DefaultFormBuilder formBuilder =
            new DefaultFormBuilder(new FormLayout(
                "3dlu, max(10dlu;pref), 3dlu, max(30dlu;pref), 20dlu, max(10dlu;pref), 3dlu, max(30dlu;pref), 0dlu:grow"));
        JTextField textCvValue = BasicComponentFactory.createTextField(valueConverterModel, false);
        textCvValue.setDocument(new InputValidationDocument(InputValidationDocument.NUMERIC));
        textCvValue.setEditable(false);

        // the needs reboot Icon is invisible by default
        ImageIcon warnIcon = ImageUtils.createImageIcon(NodeTree.class, "/icons/warn.png");
        JLabel needsRebootIcon =
            new JLabel(Resources.getString(getClass(), "rebootrequired"), warnIcon, SwingConstants.LEADING);
        needsRebootIcon.setVisible(false);

        ValidationComponentUtils.setMandatory(textCvValue, true);
        ValidationComponentUtils.setMessageKey(textCvValue, "validation.cvvalue_key");

        formBuilder.leadingColumnOffset(1);
        formBuilder.nextLine(0);
        formBuilder.append(BasicComponentFactory.createLabel(cvNumberModel, new CvNodeMessageFormat("{0} (CV{1})")), 4);
        formBuilder.nextLine();
        formBuilder.append(textCvValue, 2);
        formBuilder.append(needsRebootIcon);

        formBuilder.nextLine();

        offsetCvSelection =
            new SelectionInList<OffsetValue>((ListModel<OffsetValue>) offsetCvList,
                new PropertyAdapter<BitfieldExclusiveBean>(bitfieldBean, "offsetValue", true));

        ComboBoxAdapter<OffsetValue> comboBoxAdapterOffsetCv = new ComboBoxAdapter<OffsetValue>(offsetCvSelection);
        final JComboBox comboOffsetCv = new JComboBox();
        comboOffsetCv.setModel(comboBoxAdapterOffsetCv);

        formBuilder.append(Resources.getString(getClass(), "offset"), comboOffsetCv, 5);

        final BeanAdapter<BitfieldExclusiveBean> bitfieldBeanAdapter =
            new BeanAdapter<BitfieldExclusiveBean>(bitfieldBean, true);

        // create the radio buttons
        for (int indexBit = 0; indexBit < NUM_OPTIONS; indexBit++) {
            // prepare the model
            ValueModel bitValueModel = bitfieldBeanAdapter.getValueModel(BitfieldBean.BIT_PROPERTY + indexBit);
            bitValueModels[indexBit] = bitValueModel;

            radioButtonBits[indexBit] = BasicComponentFactory.createRadioButton(bitValueModel, Boolean.TRUE, null);
            radioButtonBits[indexBit].setEnabled(false);
        }

        JLabel[] bitLabels = new JLabel[NUM_OPTIONS];
        for (int indexBit = 0; indexBit < (NUM_OPTIONS / 2); indexBit++) {
            // prepare the UI
            formBuilder.nextLine();
            bitLabels[indexBit] = formBuilder.append(Integer.toString(indexBit), radioButtonBits[indexBit]);
            bitLabels[indexBit + (NUM_OPTIONS / 2)] =
                formBuilder.append(Integer.toString(indexBit + (NUM_OPTIONS / 2)),
                    radioButtonBits[indexBit + (NUM_OPTIONS / 2)]);
        }

        // add bindings for enable/disable the textfield
        PropertyConnector.connect(needsRebootModel, "value", needsRebootIcon, "visible");

        // add bindings for enable/disable the checkbox
        for (int indexBit = 0; indexBit < NUM_OPTIONS; indexBit++) {
            PropertyConnector.connect(allowedValuesBean, "bit" + indexBit, radioButtonBits[indexBit], "enabled");
            PropertyConnector.connect(allowedValuesBean, "bit" + indexBit, radioButtonBits[indexBit], "visible");
            PropertyConnector.connect(allowedValuesBean, "bit" + indexBit, bitLabels[indexBit], "visible");
        }

        final PropertyChangeListener localChangeListener = new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                LOGGER.info("The bitfield bean value has changed: {}, propName: {}", evt.getNewValue(),
                    evt.getPropertyName());

                Byte bitfieldValue = bitfieldBean.getBitfieldValue();

                cvValueModel.setValue(ByteUtils.getInt(bitfieldValue));

                if (!setInProgress) {
                    triggerValidation("signalValidState");
                }
            }
        };

        bitfieldBean.addPropertyChangeListener(localChangeListener);

        cvValueModel.addValueChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                LOGGER.info("Converter model has changed, evt: {}", evt);
                Byte value = null;
                if (evt.getNewValue() instanceof Number) {
                    Number number = (Number) evt.getNewValue();
                    value = number.byteValue();
                }
                LOGGER.info("Set the bitfield value: {}", (value != null ? ByteUtils.getInt(value) : null));

                if (localChangeListener != null) {
                    LOGGER.debug("Remove property changeListener from bitfieldBean.");
                    bitfieldBean.removePropertyChangeListener(localChangeListener);
                }

                bitfieldBean.setBitfieldValue(value);

                // add the change listener again
                if (localChangeListener != null) {
                    LOGGER.debug("Add property changeListener to bitfieldBean again.");
                    bitfieldBean.addPropertyChangeListener(localChangeListener);
                }
            }
        });

        return formBuilder;
    }

    @Override
    public void setValue(CvNode cvNode, Map<String, CvNode> cvNumberToNodeMap) {
        LOGGER.debug("Set the new value from node: {}", cvNode);
        setInProgress = true;

        boolean timeout = cvNode.getConfigVar().isTimeout();

        // set textfield editable before the value is set because the validation is triggered
        writeEnabled.setValue(!(ModeType.RO.equals(cvNode.getCV().getMode()) || timeout));

        // bitfield value -> get the values attribute
        CVType cv = cvNode.getCV();

        // the radio values are the values for every bit
        String radiovalues = cv.getRadiovalues();
        String radioGroups = cv.getRadioGroups();
        LOGGER.info("Current radiovalues: {}, radioGroups: {}, cv: {}", radiovalues, radioGroups, cv);

        // list that keeps all radio values for later processing
        List<OffsetValue> allRadioValues = new ArrayList<>();
        // process radio values
        if (StringUtils.isNotBlank(radiovalues)) {
            String[] splited = radiovalues.split(",");

            radioValues = new Byte[NUM_OPTIONS];
            int index = 0;
            for (String part : splited) {

                byte value = (byte) (Integer.parseInt(part) & 0xFF);

                LOGGER.debug("Prepare value to be added to offset combo: {}", ByteUtils.getInt(value));

                OffsetValue offsetValue = new OffsetValue(value);
                allRadioValues.add(offsetValue);

                if (index < NUM_OPTIONS) {
                    radioValues[index] = Byte.valueOf(value);
                }

                index++;
            }
        }
        else {
            // clear existing values
            radioValues = null;
        }

        // evaluate language
        String lang = XmlLocaleUtils.getXmlLocaleVendorCV();

        // clear all values
        offsetCvList.clear();

        // reset the index for the start of the second group
        upperPartBitValues = null;

        // process radio groups
        List<Integer> enabledRadioBits = new ArrayList<>();
        if (StringUtils.isNotBlank(radioGroups)) {
            String[] splitedGroups = radioGroups.split(";");
            if (splitedGroups.length > 1) {

                // process the splitedGroups
                // enable only the radio buttons in the first group
                String[] splitedLowerPart = splitedGroups[0].split(",");
                for (String part : splitedLowerPart) {
                    Integer value = Integer.valueOf(part) /*- 1*/;
                    enabledRadioBits.add(value);
                }

                // process the 2nd group
                int upperPartBits = -1;
                String[] splitedUpperPart = splitedGroups[1].split(",");
                for (String part : splitedUpperPart) {

                    int value = Integer.parseInt(part);
                    LOGGER.info("Current upper part value: {}", value);

                    // find the matching offset value
                    OffsetValue offsetValue = allRadioValues.get(value - 1);
                    LOGGER.info("Current offset value: {}", offsetValue);

                    offsetValue.setDescription(getBitDescription(cv.getBitdescription(), value, lang));
                    offsetCvList.add(offsetValue);

                    upperPartBits += offsetValue.getValue();
                }

                if (upperPartBits > -1) {
                    upperPartBitValues = upperPartBits;
                    LOGGER.info("Set the upperPartBitValues: {}", upperPartBitValues);
                }

            }
        }
        // reset selection
        offsetCvSelection.setSelection(null);

        // evaluate the allowed bit values
        Integer allowedBitValues = null;
        BitfieldBean16 enabledBitIndexes = null;

        if (ModeType.RO.equals(cvNode.getCV().getMode()) || timeout) {
            allowedBitValues = Integer.valueOf(0);
        }
        else if (!CollectionUtils.isEmpty(enabledRadioBits)) {
            enabledBitIndexes = new BitfieldBean16();
            for (Integer bit : enabledRadioBits) {
                LOGGER.debug("Set bit as enabled: {}", bit - 1);
                enabledBitIndexes.setBit(true, bit - 1);
            }

            allowedBitValues = enabledBitIndexes.getBitfield();
        }
        else {
            int radioValueMask = 0;
            for (int index = 0; index < NUM_OPTIONS; index++) {

                Byte radioValue = radioValues[NUM_OPTIONS - index - 1];
                if (radioValue != null) {
                    radioValueMask = (radioValueMask << 1) | 1;
                }
                else {
                    radioValueMask = (radioValueMask << 1) | 0;
                }
            }

            allowedBitValues = Integer.valueOf(radioValueMask);
        }
        // this calls the binding for the enabled state of the checkbox
        allowedValuesBean.setBitfield(allowedBitValues);

        // prepare the text of the radio buttons
        for (int indexBit = 0; indexBit < NUM_OPTIONS; indexBit++) {

            boolean isBitSet = true;
            if (enabledBitIndexes != null) {
                if (!enabledBitIndexes.getBit(indexBit)) {
                    isBitSet = false;
                }
            }

            if (isBitSet && indexBit < allRadioValues.size()) {
                try {
                    OffsetValue offsetValue = allRadioValues.get(indexBit);

                    StringBuilder sb = new StringBuilder();
                    sb.append(getBitDescription(cv.getBitdescription(), indexBit + 1, lang));
                    sb.append(" (").append(ByteUtils.getInt(offsetValue.getValue())).append(")").toString();
                    MnemonicUtils.configure(radioButtonBits[indexBit], sb.toString());

                    radioButtonBits[indexBit]
                        .setToolTipText(getBitDescriptionHelp(cv.getBitdescription(), indexBit + 1, lang));
                }
                catch (Exception ex) {
                    LOGGER.warn("Configure radio button failed for indexBit: {}", indexBit, ex);
                }
            }
            else {
                MnemonicUtils.configure(radioButtonBits[indexBit], "");

                radioButtonBits[indexBit].setToolTipText(null);
            }
        }

        // initialize the bitfieldBean
        bitfieldBean.setAllRadioValues(allRadioValues);
        bitfieldBean.setEnabledRadioBits(enabledRadioBits);
        bitfieldBean.setOffsetCvListValues(offsetCvList);

        // set the value in the cvValueBean
        super.setValue(cvNode, cvNumberToNodeMap);

        // process the new value and set all elements correct
        refreshComponents();

        setInProgress = false;

        cvAdapter.resetChanged();
        getTrigger().triggerFlush();

        triggerValidation(null);
    }

    private void refreshComponents() {
        // enable / disable the checkboxes
        Number value = cvValueBean.getCvValue();

        if (value == null) {
            LOGGER.info("Get the value from the config var.");
            try {
                String val = cvValueBean.getCvNode().getConfigVar().getValue();
                value = Byte.valueOf(val);
            }
            catch (NumberFormatException ex) {
                LOGGER.warn("Read value from config var failed: {}", ex.getMessage());
            }
            catch (Exception ex) {
                LOGGER.warn("Read value from config var failed.", ex);
            }
        }

        LOGGER.info("Initialize the bitfieldBean with value: {}", value);

        bitfieldBean.setBitfieldValue(value);
    }

    private String getBitDescription(List<BitdescriptionType> bitdescriptions, int bit, String lang) {
        String description = null;
        for (BitdescriptionType bitdescription : bitdescriptions) {
            if (lang.equals(bitdescription.getLang()) && bit == bitdescription.getBitnum().intValue()) {
                description = bitdescription.getText();
                break;
            }
        }
        return description;
    }

    private String getBitDescriptionHelp(List<BitdescriptionType> bitdescriptions, int bit, String lang) {
        String description = null;
        for (BitdescriptionType bitdescription : bitdescriptions) {
            if (lang.equals(bitdescription.getLang()) && bit == bitdescription.getBitnum().intValue()) {
                description = bitdescription.getHelp();
                break;
            }
        }
        return description;
    }

    @Override
    public void updateCvValues(Map<String, CvNode> cvNumberToNodeMap, CvDefinitionTreeTableModel treeModel) {
        CvNode cvNode = (CvNode) cvNumberModel.getValue();
        cvNode.setValueAt(cvValueBean.getCvValue(), CvNode.COLUMN_NEW_VALUE);

        // treeModel.refreshTreeTable(cvNode);
    }

    /**
     * Refresh the displayed value from the node.
     */
    public void refreshValue() {
        LOGGER.info("Refresh the value!");
        super.refreshValue();

        // refresh the components with the new value
        refreshComponents();
    }
}
