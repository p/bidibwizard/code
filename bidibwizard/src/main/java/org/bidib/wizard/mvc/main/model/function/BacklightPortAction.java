package org.bidib.wizard.mvc.main.model.function;

import org.bidib.jbidibc.exchange.lcmacro.BacklightActionType;
import org.bidib.jbidibc.exchange.lcmacro.BacklightPortActionType;
import org.bidib.jbidibc.exchange.lcmacro.BacklightPortPoint;
import org.bidib.jbidibc.exchange.lcmacro.LcMacroPointType;
import org.bidib.wizard.comm.BacklightPortStatus;
import org.bidib.wizard.mvc.main.model.BacklightPort;

public class BacklightPortAction extends SimplePortAction<BacklightPortStatus, BacklightPort> {
    /**
     * Creates a new instance of BacklightPortAction and set the action to {@link BacklightPortStatus#START}.
     */
    public BacklightPortAction() {
        this(BacklightPortStatus.START);
    }

    public BacklightPortAction(BacklightPortStatus action) {
        this(action, null, 0, 0);
    }

    /**
     * Creates a new instance of BacklightPortAction and set the action to {@link BacklightPortStatus#START}.
     * 
     * @param delay
     *            the delay value
     * @param backlightPort
     *            the servoPort instance
     * @param value
     *            the target value of the backlight
     */
    public BacklightPortAction(BacklightPortStatus action, BacklightPort backlightPort, int delay, int value) {
        super(action, KEY_BACKLIGHT, backlightPort, delay, value);
    }

    public String getDebugString() {
        int id = 0;

        if (getPort() != null) {
            id = getPort().getId();
        }
        return "@" + getDelay() + " Backlight:" + String.format("%02d", id) + "->" + getValue();
    }

    @Override
    public LcMacroPointType toLcMacroPoint() {
        BacklightPortActionType backlightPortActionType = new BacklightPortActionType();
        backlightPortActionType.setAction(BacklightActionType.fromValue(getAction().name()));
        backlightPortActionType.setBrightness(getValue());
        BacklightPortPoint backlightPortPoint =
            new BacklightPortPoint()
                .withDelay(getDelay()).withOutputNumber(getPort().getId())
                .withBacklightPortActionType(backlightPortActionType);
        return backlightPortPoint;
    }

    public static class BacklightPortActionBuilder extends BacklightPortActionBuilderBase<BacklightPortActionBuilder> {
        public static BacklightPortActionBuilder backlightPortAction() {
            return new BacklightPortActionBuilder();
        }

        public BacklightPortActionBuilder() {
            super(new BacklightPortAction());
        }

        public BacklightPortAction build() {
            return getInstance();
        }
    }

    static class BacklightPortActionBuilderBase<GeneratorT extends BacklightPortActionBuilderBase<GeneratorT>> {
        private BacklightPortAction instance;

        protected BacklightPortActionBuilderBase(BacklightPortAction aInstance) {
            instance = aInstance;
        }

        protected BacklightPortAction getInstance() {
            return instance;
        }

        @SuppressWarnings("unchecked")
        public GeneratorT withDelay(int delay) {
            instance.setDelay(delay);

            return (GeneratorT) this;
        }

        @SuppressWarnings("unchecked")
        public GeneratorT withPort(BacklightPort port) {
            instance.setPort(port);

            return (GeneratorT) this;
        }

        @SuppressWarnings("unchecked")
        public GeneratorT withAction(BacklightPortStatus action) {
            instance.setAction(action);

            return (GeneratorT) this;
        }

        @SuppressWarnings("unchecked")
        public GeneratorT withBrightness(int brightness) {
            // convert the percentage to a value between 0..255
            brightness = (int) (brightness * 2.55);
            instance.setValue(brightness);

            return (GeneratorT) this;
        }
    }
}
