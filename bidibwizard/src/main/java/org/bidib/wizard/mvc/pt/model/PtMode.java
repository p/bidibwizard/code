package org.bidib.wizard.mvc.pt.model;

/**
 * The programming mode used for the programming track.
 */
public enum PtMode {
    BYTE("byte"), BIT("bit");

    private final String key;

    private PtMode(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }
}
