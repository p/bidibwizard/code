package org.bidib.wizard.mvc.script.view.input;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JTextField;

import org.apache.commons.lang3.StringUtils;
import org.bidib.wizard.common.context.ApplicationContext;
import org.bidib.wizard.common.locale.Resources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.view.ValidationComponentUtils;

public class StringInputPanel extends AbstractInputSelectionPanel {
    private static final Logger LOGGER = LoggerFactory.getLogger(StringInputPanel.class);

    private ValueModel stringValueModel;

    private ItemSelectionModel<String> stringSelectionModel;

    @Override
    public void initialize(
        final ApplicationContext context, final DefaultFormBuilder dialogBuilder, ValidationSupport validationSupport,
        String variableName, String variableType, String caption, String defaultValue, String prevValue) {
        this.variableName = variableName;
        setDefaultValue(defaultValue);

        stringSelectionModel = new ItemSelectionModel<>();

        stringValueModel =
            new PropertyAdapter<ItemSelectionModel<String>>(stringSelectionModel,
                ItemSelectionModel.PROPERTY_SELECTED_ITEM, true);

        JTextField textInput = BasicComponentFactory.createTextField(stringValueModel, false);

        caption = "<html>" + caption + "</html>";

        dialogBuilder.append(caption, textInput);

        ValidationComponentUtils.setMandatory(textInput, true);
        ValidationComponentUtils.setMessageKeys(textInput, "validation." + variableName);

        String initialValue = defaultValue;
        if (StringUtils.isNotBlank(prevValue)) {
            initialValue = prevValue;
        }

        if (StringUtils.isNotBlank(initialValue)) {
            LOGGER.info("Set the initial value: {}", initialValue);
            stringSelectionModel.setSelectedItem(StringUtils.strip(initialValue, "\""));
        }

        content = dialogBuilder.build();

        // validate on change
        stringSelectionModel.addPropertyChangeListener(ItemSelectionModel.PROPERTY_SELECTED_ITEM,
            new PropertyChangeListener() {
                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    validationSupport.triggerValidation();
                }
            });
    }

    public void validate(final ValidationResult validationResult) {
        if (StringUtils.isBlank(stringSelectionModel.getSelectedItem())) {
            validationResult.addError(variableName + " " + Resources.getString((Class<?>) null, "not_empty"),
                "validation." + variableName);
        }
    }

    @Override
    public Object getSelectedValue() {
        if (stringValueModel.getValue() != null) {
            return stringValueModel.getValue();
        }
        return null;
    }
}
