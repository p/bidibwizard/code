package org.bidib.wizard.mvc.main.view.table.listener;

public interface ButtonListener {
    void buttonPressed(int row, int column);
}
