package org.bidib.wizard.mvc.locolist.view;

import org.bidib.jbidibc.core.enumeration.CsQueryTypeEnum;

import com.jgoodies.binding.beans.Model;

public class LocoListBeanModel extends Model {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTYNAME_ADDRESS = "address";

    public static final String PROPERTYNAME_CS_QUERY_TYPE = "csQueryType";

    private Integer address;

    private CsQueryTypeEnum csQueryType = CsQueryTypeEnum.LOCO_LIST;

    public LocoListBeanModel() {
    }

    /**
     * @return the address
     */
    public Integer getAddress() {
        return address;
    }

    /**
     * @param address
     *            the address to set
     */
    public void setAddress(Integer address) {
        Integer oldAddress = this.address;
        this.address = address;
        firePropertyChange(PROPERTYNAME_ADDRESS, oldAddress, address);
    }

    /**
     * @return the csQuery type
     */
    public CsQueryTypeEnum getCsQueryType() {
        return csQueryType;
    }

    /**
     * @param csQueryType
     *            the csQuery type to set
     */
    public void setCsQueryType(CsQueryTypeEnum csQueryType) {
        CsQueryTypeEnum oldValue = this.csQueryType;
        this.csQueryType = csQueryType;
        firePropertyChange(PROPERTYNAME_CS_QUERY_TYPE, oldValue, csQueryType);
    }

}
