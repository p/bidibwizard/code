package org.bidib.wizard.mvc.main.view.component;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;

import org.apache.commons.lang3.StringUtils;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.dialog.EscapeDialog;
import org.bidib.wizard.utils.InputValidationDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.forms.builder.ButtonBarBuilder;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

public class InsertPortsConfirmDialog extends EscapeDialog {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(InsertPortsConfirmDialog.class);

    private static final String ENCODED_DIALOG_COLUMN_SPECS = "pref, 10dlu, pref, 3dlu, pref:grow";

    private int result = JOptionPane.CANCEL_OPTION;

    private JTextField textPortCount;

    private int portsCount;

    public InsertPortsConfirmDialog(Frame frame, boolean modal) {
        super(frame, Resources.getString(InsertPortsConfirmDialog.class, "title"), modal);

        getContentPane().setLayout(new BorderLayout());

        DefaultFormBuilder builder =
            new DefaultFormBuilder(new FormLayout(ENCODED_DIALOG_COLUMN_SPECS,
                "pref, 10dlu, top:35dlu, 5dlu, pref, 3dlu, pref, 15dlu, pref"));
        builder.border(Borders.DIALOG);

        JLabel iconLabel = new JLabel(UIManager.getIcon("OptionPane.questionIcon"));

        int row = 1;
        CellConstraints cc = new CellConstraints();
        builder.add(iconLabel, cc.xywh(1, row, 1, 2));

        JLabel messageLabel = new JLabel(Resources.getString(getClass(), "message-info"));
        Font font = messageLabel.getFont();
        font = font.deriveFont(16.0f);
        messageLabel.setFont(font);
        messageLabel.setForeground(Color.RED);

        builder.add(messageLabel, cc.xyw(3, row, 3));
        row += 2;

        JLabel messageInfoLabel = new JLabel(Resources.getString(getClass(), "message"));
        builder.add(messageInfoLabel, cc.xyw(3, row, 3));
        row += 2;

        textPortCount = new JTextField();
        textPortCount.setDocument(new InputValidationDocument(2, InputValidationDocument.NUMERIC));
        builder.add(new JLabel(Resources.getString(getClass(), "portsCount")), cc.xy(3, row));
        builder.add(textPortCount, cc.xy(5, row));
        row += 2;

        // buttons
        JButton continueButton = new JButton(Resources.getString(getClass(), "continue"));

        continueButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                fireContinue();
            }
        });

        JButton cancel = new JButton(Resources.getString(getClass(), "cancel"));

        cancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                fireCancel();
            }
        });

        JPanel buttons = new ButtonBarBuilder().addGlue().addButton(continueButton, cancel).build();

        builder.add(buttons, cc.xyw(3, row, 3));
        getContentPane().add(builder.build());

        pack();

        setLocationRelativeTo(frame);
        setMinimumSize(getSize());
        setVisible(true);
    }

    private void fireContinue() {
        LOGGER.info("Continue operation. Number of ports to insert: {}", textPortCount.getText());

        if (StringUtils.isNotEmpty(textPortCount.getText())) {

            portsCount = Integer.parseInt(textPortCount.getText());

            result = JOptionPane.YES_OPTION;
        }

        // result = JOptionPane.YES_OPTION;
    }

    private void fireCancel() {

    }

    public int getResult() {
        return result;
    }

    public int getPortsCount() {
        return portsCount;
    }

}
