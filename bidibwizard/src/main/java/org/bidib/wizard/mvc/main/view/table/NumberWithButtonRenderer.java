package org.bidib.wizard.mvc.main.view.table;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.TableCellRenderer;

import org.bidib.wizard.utils.InputValidationDocument;

public class NumberWithButtonRenderer implements TableCellRenderer {
    private final JPanel panel = new JPanel();

    private final JTextField textField = new JTextField();

    private final JButton button;

    public NumberWithButtonRenderer(String buttonText) {
        textField.setHorizontalAlignment(SwingConstants.RIGHT);

        InputValidationDocument numericDocument = new InputValidationDocument(4, InputValidationDocument.NUMERIC);
        textField.setDocument(numericDocument);
        textField.setColumns(4);

        button = new JButton(buttonText);
        panel.setLayout(new GridBagLayout());

        GridBagConstraints c = new GridBagConstraints();

        c.anchor = GridBagConstraints.FIRST_LINE_START;
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0;
        c.insets = new Insets(0, 0, 2, 2);
        c.weightx = 1;
        panel.add(textField, c);
        c.anchor = GridBagConstraints.FIRST_LINE_END;
        c.gridx++;
        c.weightx = 0;
        panel.add(button, c);
    }

    public Component getTableCellRendererComponent(
        JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        if (isSelected) {
            panel.setForeground(table.getSelectionForeground());
            panel.setBackground(table.getSelectionBackground());
        }
        else {
            panel.setForeground(table.getForeground());
            panel.setBackground(table.getBackground());
        }
        textField.setText(value != null ? value.toString() : "");
        return panel;
    }
}
