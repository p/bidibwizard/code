package org.bidib.wizard.mvc.main.view.cvdef;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.bidib.jbidibc.exchange.vendorcv.NodeType;
import org.bidib.jbidibc.exchange.vendorcv.NodetextType;
import org.bidib.jbidibc.exchange.vendorcv.VendorCVUtils;
import org.bidib.wizard.utils.XmlLocaleUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LongNodeNode extends NodeNode {
    private static final Logger LOGGER = LoggerFactory.getLogger(LongNodeNode.class);

    public static final int COLUMN_DESCRIPTION = 0;

    public static final int COLUMN_NUMBER = 1;

    public static final int COLUMN_VALUE = 2;

    public static final int COLUMN_NEW_VALUE = 3;

    private LongCvNode subCvMasterNode;

    public LongNodeNode(NodeType node, int index, String lang) {
        super(node, index, lang);
    }

    public void setSubCvMasterNode(LongCvNode subCvMasterNode) {
        this.subCvMasterNode = subCvMasterNode;
    }

    public LongCvNode getSubCvMasterNode() {
        return subCvMasterNode;
    }

    @Override
    public Object getValueAt(int column) {
        NodeType nodeType = (NodeType) getNode();
        LOGGER.trace("Get value for column: {}", column);
        switch (column) {
            case COLUMN_DESCRIPTION:
                // we must check the language ...
                NodetextType nodetext = VendorCVUtils.getNodetextOfLanguage(nodeType, lang);

                if (nodetext != null) {
                    String value = nodetext.getText();
                    String replacementD = Integer.toString(index);
                    value = value.replaceAll("%%d", replacementD);
                    String replacementP = Integer.toString(index + 1);
                    value = value.replaceAll("%%p", replacementP);
                    LOGGER.trace("Prepared value: {}", value);
                    return value;
                }
                break;
            case COLUMN_NUMBER:
                if (subCvMasterNode != null) {
                    int masterCvNumber = Integer.parseInt(subCvMasterNode.getCV().getNumber());
                    int lastSlaveCvNumber =
                        Integer.parseInt(subCvMasterNode.getSlaveNodes().get(2).getCV().getNumber());
                    return String.format("%d-%d", masterCvNumber, lastSlaveCvNumber);
                }
                break;
            case COLUMN_VALUE:
                if (subCvMasterNode != null) {
                    try {
                        String masterCvValue = subCvMasterNode.getConfigVar().getValue();
                        long value = 0;
                        for (int idx = 2; idx > -1; idx--) {
                            LongCvNode slave = subCvMasterNode.getSlaveNodes().get(idx);
                            String slaveValue = slave.getConfigVar().getValue();
                            value = (value << 8) | (Long.parseLong(slaveValue) & 0xFF);
                        }
                        value = (value << 8) | (Long.parseLong(masterCvValue) & 0xFF);
                        return String.format("%d", value);
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Parse long values failed.");
                        return "?";
                    }
                }
                break;
            case COLUMN_NEW_VALUE:
                if (subCvMasterNode != null) {
                    try {
                        Integer masterCvValue = (Integer) subCvMasterNode.getNewValue();
                        if (masterCvValue != null) {
                            long value = 0;
                            for (int idx = 2; idx > -1; idx--) {
                                LongCvNode slave = subCvMasterNode.getSlaveNodes().get(idx);
                                Integer slaveValue = (Integer) slave.getNewValue();
                                value = (value << 8) | (slaveValue & 0xFF);
                            }

                            value = (value << 8) | (masterCvValue & 0xFF);
                            return String.format("%d", value);
                        }
                        return "-";
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Process new long values failed.");
                        return "-";
                    }
                }
                break;
            case COLUMN_KEY:
                NodetextType nodetextDef =
                    VendorCVUtils.getNodetextOfLanguage(nodeType, XmlLocaleUtils.DEFAULT_LOCALE_VENDOR_CV);
                if (nodetextDef != null) {
                    String value = nodetextDef.getText();
                    return value;
                }
                break;
            default:
                break;
        }

        return null;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
