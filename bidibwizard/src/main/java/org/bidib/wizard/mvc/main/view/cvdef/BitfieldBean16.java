package org.bidib.wizard.mvc.main.view.cvdef;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.beans.Model;

public class BitfieldBean16 extends Model {
    private static final Logger LOGGER = LoggerFactory.getLogger(BitfieldBean16.class);

    private static final long serialVersionUID = 1L;

    public final static String BIT_PROPERTY = "bit";

    public final static String BIT0_PROPERTY = "bit0";

    public final static String BIT1_PROPERTY = "bit1";

    public final static String BIT2_PROPERTY = "bit2";

    public final static String BIT3_PROPERTY = "bit3";

    public final static String BIT4_PROPERTY = "bit4";

    public final static String BIT5_PROPERTY = "bit5";

    public final static String BIT6_PROPERTY = "bit6";

    public final static String BIT7_PROPERTY = "bit7";

    private Integer bitfield = new Integer((byte) 0);

    private Integer radioBits = new Integer(0);

    public Integer getBitfield() {
        return bitfield;
    }

    public boolean getBit(int index) {
        index++;
        return (bitfield & index) == index;
    }

    public boolean getBit0() {
        return (bitfield & 0x01) == 0x01;
    }

    public boolean getBit1() {
        return (bitfield & 0x02) == 0x02;
    }

    public boolean getBit2() {
        return (bitfield & 0x04) == 0x04;
    }

    public boolean getBit3() {
        return (bitfield & 0x08) == 0x08;
    }

    public boolean getBit4() {
        return (bitfield & 0x10) == 0x10;
    }

    public boolean getBit5() {
        return (bitfield & 0x20) == 0x20;
    }

    public boolean getBit6() {
        return (bitfield & 0x40) == 0x40;
    }

    public boolean getBit7() {
        return (bitfield & 0x80) == 0x80;
    }

    public boolean getBit8() {
        return isBitSet(8);
    }

    public boolean getBit9() {
        return isBitSet(9);
    }

    public boolean getBit10() {
        return isBitSet(10);
    }

    public boolean getBit11() {
        return isBitSet(11);
    }

    public boolean getBit12() {
        return isBitSet(12);
    }

    public boolean getBit13() {
        return isBitSet(13);
    }

    public boolean getBit14() {
        return isBitSet(14);
    }

    public boolean getBit15() {
        return isBitSet(15);
    }

    private boolean isBitSet(int bitPos) {
        return (bitfield & (1 << bitPos)) == (1 << bitPos);
    }

    public void setBit0(boolean bit) {
        setBit(bit, 0);
    }

    public void setBit1(boolean bit) {
        setBit(bit, 1);
    }

    public void setBit2(boolean bit) {
        setBit(bit, 2);
    }

    public void setBit3(boolean bit) {
        setBit(bit, 3);
    }

    public void setBit4(boolean bit) {
        setBit(bit, 4);
    }

    public void setBit5(boolean bit) {
        setBit(bit, 5);
    }

    public void setBit6(boolean bit) {
        setBit(bit, 6);
    }

    public void setBit7(boolean bit) {
        setBit(bit, 7);
    }

    public void setBit(boolean bit, int bitpos) {
        Integer newValue = null;
        if (bit) {

            // support for Radiobits, if the bit is a radio bit it must be set exclusively in its group
            if (radioBits != null) {
                if ((radioBits.intValue() & (1 << bitpos)) == (1 << bitpos)) {

                    LOGGER.debug("The current bit is a radio bit: {}", bitpos);
                    int clearedRadioBitsValue = (bitfield & ~(radioBits.intValue()));
                    // set the value
                    newValue = (1 << bitpos) | clearedRadioBitsValue;
                }
                else {
                    newValue = (bitfield | (1 << bitpos));
                }
            }
            else {
                newValue = (bitfield | (1 << bitpos));
            }
        }
        else {
            newValue = (bitfield & ~(1 << bitpos));
        }

        setBitfield(newValue.intValue());
    }

    public void setBitfield(Number bitfieldNum) {
        LOGGER.debug("Set the bitfield value: {}", (bitfieldNum != null ? (bitfieldNum.intValue() & 0xFF) : null));
        Integer newValue = null;
        if (bitfieldNum == null) {
            newValue = new Integer(0);
        }
        else {
            newValue = bitfieldNum.intValue();
        }
        // Integer oldValue = this.bitfield;
        this.bitfield = newValue;

        // for (int index = 0; index < 8; index++) {
        // byte pos = (byte) (0x01 << index);
        // Boolean oldBitValue = (oldValue & pos) == pos;
        // Boolean newBitValue = (bitfield & pos) == pos;
        //
        // firePropertyChange(BIT_PROPERTY + index, oldBitValue, newBitValue);
        // }

        fireMultiplePropertiesChanged();
    }

    /**
     * @return the radioBits
     */
    public Integer getRadioBits() {
        return radioBits;
    }

    /**
     * @param radioBits
     *            the radioBits to set
     */
    public void setRadioBits(Integer radioBits) {
        this.radioBits = radioBits;
    }
}