package org.bidib.wizard.mvc.stepcontrol.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.IndexedPropertyChangeEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;

import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.common.view.panel.ColorPane;
import org.bidib.wizard.mvc.main.view.menu.BasicPopupMenu;
import org.bidib.wizard.mvc.stepcontrol.model.CvConsoleModel;
import org.bidib.wizard.mvc.stepcontrol.model.CvConsoleModel.ConsoleLine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vlsolutions.swing.docking.DockKey;
import com.vlsolutions.swing.docking.Dockable;

public class CvConsoleView implements Dockable {

    private static final Logger LOGGER = LoggerFactory.getLogger(CvConsoleView.class);

    private final DockKey DOCKKEY = new DockKey("CvConsoleView");

    private final JPanel contentPanel;

    private final ColorPane coloredTextPane = new ColorPane();

    private final CvConsoleModel consoleModel;

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss.SSS");

    public CvConsoleView() {
        DOCKKEY.setName(Resources.getString(getClass(), "title"));
        DOCKKEY.setFloatEnabled(true);
        DOCKKEY.setAutoHideEnabled(false);

        LOGGER.info("Create new ConsoleView");
        contentPanel = new JPanel();
        contentPanel.setLayout(new BorderLayout());

        coloredTextPane.setEditable(false);

        JScrollPane scrollPane = new JScrollPane(coloredTextPane);
        contentPanel.add(scrollPane);

        consoleModel = CvConsoleModel.getConsoleModel();

        consoleModel.addPropertyChangeListener(CvConsoleModel.PROPERTY_CONSOLE_CONTENT, new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                LOGGER.info("Property was changed: {}", evt);
                if (evt instanceof IndexedPropertyChangeEvent) {
                    IndexedPropertyChangeEvent ipce = (IndexedPropertyChangeEvent) evt;

                    ConsoleLine newLine = (ConsoleLine) ipce.getNewValue();

                    if (newLine != null) {
                        String now = dateFormat.format(new Date());
                        coloredTextPane.append(newLine.getColor(), now + " - " + newLine.getMessage() + "\n");
                    }
                }
            }
        });

        consoleModel.addPropertyChangeListener(CvConsoleModel.PROPERTY_CONSOLE_CONTENT_SIZE,
            new PropertyChangeListener() {
                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    LOGGER.info("Property was changed: {}", evt);

                    if (evt.getNewValue() != null) {
                        Integer value = (Integer) evt.getNewValue();
                        if (value == 0) {
                            // clear the console
                            coloredTextPane.clear();
                        }
                    }
                }
            });

        JPopupMenu popupMenu = new BasicPopupMenu();
        JMenuItem clearConsole = new JMenuItem(Resources.getString(getClass(), "clear_console"));
        clearConsole.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fireClearConsole();
            }
        });
        popupMenu.add(clearConsole);
        coloredTextPane.setComponentPopupMenu(popupMenu);
    }

    @Override
    public Component getComponent() {
        return contentPanel;
    }

    @Override
    public DockKey getDockKey() {
        return DOCKKEY;
    }

    public void close() {
    }

    private void fireClearConsole() {
        LOGGER.info("clear the console.");

        consoleModel.clear();
    }
}
