package org.bidib.wizard.mvc.main.model.function;

import org.bidib.jbidibc.exchange.lcmacro.FlagActionType;
import org.bidib.jbidibc.exchange.lcmacro.FlagOperationType;
import org.bidib.jbidibc.exchange.lcmacro.FlagPoint;
import org.bidib.jbidibc.exchange.lcmacro.LcMacroPointType;
import org.bidib.wizard.comm.FlagStatus;
import org.bidib.wizard.mvc.main.model.Flag;

public class FlagFunction extends SystemFunction<FlagStatus> {
    private Flag flag;

    public FlagFunction() {
        this(FlagStatus.CLEAR);
    }

    public FlagFunction(FlagStatus action) {
        this(action, null);
    }

    public FlagFunction(FlagStatus action, Flag flag) {
        super(action, KEY_FLAG);
        this.flag = flag;
    }

    public Flag getFlag() {
        return flag;
    }

    public void setFlag(Flag flag) {
        this.flag = flag;
    }

    public String getDebugString() {
        int id = 0;

        if (getFlag() != null) {
            id = getFlag().getId();
        }
        return "Flag" + getAction().name().substring(0, 1) + getAction().name().substring(1).toLowerCase() + id;
    }

    @Override
    public LcMacroPointType toLcMacroPoint() {
        FlagPoint flagPoint = new FlagPoint();
        FlagActionType flagActionType = new FlagActionType();
        flagActionType.setOperation(FlagOperationType.fromValue(getAction().getType().name()));
        flagActionType.setFlagNumber(getFlag().getId());
        flagPoint.setFlagActionType(flagActionType);
        return flagPoint;
    }
}
