package org.bidib.wizard.mvc.postprocess.dccdevices;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.UnmarshallerHandler;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.apache.commons.collections4.CollectionUtils;
import org.bidib.wizard.dccdevices.DecoderType;
import org.bidib.wizard.dccdevices.UserDevice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.InputSource;
import org.xml.sax.XMLFilter;
import org.xml.sax.XMLReader;

public class MonitorDccDevicesReader {

    private static final Logger LOGGER = LoggerFactory.getLogger(MonitorDccDevicesReader.class);

    private static final String JAXB_PACKAGE = "org.bidib.wizard.dccdevices";

    public static final String XSD_LOCATION = "/xsd/dcc-devices.xsd";

    public List<DecoderType> getLocoDecoders() {

        List<DecoderType> decoders = null;

        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(JAXB_PACKAGE);

            // Create the XMLFilter
            XMLFilter filter = new NamespaceFilter();

            // Set the parent XMLReader on the XMLFilter
            SAXParserFactory spf = SAXParserFactory.newInstance();
            SAXParser sp = spf.newSAXParser();
            XMLReader xr = sp.getXMLReader();
            filter.setParent(xr);

            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

            // Set UnmarshallerHandler as ContentHandler on XMLFilter
            UnmarshallerHandler unmarshallerHandler = unmarshaller.getUnmarshallerHandler();
            filter.setContentHandler(unmarshallerHandler);

            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            StreamSource streamSource =
                new StreamSource(MonitorDccDevicesReader.class.getResourceAsStream(XSD_LOCATION));
            Schema schema = schemaFactory.newSchema(streamSource);
            unmarshaller.setSchema(schema);

            String userHome = System.getProperty("user.home");
            LOGGER.info("userHome: {}", userHome);
            File devices = new File(userHome, "Documents/BiDiB-Monitor/DccDevices.bidib");

            FileInputStream is = new FileInputStream(devices);
            InputSource xml = new InputSource(is);

            filter.parse(xml);
            UserDevice userDevice = (UserDevice) unmarshallerHandler.getResult();

            LOGGER.info("unmarshalled userDevice: {}", userDevice);

            // get only loco decoders
            if (CollectionUtils.isNotEmpty(userDevice.getDecoder())) {
                decoders = new LinkedList<DecoderType>();
                for (DecoderType decoder : userDevice.getDecoder()) {
                    if ("Loco".equals(decoder.getDecoderType())) {
                        decoders.add(decoder);
                    }
                }
            }

        }
        catch (FileNotFoundException ex) {
            LOGGER.info("Read DccDevices.bidib failed, the file DccDevices.bidib was not found.");
        }
        catch (Exception ex) {
            LOGGER.warn("Read DccDevices.bidib failed.", ex);
        }

        return decoders;
    }
}
