package org.bidib.wizard.mvc.booster.model;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.apache.commons.lang.StringUtils;
import org.bidib.jbidibc.core.StringData;
import org.bidib.jbidibc.core.enumeration.BoosterState;
import org.bidib.jbidibc.core.enumeration.CommandStationState;
import org.bidib.wizard.mvc.main.model.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.beans.Model;

public class BoosterModel extends Model {
    private static final Logger LOGGER = LoggerFactory.getLogger(BoosterModel.class);

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_BOOSTER_STATUS = "boosterStatus";

    public static final String PROPERTY_COMMANDSTATION_STATUS = "commandStationState";

    public static final String PROPERTY_BOOSTER_CURRENT = "boosterCurrent";

    public static final String PROPERTY_BOOSTER_MAX_CURRENT = "boosterMaxCurrent";

    public static final String PROPERTY_BOOSTER_VOLTAGE = "boosterVoltage";

    public static final String PROPERTY_BOOSTER_TEMPERATURE = "boosterTemperature";

    public static final String PROPERTY_BOOSTER_LABEL = "nodeLabel";

    private final Node booster;

    private BoosterState state;

    private CommandStationState commandStationState;

    private int maxCurrent;

    private int current;

    private int voltage;

    private int temperature;

    private String nodeLabel;

    private long lastCurrentUpdate;

    private PropertyChangeListener propertyChangeListener;

    private final PropertyChangeListener boosterListener;

    public BoosterModel(final Node booster, final PropertyChangeListener boosterListener) {
        this.booster = booster;
        this.boosterListener = boosterListener;

        LOGGER.info("Create new BoosterModel for booster: {}", booster);
    }

    public void registerNode() {
        propertyChangeListener = new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                // update the label with the name ...
                LOGGER.info("Property has changed, name: {}, value: {}", evt.getPropertyName(), evt.getNewValue());

                String nodeLabel = prepareNodeLabel();
                setNodeLabel(nodeLabel);

                final PropertyChangeEvent evt1 =
                    new PropertyChangeEvent(BoosterModel.this, PROPERTY_BOOSTER_LABEL, null, nodeLabel);
                boosterListener.propertyChange(evt1);
            }
        };
        this.booster.addPropertyChangeListener(propertyChangeListener);
    }

    public void freeNode() {
        this.booster.removePropertyChangeListener(propertyChangeListener);
    }

    /**
     * @return the booster node
     */
    public Node getBooster() {
        return booster;
    }

    /**
     * @return the node address
     */
    public byte[] getNodeAddress() {
        return booster.getNode().getAddr();
    }

    /**
     * @param nodeLabel
     *            the node label to set
     */
    public void setNodeLabel(String nodeLabel) {
        String oldValue = this.nodeLabel;

        this.nodeLabel = nodeLabel;

        firePropertyChange(PROPERTY_BOOSTER_LABEL, oldValue, this.nodeLabel);
    }

    /**
     * @return the node label
     */
    public String getNodeLabel() {
        return nodeLabel;
    }

    /**
     * @return the state
     */
    public BoosterState getState() {
        return state;
    }

    /**
     * @param state
     *            the state to set
     */
    public void setState(BoosterState state) {
        BoosterState oldValue = this.state;
        this.state = state;

        firePropertyChange(PROPERTY_BOOSTER_STATUS, oldValue, this.state);
    }

    /**
     * @return the commandStationState
     */
    public CommandStationState getCommandStationState() {
        return commandStationState;
    }

    /**
     * @param commandStationState
     *            the commandStationState to set
     */
    public void setCommandStationState(CommandStationState commandStationState) {
        CommandStationState oldValue = this.commandStationState;

        this.commandStationState = commandStationState;
        firePropertyChange(PROPERTY_COMMANDSTATION_STATUS, oldValue, this.commandStationState);

        // if (CommandStationState.GO == commandStationState) {
        // LOGGER.info("Must start the watchdog timer to send MSG_CS_SET_STATE(GO) continuously for node: {}",
        // booster);
        // }
    }

    /**
     * @return the maxCurrent
     */
    public int getMaxCurrent() {
        return maxCurrent;
    }

    /**
     * @param maxCurrent
     *            the maxCurrent to set
     */
    public void setMaxCurrent(int maxCurrent) {
        int oldValue = this.maxCurrent;

        // do not convert value
        this.maxCurrent = maxCurrent;

        firePropertyChange(PROPERTY_BOOSTER_MAX_CURRENT, oldValue, this.maxCurrent);
    }

    /**
     * @return the current
     */
    public int getCurrent() {
        return current;
    }

    /**
     * @param current
     *            the current to set
     */
    public void setCurrent(int current) {
        int oldValue = this.current;

        this.current = current;

        // set the last current update time
        setLastCurrentUpdate(System.currentTimeMillis());

        firePropertyChange(PROPERTY_BOOSTER_CURRENT, oldValue, this.current);
    }

    /**
     * @return the lastCurrentUpdate
     */
    public long getLastCurrentUpdate() {
        return lastCurrentUpdate;
    }

    /**
     * @param lastCurrentUpdate
     *            the lastCurrentUpdate to set
     */
    public void setLastCurrentUpdate(long lastCurrentUpdate) {
        this.lastCurrentUpdate = lastCurrentUpdate;
    }

    /**
     * @return the voltage
     */
    public int getVoltage() {
        return voltage;
    }

    /**
     * @param voltage
     *            the voltage to set
     */
    public void setVoltage(int voltage) {
        int oldValue = this.voltage;

        this.voltage = voltage;

        firePropertyChange(PROPERTY_BOOSTER_VOLTAGE, oldValue, this.voltage);
    }

    /**
     * @return the temperature
     */
    public int getTemperature() {
        return temperature;
    }

    /**
     * @param temperature
     *            the temperature to set
     */
    public void setTemperature(int temperature) {
        int oldValue = this.temperature;

        this.temperature = temperature;

        firePropertyChange(PROPERTY_BOOSTER_TEMPERATURE, oldValue, this.temperature);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof BoosterModel) {
            BoosterModel other = (BoosterModel) obj;
            if (booster.equals(other.getBooster())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        return booster.hashCode();
    }

    public String prepareNodeLabel() {
        String nodeLabel = booster.getLabel();
        if (StringUtils.isBlank(nodeLabel)) {
            // try to get the product name
            String productString = booster.getNode().getStoredString(StringData.INDEX_PRODUCTNAME);
            if (StringUtils.isNotBlank(productString)) {
                nodeLabel = productString;
            }
        }
        return nodeLabel;
    }
}
