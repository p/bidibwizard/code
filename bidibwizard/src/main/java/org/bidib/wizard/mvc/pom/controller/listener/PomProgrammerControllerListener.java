package org.bidib.wizard.mvc.pom.controller.listener;

import org.bidib.jbidibc.core.AddressData;
import org.bidib.jbidibc.core.enumeration.CommandStationPom;
import org.bidib.wizard.mvc.main.model.Node;

public interface PomProgrammerControllerListener {
    void close();

    void sendRequest(Node node, AddressData locoAddress, CommandStationPom opCode, int cvNumber, int cvValue);
}
