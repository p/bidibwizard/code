package org.bidib.wizard.mvc.cvprogrammer.view;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import org.bidib.jbidibc.core.enumeration.PomAddressing;
import org.bidib.jbidibc.core.enumeration.PomDecoder;
import org.bidib.jbidibc.core.enumeration.PomOperation;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.common.view.converter.StringConverter;
import org.bidib.wizard.mvc.cvprogrammer.model.CvProgrammerModel;
import org.bidib.wizard.mvc.cvprogrammer.view.listener.CvProgrammerViewListener;
import org.bidib.wizard.mvc.cvprogrammer.view.panel.CvReadPanel;
import org.bidib.wizard.mvc.cvprogrammer.view.panel.CvWritePanel;
import org.bidib.wizard.mvc.cvprogrammer.view.panel.listener.CvRequestListener;
import org.bidib.wizard.utils.InputValidationDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.beans.Model;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.binding.value.ConverterValueModel;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.common.collect.ArrayListModel;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.debug.FormDebugPanel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

public class CvProgrammerView extends JDialog {
    private static final Logger LOGGER = LoggerFactory.getLogger(CvProgrammerView.class);

    private static final long serialVersionUID = 1L;

    private final Collection<CvProgrammerViewListener> listeners = new LinkedList<CvProgrammerViewListener>();

    private final CvProgrammerModel cvProgrammerModel;

    private final CvReadPanel cvReadPanel;

    private final CvWritePanel cvWritePanel;

    private final JButton closeButton = new JButton(Resources.getString(getClass(), "close"));

    private final CvModel cvModel;

    public class CvModel extends Model {
        private static final long serialVersionUID = 1L;

        public static final String PROPERTYNAME_ACTION = "action";

        public static final String PROPERTYNAME_ADDRESS = "address";

        public static final String PROPERTYNAME_OPERATION = "operation";

        public static final String PROPERTYNAME_ADDRESSING = "addressing";

        private PomDecoder action = PomDecoder.LOCO;

        private Integer address;

        private PomOperation operation = PomOperation.RD_BYTE;

        private PomAddressing addressing = PomAddressing.POM;

        public CvModel() {

        }

        public void setAction(PomDecoder action) {
            PomDecoder oldAction = getAction();
            this.action = action;
            firePropertyChange(PROPERTYNAME_ACTION, oldAction, action);
        }

        public PomDecoder getAction() {
            return action;
        }

        /**
         * @return the address
         */
        public Integer getAddress() {
            return address;
        }

        /**
         * @param address
         *            the address to set
         */
        public void setAddress(Integer address) {
            Integer oldAddress = getAddress();
            this.address = address;
            LOGGER.info("Address changed, new: {}, old: {}", address, oldAddress);
            firePropertyChange(PROPERTYNAME_ADDRESS, oldAddress, address);
        }

        /**
         * @return the operation
         */
        public PomOperation getOperation() {
            return operation;
        }

        /**
         * @param operation
         *            the operation to set
         */
        public void setOperation(PomOperation operation) {
            PomOperation oldOperation = getOperation();
            this.operation = operation;
            firePropertyChange(PROPERTYNAME_OPERATION, oldOperation, operation);
        }

        /**
         * @return the addressing
         */
        public PomAddressing getAddressing() {
            return addressing;
        }

        /**
         * @param addressing
         *            the addressing to set
         */
        public void setAddressing(PomAddressing addressing) {
            PomAddressing oldAddressing = getAddressing();
            this.addressing = addressing;
            firePropertyChange(PROPERTYNAME_ADDRESSING, oldAddressing, addressing);
        }
    }

    private class PomDecoderCellRenderer extends DefaultListCellRenderer {
        private static final long serialVersionUID = 1L;

        @Override
        public Component getListCellRendererComponent(
            JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {

            if (value instanceof PomDecoder) {
                String key = ((PomDecoder) value).getKey();
                value = Resources.getString(PomDecoder.class, key);
            }
            return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        }
    }

    public CvProgrammerView(JFrame parent, final CvProgrammerModel cvProgrammerModel, int x, int y) {
        super(parent);
        this.cvProgrammerModel = cvProgrammerModel;
        cvModel = new CvModel();

        DefaultFormBuilder builder = null;
        boolean debug = false;
        if (debug) {
            JPanel panel = new FormDebugPanel();
            builder = new DefaultFormBuilder(new FormLayout("pref, 3dlu, fill:50dlu:grow"), panel);
        }
        else {
            builder = new DefaultFormBuilder(new FormLayout("pref, 3dlu, fill:50dlu:grow"));
        }
        builder.border(Borders.DIALOG);

        ArrayListModel<PomDecoder> actionListModel = new ArrayListModel<PomDecoder>(Arrays.asList(PomDecoder.values()));
        ValueModel actionModel = new PropertyAdapter<CvModel>(cvModel, CvModel.PROPERTYNAME_ACTION, true);
        SelectionInList<CvModel> silAction = new SelectionInList<CvModel>(actionListModel, actionModel);

        // use the list renderer that shows the name
        builder.append(new JLabel(Resources.getString(getClass(), "action")),
            BasicComponentFactory.createComboBox(silAction, new PomDecoderCellRenderer()));

        ValueModel addressModel = new PropertyAdapter<CvModel>(cvModel, CvModel.PROPERTYNAME_ADDRESS, true);
        ConverterValueModel addressConverterModel =
            new ConverterValueModel(addressModel, new StringConverter(new DecimalFormat("#")));

        JTextField textAddress = BasicComponentFactory.createTextField(addressConverterModel, false);
        textAddress.setDocument(new InputValidationDocument(InputValidationDocument.NUMERIC));
        builder.append(new JLabel(Resources.getString(getClass(), "address")), textAddress);

        FormLayout layout =
            new FormLayout("pref, 3dlu, p, 6dlu, p, 6dlu, p, 0:grow", "p, 3dlu, p, 3dlu, p, 3dlu, p, 3dlu, p, 3dlu, p");

        PanelBuilder pbuilder = new PanelBuilder(layout);
        CellConstraints cc = new CellConstraints();
        pbuilder.addSeparator(Resources.getString(getClass(), "operation"), cc.xyw(1, 1, 8));
        ValueModel operationModel = new PropertyAdapter<CvModel>(cvModel, CvModel.PROPERTYNAME_OPERATION, true);
        int index = 1;
        for (PomOperation operation : PomOperation.values()) {

            JRadioButton radio =
                BasicComponentFactory.createRadioButton(operationModel, operation,
                    Resources.getString(PomOperation.class, operation.getKey()));

            // TODO remove after support added
            switch (operation) {
                case RD_BLOCK:
                    radio.setEnabled(false);
                    // break;
                    // don't add the read block radio
                    continue;
                case WR_BIT:
                    // radio.setEnabled(false);
                    break;
                default:
                    break;
            }

            // add radio
            pbuilder.add(radio, cc.xy(index, 3));
            index += 2;
        }

        pbuilder.addSeparator(Resources.getString(getClass(), "addressType"), cc.xyw(1, 5, 8));
        ValueModel addressingModel = new PropertyAdapter<CvModel>(cvModel, CvModel.PROPERTYNAME_ADDRESSING, true);
        index = 1;
        for (PomAddressing addressing : PomAddressing.values()) {
            JRadioButton radio =
                BasicComponentFactory.createRadioButton(addressingModel, addressing,
                    Resources.getString(PomAddressing.class, addressing.getKey()));
            pbuilder.add(radio, cc.xyw(index, 7, (index == 1 ? 1 : 3)));

            // TODO remove after support added
            switch (addressing) {
                case XPOM:
                    radio.setEnabled(false);
                    break;
                default:
                    break;
            }
            index += 2;
        }

        builder.append(pbuilder.getPanel(), 3);

        // add bindings for enable/disable the textfield
        PropertyConnector.connect(cvModel, CvModel.PROPERTYNAME_ADDRESS, cvProgrammerModel, "decoderAddress");

        cvReadPanel = new CvReadPanel(cvProgrammerModel);
        cvWritePanel = new CvWritePanel(cvProgrammerModel, cvModel);

        setResizable(true);
        setTitle(Resources.getString(getClass(), "title"));

        builder.append(cvReadPanel, 3);
        builder.append(cvWritePanel, 3);

        cvWritePanel.addCvRequestListener(new CvRequestListener() {
            @Override
            public void send(int cvValue) {
                fireSend(cvValue);
            }
        });

        getContentPane().add(builder.build());

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                close();
            }
        });
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

        pack();
        setMinimumSize(getSize());
        setSize(new Dimension(getSize().width + 10, getSize().height));

        int labelWidth = Math.max(cvReadPanel.getLabelWidth(), cvWritePanel.getLabelWidth());

        cvReadPanel.setLabelWidth(labelWidth);
        cvWritePanel.setLabelWidth(labelWidth);

        int buttonWidth =
            Math.max(/* Math.max(cvReadPanel.getButtonWidth(), */cvWritePanel.getButtonWidth()/* ) */,
                closeButton.getWidth());

        cvWritePanel.setButtonWidth(buttonWidth);
        closeButton.setPreferredSize(new Dimension(buttonWidth, closeButton.getSize().height));

        setLocation(x, y);

        setVisible(true);
    }

    public void addCvProgrammerViewListener(CvProgrammerViewListener l) {
        listeners.add(l);
    }

    private void close() {
        setVisible(false);
        fireClose();
    }

    private void fireClose() {
        for (CvProgrammerViewListener l : listeners) {
            l.close();
        }
    }

    private void fireSend(int cvValue) {

        PomDecoder action = cvModel.getAction();
        int address = cvModel.getAddress().intValue();
        PomOperation operation = cvModel.getOperation();
        int cvNumber = cvProgrammerModel.getNumber();
        PomAddressing addressing = cvModel.getAddressing();

        for (CvProgrammerViewListener l : listeners) {
            l.sendRequest(action, address, operation, addressing, cvNumber, cvValue);
        }
    }
}
