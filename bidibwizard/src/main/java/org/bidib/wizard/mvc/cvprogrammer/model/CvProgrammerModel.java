package org.bidib.wizard.mvc.cvprogrammer.model;

import java.util.Collection;
import java.util.LinkedList;

import org.bidib.jbidibc.core.AddressData;
import org.bidib.jbidibc.core.enumeration.AddressTypeEnum;
import org.bidib.wizard.mvc.cvprogrammer.model.listener.ConfigVariableListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CvProgrammerModel {
    private static final Logger LOGGER = LoggerFactory.getLogger(CvProgrammerModel.class);

    private final Collection<ConfigVariableListener> listeners = new LinkedList<ConfigVariableListener>();

    private AddressData selectedDecoderAddress;

    private int number = 1;

    private int value;

    public void addConfigVariableListener(ConfigVariableListener l) {
        listeners.add(l);
    }

    public void removeConfigVariableListener(ConfigVariableListener l) {
        listeners.remove(l);
    }

    /**
     * @return the selectedDecoderAddress
     */
    public AddressData getSelectedDecoderAddress() {
        return selectedDecoderAddress;
    }

    /**
     * @param selectedDecoderAddress
     *            the selectedDecoderAddress to set
     */
    public void setSelectedDecoderAddress(AddressData selectedDecoderAddress) {
        LOGGER.info("Set the selected decoder address: {}", selectedDecoderAddress);
        this.selectedDecoderAddress = selectedDecoderAddress;
        fireSelectedDecoderAddressChanged(selectedDecoderAddress);
    }

    /**
     * @param selectedDecoderAddress
     *            the selectedDecoderAddress to set
     */
    public void setDecoderAddress(Integer selectedDecoderAddress) {
        LOGGER.info("Set the selected decoder address: {}", selectedDecoderAddress);
        if (selectedDecoderAddress != null) {
            this.selectedDecoderAddress =
                new AddressData(selectedDecoderAddress.intValue(), AddressTypeEnum.LOCOMOTIVE_FORWARD);
        }
        else {
            this.selectedDecoderAddress = null;
        }
        fireSelectedDecoderAddressChanged(this.selectedDecoderAddress);
    }

    public Integer getDecoderAddress() {
        if (selectedDecoderAddress != null) {
            return selectedDecoderAddress.getAddress();
        }
        return null;
    }

    /**
     * @return the CV number
     */
    public int getNumber() {
        return number;
    }

    /**
     * @param number
     *            the CV number to set
     */
    public void setNumber(int number) {
        if (this.number != number) {
            this.number = number;
            fireNumberChanged(number);
        }
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        if (this.value != value) {
            this.value = value;
            fireValueChanged(value);
        }
    }

    private void fireNumberChanged(int number) {
        for (ConfigVariableListener l : listeners) {
            l.numberChanged(number);
        }
    }

    private void fireValueChanged(int value) {
        for (ConfigVariableListener l : listeners) {
            l.valueChanged(value);
        }
    }

    private void fireSelectedDecoderAddressChanged(AddressData selectedDecoderAddress) {

        for (ConfigVariableListener l : listeners) {
            l.selectedDecoderAddressChanged(selectedDecoderAddress);
        }
    }
}
