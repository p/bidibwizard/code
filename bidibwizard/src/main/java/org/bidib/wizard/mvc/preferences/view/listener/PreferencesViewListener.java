package org.bidib.wizard.mvc.preferences.view.listener;

import java.util.Date;

import org.bidib.wizard.mvc.common.model.PreferencesPortType;

public interface PreferencesViewListener {
    /**
     * The port type has changed.
     * 
     * @param portType
     *            the new port type value
     */
    void selectedPortTypeChanged(PreferencesPortType portType);

    /**
     * The serial symLink value has changed.
     * 
     * @param serialSymLink
     *            the new serialSymLink value
     */
    void selectedSerialSymLinkChanged(String serialSymLink);

    /**
     * The COM port value has changed.
     * 
     * @param comPort
     *            the new comPort value
     */
    void selectedComPortChanged(String comPort);

    /**
     * The UDP host value has changed.
     * 
     * @param udpHost
     *            the new udpHost value
     */
    void selectedUdpHostChanged(String udpHost);

    /**
     * The TCP host value has changed.
     * 
     * @param tcpHost
     *            the new tcpHost value
     */
    void selectedTcpHostChanged(String tcpHost);

    /**
     * The serialEnabled selection has changed.
     * 
     * @param serialEnabled
     *            the new serialEnabled value
     */
    void serialEnabledChanged(boolean serialEnabled);

    /**
     * The udpEnabled selection has changed.
     * 
     * @param udpEnabled
     *            the new udpEnabled value
     */
    void udpEnabledChanged(boolean udpEnabled);

    /**
     * The tcpEnabled selection has changed.
     * 
     * @param tcpEnabled
     *            the new tcpEnabled value
     */
    void tcpEnabledChanged(boolean tcpEnabled);

    /**
     * The plainTcpEnabled selection has changed.
     * 
     * @param plainTcpEnabled
     *            the new plainTcpEnabled value
     */
    void plainTcpEnabledChanged(boolean plainTcpEnabled);

    /**
     * User pressed save.
     */
    void save();

    /**
     * User pressed cancel.
     */
    void cancel();

    /**
     * The start time has changed.
     * 
     * @param startTime
     *            the new start time value
     */
    void startTimeChanged(Date startTime);

    /**
     * The time factor has changed.
     * 
     * @param timeFactor
     *            the new time factor value
     */
    void timeFactorChanged(int timeFactor);

    /**
     * The powerUser has changed.
     * 
     * @param powerUser
     *            the new powerUser value
     */
    void powerUserChanged(boolean powerUser);

    /**
     * The showBoosterTable has changed.
     * 
     * @param showBoosterTable
     *            the new showBoosterTable value
     */
    void showBoosterTableChanged(boolean showBoosterTable);

    /**
     * The alwaysShowProductNameInTree has changed.
     * 
     * @param alwaysShowProductNameInTree
     *            the new alwaysShowProductNameInTree value
     */
    void alwaysShowProductNameInTreeChanged(boolean alwaysShowProductNameInTree);

    /**
     * The ignoreWaitTimeout has changed.
     * 
     * @param ignoreWaitTimeout
     *            the new ignoreWaitTimeout value
     */
    void ignoreWaitTimeoutChanged(boolean ignoreWaitTimeout);

    /**
     * The ignoreWrongReceiveMessageNumber has changed.
     * 
     * @param ignoreWrongReceiveMessageNumber
     *            the new ignoreWrongReceiveMessageNumber value
     */
    void ignoreWrongReceiveMessageNumberChanged(boolean ignoreWrongReceiveMessageNumber);

    /**
     * The ignoreFlowControl has changed.
     * 
     * @param ignoreFlowControl
     *            the new ignoreFlowControl value
     */
    @Deprecated
    void ignoreFlowControlChanged(boolean ignoreFlowControl);

    /**
     * The useHotPlugController has changed.
     * 
     * @param useHotPlugController
     *            the new useHotPlugController value
     */
    void useHotPlugControllerChanged(boolean useHotPlugController);

    /**
     * The serialUseHardwareFlowControl flag has changed.
     * 
     * @param serialUseHardwareFlowControl
     *            the new serialUseHardwareFlowControl value
     */
    void serialUseHardwareFlowControlChanged(boolean serialUseHardwareFlowControl);

    /**
     * The logFileAppend flag has changed.
     * 
     * @param logFileAppend
     *            the new logFileAppend value
     */
    void logFileAppendChanged(boolean logFileAppend);

    /**
     * The property has changed.
     * 
     * @param propertyName
     *            the name of the property
     * @param value
     *            the new value
     */
    void propertyChanged(String propertyName, Object value);
}
