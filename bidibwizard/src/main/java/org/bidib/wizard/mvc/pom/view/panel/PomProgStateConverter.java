package org.bidib.wizard.mvc.pom.view.panel;

import org.bidib.jbidibc.core.enumeration.PomProgState;

import com.jgoodies.binding.value.BindingConverter;

/**
 * Converts Values to Strings and vice-versa using a given Format.
 */
public final class PomProgStateConverter implements BindingConverter<PomProgState, String> {

    /**
     * Formats the source value and returns a String representation.
     * 
     * @param sourceValue
     *            the source value
     * @return the formatted sourceValue
     */
    @Override
    public String targetValue(PomProgState sourceValue) {
        if (sourceValue != null) {
            return sourceValue.name();
        }
        return null;
    }

    /**
     * Parses the given String encoding and sets it as the subject's new value. Silently catches {@code ParseException}.
     * 
     * @param targetValue
     *            the value to be converted and set as new subject value
     */
    @Override
    public PomProgState sourceValue(String targetValue) {
        return null;
    }
}
