package org.bidib.wizard.mvc.main.view.component;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

import javax.swing.JPanel;

import org.bidib.wizard.comm.Communication;
import org.bidib.wizard.comm.CommunicationFactory;
import org.bidib.wizard.comm.listener.CommunicationListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.hansolo.steelseries.extras.Clock;
import eu.hansolo.steelseries.tools.BackgroundColor;
import eu.hansolo.steelseries.tools.PointerType;

public class AnalogClock extends JPanel implements CommunicationListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(AnalogClock.class);

    private static final long serialVersionUID = 1L;

    private final Clock clock = new Clock();

    private Communication communication;

    private final Calendar startTime = Calendar.getInstance();

    private long timeOffset = 0;

    private int timeFactor = 0;

    private boolean stopped = false;

    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    private ScheduledFuture<?> modelClockTimer;

    // helper object to lock the communication access
    private Object commLock = new Object();

    public AnalogClock(Date startTime, int timeFactor) {
        setLayout(new GridBagLayout());
        clock.setBackgroundColor(BackgroundColor.WHITE);
        clock.setPointerType(PointerType.TYPE2);

        GridBagConstraints c = new GridBagConstraints();

        c.anchor = GridBagConstraints.CENTER;
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 1;
        c.weighty = 1;
        add(clock, c);
        setStartTime(startTime);
        setTimeFactor(timeFactor);

        CommunicationFactory.addCommunicationListener(this);
    }

    private Calendar getStartTime() {
        return startTime;
    }

    private Calendar getTime() {
        Calendar result = (Calendar) startTime.clone();
        long diff = System.currentTimeMillis() - result.getTime().getTime() - timeOffset;

        result.add(Calendar.MILLISECOND, (int) diff * timeFactor);
        return result;
    }

    private int getTimeFactor() {
        return timeFactor;
    }

    public void setStartTime(Date startTime) {
        Calendar start = Calendar.getInstance();

        start.setTime(startTime);
        this.startTime.set(Calendar.HOUR_OF_DAY, start.get(Calendar.HOUR_OF_DAY));
        this.startTime.set(Calendar.MINUTE, start.get(Calendar.MINUTE));
        this.startTime.set(Calendar.SECOND, start.get(Calendar.SECOND));
        timeOffset = System.currentTimeMillis() - this.startTime.getTime().getTime();
    }

    public void setTimeFactor(int timeFactor) {
        this.timeFactor = timeFactor;
        restartModelClock();
    }

    public void start() {
        stopped = false;
        synchronized (commLock) {
            if (communication != null) {
                communication.sendTime(getStartTime(), getTimeFactor());
            }
        }
    }

    private void restartModelClock() {
        if (modelClockTimer != null) {
            LOGGER.info("Stop the model clock timer");
            modelClockTimer.cancel(true);
            modelClockTimer = null;
        }

        synchronized (commLock) {
            if (communication == null) {
                return;
            }
        }
        timeOffset = System.currentTimeMillis() - this.startTime.getTime().getTime();

        modelClockTimer = scheduler.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                if (!stopped) {
                    Calendar currentTime = getTime();
                    LOGGER.info("Send current time: {}", currentTime.getTime());
                    synchronized (commLock) {
                        communication.sendTime(currentTime, getTimeFactor());
                    }
                }
            }
        }, 60000 / timeFactor, 60000 / timeFactor, MILLISECONDS);

        LOGGER.info("Send start clock: {}, factor: {}", getStartTime().getTime(), getTimeFactor());
        communication.sendTime(getStartTime(), getTimeFactor());
    }

    public void stop() {
        stopped = true;

        if (modelClockTimer != null) {
            LOGGER.info("Stop the model clock timer");
            modelClockTimer.cancel(true);
            modelClockTimer = null;
        }

        synchronized (commLock) {
            if (communication != null) {
                communication.sendTime(getStartTime(), 0);
            }
        }

    }

    @Override
    public void opening() {

    }

    @Override
    public void opened(String port) {
        LOGGER.info("The port was opened: {}", port);
    }

    @Override
    public void initialized() {
        LOGGER.info("The initial loading phase has passed.");
        // communication is opened, start the timers
        synchronized (commLock) {
            communication = CommunicationFactory.getInstance();
        }
        stopped = false;

        // initially update the time label after one second
        scheduler.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                if (!stopped) {
                    Calendar time = getTime();

                    clock.setHour(time.get(Calendar.HOUR_OF_DAY));
                    clock.setMinute(time.get(Calendar.MINUTE));
                    clock.setSecond(time.get(Calendar.SECOND));
                }
            }
        }, 0, 1, SECONDS);

        restartModelClock();
    }

    @Override
    public void status(String statusText, int displayDuration) {

    }

    @Override
    public void closed(String port) {
        LOGGER.info("The port was closed: {}", port);

        stop();
    }
}
