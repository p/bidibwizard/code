package org.bidib.wizard.mvc.booster.view;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class CurrentCellRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = 1L;

    public CurrentCellRenderer() {
    }

    public Component getTableCellRendererComponent(
        JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        if (value instanceof Integer) {
            int val = ((Integer) value).intValue();
            StringBuffer sb = new StringBuffer();
            if (val >= 0) {
                sb.append(val).append(" mA");
            }
            else {
                sb.append("--- mA");
            }
            setText(sb.toString());
        }
        else {
            setText(null);
        }

        return this;
    }
}