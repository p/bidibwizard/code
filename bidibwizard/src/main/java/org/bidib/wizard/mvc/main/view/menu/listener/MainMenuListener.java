package org.bidib.wizard.mvc.main.view.menu.listener;

import org.bidib.wizard.mvc.main.model.MainModel;

public interface MainMenuListener {
    /**
     * Show about dialog.
     */
    void about();

    /**
     * Disconnect to BiDiB.
     */
    void connect();

    /**
     * Disconnect from BiDiB.
     */
    void disconnect();

    /**
     * Switch all booster off.
     */
    void allBoosterOff();

    /**
     * Switch all booster on.
     */
    void allBoosterOn();

    /**
     * Exit the application.
     */
    void exit();

    /**
     * Open the preferences.
     */
    void preferences();

    /**
     * Collect the logfiles
     */
    void collectLogFiles();

    /**
     * Open the log panel.
     */
    void logPanel();

    /**
     * Open the booster table.
     */
    void boosterTable();

    /**
     * Open the ping table.
     */
    void pingTable();

    /**
     * Open the console.
     */
    void console();

    /**
     * Open the node script editor.
     * 
     * @param mainModel
     *            the main model
     */
    void nodeScript(final MainModel mainModel);

    /**
     * Open the railcom plus panel.
     * 
     * @param mainModel
     *            the main model
     */
    void railcomPlus(final MainModel mainModel);

    /**
     * Open the POM Update panel.
     * 
     * @param mainModel
     *            the main model
     */
    void pomUpdate(final MainModel mainModel);

    /**
     * Open the debug interface panel.
     */
    void debugInterface();

    /**
     * Show the node script wizard.
     * 
     * @param mainModel
     *            the main model
     */
    void showNodeScriptWizard(final MainModel mainModel);
}
