package org.bidib.wizard.mvc.pomupdate.controller;

public class PomUpdateUtils {

    public static int calculateResendPacketCount(int pomRepeat, int minPomRepeat) {

        int resendPacketCount = (minPomRepeat / pomRepeat) + ((minPomRepeat % pomRepeat) > 0 ? 1 : 0);
        return resendPacketCount;
    }
}
