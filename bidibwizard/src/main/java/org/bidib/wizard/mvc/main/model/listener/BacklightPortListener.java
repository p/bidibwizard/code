package org.bidib.wizard.mvc.main.model.listener;

import org.bidib.wizard.comm.BacklightPortStatus;
import org.bidib.wizard.mvc.main.model.BacklightPort;

public interface BacklightPortListener extends OutputListener<BacklightPortStatus> {
    /**
     * The port config value has changed.
     * 
     * @param port
     *            the backlight port
     */
    void valuesChanged(BacklightPort port);
}
