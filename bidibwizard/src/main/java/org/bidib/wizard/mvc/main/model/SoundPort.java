package org.bidib.wizard.mvc.main.model;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.util.LinkedHashMap;
import java.util.Map;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.SoundPortEnum;
import org.bidib.jbidibc.core.port.BytePortConfigValue;
import org.bidib.jbidibc.core.port.PortConfigValue;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.wizard.comm.SoundPortStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SoundPort extends Port<SoundPortStatus> implements ConfigurablePort<SoundPortStatus> {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(SoundPort.class);

    static {
        try {
            // Q: why is this needed? A: export of beans with XMLDecoder
            PropertyDescriptor[] descriptor = Introspector.getBeanInfo(SoundPort.class).getPropertyDescriptors();

            for (int i = 0; i < descriptor.length; i++) {
                PropertyDescriptor propertyDescriptor = descriptor[i];
                if (propertyDescriptor.getName().equals("value")) {
                    propertyDescriptor.setValue("transient", Boolean.TRUE);
                }
                else if (propertyDescriptor.getName().equals("portConfig")) {
                    propertyDescriptor.setValue("transient", Boolean.TRUE);
                }
                else if (propertyDescriptor.getName().equals("portConfigX")) {
                    propertyDescriptor.setValue("transient", Boolean.TRUE);
                }
                else if (propertyDescriptor.getName().equals("hasSoundPortConfig")) {
                    propertyDescriptor.setValue("transient", Boolean.TRUE);
                }
            }
        }
        catch (IntrospectionException e) {
            throw new RuntimeException(e);
        }
    }

    private int pulseTime;

    private transient boolean hasSoundPortConfig;

    public SoundPort() {
        super(null);
        setStatus(SoundPortStatus.PLAY);
    }

    public SoundPort(GenericPort genericPort) {
        super(genericPort);
    }

    private SoundPort(Builder builder) {
        super(null);
        setId(builder.id);
        setLabel(builder.label);
    }

    @Override
    protected LcOutputType getPortType() {
        return LcOutputType.SOUNDPORT;
    }

    @Override
    protected SoundPortStatus internalGetStatus() {
        return SoundPortStatus.valueOf(SoundPortEnum.valueOf(genericPort.getPortStatus()));
    }

    /**
     * @return the pulseTime
     */
    public int getPulseTime() {
        if (genericPort != null) {
            Number pulseTime = genericPort.getPortConfigValue(BidibLibrary.BIDIB_PCFG_TICKS);
            if (pulseTime != null) {
                this.pulseTime = ByteUtils.getInt(pulseTime.byteValue());
            }
            else {
                LOGGER.warn("No value received from generic for BIDIB_PCFG_TICKS!");
            }
        }

        return pulseTime;
    }

    /**
     * @param pulseTime
     *            the pulseTime to set
     */
    public void setPulseTime(int pulseTime) {

        LOGGER.info("Set pulse time: {}", pulseTime);
        this.pulseTime = pulseTime;
        if (genericPort != null) {
            genericPort.setPortConfigValue(BidibLibrary.BIDIB_PCFG_TICKS,
                new BytePortConfigValue(ByteUtils.getLowByte(pulseTime)));
        }
    }

    @Override
    public byte[] getPortConfig() {
        return new byte[] { 0, 0, 0, 0 };
    }

    @Override
    public void setPortConfigX(Map<Byte, PortConfigValue<?>> portConfig) {

        LOGGER.info("Set the port config for the sound port.");

        Number pulseTime = getPortConfigValue(BidibLibrary.BIDIB_PCFG_TICKS, portConfig);
        if (pulseTime != null) {
            setPulseTime(ByteUtils.getInt(pulseTime.byteValue()));
            setHasSoundPortConfig(true);
        }
        else {
            LOGGER.warn("No value received for BIDIB_PCFG_TICKS!");
        }

        // call the super class
        super.setPortConfigX(portConfig);
    }

    @Override
    public Map<Byte, PortConfigValue<?>> getPortConfigX() {

        if (genericPort != null) {
            return genericPort.getPortConfig();
        }

        Map<Byte, PortConfigValue<?>> portConfigX = new LinkedHashMap<Byte, PortConfigValue<?>>();

        // add the ticks
        if (isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_TICKS)) {
            portConfigX.put(BidibLibrary.BIDIB_PCFG_TICKS, new BytePortConfigValue(ByteUtils.getLowByte(pulseTime)));
        }
        return portConfigX;
    }

    /**
     * @return the port has sound port config available
     */
    public boolean isHasSoundPortConfig() {
        if (genericPort != null) {
            // return genericPort.isHasPortConfig();
            hasSoundPortConfig = genericPort.isHasPortConfig(BidibLibrary.BIDIB_PCFG_TICKS);
            LOGGER.info("The generic port has sound port config: {}", hasSoundPortConfig);
        }
        return hasSoundPortConfig;
    }

    /**
     * @param hasSoundPortConfig
     *            the sound port config available flag
     */
    public void setHasSoundPortConfig(boolean hasSoundPortConfig) {
        LOGGER.info("The port has sound port config available: {}, port: {}", hasSoundPortConfig, this);
        this.hasSoundPortConfig = hasSoundPortConfig;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof SoundPort) {
            return ((SoundPort) obj).getId() == getId();
        }
        return false;
    }

    @Override
    public int hashCode() {
        return getId();
    }

    public static class Builder {
        private final int id;

        private String label;

        public Builder(int id) {
            this.id = id;
        }

        public Builder setLabel(String label) {
            this.label = label;
            return this;
        }

        public SoundPort build() {
            return new SoundPort(this);
        }
    }
}
