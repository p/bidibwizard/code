package org.bidib.wizard.mvc.main.view.table;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;

public class ComboBoxEditor<E> extends DefaultCellEditor {
    private static final long serialVersionUID = 1L;

    public ComboBoxEditor(E[] items) {
        super(new JComboBox<E>(items));
    }
}
