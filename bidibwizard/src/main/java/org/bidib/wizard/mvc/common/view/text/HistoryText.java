/*
 * HistoryText.java - Common code for text components with a history
 * :tabSize=8:indentSize=8:noTabs=false:
 * :folding=explicit:collapseFolds=1:
 *
 * Copyright (C) 2004 Slava Pestov
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.bidib.wizard.mvc.common.view.text;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

//{{{ Imports
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;

//}}}

/**
 * Controller (manager of models) for HistoryTextArea.
 * 
 * @author Slava Pestov
 * @version $Id: HistoryText.java 13414 2008-08-24 08:38:18Z k_satoda $
 */
public class HistoryText {
    // {{{ HistoryText constructor
    public HistoryText(JTextComponent text, String name) {
        this.text = text;
        setModel(name);
        index = -1;
    } // }}}

    // {{{ fireActionPerformed() method
    public void fireActionPerformed() {
    } // }}}

    // {{{ getIndex() mehtod
    public int getIndex() {
        return index;
    } // }}}

    // {{{ setIndex() mehtod
    public void setIndex(int index) {
        this.index = index;
    } // }}}

    // {{{ getModel() method
    /**
     * Returns the underlying history controller.
     * 
     * @since jEdit 4.3pre1
     */
    public HistoryModel getModel() {
        return historyModel;
    } // }}}

    // {{{ setModel() method
    /**
     * Sets the history list controller.
     * 
     * @param name
     *            The model name
     * @since jEdit 4.3pre1
     */
    public void setModel(String name) {
        if (name == null)
            historyModel = null;
        else
            historyModel = HistoryModel.getModel(name);
        index = -1;
    } // }}}

    // {{{ setInstantPopups() method
    /**
     * Sets if selecting a value from the popup should immediately fire an ActionEvent.
     */
    public void setInstantPopups(boolean instantPopups) {
        this.instantPopups = instantPopups;
    } // }}}

    // {{{ getInstantPopups() method
    /**
     * Returns if selecting a value from the popup should immediately fire an ActionEvent.
     */
    public boolean getInstantPopups() {
        return instantPopups;
    } // }}}

    // {{{ addCurrentToHistory() method
    /**
     * Adds the currently entered item to the history.
     */
    public void addCurrentToHistory() {
        if (historyModel != null) {
            historyModel.addItem(getText());
        }
        index = 0;
    } // }}}

    // {{{ doBackwardSearch() method
    public void doBackwardSearch() {
        if (historyModel == null)
            return;

        if (text.getSelectionEnd() != getDocument().getLength()) {
            text.setCaretPosition(getDocument().getLength());
        }

        int start = getInputStart();
        String t = getText().substring(0, text.getSelectionStart() - start);
        if (t == null) {
            historyPrevious();
            return;
        }

        for (int i = index + 1; i < historyModel.getSize(); i++) {
            String item = historyModel.getItem(i);
            if (item.startsWith(t)) {
                text.replaceSelection(item.substring(t.length()));
                text.select(getInputStart() + t.length(), getDocument().getLength());
                index = i;
                return;
            }
        }

        text.getToolkit().beep();
    } // }}}

    // {{{ doForwardSearch() method
    public void doForwardSearch() {
        if (historyModel == null)
            return;

        if (text.getSelectionEnd() != getDocument().getLength()) {
            text.setCaretPosition(getDocument().getLength());
        }

        int start = getInputStart();
        String t = getText().substring(0, text.getSelectionStart() - start);
        if (t == null) {
            historyNext();
            return;
        }

        for (int i = index - 1; i >= 0; i--) {
            String item = historyModel.getItem(i);
            if (item.startsWith(t)) {
                text.replaceSelection(item.substring(t.length()));
                text.select(getInputStart() + t.length(), getDocument().getLength());
                index = i;
                return;
            }
        }

        text.getToolkit().beep();
    } // }}}

    // {{{ historyPrevious() method
    public void historyPrevious() {
        if (historyModel == null)
            return;

        if (index == historyModel.getSize() - 1)
            text.getToolkit().beep();
        else if (index == -1) {
            current = getText();
            setText(historyModel.getItem(0));
            index = 0;
        }
        else {
            // have to do this because setText() sets index to -1
            int newIndex = index + 1;
            setText(historyModel.getItem(newIndex));
            index = newIndex;
        }
    } // }}}

    // {{{ historyNext() method
    public void historyNext() {
        if (historyModel == null)
            return;

        if (index == -1)
            text.getToolkit().beep();
        else if (index == 0)
            setText(current);
        else {
            // have to do this because setText() sets index to -1
            int newIndex = index - 1;
            setText(historyModel.getItem(newIndex));
            index = newIndex;
        }
    } // }}}

    // {{{ getDocument() method
    public Document getDocument() {
        return text.getDocument();
    } // }}}

    // {{{ getText() method
    /**
     * Subclasses can override this to provide funky history behavior, for JTextPanes and such.
     */
    public String getText() {
        return text.getText();
    } // }}}

    // {{{ setText() method
    /**
     * Subclasses can override this to provide funky history behavior, for JTextPanes and such.
     */
    public void setText(String text) {
        this.index = -1;
        this.text.setText(text);
    } // }}}

    // {{{ getInputStart() method
    /**
     * Subclasses can override this to provide funky history behavior, for JTextPanes and such.
     */
    public int getInputStart() {
        return 0;
    } // }}}

    // {{{ showPopupMenu() method
    public void showPopupMenu(String t, int x, int y) {
        if (historyModel == null)
            return;

        text.requestFocus();

        if (popup != null && popup.isVisible()) {
            popup.setVisible(false);
            popup = null;
            return;
        }

        popup = new JPopupMenu() {
            @Override
            public void setVisible(boolean b) {
                if (!b) {
                    popup = null;
                }
                super.setVisible(b);
            }
        };

        // TODO I18N
        JMenuItem caption = new JMenuItem("History");
        caption.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                new ListModelEditor().open(historyModel);
            }
        });
        popup.add(caption);
        popup.addSeparator();

        for (int i = 0; i < historyModel.getSize(); i++) {
            String item = historyModel.getItem(i);
            if (item.startsWith(t)) {
                JMenuItem menuItem = new JMenuItem(item);
                menuItem.setActionCommand(String.valueOf(i));
                menuItem.addActionListener(new ActionHandler());
                popup.add(menuItem);
            }
        }

        showPopupMenu(popup, text, x, y, false);
    } // }}}

    // {{{ showPopupMenu() method
    public void showPopupMenu(boolean search) {
        if (search)
            showPopupMenu(getText().substring(getInputStart(), text.getSelectionStart()), 0, text.getHeight());
        else
            showPopupMenu("", 0, text.getHeight());
    } // }}}

    // {{{ Private members
    private JTextComponent text;

    private HistoryModel historyModel;

    private int index;

    private String current;

    private JPopupMenu popup;

    private boolean instantPopups;

    // }}}

    // {{{ ActionHandler class
    class ActionHandler implements ActionListener {
        public void actionPerformed(ActionEvent evt) {
            int ind = Integer.parseInt(evt.getActionCommand());
            if (ind == -1) {
                if (index != -1)
                    setText(current);
            }
            else {
                setText(historyModel.getItem(ind));
                index = ind;
            }
            if (instantPopups) {
                addCurrentToHistory();
                fireActionPerformed();
            }
        }
    } // }}}

    // {{{ showPopupMenu() method
    /**
     * Shows the specified popup menu, ensuring it is displayed within the bounds of the screen.
     * 
     * @param popup
     *            The popup menu
     * @param comp
     *            The component to show it for
     * @param x
     *            The x co-ordinate
     * @param y
     *            The y co-ordinate
     * @param point
     *            If true, then the popup originates from a single point; otherwise it will originate from the component
     *            itself. This affects positioning in the case where the popup does not fit onscreen.
     * 
     * @since jEdit 4.1pre1
     */
    public static void showPopupMenu(JPopupMenu popup, Component comp, int x, int y, boolean point) {
        int offsetX = 0;
        int offsetY = 0;

        int extraOffset = point ? 1 : 0;

        Component win = comp;
        while (!(win instanceof Window || win == null)) {
            offsetX += win.getX();
            offsetY += win.getY();
            win = win.getParent();
        }

        if (win != null) {
            Dimension size = popup.getPreferredSize();

            Rectangle screenSize = GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds();

            if (x + offsetX + size.width + win.getX() > screenSize.width && x + offsetX + win.getX() >= size.width) {
                // System.err.println("x overflow");
                if (point)
                    x -= size.width + extraOffset;
                else
                    x = win.getWidth() - size.width - offsetX + extraOffset;
            }
            else {
                x += extraOffset;
            }

            // System.err.println("y=" + y + ",offsetY=" + offsetY
            // + ",size.height=" + size.height
            // + ",win.height=" + win.getHeight());
            if (y + offsetY + size.height + win.getY() > screenSize.height && y + offsetY + win.getY() >= size.height) {
                if (point)
                    y = win.getHeight() - size.height - offsetY + extraOffset;
                else
                    y = -size.height - 1;
            }
            else {
                y += extraOffset;
            }

            popup.show(comp, x, y);
        }
        else
            popup.show(comp, x + extraOffset, y + extraOffset);

    } // }}}
}
