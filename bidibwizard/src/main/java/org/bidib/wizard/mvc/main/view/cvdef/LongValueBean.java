package org.bidib.wizard.mvc.main.view.cvdef;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.common.bean.Bean;

public class LongValueBean extends Bean {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(LongValueBean.class);

    private LongCvNode longCvNode;

    /**
     * @return the longCvNode
     */
    public LongCvNode getLongCvNode() {
        return longCvNode;
    }

    /**
     * @param longCvNode
     *            the longCvNode to set
     */
    public void setLongCvNode(LongCvNode longCvNode) {
        LOGGER.info("Set the long CV node: {}", longCvNode);
        this.longCvNode = longCvNode;
    }

}
