package org.bidib.wizard.mvc.preferences.model.listener;

public interface PreferencesModelListener {
    void commPortsChanged();
}
