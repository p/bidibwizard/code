package org.bidib.wizard.mvc.dmx.view.panel;

import javax.swing.AbstractAction;

public abstract class LocationAwareAction<T> extends AbstractAction {
    private static final long serialVersionUID = 1L;

    protected DmxChartPanel dmxChartPanel;

    protected T actionObject;

    public LocationAwareAction(String name, T actionObject, DmxChartPanel dmxChartPanel) {
        super(name);
        this.dmxChartPanel = dmxChartPanel;
        this.actionObject = actionObject;
    }

    public T getActionObject() {
        return (T) actionObject;
    }
}