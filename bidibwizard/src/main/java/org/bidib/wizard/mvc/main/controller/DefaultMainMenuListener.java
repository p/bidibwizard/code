package org.bidib.wizard.mvc.main.controller;

import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.bidib.jbidibc.core.enumeration.CommandStationState;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.dialog.AboutDialog;
import org.bidib.wizard.dialog.FileDialog;
import org.bidib.wizard.mvc.booster.controller.BoosterTableController;
import org.bidib.wizard.mvc.console.controller.ConsoleController;
import org.bidib.wizard.mvc.debug.controller.DebugInterfaceController;
import org.bidib.wizard.mvc.logger.controller.LogPanelController;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.view.MainView;
import org.bidib.wizard.mvc.main.view.component.BoosterOnConfirmDialog;
import org.bidib.wizard.mvc.main.view.menu.listener.MainMenuListener;
import org.bidib.wizard.mvc.ping.controller.PingTableController;
import org.bidib.wizard.mvc.pomupdate.controller.PomUpdateController;
import org.bidib.wizard.mvc.preferences.controller.PreferencesController;
import org.bidib.wizard.mvc.preferences.model.Preferences;
import org.bidib.wizard.mvc.railcomplus.controller.RailcomPlusController;
import org.bidib.wizard.mvc.script.controller.NodeScriptController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultMainMenuListener implements MainMenuListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultMainMenuListener.class);

    private final MainView view;

    private final MainControllerInterface mainController;

    public DefaultMainMenuListener(final MainView view, final MainControllerInterface mainController) {
        this.view = view;
        this.mainController = mainController;
    }

    @Override
    public void about() {
        try {
            new AboutDialog(view);
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void exit() {
        mainController.stop();
    }

    @Override
    public void preferences() {
        PreferencesController preferencesController = new PreferencesController(view);

        preferencesController.start();
    }

    @Override
    public void connect() {
        LOGGER.info("Let the mainController open the connection.");
        mainController.openConnection();
    }

    @Override
    public void disconnect() {
        LOGGER.info("Let the mainController close the connection.");

        mainController.closeConnection();

        // clear the nodes
        mainController.clearNodes();
    }

    @Override
    public void allBoosterOff() {
        LOGGER.info("Switch all boosters off!");
        mainController.allBoosterOff();
    }

    @Override
    public void allBoosterOn() {
        LOGGER.info("Switch all boosters on!");
        int result = JOptionPane.CANCEL_OPTION;
        CommandStationState requestedCommandStationState = CommandStationState.GO;

        if (!Preferences.getInstance().isAllBoosterOnDoNotConfirmSwitch()) {
            // show a confirm dialog
            BoosterOnConfirmDialog allBoosterOnConfirmDialog =
                new BoosterOnConfirmDialog(JOptionPane.getFrameForComponent(view), true);

            result = allBoosterOnConfirmDialog.getResult();
            requestedCommandStationState = allBoosterOnConfirmDialog.getCommandStationState();
        }
        else {
            result = Preferences.getInstance().getAllBoosterOnSavedAction();
            requestedCommandStationState = Preferences.getInstance().getAllBoosterOnRequestedCommandStationState();

            LOGGER.info("Fetched action for all booster on from preferences: {}", result);
        }

        if (JOptionPane.CANCEL_OPTION == result) {
            LOGGER.info("User cancelled allBoosterOnConfirmDialog.");
            return;
        }

        mainController.allBoosterOn(result == BoosterOnConfirmDialog.RESULT_CONTINUE_BOOSTER_AND_COMMANDSTATION,
            requestedCommandStationState);
    }

    @Override
    public void collectLogFiles() {
        LOGGER.info("Collect the logfiles.");

        final String logFilePath = Preferences.getInstance().getLogFilePath();
        if (StringUtils.isNotBlank(logFilePath)) {
            final File logfile = new File(new File(logFilePath), "BiDiBWizard.log");

            if (logfile.exists()) {
                LOGGER.info("Found logfile to copy: {}", logfile);

                final FileFilter[] ff = null;
                FileDialog dialog = new FileDialog(view, FileDialog.SAVE, (String) null, ff) {
                    @Override
                    public void approve(String selectedFile) {
                        File file = new File(selectedFile);
                        if (file != null && file.isDirectory()) {

                            String zipFilePath = file.getPath();
                            LOGGER.info("Copy the logfiles to directory: {}", zipFilePath);

                            try {
                                File targetWizardLog = new File(file, "BiDiBWizard.log");
                                FileUtils.copyFile(logfile, targetWizardLog);

                                File logfileRXTX = new File(new File(logFilePath), "BiDiBWizard-RXTX.log");
                                File targetWizardRXTXLog = null;
                                if (logfileRXTX.exists()) {
                                    targetWizardRXTXLog = new File(file, "BiDiBWizard-RXTX.log");
                                    FileUtils.copyFile(logfileRXTX, targetWizardRXTXLog);
                                }

                                File logfileRAW = new File(new File(logFilePath), "BiDiBWizard-RAW.log");
                                File targetWizardRAWLog = null;
                                if (logfileRAW.exists()) {
                                    targetWizardRAWLog = new File(file, "BiDiBWizard-RAW.log");
                                    FileUtils.copyFile(logfileRAW, targetWizardRAWLog);
                                }

                                // add files to zip
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-HHmm");
                                File zipFile = new File(file, "BiDiBWizard-logs-" + sdf.format(new Date()) + ".zip");

                                LOGGER.info("Zip the logfiles to file: {}", zipFile);

                                FileOutputStream fileOutputStream = new FileOutputStream(zipFile);
                                ZipOutputStream zipOutputStream = new ZipOutputStream(fileOutputStream);

                                ZipEntry zipEntry = new ZipEntry(targetWizardLog.getName());
                                zipOutputStream.putNextEntry(zipEntry);

                                FileUtils.copyFile(targetWizardLog, zipOutputStream);

                                // close ZipEntry to store the stream to the file
                                zipOutputStream.closeEntry();

                                // copy the RXTX log
                                if (targetWizardRXTXLog != null) {
                                    zipEntry = new ZipEntry(targetWizardRXTXLog.getName());
                                    zipOutputStream.putNextEntry(zipEntry);

                                    FileUtils.copyFile(targetWizardRXTXLog, zipOutputStream);

                                    // close ZipEntry to store the stream to the file
                                    zipOutputStream.closeEntry();
                                }

                                // copy the RAW log
                                if (targetWizardRAWLog != null) {
                                    zipEntry = new ZipEntry(targetWizardRAWLog.getName());
                                    zipOutputStream.putNextEntry(zipEntry);

                                    FileUtils.copyFile(targetWizardRAWLog, zipOutputStream);

                                    // close ZipEntry to store the stream to the file
                                    zipOutputStream.closeEntry();
                                }

                                zipOutputStream.close();

                                fileOutputStream.close();

                                LOGGER.info("Open folder: {}", file);
                                Desktop.getDesktop().open(file);
                            }
                            catch (Exception ex) {
                                LOGGER.warn("Copy logfile to backup location failed.", ex);

                                JOptionPane.showMessageDialog(view,
                                    Resources.getString(DefaultMainMenuListener.class, "copy-logfiles-failed",
                                        new Object[] { ex.getLocalizedMessage() }),
                                    Resources.getString(DefaultMainMenuListener.class, "title-error"),
                                    JOptionPane.ERROR_MESSAGE);
                            }
                        }
                    }
                };
                dialog.setApproveButtonText(Resources.getString(DefaultMainMenuListener.class, "save-under"));
                dialog.showDialog();
            }

        }
        else {
            LOGGER.warn("No logFilePath available.");
        }
    }

    @Override
    public void logPanel() {
        LOGGER.info("Open the log panel.");

        LogPanelController logPanelController = new LogPanelController();
        logPanelController.start(view.getDesktop());
    }

    @Override
    public void boosterTable() {
        LOGGER.info("Open the booster table.");

        BoosterTableController boosterTableController = new BoosterTableController();
        boosterTableController.start(view.getDesktop(), mainController);
    }

    @Override
    public void pingTable() {
        LOGGER.info("Open the ping table.");

        PingTableController pingTableController = new PingTableController();
        pingTableController.start(view.getDesktop(), mainController);
    }

    @Override
    public void console() {
        LOGGER.info("Open the console.");

        ConsoleController consoleController = new ConsoleController();
        consoleController.start(view.getDesktop());
    }

    @Override
    public void nodeScript(final MainModel mainModel) {
        LOGGER.info("Open the nodeScript editor.");

        NodeScriptController nodeScriptController = NodeScriptController.getInstance();
        nodeScriptController.showView(view.getDesktop(), mainModel, mainController);
    }

    @Override
    public void railcomPlus(final MainModel mainModel) {
        LOGGER.info("Open the RailcomPlus panel.");

        RailcomPlusController railcomPlusController =
            new RailcomPlusController(mainModel, view.getDesktop(), mainModel.getSelectedNode());
        railcomPlusController.start(mainController);
    }

    @Override
    public void pomUpdate(final MainModel mainModel) {
        LOGGER.info("Open the PomUpdate panel.");

        PomUpdateController pomUpdateController =
            new PomUpdateController(mainModel, view.getDesktop(), mainModel.getSelectedNode());
        pomUpdateController.start(mainController);
    }

    @Override
    public void debugInterface() {
        LOGGER.info("Open the debug interface panel.");

        DebugInterfaceController debugInterfaceController = new DebugInterfaceController(view.getDesktop());
        debugInterfaceController.start();
    }

    @Override
    public void showNodeScriptWizard(final MainModel mainModel) {

        LOGGER.info("Open the nodeScript wizard.");

        NodeScriptController nodeScriptController = NodeScriptController.getInstance();
        nodeScriptController.showWizard(view.getDesktop(), mainModel, mainController);
    }
}
