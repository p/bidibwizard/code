package org.bidib.wizard.mvc.main.model.function;

import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.mvc.main.model.Port;

public abstract class SimplePortAction<T extends BidibStatus, P extends Port<?>> extends PortAction<T, P>
    implements PortValueAware {

    private int value;

    public SimplePortAction(T action, String key, P port) {
        this(action, key, port, 0, 0);
    }

    public SimplePortAction(T action, String key, P port, int delay, int value) {
        super(action, key, port, delay);
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
