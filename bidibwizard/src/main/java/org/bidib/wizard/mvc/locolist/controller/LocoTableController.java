package org.bidib.wizard.mvc.locolist.controller;

import javax.swing.SwingUtilities;

import org.bidib.jbidibc.core.DefaultMessageListener;
import org.bidib.jbidibc.core.DriveState;
import org.bidib.jbidibc.core.enumeration.CsQueryTypeEnum;
import org.bidib.wizard.comm.CommunicationFactory;
import org.bidib.wizard.comm.listener.CommunicationListener;
import org.bidib.wizard.mvc.common.view.DockKeys;
import org.bidib.wizard.mvc.locolist.controller.listener.LocoTableControllerListener;
import org.bidib.wizard.mvc.locolist.model.LocoTableModel;
import org.bidib.wizard.mvc.locolist.view.LocoTableView;
import org.bidib.wizard.mvc.main.controller.MainControllerInterface;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.model.listener.DefaultNodeListListener;
import org.bidib.wizard.mvc.main.model.listener.NodeListListener;
import org.bidib.wizard.mvc.main.model.listener.NodeSelectionListener;
import org.bidib.wizard.utils.DockUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vlsolutions.swing.docking.Dockable;
import com.vlsolutions.swing.docking.DockableState;
import com.vlsolutions.swing.docking.DockingConstants;
import com.vlsolutions.swing.docking.DockingDesktop;
import com.vlsolutions.swing.docking.DockingUtilities;
import com.vlsolutions.swing.docking.RelativeDockablePosition;
import com.vlsolutions.swing.docking.TabbedDockableContainer;
import com.vlsolutions.swing.docking.event.DockableStateChangeEvent;
import com.vlsolutions.swing.docking.event.DockableStateChangeListener;

public class LocoTableController implements LocoTableControllerListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(LocoTableController.class);

    private LocoTableView locoTableView;

    private LocoTableModel locoTableModel;

    private DefaultMessageListener messageListener;

    private Node selectedNode;

    public void start(final DockingDesktop desktop, final MainControllerInterface mainController) {

        // check if the loco table view is already opened
        String searchKey = DockKeys.LOCO_TABLE_VIEW;
        LOGGER.info("Search for view with key: {}", searchKey);
        Dockable view = desktop.getContext().getDockableByKey(searchKey);
        if (view != null) {
            LOGGER.info("Select the existing loco list table view.");
            DockUtils.selectWindow(view);
            return;
        }

        LOGGER.info("Create new LocoTableView.");

        locoTableModel = new LocoTableModel();

        locoTableView = new LocoTableView(this, locoTableModel);

        // add the loco panel next to the booster panel
        DockableState[] dockables = desktop.getDockables();
        LOGGER.info("Current dockables: {}", new Object[] { dockables });
        if (dockables.length > 1) {

            DockableState boosterTableView = null;
            // search the booster table view
            for (DockableState dockable : dockables) {

                if (DockKeys.DOCKKEY_BOOSTER_TABLE_VIEW.equals(dockable.getDockable().getDockKey())) {
                    LOGGER.info("Found the booster table view dockable.");
                    boosterTableView = dockable;

                    break;
                }
            }

            Dockable dock = desktop.getDockables()[1].getDockable();
            if (boosterTableView != null) {
                LOGGER.info("Add the loco panel view to the booster table view panel.");
                dock = boosterTableView.getDockable();

                TabbedDockableContainer container = DockingUtilities.findTabbedDockableContainer(dock);
                int order = 0;
                if (container != null) {
                    order = container.getTabCount();
                }
                LOGGER.info("Add new loco panel at order: {}", order);

                desktop.createTab(dock, locoTableView, order, true);
                desktop.setDockableHeight(locoTableView, 0.3d);
            }
            else {
                desktop.split(dock, locoTableView, DockingConstants.SPLIT_RIGHT);
                desktop.setDockableHeight(locoTableView, 0.3d);
            }
        }
        else {
            desktop.addDockable(locoTableView, RelativeDockablePosition.RIGHT);
        }

        // create the nodeList listener
        final NodeListListener nodeListListener = new DefaultNodeListListener() {

            @Override
            public void listNodeAdded(Node node) {
                LOGGER.info("The nodelist has a new node: {}", node);

                // nodeNew(node);
            }

            @Override
            public void listNodeRemoved(Node node) {
                LOGGER.info("The nodelist has a node removed: {}", node);
                nodeLost(node);
            }
        };
        // register as nodeList listener at the main controller
        mainController.addNodeListListener(nodeListListener);

        NodeSelectionListener nodeSelectionListener = new NodeSelectionListener() {

            @Override
            public void selectedNodeChanged(Node selectedNode) {
                LOGGER.info("The selected node has changed: {}", selectedNode);
                setSelectedNode(selectedNode);
            }
        };
        mainController.addNodeSelectionListListener(nodeSelectionListener);

        try {

            CommunicationFactory.addCommunicationListener(new CommunicationListener() {

                @Override
                public void status(String statusText, int displayDuration) {
                }

                @Override
                public void opening() {

                }

                @Override
                public void opened(String port) {
                    LOGGER.info("The communication was opened.");
                }

                @Override
                public void initialized() {
                }

                @Override
                public void closed(String port) {
                    LOGGER.info("The communication was closed. Remove all locos from the table.");

                    locoTableModel.removeAllLocos();

                    setSelectedNode(null);
                }
            });

            // TODO implemenent the handlers
            messageListener = new DefaultMessageListener() {

                @Override
                public void csDriveState(byte[] address, DriveState driveState) {
                    LOGGER.info("Received drive state: {}", driveState);

                    if (SwingUtilities.isEventDispatchThread()) {
                        locoTableModel.setDriveState(address, driveState);
                    }
                    else {
                        SwingUtilities.invokeLater(new Runnable() {

                            @Override
                            public void run() {
                                locoTableModel.setDriveState(address, driveState);
                            }
                        });
                    }
                }

            };
            CommunicationFactory.addMessageListener(messageListener);
        }
        catch (Exception ex) {
            LOGGER.warn("Register controller as node listener failed.", ex);
        }

        desktop.addDockableStateChangeListener(new DockableStateChangeListener() {

            @Override
            public void dockableStateChanged(DockableStateChangeEvent event) {
                if (event.getNewState().getDockable().equals(locoTableView) && event.getNewState().isClosed()) {
                    LOGGER.info("LocoTableView was closed, free resources.");

                    try {
                        // remove node listener from communication factory
                        if (nodeListListener != null) {
                            mainController.removeNodeListListener(nodeListListener);
                        }
                        if (messageListener != null) {
                            CommunicationFactory.removeMessageListener(messageListener);
                        }
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Unregister controller as node listener failed.", ex);
                    }

                    try {
                        // remove node listener from communication factory
                        if (nodeSelectionListener != null) {
                            mainController.removeNodeSelectionListener(nodeSelectionListener);
                        }
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Unregister controller as node selection listener failed.", ex);
                    }
                }
            }
        });

    }

    private void nodeLost(final Node node) {

        if (node != null && node.equals(selectedNode)) {
            LOGGER.info("The selected node was removed. Clear the list of loco for node: {}", node);
            if (SwingUtilities.isEventDispatchThread()) {
                locoTableModel.removeAllLocos();

                setSelectedNode(null);
            }
            else {
                SwingUtilities.invokeLater(new Runnable() {

                    @Override
                    public void run() {
                        locoTableModel.removeAllLocos();

                        setSelectedNode(null);
                    }
                });
            }
        }
    }

    public void setSelectedNode(final Node node) {
        LOGGER.info("Set the selected node: {}", node);

        if (node != null && org.bidib.jbidibc.core.utils.NodeUtils.hasCommandStationFunctions(node.getUniqueId())) {
            this.selectedNode = node;
            locoTableModel.setCsNodeSelected(true);

            if (locoTableView != null) {

                locoTableView.setDockTabName(node);

                if (selectedNode != null) {
                    queryCommandStationValue(selectedNode, CsQueryTypeEnum.LOCO_LIST, null);
                }
            }
        }
        else {
            this.selectedNode = null;
            locoTableModel.setCsNodeSelected(false);
        }
    }

    @Override
    public void queryCommandStationList(CsQueryTypeEnum csQueryType, Integer locoAddress) {
        LOGGER.info("Remove all locos from table before query the loco list from command station, locoAddress: {}",
            locoAddress);
        locoTableModel.removeAllLocos();

        queryCommandStationValue(selectedNode, csQueryType, locoAddress);
    }

    private void queryCommandStationValue(final Node node, CsQueryTypeEnum csQueryType, Integer locoAddress) {

        LOGGER.info("Query the loco list from command station, csQueryType: {}, locoAddress: {}", csQueryType,
            locoAddress);

        if (node != null) {
            try {

                // trigger the command station value for loco list
                CommunicationFactory.getInstance().queryCommandStationValue(node.getNode(), csQueryType, locoAddress);
            }
            catch (Exception ex) {
                LOGGER.warn("Query the loco list from command station failed.", ex);

                // TODO set an error flag or something in the node
            }
        }

    }
}
