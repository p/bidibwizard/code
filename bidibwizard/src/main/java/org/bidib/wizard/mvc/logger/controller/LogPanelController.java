package org.bidib.wizard.mvc.logger.controller;

import org.bidib.wizard.mvc.common.view.DockKeys;
import org.bidib.wizard.mvc.logger.view.BidibLogsAppender;
import org.bidib.wizard.mvc.logger.view.LogPanelView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Appender;

import com.vlsolutions.swing.docking.Dockable;
import com.vlsolutions.swing.docking.DockableState;
import com.vlsolutions.swing.docking.DockingConstants;
import com.vlsolutions.swing.docking.DockingDesktop;
import com.vlsolutions.swing.docking.DockingUtilities;
import com.vlsolutions.swing.docking.RelativeDockablePosition;
import com.vlsolutions.swing.docking.TabbedDockableContainer;
import com.vlsolutions.swing.docking.event.DockableStateChangeEvent;
import com.vlsolutions.swing.docking.event.DockableStateChangeListener;

public class LogPanelController {
    private static final Logger LOGGER = LoggerFactory.getLogger(LogPanelController.class);

    private LogPanelView view;

    public void start(final DockingDesktop desktop) {

        // check if the log panel is already opened
        String searchKey = "LogPanelView";
        LOGGER.info("Search for view with key: {}", searchKey);
        Dockable logPanelView = desktop.getContext().getDockableByKey(searchKey);
        if (logPanelView != null) {
            LOGGER.info("Select the existing log panel view.");
            selectWindow(logPanelView);
            return;
        }

        try {
            // assume SLF4J is bound to logback in the current environment
            LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();

            // ch.qos.logback.classic.Logger log = context.getLogger("org.bidib");
            ch.qos.logback.classic.Logger logTX = context.getLogger("TX");
            ch.qos.logback.classic.Logger logRX = context.getLogger("RX");
            Appender<ILoggingEvent> appender = logTX.getAppender(BidibLogsAppender.APPENDER_NAME);

            if (appender == null) {

                BidibLogsAppender simpleLogPanelAppender = new BidibLogsAppender();
                simpleLogPanelAppender.setContext(context);
                simpleLogPanelAppender.setName(BidibLogsAppender.APPENDER_NAME);
                simpleLogPanelAppender.start();

                logTX.setAdditive(false);
                logTX.setLevel(Level.INFO);
                logTX.addAppender(simpleLogPanelAppender);

                logRX.setAdditive(false);
                logRX.setLevel(Level.INFO);
                logRX.addAppender(simpleLogPanelAppender);
            }
            else {
                LOGGER.info("LogPanel appender is already assigned.");
            }
        }
        catch (Exception ex) {
            LOGGER.info("Add simple log panel logger failed.");
        }

        LOGGER.info("Create new LogPanelView.");

        view = new LogPanelView();

        // add the log panel next to the booster panel
        DockableState[] dockables = desktop.getDockables();
        LOGGER.info("Current dockables: {}", new Object[] { dockables });
        if (dockables.length > 1) {

            DockableState boosterTableView = null;
            // search the booster table view
            for (DockableState dockable : dockables) {

                if (DockKeys.DOCKKEY_BOOSTER_TABLE_VIEW.equals(dockable.getDockable().getDockKey())) {
                    LOGGER.info("Found the booster table view dockable.");
                    boosterTableView = dockable;

                    break;
                }
            }

            Dockable dock = desktop.getDockables()[1].getDockable();
            if (boosterTableView != null) {
                LOGGER.info("Add the log panel view to the booster table view panel.");
                dock = boosterTableView.getDockable();

                TabbedDockableContainer container = DockingUtilities.findTabbedDockableContainer(dock);
                int order = 0;
                if (container != null) {
                    order = container.getTabCount();
                }
                LOGGER.info("Add new log panel at order: {}", order);

                desktop.createTab(dock, view, order, true);
            }
            else {
                desktop.split(dock, view, DockingConstants.SPLIT_RIGHT);
            }
        }
        else {
            desktop.addDockable(view, RelativeDockablePosition.RIGHT);
        }

        desktop.addDockableStateChangeListener(new DockableStateChangeListener() {

            @Override
            public void dockableStateChanged(DockableStateChangeEvent event) {
                if (event.getNewState().getDockable().equals(view) && event.getNewState().isClosed()) {
                    LOGGER.info("LogPanelView was closed, free resources.");

                    view.close();

                    try {
                        // assume SLF4J is bound to logback in the current environment
                        LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();

                        // ch.qos.logback.classic.Logger log = context.getLogger("org.bidib");
                        ch.qos.logback.classic.Logger logTX = context.getLogger("TX");
                        ch.qos.logback.classic.Logger logRX = context.getLogger("RX");
                        Appender<ILoggingEvent> appender = logTX.getAppender(BidibLogsAppender.APPENDER_NAME);
                        LOGGER.info("Current BidibLogsAppender: {}", appender);

                        if (appender != null) {
                            LOGGER.info("Detach and stop the appender.");
                            logTX.detachAppender(appender);
                            logRX.detachAppender(appender);

                            appender.stop();
                        }
                    }
                    catch (Exception ex) {
                        LOGGER.info("Remove BidibLogsAppender failed.");
                    }
                }
            }
        });
    }

    private void selectWindow(Dockable dockable) {

        // TODO this should also work with non-tabbed windows
        TabbedDockableContainer container = DockingUtilities.findTabbedDockableContainer(dockable);
        if (container != null) {
            container.setSelectedDockable(dockable);
        }
        else {
            LOGGER.warn("Container not available, select component directly.");
            dockable.getComponent().requestFocusInWindow();
        }
    }
}
