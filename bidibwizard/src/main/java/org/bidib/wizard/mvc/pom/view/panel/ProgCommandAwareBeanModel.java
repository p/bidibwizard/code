package org.bidib.wizard.mvc.pom.view.panel;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.bidib.jbidibc.core.enumeration.PomOperation;
import org.bidib.wizard.mvc.pom.view.command.PomOperationCommand;

import com.jgoodies.binding.beans.Model;

public class ProgCommandAwareBeanModel extends Model {
    private static final long serialVersionUID = 1L;

    public static final String PROPERTYNAME_CURRENT_OPERATION = "currentOperation";

    public static final String PROPERTYNAME_EXECUTION = "execution";

    public static final String PROPERTYNAME_EXECUTING_PROG_COMMAND = "executingProgCommand";

    public static final String PROPERTYNAME_PROG_COMMANDS = "progCommands";

    public static final String PROPERTYNAME_EXECUTED_PROG_COMMANDS = "executedProgCommands";

    private PomOperation currentOperation;

    private ExecutionType execution;

    private PomOperationCommand<? extends ProgCommandAwareBeanModel> executingProgCommand;

    private List<PomOperationCommand<? extends ProgCommandAwareBeanModel>> progCommands =
        new LinkedList<PomOperationCommand<? extends ProgCommandAwareBeanModel>>();

    private List<PomOperationCommand<? extends ProgCommandAwareBeanModel>> executedProgCommands =
        new LinkedList<PomOperationCommand<? extends ProgCommandAwareBeanModel>>();

    public static final String PROPERTYNAME_CURRENT_DECODER_ADDRESS = "currentDecoderAddress";

    private Integer currentDecoderAddress;

    public enum ExecutionType {
        READ, WRITE;
    }

    public ProgCommandAwareBeanModel() {
    }

    /**
     * @return the currentOperation
     */
    public PomOperation getCurrentOperation() {
        return currentOperation;
    }

    /**
     * @param currentOperation
     *            the currentOperation to set
     */
    public void setCurrentOperation(PomOperation currentOperation) {
        PomOperation oldCurrentOperation = this.currentOperation;
        this.currentOperation = currentOperation;
        firePropertyChange(PROPERTYNAME_CURRENT_OPERATION, oldCurrentOperation, currentOperation);
    }

    /**
     * @return the execution
     */
    public ExecutionType getExecution() {
        return execution;
    }

    /**
     * @param execution
     *            the execution to set
     */
    public void setExecution(ExecutionType execution) {
        ExecutionType oldExecution = this.execution;
        this.execution = execution;
        firePropertyChange(PROPERTYNAME_EXECUTION, oldExecution, execution);
    }

    /**
     * @return the executingProgCommand
     */
    public PomOperationCommand<? extends ProgCommandAwareBeanModel> getExecutingProgCommand() {
        return executingProgCommand;
    }

    /**
     * @param executingProgCommand
     *            the executingProgCommand to set
     */
    public void setExecutingProgCommand(PomOperationCommand<? extends ProgCommandAwareBeanModel> executingProgCommand) {
        PomOperationCommand<? extends ProgCommandAwareBeanModel> oldExecutingProgCommand = this.executingProgCommand;
        this.executingProgCommand = executingProgCommand;
        firePropertyChange(PROPERTYNAME_EXECUTING_PROG_COMMAND, oldExecutingProgCommand, executingProgCommand);
    }

    /**
     * @return the progCommands
     */
    public List<PomOperationCommand<? extends ProgCommandAwareBeanModel>> getProgCommands() {
        return progCommands;
    }

    /**
     * @param progCommands
     *            the progCommands to set
     */
    public void setProgCommands(List<PomOperationCommand<? extends ProgCommandAwareBeanModel>> progCommands) {
        List<PomOperationCommand<? extends ProgCommandAwareBeanModel>> oldProgCommands = this.progCommands;
        this.progCommands = progCommands;
        firePropertyChange(PROPERTYNAME_PROG_COMMANDS, oldProgCommands, progCommands);
    }

    /**
     * @return the executedProgCommands
     */
    public List<PomOperationCommand<? extends ProgCommandAwareBeanModel>> getExecutedProgCommands() {
        return executedProgCommands;
    }

    /**
     * @param executedProgCommands
     *            the executedProgCommands to set
     */
    public void setExecutedProgCommands(
        List<PomOperationCommand<? extends ProgCommandAwareBeanModel>> executedProgCommands) {
        List<PomOperationCommand<? extends ProgCommandAwareBeanModel>> oldExecutedProgCommands =
            this.executedProgCommands;
        this.executedProgCommands = executedProgCommands;
        firePropertyChange(PROPERTYNAME_PROG_COMMANDS, oldExecutedProgCommands, executedProgCommands);
    }

    /**
     * @return the currentDecoderAddress
     */
    public Integer getCurrentDecoderAddress() {
        return currentDecoderAddress;
    }

    /**
     * @param currentDecoderAddress
     *            the currentDecoderAddress to set
     */
    public void setCurrentDecoderAddress(Integer currentDecoderAddress) {
        this.currentDecoderAddress = currentDecoderAddress;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}