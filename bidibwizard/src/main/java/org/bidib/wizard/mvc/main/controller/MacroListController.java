package org.bidib.wizard.mvc.main.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.bidib.wizard.mvc.main.model.Macro;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.model.Port;
import org.bidib.wizard.mvc.main.model.function.Function;
import org.bidib.wizard.mvc.main.model.function.PortAction;
import org.bidib.wizard.mvc.main.view.panel.MacroListPanel;
import org.bidib.wizard.utils.PortListUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MacroListController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MacroListController.class);

    private final MainModel mainModel;

    private MacroListPanel macroListPanel;

    public MacroListController(final MainModel mainModel) {
        this.mainModel = mainModel;
    }

    public MacroListPanel createPanel() {
        LOGGER.info("Create new macro list panel.");

        macroListPanel = new MacroListPanel(mainModel);

        return macroListPanel;
    }

    /**
     * Move the ports in all macros
     * 
     * @param model
     *            the model
     * @param node
     *            the node
     * @param port
     *            the start port
     * @param allPorts
     *            the ports
     * @param portsCount
     *            number of ports to move
     */
    public <E extends Port<?>> void movePortsInAllMacros(
        final MainModel model, final Node node, E port, List<E> allPorts, int portsCount) {
        movePortsInMacros(model, node, port, allPorts, portsCount);
    }

    public <E extends Port<?>> void movePortsInMacros(
        final MainModel model, final Node node, E port, List<E> allPorts, int portsCount) {

        LcOutputType outputType = Port.getPortType(port);

        // iterate over the macros of the node
        for (Macro macro : model.getMacros()) {

            processMacro(model, node, macro, outputType, port, allPorts, portsCount);
        }

    }

    protected <E extends Port<?>> void processMacro(
        final MainModel model, final Node node, final Macro macro, LcOutputType outputType, final E port,
        List<E> allPorts, int portsCount) {

        List<MacroStepPortEntry> portsToMove = new ArrayList<>();

        // check which ports are in use behind the provided port in macros
        for (Function<? extends BidibStatus> step : macro.getFunctions()) {
            if (step instanceof PortAction) {
                PortAction<BidibStatus, Port<?>> portAction = (PortAction<BidibStatus, Port<?>>) step;
                if (Port.getPortType(portAction.getPort()).equals(outputType)
                    && portAction.getPort().getId() >= port.getId()) {
                    // only matching ports

                    LOGGER.info("Add port to move: {}", portAction.getPort());
                    MacroStepPortEntry entry = new MacroStepPortEntry(portAction.getPort(), portAction);
                    portsToMove.add(entry);
                }
            }
        }

        if (CollectionUtils.isNotEmpty(portsToMove)) {
            LOGGER.info("Found macro steps to manipulate: {}", portsToMove);
            // from the highest port number backwards to the lowest replace the port numbers

            for (MacroStepPortEntry entry : portsToMove) {
                E currentPort = (E) entry.getPort();
                int portNum = currentPort.getId();
                int replacedPortNum = portNum + portsCount;
                LOGGER.info("Current 'old' port with portNum: {}, replacedPortNum: {}", portNum, replacedPortNum);

                // get the port with the next id
                Port<?> replacedPort = PortListUtils.findPortByPortNumber(allPorts, replacedPortNum);
                if (replacedPort != null) {
                    LOGGER.info("Set the replaced port: {}, portNum: {}", replacedPort, replacedPort.getId());
                    entry.getStep().setPort(replacedPort);
                }
                else {
                    LOGGER.info("Replaced port is not available, replacedPortNum: {}", replacedPortNum);
                }
            }

            MainControllerInterface mainController =
                DefaultApplicationContext.getInstance().get(DefaultApplicationContext.KEY_MAIN_CONTROLLER,
                    MainControllerInterface.class);
            mainController.replaceMacro(macro, true);
        }
        else {
            LOGGER.info("No ports to move in macros found.");
        }
    }

    private static final class MacroStepPortEntry {
        private PortAction<BidibStatus, Port<?>> step;

        private Port<?> port;

        public MacroStepPortEntry(Port<?> port, PortAction<BidibStatus, Port<?>> step) {
            this.port = port;
            this.step = step;
        }

        public Port<?> getPort() {
            return port;
        }

        public PortAction<BidibStatus, Port<?>> getStep() {
            return step;
        }

        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
        }
    }

}
