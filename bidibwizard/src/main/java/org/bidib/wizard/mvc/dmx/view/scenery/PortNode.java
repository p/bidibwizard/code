package org.bidib.wizard.mvc.dmx.view.scenery;

import javax.swing.tree.DefaultMutableTreeNode;

import org.bidib.wizard.mvc.main.model.Port;

public class PortNode extends DefaultMutableTreeNode {
    private static final long serialVersionUID = 1L;

    public PortNode(Port<?> port) {
        super(port);
    }

    public Port<?> getPort() {
        return (Port<?>) getUserObject();
    }
}
