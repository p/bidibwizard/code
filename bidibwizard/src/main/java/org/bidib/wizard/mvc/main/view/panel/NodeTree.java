package org.bidib.wizard.mvc.main.view.panel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import javax.swing.ImageIcon;
import javax.swing.SwingUtilities;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreePath;

import org.bidib.jbidibc.core.enumeration.IdentifyState;
import org.bidib.jbidibc.core.enumeration.SysErrorEnum;
import org.bidib.jbidibc.core.utils.NodeUtils;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.common.utils.ImageUtils;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.view.component.LabelTree;
import org.bidib.wizard.mvc.main.view.panel.renderer.BidibNodeNameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The <code>NodeTree</code> displays the nodes of the system in a tree.
 */
public class NodeTree extends LabelTree<Node> {
    private static final Logger LOGGER = LoggerFactory.getLogger(NodeTree.class);

    private static final long serialVersionUID = 1L;

    public NodeTree() {
        super(new LabelTreeModel(new NodeTreeNode()));

        // show the root handles to allow expand / collapse
        setShowsRootHandles(true);
        // don't expand / collapse on click on item
        setToggleClickCount(0);

        initialize();
    }

    /**
     * Initialize the component
     */
    protected void initialize() {
        // Set the icon for leaf nodes.
        ImageIcon bidibLeafIcon = ImageUtils.createImageIcon(NodeTree.class, "/icons/green-leaf.png");
        ImageIcon bidibNodeIcon = ImageUtils.createImageIcon(NodeTree.class, "/icons/green-node.png");
        ImageIcon bidibIdentifyIcon = ImageUtils.createImageIcon(NodeTree.class, "/icons/red-leaf.png");
        ImageIcon bidibErrorIcon = ImageUtils.createImageIcon(NodeTree.class, "/icons/error-leaf.png");
        ImageIcon bidibRestartIcon = ImageUtils.createImageIcon(NodeTree.class, "/icons/restart-pending.png");
        ImageIcon bidibLeafWarnIcon = ImageUtils.createImageIcon(NodeTree.class, "/icons/green-leaf-warn.png");
        ImageIcon bidibNodeWarnIcon = ImageUtils.createImageIcon(NodeTree.class, "/icons/green-node-warn.png");

        String messageUnsupportedProtocol = Resources.getString(NodeTree.class, "unsupported-protocol-version");

        if (bidibNodeIcon != null) {
            setCellRenderer(new BidibNodeRenderer(this, bidibLeafIcon, bidibNodeIcon, bidibIdentifyIcon, bidibErrorIcon,
                bidibRestartIcon, bidibLeafWarnIcon, bidibNodeWarnIcon, messageUnsupportedProtocol));
        }
        else {
            LOGGER.warn("BidibNode icon missing; using default.");
        }

    }

    @Override
    public void setItems(Node[] items) {
        LOGGER.info("Set the node items, size: {}, items: {}", items.length, items);

        DefaultTreeModel model = (DefaultTreeModel) getModel();

        Object root = model.getRoot();
        if (root instanceof MutableTreeNode) {
            MutableTreeNode rootNode = (MutableTreeNode) root;

            // TODO try optimize this, we don't remove the already existing nodes
            // remove all nodes from the tree
            for (int i = rootNode.getChildCount(); i > 0; i--) {
                LOGGER.trace("remove child at index: {}", i - 1);
                try {

                    rootNode.remove(i - 1);
                }
                catch (Exception ex) {
                    LOGGER.warn("Remove node failed, index: " + (i - 1), ex);
                }
            }

            List<Node> nodeItems = new LinkedList<Node>(Arrays.asList(items));

            // add the nodes to the tree
            NodeTreeNode interfaceNode = null;
            // search the interface node
            for (Node itemNode : nodeItems) {
                if (NodeUtils.convertAddress(itemNode.getNode().getAddr()) == NodeUtils.INTERFACE_ADDRESS) {
                    LOGGER.debug("Adding interface node: {}", itemNode);
                    // this is the interface node
                    interfaceNode = new NodeTreeNode(itemNode);
                    rootNode.insert(interfaceNode, NodeUtils.INTERFACE_ADDRESS);
                    nodeItems.remove(itemNode);
                    break;
                }
            }

            if (interfaceNode != null) {
                // add the nodes
                addNodesOfLevel(interfaceNode, nodeItems);
            }
        }

        updateUI();

        setRootVisible(true);
        // expand root and make it invisible
        expandPath(getPathForRow(0));

        if (getRowCount() > 0) {
            // expandRow(getRowCount() - 1);
            DefaultMutableTreeNode rootNode = (DefaultMutableTreeNode) model.getRoot();
            sortTree(rootNode);

            expandAll();
        }

        setRootVisible(false);
    }

    private void addNodesOfLevel(NodeTreeNode parentNode, List<Node> nodeItems) {
        LOGGER.debug("Add children to node: {}", parentNode);

        List<Node> toRemove = new LinkedList<Node>();

        for (int index = 0; index < nodeItems.size(); index++) {
            Node itemNode = nodeItems.get(index);

            // check if the hierarchy level is correct
            if (isChildOfNode(parentNode, itemNode)) {
                LOGGER.info("Adding new node: {}, address: {}", itemNode, itemNode.getNode().getAddr());
                parentNode.add(new NodeTreeNode(itemNode));

                toRemove.add(itemNode);
            }
        }

        LOGGER.trace("Remove inserted nodes from original list of nodeItems: {}", toRemove);
        nodeItems.removeAll(toRemove);

        if (nodeItems.size() == 0) {
            LOGGER.trace("No more node items to add.");
            return;
        }

        @SuppressWarnings("unchecked")
        Enumeration<NodeTreeNode> children = parentNode.children();
        while (children.hasMoreElements()) {
            NodeTreeNode childNode = children.nextElement();
            addNodesOfLevel(childNode, nodeItems);
        }
    }

    protected boolean isChildOfNode(NodeTreeNode parentTreeNode, Node node) {
        // get the parent node from the tree node
        Node parentNode = (Node) parentTreeNode.getUserObject();
        // compare the address parts
        byte[] parentAddr = parentNode.getNode().getAddr();
        byte[] nodeAddr = node.getNode().getAddr();

        // get the length of the parent addr
        int parentLen = 0;
        for (int index = 0; index < parentAddr.length; index++) {
            if (parentAddr[index] != 0) {
                parentLen++;
            }
        }
        LOGGER.debug("Length of parent address: {}", parentLen);

        // get the length of the node addr
        int nodeLen = 0;
        for (int index = 0; index < nodeAddr.length; index++) {
            if (nodeAddr[index] != 0) {
                nodeLen++;
            }
        }
        LOGGER.debug("Length of node address: {}", nodeLen);

        if (nodeLen != (parentLen + 1)) {
            LOGGER.debug("Node is not a child of the provided parent because length of address does not match.");
            return false;
        }

        for (int index = 0; index < (nodeLen - 1); index++) {
            LOGGER.debug("Comparing at index: {}, nodeAddr: {}, parentAddr: {}", index, nodeAddr[index],
                parentAddr[index]);
            if (nodeAddr[index] != parentAddr[index]) {
                return false;
            }
        }
        return true;
    }

    public void labelChanged(final Node node, String label) {
        LOGGER.info("The label was changed on node: {}, new label: {}", node, label);

        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                LOGGER.info("Sort the tree.");
                DefaultMutableTreeNode rootNode = (DefaultMutableTreeNode) getModel().getRoot();

                DefaultMutableTreeNode nodeToStart = rootNode;

                @SuppressWarnings("unchecked")
                Enumeration<DefaultMutableTreeNode> e = rootNode.depthFirstEnumeration();
                while (e.hasMoreElements()) {
                    DefaultMutableTreeNode treeNode = e.nextElement();
                    if (Objects.equals(treeNode.getUserObject(), node)) {
                        LOGGER.info("Found tree node to start sort: {}", treeNode);
                        nodeToStart = (DefaultMutableTreeNode) treeNode.getParent();
                        break;
                    }
                }

                sortTree(nodeToStart);

                updateUI();
            }
        });
    }

    public void refreshTree() {
        LOGGER.info("Refresh tree is called.");
        ((DefaultTreeModel) getModel()).reload();

        DefaultMutableTreeNode rootNode = (DefaultMutableTreeNode) getModel().getRoot();
        sortTree(rootNode);

        updateUI();
    }

    private static void sortTree(DefaultMutableTreeNode root) {
        LOGGER.debug("Sort tree starting from: {}", root);
        Enumeration<?> e = root.depthFirstEnumeration();
        while (e.hasMoreElements()) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) e.nextElement();
            if (!node.isLeaf()) {
                sort3(node);
            }
        }
    }

    private static Comparator<DefaultMutableTreeNode> tnc = new Comparator<DefaultMutableTreeNode>() {
        @Override
        public int compare(DefaultMutableTreeNode a, DefaultMutableTreeNode b) {
            // Sort the parent and child nodes separately:
            LOGGER.debug("Compare nodes, a: {}, b: {}", a, b);
            if (a.isLeaf() && !b.isLeaf()) {
                return 1;
            }
            else if (!a.isLeaf() && b.isLeaf()) {
                return -1;
            }
            else {
                String sa = BidibNodeNameUtils.prepareLabel((Node) a.getUserObject(), false, false).getNodeLabel();
                String sb = BidibNodeNameUtils.prepareLabel((Node) b.getUserObject(), false, false).getNodeLabel();
                LOGGER.debug("Compare node labels: {} <=> {}", sa, sb);
                return sa.compareToIgnoreCase(sb);
            }
        }
    };

    private static void sort3(DefaultMutableTreeNode parent) {
        int n = parent.getChildCount();

        List<DefaultMutableTreeNode> children = new ArrayList<DefaultMutableTreeNode>(n);
        for (int i = 0; i < n; i++) {
            children.add((DefaultMutableTreeNode) parent.getChildAt(i));
        }
        Collections.sort(children, tnc); // iterative merge sort
        parent.removeAllChildren();
        for (MutableTreeNode node : children) {
            parent.add(node);
        }
    }

    @Override
    public void setSelectedItem(Node node) {
        LOGGER.info("Select the node in the tree: {}", node);

        TreePath pathToSelect = null;

        int totalRows = getRowCount();
        for (int row = 0; row < totalRows; row++) {
            TreePath path = getPathForRow(row);

            Object currentUserObject = ((DefaultMutableTreeNode) path.getLastPathComponent()).getUserObject();

            LOGGER.debug("Current user object: {}, path: {}", currentUserObject, path);
            if (currentUserObject instanceof Node) {
                Node currentNode = (Node) currentUserObject;
                if (currentNode.getUniqueId() == node.getUniqueId()) {
                    LOGGER.info("Found node to select: {}", currentNode);
                    pathToSelect = path;
                    break;
                }
            }
        }

        if (pathToSelect != null) {
            setSelectionPath(pathToSelect);

            selectedValueChanged(-1);
        }
        else {
            LOGGER.warn("The provided node to select was not found: {}", node);
            throw new IllegalArgumentException("The provided node to select was not found: " + node);
        }
    }

    public static final class NodeTreeNode extends DefaultMutableTreeNode {
        private static final long serialVersionUID = 1L;

        private Node node;

        public NodeTreeNode() {
            this(null);
        }

        public NodeTreeNode(Node node) {
            super(node);
            this.node = node;
        }

        /**
         * @return the identifyState
         */
        public IdentifyState getIdentifyState() {
            if (this.node != null) {
                return this.node.getIdentifyState();
            }
            return null;
        }

        /**
         * @return the error state
         */
        public SysErrorEnum getErrorState() {
            if (this.node != null) {
                return this.node.getErrorState();
            }
            return null;
        }
    }

}
