package org.bidib.wizard.mvc.main.view.menu;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.Collection;
import java.util.LinkedList;

import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.main.model.Accessory;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.listener.AccessoryListListener;
import org.bidib.wizard.mvc.main.model.listener.AccessoryListener;
import org.bidib.wizard.mvc.main.view.menu.listener.AccessoryTableMenuListener;

public class AccessoryTableMenu extends BasicPopupMenu implements AccessoryListener {
    private static final long serialVersionUID = 1L;

    public static final KeyStroke KEYSTROKE_DELETE = KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0);

    public static final KeyStroke KEYSTROKE_PASTE =
        KeyStroke.getKeyStroke(KeyEvent.VK_V, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask());

    public static final KeyStroke KEYSTROKE_CUT =
        KeyStroke.getKeyStroke(KeyEvent.VK_X, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask());

    public static final KeyStroke KEYSTROKE_COPY =
        KeyStroke.getKeyStroke(KeyEvent.VK_C, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask());

    private final Collection<AccessoryTableMenuListener> menuListeners = new LinkedList<AccessoryTableMenuListener>();

    private final JMenuItem insertEmptyBefore = new JMenuItem(Resources.getString(getClass(), "insertEmptyBefore"));

    private final JMenuItem insertEmptyAfter = new JMenuItem(Resources.getString(getClass(), "insertEmptyAfter"));

    private final JMenuItem delete = new JMenuItem(Resources.getString(getClass(), "delete"));

    private final JMenuItem selectAll = new JMenuItem(Resources.getString(getClass(), "selectAll"));

    private final JMenuItem cut = new JMenuItem(Resources.getString(getClass(), "cut"));

    private final JMenuItem copy = new JMenuItem(Resources.getString(getClass(), "copy"));

    private final JMenuItem pasteAfter = new JMenuItem(Resources.getString(getClass(), "pasteAfter"));

    private Accessory accessory = null;

    public AccessoryTableMenu(final MainModel model) {
        cut.setAccelerator(KEYSTROKE_CUT);
        cut.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireCut();
            }
        });
        add(cut);

        copy.setAccelerator(KEYSTROKE_COPY);
        copy.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireCopy();
            }
        });
        add(copy);

        pasteAfter.setAccelerator(KEYSTROKE_PASTE);
        pasteAfter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                firePasteAfter();
            }
        });
        add(pasteAfter);

        insertEmptyBefore.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireInsertEmptyBefore();
            }
        });
        add(insertEmptyBefore);

        insertEmptyAfter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireInsertEmptyAfter();
            }
        });
        add(insertEmptyAfter);

        delete.setAccelerator(KEYSTROKE_DELETE);
        delete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireDelete();
            }
        });
        add(delete);

        addSeparator();

        selectAll.setAccelerator(
            KeyStroke.getKeyStroke(KeyEvent.VK_A, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
        selectAll.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireSelectAll();
            }
        });
        selectAll.setEnabled(false);
        add(selectAll);

        model.addAccessoryListListener(new AccessoryListListener() {
            @Override
            public void accessoryChanged(int accessoryId) {
                Accessory accessory = getAccessory();

                if (accessory != null) {
                    accessory.removeAccessoryListener(AccessoryTableMenu.this);
                }

                accessory = model.getSelectedAccessory();

                enableMenuItems(accessory);
                if (accessory != null) {
                    accessory.addAccessoryListener(AccessoryTableMenu.this);
                }
                setAccessory(accessory);
            }

            @Override
            public void listChanged() {
            }

            @Override
            public void pendingChangesChanged() {

            }
        });
    }

    public void addMenuListener(AccessoryTableMenuListener l) {
        menuListeners.add(l);
    }

    private void enableMenuItems(Accessory accessory) {
        if (accessory != null) {
            boolean accessoryEmpty = accessory.getAspectCount() == 0;
            boolean accessoryFull = accessory.getAspectCount() >= accessory.getMacroSize();

            insertEmptyBefore.setEnabled(!accessoryFull);
            insertEmptyAfter.setEnabled(!accessoryFull);
            pasteAfter.setEnabled(!accessoryFull);
            copy.setEnabled(!accessoryEmpty);
            cut.setEnabled(!accessoryEmpty);
            delete.setEnabled(!accessoryEmpty);
            selectAll.setEnabled(!accessoryEmpty);
        }
        else {
            insertEmptyBefore.setEnabled(false);
            insertEmptyAfter.setEnabled(false);
            pasteAfter.setEnabled(false);
            copy.setEnabled(false);
            cut.setEnabled(false);
            delete.setEnabled(false);
            selectAll.setEnabled(false);
        }
    }

    /**
     * Update the menu items on an immutable aspect.
     */
    public void updateImmutableMenuItems() {
        insertEmptyBefore.setEnabled(false);
        insertEmptyAfter.setEnabled(false);
        pasteAfter.setEnabled(false);
        copy.setEnabled(false);
        cut.setEnabled(false);
        delete.setEnabled(false);
        selectAll.setEnabled(false);
    }

    public void fireDelete() {
        for (AccessoryTableMenuListener l : menuListeners) {
            l.delete();
        }
    }

    private void fireInsertEmptyAfter() {
        for (AccessoryTableMenuListener l : menuListeners) {
            l.insertEmptyAfter();
        }
    }

    private void fireInsertEmptyBefore() {
        for (AccessoryTableMenuListener l : menuListeners) {
            l.insertEmptyBefore();
        }
    }

    private void fireSelectAll() {
        for (AccessoryTableMenuListener l : menuListeners) {
            l.selectAll();
        }
    }

    public void fireCopy() {
        for (AccessoryTableMenuListener l : menuListeners) {
            l.copy();
        }
    }

    public void fireCut() {
        for (AccessoryTableMenuListener l : menuListeners) {
            l.cut();
        }
    }

    public void firePasteAfter() {
        for (AccessoryTableMenuListener l : menuListeners) {
            l.pasteAfter();
        }
    }

    public Accessory getAccessory() {
        return accessory;
    }

    public void setAccessory(Accessory accessory) {
        this.accessory = accessory;
    }

    @Override
    public void labelChanged(String label) {
    }

    @Override
    public void macrosChanged() {
        enableMenuItems(accessory);
    }

    @Override
    public void accessoryStateChanged(int accessoryId, int aspect) {

    }
}
