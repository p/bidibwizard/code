package org.bidib.wizard.mvc.accessory.controller;

import java.util.Collection;
import java.util.LinkedList;

import javax.swing.JFrame;

import org.bidib.jbidibc.core.AddressData;
import org.bidib.jbidibc.core.DefaultMessageListener;
import org.bidib.jbidibc.core.MessageListener;
import org.bidib.jbidibc.core.enumeration.AccessoryAcknowledge;
import org.bidib.jbidibc.core.enumeration.TimeBaseUnitEnum;
import org.bidib.jbidibc.core.enumeration.TimingControlEnum;
import org.bidib.wizard.comm.Communication;
import org.bidib.wizard.comm.CommunicationFactory;
import org.bidib.wizard.mvc.accessory.controller.listener.AccessoryControllerListener;
import org.bidib.wizard.mvc.accessory.model.AccessoryModel;
import org.bidib.wizard.mvc.accessory.view.AccessoryView;
import org.bidib.wizard.mvc.accessory.view.listener.AccessoryViewListener;
import org.bidib.wizard.mvc.main.model.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AccessoryController {
    private static final Logger LOGGER = LoggerFactory.getLogger(AccessoryController.class);

    private final Collection<AccessoryControllerListener> listeners = new LinkedList<AccessoryControllerListener>();

    private final Node node;

    private final JFrame parent;

    private final int x;

    private final int y;

    private final AccessoryModel model = new AccessoryModel();

    private MessageListener messageListener;

    public AccessoryController(Node node, JFrame parent, int x, int y) {
        this.node = node;
        this.parent = parent;
        this.x = x;
        this.y = y;
    }

    public void addAccessoryControllerListener(AccessoryControllerListener listener) {
        listeners.add(listener);
    }

    public void start() {
        final Communication communication = CommunicationFactory.getInstance();

        messageListener = new DefaultMessageListener() {
            @Override
            public void csAccessoryAcknowledge(byte[] address, int decoderAddress, AccessoryAcknowledge acknowledge) {
                LOGGER.info("csAccessoryAcknowledge, decoderAddress: {}, acknowledge: {}", decoderAddress, acknowledge);
                if (decoderAddress == model.getDccAddress()) {
                    model.setAcknowledge(acknowledge);
                    // reset the address
                    // model.setDccAddress(null);
                }
            }
        };

        communication.addMessageListener(messageListener);

        final AccessoryView accessoryView = new AccessoryView(parent, model, x, y);
        accessoryView.addAccessoryViewListener(new AccessoryViewListener() {

            @Override
            public void close() {

                LOGGER.info("Close the dialog.");
                if (messageListener != null) {
                    LOGGER.info("Remove the message listener.");
                    communication.removeMessageListener(messageListener);

                    messageListener = null;
                }

                // fireClose();
            }

            @Override
            public void sendAccessoryRequest(
                AddressData dccAddress, Integer aspect, Integer switchTime, TimeBaseUnitEnum timeBaseUnit,
                TimingControlEnum timingControl) {
                LOGGER.info("Send the DCC accessory request, address: {}, aspect: {}", dccAddress, aspect);
                // reset the acknowledge to ha the new acknowledge signaled
                model.setAcknowledge(null);
                // set the DCC address because we evaluate it in the acknowledge
                model.setDccAddress(dccAddress.getAddress());

                for (AccessoryControllerListener listener : listeners) {
                    AccessoryAcknowledge accessoryAcknowledge =
                        listener
                            .sendAccessoryRequest(node, dccAddress, aspect, switchTime, timeBaseUnit, timingControl);

                    LOGGER.info("Returned acknowledge: {}", accessoryAcknowledge);

                    model.setAcknowledge(accessoryAcknowledge);
                }
            }
        });
    }
}
