package org.bidib.wizard.mvc.booster.view;

import java.awt.Component;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

import org.bidib.jbidibc.core.enumeration.CommandStationState;
import org.bidib.wizard.common.utils.ImageUtils;

public class CommandStationStateCellRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = 1L;

    Map<CommandStationState, ImageIcon> commandStationStateIcons;

    public CommandStationStateCellRenderer() {

        commandStationStateIcons = new LinkedHashMap<>();
        commandStationStateIcons.put(CommandStationState.OFF,
            ImageUtils.createImageIcon(CommandStationStateCellRenderer.class, "/icons/commandstation/DCC_OFF_00.png"));
        commandStationStateIcons.put(CommandStationState.STOP,
            ImageUtils.createImageIcon(CommandStationStateCellRenderer.class, "/icons/commandstation/DCC_STOP_01.png"));
        commandStationStateIcons.put(CommandStationState.SOFTSTOP, ImageUtils.createImageIcon(
            CommandStationStateCellRenderer.class, "/icons/commandstation/DCC_SOFTSTOP_02.png"));
        commandStationStateIcons.put(CommandStationState.GO,
            ImageUtils.createImageIcon(CommandStationStateCellRenderer.class, "/icons/commandstation/DCC_GO_03.png"));
        commandStationStateIcons.put(CommandStationState.PROG,
            ImageUtils.createImageIcon(CommandStationStateCellRenderer.class, "/icons/commandstation/DCC_PROG_08.png"));
        commandStationStateIcons.put(CommandStationState.PROGBUSY, ImageUtils.createImageIcon(
            CommandStationStateCellRenderer.class, "/icons/commandstation/DCC_PROGBUSY_09.png"));
        commandStationStateIcons.put(CommandStationState.BUSY,
            ImageUtils.createImageIcon(CommandStationStateCellRenderer.class, "/icons/commandstation/DCC_BUSY_0D.png"));
        commandStationStateIcons
            .put(CommandStationState.QUERY, ImageUtils.createImageIcon(CommandStationStateCellRenderer.class,
                "/icons/commandstation/DCC_QUERY_FF.png"));
        commandStationStateIcons.put(CommandStationState.GO_IGN_WD, ImageUtils.createImageIcon(
            CommandStationStateCellRenderer.class, "/icons/commandstation/DCC_GO_IGN_WD_04.png"));

        setHorizontalAlignment(SwingConstants.CENTER);
    }

    public Component getTableCellRendererComponent(
        JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        // provide value == null because default processing is setText(value.toString())
        super.getTableCellRendererComponent(table, null, isSelected, hasFocus, row, column);

        if (value instanceof CommandStationState) {
            CommandStationState commandStationState = (CommandStationState) value;

            Icon icon = commandStationStateIcons.get(commandStationState);
            setIcon(icon);

            setToolTipText(commandStationState.toString());
        }
        else {
            setIcon(null);
            setToolTipText(null);
        }

        return this;
    }
}