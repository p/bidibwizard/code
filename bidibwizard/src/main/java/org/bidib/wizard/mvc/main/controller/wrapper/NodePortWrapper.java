package org.bidib.wizard.mvc.main.controller.wrapper;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.bidib.wizard.mvc.main.controller.PortScripting;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.model.Port;
import org.bidib.wizard.script.engine.ScriptEngine;

public class NodePortWrapper {

    private final Map<Port<?>, ScriptEngine<PortScripting>> portRegistry = new LinkedHashMap<>();

    private final Node node;

    public NodePortWrapper(final Node node) {
        this.node = node;
    }

    public Node getNode() {
        return node;
    }

    public void addPort(Port<?> port, ScriptEngine<PortScripting> scriptEngine) {

        portRegistry.put(port, scriptEngine);
    }

    public ScriptEngine<PortScripting> removePort(Port<?> port) {

        ScriptEngine<PortScripting> scriptEngine = portRegistry.remove(port);
        return scriptEngine;
    }

    public boolean isEmpty() {
        return portRegistry.isEmpty();
    }

    public Set<Port<?>> getKeySet() {
        return portRegistry.keySet();
    }
}
