package org.bidib.wizard.mvc.main.model.function;

public interface Delayable {
    /**
     * @return the delay value
     */
    int getDelay();

    /**
     * @param delay
     *            the delay value
     */
    void setDelay(int delay);
}
