package org.bidib.wizard.mvc.cvprogrammer.model.listener;

import org.bidib.jbidibc.core.AddressData;

public interface ConfigVariableListener {
    void numberChanged(int number);

    void valueChanged(int value);

    void selectedDecoderAddressChanged(AddressData selectedDecoderAddress);
}
