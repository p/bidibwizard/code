package org.bidib.wizard.mvc.main.view.panel;

import org.bidib.wizard.mvc.main.model.listener.CvDefinitionRequestListener;

// TODO find a better name
public interface CvDefinitionRequestListenerAware {

    void addCvDefinitionRequestListener(CvDefinitionRequestListener l);
}
