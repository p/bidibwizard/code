package org.bidib.wizard.mvc.main.view.table;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.MouseEvent;
import java.util.EventObject;

import javax.swing.AbstractCellEditor;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellEditor;

import org.apache.commons.lang3.StringUtils;
import org.bidib.jbidibc.core.enumeration.PortConfigStatus;
import org.bidib.wizard.common.utils.ImageUtils;
import org.bidib.wizard.mvc.main.model.Port;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PortConfigErrorAwareEditor extends AbstractCellEditor implements TableCellEditor {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(PortConfigErrorAwareEditor.class);

    private final int portColumnIndex;

    private final JTextField textField = new JTextField();

    private int clickCountToStart = 2;

    private final JPanel basePanel;

    private final JLabel errorMarker;

    private final ImageIcon errorIcon;

    private final ImageIcon inactiveIcon;

    public PortConfigErrorAwareEditor(int portColumnIndex) {
        this.portColumnIndex = portColumnIndex;

        errorIcon = ImageUtils.createImageIcon(PortConfigErrorAwareRenderer.class, "/icons/error-leaf.png");
        inactiveIcon = ImageUtils.createImageIcon(PortConfigErrorAwareRenderer.class, "/icons/cancel.png");

        errorMarker = new JLabel();
        errorMarker.setOpaque(false);
        errorMarker.setFocusable(false);
        basePanel = new JPanel(new BorderLayout(2, 0)) {
            private static final long serialVersionUID = 1L;

            @Override
            public void requestFocus() {
                super.requestFocus();
                if (textField != null) {
                    textField.requestFocus();
                }
            }
        };
        basePanel.setOpaque(false);
        basePanel.add(textField, BorderLayout.CENTER);
        basePanel.add(errorMarker, BorderLayout.WEST);
        basePanel.setFocusable(false);
    }

    /**
     * Specifies the number of clicks needed to start editing.
     * 
     * @param count
     *            an int specifying the number of clicks needed to start editing
     * @see #getClickCountToStart
     */
    public void setClickCountToStart(int count) {
        clickCountToStart = count;
    }

    /**
     * Returns the number of clicks needed to start editing.
     * 
     * @return the number of clicks needed to start editing
     */
    public int getClickCountToStart() {
        return clickCountToStart;
    }

    /**
     * Returns true if <code>anEvent</code> is <b>not</b> a <code>MouseEvent</code>. Otherwise, it returns true if the
     * necessary number of clicks have occurred, and returns false otherwise.
     * 
     * @param anEvent
     *            the event
     * @return true if cell is ready for editing, false otherwise
     * @see #setClickCountToStart
     * @see #shouldSelectCell
     */
    public boolean isCellEditable(EventObject anEvent) {
        if (anEvent instanceof MouseEvent) {
            return ((MouseEvent) anEvent).getClickCount() >= clickCountToStart;
        }
        return true;
    }

    @Override
    public Object getCellEditorValue() {
        return textField.getText();
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {

        textField.setText(null);
        textField.setToolTipText(null);

        if (value instanceof Port<?>) {
            Port<?> port = (Port<?>) table.getModel().getValueAt(row, portColumnIndex);
            if (port != null) {
                PortConfigStatus configStatus = port.getConfigStatus();
                // setEnabled(port.isEnabled());
                if (StringUtils.isBlank(port.getLabel())) {
                    // String text = Resources.getString(port.getClass(), "label") + "_" + row;
                    textField.setText(null);
                }
                else {
                    textField.setText(port.getLabel());
                }
                if (port.isInactive()) {
                    // the port is inactive
                    errorMarker.setIcon(inactiveIcon);
                    textField.setToolTipText("This port is inactive");
                }
                else if (port.isEnabled()) {
                    if (PortConfigStatus.CONFIG_ERROR.equals(configStatus)) {
                        // error detected
                        errorMarker.setIcon(errorIcon);
                        textField.setToolTipText("This port has an error: " + port.getPortConfigErrorCode());
                    }
                }
            }
        }
        else {
            LOGGER.warn("Provided value is not a port: {}", value);
        }
        return basePanel;
    }

}
