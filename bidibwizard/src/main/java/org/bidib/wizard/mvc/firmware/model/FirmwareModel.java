package org.bidib.wizard.mvc.firmware.model;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.bidib.jbidibc.exchange.firmware.FirmwareNode;
import org.bidib.wizard.mvc.firmware.model.listener.FirmwareModelListener;
import org.bidib.wizard.mvc.main.model.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.beans.Model;

public class FirmwareModel extends Model {
    private static final Logger LOGGER = LoggerFactory.getLogger(FirmwareModel.class);

    private static final long serialVersionUID = 1L;

    public static final String PROPERTYNAME_IN_PROGRESS = "inProgress";

    public static final String PROPERTYNAME_IDLE = "idle";

    public static final String PROPERTYNAME_IDLE_AND_VALID = "idleAndValid";

    public static final String PROPERTYNAME_UPDATE_STATUS = "updateStatus";

    public static final String PROPERTYNAME_CV_DEFINITION_FILES = "cvDefinitionFiles";

    public static final String PROPERTYNAME_UPDATE_VERSION = "updateVersion";

    private final Collection<FirmwareModelListener> listeners = new LinkedList<FirmwareModelListener>();

    private boolean inProgress = false;

    private int progressValue = 0;

    private Node node;

    private String nodeName;

    private String productName;

    private String uniqueId;

    private String nodeCurrentVersion;

    private boolean errorDetected = false;

    private String firmwareArchivePath;

    private String cvDefinitionArchivePath;

    private List<FirmwareNode> firmwareFiles;

    private List<String> cvDefinitionFiles;

    private UpdateStatus updateStatus = UpdateStatus.NONE;

    private boolean isBootloaderRootNode;

    private String updateVersion;

    public void addFirmwareModelListener(FirmwareModelListener l) {
        listeners.add(l);
    }

    public void removeFirmwareModelListener(FirmwareModelListener l) {
        listeners.remove(l);
    }

    public boolean isInProgress() {
        return inProgress;
    }

    public void setInProgress(boolean inProgress) {
        boolean oldValue = this.inProgress;
        boolean oldIdleAndValid = isIdleAndValid();

        this.inProgress = inProgress;
        firePropertyChange(PROPERTYNAME_IN_PROGRESS, oldValue, inProgress);

        firePropertyChange(PROPERTYNAME_IDLE, !oldValue, !inProgress);

        firePropertyChange(PROPERTYNAME_IDLE_AND_VALID, oldIdleAndValid, isIdleAndValid());
    }

    public void setIdle(boolean idle) {
        boolean oldValue = !this.inProgress;
        boolean oldIdleAndValid = isIdleAndValid();

        this.inProgress = !idle;
        firePropertyChange(PROPERTYNAME_IDLE, oldValue, inProgress);

        firePropertyChange(PROPERTYNAME_IDLE_AND_VALID, oldIdleAndValid, isIdleAndValid());
    }

    public boolean isIdle() {
        return !inProgress;
    }

    public boolean isIdleAndValid() {
        boolean isIdleAndValid = !inProgress && !UpdateStatus.NODE_LOST.equals(updateStatus);
        LOGGER.info("isIdleAndValid: {}, inProgress: {}, updateStatus: {}", isIdleAndValid, inProgress, updateStatus);
        return isIdleAndValid;
    }

    public int getProgressValue() {
        return progressValue;
    }

    public void setProgressValue(int progressValue) {
        if (this.progressValue != progressValue) {
            this.progressValue = progressValue;
            fireProgressChanged(progressValue);
        }
    }

    public void addProcessingStatus(String processingStatus, final int style, Object... args) {

        fireProcessingStatusChanged(processingStatus, style, args);
    }

    /**
     * @return the node
     */
    public Node getNode() {
        return node;
    }

    /**
     * @param node
     *            the node to set
     */
    public void setNode(Node node) {
        this.node = node;
    }

    /**
     * @return the nodeName
     */
    public String getNodeName() {
        return nodeName;
    }

    /**
     * @param nodeName
     *            the nodeName to set
     */
    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    /**
     * @return the productName
     */
    public String getProductName() {
        return productName;
    }

    /**
     * @param productName
     *            the productName to set
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     * @return the unique id
     */
    public String getUniqueId() {
        return uniqueId;
    }

    /**
     * @param uniqueId
     *            the unique id to set
     */
    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    /**
     * @return the firmware version
     */
    public String getNodeCurrentVersion() {
        return nodeCurrentVersion;
    }

    /**
     * @param version
     *            the current firmware version on the node to set
     */
    public void setNodeCurrentVersion(String version) {
        this.nodeCurrentVersion = version;
    }

    /**
     * @return the errorDetected
     */
    public boolean isErrorDetected() {
        return errorDetected;
    }

    /**
     * Set the firmware file to use
     * 
     * @param firmwareFiles
     *            the files
     * @param firmwareArchivePath
     *            the path to the archive
     */
    public void setFirmwareFiles(String firmwareArchivePath, List<FirmwareNode> firmwareFiles) {
        this.firmwareArchivePath = firmwareArchivePath;
        this.firmwareFiles = firmwareFiles;

        fireFirmwareFileChanged();
    }

    /**
     * @return the list of firmwareFiles
     */
    public List<FirmwareNode> getFirmwareFiles() {
        List<FirmwareNode> files = new LinkedList<>();
        files.addAll(firmwareFiles);
        return Collections.unmodifiableList(files);
    }

    /**
     * @return the cvDefinitionFiles
     */
    public List<String> getCvDefinitionFiles() {
        return Collections.unmodifiableList(cvDefinitionFiles);
    }

    /**
     * @param cvDefinitionArchivePath
     *            the cv definition archive path
     * @param cvDefinitionFiles
     *            the cvDefinitionFiles to set
     */
    public void setCvDefinitionFiles(String cvDefinitionArchivePath, List<String> cvDefinitionFiles) {
        this.cvDefinitionArchivePath = cvDefinitionArchivePath;
        List<String> oldValue = this.cvDefinitionFiles;

        if (cvDefinitionFiles == null) {
            cvDefinitionFiles = Collections.emptyList();
        }

        this.cvDefinitionFiles = cvDefinitionFiles;

        firePropertyChange(PROPERTYNAME_CV_DEFINITION_FILES, oldValue, cvDefinitionFiles);
    }

    /**
     * @return the cvDefinitionArchivePath
     */
    public String getCvDefinitionArchivePath() {
        return cvDefinitionArchivePath;
    }

    /**
     * @return the firmwareArchivePath
     */
    public String getFirmwareArchivePath() {
        return firmwareArchivePath;
    }

    /**
     * @return the updateStatus
     */
    public UpdateStatus getUpdateStatus() {
        return updateStatus;
    }

    /**
     * @param updateStatus
     *            the updateStatus to set
     */
    public void setUpdateStatus(UpdateStatus updateStatus) {
        UpdateStatus oldValue = this.updateStatus;
        boolean oldIdleAndValid = isIdleAndValid();

        this.updateStatus = updateStatus;
        firePropertyChange(PROPERTYNAME_UPDATE_STATUS, oldValue, updateStatus);

        firePropertyChange(PROPERTYNAME_IDLE_AND_VALID, oldIdleAndValid, isIdleAndValid());
    }

    /**
     * @return the isBootloaderRootNode
     */
    public boolean isBootloaderRootNode() {
        return isBootloaderRootNode;
    }

    /**
     * @param isBootloaderRootNode
     *            the isBootloaderRootNode to set
     */
    public void setBootloaderRootNode(boolean isBootloaderRootNode) {
        this.isBootloaderRootNode = isBootloaderRootNode;
    }

    /**
     * @return the updateVersion
     */
    public String getUpdateVersion() {
        return updateVersion;
    }

    /**
     * @param updateVersion
     *            the updateVersion to set
     */
    public void setUpdateVersion(String updateVersion) {
        String oldValue = this.updateVersion;

        this.updateVersion = updateVersion;
        firePropertyChange(PROPERTYNAME_UPDATE_VERSION, oldValue, updateVersion);
    }

    private void fireProcessingStatusChanged(String processingStatus, final int style, Object... args) {
        for (FirmwareModelListener l : listeners) {
            l.processingStatusChanged(processingStatus, style, args);
        }
    }

    private void fireProgressChanged(int progressValue) {
        for (FirmwareModelListener l : listeners) {
            l.progressValueChanged(progressValue);
        }
    }

    private void fireFirmwareFileChanged() {
        for (FirmwareModelListener l : listeners) {
            l.firmwareFileChanged();
        }
    }
}
