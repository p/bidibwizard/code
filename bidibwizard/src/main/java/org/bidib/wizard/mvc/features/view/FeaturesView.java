package org.bidib.wizard.mvc.features.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Collection;
import java.util.HashSet;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.WindowConstants;

import org.bidib.jbidibc.core.Feature;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.features.model.FeaturesModel;
import org.bidib.wizard.mvc.features.view.listener.FeaturesViewListener;
import org.bidib.wizard.mvc.features.view.panel.FeaturesPanel;
import org.bidib.wizard.mvc.features.view.panel.listener.FeaturesWriteListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.forms.builder.ButtonBarBuilder;
import com.jgoodies.forms.factories.Borders;

public class FeaturesView extends JDialog {
    private static final Logger LOGGER = LoggerFactory.getLogger(FeaturesView.class);

    private static final long serialVersionUID = 1L;

    private final Collection<FeaturesViewListener> listeners = new HashSet<FeaturesViewListener>();

    private final JButton saveButton = new JButton(Resources.getString(getClass(), "save"));

    private final JButton cancelButton = new JButton(Resources.getString(getClass(), "cancel"));

    private final FeaturesPanel featuresPanel;

    public FeaturesView(JFrame parent, FeaturesModel model, int x, int y) {
        super(parent);
        // this.model = model;

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                cancel();
            }
        });
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        setLocation(x, y);
        setResizable(true);
        setTitle(Resources.getString(getClass(), "title", model.getNode()));

        JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.setPreferredSize(new Dimension(600, 500));

        featuresPanel = new FeaturesPanel(model);

        JScrollPane scroll = new JScrollPane();
        scroll.setViewportView(featuresPanel.getTable());
        mainPanel.add(scroll, BorderLayout.CENTER);

        featuresPanel.addFeaturesWriteListener(new FeaturesWriteListener() {
            @Override
            public void write(Collection<Feature> features) {
                fireWrite(features);
            }
        });

        // prepare the buttons
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                save();
            }
        });

        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cancel();
            }
        });

        JPanel buttons =
            new ButtonBarBuilder().addGlue().addButton(saveButton, cancelButton).border(Borders.DLU2).build();

        mainPanel.add(buttons, BorderLayout.SOUTH);

        getContentPane().add(mainPanel);
        pack();

        // setMinimumSize(getSize());
        // setSize(new Dimension(getSize().width + 10, getSize().height));
        // setPreferredSize(new Dimension(600, 200));

        int buttonWidth = Math.max(saveButton.getWidth(), cancelButton.getWidth());

        saveButton.setPreferredSize(new Dimension(buttonWidth, saveButton.getSize().height));
        cancelButton.setPreferredSize(new Dimension(buttonWidth, cancelButton.getSize().height));
    }

    public void addFeaturesViewListener(FeaturesViewListener listener) {
        listeners.add(listener);
    }

    private void save() {
        LOGGER.debug("Save the changed features.");

        // write the features to the node
        featuresPanel.writeFeatures();

        // close the dialog
        setVisible(false);
        fireClose();
    }

    private void cancel() {
        setVisible(false);
        fireClose();
    }

    private void fireClose() {
        for (FeaturesViewListener listener : listeners) {
            listener.close();
        }
    }

    private void fireWrite(Collection<Feature> features) {
        for (FeaturesViewListener listener : listeners) {
            listener.write(features);
        }
    }
}
