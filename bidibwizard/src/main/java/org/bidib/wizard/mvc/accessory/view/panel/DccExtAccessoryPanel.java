package org.bidib.wizard.mvc.accessory.view.panel;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JButton;

import org.bidib.jbidibc.core.AddressData;
import org.bidib.jbidibc.core.enumeration.AddressTypeEnum;
import org.bidib.jbidibc.core.enumeration.TimeBaseUnitEnum;
import org.bidib.jbidibc.core.enumeration.TimingControlEnum;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.accessory.model.AccessoryBeanModel;
import org.bidib.wizard.mvc.accessory.model.AccessoryModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.validation.util.PropertyValidationSupport;

public class DccExtAccessoryPanel extends AbstractAccessoryPanel<AccessoryBeanModel> {
    private static final Logger LOGGER = LoggerFactory.getLogger(DccExtAccessoryPanel.class);

    private static final String ENCODED_LOCAL_COLUMN_SPECS =
        "pref, 3dlu, pref, 3dlu, pref, 3dlu, pref, 3dlu, pref:grow";

    // private ValueModel timingControlModel;

    private final AccessoryBeanModel accessoryBeanModel;

    // private JComponent[] timingControlButtons;

    private JButton aspectButtons[];

    public DccExtAccessoryPanel(final AccessoryModel accessoryModel) {
        super(accessoryModel);
        accessoryBeanModel = new AccessoryBeanModel();
    }

    @Override
    protected AccessoryBeanModel getAccessoryBeanModel() {
        return accessoryBeanModel;
    }

    @Override
    protected void addSpecificComponents(final DefaultFormBuilder builder) {
        // goto next line
        builder.nextLine();

        // // timing control
        // builder.append(Resources.getString(AbstractAccessoryPanel.class, "timingControl"));
        // timingControlModel =
        // new PropertyAdapter<AccessoryBeanModel>(accessoryBeanModel, AccessoryBeanModel.PROPERTYNAME_TIMING_CONTROL,
        // true);
        // timingControlButtons = new JComponent[TimingControlEnum.values().length];
        // int index = 0;
        //
        // DefaultFormBuilder timingControlBuilder = new DefaultFormBuilder(new FormLayout("pref, 3dlu, pref"));
        //
        // for (TimingControlEnum timingControl : TimingControlEnum.values()) {
        //
        // JRadioButton radio =
        // BasicComponentFactory.createRadioButton(timingControlModel, timingControl,
        // Resources.getString(TimingControlEnum.class, timingControl.getKey()));
        // timingControlButtons[index++] = radio;
        //
        // // add radio button
        // timingControlBuilder.append(radio);
        // }
        // builder.append(timingControlBuilder.build(), 7);
        //
        // builder.nextLine();

        DefaultFormBuilder localBuilder = new DefaultFormBuilder(new FormLayout(ENCODED_LOCAL_COLUMN_SPECS));
        // add the aspects
        aspectButtons = new JButton[3];
        for (int aspect = 0; aspect < 3; aspect++) {

            JButton aspectButton = new JButton(new AbstractAction(
                "Aspect " + aspect) {
                private static final long serialVersionUID = 1L;

                @Override
                public void actionPerformed(ActionEvent e) {
                    // set aspect button was pressed
                    Integer aspect = (Integer) ((JButton) e.getSource()).getClientProperty("aspect");
                    LOGGER.info("Pressed button: {}, aspect: {}", e.getActionCommand(), aspect);

                    addLogText("Send DCC accessory request, address: {}, aspect: {}, switch time: {}, time base: {}",
                        accessoryBeanModel.getDccAddress(), aspect, accessoryBeanModel.getSwitchTime(),
                        Resources.getString(TimeBaseUnitEnum.class, accessoryBeanModel.getTimeBaseUnit().getKey()));

                    // set the aspect triggers the property change listener
                    accessoryBeanModel.setAcknowledge(null);
                    accessoryBeanModel.setAspect(aspect);
                    // send the request
                    AddressData addressData =
                        new AddressData(accessoryBeanModel.getDccAddress(), AddressTypeEnum.EXTENDED_ACCESSORY);
                    sendRequest(addressData, accessoryBeanModel.getAspect(), accessoryBeanModel.getSwitchTime(),
                        accessoryBeanModel.getTimeBaseUnit(), TimingControlEnum.COIL_ON_OFF);
                }
            });
            aspectButton.putClientProperty("aspect", Integer.valueOf(aspect));
            aspectButton.setEnabled(false);
            aspectButtons[aspect] = aspectButton;
            localBuilder.append(aspectButton);
        }
        builder.append(localBuilder.build(), 9);
    }

    @Override
    protected void addSpecificValidation() {

        // if the model is valid, the aspect buttons are enabled.
        for (int aspect = aspectButtons.length - 1; aspect > -1; aspect--) {
            PropertyConnector.connect(accessoryValidationModel, AccessoryValidationResultModel.PROPERTY_VALID_STATE,
                aspectButtons[aspect], "enabled");
        }
    }

    @Override
    protected void validateSpecificPanel(PropertyValidationSupport support) {

    }

    private static final int MAX_ADDRESS = 511;

    @Override
    protected int getMaxAddress() {
        return MAX_ADDRESS;
    }
}
