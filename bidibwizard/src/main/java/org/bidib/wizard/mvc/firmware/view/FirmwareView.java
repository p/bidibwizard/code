package org.bidib.wizard.mvc.firmware.view;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Collection;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.WindowConstants;

import org.bidib.jbidibc.core.utils.CollectionUtils;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.dialog.EscapeDialog;
import org.bidib.wizard.mvc.firmware.model.FirmwareModel;
import org.bidib.wizard.mvc.firmware.model.listener.FirmwareModelListener;
import org.bidib.wizard.mvc.firmware.view.listener.FirmwareViewListener;
import org.bidib.wizard.mvc.firmware.view.panel.FirmwareUpdatePanel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FirmwareView extends EscapeDialog {
    private static final Logger LOGGER = LoggerFactory.getLogger(FirmwareView.class);

    private static final long serialVersionUID = 1L;

    private final Collection<FirmwareViewListener> listeners = new LinkedList<FirmwareViewListener>();

    private final JButton closeButton = new JButton(Resources.getString(getClass(), "close"));

    private final FirmwareModel firmwareModel;

    public FirmwareView(JFrame parent, final FirmwareModel firmwareModel) {
        super(parent, Resources.getString(FirmwareView.class, "title"), true);
        this.firmwareModel = firmwareModel;

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                tryCloseDialog();
            }
        });
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

        FirmwareUpdatePanel mainPanel = new FirmwareUpdatePanel();
        mainPanel.setCloseButton(closeButton);
        mainPanel.setFirmwareModel(firmwareModel);

        getContentPane().setLayout(new BorderLayout());
        getContentPane().add(mainPanel.createPanel());
        pack();

        setMinimumSize(getPreferredSize());

        closeButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                tryCloseDialog();
            }
        });

        firmwareModel.addFirmwareModelListener(new FirmwareModelListener() {

            @Override
            public void progressValueChanged(int progressValue) {
            }

            @Override
            public void firmwareFileChanged() {
                if (CollectionUtils.hasElements(firmwareModel.getFirmwareFiles())) {
                    fireUpdate();
                }
                else {
                    LOGGER.info("No firmware files available.");
                }
            }

            @Override
            public void processingStatusChanged(String processingStatus, final int style, Object... args) {
            }

        });
    }

    @Override
    protected void performCancelAction(KeyEvent e) {
        tryCloseDialog();
    }

    private void tryCloseDialog() {
        if (!firmwareModel.isInProgress()) {
            setVisible(false);
            try {
                fireClose();
            }
            catch (Exception ex) {
                LOGGER.warn("Close firmware update dialog failed.", ex);
            }
        }
        else {
            LOGGER.warn("Firmware update is in progress! Dialog is not closed!");
        }
    }

    public void addFirmwareViewListener(FirmwareViewListener l) {
        listeners.add(l);
    }

    private void fireClose() {
        LOGGER.info("Close the firmware view.");
        for (FirmwareViewListener l : listeners) {
            l.close();
        }
    }

    private void fireUpdate() {
        for (FirmwareViewListener l : listeners) {
            l.updateFirmware();
        }
    }
}
