package org.bidib.wizard.mvc.simulation.view.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Rectangle2D;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import org.apache.commons.lang.StringUtils;
import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.Feature;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.core.utils.NodeUtils;
import org.bidib.jbidibc.simulation.SimulatorNode;
import org.bidib.wizard.comm.BacklightPortStatus;
import org.bidib.wizard.comm.InputPortStatus;
import org.bidib.wizard.comm.LightPortStatus;
import org.bidib.wizard.dk.nullesoft.Airlog.LED;
import org.bidib.wizard.mvc.main.model.BacklightPort;
import org.bidib.wizard.mvc.main.model.InputPort;
import org.bidib.wizard.mvc.main.model.LightPort;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.simulation.events.BacklightPortStatusEvent;
import org.bidib.wizard.simulation.events.InputPortSetStatusEvent;
import org.bidib.wizard.simulation.events.InputPortStatusEvent;
import org.bidib.wizard.simulation.events.LightPortStatusEvent;
import org.bushe.swing.event.EventBus;
import org.bushe.swing.event.annotation.AnnotationProcessor;
import org.bushe.swing.event.annotation.EventSubscriber;
import org.jdesktop.swingx.JXButton;
import org.jdesktop.swingx.painter.AbstractLayoutPainter.HorizontalAlignment;
import org.jdesktop.swingx.painter.AbstractLayoutPainter.VerticalAlignment;
import org.jdesktop.swingx.painter.ShapePainter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.debug.FormDebugPanel;
import com.jgoodies.forms.layout.FormLayout;
import com.vlsolutions.swing.docking.DockKey;

public class OneDMXPanel extends AbstractSimulatorNodePanel {
    private static final Logger LOGGER = LoggerFactory.getLogger(OneDMXPanel.class);

    private final DockKey DOCKKEY;

    private final SimulationViewContainer container;

    private Map<Integer, LED> lightPortLeds = new HashMap<>();

    private Map<Integer, LedButton> inputPortLeds = new HashMap<>();

    private Map<Integer, LED> backlightPortLeds = new HashMap<>();

    public OneDMXPanel(final SimulationViewContainer container, final Node node) {
        super(node);
        this.container = container;
        String uuid = NodeUtils.getUniqueIdAsString(node.getUniqueId());
        DOCKKEY = new DockKey(getClass().getSimpleName() + "-" + StringUtils.trimToEmpty(uuid));

        // enable floating
        DOCKKEY.setFloatEnabled(true);
    }

    @Override
    public void createComponents(SimulatorNode simulator) {
        DefaultFormBuilder formBuilder = null;
        boolean debug = false;
        if (debug) {
            JPanel panel = new FormDebugPanel();
            formBuilder = new DefaultFormBuilder(new FormLayout("pref, fill:50dlu:grow"), panel);
        }
        else {
            JPanel panel = new JPanel(new BorderLayout());
            formBuilder = new DefaultFormBuilder(new FormLayout("pref, fill:50dlu:grow"), panel);
        }

        Feature lightPortCount = simulator.getFeature(BidibLibrary.FEATURE_CTRL_LIGHT_COUNT);
        if (lightPortCount != null && lightPortCount.getValue() > 0) {
            JPanel lightPorts = new JPanel();
            lightPorts.setBorder(new EmptyBorder(5, 5, 5, 5));
            lightPorts.setLayout(new GridLayout(0, 8, 5, 5));

            LOGGER.info("Initializing lightPort LEDs.");

            for (int i = 0; i < lightPortCount.getValue(); i++) {
                LED led = new LED(Color.LIGHT_GRAY, Color.GREEN, true);
                led.setSize(10, 10);
                led.setToolTipText("Light " + i);

                lightPortLeds.put(Integer.valueOf(i), led);

                lightPorts.add(led);
            }

            formBuilder.append(new JLabel("Light ports"), lightPorts);
        }
        else {
            LOGGER.warn("No configured LightPorts available.");
        }

        Feature inputPortCount = simulator.getFeature(BidibLibrary.FEATURE_CTRL_INPUT_COUNT);
        if (inputPortCount != null && inputPortCount.getValue() > 0) {
            JPanel inputPorts = new JPanel();

            inputPorts.setBorder(new EmptyBorder(5, 5, 5, 5));
            inputPorts.setLayout(new GridLayout(0, 8, 5, 5));

            LOGGER.info("Initializing inputPort LEDs.");
            for (int portNum = 0; portNum < inputPortCount.getValue(); portNum++) {
                final LedButton led = new LedButton(portNum);
                led.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        toggleInputPort(led.getPortNum());
                    }
                });
                led.setSize(new Dimension(12, 12));
                led.setMinimumSize(new Dimension(12, 12));
                led.setPreferredSize(new Dimension(12, 12));
                led.setMargin(new Insets(5, 5, 5, 5));

                led.setToolTipText("Input " + portNum);

                led.setForeground(Color.RED);

                Rectangle2D.Double rect = new Rectangle2D.Double(0, 0, 12, 10);
                ShapePainter painter = new ShapePainter(rect);
                painter.setHorizontalAlignment(HorizontalAlignment.CENTER);
                painter.setVerticalAlignment(VerticalAlignment.CENTER);
                painter.setFillPaint(led.getForeground());
                led.setForegroundPainter(painter);
                led.setBackgroundPainter(painter);
                led.setBorder(new LineBorder(Color.DARK_GRAY, 1));

                LOGGER.info("Current fg painter: {}", painter);

                inputPortLeds.put(Integer.valueOf(portNum), led);

                inputPorts.add(led);
            }

            formBuilder.append(new JLabel("Input ports"), inputPorts);
        }
        else {
            LOGGER.warn("No configured InputPorts available.");
        }

        Feature backlightPortCount = simulator.getFeature(BidibLibrary.FEATURE_CTRL_BACKLIGHT_COUNT);
        if (backlightPortCount != null && backlightPortCount.getValue() > 0) {
            JPanel backlightPorts = new JPanel();
            backlightPorts.setBorder(new EmptyBorder(5, 5, 5, 5));

            backlightPorts.setLayout(new GridLayout(0, 8, 5, 5));

            LOGGER.info("Initializing backlightPort LEDs.");
            for (int i = 0; i < backlightPortCount.getValue(); i++) {
                LED led = new LED(Color.LIGHT_GRAY, Color.GREEN, true);
                led.setSize(10, 10);
                led.setToolTipText("Backlight " + i);

                backlightPortLeds.put(Integer.valueOf(i), led);

                backlightPorts.add(led);
            }

            formBuilder.append(new JLabel("Backlight ports"), backlightPorts);
        }
        else {
            LOGGER.warn("No configured Backlight" + "Ports available.");
        }

        contentPanel = formBuilder.build();

        AnnotationProcessor.process(this);

        // query the status of the input ports
        simulator.queryStatus(InputPort.class);
        simulator.queryStatus(LightPort.class);
        simulator.queryStatus(BacklightPort.class);
    }

    private static final class LedButton extends JXButton {
        private static final long serialVersionUID = 1L;

        private int portNum;

        private InputPortStatus status = InputPortStatus.OFF;

        public LedButton(int portNum) {
            super();
            this.portNum = portNum;
        }

        public int getPortNum() {
            return portNum;
        }

        /**
         * @return the status
         */
        public InputPortStatus getStatus() {
            return status;
        }

        /**
         * @param status
         *            the status to set
         */
        public void setStatus(InputPortStatus status) {
            this.status = status;

            if (status == InputPortStatus.OFF) {
                ((ShapePainter) getBackgroundPainter()).setFillPaint(Color.LIGHT_GRAY);
                ((ShapePainter) getForegroundPainter()).setFillPaint(Color.LIGHT_GRAY);
            }
            else {
                ((ShapePainter) getBackgroundPainter()).setFillPaint(Color.RED);
                ((ShapePainter) getForegroundPainter()).setFillPaint(Color.RED);
            }

        }
    }

    @Override
    public DockKey getDockKey() {
        return DOCKKEY;
    }

    @Override
    public void stop() {
        AnnotationProcessor.unprocess(this);

        container.close(this);
    }

    private void toggleInputPort(int portNum) {
        LedButton ledButton = inputPortLeds.get(portNum);
        String nodeAddress = ByteUtils.bytesToHex(getNode().getNode().getAddr());

        InputPortSetStatusEvent inputPortSetStatusEvent =
            new InputPortSetStatusEvent(nodeAddress, portNum, ledButton.getStatus());
        LOGGER.info("Publish the inputPortSetStatusEvent: {}", inputPortSetStatusEvent);
        EventBus.publish(inputPortSetStatusEvent);
    }

    @EventSubscriber(eventClass = InputPortStatusEvent.class)
    public void inputPortStatusChanged(InputPortStatusEvent statusEvent) {
        LOGGER.info("The inputport status has changed, portNumber: {}, status: {}, node: {}",
            statusEvent.getPortNumber(), statusEvent.getStatus(), getNode());

        // check if the node address matches
        if (!isMatchingAddress(statusEvent.getNodeAddr())) {
            return;
        }

        int id = statusEvent.getPortNumber();
        InputPortStatus status = statusEvent.getStatus();

        LedButton led = inputPortLeds.get(Integer.valueOf(id));

        if (led != null) {
            LOGGER.trace("Found input to switch.");
            led.setStatus(status);
            // if (status == InputPortStatus.OFF) {
            // //led.usePrimary();
            // ((ShapePainter)led.getBackgroundPainter()).setFillPaint(Color.LIGHT_GRAY);
            // ((ShapePainter)led.getForegroundPainter()).setFillPaint(Color.LIGHT_GRAY);
            // }
            // else {
            // // led.useSecondary();
            // ((ShapePainter)led.getBackgroundPainter()).setFillPaint(Color.RED);
            // ((ShapePainter)led.getForegroundPainter()).setFillPaint(Color.RED);
            // }
        }
        else {
            LOGGER.trace("Led not found.");
        }
    }

    @EventSubscriber(eventClass = LightPortStatusEvent.class)
    public void lightPortStatusChanged(LightPortStatusEvent statusEvent) {
        LOGGER.info("The lightport status has changed, portNumber: {}, status: {}, node: {}",
            statusEvent.getPortNumber(), statusEvent.getStatus(), getNode());

        // check if the node address matches
        if (!isMatchingAddress(statusEvent.getNodeAddr())) {
            return;
        }

        LightPortStatus status = statusEvent.getStatus();

        int id = statusEvent.getPortNumber();

        LED led = lightPortLeds.get(Integer.valueOf(id));

        if (led != null) {
            LOGGER.trace("Found led to switch.");
            if (status == LightPortStatus.OFF) {
                led.usePrimary();
            }
            else {
                led.useSecondary();
            }
        }
        else {
            LOGGER.trace("Led not found.");
        }
    }

    @EventSubscriber(eventClass = BacklightPortStatusEvent.class)
    public void backlightPortStatusChanged(BacklightPortStatusEvent statusEvent) {
        LOGGER.info("The backlightport status has changed, port: {}, status: {}, node: {}", statusEvent.getPort(),
            statusEvent.getStatus(), getNode());

        // check if the node address matches
        if (!isMatchingAddress(statusEvent.getNodeAddr())) {
            return;
        }

        BacklightPort port = statusEvent.getPort();
        BacklightPortStatus status = statusEvent.getStatus();

        int id = port.getId();

        LED led = backlightPortLeds.get(Integer.valueOf(id));

        if (led != null) {
            LOGGER.trace("Found led to switch.");
            if (status == BacklightPortStatus.START) {
                led.usePrimary();
            }
            else {
                led.useSecondary();
            }
        }
        else {
            LOGGER.trace("Led not found.");
        }
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("OneDMXPanel, dockKey: ");
        sb.append(DOCKKEY).append(", node: ").append(getNode());

        return sb.toString();
    }
}
