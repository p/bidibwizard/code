package org.bidib.wizard.mvc.firmware.model.listener;

public interface FirmwareModelListener {

    /**
     * Signal changes of the processing status.
     * 
     * @param processingStatus
     *            the processing status text
     * @param style
     *            the style (0: normal, 1: red and bold)
     * @param args
     *            the args that are replaced in the processing status text
     */
    void processingStatusChanged(String processingStatus, final int style, Object... args);

    /**
     * The progress value has changed.
     * 
     * @param progressValue
     *            the new progress value
     */
    void progressValueChanged(int progressValue);

    /**
     * Signals that the selected firmware file has changed.
     */
    void firmwareFileChanged();
}
