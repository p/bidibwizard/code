package org.bidib.wizard.mvc.script.view.wizard;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.bidib.jbidibc.core.Node;
import org.bidib.schema.nodescriptsources.NodeScriptSources;
import org.bidib.wizard.common.context.ApplicationContext;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.main.view.table.AbstractEmptyTable;
import org.bidib.wizard.mvc.script.controller.NodeScriptKeywords;
import org.bidib.wizard.mvc.script.model.NodeScriptData;
import org.bidib.wizard.mvc.script.model.NodeScriptSource;
import org.bidib.wizard.mvc.script.model.NodeScriptSource.ScriptSource;
import org.bidib.wizard.mvc.script.view.utils.NodeRequirementException;
import org.bidib.wizard.mvc.script.view.utils.NodeScriptUtils;
import org.bidib.wizard.script.node.NodeScriptContextKeys;
import org.bidib.wizard.utils.SearchPathUtils;
import org.jdesktop.swingx.JXHyperlink;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.adapter.SingleListSelectionAdapter;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.common.collect.ArrayListModel;
import com.jgoodies.forms.factories.DefaultComponentFactory;
import com.jidesoft.dialog.AbstractPage;
import com.jidesoft.dialog.ButtonEvent;
import com.jidesoft.dialog.ButtonNames;
import com.jidesoft.dialog.PageEvent;
import com.jidesoft.dialog.PageListener;
import com.jidesoft.swing.JideBoxLayout;
import com.jidesoft.swing.JideSplitPane;
import com.jidesoft.swing.JideTabbedPane;
import com.jidesoft.wizard.DefaultWizardPage;

public class SelectApplicationPage extends DefaultWizardPage {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(SelectApplicationPage.class);

    private final ApplicationContext scriptContext;

    private final Map<NodeScriptSource, NodeScriptSources> mapNodeScriptSources;

    private JideTabbedPane tabbedPane;

    private SelectionInList<NodeScriptData> applicationSelectionUser;

    private SelectionInList<NodeScriptData> applicationSelectionInstallation;

    private ArrayListModel<NodeScriptData> applicationsUser = new ArrayListModel<>();

    private ArrayListModel<NodeScriptData> applicationsInstallation = new ArrayListModel<>();

    private AbstractEmptyTable tableApplicationsUser;

    private AbstractEmptyTable tableApplicationsInstallation;

    private JPanel instructionPanel;

    private JLabel instruction;

    private JXHyperlink link;

    public SelectApplicationPage(Icon icon, final ApplicationContext scriptContext,
        final Map<NodeScriptSource, NodeScriptSources> mapNodeScriptSources) {
        super(Resources.getString(SelectApplicationPage.class, "title"),
            Resources.getString(SelectApplicationPage.class, "description"), icon);

        this.scriptContext = scriptContext;
        this.mapNodeScriptSources = mapNodeScriptSources;
    }

    @Override
    public int getSelectedStepIndex() {
        return 2;
    }

    @Override
    protected void initContentPane() {
        super.initContentPane();

        instruction = new JLabel();
        instruction.setVerticalAlignment(SwingConstants.TOP);
        instruction.setPreferredSize(new Dimension(400, 50));

        link = new JXHyperlink();

        // create the selection for applications
        applicationSelectionUser = new SelectionInList<>((ListModel<NodeScriptData>) applicationsUser);
        applicationSelectionInstallation = new SelectionInList<>((ListModel<NodeScriptData>) applicationsInstallation);

        tableApplicationsUser = createApplicationTable(applicationSelectionUser);
        tableApplicationsInstallation = createApplicationTable(applicationSelectionInstallation);

        addPageListener(new PageListener() {

            @Override
            public void pageEventFired(PageEvent e) {

                switch (e.getID()) {
                    case PageEvent.PAGE_CLOSING:
                        if (e.getSource() instanceof JButton) {
                            JButton button = (JButton) e.getSource();
                            if (ButtonNames.NEXT.equals(button.getName())) {
                                LOGGER.info("The next button was pressed.");

                                // get the selected tab
                                int selectedIndex = tabbedPane.getSelectedIndex();
                                LOGGER.info("Currently selected index: {}", selectedIndex);
                                NodeScriptData nodeScriptData = null;
                                switch (selectedIndex) {
                                    case 0:
                                        // user
                                        nodeScriptData = applicationSelectionUser.getSelection();
                                        break;
                                    case 1:
                                        // installation
                                        nodeScriptData = applicationSelectionInstallation.getSelection();
                                        break;

                                    default:
                                        break;
                                }

                                if (nodeScriptData != null) {
                                    LOGGER.info("Set the selected path: {}", nodeScriptData.getFilePath());
                                    scriptContext.register(NodeScriptContextKeys.KEY_SELECTED_NODESCRIPT_PATH,
                                        nodeScriptData.getFilePath());
                                }
                                else {
                                    LOGGER.warn("No selection available.");
                                }
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
        });
    }

    private AbstractEmptyTable createApplicationTable(SelectionInList<NodeScriptData> applicationSelection) {

        AbstractEmptyTable tableApplications =
            new AbstractEmptyTable(
                new NodeScriptDataTableAdapter(applicationSelection,
                    new String[] { Resources.getString(SelectApplicationPage.class, "application") }),
                Resources.getString(AbstractEmptyTable.class, "empty_table")) {
                private static final long serialVersionUID = 1L;
            };

        tableApplications
            .setSelectionModel(new SingleListSelectionAdapter(applicationSelection.getSelectionIndexHolder()));
        tableApplications.setRowResizable(true);
        tableApplications.setRowAutoResizes(true);
        tableApplications.setTableHeader(null);

        tableApplications.setRowMargin(5);
        tableApplications.setShowHorizontalLines(true);
        tableApplications.setGridColor(Color.BLACK);

        tableApplications.setDefaultCellRenderer(new NodeScriptDataCellRenderer());

        tableApplications.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent e) {

                if (!e.getValueIsAdjusting()) {

                    NodeScriptData application = applicationSelection.getSelection();

                    LOGGER.info("Selection has changed, selected application: {}", application);
                    if (application != null) {
                        fireButtonEvent(ButtonEvent.ENABLE_BUTTON, ButtonNames.NEXT);
                        instruction
                            .setText("<html>" + applicationSelection.getSelection().getInstruction() + "</html>");

                        try {
                            String hyperLink = applicationSelection.getSelection().getHyperlink();
                            if (StringUtils.isNotBlank(hyperLink)) {
                                link.setURI(new URI(hyperLink));
                            }
                            else {
                                link.setURI(null);
                            }
                        }
                        catch (URISyntaxException ex) {
                            LOGGER.warn("Set the link uri failed.", ex);
                            link.setURI(null);
                        }
                    }
                    else {
                        fireButtonEvent(ButtonEvent.DISABLE_BUTTON, ButtonNames.NEXT);
                        instruction.setText(null);
                        link.setURI(null);
                    }
                }
            }
        });

        return tableApplications;
    }

    @Override
    protected JComponent createDefaultContentPane() {

        // add the contents
        instructionPanel = new JPanel();
        instructionPanel.setLayout(new JideBoxLayout(instructionPanel, JideBoxLayout.PAGE_AXIS));
        instructionPanel.setBorder(new EmptyBorder(3, 3, 3, 3));

        instructionPanel.add(DefaultComponentFactory
            .getInstance().createSeparator(Resources.getString(SelectApplicationPage.class, "info")));
        instructionPanel.add(instruction, JideBoxLayout.VARY);
        instructionPanel.add(link);

        tabbedPane = new JideTabbedPane();
        // add user tab
        tabbedPane.addTab(Resources.getString(SelectApplicationPage.class, "tab_user"),
            new JScrollPane(tableApplicationsUser));
        // add installation tab
        tabbedPane.addTab(Resources.getString(SelectApplicationPage.class, "tab_installation"),
            new JScrollPane(tableApplicationsInstallation));

        JideSplitPane jideSplitPane = new JideSplitPane(JideSplitPane.VERTICAL_SPLIT);

        jideSplitPane.addPane(tabbedPane);

        jideSplitPane.addPane(new JScrollPane(instructionPanel));

        jideSplitPane.setProportionalLayout(true);
        jideSplitPane.setProportions(new double[] { 0.7 });

        return jideSplitPane;
    }

    @Override
    public void setupWizardButtons() {
        LOGGER.info("Setup the wizard buttons.");

        applicationsUser.clear();
        applicationsInstallation.clear();

        String selectedCategory = null;
        List<AbstractPage> visitedPages = getOwner().getVisitedPages();
        for (AbstractPage page : visitedPages) {
            if (page instanceof SelectCategoryPage) {
                SelectCategoryPage categoryPage = (SelectCategoryPage) page;

                selectedCategory = categoryPage.getSelectedCategory();

                LOGGER.info("selectedCategory: {}", selectedCategory);
            }
        }

        fireButtonEvent(ButtonEvent.ENABLE_BUTTON, ButtonNames.BACK);
        fireButtonEvent(ButtonEvent.DISABLE_BUTTON, ButtonNames.NEXT);
        fireButtonEvent(ButtonEvent.SET_DEFAULT_BUTTON, ButtonNames.NEXT);
        fireButtonEvent(ButtonEvent.HIDE_BUTTON, ButtonNames.FINISH);

        if (StringUtils.isNotEmpty(selectedCategory)) {

            // must process all provided sources

            for (Entry<NodeScriptSource, NodeScriptSources> entry : mapNodeScriptSources.entrySet()) {

                NodeScriptSource nodeScriptCategory = entry.getKey();

                ScriptSource source = nodeScriptCategory.getScriptSource();
                String searchLocation = nodeScriptCategory.getFilePath();
                LOGGER.info("Current search location for nodeScript files: {}, selectedCategory: {}, source: {}",
                    searchLocation, selectedCategory, source);
                File searchDir = new File(searchLocation).getParentFile();
                File nodescriptDir = new File(searchDir, selectedCategory);

                List<File> nodescriptFiles = SearchPathUtils.findFiles("*.nodescript", nodescriptDir.getPath());

                if (CollectionUtils.isNotEmpty(nodescriptFiles)) {
                    for (File nodescriptFile : nodescriptFiles) {
                        LOGGER.info("Introspect current file: {}", nodescriptFile);
                        LOGGER.info("Introspect current file URI: {}", nodescriptFile.toURI().toString());

                        try {
                            if (nodescriptFile.exists()) {
                                // introspect the file
                                introspectFile(nodescriptFile, source);
                            }
                            // inside a jar
                            else if (nodescriptFile.toString().startsWith("jar:file")) {
                                LOGGER.info("Process file from jar file.");
                                URI uri = nodescriptFile.toURI();
                                final String[] array = uri.toString().split("!");

                                String filePath = array[1];
                                introspectInternalFile(nodescriptFile, filePath, source);
                            }
                            else {
                                LOGGER.info("The searched nodescript does not exist: {}", nodescriptFile);
                            }
                        }
                        catch (Exception ex) {
                            LOGGER.warn("Introspect file failed: {}", nodescriptFile, ex);
                        }
                    }
                }
                else {
                    LOGGER.info("The provided nodescript directory does not contain nodescript files: {}",
                        nodescriptDir);
                }
            }
        }

        // enable / disable tabs
        tabbedPane.setEnabledAt(0, !applicationsUser.isEmpty());
        tabbedPane.setEnabledAt(1, !applicationsInstallation.isEmpty());

    }

    private void introspectFile(final File nodescriptFile, ScriptSource source) {
        LOGGER.info("Introspect file with path: {}, source: {}", nodescriptFile.getPath(), source);

        String encoding = NodeScriptUtils.detectFileEncoding(nodescriptFile);

        // introspect the file: search for ##instruction

        try (BufferedReader reader =
            new BufferedReader(new InputStreamReader(new FileInputStream(nodescriptFile), encoding))) {

            introspectLines(reader, nodescriptFile, source);
        }
        catch (Exception ex) {
            LOGGER.warn("Read script from file failed.", ex);
        }
    }

    private void introspectInternalFile(final File nodescriptFile, String filePath, ScriptSource source) {
        LOGGER.info("Introspect internal file with path: {}, source: {}", filePath, source);

        String encoding = null;
        try (InputStream is = SelectApplicationPage.class.getResourceAsStream(filePath)) {

            encoding = NodeScriptUtils.detectFileEncoding(is);
        }
        catch (Exception ex) {
            LOGGER.warn("Detect encoding from stream failed: {}", nodescriptFile, ex);
            encoding = "UTF-8";
        }

        // introspect the file: search for ##instruction
        try (BufferedReader reader =
            new BufferedReader(
                new InputStreamReader(SelectApplicationPage.class.getResourceAsStream(filePath), encoding))) {

            introspectLines(reader, nodescriptFile, source);
        }
        catch (Exception ex) {
            LOGGER.warn("Read script from internal file failed.", ex);
        }
    }

    private void introspectLines(final BufferedReader reader, final File nodescriptFile, ScriptSource source)
        throws IOException {
        List<String> lines = IOUtils.readLines(reader);
        NodeScriptData nodeScriptData = null;

        String application = null;
        String instruction = null;
        String hyperlink = null;
        Boolean nodeRequirementsMatched = null;

        Node selectedNode = scriptContext.get(NodeScriptContextKeys.KEY_SELECTED_CORE_NODE, Node.class);

        for (String line : lines) {

            // application is the preferred source, but with fallback to instruction
            if (line.startsWith(NodeScriptKeywords.KEYWORD_APPLICATION)) {
                LOGGER.info("Found application line: {}", line);

                application = NodeScriptUtils.extractInstruction(line);
                if (StringUtils.isEmpty(application)) {
                    application = nodescriptFile.getName();
                }
                LOGGER.info("Prepared application: {}", application);
            }
            else if (line.startsWith(NodeScriptKeywords.KEYWORD_INSTRUCTION)) {
                LOGGER.info("Found instruction line: {}", line);

                instruction = NodeScriptUtils.extractInstruction(line);
                if (StringUtils.isEmpty(instruction)) {
                    instruction = nodescriptFile.getName();
                }

                hyperlink = NodeScriptUtils.extractInstructionHyperlink(line);
                LOGGER.info("Prepared instruction: {}", instruction);
            }
            else if (line.startsWith(NodeScriptKeywords.KEYWORD_REQUIRE)) {
                LOGGER.info("Found require line: {}", line);
                String require = line;

                try {
                    NodeScriptUtils.checkMatchingNodeVidPid(selectedNode, require);

                    nodeRequirementsMatched = Boolean.TRUE;
                }
                catch (NodeRequirementException ex) {
                    LOGGER.warn("The current script is not allowed to run on the selected node!", ex);
                    nodeRequirementsMatched = Boolean.FALSE;
                }
            }

            // if we have application and instruction we're done
            if (StringUtils.isNotBlank(application) && StringUtils.isNotBlank(instruction)
                && nodeRequirementsMatched != null) {
                nodeScriptData =
                    new NodeScriptData(application, instruction, hyperlink, nodescriptFile.getPath(),
                        nodeRequirementsMatched.booleanValue());
                break;
            }
        }

        if (nodeScriptData == null) {

            if (nodeRequirementsMatched == null) {
                nodeRequirementsMatched = Boolean.TRUE;
            }
            // application and instruction available
            if (StringUtils.isNotBlank(application) && StringUtils.isNotBlank(instruction)) {
                nodeScriptData =
                    new NodeScriptData(application, instruction, hyperlink, nodescriptFile.getPath(),
                        nodeRequirementsMatched.booleanValue());
            }
            // application available and instruction missing
            else if (StringUtils.isNotBlank(application)) {
                nodeScriptData =
                    new NodeScriptData(application, application, hyperlink, nodescriptFile.getPath(),
                        nodeRequirementsMatched.booleanValue());
            }
            // instruction available and application missing
            else if (StringUtils.isNotBlank(instruction)) {
                nodeScriptData =
                    new NodeScriptData(instruction, instruction, hyperlink, nodescriptFile.getPath(),
                        nodeRequirementsMatched.booleanValue());
            }
        }

        if (nodeScriptData != null) {
            switch (source) {
                case user:
                    applicationsUser.add(nodeScriptData);
                    break;
                case installation:
                    applicationsInstallation.add(nodeScriptData);
                    break;
                default:
                    break;
            }
        }
    }

    private final static class NodeScriptDataCellRenderer extends DefaultTableCellRenderer {
        private static final long serialVersionUID = 1L;

        public Component getTableCellRendererComponent(
            JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            JLabel c = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

            if (value instanceof NodeScriptData) {
                NodeScriptData nodeScriptData = (NodeScriptData) value;
                String instruction = nodeScriptData.getInstruction();
                c.setToolTipText("<html>" + instruction + "</html>");
            }
            return c;
        }
    }
}
