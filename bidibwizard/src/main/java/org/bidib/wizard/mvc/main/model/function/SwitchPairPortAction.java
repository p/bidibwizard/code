package org.bidib.wizard.mvc.main.model.function;

import org.bidib.jbidibc.exchange.lcmacro.LcMacroPointType;
import org.bidib.jbidibc.exchange.lcmacro.SwitchPairPortPoint;
import org.bidib.jbidibc.exchange.lcmacro.SwitchPortActionType;
import org.bidib.wizard.comm.SwitchPortStatus;
import org.bidib.wizard.mvc.main.model.SwitchPairPort;

public class SwitchPairPortAction extends PortAction<SwitchPortStatus, SwitchPairPort> {
    public SwitchPairPortAction() {
        this(SwitchPortStatus.ON);
    }

    public SwitchPairPortAction(SwitchPortStatus action) {
        this(action, null, 0);
    }

    public SwitchPairPortAction(SwitchPairPort port, int delay) {
        this(SwitchPortStatus.ON, port, delay);
    }

    public SwitchPairPortAction(SwitchPortStatus action, SwitchPairPort port, int delay) {
        super(action, KEY_SWITCHPAIR, port, delay);
    }

    public String getDebugString() {
        int id = 0;

        if (getPort() != null) {
            id = getPort().getId();
        }
        return "@" + getDelay() + " S-Port:" + String.format("%02d", id) + "->" + getAction().name().toLowerCase();
    }

    @Override
    public LcMacroPointType toLcMacroPoint() {
        SwitchPairPortPoint switchPairPortPoint = new SwitchPairPortPoint();
        switchPairPortPoint.setDelay(getDelay());
        switchPairPortPoint.setOutputNumber(getPort().getId());
        switchPairPortPoint.setSwitchPortActionType(SwitchPortActionType.fromValue(getAction().name()));
        return switchPairPortPoint;
    }
}
