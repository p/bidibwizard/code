package org.bidib.wizard.mvc.main.view.panel;

import java.awt.Point;
import java.awt.event.MouseEvent;
import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;

import javax.swing.JPopupMenu;
import javax.swing.event.ListSelectionListener;

import org.bidib.wizard.dialog.LabelDialog;
import org.bidib.wizard.dialog.NodeLabelDialog;
import org.bidib.wizard.mvc.main.model.LabelAware;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.view.component.LabeledDisplayItems;
import org.bidib.wizard.mvc.main.view.menu.listener.LabelListMenuListener;
import org.bidib.wizard.mvc.main.view.panel.listener.LabelChangedListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class LabelListPanel<T extends Serializable> implements LabelListMenuListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(LabelListPanel.class);

    private final Collection<LabelChangedListener<T>> labelChangedListeners = new LinkedList<>();

    private LabeledDisplayItems<T> labelList;

    /**
     * Creates a new instance of the LabelListPanel.
     * 
     * @param labelList
     *            the label list instance
     */
    public LabelListPanel(LabeledDisplayItems<T> labelList) {
        LOGGER.info("Create new LabelListPanel with labelList: {}", labelList);
        this.labelList = labelList;
    }

    protected LabeledDisplayItems<T> getLabelList() {
        return labelList;
    }

    public void addLabelChangedListener(LabelChangedListener<T> l) {
        labelChangedListeners.add(l);
    }

    public void addListSelectionListener(ListSelectionListener l) {
        labelList.addListSelectionListener(l);
    }

    @Override
    public void editLabel() {
        int selectedIndex = labelList.getSelectedIndex();
        if (selectedIndex >= 0) {
            final Object object = labelList.getElementAt(selectedIndex);
            final Point itemPosition = selectedIndexToLocation(selectedIndex);

            if (object instanceof Node) {
                final Node node = (Node) object;
                new NodeLabelDialog(
                    node, itemPosition.x, itemPosition.y) {
                    @Override
                    public void labelChanged(String value) {
                        LOGGER.info("The label of the node was changed to value: '{}'", value);
                        if (node instanceof LabelAware) {
                            ((LabelAware) node).setLabel(value);
                        }
                        fireLabelChanged((T) node, value);
                    }
                };
            }
            else {
                new LabelDialog(
                    object.toString(), itemPosition.x, itemPosition.y) {
                    @Override
                    public void labelChanged(String value) {
                        if (object instanceof LabelAware) {
                            ((LabelAware) object).setLabel(value);
                        }
                        fireLabelChanged((T) object, value);
                    }
                };
            }
        }
    }

    protected void fireLabelChanged(T object, String label) {
        for (LabelChangedListener<T> l : labelChangedListeners) {
            l.labelChanged(object, label);
        }
    }

    protected void handleMouseEvent(MouseEvent e, JPopupMenu popupMenu) {
        if (e.isPopupTrigger() && labelList.getItemSize() > 0) {
            labelList.selectElement(e.getPoint());
            popupMenu.show(e.getComponent(), e.getX(), e.getY());
        }
    }

    protected Point selectedIndexToLocation() {
        int selectedIndex = labelList.getSelectedIndex();
        return selectedIndexToLocation(selectedIndex);
    }

    private Point selectedIndexToLocation(int selectedIndex) {
        Point result = new Point();

        if (selectedIndex >= 0) {
            final Point listPosition = labelList.getLocationOnScreen();
            final Point itemPosition = labelList.indexToLocation(selectedIndex);
            result.setLocation(listPosition.x + itemPosition.x, listPosition.y + itemPosition.y);
        }
        return result;
    }
}
