package org.bidib.wizard.mvc.pom.view.panel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import org.bidib.jbidibc.core.AddressData;
import org.bidib.jbidibc.core.enumeration.PomOperation;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.common.view.converter.StringConverter;
import org.bidib.wizard.mvc.common.view.validation.PropertyValidationI18NSupport;
import org.bidib.wizard.mvc.pom.model.PomMode;
import org.bidib.wizard.mvc.pom.model.PomProgrammerModel;
import org.bidib.wizard.mvc.pom.view.CurrentAddressBeanModel;
import org.bidib.wizard.mvc.pom.view.command.PomDirectAccessCommand;
import org.bidib.wizard.mvc.pom.view.command.PomOperationCommand;
import org.bidib.wizard.mvc.pom.view.panel.ProgCommandAwareBeanModel.ExecutionType;
import org.bidib.wizard.utils.InputValidationDocument;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.binding.value.ConverterValueModel;
import com.jgoodies.binding.value.ValueHolder;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.builder.ButtonBarBuilder;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.validation.Severity;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.ValidationResultModel;
import com.jgoodies.validation.util.PropertyValidationSupport;
import com.jgoodies.validation.view.ValidationComponentUtils;

public class DirectAccessPanel extends AbstractPomPanel<DirectAccessProgBeanModel> {

    public static final int MIN_SUSI_CV = 897;

    private final DirectAccessProgBeanModel directAccessProgBeanModel;

    private ValueModel cvNumberValueModel;

    private ValueModel cvValueValueModel;

    private JTextField cvNumber;

    private JTextField cvValue;

    private InputValidationDocument cvValueDocument;

    private PomValidationResultModel directValidationModel;

    private CvWritePanel cvWritePanel;

    private JComponent[] modeButtons;

    public DirectAccessPanel(final PomProgrammerModel cvProgrammerModel,
        final CurrentAddressBeanModel currentAddressBeanModel) {
        super(cvProgrammerModel, currentAddressBeanModel);

        directAccessProgBeanModel = new DirectAccessProgBeanModel();
        setProgCommandAwareBeanModel(directAccessProgBeanModel);
    }

    @Override
    protected void createWorkerPanel(DefaultFormBuilder builder, final PomValidationResultModel parentValidationModel) {

        builder.append(new JLabel(Resources.getString(getClass(), "direct-message")), 7);

        // create the panel content
        builder.append(Resources.getString(getClass(), "mode"));

        ValueModel modeModel =
            new PropertyAdapter<DirectAccessProgBeanModel>(directAccessProgBeanModel,
                DirectAccessProgBeanModel.PROPERTYNAME_MODE, true);
        modeButtons = new JComponent[PomMode.values().length];
        int index = 0;
        for (PomMode mode : PomMode.values()) {

            JRadioButton radio =
                BasicComponentFactory.createRadioButton(modeModel, mode,
                    Resources.getString(PomMode.class, mode.getKey()));
            modeButtons[index++] = radio;

            // add radio button
            builder.append(radio);
        }
        modeModel.addValueChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                LOGGER.debug("The mode has changed: {}", evt.getNewValue());
                // clear the CV value when the mode is changed
                directAccessProgBeanModel.setCvValue(null);
            }
        });

        builder.nextLine();

        builder.append(Resources.getString(getClass(), "cv-number"));

        cvNumberValueModel =
            new PropertyAdapter<DirectAccessProgBeanModel>(directAccessProgBeanModel,
                DirectAccessProgBeanModel.PROPERTYNAME_CV_NUMBER, true);

        final ValueModel cvNumberConverterModel =
            new ConverterValueModel(cvNumberValueModel, new StringConverter(new DecimalFormat("#")));

        // create the textfield for the CV number
        cvNumber = new JTextField();
        cvNumber.setDocument(new InputValidationDocument(4, InputValidationDocument.NUMERIC));
        cvNumber.setColumns(4);
        // bind manually because we changed the document of the textfield
        Bindings.bind(cvNumber, cvNumberConverterModel, false);
        builder.append(cvNumber);

        ValidationComponentUtils.setMandatory(cvNumber, true);
        ValidationComponentUtils.setMessageKey(cvNumber, "validation.cvnumber_key");

        // add a validation model that can trigger a button state with the validState property
        directValidationModel = new PomValidationResultModel(parentValidationModel);

        directAccessProgBeanModel.addPropertyChangeListener(DirectAccessProgBeanModel.PROPERTYNAME_CV_NUMBER,
            new PropertyChangeListener() {

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    LOGGER.debug("CV number has changed: {}", directAccessProgBeanModel.getCvNumber());
                    triggerValidation();
                }
            });

        builder.nextLine();

        builder.append(Resources.getString(getClass(), "cv-value"));

        cvValueValueModel =
            new PropertyAdapter<DirectAccessProgBeanModel>(directAccessProgBeanModel,
                DirectAccessProgBeanModel.PROPERTYNAME_CV_VALUE, true);

        final ValueModel cvValueConverterModel =
            new ConverterValueModel(cvValueValueModel, new StringConverter(new DecimalFormat("#")));

        // create the textfield for the CV value
        cvValue = new JTextField();
        cvValueDocument = new InputValidationDocument(3, InputValidationDocument.NUMERIC);
        cvValue.setDocument(cvValueDocument);
        cvValue.setColumns(3);
        // bind manually because we changed the document of the textfield
        Bindings.bind(cvValue, cvValueConverterModel, false);
        builder.append(cvValue);

        ValidationComponentUtils.setMessageKey(cvValue, "validation.cvvalue_key");

        directAccessProgBeanModel.addPropertyChangeListener(DirectAccessProgBeanModel.PROPERTYNAME_MODE,
            new PropertyChangeListener() {

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    LOGGER.debug("Operation has changed: {}", directAccessProgBeanModel.getMode());

                    // only accept 0 and 1 if bit mode
                    switch (directAccessProgBeanModel.getMode()) {
                        case BIT:
                            cvValueDocument.setLimit(1);
                            cvValueDocument.setAcceptedChars("01");
                            readButton.setText(Resources.getString(DirectAccessPanel.class, "verify"));
                            break;
                        default:
                            cvValueDocument.setLimit(3);
                            cvValueDocument.setAcceptedChars(InputValidationDocument.NUMERIC);
                            readButton.setText(Resources.getString(AbstractPomPanel.class, "read"));
                            break;
                    }
                }
            });
        directAccessProgBeanModel.addPropertyChangeListener(DirectAccessProgBeanModel.PROPERTYNAME_CV_VALUE,
            new PropertyChangeListener() {

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    LOGGER.debug("CV value has changed: {}", directAccessProgBeanModel.getCvValue());
                    triggerValidation();
                }
            });

        cvWritePanel = new CvWritePanel(cvProgrammerModel, directAccessProgBeanModel);

        builder.append(cvWritePanel.createPanel(), 3);

        // readButtonEnabled = new ValueHolder(false);
        cvNumberValueModel.addValueChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                triggerValidation();
            }
        });

        readButtonEnabled = new ValueHolder(false);
        readButton.setEnabled(false);
        readButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                fireRead();
            }
        });

        writeButtonEnabled = new ValueHolder(false);
        writeButton.setEnabled(false);
        writeButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                fireWrite();
            }
        });

        cvValueValueModel.addValueChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                LOGGER.info("The CV value has changed: {}", evt.getNewValue());
                cvWritePanel.updateByteValue(evt.getNewValue());

                // enable the write button if the CV value is set and the CV number is set too
                triggerValidation();
            }
        });

        // prepare the read and write buttons
        JPanel progActionButtons = new ButtonBarBuilder().addGlue().addButton(readButton, writeButton).build();
        builder.append(progActionButtons, 7);
    }

    @Override
    protected ValidationResultModel getValidationResultModel() {
        return directValidationModel;
    }

    private ValidationResult validate() {
        PropertyValidationSupport support = new PropertyValidationI18NSupport(cvNumberValueModel, "validation");

        // only CV numbers up to 1023 are valid
        if (directAccessProgBeanModel.getCvNumber() == null) {
            support.addError("cvnumber_key", "not_empty");
        }
        else if (directAccessProgBeanModel.getCvNumber().intValue() < 1
            || directAccessProgBeanModel.getCvNumber().intValue() > 1023) {
            support.addError("cvnumber_key", "invalid_value;min=1,max=1023");
        }

        if (directAccessProgBeanModel.getCvValue() == null) {
            if (PomMode.BYTE.equals(directAccessProgBeanModel.getMode())) {
                support.add(Severity.INFO, "cvvalue_key", "not_empty_for_write");
            }
            else {
                support.addError("cvvalue_key", "not_empty_for_bit_verify");
            }
        }
        else if (directAccessProgBeanModel.getCvValue().intValue() < 0
            || directAccessProgBeanModel.getCvValue().intValue() > 255) {
            support.addError("cvvalue_key", "invalid_value;min=0,max=255");
        }

        ValidationResult validationResult = support.getResult();
        LOGGER.info("Prepared validationResult: {}", validationResult);
        return validationResult;
    }

    @Override
    protected void triggerValidation() {
        ValidationResult validationResult = validate();
        directValidationModel.setResult(validationResult);

        // enable or disable the buttons
        LOGGER.debug("Set the writeButtonEnabled, validStateNoWarnOrErrors: {}, validState: {}",
            directValidationModel.getValidStateNoWarnOrErrors(), directValidationModel.getValidState());
        writeButtonEnabled.setValue(directValidationModel.getValidState());

        readButtonEnabled.setValue(directValidationModel.getValidStateNoWarnOrErrors());
    }

    @Override
    protected void disableInputElements() {

        // disable the write panel
        cvWritePanel.setEnabled(false);

        cvValue.setEnabled(false);
        cvNumber.setEnabled(false);

        for (JComponent comp : modeButtons) {
            comp.setEnabled(false);
        }

        super.disableInputElements();
    }

    @Override
    protected void enableInputElements() {

        // disable the write panel
        cvWritePanel.setEnabled(true);

        cvValue.setEnabled(true);
        cvNumber.setEnabled(true);

        for (JComponent comp : modeButtons) {
            comp.setEnabled(true);
        }

        // check the validation model ...
        triggerValidation();
    }

    private int prepareBitCvValue(boolean write) {

        // Beim Bit Schreiben wird das zuschreibende Bit mittels DATA bestimmt: DATA = 111KDBBB,
        // wobei BBB die Bitposition angibt und D den Wert des Bits. K ist die Operation (1=write,
        // 0=read)(identisch zur DCC Definition)

        // set integer value from selected bit
        int cvValue = cvWritePanel.getSelectedBit();

        int intVal = (directAccessProgBeanModel.getCvValue() != null ? directAccessProgBeanModel.getCvValue() : 0);

        // set the value of the bit
        if (intVal != 0) {
            cvValue = (byte) (cvValue | 0x08);
        }

        // set the flag for bit write
        if (write) {
            cvValue = (cvValue | 0x10);
        }

        // set the top 3 bits to 1
        cvValue |= 0xE0;

        LOGGER.info("Prepared bit-based cvValue: {}", ByteUtils.byteToHex(cvValue));

        return cvValue;

    }

    private int prepareByteCvValue() {
        // set integer value from bit values
        int cvValue = (directAccessProgBeanModel.getCvValue() != null ? directAccessProgBeanModel.getCvValue() : 0);
        LOGGER.info("Prepared byte-based cvValue: {}", ByteUtils.byteToHex(cvValue));
        return cvValue;
    }

    private void fireWrite() {
        // disable the input elements
        disableInputElements();

        // perform operation
        LOGGER.info("Prepare the write request for model: {}", directAccessProgBeanModel);

        // clear the executed commands
        directAccessProgBeanModel.getExecutedProgCommands().clear();

        // prepare the list of commands that must be executed
        List<PomOperationCommand<? extends ProgCommandAwareBeanModel>> progCommands =
            directAccessProgBeanModel.getProgCommands();
        progCommands.clear();

        int cvValue = 0;
        switch (directAccessProgBeanModel.getMode()) {
            case BIT:
                cvValue = prepareBitCvValue(true);
                break;
            default:
                cvValue = prepareByteCvValue();
                break;
        }

        int cvNumber = directAccessProgBeanModel.getCvNumber();

        int directDccAddress = currentAddressBeanModel.getDccAddress();

        LOGGER.info("Prepared DCC address: {}", directDccAddress);

        AddressData addressData = new AddressData(directDccAddress, currentAddressBeanModel.getAddressType());

        PomOperation operation =
            (PomMode.BIT.equals(directAccessProgBeanModel.getMode()) ? PomOperation.WR_BIT : PomOperation.WR_BYTE);
        directAccessProgBeanModel.setCurrentOperation(operation);
        directAccessProgBeanModel.setExecution(ExecutionType.WRITE);

        progCommands.add(new PomDirectAccessCommand(addressData, operation, cvNumber, cvValue));

        startTimeoutControl(DEFAULT_TIMEOUT);

        fireNextCommand();
    }

    private void fireRead() {
        // disable the input elements
        disableInputElements();

        // clear the executed commands
        directAccessProgBeanModel.getExecutedProgCommands().clear();

        // prepare the list of commands that must be executed
        List<PomOperationCommand<? extends ProgCommandAwareBeanModel>> progCommands =
            directAccessProgBeanModel.getProgCommands();
        progCommands.clear();

        // get the prepared CV value that is send to the interface
        LOGGER.info("Prepare the read request for model: {}", directAccessProgBeanModel);
        int cvValue = 0;
        switch (directAccessProgBeanModel.getMode()) {
            case BIT:
                cvValue = prepareBitCvValue(false);
                break;
            default:
                cvValue = prepareByteCvValue();
                break;
        }

        int cvNumber = directAccessProgBeanModel.getCvNumber();

        int directDccAddress = currentAddressBeanModel.getDccAddress();

        LOGGER.info("Prepared DCC address: {}", directDccAddress);

        AddressData addressData = new AddressData(directDccAddress, currentAddressBeanModel.getAddressType());

        // TODO for bit read operations we must use byte read and extract the bit
        PomOperation operation =
            (PomMode.BIT.equals(directAccessProgBeanModel.getMode()) ? PomOperation.RD_BYTE : PomOperation.RD_BYTE);

        // this is for testing XPOM
        // PomOperation operation = PomOperation.X_RD_BLOCK;

        directAccessProgBeanModel.setCurrentOperation(operation);
        directAccessProgBeanModel.setExecution(ExecutionType.READ);

        progCommands.add(new PomDirectAccessCommand(addressData, operation, cvNumber, cvValue));

        int timeout = DEFAULT_TIMEOUT;
        if (cvNumber > MIN_SUSI_CV) {
            // add command again for SUSI CVs
            progCommands.add(new PomDirectAccessCommand(addressData, operation, cvNumber, cvValue));

            timeout = DEFAULT_SUSI_READ_FIRST_TIMEOUT;
        }

        if (operation.equals(PomOperation.RD_BYTE)) {
            // clear the CV value now ...
            cvValueValueModel.setValue(null);
        }

        startTimeoutControl(timeout);

        fireNextCommand();
    }

    @Override
    protected Object getCurrentOperation() {
        return directAccessProgBeanModel.getCurrentOperation();
    }

}
