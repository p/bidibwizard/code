package org.bidib.wizard.mvc.common.view.renderer;

import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import org.bidib.wizard.common.utils.ImageUtils;
import org.bidib.wizard.mvc.main.model.Port;

public class PortIdentifierTableCellRenderer extends DefaultTableCellRenderer {
    private static final long serialVersionUID = 1L;

    private final ImageIcon mappingEnabledIcon;

    public PortIdentifierTableCellRenderer() {
        mappingEnabledIcon =
            ImageUtils.createImageIcon(PortIdentifierTableCellRenderer.class, "/icons/arrow_switch.png");
    }

    public Component getTableCellRendererComponent(
        JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

        // call super to set the correct color if selected
        super.getTableCellRendererComponent(table, null, isSelected, hasFocus, row, column);

        // renderer only handles ports
        if (value instanceof Port) {
            Port<?> port = (Port<?>) value;
            boolean enabled = port.isEnabled();
            setEnabled(enabled);
            setIcon(null);

            setText(port.getPortIdentifier());
            if (port.isRemappingEnabled()) {
                setIcon(mappingEnabledIcon);
            }
        }
        else {
            setEnabled(false);
            setText(null);
        }

        return this;
    }
}