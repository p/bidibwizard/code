package org.bidib.wizard.mvc.stepcontrol.model;

import org.bidib.jbidibc.core.enumeration.BidibEnum;
import org.bidib.jbidibc.core.utils.ByteUtils;

public enum TurnTableType implements BidibEnum {
    linear("linear", 0), round("round", 1);

    private byte type;

    private final String key;

    TurnTableType(String key, int type) {
        this.key = key;
        this.type = ByteUtils.getLowByte(type);
    }

    @Override
    public byte getType() {
        return type;
    }

    public String getKey() {
        return key;
    }

    public int getCvValue() {
        return ByteUtils.getInt(type);
    }

    public static TurnTableType fromValue(byte type) {
        TurnTableType result = null;
        for (TurnTableType e : values()) {
            if (e.type == type) {
                result = e;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("cannot map " + type + " to a turnTableType enum");
        }
        return result;
    }
}