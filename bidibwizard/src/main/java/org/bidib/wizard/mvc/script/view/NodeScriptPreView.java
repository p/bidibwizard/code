package org.bidib.wizard.mvc.script.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.script.model.NodeScriptModel;
import org.fife.ui.rsyntaxtextarea.AbstractTokenMakerFactory;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.TokenMakerFactory;
import org.fife.ui.rtextarea.RTextScrollPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.debug.FormDebugPanel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.FormLayout;
import com.vlsolutions.swing.docking.DockKey;
import com.vlsolutions.swing.docking.Dockable;

public class NodeScriptPreView implements Dockable {
    private static final Logger LOGGER = LoggerFactory.getLogger(NodeScriptPreView.class);

    public static final String NODE_SCRIPT_PREVIEW = "NodeScriptPreView";

    public final DockKey DOCKKEY = new DockKey(NODE_SCRIPT_PREVIEW);

    private static final String ENCODED_DIALOG_COLUMN_SPECS = "pref, 3dlu, fill:50dlu:grow, 3dlu, pref";

    private final JPanel contentPanel;

    private final NodeScriptModel nodeScriptModel;

    private RSyntaxTextArea textArea;

    /**
     * @param nodeScriptModel
     *            the nodeScript model
     */
    public NodeScriptPreView(final NodeScriptModel nodeScriptModel) {
        this.nodeScriptModel = nodeScriptModel;

        DOCKKEY.setName(Resources.getString(getClass(), "title"));
        DOCKKEY.setFloatEnabled(true);
        DOCKKEY.setAutoHideEnabled(false);

        LOGGER.info("Create new NodeScriptPreView");

        DefaultFormBuilder dialogBuilder = null;
        boolean debugDialog = false;
        if (debugDialog) {
            JPanel panel = new FormDebugPanel();
            dialogBuilder = new DefaultFormBuilder(new FormLayout(ENCODED_DIALOG_COLUMN_SPECS), panel);
        }
        else {
            JPanel panel = new JPanel(new BorderLayout());
            dialogBuilder = new DefaultFormBuilder(new FormLayout(ENCODED_DIALOG_COLUMN_SPECS), panel);
        }
        dialogBuilder.border(Borders.DIALOG);

        textArea = new RSyntaxTextArea(20, 60);

        AbstractTokenMakerFactory atmf = (AbstractTokenMakerFactory) TokenMakerFactory.getDefaultInstance();
        atmf.putMapping("text/bidibscript", "org.bidib.wizard.mvc.script.view.modes.BidibTokenMaker");

        textArea.setSyntaxEditingStyle("text/bidibscript");

        textArea.setCodeFoldingEnabled(true);
        RTextScrollPane sp = new RTextScrollPane(textArea);

        // dialogBuilder.appendRow("3dlu");
        dialogBuilder.appendRow("fill:300px:grow");
        // dialogBuilder.nextLine(2);

        dialogBuilder.append(sp, 5);

        // create the content panel
        contentPanel = new JPanel();
        contentPanel.setLayout(new BorderLayout());

        JScrollPane scrollPane = new JScrollPane(dialogBuilder.build());
        contentPanel.add(scrollPane, BorderLayout.CENTER);

        this.nodeScriptModel.addPropertyChangeListener(NodeScriptModel.PROPERTYNAME_PREPROCESSED_SCRIPT,
            new PropertyChangeListener() {

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    // TODO Auto-generated method stub
                    setPreProcessedScript();
                }
            });

        setPreProcessedScript();
    }

    private void setPreProcessedScript() {
        String preProcessedScript = nodeScriptModel.getPreProcessedScript();
        textArea.setText(preProcessedScript);
        textArea.setCaretPosition(0);
    }

    @Override
    public DockKey getDockKey() {
        return DOCKKEY;
    }

    @Override
    public Component getComponent() {
        return contentPanel;
    }
}
