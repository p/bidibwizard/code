package org.bidib.wizard.mvc.main.view.panel;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.DropMode;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import org.bidib.wizard.comm.AccessoryOkayStatus;
import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.comm.MacroStatus;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.main.model.AnalogPort;
import org.bidib.wizard.mvc.main.model.BacklightPort;
import org.bidib.wizard.mvc.main.model.Flag;
import org.bidib.wizard.mvc.main.model.InputPort;
import org.bidib.wizard.mvc.main.model.LightPort;
import org.bidib.wizard.mvc.main.model.Macro;
import org.bidib.wizard.mvc.main.model.MacroTableModel;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.MotorPort;
import org.bidib.wizard.mvc.main.model.ServoPort;
import org.bidib.wizard.mvc.main.model.SoundPort;
import org.bidib.wizard.mvc.main.model.SwitchPairPort;
import org.bidib.wizard.mvc.main.model.SwitchPort;
import org.bidib.wizard.mvc.main.model.function.AccessoryOkayFunction;
import org.bidib.wizard.mvc.main.model.function.AnalogPortAction;
import org.bidib.wizard.mvc.main.model.function.BacklightPortAction;
import org.bidib.wizard.mvc.main.model.function.Delayable;
import org.bidib.wizard.mvc.main.model.function.FlagFunction;
import org.bidib.wizard.mvc.main.model.function.Function;
import org.bidib.wizard.mvc.main.model.function.InputFunction;
import org.bidib.wizard.mvc.main.model.function.LightPortAction;
import org.bidib.wizard.mvc.main.model.function.MacroFunction;
import org.bidib.wizard.mvc.main.model.function.MotorPortAction;
import org.bidib.wizard.mvc.main.model.function.RandomDelayFunction;
import org.bidib.wizard.mvc.main.model.function.ServoMoveQueryFunction;
import org.bidib.wizard.mvc.main.model.function.ServoPortAction;
import org.bidib.wizard.mvc.main.model.function.SoundPortAction;
import org.bidib.wizard.mvc.main.model.function.SwitchPairPortAction;
import org.bidib.wizard.mvc.main.model.function.SwitchPortAction;
import org.bidib.wizard.mvc.main.model.listener.MacroListListener;
import org.bidib.wizard.mvc.main.view.menu.AccessoryTableMenu;
import org.bidib.wizard.mvc.main.view.menu.MacroTableMenu;
import org.bidib.wizard.mvc.main.view.menu.listener.MacroTableMenuListener;
import org.bidib.wizard.mvc.main.view.panel.listener.MacroTableListener;
import org.bidib.wizard.mvc.main.view.table.AbstractEmptyTable;
import org.bidib.wizard.mvc.main.view.table.ComboBoxEditor;
import org.bidib.wizard.mvc.main.view.table.ComboBoxRenderer;
import org.bidib.wizard.mvc.main.view.table.MacroTableRowTransferHandler;
import org.bidib.wizard.mvc.main.view.table.NumberWithLabelEditor;
import org.bidib.wizard.mvc.main.view.table.NumberWithLabelRenderer;
import org.bidib.wizard.utils.MacroListUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MacroContentPanel extends JPanel {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(MacroContentPanel.class);

    private final Collection<MacroTableListener> tableListeners = new LinkedList<MacroTableListener>();

    private final MainModel model;

    private final MacroTableModel tableModel;

    private final AbstractEmptyTable table;

    private final MacroTableMenu macroTableMenu;

    public MacroContentPanel(final MainModel model) {
        this.macroTableMenu = new MacroTableMenu(model);
        this.model = model;

        setLayout(new BorderLayout());
        setBorder(new EmptyBorder(5, 2, 5, 3));

        macroTableMenu.addMenuListener(new MacroTableMenuListener() {
            @Override
            public void copy() {
                fireCopy(table.getSelectedRows());
            }

            @Override
            public void cut() {
                fireCut(table.getSelectedRows());
            }

            @Override
            public void delete() {
                fireDelete(table.getSelectedRows());
            }

            @Override
            public void insertEmptyAfter() {
                fireInsertEmptyAfter(table.getSelectedRow());
            }

            @Override
            public void insertEmptyBefore() {
                fireInsertEmptyBefore(table.getSelectedRow());
            }

            @Override
            public void pasteAfter() {
                firePasteAfter(table.getSelectedRow());
            }

            @Override
            public void pasteBefore() {
                firePasteBefore(table.getSelectedRow());
            }

            @Override
            public void pasteInvertedAfter() {
                firePasteInvertedAfter(table.getSelectedRow());
            }

            @Override
            public void selectAll() {
                int rowCount = table.getRowCount();

                if (rowCount > 0) {
                    table.setRowSelectionInterval(0, rowCount - 1);
                }
            }
        });

        tableModel = new MacroTableModel(model);
        table = new AbstractEmptyTable(tableModel, Resources.getString(getClass(), "emptyTable")) {
            private static final long serialVersionUID = 1L;

            protected String getErrorText() {
                if (model.getSelectedMacro() != null && model.getSelectedMacro().isContainsError()) {
                    return Resources.getString(MacroContentPanel.class, "restore-macro-failed");
                }
                return null;
            }

            @Override
            public Dimension getMinimumSize() {
                // the table should retain a minimum visible size
                return new Dimension(200, 100);
            }

            public TableCellEditor getCellEditor(int row, int column) {
                TableCellEditor result = null;
                Function<?> function = model.getSelectedMacro().getFunction(row);

                switch (column) {
                    case MacroTableModel.COLUMN_DELAY:
                        if (function instanceof Delayable) {
                            result =
                                new NumberWithLabelEditor(Resources.getString(MacroContentPanel.class, "delay") + ":",
                                    "Ticks", 250);
                        }
                        else if (function instanceof RandomDelayFunction) {
                            result =
                                new NumberWithLabelEditor(Resources.getString(MacroContentPanel.class, "maximum") + ":",
                                    "Ticks");
                        }
                        break;
                    case MacroTableModel.COLUMN_PORT_TYPE:
                        result = new ComboBoxEditor<Function<?>>(MacroListUtils.prepareAvailableFunctions(model));
                        break;
                    case MacroTableModel.COLUMN_ACTION:
                        if (function != null) {
                            BidibStatus[] actions = getActions(function.getAction());

                            if (actions.length > 1) {
                                result = new ComboBoxEditor<BidibStatus>(actions);
                            }
                        }
                        break;
                    case MacroTableModel.COLUMN_PORT_NUMBER:
                        if (function instanceof AccessoryOkayFunction) {
                            AccessoryOkayFunction aof = (AccessoryOkayFunction) function;
                            if (!AccessoryOkayStatus.NO_FEEDBACK.equals(aof.getAction())) {
                                List<InputPort> inputPorts = new ArrayList<InputPort>();
                                inputPorts.add(InputPort.NONE);
                                inputPorts.addAll(model.getEnabledInputPorts());
                                result = new ComboBoxEditor<InputPort>(inputPorts.toArray(new InputPort[0]));
                            }
                            else {
                                // no renderer if 'no feedback'
                            }
                        }
                        else if (function instanceof ServoMoveQueryFunction) {
                            List<ServoPort> servoPorts = new ArrayList<ServoPort>();
                            servoPorts.add(ServoPort.NONE);
                            servoPorts.addAll(model.getServoPorts());
                            result = new ComboBoxEditor<ServoPort>(servoPorts.toArray(new ServoPort[0]));
                        }
                        else if (function instanceof AnalogPortAction) {
                            result = new ComboBoxEditor<AnalogPort>(model.getAnalogPorts().toArray(new AnalogPort[0]));
                        }
                        else if (function instanceof BacklightPortAction) {
                            result =
                                new ComboBoxEditor<BacklightPort>(
                                    model.getBacklightPorts().toArray(new BacklightPort[0]));
                        }
                        else if (function instanceof FlagFunction) {
                            result = new ComboBoxEditor<Flag>(model.getFlags().toArray(new Flag[0]));
                        }
                        else if (function instanceof InputFunction) {
                            List<InputPort> inputPorts = new ArrayList<InputPort>();
                            inputPorts.add(InputPort.NONE);
                            inputPorts.addAll(model.getEnabledInputPorts());
                            result = new ComboBoxEditor<InputPort>(inputPorts.toArray(new InputPort[0]));
                        }
                        else if (function instanceof LightPortAction) {
                            LinkedList<LightPort> ports = new LinkedList<>();
                            ports.add(LightPort.NONE);
                            ports.addAll(model.getEnabledLightPorts());
                            result = new ComboBoxEditor<LightPort>(ports.toArray(new LightPort[0]));
                        }
                        else if (function instanceof MacroFunction) {
                            MacroFunction macroFunction = (MacroFunction) function;
                            if (MacroStatus.END.equals(macroFunction.getAction())) {
                                // no renderer if 'End' because end always stops the current macro
                            }
                            else {
                                result = new ComboBoxEditor<Macro>(model.getMacros().toArray(new Macro[0]));
                            }
                        }
                        else if (function instanceof MotorPortAction) {
                            result = new ComboBoxEditor<MotorPort>(model.getMotorPorts().toArray(new MotorPort[0]));
                        }
                        else if (function instanceof ServoPortAction) {
                            result = new ComboBoxEditor<ServoPort>(model.getServoPorts().toArray(new ServoPort[0]));
                        }
                        else if (function instanceof SoundPortAction) {
                            result = new ComboBoxEditor<SoundPort>(model.getSoundPorts().toArray(new SoundPort[0]));
                        }
                        else if (function instanceof SwitchPortAction) {
                            LinkedList<SwitchPort> ports = new LinkedList<>();
                            ports.add(SwitchPort.NONE);
                            ports.addAll(model.getEnabledSwitchPorts());
                            result = new ComboBoxEditor<SwitchPort>(ports.toArray(new SwitchPort[0]));
                        }
                        else if (function instanceof SwitchPairPortAction) {
                            LinkedList<SwitchPairPort> ports = new LinkedList<>();
                            ports.add(SwitchPairPort.NONE);
                            ports.addAll(model.getEnabledSwitchPairPorts());
                            result = new ComboBoxEditor<SwitchPairPort>(ports.toArray(new SwitchPairPort[0]));
                        }
                        break;
                    case MacroTableModel.COLUMN_EXTRA:
                        if (function instanceof ServoPortAction) {
                            result =
                                new NumberWithLabelEditor(
                                    Resources.getString(MacroContentPanel.class, "destination") + ":", "%", 100);
                        }
                        else if (function instanceof BacklightPortAction) {
                            result =
                                new NumberWithLabelEditor(
                                    Resources.getString(MacroContentPanel.class, "brightness") + ":", "%");
                        }
                        break;
                }
                return result;
            }

            public TableCellRenderer getCellRenderer(int row, int column) {
                TableCellRenderer result = super.getCellRenderer(row, column);

                if (model.getSelectedMacro() != null && row >= 0
                    && row < model.getSelectedMacro().getFunctions().size()) {
                    Function<? extends BidibStatus> function = model.getSelectedMacro().getFunction(row);

                    switch (column) {
                        case MacroTableModel.COLUMN_DELAY:
                            if (function instanceof Delayable) {
                                result =
                                    new NumberWithLabelRenderer(
                                        Resources.getString(MacroContentPanel.class, "delay") + ":", "Ticks");
                            }
                            else if (function instanceof RandomDelayFunction) {
                                result =
                                    new NumberWithLabelRenderer(
                                        Resources.getString(MacroContentPanel.class, "maximum") + ":", "Ticks");
                            }
                            break;
                        case MacroTableModel.COLUMN_PORT_TYPE:
                            result = new ComboBoxRenderer<Function<?>>(MacroListUtils.prepareAvailableFunctions(model));
                            break;
                        case MacroTableModel.COLUMN_ACTION:
                            if (function != null) {
                                BidibStatus[] actions = getActions(function.getAction());

                                if (actions.length > 1) {
                                    result = new ComboBoxRenderer<BidibStatus>(actions);
                                }
                            }
                            break;
                        case MacroTableModel.COLUMN_PORT_NUMBER:
                            if (function instanceof AccessoryOkayFunction) {
                                AccessoryOkayFunction aof = (AccessoryOkayFunction) function;
                                if (!AccessoryOkayStatus.NO_FEEDBACK.equals(aof.getAction())) {
                                    List<InputPort> inputPorts = new ArrayList<InputPort>();
                                    inputPorts.add(InputPort.NONE);
                                    inputPorts.addAll(model.getEnabledInputPorts());
                                    result = new ComboBoxRenderer<InputPort>(inputPorts.toArray(new InputPort[0]));
                                }
                                else {
                                    // no renderer if 'no feedback'
                                }
                            }
                            else if (function instanceof ServoMoveQueryFunction) {
                                List<ServoPort> servoPorts = new ArrayList<ServoPort>();
                                servoPorts.add(ServoPort.NONE);
                                servoPorts.addAll(model.getServoPorts());
                                result = new ComboBoxRenderer<ServoPort>(servoPorts.toArray(new ServoPort[0]));
                            }
                            else if (function instanceof AnalogPortAction) {
                                result =
                                    new ComboBoxRenderer<AnalogPort>(model.getAnalogPorts().toArray(new AnalogPort[0]));
                            }
                            else if (function instanceof BacklightPortAction) {
                                result =
                                    new ComboBoxRenderer<BacklightPort>(
                                        model.getBacklightPorts().toArray(new BacklightPort[0]));
                            }
                            else if (function instanceof FlagFunction) {
                                result = new ComboBoxRenderer<Flag>(model.getFlags().toArray(new Flag[0]));
                            }
                            else if (function instanceof InputFunction) {
                                List<InputPort> inputPorts = new LinkedList<>();
                                inputPorts.add(InputPort.NONE);
                                inputPorts.addAll(model.getEnabledInputPorts());
                                result = new ComboBoxRenderer<InputPort>(inputPorts.toArray(new InputPort[0]));
                            }
                            else if (function instanceof LightPortAction) {
                                LinkedList<LightPort> ports = new LinkedList<>();
                                ports.add(LightPort.NONE);
                                ports.addAll(model.getEnabledLightPorts());
                                result = new ComboBoxRenderer<LightPort>(ports.toArray(new LightPort[0]));
                            }
                            else if (function instanceof MacroFunction) {
                                MacroFunction macroFunction = (MacroFunction) function;
                                if (MacroStatus.END.equals(macroFunction.getAction())) {
                                    // no renderer if 'End' because end always stops the current macro
                                }
                                else {
                                    result = new ComboBoxRenderer<Macro>(model.getMacros().toArray(new Macro[0]));
                                }
                            }
                            else if (function instanceof MotorPortAction) {
                                result =
                                    new ComboBoxRenderer<MotorPort>(model.getMotorPorts().toArray(new MotorPort[0]));
                            }
                            else if (function instanceof ServoPortAction) {
                                result =
                                    new ComboBoxRenderer<ServoPort>(model.getServoPorts().toArray(new ServoPort[0]));
                            }
                            else if (function instanceof SoundPortAction) {
                                result =
                                    new ComboBoxRenderer<SoundPort>(model.getSoundPorts().toArray(new SoundPort[0]));
                            }
                            else if (function instanceof SwitchPortAction) {
                                LinkedList<SwitchPort> ports = new LinkedList<>();
                                ports.add(SwitchPort.NONE);
                                ports.addAll(model.getEnabledSwitchPorts());
                                result = new ComboBoxRenderer<SwitchPort>(ports.toArray(new SwitchPort[0]));
                            }
                            else if (function instanceof SwitchPairPortAction) {
                                LinkedList<SwitchPairPort> ports = new LinkedList<>();
                                ports.add(SwitchPairPort.NONE);
                                ports.addAll(model.getEnabledSwitchPairPorts());
                                result = new ComboBoxRenderer<SwitchPairPort>(ports.toArray(new SwitchPairPort[0]));
                            }
                            break;
                        case MacroTableModel.COLUMN_EXTRA:
                            if (function instanceof ServoPortAction) {
                                result =
                                    new NumberWithLabelRenderer(
                                        Resources.getString(MacroContentPanel.class, "destination") + ":", "%");
                            }
                            else if (function instanceof BacklightPortAction) {
                                result =
                                    new NumberWithLabelRenderer(
                                        Resources.getString(MacroContentPanel.class, "brightness") + ":", "%");
                            }
                            break;
                    }
                }
                return result;
            }
        };
        table.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(MacroTableMenu.KEYSTROKE_CUT, "cut");
        table.getActionMap().put("cut", new AbstractAction() {
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {
                macroTableMenu.fireCut();
            }
        });
        table.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(MacroTableMenu.KEYSTROKE_COPY, "copy");
        table.getActionMap().put("copy", new AbstractAction() {
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {
                macroTableMenu.fireCopy();
            }
        });
        table.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(MacroTableMenu.KEYSTROKE_PASTE, "paste");
        table.getActionMap().put("paste", new AbstractAction() {
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {
                macroTableMenu.firePasteAfter();
            }
        });
        table.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(AccessoryTableMenu.KEYSTROKE_DELETE,
            "delete");
        table.getActionMap().put("delete", new AbstractAction() {
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {
                macroTableMenu.fireDelete();
            }
        });
        table.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        table.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                handleMouseEvent(e, macroTableMenu);
            }

            public void mouseReleased(MouseEvent e) {
                handleMouseEvent(e, macroTableMenu);
            }
        });
        // table.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        table.setSortable(false);

        // enable drag and drop
        table.setDragEnabled(true);
        table.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        table.setDropMode(DropMode.INSERT_ROWS);
        table.setTransferHandler(new MacroTableRowTransferHandler(table));

        for (int i = 0; i < table.getColumnModel().getColumnCount(); i++) {
            TableColumn column = table.getColumnModel().getColumn(i);
            switch (i) {
                case MacroTableModel.COLUMN_STEP:
                    column.setPreferredWidth(60);
                    break;
                case MacroTableModel.COLUMN_DELAY:
                    column.setPreferredWidth(120);
                    break;
                case MacroTableModel.COLUMN_EXTRA:
                    column.setPreferredWidth(120);
                    break;
                default:
                    column.setPreferredWidth(150);
                    break;
            }
        }

        setMinimumSize(new Dimension(400, 100));
        setPreferredSize(new Dimension(600, 200));

        JScrollPane scrollpane = new JScrollPane();
        scrollpane.setViewportView(table);

        add(scrollpane, BorderLayout.CENTER);

        model.addMacroListListener(new MacroListListener() {
            @Override
            public void listChanged() {
                tableModel.setRowCount(0);
            }

            @Override
            public void macroChanged() {
                Macro macro = model.getSelectedMacro();
                LOGGER.info("The macro has changed: {}", macro);
                if (macro != null) {
                    tableModel.setMacro(macro);
                }
            }

            @Override
            public void pendingChangesChanged() {
                LOGGER.info("pendingChangesChanged");
            }
        });
    }

    public void addTableListener(MacroTableListener l) {
        tableListeners.add(l);
    }

    private void fireCopy(int[] rows) {
        for (MacroTableListener l : tableListeners) {
            l.copy(getFunctions(rows));
        }
    }

    private void fireCut(int[] rows) {
        for (MacroTableListener l : tableListeners) {
            l.cut(rows, getFunctions(rows));
        }
    }

    private void fireDelete(int[] rows) {
        for (MacroTableListener l : tableListeners) {
            l.delete(rows);
        }
    }

    private void fireInsertEmptyAfter(int row) {
        for (MacroTableListener l : tableListeners) {
            l.insertEmptyAfter(row);
        }
    }

    private void fireInsertEmptyBefore(int row) {
        for (MacroTableListener l : tableListeners) {
            l.insertEmptyBefore(row);
        }
    }

    private void firePasteAfter(int row) {
        for (MacroTableListener l : tableListeners) {
            l.pasteAfter(row);
        }
    }

    private void firePasteBefore(int row) {
        for (MacroTableListener l : tableListeners) {
            l.pasteBefore(row);
        }
    }

    private void firePasteInvertedAfter(int row) {
        for (MacroTableListener l : tableListeners) {
            l.pasteInvertedAfter(row);
        }
    }

    private Function<? extends BidibStatus>[] getFunctions(int[] rows) {
        Function<? extends BidibStatus>[] result = new Function<?>[rows.length];

        for (int index = 0; index < rows.length; index++) {
            result[index] = model.getSelectedMacro().getFunction(rows[index]);
        }
        return result;
    }

    private int getRow(Point point) {
        return table.rowAtPoint(point);
    }

    private void handleMouseEvent(MouseEvent e, JPopupMenu popupMenu) {
        if (e.isPopupTrigger()) {
            int row = getRow(e.getPoint());

            if (table.getSelectedRowCount() == 0 && table.getRowCount() > 0 && row >= 0 && row < table.getRowCount()) {
                table.setRowSelectionInterval(row, row);
            }
            table.grabFocus();
            popupMenu.show(e.getComponent(), e.getX(), e.getY());
        }
    }
}
