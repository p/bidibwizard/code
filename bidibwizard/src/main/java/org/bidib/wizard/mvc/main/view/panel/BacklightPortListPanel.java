package org.bidib.wizard.mvc.main.view.panel;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.AbstractAction;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.event.ChangeEvent;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;

import org.apache.commons.collections4.CollectionUtils;
import org.bidib.wizard.comm.BacklightPortStatus;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.common.view.slider.SliderValueChangeListener;
import org.bidib.wizard.mvc.main.model.BacklightPort;
import org.bidib.wizard.mvc.main.model.BacklightPortTableModel;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.model.Port;
import org.bidib.wizard.mvc.main.model.SimplePortTableModel;
import org.bidib.wizard.mvc.main.model.listener.BacklightPortListener;
import org.bidib.wizard.mvc.main.model.listener.PortListListener;
import org.bidib.wizard.mvc.main.model.listener.PortValueListener;
import org.bidib.wizard.mvc.main.view.table.NumberCellEditor;
import org.bidib.wizard.mvc.main.view.table.NumberCellRenderer;
import org.bidib.wizard.mvc.main.view.table.PortTable;
import org.bidib.wizard.mvc.main.view.table.SliderEditor;
import org.bidib.wizard.mvc.main.view.table.TwoDimensionalArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jidesoft.grid.TableColumnChooser;

public class BacklightPortListPanel
    extends SimplePortListPanel<BacklightPortStatus, BacklightPort, BacklightPortListener>
    implements PortListListener, PortValueListener<BacklightPortStatus> {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(BacklightPortListPanel.class);

    private final MainModel mainModel;

    private AtomicBoolean updateModelInProgress = new AtomicBoolean();

    private static class BacklightSliderEditor extends SliderEditor {
        private static final long serialVersionUID = 1L;

        private int rowNumber;

        public BacklightSliderEditor(int minValue, int maxValue) {
            super(minValue, maxValue, null);
        }

        protected void setRowNumber(int rowNumber) {
            this.rowNumber = rowNumber;
        }

        protected int getRowNumber() {
            return rowNumber;
        }
    }

    private class BacklightPortTable extends PortTable {
        private static final long serialVersionUID = 1L;

        private final TwoDimensionalArray<SliderEditor> sliderEditors = new TwoDimensionalArray<SliderEditor>();

        public BacklightPortTable(TableModel tableModel, String emptyTableText) {
            super(tableModel, emptyTableText);
        }

        public TableCellEditor getCellEditor(int row, int column) {
            TableCellEditor result = null;

            switch (column) {
                case BacklightPortTableModel.COLUMN_DIM_SLOPE_DOWN:
                case BacklightPortTableModel.COLUMN_DIM_SLOPE_UP:
                    result = sliderEditors.get(row, column);
                    break;
                case BacklightPortTableModel.COLUMN_VALUE:
                    result = sliderEditors.get(row, column);
                    break;
                case BacklightPortTableModel.COLUMN_DMX_MAPPING:
                    result = new NumberCellEditor(255);
                    break;
                default:
                    result = super.getCellEditor(row, column);
                    break;
            }
            return result;
        }

        public TableCellRenderer getCellRenderer(int row, int column) {
            TableCellRenderer result = null;

            switch (column) {
                case BacklightPortTableModel.COLUMN_DIM_SLOPE_DOWN:
                case BacklightPortTableModel.COLUMN_DIM_SLOPE_UP:
                    LOGGER.debug("Get cell renderer for row: {}, column: {}", row, column);
                    try {
                        result = sliderEditors.get(row, column);
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Get slider editor failed for row: " + row + ", column: " + column, ex);
                    }

                    if (result == null) {
                        LOGGER.debug("Create new SliderEditor for row: {}, column: {}", row, column);
                        SliderEditor sliderEditor = new SliderEditor(1, true);
                        // TODO change this
                        sliderEditor.setMaxValue(65535);
                        sliderEditor.createComponent(1);

                        sliderEditors.set(sliderEditor, row, column);
                        result = sliderEditor;
                    }
                    break;
                case BacklightPortTableModel.COLUMN_DMX_MAPPING:
                    result = new NumberCellRenderer();
                    break;
                case BacklightPortTableModel.COLUMN_VALUE:
                    LOGGER.debug("Get cell renderer for row: {}, column: {}", row, column);
                    try {
                        result = sliderEditors.get(row, column);
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Get slider editor failed for row: " + row + ", column: " + column, ex);
                    }

                    if (result == null) {
                        LOGGER.debug("Create new BacklightSliderEditor for row: {}, column: {}", row, column);

                        BacklightPort backlightPort =
                            (BacklightPort) tableModel.getValueAt(row, BacklightPortTableModel.COLUMN_PORT_INSTANCE);
                        LOGGER.debug("Prepare new sliderEditor with initial value: {}",
                            backlightPort.getRelativeValue());

                        final BacklightSliderEditor sliderEditor = new BacklightSliderEditor(0, 100);
                        sliderEditor.setRowNumber(row);
                        sliderEditor.createComponent(backlightPort.getRelativeValue());

                        final SliderValueChangeListener sliderValueChangeListener = new SliderValueChangeListener() {

                            @Override
                            public void stateChanged(ChangeEvent e, boolean isAdjusting, int value) {
                                // only handle if not adjusting
                                if (!isAdjusting) {
                                    BacklightPort port =
                                        (BacklightPort) tableModel.getValueAt(sliderEditor.getRowNumber(),
                                            tableModel.getColumnCount() - 1);
                                    port.setAdjusting(false);
                                    LOGGER.info("Update with final value, backlight port: {}, row: {}, value: {}",
                                        port.getDebugString(), sliderEditor.getRowNumber(), value);

                                    if (!updateModelInProgress.get()) {
                                        // int finalValue = port.getAbsoluteValue(value);
                                        // LOGGER.info("Would set the final value on the port: {}", finalValue);
                                        // port.setValue(finalValue);
                                        fireTestButtonPressed(port);
                                    }
                                    else {
                                        LOGGER.debug(
                                            "Do not send new port status to backlight because the model was updated: {}",
                                            port);
                                    }
                                }
                                else {
                                    // value is adjusting
                                    BacklightPort port =
                                        (BacklightPort) tableModel.getValueAt(sliderEditor.getRowNumber(),
                                            tableModel.getColumnCount() - 1);

                                    port.setAdjusting(true);
                                    if (port.getPreAdjustingValue() < 0) {
                                        LOGGER.info("Set the pre-adjusting value: {}, row: {}", port.getValue(),
                                            sliderEditor.getRowNumber());
                                        port.setPreAdjustingValue(port.getValue());
                                    }
                                    // set the temporary value
                                    port.setValue(port.getAbsoluteValue(value), true);

                                    LOGGER.info("Update during adjusting backlight port: {}, row: {}",
                                        port.getDebugString(), sliderEditor.getRowNumber());
                                    if (!updateModelInProgress.get()) {
                                        fireTestButtonPressed(port);
                                    }
                                    else {
                                        LOGGER.debug(
                                            "Do not send new port status to backlight because the model was updated: {}",
                                            port);
                                    }
                                }
                            }
                        };
                        sliderEditor.setSliderValueChangeListener(sliderValueChangeListener);

                        sliderEditors.set(sliderEditor, row, column);
                        result = sliderEditor;
                    }
                    break;
                default:
                    result = super.getCellRenderer(row, column);
                    break;
            }
            return result;
        }

        @Override
        public void clearTable() {
            // remove all rows and remove all slider editors
            tableModel.setRowCount(0);
            sliderEditors.clear();
        }

        public void updateSliderPosition(int row, int column, int value) {
            SliderEditor current = sliderEditors.get(row, column);
            if (current != null) {
                LOGGER.info("Set the new slider value: {}", value);
                // current.setValue(value);
            }
            else {
                LOGGER.info("No editor available for row: {}, column: {}", row, column);
            }
        }
    }

    public BacklightPortListPanel(MainModel mainModel) {
        super(new BacklightPortTableModel(mainModel), mainModel.getBacklightPorts(),
            Resources.getString(BacklightPortListPanel.class, "emptyTable"));

        this.mainModel = mainModel;
        this.mainModel.addBacklightPortValueListener(this);

        // remove the column with the port instance
        TableColumnChooser.hideColumn(table, BacklightPortTableModel.COLUMN_PORT_INSTANCE);

        // let the left and right key change the slider value
        table.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0),
            "left");
        table.getActionMap().put("left", new AbstractAction() {
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {
                JTable table = (JTable) e.getSource();
                TableCellEditor editor = table.getCellEditor(table.getSelectedRow(), table.getSelectedColumn());

                if (editor instanceof SliderEditor) {
                    table.editCellAt(table.getSelectedRow(), table.getSelectedColumn());
                    ((SliderEditor) editor).setDown();
                }
            }
        });
        table.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0),
            "right");
        table.getActionMap().put("right", new AbstractAction() {
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {
                JTable table = (JTable) e.getSource();
                TableCellEditor editor = table.getCellEditor(table.getSelectedRow(), table.getSelectedColumn());

                if (editor instanceof SliderEditor) {
                    table.editCellAt(table.getSelectedRow(), table.getSelectedColumn());
                    ((SliderEditor) editor).setUp();
                }
            }
        });

        // add the panel as port listener
        mainModel.addBacklightPortListListener(this);
    }

    @Override
    protected PortTable createPortTable(
        SimplePortTableModel<BacklightPortStatus, BacklightPort, BacklightPortListener> tableModel,
        String emptyTableText) {

        PortTable portTable = new BacklightPortTable(tableModel, emptyTableText);

        return portTable;
    }

    @Override
    public void listChanged() {
        table.clearTable();
        for (BacklightPort port : mainModel.getBacklightPorts()) {
            tableModel.addRow(port);
        }
        table.packColumn(table.getColumnCount() - 2, 2);
    }

    private void fireTestButtonPressed(BacklightPort port) {
        LOGGER.debug("Slider value has changed for port: {}", port);

        for (BacklightPortListener l : tableModel.getPortListeners()) {
            l.testButtonPressed(port);
        }
    }

    @Override
    public void valueChanged(Port<BacklightPortStatus> port) {
        LOGGER.debug("The value of a backlight port has changed: {}, value: {}", port,
            ((BacklightPort) port).getValue());

        try {
            // do not trigger sending new configuration to port ...
            updateModelInProgress.set(true);
            LOGGER.info("Update model has started.");

            int rowCount = tableModel.getRowCount();
            for (int row = 0; row < rowCount; row++) {
                BacklightPort backlightPort =
                    (BacklightPort) tableModel.getValueAt(row, BacklightPortTableModel.COLUMN_PORT_INSTANCE);
                if (backlightPort.getId() == port.getId()) {
                    LOGGER.debug("Found the matching port: {}", backlightPort);
                    ((BacklightPortTable) table).updateSliderPosition(row, BacklightPortTableModel.COLUMN_VALUE,
                        backlightPort.getRelativeValue());
                    break;
                }
            }
        }
        finally {
            LOGGER.debug("Update model has finished.");
            updateModelInProgress.set(false);
        }
    }

    @Override
    protected void refreshPorts() {

        Node node = mainModel.getSelectedNode();
        if (node != null) {
            if (node.getNode().isPortFlatModelAvailable()) {
                if (CollectionUtils.isNotEmpty(node.getGenericPorts())) {
                    mainModel.getBacklightPorts();
                }
                else {
                    LOGGER.info(
                        "The node supports flat port model but no generic ports are available. Skip get backlight ports.");
                }
            }
            else {
                mainModel.getBacklightPorts();
            }
        }
    }
}
