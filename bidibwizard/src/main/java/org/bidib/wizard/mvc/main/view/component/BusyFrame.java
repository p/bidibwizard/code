package org.bidib.wizard.mvc.main.view.component;

public interface BusyFrame {
    /**
     * Set or reset the frame busy state.
     * 
     * @param busy
     *            {@code true}: set the wait cursor, {@code false}: set the default cursor
     * @return the cursor was changed
     */
    boolean setBusy(boolean busy);
}
