package org.bidib.wizard.mvc.main.model;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Vector;

import javax.swing.SwingUtilities;

import org.apache.commons.lang.StringUtils;
import org.bidib.jbidibc.core.AccessoryState;
import org.bidib.jbidibc.core.AccessoryStateOptions;
import org.bidib.jbidibc.core.enumeration.TimeBaseUnitEnum;
import org.bidib.jbidibc.core.utils.AccessoryStateUtils;
import org.bidib.jbidibc.core.utils.AccessoryStateUtils.ErrorAccessoryState.AccessoryExecutionState;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.main.model.listener.AccessoryListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.beans.Model;

public class Accessory extends Model implements LabelAware {
    private static final long serialVersionUID = 1L;

    static {
        try {
            // Q: why is this needed? A: export of beans with XMLDecoder
            PropertyDescriptor[] descriptor = Introspector.getBeanInfo(Accessory.class).getPropertyDescriptors();

            for (int i = 0; i < descriptor.length; i++) {
                PropertyDescriptor propertyDescriptor = descriptor[i];
                if (propertyDescriptor.getName().equals("accessoryExecutionState")) {
                    propertyDescriptor.setValue("transient", Boolean.TRUE);
                }
                else if (propertyDescriptor.getName().equals("accessoryState")) {
                    propertyDescriptor.setValue("transient", Boolean.TRUE);
                }
                else if (propertyDescriptor.getName().equals("accessoryConfigState")) {
                    propertyDescriptor.setValue("transient", Boolean.TRUE);
                }
                else if (propertyDescriptor.getName().equals("accessorySaveState")) {
                    propertyDescriptor.setValue("transient", Boolean.TRUE);
                }
                else if (propertyDescriptor.getName().equals("listeners")) {
                    propertyDescriptor.setValue("transient", Boolean.TRUE);
                }
            }
        }
        catch (IntrospectionException e) {
            throw new RuntimeException(e);
        }
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(Accessory.class);

    public static final String PROPERTY_PENDING_CHANGES = "pendingChanges";

    public static final String PROPERTY_TOTAL_ASPECTS = "totalAspects";

    public static final String PROPERTY_STARTUP_STATE = "startupState";

    public static final String PROPERTYNAME_SWITCH_TIME = "switchTime";

    public static final String PROPERTYNAME_TIME_BASE_UNIT = "timeBaseUnit";

    public static final String PROPERTY_HAS_EMERGENCY_STOP = "hasEmergencyStop";

    public static final String PROPERTY_HAS_STATE_WITH_ADDITIONAL_INFO = "hasStateWithAdditionalInfo";

    public static final String PROPERTY_LABEL = "label";

    public static final Accessory NONE = new Accessory.Builder(-1)./* setLabel("<none>"). */build();

    private transient final Collection<AccessoryListener> listeners = new LinkedList<AccessoryListener>();

    private int id;

    private String label;

    private Vector<MacroRef> aspects = new Vector<>();

    private int macroSize;

    private transient AccessoryExecutionState accessoryExecutionState;

    private transient AccessoryState accessoryState;

    private transient AccessoryStateOptions accessoryStateOptions;

    private static enum AccessoryConfigState {
        pending, initialized;
    }

    /**
     * The accessory startup state
     */
    private Integer startupState;

    /**
     * The accessory switch time
     */
    private Integer switchTime;

    /**
     * The accessory switch time bas unit
     */
    private TimeBaseUnitEnum timeBaseUnit = TimeBaseUnitEnum.UNIT_100MS;

    /**
     * The accessory supports emergency stop
     */
    private Boolean hasEmergencyStop;

    /**
     * The accessory state will supply additional info
     */
    private Boolean hasStateWithAdditionalInfo;

    private boolean macroMapped;

    private int totalAspects;

    private transient AccessoryConfigState accessoryConfigState = AccessoryConfigState.pending;

    private transient AccessorySaveState accessorySaveState = AccessorySaveState.PENDING_CHANGES;

    private transient boolean editable = true;

    public Accessory() {

    }

    private Accessory(Builder builder) {
        setId(builder.id);
        setLabel(builder.label);
    }

    public void addAccessoryListener(AccessoryListener l) {
        listeners.add(l);
    }

    public void removeAccessoryListener(AccessoryListener l) {
        listeners.remove(l);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        String oldValue = this.label;
        this.label = label;

        firePropertyChange(PROPERTY_LABEL, oldValue, label);
    }

    public void addAspect(MacroRef macro) {
        addAspect(aspects.size(), macro);
    }

    private boolean addAspect(int index, MacroRef macroRef) {
        boolean result = false;

        if (macroRef != null && index >= 0 && aspects.size() < macroSize) {

            aspects.add(index, macroRef);
            fireMacrosChanged();
            result = true;
        }
        else {
            LOGGER.warn("This macro is not added because the maximum macro size is reached, index: {}, macroRef: {}",
                index, macroRef);
        }
        return result;
    }

    public void addAspectAfter(int index, MacroRef macro) {
        addAspect(index + 1, macro);
    }

    public void addAspectsAfter(int index, MacroRef[] macros) {
        LOGGER.debug("Add aspects after, index: {}, macros: {}", index, macros);
        if (macros != null) {
            for (MacroRef macro : macros) {
                index++;
                addAspect(index, macro);
            }
        }
    }

    public void addAspectBefore(int index, MacroRef macro) {
        addAspect(index, macro);
    }

    public void clearAspects() {
        aspects.clear();
        fireMacrosChanged();
    }

    public int getAspectCount() {
        return aspects.size();
    }

    /**
     * <p>
     * Be careful when change this because the XML encoder uses this method!
     * </p>
     * 
     * @return the aspects of the accessory
     */
    public Collection<MacroRef> getAspects() {
        return new Vector<MacroRef>(aspects);
    }

    /**
     * Set the aspects.
     * <p>
     * Be careful when change this because the XML encoder uses this method!
     * </p>
     * 
     * @param aspects
     *            the aspects
     */
    public void setAspects(List<MacroRef> aspects) {
        this.aspects = new Vector<>(aspects);
    }

    public void removeMacro(int index) {
        aspects.remove(index);
        fireMacrosChanged();
    }

    public void setMacro(int index, int macro) {
        aspects.set(index, new MacroRef(macro));

        fireMacrosChanged();
    }

    public int getMacroSize() {
        return macroSize;
    }

    /**
     * Set the maximum number of aspects == macros that can be stored in an accessory.
     * 
     * @param macroSize
     *            the maximum number of macros
     */
    public void setMacroSize(int macroSize) {
        this.macroSize = macroSize;
        if (this.aspects.size() > macroSize) {
            this.aspects.setSize(macroSize);
        }
    }

    /**
     * @return the macroMapped
     */
    public boolean isMacroMapped() {
        return macroMapped;
    }

    /**
     * @param macroMapped
     *            the macroMapped to set
     */
    public void setMacroMapped(boolean macroMapped) {
        this.macroMapped = macroMapped;
    }

    /**
     * @return the startupState
     */
    public Integer getStartupState() {
        return startupState;
    }

    /**
     * @param startupState
     *            the startupState to set
     */
    public void setStartupState(Integer startupState) {
        Integer oldValue = this.startupState;

        this.startupState = startupState;

        firePropertyChange(PROPERTY_STARTUP_STATE, oldValue, startupState);

        if (!Objects.equals(oldValue, startupState)) {
            firePendingChanges();
        }
    }

    /**
     * @return the switchTime
     */
    public Integer getSwitchTime() {
        return switchTime;
    }

    /**
     * @param switchTime
     *            the switchTime to set
     */
    public void setSwitchTime(Integer switchTime) {
        Integer oldValue = this.switchTime;
        this.switchTime = switchTime;

        firePropertyChange(PROPERTYNAME_SWITCH_TIME, oldValue, switchTime);

        if (!Objects.equals(oldValue, switchTime)) {
            firePendingChanges();
        }
    }

    /**
     * @return the timeBaseUnit
     */
    public TimeBaseUnitEnum getTimeBaseUnit() {
        return timeBaseUnit;
    }

    /**
     * @param timeBaseUnit
     *            the timeBaseUnit to set
     */
    public void setTimeBaseUnit(TimeBaseUnitEnum timeBaseUnit) {
        TimeBaseUnitEnum oldValue = this.timeBaseUnit;
        this.timeBaseUnit = timeBaseUnit;

        firePropertyChange(PROPERTYNAME_TIME_BASE_UNIT, oldValue, timeBaseUnit);

        if (!Objects.equals(oldValue, timeBaseUnit)) {
            firePendingChanges();
        }
    }

    private void firePendingChanges() {

        setAccessorySaveState(AccessorySaveState.PENDING_CHANGES);
    }

    /**
     * @return the hasEmergencyStop
     */
    public Boolean isHasEmergencyStop() {
        return hasEmergencyStop;
    }

    /**
     * @param hasEmergencyStop
     *            the hasEmergencyStop to set
     */
    public void setHasEmergencyStop(Boolean hasEmergencyStop) {
        Boolean oldValue = this.hasEmergencyStop;

        this.hasEmergencyStop = hasEmergencyStop;

        firePropertyChange(PROPERTY_HAS_EMERGENCY_STOP, oldValue, hasEmergencyStop);
    }

    /**
     * @return the hasStateWithAdditionalInfo
     */
    public Boolean isHasStateWithAdditionalInfo() {
        return hasEmergencyStop;
    }

    /**
     * @param hasStateWithAdditionalInfo
     *            the hasStateWithAdditionalInfo to set
     */
    public void setHasStateWithAdditionalInfo(Boolean hasStateWithAdditionalInfo) {
        Boolean oldValue = this.hasStateWithAdditionalInfo;

        this.hasStateWithAdditionalInfo = hasStateWithAdditionalInfo;

        firePropertyChange(PROPERTY_HAS_STATE_WITH_ADDITIONAL_INFO, oldValue, hasStateWithAdditionalInfo);
    }

    /**
     * @return the totalAspects
     */
    public int getTotalAspects() {
        return totalAspects;
    }

    /**
     * @param totalAspects
     *            the totalAspects to set
     */
    public void setTotalAspects(int totalAspects) {
        LOGGER.info("Set total number of aspects: {}", totalAspects);
        // int oldValue = this.totalAspects;

        this.totalAspects = totalAspects;

        if (!macroMapped) {
            // TODO signal the total number of aspects has changed
            fireTotalAspectsChanged(id);
            // firePropertyChange(PROPERTY_TOTAL_ASPECTS, -1, id/* oldValue, this.totalAspects */);
        }
    }

    public AccessoryExecutionState getAccessoryExecutionState() {
        return accessoryExecutionState;
    }

    public void setAccessoryExecutionState(AccessoryExecutionState accessoryExecutionState, int aspect) {
        LOGGER.info("Set the new accessoryExecutionState: {}", accessoryExecutionState);
        this.accessoryExecutionState = accessoryExecutionState;

        fireAccessoryStateChanged(aspect);
    }

    public void setAccessoryState(
        final AccessoryState accessoryState, final AccessoryStateOptions accessoryStateOptions) {
        LOGGER.info("Set the new accessoryState: {}, accessoryStateOptions: {}", accessoryState, accessoryStateOptions);
        this.accessoryState = accessoryState;
        this.accessoryStateOptions = accessoryStateOptions;

        if (AccessoryConfigState.pending.equals(accessoryConfigState)) {

            LOGGER.info("The initial AccessoryState is delivered: {}", accessoryState);

            setTotalAspects(ByteUtils.getInt(accessoryState.getTotal()));

            accessoryConfigState = AccessoryConfigState.initialized;
        }

        AccessoryExecutionState accessoryExecutionState =
            AccessoryStateUtils.getExecutionState(accessoryState.getAspect(), accessoryState.getExecute());
        LOGGER.debug("Set the new accessoryState: {}, accessoryExecutionState: {}", accessoryState,
            accessoryExecutionState);

        // prevent send state changed twice
        LOGGER.info("Set the new accessoryExecutionState: {}", accessoryExecutionState);
        this.accessoryExecutionState = accessoryExecutionState;

        fireAccessoryStateChanged(accessoryState.getAspect());
    }

    public AccessoryState getAccessoryState() {
        return accessoryState;
    }

    public AccessoryStateOptions getAccessoryStateOptions() {
        return accessoryStateOptions;
    }

    private void fireMacrosChanged() {
        // setPendingChanges(true);
        setAccessorySaveState(AccessorySaveState.PENDING_CHANGES);

        for (AccessoryListener l : listeners) {
            l.macrosChanged();
        }
    }

    private void fireAccessoryStateChanged(final int aspect) {
        if (SwingUtilities.isEventDispatchThread()) {
            for (AccessoryListener l : listeners) {
                l.accessoryStateChanged(getId(), aspect);
            }
        }
        else {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    for (AccessoryListener l : listeners) {
                        l.accessoryStateChanged(getId(), aspect);
                    }
                }
            });
        }
    }

    private void fireTotalAspectsChanged(final int id) {

        if (SwingUtilities.isEventDispatchThread()) {
            LOGGER.info("Fire total number of aspects changed: {}", id);
            firePropertyChange(PROPERTY_TOTAL_ASPECTS, -1, id/* oldValue, this.totalAspects */);
        }
        else {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    LOGGER.info("Fire total number of aspects changed: {}", id);
                    firePropertyChange(PROPERTY_TOTAL_ASPECTS, -1, id/* oldValue, this.totalAspects */);
                }
            });
        }
    }

    /**
     * @return the accessorySaveState
     */
    public AccessorySaveState getAccessorySaveState() {
        return accessorySaveState;
    }

    /**
     * @param accessorySaveState
     *            the accessorySaveState to set
     */
    public void setAccessorySaveState(AccessorySaveState accessorySaveState) {
        AccessorySaveState oldValue = this.accessorySaveState;
        this.accessorySaveState = accessorySaveState;

        firePropertyChange(PROPERTY_PENDING_CHANGES, oldValue, this.accessorySaveState);
    }

    /**
     * @return the editable
     */
    public boolean isEditable() {
        return editable;
    }

    /**
     * @param editable
     *            the editable to set
     */
    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Accessory) {
            return ((Accessory) obj).getId() == getId();
        }
        return false;
    }

    @Override
    public int hashCode() {
        return getId();
    }

    public String getDebugString() {
        StringBuilder sb = new StringBuilder(getClass().getSimpleName());
        sb
            .append("[id=").append(id).append(",label=").append(label).append(",macros=").append(aspects)
            .append(",macroSize=").append(macroSize).append(",startup=").append(startupState).append("]");
        return sb.toString();
    }

    public String toString() {
        String result = "";

        if (StringUtils.isNotBlank(label)) {
            result = label;
        }
        else {
            result = Resources.getString(getClass(), "label") + "_" + id;
        }
        return result;
    }

    public static class Builder {
        private final int id;

        private String label;

        public Builder(int id) {
            this.id = id;
        }

        public Builder setLabel(String label) {
            this.label = label;
            return this;
        }

        public Accessory build() {
            return new Accessory(this);
        }
    }

    /**
     * Initialize the accessory with default values.
     */
    public void initialize() {
        clearAspects();
    }
}
