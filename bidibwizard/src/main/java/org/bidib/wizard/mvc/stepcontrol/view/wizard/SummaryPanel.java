package org.bidib.wizard.mvc.stepcontrol.view.wizard;

import java.awt.BorderLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.common.view.wizard.JWizardComponents;
import org.bidib.wizard.mvc.stepcontrol.model.ConfigurationWizardModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.debug.FormDebugPanel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.FormLayout;

public class SummaryPanel extends AbstractWizardPanel {
    private static final Logger LOGGER = LoggerFactory.getLogger(SummaryPanel.class);

    private ConfigurationWizardModel configurationWizardModel;

    public SummaryPanel(JWizardComponents wizardComponents, final ConfigurationWizardModel configurationWizardModel) {
        super(wizardComponents);

        this.configurationWizardModel = configurationWizardModel;
    }

    @Override
    protected void initPanel() {
        super.initPanel();

        // prepare the form
        DefaultFormBuilder builder = null;
        boolean debugDialog = false;
        if (debugDialog) {
            JPanel panel = new FormDebugPanel();
            builder = new DefaultFormBuilder(new FormLayout("p, 3dlu, p:g"), panel);
        }
        else {
            JPanel panel = new JPanel(new BorderLayout());
            builder = new DefaultFormBuilder(new FormLayout("p, 3dlu, p:g"), panel);
        }
        builder.border(Borders.TABBED_DIALOG);

        builder.append(new JLabel(Resources.getString(getClass(), "message")), 3);

        // html content
        ValueModel htmlContentValueModel =
            new PropertyAdapter<ConfigurationWizardModel>(configurationWizardModel,
                ConfigurationWizardModel.PROPERTYNAME_HTMLCONTENT, true);
        JLabel htmlContentLabel = BasicComponentFactory.createLabel(htmlContentValueModel);
        builder.append(htmlContentLabel, 3);

        // build the panel
        this.panel = builder.build();
    }

    @Override
    public void update() {
        LOGGER.info("update is called.");
        super.update();

        configurationWizardModel.triggerUpdateHtmlContent();
        // updateGearingComponents();
    }
}
