package org.bidib.wizard.mvc.pt.view.panel.listener;

import org.bidib.jbidibc.core.enumeration.PtOperation;

public interface PtRequestListener {
    /**
     * Send the PT request.
     * 
     * @param ptResultListener
     *            the result listener that performs the request
     * @param operation
     *            the operation
     * @param cvNumber
     *            the CV number
     * @param cvValue
     *            the CV value
     */
    void sendRequest(PtResultListener ptResultListener, PtOperation operation, int cvNumber, int cvValue);
}
