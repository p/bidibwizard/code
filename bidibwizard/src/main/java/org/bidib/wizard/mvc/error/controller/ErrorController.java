package org.bidib.wizard.mvc.error.controller;

import java.lang.Thread.UncaughtExceptionHandler;

import org.bidib.wizard.mvc.error.model.ErrorModel;
import org.bidib.wizard.mvc.error.view.ErrorView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ErrorController {
    private static final Logger LOG = LoggerFactory.getLogger(ErrorController.class.getName());

    private static final ErrorModel model = new ErrorModel();

    public void start() {
        Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable e) {
                LOG.error("Uncaught exception detected: " + model.getStackTrace(e));
                setError(e);
            }
        });
        new ErrorView(
            model) {
            @Override
            public void windowClosed() {
                System.exit(20);
            }
        };
    }

    public static void setError(Throwable e) {
        model.setError(e);
    }
}
