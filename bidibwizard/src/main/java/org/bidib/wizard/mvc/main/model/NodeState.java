package org.bidib.wizard.mvc.main.model;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Vector;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.bidib.jbidibc.core.Feature;
import org.bidib.jbidibc.core.StringData;
import org.bidib.jbidibc.core.exception.InvalidConfigurationException;
import org.bidib.jbidibc.core.node.ConfigurationVariable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is used to import and export the node.
 * 
 */
public class NodeState {
    private static final Logger LOGGER = LoggerFactory.getLogger(NodeState.class);

    private List<Accessory> accessories;

    private List<Macro> macros;

    private List<AnalogPort> analogPorts;

    private List<BacklightPort> backlightPorts;

    private List<FeedbackPort> feedbackPorts;

    private List<LightPort> lightPorts;

    private List<ServoPort> servoPorts;

    private List<InputPort> inputPorts;

    private List<SwitchPort> switchPorts;

    private List<SwitchPairPort> switchPairPorts;

    private List<SoundPort> soundPorts;

    private List<MotorPort> motorPorts;

    private List<Feature> features;

    private List<ConfigurationVariable> configurationVariables;

    private List<StringData> nodeStrings;

    private boolean flatPortModel;

    public NodeState() {
    }

    public NodeState(Collection<Accessory> accessories, Collection<AnalogPort> analogPorts,
        Collection<BacklightPort> backlightPorts, Collection<FeedbackPort> feedbackPorts,
        Collection<LightPort> lightPorts, Collection<Macro> macros, List<ServoPort> servoPorts,
        Collection<InputPort> inputPorts, Collection<SwitchPort> switchPorts,
        Collection<SwitchPairPort> switchPairPorts, Collection<SoundPort> soundPorts, Collection<MotorPort> motorPorts,
        Collection<Feature> features, Collection<ConfigurationVariable> configurationVariables,
        Collection<StringData> nodeStrings) {
        setMacros(macros);
        setAccessories(accessories);

        LOGGER.info("Number of lightports: {}", lightPorts.size());

        setAnalogPorts(analogPorts);
        setBacklightPorts(backlightPorts);
        setFeedbackPorts(feedbackPorts);
        setLightPorts(lightPorts);
        setServoPorts(servoPorts);
        setInputPorts(inputPorts);
        setSoundPorts(soundPorts);
        setSwitchPorts(switchPorts);
        setSwitchPairPorts(switchPairPorts);
        setMotorPorts(motorPorts);
        setFeatures(features);
        setConfigurationVariables(configurationVariables);
        setNodeStrings(nodeStrings);
    }

    public Collection<Accessory> getAccessories() {
        return accessories;
    }

    public void setAccessory(int index, Accessory accessory) {
        accessories.set(index, accessory);
    }

    public void setAccessories(Collection<Accessory> accessories) {
        this.accessories = new Vector<Accessory>(accessories);
    }

    /**
     * @return the analog ports
     */
    public Collection<AnalogPort> getAnalogPorts() {
        return analogPorts;
    }

    public void setAnalogPort(int index, AnalogPort analogPort) {
        analogPorts.set(index, analogPort);
    }

    public void setAnalogPorts(Collection<AnalogPort> analogPorts) {
        this.analogPorts = new Vector<AnalogPort>(analogPorts);
    }

    /**
     * @return the backlight ports
     */
    public Collection<BacklightPort> getBacklightPorts() {
        return backlightPorts;
    }

    public void setBacklightPort(int index, BacklightPort backlightPort) {
        backlightPorts.set(index, backlightPort);
    }

    public void setBacklightPorts(Collection<BacklightPort> backlightPorts) {
        this.backlightPorts = new Vector<BacklightPort>(backlightPorts);
    }

    /**
     * @return the feedback ports
     */
    public List<FeedbackPort> getFeedbackPorts() {
        return feedbackPorts;
    }

    public void setFeedbackPort(int index, FeedbackPort feedbackPort) {
        feedbackPorts.set(index, feedbackPort);
    }

    public void setFeedbackPorts(Collection<FeedbackPort> feedbackPorts) {
        this.feedbackPorts = new Vector<FeedbackPort>(feedbackPorts);
    }

    /**
     * @return the light ports
     */
    public Collection<LightPort> getLightPorts() {
        return lightPorts;
    }

    public void setLightPorts(Collection<LightPort> lightPorts) {
        this.lightPorts = new Vector<LightPort>(lightPorts);
    }

    public Collection<Macro> getMacros() {
        return macros;
    }

    public void setMacro(int index, Macro macro) {
        macros.set(index, macro);
    }

    public void setMacros(Collection<Macro> macros) {
        this.macros = new Vector<Macro>(macros);
    }

    public List<ServoPort> getServoPorts() {
        return servoPorts;
    }

    public void setServoPort(int index, ServoPort servoPort) {
        servoPorts.set(index, servoPort);
    }

    public void setServoPorts(List<ServoPort> servoPorts) {
        this.servoPorts = new ArrayList<ServoPort>(servoPorts);
    }

    /**
     * @return the inputPorts
     */
    public Collection<InputPort> getInputPorts() {
        return inputPorts;
    }

    public void setInputPort(int index, InputPort inputPort) {
        inputPorts.set(index, inputPort);
    }

    /**
     * @param inputPorts
     *            the inputPorts to set
     */
    public void setInputPorts(Collection<InputPort> inputPorts) {
        this.inputPorts = new Vector<InputPort>(inputPorts);
    }

    /**
     * @return the soundPorts
     */
    public Collection<SoundPort> getSoundPorts() {
        return soundPorts;
    }

    /**
     * @param soundPorts
     *            the soundPorts to set
     */
    public void setSoundPorts(Collection<SoundPort> soundPorts) {
        this.soundPorts = new Vector<SoundPort>(soundPorts);
    }

    public void setSoundPort(int index, SoundPort soundPort) {
        soundPorts.set(index, soundPort);
    }

    /**
     * @return the switchPorts
     */
    public Collection<SwitchPort> getSwitchPorts() {
        return switchPorts;
    }

    public void setSwitchPort(int index, SwitchPort switchPort) {
        switchPorts.set(index, switchPort);
    }

    /**
     * @param switchPairPorts
     *            the switchPairPorts to set
     */
    public void setSwitchPairPorts(Collection<SwitchPairPort> switchPairPorts) {
        this.switchPairPorts = new Vector<SwitchPairPort>(switchPairPorts);
    }

    /**
     * @return the switchPairPorts
     */
    public Collection<SwitchPairPort> getSwitchPairPorts() {
        return switchPairPorts;
    }

    public void setSwitchPairPort(int index, SwitchPairPort switchPairPort) {
        switchPairPorts.set(index, switchPairPort);
    }

    /**
     * @param switchPorts
     *            the switchPorts to set
     */
    public void setSwitchPorts(Collection<SwitchPort> switchPorts) {
        this.switchPorts = new Vector<SwitchPort>(switchPorts);
    }

    /**
     * @return the motorPorts
     */
    public Collection<MotorPort> getMotorPorts() {
        return motorPorts;
    }

    public void setMotorPort(int index, MotorPort motorPort) {
        motorPorts.set(index, motorPort);
    }

    /**
     * @param motorPorts
     *            the motorPorts to set
     */
    public void setMotorPorts(Collection<MotorPort> motorPorts) {
        this.motorPorts = new Vector<MotorPort>(motorPorts);
    }

    /**
     * @return the features
     */
    public Collection<Feature> getFeatures() {
        return features;
    }

    /**
     * @param features
     *            the features to set
     */
    public void setFeatures(Collection<Feature> features) {
        this.features = new Vector<Feature>();
        if (features != null) {
            this.features.addAll(features);
        }
    }

    /**
     * @return the configurationVariables
     */
    public Collection<ConfigurationVariable> getConfigurationVariables() {
        return configurationVariables;
    }

    /**
     * @param configurationVariables
     *            the configurationVariables to set
     */
    public void setConfigurationVariables(Collection<ConfigurationVariable> configurationVariables) {
        this.configurationVariables = new Vector<ConfigurationVariable>();
        if (configurationVariables != null) {
            this.configurationVariables.addAll(configurationVariables);
        }
    }

    /**
     * @return the nodeStrings
     */
    public Collection<StringData> getNodeStrings() {
        return nodeStrings;
    }

    /**
     * @param nodeStrings
     *            the nodeStrings to set
     */
    public void setNodeStrings(Collection<StringData> nodeStrings) {
        this.nodeStrings = new Vector<StringData>();
        if (nodeStrings != null) {
            this.nodeStrings.addAll(nodeStrings);
        }
    }

    /**
     * @return the flatPortModel
     */
    public boolean isFlatPortModel() {
        return flatPortModel;
    }

    /**
     * @param flatPortModel
     *            the flatPortModel to set
     */
    public void setFlatPortModel(boolean flatPortModel) {
        this.flatPortModel = flatPortModel;
    }

    public static NodeState load(String fileName) {
        NodeState result = null;
        XMLDecoder d = null;

        InputStream is = null;

        try {
            try {
                // try to open file with gzip stream
                is = new BufferedInputStream(new FileInputStream(fileName));
                d = new XMLDecoder(new GZIPInputStream(is));

                result = (NodeState) d.readObject();
            }
            catch (IOException e) {
                LOGGER.warn("Load zipped node file failed. Try unzipped.", e);
                if (is != null) {
                    try {
                        is.close();
                    }
                    catch (IOException e1) {
                        LOGGER.warn("Close inputstream failed.", e1);
                    }
                    is = null;
                }
                if (is == null) {
                    is = new BufferedInputStream(new FileInputStream(fileName));
                }
                d = new XMLDecoder(is);

                result = (NodeState) d.readObject();
            }
        }
        catch (FileNotFoundException e) {
            LOGGER.warn("Load node file failed.", e);
        }
        catch (ArrayIndexOutOfBoundsException ex) {
            LOGGER.warn("Load node file failed.", ex);
            InvalidConfigurationException th = new InvalidConfigurationException("Load node file failed.");
            th.setReason("invalid-file-format");
            throw th;
        }
        finally {
            if (d != null) {
                d.close();
            }
            if (is != null) {
                try {
                    is.close();
                }
                catch (Exception ex) {
                    LOGGER.warn("Close input stream failed.", ex);
                }
            }
        }
        return result;
    }

    /**
     * Save the configuration of the node.
     * 
     * @param fileName
     *            the filename
     * @throws IOException
     */
    public void save(String fileName) throws IOException {
        XMLEncoder e = new XMLEncoder(new GZIPOutputStream(new BufferedOutputStream(new FileOutputStream(fileName))));

        e.writeObject(this);
        e.close();
    }
}
