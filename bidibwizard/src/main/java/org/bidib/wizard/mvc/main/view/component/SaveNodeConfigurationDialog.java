package org.bidib.wizard.mvc.main.view.component;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.apache.commons.lang.StringUtils;
import org.bidib.jbidibc.core.utils.NodeUtils;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.dialog.EscapeDialog;
import org.bidib.wizard.mvc.main.model.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.forms.builder.ButtonBarBuilder;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.FormLayout;

public abstract class SaveNodeConfigurationDialog extends EscapeDialog {
    private static final Logger LOGGER = LoggerFactory.getLogger(SaveNodeConfigurationDialog.class);

    private static final long serialVersionUID = 1L;

    public static final String SAVE_MACROS = "saveMacros";

    public static final String SAVE_ACCESSORIES = "saveAccessories";

    public static final String SAVE_BACKLIGHTPORTS = "saveBacklightPorts";

    public static final String SAVE_LIGHTPORTS = "saveLightPorts";

    public static final String SAVE_SWITCHPORTS = "saveSwitchPorts";

    public static final String SAVE_SERVOPORTS = "saveServoPorts";

    public static final String SAVE_FEATURES = "saveFeatures";

    public static final String SAVE_CVS = "saveConfigurationVariables";

    public static final String SAVE_ERRORS = "saveErrors";

    public static final String FEATURES_RESTORED = "featuresRestored";

    public static final String CV_VALUES_RESTORED = "cvValuesRestored";

    public static final String MACRO_AND_ACCESSORIES_RESTORED = "macroAndAccessoriesRestored";

    public static final String DEFAULT_IMPORT_CV_ONLY = "defaultImportCVOnly";

    private JCheckBox checkSaveMacros;

    private JCheckBox checkSaveAccessories;

    private JCheckBox checkSaveLightportConfiguration;

    private JCheckBox checkSaveBacklightportConfiguration;

    private JCheckBox checkSaveSwitchportConfiguration;

    private JCheckBox checkSaveServoportConfiguration;

    private JCheckBox checkSaveFeatures;

    private JCheckBox checkSaveConfigurationVariables;

    public SaveNodeConfigurationDialog(Frame frame, String title, String description, final Node node,
        final Map<String, Object> importParams, boolean modal) {
        super(frame, (StringUtils.isNotBlank(title) ? title : Resources.getString(SaveNodeConfigurationDialog.class,
            "title")), modal);

        DefaultFormBuilder formBuilder = new DefaultFormBuilder(new FormLayout("pref", "")).border(Borders.DIALOG);

        // prepare a panel with checkboxes for loading macro content before export
        JLabel descriptionLabel =
            new JLabel((StringUtils.isNotBlank(description) ? "<html>" + description + "</html>" : "<html>"
                + Resources.getString(SaveNodeConfigurationDialog.class, "description"))
                + "</html>");
        descriptionLabel.setBorder(new EmptyBorder(5, 5, 10, 5));
        formBuilder.append(descriptionLabel);

        boolean defaultImportCVOnly = false;
        Boolean paramDefaultImportCVOnly = (Boolean) importParams.get(DEFAULT_IMPORT_CV_ONLY);
        if (paramDefaultImportCVOnly != null) {
            defaultImportCVOnly = paramDefaultImportCVOnly.booleanValue();
        }

        if (NodeUtils.hasAccessoryFunctions(node.getUniqueId())) {
            checkSaveMacros =
                new JCheckBox(Resources.getString(SaveNodeConfigurationDialog.class, "checkSaveMacros"), true);
            formBuilder.append(checkSaveMacros);
            checkSaveAccessories =
                new JCheckBox(Resources.getString(SaveNodeConfigurationDialog.class, "checkSaveAccessories"), true);
            formBuilder.append(checkSaveAccessories);

            if (!isParamEnabled(importParams, MACRO_AND_ACCESSORIES_RESTORED)) {
                // uncheck and disable
                checkSaveMacros.setSelected(false);
                checkSaveMacros.setEnabled(false);

                checkSaveAccessories.setSelected(false);
                checkSaveAccessories.setEnabled(false);
            }
        }

        if (NodeUtils.hasSwitchFunctions(node.getUniqueId())) {
            checkSaveLightportConfiguration =
                new JCheckBox(
                    Resources.getString(SaveNodeConfigurationDialog.class, "checkSaveLightportConfiguration"), true);
            formBuilder.append(checkSaveLightportConfiguration);
            checkSaveBacklightportConfiguration =
                new JCheckBox(Resources.getString(SaveNodeConfigurationDialog.class,
                    "checkSaveBacklightportConfiguration"), true);
            formBuilder.append(checkSaveBacklightportConfiguration);
            checkSaveServoportConfiguration =
                new JCheckBox(
                    Resources.getString(SaveNodeConfigurationDialog.class, "checkSaveServoportConfiguration"), true);
            formBuilder.append(checkSaveServoportConfiguration);
            checkSaveSwitchportConfiguration =
                new JCheckBox(
                    Resources.getString(SaveNodeConfigurationDialog.class, "checkSaveSwitchportConfiguration"), true);
            formBuilder.append(checkSaveSwitchportConfiguration);

            if (defaultImportCVOnly) {
                // uncheck and disable
                checkSaveLightportConfiguration.setSelected(false);
                checkSaveLightportConfiguration.setEnabled(false);

                checkSaveBacklightportConfiguration.setSelected(false);
                checkSaveBacklightportConfiguration.setEnabled(false);

                checkSaveServoportConfiguration.setSelected(false);
                checkSaveServoportConfiguration.setEnabled(false);

                checkSaveSwitchportConfiguration.setSelected(false);
                checkSaveSwitchportConfiguration.setEnabled(false);
            }
        }

        checkSaveFeatures =
            new JCheckBox(Resources.getString(SaveNodeConfigurationDialog.class, "checkSaveFeatures"), true);
        formBuilder.append(checkSaveFeatures);

        if (!isParamEnabled(importParams, FEATURES_RESTORED)) {
            checkSaveFeatures.setSelected(false);
        }
        checkSaveConfigurationVariables =
            new JCheckBox(Resources.getString(SaveNodeConfigurationDialog.class, "checkSaveConfigurationVariables"),
                true);
        formBuilder.append(checkSaveConfigurationVariables);

        if (!isParamEnabled(importParams, CV_VALUES_RESTORED)) {
            checkSaveConfigurationVariables.setSelected(false);
        }

        JLabel transferDescriptionLabel =
            new JLabel((StringUtils.isNotBlank(description) ? "<html>" + description + "</html>" : "<html>"
                + Resources.getString(SaveNodeConfigurationDialog.class, "transfer-description"))
                + "</html>");
        transferDescriptionLabel.setBorder(new EmptyBorder(10, 5, 10, 5));
        formBuilder.append(transferDescriptionLabel);

        JButton yesButton = new JButton(Resources.getString(SaveNodeConfigurationDialog.class, "ok"));
        yesButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                fireContinue(node);
            }
        });

        JButton noButton = new JButton(Resources.getString(SaveNodeConfigurationDialog.class, "cancel"));
        noButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                fireCancel(node);
            }
        });
        JPanel buttonBar = new ButtonBarBuilder().addButton(yesButton, noButton).build();
        JPanel southPanel = new JPanel(new BorderLayout());
        southPanel.add(buttonBar, BorderLayout.EAST);
        formBuilder.append(southPanel);

        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());
        contentPane.add(formBuilder.build(), BorderLayout.CENTER);
    }

    public void showDialog() {

        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private boolean isParamEnabled(final Map<String, Object> params, String key) {

        if (params != null && params.containsKey(key)) {
            try {
                Boolean value = (Boolean) params.get(key);
                return value.booleanValue();
            }
            catch (Exception ex) {
                LOGGER.warn("Get value for key failed: {}", key, ex);
            }
        }

        return false;
    }

    public void setSaveFeaturesEnabled(boolean enabled) {
        checkSaveFeatures.setEnabled(enabled);
        if (!enabled) {
            checkSaveFeatures.setSelected(false);
        }
    }

    public void setSaveCVsEnabled(boolean enabled) {
        checkSaveConfigurationVariables.setEnabled(enabled);
        if (!enabled) {
            checkSaveConfigurationVariables.setSelected(false);
        }
    }

    protected Boolean isSaveMacros() {
        if (checkSaveMacros != null) {
            return new Boolean(checkSaveMacros.isSelected());
        }
        return Boolean.FALSE;
    }

    protected Boolean isSaveAccessories() {
        if (checkSaveAccessories != null) {
            return new Boolean(checkSaveAccessories.isSelected());
        }
        return Boolean.FALSE;
    }

    protected Boolean isSaveBacklightPorts() {
        if (checkSaveBacklightportConfiguration != null) {
            return new Boolean(checkSaveBacklightportConfiguration.isSelected());
        }
        return Boolean.FALSE;
    }

    protected Boolean isSaveLightPorts() {
        if (checkSaveLightportConfiguration != null) {
            return new Boolean(checkSaveLightportConfiguration.isSelected());
        }
        return Boolean.FALSE;
    }

    protected Boolean isSaveServoPorts() {
        if (checkSaveServoportConfiguration != null) {
            return new Boolean(checkSaveServoportConfiguration.isSelected());
        }
        return Boolean.FALSE;
    }

    protected Boolean isSaveSwitchPorts() {
        if (checkSaveSwitchportConfiguration != null) {
            return new Boolean(checkSaveSwitchportConfiguration.isSelected());
        }
        return Boolean.FALSE;
    }

    protected Boolean isSaveFeatures() {
        if (checkSaveFeatures != null) {
            return new Boolean(checkSaveFeatures.isSelected());
        }
        return Boolean.FALSE;
    }

    protected Boolean isSaveConfigurationVariables() {
        if (checkSaveConfigurationVariables != null) {
            return new Boolean(checkSaveConfigurationVariables.isSelected());
        }
        return Boolean.FALSE;
    }

    protected abstract void fireContinue(final Node node);

    protected abstract void fireCancel(final Node node);
}
