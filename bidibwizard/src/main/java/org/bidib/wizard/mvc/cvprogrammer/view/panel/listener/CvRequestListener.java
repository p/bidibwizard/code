package org.bidib.wizard.mvc.cvprogrammer.view.panel.listener;

public interface CvRequestListener {
    void send(int cvValue);
}
