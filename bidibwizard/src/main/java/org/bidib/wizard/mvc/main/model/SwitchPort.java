package org.bidib.wizard.mvc.main.model;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.util.LinkedHashMap;
import java.util.Map;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.enumeration.IoBehaviourSwitchEnum;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.LoadTypeEnum;
import org.bidib.jbidibc.core.enumeration.SwitchPortEnum;
import org.bidib.jbidibc.core.port.BytePortConfigValue;
import org.bidib.jbidibc.core.port.PortConfigValue;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.wizard.comm.SwitchPortStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SwitchPort extends Port<SwitchPortStatus> implements ConfigurablePort<SwitchPortStatus>, TicksAware {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(SwitchPort.class);

    static {
        try {
            // Q: why is this needed? A: export of beans with XMLDecoder
            PropertyDescriptor[] descriptor = Introspector.getBeanInfo(SwitchPort.class).getPropertyDescriptors();

            for (int i = 0; i < descriptor.length; i++) {
                PropertyDescriptor propertyDescriptor = descriptor[i];
                if (propertyDescriptor.getName().equals("value")) {
                    propertyDescriptor.setValue("transient", Boolean.TRUE);
                }
                else if (propertyDescriptor.getName().equals("portConfig")) {
                    propertyDescriptor.setValue("transient", Boolean.TRUE);
                }
                else if (propertyDescriptor.getName().equals("portConfigX")) {
                    propertyDescriptor.setValue("transient", Boolean.TRUE);
                }
                else if (propertyDescriptor.getName().equals("hasSwitchPortConfig")) {
                    propertyDescriptor.setValue("transient", Boolean.TRUE);
                }
            }
        }
        catch (IntrospectionException e) {
            throw new RuntimeException(e);
        }
    }

    public static final SwitchPort NONE = new SwitchPort.Builder(-1).setLabel("<none>").build();

    private IoBehaviourSwitchEnum outputBehaviour = IoBehaviourSwitchEnum.UNKNOWN;

    private LoadTypeEnum loadType = LoadTypeEnum.UNKNOWN;

    private int switchOffTime;

    private transient boolean hasSwitchPortConfig;

    public SwitchPort() {
        super(null);
        setStatus(SwitchPortStatus.ON);
    }

    public SwitchPort(GenericPort genericPort) {
        super(genericPort);
    }

    private SwitchPort(Builder builder) {
        super(null);
        setId(builder.id);
        setLabel(builder.label);
    }

    @Override
    protected LcOutputType getPortType() {
        return LcOutputType.SWITCHPORT;
    }

    @Override
    protected SwitchPortStatus internalGetStatus() {
        return SwitchPortStatus.valueOf(SwitchPortEnum.valueOf(genericPort.getPortStatus()));
    }

    /**
     * @return the outputBehaviour
     */
    public IoBehaviourSwitchEnum getOutputBehaviour() {
        if (genericPort != null) {
            if (isEnabled()) {
                Number outputBehaviour = genericPort.getPortConfigValue(BidibLibrary.BIDIB_PCFG_SWITCH_CTRL);
                if (outputBehaviour != null) {
                    try {
                        this.outputBehaviour = IoBehaviourSwitchEnum.valueOf(outputBehaviour.byteValue());
                    }
                    catch (IllegalArgumentException ex) {
                        LOGGER.warn("Get outputBehaviour failed on port: {}, outputBehaviour: {}",
                            genericPort.getPortNumber(), outputBehaviour);
                        this.outputBehaviour = IoBehaviourSwitchEnum.UNKNOWN;
                    }
                }
                else {
                    LOGGER.warn("No value received from generic port for BIDIB_PCFG_IO_CTRL!");
                    this.outputBehaviour = IoBehaviourSwitchEnum.UNKNOWN;
                }
            }
            else {
                LOGGER.debug("The current port is not enabled, return UNKNOWN.");
                this.outputBehaviour = IoBehaviourSwitchEnum.UNKNOWN;
            }
        }
        return outputBehaviour;
    }

    /**
     * @param outputBehaviour
     *            the outputBehaviour to set
     */
    public void setOutputBehaviour(IoBehaviourSwitchEnum outputBehaviour) {
        LOGGER.info("Set outputBehaviour: {}", outputBehaviour);
        this.outputBehaviour = outputBehaviour;
        if (genericPort != null) {
            genericPort.setPortConfigValue(BidibLibrary.BIDIB_PCFG_SWITCH_CTRL,
                new BytePortConfigValue(outputBehaviour.getType()));
        }
    }

    public void setLoadType(LoadTypeEnum loadType) {
        LOGGER.info("Set load type: {}", loadType);
        this.loadType = loadType;
        if (genericPort != null) {
            genericPort.setPortConfigValue(BidibLibrary.BIDIB_PCFG_LOAD_TYPE,
                new BytePortConfigValue(loadType.getType()));
        }
    }

    public LoadTypeEnum getLoadType() {
        if (genericPort != null) {
            if (isEnabled()) {
                Number loadType = genericPort.getPortConfigValue(BidibLibrary.BIDIB_PCFG_LOAD_TYPE);
                if (loadType != null) {
                    try {
                        this.loadType = LoadTypeEnum.valueOf(loadType.byteValue());
                    }
                    catch (IllegalArgumentException ex) {
                        LOGGER.warn("Get loadType failed on port: {}, loadType: {}", genericPort.getPortNumber(),
                            loadType);
                        this.loadType = LoadTypeEnum.UNKNOWN;
                    }
                }
                else {
                    LOGGER.warn("No value received from generic port for BIDIB_PCFG_LOAD_TYPE!");
                    this.loadType = LoadTypeEnum.UNKNOWN;
                }
            }
            else {
                LOGGER.debug("The current port is not enabled, return UNKNOWN.");
                this.loadType = LoadTypeEnum.UNKNOWN;
            }
        }
        return loadType;
    }

    @Override
    public int getTicks() {
        return getSwitchOffTime();
    }

    /**
     * @return the switchOffTime
     */
    public int getSwitchOffTime() {
        if (genericPort != null) {
            Number switchOffTime = genericPort.getPortConfigValue(BidibLibrary.BIDIB_PCFG_TICKS);
            if (switchOffTime != null) {
                this.switchOffTime = ByteUtils.getInt(switchOffTime.byteValue());
            }
            else {
                LOGGER.warn("No value received from generic for BIDIB_PCFG_TICKS!");
            }
        }
        return switchOffTime;
    }

    /**
     * @param switchOffTime
     *            the switchOffTime to set
     */
    public void setSwitchOffTime(int switchOffTime) {
        LOGGER.info("Set switchOff time: {}", switchOffTime);
        this.switchOffTime = switchOffTime;
        if (genericPort != null) {
            genericPort.setPortConfigValue(BidibLibrary.BIDIB_PCFG_TICKS,
                new BytePortConfigValue(ByteUtils.getLowByte(switchOffTime)));
        }
    }

    @Override
    public byte[] getPortConfig() {
        return new byte[] { outputBehaviour.getType(), ByteUtils.getLowByte(switchOffTime), 0, 0 };
    }

    public void setPortConfig(byte[] portConfig) {
        LOGGER.info("Set the switch port parameters: {}", ByteUtils.bytesToHex(portConfig));

        setPortConfigEnabled(true);

        try {
            setOutputBehaviour(IoBehaviourSwitchEnum.valueOf(portConfig[0]));
            getKnownPortConfigKeys().add(Byte.valueOf(BidibLibrary.BIDIB_PCFG_SWITCH_CTRL));

        }
        catch (Exception ex) {
            LOGGER.warn("Set the IoBehaviour failed.", ex);
            // TODO show an error on this port
            setOutputBehaviour(IoBehaviourSwitchEnum.UNKNOWN);
        }
        setSwitchOffTime(ByteUtils.getInt(portConfig[1]));
        getKnownPortConfigKeys().add(Byte.valueOf(BidibLibrary.BIDIB_PCFG_TICKS));
    }

    @Override
    public void setPortConfigX(Map<Byte, PortConfigValue<?>> portConfig) {

        LOGGER.info("Set the port config for the switch port.");

        Number ioBehaviour = getPortConfigValue(BidibLibrary.BIDIB_PCFG_SWITCH_CTRL, portConfig);
        if (ioBehaviour != null) {
            setOutputBehaviour(IoBehaviourSwitchEnum.valueOf(ioBehaviour.byteValue()));
            setHasSwitchPortConfig(true);
        }
        else {
            LOGGER.warn("No value received for BIDIB_PCFG_IO_CTRL!");
        }

        Number switchOffTime = getPortConfigValue(BidibLibrary.BIDIB_PCFG_TICKS, portConfig);
        if (switchOffTime != null) {
            setSwitchOffTime(ByteUtils.getInt(switchOffTime.byteValue()));
            setHasSwitchPortConfig(true);
        }
        else {
            LOGGER.warn("No value received for BIDIB_PCFG_TICKS!");
        }

        Number loadType = getPortConfigValue(BidibLibrary.BIDIB_PCFG_LOAD_TYPE, portConfig);
        if (loadType != null) {
            setLoadType(LoadTypeEnum.valueOf(loadType.byteValue()));
            setHasSwitchPortConfig(true);
        }
        else {
            LOGGER.warn("No value received for BIDIB_PCFG_LOAD_TYPE!");
        }

        // call the super class
        super.setPortConfigX(portConfig);
    }

    @Override
    public Map<Byte, PortConfigValue<?>> getPortConfigX() {

        if (genericPort != null) {
            return genericPort.getPortConfig();
        }

        Map<Byte, PortConfigValue<?>> portConfigX = new LinkedHashMap<Byte, PortConfigValue<?>>();

        // add the ioBehaviour
        if (isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_SWITCH_CTRL)) {
            portConfigX.put(BidibLibrary.BIDIB_PCFG_SWITCH_CTRL,
                new BytePortConfigValue(ByteUtils.getLowByte(outputBehaviour.getType())));
        }
        // add the ticks
        if (isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_TICKS)) {
            portConfigX.put(BidibLibrary.BIDIB_PCFG_TICKS,
                new BytePortConfigValue(ByteUtils.getLowByte(switchOffTime)));
        }
        // add the load type
        if (isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_LOAD_TYPE)) {
            portConfigX.put(BidibLibrary.BIDIB_PCFG_LOAD_TYPE,
                new BytePortConfigValue(ByteUtils.getLowByte(loadType.getType())));
        }
        return portConfigX;
    }

    /**
     * @return the port has switch port config available
     */
    public boolean isHasSwitchPortConfig() {
        if (genericPort != null) {
            // return genericPort.isHasPortConfig();
            hasSwitchPortConfig =
                genericPort.isHasPortConfig(BidibLibrary.BIDIB_PCFG_SWITCH_CTRL)
                    || genericPort.isHasPortConfig(BidibLibrary.BIDIB_PCFG_TICKS)
                    || genericPort.isHasPortConfig(BidibLibrary.BIDIB_PCFG_LOAD_TYPE);
            LOGGER.info("The generic port has switch port config: {}", hasSwitchPortConfig);
        }
        return hasSwitchPortConfig;
    }

    /**
     * @param hasSwitchPortConfig
     *            the switch port config available flag
     */
    public void setHasSwitchPortConfig(boolean hasSwitchPortConfig) {
        LOGGER.info("The port has switch port config available: {}, port: {}", hasSwitchPortConfig, this);
        this.hasSwitchPortConfig = hasSwitchPortConfig;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof SwitchPort) {
            return ((SwitchPort) obj).getId() == getId();
        }
        return false;
    }

    @Override
    public int hashCode() {
        return getId();
    }

    public static class Builder {
        private final int id;

        private String label;

        public Builder(int id) {
            this.id = id;
        }

        public Builder setLabel(String label) {
            this.label = label;
            return this;
        }

        public SwitchPort build() {
            return new SwitchPort(this);
        }
    }
}
