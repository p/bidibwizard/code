package org.bidib.wizard.mvc.script.controller.exception;

public class VelocityExceptionThrower {

    public void throwUserDefined(String message) {
        throw new UserDefinedVelocityException(message);
    }

}
