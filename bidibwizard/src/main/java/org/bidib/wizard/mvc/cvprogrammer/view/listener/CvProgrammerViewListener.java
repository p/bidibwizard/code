package org.bidib.wizard.mvc.cvprogrammer.view.listener;

import org.bidib.jbidibc.core.enumeration.PomAddressing;
import org.bidib.jbidibc.core.enumeration.PomDecoder;
import org.bidib.jbidibc.core.enumeration.PomOperation;

public interface CvProgrammerViewListener {
    void close();

    void sendRequest(
        PomDecoder action, int address, PomOperation operation, PomAddressing addressing, int cvNumber, int cvValue);
}
