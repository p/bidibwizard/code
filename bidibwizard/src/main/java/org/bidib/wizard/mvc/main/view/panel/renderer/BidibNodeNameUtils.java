package org.bidib.wizard.mvc.main.view.panel.renderer;

import org.apache.commons.lang.StringUtils;
import org.bidib.jbidibc.core.StringData;
import org.bidib.jbidibc.core.enumeration.SysErrorEnum;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.main.model.LabelAware;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.view.component.LabelList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BidibNodeNameUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(BidibNodeNameUtils.class);

    public static final class NodeLabelData {
        private String nodeLabel;

        private String nodeToolTipText;

        public NodeLabelData() {
        }

        /**
         * @return the nodeLabel
         */
        public String getNodeLabel() {
            return nodeLabel;
        }

        /**
         * @param nodeLabel
         *            the nodeLabel to set
         */
        public void setNodeLabel(String nodeLabel) {
            this.nodeLabel = nodeLabel;
        }

        /**
         * @return the nodeToolTipText
         */
        public String getNodeToolTipText() {
            return nodeToolTipText;
        }

        /**
         * @param nodeToolTipText
         *            the nodeToolTipText to set
         */
        public void setNodeToolTipText(String nodeToolTipText) {
            this.nodeToolTipText = nodeToolTipText;
        }
    }

    public static NodeLabelData prepareLabel(
        final Node node, boolean alwaysShowProductNameInTree, boolean prettyFormatted) {
        final NodeLabelData labelData = new NodeLabelData();

        if (node != null) {
            if (node.getNode().hasStoredStrings()) {
                // the node has stored strings, prepare the label with the stored strings
                String userString = node.getNode().getStoredString(StringData.INDEX_USERNAME);
                if (StringUtils.isNotBlank(userString)) {
                    // user string is available
                    boolean addClosingHtmlTag = false;
                    StringBuilder labelText = new StringBuilder();
                    String label = node.toString();
                    if (label.startsWith(userString)) {
                        String part = label.substring(userString.length());
                        if (StringUtils.isNotBlank(part)) {
                            if (prettyFormatted) {
                                labelText
                                    .append("<html>").append(userString).append("<font color=\"blue\">").append(part)
                                    .append("</font>");
                                addClosingHtmlTag = true;
                            }
                            else {
                                labelText.append(label);
                            }
                        }
                        else {
                            labelText.append(label);
                        }
                    }
                    else {
                        labelText.append(userString);
                    }

                    String productString = node.getNode().getStoredString(StringData.INDEX_PRODUCTNAME);
                    if (StringUtils.isNotBlank(productString)) {
                        labelData.setNodeToolTipText(productString);

                        if (alwaysShowProductNameInTree) {
                            labelText.append(" (").append(productString).append(")");
                        }
                    }
                    else {
                        labelData.setNodeToolTipText(getDefaultTooltip(node));
                    }
                    if (addClosingHtmlTag) {
                        labelText.append("</html>");
                    }
                    labelData.setNodeLabel(labelText.toString());
                }
                else {
                    // no user string available
                    if (prettyFormatted) {
                        StringBuilder sb = new StringBuilder("<html><font color=\"blue\">");
                        sb.append(node.toString()).append("</font>");

                        String productString = node.getNode().getStoredString(StringData.INDEX_PRODUCTNAME);
                        if (StringUtils.isNotBlank(productString)) {
                            sb.append("<font color=\"black\">");
                            sb.append(" (");
                            sb.append(productString);
                            sb.append(")</font>");
                        }
                        sb.append("</html>");
                        labelData.setNodeLabel(sb.toString());
                    }
                    else {
                        labelData.setNodeLabel(node.toString());
                    }
                    labelData.setNodeToolTipText(getDefaultTooltip(node));
                }
            }
            else {
                // node has no stored strings
                labelData.setNodeLabel(node.toString());
                labelData.setNodeToolTipText(getDefaultTooltip(node));
            }
        }
        else {
            labelData.setNodeLabel(null);
            labelData.setNodeToolTipText(null);
        }

        return labelData;
    }

    /**
     * Get the node name.
     * 
     * @param node
     *            the node
     * @return the node name
     */
    public static String getNodeName(Node node) {
        StringBuilder labelText = new StringBuilder();

        if (node != null) {
            if (node.getNode().hasStoredStrings()) {
                // the node has stored strings, prepare the label with the stored strings
                String userString = node.getNode().getStoredString(StringData.INDEX_USERNAME);
                if (StringUtils.isNotBlank(userString)) {
                    // user string is available

                    String label = node.toString();
                    if (label.startsWith(userString)) {

                        labelText.append(label);

                    }
                    else {
                        labelText.append(userString);
                    }
                }
                else {
                    // no user string available
                    labelText.append(node.toString());
                }
            }
            else {
                // node has no stored strings
                labelText.append(node.toString());
            }
        }

        return labelText.toString();
    }

    private static String getDefaultTooltip(Node node) {
        StringBuilder tooltipText = new StringBuilder();
        if (node.getErrorState() != null && !SysErrorEnum.BIDIB_ERR_NONE.equals(node.getErrorState())) {
            tooltipText.append(Resources.getString(BidibNodeNameUtils.class, "node-error-reported",
                new Object[] { Resources.getString(SysErrorEnum.class, node.getErrorState().name(),
                    new Object[] { getErrorReason(node) }) }));
        }
        else if (node instanceof LabelAware && StringUtils.isNotBlank(((LabelAware) node).getLabel())) {
            tooltipText.append(((LabelAware) node).getLabel());
        }
        else if (node.isNodeHasError(true)) {
            tooltipText.append(Resources.getString(BidibNodeNameUtils.class, "node-error-reported",
                new Object[] { getNodeErrorReason(node) }));
        }
        else {
            // no label assigned, prepare tooltip
            tooltipText.append(Resources.getString(LabelList.class, "chooseLabel"));
        }

        return tooltipText.toString();
    }

    private static String getErrorReason(Node node) {
        String reason = "";
        try {
            reason =
                (SysErrorEnum.BIDIB_ERR_TXT.equals(node.getErrorState()) ? new String(node.getReasonData(), "UTF-8") : ByteUtils
                    .toString(node.getReasonData()));
        }
        catch (Exception ex) {
            LOGGER.warn("Convert reason data to reason failed.", ex);
        }
        return reason;
    }

    private static String getNodeErrorReason(Node node) {
        String reason = "";
        try {
            if (node.getReasonData() != null) {
                reason = new String(node.getReasonData(), "UTF-8");
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Convert reason data to reason failed.", ex);
        }
        return reason;
    }
}
