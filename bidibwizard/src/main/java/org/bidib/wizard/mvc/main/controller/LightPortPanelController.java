package org.bidib.wizard.mvc.main.controller;

import java.awt.Frame;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.JOptionPane;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.lang3.time.StopWatch;
import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.PortConfigKeys;
import org.bidib.jbidibc.core.exception.InvalidConfigurationException;
import org.bidib.jbidibc.core.node.ConfigurationVariable;
import org.bidib.jbidibc.core.port.BytePortConfigValue;
import org.bidib.jbidibc.core.port.Int16PortConfigValue;
import org.bidib.jbidibc.core.port.PortConfigValue;
import org.bidib.jbidibc.core.port.RgbPortConfigValue;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.core.utils.ProductUtils;
import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.comm.Communication;
import org.bidib.wizard.comm.CommunicationFactory;
import org.bidib.wizard.comm.LightPortStatus;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.labels.LabelFactory;
import org.bidib.wizard.labels.Labels;
import org.bidib.wizard.labels.LightPortLabelFactory;
import org.bidib.wizard.labels.PortLabelUtils;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.bidib.wizard.mvc.common.view.cvdefinition.CvDefinitionTreeTableModel;
import org.bidib.wizard.mvc.common.view.cvdefinition.CvValueUtils;
import org.bidib.wizard.mvc.main.controller.wrapper.NodePortWrapper;
import org.bidib.wizard.mvc.main.model.LightPort;
import org.bidib.wizard.mvc.main.model.LightPortTableModel;
import org.bidib.wizard.mvc.main.model.Macro;
import org.bidib.wizard.mvc.main.model.MacroSaveState;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.model.Port;
import org.bidib.wizard.mvc.main.model.listener.CvDefinitionListener;
import org.bidib.wizard.mvc.main.model.listener.CvDefinitionRequestListener;
import org.bidib.wizard.mvc.main.model.listener.DefaultNodeListListener;
import org.bidib.wizard.mvc.main.model.listener.LightPortListener;
import org.bidib.wizard.mvc.main.view.component.BusyFrame;
import org.bidib.wizard.mvc.main.view.component.InsertPortsConfirmDialog;
import org.bidib.wizard.mvc.main.view.cvdef.CvNode;
import org.bidib.wizard.mvc.main.view.cvdef.CvNodeUtils;
import org.bidib.wizard.mvc.main.view.cvdef.LongCvNode;
import org.bidib.wizard.mvc.main.view.panel.LightPortListPanel;
import org.bidib.wizard.mvc.main.view.statusbar.StatusBar;
import org.bidib.wizard.mvc.script.view.ScriptParser;
import org.bidib.wizard.script.ScriptCommand;
import org.bidib.wizard.script.engine.ScriptEngine;
import org.bidib.wizard.script.switching.LightPortCommand;
import org.bidib.wizard.script.switching.WaitCommand;
import org.bidib.wizard.utils.PortListUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jidesoft.grid.DefaultExpandableRow;

public class LightPortPanelController implements CvDefinitionListener, PortScripting {

    private static final Logger LOGGER = LoggerFactory.getLogger(LightPortPanelController.class);

    // NeoControl
    public static final String KEYWORD_CHANNEL_B_START_NUMBER = "channel_B_start_number";

    // NeoEWS
    public static final String KEYWORD_CHANNEL_A_LAST_ELEMENT = "channel_A_last_element";

    private final MainModel mainModel;

    private LightPortListPanel lightPortListPanel;

    private final Map<Node, NodePortWrapper> testToggleRegistry = new LinkedHashMap<>();

    private Map<String, CvNode> mapKeywordToNode;

    private List<CvDefinitionRequestListener> cvDefinitionRequestListeners = new LinkedList<>();

    private List<ConfigurationVariable> requiredConfigVariables = new LinkedList<>();

    private Integer channelBStartNumber;

    public LightPortPanelController(final MainModel mainModel) {
        this.mainModel = mainModel;
    }

    public LightPortListPanel createPanel() {
        final Labels lightPortLabels =
            DefaultApplicationContext.getInstance().get(DefaultApplicationContext.KEY_LIGHTPORT_LABELS, Labels.class);

        // add the cv definition listener
        mainModel.addCvDefinitionListener(this);

        final LightPortListPanel lightPortListPanel = new LightPortListPanel(this, mainModel);

        lightPortListPanel.addPortListener(new LightPortListener<LightPortStatus>() {
            @Override
            public void labelChanged(Port<LightPortStatus> port, String label) {
                LOGGER.info("The label is changed, port: {}, label: {}", port, label);
                port.setLabel(label);

                try {
                    LightPortLabelFactory factory = new LightPortLabelFactory();
                    long uniqueId = mainModel.getSelectedNode().getNode().getUniqueId();
                    factory.replaceLabel(lightPortLabels, uniqueId, port.getId(), label);

                    factory.saveLabels(uniqueId, lightPortLabels);
                }
                catch (InvalidConfigurationException ex) {
                    LOGGER.warn("Save light port labels failed.", ex);

                    String labelPath = ex.getReason();
                    JOptionPane.showMessageDialog(JOptionPane.getFrameForComponent(null),
                        Resources.getString(Labels.class, "labelfileerror.message", new Object[] { labelPath }),
                        Resources.getString(Labels.class, "labelfileerror.title"), JOptionPane.ERROR_MESSAGE);
                }

                lightPortListPanel.repaint();
            }

            @Override
            public void statusChanged(Port<LightPortStatus> port, LightPortStatus status) {
                LOGGER.debug("Status of light port has changed, port: {}", port);
            }

            @Override
            public void valuesChanged(LightPort port, PortConfigKeys... portConfigKeys) {

                LOGGER.info("The port value are changed for port: {}, portConfigKeys: {}", port, portConfigKeys);

                Map<Byte, PortConfigValue<?>> values = new LinkedHashMap<>();

                for (PortConfigKeys key : portConfigKeys) {
                    switch (key) {
                        case BIDIB_PCFG_DIMM_DOWN:
                            int dimMin = port.getDimMin();

                            if (port.isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_DIMM_DOWN_8_8)) {
                                values.put(BidibLibrary.BIDIB_PCFG_DIMM_DOWN_8_8,
                                    new Int16PortConfigValue(ByteUtils.getWORD(dimMin)));
                            }
                            else if (port.isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_DIMM_DOWN)) {
                                values.put(BidibLibrary.BIDIB_PCFG_DIMM_DOWN,
                                    new BytePortConfigValue(ByteUtils.getLowByte(dimMin)));
                            }
                            break;
                        case BIDIB_PCFG_DIMM_UP:
                            int dimMax = port.getDimMax();

                            if (port.isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_DIMM_UP_8_8)) {
                                values.put(BidibLibrary.BIDIB_PCFG_DIMM_UP_8_8,
                                    new Int16PortConfigValue(ByteUtils.getWORD(dimMax)));
                            }
                            else if (port.isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_DIMM_UP)) {
                                values.put(BidibLibrary.BIDIB_PCFG_DIMM_UP,
                                    new BytePortConfigValue(ByteUtils.getLowByte(dimMax)));
                            }
                            break;
                        case BIDIB_PCFG_LEVEL_PORT_ON:
                            int pwmMax = port.getPwmMax();
                            values.put(BidibLibrary.BIDIB_PCFG_LEVEL_PORT_ON,
                                new BytePortConfigValue(ByteUtils.getLowByte(pwmMax)));
                            break;
                        case BIDIB_PCFG_LEVEL_PORT_OFF:
                            int pwmMin = port.getPwmMin();
                            values.put(BidibLibrary.BIDIB_PCFG_LEVEL_PORT_OFF,
                                new BytePortConfigValue(ByteUtils.getLowByte(pwmMin)));
                            break;
                        case BIDIB_PCFG_RGB:
                            Integer rgbValue = port.getRgbValue();
                            if (port.isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_RGB) && rgbValue != null) {
                                LOGGER.info("The lightport has RGB support: {}", port);

                                values.put(BidibLibrary.BIDIB_PCFG_RGB,
                                    new RgbPortConfigValue(ByteUtils.getRGB(rgbValue)));
                            }
                            break;
                        case BIDIB_PCFG_TRANSITION_TIME:
                            Integer transitionTime = port.getTransitionTime();
                            if (port.isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_TRANSITION_TIME)
                                && transitionTime != null) {
                                LOGGER.info("The lightport has TransitionTime support: {}", port);

                                values.put(BidibLibrary.BIDIB_PCFG_TRANSITION_TIME,
                                    new Int16PortConfigValue(transitionTime));
                            }
                            break;
                        default:
                            LOGGER.warn("Unsupported port config key detected: {}", key);
                            break;
                    }
                }

                try {
                    LOGGER.info("Set the port params: {}", values);
                    // don't set the port type param to not send BIDIB_PCFG_RECONFIG
                    CommunicationFactory.getInstance().setPortParameters(mainModel.getSelectedNode().getNode(), port,
                        null, values);
                }
                catch (Exception ex) {
                    LOGGER.warn("Set the lightport parameters failed.", ex);
                    // model.getSelectedNode().setNodeHasError(true);
                    mainModel.setNodeHasError(mainModel.getSelectedNode(), true);
                }
            }

            @Override
            public void testButtonPressed(Port<LightPortStatus> port) {
                LOGGER.info("The test button was pressed for port: {}", port);

                Node node = mainModel.getSelectedNode();
                if (LightPortStatus.TEST != port.getStatus()) {
                    stopTestToggleTask(node, port);

                    CommunicationFactory.getInstance().activateLightPort(node.getNode(), port.getId(),
                        (LightPortStatus) port.getStatus());
                }
                else {
                    addTestToggleTask(node, port);
                }
            }

            @Override
            public void configChanged(Port<LightPortStatus> port) {
                LOGGER.info("The port config has changed for port: {}", port);
            }

            @Override
            public void changePortType(LcOutputType portType, LightPort port) {
                LOGGER.info("The port type will change to: {}, port: {}", portType, port);

                Map<Byte, PortConfigValue<?>> values = new LinkedHashMap<>();

                CommunicationFactory.getInstance().setPortParameters(mainModel.getSelectedNode().getNode(), port,
                    portType, values);
            }
        });

        mainModel.addNodeListListener(new DefaultNodeListListener() {
            @Override
            public void nodeWillChange() {
                LOGGER.info("The selected node will change!");
                try {
                    List<Node> nodes = new LinkedList<>();
                    for (Node node : testToggleRegistry.keySet()) {
                        nodes.add(node);
                    }
                    LOGGER.info("Found nodes to stop the test toggle task: {}", nodes);
                    for (Node node : nodes) {
                        stopTestToggleTask(node, null);
                    }
                    LOGGER.info("Stop the test toggle task passed for nodes: {}", nodes);
                }
                catch (Exception ex) {
                    LOGGER.warn("Stop test toggle tasks failed.", ex);
                }

                clearKeywordToNodeMap();
            }

            @Override
            public void nodeChanged() {
                LOGGER.info("The node has changed. Trigger load the CV values.");
                // triggerLoadCvValues();
            }
        });

        // keep the reference
        this.lightPortListPanel = lightPortListPanel;

        return lightPortListPanel;
    }

    public void addTestToggleTask(final Node node, final Port<?> port) {
        LOGGER.info("Add test toggle task for node: {}, port: {}", node, port);

        NodePortWrapper nodePortWrapper = testToggleRegistry.remove(node);
        ScriptEngine<PortScripting> scriptEngine = null;
        if (nodePortWrapper != null) {
            scriptEngine = nodePortWrapper.removePort(port);
        }

        if (scriptEngine != null) {
            LOGGER.info("Found a node scripting engine in the registry: {}", scriptEngine);
            try {
                scriptEngine.stopScript(Long.valueOf(2000));
            }
            catch (Exception ex) {
                LOGGER.warn("Stop script failed.", ex);
            }
        }

        HashMap<String, Object> context = new LinkedHashMap<>();
        context.put(ScriptParser.KEY_SELECTED_NODE, node);
        context.put(ScriptParser.KEY_MAIN_MODEL, mainModel);

        scriptEngine = new ScriptEngine<PortScripting>(this, context);

        List<ScriptCommand<PortScripting>> scriptCommands = new LinkedList<ScriptCommand<PortScripting>>();
        LightPortCommand<PortScripting> spc = new LightPortCommand<>();
        spc.parse("LIGHT " + port.getId() + " ON");
        scriptCommands.add(spc);
        WaitCommand<PortScripting> wc = new WaitCommand<>();
        wc.parse("WAIT 2000");
        scriptCommands.add(wc);
        spc = new LightPortCommand<>();
        spc.parse("LIGHT " + port.getId() + " OFF");
        scriptCommands.add(spc);
        wc = new WaitCommand<>();
        wc.parse("WAIT 2000");
        scriptCommands.add(wc);

        LOGGER.info("Prepared list of commands: {}", scriptCommands);

        scriptEngine.setScriptCommands(scriptCommands);
        // repeating
        scriptEngine.setScriptRepeating(true);

        if (nodePortWrapper == null) {
            LOGGER.info("Create new NodePortWrapper for node: {}", node);
            nodePortWrapper = new NodePortWrapper(node);
        }

        LOGGER.info("Put script engine in registry for node: {}", node);
        nodePortWrapper.addPort(port, scriptEngine);

        testToggleRegistry.put(node, nodePortWrapper);

        scriptEngine.startScript();
    }

    public void stopTestToggleTask(final Node node, final Port<?> port) {
        LOGGER.info("Stop test toggle task for node: {}, port: {}", node, port);

        NodePortWrapper nodePortWrapper = testToggleRegistry.get(node);

        if (nodePortWrapper != null) {
            Set<Port<?>> toRemove = new HashSet<>();
            if (port != null) {
                toRemove.add(port);
            }
            else {
                toRemove.addAll(nodePortWrapper.getKeySet());
            }

            for (Port<?> removePort : toRemove) {
                ScriptEngine<PortScripting> engine = nodePortWrapper.removePort(removePort);

                if (engine != null) {
                    LOGGER.info("Found a node scripting engine in the registry: {}", engine);
                    try {
                        engine.stopScript(Long.valueOf(2000));
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Stop script failed.", ex);
                    }
                }
                else {
                    LOGGER.warn("No scripting engine found for node: {}", node);
                }
            }

            if (nodePortWrapper.isEmpty()) {
                LOGGER.info("No more ports registered for node: {}", node);
                testToggleRegistry.remove(node);
            }
        }
    }

    @Override
    public void sendPortStatusAction(int port, BidibStatus portStatus) {
        // TODO Auto-generated method stub
        LOGGER.info("Switch the light port: {}, portStatus: {}", port, portStatus);
        try {
            Node node = mainModel.getSelectedNode();
            LightPortStatus lightPortStatus = (LightPortStatus) portStatus;
            CommunicationFactory.getInstance().activateLightPort(node.getNode(), port, lightPortStatus);
        }
        catch (Exception ex) {
            LOGGER.warn("Switch light port failed.", ex);
        }
    }

    @Override
    public void sendPortValueAction(int port, int portValue) {
        // TODO Auto-generated method stub

    }

    private void clearKeywordToNodeMap() {
        LOGGER.info("Clear the keywordToNode map.");
        mapKeywordToNode = null;
    }

    public void insertPorts(
        int selectedRow, LightPortTableModel lightPortTableModel, LightPortListener<LightPortStatus> portListener) {

        BusyFrame busyFrame = null;
        StopWatch sw = new StopWatch();

        // show a confirm dialog
        InsertPortsConfirmDialog insertPortsConfirmDialog =
            new InsertPortsConfirmDialog(JOptionPane.getFrameForComponent(null), true);

        int result = insertPortsConfirmDialog.getResult();

        if (result == JOptionPane.CANCEL_OPTION) {
            LOGGER.info("User canceled the insert ports operation.");
            return;
        }

        // check if all makros were loaded from the node

        final Node node = mainModel.getSelectedNode();

        if (mainModel.hasUnloadedMacros()) {
            LOGGER.info("Unloaded macros detected. Load the macros from the node.");

            final Communication communication = CommunicationFactory.getInstance();

            List<Macro> macroList = new LinkedList<Macro>();
            for (Macro macro : mainModel.getMacros()) {

                if (MacroSaveState.NOT_LOADED_FROM_NODE.equals(macro.getMacroSaveState())) {
                    Macro macroWithContent = communication.getMacroContent(node, macro);
                    LOGGER.info("Load macro content: {}", macroWithContent);

                    // reset the changed flag on the macro
                    macroWithContent.setMacroSaveState(MacroSaveState.PERMANENTLY_STORED_ON_NODE);
                    macroWithContent.setFlatPortModel(node.getNode().isPortFlatModelAvailable());

                    macroList.add(macroWithContent);
                }
                else {
                    macroList.add(macro);
                }
            }
            LOGGER.info("Set the loaded macros for the node: {}", macroList);
            mainModel.setMacros(macroList);
        }

        int portsCount = insertPortsConfirmDialog.getPortsCount();
        LOGGER.info("Insert number of ports: {}", portsCount);

        try {
            Frame frame = JOptionPane.getFrameForComponent(lightPortListPanel);
            if (frame instanceof BusyFrame) {
                busyFrame = (BusyFrame) frame;
                busyFrame.setBusy(true);
            }

            sw.start();

            final Labels lightPortLabels =
                DefaultApplicationContext.getInstance().get(DefaultApplicationContext.KEY_LIGHTPORT_LABELS,
                    Labels.class);

            List<LightPort> allLightPorts = mainModel.getLightPorts();

            LightPort firstPort =
                (LightPort) lightPortTableModel.getValueAt(selectedRow, LightPortTableModel.COLUMN_PORT_INSTANCE);
            LOGGER.info("Found lightPort to start insert the new ports: {}, port number: {}", firstPort,
                firstPort.getId());

            int startPortRangeValue = firstPort.getId() + portsCount - 1;
            Integer portRangeUpperBound = null;
            if (channelBStartNumber != null) {

                portRangeUpperBound = testValidRange(firstPort, portsCount, channelBStartNumber);

                LOGGER.info("Validated range, portRangeUpperBound: {}", portRangeUpperBound);
            }

            Set<LightPort> ports = findAllPortsInRange(allLightPorts, startPortRangeValue, portRangeUpperBound);

            for (LightPort currentPort : ports) {

                LOGGER.info("Change the label of the port: {}, id: {}", currentPort, currentPort.getId());
                // and move the port label and the port config

                int portNum = currentPort.getId();

                // get the predecessor port
                Port<?> predecessorPort = PortListUtils.findPortByPortNumber(allLightPorts, portNum - portsCount);

                if (predecessorPort != null) {
                    LOGGER.info("Current predecessorPort: {}, port number: {}", predecessorPort,
                        predecessorPort.getId());

                    // move the label
                    String label = predecessorPort.getLabel();
                    currentPort.setLabel(label);
                    predecessorPort.setLabel(null);

                    PortLabelUtils.replaceLabel(lightPortLabels, node.getUniqueId(), currentPort.getId(),
                        currentPort.getLabel(), LightPortLabelFactory.DEFAULT_LABELTYPE);

                    // move the port config
                    Map<Byte, PortConfigValue<?>> configX = predecessorPort.getPortConfigX();
                    currentPort.setPortConfigX(configX);

                    // initiate transfer to node
                    List<PortConfigKeys> portConfigKeys = new ArrayList<>();
                    for (byte key : currentPort.getKnownPortConfigKeys()) {
                        portConfigKeys.add(PortConfigKeys.valueOf(key));
                    }
                    portListener.valuesChanged(currentPort, portConfigKeys.toArray(new PortConfigKeys[0]));
                }
                else {
                    LOGGER.info("No predecessorPort available with port number: {}", portNum - portsCount);
                }
            }

            // replace the labels of the inserted ports
            for (int portNum = 0; portNum < portsCount; portNum++) {
                Port<?> replacedPort = PortListUtils.findPortByPortNumber(allLightPorts, firstPort.getId() + portNum);
                if (replacedPort != null) {
                    LOGGER.info("Remove label of port: {}, port number: {}", replacedPort, replacedPort.getId());
                    replacedPort.setLabel(null);

                    PortLabelUtils.replaceLabel(lightPortLabels, node.getUniqueId(), replacedPort.getId(),
                        replacedPort.getLabel(), LightPortLabelFactory.DEFAULT_LABELTYPE);

                    // TODO do we have to initialize the values of the inserted node?
                }
            }

            MacroListController macroListController =
                DefaultApplicationContext.getInstance().get(DefaultApplicationContext.KEY_MACROLIST_CONTROLLER,
                    MacroListController.class);

            LabelFactory labelFactory = new LightPortLabelFactory();
            PortLabelUtils.saveLabels(labelFactory, lightPortLabels, node);

            // move the ports in all macros and store the changed macro on the node
            macroListController.movePortsInAllMacros(mainModel, node, firstPort, allLightPorts, portsCount);

            lightPortTableModel.fireTableDataChanged();
        }
        finally {
            if (busyFrame != null) {
                LOGGER.info("Unset busy frame.");
                busyFrame.setBusy(false);
            }
            sw.stop();
        }

        LOGGER.info("Load CV values has finished! Total loading duration: {}", sw);
        StatusBar statusBar =
            DefaultApplicationContext.getInstance().get(DefaultApplicationContext.KEY_STATUS_BAR, StatusBar.class);
        statusBar.setStatus(
            String.format(Resources.getString(LightPortPanelController.class, "insert-ports-finished"), portsCount, sw),
            StatusBar.DISPLAY_NORMAL);

    }

    private void readCurrentCvNodeValuesFromCVDefinition(final Node node) {
        LOGGER.info("Read the values from CV for node: {}", node);

        if (node == null
            || (!ProductUtils.isNeoControl(node.getUniqueId()) && !ProductUtils.isNeoEWS(node.getUniqueId()))) {
            LOGGER.warn("No node available or not a NeoControl: {}", node);

            if (mapKeywordToNode != null) {
                mapKeywordToNode.clear();
            }
            return;
        }

        LOGGER.info("Get the cvDefinitionTreeTableModel from the node: {}", node);
        CvDefinitionTreeTableModel cvDefinitionTreeTableModel = node.getCvDefinitionTreeTableModel();
        if (cvDefinitionTreeTableModel != null) {
            // search the keywords
            mapKeywordToNode = new LinkedHashMap<>();

            DefaultExpandableRow rootNode = (DefaultExpandableRow) cvDefinitionTreeTableModel.getRoot();
            if (rootNode != null) {
                CvNodeUtils.harvestKeywordNodes(rootNode, mapKeywordToNode);
            }

            LOGGER.info("Found keywords in nodes: {}", mapKeywordToNode.keySet());
        }

        // read the values from the node
        if (MapUtils.isNotEmpty(mapKeywordToNode)) {
            List<ConfigurationVariable> configVariables = new LinkedList<>();
            for (CvNode cvNode : mapKeywordToNode.values()) {
                // LOGGER.info("Process cvNode: {}", cvNode);

                prepareConfigVariables(cvNode, configVariables, node.getCvNumberToJideNodeMap());
            }

            // keep the list of config variables
            this.requiredConfigVariables.clear();
            if (CollectionUtils.isNotEmpty(configVariables)) {
                this.requiredConfigVariables.addAll(configVariables);
            }

            // // add the current position with the special CV
            // prepareGetCurrentPosition(configVariables);

            fireLoadConfigVariables(configVariables);
        }
        else {
            LOGGER.warn("No values available in mapKeywordToNode!");
        }

    }

    public void addCvDefinitionRequestListener(CvDefinitionRequestListener l) {
        cvDefinitionRequestListeners.add(l);
    }

    private void fireLoadConfigVariables(List<ConfigurationVariable> configVariables) {
        // TODO decouple the AWT-thread from this work?
        LOGGER.info("Load the config variables.");
        for (CvDefinitionRequestListener l : cvDefinitionRequestListeners) {
            l.loadCvValues(configVariables);
        }
    }

    private void prepareConfigVariables(
        final CvNode cvNode, final List<ConfigurationVariable> configVariables,
        final Map<String, CvNode> cvNumberToNodeMap) {

        try {
            switch (cvNode.getCV().getType()) {
                case LONG:
                    // LONG nodes are processed
                    LongCvNode masterNode = ((LongCvNode) cvNode).getMasterNode();
                    configVariables.add(masterNode.getConfigVar());
                    for (CvNode slaveNode : masterNode.getSlaveNodes()) {
                        configVariables.add(slaveNode.getConfigVar());
                    }
                    break;
                case INT:
                    // INT nodes are processed
                    configVariables.add(cvNode.getConfigVar());
                    int highCvNum = Integer.parseInt(cvNode.getCV().getHigh());
                    int cvNumber = Integer.parseInt(cvNode.getCV().getNumber());

                    if (highCvNum == cvNumber) {
                        // search the low CV
                        CvNode lowCvNode = cvNumberToNodeMap.get(cvNode.getCV().getLow());
                        configVariables.add(lowCvNode.getConfigVar());
                    }
                    else {
                        // search the high CV
                        CvNode highCvNode = cvNumberToNodeMap.get(cvNode.getCV().getHigh());
                        if (highCvNode == null) {
                            LOGGER.warn("The highCvNode is not available: {}", cvNode.getCV());
                        }
                        configVariables.add(highCvNode.getConfigVar());
                    }
                    break;
                default:
                    configVariables.add(cvNode.getConfigVar());
                    break;
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Prepare config variables to read from node failed.", ex);
        }

    }

    private CvNode getCvNodeByKeyword(String keyword) {
        CvNode cvNode = mapKeywordToNode.get(keyword);
        return cvNode;
    }

    private Integer getConfigVarIntValue(String keyword, final Node selectedNode) {
        CvNode cvNode = getCvNodeByKeyword(keyword);
        Integer value = CvValueUtils.getConfigVarIntValue(cvNode, selectedNode.getCvNumberToJideNodeMap());
        return value;
    }

    /**
     * Find all ports greater than the provided port number in reverse order.
     * 
     * @param ports
     *            the ports
     * @param portNum
     *            the port number
     * @param portNumUpperBound
     *            the port number that is used as upper limit (exclusive)
     * @return the set of ports in reverse order
     */
    public static <T extends Port<?>> Set<T> findAllPortsInRange(
        List<T> ports, final int portNum, final Integer portNumUpperBound) {
        LOGGER.info("Find all ports with port number greater than: {}, lower than: {}", portNum, portNumUpperBound);

        Set<T> filteredSet = new TreeSet<>(new Comparator<T>() {
            @Override
            public int compare(T o1, T o2) {
                // inverted sort order
                return Integer.compare(o2.getId(), o1.getId());
            }
        });
        filteredSet.addAll(ports);

        CollectionUtils.filter(filteredSet, new Predicate<T>() {

            @Override
            public boolean evaluate(T port) {
                if (port.getId() > portNum) {

                    // check if upper bound is provided
                    if (portNumUpperBound != null && portNumUpperBound > port.getId()) {
                        return true;
                    }
                }
                return false;
            }
        });

        return filteredSet;
    }

    protected Integer testValidRange(final LightPort firstPort, int portsCount, Integer channelBStartNumber) {

        LOGGER.info("Test the valid range: {}, portsCount: {}, channelBStartNumber: {}", firstPort, portsCount,
            channelBStartNumber);

        if (channelBStartNumber == null) {
            throw new IllegalArgumentException("The channelBStartNumber must be provided.");
        }

        Integer upperRangeValue = channelBStartNumber * 3 /* 3 ports per WS28xx */;
        // situation 1: start is below channel b start num and portsCount will not cross the channel border

        int startPortRangeValue = firstPort.getId() + portsCount - 1;
        if (firstPort.getId() < upperRangeValue) {
            if (startPortRangeValue < upperRangeValue) {
                LOGGER.info(
                    "We want to move ports in channel A and the number of ports to insert will match into channel A range.");
                // we move ports in channel A -> the upper range is the channel A limit
            }
            else {
                LOGGER.warn(
                    "The number of inserted port crosses the channel border. Ports configurations will be dropped out.");
            }
        }
        else {
            LOGGER.info("The inserted ports will start in channel B. No upper range limit required.");
            upperRangeValue = null;
        }

        return upperRangeValue;
    }

    @Override
    public void cvDefinitionValuesChanged(final boolean read) {
        LOGGER.info("The CV definition values have changed, read: {}", read);

        final Node node = mainModel.getSelectedNode();
        if (node == null
            || (!ProductUtils.isNeoControl(node.getUniqueId()) && !ProductUtils.isNeoEWS(node.getUniqueId()))) {
            return;
        }

        // look for keyword
        if (ProductUtils.isNeoControl(node.getUniqueId())) {
            channelBStartNumber = getConfigVarIntValue(KEYWORD_CHANNEL_B_START_NUMBER, node);
        }
        else {
            Integer channelALastElement = getConfigVarIntValue(KEYWORD_CHANNEL_A_LAST_ELEMENT, node);
            if (channelALastElement != null) {
                LOGGER.info("Get the channel B start number from channel A last element value: {}",
                    channelALastElement);
                channelBStartNumber = channelALastElement + 1;
            }
        }
        LOGGER.info("Fetched channel B start number: {}", channelBStartNumber);
    }

    @Override
    public void cvDefinitionChanged() {
        LOGGER.info("The CV definition has changed.");
        final Node node = mainModel.getSelectedNode();
        if (node == null) {
            return;
        }

        triggerLoadCvValues(node);
    }

    private void triggerLoadCvValues(final Node node) {

        if (mapKeywordToNode == null
            && (ProductUtils.isNeoControl(node.getUniqueId()) || ProductUtils.isNeoEWS(node.getUniqueId()))) {
            LOGGER.info("A NeoControl or NeoEWS is detected. Get the start number of port for channel B.");
            readCurrentCvNodeValuesFromCVDefinition(node);
        }

    }

}
