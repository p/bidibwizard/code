package org.bidib.wizard.mvc.stepcontrol.model;

import org.bidib.wizard.mvc.stepcontrol.model.StepControlAspect.Polarity;

import com.jgoodies.binding.beans.Model;

public class StepControlAspectModel extends Model {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTYNAME_STEP_CONTROL_ASPECT = "stepControlAspect";

    public static final String PROPERTYNAME_STEP_NUMBER = "stepNumber";

    public static final String PROPERTYNAME_POSITION = "position";

    public static final String PROPERTYNAME_POLARITY = "polarity";

    private StepControlAspect stepControlAspect;

    public StepControlAspectModel() {
    }

    /**
     * @return the stepControlAspect
     */
    public StepControlAspect getStepControlAspect() {
        return stepControlAspect;
    }

    /**
     * @param stepControlAspect
     *            the stepControlAspect to set
     */
    public void setStepControlAspect(StepControlAspect stepControlAspect) {
        StepControlAspect oldValue = this.stepControlAspect;
        this.stepControlAspect = stepControlAspect;

        firePropertyChange(PROPERTYNAME_STEP_CONTROL_ASPECT, oldValue, stepControlAspect);
    }

    /**
     * @return the stepNumber
     */
    public Integer getStepNumber() {
        return (stepControlAspect != null ? stepControlAspect.getAspectNumber() : null);
    }

    /**
     * @param stepNumber
     *            the stepNumber to set
     */
    public void setStepNumber(int stepNumber) {
        int oldValue = stepControlAspect.getAspectNumber();
        stepControlAspect.setAspectNumber(stepNumber);

        firePropertyChange(PROPERTYNAME_STEP_NUMBER, oldValue, stepNumber);
    }

    /**
     * @return the position
     */
    public long getPosition() {
        return (stepControlAspect != null ? stepControlAspect.getPosition() : null);
    }

    /**
     * @param position
     *            the position to set
     */
    public void setPosition(long position) {
        long oldValue = stepControlAspect.getPosition();
        stepControlAspect.setPosition(position);

        firePropertyChange(PROPERTYNAME_POSITION, oldValue, position);
    }

    /**
     * @return the polarity
     */
    public Polarity getPolarity() {
        return (stepControlAspect != null ? stepControlAspect.getPolarity() : null);
    }

    /**
     * @param polarity
     *            the polarity to set
     */
    public void setPolarity(Polarity polarity) {
        Polarity oldValue = stepControlAspect.getPolarity();
        stepControlAspect.setPolarity(polarity);

        firePropertyChange(PROPERTYNAME_POLARITY, oldValue, polarity);
    }

}
