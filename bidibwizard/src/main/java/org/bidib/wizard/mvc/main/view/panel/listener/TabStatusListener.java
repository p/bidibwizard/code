package org.bidib.wizard.mvc.main.view.panel.listener;

import javax.swing.JPanel;

public interface TabStatusListener {

    /**
     * Update the pending changes marker.
     * 
     * @param source
     *            the source component
     * @param hasPendingChanges
     *            the pending changes flag
     */
    void updatePendingChanges(JPanel source, boolean hasPendingChanges);
}
