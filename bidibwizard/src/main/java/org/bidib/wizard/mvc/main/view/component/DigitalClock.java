package org.bidib.wizard.mvc.main.view.component;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;

import java.awt.Font;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

import javax.swing.JLabel;
import javax.swing.SwingUtilities;

import org.bidib.wizard.comm.Communication;
import org.bidib.wizard.comm.CommunicationFactory;
import org.bidib.wizard.comm.listener.CommunicationListener;
import org.bidib.wizard.mvc.common.model.PreferencesPortType;
import org.bidib.wizard.mvc.main.view.panel.listener.ModelClockStatusListener;
import org.bidib.wizard.mvc.preferences.model.Preferences;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DigitalClock extends JLabel implements CommunicationListener, ModelClockStatusListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(DigitalClock.class);

    private static final long serialVersionUID = 1L;

    private static final long INITIAL_DELAY = 60000;

    private static final long PERIOD = 60000;

    private Communication communication;

    private final SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");

    private final Calendar startTime = Calendar.getInstance();

    private long timeOffset = 0;

    private int timeFactor = 0;

    private boolean stopped = false;

    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    private ScheduledFuture<?> modelClockTimer;

    private ScheduledFuture<?> statusClock;

    private boolean startEnabled;

    // helper object to lock the communication access
    private Object commLock = new Object();

    public DigitalClock() {

        // enable start by default
        startEnabled = true;

        final Preferences preferences = Preferences.getInstance();

        checkSelectedPortType(preferences);

        preferences.addPropertyChangeListener(Preferences.PROPERTY_SELECTED_PORTTYPE, new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                checkSelectedPortType(preferences);
            }
        });

        Date startTime = preferences.getStartTime();
        int timeFactor = preferences.getTimeFactor();

        setStartTime(startTime);
        setTimeFactor(timeFactor);

        LOGGER.info("Add digital clock as communication listener.");
        CommunicationFactory.addCommunicationListener(this);
    }

    public void setBoldFont() {
        setFont(new Font("SansSerif", Font.BOLD, 20));
    }

    private Calendar getStartTime() {
        return startTime;
    }

    private Calendar getTime() {
        Calendar result = (Calendar) startTime.clone();
        long diff = System.currentTimeMillis() - result.getTime().getTime() - timeOffset;

        result.add(Calendar.MILLISECOND, (int) diff * timeFactor);
        return result;
    }

    private int getTimeFactor() {
        return timeFactor;
    }

    public void setStartTime(Date startTime) {
        Calendar start = Calendar.getInstance();

        start.setTime(startTime);
        this.startTime.set(Calendar.HOUR_OF_DAY, start.get(Calendar.HOUR_OF_DAY));
        this.startTime.set(Calendar.MINUTE, start.get(Calendar.MINUTE));
        this.startTime.set(Calendar.SECOND, start.get(Calendar.SECOND));
        timeOffset = System.currentTimeMillis() - this.startTime.getTime().getTime();
    }

    public void setTimeFactor(int timeFactor) {
        this.timeFactor = timeFactor;
        restartModelClock();
    }

    /**
     * @return the startEnabled
     */
    public boolean isStartEnabled() {
        return startEnabled;
    }

    /**
     * @param startEnabled
     *            the startEnabled to set
     */
    public void setStartEnabled(boolean startEnabled) {
        this.startEnabled = startEnabled;
    }

    public void start() {
        stopped = false;
        // startEnabled = true;
        LOGGER.info("Start the digital clock.");

        restartModelClock();
    }

    public void stop() {
        LOGGER.info("Stop the digital clock.");
        stopped = true;
        // startEnabled = false;

        if (modelClockTimer != null) {
            LOGGER.info("Stop the model clock timer");
            modelClockTimer.cancel(true);
            modelClockTimer = null;
        }
        if (statusClock != null) {
            statusClock.cancel(true);
            statusClock = null;
        }

        LOGGER.info("Send the clock passed.");

        setText(null);
    }

    @Override
    public void opening() {

    }

    @Override
    public void opened(String port) {
        LOGGER.info("The port was opened: {}", port);
    }

    private void restartModelClock() {
        if (modelClockTimer != null) {
            LOGGER.info("Stop the model clock timer");
            modelClockTimer.cancel(true);
            modelClockTimer = null;
        }
        if (statusClock != null) {
            statusClock.cancel(true);
            statusClock = null;
        }

        synchronized (commLock) {
            communication = CommunicationFactory.getInstance();
            if (communication == null || !communication.isOpened()) {
                LOGGER.info("No communication available or closed.");
                return;
            }
        }

        if (startEnabled && !preventStartClock) {
            LOGGER.info("The model clock start is enabled.");
            stopped = false;
        }
        else {
            LOGGER.info("Start model clock is not enabled.");

            return;
        }

        // communication is opened, start the timers
        timeOffset = System.currentTimeMillis() - this.startTime.getTime().getTime();

        modelClockTimer = scheduler.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                LOGGER.trace("Model clock timer has expired, stopped: {}", stopped);
                if (!stopped) {
                    Calendar currentTime = getTime();
                    LOGGER.info("Send current time: {}", currentTime.getTime());
                    synchronized (commLock) {
                        communication.sendTime(currentTime, getTimeFactor());
                    }
                }
            }
        }, INITIAL_DELAY / timeFactor, PERIOD / timeFactor, MILLISECONDS);

        // initially update the time label after one second
        statusClock = scheduler.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                if (!stopped) {
                    LOGGER.trace("Update the model clock.");
                    updateText(getTime().getTime());
                }
                else {
                    LOGGER.trace("Clock is stopped.");
                }
            }
        }, 0, 1, SECONDS);

        LOGGER.info("Send start clock: {}, factor: {}", getStartTime().getTime(), getTimeFactor());
        updateText(getTime().getTime());
        communication.sendTime(getStartTime(), getTimeFactor());
    }

    @Override
    public void initialized() {
        LOGGER.info("The initial loading phase has passed.");

        if (startEnabled) {
            restartModelClock();
        }
        else {
            LOGGER.info("The model clock is not enabled to start.");
        }
    }

    @Override
    public void status(String statusText, int displayDuration) {

    }

    @Override
    public void closed(String port) {
        LOGGER.info("The port was closed: {}", port);

        stop();
    }

    private void updateText(final Date date) {
        if (SwingUtilities.isEventDispatchThread()) {
            setText(format.format(date));
        }
        else {
            SwingUtilities.invokeLater(new Runnable() {

                @Override
                public void run() {
                    setText(format.format(date));
                }
            });
        }
    }

    @Override
    public void setModelTimeStartStatus(boolean enabled) {
        LOGGER.info("Set the start enabled flag: {}", enabled);
        startEnabled = enabled;
    }

    private boolean preventStartClock;

    private void checkSelectedPortType(final Preferences preferences) {
        PreferencesPortType portType = preferences.getSelectedPortType();
        if (portType != null
            && PreferencesPortType.ConnectionPortType.SerialOverTcpPort.equals(portType.getConnectionPortType())) {
            LOGGER.info("The current selected port type is: {}. Do not start the clock.", portType);

            preventStartClock = true;
        }
    }

}
