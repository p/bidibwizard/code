package org.bidib.wizard.mvc.cvprogrammer.view.panel;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.text.DecimalFormat;
import java.util.Collection;
import java.util.LinkedList;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;

import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.cvprogrammer.model.CvProgrammerModel;
import org.bidib.wizard.mvc.cvprogrammer.view.panel.listener.CvReadListener;

public class CvReadPanel extends JPanel {
    private static final long serialVersionUID = 1L;

    private final Collection<CvReadListener> listeners = new LinkedList<CvReadListener>();

    private final JLabel cvNumberLabel = new JLabel(Resources.getString(getClass(), "name") + ":");

    private final NumberFormatter formatter = new NumberFormatter(
        new DecimalFormat("#")) {
        private static final long serialVersionUID = 1L;

        {
            setAllowsInvalid(false);
            setMinimum(1);
        }
    };

    private final JSpinner cvNumber;

    public CvReadPanel(final CvProgrammerModel model) {
        setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
            Resources.getString(getClass(), "title") + ":"));
        setLayout(new GridBagLayout());

        GridBagConstraints c = new GridBagConstraints();

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 0;
        c.insets = new Insets(5, 5, 5, 5);
        c.weighty = 1;
        add(cvNumberLabel, c);

        SpinnerNumberModel spinnerModel = new SpinnerNumberModel();

        spinnerModel.setMinimum(1);
        spinnerModel.setValue(model.getNumber()); // set initial value

        cvNumber = new JSpinner(spinnerModel);

        cvNumber.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                JSpinner spinner = (JSpinner) e.getSource();

                model.setNumber((Integer) spinner.getValue());
            }
        });
        ((JSpinner.NumberEditor) cvNumber.getEditor()).getTextField().setFormatterFactory(
            new DefaultFormatterFactory(formatter, formatter, formatter));
        c.gridx++;
        c.weightx = 0.1;
        add(cvNumber, c);

        c.gridx++;
        c.weightx = 0.9;
        add(Box.createGlue(), c);
    }

    public void addCvReadListener(CvReadListener l) {
        listeners.add(l);
    }

    public int getLabelWidth() {
        return cvNumberLabel.getWidth();
    }

    public void setLabelWidth(int width) {
        cvNumberLabel.setPreferredSize(new Dimension(width, cvNumberLabel.getSize().height));
    }
}
