package org.bidib.wizard.mvc.main.model;

import java.io.Serializable;

public interface LabelAware extends Serializable {
    /**
     * @return the label
     */
    String getLabel();

    /**
     * @param label
     *            the label to set
     */
    void setLabel(String label);
}
