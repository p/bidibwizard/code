package org.bidib.wizard.mvc.main.view.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vlsolutions.swing.docking.DockKey;
import com.vlsolutions.swing.docking.Dockable;
import com.vlsolutions.swing.docking.DockingUtilities;
import com.vlsolutions.swing.docking.TabbedDockableContainer;

public class WindowListMenu {
    private static final Logger LOGGER = LoggerFactory.getLogger(WindowListMenu.class);

    private DockingFramesRegistry dockingFramesRegistry;

    private Map<String, JMenuItem> stickyItems = new TreeMap<>();

    private final JMenu menu;

    public WindowListMenu(DockingFramesRegistry dockingFramesRegistry) {
        this.dockingFramesRegistry = dockingFramesRegistry;

        menu = new JMenu();
    }

    public JMenu getJMenu() {
        return menu;
    }

    public void addStickyItem(String dockKeyKey, JMenuItem menuItem) {
        stickyItems.put(dockKeyKey, menuItem);
    }

    public void prepareMenu() {
        menu.removeAll();
        // add sticky items
        for (JMenuItem menuItem : stickyItems.values()) {
            addMenuItem(menu, menuItem);
        }

        // query the registry
        for (Dockable dockable : dockingFramesRegistry.getDockables()) {
            String dockKeyKey = dockable.getDockKey().getKey();
            if (!stickyItems.containsKey(dockKeyKey)) {
                JMenuItem editLabel = new JMenuItem(dockable.getDockKey().getName());

                editLabel.setActionCommand(dockable.getDockKey().getKey());
                LOGGER.trace("Created new menu item: {}", editLabel);
                editLabel.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        String dockKey = e.getActionCommand();
                        Dockable dockable = dockingFramesRegistry.getDockable(new DockKey(dockKey));
                        LOGGER.trace("Select dockable: {}", dockable);
                        selectWindow(dockable);
                    }
                });

                addMenuItem(menu, editLabel);
            }
        }
    }

    private void selectWindow(Dockable dockable) {

        // TODO this should also work with non-tabbed windows
        TabbedDockableContainer container = DockingUtilities.findTabbedDockableContainer(dockable);
        if (container != null) {
            container.setSelectedDockable(dockable);
        }
        else {
            LOGGER.warn("Container not available, select component directly.");
            dockable.getComponent().requestFocusInWindow();
        }
    }

    private void addMenuItem(Object menu, JMenuItem menuItem) {
        if (menu instanceof JMenu) {
            ((JMenu) menu).add(menuItem);
        }
        else if (menu instanceof JPopupMenu) {
            ((JPopupMenu) menu).add(menuItem);
        }
    }
}
