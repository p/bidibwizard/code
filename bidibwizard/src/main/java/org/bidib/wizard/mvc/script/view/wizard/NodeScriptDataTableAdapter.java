package org.bidib.wizard.mvc.script.view.wizard;

import javax.swing.ListModel;

import org.bidib.wizard.mvc.script.model.NodeScriptData;

import com.jgoodies.binding.adapter.AbstractTableAdapter;

public class NodeScriptDataTableAdapter extends AbstractTableAdapter<NodeScriptData> {
    private static final long serialVersionUID = 1L;

    public static final int COLUMN_APPLICATION = 0;

    public static final int COLUMN_INFO = 1;

    public NodeScriptDataTableAdapter(ListModel<NodeScriptData> listModel, String[] columnNames) {
        super(listModel, columnNames);
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        NodeScriptData nodeScriptData = (NodeScriptData) getRow(rowIndex);
        switch (columnIndex) {
            case COLUMN_APPLICATION:
                return nodeScriptData.toString();
            default:
                break;
        }
        return nodeScriptData;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case COLUMN_APPLICATION:
                return String.class;
            default:
                break;
        }
        return super.getColumnClass(columnIndex);
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }
}
