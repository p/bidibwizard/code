package org.bidib.wizard.mvc.pom.view.panel;

import java.awt.CardLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.wizard.mvc.common.view.panel.DisabledPanel;
import org.bidib.wizard.mvc.pom.model.PomMode;
import org.bidib.wizard.mvc.pom.model.PomProgrammerModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.FormLayout;

public class CvWritePanel {

    private static final Logger LOGGER = LoggerFactory.getLogger(CvWritePanel.class);

    private final JCheckBox[] checkBitValues = new JCheckBox[8];

    private final JRadioButton[] radioBitValues = new JRadioButton[8];

    private JPanel byteValuePanel;

    private JPanel bitValuePanel;

    private final PomProgrammerModel programmerModel;

    private final DirectAccessProgBeanModel cvModel;

    private CardLayout cardLayout;

    private boolean lockUpdate;

    private JPanel panel;

    public CvWritePanel(final PomProgrammerModel model, final DirectAccessProgBeanModel cvModel) {
        this.programmerModel = model;
        this.cvModel = cvModel;
    }

    public JPanel createPanel() {

        panel = new JPanel();
        panel.setOpaque(false);
        cardLayout = new CardLayout();
        panel.setLayout(cardLayout);
        panel.add(prepareByteValuePanel(), "byte");
        panel.add(prepareBitValuePanel(), "bit");

        cvModel.addPropertyChangeListener(DirectAccessProgBeanModel.PROPERTYNAME_MODE, new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                LOGGER.debug("Operation has changed: {}", cvModel.getMode());
                switch (cvModel.getMode()) {
                    case BIT:
                        programmerModel.clearCvValue();
                        cardLayout.show(panel, "bit");
                        radioBitValues[0].setSelected(true);
                        break;
                    default:
                        programmerModel.clearCvValue();
                        cardLayout.show(panel, "byte");
                        break;
                }
            }
        });
        return panel;
    }

    private JPanel prepareByteValuePanel() {

        // create the panel for the bits in the byte
        FormLayout layout =
            new FormLayout("p, 3dlu, p, 3dlu, p, 3dlu, p, 3dlu, p, 3dlu, p, 3dlu, p, 3dlu, p, 0:grow", "p");

        PanelBuilder pbuilder = new PanelBuilder(layout);

        for (int bit = 7; bit >= 0; bit--) {
            checkBitValues[bit] = new JCheckBox(String.valueOf(bit), false);
            checkBitValues[bit].setContentAreaFilled(false);
            checkBitValues[bit].addItemListener(new ItemListener() {
                public void itemStateChanged(ItemEvent e) {
                    // if the bit panel is active we do not process here
                    if (PomMode.BIT.equals(cvModel.getMode())) {
                        return;
                    }

                    if (lockUpdate) {
                        LOGGER.info("The item state has changed but lockUpdate is set.");
                        return;
                    }

                    // set integer value from bit values
                    final StringBuffer bitValue = new StringBuffer();

                    for (int bit = checkBitValues.length - 1; bit >= 0; bit--) {
                        bitValue.append(checkBitValues[bit].isSelected() ? '1' : '0');
                    }

                    LOGGER.info("Prepared byte value: {}", bitValue);
                    try {
                        // update the value model with the new value
                        cvModel.setCvValue(Integer.parseInt(bitValue.toString(), 2));
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Set CV value failed.", ex);
                    }
                }
            });

            pbuilder.add(checkBitValues[bit]);
            if (bit > 0) {
                pbuilder.nextColumn(2);
            }
        }
        pbuilder.nextColumn();

        byteValuePanel = pbuilder.getPanel();
        return byteValuePanel;
    }

    private JPanel prepareBitValuePanel() {
        // create the panel for the bit values
        FormLayout layout =
            new FormLayout("p, 3dlu, p, 3dlu, p, 3dlu, p, 3dlu, p, 3dlu, p, 3dlu, p, 3dlu, p, 0:grow", "p");

        PanelBuilder pbuilder = new PanelBuilder(layout);

        // Group the radio buttons.
        ButtonGroup group = new ButtonGroup();

        for (int bit = 7; bit >= 0; bit--) {
            radioBitValues[bit] = new JRadioButton(String.valueOf(bit), false);
            radioBitValues[bit].setContentAreaFilled(false);
            group.add(radioBitValues[bit]);

            pbuilder.add(radioBitValues[bit]);
            if (bit > 0) {
                pbuilder.nextColumn(2);
            }
        }
        pbuilder.nextColumn();

        bitValuePanel = pbuilder.getPanel();
        return bitValuePanel;
    }

    public void updateByteValue(Object cvValue) {
        LOGGER.info("Update the byte value: {}", cvValue);

        if (cvValue == null) {
            // clear all check boxes
            cvValue = "0";
        }

        try {
            String bitValue = Integer.toBinaryString(Integer.parseInt(cvValue.toString()));

            lockUpdate = true;

            for (int index = 0; index < checkBitValues.length; index++) {
                if (index < bitValue.length()) {
                    checkBitValues[index].setSelected(bitValue.charAt(bitValue.length() - index - 1) == '1');
                }
                else {
                    checkBitValues[index].setSelected(false);
                }
            }
        }
        catch (NumberFormatException ex) {
            for (int index = 0; index < checkBitValues.length; index++) {
                checkBitValues[index].setSelected(false);
            }
        }

        finally {
            lockUpdate = false;
        }

    }

    public int getSelectedBit() {
        // set integer value from bit values

        // Beim Bit Schreiben wird das zuschreibende Bit mittels DATA bestimmt: DATA = 111KDBBB,
        // wobei BBB die Bitposition angibt und D den Wert des Bits. K ist die Operation (1=write,
        // 0=read)(identisch zur DCC Definition)

        byte val = (byte) 0x00;
        for (int bit = radioBitValues.length - 1; bit >= 0; bit--) {
            if (radioBitValues[bit].isSelected()) {
                // TODO fix RD and WR bit value
                val = (byte) (bit);
                break;
            }
        }

        LOGGER.info("Prepared bitValue: {}", ByteUtils.byteToHex(val));

        return ByteUtils.getInt(val);
    }

    public void setEnabled(boolean enabled) {
        panel.setEnabled(enabled);
        // bitValuePanel.setEnabled(enabled);
        // byteValuePanel.setEnabled(enabled);

        if (enabled) {
            DisabledPanel.enable(bitValuePanel);
            DisabledPanel.enable(byteValuePanel);
        }
        else {
            DisabledPanel.disable(bitValuePanel);
            DisabledPanel.disable(byteValuePanel);
        }
    }
}
