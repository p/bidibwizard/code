package org.bidib.wizard.mvc.accessory.controller.listener;

import org.bidib.jbidibc.core.AddressData;
import org.bidib.jbidibc.core.enumeration.AccessoryAcknowledge;
import org.bidib.jbidibc.core.enumeration.TimeBaseUnitEnum;
import org.bidib.jbidibc.core.enumeration.TimingControlEnum;
import org.bidib.wizard.mvc.main.model.Node;

public interface AccessoryControllerListener {

    /**
     * Send the accessory request.
     * 
     * @param node
     *            the BiDiB node
     * @param dccAddress
     *            the DCC address to switch
     * @param aspect
     *            the aspect to set
     * @param switchTime
     *            the switch time
     * @param timeBaseUnit
     *            the time base unit (100ms or 1s)
     * @param timingControl
     *            the timing control
     * @return the accessory acknowledge
     */
    AccessoryAcknowledge sendAccessoryRequest(
        Node node, AddressData dccAddress, Integer aspect, Integer switchTime, TimeBaseUnitEnum timeBaseUnit,
        TimingControlEnum timingControl);
}
