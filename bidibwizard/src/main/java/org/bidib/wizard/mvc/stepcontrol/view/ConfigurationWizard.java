package org.bidib.wizard.mvc.stepcontrol.view;

import java.awt.Frame;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.common.utils.ImageUtils;
import org.bidib.wizard.mvc.common.view.ComponentUtils;
import org.bidib.wizard.mvc.common.view.wizard.CancelAction;
import org.bidib.wizard.mvc.common.view.wizard.FinishAction;
import org.bidib.wizard.mvc.stepcontrol.model.ConfigurationWizardModel;
import org.bidib.wizard.mvc.stepcontrol.model.ConfigurationWizardModel.WizardStatus;
import org.bidib.wizard.mvc.stepcontrol.view.wizard.CustomLogoJWizardDialog;
import org.bidib.wizard.mvc.stepcontrol.view.wizard.StepMotorCharacteristicsPanel;
import org.bidib.wizard.mvc.stepcontrol.view.wizard.SummaryPanel;
import org.bidib.wizard.mvc.stepcontrol.view.wizard.TableTypePanel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConfigurationWizard {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigurationWizard.class);

    private ConfigurationWizardModel configurationWizardModel;

    public ConfigurationWizard(final ConfigurationWizardModel configurationWizardModel) {
        this.configurationWizardModel = configurationWizardModel;
    }

    public void showWizard() {
        Frame frame = JOptionPane.getFrameForComponent(null);
        ImageIcon turntableLogo = ImageUtils.createImageIcon(getClass(), "/icons/stepcontrol/turntable.png");
        ImageIcon transferTableLogo = ImageUtils.createImageIcon(getClass(), "/icons/stepcontrol/transfertable.png");

        // create the modal wizard: the constructor takes the parent frame
        final CustomLogoJWizardDialog wizardDialog =
            new CustomLogoJWizardDialog(frame, new ImageIcon[] { transferTableLogo, turntableLogo }, true);
        wizardDialog.getWizardComponents().setFinishAction(new FinishAction(
            wizardDialog.getWizardComponents()) {

            @Override
            public void performAction() {
                configurationWizardModel.setWizardStatus(WizardStatus.finished);
                wizardDialog.dispose();
            }
        });
        wizardDialog.getWizardComponents().setCancelAction(new CancelAction(
            wizardDialog.getWizardComponents()) {

            @Override
            public void performAction() {
                configurationWizardModel.setWizardStatus(WizardStatus.aborted);
                wizardDialog.dispose();
            }
        });

        wizardDialog.getWizardComponents().getFinishButton().setText(Resources.getString(getClass(), "L_FinishButton"));
        wizardDialog
            .getWizardComponents().getFinishButton()
            .setMnemonic(Resources.getString(getClass(), "L_FinishButtonMnem").charAt(0));

        SwingUtilities.updateComponentTreeUI(wizardDialog);

        // set the dialog title
        wizardDialog.setTitle(Resources.getString(getClass(), "title"));

        // add panels to the wizard
        wizardDialog.getWizardComponents().addWizardPanel(
            new TableTypePanel(wizardDialog.getWizardComponents(), configurationWizardModel));
        wizardDialog.getWizardComponents().addWizardPanel(
            new StepMotorCharacteristicsPanel(wizardDialog.getWizardComponents(), configurationWizardModel));
        wizardDialog.getWizardComponents().addWizardPanel(
            new SummaryPanel(wizardDialog.getWizardComponents(), configurationWizardModel));

        wizardDialog.setSize(620, 380);
        wizardDialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        ComponentUtils.centerComponentOnScreen(wizardDialog);

        // show the wizard
        wizardDialog.setVisible(true);

        LOGGER.info("The wizard has finished, configurationWizardModel: {}", configurationWizardModel);
    }

    public ConfigurationWizardModel getConfigurationWizardModel() {
        // only return the model if the wizard has finished
        if (configurationWizardModel.getWizardStatus() == WizardStatus.finished) {
            return configurationWizardModel;
        }
        return null;
    }

}
