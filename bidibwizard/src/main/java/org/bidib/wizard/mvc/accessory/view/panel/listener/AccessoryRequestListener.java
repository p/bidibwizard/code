package org.bidib.wizard.mvc.accessory.view.panel.listener;

import org.bidib.jbidibc.core.AddressData;
import org.bidib.jbidibc.core.enumeration.TimeBaseUnitEnum;
import org.bidib.jbidibc.core.enumeration.TimingControlEnum;

public interface AccessoryRequestListener {

    /**
     * Send the accessory request.
     * 
     * @param accessoryResultListener
     *            the accessory result listener
     * @param dccAddress
     *            the DCC address to switch
     * @param aspect
     *            the aspect to set
     * @param switchTime
     *            the switch time
     * @param timeBaseUnit
     *            the time base unit (100ms or 1s)
     * @param timingControl
     *            the timing control
     */
    void sendRequest(
        AccessoryResultListener accessoryResultListener, AddressData dccAddress, Integer aspect, Integer switchTime,
        TimeBaseUnitEnum timeBaseUnit, TimingControlEnum timingControl);
}
