package org.bidib.wizard.mvc.main.view.table;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.LinkedList;

import javax.swing.AbstractCellEditor;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

import org.bidib.wizard.mvc.main.view.table.listener.ButtonListener;

public class ComboBoxWithButtonEditor extends AbstractCellEditor implements TableCellEditor {
    private static final long serialVersionUID = 1L;

    private final Collection<ButtonListener> buttonListeners = new LinkedList<ButtonListener>();

    private final JPanel panel = new JPanel();

    private final JComboBox<Object> comboBox;

    private final JButton button;

    private int row = 0;

    private int column = 0;

    public ComboBoxWithButtonEditor(Object[] items, String buttonText) {
        comboBox = new JComboBox<Object>(items.clone());
        button = new JButton(buttonText);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireEditingStopped();
                fireButtonPressed();
            }
        });
        panel.setLayout(new GridBagLayout());
        panel.setFocusCycleRoot(true);

        GridBagConstraints c = new GridBagConstraints();

        c.anchor = GridBagConstraints.FIRST_LINE_START;
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0;
        c.insets = new Insets(0, 0, 2, 2);
        c.weightx = 1;
        panel.add(comboBox, c);
        c.anchor = GridBagConstraints.FIRST_LINE_END;
        c.gridx++;
        c.weightx = 0;
        panel.add(button, c);
    }

    public void addButtonListener(ButtonListener l) {
        buttonListeners.add(l);
    }

    private void fireButtonPressed() {
        for (ButtonListener l : buttonListeners) {
            l.buttonPressed(row, column);
        }
    }

    @Override
    public Object getCellEditorValue() {
        return comboBox.getSelectedItem();
    }

    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        this.row = row;
        this.column = column;
        if (isSelected) {
            panel.setForeground(table.getSelectionForeground());
            panel.setBackground(table.getSelectionBackground());
        }
        else {
            panel.setForeground(table.getForeground());
            panel.setBackground(table.getBackground());
        }
        comboBox.setSelectedItem(value);
        return panel;
    }
}
