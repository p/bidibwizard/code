package org.bidib.wizard.mvc.main.view.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.LinkedList;

import javax.swing.JMenuItem;

import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.main.view.menu.listener.PortListMenuListener;

public class PortListMenu extends BasicPopupMenu {
    private static final long serialVersionUID = 1L;

    private final Collection<PortListMenuListener> menuListeners = new LinkedList<>();

    private JMenuItem mapPortItem;

    private JMenuItem insertPortsItem;

    public PortListMenu() {
        JMenuItem editLabel = new JMenuItem(Resources.getString(getClass(), "editLabel") + " ...");

        editLabel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fireEditLabel();
            }
        });
        add(editLabel);

        mapPortItem = new JMenuItem(Resources.getString(getClass(), "mapPort") + " ...");

        mapPortItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fireMapPort();
            }
        });
        add(mapPortItem);

        insertPortsItem = new JMenuItem(Resources.getString(getClass(), "insertPorts") + " ...");
        insertPortsItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fireInsertPorts();
            }
        });
        add(insertPortsItem);
    }

    public void addMenuListener(PortListMenuListener l) {
        menuListeners.add(l);
    }

    private void fireEditLabel() {
        for (PortListMenuListener l : menuListeners) {
            l.editLabel();
        }
    }

    private void fireMapPort() {
        for (PortListMenuListener l : menuListeners) {
            l.mapPort();
        }
    }

    private void fireInsertPorts() {
        for (PortListMenuListener l : menuListeners) {
            l.insertPorts();
        }
    }

    public void setMapPortEnabled(boolean enabled, boolean visible) {
        mapPortItem.setEnabled(enabled);
        mapPortItem.setVisible(visible);
    }

    public void setInsertPortsEnabled(boolean enabled, boolean visible) {
        insertPortsItem.setEnabled(enabled);
        insertPortsItem.setVisible(visible);
    }
}
