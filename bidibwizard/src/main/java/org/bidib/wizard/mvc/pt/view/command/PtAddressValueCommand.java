package org.bidib.wizard.mvc.pt.view.command;

import org.bidib.jbidibc.core.enumeration.AddressMode;
import org.bidib.jbidibc.core.enumeration.PtOperation;
import org.bidib.wizard.mvc.pt.view.panel.AddressProgBeanModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PtAddressValueCommand extends PtOperationCommand<AddressProgBeanModel> {
    private static final Logger LOGGER = LoggerFactory.getLogger(PtAddressValueCommand.class);

    public enum ValueType {
        low, high;
    }

    private final ValueType valueType;

    public PtAddressValueCommand(PtOperation ptOperation, int cvNumber, int cvValue, ValueType valueType) {
        super(ptOperation, cvNumber, cvValue);
        this.valueType = valueType;
    }

    /**
     * @param cvValueResult
     *            the cvValueResult to set
     */
    public void setCvValueResult(Integer cvValueResult) {
        super.setCvValueResult(cvValueResult);
    }

    @Override
    public void postExecute(AddressProgBeanModel addressProgBeanModel) {
        super.postExecute(addressProgBeanModel);

        LOGGER.debug("PostExecute, cvValueresult: {}", getCvValueResult());
        if (getCvValueResult() != null) {
            // set the address
            Integer addressValue = addressProgBeanModel.getAddress();
            int address = addressValue != null ? addressValue.intValue() : 0;
            int addressPart = getCvValueResult().intValue();
            AddressMode mode = addressProgBeanModel.getAddressMode();

            LOGGER.debug("Stored address: {}, new addressPart: {}, valueType: {}, mode: {}", address, addressPart,
                valueType, mode);

            if (AddressMode.LONG.equals(mode)) {
                if (ValueType.high.equals(valueType)) {
                    // result from CV18
                    address = (address & 0xFF00) + (addressPart & 0xFF);
                }
                else {
                    // result from CV17
                    address = (address & 0xFF) + (((addressPart & 0xFF) - 192) * 256);
                }
            }
            else {
                address = (addressPart & 0xFF);
            }
            LOGGER.debug("Prepared new address: {}", address);
            addressProgBeanModel.setAddress(address);
        }
    }
}
