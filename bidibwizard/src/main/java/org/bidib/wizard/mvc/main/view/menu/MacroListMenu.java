package org.bidib.wizard.mvc.main.view.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.LinkedList;

import javax.swing.JMenuItem;

import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.main.view.menu.listener.MacroListMenuListener;

public class MacroListMenu extends BasicPopupMenu {
    private static final long serialVersionUID = 1L;

    private final Collection<MacroListMenuListener> menuListeners = new LinkedList<MacroListMenuListener>();

    public MacroListMenu() {
        JMenuItem editLabel = new JMenuItem(Resources.getString(getClass(), "editLabel") + " ...");

        editLabel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fireEditLabel();
            }
        });
        add(editLabel);

        JMenuItem start = new JMenuItem(Resources.getString(getClass(), "start"));

        start.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fireStart();
            }
        });
        start.setToolTipText(Resources.getString(getClass(), "start.tooltip"));
        add(start);

        JMenuItem stop = new JMenuItem(Resources.getString(getClass(), "stop"));

        stop.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fireStop();
            }
        });
        stop.setToolTipText(Resources.getString(getClass(), "stop.tooltip"));
        add(stop);

        addSeparator();

        JMenuItem imp = new JMenuItem(Resources.getString(getClass(), "import") + " ...");

        imp.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fireImport();
            }
        });
        add(imp);

        JMenuItem export = new JMenuItem(Resources.getString(getClass(), "export") + " ...");

        export.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fireExport();
            }
        });
        add(export);

        addSeparator();

        JMenuItem initialize = new JMenuItem(Resources.getString(getClass(), "initialize"));

        initialize.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fireInitialize();
            }
        });
        initialize.setToolTipText(Resources.getString(getClass(), "initialize.tooltip"));
        add(initialize);
    }

    public void addMenuListener(MacroListMenuListener l) {
        menuListeners.add(l);
    }

    private void fireEditLabel() {
        for (MacroListMenuListener l : menuListeners) {
            l.editLabel();
        }
    }

    private void fireExport() {
        for (MacroListMenuListener l : menuListeners) {
            l.exportMacro();
        }
    }

    private void fireImport() {
        for (MacroListMenuListener l : menuListeners) {
            l.importMacro();
        }
    }

    private void fireStart() {
        for (MacroListMenuListener l : menuListeners) {
            l.startMacro();
        }
    }

    private void fireStop() {
        for (MacroListMenuListener l : menuListeners) {
            l.stopMacro();
        }
    }

    private void fireInitialize() {
        for (MacroListMenuListener l : menuListeners) {
            l.initializeMacro();
        }
    }
}
