package org.bidib.wizard.mvc.features.view.panel;

import java.awt.Component;
import java.awt.event.MouseEvent;
import java.util.EventObject;

import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellEditor;

import org.bidib.jbidibc.core.Feature;
import org.bidib.jbidibc.core.enumeration.FeatureEnum;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FeatureAwareEditor extends AbstractCellEditor implements TableCellEditor, RangeValidationCallback {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(FeatureAwareEditor.class);

    private final JTextField textField = new JTextField();

    private int clickCountToStart = 2;

    /**
     * Create a new instance of FeatureAwareEditor
     */
    public FeatureAwareEditor() {
        IntegerInputValidationDocument doc =
            new IntegerInputValidationDocument(3, IntegerInputValidationDocument.NUMERIC);
        doc.setRangeValidationCallback(this);
        textField.setDocument(doc);
    }

    /**
     * Specifies the number of clicks needed to start editing.
     * 
     * @param count
     *            an int specifying the number of clicks needed to start editing
     * @see #getClickCountToStart
     */
    public void setClickCountToStart(int count) {
        clickCountToStart = count;
    }

    /**
     * Returns the number of clicks needed to start editing.
     * 
     * @return the number of clicks needed to start editing
     */
    public int getClickCountToStart() {
        return clickCountToStart;
    }

    /**
     * Returns true if <code>anEvent</code> is <b>not</b> a <code>MouseEvent</code>. Otherwise, it returns true if the
     * necessary number of clicks have occurred, and returns false otherwise.
     * 
     * @param anEvent
     *            the event
     * @return true if cell is ready for editing, false otherwise
     * @see #setClickCountToStart
     * @see #shouldSelectCell
     */
    public boolean isCellEditable(EventObject anEvent) {
        if (anEvent instanceof MouseEvent) {
            return ((MouseEvent) anEvent).getClickCount() >= clickCountToStart;
        }
        return true;
    }

    @Override
    public Object getCellEditorValue() {
        return textField.getText();
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {

        textField.setText(null);
        textField.setToolTipText(null);

        editedFeature = null;

        if (value instanceof Feature) {

            Feature feature = (Feature) value;
            editedFeature = FeatureEnum.valueOf(ByteUtils.getLowByte(feature.getType()));

            int featureValue = feature.getValue();

            textField.setText(Integer.toString(featureValue));
            // textField.setToolTipText("This port is inactive");
        }
        else {
            LOGGER.warn("Provided value is not a feature: {}", value);
        }
        return textField;
    }

    private FeatureEnum editedFeature;

    @Override
    public int getMinValue() {
        if (editedFeature != null) {
            Integer minValue = editedFeature.getMin();
            if (minValue != null) {
                return minValue.intValue();
            }
        }
        return 0;
    }

    @Override
    public int getMaxValue() {
        if (editedFeature != null) {
            Integer maxValue = editedFeature.getMax();
            if (maxValue != null) {
                return maxValue.intValue();
            }
        }
        return 255;
    }
}
