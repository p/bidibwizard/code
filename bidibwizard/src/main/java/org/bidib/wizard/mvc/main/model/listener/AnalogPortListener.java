package org.bidib.wizard.mvc.main.model.listener;

import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.mvc.main.model.AnalogPort;

public interface AnalogPortListener<S extends BidibStatus> extends OutputListener<S> {
    void valuesChanged(AnalogPort port);
}
