package org.bidib.wizard.mvc.accessory.view.panel;

import java.awt.BorderLayout;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import org.bidib.jbidibc.core.AddressData;
import org.bidib.jbidibc.core.enumeration.AccessoryAcknowledge;
import org.bidib.jbidibc.core.enumeration.AccessoryAddressingEnum;
import org.bidib.jbidibc.core.enumeration.TimeBaseUnitEnum;
import org.bidib.jbidibc.core.enumeration.TimingControlEnum;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.accessory.model.AccessoryBeanModel;
import org.bidib.wizard.mvc.accessory.model.AccessoryModel;
import org.bidib.wizard.mvc.accessory.view.panel.listener.AccessoryRequestListener;
import org.bidib.wizard.mvc.accessory.view.panel.listener.AccessoryResultListener;
import org.bidib.wizard.mvc.common.view.converter.StringConverter;
import org.bidib.wizard.mvc.common.view.validation.IconFeedbackPanel;
import org.bidib.wizard.mvc.common.view.validation.PropertyValidationI18NSupport;
import org.bidib.wizard.utils.InputValidationDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.helpers.MessageFormatter;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.binding.value.ConverterValueModel;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.debug.FormDebugPanel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.util.PropertyValidationSupport;
import com.jgoodies.validation.view.ValidationComponentUtils;

public abstract class AbstractAccessoryPanel<T extends AccessoryBeanModel> implements AccessoryResultListener {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    private static final String ENCODED_DIALOG_COLUMN_SPECS =
        "pref, 3dlu, pref, 10dlu, pref, 3dlu, pref, 3dlu, pref, 3dlu, pref:grow";

    private final List<AccessoryRequestListener> listeners = new LinkedList<AccessoryRequestListener>();

    protected AccessoryValidationResultModel accessoryValidationModel;

    private ValueModel addressValueModel;

    private ValueModel switchTimeValueModel;

    private JComponent[] addressingButtons;

    private JComponent[] baseUnitButtons;

    private final static String NEWLINE = "\n";

    private JTextArea loggerArea;

    protected boolean activeTab;

    public AbstractAccessoryPanel(final AccessoryModel accessoryModel) {
        // this.accessoryModel = accessoryModel;
    }

    protected abstract T getAccessoryBeanModel();

    public JPanel createPanel() {

        DefaultFormBuilder builder = null;
        boolean debugDialog = false;
        if (debugDialog) {
            JPanel panel = new FormDebugPanel();
            builder = new DefaultFormBuilder(new FormLayout(ENCODED_DIALOG_COLUMN_SPECS), panel);
        }
        else {
            JPanel panel = new JPanel(new BorderLayout());
            builder = new DefaultFormBuilder(new FormLayout(ENCODED_DIALOG_COLUMN_SPECS), panel);
        }
        builder.border(Borders.DIALOG);

        // add a validation model that can trigger a button state with the validState property
        accessoryValidationModel = new AccessoryValidationResultModel();

        addressValueModel =
            new PropertyAdapter<T>(getAccessoryBeanModel(), AccessoryModel.PROPERTYNAME_DCC_ADDRESS, true);

        final ValueModel addressConverterModel =
            new ConverterValueModel(addressValueModel, new StringConverter(new DecimalFormat("#")));

        JTextField addressText = new JTextField();
        InputValidationDocument addressDocument = new InputValidationDocument(4, InputValidationDocument.NUMERIC);
        addressText.setDocument(addressDocument);
        addressText.setColumns(4);

        // bind manually because we changed the document of the textfield
        Bindings.bind(addressText, addressConverterModel, false);

        builder.append(Resources.getString(AbstractAccessoryPanel.class, "dcc-address"), addressText);

        ValidationComponentUtils.setMandatory(addressText, true);
        ValidationComponentUtils.setMessageKeys(addressText, "validation.address_key");

        JLabel addressingLabel = builder.append(Resources.getString(AbstractAccessoryPanel.class, "addressing"));
        addressingLabel.setHorizontalAlignment(SwingConstants.TRAILING);
        ValueModel addressingModel =
            new PropertyAdapter<T>(getAccessoryBeanModel(), AccessoryModel.PROPERTYNAME_ACCESSORY_ADDRESSING, true);
        addressingButtons = new JComponent[AccessoryAddressingEnum.values().length];
        int index = 0;
        for (AccessoryAddressingEnum accessoryAddressing : AccessoryAddressingEnum.values()) {

            JRadioButton radio =
                BasicComponentFactory.createRadioButton(addressingModel, accessoryAddressing,
                    Resources.getString(AccessoryAddressingEnum.class, accessoryAddressing.getKey()));
            radio.setToolTipText(Resources.getString(AccessoryAddressingEnum.class, accessoryAddressing.getKey()
                + ".tooltip"));

            addressingButtons[index++] = radio;

            // add radio button
            builder.append(radio);
        }

        ValueModel directAddressValueModel =
            new PropertyAdapter<T>(getAccessoryBeanModel(), AccessoryModel.PROPERTYNAME_DCC_ADDRESS, true);
        final ValueModel directAddressConverterModel =
            new ConverterValueModel(directAddressValueModel, new RCN213Converter(addressingModel,
                new DecimalFormat("#")));

        JTextField directAddressText = new JTextField();
        directAddressText.setColumns(4);
        // bind manually because we changed the document of the textfield
        Bindings.bind(directAddressText, directAddressConverterModel, false);

        builder.append(directAddressText);
        // set disabled after appending to builder
        directAddressText.setEditable(false);
        directAddressText.setEnabled(false);

        builder.nextLine();

        switchTimeValueModel =
            new PropertyAdapter<T>(getAccessoryBeanModel(), AccessoryModel.PROPERTYNAME_SWITCH_TIME, true);

        final ValueModel switchTimeConverterModel =
            new ConverterValueModel(switchTimeValueModel, new StringConverter(new DecimalFormat("#")));

        JTextField switchTimeText = new JTextField();
        InputValidationDocument switchTimeDocument = new InputValidationDocument(3, InputValidationDocument.NUMERIC);
        switchTimeText.setDocument(switchTimeDocument);
        switchTimeText.setColumns(3);

        // bind manually because we changed the document of the textfield
        Bindings.bind(switchTimeText, switchTimeConverterModel, false);

        builder.append(Resources.getString(AbstractAccessoryPanel.class, "switchTime"), switchTimeText);

        ValidationComponentUtils.setMandatory(switchTimeText, true);
        ValidationComponentUtils.setMessageKeys(switchTimeText, "validation.switchtime_key");

        JLabel timeBaseUnit = builder.append(Resources.getString(AbstractAccessoryPanel.class, "timeBaseUnit"));
        timeBaseUnit.setHorizontalAlignment(SwingConstants.TRAILING);
        ValueModel modeModel =
            new PropertyAdapter<T>(getAccessoryBeanModel(), AccessoryModel.PROPERTYNAME_TIME_BASE_UNIT, true);
        baseUnitButtons = new JComponent[TimeBaseUnitEnum.values().length];

        index = 0;
        for (TimeBaseUnitEnum baseUnit : TimeBaseUnitEnum.values()) {

            JRadioButton radio =
                BasicComponentFactory.createRadioButton(modeModel, baseUnit,
                    Resources.getString(TimeBaseUnitEnum.class, baseUnit.getKey()));
            baseUnitButtons[index++] = radio;

            // add radio button
            builder.append(radio);
        }

        // add the specific components ...
        addSpecificComponents(builder);

        // add a gap
        builder.appendRow("3dlu");
        builder.nextLine();

        // prepare the logger area
        loggerArea = new JTextArea(10, 45);
        loggerArea.setFont(UIManager.getDefaults().getFont("Label.font"));

        JScrollPane scrollLog = new JScrollPane();
        scrollLog.getViewport().add(loggerArea);

        builder.appendRow("fill:100dlu:grow");
        builder.nextLine();
        builder.append(scrollLog, 11);

        getAccessoryBeanModel().addPropertyChangeListener(AccessoryModel.PROPERTYNAME_DCC_ADDRESS,
            new PropertyChangeListener() {

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    LOGGER.debug("DCC address has changed: {}", getAccessoryBeanModel().getDccAddress());
                    triggerValidation();
                }
            });
        getAccessoryBeanModel().addPropertyChangeListener(AccessoryModel.PROPERTYNAME_ACCESSORY_ADDRESSING,
            new PropertyChangeListener() {

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    LOGGER.debug("Accessory addressing has changed: {}", getAccessoryBeanModel()
                        .getAccessoryAddressing());
                    // triggerValidation();
                    Integer dccAddress = getAccessoryBeanModel().getDccAddress();
                    if (dccAddress != null) {
                        // force update of direct address textfield
                        getAccessoryBeanModel().setDccAddress(null);

                        getAccessoryBeanModel().setDccAddress(dccAddress);
                    }
                }
            });
        getAccessoryBeanModel().addPropertyChangeListener(AccessoryModel.PROPERTYNAME_SWITCH_TIME,
            new PropertyChangeListener() {

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    LOGGER.debug("Switch time has changed: {}", getAccessoryBeanModel().getSwitchTime());
                    triggerValidation();
                }
            });

        // add validation for e.g. aspect buttons
        addSpecificValidation();

        LOGGER.info("Create iconfeedback panel.");
        JComponent cvIconPanel = new IconFeedbackPanel(accessoryValidationModel, builder.build());
        DefaultFormBuilder feedbackBuilder = null;
        feedbackBuilder = new DefaultFormBuilder(new FormLayout("p:g"), new LogAreaAwarePanel() {
            private static final long serialVersionUID = 1L;

            @Override
            public void clearLogArea() {
                // clear the log area of the DccAccessoryPanel
                AbstractAccessoryPanel.this.clearLogArea();
            }
        });

        feedbackBuilder.appendRow("fill:p:grow");
        feedbackBuilder.add(cvIconPanel);

        JPanel panel = feedbackBuilder.build();
        triggerValidation();
        return panel;
    }

    protected abstract void addSpecificComponents(final DefaultFormBuilder builder);

    protected abstract void addSpecificValidation();

    public void addAccessoryRequestListener(AccessoryRequestListener listener) {
        listeners.add(listener);
    }

    protected List<AccessoryRequestListener> getAccessoryRequestListeners() {
        return listeners;
    }

    protected void sendRequest(
        AddressData dccAddress, Integer aspect, Integer switchTime, TimeBaseUnitEnum timeBaseUnit,
        TimingControlEnum timingControl) {
        disableInputElements();

        // send to bidib
        for (AccessoryRequestListener listener : getAccessoryRequestListeners()) {
            listener.sendRequest(this, dccAddress, aspect, switchTime, timeBaseUnit, timingControl);
        }
    }

    @Override
    public void signalAcknowledgeChanged(AccessoryAcknowledge acknowledge) {

        if (!isActive()) {
            LOGGER.info("Do not process result because this is not the active tab.");
            return;
        }

        LOGGER.info("Accessory acknowledge has changed: {}", acknowledge);
        if (acknowledge != null) {
            addLogText("Received acknowledge: {}", acknowledge);

            enableInputElements();
        }

    }

    @Override
    public void setActive(boolean active) {
        LOGGER.info("Set the active flag: {}", active);
        this.activeTab = active;
    }

    @Override
    public boolean isActive() {
        return activeTab;
    }

    protected void disableInputElements() {
    }

    protected void enableInputElements() {
    }

    public void clearLogArea() {
        if (loggerArea != null) {
            loggerArea.setText(null);
        }
    }

    public void addLogText(String logLine, Object... args) {
        LOGGER.info("Add text to loggerArea, logLine: {}, args: {}", logLine, args);

        if (args != null) {
            logLine = MessageFormatter.arrayFormat(logLine, args).getMessage();
        }
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SSS - ");
        final StringBuffer message = new StringBuffer(sdf.format(new Date()));
        message.append(logLine).append(NEWLINE);

        if (SwingUtilities.isEventDispatchThread()) {

            loggerArea.append(message.toString());
            loggerArea.setCaretPosition(loggerArea.getDocument().getLength());
        }
        else {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    loggerArea.append(message.toString());
                    loggerArea.setCaretPosition(loggerArea.getDocument().getLength());
                }
            });
        }
    }

    private static final int MIN_ADDRESS = 0;

    protected abstract int getMaxAddress();

    private static final int MIN_SWITCHTIME = 1;

    private static final int MAX_SWITCHTIME = 127;

    private ValidationResult validate() {
        PropertyValidationSupport support = new PropertyValidationI18NSupport(getAccessoryBeanModel(), "validation");

        // only addresses between 1 and maxAddress are valid
        if (getAccessoryBeanModel().getDccAddress() == null) {
            support.addError("address_key", "not_empty");
        }
        else if (getAccessoryBeanModel().getDccAddress().intValue() < MIN_ADDRESS
            || getAccessoryBeanModel().getDccAddress().intValue() > getMaxAddress()) {
            support.addError("address_key", "invalid_value;min=" + MIN_ADDRESS + ",max=" + getMaxAddress());
        }

        // switch time is valid from 1..127
        if (getAccessoryBeanModel().getSwitchTime() == null) {
            support.addError("switchtime_key", "not_empty");
        }
        else if (getAccessoryBeanModel().getSwitchTime().intValue() < MIN_SWITCHTIME
            || getAccessoryBeanModel().getSwitchTime().intValue() > MAX_SWITCHTIME) {
            support.addError("switchtime_key", "invalid_value;min=" + MIN_SWITCHTIME + ",max=" + MAX_SWITCHTIME);
        }

        ValidationResult validationResult = support.getResult();
        LOGGER.info("Prepared validationResult: {}", validationResult);
        return validationResult;
    }

    protected abstract void validateSpecificPanel(PropertyValidationSupport support);

    protected void triggerValidation() {
        ValidationResult validationResult = validate();
        accessoryValidationModel.setResult(validationResult);
    }
}
