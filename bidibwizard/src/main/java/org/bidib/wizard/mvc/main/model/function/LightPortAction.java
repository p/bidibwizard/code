package org.bidib.wizard.mvc.main.model.function;

import org.bidib.jbidibc.exchange.lcmacro.LcMacroPointType;
import org.bidib.jbidibc.exchange.lcmacro.LightPortActionType;
import org.bidib.jbidibc.exchange.lcmacro.LightPortPoint;
import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.comm.LightPortStatus;
import org.bidib.wizard.mvc.main.model.LightPort;

public class LightPortAction extends PortAction<LightPortStatus, LightPort> {
    public LightPortAction() {
        this(LightPortStatus.ON);
    }

    public LightPortAction(LightPort port, int delay) {
        this(LightPortStatus.ON, port, delay);
    }

    public LightPortAction(LightPortStatus action) {
        this(action, null, 0);
    }

    public LightPortAction(LightPortStatus action, LightPort port, int delay) {
        super(action, KEY_LIGHT, port, delay);
    }

    public String getDebugString() {
        int id = 0;

        if (getPort() != null) {
            id = getPort().getId();
        }
        return "@" + getDelay() + " L-Port:" + String.format("%02d", id) + "->" + getAction().name().toLowerCase();
    }

    @Override
    public LcMacroPointType toLcMacroPoint() {
        LightPortPoint lightPortPoint = new LightPortPoint();
        lightPortPoint.setDelay(getDelay());
        lightPortPoint.setLightPortActionType(LightPortActionType.fromValue(getAction().name()));
        lightPortPoint.setOutputNumber(getPort().getId());
        return lightPortPoint;
    }

    public static class LightPortActionBuilder extends LightPortActionBuilderBase<LightPortActionBuilder> {
        public static LightPortActionBuilder lightPortAction() {
            return new LightPortActionBuilder();
        }

        public LightPortActionBuilder() {
            super(new LightPortAction());
        }

        public LightPortAction build() {
            return getInstance();
        }
    }

    static class LightPortActionBuilderBase<GeneratorT extends LightPortActionBuilderBase<GeneratorT>> {
        private LightPortAction instance;

        protected LightPortActionBuilderBase(LightPortAction aInstance) {
            instance = aInstance;
        }

        protected LightPortAction getInstance() {
            return instance;
        }

        @SuppressWarnings("unchecked")
        public GeneratorT withDelay(int delay) {
            instance.setDelay(delay);

            return (GeneratorT) this;
        }

        @SuppressWarnings("unchecked")
        public GeneratorT withPort(LightPort port) {
            instance.setPort(port);

            return (GeneratorT) this;
        }

        @SuppressWarnings("unchecked")
        public GeneratorT withAction(BidibStatus action) {
            instance.setAction((LightPortStatus) action);

            return (GeneratorT) this;
        }
    }
}
