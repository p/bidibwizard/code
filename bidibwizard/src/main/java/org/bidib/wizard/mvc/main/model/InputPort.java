package org.bidib.wizard.mvc.main.model;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.util.LinkedHashMap;
import java.util.Map;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.enumeration.InputPortEnum;
import org.bidib.jbidibc.core.enumeration.IoBehaviourInputEnum;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.port.BytePortConfigValue;
import org.bidib.jbidibc.core.port.PortConfigValue;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.wizard.comm.InputPortStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InputPort extends Port<InputPortStatus> implements ConfigurablePort<InputPortStatus>, TicksAware {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(InputPort.class);

    static {
        try {
            // Q: why is this needed? A: export of beans with XMLDecoder
            PropertyDescriptor[] descriptor = Introspector.getBeanInfo(InputPort.class).getPropertyDescriptors();

            for (int i = 0; i < descriptor.length; i++) {
                PropertyDescriptor propertyDescriptor = descriptor[i];
                if (propertyDescriptor.getName().equals("value")) {
                    propertyDescriptor.setValue("transient", Boolean.TRUE);
                }
                else if (propertyDescriptor.getName().equals("status")) {
                    propertyDescriptor.setValue("transient", Boolean.TRUE);
                }
                else if (propertyDescriptor.getName().equals("hasInputPortConfig")) {
                    propertyDescriptor.setValue("transient", Boolean.TRUE);
                }
            }
        }
        catch (IntrospectionException e) {
            throw new RuntimeException(e);
        }
    }

    public static final InputPort NONE = new InputPort.Builder(-1).setLabel("<none>").build();

    private IoBehaviourInputEnum inputBehaviour = IoBehaviourInputEnum.UNKNOWN;

    private int switchOffTime;

    private transient boolean hasInputPortConfig;

    public InputPort() {
        super(null);
        setStatus(InputPortStatus.OFF);
    }

    public InputPort(GenericPort genericPort) {
        super(genericPort);
    }

    private InputPort(Builder builder) {
        super(null);
        setId(builder.id);
        setLabel(builder.label);
    }

    @Override
    protected LcOutputType getPortType() {
        return LcOutputType.INPUTPORT;
    }

    @Override
    protected InputPortStatus internalGetStatus() {
        InputPortStatus portStat = InputPortStatus.valueOf(InputPortEnum.valueOf(genericPort.getPortStatus()));
        LOGGER.debug("Prepared internal status: {}", portStat);
        return portStat;
    }

    /**
     * @return the ioBehaviour
     */
    public IoBehaviourInputEnum getInputBehaviour() {
        if (genericPort != null) {
            if (isEnabled()) {
                Number inputBehaviour = genericPort.getPortConfigValue(BidibLibrary.BIDIB_PCFG_INPUT_CTRL);
                if (inputBehaviour != null) {
                    try {
                        this.inputBehaviour = IoBehaviourInputEnum.valueOf(inputBehaviour.byteValue());
                        LOGGER.info("Prepared inputBehaviour from value: {}, inputBehaviour: {}",
                            ByteUtils.byteToHex(inputBehaviour.byteValue()), this.inputBehaviour);
                    }
                    catch (IllegalArgumentException ex) {
                        LOGGER.warn("Get inputBehaviour failed on port: {}, inputBehaviour: {}",
                            genericPort.getPortNumber(), inputBehaviour);
                        this.inputBehaviour = IoBehaviourInputEnum.UNKNOWN;
                    }
                }
                else {
                    LOGGER.warn("No value received from generic port for BIDIB_PCFG_INPUT_CTRL!");
                    this.inputBehaviour = IoBehaviourInputEnum.UNKNOWN;
                }
            }
            else {
                LOGGER.info("The current port is not enabled, return UNKNOWN.");
                this.inputBehaviour = IoBehaviourInputEnum.UNKNOWN;
            }
        }
        return inputBehaviour;
    }

    /**
     * @param inputBehaviour
     *            the inputBehaviour to set
     */
    public void setInputBehaviour(IoBehaviourInputEnum inputBehaviour) {
        LOGGER.info("Set inputBehaviour: {}", inputBehaviour);
        this.inputBehaviour = inputBehaviour;
        if (genericPort != null) {
            genericPort.setPortConfigValue(BidibLibrary.BIDIB_PCFG_INPUT_CTRL,
                new BytePortConfigValue(inputBehaviour.getType()));
        }
    }

    @Override
    public int getTicks() {
        return getSwitchOffTime();
    }

    /**
     * @return the switchOffTime
     */
    public int getSwitchOffTime() {
        if (genericPort != null) {
            Number switchOffTime = genericPort.getPortConfigValue(BidibLibrary.BIDIB_PCFG_TICKS);
            if (switchOffTime != null) {
                this.switchOffTime = ByteUtils.getInt(switchOffTime.byteValue());
            }
            else {
                LOGGER.warn("No value received from generic for BIDIB_PCFG_TICKS!");
            }
        }
        return switchOffTime;
    }

    /**
     * @param switchOffTime
     *            the switchOffTime to set
     */
    public void setSwitchOffTime(int switchOffTime) {
        LOGGER.info("Set switchOff time: {}", switchOffTime);
        this.switchOffTime = switchOffTime;
        if (genericPort != null) {
            genericPort.setPortConfigValue(BidibLibrary.BIDIB_PCFG_TICKS,
                new BytePortConfigValue(ByteUtils.getLowByte(switchOffTime)));
        }
    }

    @Override
    public byte[] getPortConfig() {
        return new byte[] { 0, 0, 0, 0 };
    }

    @Override
    public void setPortConfigX(Map<Byte, PortConfigValue<?>> portConfig) {

        LOGGER.info("Set the port config for the input port.");

        Number inputBehaviour = getPortConfigValue(BidibLibrary.BIDIB_PCFG_INPUT_CTRL, portConfig);
        if (inputBehaviour != null) {
            setInputBehaviour(IoBehaviourInputEnum.valueOf(inputBehaviour.byteValue()));
            setHasInputPortConfig(true);
        }
        else {
            LOGGER.warn("No value received for BIDIB_PCFG_IO_CTRL!");
        }

        Number switchOffTime = getPortConfigValue(BidibLibrary.BIDIB_PCFG_TICKS, portConfig);
        if (switchOffTime != null) {
            setSwitchOffTime(ByteUtils.getInt(switchOffTime.byteValue()));
            setHasInputPortConfig(true);
        }
        else {
            LOGGER.warn("No value received for BIDIB_PCFG_TICKS!");
        }

        // call the super class
        super.setPortConfigX(portConfig);
    }

    @Override
    public Map<Byte, PortConfigValue<?>> getPortConfigX() {

        if (genericPort != null) {
            return genericPort.getPortConfig();
        }

        Map<Byte, PortConfigValue<?>> portConfigX = new LinkedHashMap<Byte, PortConfigValue<?>>();

        // add the ioBehaviour
        if (isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_INPUT_CTRL)) {

            portConfigX.put(BidibLibrary.BIDIB_PCFG_INPUT_CTRL,
                new BytePortConfigValue(ByteUtils.getLowByte(inputBehaviour.getType())));
        }

        // add the ticks
        if (isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_TICKS)) {
            portConfigX.put(BidibLibrary.BIDIB_PCFG_TICKS,
                new BytePortConfigValue(ByteUtils.getLowByte(switchOffTime)));
        }
        return portConfigX;
    }

    /**
     * @return the port has input port config available
     */
    public boolean isHasInputPortConfig() {
        if (genericPort != null) {
            // return genericPort.isHasPortConfig();
            hasInputPortConfig =
                genericPort.isHasPortConfig(BidibLibrary.BIDIB_PCFG_INPUT_CTRL)
                    || genericPort.isHasPortConfig(BidibLibrary.BIDIB_PCFG_TICKS);
            LOGGER.info("The generic port has input port config: {}", hasInputPortConfig);
        }

        return hasInputPortConfig;
    }

    /**
     * @param hasInputPortConfig
     *            the input port config available flag
     */
    public void setHasInputPortConfig(boolean hasInputPortConfig) {
        LOGGER.info("The port has input port config available: {}, port: {}", hasInputPortConfig, this);
        this.hasInputPortConfig = hasInputPortConfig;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof InputPort) {
            return ((InputPort) obj).getId() == getId();
        }
        return false;
    }

    @Override
    public int hashCode() {
        return getId();
    }

    public static class Builder {
        private final int id;

        private String label;

        public Builder(int id) {
            this.id = id;
        }

        public Builder setLabel(String label) {
            this.label = label;
            return this;
        }

        public InputPort build() {
            return new InputPort(this);
        }
    }
}
