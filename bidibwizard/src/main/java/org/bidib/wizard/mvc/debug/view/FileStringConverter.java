package org.bidib.wizard.mvc.debug.view;

import java.io.File;

import com.jgoodies.binding.value.BindingConverter;

public class FileStringConverter implements BindingConverter<File, String> {

    @Override
    public String targetValue(File sourceValue) {
        if (sourceValue != null) {
            return sourceValue.getPath();
        }
        return null;
    }

    @Override
    public File sourceValue(String targetValue) {
        return null;
    }

}
