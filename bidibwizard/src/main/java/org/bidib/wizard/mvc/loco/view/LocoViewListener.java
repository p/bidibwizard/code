package org.bidib.wizard.mvc.loco.view;

public interface LocoViewListener {

    void stop();

    void emergencyStop();
}
