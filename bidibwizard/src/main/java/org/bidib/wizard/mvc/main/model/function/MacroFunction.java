package org.bidib.wizard.mvc.main.model.function;

import org.bidib.jbidibc.exchange.lcmacro.LcMacroPointType;
import org.bidib.jbidibc.exchange.lcmacro.MacroActionPoint;
import org.bidib.jbidibc.exchange.lcmacro.MacroActionType;
import org.bidib.jbidibc.exchange.lcmacro.MacroOperationType;
import org.bidib.wizard.comm.MacroStatus;

public class MacroFunction extends SystemFunction<MacroStatus> {
    private int macroId;

    public MacroFunction() {
        this(MacroStatus.START);
    }

    public MacroFunction(MacroStatus action) {
        this(action, 0);
    }

    public MacroFunction(MacroStatus action, int macroId) {
        super(action, KEY_MACRO);
        this.macroId = macroId;
    }

    public int getMacroId() {
        return macroId;
    }

    public void setMacroId(int macroId) {
        this.macroId = macroId;
    }

    public String getDebugString() {
        return getAction().name().substring(0, 1) + getAction().name().substring(1).toLowerCase() + "Macro " + macroId;
    }

    @Override
    public LcMacroPointType toLcMacroPoint() {
        MacroActionPoint macroActionPoint = new MacroActionPoint();
        MacroActionType macroActionType = new MacroActionType();
        macroActionType.setMacroNumber(getMacroId());
        macroActionType.setOperation(MacroOperationType.fromValue(getAction().name()));
        macroActionPoint.setMacroActionType(macroActionType);
        return macroActionPoint;
    }
}
