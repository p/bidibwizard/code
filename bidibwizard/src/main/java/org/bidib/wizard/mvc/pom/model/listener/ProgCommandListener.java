package org.bidib.wizard.mvc.pom.model.listener;

import org.bidib.jbidibc.core.enumeration.PomProgState;

public interface ProgCommandListener {

    /**
     * The POM programming command has finished.
     * 
     * @param pomProgState
     *            the new programming state
     */
    void progPomFinished(PomProgState pomProgState);
}
