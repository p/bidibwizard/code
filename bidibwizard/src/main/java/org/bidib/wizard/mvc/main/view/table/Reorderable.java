package org.bidib.wizard.mvc.main.view.table;

public interface Reorderable {
    public void reorder(int fromIndex, int toIndex, int rowCount);
}
