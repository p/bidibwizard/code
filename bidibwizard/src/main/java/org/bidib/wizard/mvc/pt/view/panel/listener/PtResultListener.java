package org.bidib.wizard.mvc.pt.view.panel.listener;

import org.bidib.jbidibc.core.enumeration.CommandStationProgState;

public interface PtResultListener {

    /**
     * Sets the listener active or inactive.
     * 
     * @param active
     *            the active flag
     */
    void setActive(boolean active);

    /**
     * @return listener is active
     */
    boolean isActive();

    /**
     * Add a new line to the logger area.
     * 
     * @param logLine
     *            the message to log. Use {} as placeholders for args.
     * @param args
     *            the args
     */
    void addLogText(String logLine, Object... args);

    /**
     * Signals that the command station prog state has changed.
     * 
     * @param commandStationProgState
     *            the new command station prog state
     */
    void signalCommandStationProgStateChanged(CommandStationProgState commandStationProgState);
}
