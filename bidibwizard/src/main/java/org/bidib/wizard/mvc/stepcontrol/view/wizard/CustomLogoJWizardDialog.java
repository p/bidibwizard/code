package org.bidib.wizard.mvc.stepcontrol.view.wizard;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;

import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.bidib.wizard.mvc.common.view.wizard.CancelAction;
import org.bidib.wizard.mvc.common.view.wizard.DefaultJWizardComponents;
import org.bidib.wizard.mvc.common.view.wizard.FinishAction;
import org.bidib.wizard.mvc.common.view.wizard.SimpleButtonPanel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.debug.FormDebugPanel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.FormLayout;

public class CustomLogoJWizardDialog extends JDialog {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomLogoJWizardDialog.class);

    DefaultJWizardComponents wizardComponents;

    JPanel buttonPanel;

    JLabel statusLabel = new JLabel();

    ImageIcon[] logos;

    /**
     * Creates a new CustomLogoJWizardDialog without parent.
     * 
     * @param logos
     *            one or more ImageIcon objects to be shown within the wizard
     */
    public CustomLogoJWizardDialog(ImageIcon... logos) {
        this.logos = logos;
        wizardComponents = new DefaultJWizardComponents();
        init();
    }

    /**
     * Creates a new CustomLogoJWizardDialog with a frame as owner.
     * 
     * @param owner
     *            Owner of this wizard dialog
     * @param logos
     *            an ImageIcon object to be shown within the wizard
     * @param modal
     *            true, if the wizard should be shown modal; false otherwise
     */
    public CustomLogoJWizardDialog(Frame owner, ImageIcon[] logos, boolean modal) {
        super(owner, modal);
        this.logos = logos;
        wizardComponents = new DefaultJWizardComponents();
        init();
    }

    private void init() {

        JPanel logoPanel = new JPanel();

        StringBuilder sb = new StringBuilder("<html>");

        sb.append("<table>");

        for (ImageIcon logo : logos) {
            String fileString;
            if (logo.toString().indexOf("file:") < 0 && logo.toString().indexOf("http:") < 0) {
                fileString = "file:///" + System.getProperty("user.dir") + "/" + logo.toString();
                fileString = fileString.replaceAll("\\\\", "/");
            }
            else {
                fileString = logo.toString();
            }
            sb.append("<tr><td>");

            sb.append("<img src='").append(fileString).append("'>");

            sb.append("</td></tr>");
        }
        sb.append("</table>");
        sb.append("</html>");

        logoPanel.add(new JLabel(sb.toString()), BorderLayout.CENTER);

        // prepare the form
        DefaultFormBuilder builder = null;
        boolean debugDialog = false;
        if (debugDialog) {
            JPanel panel = new FormDebugPanel();
            builder = new DefaultFormBuilder(new FormLayout("p, 3dlu, p:g"), panel);
        }
        else {
            JPanel panel = new JPanel(new BorderLayout());
            builder = new DefaultFormBuilder(new FormLayout("p, 3dlu, p:g"), panel);
        }
        builder.border(Borders.TABBED_DIALOG);

        builder.appendRow("fill:150dlu:g");

        builder.append(logoPanel);
        builder.append(wizardComponents.getWizardPanelsContainer());

        buttonPanel = new SimpleButtonPanel(wizardComponents);

        try {
            builder.appendRow("5dlu");
            builder.nextLine();
            builder.addSeparator(null);
            builder.appendRow("20dlu");
            builder.nextLine(1);

            builder.append(buttonPanel, 3);
        }
        catch (Exception ex) {
            LOGGER.warn("Add panels to form failed.", ex);
        }
        JPanel content = builder.build();
        this.getContentPane().add(content);

        wizardComponents.setFinishAction(new FinishAction(
            wizardComponents) {
            public void performAction() {
                dispose();
            }
        });
        wizardComponents.setCancelAction(new CancelAction(
            wizardComponents) {
            public void performAction() {
                dispose();
            }
        });

        Dimension dim = getPreferredSize();
        dim.height += 40;
        setPreferredSize(dim);
        setMinimumSize(dim);

        pack();
    }

    /**
     * Returns this wizards components.
     * 
     * @return DefaultJWizardComponents
     */
    public DefaultJWizardComponents getWizardComponents() {
        return wizardComponents;
    }

    /**
     * Sets this wizards components.
     * 
     * @param aWizardComponents
     *            DefaultJWizardComponents
     */
    public void setWizardComponents(DefaultJWizardComponents aWizardComponents) {
        wizardComponents = aWizardComponents;
    }

    /** Shows this dialog. */
    public void setVisible(boolean visible) {
        wizardComponents.updateComponents();
        super.setVisible(visible);
    }

}
