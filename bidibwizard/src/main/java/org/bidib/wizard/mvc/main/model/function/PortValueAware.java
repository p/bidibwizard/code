package org.bidib.wizard.mvc.main.model.function;

public interface PortValueAware {

    /**
     * @param value
     *            the port value to set
     */
    void setValue(int value);

    /**
     * @return the port value
     */
    int getValue();
}
