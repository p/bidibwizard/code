package org.bidib.wizard.mvc.pt.model;

import java.util.LinkedList;
import java.util.List;

import javax.swing.SwingUtilities;

import org.bidib.jbidibc.core.enumeration.CommandStationProgState;
import org.bidib.jbidibc.core.enumeration.CommandStationState;
import org.bidib.wizard.mvc.pt.model.listener.ConfigVariableListener;
import org.bidib.wizard.mvc.pt.model.listener.ProgCommandListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.beans.Model;

public class PtProgrammerModel extends Model {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(PtProgrammerModel.class);

    public static final String PROPERTYNAME_COMMANDSTATIONPROGSTATE = "commandStationProgState";

    public static final String PROPERTYNAME_CVNUMBER = "cvNumber";

    public static final String PROPERTYNAME_CVVALUE = "cvValue";

    private final List<ConfigVariableListener> listeners = new LinkedList<ConfigVariableListener>();

    private final List<ProgCommandListener> progCommandListeners = new LinkedList<ProgCommandListener>();

    private int cvNumber = 1;

    private Integer cvValue;

    private CommandStationState commandStationState;

    private CommandStationProgState commandStationProgState;

    public void addConfigVariableListener(ConfigVariableListener l) {
        listeners.add(l);
    }

    public void removeConfigVariableListener(ConfigVariableListener l) {
        listeners.remove(l);
    }

    public void addProgCommandListener(ProgCommandListener l) {
        progCommandListeners.add(l);
    }

    public void removeProgCommandListener(ProgCommandListener l) {
        progCommandListeners.remove(l);
    }

    /**
     * @return the CV number
     */
    public int getCvNumber() {
        return cvNumber;
    }

    /**
     * @param cvNumber
     *            the CV number to set
     */
    public void setCvNumber(int cvNumber) {
        int oldNumber = this.cvNumber;
        this.cvNumber = cvNumber;
        firePropertyChange(PROPERTYNAME_CVNUMBER, oldNumber, cvNumber);
    }

    public Integer getCvValue() {
        return cvValue;
    }

    public void setCvValue(Integer value) {
        LOGGER.debug("Set the CV value: {}", value);
        // if (this.cvValue != value) {
        Integer oldValue = cvValue;
        this.cvValue = value;
        firePropertyChange(PROPERTYNAME_CVVALUE, oldValue, cvValue);
    }

    public void clearCvValue() {
        LOGGER.debug("Clear the CV value.");
        Integer oldValue = cvValue;
        cvValue = null;
        firePropertyChange(PROPERTYNAME_CVVALUE, oldValue, cvValue);
    }

    /**
     * @return the commandStationState
     */
    public CommandStationState getCommandStationState() {
        return commandStationState;
    }

    /**
     * @param commandStationState
     *            the commandStationState to set
     */
    public void setCommandStationState(CommandStationState commandStationState) {
        LOGGER.info("Set the new command station state: {}", commandStationState);
        LOGGER.info("The commandStationState has been changed: {}", commandStationState);
        this.commandStationState = commandStationState;
        fireCommandStationStateChanged(commandStationState);
    }

    /**
     * @return the commandStationProgState
     */
    public CommandStationProgState getCommandStationProgState() {
        return commandStationProgState;
    }

    /**
     * @param commandStationProgState
     *            the commandStationProgState to set
     */
    public void setCommandStationProgState(CommandStationProgState commandStationProgState) {
        LOGGER.info("Set the new command station prog state: {}", commandStationProgState);

        // if (commandStationProgState == null) {
        // LOGGER.warn("No state available.");
        // }

        CommandStationProgState oldCommandStationProgState = this.commandStationProgState;

        this.commandStationProgState = commandStationProgState;

        firePropertyChange(PROPERTYNAME_COMMANDSTATIONPROGSTATE, oldCommandStationProgState, commandStationProgState);

        fireCommandStationProgStateChanged(commandStationProgState);
    }

    public void updateCommandStationProgResult(
        final CommandStationProgState commandStationProgState, int remainingTime, int cvNumber, final int cvValue) {
        LOGGER.info("update the command station result: {}, remainingTime: {}, cvNumber: {}, cvValue: {}",
            commandStationProgState, remainingTime, cvNumber, cvValue);

        if (SwingUtilities.isEventDispatchThread()) {
            notifyCommandStationProgResult(commandStationProgState, cvValue);
        }
        else {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    notifyCommandStationProgResult(commandStationProgState, cvValue);
                }
            });
        }
    }

    private void notifyCommandStationProgResult(final CommandStationProgState commandStationProgState, int cvValue) {

        setCommandStationProgState(commandStationProgState);

        if (CommandStationProgState.PROG_OKAY.equals(commandStationProgState)) {
            setCvValue(cvValue);
        }

        // TODO signal that all values returned from the new state are set ...

        fireProgCommandFinished(commandStationProgState);
    }

    private void fireProgCommandFinished(CommandStationProgState commandStationProgState) {
        for (ProgCommandListener l : progCommandListeners) {
            l.progCommandStationFinished(commandStationProgState);
        }
    }

    private void fireCommandStationStateChanged(CommandStationState commandStationState) {
        for (ConfigVariableListener l : listeners) {
            l.commandStationStateChanged(commandStationState);
        }
    }

    private void fireCommandStationProgStateChanged(CommandStationProgState commandStationProgState) {
        for (ConfigVariableListener l : listeners) {
            l.commandStationProgStateChanged(commandStationProgState);
        }
    }
}
