package org.bidib.wizard.mvc.main.model;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.SwingUtilities;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.collections4.Predicate;
import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.enumeration.CommandStationState;
import org.bidib.jbidibc.core.enumeration.IdentifyState;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.LightPortEnum;
import org.bidib.jbidibc.core.enumeration.SysErrorEnum;
import org.bidib.jbidibc.core.node.ConfigurationVariable;
import org.bidib.jbidibc.core.node.RootNode;
import org.bidib.jbidibc.core.port.PortConfigValue;
import org.bidib.jbidibc.core.port.PortMapUtils;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.core.utils.NodeUtils;
import org.bidib.jbidibc.core.utils.ProductUtils;
import org.bidib.jbidibc.exchange.vendorcv.VendorCvData;
import org.bidib.wizard.comm.AnalogPortStatus;
import org.bidib.wizard.comm.BacklightPortStatus;
import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.comm.BoosterStatus;
import org.bidib.wizard.comm.InputPortStatus;
import org.bidib.wizard.comm.LightPortStatus;
import org.bidib.wizard.comm.MotorPortStatus;
import org.bidib.wizard.comm.ServoPortStatus;
import org.bidib.wizard.comm.SoundPortStatus;
import org.bidib.wizard.comm.SwitchPortStatus;
import org.bidib.wizard.labels.AnalogPortLabelFactory;
import org.bidib.wizard.labels.BacklightPortLabelFactory;
import org.bidib.wizard.labels.InputPortLabelFactory;
import org.bidib.wizard.labels.LightPortLabelFactory;
import org.bidib.wizard.labels.PortLabelUtils;
import org.bidib.wizard.labels.SoundPortLabelFactory;
import org.bidib.wizard.labels.SwitchPairPortLabelFactory;
import org.bidib.wizard.labels.SwitchPortLabelFactory;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.bidib.wizard.mvc.main.model.listener.AccessoryListListener;
import org.bidib.wizard.mvc.main.model.listener.BoosterStatusListener;
import org.bidib.wizard.mvc.main.model.listener.CommandStationStatusListener;
import org.bidib.wizard.mvc.main.model.listener.CvDefinitionListener;
import org.bidib.wizard.mvc.main.model.listener.MacroListListener;
import org.bidib.wizard.mvc.main.model.listener.NodeListListener;
import org.bidib.wizard.mvc.main.model.listener.NodeSelectionListener;
import org.bidib.wizard.mvc.main.model.listener.PortListListener;
import org.bidib.wizard.mvc.main.model.listener.PortListener;
import org.bidib.wizard.mvc.main.model.listener.PortValueListener;
import org.bidib.wizard.utils.PortListUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.common.collect.ArrayListModel;

public class MainModel implements PortsProvider {
    private static final Logger LOGGER = LoggerFactory.getLogger(MainModel.class);

    private Collection<AccessoryListListener> accessoryListListeners = new LinkedList<>();

    private Collection<PortListListener> analogPortListListeners = new LinkedList<>();

    private Collection<PortListener<AnalogPortStatus>> analogPortListeners = new LinkedList<>();

    private Collection<BoosterStatusListener> boosterStatusListeners = new LinkedList<>();

    private Collection<CommandStationStatusListener> commandStationStatusListeners = new LinkedList<>();

    private Collection<PortListener<InputPortStatus>> inputPortListeners = new LinkedList<>();

    private Collection<PortListListener> inputPortListListeners = new LinkedList<>();

    private Collection<PortListListener> lightPortListListeners = new LinkedList<>();

    private Collection<PortListener<LightPortStatus>> lightPortListeners = new LinkedList<>();

    private Collection<PortListListener> backlightPortListListeners = new LinkedList<>();

    private Collection<PortValueListener<BacklightPortStatus>> backlightPortValueListeners = new LinkedList<>();

    private Collection<PortListener<BacklightPortStatus>> backlightPortListeners = new LinkedList<>();

    private Collection<MacroListListener> macroListListeners = new LinkedList<>();

    private Collection<NodeListListener> nodeListListeners = new LinkedList<>();

    private Collection<NodeSelectionListener> nodeSelectionListeners = new LinkedList<>();

    private Collection<PortValueListener<ServoPortStatus>> servoPortValueListeners = new LinkedList<>();

    private Collection<PortValueListener<MotorPortStatus>> motorPortValueListeners = new LinkedList<>();

    private Collection<PortListListener> soundPortListListeners = new LinkedList<>();

    private Collection<PortListener<SoundPortStatus>> soundPortListeners = new LinkedList<>();

    private Collection<PortListListener> switchPortListListeners = new LinkedList<>();

    private Collection<PortListListener> switchPairPortListListeners = new LinkedList<>();

    private Collection<PortListener<SwitchPortStatus>> switchPortListeners = new LinkedList<>();

    private Collection<PortListener<SwitchPortStatus>> switchPairPortListeners = new LinkedList<>();

    private Collection<CvDefinitionListener> cvDefinitionListeners = new LinkedList<>();

    private ArrayListModel<Accessory> accessories = new ArrayListModel<>();

    private List<AnalogPort> analogPorts = new LinkedList<>();

    private List<ConfigurationVariable> configurationVariables = new LinkedList<>();

    private int boosterCurrent;

    private long lastCurrentUpdate;

    private int boosterMaximumCurrent;

    private BoosterStatus boosterStatus;

    private CommandStationState commandStationState;

    private int boosterTemperature;

    private int boosterVoltage;

    private List<Flag> flags = new LinkedList<>();

    private List<InputPort> inputPorts = new LinkedList<>();

    private List<LightPort> lightPorts = new LinkedList<>();

    private List<BacklightPort> backlightPorts = new LinkedList<>();

    private List<Macro> macros = new LinkedList<>();

    private Set<Node> nodes = new LinkedHashSet<>();

    private List<SoundPort> soundPorts = new LinkedList<>();

    private List<SwitchPort> switchPorts = new LinkedList<>();

    private List<SwitchPairPort> switchPairPorts = new LinkedList<>();

    private Accessory selectedAccessory;

    private Macro selectedMacro;

    private Node selectedNode;

    private Object nodeLock = new Object();

    private StatusModel statusModel = new StatusModel();

    private Object vendorCVLock = new Object();

    private Object configVarsLock = new Object();

    private PropertyChangeListener macroPendingChangesListener;

    private PropertyChangeListener accessoryPendingChangesListener;

    private AtomicBoolean initialLoadFinished = new AtomicBoolean(false);

    public MainModel() {

        macroPendingChangesListener = new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                LOGGER.info("The pending changes flag of a macro has been changed.");

                // force refresh of macro list
                fireMacroPendingChangesChanged();
            }
        };

        accessoryPendingChangesListener = new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                LOGGER.info("The property of an accessory has been changed: {}", evt.getPropertyName());
                switch (evt.getPropertyName()) {
                    case Accessory.PROPERTY_PENDING_CHANGES:
                        // force refresh of accessory list
                        fireAccessoryPendingChangesChanged();
                        break;
                    case Accessory.PROPERTY_TOTAL_ASPECTS:
                        int accessoryId = (evt.getNewValue() != null ? (int) evt.getNewValue() : -1);
                        LOGGER.info("The total aspects of an accessory have been changed, accessory id: {}",
                            accessoryId);

                        // perform with accessory id
                        fireAccessoryChanged(accessoryId);
                        break;
                    default:
                        break;
                }

            }
        };
    }

    public void addAccessoryListListener(AccessoryListListener l) {
        accessoryListListeners.add(l);
    }

    public void addAnalogPortListListener(PortListListener l) {
        analogPortListListeners.add(l);
    }

    public void addAnalogPortListener(PortListener<AnalogPortStatus> l) {
        analogPortListeners.add(l);
    }

    public void addBoosterStatusListener(BoosterStatusListener l) {
        boosterStatusListeners.add(l);
    }

    public void addCommandStationStatusListener(CommandStationStatusListener commandStationStatusListener) {
        commandStationStatusListeners.add(commandStationStatusListener);
    }

    public void addInputPortListener(PortListener<InputPortStatus> l) {
        inputPortListeners.add(l);
    }

    public void addInputPortListListener(PortListListener l) {
        inputPortListListeners.add(l);
    }

    public void addLightPortListListener(PortListListener l) {
        lightPortListListeners.add(l);
    }

    public void addBacklightPortListListener(PortListListener l) {
        backlightPortListListeners.add(l);
    }

    public void addBacklightPortValueListener(PortValueListener<BacklightPortStatus> l) {
        backlightPortValueListeners.add(l);
    }

    public void addMacroListListener(MacroListListener l) {
        macroListListeners.add(l);
    }

    // public void addMotorPortListListener(PortListListener l) {
    // motorPortListListeners.add(l);
    // }

    /**
     * Add a node to the list of nodes.
     * 
     * @param node
     *            the new node to add
     */
    public void addNode(Node node) {
        boolean nodeListChanged = false;
        synchronized (nodes) {
            LOGGER.info("Add new node: {}", node);
            if (nodes.add(node)) {
                // fireNodeListChanged();
                nodeListChanged = true;
            }
        }
        if (nodeListChanged) {
            fireNodeListChanged();

            fireNodeListAdded(node);
        }
    }

    /**
     * Remove a node from the list of nodes.
     * 
     * @param node
     *            the node to remove
     */
    public void removeNode(Node node) {
        boolean nodeListChanged = false;

        List<Node> nodesToRemove = null;
        synchronized (nodes) {
            LOGGER.info("Remove node: {}", node);

            // if a hub node is removed then we must remove all children of this hub
            nodesToRemove = new LinkedList<Node>();
            nodesToRemove.add(node);

            if (NodeUtils.hasSubNodesFunctions(node.getUniqueId())) {
                byte[] addr = node.getNode().getAddr();
                LOGGER.info(
                    "The removed node has subnode functions. We must remove all subnodes, too. Address of current node: {}",
                    addr);
                if (addr != null && addr.length > 0) {

                    boolean removedRootNode = Arrays.equals(addr, RootNode.ROOTNODE_ADDR);

                    for (Node currentNode : nodes) {
                        LOGGER.info("Check if we must remove the current node: {}", currentNode);
                        byte[] currentAddr = currentNode.getNode().getAddr();
                        if (removedRootNode) {
                            // this is a subnode
                            LOGGER.info("Found a subnode to be removed: {}", currentNode);
                            nodesToRemove.add(currentNode);
                        }
                        else if (currentAddr.length > addr.length) {
                            // potential subnode
                            if (currentAddr[addr.length - 1] == addr[addr.length - 1]) {
                                // this is a subnode
                                LOGGER.info("Found a subnode to be removed: {}", currentNode);
                                nodesToRemove.add(currentNode);
                            }
                        }
                    }
                }
            }

            if (nodes.removeAll(nodesToRemove)) {
                LOGGER.info("Removed all nodes that must be removed: {}", nodesToRemove);
                nodeListChanged = true;
            }
            else {
                LOGGER.warn("Remove nodes from nodes list failed: {}", nodesToRemove);
            }

            LOGGER.info("Remaining nodes in list: {}", nodes);

            if (nodesToRemove.contains(getSelectedNode())) {
                LOGGER.info("The active node in model was removed: {}", getSelectedNode());
                setSelectedNode(null);
            }
        }
        if (nodeListChanged) {
            fireNodeListChanged();

            if (CollectionUtils.isNotEmpty(nodesToRemove)) {
                for (Node currentNode : nodesToRemove) {
                    fireNodeListRemoved(currentNode);
                }
            }
        }
    }

    public void addNodeListListener(NodeListListener l) {
        nodeListListeners.add(l);
    }

    public void removeNodeListListener(NodeListListener l) {
        nodeListListeners.remove(l);
    }

    public void addNodeSelectionListListener(NodeSelectionListener l) {
        nodeSelectionListeners.add(l);
    }

    public void removeNodeSelectionListener(NodeSelectionListener l) {
        nodeSelectionListeners.remove(l);
    }

    public void addMotorPortValueListener(PortValueListener<MotorPortStatus> l) {
        LOGGER.info("Add motor port value listener: {}", l);
        motorPortValueListeners.add(l);
    }

    public void removeMotorPortValueListener(PortValueListener<MotorPortStatus> l) {
        boolean removed = motorPortValueListeners.remove(l);
        LOGGER.info("Remove motor port value listener: {}, removed: {}", l, removed);
    }

    public void addServoPortValueListener(PortValueListener<ServoPortStatus> l) {
        servoPortValueListeners.add(l);
    }

    public void addSoundPortListListener(PortListListener l) {
        soundPortListListeners.add(l);
    }

    public void removeSoundPortListListener(PortListListener soundPortListListener) {
        soundPortListListeners.remove(soundPortListListener);
    }

    public void addSoundPortListener(PortListener<SoundPortStatus> l) {
        soundPortListeners.add(l);
    }

    public void addSwitchPortListListener(PortListListener l) {
        switchPortListListeners.add(l);
    }

    public void addSwitchPortListener(PortListener<SwitchPortStatus> l) {
        switchPortListeners.add(l);
    }

    public void addSwitchPairPortListListener(PortListListener l) {
        switchPairPortListListeners.add(l);
    }

    public void addSwitchPairPortListener(PortListener<SwitchPortStatus> l) {
        switchPairPortListeners.add(l);
    }

    public void addLightPortListener(PortListener<LightPortStatus> l) {
        lightPortListeners.add(l);
    }

    public void addBacklightPortListener(PortListener<BacklightPortStatus> l) {
        backlightPortListeners.add(l);
    }

    public void addCvDefinitionListener(CvDefinitionListener l) {
        cvDefinitionListeners.add(l);
    }

    private void fireAccessoryPendingChangesChanged() {
        if (SwingUtilities.isEventDispatchThread()) {
            for (AccessoryListListener l : accessoryListListeners) {
                l.pendingChangesChanged();
            }
        }
        else {
            SwingUtilities.invokeLater(new Runnable() {

                @Override
                public void run() {
                    for (AccessoryListListener l : accessoryListListeners) {
                        l.pendingChangesChanged();
                    }
                }
            });
        }
    }

    private void fireAccessoryChanged(final int accessoryId) {

        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                for (AccessoryListListener l : accessoryListListeners) {
                    l.accessoryChanged(accessoryId);
                }
            }
        });
    }

    private void fireAccessoryListChanged() {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                for (AccessoryListListener l : accessoryListListeners) {
                    l.listChanged();
                }
            }
        });
    }

    private void fireAnalogPortListChanged() {
        notifyPortListListeners(analogPortListListeners);
    }

    private void fireBoosterCurrentChanged(int current) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                for (BoosterStatusListener l : boosterStatusListeners) {
                    l.currentChanged(current);
                }
            }
        });
    }

    private void fireBoosterMaximumCurrentChanged(int maximumCurrent) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                for (BoosterStatusListener l : boosterStatusListeners) {
                    l.maximumCurrentChanged(maximumCurrent);
                }
            }
        });
    }

    private void fireBoosterStatusChanged(BoosterStatus status) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                for (BoosterStatusListener l : boosterStatusListeners) {
                    l.stateChanged(status);
                }
            }
        });
    }

    private void fireBoosterTemperatureChanged(int temperature) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                for (BoosterStatusListener l : boosterStatusListeners) {
                    l.temperatureChanged(temperature);
                }
            }
        });
    }

    private void fireBoosterVoltageChanged(int voltage) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                for (BoosterStatusListener l : boosterStatusListeners) {
                    l.voltageChanged(voltage);
                }
            }
        });
    }

    private void fireCommandStationStatusChanged(Node node, CommandStationState status) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                for (CommandStationStatusListener l : commandStationStatusListeners) {
                    l.stateChanged(node, status);
                }
            }
        });
    }

    private void fireFlagListChanged() {
    }

    private void fireInputPortStatusChanged(Port<InputPortStatus> port, InputPortStatus status) {
        notifyPortStatusListeners(inputPortListeners, port, status);
    }

    private <T extends BidibStatus> void notifyPortStatusListeners(
        final Collection<PortListener<T>> listeners, final Port<T> port, final T status) {
        if (SwingUtilities.isEventDispatchThread()) {
            for (PortListener<T> l : listeners) {
                l.statusChanged(port, status);
            }
        }
        else {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    for (PortListener<T> l : listeners) {
                        l.statusChanged(port, status);
                    }
                }
            });
        }
    }

    private <T extends BidibStatus> void notifyPortConfigListeners(
        final Collection<PortListener<T>> listeners, final Port<T> port) {
        if (SwingUtilities.isEventDispatchThread()) {
            for (PortListener<T> l : listeners) {
                l.configChanged(port);
            }
        }
        else {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    for (PortListener<T> l : listeners) {
                        l.configChanged(port);
                    }
                }
            });
        }
    }

    private void fireInputPortListChanged() {
        notifyPortListListeners(inputPortListListeners);
    }

    private void fireLightPortListChanged() {
        notifyPortListListeners(lightPortListListeners);
    }

    private void fireBacklightPortListChanged() {
        notifyPortListListeners(backlightPortListListeners);
    }

    private void notifyPortListListeners(final Collection<PortListListener> listeners) {
        if (SwingUtilities.isEventDispatchThread()) {
            for (PortListListener l : listeners) {
                l.listChanged();
            }
        }
        else {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    for (PortListListener l : listeners) {
                        l.listChanged();
                    }
                }
            });
        }
    }

    private <T extends BidibStatus> void notifyPortValueListeners(
        final Collection<PortValueListener<T>> listeners, final Port<T> port) {
        if (SwingUtilities.isEventDispatchThread()) {
            for (PortValueListener<T> l : listeners) {
                l.valueChanged(port);
            }
        }
        else {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    for (PortValueListener<T> l : listeners) {
                        l.valueChanged(port);
                    }
                }
            });
        }
    }

    private void fireBacklightPortValueChanged(final BacklightPort port) {
        notifyPortValueListeners(backlightPortValueListeners, port);
    }

    private void fireMacroPendingChangesChanged() {
        if (SwingUtilities.isEventDispatchThread()) {
            for (MacroListListener l : macroListListeners) {
                l.pendingChangesChanged();
            }
        }
        else {
            SwingUtilities.invokeLater(new Runnable() {

                @Override
                public void run() {
                    for (MacroListListener l : macroListListeners) {
                        l.pendingChangesChanged();
                    }
                }
            });
        }
    }

    private void fireMacroChanged() {
        if (SwingUtilities.isEventDispatchThread()) {
            for (MacroListListener l : macroListListeners) {
                l.macroChanged();
            }
        }
        else {
            SwingUtilities.invokeLater(new Runnable() {

                @Override
                public void run() {
                    for (MacroListListener l : macroListListeners) {
                        l.macroChanged();
                    }
                }
            });
        }
    }

    private void fireMacroListChanged() {
        if (SwingUtilities.isEventDispatchThread()) {
            for (MacroListListener l : macroListListeners) {
                l.listChanged();
            }
        }
        else {
            SwingUtilities.invokeLater(new Runnable() {

                @Override
                public void run() {
                    for (MacroListListener l : macroListListeners) {
                        l.listChanged();
                    }
                }
            });
        }
    }

    // private void fireMotorPortListChanged() {
    // notifyPortListListeners(motorPortListListeners);
    // }

    private void fireNodeWillChange() {
        Collection<NodeListListener> listeners =
            new LinkedList<NodeListListener>(Collections.unmodifiableCollection(nodeListListeners));

        for (NodeListListener l : listeners) {
            l.nodeWillChange();
        }
    }

    private void fireNodeChanged() {
        Collection<NodeListListener> listeners =
            new LinkedList<NodeListListener>(Collections.unmodifiableCollection(nodeListListeners));

        for (NodeListListener l : listeners) {
            l.nodeChanged();
        }

        Collection<NodeSelectionListener> nodeSelectionListeners =
            Collections.unmodifiableCollection(this.nodeSelectionListeners);

        for (NodeSelectionListener l : nodeSelectionListeners) {
            l.selectedNodeChanged(selectedNode);
        }
    }

    private void fireNodeListChanged() {
        LOGGER.info("Notify the listeners that the node list has changed.");
        Collection<NodeListListener> listeners =
            new LinkedList<NodeListListener>(Collections.unmodifiableCollection(nodeListListeners));
        for (NodeListListener l : listeners) {
            l.listChanged();
        }
    }

    private void fireNodeListAdded(Node node) {
        LOGGER.info("Notify the listeners that the node list has added a node.");
        Collection<NodeListListener> listeners =
            new LinkedList<NodeListListener>(Collections.unmodifiableCollection(nodeListListeners));

        for (NodeListListener l : listeners) {
            l.listNodeAdded(node);
        }
    }

    private void fireNodeListRemoved(Node node) {
        LOGGER.info("Notify the listeners that the node list has removed a node.");
        Collection<NodeListListener> listeners =
            new LinkedList<NodeListListener>(Collections.unmodifiableCollection(nodeListListeners));

        for (NodeListListener l : listeners) {
            l.listNodeRemoved(node);
        }
    }

    private void fireNodeStateChanged() {
        LOGGER.info("Notify the listeners that the node list has changed.");
        Collection<NodeListListener> listeners =
            new LinkedList<NodeListListener>(Collections.unmodifiableCollection(nodeListListeners));

        for (NodeListListener l : listeners) {
            l.nodeStateChanged();
        }
    }

    private void fireServoPortValueChanged(final ServoPort port) {
        notifyPortValueListeners(servoPortValueListeners, port);
    }

    private void fireMotorPortValueChanged(final MotorPort port) {
        notifyPortValueListeners(motorPortValueListeners, port);
    }

    private void fireSoundPortListChanged() {
        notifyPortListListeners(soundPortListListeners);
    }

    private void fireSwitchPortListChanged() {
        notifyPortListListeners(switchPortListListeners);
    }

    private void fireSwitchPairPortListChanged() {
        notifyPortListListeners(switchPairPortListListeners);
    }

    private void fireCvDefinitionChanged() {
        notifyCvDefinitionListeners(cvDefinitionListeners);
    }

    private void notifyCvDefinitionListeners(final Collection<CvDefinitionListener> listeners) {
        if (SwingUtilities.isEventDispatchThread()) {
            for (CvDefinitionListener l : listeners) {
                l.cvDefinitionChanged();
            }
        }
        else {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    for (CvDefinitionListener l : listeners) {
                        l.cvDefinitionChanged();
                    }
                }
            });
        }
    }

    public List<AnalogPort> getAnalogPorts() {
        LOGGER.debug("Get analog ports.");
        synchronized (analogPorts) {
            if (getSelectedNode() != null && CollectionUtils.isNotEmpty(getSelectedNode().getGenericPorts())) {
                // fetch the analog ports from the generic ports
                LOGGER.trace("Get the analog ports from the generic ports.");

                analogPorts.clear();
                analogPorts.addAll(getSelectedNode().getAnalogPorts());

                // set the port labels
                PortLabelUtils.applyPortLabels(getSelectedNode(), DefaultApplicationContext.KEY_ANALOGPORT_LABELS,
                    AnalogPortLabelFactory.DEFAULT_LABELTYPE, analogPorts);
            }
            else {
                LOGGER.trace("The selected port has no generic ports.");
            }

            LOGGER.debug("Return number of analogPorts: {}", analogPorts.size());
            return Collections.unmodifiableList(analogPorts);
        }
    }

    public int getBoosterCurrent() {
        return boosterCurrent;
    }

    public int getBoosterMaximumCurrent() {
        return boosterMaximumCurrent;
    }

    public BoosterStatus getBoosterStatus() {
        return boosterStatus;
    }

    public int getBoosterTemperature() {
        return boosterTemperature;
    }

    public int getBoosterVoltage() {
        return boosterVoltage;
    }

    public CommandStationState getCommandStationState() {
        return commandStationState;
    }

    public List<FeedbackPort> getFeedbackPorts() {

        Node node = getSelectedNode();
        if (node != null) {
            return node.getFeedbackPorts();
        }
        LOGGER.warn("No node selected to get the feedback ports from.");
        return Collections.emptyList();
    }

    public List<Flag> getFlags() {
        return Collections.unmodifiableList(flags);
    }

    public List<LightPort> getLightPorts() {
        LOGGER.debug("Get light ports.");
        synchronized (lightPorts) {
            if (getSelectedNode() != null && CollectionUtils.isNotEmpty(getSelectedNode().getGenericPorts())) {
                // fetch the light ports from the generic ports
                LOGGER.trace("Get the light ports from the generic ports.");

                lightPorts.clear();
                lightPorts.addAll(getSelectedNode().getLightPorts());

                // set the port labels
                PortLabelUtils.applyPortLabels(getSelectedNode(), DefaultApplicationContext.KEY_LIGHTPORT_LABELS,
                    LightPortLabelFactory.DEFAULT_LABELTYPE, lightPorts);
            }
            else {
                LOGGER.trace("The selected port has no generic ports.");
            }

            LOGGER.debug("Return number of lightPorts: {}", lightPorts.size());
            return Collections.unmodifiableList(lightPorts);
        }
    }

    public List<LightPort> getEnabledLightPorts() {
        synchronized (lightPorts) {
            List<LightPort> enabledPorts = new LinkedList<>();
            for (LightPort port : lightPorts) {
                if (port.isEnabled()) {
                    enabledPorts.add(port);
                }
            }
            LOGGER.debug("Get the enabled light ports: {}", enabledPorts);
            return Collections.unmodifiableList(enabledPorts);
        }
    }

    public List<BacklightPort> getBacklightPorts() {
        LOGGER.debug("Get backlight ports.");

        synchronized (backlightPorts) {
            if (getSelectedNode() != null && CollectionUtils.isNotEmpty(getSelectedNode().getGenericPorts())) {
                // fetch the backlight ports from the generic ports
                LOGGER.trace("Get the backlight ports from the generic ports.");

                backlightPorts.clear();
                backlightPorts.addAll(getSelectedNode().getBacklightPorts());

                // set the port labels
                PortLabelUtils.applyPortLabels(getSelectedNode(), DefaultApplicationContext.KEY_BACKLIGHTPORT_LABELS,
                    BacklightPortLabelFactory.DEFAULT_LABELTYPE, backlightPorts);
            }
            else {
                LOGGER.trace("The selected port has no generic ports.");
            }

            LOGGER.debug("Return number of backlightPorts: {}", backlightPorts.size());
            return Collections.unmodifiableList(backlightPorts);
        }
    }

    public List<MotorPort> getMotorPorts() {
        LOGGER.debug("Get motor ports.");

        Node node = getSelectedNode();
        if (node != null) {
            List<MotorPort> motorPorts = node.getMotorPorts();
            return motorPorts;
        }
        LOGGER.warn("No node selected to get the motor ports from.");
        return Collections.emptyList();
    }

    /**
     * @return the selected node instance
     */
    public Node getSelectedNode() {
        synchronized (nodeLock) {
            return selectedNode;
        }
    }

    public Collection<Node> getNodes() {
        LOGGER.trace("Get the nodes.");
        synchronized (nodes) {
            return Collections.unmodifiableCollection(nodes);
        }
    }

    public List<ServoPort> getServoPorts() {
        LOGGER.debug("Get servo ports.");

        Node node = getSelectedNode();
        if (node != null) {
            List<ServoPort> servoPorts = node.getServoPorts();
            return servoPorts;
        }
        LOGGER.warn("No node selected to get the servo ports from.");
        return Collections.emptyList();
    }

    public List<SoundPort> getSoundPorts() {
        LOGGER.debug("Get sound ports.");
        synchronized (soundPorts) {
            if (getSelectedNode() != null && CollectionUtils.isNotEmpty(getSelectedNode().getGenericPorts())) {
                // fetch the sound ports from the generic ports
                LOGGER.trace("Get the sound ports from the generic ports.");

                soundPorts.clear();
                soundPorts.addAll(getSelectedNode().getSoundPorts());

                // set the port labels
                PortLabelUtils.applyPortLabels(getSelectedNode(), DefaultApplicationContext.KEY_SOUNDPORT_LABELS,
                    SoundPortLabelFactory.DEFAULT_LABELTYPE, soundPorts);
            }
            else {
                LOGGER.trace("The selected port has no generic ports.");
            }

            LOGGER.debug("Return number of soundPorts: {}", soundPorts.size());
            return Collections.unmodifiableList(soundPorts);
        }
    }

    public StatusModel getStatusModel() {
        return statusModel;
    }

    public List<SoundPort> getEnabledSoundPorts() {
        synchronized (soundPorts) {
            List<SoundPort> enabledPorts = new LinkedList<>();
            for (SoundPort port : soundPorts) {
                if (port.isEnabled()) {
                    enabledPorts.add(port);
                }
            }
            return Collections.unmodifiableList(enabledPorts);
        }
    }

    public List<SwitchPort> getSwitchPorts() {
        LOGGER.debug("Get switch ports.");
        synchronized (switchPorts) {
            if (getSelectedNode() != null && CollectionUtils.isNotEmpty(getSelectedNode().getGenericPorts())) {
                // fetch the switch ports from the generic ports

                if (!CollectionUtils.isEqualCollection(switchPorts, getSelectedNode().getSwitchPorts())) {
                    LOGGER.info("Get the switch ports from the generic ports.");

                    switchPorts.clear();
                    switchPorts.addAll(getSelectedNode().getSwitchPorts());

                    // set the port labels
                    PortLabelUtils.applyPortLabels(getSelectedNode(), DefaultApplicationContext.KEY_SWITCHPORT_LABELS,
                        SwitchPortLabelFactory.DEFAULT_LABELTYPE, switchPorts);
                }
                else {
                    LOGGER.info("The switch port collection has not changed.");
                }
            }
            else {
                LOGGER.trace("The selected node has no generic ports.");
            }

            LOGGER.debug("Return number of switchPorts: {}", switchPorts.size());
            return Collections.unmodifiableList(switchPorts);
        }
    }

    public List<SwitchPort> getEnabledSwitchPorts() {
        synchronized (switchPorts) {
            List<SwitchPort> enabledPorts = new LinkedList<>();
            for (SwitchPort port : switchPorts) {
                if (port.isEnabled()) {
                    enabledPorts.add(port);
                }
            }
            return Collections.unmodifiableList(enabledPorts);
        }
    }

    public List<SwitchPairPort> getSwitchPairPorts() {
        LOGGER.debug("Get switchPair ports.");
        synchronized (switchPairPorts) {
            if (getSelectedNode() != null && CollectionUtils.isNotEmpty(getSelectedNode().getGenericPorts())) {
                // fetch the switch ports from the generic ports

                if (!CollectionUtils.isEqualCollection(switchPairPorts, getSelectedNode().getSwitchPairPorts())) {
                    LOGGER.info("Get the switchPair ports from the generic ports.");

                    switchPairPorts.clear();
                    switchPairPorts.addAll(getSelectedNode().getSwitchPairPorts());

                    LOGGER.info("Fetched the switchPair ports from the generic ports of the node: {}", switchPairPorts);

                    // set the port labels
                    PortLabelUtils.applyPortLabels(getSelectedNode(),
                        DefaultApplicationContext.KEY_SWITCHPAIRPORT_LABELS,
                        SwitchPairPortLabelFactory.DEFAULT_LABELTYPE, switchPairPorts);
                }
                else {
                    LOGGER.info("The switchPair port collection has not changed.");
                }
            }
            else {
                LOGGER.trace("The selected node has no generic ports.");
            }

            LOGGER.debug("Return number of switchPairPorts: {}", switchPairPorts.size());
            return Collections.unmodifiableList(switchPairPorts);
        }
    }

    public List<SwitchPairPort> getEnabledSwitchPairPorts() {
        synchronized (switchPairPorts) {
            List<SwitchPairPort> enabledPorts = new LinkedList<>();
            for (SwitchPairPort port : switchPairPorts) {
                if (port.isEnabled()) {
                    enabledPorts.add(port);
                }
            }
            return Collections.unmodifiableList(enabledPorts);
        }
    }

    @Override
    public boolean isFlatPortModel() {
        if (getSelectedNode() != null) {
            return getSelectedNode().getNode().isPortFlatModelAvailable();
        }
        return false;
    }

    public void replaceAccessory(Accessory accessory) {
        synchronized (accessories) {

            Accessory prev = accessories.get(accessory.getId());
            if (prev != null) {
                prev.removePropertyChangeListener(accessoryPendingChangesListener);

                if (prev.getStartupState() != null && accessory.getStartupState() == null) {
                    // keep the startup state

                    int aspectCount = accessory.getAspects().size();
                    int prevStartupState = prev.getStartupState();
                    if (prevStartupState < aspectCount || prevStartupState == BidibLibrary.ASPECT_PARAM_RESTORE
                        || prevStartupState == BidibLibrary.ASPECT_PARAM_UNCHANGED) {
                        LOGGER.info("Keep the previous startup state for this accessory: {}", prevStartupState);
                        accessory.setStartupState(prevStartupState);
                    }
                    else {
                        LOGGER.info("Set the startup state to unchanged.");
                        accessory.setStartupState(BidibLibrary.ASPECT_PARAM_UNCHANGED);
                    }
                }
                if (prev.getSwitchTime() != null && accessory.getSwitchTime() == null) {
                    // keep the switch time
                    accessory.setSwitchTime(prev.getSwitchTime());
                }
                if (prev.getTimeBaseUnit() != null && accessory.getTimeBaseUnit() == null) {
                    // keep the time base unit
                    accessory.setTimeBaseUnit(prev.getTimeBaseUnit());
                }
            }

            accessories.set(accessory.getId(), accessory);

            accessory.addPropertyChangeListener(accessoryPendingChangesListener);
        }
        fireAccessoryListChanged();
        setSelectedAccessory(accessory);
    }

    /**
     * Returns the currently selected macro.
     * 
     * @return the currently selected macro
     */
    public Macro getSelectedMacro() {
        return selectedMacro;
    }

    /**
     * Set the selected macro.
     * 
     * @param macro
     *            the selected macro
     */
    public void setSelectedMacro(Macro macro) {

        if (selectedMacro != null && MacroSaveState.PENDING_CHANGES.equals(selectedMacro.getMacroSaveState())) {
            // log warning
            LOGGER.warn("The current macro has pending changes.");
        }

        this.selectedMacro = macro;
        fireMacroChanged();
    }

    /**
     * Set the macros of the current node.
     * 
     * @param macros
     *            the macros
     */
    public void setMacros(Collection<Macro> macros) {
        synchronized (this.macros) {
            // remove existing property change listener
            for (Macro macro : this.macros) {
                macro.removePropertyChangeListener(Macro.PROPERTY_PENDING_CHANGES, macroPendingChangesListener);
            }

            // clear references to existing macros
            this.macros.clear();
            if (macros != null) {
                // ... and add new macros
                this.macros.addAll(macros);
            }

            // add property change listener to new macros
            for (Macro macro : this.macros) {
                macro.addPropertyChangeListener(Macro.PROPERTY_PENDING_CHANGES, macroPendingChangesListener);
            }
        }
        fireMacroListChanged();
    }

    @Override
    public List<Macro> getMacros() {
        synchronized (macros) {
            return Collections.unmodifiableList(macros);
        }
    }

    /**
     * Replace the macro with the provided macro.
     * 
     * @param macro
     *            the new macro
     * @throws PropertyVetoException
     */
    public void replaceMacro(Macro macro) {
        LOGGER.info("Replace the macro: {}", macro);

        synchronized (macros) {

            // remove existing property change listener
            Macro existingMacro = macros.get(macro.getId());
            if (existingMacro != null) {
                existingMacro.removePropertyChangeListener(Macro.PROPERTY_PENDING_CHANGES, macroPendingChangesListener);
            }

            // keep the name of the old macro
            macro.setLabel(existingMacro.getLabel());

            macros.set(macro.getId(), macro);

            // add property change listener to new macro
            macro.addPropertyChangeListener(Macro.PROPERTY_PENDING_CHANGES, macroPendingChangesListener);
        }

        fireMacroListChanged();
        setSelectedMacro(macro);
    }

    /**
     * Check if all macros were loaded from the node.
     * 
     * @return unloaded macro detected
     */
    public boolean hasUnloadedMacros() {
        synchronized (macros) {
            for (Macro macro : macros) {

                if (MacroSaveState.NOT_LOADED_FROM_NODE.equals(macro.getMacroSaveState())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @return the selected accessory
     */
    public Accessory getSelectedAccessory() {
        return selectedAccessory;
    }

    /**
     * Set the selected accessory.
     * 
     * @param accessory
     *            the selected accessory
     */
    public void setSelectedAccessory(Accessory accessory) {
        if (selectedAccessory != null
            && (AccessorySaveState.PERMANENTLY_STORED_ON_NODE != selectedAccessory.getAccessorySaveState())) {
            // log warning
            LOGGER.warn("The current accessory has pending changes.");
        }

        this.selectedAccessory = accessory;
        fireAccessoryChanged((accessory != null ? accessory.getId() : -1));
    }

    public List<Accessory> getAccessories() {
        synchronized (accessories) {
            return Collections.unmodifiableList(accessories);
        }
    }

    public ArrayListModel<Accessory> getAccessoriesListModel() {
        return accessories;
    }

    public void setAccessories(Collection<Accessory> accessories) {
        synchronized (this.accessories) {
            LOGGER.info("Set accessories: {}", accessories);

            // remove existing property change listener
            for (Accessory accessory : this.accessories) {
                accessory.removePropertyChangeListener(accessoryPendingChangesListener);
            }

            this.accessories.clear();
            if (accessories != null) {
                this.accessories.addAll(accessories);
            }

            // add property change listener to new accessory
            for (Accessory accessory : this.accessories) {
                accessory.addPropertyChangeListener(accessoryPendingChangesListener);
            }
        }
        // reset the accessory
        setSelectedAccessory(null);

        fireAccessoryListChanged();
    }

    public void setAnalogPorts(Collection<AnalogPort> analogPorts) {
        synchronized (this.analogPorts) {
            this.analogPorts.clear();
            if (analogPorts != null) {
                this.analogPorts.addAll(analogPorts);
            }
        }
        fireAnalogPortListChanged();
    }

    public void setAnalogPortConfig(int portNumber, Map<Byte, PortConfigValue<?>> portConfig) {
        // synchronize because the backlightPorts can be accessed from different threads
        try {
            AnalogPort analogPort = null;
            synchronized (this.analogPorts) {
                if (analogPorts != null) {
                    LOGGER.info("Set analogPort config for portNumber: {}", portNumber);
                    analogPort = this.analogPorts.get(portNumber);
                    analogPort.setPortConfigX(portConfig);
                }
            }

            if (analogPort != null) {
                fireAnalogPortConfigChanged(analogPort);
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Set analogport config failed for portNumber: {}", portNumber, ex);
        }
    }

    private void fireAnalogPortConfigChanged(Port<AnalogPortStatus> port) {
        notifyPortConfigListeners(analogPortListeners, port);
    }

    public void setBoosterCurrent(int current) {

        // set the last current update time
        setLastCurrentUpdate(System.currentTimeMillis());

        if (current != this.boosterCurrent) {
            LOGGER.debug("Booster current has changed, new: {}, old: {}", current, boosterCurrent);
            this.boosterCurrent = current;
            fireBoosterCurrentChanged(current);
        }
        else {
            LOGGER.trace("Current is equal, do not send change notification.");
        }
    }

    /**
     * @return the lastCurrentUpdate
     */
    public long getLastCurrentUpdate() {
        return lastCurrentUpdate;
    }

    /**
     * @param lastCurrentUpdate
     *            the lastCurrentUpdate to set
     */
    public void setLastCurrentUpdate(long lastCurrentUpdate) {
        this.lastCurrentUpdate = lastCurrentUpdate;
    }

    public void setBoosterMaximumCurrent(int maximumCurrent) {
        LOGGER.info("Set the new maximum current value: {}, previous: {}", maximumCurrent, boosterMaximumCurrent);
        // if (maximumCurrent != this.boosterMaximumCurrent) {
        this.boosterMaximumCurrent = maximumCurrent;
        fireBoosterMaximumCurrentChanged(maximumCurrent);
        // }
    }

    public void setBoosterStatus(BoosterStatus status) {
        LOGGER.info("Set the new booster status: {}", status);
        if (status != this.boosterStatus) {
            this.boosterStatus = status;
            LOGGER.info("Fire booster status changed: {}", status);
            fireBoosterStatusChanged(status);
        }
    }

    public void setBoosterTemperature(int temperature) {
        if (temperature != this.boosterTemperature) {
            this.boosterTemperature = temperature;
            fireBoosterTemperatureChanged(temperature);
        }
    }

    public void setBoosterVoltage(int voltage) {
        if (voltage != this.boosterVoltage) {
            this.boosterVoltage = voltage;
            fireBoosterVoltageChanged(voltage);
        }
    }

    public void setCommandStationState(Node node, CommandStationState commandStationState) {

        if (getSelectedNode() != null && getSelectedNode().equals(node)) {
            if (commandStationState != this.commandStationState) {
                this.commandStationState = commandStationState;
            }
        }

        if (node != null) {
            fireCommandStationStatusChanged(node, commandStationState);
        }
    }

    public void setFlags(Collection<Flag> flags) {
        // synchronize because the flags can be accessed from different threads
        synchronized (this.flags) {
            this.flags.clear();
            if (flags != null) {
                this.flags.addAll(flags);
            }
        }
        fireFlagListChanged();
    }

    public List<InputPort> getInputPorts() {
        LOGGER.debug("Get input ports.");
        synchronized (inputPorts) {
            if (getSelectedNode() != null && CollectionUtils.isNotEmpty(getSelectedNode().getGenericPorts())) {
                // fetch the input ports from the generic ports

                if (!CollectionUtils.isEqualCollection(inputPorts, getSelectedNode().getInputPorts())) {
                    LOGGER.info("Get the input ports from the generic ports.");

                    inputPorts.clear();
                    inputPorts.addAll(getSelectedNode().getInputPorts());

                    // set the port labels
                    PortLabelUtils.applyPortLabels(getSelectedNode(), DefaultApplicationContext.KEY_INPUTPORT_LABELS,
                        InputPortLabelFactory.DEFAULT_LABELTYPE, inputPorts);
                }
                else {
                    LOGGER.info("The input port collection has not changed.");
                }
            }
            else {
                LOGGER.trace("The selected port has no generic ports.");
            }

            LOGGER.debug("Return number of inputPorts: {}", inputPorts.size());
            return Collections.unmodifiableList(inputPorts);
        }
    }

    public List<InputPort> getEnabledInputPorts() {
        synchronized (inputPorts) {
            List<InputPort> enabledPorts = new LinkedList<>();
            for (InputPort port : inputPorts) {
                if (port.isEnabled()) {
                    enabledPorts.add(port);
                }
            }
            LOGGER.debug("Get the enabled input ports: {}", enabledPorts);
            return Collections.unmodifiableList(enabledPorts);
        }
    }

    public void setInputPorts(Collection<InputPort> inputPorts) {
        // synchronize because the inputPorts can be accessed from different threads
        synchronized (this.inputPorts) {
            LOGGER.info("Set the input ports.");
            this.inputPorts.clear();
            if (inputPorts != null) {
                this.inputPorts.addAll(inputPorts);
            }
            LOGGER.info("Added inputPorts: {}", inputPorts);
        }
        fireInputPortListChanged();
    }

    public void setInputPortStatus(final int keyNumber, int keyState) {

        // boolean fireStatusChanged = false;
        InputPort port = null;
        synchronized (this.inputPorts) {

            // support the flat model
            if (getSelectedNode() != null && getSelectedNode().getNode().isPortFlatModelAvailable()) {
                // make sure the input ports are available
                if (CollectionUtils.isNotEmpty(getInputPorts())) {

                    InputPort inputPort = IterableUtils.find(inputPorts, new Predicate<InputPort>() {
                        @Override
                        public boolean evaluate(InputPort port) {
                            return port.getId() == keyNumber;
                        }
                    });

                    LOGGER.info("Found input port {} to set the keyState: {}", inputPort, keyState);

                    if (inputPort != null) {
                        InputPortStatus status = (keyState == 1 ? InputPortStatus.ON : InputPortStatus.OFF);

                        inputPort.setStatus(status);
                        // fireInputPortStatusChanged(inputPort, status);
                        port = inputPort;
                    }
                    else {
                        LOGGER.warn("No input port in flat port model available for keyNumber: {}", keyNumber);
                    }
                }
            }
            else {

                if (getInputPorts().size() > keyNumber) {
                    InputPort inputPort = getInputPorts().get(keyNumber);
                    InputPortStatus status = (keyState == 1 ? InputPortStatus.ON : InputPortStatus.OFF);

                    inputPort.setStatus(status);
                    // fireInputPortStatusChanged(inputPort, status);
                    port = inputPort;
                }
                else {
                    LOGGER.warn("No input port available for keyNumber: {}", keyNumber);
                }
            }
        }

        if (port != null) {
            fireInputPortStatusChanged(port, null);
        }
    }

    public void setInputPortConfig(int portNumber, Map<Byte, PortConfigValue<?>> portConfig) {
        // synchronize because the inputPorts can be accessed from different threads
        try {
            InputPort inputPort = null;
            synchronized (this.inputPorts) {
                if (inputPorts != null) {
                    LOGGER.info("Set inputPort config for portNumber: {}", portNumber);
                    inputPort = this.inputPorts.get(portNumber);
                    inputPort.setPortConfigX(portConfig);
                }
            }
            if (inputPort != null) {
                fireInputPortConfigChanged(inputPort);
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Set inputPort config failed for portNumber: {}", portNumber, ex);
        }
    }

    private void fireInputPortConfigChanged(Port<InputPortStatus> port) {
        notifyPortConfigListeners(inputPortListeners, port);
    }

    public void setLightPorts(Collection<LightPort> lightPorts) {
        // synchronize because the lightPorts can be accessed from different threads
        synchronized (this.lightPorts) {
            this.lightPorts.clear();
            if (lightPorts != null) {
                this.lightPorts.addAll(lightPorts);
            }
        }
        fireLightPortListChanged();
    }

    public void setLightPortConfig(int portNumber, Map<Byte, PortConfigValue<?>> portConfig) {
        // synchronize because the lightPorts can be accessed from different threads
        try {
            LightPort lightPort = null;
            synchronized (this.lightPorts) {
                if (lightPorts != null) {
                    LOGGER.info("Set lightport config for portNumber: {}", portNumber);
                    lightPort = this.lightPorts.get(portNumber);
                    lightPort.setPortConfigX(portConfig);
                }
            }
            if (lightPort != null) {
                fireLightPortConfigChanged(lightPort);
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Set lightport config failed for portNumber: {}", portNumber, ex);
        }
    }

    public void publishPortConfigChanged(LcOutputType portType, int portNum) {
        LOGGER.info("The config of the port has changed: {}", portNum);
        Port<?> port = null;
        switch (portType) {
            case LIGHTPORT:
                port = PortListUtils.findPortByPortNumber(lightPorts, portNum);
                fireLightPortConfigChanged((Port<LightPortStatus>) port);
                break;
            case INPUTPORT:
                port = PortListUtils.findPortByPortNumber(inputPorts, portNum);
                fireInputPortConfigChanged((Port<InputPortStatus>) port);
                break;
            default:
                break;
        }
    }

    private void fireLightPortConfigChanged(Port<LightPortStatus> port) {
        notifyPortConfigListeners(lightPortListeners, port);
    }

    public void setBacklightPorts(Collection<BacklightPort> backlightPorts) {
        // synchronize because the backlightPorts can be accessed from different threads
        synchronized (this.backlightPorts) {
            this.backlightPorts.clear();
            if (backlightPorts != null) {
                this.backlightPorts.addAll(backlightPorts);
            }
        }
        fireBacklightPortListChanged();
    }

    public void setBacklightPortConfig(int portNumber, final Map<Byte, PortConfigValue<?>> portConfig) {
        // synchronize because the backlightPorts can be accessed from different threads

        SwingUtilities.invokeLater(new Runnable() {
            public void run() {

                try {
                    BacklightPort backlightPort = null;
                    synchronized (MainModel.this.backlightPorts) {
                        if (backlightPorts != null) {
                            backlightPort = MainModel.this.backlightPorts.get(portNumber);

                            if (!backlightPort.isAdjusting()) {
                                backlightPort.setPortConfigX(portConfig);
                            }
                            else {
                                LOGGER.info("Ignore changed config during adjusting backlight value.");
                                backlightPort = null;
                            }
                        }
                    }
                    if (backlightPort != null) {
                        fireBacklightPortConfigChanged(backlightPort);
                    }
                }
                catch (Exception ex) {
                    LOGGER.warn("Set backlightport config failed for portNumber: {}", portNumber, ex);
                }
            }
        });
    }

    private void fireBacklightPortConfigChanged(Port<BacklightPortStatus> port) {
        notifyPortConfigListeners(backlightPortListeners, port);
    }

    /**
     * Set the backlight port value.
     * 
     * @param portNumber
     *            the port number
     * @param portValue
     *            the port value
     */
    public void setBacklightPortValue(int portNumber, int portValue) {
        if (getBacklightPorts().size() > portNumber) {
            final BacklightPort port = getBacklightPorts().get(portNumber);

            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    int oldValue = port.getValue();
                    if (oldValue != portValue) {
                        port.setValue(portValue, false);

                        fireBacklightPortValueChanged(port);
                    }
                }
            });
        }
    }

    /**
     * Set the selected node instance
     * 
     * @param node
     *            the selected node instance
     */
    public void setSelectedNode(Node node) {
        LOGGER.info("Set the selected node in the main model: {}", node);

        synchronized (nodeLock) {

            if (node != null && node.equals(selectedNode)) {
                LOGGER.info("The selected node has not changed.");
                return;
            }

            fireNodeWillChange();

            this.selectedNode = node;

            // clear the cached node values
            clearCachedNodeValues();
        }
        fireNodeChanged();
    }

    private void clearCachedNodeValues() {
        LOGGER.info("Clear the cached node values.");

        boosterCurrent = 0;
        boosterMaximumCurrent = 0;
        boosterTemperature = 0;
        boosterVoltage = 0;

        commandStationState = null;

        // clear all ports
        synchronized (analogPorts) {
            analogPorts.clear();
        }
        synchronized (backlightPorts) {
            backlightPorts.clear();
        }

        synchronized (inputPorts) {
            inputPorts.clear();
        }
        synchronized (lightPorts) {
            lightPorts.clear();
        }
        // synchronized (motorPorts) {
        // motorPorts.clear();
        // }
        // synchronized (servoPorts) {
        // servoPorts.clear();
        // }
        synchronized (soundPorts) {
            soundPorts.clear();
        }
        synchronized (switchPorts) {
            switchPorts.clear();
        }
        synchronized (switchPairPorts) {
            switchPairPorts.clear();
        }
    }

    /**
     * Clear the cached nodes.
     */
    public void clearNodes() {
        setNodes(null);
        setSelectedNode(null);
    }

    public void setNodes(Collection<Node> nodes) {
        LOGGER.debug("Set the nodes: {}", nodes);
        synchronized (this.nodes) {
            this.nodes.clear();
            if (nodes != null) {
                this.nodes.addAll(nodes);
            }
        }
        fireNodeListChanged();
    }

    public void setServoPortValue(int portNumber, int portValue) {
        List<ServoPort> servoPorts = getServoPorts();

        if (servoPorts.size() > portNumber) {
            ServoPort port = servoPorts.get(portNumber);

            int oldValue = port.getValue();
            if (oldValue != portValue) {
                port.setValue(portValue);

                fireServoPortValueChanged(port);
            }
        }
    }

    public void setMotorPortValue(int portNumber, int portValue) {
        List<MotorPort> motorPorts = getMotorPorts();

        if (motorPorts.size() > portNumber) {
            MotorPort port = motorPorts.get(portNumber);

            int oldValue = port.getValue();
            // if (oldValue != portValue) {
            port.setValue(portValue);

            fireMotorPortValueChanged(port);
            // }
        }
    }

    public void setSoundPorts(Collection<SoundPort> soundPorts) {

        synchronized (this.soundPorts) {
            LOGGER.info("Set the new sound ports: {}", soundPorts);

            // support the flat model
            Node selectedNode = getSelectedNode();
            if (selectedNode != null && selectedNode.getNode().isPortFlatModelAvailable()) {
                // TODO
                selectedNode.getSoundPorts();
            }
            else {
                this.soundPorts.clear();
                if (soundPorts != null) {
                    this.soundPorts.addAll(soundPorts);
                }
            }
        }
        fireSoundPortListChanged();
    }

    public void setSoundPortConfig(int portNumber, Map<Byte, PortConfigValue<?>> portConfig) {
        // synchronize because the soundPorts can be accessed from different threads
        try {
            synchronized (this.soundPorts) {
                if (soundPorts != null) {
                    this.soundPorts.get(portNumber).setPortConfigX(portConfig);
                }
            }
            fireSoundPortListChanged();
        }
        catch (Exception ex) {
            LOGGER.warn("Set soundport config failed for portNumber: {}", portNumber, ex);
        }
    }

    public void setSwitchPorts(Collection<SwitchPort> switchPorts) {
        synchronized (this.switchPorts) {
            LOGGER.info("Set the new switch ports: {}", switchPorts);

            // support the flat model
            Node selectedNode = getSelectedNode();
            if (selectedNode != null && selectedNode.getNode().isPortFlatModelAvailable()) {
                // TODO
                selectedNode.getSwitchPorts();
            }
            else {
                this.switchPorts.clear();
                if (switchPorts != null) {
                    this.switchPorts.addAll(switchPorts);
                }
            }
        }
        fireSwitchPortListChanged();
    }

    public void setSwitchPortConfig(int portNumber, Map<Byte, PortConfigValue<?>> portConfig) {
        // synchronize because the switchPorts can be accessed from different threads
        try {
            synchronized (this.switchPorts) {
                if (switchPorts != null) {
                    this.switchPorts.get(portNumber).setPortConfigX(portConfig);
                }
            }
            fireSwitchPortListChanged();
        }
        catch (Exception ex) {
            LOGGER.warn("Set switchport config failed for portNumber: {}", portNumber, ex);
        }
    }

    public void setSwitchPortStatus(final int portNumber, int keyState) {
        SwitchPort port = null;

        synchronized (switchPorts) {
            // support the flat model
            if (getSelectedNode() != null && getSelectedNode().getNode().isPortFlatModelAvailable()) {
                // make sure the input ports are available
                if (CollectionUtils.isNotEmpty(getSwitchPorts())) {

                    SwitchPort switchPort = IterableUtils.find(switchPorts, new Predicate<SwitchPort>() {
                        @Override
                        public boolean evaluate(SwitchPort port) {
                            return port.getId() == portNumber;
                        }
                    });

                    if (switchPort != null) {
                        SwitchPortStatus status = (keyState == 1 ? SwitchPortStatus.ON : SwitchPortStatus.OFF);
                        LOGGER.debug("SwitchPort status has changed, port: {}, status: {}", switchPort, status);
                        switchPort.setStatus(status);
                        port = switchPort;
                    }
                    else {
                        LOGGER.warn("No switch port in flat port model available for portNumber: {}", portNumber);
                    }
                }
            }
            else {

                if (portNumber < switchPorts.size()) {
                    SwitchPort switchPort = switchPorts.get(portNumber);
                    SwitchPortStatus status = (keyState == 1 ? SwitchPortStatus.ON : SwitchPortStatus.OFF);
                    LOGGER.debug("SwitchPort status has changed, port: {}, status: {}", switchPort, status);
                    switchPort.setStatus(status);
                    port = switchPort;
                }
                else {
                    LOGGER.warn("No switch port available for portNumber: {}", portNumber);
                }
            }
        }

        if (port != null) {
            fireSwitchPortStatusChanged(port, null);
        }
    }

    public void setSwitchPairPorts(Collection<SwitchPairPort> switchPairPorts) {
        synchronized (this.switchPairPorts) {
            LOGGER.info("Set the new switchPair ports: {}", switchPairPorts);

            // support the flat model
            Node selectedNode = getSelectedNode();
            if (selectedNode != null && selectedNode.getNode().isPortFlatModelAvailable()) {
                // TODO
                selectedNode.getSwitchPairPorts();
            }
            else {
                this.switchPairPorts.clear();
                if (switchPairPorts != null) {
                    this.switchPairPorts.addAll(switchPairPorts);
                }
            }
        }
        fireSwitchPairPortListChanged();
    }

    public void setSwitchPairPortConfig(int portNumber, Map<Byte, PortConfigValue<?>> portConfig) {
        // synchronize because the switchPairPorts can be accessed from different threads
        try {
            synchronized (this.switchPairPorts) {
                if (switchPairPorts != null) {
                    this.switchPairPorts.get(portNumber).setPortConfigX(portConfig);
                }
            }
            fireSwitchPairPortListChanged();
        }
        catch (Exception ex) {
            LOGGER.warn("Set switchPair port config failed for portNumber: {}", portNumber, ex);
        }
    }

    public void setSwitchPairPortStatus(final int portNumber, int keyState) {
        SwitchPairPort port = null;

        synchronized (switchPairPorts) {
            // support the flat model
            if (getSelectedNode() != null && getSelectedNode().getNode().isPortFlatModelAvailable()) {
                // make sure the switch pair ports are available
                if (CollectionUtils.isNotEmpty(getSwitchPairPorts())) {

                    SwitchPairPort switchPairPort =
                        IterableUtils.find(switchPairPorts, new Predicate<SwitchPairPort>() {
                            @Override
                            public boolean evaluate(SwitchPairPort port) {
                                return port.getId() == portNumber;
                            }
                        });

                    if (switchPairPort != null) {
                        SwitchPortStatus status = (keyState == 1 ? SwitchPortStatus.ON : SwitchPortStatus.OFF);
                        LOGGER.debug("SwitchPort status has changed, port: {}, status: {}", switchPairPort, status);
                        switchPairPort.setStatus(status);
                        port = switchPairPort;
                    }
                    else {
                        LOGGER.warn("No switchPair port in flat port model available for portNumber: {}", portNumber);
                    }
                }
            }
            else {

                if (portNumber < switchPairPorts.size()) {
                    SwitchPairPort switchPairPort = switchPairPorts.get(portNumber);
                    SwitchPortStatus status = (keyState == 1 ? SwitchPortStatus.ON : SwitchPortStatus.OFF);
                    LOGGER.debug("SwitchPairPort status has changed, port: {}, status: {}", switchPairPort, status);
                    switchPairPort.setStatus(status);
                    port = switchPairPort;
                }
                else {
                    LOGGER.warn("No switchPair port available for portNumber: {}", portNumber);
                }
            }
        }

        if (port != null) {
            fireSwitchPairPortStatusChanged(port, null);
        }
    }

    public void setSoundPortStatus(final int portNumber, int keyState) {
        SoundPort port = null;

        synchronized (soundPorts) {
            // support the flat model
            if (getSelectedNode() != null && getSelectedNode().getNode().isPortFlatModelAvailable()) {
                // make sure the input ports are available
                if (CollectionUtils.isNotEmpty(getSoundPorts())) {

                    SoundPort soundPort = IterableUtils.find(soundPorts, new Predicate<SoundPort>() {
                        @Override
                        public boolean evaluate(SoundPort port) {
                            return port.getId() == portNumber;
                        }
                    });

                    if (soundPort != null) {
                        SoundPortStatus status = (keyState == 1 ? SoundPortStatus.PLAY : SoundPortStatus.STOP);
                        LOGGER.debug("SoundPort status has changed, port: {}, status: {}", soundPort, status);
                        soundPort.setStatus(status);
                        port = soundPort;
                    }
                    else {
                        LOGGER.warn("No sound port in flat port model available for portNumber: {}", portNumber);
                    }
                }
            }
            else {

                if (portNumber < soundPorts.size()) {
                    SoundPort soundPort = soundPorts.get(portNumber);
                    SoundPortStatus status = (keyState == 1 ? SoundPortStatus.PLAY : SoundPortStatus.STOP);
                    LOGGER.debug("SoundPort status has changed, port: {}, status: {}", soundPort, status);
                    soundPort.setStatus(status);
                    port = soundPort;
                }
                else {
                    LOGGER.warn("No sound port available for portNumber: {}", portNumber);
                }
            }
        }

        if (port != null) {
            fireSoundPortStatusChanged(port, null);
        }
    }

    public void setLightPortStatus(final int portNumber, int portState) {

        synchronized (lightPorts) {
            // support the flat model
            if (getSelectedNode() != null && getSelectedNode().getNode().isPortFlatModelAvailable()) {
                // make sure the input ports are available
                if (CollectionUtils.isNotEmpty(getLightPorts())) {

                    LightPort lightPort = IterableUtils.find(lightPorts, new Predicate<LightPort>() {
                        @Override
                        public boolean evaluate(LightPort port) {
                            return port.getId() == portNumber;
                        }
                    });

                    if (lightPort != null) {
                        LightPortEnum type = LightPortEnum.valueOf(ByteUtils.getLowByte(portState));
                        LightPortStatus status = LightPortStatus.valueOf(type);

                        LOGGER.debug("LightPort status has changed, port: {}, status: {}", lightPort, status);
                        lightPort.setStatus(status);
                        fireLightPortStatusChanged(lightPort, status);
                    }
                    else {
                        LOGGER.warn("No input port in flat port model available for portNumber: {}", portNumber);
                    }
                }
            }
            else {

                if (portNumber < lightPorts.size()) {
                    LightPort port = lightPorts.get(portNumber);

                    LightPortEnum type = LightPortEnum.valueOf(ByteUtils.getLowByte(portState));
                    LightPortStatus status = LightPortStatus.valueOf(type);

                    LOGGER.debug("LightPort status has changed, port: {}, status: {}", port, status);
                    port.setStatus(status);
                    fireLightPortStatusChanged(port, status);
                }
                else {
                    LOGGER.warn("No input port available for portNumber: {}", portNumber);
                }
            }
        }
    }

    private void fireSwitchPortStatusChanged(Port<SwitchPortStatus> port, SwitchPortStatus status) {
        notifyPortStatusListeners(switchPortListeners, port, status);
    }

    private void fireSwitchPairPortStatusChanged(Port<SwitchPortStatus> port, SwitchPortStatus status) {
        notifyPortStatusListeners(switchPairPortListeners, port, status);
    }

    private void fireSoundPortStatusChanged(Port<SoundPortStatus> port, SoundPortStatus status) {
        notifyPortStatusListeners(soundPortListeners, port, status);
    }

    private void fireLightPortStatusChanged(Port<LightPortStatus> port, LightPortStatus status) {
        notifyPortStatusListeners(lightPortListeners, port, status);
    }

    public void setIdentifyState(Node node, IdentifyState identifyState) {
        LOGGER.debug("setIdentifyState, node: {}, identifyState: {}", node, identifyState);
        node.setIdentifyState(identifyState);
        fireNodeStateChanged();
    }

    public void setErrorState(Node node, SysErrorEnum sysError, byte[] reasonData) {
        LOGGER.error("setErrorState, node: {}, sysError: {}", node, sysError);
        node.setErrorState(sysError, reasonData);
        fireNodeStateChanged();
    }

    public void setNodeHasError(Node node, boolean nodeHasError) {
        setNodeHasError(node, nodeHasError, null);
    }

    public void setNodeHasError(Node node, boolean nodeHasError, String reason) {
        LOGGER.debug("setErrorState, node: {}, nodeHasError: {}", node, nodeHasError);
        node.setNodeHasError(nodeHasError);
        if (reason != null) {
            node.setReasonData(reason.getBytes());
        }
        fireNodeStateChanged();
    }

    public void setCvDefinition(VendorCvData vendorCV) {
        LOGGER.info("Set the CV definition: {}", vendorCV);
        // synchronized (vendorCVLock) {
        // this.vendorCvData = vendorCV;
        // }
        fireCvDefinitionChanged();
    }

    public VendorCvData getVendorCV() {
        synchronized (vendorCVLock) {
            // return vendorCvData;
            VendorCvData vendorCvData = null;
            if (selectedNode != null) {
                vendorCvData = selectedNode.getVendorCV();
            }
            return vendorCvData;
        }
    }

    public boolean isCvDefinitionAvailable() {

        // check if the cv definition is available
        synchronized (vendorCVLock) {

            if (selectedNode != null) {
                return (selectedNode.getVendorCV() != null);
            }
            return false;
            // return (vendorCvData != null);
        }
    }

    /**
     * @return the configurationVariables
     */
    public List<ConfigurationVariable> getConfigurationVariables() {
        synchronized (configVarsLock) {
            return configurationVariables;
        }
    }

    /**
     * @param configurationVariables
     *            the configurationVariables to set
     */
    public void setConfigurationVariables(List<ConfigurationVariable> configurationVariables) {
        synchronized (configVarsLock) {
            this.configurationVariables = new LinkedList<ConfigurationVariable>();
            if (configurationVariables != null) {
                this.configurationVariables.addAll(configurationVariables);
            }
        }

        fireConfigurationVariablesChanged(false);
    }

    /**
     * @param configurationVars
     *            the configurationVariable values to update
     * @param read
     *            the update was forced by a read operation
     */
    public void updateConfigurationVariableValues(List<ConfigurationVariable> configurationVars, boolean read) {
        // iterate over the collection of stored variables in the model and update the values.
        // After that notify the tree and delete the new values that are now stored in the node
        synchronized (configVarsLock) {
            for (ConfigurationVariable updatedCV : configurationVars) {
                int index = configurationVariables.indexOf(updatedCV);
                LOGGER.debug("The index of the stored value: {}", index);
                if (index > -1) {
                    ConfigurationVariable storedCV = configurationVariables.get(index);
                    storedCV.setValue(updatedCV.getValue());
                    storedCV.setTimeout(updatedCV.isTimeout());
                }
                else {
                    LOGGER.info("Add new CV because the updated CV was not found in the stored CV values: {}",
                        updatedCV);

                    configurationVariables.add(updatedCV);
                }
            }

            LOGGER.info("Number of stored CV: {}", configurationVariables.size());
        }

        fireConfigurationVariablesChanged(read);
    }

    private void fireConfigurationVariablesChanged(boolean read) {
        notifyConfigurationVariableValueListeners(cvDefinitionListeners, read);
    }

    private void notifyConfigurationVariableValueListeners(
        final Collection<CvDefinitionListener> listeners, final boolean read) {
        if (SwingUtilities.isEventDispatchThread()) {
            for (CvDefinitionListener l : listeners) {
                l.cvDefinitionValuesChanged(read);
            }
        }
        else {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    for (CvDefinitionListener l : listeners) {
                        l.cvDefinitionValuesChanged(read);
                    }
                }
            });
        }
    }

    /**
     * Update all port lists with the values from the model
     */
    public void updatePortLists() {
        LOGGER.info("updatePortLists.");

        // TODO clear the caches

        // notify the listeners
        fireAnalogPortListChanged();
        fireBacklightPortListChanged();
        fireInputPortListChanged();
        fireLightPortListChanged();
        // fireMotorPortListChanged();

        fireSoundPortListChanged();
        fireSwitchPortListChanged();
        fireSwitchPairPortListChanged();

        if (selectedNode != null) {

            if (ProductUtils.isOneControl(selectedNode.getUniqueId())
                || ProductUtils.isOneDriveTurn(selectedNode.getUniqueId())) {
                int gpioId = 0;
                for (InputPort inputPort : getInputPorts()) {
                    inputPort.setPortIdentifier("GPIO " + gpioId++);
                }
                gpioId = 0;
                for (SwitchPort switchPort : getSwitchPorts()) {
                    LOGGER.debug("Current switch port: {}", switchPort);
                    if (!switchPort.isRemappingEnabled()
                        || !PortMapUtils.supportsPortType(LcOutputType.INPUTPORT, switchPort.getPortConfigX())) {
                        continue;
                    }
                    switchPort.setPortIdentifier("GPIO " + gpioId++);
                }
            }

            selectedNode.updatePortLists();
        }
    }

    public void signalInitialLoadFinished() {
        LOGGER.info("The initial load has finished.");
        initialLoadFinished.set(true);
    }

    public void signalResetInitialLoadFinished() {
        LOGGER.info("The initial load is reset.");
        initialLoadFinished.set(false);
    }

    public boolean isInitialLoadFinished() {
        return initialLoadFinished.get();
    }
}
