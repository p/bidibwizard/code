package org.bidib.wizard.mvc.preferences.controller;

import java.io.File;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.SystemUtils;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.bidib.jbidibc.core.exception.InvalidLibraryException;
import org.bidib.jbidibc.rxtx.PortIdentifierUtils;
import org.bidib.jbidibc.scm.ScmPortIdentifierUtils;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.common.model.CommPort;
import org.bidib.wizard.mvc.common.model.PreferencesPortType;
import org.bidib.wizard.mvc.preferences.model.Preferences;
import org.bidib.wizard.mvc.preferences.model.PreferencesModel;
import org.bidib.wizard.mvc.preferences.view.PreferencesView;
import org.bidib.wizard.mvc.preferences.view.listener.PreferencesViewListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PreferencesController {
    private static final Logger LOGGER = LoggerFactory.getLogger(PreferencesController.class);

    private final JFrame parent;

    private PreferencesModel model;

    public PreferencesController(JFrame parent) {
        this.parent = parent;
    }

    private void loadCommPorts() {
        if (model.isSerialEnabled()) {
            LOGGER.info("Load the comm ports, model: {}", model);
            Set<CommPort> commPorts = new HashSet<CommPort>();

            try {
                // use PortIdentifierUtils because we must load the RXTX libraries
                List<String> portIdentifiers = null;
                if ("SCM".equals(Preferences.getInstance().getSerialPortProvider())) {
                    portIdentifiers = ScmPortIdentifierUtils.getPortIdentifiers();
                }
                else {
                    portIdentifiers = PortIdentifierUtils.getPortIdentifiers();
                }

                if (portIdentifiers != null) {
                    for (String id : portIdentifiers) {
                        LOGGER.info("Add new CommPort with id: {}", id);
                        commPorts.add(new CommPort(id));
                    }
                }

                if (StringUtils.isNotBlank(model.getPreviousSelectedComPort())) {
                    LOGGER.info("Add the previous selected COM Port: {}", model.getPreviousSelectedComPort());
                    commPorts.add(new CommPort(model.getPreviousSelectedComPort().trim()));
                }
            }
            catch (InvalidLibraryException ex) {
                LOGGER.warn(
                    "Fetch port identifiers failed. This can be caused because the ext/lib directory of the Java installation contains an old RXTXComm.jar!",
                    ex);

                JOptionPane.showMessageDialog(parent,
                    Resources.getString(PreferencesController.class, "fetch-port-identifiers-failed",
                        new Object[] { new File(SystemUtils.getJavaHome(), "lib/ext").getPath() }),
                    Resources.getString(PreferencesController.class, "title-error"), JOptionPane.ERROR_MESSAGE);
            }
            catch (Exception ex) {
                LOGGER.warn("Fetch port identifiers failed.", ex);
            }
            model.setCommPorts(commPorts);
        }
        else {
            LOGGER.info("No comm ports loaded because serial enabled flag is not set.");
        }
    }

    public void start() {
        LOGGER.debug("Start the preferences controller.");
        model = Preferences.getInstance().createPreferencesModel();

        if (model.isSerialEnabled()) {
            LOGGER.info("Load the comm ports of the system.");
            loadCommPorts();
        }

        PreferencesView view = new PreferencesView(parent, model);

        view.addPreferencesViewListener(new PreferencesViewListener() {
            @Override
            public void selectedPortTypeChanged(PreferencesPortType portType) {
                LOGGER.info("The selected port type has changed: {}", portType);

                // set the new selected port type in the model
                model.setSelectedPortType(portType);
            }

            @Override
            public void serialEnabledChanged(boolean serialEnabled) {
                LOGGER.info("serialEnabled has changed: {}", serialEnabled);

                // set the new serialEnabled value in the model
                model.setSerialEnabled(serialEnabled);
            }

            @Override
            public void udpEnabledChanged(boolean udpEnabled) {
                LOGGER.info("udpEnabled has changed: {}", udpEnabled);

                // set the new udp value in the model
                model.setUdpEnabled(udpEnabled);
            }

            @Override
            public void tcpEnabledChanged(boolean tcpEnabled) {
                LOGGER.info("tcpEnabled has changed: {}", tcpEnabled);

                // set the new tcp value in the model
                model.setTcpEnabled(tcpEnabled);
            }

            @Override
            public void plainTcpEnabledChanged(boolean plainTcpEnabled) {
                LOGGER.info("plainTcpEnabled has changed: {}", plainTcpEnabled);

                // set the new plain tcp value in the model
                model.setPlainTcpEnabled(plainTcpEnabled);
            }

            @Override
            public void save() {
                LOGGER.info("Save the preferences values.");

                // save the mode in the preferences
                Preferences.getInstance().save(model);

                // reload the logger configuration because the path to the log file might have changed
                Preferences.getInstance().reloadLoggerConfiguration(false);
            }

            @Override
            public void cancel() {
            }

            @Override
            public void startTimeChanged(Date startTime) {
                // set the new start time in the model
                model.setStartTime(startTime);
            }

            @Override
            public void timeFactorChanged(int timeFactor) {
                // set the new time factor in the model
                model.setTimeFactor(timeFactor);
            }

            @Override
            public void powerUserChanged(boolean powerUser) {
                // set the new powerUser flag in the model
                model.setPowerUser(powerUser);
            }

            @Override
            public void selectedSerialSymLinkChanged(String serialSymLink) {
                model.setPreviousSelectedSerialSymLink(serialSymLink);
            }

            @Override
            public void selectedUdpHostChanged(String udpHost) {
                model.setPreviousSelectedUdpHost(udpHost);
            }

            @Override
            public void selectedTcpHostChanged(String tcpHost) {
                model.setPreviousSelectedTcpHost(tcpHost);
            }

            @Override
            public void selectedComPortChanged(String comPort) {
                model.setPreviousSelectedComPort(comPort);
            }

            @Override
            public void showBoosterTableChanged(boolean showBoosterTable) {
                // set the new showBoosterTable flag in the model
                model.setShowBoosterTable(showBoosterTable);
            }

            @Override
            public void alwaysShowProductNameInTreeChanged(boolean alwaysShowProductNameInTree) {
                // set the new alwaysShowProductNameInTree flag in the model
                model.setAlwaysShowProductNameInTree(alwaysShowProductNameInTree);
            }

            @Override
            public void ignoreWaitTimeoutChanged(boolean ignoreWaitTimeout) {
                // set the new ignoreWaitTimeout flag in the model
                model.setIgnoreWaitTimeout(ignoreWaitTimeout);
            }

            @Override
            public void ignoreWrongReceiveMessageNumberChanged(boolean ignoreWrongReceiveMessageNumber) {
                // set the new ignoreWrongReceiveMessageNumber flag in the model
                model.setIgnoreWrongReceiveMessageNumber(ignoreWrongReceiveMessageNumber);
            }

            @Override
            public void ignoreFlowControlChanged(boolean ignoreFlowControl) {
                // set the new ignoreFlowControl flag in the model
                model.setIgnoreFlowControl(ignoreFlowControl);
            }

            @Override
            public void useHotPlugControllerChanged(boolean useHotPlugController) {
                // set the new useHotPlugController flag in the model
                model.setUseHotPlugController(useHotPlugController);
            }

            @Override
            public void logFileAppendChanged(boolean logFileAppend) {
                // set the new logFileAppend flag in the model
                model.setLogFileAppend(logFileAppend);
            }

            @Override
            public void serialUseHardwareFlowControlChanged(boolean serialUseHardwareFlowControl) {
                // set the new serialUseHardwareFlowControl flag in the model
                model.setSerialUseHardwareFlowControl(serialUseHardwareFlowControl);
            }

            @Override
            public void propertyChanged(String propertyName, Object value) {

                try {
                    FieldUtils.writeDeclaredField(model, propertyName, value, true);
                }
                catch (Exception ex) {
                    LOGGER.warn("Ser the property value failed.", ex);
                }

            }
        });
    }
}
