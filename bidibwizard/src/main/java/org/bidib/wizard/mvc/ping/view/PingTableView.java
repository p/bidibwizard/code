package org.bidib.wizard.mvc.ping.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.util.EventObject;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListModel;
import javax.swing.SwingUtilities;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.common.view.DockKeys;
import org.bidib.wizard.mvc.common.view.table.CustomBooleanCellEditor;
import org.bidib.wizard.mvc.common.view.table.CustomBooleanCellRenderer;
import org.bidib.wizard.mvc.main.view.table.AbstractEmptyTable;
import org.bidib.wizard.mvc.ping.model.NodePingModel;
import org.bidib.wizard.mvc.ping.model.PingTableModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.adapter.SingleListSelectionAdapter;
import com.jgoodies.binding.list.SelectionInList;
import com.vlsolutions.swing.docking.DockKey;
import com.vlsolutions.swing.docking.Dockable;

public class PingTableView implements Dockable {
    private static final Logger LOGGER = LoggerFactory.getLogger(PingTableView.class);

    private final JPanel contentPanel;

    private SelectionInList<NodePingModel> nodeSelection;

    public PingTableView(PingTableModel pingTableModel) {

        DockKeys.DOCKKEY_PING_TABLE_VIEW.setName(Resources.getString(getClass(), "title"));
        DockKeys.DOCKKEY_PING_TABLE_VIEW.setFloatEnabled(true);
        DockKeys.DOCKKEY_PING_TABLE_VIEW.setAutoHideEnabled(false);

        LOGGER.info("Create new PingTableView");

        contentPanel = new JPanel();
        contentPanel.setLayout(new BorderLayout());

        nodeSelection =
            new SelectionInList<NodePingModel>((ListModel<NodePingModel>) pingTableModel.getNodeListModel());

        TableModel tableModel = new PingTableTableModel(nodeSelection);

        // create a booster table
        AbstractEmptyTable pingTable =
            new AbstractEmptyTable(tableModel, Resources.getString(getClass(), "empty_table")) {
                private static final long serialVersionUID = 1L;

                @Override
                public boolean isSkipPackColumn() {
                    return true;
                }

                @Override
                public boolean editCellAt(int row, int column, EventObject e) {
                    boolean isEditing = super.editCellAt(row, column, e);
                    LOGGER.info("isEditing: {}", isEditing);

                    // this is necessary for the polarity toggle button to work correct
                    if (column == PingTableTableModel.COLUMN_STATUS) {
                        SwingUtilities.invokeLater(new Runnable() {
                            public void run() {
                                if (getCellEditor() != null) {
                                    getCellEditor().stopCellEditing();
                                }
                            }
                        });
                    }
                    return isEditing;
                }
            };

        pingTable.setSelectionModel(new SingleListSelectionAdapter(nodeSelection.getSelectionIndexHolder()));
        pingTable.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);

        TableColumn tc = pingTable.getColumn(PingTableTableModel.COLUMN_UNIQUE_ID);
        tc.setMinWidth(120);
        tc.setMaxWidth(150);
        tc.setPreferredWidth(150);
        tc = pingTable.getColumnModel().getColumn(PingTableTableModel.COLUMN_DESCRIPTION);
        tc.setPreferredWidth(300);
        // tc = pingTable.getColumnModel().getColumn(PingTableTableModel.COLUMN_STATUS);
        // tc.setCellRenderer(new PingStateCellRenderer());
        // tc.setMaxWidth(60);
        // TODO set more renderers
        tc = pingTable.getColumnModel().getColumn(PingTableTableModel.COLUMN_STATUS);
        tc.setCellEditor(new CustomBooleanCellEditor("/icons/accessory-successful.png", "/icons/cross.png"));
        tc.setCellRenderer(new CustomBooleanCellRenderer("/icons/accessory-successful.png", "/icons/cross.png"));
        tc.setMaxWidth(60);

        // Highlighter simpleStriping = HighlighterFactory.createSimpleStriping();
        // pingTable.setHighlighters(simpleStriping);

        JScrollPane scrollPane = new JScrollPane(pingTable);
        contentPanel.add(scrollPane, BorderLayout.CENTER);
    }

    @Override
    public DockKey getDockKey() {
        return DockKeys.DOCKKEY_PING_TABLE_VIEW;
    }

    @Override
    public Component getComponent() {
        return contentPanel;
    }

}
