package org.bidib.wizard.mvc.main.view.panel;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.LinkedList;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JToggleButton;

import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.main.model.StatusModel;
import org.bidib.wizard.mvc.main.model.listener.DefaultStatusListener;
import org.bidib.wizard.mvc.main.view.panel.listener.StatusListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StatusButtonPanel extends JPanel {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(StatusButtonPanel.class);

    private Collection<StatusListener> statusListeners = new LinkedList<StatusListener>();

    public enum Axis {
        lineAxis, pageAxis
    }

    private final JToggleButton onButton;

    public StatusButtonPanel(StatusModel model, Axis axis) {

        onButton = new JToggleButton();

        onButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JToggleButton button = (JToggleButton) e.getSource();

                if (button.isSelected()) {
                    fireSwitchedOn();
                }
                else {
                    fireSwitchedOff();
                }
            }
        });
        onButton.setSelected(model.isRunning());
        updateButton(model.isRunning());

        if (Axis.pageAxis.equals(axis)) {
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();

            c.anchor = GridBagConstraints.CENTER;
            c.fill = GridBagConstraints.BOTH;
            c.gridx = 0;
            c.insets = new Insets(5, 5, 5, 5);
            add(onButton, c);

            Dimension size = onButton.getPreferredSize();
            onButton.setPreferredSize(new Dimension(60, size.height));
        }
        else {
            onButton.setMargin(new Insets(0, 5, 0, 5));
            add(onButton);
        }

        model.addStatusListener(new DefaultStatusListener() {

            @Override
            public void runningChanged(boolean running) {
                updateButton(running);
            }
        });
    }

    private void updateButton(boolean running) {
        if (running) {
            onButton.setText(Resources.getString(StatusButtonPanel.class, "on"));
            onButton.setToolTipText(Resources.getString(StatusButtonPanel.class, "on-tooltip"));
        }
        else {
            onButton.setText(Resources.getString(StatusButtonPanel.class, "off"));
            onButton.setToolTipText(Resources.getString(StatusButtonPanel.class, "off-tooltip"));
        }
    }

    public void setTitleBorder() {
        setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
            Resources.getString(getClass(), "title") + ":"));
    }

    public void setButtonHeight(int height) {
        LOGGER.info("Set the button height: {}", height);

        onButton.setPreferredSize(new Dimension(60, height));
    }

    public JToggleButton getButton() {
        return onButton;
    }

    public void addStatusListener(StatusListener l) {
        statusListeners.add(l);
    }

    private void fireSwitchedOff() {
        for (StatusListener l : statusListeners) {
            l.switchedOff();
        }
    }

    private void fireSwitchedOn() {
        for (StatusListener l : statusListeners) {
            l.switchedOn();
        }
    }
}
