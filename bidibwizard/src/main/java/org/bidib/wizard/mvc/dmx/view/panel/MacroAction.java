package org.bidib.wizard.mvc.dmx.view.panel;

import java.awt.Point;
import java.awt.event.ActionEvent;

import org.bidib.wizard.comm.MacroStatus;
import org.bidib.wizard.mvc.dmx.model.DmxChannel;
import org.bidib.wizard.mvc.main.model.Macro;
import org.bidib.wizard.mvc.main.model.function.MacroFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The <code>PortAction</code> creates a point for the dmxChannel with a port assigned.
 */
public class MacroAction extends LocationAwareAction<DmxChannel> {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(MacroAction.class);

    private Macro macro;

    private DmxChannel dmxChannel;

    private DmxDataItem originalDataItem;

    public MacroAction(Macro macro, DmxChannel dmxChannel, DmxChartPanel dmxChartPanel, DmxDataItem originalDataItem) {
        super(macro.toString(), dmxChannel, dmxChartPanel);
        this.macro = macro;
        this.dmxChannel = dmxChannel;
        this.originalDataItem = originalDataItem;
    }

    @Override
    public void actionPerformed(ActionEvent event) {

        Point currentMousePoint = dmxChartPanel.getCurrentMousePoint();

        String seriesKey = Integer.toString(dmxChannel.getChannelId());
        LOGGER.info("Selected key: {}, currentMousePoint: {}", seriesKey, currentMousePoint);

        int currentX = (int) Math.round(currentMousePoint.getX());
        int currentY = (int) Math.round(currentMousePoint.getY());

        if (originalDataItem != null) {
            LOGGER.info("Use the coordinates of the original data item: {}", originalDataItem);
            currentX = originalDataItem.getTimeOffset();
            currentY = originalDataItem.getBrightness();
        }

        LOGGER.info("Add new point at X: {}, Y: {}", currentX, currentY);

        MacroFunction macroFunction = new MacroFunction(MacroStatus.START, macro.getId());
        dmxChartPanel.createDataItem(seriesKey, currentX, currentY, macroFunction, dmxChannel, originalDataItem);
    }
}
