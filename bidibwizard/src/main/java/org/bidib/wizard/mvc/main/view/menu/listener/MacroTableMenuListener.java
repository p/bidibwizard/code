package org.bidib.wizard.mvc.main.view.menu.listener;

public interface MacroTableMenuListener {
    void copy();

    void cut();

    void delete();

    void insertEmptyAfter();

    void insertEmptyBefore();

    void pasteAfter();

    void pasteBefore();

    void selectAll();

    void pasteInvertedAfter();
}
