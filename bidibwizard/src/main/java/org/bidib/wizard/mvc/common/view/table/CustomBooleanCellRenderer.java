package org.bidib.wizard.mvc.common.view.table;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

public class CustomBooleanCellRenderer extends CustomCheckBox implements TableCellRenderer {

    private static final long serialVersionUID = 1L;

    public CustomBooleanCellRenderer(String pathSelectedIcon, String pathUnselectedIcon) {
        super(pathSelectedIcon, pathUnselectedIcon);
    }

    @Override
    public Component getTableCellRendererComponent(
        JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        if (value instanceof Boolean) {
            boolean selected = (boolean) value;
            setSelected(selected);
        }
        if (isSelected) {
            setForeground(table.getSelectionForeground());
            setBackground(table.getSelectionBackground());
        }
        else {
            setForeground(table.getForeground());
            setBackground(table.getBackground());
        }

        return this;
    }

}
