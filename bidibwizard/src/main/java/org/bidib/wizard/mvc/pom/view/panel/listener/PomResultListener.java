package org.bidib.wizard.mvc.pom.view.panel.listener;

import org.bidib.jbidibc.core.enumeration.PomProgState;

public interface PomResultListener {

    /**
     * Sets the listener active or inactive.
     * 
     * @param active
     *            the active flag
     */
    void setActive(boolean active);

    /**
     * @return listener is active
     */
    boolean isActive();

    /**
     * Add a new line to the logger area.
     * 
     * @param logLine
     *            the message to log. Use {} as placeholders for args.
     * @param args
     *            the args
     */
    void addLogText(String logLine, Object... args);

    /**
     * Signals that the POM prog state has changed.
     * 
     * @param pomProgState
     *            the new POM prog state
     */
    void signalPomProgStateChanged(PomProgState pomProgState);
}
