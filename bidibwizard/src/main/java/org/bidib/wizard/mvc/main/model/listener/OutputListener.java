package org.bidib.wizard.mvc.main.model.listener;

import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.mvc.main.model.Port;

public interface OutputListener<S extends BidibStatus> extends PortListener<S> {
    /**
     * The test button was pressed
     * 
     * @param port
     *            the port
     */
    void testButtonPressed(Port<S> port);
}
