package org.bidib.wizard.mvc.main.view.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.LinkedList;

import javax.swing.JMenuItem;

import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.main.view.menu.listener.AccessoryListMenuListener;

public class AccessoryListMenu extends BasicPopupMenu {
    private static final long serialVersionUID = 1L;

    private final Collection<AccessoryListMenuListener> menuListeners = new LinkedList<AccessoryListMenuListener>();

    private final JMenuItem editLabel;

    private final JMenuItem importAccessory;

    private final JMenuItem exportAccessory;

    private final JMenuItem initialize;

    public AccessoryListMenu() {
        editLabel = new JMenuItem(Resources.getString(getClass(), "editLabel") + " ...");

        editLabel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fireEditLabel();
            }
        });
        add(editLabel);

        addSeparator();

        importAccessory = new JMenuItem(Resources.getString(getClass(), "import") + " ...");

        importAccessory.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fireImport();
            }
        });
        add(importAccessory);

        exportAccessory = new JMenuItem(Resources.getString(getClass(), "export") + " ...");

        exportAccessory.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fireExport();
            }
        });
        add(exportAccessory);

        addSeparator();

        initialize = new JMenuItem(Resources.getString(getClass(), "initialize"));

        initialize.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fireInitialize();
            }
        });
        initialize.setToolTipText(Resources.getString(getClass(), "initialize.tooltip"));
        add(initialize);
    }

    public void addMenuListener(AccessoryListMenuListener l) {
        menuListeners.add(l);
    }

    private void fireEditLabel() {
        for (AccessoryListMenuListener l : menuListeners) {
            l.editLabel();
        }
    }

    private void fireExport() {
        for (AccessoryListMenuListener l : menuListeners) {
            l.exportAccessory();
        }
    }

    private void fireImport() {
        for (AccessoryListMenuListener l : menuListeners) {
            l.importAccessory();
        }
    }

    private void fireInitialize() {
        for (AccessoryListMenuListener l : menuListeners) {
            l.initializeAccessory();
        }
    }

    public void setEnabled(boolean enabled) {
        editLabel.setEnabled(enabled);
        importAccessory.setEnabled(enabled);
        exportAccessory.setEnabled(enabled);
        initialize.setEnabled(enabled);
    }
}
