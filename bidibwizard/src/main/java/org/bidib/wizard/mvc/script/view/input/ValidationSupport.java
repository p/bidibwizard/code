package org.bidib.wizard.mvc.script.view.input;

public interface ValidationSupport {

    /**
     * Trigger the validation.
     */
    void triggerValidation();

    /**
     * Add a panel to be validated.
     * 
     * @param panel
     *            panel to be validated
     */
    void addValidationComponent(AbstractInputSelectionPanel panel);

    /**
     * Remove a panel to be validated.
     * 
     * @param panel
     *            panel to be validated
     */
    void removeValidationComponent(AbstractInputSelectionPanel panel);
}
