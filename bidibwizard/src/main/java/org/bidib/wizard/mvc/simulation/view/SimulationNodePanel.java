package org.bidib.wizard.mvc.simulation.view;

import org.bidib.jbidibc.simulation.SimulatorNode;
import org.bidib.wizard.mvc.main.model.Node;

import com.vlsolutions.swing.docking.Dockable;
import com.vlsolutions.swing.docking.event.DockableStateChangeListener;

public interface SimulationNodePanel extends Dockable, DockableStateChangeListener {

    void createComponents(SimulatorNode simulator);

    Node getNode();
}
