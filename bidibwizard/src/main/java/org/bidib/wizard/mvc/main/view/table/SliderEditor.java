package org.bidib.wizard.mvc.main.view.table;

import java.awt.Component;
import java.awt.Dimension;

import javax.swing.AbstractCellEditor;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import org.bidib.jbidibc.ui.LogarithmicJSlider;
import org.bidib.wizard.mvc.common.view.slider.SliderValueChangeListener;
import org.bidib.wizard.mvc.main.model.BacklightPort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SliderEditor extends AbstractCellEditor implements TableCellEditor, TableCellRenderer {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(SliderEditor.class);

    protected final JPanel panel;

    protected JSlider slider;

    protected SliderValueLabel sliderValue;

    private SliderValueChangeListener sliderValueChangeListener;

    protected int minValue;

    protected int maxValue;

    private boolean useLogarithmicSlider;

    protected class SliderValueLabel extends JLabel {
        private static final long serialVersionUID = 1L;

        @Override
        public void setText(String text) {
            LOGGER.debug("Set the text of slider value: {}", text);
            super.setText(text);
        }
    }

    public SliderEditor() {
        this(0, 255, null);
    }

    public SliderEditor(int minValue) {
        this(minValue, 255, null);
    }

    public SliderEditor(int minValue, boolean useLogarithmicSlider) {
        this(minValue, 255, null);
        this.useLogarithmicSlider = useLogarithmicSlider;
    }

    /**
     * Creates a new slider editor instance with the minimum value as initial value set.
     * 
     * @param minValue
     *            the minimum value
     * @param maxValue
     *            the maximum value
     * @param changeListener
     *            the change listener
     */
    public SliderEditor(int minValue, int maxValue, final SliderValueChangeListener changeListener) {
        this.minValue = minValue;
        this.maxValue = maxValue;

        panel = new JPanel();
        panel.setFocusCycleRoot(true);

        sliderValueChangeListener = changeListener;
    }

    /*
     * Create the component and initialize the labels. This method must be called before the component can be used.
     * 
     * @param initialValue the initial value
     */
    public void createComponent(int initialValue) {
        LOGGER.debug("Prepare slider with initialValue: {}, minValue: {}, maxValue: {}", initialValue, minValue,
            maxValue);

        if (initialValue < minValue) {
            initialValue = minValue;
        }

        // if (!useLogarithmicSlider) {
        // slider = new JSlider(minValue, maxValue, initialValue);
        // }
        // else {
        // slider = new LogarithmicJSlider(minValue, maxValue, initialValue);
        // }
        createJSlider(minValue, maxValue, initialValue);

        slider.setOpaque(false);
        slider.setExtent(1);
        sliderValue = new SliderValueLabel();

        slider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                JSlider source = (JSlider) e.getSource();

                updateSliderTextValue(source.getValue(), true);

                if (!source.getValueIsAdjusting()) {
                    stopCellEditing();
                    if (sliderValueChangeListener != null) {
                        // forward event
                        sliderValueChangeListener.stateChanged(e, false, source.getValue());
                    }
                }
                else {
                    if (sliderValueChangeListener != null) {
                        // forward event
                        sliderValueChangeListener.stateChanged(e, true, source.getValue());
                    }
                }

            }
        });

        prepareSliderTextLabel(maxValue, initialValue);

        panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
        panel.add(slider);

        panel.add(Box.createRigidArea(new Dimension(5, 0)));
        panel.add(sliderValue);
        panel.add(Box.createRigidArea(new Dimension(3, 0)));

        panel.validate();
    }

    protected void createJSlider(int minValue, int maxValue, int initialValue) {
        if (!useLogarithmicSlider) {
            slider = new JSlider(minValue, maxValue, initialValue);
        }
        else {
            slider = new LogarithmicJSlider(minValue, maxValue, initialValue);
        }
    }

    protected void prepareSliderTextLabel(int maxValue, int initialValue) {
        sliderValue.setHorizontalAlignment(SwingConstants.TRAILING);

        sliderValue.setText(Integer.toString(maxValue));

        Dimension d = sliderValue.getMinimumSize();
        LOGGER.debug("Set the initial value: {}", initialValue);
        sliderValue.setText(Integer.toString(initialValue));

        LOGGER.trace("Current dimension: {}", d);

        sliderValue.setPreferredSize(d);
        sliderValue.setMaximumSize(d);
        sliderValue.setMinimumSize(d);
    }

    /**
     * @param value
     *            the relative value to set
     */
    protected void updateSliderTextValue(int value, boolean isSelected) {

        String newValue = Integer.toString(value);
        LOGGER.trace("Set the new value: {}", newValue);
        sliderValue.setText(newValue);
    }

    public void setSliderValueChangeListener(SliderValueChangeListener sliderValueChangeListener) {
        this.sliderValueChangeListener = sliderValueChangeListener;
    }

    public void setMaxValue(int maxValue) {
        this.maxValue = maxValue;
        if (slider != null) {
            slider.setMaximum(maxValue);
        }
    }

    public void setMinValue(int minValue) {
        this.minValue = minValue;
        if (slider != null) {
            slider.setMinimum(minValue);
        }
    }

    @Override
    public Object getCellEditorValue() {
        return new Integer(slider.getValue());
    }

    @Override
    public boolean stopCellEditing() {
        return super.stopCellEditing();
    }

    @Override
    protected void fireEditingStopped() {
        super.fireEditingStopped();
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        if (isSelected) {
            slider.setForeground(table.getSelectionForeground());
            slider.setBackground(table.getSelectionBackground());
        }
        else {
            slider.setForeground(table.getForeground());
            slider.setBackground(table.getBackground());
        }
        LOGGER.info("Set the new value for table cell editor: {}, row: {}, column: {}", value, row, column);

        slider.setValue((Integer) value);
        return panel;
    }

    @Override
    public Component getTableCellRendererComponent(
        JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        if (isSelected) {
            slider.setForeground(table.getSelectionForeground());
            slider.setBackground(table.getSelectionBackground());
            panel.setForeground(table.getSelectionForeground());
            panel.setBackground(table.getSelectionBackground());
            sliderValue.setForeground(table.getSelectionForeground());
        }
        else {
            slider.setForeground(table.getForeground());
            slider.setBackground(table.getBackground());
            panel.setForeground(table.getForeground());
            panel.setBackground(table.getBackground());
            sliderValue.setForeground(table.getForeground());
        }
        LOGGER.trace("Set the new value for table cell renderer: {}", value);

        if (value instanceof BacklightPort) {
            BacklightPort backlightPort = (BacklightPort) value;
            int portValue = backlightPort.getValue();
            slider.setValue(portValue);
            updateSliderTextValue(portValue, isSelected);
        }
        else {
            slider.setValue((Integer) value);
            updateSliderTextValue((Integer) value, isSelected);
        }

        slider.updateUI();
        return panel;
    }

    public void setDown() {
        LOGGER.trace("set slider value down: {}", slider.getValue());
        slider.setValue(slider.getValue() - 1);
    }

    public void setUp() {
        LOGGER.trace("set slider value up: {}", slider.getValue());
        slider.setValue(slider.getValue() + 1);
    }

    public void setValue(int value) {
        LOGGER.debug("Set the new value: {}", value);
        slider.getModel().setValueIsAdjusting(true);
        slider.setValue(value);
        sliderValue.setText(Integer.toString(value));
        slider.getModel().setValueIsAdjusting(false);
    }

    public void setEnabled(boolean enabled) {
        panel.setEnabled(enabled);
        slider.setEnabled(enabled);
        sliderValue.setEnabled(enabled);
    }
}
