package org.bidib.wizard.mvc.common.model;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PreferencesPortType {

    public enum ConnectionPortType {
        SerialPort(null), SerialSymLink(null), SerialSimulation("serial"), UdpPort(null), UdpSimulation("udp"), TcpPort(
            null), TcpSimulation("tcp"), SerialOverTcpPort(null), SerialOverTcpSimulation("plaintcp");

        private String connectionName;

        private ConnectionPortType(String connectionName) {
            this.connectionName = connectionName;
        }

        /**
         * @return the connectionName
         */
        public String getConnectionName() {
            return connectionName;
        }

    }

    private static final Logger LOGGER = LoggerFactory.getLogger(PreferencesPortType.class);

    private final ConnectionPortType connectionPortType;

    private String connectionName;

    public PreferencesPortType(ConnectionPortType connectionPortType) {
        this(connectionPortType, null);
    }

    public PreferencesPortType(ConnectionPortType connectionPortType, String connectionName) {
        this.connectionPortType = connectionPortType;
        this.connectionName = connectionName;
    }

    /**
     * @return the connectionName
     */
    public String getConnectionName() {

        if (isSimulation(this)) {
            return connectionPortType.connectionName;
        }
        return connectionName;
    }

    /**
     * @return the connection port type
     */
    public ConnectionPortType getConnectionPortType() {
        return connectionPortType;
    }

    /**
     * @param connectionName
     *            the connectionName to set
     */
    public void setConnectionName(String connectionName) {
        this.connectionName = connectionName;
    }

    public static boolean isSimulation(PreferencesPortType portType) {
        if (portType != null) {
            switch (portType.connectionPortType) {
                case SerialSimulation:
                case UdpSimulation:
                case TcpSimulation:
                case SerialOverTcpSimulation:
                    return true;
                default:
                    break;
            }
        }
        return false;
    }

    /**
     * Validates that the portType is not a simulation and that the connection name is valid to connect to a BiDiB
     * system.
     * 
     * @param portType
     *            the port type
     * @return connection is valid to connect to BiDiB system
     */
    public static boolean isValidConnectionConfiguration(PreferencesPortType portType) {
        return !PreferencesPortType.isSimulation(portType) && StringUtils.isNotEmpty(portType.getConnectionName())
            && !"<none>".equals(portType.getConnectionName());
    }

    public static PreferencesPortType getValue(String value) {
        LOGGER.debug("Get the PreferencesPortType for value: {}", value);
        PreferencesPortType portType = null;
        for (ConnectionPortType e : ConnectionPortType.values()) {
            if (value.startsWith(e.name())) {
                LOGGER.debug("Create new PreferencesPortType with ConnectionPortType: {}", e);
                portType = new PreferencesPortType(e);
                break;
            }
        }

        // check if we must set the connection name
        if (portType != null && !isSimulation(portType)) {
            portType.setConnectionName(null);
            int index = value.indexOf(":");
            if (index > 1 && (index + 1) < value.length()) {
                portType.setConnectionName(value.substring(index + 1));
            }
        }
        LOGGER.debug("Prepared portType: {}", portType);
        return portType;
    }

    @Override
    public int hashCode() {
        int hash = connectionPortType.hashCode();
        if (connectionName != null) {
            hash += connectionName.hashCode();
        }
        return hash;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof PreferencesPortType) {
            if (connectionPortType.equals(((PreferencesPortType) other).getConnectionPortType())) {

                return StringUtils.equals(getConnectionName(), ((PreferencesPortType) other).getConnectionName());
            }
            return false;
        }
        return false;
    }

    @Override
    public String toString() {
        if (isSimulation(this)) {
            return connectionPortType.name();
        }
        return connectionPortType.name() + ":" + connectionName;
    }
}
