package org.bidib.wizard.mvc.common.view.cvdefinition;

import java.awt.Color;

import org.bidib.jbidibc.exchange.vendorcv.ModeType;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.main.view.cvdef.CvNode;
import org.bidib.wizard.mvc.main.view.cvdef.LongNodeNode;
import org.bidib.wizard.utils.XmlLocaleUtils;

import com.jidesoft.grid.CellStyle;
import com.jidesoft.grid.DefaultExpandableRow;
import com.jidesoft.grid.Row;
import com.jidesoft.grid.StyleModel;
import com.jidesoft.grid.TreeTableModel;

public class CvDefinitionTreeTableModel extends TreeTableModel<DefaultExpandableRow> implements StyleModel {

    private static final long serialVersionUID = 1L;

    private final String[] columnNames;

    private CellStyle styleLongNode = new CellStyle();

    private CellStyle styleReadOnly = new CellStyle();

    private CellStyle styleTimeout = new CellStyle();

    private CellStyle styleChangedValue = new CellStyle();

    private CellStyle styleDefault = new CellStyle();

    private final String lang;

    public CvDefinitionTreeTableModel() {
        super();

        this.columnNames =
            new String[] { Resources.getString(getClass(), "column.description"),
                Resources.getString(getClass(), "column.cv"), Resources.getString(getClass(), "column.value"),
                Resources.getString(getClass(), "column.newvalue"), Resources.getString(getClass(), "column.mode") };

        lang = XmlLocaleUtils.getXmlLocaleVendorCV();

        styleLongNode.setBackground(new Color(255, 242, 0));
        styleReadOnly.setBackground(Color.LIGHT_GRAY);
        styleChangedValue.setBackground(new Color(255, 201, 14));
        styleTimeout.setBackground(new Color(172, 216, 230));
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public boolean isCellStyleOn() {
        return true;
    }

    @Override
    public CellStyle getCellStyleAt(int row, int column) {
        Row rowAt = getRowAt(row);
        CellStyle style = styleDefault;

        if (rowAt instanceof LongNodeNode) {
            style = styleLongNode;
        }
        else if (rowAt instanceof CvNode) {
            CvNode cvNode = (CvNode) rowAt;

            if (cvNode.getConfigVar().isTimeout()) {
                style = styleTimeout;
            }
            else if (ModeType.RO.equals(cvNode.getCV().getMode())) {
                style = styleReadOnly;
            }
            else if (cvNode.getNewValue() != null) {
                style = styleChangedValue;
            }

            String toolTipText = CvNode.getDescriptionHelp(cvNode.getCV().getDescription(), lang);
            style.setToolTipText(toolTipText);

        }
        else {
            style.setToolTipText(null);
        }

        return style;
    }
}
