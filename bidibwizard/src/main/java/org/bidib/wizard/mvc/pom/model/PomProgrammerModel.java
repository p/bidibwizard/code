package org.bidib.wizard.mvc.pom.model;

import java.util.LinkedList;
import java.util.List;

import javax.swing.SwingUtilities;

import org.bidib.jbidibc.core.AddressData;
import org.bidib.jbidibc.core.enumeration.CommandStationState;
import org.bidib.jbidibc.core.enumeration.PomProgState;
import org.bidib.wizard.mvc.pom.model.listener.ConfigVariableListener;
import org.bidib.wizard.mvc.pom.model.listener.ProgCommandListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.beans.Model;

public class PomProgrammerModel extends Model {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(PomProgrammerModel.class);

    public static final String PROPERTYNAME_COMMANDSTATIONSTATE = "commandStationState";

    public static final String PROPERTYNAME_POMPROGSTATE = "pomProgState";

    public static final String PROPERTYNAME_CVNUMBER = "cvNumber";

    public static final String PROPERTYNAME_CVVALUE = "cvValue";

    private final List<ConfigVariableListener> listeners = new LinkedList<ConfigVariableListener>();

    private final List<ProgCommandListener> progCommandListeners = new LinkedList<ProgCommandListener>();

    private int cvNumber = 1;

    private Integer cvValue;

    private CommandStationState commandStationState;

    private PomProgState pomProgState;

    public void addConfigVariableListener(ConfigVariableListener l) {
        listeners.add(l);
    }

    public void removeConfigVariableListener(ConfigVariableListener l) {
        listeners.remove(l);
    }

    public void addProgCommandListener(ProgCommandListener l) {
        progCommandListeners.add(l);
    }

    public void removeProgCommandListener(ProgCommandListener l) {
        progCommandListeners.remove(l);
    }

    /**
     * @return the CV number
     */
    public int getCvNumber() {
        return cvNumber;
    }

    /**
     * @param cvNumber
     *            the CV number to set
     */
    public void setCvNumber(int cvNumber) {
        int oldNumber = this.cvNumber;
        this.cvNumber = cvNumber;
        firePropertyChange(PROPERTYNAME_CVNUMBER, oldNumber, cvNumber);
    }

    public Integer getCvValue() {
        return cvValue;
    }

    public void setCvValue(Integer value) {
        LOGGER.debug("Set the CV value: {}", value);
        // if (this.cvValue != value) {
        Integer oldValue = cvValue;
        this.cvValue = value;
        firePropertyChange(PROPERTYNAME_CVVALUE, oldValue, cvValue);
    }

    public void clearCvValue() {
        LOGGER.debug("Clear the CV value.");
        Integer oldValue = cvValue;
        cvValue = null;
        firePropertyChange(PROPERTYNAME_CVVALUE, oldValue, cvValue);
    }

    /**
     * @return the commandStationState
     */
    public CommandStationState getCommandStationState() {
        return commandStationState;
    }

    /**
     * @param commandStationState
     *            the commandStationState to set
     */
    public void setCommandStationState(CommandStationState commandStationState) {
        LOGGER.info("Set the new command station state: {}", commandStationState);
        CommandStationState oldCommandStationState = this.commandStationState;
        this.commandStationState = commandStationState;

        firePropertyChange(PROPERTYNAME_COMMANDSTATIONSTATE, oldCommandStationState, commandStationState);
    }

    /**
     * @return the pomProgState
     */
    public PomProgState getPomProgState() {
        return pomProgState;
    }

    /**
     * @param pomProgState
     *            the pomProgState to set
     */
    public void setPomProgState(PomProgState pomProgState) {
        LOGGER.info("Set the POM prog state: {}", pomProgState);
        PomProgState oldValue = this.pomProgState;
        this.pomProgState = pomProgState;

        firePropertyChange(PROPERTYNAME_POMPROGSTATE, oldValue, pomProgState);
    }

    public void updatePomProgResult(
        final PomProgState pomProgState, final AddressData decoderAddress, final int cvNumber, final int cvValue) {
        LOGGER.info("update the POM prog result: {}, decoderAddress: {}, cvNumber: {}, cvValue: {}", pomProgState,
            decoderAddress, cvNumber, cvValue);

        if (SwingUtilities.isEventDispatchThread()) {
            notifyPomProgResult(pomProgState, decoderAddress, cvNumber, cvValue);
        }
        else {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    notifyPomProgResult(pomProgState, decoderAddress, cvNumber, cvValue);
                }
            });
        }
    }

    private void notifyPomProgResult(
        final PomProgState pomProgState, AddressData decoderAddress, int cvNumber, int cvValue) {

        setPomProgState(pomProgState);

        if (PomProgState.POM_PROG_OKAY.equals(pomProgState)) {
            // TODO do something with the decoder address and cv number
            setCvValue(cvValue);
        }

        // TODO signal that all values returned from the new state are set ...

        fireProgCommandFinished(pomProgState);
    }

    private void fireProgCommandFinished(PomProgState pomProgState) {
        for (ProgCommandListener l : progCommandListeners) {
            l.progPomFinished(pomProgState);
        }
    }
}
