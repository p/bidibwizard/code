package org.bidib.wizard.mvc.pt.view.panel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import javax.swing.Icon;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.apache.commons.lang.BooleanUtils;
import org.bidib.jbidibc.core.enumeration.PtOperation;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.core.utils.CvUtils;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.common.utils.ImageUtils;
import org.bidib.wizard.mvc.common.view.validation.PropertyValidationI18NSupport;
import org.bidib.wizard.mvc.main.view.panel.NodeTree;
import org.bidib.wizard.mvc.pt.model.PtProgrammerModel;
import org.bidib.wizard.mvc.pt.view.command.PtOperationCommand;
import org.bidib.wizard.mvc.pt.view.command.PtRailcomConfigCommand;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.value.ValueHolder;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.builder.ButtonBarBuilder;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.ValidationResultModel;
import com.jgoodies.validation.util.PropertyValidationSupport;
import com.jgoodies.validation.view.ValidationComponentUtils;

public class RailcomPanel extends AbstractPtPanel<RailcomProgBeanModel> {

    private ValueModel channel1ValueModel;

    private ValueModel channel2ValueModel;

    private ValueModel channelUsageValueModel;

    private ValueModel railcomPlusValueModel;

    private RailcomProgBeanModel railcomProgBeanModel;

    private ValidationResultModel railcomValidationModel;

    private Icon infoIcon;

    private JComponent[] components;

    public RailcomPanel(PtProgrammerModel cvProgrammerModel) {
        super(cvProgrammerModel);

        infoIcon = ImageUtils.createImageIcon(NodeTree.class, "/icons/information.png");

        railcomProgBeanModel = new RailcomProgBeanModel();
        setProgCommandAwareBeanModel(railcomProgBeanModel);
    }

    @Override
    protected void createWorkerPanel(DefaultFormBuilder builder) {

        builder.append(new JLabel(Resources.getString(getClass(), "railcom-message")), 7);
        builder.append(new JLabel(Resources.getString(getClass(), "railcom-activate-info"), infoIcon,
            SwingConstants.LEADING), 7);

        channel1ValueModel =
            new PropertyAdapter<RailcomProgBeanModel>(railcomProgBeanModel,
                RailcomProgBeanModel.PROPERTYNAME_CHANNEL_1, true);

        JCheckBox checkChannel1 =
            BasicComponentFactory.createCheckBox(channel1ValueModel, Resources.getString(getClass(), "channel1"));
        builder.append(checkChannel1, 7);
        builder.nextLine();

        channel2ValueModel =
            new PropertyAdapter<RailcomProgBeanModel>(railcomProgBeanModel,
                RailcomProgBeanModel.PROPERTYNAME_CHANNEL_2, true);

        JCheckBox checkChannel2 =
            BasicComponentFactory.createCheckBox(channel2ValueModel, Resources.getString(getClass(), "channel2"));
        builder.append(checkChannel2, 7);
        builder.nextLine();

        channelUsageValueModel =
            new PropertyAdapter<RailcomProgBeanModel>(railcomProgBeanModel,
                RailcomProgBeanModel.PROPERTYNAME_CHANNEL_USAGE, true);

        JCheckBox checkChannelUsage =
            BasicComponentFactory.createCheckBox(channelUsageValueModel,
                Resources.getString(getClass(), "channelUsage"));
        checkChannelUsage.setToolTipText(Resources.getString(getClass(), "channelUsage.tooltip"));
        builder.append(checkChannelUsage, 7);
        builder.nextLine();

        railcomPlusValueModel =
            new PropertyAdapter<RailcomProgBeanModel>(railcomProgBeanModel,
                RailcomProgBeanModel.PROPERTYNAME_RAILCOM_PLUS, true);

        JCheckBox checkRailcomPlus =
            BasicComponentFactory.createCheckBox(railcomPlusValueModel, Resources.getString(getClass(), "railcomplus"));
        builder.append(checkRailcomPlus, 7);
        builder.nextLine();

        ValidationComponentUtils.setMessageKey(checkRailcomPlus, "validation.railcomplus_key");

        components = new JComponent[4];
        components[0] = checkChannel1;
        components[1] = checkChannel2;
        components[2] = checkChannelUsage;
        components[3] = checkRailcomPlus;

        // add a validation model that can trigger a button state with the validState property
        railcomValidationModel = new PtValidationResultModel();

        railcomProgBeanModel.addPropertyChangeListener(RailcomProgBeanModel.PROPERTYNAME_RAILCOM_PLUS,
            new PropertyChangeListener() {

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    LOGGER.debug("RailCom+ has changed: {}", railcomProgBeanModel.isRailcomPlus());
                    triggerValidation();
                }
            });
        railcomProgBeanModel.addPropertyChangeListener(RailcomProgBeanModel.PROPERTYNAME_CHANNEL_2,
            new PropertyChangeListener() {

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    LOGGER.debug("Channel2 has changed: {}", railcomProgBeanModel.isChannel2());
                    triggerValidation();
                }
            });

        readButtonEnabled = new ValueHolder(false);

        readButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                fireRead();
            }
        });

        writeButtonEnabled = new ValueHolder(true);
        // writeButton.setEnabled(false);
        writeButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                fireWrite();
            }
        });

        PropertyConnector.connect(railcomValidationModel, PtValidationResultModel.PROPERTY_VALID_STATE, writeButton,
            "enabled");

        // prepare the read and write buttons
        JPanel progActionButtons = new ButtonBarBuilder().addGlue().addButton(readButton, writeButton).build();
        builder.append(progActionButtons, 7);

    }

    @Override
    protected Object getCurrentOperation() {
        return railcomProgBeanModel.getCurrentOperation();
    }

    protected void doBindButtons() {
        // add bindings for enable/disable the write button
        PropertyConnector.connect(readButtonEnabled, "value", readButton, "enabled");
        PropertyConnector.connect(writeButtonEnabled, "value", writeButton, "enabled");
    }

    @Override
    protected ValidationResultModel getValidationResultModel() {
        return railcomValidationModel;
    }

    private ValidationResult validate() {
        PropertyValidationSupport support = new PropertyValidationI18NSupport(railcomValidationModel, "validation");

        // if the railcom plus is set we must set railcom on channel 2
        if (BooleanUtils.isTrue((Boolean) railcomPlusValueModel.getValue())
            && BooleanUtils.isFalse((Boolean) channel2ValueModel.getValue())) {
            support.addError("railcomplus_key", "channel2_required");
        }

        ValidationResult validationResult = support.getResult();
        LOGGER.info("Prepared validationResult: {}", validationResult);
        return validationResult;
    }

    @Override
    protected void triggerValidation() {
        ValidationResult validationResult = validate();
        railcomValidationModel.setResult(validationResult);
    }

    @Override
    protected void disableInputElements() {

        for (JComponent comp : components) {
            comp.setEnabled(false);
        }

        super.disableInputElements();
    }

    @Override
    protected void enableInputElements() {

        for (JComponent comp : components) {
            comp.setEnabled(true);
        }

        readButtonEnabled.setValue(true);
        writeButtonEnabled.setValue(true);
    }

    private void fireWrite() {
        // disable the input elements
        disableInputElements();

        // clear the executed commands
        railcomProgBeanModel.getExecutedProgCommands().clear();

        List<PtOperationCommand<? extends ProgCommandAwareBeanModel>> progCommands =
            railcomProgBeanModel.getProgCommands();
        progCommands.clear();

        // Set the bits in CV 28
        progCommands.add(new PtRailcomConfigCommand(PtOperation.WR_BIT, 28, CvUtils.preparePtBitCvValue(true, 0,
            Boolean.TRUE.equals(channel1ValueModel.getValue()) ? 1 : 0)));
        progCommands.add(new PtRailcomConfigCommand(PtOperation.WR_BIT, 28, CvUtils.preparePtBitCvValue(true, 1,
            Boolean.TRUE.equals(channel2ValueModel.getValue()) ? 1 : 0)));
        progCommands.add(new PtRailcomConfigCommand(PtOperation.WR_BIT, 28, CvUtils.preparePtBitCvValue(true, 2,
            Boolean.TRUE.equals(channelUsageValueModel.getValue()) ? 1 : 0)));
        progCommands.add(new PtRailcomConfigCommand(PtOperation.WR_BIT, 28, CvUtils.preparePtBitCvValue(true, 7,
            Boolean.TRUE.equals(railcomPlusValueModel.getValue()) ? 1 : 0)));

        fireNextCommand();
    }

    private void fireRead() {
        // disable the input elements
        disableInputElements();

        // clear the executed commands
        railcomProgBeanModel.getExecutedProgCommands().clear();

        List<PtOperationCommand<? extends ProgCommandAwareBeanModel>> progCommands =
            railcomProgBeanModel.getProgCommands();
        progCommands.clear();

        // Read the CV 28
        progCommands.add(new PtRailcomConfigCommand(PtOperation.RD_BYTE, 28, ByteUtils.getLowByte(0)));

        fireNextCommand();
    }
}
