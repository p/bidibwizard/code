package org.bidib.wizard.mvc.main.view.panel.listener;

public interface TabSelectionListener {

    /**
     * @param selected
     *            the tab is selected
     */
    void tabSelected(boolean selected);
}
