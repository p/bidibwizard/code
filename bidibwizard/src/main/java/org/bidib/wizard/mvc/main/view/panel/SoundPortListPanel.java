package org.bidib.wizard.mvc.main.view.panel;

import java.awt.Component;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;

import org.apache.commons.collections4.CollectionUtils;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.comm.SoundPortStatus;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.dialog.LabelDialog;
import org.bidib.wizard.mvc.common.view.renderer.PortIdentifierTableCellRenderer;
import org.bidib.wizard.mvc.main.controller.SoundPortPanelController;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.model.Port;
import org.bidib.wizard.mvc.main.model.SimplePortTableModel;
import org.bidib.wizard.mvc.main.model.SoundPort;
import org.bidib.wizard.mvc.main.model.SoundPortTableModel;
import org.bidib.wizard.mvc.main.model.listener.SoundPortListener;
import org.bidib.wizard.mvc.main.view.menu.listener.PortListMenuListener;
import org.bidib.wizard.mvc.main.view.table.DefaultPortListMenuListener;
import org.bidib.wizard.mvc.main.view.table.PortComboBoxWithButtonEditor;
import org.bidib.wizard.mvc.main.view.table.PortComboBoxWithButtonRenderer;
import org.bidib.wizard.mvc.main.view.table.PortConfigErrorAwareEditor;
import org.bidib.wizard.mvc.main.view.table.PortConfigErrorAwareRenderer;
import org.bidib.wizard.mvc.main.view.table.PortTable;
import org.bidib.wizard.mvc.main.view.table.SoundPortTableCellRenderer;
import org.bidib.wizard.utils.IntegerEditor;

import com.jidesoft.grid.TableColumnChooser;

public class SoundPortListPanel
    extends SimplePortListPanel<SoundPortStatus, SoundPort, SoundPortListener<SoundPortStatus>> {
    private static final long serialVersionUID = 1L;

    private final MainModel mainModel;

    private final class PortIntegerEditor extends IntegerEditor {
        private static final long serialVersionUID = 1L;

        private int forColumn;

        public PortIntegerEditor(int min, int max, int forColumn) {
            super(min, max);
            this.forColumn = forColumn;
        }

        @Override
        public Component getTableCellEditorComponent(
            JTable table, Object value, boolean isSelected, int row, int column) {
            if (value instanceof SoundPort) {
                SoundPort soundPort = (SoundPort) value;
                LOGGER.debug("get editor, forColumn: {}, column: {}", forColumn, column);
                switch (forColumn) {
                    default:
                        value = soundPort.getPulseTime();
                        break;
                }
            }

            return super.getTableCellEditorComponent(table, value, isSelected, row, column);
        }
    }

    public SoundPortListPanel(final SoundPortPanelController controller, MainModel model) {
        super(new SoundPortTableModel(model), model.getSoundPorts(),
            Resources.getString(SoundPortListPanel.class, "emptyTable"));

        this.mainModel = model;
        mainModel.addSoundPortListListener(this);

        TableColumn tc = table.getColumnModel().getColumn(SoundPortTableModel.COLUMN_LABEL);
        tc.setCellRenderer(new PortConfigErrorAwareRenderer(SoundPortTableModel.COLUMN_LABEL));
        tc.setCellEditor(new PortConfigErrorAwareEditor(SoundPortTableModel.COLUMN_PORT_INSTANCE));
        tc.setIdentifier(Integer.valueOf(SoundPortTableModel.COLUMN_LABEL));

        tc = table.getColumnModel().getColumn(SoundPortTableModel.COLUMN_PULSE_TIME);
        tc.setCellRenderer(new SoundPortTableCellRenderer(SoundPortTableModel.COLUMN_PULSE_TIME));
        tc.setCellEditor(new PortIntegerEditor(0, 255, SoundPortTableModel.COLUMN_PULSE_TIME));
        tc.setIdentifier(Integer.valueOf(SoundPortTableModel.COLUMN_PULSE_TIME));

        tc = table.getColumnModel().getColumn(SoundPortTableModel.COLUMN_PORT_IDENTIFIER);
        tc.setCellRenderer(new PortIdentifierTableCellRenderer());
        tc.setMaxWidth(80);
        tc.setIdentifier(Integer.valueOf(SoundPortTableModel.COLUMN_PORT_IDENTIFIER));

        // Set the status renderer
        tc = table.getColumnModel().getColumn(SoundPortTableModel.COLUMN_STATUS);
        tc.setIdentifier(Integer.valueOf(SoundPortTableModel.COLUMN_STATUS));
        tc.setCellRenderer(new DefaultTableCellRenderer());
        tc.setMaxWidth(80);

        TableColumn buttonColumn = table.getColumnModel().getColumn(SoundPortTableModel.COLUMN_TEST);
        buttonColumn.setIdentifier(Integer.valueOf(SoundPortTableModel.COLUMN_TEST));

        buttonColumn.setCellRenderer(new PortComboBoxWithButtonRenderer<BidibStatus, SoundPortStatus>(
            table.getActions(SoundPortStatus.PLAY), ">") {
            private static final long serialVersionUID = 1L;

            @Override
            protected void setSelectedValue(Port<?> port) {
                SoundPortStatus currentStatus = null;
                if (port != null) {
                    currentStatus = (SoundPortStatus) port.getStatus();
                }
                comboBox.setSelectedItem(currentStatus);
            }
        });
        PortComboBoxWithButtonEditor editor =
            new PortComboBoxWithButtonEditor(table.getActions(SoundPortStatus.PLAY), ">") {
                private static final long serialVersionUID = 1L;

                @Override
                protected void setSelectedValue(Port<?> port) {
                    SoundPortStatus currentStatus = null;
                    if (port != null) {
                        currentStatus = (SoundPortStatus) port.getStatus();
                    }
                    comboBox.setSelectedItem(currentStatus);
                }
            };
        editor.addButtonListener((SoundPortTableModel) tableModel);
        buttonColumn.setCellEditor(editor);

        TableColumnChooser.hideColumn(table, SoundPortTableModel.COLUMN_PORT_INSTANCE);
    }

    @Override
    protected PortTable createPortTable(
        final SimplePortTableModel<SoundPortStatus, SoundPort, SoundPortListener<SoundPortStatus>> tableModel,
        String emptyTableText) {

        PortTable portTable = new PortTable(tableModel, emptyTableText) {
            private static final long serialVersionUID = 1L;

            @Override
            public void clearTable() {
            }

            @Override
            protected PortListMenuListener createMenuListener() {
                // create the port list menu
                return new DefaultPortListMenuListener() {
                    @Override
                    public void editLabel() {
                        final int row = getRow(popupEvent.getPoint());
                        if (row > -1) {
                            Object val = getValueAt(row, 0);
                            if (val instanceof Port<?>) {
                                val = ((Port<?>) val).toString();
                            }
                            final Object value = val;
                            if (value instanceof String) {
                                // show the port name editor
                                new LabelDialog((String) value, popupEvent.getXOnScreen(), popupEvent.getYOnScreen()) {
                                    @Override
                                    public void labelChanged(String label) {
                                        setValueAt(label, row, 0);
                                    }
                                };
                            }
                        }
                        else {
                            LOGGER.warn("The row is not available!");
                        }
                    }

                    @Override
                    public void mapPort() {

                        final int row = getRow(popupEvent.getPoint());
                        if (row > -1) {
                            Object val = getValueAt(row, 0);
                            if (val instanceof SoundPort) {
                                SoundPort soundPort = (SoundPort) val;
                                LOGGER.info("Change mapping for port: {}", soundPort);

                                // confirm switch to switch port
                                int result =
                                    JOptionPane.showConfirmDialog(
                                        JOptionPane.getFrameForComponent(SoundPortListPanel.this),
                                        Resources.getString(SoundPortListPanel.class, "switch-port-confirm"),
                                        Resources.getString(SoundPortListPanel.class, "switch-port-title"),
                                        JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
                                if (result == JOptionPane.OK_OPTION) {
                                    LOGGER.info("Change the port to an sound port.");

                                    SoundPortTableModel switchPortTableModel = (SoundPortTableModel) getModel();
                                    switchPortTableModel.changePortType(LcOutputType.SOUNDPORT, soundPort);
                                }
                            }
                        }
                    }
                };
            }

        };

        return portTable;
    }

    @Override
    public void listChanged() {
        LOGGER.info("The port list has changed.");

        super.listChanged();

        boolean hasPortIdentifiers = false;
        List<SoundPort> ports = new LinkedList<>();
        ports.addAll(getPorts());
        synchronized (ports) {
            for (SoundPort port : ports) {
                if (port.isRemappingEnabled()) {
                    hasPortIdentifiers = true;
                    break;
                }
            }
        }

        if (mainModel.getSelectedNode() != null) {
            LOGGER.info("A node is selected.");
            boolean hasSoundPortConfig = false;
            Node node = mainModel.getSelectedNode();
            if (node.getNode().isPortFlatModelAvailable() && CollectionUtils.isNotEmpty(node.getEnabledSoundPorts())) {
                LOGGER.info("Check if the first sound port has the sound port config available.");
                hasSoundPortConfig = node.getEnabledSoundPorts().get(0).isHasSoundPortConfig();
            }
            else if (CollectionUtils.isNotEmpty(mainModel.getEnabledSoundPorts())) {
                LOGGER.info("Check if the first sound port has the sound port config available.");
                hasSoundPortConfig = mainModel.getEnabledSoundPorts().get(0).isHasSoundPortConfig();
            }

            LOGGER.info("List has changed, hasPortIdentifiers: {}, hasSoundPortConfig: {}", hasPortIdentifiers,
                hasSoundPortConfig);

            if (!hasSoundPortConfig) {
                TableColumnChooser.hideColumn(table, SoundPortTableModel.COLUMN_PULSE_TIME);
            }
            else {
                if (!TableColumnChooser.isVisibleColumn(table, SoundPortTableModel.COLUMN_PULSE_TIME)) {
                    TableColumnChooser.showColumn(table, SoundPortTableModel.COLUMN_PULSE_TIME,
                        SoundPortTableModel.COLUMN_PULSE_TIME);
                }
            }
        }

        // show/hide the port identifiers
        if (!hasPortIdentifiers) {
            TableColumnChooser.hideColumn(table, SoundPortTableModel.COLUMN_PORT_IDENTIFIER);
        }
        else {
            if (!TableColumnChooser.isVisibleColumn(table, SoundPortTableModel.COLUMN_PORT_IDENTIFIER)) {
                TableColumnChooser.showColumn(table, SoundPortTableModel.COLUMN_PORT_IDENTIFIER,
                    SoundPortTableModel.COLUMN_PORT_IDENTIFIER);
            }
        }
    }

    @Override
    protected void refreshPorts() {

        Node node = mainModel.getSelectedNode();
        if (node != null) {
            if (node != null) {
                if (node.getNode().isPortFlatModelAvailable()) {
                    if (CollectionUtils.isNotEmpty(node.getGenericPorts())) {
                        mainModel.getSoundPorts();
                    }
                    else {
                        LOGGER.info(
                            "The node supports flat port model but no generic ports are available. Skip get sound ports.");
                    }
                }
                else {
                    mainModel.getSoundPorts();
                }
            }
        }
    }
}
