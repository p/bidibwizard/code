package org.bidib.wizard.mvc.main.view.cvdef;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;

import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.exchange.vendorcv.BitdescriptionType;
import org.bidib.jbidibc.exchange.vendorcv.CVType;
import org.bidib.jbidibc.exchange.vendorcv.ModeType;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.common.utils.ImageUtils;
import org.bidib.wizard.mvc.common.view.converter.StringConverter;
import org.bidib.wizard.mvc.common.view.cvdefinition.CvDefinitionTreeTableModel;
import org.bidib.wizard.mvc.common.view.validation.PropertyValidationI18NSupport;
import org.bidib.wizard.mvc.main.view.panel.NodeTree;
import org.bidib.wizard.utils.InputValidationDocument;
import org.bidib.wizard.utils.XmlLocaleUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.beans.BeanAdapter;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.value.ConverterValueModel;
import com.jgoodies.binding.value.ValueHolder;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.common.swing.MnemonicUtils;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.util.PropertyValidationSupport;
import com.jgoodies.validation.view.ValidationComponentUtils;

public class CvBitfieldValueEditor extends CvValueNumberEditor<Byte> {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(CvBitfieldValueEditor.class);

    private Byte allowedBitValues;

    private BitfieldBean bitfieldBean = new BitfieldBean();

    private DualBitfieldBean allowedValuesBean = new DualBitfieldBean();

    private ValueModel writeEnabled;

    private ValueModel[] bitValueModels = new ValueModel[8];

    private JToggleButton[] checkBoxBits = new JToggleButton[8];

    private JToggleButton[] radioButtonBits = new JToggleButton[8];

    private JLabel radioButtonLabels[] = new JLabel[8];

    @Override
    protected ValidationResult validateModel(
        PresentationModel<CvValueBean<Byte>> model, CvValueBean<Byte> cvValueBean) {
        PropertyValidationSupport support = new PropertyValidationI18NSupport(cvValueBean, "validation");

        return support.getResult();
    }

    @Override
    protected DefaultFormBuilder prepareFormPanel() {

        writeEnabled = new ValueHolder(false);

        // Get buffered model objects.
        cvValueModel = cvAdapter.getBufferedModel("cvValue");
        valueConverterModel = new ConverterValueModel(cvValueModel, new StringConverter(new DecimalFormat("#")));

        DefaultFormBuilder formBuilder =
            new DefaultFormBuilder(new FormLayout(
                "3dlu, max(20dlu;pref), 3dlu, max(30dlu;pref), 20dlu, max(10dlu;pref), 3dlu, max(30dlu;pref), 0dlu:grow"));
        JTextField textCvValue = BasicComponentFactory.createTextField(valueConverterModel, false);
        textCvValue.setDocument(new InputValidationDocument(InputValidationDocument.NUMERIC));
        textCvValue.setEditable(false);

        // the needs reboot Icon is invisible by default
        ImageIcon warnIcon = ImageUtils.createImageIcon(NodeTree.class, "/icons/warn.png");
        JLabel needsRebootIcon =
            new JLabel(Resources.getString(getClass(), "rebootrequired"), warnIcon, SwingConstants.LEADING);
        needsRebootIcon.setVisible(false);

        ValidationComponentUtils.setMandatory(textCvValue, true);
        ValidationComponentUtils.setMessageKey(textCvValue, "validation.cvvalue_key");

        formBuilder.leadingColumnOffset(1);
        formBuilder.nextLine(0);
        formBuilder.append(BasicComponentFactory.createLabel(cvNumberModel, new CvNodeMessageFormat("{0} (CV{1})")), 8);
        formBuilder.nextLine();
        formBuilder.append(textCvValue, 1);
        formBuilder.append(needsRebootIcon);

        final BeanAdapter<BitfieldBean> bitfieldBeanAdapter = new BeanAdapter<BitfieldBean>(bitfieldBean, true);

        // create the checkboxes and radio buttons
        for (int indexBit = 0; indexBit < 8; indexBit++) {
            // prepare the model
            ValueModel bitValueModel = bitfieldBeanAdapter.getValueModel(BitfieldBean.BIT_PROPERTY + indexBit);
            bitValueModels[indexBit] = bitValueModel;
            checkBoxBits[indexBit] = BasicComponentFactory.createCheckBox(bitValueModel, null);
            checkBoxBits[indexBit].setEnabled(false);

            radioButtonBits[indexBit] = BasicComponentFactory.createRadioButton(bitValueModel, Boolean.TRUE, null);
            radioButtonBits[indexBit].setEnabled(false);
            radioButtonBits[indexBit].setVisible(false);

            // prepare the UI
            formBuilder.nextLine();
            formBuilder.append("Bit " + indexBit, checkBoxBits[indexBit]);

            JLabel radioButtonLabel = formBuilder.append(Integer.toString(indexBit), radioButtonBits[indexBit]);
            radioButtonLabels[indexBit] = radioButtonLabel;
            radioButtonLabels[indexBit].setVisible(false);
        }

        // add bindings
        PropertyConnector.connect(needsRebootModel, "value", needsRebootIcon, "visible");

        // add bindings for enable/disable the checkbox
        for (int indexBit = 0; indexBit < 8; indexBit++) {
            PropertyConnector.connect(allowedValuesBean, "bit" + indexBit + "LeftSide", checkBoxBits[indexBit],
                "enabled");

            // handle the radio buttons
            PropertyConnector.connect(allowedValuesBean, "bit" + indexBit + "RightSide", radioButtonBits[indexBit],
                "enabled");
        }

        final PropertyChangeListener localChangeListener = new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                LOGGER.debug("The bitfield bean value has changed: {}, propName: {}", evt.getNewValue(),
                    evt.getPropertyName());

                Byte bitfieldValue = bitfieldBean.getBitfield();
                cvValueModel.setValue(bitfieldValue);

                if (!setInProgress) {
                    triggerValidation("signalValidState");
                }
            }
        };

        bitfieldBean.addPropertyChangeListener(localChangeListener);

        cvValueModel.addValueChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                LOGGER.debug("Converter model has changed, evt: {}", evt);
                Byte value = null;
                if (evt.getNewValue() instanceof Number) {
                    Number number = (Number) evt.getNewValue();
                    value = ByteUtils.getLowByte(number.intValue());
                }
                LOGGER.debug("Set the bitfield value: {}", (value != null ? (value & 0xFF) : null));

                if (localChangeListener != null) {
                    LOGGER.debug("Remove property changeListener from bitfieldBean.");
                    bitfieldBean.removePropertyChangeListener(localChangeListener);
                }
                bitfieldBean.setBitfield(value);
                if (localChangeListener != null) {
                    LOGGER.debug("Add property changeListener to bitfieldBean again.");
                    bitfieldBean.addPropertyChangeListener(localChangeListener);
                }
            }
        });

        return formBuilder;
    }

    private boolean setInProgress;

    @Override
    public void setValue(CvNode cvNode, Map<String, CvNode> cvNumberToNodeMap) {
        LOGGER.debug("Set the new value from node: {}", cvNode);
        setInProgress = true;

        boolean timeout = cvNode.getConfigVar().isTimeout();

        // set textfield editable before the value is set because the validation is triggered
        writeEnabled.setValue(!(ModeType.RO.equals(cvNode.getCV().getMode()) || timeout));

        // bitfield value -> get the values attribute
        CVType cv = cvNode.getCV();
        String values = cv.getValues();

        // evaluate the allowed bit values
        if (ModeType.RO.equals(cvNode.getCV().getMode()) || timeout) {
            allowedBitValues = new Byte(ByteUtils.getLowByte(0));
        }
        else if (values != null) {
            try {
                allowedBitValues = new Byte(ByteUtils.getLowByte(Integer.parseInt(values)));
            }
            catch (NumberFormatException ex) {
                LOGGER.debug("Parse max value failed: {}", values);
                allowedBitValues = new Byte(ByteUtils.getLowByte(255));
            }
        }
        else {
            allowedBitValues = new Byte(ByteUtils.getLowByte(255));
        }

        // set the radio bits
        Integer radioBits = cv.getRadiobits();
        String radiovalues = cv.getRadiovalues();
        LOGGER.debug("Current radioBits: {}, radiovalues: {}, cv: {}", radioBits, radiovalues, cv);

        bitfieldBean.setRadioBits(radioBits);

        allowedValuesBean.setRadioBits(radioBits);
        // this calls the binding for the enabled state of the checkbox
        allowedValuesBean.setBitfield(allowedBitValues);

        // set the value in the cvValueBean
        super.setValue(cvNode, cvNumberToNodeMap);

        // enable / disable the checkboxes
        Number value = cvValueBean.getCvValue();
        LOGGER.debug("Initialize the bitfieldBean with value: {}", value);

        bitfieldBean.setBitfield(value);

        for (int indexBit = 0; indexBit < 8; indexBit++) {
            bitValueModels[indexBit].setValue(bitfieldBean.getBit(indexBit));
        }

        // evaluate language
        String lang = XmlLocaleUtils.getXmlLocaleVendorCV();

        // evaluate the radiobits and create radioButton or checkBox

        for (int indexBit = 0; indexBit < 8; indexBit++) {
            MnemonicUtils.configure(checkBoxBits[indexBit],
                getBitDescription(cv.getBitdescription(), indexBit + 1, lang));
            checkBoxBits[indexBit].setToolTipText(getBitDescriptionHelp(cv.getBitdescription(), indexBit + 1, lang));

            MnemonicUtils.configure(radioButtonBits[indexBit],
                getBitDescription(cv.getBitdescription(), indexBit + 1, lang));
            radioButtonBits[indexBit].setToolTipText(getBitDescriptionHelp(cv.getBitdescription(), indexBit + 1, lang));

            if (radioBits != null && ByteUtils.isBitSetEqual(radioBits.intValue(), 1, indexBit)) {
                radioButtonBits[indexBit].setVisible(true);
                radioButtonLabels[indexBit].setVisible(true);
            }
            else {
                radioButtonBits[indexBit].setVisible(false);
                radioButtonLabels[indexBit].setVisible(false);
            }
        }

        setInProgress = false;

        cvAdapter.resetChanged();
        getTrigger().triggerFlush();

        triggerValidation(null);
    }

    private String getBitDescription(List<BitdescriptionType> bitdescriptions, int bit, String lang) {
        String description = null;
        for (BitdescriptionType bitdescription : bitdescriptions) {
            if (lang.equals(bitdescription.getLang()) && bit == bitdescription.getBitnum().intValue()) {
                description = bitdescription.getText();
                break;
            }
        }
        return description;
    }

    private String getBitDescriptionHelp(List<BitdescriptionType> bitdescriptions, int bit, String lang) {
        String description = null;
        for (BitdescriptionType bitdescription : bitdescriptions) {
            if (lang.equals(bitdescription.getLang()) && bit == bitdescription.getBitnum().intValue()) {
                description = bitdescription.getHelp();
                break;
            }
        }
        return description;
    }

    @Override
    public void updateCvValues(Map<String, CvNode> cvNumberToNodeMap, CvDefinitionTreeTableModel treeModel) {
        CvNode cvNode = (CvNode) cvNumberModel.getValue();
        cvNode.setValueAt(cvValueBean.getCvValue(), CvNode.COLUMN_NEW_VALUE);

        // treeModel.refreshTreeTable(cvNode);
    }
}
