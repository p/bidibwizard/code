package org.bidib.wizard.mvc.pt.controller;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.JFrame;

import org.bidib.jbidibc.core.DefaultMessageListener;
import org.bidib.jbidibc.core.MessageListener;
import org.bidib.jbidibc.core.enumeration.CommandStationProgState;
import org.bidib.jbidibc.core.enumeration.CommandStationPt;
import org.bidib.jbidibc.core.enumeration.CommandStationState;
import org.bidib.jbidibc.core.enumeration.PtOperation;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.wizard.comm.Communication;
import org.bidib.wizard.comm.CommunicationFactory;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.pt.controller.listener.PtProgrammerControllerListener;
import org.bidib.wizard.mvc.pt.model.PtProgrammerModel;
import org.bidib.wizard.mvc.pt.view.PtProgrammerView;
import org.bidib.wizard.mvc.pt.view.listener.PtProgrammerViewListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vlsolutions.swing.docking.DockingDesktop;

public class PtProgrammerController {
    private static final Logger LOGGER = LoggerFactory.getLogger(PtProgrammerController.class);

    private final Collection<PtProgrammerControllerListener> listeners =
        new LinkedList<PtProgrammerControllerListener>();

    private final JFrame parent;

    private final Node node;

    private final int x;

    private final int y;

    private final PtProgrammerModel model = new PtProgrammerModel();

    private PtProgrammerView view;

    private MessageListener messageListener;

    private static AtomicBoolean singleton = new AtomicBoolean();

    public PtProgrammerController(Node node, JFrame parent, int x, int y) {
        this.parent = parent;
        this.node = node;
        this.x = x;
        this.y = y;
    }

    public static boolean isOpened() {
        return singleton.get();
    }

    private void setOpened(boolean opened) {
        singleton.set(opened);
    }

    public void addPtProgrammerControllerListener(PtProgrammerControllerListener l) {
        listeners.add(l);
    }

    private void fireClose() {
        for (PtProgrammerControllerListener l : listeners) {
            l.close();
        }

        // reset the opened flag
        setOpened(false);
    }

    private void fireSendRequest(PtOperation operation, int cvNumber, int cvValue) {

        LOGGER.info("Send CV request, operation: {}, cvNumber: {}, value: {}", operation, cvNumber, cvValue);

        CommandStationPt opCode = CommandStationPt.valueOf(ByteUtils.getLowByte(operation.getType()));

        // clear the stored cv value in the programmer model
        model.clearCvValue();

        LOGGER.info("Prepared opCode: {}", opCode);

        for (PtProgrammerControllerListener l : listeners) {
            l.sendRequest(node, opCode, cvNumber, cvValue);
        }
    }

    /**
     * @param activateProgMode
     *            activate the programming mode
     */
    private void fireSendCommandStationStateRequest(boolean activateProgMode) {

        // TODO if we switch to PROG mode we must make sure the booster is on! Check!
        CommandStationState commandStationState = null;
        if (activateProgMode) {
            commandStationState = CommandStationState.PROG;
        }
        else {
            commandStationState = node.getNode().getRequestedCommandStationState();
            if (commandStationState == null) {
                commandStationState = CommandStationState.GO_IGN_WD;
            }
        }
        LOGGER.info("Set the new command station state: {}, activateProgMode: {}", commandStationState,
            activateProgMode);

        for (PtProgrammerControllerListener l : listeners) {
            l.sendCommandStationStateRequest(node, commandStationState);
        }
    }

    private CommandStationState fireGetCommandStationStateRequest() {

        LOGGER.info("Get the commandStationState");

        for (PtProgrammerControllerListener l : listeners) {
            CommandStationState commandStationState = l.getCurrentCommandStationState(node);
            return commandStationState;
        }
        return null;
    }

    public void start(DockingDesktop desktop) {
        final Communication communication = CommunicationFactory.getInstance();

        view = new PtProgrammerView(model);
        view.addPtProgrammerViewListener(new PtProgrammerViewListener() {
            @Override
            public void close() {
                if (messageListener != null) {
                    LOGGER.info("Remove the message listener.");
                    communication.removeMessageListener(messageListener);

                    messageListener = null;
                }
                fireClose();
            }

            @Override
            public void sendRequest(PtOperation operation, int cvNumber, int cvValue) {
                fireSendRequest(operation, cvNumber, cvValue);
            }

            @Override
            public void sendCommandStationStateRequest(boolean activateProgMode) {
                fireSendCommandStationStateRequest(activateProgMode);
            }

            @Override
            public CommandStationState getCurrentCommandStationState() {
                return fireGetCommandStationStateRequest();
            }
        });

        messageListener = new DefaultMessageListener() {
            @Override
            public void csProgState(
                byte[] address, CommandStationProgState commandStationProgState, int remainingTime, int cvNumber,
                int cvData) {
                LOGGER.info(
                    "CV was received, node addr: {}, progState: {}, remainingTime: {}, cvNumber: {}, cvData: {}",
                    address, commandStationProgState, remainingTime, cvNumber, cvData);

                writeCommandStationProgState(commandStationProgState, remainingTime, cvNumber, cvData);
            }

            @Override
            public void csState(byte[] address, CommandStationState commandStationState) {
                LOGGER.info("The command station state has changed: {}", commandStationState);

                if (Arrays.equals(node.getNode().getAddr(), address)) {
                    LOGGER.info("The state of the selected command station node has changed.");

                    model.setCommandStationState(commandStationState);
                }
                else {
                    LOGGER.warn("Another command station has changed the state.");
                }
            }
        };

        communication.addMessageListener(messageListener);

        LOGGER.info("Initialize the view.");
        // view.initialize();
        setOpened(true);

        // view.prepareDockable(desktop, x, y);
        view.showDialog(parent, x, y);

    }

    private void writeCommandStationProgState(
        CommandStationProgState commandStationProgState, int remainingTime, int cvNumber, int cvValue) {
        model.updateCommandStationProgResult(commandStationProgState, remainingTime, cvNumber, cvValue);
    }
}
