package org.bidib.wizard.mvc.cvprogrammer.view.panel;

import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;
import java.util.Collection;
import java.util.LinkedList;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;

import org.apache.commons.lang.StringUtils;
import org.bidib.jbidibc.core.AddressData;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.cvprogrammer.model.CvProgrammerModel;
import org.bidib.wizard.mvc.cvprogrammer.model.listener.ConfigVariableListener;
import org.bidib.wizard.mvc.cvprogrammer.view.CvProgrammerView.CvModel;
import org.bidib.wizard.mvc.cvprogrammer.view.panel.listener.CvRequestListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.value.ValueHolder;
import com.jgoodies.binding.value.ValueModel;

public class CvWritePanel extends JPanel {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(CvWritePanel.class);

    private final Collection<CvRequestListener> listeners = new LinkedList<CvRequestListener>();

    private final JLabel cvValueLabel = new JLabel(Resources.getString(getClass(), "value") + ":");

    private final NumberFormatter formatter = new NumberFormatter(
        new DecimalFormat("#")) {
        private static final long serialVersionUID = 1L;

        {
            setAllowsInvalid(true);
            setMaximum(255);
            setMinimum(0);
        }
    };

    private final JFormattedTextField cvValue = new JFormattedTextField();

    private final JCheckBox[] bitValues = new JCheckBox[8];

    private final JRadioButton[] radioBitValues = new JRadioButton[8];

    private final JButton sendButton = new JButton(Resources.getString(getClass(), "send"));

    private ValueModel sendButtonEnabled;

    private final DocumentListener documentListener = new CvByteValueDocumentListener();

    private final DocumentListener documentListenerBit = new CvBitValueDocumentListener();

    private JPanel byteValuePanel;

    private final JFormattedTextField cvBitValue = new JFormattedTextField();

    private JPanel bitValuePanel;

    private final CvProgrammerModel programmerModel;

    private JPanel valueContentPanel;

    private CardLayout cardLayout;

    public CvWritePanel(final CvProgrammerModel model, final CvModel cvModel) {
        this.programmerModel = model;

        setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
            Resources.getString(getClass(), "title") + ":"));
        setLayout(new GridBagLayout());

        GridBagConstraints c = new GridBagConstraints();

        cardLayout = new CardLayout();
        valueContentPanel = new JPanel(cardLayout);
        valueContentPanel.add(prepareByteValuePanel(), "byte");
        valueContentPanel.add(prepareBitValuePanel(), "bit");

        c.gridx = 0;
        c.gridy = 0;
        c.insets = new Insets(5, 5, 5, 5);

        // add the CV value label
        add(cvValueLabel, c);

        // add the byte value panel per default
        c.gridx++;
        c.weightx = 1.0;
        c.fill = GridBagConstraints.HORIZONTAL;
        add(valueContentPanel);

        // prepare the send button
        sendButtonEnabled = new ValueHolder(false);

        sendButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // TODO maybe a better solution is to use an Integer instead of int because this could be set to zero
                if (StringUtils.isNotBlank(cvValue.getText())) {
                    programmerModel.setValue(Integer.parseInt(cvValue.getText()));
                }
                else {
                    programmerModel.setValue(0);
                }
                // before sending clear the current value from the textfield
                cvValue.setText(null);

                fireSend(programmerModel.getValue());
            }
        });

        c.gridx++;
        c.weightx = 0.0;
        add(sendButton, c);
        programmerModel.addConfigVariableListener(new ConfigVariableListener() {
            @Override
            public void numberChanged(int number) {
            }

            @Override
            public void valueChanged(int value) {
                cvValue.setValue(value);
            }

            @Override
            public void selectedDecoderAddressChanged(AddressData selectedDecoderAddress) {
                LOGGER.debug("The selected decoder address has changed: {}", selectedDecoderAddress);
                sendButtonEnabled.setValue(selectedDecoderAddress != null && selectedDecoderAddress.getAddress() > 0);
            }
        });

        // add bindings for enable/disable the read button
        sendButton.setEnabled(false);
        PropertyConnector.connect(sendButtonEnabled, "value", sendButton, "enabled");

        cvModel.addPropertyChangeListener(CvModel.PROPERTYNAME_OPERATION, new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                LOGGER.debug("Operation has changed: {}", cvModel.getOperation());
                switch (cvModel.getOperation()) {
                    case WR_BIT:
                        programmerModel.setValue(0);
                        cardLayout.show(valueContentPanel, "bit");
                        break;
                    default:
                        programmerModel.setValue(0);
                        cardLayout.show(valueContentPanel, "byte");
                        break;
                }
            }
        });
    }

    private void setValueSilently(String bitValue) {
        LOGGER.debug("Set the CV value: {}", bitValue);
        cvValue.getDocument().removeDocumentListener(documentListener);
        programmerModel.setValue(Integer.valueOf(bitValue, 2));
        cvValue.getDocument().addDocumentListener(documentListener);
    }

    private void setValueSilently(byte bitValue) {
        LOGGER.debug("Set the CV value: {}", bitValue);
        cvBitValue.getDocument().removeDocumentListener(documentListenerBit);
        programmerModel.setValue(bitValue);
        cvBitValue.getDocument().addDocumentListener(documentListenerBit);
    }

    private JPanel prepareByteValuePanel() {
        byteValuePanel = new JPanel();
        byteValuePanel.setLayout(new GridBagLayout());

        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.insets = new Insets(5, 5, 5, 5);

        cvValue.setColumns(3);
        cvValue.setFormatterFactory(new DefaultFormatterFactory(formatter, formatter, formatter));
        cvValue.setValue(0);
        byteValuePanel.add(cvValue, c);

        cvValue.getDocument().addDocumentListener(documentListener);

        for (int bit = 7; bit >= 0; bit--) {
            bitValues[bit] = new JCheckBox(String.valueOf(bit), false);
            bitValues[bit].addItemListener(new ItemListener() {
                public void itemStateChanged(ItemEvent e) {
                    // set integer value from bit values
                    final StringBuffer bitValue = new StringBuffer();

                    for (int bit = bitValues.length - 1; bit >= 0; bit--) {
                        bitValue.append(bitValues[bit].isSelected() ? '1' : '0');
                    }
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            setValueSilently(bitValue.toString());
                        }
                    });
                }
            });
            c.gridx += 1;
            byteValuePanel.add(bitValues[bit], c);
        }
        return byteValuePanel;
    }

    private JPanel prepareBitValuePanel() {
        bitValuePanel = new JPanel();
        bitValuePanel.setLayout(new GridBagLayout());

        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.NONE;
        c.gridx = 0;
        c.gridy = 0;
        c.insets = new Insets(5, 5, 5, 5);
        // bitValuePanel.add(cvValueLabel, c);

        cvBitValue.setColumns(1);
        cvBitValue.setFormatterFactory(new DefaultFormatterFactory(formatter, formatter, formatter));
        cvBitValue.setValue(0);

        cvBitValue.getDocument().addDocumentListener(documentListenerBit);

        bitValuePanel.add(cvBitValue, c);

        // Group the radio buttons.
        ButtonGroup group = new ButtonGroup();

        for (int bit = 7; bit >= 0; bit--) {
            radioBitValues[bit] = new JRadioButton(String.valueOf(bit), false);
            radioBitValues[bit].addItemListener(new ItemListener() {
                public void itemStateChanged(ItemEvent e) {
                    updateBitValue();
                }
            });
            group.add(radioBitValues[bit]);
            c.gridx += 1;
            bitValuePanel.add(radioBitValues[bit], c);
        }
        return bitValuePanel;
    }

    private void updateBitValue() {

        // set integer value from bit values

        byte val = 0;
        for (int bit = radioBitValues.length - 1; bit >= 0; bit--) {
            if (radioBitValues[bit].isSelected()) {

                // Beim Bit Schreiben wird das zuschreibende Bit mittels DATA bestimmt: DATA = 1111DBBB,
                // wobei BBB die Bitposition angibt und D den Wert des Bits. (identisch zur DCC Definition)
                val = (byte) (bit);
                int intVal = 0;
                if (StringUtils.isNotBlank(cvBitValue.getText())) {
                    intVal = Integer.valueOf(cvBitValue.getText());
                }
                if (intVal != 0) {
                    val = (byte) (val | 0xF8);
                }

                break;
            }
        }
        LOGGER.info("Prepared bitValue: {}", ByteUtils.byteToHex(val));
        final byte bitValue = val;

        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                setValueSilently(bitValue);
            }
        });
    }

    public void addCvRequestListener(CvRequestListener l) {
        listeners.add(l);
    }

    private void fireSend(int cvValue) {
        for (CvRequestListener l : listeners) {
            l.send(cvValue);
        }
    }

    public int getButtonWidth() {
        return sendButton.getWidth();
    }

    public int getLabelWidth() {
        return cvValueLabel.getWidth();
    }

    public void setButtonWidth(int width) {
        sendButton.setPreferredSize(new Dimension(width, sendButton.getSize().height));
    }

    public void setLabelWidth(int width) {
        cvValueLabel.setPreferredSize(new Dimension(width, cvValueLabel.getSize().height));
    }

    private void valueChanged(String value) {
        try {
            String bitValue = Integer.toBinaryString(Integer.parseInt(value));

            for (int index = 0; index < bitValues.length; index++) {
                if (index < bitValue.length()) {
                    bitValues[index].setSelected(bitValue.charAt(bitValue.length() - index - 1) == '1');
                }
                else {
                    bitValues[index].setSelected(false);
                }
            }
        }
        catch (NumberFormatException ex) {
            for (int index = 0; index < bitValues.length; index++) {
                bitValues[index].setSelected(false);
            }
        }
    }

    private class CvByteValueDocumentListener implements DocumentListener {
        public void changedUpdate(DocumentEvent e) {
            valueChanged(cvValue.getText());
        }

        public void insertUpdate(DocumentEvent e) {
            valueChanged(cvValue.getText());
        }

        public void removeUpdate(DocumentEvent e) {
            valueChanged(cvValue.getText());
        }
    }

    private class CvBitValueDocumentListener implements DocumentListener {
        public void changedUpdate(DocumentEvent e) {
            bitValueChanged();
        }

        public void insertUpdate(DocumentEvent e) {
            bitValueChanged();
        }

        public void removeUpdate(DocumentEvent e) {
            bitValueChanged();
        }
    }

    private void bitValueChanged() {
        updateBitValue();
    }
}
