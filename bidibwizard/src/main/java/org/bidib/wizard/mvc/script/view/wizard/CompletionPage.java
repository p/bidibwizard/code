package org.bidib.wizard.mvc.script.view.wizard;

import java.awt.BorderLayout;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import org.apache.commons.collections4.CollectionUtils;
import org.bidib.wizard.common.context.ApplicationContext;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.script.view.ScriptParser;
import org.bidib.wizard.script.node.NodeScriptCommandList.ExecutionStatus;
import org.bidib.wizard.script.node.NodeScriptContextKeys;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jidesoft.dialog.ButtonEvent;
import com.jidesoft.dialog.ButtonNames;
import com.jidesoft.dialog.PageEvent;
import com.jidesoft.dialog.PageListener;
import com.jidesoft.wizard.CompletionWizardPage;
import com.jidesoft.wizard.WizardIconsFactory;

public class CompletionPage extends CompletionWizardPage {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(CompletionPage.class);

    private final ApplicationContext scriptContext;

    private ExecutionStatus executionStatus;

    protected final ScheduledExecutorService waitForScriptTerminationWorker = Executors.newScheduledThreadPool(1);

    public CompletionPage(final ApplicationContext scriptContext) {
        super(Resources.getString(CompletionPage.class, "title"),
            Resources.getString(CompletionPage.class, "description.waiting"));

        this.scriptContext = scriptContext;
    }

    @Override
    protected void initContentPane() {
        super.initContentPane();

        executionStatus = scriptContext.get(NodeScriptContextKeys.KEY_SCRIPTEXECUTIONSTATUS, ExecutionStatus.class);
        LOGGER.info("initContentPane, current executionStatus: {}", executionStatus);

        updatePageLabels(executionStatus);

        addPageListener(new PageListener() {

            @Override
            public void pageEventFired(PageEvent e) {

                switch (e.getID()) {
                    case PageEvent.PAGE_CLOSING:
                        if (e.getSource() instanceof JButton) {
                            JButton button = (JButton) e.getSource();
                            if (ButtonNames.BACK.equals(button.getName())) {
                                LOGGER.info("The back button was pressed, unregister the execution status.");
                                scriptContext.unregister(NodeScriptContextKeys.KEY_SCRIPTEXECUTIONSTATUS);
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
        });
    }

    private void updatePageLabels(final ExecutionStatus executionStatus) {
        LOGGER.info("Update the page labels, current executionStatus: {}", executionStatus);

        if (executionStatus == null) {
            setDescription(Resources.getString(CompletionPage.class, "description.waiting"));
            addWarning(Resources.getString(CompletionPage.class, "content.waiting"));
        }
        else {
            switch (executionStatus) {
                case finished:
                    addInfo(Resources.getString(CompletionPage.class, "description"));
                    addSpace();
                    addText(Resources.getString(CompletionPage.class, "content.finish"));

                    LOGGER.info("Enable the finish button, executionStatus: {}", executionStatus);
                    fireButtonEvent(ButtonEvent.ENABLE_BUTTON, ButtonNames.FINISH);
                    break;
                case finishedWithErrors:

                    // format and show errors
                    final List<String> scriptErrors = scriptContext.get(ScriptParser.KEY_SCRIPT_ERRORS, List.class);
                    LOGGER.warn("Current scriptErrors: {}", scriptErrors);

                    if (CollectionUtils.isNotEmpty(scriptErrors)) {
                        StringBuilder sb = new StringBuilder("<html>");
                        sb.append(Resources.getString(CompletionPage.class, "content.problem-script-errors"));
                        sb.append("<br/><list>");
                        for (String error : scriptErrors) {
                            sb.append("<li>").append(error).append("</li>");
                        }
                        sb.append("</list>").append("</html>");

                        addComponent(new IconMultilinePanel(WizardIconsFactory.getImageIcon("icons/warning.gif"),
                            sb.toString()));
                    }
                    else {
                        addWarning(Resources.getString(CompletionPage.class, "content.problem"));
                    }

                    LOGGER.info("Enable the finish button, executionStatus: {}", executionStatus);
                    // fireButtonEvent(ButtonEvent.ENABLE_BUTTON, ButtonNames.FINISH);
                    break;
                case aborted:
                    // setDescription(Resources.getString(CompletionPage.class, "description.aborted"));
                    addWarning(Resources.getString(CompletionPage.class, "content.aborted"));
                    break;
                case running:
                    addWarning(Resources.getString(CompletionPage.class, "content.waiting"));
                    break;
                default:
                    addWarning(Resources.getString(CompletionPage.class, "content.problem"));
                    break;
            }
        }
    }

    private static class IconMultilinePanel extends JPanel {
        private static final long serialVersionUID = 1L;

        public IconMultilinePanel(Icon paramIcon, String paramString) {
            JLabel localJLabel = new JLabel(paramIcon);
            localJLabel.setVerticalAlignment(1);
            localJLabel.setOpaque(false);
            // MultilineLabel localMultilineLabel = new MultilineLabel(paramString);
            JLabel localMultilineLabel = new JLabel(paramString);
            localMultilineLabel.setOpaque(false);
            localMultilineLabel.setHorizontalTextPosition(SwingConstants.LEFT);
            setLayout(new BorderLayout(5, 5));
            add(localJLabel, "Before");
            add(localMultilineLabel, "Center");
            setOpaque(false);
        }
    }

    @Override
    public void setupWizardButtons() {
        LOGGER.info("Setup the wizard buttons.");
        super.setupWizardButtons();

        executionStatus = scriptContext.get(NodeScriptContextKeys.KEY_SCRIPTEXECUTIONSTATUS, ExecutionStatus.class);

        LOGGER.info("Setup the wizard buttons, current executionStatus: {}", executionStatus);

        // if we go back and come on this page again we must call reset to refresh the labels with the call of
        // initContentPane. reset will call initContentPane.
        if (executionStatus == null) {
            reset();
        }

        LOGGER.info("Disable the finish button, executionStatus: {}", executionStatus);
        fireButtonEvent(ButtonEvent.DISABLE_BUTTON, ButtonNames.FINISH);

        final AtomicBoolean scriptFinishedLock =
            scriptContext.get(NodeScriptContextKeys.KEY_SCRIPTEXECUTIONSTATUSLOCK, AtomicBoolean.class);

        LOGGER.info("Schedule the scriptTerminationWorker.");

        waitForScriptTerminationWorker.schedule(new Runnable() {
            public void run() {

                do {
                    final ExecutionStatus executionStatus =
                        scriptContext.get(NodeScriptContextKeys.KEY_SCRIPTEXECUTIONSTATUS, ExecutionStatus.class);

                    LOGGER.info("Wait for termination of script. Current executionStatus: {}", executionStatus);

                    if (ExecutionStatus.pending == executionStatus || ExecutionStatus.running == executionStatus
                        || executionStatus == null) {
                        LOGGER.info("Wait for the script to finish.");
                        synchronized (scriptFinishedLock) {
                            LOGGER.info("Current scriptFinishedLock: {}", scriptFinishedLock);
                            if (!scriptFinishedLock.get()) {
                                try {
                                    // wait for termination of the script ...
                                    if (executionStatus == null) {
                                        scriptFinishedLock.wait(500);
                                    }
                                    else {
                                        LOGGER.info("Wait for termination of script.");
                                        scriptFinishedLock.wait();
                                    }
                                    LOGGER.info("Wait for termination of script has finished.");
                                }
                                catch (InterruptedException ex) {
                                    LOGGER.warn("Wait for termination of script was interrupted.", ex);
                                }
                            }
                            else {
                                LOGGER.info("Script is signalled as finished already.");
                            }
                        }
                    }
                    else {
                        LOGGER.info("The script has finished already.");
                    }

                    if (CompletionPage.this.executionStatus != executionStatus) {
                        SwingUtilities.invokeLater(new Runnable() {
                            public void run() {
                                final ExecutionStatus executionStatus =
                                    scriptContext.get(NodeScriptContextKeys.KEY_SCRIPTEXECUTIONSTATUS,
                                        ExecutionStatus.class);

                                LOGGER.info("Set the execution result: {}", executionStatus);
                                // CompletionPage.this.executionStatus = executionStatus;
                                reset();

                                repaint();
                            }
                        });
                    }

                    // update the status
                    CompletionPage.this.executionStatus = executionStatus;
                }
                while (ExecutionStatus.running == executionStatus || ExecutionStatus.pending == executionStatus
                    || executionStatus == null);
                LOGGER.info("Apply script on node has finished.");
            }
        }, 0, TimeUnit.MILLISECONDS);
    }
}