package org.bidib.wizard.mvc.main.controller;

import org.bidib.wizard.mvc.main.model.Node;

public interface FeedbackPositionStatusChangeProvider {

    /**
     * @return the selectedNode
     */
    Node getSelectedNode();

}
