package org.bidib.wizard.mvc.simulation.view.panel;

import org.bidib.wizard.mvc.simulation.view.SimulationNodePanel;

public interface SimulationViewContainer {

    /**
     * @param simulationNodePanel
     *            the simulationNodePanel to close
     */
    void close(final SimulationNodePanel simulationNodePanel);
}
