package org.bidib.wizard.mvc.main.model;

public abstract class StartCondition {
    public boolean equals(Object obj) {
        return getClass().getSimpleName().equals(obj.getClass().getSimpleName());
    }

    public int hashCode() {
        return getClass().getSimpleName().hashCode();
    }
}
