package org.bidib.wizard.mvc.main.view.cvdef;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.common.bean.Bean;

/**
 * The CheckBoxStateBean handles the state of the checkboxes based on 2 properties.
 * 
 */
public class CheckBoxStateBean extends Bean {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(CheckBoxStateBean.class);

    public static final String PROPERTY_CHECKBOX_ENABLED = "checkBoxEnabled";

    private boolean enabledState = true;

    private boolean bitSet;

    public void setEnabledState(boolean enabledState) {
        boolean oldCheckBoxEnabled = isCheckBoxEnabled();

        LOGGER.debug("Set enabledState: {}, old: {}", enabledState, this.enabledState);
        this.enabledState = enabledState;

        firePropertyChange(PROPERTY_CHECKBOX_ENABLED, oldCheckBoxEnabled, isCheckBoxEnabled());
    }

    public boolean isEnabledState() {
        return enabledState;
    }

    public void setBitSet(boolean bitSet) {
        boolean oldCheckBoxEnabled = isCheckBoxEnabled();

        LOGGER.debug("Set bitSet: {}, old: {}", bitSet, this.bitSet);
        this.bitSet = bitSet;

        firePropertyChange(PROPERTY_CHECKBOX_ENABLED, oldCheckBoxEnabled, isCheckBoxEnabled());
    }

    public boolean isBitSet() {
        return bitSet;
    }

    public void setCheckBoxEnabled(boolean checkBoxEnabled) {
        // do nothing
    }

    public boolean isCheckBoxEnabled() {
        boolean isCheckBoxEnabled = !isBitSet() && isEnabledState();

        LOGGER.debug("isCheckBoxEnabled, bitSet: {}, enabledState: {}, isCheckBoxEnabled: {}", bitSet, enabledState,
            isCheckBoxEnabled);
        return isCheckBoxEnabled;
    }
}
