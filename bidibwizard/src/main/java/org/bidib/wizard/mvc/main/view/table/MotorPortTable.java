package org.bidib.wizard.mvc.main.view.table;

import java.awt.event.MouseEvent;

import javax.swing.event.ChangeEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;

import org.bidib.wizard.comm.SpeedSteps;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.common.view.slider.SliderValueChangeListener;
import org.bidib.wizard.mvc.main.model.MotorPort;
import org.bidib.wizard.mvc.main.model.MotorPortTableModel;
import org.bidib.wizard.mvc.main.view.panel.MotorPortListPanel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MotorPortTable extends PortTable {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(MotorPortTable.class);

    private final TwoDimensionalArray<MotorSliderEditor> sliderEditors = new TwoDimensionalArray<MotorSliderEditor>();

    public MotorPortTable(final TableModel motorPortTableModel, String emptyTableText) {
        super(motorPortTableModel, emptyTableText);

        int rowHeight = getRowHeight() + 2;
        LOGGER.info("Set row height: {}", rowHeight);

        setRowHeight(rowHeight);
    }

    @Override
    public String getToolTipText(MouseEvent e) {
        String tip = null;
        java.awt.Point p = e.getPoint();
        int rowIndex = rowAtPoint(p);
        int colIndex = columnAtPoint(p);

        if (rowIndex == -1 || colIndex == -1) {
            return tip;
        }

        int realColumnIndex = convertColumnIndexToModel(colIndex);

        if (realColumnIndex == MotorPortTableModel.COLUMN_VALUE) {

            MotorPort port = (MotorPort) getModel().getValueAt(rowIndex, MotorPortTableModel.COLUMN_PORT_INSTANCE);
            if (port.isEnabled()) {
                tip = prepareTooltip(port, port.getValue());
            }
            else {
                tip = super.getToolTipText();
            }

        }
        else {
            tip = super.getToolTipText();
        }
        return tip;
    }

    private String prepareTooltip(MotorPort port, int value) {
        LOGGER.trace("Set the new value: {}", value);

        StringBuffer tip = new StringBuffer(Resources.getString(MotorPortListPanel.class, "absolutevalue"));
        // tip.append(" ").append(targetAbsolute);
        // tip.append(", relative value: ");
        tip.append(value);
        return tip.toString();
    }

    public TableCellEditor getCellEditor(int row, int column) {
        TableCellEditor result = null;

        switch (column) {
            // case MotorPortTableModel.COLUMN_DIRECTION:
            // result = new MotorPortTableCellEditor(0, 255);
            // break;
            case MotorPortTableModel.COLUMN_VALUE:
                result = sliderEditors.get(row, column);
                break;
            default:
                result = super.getCellEditor(row, column);
                break;
        }
        return result;
    }

    public TableCellRenderer getCellRenderer(final int row, int column) {
        TableCellRenderer result = null;

        if (column == MotorPortTableModel.COLUMN_VALUE) {
            LOGGER.debug("Get cell renderer for row: {}", row);
            result = sliderEditors.get(row, column);

            MotorPort motorPort = (MotorPort) getModel().getValueAt(row, MotorPortTableModel.COLUMN_PORT_INSTANCE);

            if (result == null) {
                LOGGER.debug("Prepare new sliderEditor with initial value: {}, motorPort: {}", motorPort.getValue(),
                    motorPort);
                final MotorSliderEditor sliderEditor =
                    new MotorSliderEditor(motorPort, -SpeedSteps.DCC128.getSteps(), SpeedSteps.DCC128.getSteps());
                sliderEditor.setRowNumber(row);
                sliderEditor.createComponent(motorPort.getValue());

                final SliderValueChangeListener sliderValueChangeListener = new SliderValueChangeListener() {

                    @Override
                    public void stateChanged(ChangeEvent e, boolean isAdjusting, int value) {
                        // only handle if not adjusting
                        if (!isAdjusting) {
                            MotorPort motorPort =
                                (MotorPort) getModel().getValueAt(sliderEditor.getRowNumber(),
                                    getModel().getColumnCount() - 1);
                            LOGGER.info("Update motor port: {}, row: {}", motorPort.getDebugString(),
                                sliderEditor.getRowNumber());

                            sliderStateChanged(motorPort);
                        }
                    }
                };
                sliderEditor.setSliderValueChangeListener(sliderValueChangeListener);

                LOGGER.debug("Store new sliderEditor: {}, row: {}, column: {}", sliderEditor, row, column);
                sliderEditors.set(sliderEditor, row, column);
                result = sliderEditor;
            }
            if (result != null) {
                MotorSliderEditor sliderEditor = (MotorSliderEditor) result;
                sliderEditor.setEnabled(motorPort.isEnabled());
            }
            LOGGER.debug("Return sliderEditor: {}, row: {}", result, row);
        }
        else {
            result = super.getCellRenderer(row, column);
        }
        return result;
    }

    @Override
    public void clearTable() {
        LOGGER.debug("clearTable, remove all rows and remove all slider editors");
        // remove all rows and remove all slider editors
        for (int row = 0; row < getModel().getRowCount(); row++) {
            SliderEditor current = sliderEditors.get(row, MotorPortTableModel.COLUMN_VALUE);
            if (current != null) {
                current.setSliderValueChangeListener(null);
            }
        }

        ((DefaultTableModel) getModel()).setRowCount(0);
        sliderEditors.clear();
    }

    public void updateSliderPosition(int row, int column, int value) {
        SliderEditor current = sliderEditors.get(row, column);
        if (current != null) {
            LOGGER.info("updateSliderPosition, set the new slider value: {}", value);
            current.setValue(value);
        }
    }

    protected void sliderStateChanged(MotorPort motorPort) {
        LOGGER.info("The slider state has changed, motorPort: {}", motorPort);
    }

}
