package org.bidib.wizard.mvc.stepcontrol.view.wizard;

import java.awt.BorderLayout;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import org.apache.commons.lang3.StringUtils;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.common.utils.ImageUtils;
import org.bidib.wizard.mvc.common.view.wizard.JWizardComponents;
import org.bidib.wizard.mvc.stepcontrol.model.ConfigurationWizardModel;
import org.bidib.wizard.mvc.stepcontrol.model.TurnTableType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.common.swing.MnemonicUtils;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.debug.FormDebugPanel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.FormLayout;

public class TableTypePanel extends AbstractWizardPanel {

    private static final Logger LOGGER = LoggerFactory.getLogger(TableTypePanel.class);

    private ConfigurationWizardModel configurationWizardModel;

    private ValueModel turnTableTypeModel;

    private JComponent[] turnTableTypeButtons;

    public TableTypePanel(final JWizardComponents wizardComponents,
        final ConfigurationWizardModel configurationWizardModel) {
        super(wizardComponents);

        this.configurationWizardModel = configurationWizardModel;
    }

    @Override
    protected void initPanel() {
        super.initPanel();

        // prepare the form
        DefaultFormBuilder builder = null;
        boolean debugDialog = false;
        if (debugDialog) {
            JPanel panel = new FormDebugPanel();
            builder = new DefaultFormBuilder(new FormLayout("p, 3dlu, p:g"), panel);
        }
        else {
            JPanel panel = new JPanel(new BorderLayout());
            builder = new DefaultFormBuilder(new FormLayout("p, 3dlu, p:g"), panel);
        }
        builder.border(Borders.TABBED_DIALOG);

        builder.append(new JLabel(Resources.getString(getClass(), "message")), 3);

        // add the components
        turnTableTypeModel =
            new PropertyAdapter<ConfigurationWizardModel>(configurationWizardModel,
                ConfigurationWizardModel.PROPERTYNAME_TURNTABLE_TYPE, true);
        turnTableTypeButtons = new JComponent[TurnTableType.values().length];
        int index = 0;

        DefaultFormBuilder turnTableTypeBuilder = new DefaultFormBuilder(new FormLayout("pref"));

        for (TurnTableType turnTableType : TurnTableType.values()) {
            String iconKey = Resources.getString(TurnTableType.class, turnTableType.getKey() + ".icon");
            ImageIcon radioIcon = ImageUtils.createImageIcon(getClass(), "/icons/stepcontrol/" + iconKey);

            JRadioButton radio =
                createRadioButton(turnTableTypeModel, turnTableType,
                    Resources.getString(TurnTableType.class, turnTableType.getKey()), radioIcon.toString());
            turnTableTypeButtons[index++] = radio;

            // add radio button
            turnTableTypeBuilder.append(radio);
        }

        builder.append(turnTableTypeBuilder.build(), 3);
        builder.nextLine();

        this.panel = builder.build();

        LOGGER.info("initPanel finished.");
    }

    public static JRadioButton createRadioButton(ValueModel model, Object choice, String markedText, String iconPath) {

        JRadioButton radioButton = null;
        if (StringUtils.isNotBlank(iconPath)) {
            StringBuilder sb = new StringBuilder("<html>");
            sb.append("<div style=\"valign=\"middle\">");
            sb.append("<table><tr><td>");
            sb.append("<img src='").append(iconPath).append("'>");
            sb.append("</td>");
            sb.append("<td>");
            sb.append(markedText);
            sb.append("</td>");
            sb.append("</tr></table>");
            sb.append("</div></html>");
            LOGGER.info("Create button with html: {}", sb.toString());
            radioButton = new JRadioButton();
            radioButton.setContentAreaFilled(false);
            Bindings.bind(radioButton, model, choice);
            MnemonicUtils.configure(radioButton, markedText);

            radioButton.setText(sb.toString());

        }
        else {
            radioButton = new JRadioButton();
            radioButton.setContentAreaFilled(false);
            Bindings.bind(radioButton, model, choice);
            MnemonicUtils.configure(radioButton, markedText);
        }
        return radioButton;
    }

    @Override
    public void next() {
        // update the model

        super.next();
    }
}
