package org.bidib.wizard.mvc.script.controller;

public interface NodeScriptKeywords {

    public static final String KEYWORD_INPUT = "##input";

    public static final String KEYWORD_INSTRUCTION = "##instruction";

    public static final String KEYWORD_DEFINE = "##define";

    public static final String KEYWORD_SCRIPTVERSION = "##scriptversion";

    public static final String KEYWORD_HELP = "##help";

    public static final String KEYWORD_WEB = "##web";

    public static final String KEYWORD_ABOUT = "##about";

    public static final String KEYWORD_APPLICATION = "##application";

    public static final String KEYWORD_REQUIRE = "##require";
}
