package org.bidib.wizard.mvc.main.model.listener;

import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.mvc.main.model.Port;

public interface PortListener<S extends BidibStatus> {
    /**
     * The port label has changed.
     * 
     * @param port
     *            the port
     * @param label
     *            the new port label
     */
    void labelChanged(Port<S> port, String label);

    /**
     * The status of the port has changed.
     * 
     * @param port
     *            the port
     * @param status
     *            the new status
     */
    void statusChanged(Port<S> port, S status);

    /**
     * The configuration of the port has been changed.
     * 
     * @param port
     *            the port
     */
    void configChanged(Port<S> port);
}
