package org.bidib.wizard.mvc.error.view;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.error.model.ErrorModel;
import org.bidib.wizard.mvc.error.model.listener.ErrorListener;

public abstract class ErrorView {
    public ErrorView(ErrorModel model) {
        model.addErrorListener(new ErrorListener() {
            @Override
            public void errorChanged(final String errorMessage, final String stackTrace) {
                new Thread() {
                    @Override
                    public void run() {
                        JDialog dialog = getDialog(errorMessage, stackTrace);

                        dialog.setVisible(true);
                        dialog.toFront();
                        dialog.repaint();
                    }
                }.start();
            }
        });
    }

    private JDialog getDialog(String errorMessage, String stackTrace) {
        final JOptionPane pane = new JOptionPane(getPanel(errorMessage, stackTrace), JOptionPane.ERROR_MESSAGE);
        JDialog result = pane.createDialog(null, Resources.getString(ErrorView.class, "title"));

        result.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                ErrorView.this.windowClosed();
            }

            public void windowDeactivated(WindowEvent e) {
                Object value = pane.getValue();

                if (value instanceof Integer && ((Integer) value).intValue() == JOptionPane.OK_OPTION) {
                    ErrorView.this.windowClosed();
                }
            }
        });
        result.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        result.setResizable(true);
        result.setSize(new Dimension(800, 480));
        return result;
    }

    private JPanel getPanel(String errorMessage, String stackTrace) {
        JPanel result = new JPanel(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        c.anchor = GridBagConstraints.FIRST_LINE_START;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridwidth = 1;
        c.gridheight = 1;
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1;
        c.weighty = 0;
        c.insets = new Insets(5, 5, 5, 5);

        JTextField errorField = new JTextField();

        errorField.setEditable(false);
        errorField.setText(errorMessage);
        result.add(errorField, c);

        c.fill = GridBagConstraints.NONE;
        c.gridy++;

        JLabel details = new JLabel(Resources.getString(ErrorView.class, "details") + ":");

        result.add(details, c);

        JTextArea protocol = new JTextArea("", 0, 0);

        protocol.setEditable(false);
        protocol.setLineWrap(true);
        protocol.setText(stackTrace);
        protocol.setWrapStyleWord(true);

        JScrollPane scrollPane = new JScrollPane(protocol);

        c.fill = GridBagConstraints.BOTH;
        c.gridy++;
        c.weighty = 1;
        result.add(scrollPane, c);
        return result;
    }

    public abstract void windowClosed();
}
