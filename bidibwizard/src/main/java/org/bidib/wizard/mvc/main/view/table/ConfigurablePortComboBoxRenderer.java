package org.bidib.wizard.mvc.main.view.table;

import java.awt.Component;

import javax.swing.JTable;

import org.bidib.wizard.mvc.main.model.ConfigurablePort;

public class ConfigurablePortComboBoxRenderer<E> extends ComboBoxRenderer<E> {

    private static final long serialVersionUID = 1L;

    private byte[] pcfgType;

    private int portInstanceColumn;

    /**
     * Creates a new instance of ConfigurablePortComboBoxRenderer.
     * 
     * @param portInstanceColumn
     *            the port instance column
     * @param items
     *            the items to display in the ComboBox
     * @param pcfgType
     *            the types to check in the port config. If the type is not found in the port config the renderer is
     *            displayed disabled.
     */
    public ConfigurablePortComboBoxRenderer(int portInstanceColumn, E[] items, byte... pcfgType) {
        super(items);
        this.portInstanceColumn = portInstanceColumn;
        this.pcfgType = pcfgType;
    }

    public Component getTableCellRendererComponent(
        JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

        Component comp = null;

        // The value must be a port. Return the port value from the tableModel instead of the field value.
        Object portValue = table.getModel().getValueAt(row, portInstanceColumn);

        if (portValue instanceof ConfigurablePort<?>) {

            ConfigurablePort<?> port = (ConfigurablePort<?>) portValue;

            comp = super.getTableCellRendererComponent(table, getCurrentValue(port), isSelected, hasFocus, row, column);

            boolean enabled = port.isEnabled() && port.isPortConfigKeySupported(pcfgType);

            comp.setEnabled(enabled);
        }
        else {
            comp = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        }
        return comp;
    }

    protected Object getCurrentValue(ConfigurablePort<?> port) {
        return port;
    }
}
