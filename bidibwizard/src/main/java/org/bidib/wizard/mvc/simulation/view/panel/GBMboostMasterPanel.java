package org.bidib.wizard.mvc.simulation.view.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.border.EmptyBorder;

import org.apache.commons.lang.StringUtils;
import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.Feature;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.core.utils.NodeUtils;
import org.bidib.jbidibc.simulation.SimulatorNode;
import org.bidib.wizard.comm.FeedbackPortStatus;
import org.bidib.wizard.dk.nullesoft.Airlog.LED;
import org.bidib.wizard.mvc.main.model.FeedbackPort;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.simulation.events.FeedbackConfidenceSetEvent;
import org.bidib.wizard.simulation.events.FeedbackConfidenceStatusEvent;
import org.bidib.wizard.simulation.events.FeedbackPortSetStatusEvent;
import org.bidib.wizard.simulation.events.FeedbackPortStatusEvent;
import org.bushe.swing.event.EventBus;
import org.bushe.swing.event.annotation.AnnotationProcessor;
import org.bushe.swing.event.annotation.EventSubscriber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.debug.FormDebugPanel;
import com.jgoodies.forms.layout.FormLayout;
import com.vlsolutions.swing.docking.DockKey;

public class GBMboostMasterPanel extends AbstractSimulatorNodePanel {
    private static final Logger LOGGER = LoggerFactory.getLogger(GBMboostMasterPanel.class);

    private final DockKey DOCKKEY;

    private final SimulationViewContainer container;

    private Map<Integer, LED> feedbackPortLeds = new HashMap<>();

    private final AtomicBoolean statusFreeze = new AtomicBoolean();

    private final AtomicBoolean statusValid = new AtomicBoolean();

    private final AtomicBoolean statusSignal = new AtomicBoolean();

    private JToggleButton freezeButton;

    public GBMboostMasterPanel(final SimulationViewContainer container, final Node node) {
        super(node);
        this.container = container;
        String uuid = NodeUtils.getUniqueIdAsString(node.getUniqueId());
        DOCKKEY = new DockKey(getClass().getSimpleName() + "-" + StringUtils.trimToEmpty(uuid));

        // enable floating
        DOCKKEY.setFloatEnabled(true);
    }

    @Override
    public void createComponents(SimulatorNode simulator) {
        DefaultFormBuilder formBuilder = null;
        boolean debug = false;
        if (debug) {
            JPanel panel = new FormDebugPanel();
            formBuilder = new DefaultFormBuilder(new FormLayout("pref, 3dlu, pref, fill:50dlu:grow"), panel);
        }
        else {
            JPanel panel = new JPanel(new BorderLayout());
            formBuilder = new DefaultFormBuilder(new FormLayout("pref, 3dlu, pref, fill:50dlu:grow"), panel);
        }

        Feature feedbackPortCount = simulator.getFeature(BidibLibrary.FEATURE_BM_SIZE);
        if (feedbackPortCount != null && feedbackPortCount.getValue() > 0) {
            JPanel feedbackPorts = new JPanel();
            feedbackPorts.setBorder(new EmptyBorder(5, 5, 5, 5));

            feedbackPorts.setLayout(new GridLayout(2, 8, 5, 5));

            int feedbackPortCountValue = feedbackPortCount.getValue();

            LOGGER.info("Initializing lightPort LEDs.");
            for (int i = 0; i < feedbackPortCountValue; i++) {
                final LED led = new LED(Color.GREEN, Color.RED, false);
                led.setSize(20, 20);
                led.setToolTipText("Feedback " + i);
                led.putClientProperty("portId", Integer.valueOf(i));

                led.addMouseListener(new MouseAdapter() {

                    @Override
                    public void mouseClicked(MouseEvent e) {
                        // TODO Auto-generated method stub
                        LOGGER.info("Mouse clicked on led: {}", led);

                        Integer portId = (Integer) led.getClientProperty("portId");
                        String nodeAddress = ByteUtils.bytesToHex(getNode().getNode().getAddr());
                        FeedbackPortStatus feedbackPortStatus =
                            !led.isUsePrimary() ? FeedbackPortStatus.OCCUPIED : FeedbackPortStatus.FREE;
                        FeedbackPortSetStatusEvent feedbackPortSetStatusEvent =
                            new FeedbackPortSetStatusEvent(nodeAddress, portId, feedbackPortStatus);

                        LOGGER.info("Publish the feedbackPortSetStatusEvent: {}", feedbackPortSetStatusEvent);
                        EventBus.publish(feedbackPortSetStatusEvent);
                    }
                });

                feedbackPortLeds.put(Integer.valueOf(i), led);

                feedbackPorts.add(led);
            }

            formBuilder.append("Feedback ports", feedbackPorts, 2);
        }
        else {
            LOGGER.warn("No configured FeedbackPorts available.");
        }

        freezeButton = new JToggleButton("Freeze");
        freezeButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                // toggle the freeze status
                statusFreeze.set(!statusFreeze.get());

                String nodeAddress = ByteUtils.bytesToHex(getNode().getNode().getAddr());

                FeedbackConfidenceSetEvent feedbackConfidenceEvent =
                    new FeedbackConfidenceSetEvent(nodeAddress, statusValid.get(), statusFreeze.get(),
                        statusSignal.get());
                LOGGER.info("Publish the feedbackConfidenceEvent: {}", feedbackConfidenceEvent);
                EventBus.publish(feedbackConfidenceEvent);

            }
        });
        freezeButton.setForeground(Color.GREEN.darker());

        formBuilder.append(freezeButton);

        contentPanel = formBuilder.build();

        AnnotationProcessor.process(this);

        // query the status of the input ports
        simulator.queryStatus(FeedbackPort.class);
    }

    @Override
    public DockKey getDockKey() {
        return DOCKKEY;
    }

    @Override
    public void stop() {
        AnnotationProcessor.unprocess(this);

        container.close(this);
    }

    @EventSubscriber(eventClass = FeedbackPortStatusEvent.class)
    public void updateStatus(FeedbackPortStatusEvent statusEvent) {
        LOGGER.info("The feedbackport status has changed, status: {}, node: {}", statusEvent, getNode());

        // check if the node address matches
        if (!isMatchingAddress(statusEvent.getNodeAddr())) {
            return;
        }

        FeedbackPort port = statusEvent.getPort();
        FeedbackPortStatus status = statusEvent.getStatus();

        int id = port.getId();

        LED led = feedbackPortLeds.get(Integer.valueOf(id));

        if (led != null) {
            LOGGER.trace("Found led to switch.");
            if (status == FeedbackPortStatus.FREE) {
                led.usePrimary();
            }
            else {
                led.useSecondary();
            }
        }
        else {
            LOGGER.trace("Led not found.");
        }
    }

    @EventSubscriber(eventClass = FeedbackConfidenceStatusEvent.class)
    public void updateFeedbackConfidenceStatus(FeedbackConfidenceStatusEvent statusEvent) {
        LOGGER.info("The feedbackport confidence status has changed, status: {}, node: {}", statusEvent, getNode());

        // check if the node address matches
        if (!isMatchingAddress(statusEvent.getNodeAddr())) {
            return;
        }

        statusValid.set(statusEvent.getValid());
        statusFreeze.set(statusEvent.getFreeze());
        statusSignal.set(statusEvent.getSignal());

        freezeButton.setSelected(statusFreeze.get());
        freezeButton.setForeground(statusFreeze.get() ? Color.RED : Color.GREEN.darker());
    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        StringBuffer sb = new StringBuffer(getClass().getSimpleName());
        sb.append(", dockKey: ");
        sb.append(DOCKKEY).append(", node: ").append(getNode());

        return sb.toString();
    }
}
