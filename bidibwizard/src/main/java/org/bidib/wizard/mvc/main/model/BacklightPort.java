package org.bidib.wizard.mvc.main.model;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.util.LinkedHashMap;
import java.util.Map;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.enumeration.BacklightPortEnum;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.port.BytePortConfigValue;
import org.bidib.jbidibc.core.port.Int16PortConfigValue;
import org.bidib.jbidibc.core.port.PortConfigValue;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.wizard.comm.BacklightPortStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The BacklightPort is a special port because it has no BidibStatus but an int value.
 */
public class BacklightPort extends Port<BacklightPortStatus> {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(BacklightPort.class);

    static {
        try {
            // Q: why is this needed? A: export of beans with XMLDecoder
            PropertyDescriptor[] descriptor = Introspector.getBeanInfo(BacklightPort.class).getPropertyDescriptors();

            for (int i = 0; i < descriptor.length; i++) {
                PropertyDescriptor propertyDescriptor = descriptor[i];
                if (propertyDescriptor.getName().equals("value")) {
                    propertyDescriptor.setValue("transient", Boolean.TRUE);
                }
                else if (propertyDescriptor.getName().equals("preAdjustingValue")) {
                    propertyDescriptor.setValue("transient", Boolean.TRUE);
                }
                else if (propertyDescriptor.getName().equals("adjusting")) {
                    propertyDescriptor.setValue("transient", Boolean.TRUE);
                }
            }
        }
        catch (IntrospectionException e) {
            throw new RuntimeException(e);
        }
    }

    public static final int DIMM_STRETCHMAX8_8 = 65535;

    public static final String PROPERTY_DIM_SLOPE_UP = "dimSlopeUp";

    public static final String PROPERTY_DIM_SLOPE_DOWN = "dimSlopeDown";

    public static final String PROPERTY_DMX_MAPPING = "dmxMapping";

    // time for dimming towards ON: 0=fast ... 255=slow
    private int dimSlopeUp;

    // time for dimming towards OFF: 0=fast ... 255=slow
    private int dimSlopeDown;

    // mapping to physical channel
    private int dmxMapping;

    private int value;

    private int preAdjustingValue = -1;

    private boolean adjusting;

    public BacklightPort() {
        super(null);
    }

    public BacklightPort(GenericPort genericPort) {
        super(genericPort);
    }

    /**
     * @return the dimSlopeUp
     */
    public int getDimSlopeUp() {
        return dimSlopeUp;
    }

    /**
     * @param dimSlopeUp
     *            the dimSlopeUp to set
     */
    public void setDimSlopeUp(int dimSlopeUp) {
        int oldValue = this.dimSlopeUp;
        this.dimSlopeUp = dimSlopeUp;
        firePropertyChange(PROPERTY_DIM_SLOPE_UP, oldValue, this.dimSlopeUp);
    }

    /**
     * @return the dimSlopeDown
     */
    public int getDimSlopeDown() {
        return dimSlopeDown;
    }

    /**
     * @param dimSlopeDown
     *            the dimSlopeDown to set
     */
    public void setDimSlopeDown(int dimSlopeDown) {
        int oldValue = this.dimSlopeDown;
        this.dimSlopeDown = dimSlopeDown;
        firePropertyChange(PROPERTY_DIM_SLOPE_DOWN, oldValue, this.dimSlopeDown);
    }

    /**
     * @return the dmxMapping
     */
    public int getDmxMapping() {
        LOGGER.trace("Return the dmxMapping: {}", dmxMapping);
        return dmxMapping;
    }

    /**
     * @param dmxMapping
     *            the dmxMapping to set
     */
    public void setDmxMapping(int dmxMapping) {
        LOGGER.trace("Set the dmxMapping: {}", dmxMapping);
        int oldValue = this.dmxMapping;
        this.dmxMapping = dmxMapping;
        firePropertyChange(PROPERTY_DMX_MAPPING, oldValue, this.dmxMapping);
    }

    /**
     * Get the backlight value.
     * 
     * @return backlight value
     */
    public int getValue() {
        return value;
    }

    /**
     * Set the backlight value.
     * 
     * @param value
     *            backlight value
     */
    public void setValue(int value, boolean force) {
        if (force || getPreAdjustingValue() == -1) {
            LOGGER.info("Set the new value to port: {}", value);
            this.value = value;
        }
        else {
            LOGGER.info("Prevent set value because adjusting: {}", value);
        }
    }

    /**
     * Get the absolute brightness value from the provided relative value in %. The minimum value 0% is the PORTSTAT
     * value 0, the maximum value 100% is the PORTSTAT value 255, 50% is the PORTSTAT value 127.
     * 
     * @param value
     *            relative backlight brightness value in %
     * 
     * @return absolute backlight brightness value
     */
    public int getAbsoluteValue(int value) {
        if (value < 0 || value > 100) {
            throw new IllegalArgumentException("The allowed range is 0-100. Provided value: " + value);
        }
        int stepWidth = (value * 255) / 100;
        return stepWidth;
    }

    /**
     * Get the relative servo destination value in % from the absolute value. The minimum value 0% is the PORTSTAT value
     * 0, the maximum value 100% is the PORTSTAT value 255, 50% is the PORTSTAT value 127.
     * 
     * @param value
     *            absolute servo destination value
     * 
     * @return relative servo destination value in %
     */
    public int getRelativeValue(int value) {
        if (value < 0 || value > 255) {
            throw new IllegalArgumentException("The allowed range is 0-255. Provided value: " + value);
        }
        long result = Math.round(((double) value * 100) / 255);
        return (int) result;
    }

    /**
     * Get the relative backlight brightness value in % from the absolute value. The minimum value 0% is the PORTSTAT
     * value 0, the maximum value 100% is the PORTSTAT value 255, 50% is the PORTSTAT value 127.
     * 
     * @return relative backlight brightness value in %
     */
    public int getRelativeValue() {
        long result = Math.round(((double) value * 100) / 255);
        return (int) result;
    }

    /**
     * @return the preAdjustingValue
     */
    public int getPreAdjustingValue() {
        return preAdjustingValue;
    }

    /**
     * @param preAdjustingValue
     *            the preAdjustingValue to set
     */
    public void setPreAdjustingValue(int preAdjustingValue) {
        this.preAdjustingValue = preAdjustingValue;
    }

    /**
     * @return the adjusting
     */
    public boolean isAdjusting() {
        return adjusting;
    }

    /**
     * @param adjusting
     *            the adjusting to set
     */
    public void setAdjusting(boolean adjusting) {
        this.adjusting = adjusting;
    }

    public String getDebugString() {
        return getClass().getSimpleName() + "[dimSlopeDown=" + dimSlopeDown + ",dimSlopeUp=" + dimSlopeUp
            + ",dmxMapping=" + dmxMapping + ",value=" + value + ",preAdjustingValue=" + preAdjustingValue
            + ",adjusting=" + adjusting + "]";
    }

    @Override
    public byte[] getPortConfig() {
        return new byte[] { ByteUtils.getLowByte(dimSlopeDown), ByteUtils.getLowByte(dimSlopeUp),
            ByteUtils.getLowByte(dmxMapping), 0 };
    }

    public void setPortConfig(int dimSlopeDown, int dimSlopeUp, int dmxMapping) {
        LOGGER.trace("Set port config, id: {}, dimSlopeDown: {}, dimSlopeUp: {}, dmxMapping: {}", getId(), dimSlopeDown,
            dimSlopeUp, dmxMapping);

        setDimSlopeDown(dimSlopeDown);
        getKnownPortConfigKeys().add(Byte.valueOf(BidibLibrary.BIDIB_PCFG_DIMM_DOWN_8_8));
        setDimSlopeUp(dimSlopeUp);
        getKnownPortConfigKeys().add(Byte.valueOf(BidibLibrary.BIDIB_PCFG_DIMM_UP_8_8));
        setDmxMapping(dmxMapping);
        getKnownPortConfigKeys().add(Byte.valueOf(BidibLibrary.BIDIB_PCFG_OUTPUT_MAP));
    }

    public void setPortConfig(byte[] portConfig) {
        LOGGER.info("Set the backlight port parameters: {}", ByteUtils.bytesToHex(portConfig));

        setPortConfigEnabled(true);

        setDimSlopeDown(ByteUtils.getInt(portConfig[0]));
        getKnownPortConfigKeys().add(Byte.valueOf(BidibLibrary.BIDIB_PCFG_DIMM_DOWN_8_8));
        setDimSlopeUp(ByteUtils.getInt(portConfig[1]));
        getKnownPortConfigKeys().add(Byte.valueOf(BidibLibrary.BIDIB_PCFG_DIMM_UP_8_8));
        setDmxMapping(ByteUtils.getInt(portConfig[2]));
        getKnownPortConfigKeys().add(Byte.valueOf(BidibLibrary.BIDIB_PCFG_OUTPUT_MAP));
    }

    @Override
    public void setPortConfigX(Map<Byte, PortConfigValue<?>> portConfig) {
        Number dimMax = getPortConfigValue(BidibLibrary.BIDIB_PCFG_DIMM_UP_8_8, portConfig);
        if (dimMax != null) {
            setDimSlopeUp(ByteUtils.getWORD(dimMax.intValue()));
        }
        else {
            LOGGER.warn("No value received for BIDIB_PCFG_DIMM_UP_8_8!");
        }
        Number dimMin = getPortConfigValue(BidibLibrary.BIDIB_PCFG_DIMM_DOWN_8_8, portConfig);
        if (dimMin != null) {
            setDimSlopeDown(ByteUtils.getWORD(dimMin.intValue()));
        }
        else {
            LOGGER.warn("No value received for BIDIB_PCFG_DIMM_DOWN_8_8!");
        }

        Number outputMap = getPortConfigValue(BidibLibrary.BIDIB_PCFG_OUTPUT_MAP, portConfig);
        if (outputMap != null) {
            setDmxMapping(ByteUtils.getInt(outputMap.byteValue()) + 1 /* dmxMappingOffset */);
        }
        else {
            LOGGER.warn("No value received for BIDIB_PCFG_OUTPUT_MAP!");
        }

        // call the super class
        super.setPortConfigX(portConfig);
    }

    @Override
    public Map<Byte, PortConfigValue<?>> getPortConfigX() {

        if (genericPort != null) {
            return genericPort.getPortConfig();
        }

        Map<Byte, PortConfigValue<?>> portConfigX = new LinkedHashMap<Byte, PortConfigValue<?>>();

        // add the dimm up value
        if (isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_DIMM_UP_8_8)) {
            portConfigX.put(BidibLibrary.BIDIB_PCFG_DIMM_UP_8_8, new Int16PortConfigValue(dimSlopeUp));
        }
        else if (isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_DIMM_UP)) {
            portConfigX.put(BidibLibrary.BIDIB_PCFG_DIMM_UP, new BytePortConfigValue(ByteUtils.getLowByte(dimSlopeUp)));
        }
        else {
            LOGGER.warn("No DIMM_UP config available.");
        }

        // add the dimm down value
        if (isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_DIMM_DOWN_8_8)) {
            portConfigX.put(BidibLibrary.BIDIB_PCFG_DIMM_DOWN_8_8, new Int16PortConfigValue(dimSlopeDown));
        }
        else if (isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_DIMM_DOWN)) {
            portConfigX.put(BidibLibrary.BIDIB_PCFG_DIMM_DOWN,
                new BytePortConfigValue(ByteUtils.getLowByte(dimSlopeDown)));
        }
        else {
            LOGGER.warn("No DIMM_DOWN config available.");
        }

        // add the port on
        if (isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_OUTPUT_MAP)) {
            portConfigX.put(BidibLibrary.BIDIB_PCFG_OUTPUT_MAP,
                new BytePortConfigValue(ByteUtils.getLowByte(dmxMapping)));
        }
        return portConfigX;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof BacklightPort) {
            return ((BacklightPort) obj).getId() == getId();
        }
        return false;
    }

    @Override
    public int hashCode() {
        return getId();
    }

    @Override
    protected LcOutputType getPortType() {
        return LcOutputType.BACKLIGHTPORT;
    }

    @Override
    protected BacklightPortStatus internalGetStatus() {
        return BacklightPortStatus.valueOf(BacklightPortEnum.valueOf(genericPort.getPortStatus()));
    }
}
