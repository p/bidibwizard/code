package org.bidib.wizard.mvc.common.view.icon;

import java.awt.Dimension;

import javax.swing.JLabel;

public class RotateLabel extends JLabel {

    private static final long serialVersionUID = 1L;

    private final RotatedIcon icon;

    private final Dimension size;

    public RotateLabel(final RotatedIcon icon) {
        this.icon = icon;
        size = new Dimension(icon.getIconHeight(), icon.getIconHeight());
        setIcon(icon);
    }

    @Override
    public Dimension getMinimumSize() {
        // TODO Auto-generated method stub
        return size;
    }

    @Override
    public Dimension getMaximumSize() {
        // TODO Auto-generated method stub
        return size;
    }

    @Override
    public Dimension getPreferredSize() {
        // TODO Auto-generated method stub
        return size;
    }
}
