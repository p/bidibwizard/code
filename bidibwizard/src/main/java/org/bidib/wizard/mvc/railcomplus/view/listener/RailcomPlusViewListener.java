package org.bidib.wizard.mvc.railcomplus.view.listener;

import java.util.List;

import org.bidib.jbidibc.core.RcPlusBindData;
import org.bidib.jbidibc.core.DecoderUniqueIdData;
import org.bidib.jbidibc.core.enumeration.RcPlusPhase;
import org.bidib.wizard.mvc.railcomplus.model.RailcomPlusDecoderModel;

public interface RailcomPlusViewListener {

    /**
     * Check if the feature FEATURE_GEN_RCPLUS_AVAILABLE is available and active.
     * 
     * @return the feature is available and is active.
     */
    boolean isFeatureRcplusAvailable();

    /**
     * Read the TID.
     */
    void readTid();

    /**
     * Update the SID
     */
    void updateSid();

    /**
     * Update the TID
     */
    void updateTid();

    /**
     * Send the PING_ONCE command.
     * 
     * @param phase
     *            the phase
     */
    void pingOnce(RcPlusPhase phase);

    /**
     * Send the PING command with the specified interval.
     * 
     * @param interval
     *            the repeat interval in units of 100ms, a value of 0 stops the sending of PINGs
     */
    void ping(int interval);

    /**
     * Find RailCom+ decoders.
     * 
     * @param phase
     *            the phase
     * @param decoder
     *            the decoder MUN and MID
     */
    void find(RcPlusPhase phase, DecoderUniqueIdData decoder);

    /**
     * Bind a RailCom+ decoder.
     * 
     * @param bindData
     *            the decoder MUN and MID, and the address
     */
    void bind(RcPlusBindData bindData);

    /**
     * Query the address mode of the decoder via POM.
     * 
     * @param decoder
     *            the decoder
     */
    void queryAddressMode(final RailcomPlusDecoderModel decoder);

    /**
     * Query the address mode of the decoder via POM.
     * 
     * @param decoder
     *            the decoder
     * @param cvNumbers
     *            the list of cv numbers to query
     */
    void queryAddress(final RailcomPlusDecoderModel decoder, final List<Integer> cvNumbers);
}
