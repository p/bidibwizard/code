package org.bidib.wizard.mvc.main.model;

import org.bidib.jbidibc.core.enumeration.TimeBaseUnitEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.beans.Model;

public class AccessorySwitchTimeModel extends Model {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(AccessorySwitchTimeModel.class);

    public static final String PROPERTYNAME_SWITCH_TIME = "switchTime";

    public static final String PROPERTYNAME_TIME_BASE_UNIT = "timeBaseUnit";

    /**
     * The accessory switch time
     */
    private Integer switchTime;

    /**
     * The accessory switch time bas unit
     */
    private TimeBaseUnitEnum timeBaseUnit = TimeBaseUnitEnum.UNIT_100MS;

    /**
     * @return the switchTime
     */
    public Integer getSwitchTime() {
        return switchTime;
    }

    /**
     * @param switchTime
     *            the switchTime to set
     */
    public void setSwitchTime(Integer switchTime) {
        Integer oldValue = this.switchTime;
        this.switchTime = switchTime;

        firePropertyChange(PROPERTYNAME_SWITCH_TIME, oldValue, switchTime);
    }

    /**
     * @return the timeBaseUnit
     */
    public TimeBaseUnitEnum getTimeBaseUnit() {
        return timeBaseUnit;
    }

    /**
     * @param timeBaseUnit
     *            the timeBaseUnit to set
     */
    public void setTimeBaseUnit(TimeBaseUnitEnum timeBaseUnit) {
        TimeBaseUnitEnum oldValue = this.timeBaseUnit;
        this.timeBaseUnit = timeBaseUnit;

        firePropertyChange(PROPERTYNAME_TIME_BASE_UNIT, oldValue, timeBaseUnit);
    }

}
