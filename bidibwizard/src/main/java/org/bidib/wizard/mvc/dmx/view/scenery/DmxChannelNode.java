package org.bidib.wizard.mvc.dmx.view.scenery;

import javax.swing.tree.DefaultMutableTreeNode;

import org.bidib.wizard.mvc.dmx.model.DmxChannel;

public class DmxChannelNode extends DefaultMutableTreeNode {
    private static final long serialVersionUID = 1L;

    public DmxChannelNode(final DmxChannel dmxChannel) {
        super(dmxChannel);
    }

    public DmxChannel getDmxChannel() {
        return (DmxChannel) getUserObject();
    }
}
