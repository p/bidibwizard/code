package org.bidib.wizard.mvc.features.view.panel;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.lang3.StringUtils;
import org.bidib.jbidibc.core.Feature;
import org.bidib.jbidibc.core.schema.BidibFactory;
import org.bidib.jbidibc.core.schema.bidib2.DocumentationType;
import org.bidib.jbidibc.core.schema.bidib2.FeatureCode;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.utils.XmlLocaleUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FeatureTableModel extends AbstractTableModel {
    private static final Logger LOGGER = LoggerFactory.getLogger(FeatureTableModel.class);

    private static final long serialVersionUID = 1L;

    public static final int COLUMN_FEATURE_NAME = 0;

    public static final int COLUMN_FEATURE_ID = 1;

    public static final int COLUMN_FEATURE_VALUE = 2;

    private List<FeatureCode> featureCodes;

    private static final String[] _COLUMN_NAMES_ =
        { Resources.getString(FeatureTableModel.class, "featureName"),
            Resources.getString(FeatureTableModel.class, "featureId"),
            Resources.getString(FeatureTableModel.class, "value") };

    private List<Feature> features = new LinkedList<Feature>();

    private String lang;

    public FeatureTableModel() {
        // load the feature codes
        featureCodes = BidibFactory.getFeatureCodes();

        lang = XmlLocaleUtils.getXmlLocaleVendorCV();
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        if (column == COLUMN_FEATURE_VALUE) {
            return true;
        }
        return false;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (columnIndex == COLUMN_FEATURE_VALUE) {
            return Integer.class;
        }
        return super.getColumnClass(columnIndex);
    }

    public void updateRow(Feature feature) {

        // check if the feature is already displayed, if yes: update the row, otherwise: add new row
        if (features.contains(feature)) {
            LOGGER.debug("Feature is already in list and will be removed: {}", feature);
            features.remove(feature);
        }
        // create a copy of the feature to store in the feature list
        Feature copy = new Feature(feature.getType(), feature.getValue());
        features.add(copy);

        fireTableDataChanged();
    }

    @Override
    public int getColumnCount() {
        return _COLUMN_NAMES_.length;
    }

    @Override
    public String getColumnName(int column) {
        return _COLUMN_NAMES_[column];
    }

    @Override
    public int getRowCount() {
        return features.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Feature feature = features.get(rowIndex);
        switch (columnIndex) {
            case COLUMN_FEATURE_NAME:
                return getFeatureName(feature);
            case COLUMN_FEATURE_ID:
                return feature.getFeatureName();
            default:
                return feature;
        }
    }

    private String getFeatureName(Feature feature) {

        FeatureCode featureCode = IterableUtils.find(featureCodes, new Predicate<FeatureCode>() {

            @Override
            public boolean evaluate(FeatureCode featureCode) {
                return featureCode.getId() == feature.getType();
            }
        });
        if (featureCode != null) {
            String featureName = getDocumentation(featureCode.getDocumentation(), lang);
            if (StringUtils.isNotEmpty(featureName)) {
                return featureName;
            }
        }
        return feature.getFeatureName();
    }

    private String getDocumentation(List<DocumentationType> descriptions, String lang) {

        if (descriptions != null) {
            for (DocumentationType description : descriptions) {
                if (lang.equals(description.getLanguage())) {
                    return description.getText();
                }
            }
        }
        return null;
    }

    @Override
    public void setValueAt(Object value, int rowIndex, int columnIndex) {
        Feature feature = features.get(rowIndex);
        switch (columnIndex) {
            case COLUMN_FEATURE_VALUE:
                setFeatureValue(feature, value);
                break;
            default:
                break;
        }
    }

    private void setFeatureValue(Feature feature, Object value) {
        if (value instanceof String) {
            try {
                feature.setValue(Integer.parseInt((String) value));
            }
            catch (Exception ex) {
                LOGGER.warn("Set the new feature value from string failed.", ex);
            }
        }
        else if (value instanceof Integer) {
            feature.setValue(((Integer) value).intValue());
        }
    }

    public Feature getRowAt(int rowIndex) {
        Feature feature = features.get(rowIndex);
        return feature;
    }

    public void clear() {
        features.clear();
        fireTableDataChanged();
    }

    public List<Feature> getFeatures() {
        return Collections.unmodifiableList(features);
    }
}
