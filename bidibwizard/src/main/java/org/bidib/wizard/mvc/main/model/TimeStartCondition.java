package org.bidib.wizard.mvc.main.model;

import java.util.Calendar;

public class TimeStartCondition extends StartCondition {
    private Calendar time;

    private MacroRepeatDay repeatDay = MacroRepeatDay.ALL;

    private MacroRepeatTime repeatTime = MacroRepeatTime.NONE;

    public TimeStartCondition() {
    }

    public TimeStartCondition(Calendar time) {
        this.time = time;
    }

    public void setHourly(boolean hourly) {
        if (hourly) {
            repeatTime = MacroRepeatTime.HOURLY;
        }
    }

    public void setMinutely(boolean minutely) {
        if (minutely) {
            repeatTime = MacroRepeatTime.MINUTELY;
        }
    }

    public MacroRepeatDay getRepeatDay() {
        return repeatDay;
    }

    public void setRepeatDay(MacroRepeatDay repeatDay) {
        this.repeatDay = repeatDay;
    }

    public MacroRepeatTime getRepeatTime() {
        return repeatTime;
    }

    public void setRepeatTime(MacroRepeatTime repeatTime) {
        this.repeatTime = repeatTime;
    }

    public Calendar getTime() {
        return time;
    }

    public void setTime(Calendar time) {
        this.time = time;
    }

    public String toString() {
        return getClass().getSimpleName() + "[time=" + ((time != null) ? time.getTime() : "null") + ",repeatTime="
            + repeatTime + ",repeatDay=" + repeatDay + "]";
    }
}
