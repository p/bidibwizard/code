package org.bidib.wizard.mvc.script.view.input;

public class CancelledByUserException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public CancelledByUserException(String message) {
        super(message);
    }
}
