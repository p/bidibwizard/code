package org.bidib.wizard.mvc.main.view.panel.renderer;

import org.bidib.jbidibc.core.enumeration.LightPortEnum;
import org.bidib.wizard.common.locale.Resources;

public enum LightPortStatusLabel {

    // @formatter:off
    ON(LightPortEnum.ON, "on"), OFF(LightPortEnum.OFF, "off"), UP(LightPortEnum.UP, "up"), DOWN(LightPortEnum.DOWN,
        "down"), NEON(LightPortEnum.NEON, "neon"), BLINKA(LightPortEnum.BLINKA, "blinka"), BLINKB(LightPortEnum.BLINKB,
        "blinkb"), FLASHA(LightPortEnum.FLASHA, "flasha"), FLASHB(LightPortEnum.FLASHB, "flashb"), DOUBLEFLASH(
        LightPortEnum.DOUBLEFLASH, "doubleflash"), UNKNOWN(LightPortEnum.UNKNOWN, "unknown"), TEST(LightPortEnum.TEST,
            "test");
    // @formatter:on

    private final LightPortEnum type;

    private final String key;

    private final String label;

    LightPortStatusLabel(LightPortEnum type, String key) {
        this.type = type;
        this.key = key;
        this.label = Resources.getString(LightPortStatusLabel.class, key);
    }

    public LightPortEnum getType() {
        return type;
    }

    public String getKey() {
        return key;
    }

    public String toString() {
        return label;
    }

    public static LightPortStatusLabel valueOf(LightPortEnum type) {
        LightPortStatusLabel result = null;

        for (LightPortStatusLabel e : values()) {
            if (e.type == type) {
                result = e;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("cannot map " + type + " to a light port status label");
        }
        return result;
    }
}
