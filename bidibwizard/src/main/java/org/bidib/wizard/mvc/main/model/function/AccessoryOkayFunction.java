package org.bidib.wizard.mvc.main.model.function;

import org.bidib.jbidibc.core.enumeration.AccessoryOkayEnum;
import org.bidib.jbidibc.exchange.lcmacro.AccessoryOkayActionType;
import org.bidib.jbidibc.exchange.lcmacro.AccessoryOkayPoint;
import org.bidib.jbidibc.exchange.lcmacro.LcMacroPointType;
import org.bidib.wizard.comm.AccessoryOkayStatus;
import org.bidib.wizard.mvc.main.model.InputPort;

public class AccessoryOkayFunction extends SystemFunction<AccessoryOkayStatus> {
    private InputPort input;

    public AccessoryOkayFunction() {
        this(AccessoryOkayStatus.QUERY0);
    }

    public AccessoryOkayFunction(AccessoryOkayStatus action) {
        this(action, null);
    }

    public AccessoryOkayFunction(AccessoryOkayStatus action, InputPort input) {
        super(action, Function.KEY_ACCESSORY_OKAY);
        this.input = input;
    }

    public InputPort getInput() {
        return input;
    }

    public void setInput(InputPort input) {
        this.input = input;
    }

    public String getDebugString() {
        int id = 0;

        if (getInput() != null) {
            id = getInput().getId();
        }
        return "Accessory Okay=" + getAction().name().substring(5) + "? Port=" + id;
    }

    @Override
    public LcMacroPointType toLcMacroPoint() {
        AccessoryOkayPoint accessoryOkayPoint =
            new AccessoryOkayPoint().withAccessoryOkayActionType(AccessoryOkayActionType.fromValue(getAction().name()));
        if (!AccessoryOkayEnum.NO_FEEDBACK.equals(getAction().getType()) && getInput() != null) {
            accessoryOkayPoint.setInputNumber(getInput().getId());
        }
        return accessoryOkayPoint;
    }
}
