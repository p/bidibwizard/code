package org.bidib.wizard.mvc.booster.view;

import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.booster.model.BoosterModel;
import org.bidib.wizard.mvc.booster.model.BoosterTableModel;
import org.bidib.wizard.mvc.common.view.DockKeys;
import org.bidib.wizard.mvc.main.view.table.AbstractEmptyTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.adapter.SingleListSelectionAdapter;
import com.jgoodies.binding.list.SelectionInList;
import com.vlsolutions.swing.docking.DockKey;
import com.vlsolutions.swing.docking.Dockable;

public class BoosterTableView implements Dockable {
    private static final Logger LOGGER = LoggerFactory.getLogger(BoosterTableView.class);

    private final JPanel contentPanel;

    private SelectionInList<BoosterModel> boosterSelection;

    public BoosterTableView(BoosterTableModel boosterTableModel) {

        DockKeys.DOCKKEY_BOOSTER_TABLE_VIEW.setName(Resources.getString(getClass(), "title"));
        DockKeys.DOCKKEY_BOOSTER_TABLE_VIEW.setFloatEnabled(true);
        DockKeys.DOCKKEY_BOOSTER_TABLE_VIEW.setAutoHideEnabled(false);

        LOGGER.info("Create new BoosterTableView");

        contentPanel = new JPanel();
        contentPanel.setLayout(new BorderLayout());

        boosterSelection =
            new SelectionInList<BoosterModel>((ListModel<BoosterModel>) boosterTableModel.getBoosterListModel());

        TableModel tableModel = new BoosterTableTableModel(boosterSelection);

        // create a booster table
        AbstractEmptyTable boosterTable =
            new AbstractEmptyTable(tableModel, Resources.getString(getClass(), "empty_table")) {
                @Override
                public boolean isSkipPackColumn() {
                    return true;
                }
            };

        boosterTable.setSelectionModel(new SingleListSelectionAdapter(boosterSelection.getSelectionIndexHolder()));

        TableColumnModel tcm = boosterTable.getColumnModel();

        TableColumn tc = tcm.getColumn(BoosterTableTableModel.COLUMN_UNIQUE_ID);
        tc.setMinWidth(120);
        tc.setMaxWidth(150);
        tc.setPreferredWidth(150);
        tc = boosterTable.getColumnModel().getColumn(BoosterTableTableModel.COLUMN_DESCRIPTION);
        tc.setPreferredWidth(300);
        tc = boosterTable.getColumnModel().getColumn(BoosterTableTableModel.COLUMN_STATUS);
        tc.setCellRenderer(new BoosterStateCellRenderer());
        tc.setMaxWidth(60);
        tc = boosterTable.getColumnModel().getColumn(BoosterTableTableModel.COLUMN_CS_STATUS);
        tc.setCellRenderer(new CommandStationStateCellRenderer());
        tc.setMaxWidth(60);
        tc = boosterTable.getColumnModel().getColumn(BoosterTableTableModel.COLUMN_TEMPERATURE);
        tc.setCellRenderer(new TemperatureCellRenderer());
        tc.setMaxWidth(60);
        tc = boosterTable.getColumnModel().getColumn(BoosterTableTableModel.COLUMN_VOLTAGE);
        tc.setCellRenderer(new VoltageCellRenderer());
        tc.setMaxWidth(60);
        tc = boosterTable.getColumnModel().getColumn(BoosterTableTableModel.COLUMN_MAX_CURRENT);
        tc.setCellRenderer(new CurrentCellRenderer());
        tc.setMaxWidth(70);
        tc = boosterTable.getColumnModel().getColumn(BoosterTableTableModel.COLUMN_CURRENT);
        tc.setCellRenderer(new CurrentCellRenderer());

        // Highlighter simpleStriping = HighlighterFactory.createSimpleStriping();
        // boosterTable.setHighlighters(simpleStriping);

        JScrollPane scrollPane = new JScrollPane(boosterTable);
        contentPanel.add(scrollPane, BorderLayout.CENTER);
    }

    @Override
    public DockKey getDockKey() {
        return DockKeys.DOCKKEY_BOOSTER_TABLE_VIEW;
    }

    @Override
    public Component getComponent() {
        return contentPanel;
    }
}
