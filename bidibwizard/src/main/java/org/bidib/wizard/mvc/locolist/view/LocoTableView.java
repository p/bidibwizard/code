package org.bidib.wizard.mvc.locolist.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import org.apache.commons.lang.StringUtils;
import org.bidib.jbidibc.core.StringData;
import org.bidib.jbidibc.core.enumeration.CsQueryTypeEnum;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.common.view.DockKeys;
import org.bidib.wizard.mvc.common.view.converter.StringConverter;
import org.bidib.wizard.mvc.common.view.table.CustomBooleanCellEditor;
import org.bidib.wizard.mvc.common.view.table.CustomBooleanCellRenderer;
import org.bidib.wizard.mvc.locolist.controller.listener.LocoTableControllerListener;
import org.bidib.wizard.mvc.locolist.model.LocoModel;
import org.bidib.wizard.mvc.locolist.model.LocoTableModel;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.view.table.AbstractEmptyTable;
import org.bidib.wizard.utils.InputValidationDocument;
import org.jfree.chart.plot.PlotOrientation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.adapter.ComboBoxAdapter;
import com.jgoodies.binding.adapter.SingleListSelectionAdapter;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.binding.value.ConverterValueModel;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.builder.ButtonBarBuilder;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.debug.FormDebugPanel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.FormLayout;
import com.jidesoft.grid.JideTable;
import com.jidesoft.swing.JideScrollPane;
import com.vlsolutions.swing.docking.DockKey;
import com.vlsolutions.swing.docking.Dockable;

public class LocoTableView implements Dockable {

    private static final Logger LOGGER = LoggerFactory.getLogger(LocoTableView.class);

    private final DockKey DOCKKEY = new DockKey(DockKeys.LOCO_TABLE_VIEW);

    private static final String ENCODED_DIALOG_COLUMN_SPECS =
        "pref, 3dlu, pref, 3dlu, pref, 3dlu, pref, 3dlu, pref, 3dlu, fill:150dlu:grow";

    private final JButton queryLocoListButton = new JButton(Resources.getString(getClass(), "queryLocoList"));

    private final JComponent contentPanel;

    private final LocoListBeanModel locoListBeanModel;

    private final JTextField locoAddress;

    private InputValidationDocument addressDocument;

    private ValueModel addressValueModel;

    private SelectionInList<LocoModel> locoSelection;

    private final LocoTableControllerListener locoTableController;

    public LocoTableView(final LocoTableControllerListener locoTableController, final LocoTableModel locoTableModel) {
        this.locoTableController = locoTableController;

        locoListBeanModel = new LocoListBeanModel();

        DOCKKEY.setName(Resources.getString(getClass(), "title"));
        DOCKKEY.setFloatEnabled(true);
        DOCKKEY.setAutoHideEnabled(false);

        LOGGER.info("Create new LocoTableView");

        locoSelection = new SelectionInList<LocoModel>((ListModel<LocoModel>) locoTableModel.getLocoListModel());

        TableModel tableModel = new LocoTableTableModel(locoSelection);

        // create a booster table
        AbstractEmptyTable locoTable =
            new AbstractEmptyTable(tableModel, Resources.getString(getClass(), "empty_table")) {
                private static final long serialVersionUID = 1L;

                @Override
                public boolean isSkipPackColumn() {
                    return false;
                }
            };

        locoTable.setSelectionModel(new SingleListSelectionAdapter(locoSelection.getSelectionIndexHolder()));

        locoTable.setAutoResizeMode(JideTable.AUTO_RESIZE_FILL);

        TableColumnModel tcm = locoTable.getColumnModel();
        tcm.getColumn(LocoTableTableModel.COLUMN_DIRECTION).setCellEditor(
            new CustomBooleanCellEditor("/icons/locolist/arrow_right.png", "/icons/locolist/arrow_left.png"));
        tcm.getColumn(LocoTableTableModel.COLUMN_DIRECTION).setCellRenderer(
            new CustomBooleanCellRenderer("/icons/locolist/arrow_right.png", "/icons/locolist/arrow_left.png"));

        // tcm.getColumn(LocoTableTableModel.COLUMN_SPEED).setCellRenderer(new ProgressBarCellRenderer());

        JSparklinesBarChartTableCellRenderer renderer =
            new JSparklinesBarChartTableCellRenderer(PlotOrientation.HORIZONTAL, -127.0, 127.0,
                new Color(51, 153, 255).darker(), Color.GREEN.darker());

        renderer.showAbsValue(true);
        renderer.showNumberAndChart(true, 40);
        tcm.getColumn(LocoTableTableModel.COLUMN_SPEED).setCellRenderer(renderer);

        TableColumn tc = tcm.getColumn(LocoTableTableModel.COLUMN_ADDRESS);
        tc.setMinWidth(50);
        tc.setMaxWidth(60);
        tc.setPreferredWidth(60);

        tc = tcm.getColumn(LocoTableTableModel.COLUMN_SPEED);
        tc.setMinWidth(100);
        // tc.setMaxWidth(250);
        tc.setPreferredWidth(250);

        tc = tcm.getColumn(LocoTableTableModel.COLUMN_DIRECTION);
        tc.setMinWidth(50);
        tc.setMaxWidth(60);
        tc.setPreferredWidth(60);

        tc = tcm.getColumn(LocoTableTableModel.COLUMN_SPEEDSTEPS);
        tc.setMinWidth(70);
        tc.setMaxWidth(70);
        tc.setPreferredWidth(70);

        AbstractMaskedIconRenderer locoLightRenderer = new LocoLightRenderer(0x10);
        tcm.getColumn(LocoTableTableModel.COLUMN_FUNCTIONS_LIGHT).setCellRenderer(locoLightRenderer);
        tc = tcm.getColumn(LocoTableTableModel.COLUMN_FUNCTIONS_LIGHT);
        tc.setMinWidth(40);
        tc.setMaxWidth(60);
        tc.setPreferredWidth(40);

        AbstractMaskedIconRenderer multiCheckRenderer = new MultiCheckWithLightRenderer(0x0F);
        tc = tcm.getColumn(LocoTableTableModel.COLUMN_FUNCTIONS_1_TO_4);
        tc.setCellRenderer(multiCheckRenderer);
        tc.setPreferredWidth(90);
        AbstractMaskedIconRenderer multiCheckRendererFull = new MultiCheckWithLightRenderer(0xFF);
        tc = tcm.getColumn(LocoTableTableModel.COLUMN_FUNCTIONS_5_TO_12);
        tc.setCellRenderer(multiCheckRendererFull);
        tc.setPreferredWidth(160);
        tc = tcm.getColumn(LocoTableTableModel.COLUMN_FUNCTIONS_13_TO_20);
        tc.setCellRenderer(multiCheckRendererFull);
        tc.setPreferredWidth(160);
        tc = tcm.getColumn(LocoTableTableModel.COLUMN_FUNCTIONS_21_TO_28);
        tc.setCellRenderer(multiCheckRendererFull);
        tc.setPreferredWidth(160);

        // TODO resize the columns

        // create form builder
        DefaultFormBuilder dialogBuilder = null;
        boolean debugDialog = false;
        if (debugDialog) {
            JPanel panel = new FormDebugPanel();
            dialogBuilder = new DefaultFormBuilder(new FormLayout(ENCODED_DIALOG_COLUMN_SPECS), panel);
        }
        else {
            JPanel panel = new JPanel(new BorderLayout());
            dialogBuilder = new DefaultFormBuilder(new FormLayout(ENCODED_DIALOG_COLUMN_SPECS), panel);
        }
        dialogBuilder.border(Borders.DIALOG);

        queryLocoListButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                fireQueryLocoList();
            }
        });

        final ValueModel queryLocoListButtonEnabled =
            new PropertyAdapter<LocoTableModel>(locoTableModel, LocoTableModel.PROPERTY_CS_NODE_SELECTED, true);
        PropertyConnector.connect(queryLocoListButtonEnabled, "value", queryLocoListButton, "enabled");

        SelectionInList<CsQueryTypeEnum> csQueryTypeSelection =
            new SelectionInList<CsQueryTypeEnum>(CsQueryTypeEnum.values());

        ValueModel csQueryTypeModel =
            new PropertyAdapter<LocoListBeanModel>(locoListBeanModel, LocoListBeanModel.PROPERTYNAME_CS_QUERY_TYPE,
                true);

        ComboBoxAdapter<CsQueryTypeEnum> comboBoxAdapterCsQueryType =
            new ComboBoxAdapter<CsQueryTypeEnum>(csQueryTypeSelection, csQueryTypeModel);

        final JComboBox<CsQueryTypeEnum> comboCsQueryType = new JComboBox<>();
        comboCsQueryType.setModel(comboBoxAdapterCsQueryType);
        comboCsQueryType.setRenderer(new CsQueryTypeCellRenderer());

        dialogBuilder.append(Resources.getString(getClass(), "csQueryType"), comboCsQueryType);

        // prepare the query loco list button
        JPanel locoListActionButtons = new ButtonBarBuilder().addButton(queryLocoListButton).build();
        dialogBuilder.append(locoListActionButtons);

        dialogBuilder.append(Resources.getString(getClass(), "address"));

        addressValueModel =
            new PropertyAdapter<LocoListBeanModel>(locoListBeanModel, LocoListBeanModel.PROPERTYNAME_ADDRESS, true);

        final ValueModel addressConverterModel =
            new ConverterValueModel(addressValueModel, new StringConverter(new DecimalFormat("#")));

        // create the textfield for the CV number
        locoAddress = new JTextField();
        addressDocument = new InputValidationDocument(5, InputValidationDocument.NUMERIC);
        locoAddress.setDocument(addressDocument);
        locoAddress.setColumns(5);

        // bind manually because we changed the document of the textfield
        Bindings.bind(locoAddress, addressConverterModel, false);
        dialogBuilder.append(locoAddress);

        // add the log area
        dialogBuilder.appendRow("3dlu");
        dialogBuilder.appendRow("fill:100dlu:grow");
        dialogBuilder.nextLine(2);

        JScrollPane scrollPaneLocoTable = new JScrollPane(locoTable);
        dialogBuilder.append(scrollPaneLocoTable, 11);

        JPanel contentPanelTemp = dialogBuilder.build();

        JideScrollPane scrollPane = new JideScrollPane(contentPanelTemp);
        contentPanel = scrollPane;

    }

    @Override
    public DockKey getDockKey() {
        return DOCKKEY;
    }

    @Override
    public Component getComponent() {
        return contentPanel;
    }

    public void setDockTabName(final Node node) {
        if (node != null) {
            DOCKKEY.setName(Resources.getString(getClass(), "title") + " - " + prepareNodeLabel(node));
        }
        else {
            DOCKKEY.setName(Resources.getString(getClass(), "title"));
        }
    }

    private String prepareNodeLabel(final Node node) {
        String nodeLabel = node.getLabel();
        if (StringUtils.isBlank(nodeLabel)) {
            // try to get the product name
            String productString = node.getNode().getStoredString(StringData.INDEX_PRODUCTNAME);
            if (StringUtils.isNotBlank(productString)) {
                nodeLabel = productString;
            }
        }
        return nodeLabel;
    }

    private void fireQueryLocoList() {
        if (locoTableController != null) {

            Integer address = locoListBeanModel.getAddress();
            CsQueryTypeEnum csQueryType = locoListBeanModel.getCsQueryType();

            LOGGER.info("Current selected csQueryType: {}, address: {}", csQueryType, address);
            locoTableController.queryCommandStationList(csQueryType, address);
        }
    }

    private class CsQueryTypeCellRenderer extends DefaultListCellRenderer {
        private static final long serialVersionUID = 1L;

        private Map<String, String> labelMap = new HashMap<>();

        public CsQueryTypeCellRenderer() {
            for (CsQueryTypeEnum addressTypeEnum : CsQueryTypeEnum.values()) {
                String label = Resources.getString(CsQueryTypeEnum.class, addressTypeEnum.getKey());
                labelMap.put(addressTypeEnum.getKey(), label);
            }
        }

        public Component getListCellRendererComponent(
            JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {

            JLabel renderer = (JLabel) super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);

            if (value instanceof CsQueryTypeEnum) {
                CsQueryTypeEnum addressTypeEnum = (CsQueryTypeEnum) value;
                renderer.setText(labelMap.get(addressTypeEnum.getKey()));
            }
            else {
                renderer.setText(null);
            }

            return renderer;
        }
    }

}
