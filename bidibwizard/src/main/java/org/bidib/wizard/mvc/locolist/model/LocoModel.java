package org.bidib.wizard.mvc.locolist.model;

import java.beans.PropertyChangeListener;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.bidib.wizard.comm.Direction;
import org.bidib.wizard.comm.SpeedSteps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.beans.Model;

public class LocoModel extends Model {
    private static final Logger LOGGER = LoggerFactory.getLogger(LocoModel.class);

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_LOCO_ADDRESS = "locoAddress";

    public static final String PROPERTY_SPEED = "speed";

    public static final String PROPERTY_SPEED_STEPS = "speedSteps";

    public static final String PROPERTY_DIRECTION = "direction";

    public static final String PROPERTY_FUNCTIONS = "functions";

    private final PropertyChangeListener locoChangeListener;

    private final int locoAddress;

    private int dynStateEnergy;

    // default direction is forward
    private Direction direction = Direction.FORWARD;

    private int speed;

    private SpeedSteps speedSteps = SpeedSteps.DCC128;

    private byte[] functions;

    public LocoModel(int locoAddress, final PropertyChangeListener locoChangeListener) {
        this.locoAddress = locoAddress;
        this.locoChangeListener = locoChangeListener;

        LOGGER.info("Create new LocoModel for locoAddress: {}", locoAddress);
    }

    public int getLocoAddress() {
        return locoAddress;
    }

    public int getDynStateEnergy() {
        return dynStateEnergy;
    }

    public void setDynStateEnergy(int dynStateEnergy) {
        this.dynStateEnergy = dynStateEnergy;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        Direction oldValue = this.direction;

        this.direction = direction;

        firePropertyChange(PROPERTY_DIRECTION, oldValue, this.direction);
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        int oldValue = this.speed;

        this.speed = speed;

        firePropertyChange(PROPERTY_SPEED, oldValue, this.speed);
    }

    public SpeedSteps getSpeedSteps() {
        return speedSteps;
    }

    public void setSpeedSteps(SpeedSteps speedSteps) {
        SpeedSteps oldValue = this.speedSteps;

        this.speedSteps = speedSteps;

        firePropertyChange(PROPERTY_SPEED_STEPS, oldValue, this.speedSteps);
    }

    public byte[] getFunctions() {
        return functions;
    }

    public byte getFunctions(int index) {
        return (functions != null ? functions[index] : 0);
    }

    public void setFunctions(byte[] functions) {

        LOGGER.info("Set the functions: {}", functions);
        byte[] oldValue = this.functions;
        this.functions = functions;
        firePropertyChange(PROPERTY_FUNCTIONS, oldValue, this.functions);
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof LocoModel) {
            return locoAddress == ((LocoModel) other).locoAddress;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return locoAddress;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
