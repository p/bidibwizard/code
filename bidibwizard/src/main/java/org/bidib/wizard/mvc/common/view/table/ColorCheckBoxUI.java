package org.bidib.wizard.mvc.common.view.table;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.io.Serializable;

import javax.swing.AbstractButton;
import javax.swing.ButtonModel;
import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicCheckBoxUI;

public class ColorCheckBoxUI extends BasicCheckBoxUI implements Serializable {

    private static final long serialVersionUID = 1L;

    private final static ColorCheckBoxUI buttonUI = new ColorCheckBoxUI();

    public ColorCheckBoxUI() {
    }

    public static ComponentUI createUI(JComponent c) {
        return buttonUI;
    }

    public void installUI(JComponent c) {
        super.installUI(c);
        c.setBackground(/* Color.gray */null);
    }

    public void uninstallUI(JComponent c) {
        super.uninstallUI(c);
    }

    public void paint(Graphics g, JComponent c) {
        AbstractButton b = (AbstractButton) c;
        ButtonModel model = b.getModel();
        Dimension d = b.getSize();

        g.setFont(c.getFont());
        FontMetrics fm = g.getFontMetrics();
        g.setColor(Color.white);
        // g.drawString("Am I a check box", 10, 10);

    }
}
