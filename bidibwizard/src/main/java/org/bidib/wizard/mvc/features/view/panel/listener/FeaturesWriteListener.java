package org.bidib.wizard.mvc.features.view.panel.listener;

import java.util.Collection;

import org.bidib.jbidibc.core.Feature;

public interface FeaturesWriteListener {

    /**
     * Write the features to the node.
     * 
     * @param features
     *            the features to write.
     */
    void write(Collection<Feature> features);
}
