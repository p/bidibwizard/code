package org.bidib.wizard.mvc.main.model;

import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.wizard.comm.BidibStatus;

/**
 * This class represents a null port.
 * 
 */
public class NullPort extends Port<BidibStatus> {

    private static final long serialVersionUID = 1L;

    public static final NullPort NONE = new NullPort(null);

    protected NullPort(GenericPort genericPort) {
        super(genericPort);
        setId(-1);
    }

    @Override
    protected LcOutputType getPortType() {
        return null;
    }

    @Override
    public byte[] getPortConfig() {
        return null;
    }

}
