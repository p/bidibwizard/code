package org.bidib.wizard.mvc.main.view.panel;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.apache.commons.lang.StringUtils;
import org.bidib.jbidibc.core.utils.ProductUtils;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.common.utils.ImageUtils;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.bidib.wizard.mvc.common.view.ComponentUtils;
import org.bidib.wizard.mvc.common.view.list.DefaultListCellEditor;
import org.bidib.wizard.mvc.common.view.list.DefaultMutableListModel;
import org.bidib.wizard.mvc.common.view.list.JListMutable;
import org.bidib.wizard.mvc.common.view.panel.DisabledPanel;
import org.bidib.wizard.mvc.main.controller.AccessoryPanelController;
import org.bidib.wizard.mvc.main.model.Accessory;
import org.bidib.wizard.mvc.main.model.AccessoryStartupAspectModel;
import org.bidib.wizard.mvc.main.model.AccessorySwitchTimeModel;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.view.component.DefaultTabSelectionPanel;
import org.bidib.wizard.mvc.main.view.component.LabelList;
import org.bidib.wizard.mvc.main.view.menu.AccessoryListMenu;
import org.bidib.wizard.mvc.main.view.menu.listener.AccessoryListMenuListener;
import org.bidib.wizard.mvc.main.view.panel.listener.AccessoryListListener;
import org.bidib.wizard.mvc.main.view.panel.listener.AccessoryTableListener;
import org.bidib.wizard.mvc.main.view.panel.listener.TabSelectionListener;
import org.bidib.wizard.mvc.stepcontrol.controller.StepControlControllerInterface;
import org.bidib.wizard.script.node.types.TargetType;
import org.bidib.wizard.utils.AccessoryListUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.forms.builder.ButtonBarBuilder;
import com.jgoodies.forms.factories.Borders;

public class AccessoryListPanel extends LabelListPanel<Accessory>
    implements AccessoryListMenuListener, ChangeLabelSupport, TabSelectionListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccessoryListPanel.class);

    private final Collection<AccessoryListListener> accessoryListListeners = new LinkedList<AccessoryListListener>();

    private final MainModel model;

    private final AccessoryPanel accessoryPanel;

    private final AccessoryListMenu accessoryListMenu = new AccessoryListMenu();

    private final DisabledPanel disabledBorderPanel;

    private final JPanel contentPanel;

    private JPanel buttonPanel;

    private final static class AccessoryListRenderer extends DefaultListCellRenderer {
        private static final long serialVersionUID = 1L;

        private ImageIcon pendingChangesIcon;

        private ImageIcon permanentlyStoredIcon;

        public AccessoryListRenderer() {
            pendingChangesIcon = ImageUtils.createImageIcon(NodeTree.class, "/icons/write.png");
            permanentlyStoredIcon = ImageUtils.createImageIcon(NodeTree.class, "/icons/16x16/lock.png");
        }

        @Override
        public Component getListCellRendererComponent(
            JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            // prepare the default renderer
            Component renderer = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            if (value instanceof Accessory) {
                Accessory accessory = (Accessory) value;

                switch (accessory.getAccessorySaveState()) {
                    case PENDING_CHANGES:
                        // set the pending changes icon
                        setIcon(pendingChangesIcon);
                        break;
                    case PERMANENTLY_STORED_ON_NODE:
                        setIcon(permanentlyStoredIcon);
                        break;
                    default:
                        LOGGER.error("Unknown accessory save state detected: {}", accessory.getAccessorySaveState());
                        break;
                }

                String text = null;
                if (StringUtils.isNotBlank(accessory.getLabel())) {
                    text = String.format("%1$02d : %2$s", accessory.getId(), accessory.getLabel());
                }
                else if (accessory.getId() > -1) {
                    text = String.format("%1$02d :", accessory.getId());
                }
                else {
                    text = " ";
                }

                setText(text);

            }
            return renderer;
        }
    }

    public AccessoryListPanel(final AccessoryPanelController accessoryPanelController, final MainModel model,
        final AccessoryStartupAspectModel accessoryStartupAspectModel,
        final AccessorySwitchTimeModel accessorySwitchTimeModel) {

        super(new LabelList<Accessory>(new DefaultMutableListModel() {
            private static final long serialVersionUID = 1L;

            @Override
            public void setValueAt(Object value, int index) {
                LOGGER.info("Set the new value: {}, index: {}", value, index);

                Node node = model.getSelectedNode();

                if (ProductUtils.isStepControl(node.getUniqueId())) {
                    // step control needs special handling
                    if (index < 3) {
                        LOGGER.info("The first 3 accessories are not changeable.");
                        return;
                    }
                }

                if (value instanceof String) {
                    // the name of the accessory was changed
                    Accessory accessory = (Accessory) super.get(index);
                    String label = (String) value;

                    accessoryPanelController.saveAccessoryLabel(accessory, label);
                    return;
                }

                super.setValueAt(value, index);
            }
        }) {
            private static final long serialVersionUID = 1L;

            @Override
            public boolean isCellEditable(int index) {
                Accessory selectedAccessory = getSelectedItem();
                LOGGER.info("isCellEditable, selectedAccessory: {}", selectedAccessory);
                return selectedAccessory.isEditable();
            }
        });

        this.model = model;

        contentPanel = new DefaultTabSelectionPanel() {
            private static final long serialVersionUID = 1L;

            @Override
            public void tabSelected(boolean selected) {
                LOGGER.debug("Select tab, current component is the AccessoryListPanel.");

                AccessoryListPanel.this.tabSelected(selected);
            }

            @Override
            public String getName() {
                // this is used as tab title
                return Resources.getString(AccessoryListPanel.class, "name");
            }
        };

        contentPanel.setLayout(new BorderLayout());

        getLabelList().setItems(model.getAccessories().toArray(new Accessory[0]));
        getLabelList().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        getLabelList().setCellRenderer(new AccessoryListRenderer());

        ((JListMutable) getLabelList()).setListCellEditor(new DefaultListCellEditor(new JTextField()));

        accessoryListMenu.addMenuListener(this);

        accessoryPanel =
            new AccessoryPanel(accessoryPanelController, model, accessoryStartupAspectModel, accessorySwitchTimeModel);

        JPanel panel = new JPanel(new BorderLayout());

        disabledBorderPanel = new DisabledPanel(accessoryPanel);
        panel.add(disabledBorderPanel);

        JSplitPane splitPane =
            new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, new JScrollPane((Component) getLabelList()), panel);

        splitPane.setContinuousLayout(true);
        splitPane.setOneTouchExpandable(true);
        splitPane.setResizeWeight(0.2);

        contentPanel.add(splitPane);
        setEnabled(false);

        addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    final Accessory accessory = (Accessory) ((JList<?>) e.getSource()).getSelectedValue();

                    setEnabled(accessory != null);

                    if (buttonPanel != null) {
                        List<JButton> buttons = ComponentUtils.harvestComponents(buttonPanel, JButton.class);
                        for (JButton button : buttons) {
                            button.setEnabled(accessory != null /* && accessory.isMacroMapped() */);
                        }
                    }
                }
            }
        });
        model.addAccessoryListListener(new org.bidib.wizard.mvc.main.model.listener.AccessoryListListener() {
            @Override
            public void listChanged() {
                // set the new accessories
                getLabelList().setItems(model.getAccessories().toArray(new Accessory[0]));
                setEnabled(false);
            }

            @Override
            public void accessoryChanged(int accessoryId) {
                // the selected accessory has changed
                Accessory accessory = model.getSelectedAccessory();

                setEnabled(accessory != null);

                if (buttonPanel != null) {
                    List<JButton> buttons = ComponentUtils.harvestComponents(buttonPanel, JButton.class);
                    for (JButton button : buttons) {
                        button.setEnabled(accessory != null /* && accessory.isMacroMapped() */);
                    }
                }
            }

            @Override
            public void pendingChangesChanged() {
                getLabelList().refreshView();
            }
        });
        getLabelList().addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                handleMouseEvent(e, accessoryListMenu);
            }

            public void mouseReleased(MouseEvent e) {
                handleMouseEvent(e, accessoryListMenu);
            }
        });
        addButtonPanel(panel);
    }

    public JPanel getComponent() {
        return contentPanel;
    }

    public void addAccessoryListListener(AccessoryListListener l) {
        addLabelChangedListener(l);
        accessoryListListeners.add(l);
    }

    public void addAccessoryTableListener(AccessoryTableListener l) {
        accessoryPanel.addTableListener(l);
    }

    private void addButtonPanel(JPanel parent) {

        JButton loadButton = new JButton(Resources.getString(getClass(), "reload"));

        loadButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireReloadAccessory();
            }
        });

        JButton saveButton = new JButton(Resources.getString(getClass(), "save"));

        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireSaveAccessory();
            }
        });

        buttonPanel = new ButtonBarBuilder().addGlue().addButton(loadButton, saveButton).build();
        buttonPanel.setBorder(Borders.TABBED_DIALOG);

        parent.add(buttonPanel, BorderLayout.SOUTH);
    }

    @Override
    public void exportAccessory() {
        for (AccessoryListListener l : accessoryListListeners) {
            l.exportAccessory(model.getSelectedAccessory());
        }
    }

    private void fireReloadAccessory() {
        for (AccessoryListListener l : accessoryListListeners) {
            l.reloadAccessory(model.getSelectedAccessory());
        }
    }

    private void fireSaveAccessory() {
        final Accessory selectedAccessory = model.getSelectedAccessory();
        // update the values of the selected accessory with the values in the panel
        accessoryPanel.saveAccessory(selectedAccessory);

        // write the accessory to the node
        for (AccessoryListListener l : accessoryListListeners) {
            l.saveAccessory(selectedAccessory);
        }
    }

    @Override
    public void importAccessory() {
        for (AccessoryListListener l : accessoryListListeners) {
            l.importAccessory(model.getSelectedAccessory());
        }
    }

    @Override
    public void initializeAccessory() {
        // TODO Auto-generated method stub
        for (AccessoryListListener l : accessoryListListeners) {
            l.initializeAccessory(model.getSelectedAccessory());
        }

    }

    public void setEnabled(boolean enabled) {
        disabledBorderPanel.setEnabled(enabled);
    }

    @Override
    public void changeLabel(TargetType portType) {
        int portNum = portType.getPortNum();
        String label = portType.getLabel();

        Accessory accessory = AccessoryListUtils.findAccessoryByAccessoryNumber(model.getAccessories(), portNum);
        if (accessory != null) {
            LOGGER.info("Current accessory: {}, new label: {}", accessory, label);
            accessory.setLabel(label);
            if (accessory.equals(model.getSelectedAccessory())) {
                // setLabel(label);
            }
            else {
                fireLabelChanged(accessory, label);
            }
        }
    }

    protected void handleMouseEvent(MouseEvent e, JPopupMenu popupMenu) {
        if (e.isPopupTrigger() && getLabelList().getItemSize() > 0) {
            Accessory accessory = getLabelList().selectElement(e.getPoint());

            if (accessory != null && ProductUtils.isStepControl(model.getSelectedNode().getUniqueId())) {

                if (accessory.getId() < 3) {
                    // the step control has 3 fixed items
                    popupMenu.setEnabled(false);
                }
                else {
                    popupMenu.setEnabled(true);
                }
            }
            else {
                popupMenu.setEnabled(true);
            }
            popupMenu.show(e.getComponent(), e.getX(), e.getY());
        }
    }

    @Override
    public void tabSelected(boolean selected) {
        LOGGER.info("Tab is selected: {}", selected);

        if (selected && model.getSelectedNode() != null) {
            long uniqueId = model.getSelectedNode().getUniqueId();
            boolean isStepControl = ProductUtils.isStepControl(uniqueId);

            if (isStepControl) {
                LOGGER.info("The current node is a StepControl. Trigger load of CV values.");
                try {
                    StepControlControllerInterface controller =
                        DefaultApplicationContext.getInstance().get(
                            DefaultApplicationContext.KEY_STEPCONTROL_CONTROLLER, StepControlControllerInterface.class);

                    controller.triggerLoadCvValues();
                }
                catch (Exception ex) {
                    LOGGER.warn("Trigger load CV values of StepControl failed.", ex);
                }
            }
        }
    }
}
