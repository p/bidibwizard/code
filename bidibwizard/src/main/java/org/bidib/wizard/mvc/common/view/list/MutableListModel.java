package org.bidib.wizard.mvc.common.view.list;

import javax.swing.ListModel;

// @author Santhosh Kumar T - santhosh@in.fiorano.com
public interface MutableListModel<E> extends ListModel<E> {
    public boolean isCellEditable(int index);

    public void setValueAt(E value, int index);
}
