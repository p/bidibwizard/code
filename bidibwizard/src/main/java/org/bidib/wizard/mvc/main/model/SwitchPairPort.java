package org.bidib.wizard.mvc.main.model;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.util.LinkedHashMap;
import java.util.Map;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.LoadTypeEnum;
import org.bidib.jbidibc.core.enumeration.SwitchPortEnum;
import org.bidib.jbidibc.core.port.BytePortConfigValue;
import org.bidib.jbidibc.core.port.PortConfigValue;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.wizard.comm.SwitchPortStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SwitchPairPort extends Port<SwitchPortStatus> implements ConfigurablePort<SwitchPortStatus>, TicksAware {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(SwitchPairPort.class);

    static {
        try {
            // Q: why is this needed? A: export of beans with XMLDecoder
            PropertyDescriptor[] descriptor = Introspector.getBeanInfo(SwitchPairPort.class).getPropertyDescriptors();

            for (int i = 0; i < descriptor.length; i++) {
                PropertyDescriptor propertyDescriptor = descriptor[i];
                if (propertyDescriptor.getName().equals("value")) {
                    propertyDescriptor.setValue("transient", Boolean.TRUE);
                }
                else if (propertyDescriptor.getName().equals("portConfig")) {
                    propertyDescriptor.setValue("transient", Boolean.TRUE);
                }
                else if (propertyDescriptor.getName().equals("portConfigX")) {
                    propertyDescriptor.setValue("transient", Boolean.TRUE);
                }
                else if (propertyDescriptor.getName().equals("hasSwitchPortConfig")) {
                    propertyDescriptor.setValue("transient", Boolean.TRUE);
                }
            }
        }
        catch (IntrospectionException e) {
            throw new RuntimeException(e);
        }
    }

    private int switchOffTime;

    private LoadTypeEnum loadType = LoadTypeEnum.UNKNOWN;

    private transient boolean hasSwitchPortConfig;

    public static final SwitchPairPort NONE = new SwitchPairPort.Builder(-1).setLabel("<none>").build();

    public SwitchPairPort() {
        super(null);
        setStatus(SwitchPortStatus.ON);
    }

    public SwitchPairPort(GenericPort genericPort) {
        super(genericPort);
    }

    private SwitchPairPort(Builder builder) {
        super(null);
        setId(builder.id);
        setLabel(builder.label);
    }

    @Override
    protected LcOutputType getPortType() {
        return LcOutputType.SWITCHPAIRPORT;
    }

    @Override
    protected SwitchPortStatus internalGetStatus() {
        return SwitchPortStatus.valueOf(SwitchPortEnum.valueOf(genericPort.getPortStatus()));
    }

    @Override
    public int getTicks() {
        return getSwitchOffTime();
    }

    /**
     * @return the switchOffTime
     */
    public int getSwitchOffTime() {
        if (genericPort != null) {
            Number switchOffTime = genericPort.getPortConfigValue(BidibLibrary.BIDIB_PCFG_TICKS);
            if (switchOffTime != null) {
                this.switchOffTime = ByteUtils.getInt(switchOffTime.byteValue());
            }
            else {
                LOGGER.warn("No value received from generic for BIDIB_PCFG_TICKS!");
            }
        }
        return switchOffTime;
    }

    /**
     * @param switchOffTime
     *            the switchOffTime to set
     */
    public void setSwitchOffTime(int switchOffTime) {
        LOGGER.info("Set switchOff time: {}", switchOffTime);
        this.switchOffTime = switchOffTime;
        if (genericPort != null) {
            genericPort.setPortConfigValue(BidibLibrary.BIDIB_PCFG_TICKS,
                new BytePortConfigValue(ByteUtils.getLowByte(switchOffTime)));
        }
    }

    public void setLoadType(LoadTypeEnum loadType) {
        LOGGER.info("Set load type: {}", loadType);
        this.loadType = loadType;
        if (genericPort != null) {
            genericPort.setPortConfigValue(BidibLibrary.BIDIB_PCFG_LOAD_TYPE,
                new BytePortConfigValue(loadType.getType()));
        }
    }

    public LoadTypeEnum getLoadType() {
        if (genericPort != null) {
            if (isEnabled()) {
                Number loadType = genericPort.getPortConfigValue(BidibLibrary.BIDIB_PCFG_LOAD_TYPE);
                if (loadType != null) {
                    try {
                        this.loadType = LoadTypeEnum.valueOf(loadType.byteValue());
                    }
                    catch (IllegalArgumentException ex) {
                        LOGGER.warn("Get loadType failed on port: {}, loadType: {}", genericPort.getPortNumber(),
                            loadType);
                        this.loadType = LoadTypeEnum.UNKNOWN;
                    }
                }
                else {
                    LOGGER.warn("No value received from generic port for BIDIB_PCFG_LOAD_TYPE!");
                    this.loadType = LoadTypeEnum.UNKNOWN;
                }
            }
            else {
                LOGGER.debug("The current port is not enabled, return UNKNOWN.");
                this.loadType = LoadTypeEnum.UNKNOWN;
            }
        }
        return loadType;
    }

    @Override
    public byte[] getPortConfig() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setPortConfigX(Map<Byte, PortConfigValue<?>> portConfig) {

        LOGGER.info("Set the port config for the switchPair port.");

        Number switchOffTime = getPortConfigValue(BidibLibrary.BIDIB_PCFG_TICKS, portConfig);
        if (switchOffTime != null) {
            setSwitchOffTime(ByteUtils.getInt(switchOffTime.byteValue()));
            setHasSwitchPortConfig(true);
        }
        else {
            LOGGER.warn("No value received for BIDIB_PCFG_TICKS!");
        }

        Number loadType = getPortConfigValue(BidibLibrary.BIDIB_PCFG_LOAD_TYPE, portConfig);
        if (loadType != null) {
            setLoadType(LoadTypeEnum.valueOf(loadType.byteValue()));
            setHasSwitchPortConfig(true);
        }
        else {
            LOGGER.warn("No value received for BIDIB_PCFG_LOAD_TYPE!");
        }

        // call the super class
        super.setPortConfigX(portConfig);
    }

    @Override
    public Map<Byte, PortConfigValue<?>> getPortConfigX() {

        if (genericPort != null) {
            return genericPort.getPortConfig();
        }

        Map<Byte, PortConfigValue<?>> portConfigX = new LinkedHashMap<Byte, PortConfigValue<?>>();

        // add the ticks
        if (isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_TICKS)) {
            portConfigX.put(BidibLibrary.BIDIB_PCFG_TICKS,
                new BytePortConfigValue(ByteUtils.getLowByte(switchOffTime)));
        }
        // add the load type
        if (isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_LOAD_TYPE)) {
            portConfigX.put(BidibLibrary.BIDIB_PCFG_LOAD_TYPE,
                new BytePortConfigValue(ByteUtils.getLowByte(loadType.getType())));
        }
        return portConfigX;
    }

    /**
     * @return the port has switch port config available
     */
    public boolean isHasSwitchPortConfig() {
        if (genericPort != null) {
            hasSwitchPortConfig =
                genericPort.isHasPortConfig(BidibLibrary.BIDIB_PCFG_TICKS)
                    || genericPort.isHasPortConfig(BidibLibrary.BIDIB_PCFG_LOAD_TYPE);
            LOGGER.info("The generic port has switch port config: {}", hasSwitchPortConfig);
        }
        return hasSwitchPortConfig;
    }

    /**
     * @param hasSwitchPortConfig
     *            the switch port config available flag
     */
    public void setHasSwitchPortConfig(boolean hasSwitchPortConfig) {
        LOGGER.info("The port has switch port config available: {}, port: {}", hasSwitchPortConfig, this);
        this.hasSwitchPortConfig = hasSwitchPortConfig;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof SwitchPairPort) {
            return ((SwitchPairPort) obj).getId() == getId();
        }
        return false;
    }

    @Override
    public int hashCode() {
        return getId();
    }

    public static class Builder {
        private final int id;

        private String label;

        public Builder(int id) {
            this.id = id;
        }

        public Builder setLabel(String label) {
            this.label = label;
            return this;
        }

        public SwitchPairPort build() {
            return new SwitchPairPort(this);
        }
    }
}
