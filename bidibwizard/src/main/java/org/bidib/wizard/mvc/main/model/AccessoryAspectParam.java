package org.bidib.wizard.mvc.main.model;

public class AccessoryAspectParam implements AccessoryAspect {

    private final byte param;

    private final String description;

    public AccessoryAspectParam(byte param, String description) {
        this.param = param;
        this.description = description;
    }

    public byte getParam() {
        return param;
    }

    @Override
    public String toString() {
        return description;
    }
}
