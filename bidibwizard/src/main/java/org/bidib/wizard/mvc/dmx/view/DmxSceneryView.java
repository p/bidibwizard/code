package org.bidib.wizard.mvc.dmx.view;

import java.awt.Component;

import javax.swing.JComponent;

import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.dmx.controller.listener.DmxModelerControllerListener;
import org.bidib.wizard.mvc.dmx.model.DmxSceneryModel;
import org.bidib.wizard.mvc.dmx.view.scenery.SceneryPanel;
import org.bidib.wizard.mvc.main.model.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vlsolutions.swing.docking.DockKey;
import com.vlsolutions.swing.docking.Dockable;
import com.vlsolutions.swing.docking.DockableState;
import com.vlsolutions.swing.docking.DockingDesktop;
import com.vlsolutions.swing.docking.event.DockableStateChangeEvent;
import com.vlsolutions.swing.docking.event.DockableStateChangeListener;

/**
 * The <code>DmxSceneryView</code> is the view that shows the list of the <code>DmxScenery</code> instances that are
 * currently loaded.
 */
public class DmxSceneryView implements Dockable {
    private static final Logger LOGGER = LoggerFactory.getLogger(DmxSceneryView.class);

    private final DockKey DOCKKEY = new DockKey("DmxSceneryView");

    private JComponent sceneryPanel;

    private final DmxSceneryModel dmxSceneryModel;

    private final Node node;

    private DmxModelerControllerListener dmxModelerControllerListener;

    private final DockableStateChangeListener dockableStateChangeListener;

    public DmxSceneryView(final DockingDesktop desktop, final Node node, final DmxSceneryModel dmxSceneryModel) {
        this.dmxSceneryModel = dmxSceneryModel;
        this.node = node;

        DOCKKEY.setName(Resources.getString(getClass(), "title"));
        DOCKKEY.setKey("DmxSceneryView");
        // turn off autohide and close features
        DOCKKEY.setCloseEnabled(true);
        DOCKKEY.setAutoHideEnabled(false);
        DOCKKEY.setFloatEnabled(true);

        dockableStateChangeListener = new DockableStateChangeListener() {

            @Override
            public void dockableStateChanged(DockableStateChangeEvent event) {
                LOGGER.info("The state has changed, newState: {}, prevState: {}", event.getNewState(),
                    event.getPreviousState());

                DockableState newState = event.getNewState();
                if (newState.getDockable().equals(DmxSceneryView.this) && newState.isClosed()) {
                    LOGGER.info("The DmxSceneryView is closed.");
                    // we are closed
                    desktop.removeDockableStateChangeListener(dockableStateChangeListener);

                    if (dmxModelerControllerListener != null) {
                        dmxModelerControllerListener.closeView(dmxSceneryModel);
                    }
                }

            }
        };
        desktop.addDockableStateChangeListener(dockableStateChangeListener);
    }

    public void createPanel() {
        LOGGER.info("Create the scenery panel.");
        SceneryPanel comp = new SceneryPanel(node, dmxSceneryModel);
        comp.setDmxModelerControllerListener(dmxModelerControllerListener);
        sceneryPanel = comp.createPanel();
    }

    /**
     * @param dmxModelerControllerListener
     *            the dmxModelerControllerListener to set
     */
    public void setDmxModelerControllerListener(DmxModelerControllerListener dmxModelerControllerListener) {
        this.dmxModelerControllerListener = dmxModelerControllerListener;
    }

    @Override
    public DockKey getDockKey() {
        return DOCKKEY;
    }

    @Override
    public Component getComponent() {
        return sceneryPanel;
    }
}
