package org.bidib.wizard.mvc.main.view.panel;

import javax.swing.event.ListSelectionListener;

import org.bidib.wizard.comm.AnalogPortStatus;
import org.bidib.wizard.comm.ServoPortStatus;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.listener.BacklightPortListener;
import org.bidib.wizard.mvc.main.model.listener.CvDefinitionRequestListener;
import org.bidib.wizard.mvc.main.model.listener.OutputListener;
import org.bidib.wizard.mvc.main.model.listener.ServoPortListener;
import org.bidib.wizard.mvc.main.view.panel.listener.AccessoryListListener;
import org.bidib.wizard.mvc.main.view.panel.listener.AccessoryTableListener;
import org.bidib.wizard.mvc.main.view.panel.listener.MacroListListener;
import org.bidib.wizard.mvc.main.view.panel.listener.MacroTableListener;
import org.bidib.wizard.mvc.main.view.panel.listener.NodeListActionListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vlsolutions.swing.docking.DockingDesktop;

public class MainPanel {
    private static final Logger LOGGER = LoggerFactory.getLogger(MainPanel.class);

    private final NodePanel nodePanel;

    public MainPanel(MainModel model) {
        LOGGER.info("Create new NodePanel.");
        nodePanel = new NodePanel(model);
    }

    public void create(DockingDesktop desk) {
        LOGGER.info("Create the content of the nodePanel.");
        nodePanel.create(desk);
    }

    public void addAccessoryListListener(AccessoryListListener l) {
        nodePanel.addAccessoryListListener(l);
    }

    public void addAccessoryListSelectionListener(ListSelectionListener l) {
        nodePanel.addAccessoryListSelectionListener(l);
    }

    public void addAccessoryTableListener(AccessoryTableListener l) {
        nodePanel.addAccessoryTableListener(l);
    }

    public void addAnalogPortListener(OutputListener<AnalogPortStatus> l) {
        nodePanel.addAnalogPortListener(l);
    }

    public void addBacklightPortListener(BacklightPortListener l) {
        nodePanel.addBacklightPortListener(l);
    }

    public void addMacroListListener(MacroListListener l) {
        nodePanel.addMacroListListener(l);
    }

    public void addMacroListSelectionListener(ListSelectionListener l) {
        nodePanel.addMacroListSelectionListener(l);
    }

    public void addMacroTableListener(MacroTableListener l) {
        nodePanel.addMacroTableListener(l);
    }

    public void addNodeListListener(NodeListActionListener l) {
        nodePanel.addNodeListListener(l);
    }

    public void addNodeListSelectionListener(ListSelectionListener l) {
        nodePanel.addNodeListSelectionListener(l);
    }

    public void addServoPortListener(ServoPortListener<ServoPortStatus> l) {
        nodePanel.addServoPortListener(l);
    }

    public void addCvDefinitionRequestListener(CvDefinitionRequestListener l) {
        nodePanel.addCvDefinitionRequestListener(l);
    }
}
