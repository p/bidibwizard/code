package org.bidib.wizard.mvc.main.model;

import java.util.Arrays;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.bidib.wizard.mvc.main.model.function.Function;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MacroUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(MacroUtils.class);

    public static final byte[] MACRO_START_OFF = new byte[] { (byte) 0x3F, (byte) 0xBF, (byte) 0x7F, (byte) 0xFF };

    /**
     * Apply the start clk data from the device to the macro
     * 
     * @param startClk
     *            the start clk data
     * @param macro
     *            the macro instance
     */
    public static void applyMacroStartClk(byte[] startClk, Macro macro) {

        if (startClk != null && !Arrays.equals(MACRO_START_OFF, startClk)) {
            int hour = startClk[1] & 0x1F;
            int minute = startClk[0] & 0x3F;
            int day = startClk[2] & 0x7;
            Calendar time = Calendar.getInstance();
            TimeStartCondition startCondition = new TimeStartCondition();

            if (hour == 25) {
                startCondition.setRepeatTime(MacroRepeatTime.WORKING_HOURLY);
                time.set(Calendar.HOUR_OF_DAY, 0);
            }
            else if (hour == 24) {
                startCondition.setRepeatTime(MacroRepeatTime.HOURLY);
                time.set(Calendar.HOUR_OF_DAY, 0);
            }
            else {
                time.set(Calendar.HOUR_OF_DAY, hour);
            }
            if (minute == 60) {
                startCondition.setRepeatTime(MacroRepeatTime.MINUTELY);
                time.set(Calendar.MINUTE, 0);
            }
            else if (minute == 61) {
                startCondition.setRepeatTime(MacroRepeatTime.HALF_HOURLY);
                time.set(Calendar.MINUTE, 0);
            }
            else if (minute == 62) {
                startCondition.setRepeatTime(MacroRepeatTime.QUARTER_HOURLY);
                time.set(Calendar.MINUTE, 0);
            }
            else {
                time.set(Calendar.MINUTE, minute);
            }
            // day handling
            if (day == 7) {
                // every day
                startCondition.setRepeatDay(MacroRepeatDay.ALL);
            }
            else {
                int d = (day + 2) % 7;
                if (d == 0) {
                    d = 7;
                }
                startCondition.setRepeatDay(MacroRepeatDay.values()[d]);
            }
            time.set(Calendar.SECOND, 0);
            startCondition.setTime(time);
            macro.addStartCondition(startCondition);
        }
        else {
            LOGGER.info("No startClk available or macro has no start time.");
        }
    }

    public static void applyMacroStartClk(int day, int hour, int minute, Macro macro) {

        Calendar time = Calendar.getInstance();
        TimeStartCondition startCondition = new TimeStartCondition();

        if (hour == 25) {
            startCondition.setRepeatTime(MacroRepeatTime.WORKING_HOURLY);
            time.set(Calendar.HOUR_OF_DAY, 0);
        }
        else if (hour == 24) {
            startCondition.setRepeatTime(MacroRepeatTime.HOURLY);
            time.set(Calendar.HOUR_OF_DAY, 0);
        }
        else {
            time.set(Calendar.HOUR_OF_DAY, hour);
        }
        if (minute == 60) {
            startCondition.setRepeatTime(MacroRepeatTime.MINUTELY);
            time.set(Calendar.MINUTE, 0);
        }
        else if (minute == 61) {
            startCondition.setRepeatTime(MacroRepeatTime.HALF_HOURLY);
            time.set(Calendar.MINUTE, 0);
        }
        else if (minute == 62) {
            startCondition.setRepeatTime(MacroRepeatTime.QUARTER_HOURLY);
            time.set(Calendar.MINUTE, 0);
        }
        else {
            time.set(Calendar.MINUTE, minute);
        }
        // day handling
        if (day == 7) {
            // every day
            startCondition.setRepeatDay(MacroRepeatDay.ALL);
        }
        else {
            int d = (day + 2) % 7;
            if (d == 0) {
                d = 7;
            }
            startCondition.setRepeatDay(MacroRepeatDay.values()[d]);
        }
        time.set(Calendar.SECOND, 0);
        startCondition.setTime(time);
        macro.addStartCondition(startCondition);
    }

    /**
     * Check if the macro has empty steps.
     * 
     * @param macro
     *            the macro
     * @return {@code true}: empty steps were detected, {@code false}: no empty steps were detected
     */
    public static boolean hasEmptySteps(Macro macro) {

        if (CollectionUtils.isEmpty(macro.getFunctions())) {
            return true;
        }

        List<Function<?>> tempFunctions = new LinkedList<>();
        tempFunctions.addAll(macro.getFunctions());

        boolean filtered = CollectionUtils.filter(tempFunctions, new Predicate<Function<?>>() {

            @Override
            public boolean evaluate(Function<?> function) {
                return (function != null);
            }
        });

        LOGGER.info("Macro has empty steps: {}", filtered);

        return filtered;
    }

    /**
     * Remove the empty steps in the macro.
     * 
     * @param macro
     *            the macro
     * @return {@code true}: empty steps were detected and removed, {@code false}: no empty steps were detected
     */
    public static boolean removeEmptySteps(Macro macro) {

        if (CollectionUtils.isEmpty(macro.getFunctions())) {
            return true;
        }

        List<Function<?>> tempFunctions = new LinkedList<>();
        tempFunctions.addAll(macro.getFunctions());

        boolean filtered = CollectionUtils.filter(tempFunctions, new Predicate<Function<?>>() {

            @Override
            public boolean evaluate(Function<?> function) {
                return (function != null);
            }
        });

        if (filtered) {
            LOGGER.info("Set the filtered functions.");
            macro.setFunctions(tempFunctions);
        }

        return filtered;
    }
}
