package org.bidib.wizard.mvc.main.view.menu.listener;

import org.bidib.wizard.mvc.main.model.Node;

public interface NodeListMenuListener extends LabelListMenuListener {

    void addressMessagesEnabled(boolean isSelected);

    void dccStartEnabled(boolean isSelected);

    void exportNode();

    void externalStartEnabled(boolean isSelected);

    void feedbackMessagesEnabled(boolean isSelected);

    /**
     * Set the flag to disable send feedback mirror messages.
     * 
     * @param disable
     *            disable send mirror messages flag
     */
    void feedbackMirrorDisabled(boolean disable);

    void firmwareUpdate();

    void identify(boolean isSelected);

    void importNode();

    void keyMessagesEnabled(boolean isSelected);

    /**
     * Open the features dialog.
     */
    void features();

    /**
     * Open the reset node dialog.
     */
    void reset();

    /**
     * Send a ping to the node.
     */
    void ping();

    /**
     * Read the uniqueId from the node.
     */
    void readUniqueId();

    /**
     * Open the DMX modeler.
     */
    void dmxModeler();

    /**
     * Open the loco dialog.
     */
    void loco();

    /**
     * Open the locoList table.
     */
    void locoList();

    /**
     * Open the DCC accessory dialog.
     */
    void dccAccessory();

    /**
     * Open the loco CV dialog.
     */
    void locoCv();

    /**
     * Open the loco CV PT dialog.
     */
    void locoCvPt();

    /**
     * Save the configuration data in the node.
     */
    void saveNode();

    /**
     * Show the details of the selected node.
     */
    void showDetails();

    /**
     * Bulk processing on switch node.
     */
    void bulkSwitchNode();

    /**
     * Clear the error marker on the node.
     */
    void clearErrors();

    /**
     * @return the selected item from the node list
     */
    Node getSelectedItem();

    /**
     * Generate documentation of node.
     */
    void generateDocumentation();
}
