package org.bidib.wizard.mvc.main.view.table;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.wizard.mvc.main.model.ServoPort;
import org.bidib.wizard.mvc.main.model.ServoPortTableModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServoPortTableCellRenderer extends DefaultTableCellRenderer {
    private static final Logger LOGGER = LoggerFactory.getLogger(ServoPortTableCellRenderer.class);

    private static final long serialVersionUID = 1L;

    private int forColumn;

    public ServoPortTableCellRenderer(int forColumn) {
        this.forColumn = forColumn;
    }

    public Component getTableCellRendererComponent(
        JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

        // call super to set the correct color if selected
        super.getTableCellRendererComponent(table, null, isSelected, hasFocus, row, column);

        // renderer only handles ServoPorts
        if (value instanceof ServoPort) {
            ServoPort servoPort = (ServoPort) value;

            switch (forColumn) {
                case ServoPortTableModel.COLUMN_LABEL:
                    setText(servoPort.toString());
                    setEnabled(servoPort.isEnabled());
                    break;
                case ServoPortTableModel.COLUMN_SPEED:
                    setText(Integer.toString(servoPort.getSpeed()));
                    // setEnabled(table.getModel().isCellEditable(row, column));
                    setEnabled(servoPort.isEnabled() && servoPort.isPortConfigEnabled()
                        && servoPort.isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_SERVO_SPEED));
                    break;
                case ServoPortTableModel.COLUMN_TRIM_DOWN:
                    setText(Integer.toString(servoPort.getTrimDown()));
                    setEnabled(servoPort.isEnabled() && servoPort.isPortConfigEnabled()
                        && servoPort.isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_SERVO_ADJ_L));
                    break;
                case ServoPortTableModel.COLUMN_TRIM_UP:
                    setText(Integer.toString(servoPort.getTrimUp()));
                    setEnabled(servoPort.isEnabled() && servoPort.isPortConfigEnabled()
                        && servoPort.isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_SERVO_ADJ_H));
                    break;
                default:
                    // should never happen
                    LOGGER.warn("Invalid renderer configuration detected, column: {}", column);
                    setEnabled(servoPort.isEnabled());
                    break;
            }
        }
        else {
            setEnabled(false);
            setText(null);
        }

        return this;
    }
}