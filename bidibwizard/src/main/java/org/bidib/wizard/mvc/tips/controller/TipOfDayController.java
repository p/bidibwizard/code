package org.bidib.wizard.mvc.tips.controller;

import org.bidib.wizard.mvc.tips.view.TipOfDayView;

public class TipOfDayController {

    /**
     * Show the tips of day view.
     * 
     * @param listener
     *            the closed listener
     */
    public void start(final TipOfDayClosedListener listener) {

        final TipOfDayView todView = new TipOfDayView();
        todView.showTipsOfTheDay(listener);

    }

}
