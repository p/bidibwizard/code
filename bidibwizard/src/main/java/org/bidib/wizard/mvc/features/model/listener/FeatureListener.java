package org.bidib.wizard.mvc.features.model.listener;

import org.bidib.jbidibc.core.Feature;

public interface FeatureListener {

    /**
     * Signal that the feature has changed.
     * 
     * @param feature
     *            the changed feature
     */
    void featureChanged(Feature feature);

    /**
     * Signal that the features have changed.
     */
    void featuresChanged();
}
