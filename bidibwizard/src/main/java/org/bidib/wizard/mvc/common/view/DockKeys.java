package org.bidib.wizard.mvc.common.view;

import com.vlsolutions.swing.docking.DockKey;

public final class DockKeys {

    public static final String BOOSTER_TABLE_VIEW = "BoosterTableView";

    public static final DockKey DOCKKEY_BOOSTER_TABLE_VIEW = new DockKey(BOOSTER_TABLE_VIEW);

    public static final String RAILCOM_PLUS_VIEW = "RailcomPlusView";

    public static final DockKey DOCKKEY_RAILCOM_PLUS_VIEW = new DockKey(RAILCOM_PLUS_VIEW);

    public static final String POM_UPDATE_VIEW = "PomUpdateView";

    public static final DockKey DOCKKEY_POM_UPDATE_VIEW = new DockKey(POM_UPDATE_VIEW);

    public static final String TAB_PANEL = "tabPanel";

    public static final DockKey DOCKKEY_TAB_PANEL = new DockKey(TAB_PANEL);

    public static final String DEBUG_INTERFACE_VIEW = "DebugInterfaceView";

    public static final DockKey DOCKKEY_DEBUG_INTERFACE_VIEW = new DockKey(DEBUG_INTERFACE_VIEW);

    public static final String PING_TABLE_VIEW = "PingTableView";

    public static final DockKey DOCKKEY_PING_TABLE_VIEW = new DockKey(PING_TABLE_VIEW);

    public static final String SIMULATION_VIEW = "simulationView";

    public static final DockKey DOCKKEY_SIMULATION_VIEW = new DockKey(SIMULATION_VIEW);

    public static final String LOCO_TABLE_VIEW = "LocoTableView";

    public static final DockKey DOCKKEY_LOCO_TABLE_VIEW = new DockKey(LOCO_TABLE_VIEW);
}
