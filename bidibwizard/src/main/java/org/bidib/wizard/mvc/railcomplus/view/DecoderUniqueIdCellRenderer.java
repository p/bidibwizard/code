package org.bidib.wizard.mvc.railcomplus.view;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class DecoderUniqueIdCellRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = 1L;

    public DecoderUniqueIdCellRenderer() {
    }

    public Component getTableCellRendererComponent(
        JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        if (value instanceof Integer) {
            int val = ((Integer) value).intValue();
            setText(String.format("0x%08X", val));
        }
        else {
            setText(null);
        }

        return this;
    }
}