package org.bidib.wizard.mvc.main.view.table;

import java.awt.Component;

import javax.swing.JTable;

import org.bidib.wizard.mvc.main.model.ServoPort;
import org.bidib.wizard.mvc.main.model.ServoPortTableModel;
import org.bidib.wizard.utils.IntegerEditor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServoPortTableCellEditor extends IntegerEditor {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ServoPortTableCellEditor.class);

    public ServoPortTableCellEditor(int min, int max) {
        super(min, max);
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {

        // renderer only handles ServoPorts
        if (value instanceof ServoPort) {
            ServoPort servoPort = (ServoPort) value;

            switch (column) {
                case ServoPortTableModel.COLUMN_LABEL:
                    value = servoPort.toString();
                    break;
                case ServoPortTableModel.COLUMN_SPEED:
                    value = servoPort.getSpeed();
                    break;
                case ServoPortTableModel.COLUMN_TRIM_DOWN:
                    value = servoPort.getTrimDown();
                    break;
                case ServoPortTableModel.COLUMN_TRIM_UP:
                    value = servoPort.getTrimUp();
                    break;
                default:
                    // should never happen
                    LOGGER.warn("Invalid renderer configuration detected, column: {}", column);
                    break;
            }
        }

        Component component = super.getTableCellEditorComponent(table, value, isSelected, row, column);

        return component;
    }

}
