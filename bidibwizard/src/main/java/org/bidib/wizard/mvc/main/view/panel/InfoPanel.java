package org.bidib.wizard.mvc.main.view.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.WordUtils;
import org.bidib.jbidibc.core.Feature;
import org.bidib.jbidibc.core.StringData;
import org.bidib.jbidibc.core.schema.DecoderVendorFactory;
import org.bidib.jbidibc.core.schema.bidib.products.DocumentationType;
import org.bidib.jbidibc.core.schema.bidib.products.ProductType;
import org.bidib.jbidibc.core.schema.decodervendor.VendorType;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.core.utils.NodeUtils;
import org.bidib.jbidibc.exchange.bidib.ProductsFactory;
import org.bidib.jbidibc.exchange.bidib.ProductsUtils;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.common.utils.ImageUtils;
import org.bidib.wizard.mvc.common.view.panel.ColorPane;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.view.panel.renderer.BidibNodeNameUtils;
import org.bidib.wizard.utils.XmlLocaleUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.debug.FormDebugPanel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.FormLayout;

public class InfoPanel {

    private static final Logger LOGGER = LoggerFactory.getLogger(InfoPanel.class);

    private static final String ENCODED_DIALOG_COLUMN_SPECS = "fill:pref:grow, 5dlu, 200dlu";

    private final MainModel mainModel;

    private final ColorPane infoArea = new ColorPane();

    private final JPanel contentPanel;

    private List<VendorType> vendors;

    private Map<Integer, List<ProductType>> productsMap = new HashMap<>();

    private JScrollPane logsPane;

    private final PropertyChangeListener nodeListener;

    private Node selectedNode;

    private JLabel imageContainer;

    public InfoPanel(final MainModel model) {
        this.mainModel = model;

        nodeListener = new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (org.bidib.jbidibc.core.Node.PROPERTY_FEATURES == evt.getPropertyName()) {
                    // the features have changed
                    updateNodeInfo(selectedNode);
                }

            }
        };

        vendors = DecoderVendorFactory.getDecoderVendors();

        // preload opendcc products
        loadProducts(13);

        // create form builder
        DefaultFormBuilder dialogBuilder = null;
        boolean debugDialog = false;
        if (debugDialog) {
            JPanel panel = new FormDebugPanel() {
                private static final long serialVersionUID = 1L;

                @Override
                public String getName() {
                    // this is used as tab title
                    return Resources.getString(InfoPanel.class, "name");
                }
            };
            dialogBuilder = new DefaultFormBuilder(new FormLayout(ENCODED_DIALOG_COLUMN_SPECS), panel);
        }
        else {
            JPanel panel = new JPanel(new BorderLayout()) {
                private static final long serialVersionUID = 1L;

                @Override
                public String getName() {
                    // this is used as tab title
                    return Resources.getString(InfoPanel.class, "name");
                }
            };
            dialogBuilder = new DefaultFormBuilder(new FormLayout(ENCODED_DIALOG_COLUMN_SPECS), panel);
        }
        dialogBuilder.border(Borders.TABBED_DIALOG);

        dialogBuilder.appendRow("fill:200dlu:grow");

        infoArea.setEditable(false);
        infoArea.setFont(new Font("Monospaced", Font.PLAIN, 11));

        logsPane = new JScrollPane(infoArea);

        // logsPane.setAutoscrolls(true);
        dialogBuilder.append(logsPane);

        imageContainer = new JLabel();
        imageContainer.setPreferredSize(new Dimension(200, 180));

        JPanel imagePanel = new JPanel(new BorderLayout());
        imagePanel.setOpaque(false);
        imagePanel.add(imageContainer, BorderLayout.NORTH);

        dialogBuilder.append(imagePanel);

        contentPanel = dialogBuilder.build();
    }

    public JPanel getComponent() {
        return contentPanel;
    }

    public void nodeChanged() {
        LOGGER.info("The selected node has changed.");

        updateComponentState();
    }

    private void updateComponentState() {

        Node node = mainModel.getSelectedNode();

        if (selectedNode != null && !selectedNode.equals(node)) {
            // the selected node has changed.
            selectedNode.getNode().removePropertyChangeListener(nodeListener);
        }

        if (node != null) {
            updateNodeInfo(node);

            selectedNode = node;
            // register the feature listener
            selectedNode.getNode().addPropertyChangeListener(nodeListener);
        }
        else {
            selectedNode = null;
        }
        // force scroll to top
        infoArea.setCaretPosition(0);
    }

    private void updateNodeInfo(final Node node) {
        if (node == null) {
            // no node available
            return;
        }
        LOGGER.info("Update the node info panel for node: {}", node);
        infoArea.setText(null);

        long uniqueId = node.getUniqueId();
        int vendorId = NodeUtils.getVendorId(node.getUniqueId());
        long productId = NodeUtils.getPid(node.getUniqueId(), node.getNode().getRelevantPidBits());

        List<ProductType> products = productsMap.get(vendorId);
        if (products == null) {
            products = loadProducts(vendorId);
        }

        // prepare information of node
        StringBuilder sb = new StringBuilder();
        StringBuilder description = new StringBuilder();
        String url = null;
        if (products != null) {
            final int pid = NodeUtils.getPid(uniqueId);
            ProductType product = IterableUtils.find(products, new Predicate<ProductType>() {

                @Override
                public boolean evaluate(ProductType product) {
                    return product.getProductTypeId() == pid;
                }
            });
            if (product != null) {
                description.append(product.getName());
                // get the documentation in the correct language
                String language = XmlLocaleUtils.getXmlLocale();
                DocumentationType doc = ProductsUtils.getDocumentationOfLanguage(product, language);
                if (doc != null) {
                    if (StringUtils.isNotBlank(doc.getDescription())) {
                        description.append(" - ").append(doc.getDescription());
                    }

                    url = doc.getUrl();
                }
            }
        }
        if (description.length() == 0) {
            description.append(Resources.getString(getClass(), "product-unknown"));
        }

        String productName = node.getNode().getStoredString(StringData.INDEX_PRODUCTNAME);
        if (StringUtils.isBlank(productName)) {
            productName = "";
        }

        infoArea.append(Color.BLACK, Resources.getString(getClass(), "description"));
        infoArea.append(Color.BLACK, " : ");
        infoArea.append(Color.BLUE, WordUtils.wrap(description.toString(), 80, "\n", false));
        infoArea.append(Color.BLACK, "\n");
        sb.append(Resources.getString(getClass(), "productname")).append(" : ").append(productName).append("\n");
        sb
            .append(Resources.getString(getClass(), "vendor")).append(" : ")
            .append(DecoderVendorFactory.getDecoderVendorName(vendors, vendorId)).append("\n");
        sb.append("Homepage ..... : ");
        if (StringUtils.isBlank(url)) {
            sb.append("---");
        }
        else {
            sb.append(url);
        }
        sb.append("\n");
        sb.append("Unique ID .... : ");
        infoArea.append(Color.BLACK, sb.toString());

        infoArea.append(Color.RED, ByteUtils.convertUniqueIdToString(ByteUtils.convertLongToUniqueId(uniqueId)));
        infoArea.append(Color.BLACK, ", ");
        infoArea.append(Color.BLUE.darker(), NodeUtils.getUniqueIdAsString(uniqueId));

        sb.setLength(0);
        sb.append("\n");
        sb.append(Resources.getString(getClass(), "username")).append(" : ");
        infoArea.append(Color.BLACK, sb.toString());
        infoArea.append(Color.GREEN.darker().darker(), BidibNodeNameUtils.getNodeName(node));

        sb.setLength(0);
        sb.append("\n");
        sb
            .append(Resources.getString(getClass(), "address")).append(" : ")
            .append(NodeUtils.formatAddressLong(node.getNode().getAddr())).append("\n");
        sb.append("Firmware ..... : ").append(node.getNode().getSoftwareVersion()).append("\n");
        sb
            .append(Resources.getString(getClass(), "protocol")).append(" : ")
            .append(node.getNode().getProtocolVersion()).append("\n");

        // add the features
        List<Feature> features = new LinkedList<>(node.getNode().getFeatures());
        sb.append("Feature count  : ").append(features.size()).append("\n");
        sb.append("Features ..... : \n");
        Collections.sort(features);
        for (Feature feature : features) {
            sb
                .append(String.format(" ID %1$3d, Value : %2$3d", feature.getType(), feature.getValue()))
                .append(" --- ").append(feature.getFeatureName()).append("\n");
        }

        infoArea.append(Color.BLACK, sb.toString());

        // try to load image
        try {
            LOGGER.info("Size of container: {}", imageContainer.getSize());

            ImageIcon icon =
                ImageUtils.loadImageIcon(getClass(), "/images/bidib-" + vendorId + "-" + productId + ".png", 200, 180);
            imageContainer.setIcon(icon);
        }
        catch (Exception ex) {
            LOGGER.warn("Load image of node failed, vid: {}, pid: {}.", vendorId, productId, ex);
            imageContainer.setIcon(null);
        }
    }

    private List<ProductType> loadProducts(int vendorId) {
        List<ProductType> products = ProductsFactory.getProducts(vendorId, "classpath:/xml/BiDiBProducts");
        productsMap.put(vendorId, products);
        return products;
    }
}
