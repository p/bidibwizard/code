package org.bidib.wizard.mvc.main.view.panel;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Calendar;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.common.view.panel.DisabledPanel;
import org.bidib.wizard.mvc.main.model.Macro;
import org.bidib.wizard.mvc.main.model.MacroRepeatDay;
import org.bidib.wizard.mvc.main.model.MacroRepeatTime;
import org.bidib.wizard.mvc.main.model.MacroSaveState;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.StartCondition;
import org.bidib.wizard.mvc.main.model.TimeStartCondition;
import org.bidib.wizard.mvc.main.model.function.Function;
import org.bidib.wizard.mvc.main.model.listener.MacroListListener;
import org.bidib.wizard.mvc.main.model.listener.MacroListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MacroParameterPanel extends JPanel implements MacroListener {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(MacroParameterPanel.class);

    private final JSpinner timeSpinner = new JSpinner(new SpinnerDateModel());

    private final DisabledPanel disabledRepeatPanel;

    private boolean silentUpdate;

    private Macro macro;

    private final SliderPanel cyclesPanel;

    private final SliderPanel delayPanel;

    private final JCheckBox timeButton;

    private final JComboBox<MacroRepeatTime> repeatTime;

    private final JComboBox<MacroRepeatDay> repeatDay;

    public MacroParameterPanel(final MainModel model) {
        setLayout(new GridBagLayout());

        final JPanel repeatPanel = new JPanel(new GridLayout(2, 1));

        repeatTime = new JComboBox<MacroRepeatTime>();
        repeatTime.setModel(new DefaultComboBoxModel<MacroRepeatTime>(MacroRepeatTime.values()));

        repeatDay = new JComboBox<MacroRepeatDay>();
        repeatDay.setModel(new DefaultComboBoxModel<MacroRepeatDay>(MacroRepeatDay.values()));

        timeSpinner.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                for (StartCondition startCondition : model.getSelectedMacro().getStartConditions()) {
                    if (startCondition instanceof TimeStartCondition) {
                        JSpinner spinner = (JSpinner) e.getSource();
                        Calendar spinnerTime = Calendar.getInstance();

                        spinnerTime.setTime((Date) spinner.getValue());

                        Calendar time = Calendar.getInstance();

                        time.set(Calendar.HOUR_OF_DAY, spinnerTime.get(Calendar.HOUR_OF_DAY));
                        time.set(Calendar.MINUTE, spinnerTime.get(Calendar.MINUTE));
                        ((TimeStartCondition) startCondition).setTime(time);
                    }
                }
            }
        });

        timeButton = new JCheckBox(Resources.getString(getClass(), "time") + ":");

        timeButton.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                LOGGER.info("The selected item has been changed, silentUpdate: {}.", silentUpdate);

                timeSpinner.setEnabled(e.getStateChange() == ItemEvent.SELECTED);
                disabledRepeatPanel.setEnabled(e.getStateChange() == ItemEvent.SELECTED);
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    Calendar time = Calendar.getInstance();

                    time.setTime((Date) timeSpinner.getValue());
                    if (!silentUpdate) {
                        model.getSelectedMacro().addStartCondition(new TimeStartCondition(time));
                    }
                }
                else {
                    if (!silentUpdate) {
                        model.getSelectedMacro().removeStartCondition(new TimeStartCondition());
                    }
                }

                if (!silentUpdate) {
                    Macro macro = model.getSelectedMacro();
                    macro.setMacroSaveState(MacroSaveState.PENDING_CHANGES);
                }
            }
        });

        JPanel startConditionPanel = new JPanel(new BorderLayout());
        startConditionPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
            Resources.getString(getClass(), "condition") + ":"));

        FlowLayout leftLayout = new FlowLayout();

        leftLayout.setAlignment(FlowLayout.LEFT);
        timeSpinner.setEditor(new JSpinner.DateEditor(timeSpinner, "HH:mm"));

        JPanel timePanel = new JPanel(leftLayout);

        timePanel.add(timeButton);
        timePanel.add(timeSpinner);

        startConditionPanel.add(timePanel, BorderLayout.NORTH);

        repeatTime.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for (StartCondition startCondition : model.getSelectedMacro().getStartConditions()) {
                    if (startCondition instanceof TimeStartCondition) {
                        MacroRepeatTime repeatTime = (MacroRepeatTime) ((JComboBox<?>) e.getSource()).getSelectedItem();

                        ((TimeStartCondition) startCondition).setRepeatTime(repeatTime);

                        if (!silentUpdate) {
                            Macro macro = model.getSelectedMacro();
                            macro.setMacroSaveState(MacroSaveState.PENDING_CHANGES);
                        }
                    }
                }
            }
        });
        repeatPanel.add(repeatTime);
        repeatDay.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for (StartCondition startCondition : model.getSelectedMacro().getStartConditions()) {
                    if (startCondition instanceof TimeStartCondition) {
                        ((TimeStartCondition) startCondition)
                            .setRepeatDay((MacroRepeatDay) ((JComboBox<?>) e.getSource()).getSelectedItem());

                        if (!silentUpdate) {
                            Macro macro = model.getSelectedMacro();
                            macro.setMacroSaveState(MacroSaveState.PENDING_CHANGES);
                        }
                    }
                }
            }
        });
        repeatPanel.add(repeatDay);
        repeatPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
            Resources.getString(getClass(), "repetition") + ":"));

        disabledRepeatPanel = new DisabledPanel(repeatPanel);
        startConditionPanel.add(disabledRepeatPanel, BorderLayout.CENTER);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;

        add(startConditionPanel, gbc);

        delayPanel = new SliderPanel(Resources.getString(getClass(), "delay") + ":", 1, 255, 255, true, 0) {
            private static final long serialVersionUID = 1L;

            @Override
            public void stateChanged(ChangeEvent e) {
                Macro macro = model.getSelectedMacro();

                if (macro != null) {
                    JSlider source = (JSlider) e.getSource();

                    macro.setSpeed((int) source.getValue());
                    setLabel(macro.getSpeed());

                    macro.setMacroSaveState(MacroSaveState.PENDING_CHANGES);
                }
            }
        };

        gbc.gridx++;
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.BOTH;
        add(delayPanel, gbc);

        cyclesPanel = new SliderPanel(Resources.getString(getClass(), "cycles") + ":", 1, 251, 1, false, 251) {
            private static final long serialVersionUID = 1L;

            @Override
            public void stateChanged(ChangeEvent e) {
                Macro macro = model.getSelectedMacro();

                if (macro != null) {
                    JSlider source = (JSlider) e.getSource();
                    int cycles = (int) source.getValue();
                    // if more than 250 then we have infinite cycles
                    if (cycles > Macro.MAX_CYCLES) {
                        cycles = Macro.INFINITE_CYCLES;
                    }
                    macro.setCycles(cycles);
                    setLabel((int) source.getValue());

                    macro.setMacroSaveState(MacroSaveState.PENDING_CHANGES);
                }
            }
        };

        gbc.gridx++;
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.BOTH;
        add(cyclesPanel, gbc);

        model.addMacroListListener(new MacroListListener() {
            @Override
            public void listChanged() {
            }

            @Override
            public void macroChanged() {
                processMacroChanged(model);
            }

            @Override
            public void pendingChangesChanged() {
            }
        });
    }

    private void processMacroChanged(MainModel model) {
        Macro macro = model.getSelectedMacro();
        LOGGER.info("The selected macro has changed: {}", macro);

        if (this.macro != null) {
            this.macro.removeMacroListener(this);
        }
        // set the macro
        this.macro = macro;

        if (macro != null) {
            this.macro.addMacroListener(this);
        }

        setValueSilently();
    }

    private void setValueSilently() {
        silentUpdate = true;
        try {
            if (macro != null) {
                delayPanel.setValueSilently(macro.getSpeed());
                int cycles = macro.getCycles();
                // infinite cycles is handled as 251 in the UI
                if (cycles == Macro.INFINITE_CYCLES) {
                    cycles = Macro.MAX_CYCLES + 1;
                }
                cyclesPanel.setValueSilently(cycles);

                boolean hasTimeCondition = false;

                for (StartCondition startCondition : macro.getStartConditions()) {
                    if (startCondition instanceof TimeStartCondition) {
                        hasTimeCondition = true;

                        Calendar time = ((TimeStartCondition) startCondition).getTime();

                        if (time != null) {
                            timeSpinner.setValue(time.getTime());
                        }
                        repeatDay.setSelectedIndex(((TimeStartCondition) startCondition).getRepeatDay().ordinal());
                        repeatTime.setSelectedIndex(((TimeStartCondition) startCondition).getRepeatTime().ordinal());
                    }
                }
                timeButton.setSelected(hasTimeCondition);
                timeSpinner.setEnabled(hasTimeCondition);
                disabledRepeatPanel.setEnabled(hasTimeCondition);
            }
        }
        finally {
            silentUpdate = false;
        }
    }

    @Override
    public void functionsAdded(int row, Function<? extends BidibStatus>[] functions) {

    }

    @Override
    public void functionRemoved(int row) {

    }

    @Override
    public void functionMoved(int fromIndex, int toIndex, Function<? extends BidibStatus> fromFunction) {
    }

    @Override
    public void functionsRemoved() {

    }

    @Override
    public void labelChanged(String label) {

    }

    @Override
    public void startConditionChanged() {
        LOGGER.info("The start condition has changed.");

        setValueSilently();
    }

    @Override
    public void slowdownFactorChanged() {
        LOGGER.info("The slowdown factor has changed.");

        setValueSilently();
    }

    @Override
    public void cyclesChanged() {
        LOGGER.info("The cycles have changed.");

        setValueSilently();
    }
}
