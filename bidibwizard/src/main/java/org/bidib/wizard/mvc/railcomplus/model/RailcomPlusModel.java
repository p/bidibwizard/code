package org.bidib.wizard.mvc.railcomplus.model;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.collections4.Predicate;
import org.bidib.jbidibc.core.DecoderIdAddressData;
import org.bidib.jbidibc.core.TidData;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.beans.Model;
import com.jgoodies.common.collect.ArrayListModel;

public class RailcomPlusModel extends Model {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(RailcomPlusModel.class);

    public static final String PROPERTY_DECODERS = "decoders";

    public static final String PROPERTY_TID = "tid";

    private TidData tid;

    private ArrayListModel<RailcomPlusDecoderModel> decoderList = new ArrayListModel<>();

    public RailcomPlusModel() {

    }

    /**
     * @return the decoderList model
     */
    public ArrayListModel<RailcomPlusDecoderModel> getDecoderListModel() {
        return decoderList;
    }

    /**
     * @return the tid
     */
    public TidData getTid() {
        return tid;
    }

    /**
     * @param tid
     *            the tid to set
     */
    public void setTid(TidData tid) {
        LOGGER.info("Set the TID: {}", tid);
        TidData oldValue = this.tid;
        this.tid = tid;
        firePropertyChange(PROPERTY_TID, oldValue, this.tid);
    }

    public void addDecoder(RailcomPlusDecoderModel decoder) {
        synchronized (decoderList) {
            if (!decoderList.contains(decoder)) {
                LOGGER.info("Add decoder to decoder list: {}", decoder);
                List<RailcomPlusDecoderModel> oldValue = new LinkedList<>(decoderList);

                decoderList.add(decoder);

                firePropertyChange(PROPERTY_DECODERS, oldValue, decoderList);
            }
            else {
                LOGGER.warn("Decoder is already in decoder list: {}", decoder);
            }
        }
    }

    public void removeDecoder(RailcomPlusDecoderModel decoder) {
        synchronized (decoderList) {
            LOGGER.info("Remove decoder from decoder list: {}", decoder);

            List<RailcomPlusDecoderModel> oldValue = new LinkedList<>(decoderList);
            boolean removed = decoderList.remove(decoder);

            if (removed) {
                firePropertyChange(PROPERTY_DECODERS, oldValue, decoderList);
            }
            else {
                LOGGER.warn("Decoder was not removed from decoder list: {}", decoder);
            }
        }
    }

    public RailcomPlusDecoderModel findDecoder(final DecoderIdAddressData did) {

        RailcomPlusDecoderModel decoder = null;
        final int searchManufacturer = ByteUtils.getInt(did.getManufacturedId());
        final int searchMun = ByteUtils.convertSerial(did.getDidAddress(), 0);

        LOGGER.info("Search decoder in decoder list: {}, searchManufacturer: {}, searchMun: {}", decoder,
            searchManufacturer, searchMun);

        synchronized (decoderList) {

            decoder = IterableUtils.find(decoderList, new Predicate<RailcomPlusDecoderModel>() {

                @Override
                public boolean evaluate(RailcomPlusDecoderModel dec) {
                    if (dec.getDecoderManufacturer() == searchManufacturer && dec.getDecoderMun() == searchMun) {
                        return true;
                    }
                    return false;
                }
            });
        }

        LOGGER.info("find decoder returns: {}", decoder);
        return decoder;
    }

    public void decoderChanged(RailcomPlusDecoderModel decoder) {
        int index = decoderList.indexOf(decoder);
        if (index > -1) {
            decoderList.fireContentsChanged(index);
        }
        else {
            LOGGER.warn("Decoder not found in model: {}", decoder);
        }
    }
}
