package org.bidib.wizard.mvc.main.view.panel;

import java.util.Dictionary;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class SliderPanel extends JPanel {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(SliderPanel.class);

    private static final String INFINITY = "\u221e";

    private final JSlider slider;

    private final JLabel sliderLabel = new JLabel();

    private int infiniteValue;

    public SliderPanel(String title, int min, int max, int value, boolean inverted, int infiniteValue) {
        this.infiniteValue = infiniteValue;
        setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), title));
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        slider = new JSlider(
            JSlider.HORIZONTAL) {
            private static final long serialVersionUID = 1L;

            @Override
            public Dictionary<?, ?> getLabelTable() {
                Dictionary<?, ?> result = super.getLabelTable();

                if (result != null) {
                    JLabel label = (JLabel) result.get(getInfiniteValue());

                    if (label != null) {
                        label.setText(INFINITY);
                    }
                }
                return result;
            }
        };
        slider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if (silentUpdate) {
                    LOGGER.info("Silent update flag is set, do not propagate to listeners.");
                    return;
                }
                SliderPanel.this.stateChanged(e);
            }
        });
        slider.setInverted(inverted);
        slider.setMajorTickSpacing(max - min);
        slider.setMaximum(max);
        slider.setMinimum(min);
        slider.setMinorTickSpacing(16);
        slider.setPaintLabels(true);
        slider.setPaintTicks(true);
        slider.setValue(value);
        add(Box.createVerticalGlue());
        add(slider);
        add(Box.createVerticalGlue());
        setLabel(value);
        add(sliderLabel);
    }

    protected int getInfiniteValue() {
        return infiniteValue;
    }

    public void setLabel(int value) {
        sliderLabel.setText(value != infiniteValue ? String.valueOf(value) : INFINITY);
    }

    public void setValue(int value) {
        slider.setValue(value);
    }

    private boolean silentUpdate;

    public void setValueSilently(int value) {
        try {
            silentUpdate = true;
            slider.setValue(value);
            setLabel(value);
        }
        finally {
            silentUpdate = false;
        }
    }

    public abstract void stateChanged(ChangeEvent e);
}
