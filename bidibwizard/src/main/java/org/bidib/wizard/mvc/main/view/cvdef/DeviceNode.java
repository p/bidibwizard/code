package org.bidib.wizard.mvc.main.view.cvdef;

import com.jidesoft.grid.DefaultExpandableRow;

public class DeviceNode extends DefaultExpandableRow {

    private String name;

    public DeviceNode(String name) {
        this.name = name;
    }

    @Override
    public Object getValueAt(int columnIndex) {
        return name;
    }

    @Override
    public boolean isCellEditable(int column) {
        return false;
    }

}
