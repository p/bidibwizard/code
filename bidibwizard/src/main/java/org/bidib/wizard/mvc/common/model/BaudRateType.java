package org.bidib.wizard.mvc.common.model;

import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.bidib.wizard.common.locale.Resources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BaudRateType {
    private static final Logger LOGGER = LoggerFactory.getLogger(BaudRateType.class);

    public enum BaudRate {
        Br19200(19200, Resources.getString(BaudRateType.class, "19200")), Br115200(115200, Resources.getString(
            BaudRateType.class, "115200"));

        private Integer baudRate;

        private String tooltip;

        private BaudRate(Integer baudRate, String tooltip) {
            this.baudRate = baudRate;
            this.tooltip = tooltip;
        }

        public Integer getBaudRate() {
            return baudRate;
        }

        public String getTooltip() {
            return tooltip;
        }
    }

    public static final String NONE = "<none>";

    private Integer baudRate;

    private String tooltip;

    public BaudRateType(Integer baudRate, String tooltip) {
        this.baudRate = baudRate;
        this.tooltip = tooltip;
    }

    /**
     * @return the baudRate
     */
    public Integer getBaudRate() {
        return baudRate;
    }

    /**
     * @param baudRate
     *            the baudRate to set
     */
    public void setBaudRate(Integer baudRate) {
        this.baudRate = baudRate;
    }

    /**
     * @return the tooltip
     */
    public String getTooltip() {
        return tooltip;
    }

    /**
     * @param tooltip
     *            the tooltip to set
     */
    public void setTooltip(String tooltip) {
        this.tooltip = tooltip;
    }

    @Override
    public boolean equals(Object other) {
        if (other != null && other instanceof BaudRateType) {
            return Objects.equals(baudRate, ((BaudRateType) other).baudRate);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    public String toString() {
        if (baudRate != null) {
            StringBuffer sb = new StringBuffer();
            sb.append(baudRate);
            if (StringUtils.isNotBlank(tooltip)) {
                sb.append(" : ").append(tooltip);
            }
            return sb.toString();
        }
        return NONE;
    }

    public static BaudRateType getValue(String value) {
        LOGGER.debug("Get the BaudRateType for value: {}", value);
        BaudRateType baudRateType = null;
        if (StringUtils.isNotBlank(value)) {
            Integer orgValue = Integer.valueOf(value);
            for (BaudRate br : BaudRate.values()) {
                if (orgValue.equals(br.baudRate)) {
                    LOGGER.debug("Found matching baudrate: {}", br);

                    baudRateType = new BaudRateType(br.baudRate, br.tooltip);
                    break;
                }
            }
        }

        if (baudRateType == null) {
            baudRateType = new BaudRateType(null, "<none>");
        }
        return baudRateType;
    }
}
