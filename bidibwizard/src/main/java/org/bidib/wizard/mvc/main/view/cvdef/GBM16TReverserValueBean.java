package org.bidib.wizard.mvc.main.view.cvdef;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.common.bean.Bean;

public class GBM16TReverserValueBean extends Bean {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(GBM16TReverserValueBean.class);

    // the cv number of the mode (the first number of a fixed collection)
    private Byte reverserModeValue;

    private Integer reverserOnValue;

    private Integer reverserOffValue;

    private Integer reverserPrioValue;

    private GBM16TReverserCvNode cvNodeMaster;

    /**
     * @return the cvNodeMaster
     */
    public GBM16TReverserCvNode getCvNodeMaster() {
        return cvNodeMaster;
    }

    /**
     * @param cvNodeMaster
     *            the cvNodeMaster to set
     */
    public void setCvNodeMaster(GBM16TReverserCvNode cvNodeMaster) {
        LOGGER.info("Set the master node: {}", cvNodeMaster);
        this.cvNodeMaster = cvNodeMaster;
    }

    public Byte getReverserModeValue() {
        return reverserModeValue;
    }

    public void setReverserModeValue(Byte reverserModeValue) {
        LOGGER.info("Set the reverser mode value: {}", reverserModeValue);
        Byte oldValue = this.reverserModeValue;
        this.reverserModeValue = reverserModeValue;

        firePropertyChange("reverserModeValue", oldValue, this.reverserModeValue);
    }

    public Integer getReverserOnValue() {
        return reverserOnValue;
    }

    public void setReverserOnValue(Integer reverserOnValue) {
        LOGGER.debug("Set the reverserOn value: {}", reverserOnValue);
        Integer oldValue = this.reverserOnValue;
        this.reverserOnValue = reverserOnValue;

        firePropertyChange("reverserOnValue", oldValue, this.reverserOnValue);
    }

    public Integer getReverserOffValue() {
        return reverserOffValue;
    }

    public void setReverserOffValue(Integer reverserOffValue) {
        LOGGER.debug("Set the reverserOff value: {}", reverserOffValue);
        Integer oldValue = this.reverserOffValue;
        this.reverserOffValue = reverserOffValue;

        firePropertyChange("reverserOffValue", oldValue, this.reverserOffValue);
    }

    public Integer getReverserPrioValue() {
        return reverserPrioValue;
    }

    public void setReverserPrioValue(Integer reverserPrioValue) {
        LOGGER.debug("Set the reverserPrio value: {}", reverserPrioValue);
        Integer oldValue = this.reverserPrioValue;
        this.reverserPrioValue = reverserPrioValue;

        firePropertyChange("reverserPrioValue", oldValue, this.reverserPrioValue);
    }
}
