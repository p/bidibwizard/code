package org.bidib.wizard.mvc.pom.view.panel.listener;

import org.bidib.jbidibc.core.AddressData;
import org.bidib.jbidibc.core.enumeration.PomOperation;

public interface PomRequestListener {
    /**
     * Send the PT request.
     * 
     * @param ptResultListener
     *            the result listener that performs the request
     * @param decoderAddress
     *            the decoder address
     * @param operation
     *            the operation
     * @param cvNumber
     *            the CV number
     * @param cvValue
     *            the CV value
     */
    void sendRequest(
        PomResultListener ptResultListener, AddressData decoderAddress, PomOperation operation, int cvNumber,
        int cvValue);
}
