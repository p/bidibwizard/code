package org.bidib.wizard.mvc.dmx.view.panel;

import java.awt.Point;
import java.awt.event.ActionEvent;

import org.bidib.wizard.mvc.dmx.model.DmxChannel;
import org.bidib.wizard.mvc.main.model.Port;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The <code>PortAction</code> creates a point for the dmxChannel with a port assigned.
 */
public class PortAction extends LocationAwareAction<DmxChannel> {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(PortAction.class);

    private Port<?> port;

    private DmxDataItem originalDataItem;

    public PortAction(Port<?> port, DmxChannel dmxChannel, DmxChartPanel dmxChartPanel, DmxDataItem originalDataItem) {
        super(port.toString(), dmxChannel, dmxChartPanel);
        this.port = port;
        this.originalDataItem = originalDataItem;
    }

    @Override
    public void actionPerformed(ActionEvent event) {

        Point currentMousePoint = dmxChartPanel.getCurrentMousePoint();

        String seriesKey = Integer.toString(getActionObject().getChannelId());
        LOGGER.info("Selected key: {}, currentMousePoint: {}", seriesKey, currentMousePoint);

        int currentX = (int) Math.round(currentMousePoint.getX());
        int currentY = (int) Math.round(currentMousePoint.getY());

        if (originalDataItem != null) {
            LOGGER.info("Use the coordinates of the original data item: {}", originalDataItem);
            currentX = originalDataItem.getTimeOffset();
            currentY = originalDataItem.getBrightness();
        }

        LOGGER.info("Add new point at X: {}, Y: {}", currentX, currentY);

        dmxChartPanel.createDataItem(seriesKey, currentX, currentY, port, getActionObject(), originalDataItem);

    }
}
