package org.bidib.wizard.mvc.main.controller;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.JOptionPane;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.LoadTypeEnum;
import org.bidib.jbidibc.core.enumeration.PortConfigKeys;
import org.bidib.jbidibc.core.exception.InvalidConfigurationException;
import org.bidib.jbidibc.core.port.BytePortConfigValue;
import org.bidib.jbidibc.core.port.PortConfigValue;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.comm.CommunicationFactory;
import org.bidib.wizard.comm.SwitchPortStatus;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.labels.Labels;
import org.bidib.wizard.labels.SwitchPairPortLabelFactory;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.bidib.wizard.mvc.main.controller.wrapper.NodePortWrapper;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.model.Port;
import org.bidib.wizard.mvc.main.model.SwitchPairPort;
import org.bidib.wizard.mvc.main.model.listener.DefaultNodeListListener;
import org.bidib.wizard.mvc.main.model.listener.SwitchPairPortListener;
import org.bidib.wizard.mvc.main.view.panel.SwitchPairPortListPanel;
import org.bidib.wizard.mvc.script.view.ScriptParser;
import org.bidib.wizard.script.ScriptCommand;
import org.bidib.wizard.script.engine.ScriptEngine;
import org.bidib.wizard.script.switching.SwitchPairPortCommand;
import org.bidib.wizard.script.switching.WaitCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SwitchPairPortPanelController implements PortScripting {

    private static final Logger LOGGER = LoggerFactory.getLogger(SwitchPairPortPanelController.class);

    private final MainModel mainModel;

    private final Map<Node, NodePortWrapper> testToggleRegistry = new LinkedHashMap<>();

    public SwitchPairPortPanelController(final MainModel mainModel) {
        this.mainModel = mainModel;
    }

    public SwitchPairPortListPanel createPanel() {
        final Labels switchPairPortLabels =
            DefaultApplicationContext.getInstance().get(DefaultApplicationContext.KEY_SWITCHPAIRPORT_LABELS,
                Labels.class);

        final SwitchPairPortListPanel switchPairPortListPanel = new SwitchPairPortListPanel(this, mainModel);

        switchPairPortListPanel.addPortListener(new SwitchPairPortListener<SwitchPortStatus>() {
            @Override
            public void labelChanged(Port<SwitchPortStatus> port, String label) {
                port.setLabel(label);

                try {
                    SwitchPairPortLabelFactory factory = new SwitchPairPortLabelFactory();
                    long uniqueId = mainModel.getSelectedNode().getNode().getUniqueId();
                    factory.replaceLabel(switchPairPortLabels, uniqueId, port.getId(), label);

                    factory.saveLabels(uniqueId, switchPairPortLabels);
                }
                catch (InvalidConfigurationException ex) {
                    LOGGER.warn("Save switchPair port labels failed.", ex);

                    String labelPath = ex.getReason();
                    JOptionPane.showMessageDialog(JOptionPane.getFrameForComponent(null),
                        Resources.getString(Labels.class, "labelfileerror.message", new Object[] { labelPath }),
                        Resources.getString(Labels.class, "labelfileerror.title"), JOptionPane.ERROR_MESSAGE);
                }

                switchPairPortListPanel.repaint();
            }

            @Override
            public void statusChanged(Port<SwitchPortStatus> port, SwitchPortStatus status) {
                LOGGER.debug("Status of switch port has changed, port: {}", port);
            }

            @Override
            public void valuesChanged(SwitchPairPort port, PortConfigKeys... portConfigKeys) {

                LOGGER.info("The port value are changed for port: {}", port);

                Map<Byte, PortConfigValue<?>> values = new LinkedHashMap<>();

                for (PortConfigKeys key : portConfigKeys) {
                    switch (key) {
                        case BIDIB_PCFG_TICKS:
                            int switchOffTime = port.getSwitchOffTime();
                            values.put(BidibLibrary.BIDIB_PCFG_TICKS,
                                new BytePortConfigValue(ByteUtils.getLowByte(switchOffTime)));
                            break;
                        case BIDIB_PCFG_LOAD_TYPE:
                            LoadTypeEnum loadType = port.getLoadType();
                            values.put(BidibLibrary.BIDIB_PCFG_LOAD_TYPE, new BytePortConfigValue(loadType.getType()));
                            break;
                        default:
                            LOGGER.warn("Unsupported port config key detected: {}", key);
                            break;
                    }
                }

                // don't set the port type param to not send BIDIB_PCFG_RECONFIG
                CommunicationFactory.getInstance().setPortParameters(mainModel.getSelectedNode().getNode(), port, null,
                    values);
            }

            @Override
            public void testButtonPressed(Port<SwitchPortStatus> port) {
                LOGGER.info("The test button was pressed for port: {}", port);

                Node node = mainModel.getSelectedNode();
                if (SwitchPortStatus.TEST != port.getStatus()) {
                    stopTestToggleTask(node, port);

                    CommunicationFactory.getInstance().activateSwitchPairPort(node.getNode(), port.getId(),
                        (SwitchPortStatus) port.getStatus());
                }
                else {
                    addTestToggleTask(node, port);
                }
            }

            @Override
            public void configChanged(Port<SwitchPortStatus> port) {
            }

            @Override
            public void changePortType(LcOutputType portType, SwitchPairPort port) {
                LOGGER.info("The port type will change to: {}, port: {}", portType, port);

                Map<Byte, PortConfigValue<?>> values = new LinkedHashMap<>();

                CommunicationFactory.getInstance().setPortParameters(mainModel.getSelectedNode().getNode(), port,
                    portType, values);
            }
        });

        mainModel.addNodeListListener(new DefaultNodeListListener() {
            @Override
            public void nodeWillChange() {
                LOGGER.info("The selected node will change!");
                try {
                    List<Node> nodes = new LinkedList<>();
                    for (Node node : testToggleRegistry.keySet()) {
                        nodes.add(node);
                    }
                    LOGGER.info("Found nodes to stop the test toggle task: {}", nodes);
                    for (Node node : nodes) {
                        stopTestToggleTask(node, null);
                    }
                    LOGGER.info("Stop the test toggle task passed for nodes: {}", nodes);
                }
                catch (Exception ex) {
                    LOGGER.warn("Stop test toggle tasks failed.", ex);
                }
            }
        });

        return switchPairPortListPanel;
    }

    public void addTestToggleTask(final Node node, final Port<?> port) {
        LOGGER.info("Add test toggle task for node: {}, port: {}", node, port);

        NodePortWrapper nodePortWrapper = testToggleRegistry.remove(node);
        ScriptEngine<PortScripting> scriptEngine = null;
        if (nodePortWrapper != null) {
            scriptEngine = nodePortWrapper.removePort(port);
        }

        if (scriptEngine != null) {
            LOGGER.info("Found a node scripting engine in the registry: {}", scriptEngine);
            try {
                scriptEngine.stopScript(Long.valueOf(2000));
            }
            catch (Exception ex) {
                LOGGER.warn("Stop script failed.", ex);
            }
        }

        HashMap<String, Object> context = new LinkedHashMap<>();
        context.put(ScriptParser.KEY_SELECTED_NODE, node);
        context.put(ScriptParser.KEY_MAIN_MODEL, mainModel);

        scriptEngine = new ScriptEngine<PortScripting>(this, context);

        List<ScriptCommand<PortScripting>> scriptCommands = new LinkedList<ScriptCommand<PortScripting>>();
        SwitchPairPortCommand<PortScripting> spc = new SwitchPairPortCommand<>();
        spc.parse("SPPORT " + port.getId() + " ON");
        scriptCommands.add(spc);
        WaitCommand<PortScripting> wc = new WaitCommand<>();
        wc.parse("WAIT 2000");
        scriptCommands.add(wc);
        spc = new SwitchPairPortCommand<>();
        spc.parse("SPPORT " + port.getId() + " OFF");
        scriptCommands.add(spc);
        wc = new WaitCommand<>();
        wc.parse("WAIT 2000");
        scriptCommands.add(wc);

        LOGGER.info("Prepared list of commands: {}", scriptCommands);

        scriptEngine.setScriptCommands(scriptCommands);
        // repeating
        scriptEngine.setScriptRepeating(true);

        if (nodePortWrapper == null) {
            LOGGER.info("Create new NodePortWrapper for node: {}", node);
            nodePortWrapper = new NodePortWrapper(node);
        }

        LOGGER.info("Put script engine in registry for node: {}", node);
        nodePortWrapper.addPort(port, scriptEngine);

        testToggleRegistry.put(node, nodePortWrapper);

        scriptEngine.startScript();
    }

    public void stopTestToggleTask(final Node node, final Port<?> port) {
        LOGGER.info("Stop test toggle task for node: {}, port: {}", node, port);

        NodePortWrapper nodePortWrapper = testToggleRegistry.get(node);

        if (nodePortWrapper != null) {
            Set<Port<?>> toRemove = new HashSet<>();
            if (port != null) {
                toRemove.add(port);
            }
            else {
                toRemove.addAll(nodePortWrapper.getKeySet());
            }

            for (Port<?> removePort : toRemove) {
                ScriptEngine<PortScripting> engine = nodePortWrapper.removePort(removePort);

                if (engine != null) {
                    LOGGER.info("Found a node scripting engine in the registry: {}", engine);
                    try {
                        engine.stopScript(Long.valueOf(2000));
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Stop script failed.", ex);
                    }
                }
                else {
                    LOGGER.warn("No scripting engine found for node: {}", node);
                }
            }

            if (nodePortWrapper.isEmpty()) {
                LOGGER.info("No more ports registered for node: {}", node);
                testToggleRegistry.remove(node);
            }
        }
    }

    @Override
    public void sendPortStatusAction(int port, BidibStatus portStatus) {

        LOGGER.info("Switch the pair port: {}, portStatus: {}", port, portStatus);
        try {
            Node node = mainModel.getSelectedNode();
            SwitchPortStatus switchPortStatus = (SwitchPortStatus) portStatus;
            CommunicationFactory.getInstance().activateSwitchPairPort(node.getNode(), port, switchPortStatus);
        }
        catch (Exception ex) {
            LOGGER.warn("Switch pair port failed.", ex);
        }
    }

    @Override
    public void sendPortValueAction(int port, int portValue) {

    }
}
