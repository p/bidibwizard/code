package org.bidib.wizard.mvc.script.view.listener;

import org.bidib.wizard.script.node.types.TargetType;

public interface NodeLabelScriptingListener {

    /**
     * Set the port label.
     * 
     * @param uuid
     *            the uuid of the node
     * @param targetType
     *            the targetType with the label
     */
    void setLabel(Long uuid, TargetType targetType);
}
