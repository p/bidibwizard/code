package org.bidib.wizard.mvc.main.model;

import org.bidib.wizard.common.locale.Resources;

public enum MacroRepeatTime {
    // @formatter:off
    NONE(Resources.getString(MacroRepeatTime.class, "none")), MINUTELY(Resources.getString(MacroRepeatTime.class,
        "minutely")), QUARTER_HOURLY(Resources.getString(MacroRepeatTime.class, "quarter_hourly")), HALF_HOURLY(
        Resources.getString(MacroRepeatTime.class, "half_hourly")), HOURLY(Resources.getString(MacroRepeatTime.class,
        "hourly")), WORKING_HOURLY(Resources.getString(MacroRepeatTime.class, "working_hourly"));
    // @formatter:on

    private final String label;

    MacroRepeatTime(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return label;
    }
}
