package org.bidib.wizard.mvc.main.view.panel.listener;

public interface ModelClockStatusListener {

    void setModelTimeStartStatus(boolean enabled);
}
