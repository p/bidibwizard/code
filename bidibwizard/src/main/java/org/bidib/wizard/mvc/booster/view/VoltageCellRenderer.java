package org.bidib.wizard.mvc.booster.view;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class VoltageCellRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = 1L;

    public VoltageCellRenderer() {
    }

    public Component getTableCellRendererComponent(
        JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        if (value instanceof Integer) {
            float val = ((Integer) value).floatValue();
            StringBuffer sb = new StringBuffer();
            sb.append(val / 10).append(" V");
            setText(sb.toString());
        }
        else {
            setText(null);
        }

        return this;
    }
}