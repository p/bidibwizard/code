package org.bidib.wizard.mvc.dmx.view.scenery;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.dialog.EscapeDialog;
import org.bidib.wizard.mvc.common.view.binding.MultiListSelectionAdapter;
import org.bidib.wizard.mvc.common.view.binding.MultiSelectionInList;
import org.bidib.wizard.mvc.main.model.Port;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.value.ValueHolder;
import com.jgoodies.common.collect.ObservableList;
import com.jgoodies.forms.builder.ButtonBarBuilder;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.debug.FormDebugPanel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.FormLayout;

public class PortSelectionPanel {
    private static final Logger LOGGER = LoggerFactory.getLogger(PortSelectionPanel.class);

    private static final String ENCODED_COLUMN_SPECS = "pref, 3dlu, fill:200dlu:grow";

    private final JButton cancelButton = new JButton(Resources.getString(getClass(), "cancel"));

    private final JButton applyButton = new JButton(Resources.getString(getClass(), "apply"));

    private JList<Port<?>> portList;

    private MultiSelectionInList<Port<?>> portSelection;

    private ObservableList<Port<?>> selectionHolder;

    public PortSelectionPanel() {

    }

    private JPanel createPanel(List<Port<?>> ports) {
        DefaultFormBuilder builder = null;
        boolean debug = false;
        if (debug) {
            builder = new DefaultFormBuilder(new FormLayout(ENCODED_COLUMN_SPECS), new FormDebugPanel());
        }
        else {
            builder = new DefaultFormBuilder(new FormLayout(ENCODED_COLUMN_SPECS), new JPanel());
        }
        builder.border(Borders.TABBED_DIALOG);

        portSelection = new MultiSelectionInList<Port<?>>(ports.toArray(new Port<?>[0]));
        selectionHolder = portSelection.getSelection();

        portList = new JList<Port<?>>();
        portList.setModel(portSelection.getList());
        portList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        portList.setSelectionModel(new MultiListSelectionAdapter(portSelection));

        builder.append(new JScrollPane(portList), 3);

        // prepare the close button
        JPanel buttons = new ButtonBarBuilder().addGlue().addButton(cancelButton, applyButton).build();
        builder.append(buttons, 3);

        return builder.build();
    }

    public List<Port<?>> showDialog(Frame frame, final List<Port<?>> dmxChannels) {
        JPanel dialogPanel = createPanel(dmxChannels);

        final PortSelectionDialog dialog =
            new PortSelectionDialog(frame, Resources.getString(getClass(), "title"), dialogPanel);

        final ValueHolder selectedDmxPorts = new ValueHolder();

        applyButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                // get the selected ports
                LOGGER.info("selected ports: {}", selectionHolder);

                selectedDmxPorts.setValue(Collections.unmodifiableList(selectionHolder));

                dialog.close();
            }
        });
        cancelButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                // close the dialog
                dialog.close();
            }
        });

        dialog.showDialog();

        if (selectedDmxPorts.getValue() == null) {
            selectedDmxPorts.setValue(Collections.emptyList());
        }

        LOGGER.info("Selected DMX ports: {}", selectedDmxPorts.getValue());

        return (List<Port<?>>) selectedDmxPorts.getValue();
    }

    private static final class PortSelectionDialog extends EscapeDialog {
        private static final long serialVersionUID = 1L;

        public PortSelectionDialog(Frame frame, String title, JPanel dialogPanel) {
            super(frame, title, true);

            Container contentPane = getContentPane();
            contentPane.setLayout(new BorderLayout());
            contentPane.add(dialogPanel, BorderLayout.CENTER);
        }

        private void close() {
            setVisible(false);

            dispose();
        }

        public void showDialog() {

            pack();
            setLocationRelativeTo(null);
            setVisible(true);
        }
    }
}
