package org.bidib.wizard.mvc.main.view.component;

import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ListCellRenderer;
import javax.swing.event.ListSelectionListener;

public interface LabeledDisplayItems<T> {
    void addListSelectionListener(ListSelectionListener listener);

    void addMouseListener(MouseListener l);

    T getElementAt(int index);

    T getElementAt(int x, int y);

    int getIndex(Point point);

    int getItemSize();

    Point getLocationOnScreen();

    int getRowForLocation(int x, int y);

    int getSelectedIndex();

    T getSelectedItem();

    String getToolTipText(MouseEvent e);

    Point indexToLocation(int index);

    void refreshView();

    T selectElement(Point point);

    /**
     * @param selectionMode
     *            the selection mode to set
     */
    void setSelectionMode(int selectionMode);

    /**
     * Set the items that are displayed.
     * 
     * @param items
     *            the data items to set
     */
    void setItems(T[] items);

    void setCellRenderer(ListCellRenderer<? super T> cellRenderer);

    /**
     * Set the selected item.
     * 
     * @param item
     *            the new selected item
     */
    void setSelectedItem(T item);

    void selectedValueChanged(int index);
}
