package org.bidib.wizard.mvc.preferences.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.Collection;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.dialog.EscapeDialog;
import org.bidib.wizard.mvc.preferences.model.PreferencesModel;
import org.bidib.wizard.mvc.preferences.view.listener.PreferencesViewListener;
import org.bidib.wizard.mvc.preferences.view.panel.ConnectionPanel;
import org.bidib.wizard.mvc.preferences.view.panel.MiscPanel;
import org.bidib.wizard.mvc.preferences.view.panel.RailcomPlusPanel;
import org.bidib.wizard.mvc.preferences.view.panel.TimePanel;
import org.bidib.wizard.mvc.preferences.view.panel.WizardSettingsPanel;

import com.jgoodies.forms.builder.ButtonBarBuilder;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.debug.FormDebugPanel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.FormLayout;

public class PreferencesView extends EscapeDialog {
    private static final long serialVersionUID = -1L;

    private final Collection<PreferencesViewListener> listeners = new LinkedList<PreferencesViewListener>();

    private static final String ENCODED_DIALOG_COLUMN_SPECS = "pref, 3dlu, fill:50dlu:grow";

    private final ConnectionPanel connectionPanel;

    private final MiscPanel miscPanel;

    private final TimePanel timePanel;

    private final RailcomPlusPanel railcomPlusPanel;

    private final WizardSettingsPanel wizardSettingsPanel;

    private final JTabbedPane tabbedPane;

    public PreferencesView(JFrame parent, PreferencesModel model) {
        super(parent, Resources.getString(PreferencesView.class, "title"), false);

        DefaultFormBuilder dialogBuilder = null;
        boolean debugDialog = false;
        if (debugDialog) {
            JPanel panel = new FormDebugPanel();
            dialogBuilder = new DefaultFormBuilder(new FormLayout(ENCODED_DIALOG_COLUMN_SPECS), panel);
        }
        else {
            JPanel panel = new JPanel(new BorderLayout());
            dialogBuilder = new DefaultFormBuilder(new FormLayout(ENCODED_DIALOG_COLUMN_SPECS), panel);
        }
        dialogBuilder.border(Borders.DIALOG);

        // create the tabbed pane for the preferences panels
        tabbedPane = new JTabbedPane();

        connectionPanel = new ConnectionPanel(model);
        miscPanel = new MiscPanel(model);
        railcomPlusPanel = new RailcomPlusPanel(model);
        wizardSettingsPanel = new WizardSettingsPanel(model);

        timePanel = new TimePanel(model);

        // add tabs
        tabbedPane.addTab(Resources.getString(getClass(), "tab-connection"), null/* icon */,
            connectionPanel.createPanel(), Resources.getString(getClass(), "tab-connection.tooltip"));
        tabbedPane.setMnemonicAt(0, KeyEvent.VK_1);

        tabbedPane.addTab(Resources.getString(getClass(), "tab-misc"), null/* icon */, miscPanel.createPanel(),
            Resources.getString(getClass(), "tab-misc.tooltip"));
        tabbedPane.setMnemonicAt(1, KeyEvent.VK_2);

        tabbedPane.addTab(Resources.getString(getClass(), "tab-time"), null/* icon */, timePanel.createPanel(),
            Resources.getString(getClass(), "tab-time.tooltip"));
        tabbedPane.setMnemonicAt(2, KeyEvent.VK_3);

        tabbedPane.addTab(Resources.getString(getClass(), "tab-railcomplus"), null/* icon */,
            railcomPlusPanel.prepareComponent(), Resources.getString(getClass(), "tab-railcomplus.tooltip"));
        tabbedPane.setMnemonicAt(3, KeyEvent.VK_4);

        tabbedPane.addTab(Resources.getString(getClass(), "tab-wizardsettings"), null/* icon */,
            wizardSettingsPanel.createPanel(), Resources.getString(getClass(), "tab-wizardsettings.tooltip"));
        tabbedPane.setMnemonicAt(1, KeyEvent.VK_5);

        dialogBuilder.appendRow("3dlu");
        dialogBuilder.appendRow("fill:p");
        dialogBuilder.nextLine(2);
        dialogBuilder.append(tabbedPane, 3);
        dialogBuilder.nextLine();

        // prepare buttons
        JButton save = new JButton(Resources.getString(getClass(), "save"));
        save.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                fireSave();
            }
        });

        JButton cancel = new JButton(Resources.getString(getClass(), "cancel"));
        cancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                fireCancel();
            }
        });
        JPanel buttons = new ButtonBarBuilder().addGlue().addButton(save, cancel).border(Borders.DLU4).build();

        dialogBuilder.appendRow("3dlu");
        dialogBuilder.appendRow("p");
        dialogBuilder.nextLine(2);

        dialogBuilder.append(buttons, 3);

        dialogBuilder.appendRow("fill:3dlu:grow");

        JPanel contentPanel = dialogBuilder.build();

        getContentPane().add(contentPanel);

        pack();
        setLocationRelativeTo(parent);

        setMinimumSize(getSize());
        setVisible(true);
    }

    @Override
    public Dimension getPreferredSize() {

        Dimension dim = super.getPreferredSize();
        if (dim.width < 720) {
            dim.width = 720;
        }
        return dim;
    }

    public void addPreferencesViewListener(PreferencesViewListener l) {
        listeners.add(l);
        connectionPanel.addPreferencesViewListener(l);
        miscPanel.addPreferencesViewListener(l);
        timePanel.addPreferencesViewListener(l);
        railcomPlusPanel.addPreferencesViewListener(l);
        wizardSettingsPanel.addPreferencesViewListener(l);
    }

    private void fireSave() {
        for (PreferencesViewListener l : listeners) {
            l.save();
        }
    }

    private void fireCancel() {
        for (PreferencesViewListener l : listeners) {
            l.cancel();
        }
    }
}
