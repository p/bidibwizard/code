package org.bidib.wizard.mvc.main.view.cvdef;

import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.beans.Model;

public class BitfieldBean extends Model {
    private static final Logger LOGGER = LoggerFactory.getLogger(BitfieldBean.class);

    private static final long serialVersionUID = 1L;

    public final static String BIT_PROPERTY = "bit";

    public final static String BIT0_PROPERTY = "bit0";

    public final static String BIT1_PROPERTY = "bit1";

    public final static String BIT2_PROPERTY = "bit2";

    public final static String BIT3_PROPERTY = "bit3";

    public final static String BIT4_PROPERTY = "bit4";

    public final static String BIT5_PROPERTY = "bit5";

    public final static String BIT6_PROPERTY = "bit6";

    public final static String BIT7_PROPERTY = "bit7";

    private Byte bitfield = new Byte((byte) 0);

    private Integer radioBits = new Integer(0);

    public Byte getBitfield() {
        return bitfield;
    }

    public boolean getBit(int index) {
        index++;
        return (bitfield & index) == index;
    }

    public boolean getBit0() {
        return (bitfield & 0x01) == 0x01;
    }

    public boolean getBit1() {
        return (bitfield & 0x02) == 0x02;
    }

    public boolean getBit2() {
        return (bitfield & 0x04) == 0x04;
    }

    public boolean getBit3() {
        return (bitfield & 0x08) == 0x08;
    }

    public boolean getBit4() {
        return (bitfield & 0x10) == 0x10;
    }

    public boolean getBit5() {
        return (bitfield & 0x20) == 0x20;
    }

    public boolean getBit6() {
        return (bitfield & 0x40) == 0x40;
    }

    public boolean getBit7() {
        return (bitfield & 0x80) == 0x80;
    }

    public void setBit0(boolean bit) {
        setBit(bit, 0);
    }

    public void setBit1(boolean bit) {
        setBit(bit, 1);
    }

    public void setBit2(boolean bit) {
        setBit(bit, 2);
    }

    public void setBit3(boolean bit) {
        setBit(bit, 3);
    }

    public void setBit4(boolean bit) {
        setBit(bit, 4);
    }

    public void setBit5(boolean bit) {
        setBit(bit, 5);
    }

    public void setBit6(boolean bit) {
        setBit(bit, 6);
    }

    public void setBit7(boolean bit) {
        setBit(bit, 7);
    }

    private void setBit(boolean bit, int bitpos) {
        Integer newValue = null;
        if (bit) {

            // support for Radiobits, if the bit is a radio bit it must be set exclusively in its group
            if (radioBits != null) {
                if ((radioBits.byteValue() & (1 << bitpos)) == (1 << bitpos)) {

                    LOGGER.debug("The current bit is a radio bit: {}", bitpos);
                    int clearedRadioBitsValue = (bitfield & ~(radioBits.byteValue()));
                    // set the value
                    newValue = (1 << bitpos) | clearedRadioBitsValue;
                }
                else {
                    newValue = (bitfield | (1 << bitpos));
                }
            }
            else {
                newValue = (bitfield | (1 << bitpos));
            }
        }
        else {
            newValue = (bitfield & ~(1 << bitpos));
        }

        setBitfield(ByteUtils.getLowByte(newValue.intValue()));
    }

    public void setBitfield(Number bitfieldNum) {
        LOGGER.debug("Set the bitfield value: {}", (bitfieldNum != null ? (bitfieldNum.intValue() & 0xFF) : null));
        Byte newValue = null;
        if (bitfieldNum == null) {
            newValue = new Byte((byte) 0);
        }
        else {
            newValue = ByteUtils.getLowByte(bitfieldNum.intValue());
        }
        Byte oldValue = this.bitfield;
        this.bitfield = newValue;

        for (int index = 0; index < 8; index++) {
            byte pos = (byte) (0x01 << index);
            Boolean oldBitValue = (oldValue & pos) == pos;
            Boolean newBitValue = (bitfield & pos) == pos;

            firePropertyChange(BIT_PROPERTY + index, oldBitValue, newBitValue);
        }
    }

    /**
     * @return the radioBits
     */
    public Integer getRadioBits() {
        return radioBits;
    }

    /**
     * @param radioBits
     *            the radioBits to set
     */
    public void setRadioBits(Integer radioBits) {
        this.radioBits = radioBits;
    }
}