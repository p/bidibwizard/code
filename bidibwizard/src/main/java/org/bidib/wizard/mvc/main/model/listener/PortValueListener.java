package org.bidib.wizard.mvc.main.model.listener;

import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.mvc.main.model.Port;

/**
 * The PortValueListener signals changes of the port value from the node to the application. E.g. if the servo port has
 * changed it's position.
 */
public interface PortValueListener<T extends BidibStatus> {

    /**
     * Signal that the value of the port has changed.
     * 
     * @param port
     *            the port
     */
    void valueChanged(Port<T> port);
}
