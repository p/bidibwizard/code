package org.bidib.wizard.mvc.ping.model;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.bidib.jbidibc.core.Node;
import org.bidib.wizard.common.context.ApplicationContext;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.bidib.wizard.mvc.main.model.NodeLabels;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.beans.Model;
import com.jgoodies.common.collect.ArrayListModel;

public class PingTableModel extends Model {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(PingTableModel.class);

    public static final String PROPERTY_NODES = "nodes";

    private ArrayListModel<NodePingModel> nodeList = new ArrayListModel<>();

    private ApplicationContext applicationContext;

    private NodeLabels nodeLabels;

    public PingTableModel() {

        applicationContext = DefaultApplicationContext.getInstance();

        nodeLabels = applicationContext.get(DefaultApplicationContext.KEY_NODE_LABELS, NodeLabels.class);
        nodeLabels.addPropertyChangeListener(NodeLabels.PROPERTY_LABELS, new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                LOGGER.debug("NodeLabels have been changed.");

                List<NodePingModel> nodes = new LinkedList<>(getNodes());
                for (NodePingModel node : nodes) {
                    String nodeLabel = node.prepareNodeLabel();
                    node.setNodeLabel(nodeLabel);
                }

                if (nodeList.size() > 0) {
                    nodeList.fireContentsChanged(0, nodeList.size() - 1);
                }
            }
        });
    }

    public void addNode(Node node) {
        synchronized (nodeList) {
            NodePingModel nodePingModel = new NodePingModel(node, nodeLabels);
            if (!nodeList.contains(nodePingModel)) {
                LOGGER.info("Add node to ping node list: {}", node);
                nodePingModel.registerNode();

                // set the default interval to 2s
                nodePingModel.setPingInterval(2);
                nodePingModel.setLastPing(System.currentTimeMillis());
                nodePingModel.setNodePingState(NodePingState.OFF);

                if (nodeLabels != null) {
                    String nodeLabel = nodePingModel.prepareNodeLabel();
                    nodePingModel.setNodeLabel(nodeLabel);
                }

                List<NodePingModel> oldValue = new LinkedList<>(nodeList);
                nodeList.add(nodePingModel);

                firePropertyChange(PROPERTY_NODES, oldValue, nodeList);
            }
            else {
                LOGGER.warn("Node is already in ping node list: {}", node);
            }
        }
    }

    public void removeNode(Node node) {
        synchronized (nodeList) {
            LOGGER.info("Remove node from ping node list: {}", node);

            List<NodePingModel> oldValue = new LinkedList<>(nodeList);
            int index = nodeList.indexOf(new NodePingModel(node, null));
            if (index > -1) {
                NodePingModel removed = nodeList.remove(index);
                LOGGER.info("Removed node: {}", removed);
                if (removed != null) {
                    removed.freeNode();
                }

                firePropertyChange(PROPERTY_NODES, oldValue, nodeList);
            }
        }
    }

    public ArrayListModel<NodePingModel> getNodeListModel() {
        return nodeList;
    }

    public List<NodePingModel> getNodes() {
        return Collections.unmodifiableList(nodeList);
    }

    public void setNodePingState(Node node, NodePingState nodePingState) {
        synchronized (nodeList) {
            int index = nodeList.indexOf(new NodePingModel(node, null));
            if (index > -1) {
                NodePingModel nodePingModel = nodeList.get(index);
                if (nodePingModel != null) {
                    nodePingModel.setNodePingState(nodePingState);
                }
            }
        }
    }

    public void setPongMarker(byte[] address, int marker) {
        // TODO implement
    }
}
