package org.bidib.wizard.mvc.main.view.table;

import java.awt.Component;
import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;

public class NumberWithLabelRenderer implements TableCellRenderer {

    private final JPanel panel = new JPanel();

    private final JTextField textField = new JTextField(3);

    private final JLabel leftLabel;

    private final JLabel rightLabel;

    public NumberWithLabelRenderer(String leftText, String rightText) {
        textField.setHorizontalAlignment(SwingConstants.RIGHT);

        Dimension currentDim = textField.getPreferredSize();
        textField.setPreferredSize(new Dimension(30, currentDim.height));
        textField.setMinimumSize(new Dimension(30, currentDim.height));

        leftLabel = new JLabel(leftText);
        rightLabel = new JLabel(rightText);
        panel.setFocusCycleRoot(true);

        panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));

        panel.add(leftLabel);
        panel.add(textField);
        panel.add(rightLabel);

    }

    public Component getTableCellRendererComponent(
        JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        if (isSelected) {
            panel.setForeground(hasFocus ? table.getSelectionForeground()/* .brighter() */: table
                .getSelectionForeground());
            panel.setBackground(hasFocus ? table.getSelectionBackground()/* .brighter() */: table
                .getSelectionBackground());
        }
        else {
            panel.setForeground(table.getForeground());
            panel.setBackground(table.getBackground());
        }

        TableModel model = table.getModel();
        boolean editable = model.isCellEditable(row, column);
        textField.setEnabled(editable);

        textField.setText(value != null ? value.toString() : "");
        return panel;
    }
}
