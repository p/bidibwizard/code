package org.bidib.wizard.mvc.main.view.table;

import java.awt.Dimension;
import java.text.DecimalFormat;

import javax.swing.SwingConstants;

import org.bidib.wizard.mvc.main.model.ServoPort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServoSliderEditor extends SliderEditor {
    private static final Logger LOGGER = LoggerFactory.getLogger(ServoSliderEditor.class);

    private static final long serialVersionUID = 1L;

    private final DecimalFormat format = new DecimalFormat("##0");

    private int rowNumber;

    private ServoPort port;

    public ServoSliderEditor(ServoPort servoPort, int minValue, int maxValue) {
        super(minValue, maxValue, null);
        this.port = servoPort;
    }

    public void setRowNumber(int rowNumber) {
        this.rowNumber = rowNumber;
    }

    public int getRowNumber() {
        return rowNumber;
    }

    @Override
    protected void prepareSliderTextLabel(int maxValue, int initialValue) {
        LOGGER.trace("Prepare slider text value for port: {}", port);
        sliderValue.setHorizontalAlignment(SwingConstants.LEADING);

        sliderValue.setText(Integer.toString(maxValue));

        Dimension d = sliderValue.getMinimumSize();
        updateSliderTextValue(initialValue, false);

        d.setSize(d.getWidth() * 2 + 30, d.getHeight());
        LOGGER.debug("Current dimension: {}", d);

        sliderValue.setPreferredSize(d);
        sliderValue.setMaximumSize(d);
        sliderValue.setMinimumSize(d);

        // the slider must not be opaque
        slider.setOpaque(false);

    }

    @Override
    protected void updateSliderTextValue(int value, boolean isSelected) {
        LOGGER.trace("updateSliderTextValue, value: {}, isSelected: {}", value, isSelected);

        if (port == null) {
            return;
        }
        String newValue = format.format(value);
        LOGGER.trace("Set the new value: {}", newValue);

        int range = port.getTrimUp() - port.getTrimDown();
        float factor = (float) range / 100;
        float targetValue = (factor * value) + port.getTrimDown();
        LOGGER.trace("Calculated range: {}, factor: {}, targetValue: {}", range, factor, targetValue);
        int targetAbsolute = Math.round(targetValue);

        StringBuffer sb = new StringBuffer("<html>");
        sb.append(newValue).append("% <FONT COLOR=");
        if (isSelected) {
            sb.append("WHITE");
        }
        else {
            sb.append("GRAY");
        }
        sb.append(">(").append(targetAbsolute).append(")</FONT></html>");

        sliderValue.setText(sb.toString());
    }
}