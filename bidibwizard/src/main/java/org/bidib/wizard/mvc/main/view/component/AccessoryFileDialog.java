package org.bidib.wizard.mvc.main.view.component;

import java.awt.Component;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.dialog.FileDialog;
import org.bidib.wizard.mvc.main.model.Accessory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AccessoryFileDialog extends FileDialog {
    private static final Logger LOGGER = LoggerFactory.getLogger(AccessoryFileDialog.class);

    private final String ACCESSORY_EXTENSION_LEGACY = "accessory";

    private final String ACCESSORY_EXTENSION = "axml";

    private JCheckBox checkUseLegacyFormat;

    private FileFilter accessoryFilter;

    private FileFilter accessoryFilterLegacy;

    private String accessoryDescription;

    private String accessoryDescriptionLegacy;

    private Accessory accessory;

    private final boolean legacyFormatDefault;

    public AccessoryFileDialog(Component parent, int dialogType, final Accessory accessory,
        boolean legacyFormatDefault) {
        super(parent, dialogType, null, (FileFilter[]) null);
        this.legacyFormatDefault = legacyFormatDefault;
        this.accessory = accessory;

        accessoryDescription = Resources.getString(AccessoryFileDialog.class, "accessoryDescription");
        accessoryDescriptionLegacy = Resources.getString(AccessoryFileDialog.class, "accessoryDescriptionLegacy");
        accessoryFilter = new FileNameExtensionFilter(accessoryDescription, ACCESSORY_EXTENSION);
        accessoryFilterLegacy = new FileNameExtensionFilter(accessoryDescriptionLegacy, ACCESSORY_EXTENSION_LEGACY);

        String fileName = null;
        if (accessory != null) {
            if (legacyFormatDefault) {
                fileName = accessory.toString() + "." + ACCESSORY_EXTENSION_LEGACY;
                LOGGER.info("Update the file filter for legacy, fileName: {}", fileName);
            }
            else {
                fileName = accessory.toString() + "." + ACCESSORY_EXTENSION;
                LOGGER.info("Update the file filter, fileName: {}", fileName);
            }
        }
        if (legacyFormatDefault) {
            updateFileFilter(accessoryFilterLegacy, fileName);
        }
        else {
            updateFileFilter(accessoryFilter, fileName);
        }
    }

    protected JCheckBox getCheckUseLegacyFormat() {
        return checkUseLegacyFormat;
    }

    @Override
    protected Component getAdditionalPanel() {
        // prepare a panel with checkboxes for loading macro content before export
        JPanel additionalPanel = new JPanel();
        additionalPanel.setLayout(new BoxLayout(additionalPanel, BoxLayout.PAGE_AXIS));

        // do not allow to save old format
        if (FileDialog.SAVE != dialogType) {

            final FileDialog fileDialog = this;
            checkUseLegacyFormat =
                new JCheckBox(Resources.getString(AccessoryFileDialog.class, "checkUseLegacyFormat"),
                    legacyFormatDefault);
            checkUseLegacyFormat.addItemListener(new ItemListener() {

                @Override
                public void itemStateChanged(ItemEvent e) {
                    String fileName = null;
                    FileFilter accessoryFileFilter = null;
                    switch (e.getStateChange()) {
                        case ItemEvent.SELECTED:
                            LOGGER.info("checkUseLegacyFormat is selected.");
                            accessoryFileFilter = accessoryFilterLegacy;
                            if (accessory != null) {
                                fileName = accessory.toString() + "." + ACCESSORY_EXTENSION_LEGACY;
                            }
                            break;
                        default:
                            LOGGER.info("checkUseLegacyFormat is unselected.");
                            accessoryFileFilter = accessoryFilter;
                            if (accessory != null) {
                                fileName = accessory.toString() + "." + ACCESSORY_EXTENSION;
                            }
                            break;
                    }
                    fileDialog.updateFileFilter(accessoryFileFilter, fileName);
                }
            });
            additionalPanel.add(checkUseLegacyFormat);
        }
        else {
            LOGGER.info("Save accessory in old format is disabled.");
        }
        return additionalPanel;
    }

}
