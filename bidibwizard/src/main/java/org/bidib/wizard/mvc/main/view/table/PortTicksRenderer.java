package org.bidib.wizard.mvc.main.view.table;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import org.bidib.wizard.mvc.main.model.Port;
import org.bidib.wizard.mvc.main.model.TicksAware;

public class PortTicksRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = 1L;

    public PortTicksRenderer() {
    }

    @Override
    public Component getTableCellRendererComponent(
        JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

        // call super to set the correct color if selected
        super.getTableCellRendererComponent(table, null, isSelected, hasFocus, row, column);

        // renderer only handles ports and only show a value if the port is TicksAware
        if (value instanceof Port<?>) {
            Port<?> switchPort = (Port<?>) value;
            boolean enabled = switchPort.isEnabled();
            setEnabled(enabled);
            setIcon(null);
            if (value instanceof TicksAware) {
                setText(Integer.toString(((TicksAware) value).getTicks()));
            }
        }
        else {
            setEnabled(false);
            setIcon(null);
            setText(null);
        }

        return this;
    }

}
