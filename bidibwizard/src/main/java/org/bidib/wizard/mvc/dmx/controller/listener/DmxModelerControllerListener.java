package org.bidib.wizard.mvc.dmx.controller.listener;

import org.bidib.wizard.mvc.dmx.model.DmxScenery;
import org.bidib.wizard.mvc.dmx.model.DmxSceneryModel;
import org.bidib.wizard.mvc.main.model.Port;

public interface DmxModelerControllerListener {

    /**
     * @param dmxScenery
     *            the DMX scenery to open
     */
    void openView(final DmxScenery dmxScenery);

    /**
     * @param dmxSceneryModel
     *            the view associated with the provided DMX scenery to close
     */
    void closeView(final DmxSceneryModel dmxSceneryModel);

    /**
     * The config of the port has changed.
     * 
     * @param port
     *            the port
     */
    void portConfigChanged(Port<?> port);
}
