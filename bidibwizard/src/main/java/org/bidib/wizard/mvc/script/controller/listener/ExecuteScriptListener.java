package org.bidib.wizard.mvc.script.controller.listener;

public interface ExecuteScriptListener {

    /**
     * Execute the script.
     * 
     * @param script
     *            the script
     * @param previewOnly
     *            {@code true} : preview only, don't apply; {@code false} : apply on node
     */
    void executeScript(String script, boolean previewOnly);
}
