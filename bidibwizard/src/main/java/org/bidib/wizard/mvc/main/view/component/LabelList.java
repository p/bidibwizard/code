package org.bidib.wizard.mvc.main.view.component;

import java.awt.Point;
import java.awt.event.MouseEvent;
import java.io.Serializable;

import javax.swing.ListCellRenderer;
import javax.swing.ListModel;

import org.apache.commons.lang.StringUtils;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.common.view.list.DefaultMutableListModel;
import org.bidib.wizard.mvc.common.view.list.JListMutable;
import org.bidib.wizard.mvc.main.model.LabelAware;
import org.bidib.wizard.mvc.main.model.TooltipAware;

public class LabelList<T extends Serializable> extends JListMutable<T> implements LabeledDisplayItems<T> {

    public LabelList(ListModel dataModel) {
        super(dataModel);
    }

    private static final long serialVersionUID = 1L;

    @Override
    public T getElementAt(int index) {
        return (T) getModel().getElementAt(index);
    }

    @Override
    public T getElementAt(int x, int y) {
        return getElementAt(getRowForLocation(x, y));
    }

    @Override
    public int getIndex(Point point) {
        return locationToIndex(point);
    }

    @Override
    public int getItemSize() {
        return getModel().getSize();
    }

    @Override
    public int getRowForLocation(int x, int y) {
        return getIndex(new Point(x, y));
    }

    @Override
    public T getSelectedItem() {
        return (T) getSelectedValue();
    }

    @Override
    public String getToolTipText(MouseEvent e) {
        String result = null;
        Point p = e.getPoint();
        int row = getIndex(p);

        if (row >= 0 && row < getModel().getSize()) {
            Object object = getModel().getElementAt(row);

            if (object != null) {
                String label = null;

                if (object instanceof TooltipAware) {
                    String tooltip = ((TooltipAware) object).getTooltip();
                    if (tooltip != null) {
                        if (StringUtils.isNotBlank(result)) {
                            result += " : ";
                            result += tooltip;
                        }
                        else {
                            result = tooltip;
                        }
                    }
                }
                else if (object instanceof LabelAware) {
                    label = ((LabelAware) object).getLabel();
                    if (StringUtils.isBlank(label)) {
                        // no label assigned, prepare tootip
                        result = Resources.getString(LabelList.class, "chooseLabel");
                    }
                }
            }
        }
        return result;
    }

    @Override
    public void refreshView() {
        if (getParent() != null) {
            getParent().invalidate();

            getParent().repaint();
        }
    }

    @Override
    public T selectElement(Point point) {
        setSelectedIndex(getIndex(point));
        T selectedItem = getSelectedItem();
        grabFocus();

        return selectedItem;
    }

    /**
     * @param items
     *            the data items to set
     */
    @Override
    public void setItems(T[] items) {
        ListModel<?> listModel = getModel();
        if (listModel instanceof DefaultMutableListModel) {
            // DefaultMutableListModel model = new DefaultMutableListModel();
            DefaultMutableListModel<T> mutableListModel = (DefaultMutableListModel<T>) listModel;
            mutableListModel.removeAllElements();
            for (T item : items) {
                mutableListModel.addElement(item);
            }
            // setModel(model);
        }
        else {
            setListData(items);
        }
    }

    @Override
    public void setCellRenderer(ListCellRenderer cellRenderer) {
        super.setCellRenderer(cellRenderer);
    }

    @Override
    public void setSelectionMode(int selectionMode) {
        super.setSelectionMode(selectionMode);
    }

    @Override
    public String toString() {
        return getClass().getName();
    }

    @Override
    public void setSelectedItem(T item) {
        super.setSelectedValue(item, true);
    }

    @Override
    public void selectedValueChanged(int index) {
        setSelectedIndex(index);
    }
}
