package org.bidib.wizard.mvc.main.model.listener;

import org.bidib.wizard.mvc.main.model.Node;

public interface NodeListListener {
    /**
     * The content of the node list has changed.
     */
    void listChanged();

    /**
     * The selected node in the list will change.
     */
    void nodeWillChange();

    /**
     * The selected node in the list has changed.
     */
    void nodeChanged();

    /**
     * The identify mode of the node has changed.
     */
    void nodeStateChanged();

    /**
     * A new node was added
     * 
     * @param node
     *            the new node
     */
    void listNodeAdded(final Node node);

    /**
     * A node was removed
     * 
     * @param node
     *            the removed node
     */
    void listNodeRemoved(final Node node);
}
