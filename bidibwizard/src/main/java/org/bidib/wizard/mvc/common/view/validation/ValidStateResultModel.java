package org.bidib.wizard.mvc.common.view.validation;

import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.util.DefaultValidationResultModel;

public class ValidStateResultModel extends DefaultValidationResultModel {
    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_VALID_STATE = "validState";

    public static final String PROPERTY_VALID_STATE_NO_WARN_OR_ERRORS = "validStateNoWarnOrErrors";

    private boolean validState;

    private boolean validStateNoWarnOrErrors;

    @Override
    public void setResult(ValidationResult newResult) {
        boolean oldValidState = getResult().isEmpty();
        boolean newValidState = newResult.isEmpty();

        boolean oldValidStateNoWarnErrors = !(getResult().hasWarnings() || getResult().hasErrors());
        boolean newValidStateNoWarnErrors = !(newResult.hasWarnings() || newResult.hasErrors());

        super.setResult(newResult);

        validState = newValidState;
        validStateNoWarnOrErrors = newValidStateNoWarnErrors;

        firePropertyChange(PROPERTY_VALID_STATE, oldValidState, newValidState);
        firePropertyChange(PROPERTY_VALID_STATE_NO_WARN_OR_ERRORS, oldValidStateNoWarnErrors, newValidStateNoWarnErrors);
    }

    public boolean getValidState() {
        return validState;
    }

    public boolean getValidStateNoWarnOrErrors() {
        return validStateNoWarnOrErrors;
    }
}
