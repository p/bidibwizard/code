package org.bidib.wizard.mvc.main.view.cvdef;

import java.text.DecimalFormat;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import org.apache.commons.lang.StringUtils;
import org.bidib.jbidibc.exchange.vendorcv.ModeType;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.common.utils.ImageUtils;
import org.bidib.wizard.mvc.common.view.converter.SignedCharStringConverter;
import org.bidib.wizard.mvc.common.view.cvdefinition.CvDefinitionTreeTableModel;
import org.bidib.wizard.mvc.common.view.validation.PropertyValidationI18NSupport;
import org.bidib.wizard.mvc.main.view.panel.NodeTree;
import org.bidib.wizard.utils.InputValidationDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.value.ConverterValueModel;
import com.jgoodies.binding.value.ValueHolder;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.util.PropertyValidationSupport;
import com.jgoodies.validation.view.ValidationComponentUtils;

public class CvSignedCharValueEditor extends CvValueNumberEditor<Byte> {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(CvSignedCharValueEditor.class);

    private int minValue = -128;

    private int maxValue = 127;

    private ValueModel writeEnabled;

    private String[] allowedValues;

    @Override
    protected ValidationResult validateModel(
        PresentationModel<CvValueBean<Byte>> model, CvValueBean<Byte> cvValueBean) {
        PropertyValidationSupport support = new PropertyValidationI18NSupport(cvValueBean, "validation");

        LOGGER.info("Validate the model, cvValueModel.buffering: {}", cvValueModel.isBuffering());
        if (Boolean.TRUE.equals(writeEnabled.getValue())) {

            if (allowedValues != null) {
                // only allowed values are checked
                Object current = valueConverterModel.getValue();

                for (String allowed : allowedValues) {
                    if (allowed.equals(current)) {
                        return support.getResult();
                    }
                }
                support.addError("cvvalue_key", "invalid_value");
            }
            else {
                Object val = model.getBufferedModel("cvValue").getValue();
                if (val instanceof Number) {
                    int value = ((Number) val).intValue();
                    // byte value = ((Number) val).byteValue();
                    if (value < minValue || value > maxValue) {
                        support.addError("cvvalue_key", "invalid_value;min=" + minValue + ",max=" + maxValue);
                    }
                }
                else if (val == null) {
                    support.addError("cvvalue_key", "not_empty;min=" + minValue + ",max=" + maxValue);
                }
            }
        }
        else {
            LOGGER.debug("Validation is disabled!");
        }
        return support.getResult();
    }

    protected DefaultFormBuilder prepareFormPanel() {

        writeEnabled = new ValueHolder(false);

        // Get buffered model objects.
        cvValueModel = cvAdapter.getBufferedModel("cvValue");
        valueConverterModel =
            new ConverterValueModel(cvValueModel, new SignedCharStringConverter(new DecimalFormat("#")));

        DefaultFormBuilder formBuilder =
            new DefaultFormBuilder(new FormLayout("3dlu, max(25dlu;pref), 3dlu, max(30dlu;pref), 0dlu:grow"));
        JTextField textCvValue = BasicComponentFactory.createTextField(valueConverterModel, false);
        textCvValue.setDocument(new InputValidationDocument(InputValidationDocument.SIGNED_NUMERIC));
        textCvValue.setEditable(false);

        // the needs reboot Icon is invisible by default
        ImageIcon warnIcon = ImageUtils.createImageIcon(NodeTree.class, "/icons/warn.png");
        JLabel needsRebootIcon =
            new JLabel(Resources.getString(getClass(), "rebootrequired"), warnIcon, SwingConstants.LEADING);
        needsRebootIcon.setVisible(false);

        // init the components
        ValidationComponentUtils.setMandatory(textCvValue, true);
        ValidationComponentUtils.setMessageKey(textCvValue, "validation.cvvalue_key");
        formBuilder.leadingColumnOffset(1);
        formBuilder.nextLine(0);
        formBuilder.append(BasicComponentFactory.createLabel(cvNumberModel, new CvNodeMessageFormat("{0} (CV{1})")), 4);
        formBuilder.nextLine();
        formBuilder.append(textCvValue, 1);
        formBuilder.append(needsRebootIcon);

        // add bindings for enable/disable the textfield
        PropertyConnector.connect(writeEnabled, "value", textCvValue, "editable");
        PropertyConnector.connect(needsRebootModel, "value", needsRebootIcon, "visible");

        return formBuilder;
    }

    @Override
    public void setValue(CvNode cvNode, Map<String, CvNode> cvNumberToNodeMap) {

        boolean timeout = cvNode.getConfigVar().isTimeout();

        // set textfield editable before the value is set because the validation is triggered
        writeEnabled.setValue(!(ModeType.RO.equals(cvNode.getCV().getMode()) || timeout));

        // set the value ... triggers validation
        super.setValue(cvNode, cvNumberToNodeMap);

        // check if 'values' is defined
        String values = cvValueBean.getCvNode().getCV().getValues();
        if (StringUtils.isNotBlank(values) && !("-".equals(values))) {
            // we have allowed values defined
            allowedValues = values.split(";");
        }
        else {
            allowedValues = null;
        }

        String min = cvValueBean.getCvNode().getCV().getMin();
        if (min != null) {
            try {
                minValue = Integer.parseInt(min);
            }
            catch (NumberFormatException ex) {
                LOGGER.debug("Parse min value failed: {}", min);
                minValue = -128;
            }
        }
        else {
            minValue = -128;
        }
        String max = cvValueBean.getCvNode().getCV().getMax();
        if (max != null) {
            try {
                maxValue = Integer.parseInt(max);
            }
            catch (NumberFormatException ex) {
                LOGGER.debug("Parse max value failed: {}", max);
                maxValue = 127;
            }
        }
        else {
            maxValue = 127;
        }

        triggerValidation(null);
    }

    @Override
    public void updateCvValues(Map<String, CvNode> cvNumberToNodeMap, CvDefinitionTreeTableModel treeModel) {
        CvNode cvNode = (CvNode) cvNumberModel.getValue();
        cvNode.setValueAt(cvValueBean.getCvValue(), CvNode.COLUMN_NEW_VALUE);

        // treeModel.refreshTreeTable(cvNode);
    }
}
