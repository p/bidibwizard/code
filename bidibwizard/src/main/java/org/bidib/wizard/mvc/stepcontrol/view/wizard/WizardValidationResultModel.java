package org.bidib.wizard.mvc.stepcontrol.view.wizard;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.util.DefaultValidationResultModel;

public class WizardValidationResultModel extends DefaultValidationResultModel {
    private static final Logger LOGGER = LoggerFactory.getLogger(WizardValidationResultModel.class);

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_VALID_STATE = "validState";

    public static final String PROPERTY_VALID_STATE_NO_WARN_OR_ERRORS = "validStateNoWarnOrErrors";

    private boolean validState;

    private boolean validStateNoWarnOrErrors;

    @Override
    public void setResult(ValidationResult newResult) {
        boolean oldValidState = getResult().isEmpty();
        boolean newValidState = newResult.isEmpty();

        boolean oldValidStateNoWarnErrors = !(getResult().hasWarnings() || getResult().hasErrors());
        boolean newValidStateNoWarnErrors = !(newResult.hasWarnings() || newResult.hasErrors());

        super.setResult(newResult);

        validState = newValidState;
        validStateNoWarnOrErrors = newValidStateNoWarnErrors;

        LOGGER.info("Current validState: {}, validStateNoWarnOrErrors: {}", validState, validStateNoWarnOrErrors);

        firePropertyChange(PROPERTY_VALID_STATE, oldValidState, newValidState);
        firePropertyChange(PROPERTY_VALID_STATE_NO_WARN_OR_ERRORS, oldValidStateNoWarnErrors, newValidStateNoWarnErrors);
    }

    public void setValidState(boolean valid) {
        if (valid && !validStateNoWarnOrErrors) {
            LOGGER.warn("Could not set the valid state because the validStateNoWarnOrErrors is set to false!");
            return;
        }
        boolean oldValidState = validState;
        validState = valid;
        firePropertyChange(PROPERTY_VALID_STATE, oldValidState, validState);
    }

    public boolean getValidState() {
        return validState;
    }

    public boolean getValidStateNoWarnOrErrors() {
        return validStateNoWarnOrErrors;
    }
};
