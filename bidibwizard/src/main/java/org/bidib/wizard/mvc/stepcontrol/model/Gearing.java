package org.bidib.wizard.mvc.stepcontrol.model;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.jgoodies.binding.beans.Model;

public class Gearing extends Model {

    private static final long serialVersionUID = 1L;

    public static final String YES = "yes";

    public static final String NO = "no";

    public static final String PROPERTYNAME_GEARRATIO_PRIMARY = "gearRatioPrimary";

    public static final String PROPERTYNAME_GEARRATIO_SECONDARY = "gearRatioSecondary";

    public static final String PROPERTYNAME_BACKLASH = "backLash";

    private final String key;

    // the value 0 in primary ration signals no gearing in the tools storage area of the step control
    private Integer gearRatioPrimary = 0;

    private Integer gearRatioSecondary = 1;

    private Integer backLash = 0;

    public Gearing(String key) {
        this.key = key;
    }

    /**
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * @return the gearRatioPrimary value
     */
    public Integer getGearRatioPrimary() {
        return gearRatioPrimary;
    }

    /**
     * @param gearRatioPrimary
     *            the gearRatioPrimary value to set
     */
    public void setGearRatioPrimary(Integer gearRatioPrimary) {
        Integer oldValue = this.gearRatioPrimary;

        this.gearRatioPrimary = gearRatioPrimary;

        firePropertyChange(PROPERTYNAME_GEARRATIO_PRIMARY, oldValue, gearRatioPrimary);
    }

    /**
     * @return the gearRatioSecondary value
     */
    public Integer getGearRatioSecondary() {
        return gearRatioSecondary;
    }

    /**
     * @param gearRatioSecondary
     *            the gearRatioSecondary value to set
     */
    public void setGearRatioSecondary(Integer gearRatioSecondary) {
        Integer oldValue = this.gearRatioSecondary;

        this.gearRatioSecondary = gearRatioSecondary;

        firePropertyChange(PROPERTYNAME_GEARRATIO_SECONDARY, oldValue, gearRatioSecondary);
    }

    /**
     * @return the backLash
     */
    public Integer getBackLash() {
        return backLash;
    }

    /**
     * @param backLash
     *            the backLash to set
     */
    public void setBackLash(Integer backLash) {
        Integer oldValue = this.backLash;

        this.backLash = backLash;

        firePropertyChange(PROPERTYNAME_BACKLASH, oldValue, backLash);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Gearing) {
            return key.equals(((Gearing) obj).key);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return key.hashCode();
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
