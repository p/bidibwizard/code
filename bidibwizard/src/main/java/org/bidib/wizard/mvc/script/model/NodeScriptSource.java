package org.bidib.wizard.mvc.script.model;

import java.util.Objects;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class NodeScriptSource {

    public enum ScriptSource {
        user, installation, network;
    }

    private final ScriptSource scriptSource;

    private final String filePath;

    public NodeScriptSource(ScriptSource scriptSource, String filePath) {
        this.scriptSource = scriptSource;
        this.filePath = filePath;
    }

    /**
     * @return the scriptSource
     */
    public ScriptSource getScriptSource() {
        return scriptSource;
    }

    /**
     * @return the filePath
     */
    public String getFilePath() {
        return filePath;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof NodeScriptSource) {
            NodeScriptSource other = (NodeScriptSource) obj;
            if (!Objects.equals(scriptSource, other.scriptSource)) {
                return false;
            }
            if (!Objects.equals(filePath, other.filePath)) {
                return false;
            }
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = super.hashCode();
        if (filePath != null) {
            hash += filePath.hashCode();
        }
        if (scriptSource != null) {
            hash += scriptSource.hashCode();
        }
        return hash;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
