package org.bidib.wizard.mvc.main.model.function;

import org.bidib.jbidibc.exchange.lcmacro.LcMacroPointType;
import org.bidib.jbidibc.exchange.lcmacro.SoundActionType;
import org.bidib.jbidibc.exchange.lcmacro.SoundPortActionType;
import org.bidib.jbidibc.exchange.lcmacro.SoundPortPoint;
import org.bidib.wizard.comm.SoundPortStatus;
import org.bidib.wizard.mvc.main.model.SoundPort;

public class SoundPortAction extends SimplePortAction<SoundPortStatus, SoundPort> {
    public SoundPortAction() {
        this(SoundPortStatus.PLAY);
    }

    public SoundPortAction(SoundPortStatus action) {
        this(action, null, 0, 0);
    }

    public SoundPortAction(SoundPortStatus action, SoundPort port, int delay, int value) {
        super(action, KEY_SOUND, port, delay, value);
    }

    public String getDebugString() {
        int id = 0;

        if (getPort() != null) {
            id = getPort().getId();
        }
        return "@" + getDelay() + " Sound:" + String.format("%02d", id) + "->" + getValue();
    }

    @Override
    public LcMacroPointType toLcMacroPoint() {
        SoundPortPoint soundPortPoint = new SoundPortPoint();
        soundPortPoint.setDelay(getDelay());
        soundPortPoint.setOutputNumber(getPort().getId());
        SoundPortActionType soundPortActionType = new SoundPortActionType();
        soundPortActionType.setAction(SoundActionType.fromValue(getAction().name()));
        soundPortActionType.setValue(getValue());
        soundPortPoint.setSoundPortActionType(soundPortActionType);
        return soundPortPoint;
    }
}
