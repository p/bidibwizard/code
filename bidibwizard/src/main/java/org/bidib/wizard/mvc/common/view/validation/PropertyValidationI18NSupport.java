package org.bidib.wizard.mvc.common.view.validation;

import org.bidib.wizard.common.locale.Resources;

import com.jgoodies.validation.Severity;
import com.jgoodies.validation.message.PropertyValidationMessage;
import com.jgoodies.validation.util.PropertyValidationSupport;

public class PropertyValidationI18NSupport extends PropertyValidationSupport {

    private Object target;

    private String role;

    public PropertyValidationI18NSupport(Object target, String role) {
        super(target, role);
        this.target = target;
        this.role = role;
    }

    public PropertyValidationMessage create(Severity severity, String property, String text) {
        String[] splitedText = text.split(";");
        if (splitedText != null && splitedText.length > 0) {
            text = splitedText[0];
        }
        String res = Resources.getString((Class<?>) null, text);
        if (res != null) {
            text = res;
        }

        if (splitedText.length > 1) {
            StringBuffer temp = new StringBuffer(text);
            temp.append(" (").append(splitedText[1]).append(")");
            text = temp.toString();
        }

        return new PropertyValidationMessage(severity, text, target, role, property) {
            private static final long serialVersionUID = 1L;

            @Override
            public String formattedText() {
                String aspectI18N = Resources.getString((Class<?>) null, aspect());
                return aspectI18N + " " + text();
            }
        };
    }
}
