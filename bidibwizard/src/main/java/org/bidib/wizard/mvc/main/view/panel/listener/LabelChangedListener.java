package org.bidib.wizard.mvc.main.view.panel.listener;

public interface LabelChangedListener<T> {
    /**
     * The label of the object has changed.
     * 
     * @param object
     *            the instance
     * @param label
     *            the new label
     */
    void labelChanged(final T object, String label);
}
