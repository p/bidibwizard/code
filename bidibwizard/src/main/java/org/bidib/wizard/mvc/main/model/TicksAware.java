package org.bidib.wizard.mvc.main.model;

public interface TicksAware {
    /**
     * @return the number of ticks configured for this port
     */
    int getTicks();
}
