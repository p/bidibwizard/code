package org.bidib.wizard.mvc.main.view.cvdef;

import java.util.Map;

import org.bidib.wizard.mvc.common.view.cvdefinition.CvDefinitionTreeTableModel;

import com.jgoodies.binding.value.Trigger;

public interface CvValueEditor {

    /**
     * Set the value from cvNode in the editor.
     * 
     * @param cvNode
     *            the cvNode
     * @param cvNumberToNodeMap
     *            the map with all CV numbers assigned to their nodes
     */
    void setValue(CvNode cvNode, Map<String, CvNode> cvNumberToNodeMap);

    /**
     * Update the cv values in the provided nodes.
     * 
     * @param cvNumberToNodeMap
     *            the map with all CV numbers assigned to their nodes
     * @param treeModel
     *            the treeModel that must be refreshed
     */
    void updateCvValues(Map<String, CvNode> cvNumberToNodeMap, CvDefinitionTreeTableModel treeModel);

    /**
     * Refresh the displayed value from the node.
     */
    void refreshValue();

    /**
     * Refresh the displayed value from the source cv definition.
     */
    void refreshValueFromSource();

    /**
     * @return the trigger
     */
    Trigger getTrigger();

    /**
     * @return the cvNode instance
     */
    CvNode getCvNode();
}
