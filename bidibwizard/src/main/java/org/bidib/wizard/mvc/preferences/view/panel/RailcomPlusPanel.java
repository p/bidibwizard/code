package org.bidib.wizard.mvc.preferences.view.panel;

import java.awt.BorderLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JCheckBox;
import javax.swing.JPanel;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.bidib.jbidibc.core.schema.DecoderVendorFactory;
import org.bidib.jbidibc.core.schema.decodervendor.VendorType;
import org.bidib.wizard.mvc.preferences.model.PreferencesModel;
import org.bidib.wizard.mvc.preferences.view.listener.PreferencesViewListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.debug.FormDebugPanel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.FormLayout;

public class RailcomPlusPanel {
    private static final Logger LOGGER = LoggerFactory.getLogger(RailcomPlusPanel.class);

    private static final String ENCODED_DIALOG_COLUMN_SPECS = "pref, 3dlu, fill:50dlu:grow";

    private final Collection<PreferencesViewListener> listeners = new LinkedList<PreferencesViewListener>();

    private final PreferencesModel preferencesModel;

    public RailcomPlusPanel(PreferencesModel preferencesModel) {
        this.preferencesModel = preferencesModel;
    }

    public JPanel prepareComponent() {

        DefaultFormBuilder dialogBuilder = null;
        boolean debugDialog = false;
        if (debugDialog) {
            JPanel panel = new FormDebugPanel();
            dialogBuilder = new DefaultFormBuilder(new FormLayout(ENCODED_DIALOG_COLUMN_SPECS), panel);
        }
        else {
            JPanel panel = new JPanel(new BorderLayout());
            dialogBuilder = new DefaultFormBuilder(new FormLayout(ENCODED_DIALOG_COLUMN_SPECS), panel);
        }
        dialogBuilder.border(Borders.TABBED_DIALOG);

        final List<JCheckBox> vendorCheckboxes = new ArrayList<>();
        ItemListener itemListener = new ItemListener() {

            @Override
            public void itemStateChanged(ItemEvent e) {
                LOGGER.info("The checkbox has changed: {}", e);

                List<Integer> ids = new ArrayList<>();
                for (JCheckBox check : vendorCheckboxes) {
                    if (check.isSelected()) {
                        Integer id = (Integer) check.getClientProperty("vendorId");
                        ids.add(id);
                    }
                }

                String vendorIds = StringUtils.join(ids.toArray(new Integer[0]), ";");
                LOGGER.info("Prepared vendor Ids: {}", vendorIds);
                preferencesModel.setRailcomPlusVendorIds(vendorIds);
            }
        };

        String vendorIDs = preferencesModel.getRailcomPlusVendorIds();
        String[] vendorKeys = StringUtils.split(vendorIDs, ";");

        List<VendorType> vendors = DecoderVendorFactory.getDecoderVendors();
        if (CollectionUtils.isNotEmpty(vendors)) {
            for (VendorType vendor : vendors) {

                String vendorName = vendor.getName();
                short id = vendor.getId();
                boolean isRailComPlus = vendor.isRailComPlus();
                LOGGER.info("Create checkbox for vendor: {}, id: {}, isRailComPlus: {}", vendorName, id, isRailComPlus);

                if (isRailComPlus) {
                    JCheckBox vendorBox = new JCheckBox(vendorName + " (" + id + ")");
                    vendorBox.setContentAreaFilled(false);
                    vendorBox.putClientProperty("vendorId", Integer.valueOf(id));

                    // check if the value must be checked initially
                    int index = ArrayUtils.indexOf(vendorKeys, Short.toString(id));
                    if (index > -1) {
                        vendorBox.setSelected(true);
                    }

                    vendorBox.addItemListener(itemListener);
                    vendorCheckboxes.add(vendorBox);
                    dialogBuilder.append(vendorBox);
                    dialogBuilder.nextLine();
                }
            }
        }

        return dialogBuilder.build();
    }

    public void addPreferencesViewListener(PreferencesViewListener l) {
        listeners.add(l);
    }
}
