package org.bidib.wizard.mvc.main.model.listener;

import org.bidib.jbidibc.core.AccessoryState;
import org.bidib.jbidibc.core.utils.AccessoryStateUtils.ErrorAccessoryState.AccessoryExecutionState;

public interface AccessoryExecutionListener {
    /**
     * The accessory execution state has changed.
     * 
     * @param executionState
     *            the current execution state
     * @param accessoryId
     *            the accessory id
     * @param aspect
     *            the aspect number
     * @param accessoryState
     *            the accessory state, maybe null
     */
    void executionStateChanged(
        AccessoryExecutionState executionState, int accessoryId, int aspect, AccessoryState accessoryState);
}
