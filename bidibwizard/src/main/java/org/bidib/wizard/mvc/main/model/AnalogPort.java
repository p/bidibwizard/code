package org.bidib.wizard.mvc.main.model;

import org.bidib.jbidibc.core.enumeration.AnalogPortEnum;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.wizard.comm.AnalogPortStatus;

public class AnalogPort extends Port<AnalogPortStatus> {
    private static final long serialVersionUID = 1L;

    public AnalogPort() {
        super(null);
        setStatus(AnalogPortStatus.START);
    }

    public AnalogPort(GenericPort genericPort) {
        super(genericPort);
    }

    @Override
    public byte[] getPortConfig() {
        return new byte[] { 0, 0, 0, 0 };
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof AnalogPort) {
            return ((AnalogPort) obj).getId() == getId();
        }
        return false;
    }

    @Override
    public int hashCode() {
        return getId();
    }

    @Override
    protected LcOutputType getPortType() {
        return LcOutputType.ANALOGPORT;
    }

    @Override
    protected AnalogPortStatus internalGetStatus() {
        return AnalogPortStatus.valueOf(AnalogPortEnum.valueOf(genericPort.getPortStatus()));
    }
}
