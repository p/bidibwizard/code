package org.bidib.wizard.mvc.main.view.table;

import javax.swing.JOptionPane;

import org.bidib.wizard.mvc.main.view.menu.listener.PortListMenuListener;

public class DefaultPortListMenuListener implements PortListMenuListener {

    @Override
    public void editLabel() {

    }

    @Override
    public void mapPort() {
        JOptionPane.showMessageDialog(JOptionPane.getFrameForComponent(null),
            "Support for map port is not implemented yet!");
    }

    @Override
    public void insertPorts() {
        JOptionPane.showMessageDialog(JOptionPane.getFrameForComponent(null),
            "Support for insert ports is not implemented yet!");
    }

}
