/*
 * Generated on 11/11/16 4:05 PM
 */
package org.bidib.wizard.mvc.script.view.modes;

import java.io.*;
import javax.swing.text.Segment;

import org.fife.ui.rsyntaxtextarea.*;


/**
 * Scanner for BiDiB script language.
 */
%%

%public
%class BidibTokenMaker
%extends AbstractJFlexCTokenMaker
%unicode
/* Case sensitive */
%type org.fife.ui.rsyntaxtextarea.Token


%{

	/**
	 * Type specific to XMLTokenMaker denoting a line ending with an unclosed
	 * double-quote attribute.
	 */
	public static final int INTERNAL_ATTR_DOUBLE			= -1;


	/**
	 * Type specific to XMLTokenMaker denoting a line ending with an unclosed
	 * single-quote attribute.
	 */
	public static final int INTERNAL_ATTR_SINGLE			= -2;


	/**
	 * Token type specific to XMLTokenMaker denoting a line ending with an
	 * unclosed XML tag; thus a new line is beginning still inside of the tag.
	 */
	public static final int INTERNAL_INTAG					= -3;

	/**
	 * Token type specific to XMLTokenMaker denoting a line ending with an
	 * unclosed XML tag; thus a new line is beginning still inside of the tag.
	 */
	public static final int INTERNAL_INVARIABLE				= -4;


	/**
	 * Constructor.  This must be here because JFlex does not generate a
	 * no-parameter constructor.
	 */
	public BidibTokenMaker() {
	}


	/**
	 * Adds the token specified to the current linked list of tokens as an
	 * "end token;" that is, at <code>zzMarkedPos</code>.
	 *
	 * @param tokenType The token's type.
	 */
	private void addEndToken(int tokenType) {
		addToken(zzMarkedPos,zzMarkedPos, tokenType);
	}

	/**
	 * Adds the token specified to the current linked list of tokens.
	 *
	 * @param tokenType The token's type.
	 * @see #addToken(int, int, int)
	 */
	private void addHyperlinkToken(int start, int end, int tokenType) {
		int so = start + offsetShift;
		addToken(zzBuffer, start,end, tokenType, so, true);
	}


	/**
	 * Adds the token specified to the current linked list of tokens.
	 *
	 * @param tokenType The token's type.
	 */
	private void addToken(int tokenType) {
		addToken(zzStartRead, zzMarkedPos-1, tokenType);
	}


	/**
	 * Adds the token specified to the current linked list of tokens.
	 *
	 * @param tokenType The token's type.
	 * @see #addHyperlinkToken(int, int, int)
	 */
	private void addToken(int start, int end, int tokenType) {
		int so = start + offsetShift;
		addToken(zzBuffer, start,end, tokenType, so, false);
	}


	/**
	 * Adds the token specified to the current linked list of tokens.
	 *
	 * @param array The character array.
	 * @param start The starting offset in the array.
	 * @param end The ending offset in the array.
	 * @param tokenType The token's type.
	 * @param startOffset The offset in the document at which this token
	 *        occurs.
	 * @param hyperlink Whether this token is a hyperlink.
	 */
	public void addToken(char[] array, int start, int end, int tokenType,
						int startOffset, boolean hyperlink) {
		super.addToken(array, start,end, tokenType, startOffset, hyperlink);
		zzStartRead = zzMarkedPos;
	}


	/**
	 * {@inheritDoc}
	 */
	public String[] getLineCommentStartAndEnd(int languageIndex) {
		return new String[] { "//", null };
	}


	/**
	 * Returns the first token in the linked list of tokens generated
	 * from <code>text</code>.  This method must be implemented by
	 * subclasses so they can correctly implement syntax highlighting.
	 *
	 * @param text The text from which to get tokens.
	 * @param initialTokenType The token type we should start with.
	 * @param startOffset The offset into the document at which
	 *        <code>text</code> starts.
	 * @return The first <code>Token</code> in a linked list representing
	 *         the syntax highlighted text.
	 */
	public Token getTokenList(Segment text, int initialTokenType, int startOffset) {

		resetTokenList();
		this.offsetShift = -text.offset + startOffset;

		// Start off in the proper state.
		int state = Token.NULL;
		switch (initialTokenType) {
			case Token.COMMENT_MULTILINE:
				state = MLC;
				start = text.offset;
				break;

			case Token.COMMENT_DOCUMENTATION:
				state = DOCCOMMENT;
				start = text.offset;
				break;

			case INTERNAL_INTAG:
				state = INTAG;
				break;

			case INTERNAL_INVARIABLE:
				state = INVARIABLE;
				break;

			default:
				state = Token.NULL;
		}

		s = text;
		try {
			yyreset(zzReader);
			yybegin(state);
			return yylex();
		} catch (IOException ioe) {
			ioe.printStackTrace();
			return new TokenImpl();
		}

	}


	/**
	 * Refills the input buffer.
	 *
	 * @return      <code>true</code> if EOF was reached, otherwise
	 *              <code>false</code>.
	 */
	private boolean zzRefill() {
		return zzCurrentPos>=s.offset+s.count;
	}


	/**
	 * Resets the scanner to read from a new input stream.
	 * Does not close the old reader.
	 *
	 * All internal variables are reset, the old input stream 
	 * <b>cannot</b> be reused (internal buffer is discarded and lost).
	 * Lexical state is set to <tt>YY_INITIAL</tt>.
	 *
	 * @param reader   the new input stream 
	 */
	public final void yyreset(Reader reader) {
		// 's' has been updated.
		zzBuffer = s.array;
		/*
		 * We replaced the line below with the two below it because zzRefill
		 * no longer "refills" the buffer (since the way we do it, it's always
		 * "full" the first time through, since it points to the segment's
		 * array).  So, we assign zzEndRead here.
		 */
		//zzStartRead = zzEndRead = s.offset;
		zzStartRead = s.offset;
		zzEndRead = zzStartRead + s.count - 1;
		zzCurrentPos = zzMarkedPos = zzPushbackPos = s.offset;
		zzLexicalState = YYINITIAL;
		zzReader = reader;
		zzAtBOL  = true;
		zzAtEOF  = false;
	}


%}

Letter							= [A-Za-z]
LetterOrUnderscore				= ({Letter}|"_")
NonzeroDigit						= [1-9]
Digit							= ("0"|{NonzeroDigit})
HexDigit							= ({Digit}|[A-Fa-f])
OctalDigit						= ([0-7])
AnyCharacterButApostropheOrBackSlash	= ([^\\'])
AnyCharacterButDoubleQuoteOrBackSlash	= ([^\\\"\n])
EscapedSourceCharacter				= ("u"{HexDigit}{HexDigit}{HexDigit}{HexDigit})
Escape							= ("\\"(([btnfr\"'\\])|([0123]{OctalDigit}?{OctalDigit}?)|({OctalDigit}{OctalDigit}?)|{EscapedSourceCharacter}))
NonSeparator						= ([^\t\f\r\n\ \(\)\{\}\[\]\;\,\.\=\>\<\!\~\?\:\+\-\*\/\&\|\^\%\"\']|"#"|"\\")


PreprocessorBegin   = "##"
PreprocessorEnd     = ")"

PreprocessorWord	= input|instruction|define|scriptversion|help|web|about|application

VelocityProcessorBegin   = "#"
VelocityProcessorWord = set|if|else|elseif|end|macro

IdentifierStart					= ({Letter}|"_"|"$")
IdentifierPart					= ({IdentifierStart}|{Digit}|("\\"{EscapedSourceCharacter}))

VariableBegin			= "${"
VariableEnd			    = "}"

Variable				= ("$"{IdentifierPart}*) | ({VariableBegin}{IdentifierPart}*{VariableEnd})


InTagIdentifier		= ([^ \t\n\"\'=\>]+)

LineTerminator				= (\n)
WhiteSpace				= ([ \t\f]+)

CharLiteral	= ([\']({AnyCharacterButApostropheOrBackSlash}|{Escape})[\'])
UnclosedCharLiteral			= ([\'][^\'\n]*)
ErrorCharLiteral			= ({UnclosedCharLiteral}[\'])
StringLiteral				= ([\"]({AnyCharacterButDoubleQuoteOrBackSlash}|{Escape})*[\"])
UnclosedStringLiteral		= ([\"]([\\].|[^\\\"])*[^\"]?)
ErrorStringLiteral			= ({UnclosedStringLiteral}[\"])

MLCBegin					= "#**"
MLCEnd						= "*#"

UnparsedBegin				= "#[["
UnparsedEnd					= "]]#"

LineCommentBegin			= "##"

IntegerLiteral			= ({Digit}+)
HexLiteral			= (0x{HexDigit}+)
FloatLiteral			= (({Digit}+)("."{Digit}+)?(e[+-]?{Digit}+)? | ({Digit}+)?("."{Digit}+)(e[+-]?{Digit}+)?)
ErrorNumberFormat			= (({IntegerLiteral}|{HexLiteral}|{FloatLiteral}){NonSeparator}+)


Separator2				= ([\;,.])

//Identifier				= ({IdentifierStart}{IdentifierPart}*)

URLGenDelim				= ([:\/\?#\[\]@])
URLSubDelim				= ([\!\$&'\(\)\*\+,;=])
URLUnreserved			= ({LetterOrUnderscore}|{Digit}|[\-\.\~])
URLCharacter			= ({URLGenDelim}|{URLSubDelim}|{URLUnreserved}|[%])
URLCharacters			= ({URLCharacter}*)
URLEndCharacter			= ([\/\$]|{Letter}|{Digit})
URL						= (((https?|f(tp|ile))"://"|"www.")({URLCharacters}{URLEndCharacter})?)


/* No string state */
/* No char state */
%state MLC
%state DOCCOMMENT
%state EOL_COMMENT
%state INTAG
%state INVARIABLE
%state INATTR_DOUBLE
%state INATTR_SINGLE

%%

<YYINITIAL> {

	/* Keywords */
	"add" |
	"config" |
//	"define" |
	"reselect" |
	"reset" |
	"select" |
	"set" |
	"wait"		{ addToken(Token.RESERVED_WORD); }

	/* Keywords 2 (just an optional set of keywords colored differently) */
	"action" |
	"delay" |
	"name" |
	"macroname" |
	"macronumber" |
	"number" |
	"ptype" |
	"step"		{ addToken(Token.RESERVED_WORD_2); }
	
	"text:de" |
	"text:en" |
	"default"		{ addToken(Token.RESERVED_WORD_2); }
	
	"ValueOff" |
	"ValueOn" |
	"DimmOff" |
	"DimmOn" |
	"DimmOff88" |
	"DimmOn88"   { addToken(Token.RESERVED_WORD_2); }
	
	"startup"  { addToken(Token.RESERVED_WORD_2); }
	
	"repeat" |
	"slowdown" { addToken(Token.RESERVED_WORD_2); }
	
	
	"restore" |
	"none"     { addToken(Token.DATA_TYPE); }

	/* Boolean literals. */
	"true" |
	"false"    { addToken(Token.LITERAL_BOOLEAN); }
	
	
	/* Data types */
	"int" |
	"string" |
	"boolean" |
	
	"accessory" |
	"aspect" |
	"macro" |
	"port" |
	"flag" |
	"randomDelay" |
	"off" |
	"on" |
	"start" |
	"stop" |
	"down" |
	"up" |
	"query0" |
	"query1" |
	"clear"  |
	"analog" |
	"backlight" |
	"input" |
	"light" |
	"servo" |
	"sound" |
	"switch"		{ addToken(Token.DATA_TYPE); }

	/* Functions */
	/* No functions */

	/* Preprocessor directives */

					
	{PreprocessorBegin}{PreprocessorWord} { 
									addToken(zzStartRead, zzMarkedPos - 1, Token.PREPROCESSOR);
									}

									
	{VelocityProcessorBegin}{VelocityProcessorWord} {
									addToken(zzStartRead, zzMarkedPos - 1, Token.MARKUP_CDATA);
									}

	{LineTerminator}				{ addNullToken(); return firstToken; }

//	{Identifier}					{ addToken(Token.IDENTIFIER); }
	{Variable}						{ addToken(Token.VARIABLE); }
//	{Preprocessor}					{ addToken(Token.PREPROCESSOR); }

	{WhiteSpace}					{ addToken(Token.WHITESPACE); }

	/* String/Character literals. */
	{CharLiteral}				{ addToken(Token.LITERAL_CHAR); }
	{UnclosedCharLiteral}		{ addToken(Token.ERROR_CHAR); addNullToken(); return firstToken; }
	{ErrorCharLiteral}			{ addToken(Token.ERROR_CHAR); }
	{StringLiteral}				{ addToken(Token.LITERAL_STRING_DOUBLE_QUOTE); }
	{UnclosedStringLiteral}		{ addToken(Token.ERROR_STRING_DOUBLE); addNullToken(); return firstToken; }
	{ErrorStringLiteral}			{ addToken(Token.ERROR_STRING_DOUBLE); }

	/* Comment literals. */
	{MLCBegin}					{ start = zzMarkedPos-3; yybegin(MLC); }
	{UnparsedBegin}				{ start = zzMarkedPos-3; yybegin(DOCCOMMENT); }
	{LineCommentBegin}			{ start = zzMarkedPos-2; yybegin(EOL_COMMENT); }

	/* Separators. */
	"(" |
	")" |
	"[" |
	"]" |
	"{" |
	"}"							{ addToken(Token.SEPARATOR); }

	{Separator2}					{ addToken(Token.IDENTIFIER); }

	/* Operators. */
//	"%" |
	"="		{ addToken(Token.OPERATOR); }

	/* Numbers */
	{IntegerLiteral}				{ addToken(Token.LITERAL_NUMBER_DECIMAL_INT); }
	{HexLiteral}					{ addToken(Token.LITERAL_NUMBER_HEXADECIMAL); }
	{FloatLiteral}					{ addToken(Token.LITERAL_NUMBER_FLOAT); }
	{ErrorNumberFormat}			{ addToken(Token.ERROR_NUMBER_FORMAT); }

	/* Ended with a line not in a string or comment. */
	<<EOF>>						{ addNullToken(); return firstToken; }

	/* Catch any other (unhandled) characters. */
	.							{ addToken(Token.IDENTIFIER); }

}


/* No char state */

/* No string state */

<MLC> {

	[^hwf\n*]+				{}
	{URL}					{ int temp=zzStartRead; addToken(start,zzStartRead-1, Token.COMMENT_MULTILINE); addHyperlinkToken(temp,zzMarkedPos-1, Token.COMMENT_MULTILINE); start = zzMarkedPos; }
	[hwf]					{}

	\n						{ addToken(start,zzStartRead-1, Token.COMMENT_MULTILINE); return firstToken; }
	{MLCEnd}					{ yybegin(YYINITIAL); addToken(start,zzStartRead+2-1, Token.COMMENT_MULTILINE); }
	"*"						{}
	<<EOF>>					{ addToken(start,zzStartRead-1, Token.COMMENT_MULTILINE); return firstToken; }

}


<DOCCOMMENT> {
	{UnparsedEnd}			{ yybegin(YYINITIAL); addToken(start,zzStartRead+2, Token.COMMENT_DOCUMENTATION); }

	[^hwf\n*]+				{}
	{URL}					{ int temp=zzStartRead; addToken(start,zzStartRead-1, Token.COMMENT_DOCUMENTATION); addHyperlinkToken(temp,zzMarkedPos-1, Token.COMMENT_DOCUMENTATION); start = zzMarkedPos; }
	[hwf]					{}

	\n						{ addToken(start,zzStartRead-1, Token.COMMENT_DOCUMENTATION); return firstToken; }
//	{UnparsedEnd}			{ yybegin(YYINITIAL); addToken(start,zzStartRead+2-1, Token.COMMENT_DOCUMENTATION); }
	"*"						{}
	<<EOF>>					{ yybegin(YYINITIAL); addToken(start,zzEndRead, Token.COMMENT_DOCUMENTATION); return firstToken; }

}


<EOL_COMMENT> {
//	{PreprocessorWord}      { int temp=zzStartRead; addToken(start,zzStartRead-2, Token.COMMENT_EOL); addToken(temp, zzMarkedPos - 2, Token.PREPROCESSOR); start = zzMarkedPos; }
//	{WhiteSpace}*
	[^hwf\n]+				{}
	{URL}					{ int temp=zzStartRead; addToken(start,zzStartRead-2, Token.COMMENT_EOL); addHyperlinkToken(temp,zzMarkedPos-2, Token.COMMENT_EOL); start = zzMarkedPos; }
	[hwf]					{}
	\n						{ addToken(start,zzStartRead-1, Token.COMMENT_EOL); addNullToken(); return firstToken; }
	<<EOF>>					{ addToken(start,zzStartRead-1, Token.COMMENT_EOL); addNullToken(); return firstToken; }
	
<INTAG> {
	//{PreprocessorWord}			{ addToken(Token.PREPROCESSOR); }
	//"%"							{ addToken(Token.PREPROCESSOR); /* Not valid but we'll still accept it */ }
	{PreprocessorEnd}			{ addToken(Token.PREPROCESSOR); }
	{InTagIdentifier}			{ addToken(Token.MARKUP_TAG_ATTRIBUTE); }
	{WhiteSpace}+				{ addToken(Token.WHITESPACE); }
	"="							{ addToken(Token.OPERATOR); }
	<<EOF>>						{ addNullToken(); return firstToken; }
}

<INVARIABLE> {
	//{PreprocessorWord}			{ addToken(Token.PREPROCESSOR); }
	{VariableEnd}				{ yybegin(YYINITIAL); addToken(Token.PREPROCESSOR); }
	{IdentifierPart}*			{ addToken(Token.VARIABLE); }
	{WhiteSpace}+				{ addToken(Token.WHITESPACE); }
	"="							{ addToken(Token.OPERATOR); }
	<<EOF>>						{ addNullToken(); return firstToken; }
}

<INATTR_DOUBLE> {
	[^\"]*						{}
	[\"]						{ yybegin(INTAG); addToken(start,zzStartRead, Token.MARKUP_TAG_ATTRIBUTE_VALUE); }
	<<EOF>>						{ addToken(start,zzStartRead-1, Token.MARKUP_TAG_ATTRIBUTE_VALUE); addEndToken(INTERNAL_ATTR_DOUBLE); return firstToken; }
}

<INATTR_SINGLE> {
	[^\']*						{}
	[\']						{ yybegin(INTAG); addToken(start,zzStartRead, Token.MARKUP_TAG_ATTRIBUTE_VALUE); }
	<<EOF>>						{ addToken(start,zzStartRead-1, Token.MARKUP_TAG_ATTRIBUTE_VALUE); addEndToken(INTERNAL_ATTR_SINGLE); return firstToken; }
}
	
}

