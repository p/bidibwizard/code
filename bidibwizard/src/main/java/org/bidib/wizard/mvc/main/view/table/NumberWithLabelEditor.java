package org.bidib.wizard.mvc.main.view.table;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.text.DecimalFormat;
import java.text.ParseException;

import javax.swing.AbstractCellEditor;
import javax.swing.BoxLayout;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.TableCellEditor;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;

import org.apache.commons.lang.StringUtils;
import org.bidib.wizard.utils.InputValidationDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NumberWithLabelEditor extends AbstractCellEditor implements TableCellEditor {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(NumberWithLabelEditor.class);

    private final JPanel panel = new JPanel();

    private final JFormattedTextField textField;

    private final NumberFormatter formatter = new NumberFormatter(
        new DecimalFormat("#")) {
        private static final long serialVersionUID = 1L;

        {
            // setAllowsInvalid(false);
            setMinimum(0);
        }
    };

    private final JLabel leftLabel;

    private final JLabel rightLabel;

    public NumberWithLabelEditor(String leftText, String rightText, int maximum) {
        textField = new JFormattedTextField();
        formatter.setMaximum(maximum);
        textField.setFormatterFactory(new DefaultFormatterFactory(formatter, formatter, formatter));
        textField.setHorizontalAlignment(SwingConstants.RIGHT);
        textField.setDocument(new InputValidationDocument(3, InputValidationDocument.NUMERIC));

        Dimension currentDim = textField.getPreferredSize();
        textField.setPreferredSize(new Dimension(30, currentDim.height));
        textField.setMinimumSize(new Dimension(30, currentDim.height));

        // textField.setFocusCycleRoot(true);
        leftLabel = new JLabel(leftText);
        leftLabel.setFocusable(false);
        rightLabel = new JLabel(rightText);
        rightLabel.setFocusable(false);

        panel.setFocusCycleRoot(true);

        panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));

        panel.add(leftLabel);
        panel.add(textField);
        panel.add(rightLabel);

        panel.addFocusListener(new FocusListener() {

            @Override
            public void focusLost(FocusEvent e) {

            }

            @Override
            public void focusGained(FocusEvent e) {
                LOGGER.info("Focus gained.");
                textField.requestFocus();

            }
        });
    }

    public NumberWithLabelEditor(String leftText, String rightText) {
        this(leftText, rightText, 255);
    }

    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        if (isSelected) {
            panel.setForeground(table.getSelectionForeground());
            panel.setBackground(table.getSelectionBackground());
        }
        else {
            panel.setForeground(table.getForeground());
            panel.setBackground(table.getBackground());
        }
        LOGGER.debug("Set value in editor: {}", value);
        textField.setText(value != null ? value.toString() : "");
        return panel;
    }

    @Override
    public Object getCellEditorValue() {
        try {
            if (StringUtils.isNotBlank(textField.getText())) {
                return Integer.parseInt(textField.getText());
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Parse textfield value failed.", ex);
        }
        LOGGER.debug("Allow empty string in text field. Return minimum value: {}", formatter.getMinimum());
        Comparable<?> minValue = formatter.getMinimum();
        if (minValue instanceof Number) {
            Number numMinValue = (Number) minValue;

            return Integer.valueOf(numMinValue.intValue());
        }
        return Integer.valueOf(0);
    }

    @Override
    public boolean stopCellEditing() {
        try {
            if (StringUtils.isNotBlank(textField.getText())) {
                formatter.setAllowsInvalid(false);
                int value = (Integer) formatter.stringToValue(textField.getText());

                LOGGER.debug("Current value: {}", value);
            }
            else {
                LOGGER.info("Allow empty string in text field.");
            }
        }
        catch (ParseException e) {
            LOGGER.warn("Parse text value failed: {}", e.getMessage());
            return false;
        }
        finally {
            formatter.setAllowsInvalid(true);
        }
        return super.stopCellEditing();
    }
}
