package org.bidib.wizard.mvc.main.view.cvdef;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.beans.Model;

public class BitfieldExclusiveBean extends Model {
    private static final Logger LOGGER = LoggerFactory.getLogger(BitfieldExclusiveBean.class);

    private static final long serialVersionUID = 1L;

    public final static String BIT_PROPERTY = "bit";

    public final static String BIT0_PROPERTY = "bit0";

    public final static String BIT1_PROPERTY = "bit1";

    public final static String BIT2_PROPERTY = "bit2";

    public final static String BIT3_PROPERTY = "bit3";

    public final static String BIT4_PROPERTY = "bit4";

    public final static String BIT5_PROPERTY = "bit5";

    public final static String BIT6_PROPERTY = "bit6";

    public final static String BIT7_PROPERTY = "bit7";

    private Integer bitfield = Integer.valueOf(0);

    private List<OffsetValue> allRadioValues;

    private OffsetValue zeroOffsetValue;

    private List<OffsetValue> offsetCvList;

    private List<Integer> enabledRadioBits;

    private OffsetValue offsetValue;

    private final int numOptions;

    public BitfieldExclusiveBean(int numOptions) {
        this.numOptions = numOptions;
    }

    public void setAllRadioValues(List<OffsetValue> allRadioValues) {
        this.allRadioValues = allRadioValues;
    }

    public void setEnabledRadioBits(List<Integer> enabledRadioBits) {
        this.enabledRadioBits = enabledRadioBits;
    }

    public void setOffsetCvListValues(final List<OffsetValue> offsetCvList) {
        this.offsetCvList = offsetCvList;

        // check if an offset value for '0' is defined in the offsetCvList
        zeroOffsetValue = null;
        if (CollectionUtils.isNotEmpty(offsetCvList)) {
            // search the zero offset value
            for (OffsetValue currentOffsetValue : offsetCvList) {
                if (currentOffsetValue.getValue() == 0) {
                    zeroOffsetValue = currentOffsetValue;
                    LOGGER.info("Found zeroOffsetValue: {}", zeroOffsetValue);
                    break;
                }
            }
        }
    }

    public boolean getBit(int index) {
        index++;
        return (bitfield & index) == index;
    }

    public boolean getBit0() {
        return (bitfield & 0x01) == 0x01;
    }

    public boolean getBit1() {
        return (bitfield & 0x02) == 0x02;
    }

    public boolean getBit2() {
        return (bitfield & 0x04) == 0x04;
    }

    public boolean getBit3() {
        return (bitfield & 0x08) == 0x08;
    }

    public boolean getBit4() {
        return (bitfield & 0x10) == 0x10;
    }

    public boolean getBit5() {
        return (bitfield & 0x20) == 0x20;
    }

    public boolean getBit6() {
        return (bitfield & 0x40) == 0x40;
    }

    public boolean getBit7() {
        return (bitfield & 0x80) == 0x80;
    }

    public boolean getBit8() {
        return isBitSet(8);
    }

    public boolean getBit9() {
        return isBitSet(9);
    }

    public boolean getBit10() {
        return isBitSet(10);
    }

    public boolean getBit11() {
        return isBitSet(11);
    }

    public boolean getBit12() {
        return isBitSet(12);
    }

    public boolean getBit13() {
        return isBitSet(13);
    }

    public boolean getBit14() {
        return isBitSet(14);
    }

    public boolean getBit15() {
        return isBitSet(15);
    }

    public boolean isBitSet(int bitPos) {
        return (bitfield & (1 << bitPos)) == (1 << bitPos);
    }

    public void setBit0(boolean bit) {
        setBit(bit, 0);
    }

    public void setBit1(boolean bit) {
        setBit(bit, 1);
    }

    public void setBit2(boolean bit) {
        setBit(bit, 2);
    }

    public void setBit3(boolean bit) {
        setBit(bit, 3);
    }

    public void setBit4(boolean bit) {
        setBit(bit, 4);
    }

    public void setBit5(boolean bit) {
        setBit(bit, 5);
    }

    public void setBit6(boolean bit) {
        setBit(bit, 6);
    }

    public void setBit7(boolean bit) {
        setBit(bit, 7);
    }

    public void setBit8(boolean bit) {
        setBit(bit, 8);
    }

    public void setBit9(boolean bit) {
        setBit(bit, 9);
    }

    public void setBit10(boolean bit) {
        setBit(bit, 10);
    }

    public void setBit11(boolean bit) {
        setBit(bit, 11);
    }

    public void setBit12(boolean bit) {
        setBit(bit, 12);
    }

    public void setBit13(boolean bit) {
        setBit(bit, 13);
    }

    public void setBit14(boolean bit) {
        setBit(bit, 14);
    }

    public void setBit15(boolean bit) {
        setBit(bit, 15);
    }

    public void setBit(boolean bit, int bitpos) {
        LOGGER.info("Set bit to '{}' at bitpos: {}", bit, bitpos);
        Integer newValue = null;
        if (bit) {
            newValue = (1 << bitpos);
        }
        else {
            newValue = (bitfield & ~(1 << bitpos));
        }
        LOGGER.info("The new value: {}", newValue);

        // set the bits of the bitfield
        this.bitfield = (int) newValue;

        fireMultiplePropertiesChanged();
    }

    public void setBitfieldValue(Number bitfieldNum) {
        LOGGER.info("Set the bitfield value: {}", (bitfieldNum != null ? (bitfieldNum.intValue() & 0xFF) : null));

        byte newValue = 0;
        if (bitfieldNum != null) {
            newValue = bitfieldNum.byteValue();
        }

        LOGGER.info("Current newValue: {}", ByteUtils.getInt(newValue));

        OffsetValue newOffsetValue = null;
        if (CollectionUtils.isNotEmpty(offsetCvList)) {
            if (ByteUtils.getInt(newValue) > 0) {
                OffsetValue[] offsetValues = offsetCvList.toArray(new OffsetValue[0]);
                // sort the array in natural order
                Arrays.sort(offsetValues);
                CollectionUtils.reverseArray(offsetValues);

                for (OffsetValue currentOffsetValue : offsetValues) {
                    byte currentVal = currentOffsetValue.getValue();
                    if ((newValue & currentVal) == currentVal) {
                        LOGGER.info("Found offset value to select the offsetValue: {}", currentOffsetValue);
                        newOffsetValue = currentOffsetValue;

                        break;
                    }
                }
            }
        }

        if (newOffsetValue != null) {
            // clean the bits of the offset value
            newValue = (byte) (newValue & ~(newOffsetValue.getValue()));
            LOGGER.info("Cleaned the bits of the offset value: {}", ByteUtils.getInt(newValue));
        }
        else if (zeroOffsetValue != null) {
            LOGGER.info("Use the zeroOffsetValue: {}", zeroOffsetValue);
            newOffsetValue = zeroOffsetValue;
        }

        // TODO get the correct bit from the lower group values
        if (CollectionUtils.isNotEmpty(enabledRadioBits) && CollectionUtils.isNotEmpty(allRadioValues)) {
            try {

                // search for the new value in the allRadioValues
                for (int idx = 0; idx < allRadioValues.size(); idx++) {

                    if (allRadioValues.get(idx).getValue() == newValue) {
                        LOGGER.info("Found the matching value at index: {}", idx);
                        newValue = ByteUtils.getLowByte(idx);
                        break;
                    }
                }

                LOGGER.info("Found new value to set: {}", ByteUtils.getInt(newValue));
            }
            catch (IndexOutOfBoundsException ex) {
                LOGGER.warn("Get assigned radio value failed for newValue: {}", ByteUtils.getInt(newValue), ex);
            }
        }

        // set the bit of the bitfield
        setBit(true, newValue);

        setOffsetValue(newOffsetValue);

        fireMultiplePropertiesChanged();
    }

    /**
     * @return the byte value of the current bitfield
     */
    public Byte getBitfieldValue() {
        // prepare the value from the set bits
        byte retVal = 0;
        for (int bitPos = 0; bitPos < numOptions; bitPos++) {
            if (isBitSet(bitPos)) {
                LOGGER.info("The bit is set at bitPos: {}", bitPos);
                try {
                    OffsetValue currentOffsetValue = allRadioValues.get(bitPos);
                    if (currentOffsetValue != null) {
                        retVal = currentOffsetValue.getValue();
                        LOGGER.info("The value of the current bit: {}", ByteUtils.getInt(retVal));
                        break;
                    }
                }
                catch (IndexOutOfBoundsException ex) {
                    LOGGER.warn("Get radio value failed for bitPos: {}", bitPos, ex);
                }
            }
        }

        if (offsetValue != null) {
            retVal += offsetValue.getValue();
        }
        LOGGER.info("Return the current bitfield value: {}", ByteUtils.getInt(retVal));
        return retVal;
    }

    /**
     * @return the offsetValue
     */
    public OffsetValue getOffsetValue() {
        return offsetValue;
    }

    /**
     * @param offsetValue
     *            the offsetValue to set
     */
    public void setOffsetValue(OffsetValue offsetValue) {
        LOGGER.info("Set the new offset value: {}", offsetValue);
        OffsetValue oldValue = this.offsetValue;
        this.offsetValue = offsetValue;

        firePropertyChange("offsetValue", oldValue, offsetValue);
    }
}