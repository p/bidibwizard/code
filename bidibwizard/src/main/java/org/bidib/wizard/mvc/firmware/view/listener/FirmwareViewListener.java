package org.bidib.wizard.mvc.firmware.view.listener;

import org.bidib.wizard.mvc.firmware.view.panel.listener.FirmwareProcessListener;

public interface FirmwareViewListener extends FirmwareProcessListener {
    /**
     * Close the view.
     */
    void close();
}
