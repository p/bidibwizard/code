package org.bidib.wizard.mvc.main.view.table;

import java.awt.Component;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTable;

import org.bidib.wizard.mvc.main.model.ConfigurablePort;

public class ConfigurablePortComboBoxEditor<E> extends ComboBoxEditor<E> {

    private static final long serialVersionUID = 1L;

    private final int portInstanceColumn;

    private final ComboBoxModel<E> allItemsModel;

    private final ComboBoxModel<E> filteredItemsModel;

    public ConfigurablePortComboBoxEditor(int portInstanceColumn, E[] allItems, E[] filteredItems) {
        super(allItems);
        allItemsModel = ((JComboBox) getComponent()).getModel();

        this.filteredItemsModel = new DefaultComboBoxModel<>(filteredItems);
        this.portInstanceColumn = portInstanceColumn;

    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        // get value at column 'int portInstanceColumn' must return the port
        Object portValue = table.getModel().getValueAt(row, portInstanceColumn);

        if (portValue instanceof ConfigurablePort) {
            // ConfigurablePort<?> port = (ConfigurablePort<?>) portValue;
            // boolean mappingEnabled = port.isRemappingEnabled();
            // if (mappingEnabled) {
            ((JComboBox) getComponent()).setModel(filteredItemsModel);
            // }
            // else {
            // ((JComboBox<E>) getComponent()).setModel(allItemsModel);
            // }
        }
        else {
            ((JComboBox<E>) getComponent()).setModel(allItemsModel);
        }

        Component comp = super.getTableCellEditorComponent(table, value, isSelected, row, column);

        return comp;
    }
}
