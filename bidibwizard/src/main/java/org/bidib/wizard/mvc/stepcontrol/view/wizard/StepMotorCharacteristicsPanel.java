package org.bidib.wizard.mvc.stepcontrol.view.wizard;

import java.awt.BorderLayout;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.common.view.converter.StringConverter;
import org.bidib.wizard.mvc.common.view.validation.IconFeedbackPanel;
import org.bidib.wizard.mvc.common.view.validation.PropertyValidationI18NSupport;
import org.bidib.wizard.mvc.common.view.wizard.JWizardComponents;
import org.bidib.wizard.mvc.stepcontrol.model.ConfigurationWizardModel;
import org.bidib.wizard.mvc.stepcontrol.model.Gearing;
import org.bidib.wizard.mvc.stepcontrol.model.MotorSizeType;
import org.bidib.wizard.utils.InputValidationDocument;
import org.jdesktop.swingx.JXTextField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.adapter.ComboBoxAdapter;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.value.ConverterValueModel;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.debug.FormDebugPanel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.ValidationResultModel;
import com.jgoodies.validation.util.PropertyValidationSupport;
import com.jgoodies.validation.view.ValidationComponentUtils;

public class StepMotorCharacteristicsPanel extends AbstractWizardPanel {
    private static final Logger LOGGER = LoggerFactory.getLogger(StepMotorCharacteristicsPanel.class);

    private ConfigurationWizardModel configurationWizardModel;

    private ValueModel selectionHolderMotorSize;

    private JComboBox<MotorSizeType> comboMotorSize;

    private ValueModel stepCountValueModel;

    private JTextField textStepCount;

    private InputValidationDocument stepCountDocument;

    private ValueModel gearingModel;

    private JLabel gearRatioLabel;

    private JXTextField textGearRatioPrimary;

    private JXTextField textGearRatioSecondary;

    private InputValidationDocument gearRatioDocument;

    private JTextField textBackLash;

    private JLabel backLashLabel;

    private ValueModel microSteppingModel;

    private JTextField textMicroStepping;

    private WizardValidationResultModel motorCharacteristicsValidationModel;

    public StepMotorCharacteristicsPanel(final JWizardComponents wizardComponents,
        final ConfigurationWizardModel configurationWizardModel) {
        super(wizardComponents);

        this.configurationWizardModel = configurationWizardModel;
    }

    @Override
    protected void initPanel() {
        super.initPanel();

        motorCharacteristicsValidationModel = new WizardValidationResultModel();

        // prepare the form
        DefaultFormBuilder builder = null;
        boolean debugDialog = false;
        if (debugDialog) {
            JPanel panel = new FormDebugPanel();
            builder = new DefaultFormBuilder(new FormLayout("p, 3dlu, p:g"), panel);
        }
        else {
            JPanel panel = new JPanel(new BorderLayout());
            builder = new DefaultFormBuilder(new FormLayout("p, 3dlu, p:g"), panel);
        }
        builder.border(Borders.TABBED_DIALOG);

        builder.append(new JLabel(Resources.getString(getClass(), "message")), 3);

        // add the components
        selectionHolderMotorSize =
            new PropertyAdapter<ConfigurationWizardModel>(configurationWizardModel,
                ConfigurationWizardModel.PROPERTYNAME_MOTORSIZE_TYPE, true);

        List<MotorSizeType> motorSizes = new ArrayList<>();
        for (MotorSizeType motorSize : MotorSizeType.values()) {
            motorSizes.add(motorSize);
        }

        ComboBoxAdapter<MotorSizeType> comboBoxAdapterMotorSize =
            new ComboBoxAdapter<MotorSizeType>(motorSizes, selectionHolderMotorSize);
        comboMotorSize = new JComboBox<>();
        comboMotorSize.setModel(comboBoxAdapterMotorSize);

        builder.append(Resources.getString(getClass(), "motorSize"), comboMotorSize);

        ValidationComponentUtils.setMandatory(comboMotorSize, true);
        ValidationComponentUtils.setMessageKeys(comboMotorSize, "validation.motorSize");

        // step count
        stepCountValueModel =
            new PropertyAdapter<ConfigurationWizardModel>(configurationWizardModel,
                ConfigurationWizardModel.PROPERTYNAME_STEPCOUNT, true);

        final ValueModel stepCountConverterModel =
            new ConverterValueModel(stepCountValueModel, new StringConverter(new DecimalFormat("#")));
        // create the textfield for the stepCount number
        textStepCount = new JTextField();
        stepCountDocument = new InputValidationDocument(5, InputValidationDocument.NUMERIC);
        textStepCount.setDocument(stepCountDocument);
        textStepCount.setColumns(5);

        // bind manually because we changed the document of the textfield
        Bindings.bind(textStepCount, stepCountConverterModel, false);
        builder.append(Resources.getString(getClass(), "stepCount"), textStepCount);

        ValidationComponentUtils.setMandatory(textStepCount, true);
        ValidationComponentUtils.setMessageKeys(textStepCount, "validation.stepCount");

        // gearing
        DefaultFormBuilder gearingBuilder = new DefaultFormBuilder(new FormLayout("pref, 3dlu, pref"));

        gearingModel =
            new PropertyAdapter<ConfigurationWizardModel>(configurationWizardModel,
                ConfigurationWizardModel.PROPERTYNAME_GEARING, true);

        for (int i = 0; i < 2; i++) {
            Gearing gearing = null;
            switch (i) {
                case 0:
                    gearing = new Gearing(Gearing.NO);
                    break;
                default:
                    gearing = (Gearing) gearingModel.getValue();
                    if (gearing == null || Gearing.NO.equals(gearing.getKey())) {
                        gearing = new Gearing(Gearing.YES);
                    }
                    break;
            }

            JRadioButton radio =
                BasicComponentFactory.createRadioButton(gearingModel, gearing,
                    Resources.getString(Gearing.class, gearing.getKey()));

            // add radio button
            gearingBuilder.append(radio);
        }

        builder.append(Resources.getString(getClass(), "gearing"), gearingBuilder.build());
        builder.nextLine();

        // add the input elements for the gearing
        DefaultFormBuilder gearingAddedBuilder =
            new DefaultFormBuilder(new FormLayout("pref, 3dlu, pref, 3dlu, pref, 3dlu, pref"));

        // create the textfield for the gear ratio number
        ValueModel gearRatioPrimaryValueModel =
            new PropertyAdapter<Gearing>(gearingModel, Gearing.PROPERTYNAME_GEARRATIO_PRIMARY, true);

        final ValueModel gearRatioPrimaryConverterModel =
            new ConverterValueModel(gearRatioPrimaryValueModel, new StringConverter(new DecimalFormat("#")));

        textGearRatioPrimary = new JXTextField();
        InputValidationDocument gearRatioPrimaryDocument =
            new InputValidationDocument(3, InputValidationDocument.NUMERIC);
        textGearRatioPrimary.setDocument(gearRatioPrimaryDocument);
        textGearRatioPrimary.setColumns(3);

        // bind manually because we changed the document of the textfield
        Bindings.bind(textGearRatioPrimary, gearRatioPrimaryConverterModel, false);
        gearRatioLabel = gearingAddedBuilder.append(Resources.getString(getClass(), "gearRatio"), textGearRatioPrimary);

        ValidationComponentUtils.setMandatory(textGearRatioPrimary, true);
        ValidationComponentUtils.setMessageKeys(textGearRatioPrimary,
            "validation." + Gearing.PROPERTYNAME_GEARRATIO_PRIMARY);

        // create the textfield for the gear ratio number
        ValueModel gearRatioSecondaryValueModel =
            new PropertyAdapter<Gearing>(gearingModel, Gearing.PROPERTYNAME_GEARRATIO_SECONDARY, true);

        final DecimalFormat decFormat = new DecimalFormat("#");
        final ValueModel gearRatioSecondaryConverterModel =
            new ConverterValueModel(gearRatioSecondaryValueModel, new StringConverter(decFormat));

        textGearRatioSecondary = new JXTextField();
        gearRatioDocument = new InputValidationDocument(5, InputValidationDocument.NUMERIC);
        textGearRatioSecondary.setDocument(gearRatioDocument);
        textGearRatioSecondary.setColumns(5);

        // bind manually because we changed the document of the textfield
        Bindings.bind(textGearRatioSecondary, gearRatioSecondaryConverterModel, false);
        gearingAddedBuilder.append(":", textGearRatioSecondary);

        ValidationComponentUtils.setMandatory(textGearRatioSecondary, true);
        ValidationComponentUtils.setMessageKeys(textGearRatioSecondary,
            "validation." + Gearing.PROPERTYNAME_GEARRATIO_SECONDARY);

        gearRatioLabel.setEnabled(false);

        textGearRatioPrimary.setEnabled(false);
        textGearRatioSecondary.setEnabled(false);

        // back lash
        ValueModel backLashValueModel = new PropertyAdapter<Gearing>(gearingModel, Gearing.PROPERTYNAME_BACKLASH, true);

        final ValueModel backLashConverterModel =
            new ConverterValueModel(backLashValueModel, new StringConverter(new DecimalFormat("#")));

        textBackLash = new JXTextField();
        InputValidationDocument backLashDocument = new InputValidationDocument(5, InputValidationDocument.NUMERIC);
        textBackLash.setDocument(backLashDocument);
        textBackLash.setColumns(5);

        Bindings.bind(textBackLash, backLashConverterModel, false);
        backLashLabel = gearingAddedBuilder.append(Resources.getString(getClass(), "backLash"), textBackLash, 3);

        ValidationComponentUtils.setMandatory(textBackLash, true);
        ValidationComponentUtils.setMessageKeys(textBackLash, "validation." + Gearing.PROPERTYNAME_BACKLASH);

        textBackLash.setEnabled(false);
        backLashLabel.setEnabled(false);

        builder.append(new JLabel(), gearingAddedBuilder.build());
        builder.nextLine();

        // micro stepping
        microSteppingModel =
            new PropertyAdapter<ConfigurationWizardModel>(configurationWizardModel,
                ConfigurationWizardModel.PROPERTYNAME_MICROSTEPPING, true);

        final ValueModel microSteppingConverterModel =
            new ConverterValueModel(microSteppingModel, new StringConverter(new DecimalFormat("#")));
        // create the textfield for the micro stepping number
        textMicroStepping = new JTextField();
        InputValidationDocument microSteppingDocument = new InputValidationDocument(5, InputValidationDocument.NUMERIC);
        textMicroStepping.setDocument(microSteppingDocument);
        textMicroStepping.setColumns(5);

        // bind manually because we changed the document of the textfield
        Bindings.bind(textMicroStepping, microSteppingConverterModel, false);
        builder.append(Resources.getString(getClass(), "microStepping"), textMicroStepping);

        ValidationComponentUtils.setMandatory(textMicroStepping, true);
        ValidationComponentUtils.setMessageKeys(textMicroStepping,
            "validation." + ConfigurationWizardModel.PROPERTYNAME_MICROSTEPPING);

        // TODO why disabled?
        // textMicroStepping.setEditable(false);

        // total step count
        ValueModel totalStepCountModel =
            new PropertyAdapter<ConfigurationWizardModel>(configurationWizardModel,
                ConfigurationWizardModel.PROPERTYNAME_TOTALSTEPCOUNT, true);

        final ValueModel totalStepCountConverterModel =
            new ConverterValueModel(totalStepCountModel, new StringConverter(new DecimalFormat("#")));
        // create the textfield for the micro stepping number
        JTextField textTotalStepCount = new JTextField();
        InputValidationDocument totalStepCountDocument =
            new InputValidationDocument(5, InputValidationDocument.NUMERIC);
        textTotalStepCount.setDocument(totalStepCountDocument);
        textTotalStepCount.setColumns(5);

        // bind manually because we changed the document of the textfield
        Bindings.bind(textTotalStepCount, totalStepCountConverterModel, false);
        builder.append(Resources.getString(getClass(), "totalStepCount"), textTotalStepCount);

        textTotalStepCount.setEditable(false);

        // check if we have validation enabled
        if (getValidationResultModel() != null) {
            LOGGER.info("Create iconfeedback panel.");
            JComponent cvIconPanel = new IconFeedbackPanel(getValidationResultModel(), builder.build());
            DefaultFormBuilder feedbackBuilder = null;
            feedbackBuilder = new DefaultFormBuilder(new FormLayout("p:g"));

            feedbackBuilder.appendRow("fill:p:grow");
            feedbackBuilder.add(cvIconPanel);

            this.panel = feedbackBuilder.build();
        }
        else {
            this.panel = builder.build();
        }

        configurationWizardModel.addPropertyChangeListener(ConfigurationWizardModel.PROPERTYNAME_STEPCOUNT,
            new PropertyChangeListener() {

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    triggerValidation();
                }
            });
        configurationWizardModel.addPropertyChangeListener(ConfigurationWizardModel.PROPERTYNAME_MICROSTEPPING,
            new PropertyChangeListener() {

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    triggerValidation();
                }
            });
        configurationWizardModel.addPropertyChangeListener(ConfigurationWizardModel.PROPERTYNAME_MOTORSIZE_TYPE,
            new PropertyChangeListener() {

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    triggerValidation();
                }
            });
        configurationWizardModel.addPropertyChangeListener(ConfigurationWizardModel.PROPERTYNAME_GEARING,
            new PropertyChangeListener() {

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    updateGearingComponents();
                }
            });

        final PropertyChangeListener gearingModelChangeListener = new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                LOGGER.info("Property changed, evt: {}", evt);
                if (evt != null && (Gearing.PROPERTYNAME_GEARRATIO_PRIMARY.equals(evt.getPropertyName())
                    || Gearing.PROPERTYNAME_GEARRATIO_SECONDARY.equals(evt.getPropertyName()))) {
                    configurationWizardModel.triggerUpdateTotalSteps();
                }

                triggerValidation();
            }
        };

        gearingModel.addValueChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                LOGGER.info("The value of the gearing model has changed, evt: {}", evt);

                // we must remove and add the listener to gearing if gearing was changed.
                // otherwise the calculation of the total step count does not work.
                Gearing oldValue = (Gearing) evt.getOldValue();
                if (oldValue != null) {
                    oldValue.removePropertyChangeListener(Gearing.PROPERTYNAME_GEARRATIO_PRIMARY,
                        gearingModelChangeListener);
                    oldValue.removePropertyChangeListener(Gearing.PROPERTYNAME_GEARRATIO_SECONDARY,
                        gearingModelChangeListener);
                    oldValue.removePropertyChangeListener(Gearing.PROPERTYNAME_BACKLASH, gearingModelChangeListener);
                }
                Gearing newValue = (Gearing) evt.getNewValue();
                if (newValue != null) {
                    newValue.addPropertyChangeListener(Gearing.PROPERTYNAME_GEARRATIO_PRIMARY,
                        gearingModelChangeListener);
                    newValue.addPropertyChangeListener(Gearing.PROPERTYNAME_GEARRATIO_SECONDARY,
                        gearingModelChangeListener);
                    newValue.addPropertyChangeListener(Gearing.PROPERTYNAME_BACKLASH, gearingModelChangeListener);
                }
                triggerValidation();
            }
        });

        if (gearingModel.getValue() != null) {
            LOGGER.info("Add initial property change listener for gearing properties.");
            Gearing gearing = (Gearing) gearingModel.getValue();
            gearing.addPropertyChangeListener(Gearing.PROPERTYNAME_GEARRATIO_PRIMARY, gearingModelChangeListener);
            gearing.addPropertyChangeListener(Gearing.PROPERTYNAME_GEARRATIO_SECONDARY, gearingModelChangeListener);
            gearing.addPropertyChangeListener(Gearing.PROPERTYNAME_BACKLASH, gearingModelChangeListener);
        }

        PropertyConnector.connect(motorCharacteristicsValidationModel, "validStateNoWarnOrErrors", this, "stepValid");

        LOGGER.info("initPanel finished.");
    }

    protected ValidationResultModel getValidationResultModel() {
        return motorCharacteristicsValidationModel;
    }

    private ValidationResult validate() {
        PropertyValidationSupport support = new PropertyValidationI18NSupport(configurationWizardModel, "validation");

        if (comboMotorSize.getSelectedItem() == null) {
            support.addError("motorSize", "not_empty");
        }

        // TODO get the validation ranges from CV definition

        if (configurationWizardModel.getStepCount() == null || configurationWizardModel.getStepCount() < 1) {
            support.addError("stepCount", "invalid_value;min=4,max=16384");
        }

        Gearing gearing = configurationWizardModel.getGearing();
        if (gearing != null && Gearing.YES.equals(gearing.getKey())) {
            // gearing is active
            if (gearing.getBackLash() == null || gearing.getBackLash() < 0) {
                support.addError(Gearing.PROPERTYNAME_BACKLASH, "invalid_value;min=0,max=1000");
            }
            // enable validation for gearRatio primary
            if (gearing.getGearRatioPrimary() == null || gearing.getGearRatioPrimary() < 1
                || gearing.getGearRatioPrimary() > 255) {
                support.addError(Gearing.PROPERTYNAME_GEARRATIO_PRIMARY, "invalid_value;min=1,max=255");
            }
            if (gearing.getGearRatioSecondary() == null || gearing.getGearRatioSecondary() < 1
                || gearing.getGearRatioSecondary() > 20000) {
                support.addError(Gearing.PROPERTYNAME_GEARRATIO_SECONDARY, "invalid_value;min=1,max=20000");
            }
        }

        if (configurationWizardModel.getMicroStepping() == null || configurationWizardModel.getMicroStepping() < 1) {
            support.addError("microStepping", "invalid_value;min=1,max=128");
        }

        ValidationResult validationResult = support.getResult();
        LOGGER.info("Prepared validationResult: {}", validationResult);
        return validationResult;
    }

    protected void triggerValidation() {
        ValidationResult validationResult = validate();
        motorCharacteristicsValidationModel.setResult(validationResult);
    }

    @Override
    public void update() {
        LOGGER.info("update is called.");
        super.update();
        updateGearingComponents();
    }

    private void updateGearingComponents() {
        if (configurationWizardModel.getGearing() != null
            && Gearing.YES.equals(configurationWizardModel.getGearing().getKey())) {
            // set the gearing parts enabled
            gearRatioLabel.setEnabled(true);

            textGearRatioPrimary.setEnabled(true);
            textGearRatioSecondary.setEnabled(true);

            textBackLash.setEnabled(true);
            backLashLabel.setEnabled(true);
        }
        else {
            // set the gearing parts disabled
            gearRatioLabel.setEnabled(false);

            textGearRatioPrimary.setEnabled(false);
            textGearRatioSecondary.setEnabled(false);

            textBackLash.setEnabled(false);
            backLashLabel.setEnabled(false);
        }

        triggerValidation();
    }
}
