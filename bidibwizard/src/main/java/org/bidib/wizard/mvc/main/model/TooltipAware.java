package org.bidib.wizard.mvc.main.model;

public interface TooltipAware {
    /**
     * @return the tooltip
     */
    String getTooltip();
}
