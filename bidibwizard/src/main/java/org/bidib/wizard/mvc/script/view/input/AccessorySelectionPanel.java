package org.bidib.wizard.mvc.script.view.input;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JComboBox;

import org.apache.commons.lang3.StringUtils;
import org.bidib.wizard.common.context.ApplicationContext;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.common.view.renderer.AccessoryRenderer;
import org.bidib.wizard.mvc.main.model.Accessory;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.script.view.ScriptParser;
import org.bidib.wizard.utils.AccessoryListUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.adapter.ComboBoxAdapter;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.view.ValidationComponentUtils;

public class AccessorySelectionPanel extends AbstractInputSelectionPanel {
    private static final Logger LOGGER = LoggerFactory.getLogger(AccessorySelectionPanel.class);

    private ValueModel selectionHolderAccessory;

    private ItemSelectionModel<Accessory> accessorySelectionModel;

    private SelectionInList<Accessory> accessorySelection;

    @Override
    public void initialize(
        final ApplicationContext context, final DefaultFormBuilder dialogBuilder, ValidationSupport validationSupport,
        String variableName, String variableType, String caption, String defaultValue, String prevValue) {
        this.variableName = variableName;
        setDefaultValue(defaultValue);

        accessorySelectionModel = new ItemSelectionModel<>();
        accessorySelectionModel.addPropertyChangeListener(ItemSelectionModel.PROPERTY_SELECTED_ITEM,
            new PropertyChangeListener() {

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    // TODO Auto-generated method stub
                    LOGGER.debug("Selected item has changed: {}", accessorySelectionModel.getSelectedItem());
                    validationSupport.triggerValidation();
                }
            });

        MainModel mainModel = context.get(ScriptParser.KEY_MAIN_MODEL, MainModel.class);

        List<Accessory> accessories = new LinkedList<>(mainModel.getAccessories());
        accessories.add(0, Accessory.NONE);

        accessorySelection = new SelectionInList<Accessory>(accessories);

        selectionHolderAccessory =
            new PropertyAdapter<ItemSelectionModel>(accessorySelectionModel, ItemSelectionModel.PROPERTY_SELECTED_ITEM,
                true);

        ComboBoxAdapter<Accessory> comboBoxAdapterAccessories =
            new ComboBoxAdapter<Accessory>(accessorySelection, selectionHolderAccessory);

        JComboBox<Accessory> comboAccessories = new JComboBox<Accessory>();
        comboAccessories.setModel(comboBoxAdapterAccessories);
        comboAccessories.setRenderer(new AccessoryRenderer());

        caption = "<html>" + caption + "</html>";

        dialogBuilder.append(caption, comboAccessories);

        ValidationComponentUtils.setMandatory(comboAccessories, true);
        ValidationComponentUtils.setMessageKeys(comboAccessories, "validation." + variableName);

        String initialValue = defaultValue;
        if (StringUtils.isNotBlank(prevValue)) {
            initialValue = prevValue;
        }

        if (StringUtils.isNotBlank(initialValue)) {
            try {
                Integer def = Integer.valueOf(initialValue);
                LOGGER.info("Set the initial value: {}", def);

                Accessory accessory =
                    AccessoryListUtils.findAccessoryByAccessoryNumber(accessorySelection.getList(), def.intValue());

                accessorySelectionModel.setSelectedItem(accessory);
            }
            catch (Exception ex) {
                LOGGER.warn("Parse initial value to accessory number failed: {}", initialValue, ex);
            }
        }
        else {
            accessorySelectionModel.setSelectedItem(Accessory.NONE);
        }

        content = dialogBuilder.build();
    }

    public void validate(final ValidationResult validationResult) {

        if (selectionHolderAccessory.getValue() == null || Accessory.NONE.equals(selectionHolderAccessory.getValue())) {
            validationResult.addError(Resources.getString((Class<?>) null, "validation.select_accessory"),
                "validation." + variableName);
        }
    }

    @Override
    public Object getSelectedValue() {
        if (selectionHolderAccessory.getValue() != null) {
            return Integer.valueOf(((Accessory) selectionHolderAccessory.getValue()).getId());
        }
        return null;
    }
}
