package org.bidib.wizard.mvc.main.model;

import java.util.Collection;
import java.util.LinkedList;

import org.bidib.wizard.mvc.main.model.listener.StatusListener;
import org.bidib.wizard.mvc.main.view.panel.listener.ModelClockStatusListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StatusModel {
    private static final Logger LOGGER = LoggerFactory.getLogger(StatusModel.class);

    private final Collection<StatusListener> listeners = new LinkedList<StatusListener>();

    private final Collection<ModelClockStatusListener> modelClockStatusListeners =
        new LinkedList<ModelClockStatusListener>();

    private boolean cd;

    private boolean running = true;

    private boolean rx;

    private boolean tx;

    private boolean cts;

    private boolean modelClockStartEnabled;

    public void addStatusListener(StatusListener l) {
        listeners.add(l);
    }

    public void addModelClockStatusListener(ModelClockStatusListener l) {
        modelClockStatusListeners.add(l);
    }

    /**
     * @return the port status connected
     */
    public boolean isCd() {
        return cd;
    }

    /**
     * @param cd
     *            the port status connected flag
     */
    public void setCd(boolean cd) {
        LOGGER.debug("setCd: {}", cd);
        this.cd = cd;
        fireCdChanged(cd);
    }

    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
        fireRunningChanged(running);
    }

    public boolean isRx() {
        return rx;
    }

    public void setRx(boolean rx) {
        LOGGER.trace("setRx: {}", rx);
        this.rx = rx;
        fireRxChanged(rx);
    }

    public boolean isTx() {
        return tx;
    }

    public void setTx(boolean tx) {
        LOGGER.trace("setTx: {}", tx);
        this.tx = tx;
        fireTxChanged(tx);
    }

    public boolean isCts() {
        return cts;
    }

    public void setCts(boolean cts) {
        LOGGER.trace("setCts: {}", cts);
        this.cts = cts;
        fireCtsChanged(cts);
    }

    /**
     * @return the modelClockStartEnabled
     */
    public boolean isModelClockStartEnabled() {
        return modelClockStartEnabled;
    }

    /**
     * @param modelClockStartEnabled
     *            the modelClockStartEnabled to set
     */
    public void setModelClockStartEnabled(boolean modelClockStartEnabled) {
        LOGGER.info("Set modelClockStartEnabled: {}", modelClockStartEnabled);
        this.modelClockStartEnabled = modelClockStartEnabled;
        fireModelClockStartEnabledChanged(modelClockStartEnabled);
    }

    private void fireModelClockStartEnabledChanged(boolean enabled) {
        for (ModelClockStatusListener l : modelClockStatusListeners) {
            l.setModelTimeStartStatus(enabled);
        }
    }

    private void fireRunningChanged(boolean running) {
        for (StatusListener l : listeners) {
            l.runningChanged(running);
        }
    }

    private void fireRxChanged(boolean rx) {
        for (StatusListener l : listeners) {
            l.rxChanged(rx);
        }
    }

    private void fireTxChanged(boolean tx) {
        for (StatusListener l : listeners) {
            l.txChanged(tx);
        }
    }

    private void fireCdChanged(boolean cd) {
        for (StatusListener l : listeners) {
            l.cdChanged(cd);
        }
    }

    private void fireCtsChanged(boolean cts) {
        for (StatusListener l : listeners) {
            l.ctsChanged(cts);
        }
    }
}
