package org.bidib.wizard.mvc.main.model;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.bidib.jbidibc.exchange.lcmacro.AccessoryOkayPoint;
import org.bidib.jbidibc.exchange.lcmacro.AnalogPortPoint;
import org.bidib.jbidibc.exchange.lcmacro.BacklightPortPoint;
import org.bidib.jbidibc.exchange.lcmacro.CriticalSectionPoint;
import org.bidib.jbidibc.exchange.lcmacro.DelayPoint;
import org.bidib.jbidibc.exchange.lcmacro.FlagPoint;
import org.bidib.jbidibc.exchange.lcmacro.InputPortPoint;
import org.bidib.jbidibc.exchange.lcmacro.LcMacroExporter;
import org.bidib.jbidibc.exchange.lcmacro.LcMacroPointType;
import org.bidib.jbidibc.exchange.lcmacro.LcMacroType;
import org.bidib.jbidibc.exchange.lcmacro.LightPortPoint;
import org.bidib.jbidibc.exchange.lcmacro.MacroActionPoint;
import org.bidib.jbidibc.exchange.lcmacro.MotorPortPoint;
import org.bidib.jbidibc.exchange.lcmacro.PortModelType;
import org.bidib.jbidibc.exchange.lcmacro.RandomDelayPoint;
import org.bidib.jbidibc.exchange.lcmacro.RepeatPeriodType;
import org.bidib.jbidibc.exchange.lcmacro.RepeatWeekdayType;
import org.bidib.jbidibc.exchange.lcmacro.ServoMoveQueryPoint;
import org.bidib.jbidibc.exchange.lcmacro.ServoPortPoint;
import org.bidib.jbidibc.exchange.lcmacro.SoundPortPoint;
import org.bidib.jbidibc.exchange.lcmacro.StartClkType;
import org.bidib.jbidibc.exchange.lcmacro.SwitchPairPortPoint;
import org.bidib.jbidibc.exchange.lcmacro.SwitchPortPoint;
import org.bidib.wizard.comm.AccessoryOkayStatus;
import org.bidib.wizard.comm.AnalogPortStatus;
import org.bidib.wizard.comm.BacklightPortStatus;
import org.bidib.wizard.comm.CriticalFunctionStatus;
import org.bidib.wizard.comm.FlagStatus;
import org.bidib.wizard.comm.InputStatus;
import org.bidib.wizard.comm.LightPortStatus;
import org.bidib.wizard.comm.MacroStatus;
import org.bidib.wizard.comm.MotorPortStatus;
import org.bidib.wizard.comm.ServoPortStatus;
import org.bidib.wizard.comm.SoundPortStatus;
import org.bidib.wizard.comm.SwitchPortStatus;
import org.bidib.wizard.mvc.main.model.function.AccessoryOkayFunction;
import org.bidib.wizard.mvc.main.model.function.AnalogPortAction;
import org.bidib.wizard.mvc.main.model.function.BacklightPortAction;
import org.bidib.wizard.mvc.main.model.function.CriticalFunction;
import org.bidib.wizard.mvc.main.model.function.DelayFunction;
import org.bidib.wizard.mvc.main.model.function.EmptyFunction;
import org.bidib.wizard.mvc.main.model.function.FlagFunction;
import org.bidib.wizard.mvc.main.model.function.Function;
import org.bidib.wizard.mvc.main.model.function.InputFunction;
import org.bidib.wizard.mvc.main.model.function.LightPortAction;
import org.bidib.wizard.mvc.main.model.function.MacroFunction;
import org.bidib.wizard.mvc.main.model.function.MotorPortAction;
import org.bidib.wizard.mvc.main.model.function.RandomDelayFunction;
import org.bidib.wizard.mvc.main.model.function.ServoMoveQueryFunction;
import org.bidib.wizard.mvc.main.model.function.ServoPortAction;
import org.bidib.wizard.mvc.main.model.function.SoundPortAction;
import org.bidib.wizard.mvc.main.model.function.SwitchPairPortAction;
import org.bidib.wizard.mvc.main.model.function.SwitchPortAction;
import org.bidib.wizard.utils.NodeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MacroFactory {
    private static final Logger LOGGER = LoggerFactory.getLogger(MacroFactory.class);

    public enum ExportFormat {
        serialization, jaxb
    }

    private MacroFactory() {
    }

    public static void saveMacro(String fileName, Macro macro, ExportFormat exportFormat)
        throws FileNotFoundException, IOException {
        saveMacro(fileName, macro, exportFormat, true);
    }

    public static void saveMacro(String fileName, Macro macro, ExportFormat exportFormat, boolean gzip)
        throws FileNotFoundException, IOException {

        switch (exportFormat) {
            case jaxb:
                new MacroFactory().saveMacroWithJaxb(fileName, macro, /* gzip */false);
                break;
            default:
                new MacroFactory().saveMacroWithXmlEncoder(fileName, macro, gzip);
                break;
        }
    }

    public static Macro loadMacro(String fileName, ExportFormat exportFormat, PortsProvider portsProvider) {

        Macro macro = null;
        switch (exportFormat) {
            case jaxb:
                macro = new MacroFactory().loadMacroWithJaxb(fileName, portsProvider);
                break;
            default:
                macro = new MacroFactory().loadMacroWithXmlDecoder(fileName, portsProvider);
                break;
        }
        return macro;
    }

    protected void saveMacroWithXmlEncoder(String fileName, Macro macro, boolean gzip)
        throws FileNotFoundException, IOException {
        OutputStream os = new BufferedOutputStream(new FileOutputStream(fileName));
        if (gzip) {
            LOGGER.debug("Use gzip to compress macro.");
            os = new GZIPOutputStream(os);
        }
        XMLEncoder e = new XMLEncoder(os);

        e.writeObject(macro);
        e.close();
    }

    protected void saveMacroWithJaxb(String fileName, Macro macro, boolean gzip) {
        LOGGER.debug("Save the macro to file: {}", fileName);

        LcMacroType lcMacro = new LcMacroType();
        lcMacro.setRepeat(macro.getCycles());
        lcMacro.setMacroId(macro.getId());
        lcMacro.setMacroName(macro.toString());
        lcMacro.setSlowdown(macro.getSpeed());
        lcMacro.setMaxMacroPoints(macro.getFunctionSize());
        lcMacro.setPortModel(macro.isFlatPortModel() ? PortModelType.FLAT : PortModelType.TYPE);

        if (macro.getStartConditions() != null) {
            Collection<StartCondition> startConditions = macro.getStartConditions();
            Iterator<StartCondition> iter = startConditions.iterator();
            while (iter.hasNext()) {
                StartCondition startCondition = iter.next();
                if (startCondition instanceof TimeStartCondition) {
                    TimeStartCondition timeStartCondition = (TimeStartCondition) startCondition;
                    MacroRepeatDay repeatDay = timeStartCondition.getRepeatDay();

                    StartClkType startClk = new StartClkType();
                    startClk.setWeekday(RepeatWeekdayType.fromValue(repeatDay.name()));
                    MacroRepeatTime repeatTime = timeStartCondition.getRepeatTime();

                    // set the repeat period
                    switch (repeatTime) {
                        case MINUTELY:
                            startClk.setRepeatPeriod(RepeatPeriodType.EVERY_MINUTE);
                            break;
                        case QUARTER_HOURLY:
                            startClk.setRepeatPeriod(RepeatPeriodType.EVERY_QUARTER_HOUR);
                            break;
                        case HALF_HOURLY:
                            startClk.setRepeatPeriod(RepeatPeriodType.EVERY_HALF_HOUR);
                            break;
                        case HOURLY:
                            startClk.setRepeatPeriod(RepeatPeriodType.EVERY_HOUR);
                            break;
                        default:
                            break;
                    }

                    Calendar cal = timeStartCondition.getTime();
                    startClk.setHour((byte) cal.get(Calendar.HOUR_OF_DAY));
                    startClk.setMinute((byte) cal.get(Calendar.MINUTE));
                    // TODO timezone is missing in schema! do we need that?

                    lcMacro.setStartClk(startClk);
                }
            }
        }

        // process the functions
        for (Function<?> function : macro.getFunctions()) {
            LOGGER.info("Process current function: {}", function);

            if (function != null) {
                lcMacro.getLcMacroPoint().add(function.toLcMacroPoint());
            }
            else {
                LOGGER.warn("Empty macro step is not exported!");
            }
        }
        new LcMacroExporter().saveMacro(lcMacro, fileName, gzip);
    }

    protected Macro loadMacroWithXmlDecoder(String fileName, PortsProvider portsProvider) {
        Macro macro = null;
        XMLDecoder d = null;

        try {
            try {
                d =
                    new XMLDecoder(XmlLoader
                        .changePackage(new InputStreamReader(new GZIPInputStream(new FileInputStream(fileName)))));
                macro = (Macro) d.readObject();
            }
            catch (IOException e) {
                d = new XMLDecoder(XmlLoader.changePackage(new FileReader(fileName)));
                macro = (Macro) d.readObject();
            }
        }
        catch (FileNotFoundException e) {
            LOGGER.trace("Read macros from file failed because the file is not available.", e);
        }
        catch (UnsupportedEncodingException e) {
            LOGGER.warn("The selected encoding is not supported.", e);
        }
        finally {
            if (d != null) {
                d.close();
            }
        }

        if (macro != null) {
            LOGGER.info("Loaded the macro from file: {}", macro);
            final Map<String, Object> params = new HashMap<>();
            replaceReferences(macro, portsProvider, params);

            // TODO show warning if errors in params

        }
        return macro;
    }

    private void addEmptyFunction(final List<Function<?>> functions) {
        EmptyFunction function = new EmptyFunction();
        functions.add(function);
    }

    protected Macro loadMacroWithJaxb(String fileName, PortsProvider portsProvider) {
        LOGGER.debug("Load macro from file: {}", fileName);

        LcMacroType lcMacro = new LcMacroExporter().loadMacro(fileName);
        Macro macro = null;
        if (lcMacro != null) {
            macro = new Macro(lcMacro.getMaxMacroPoints());
            macro.setId(lcMacro.getMacroId());
            macro.setLabel(lcMacro.getMacroName());
            macro.setCycles(lcMacro.getRepeat());
            macro.setSpeed(lcMacro.getSlowdown());

            macro.setFlatPortModel(lcMacro.getPortModel() == PortModelType.FLAT);

            boolean nodeIsFlatPortModel = portsProvider.isFlatPortModel();

            List<Function<?>> functions = new LinkedList<Function<?>>();
            for (LcMacroPointType lcMacroPoint : lcMacro.getLcMacroPoint()) {

                if (lcMacroPoint instanceof ServoMoveQueryPoint) {
                    ServoMoveQueryPoint servoMoveQueryPoint = (ServoMoveQueryPoint) lcMacroPoint;
                    // search the servo port
                    ServoPort port =
                        NodeUtils.getPort(portsProvider.getServoPorts(), servoMoveQueryPoint.getOutputNumber(),
                            macro.isFlatPortModel(), nodeIsFlatPortModel);
                    if (port == null) {
                        LOGGER.warn("No ServoPort available with output number: {}",
                            servoMoveQueryPoint.getOutputNumber());
                        addEmptyFunction(functions);
                    }
                    else {
                        ServoMoveQueryFunction function = new ServoMoveQueryFunction(port);
                        functions.add(function);
                    }
                }
                else if (lcMacroPoint instanceof AccessoryOkayPoint) {
                    AccessoryOkayPoint accessoryOkayPoint = (AccessoryOkayPoint) lcMacroPoint;
                    AccessoryOkayStatus inputStatus = null;
                    InputPort port = null;
                    switch (accessoryOkayPoint.getAccessoryOkayActionType()) {
                        case NO_FEEDBACK:
                            inputStatus = AccessoryOkayStatus.NO_FEEDBACK;
                            break;
                        case QUERY_1:
                            // search the input port
                            port =
                                NodeUtils.getPort(portsProvider.getEnabledInputPorts(),
                                    accessoryOkayPoint.getInputNumber(), macro.isFlatPortModel(), nodeIsFlatPortModel);
                            if (port == null) {
                                LOGGER.warn("No InputPort available with port number: {}",
                                    accessoryOkayPoint.getInputNumber());
                                port = InputPort.NONE;
                                // addEmptyFunction(functions);
                            }
                            // else {
                            inputStatus = AccessoryOkayStatus.QUERY1;
                            // }
                            break;
                        default:
                            // search the input port
                            port =
                                NodeUtils.getPort(portsProvider.getEnabledInputPorts(),
                                    accessoryOkayPoint.getInputNumber(), macro.isFlatPortModel(), nodeIsFlatPortModel);
                            if (port == null) {
                                LOGGER.warn("No InputPort available with port number: {}",
                                    accessoryOkayPoint.getInputNumber());
                                port = InputPort.NONE;
                                // addEmptyFunction(functions);
                            }
                            // else {
                            inputStatus = AccessoryOkayStatus.QUERY0;
                            // }
                            break;
                    }
                    AccessoryOkayFunction function = new AccessoryOkayFunction(inputStatus, port);
                    functions.add(function);
                }
                else if (lcMacroPoint instanceof BacklightPortPoint) {
                    BacklightPortPoint backlightPortPoint = (BacklightPortPoint) lcMacroPoint;
                    // search the light port
                    BacklightPort port =
                        NodeUtils.getPort(portsProvider.getBacklightPorts(), backlightPortPoint.getOutputNumber(),
                            macro.isFlatPortModel(), nodeIsFlatPortModel);
                    if (port == null) {
                        LOGGER.warn("No BacklightPort available with output number: {}",
                            backlightPortPoint.getOutputNumber());
                        addEmptyFunction(functions);
                    }
                    else {
                        BacklightPortAction action =
                            new BacklightPortAction(
                                BacklightPortStatus
                                    .valueOf(backlightPortPoint.getBacklightPortActionType().getAction().name()),
                                port, backlightPortPoint.getDelay(),
                                backlightPortPoint.getBacklightPortActionType().getBrightness());
                        functions.add(action);
                    }
                }
                else if (lcMacroPoint instanceof LightPortPoint) {
                    LightPortPoint lightPortPoint = (LightPortPoint) lcMacroPoint;
                    // search the light port
                    LightPort port =
                        NodeUtils.getPort(portsProvider.getLightPorts(), lightPortPoint.getOutputNumber(),
                            macro.isFlatPortModel(), nodeIsFlatPortModel);
                    if (port == null) {
                        LOGGER.warn("No LightPort available with output number: {}", lightPortPoint.getOutputNumber());
                        addEmptyFunction(functions);
                    }
                    else {
                        LightPortAction action =
                            new LightPortAction(LightPortStatus.valueOf(lightPortPoint.getLightPortActionType().name()),
                                port, lightPortPoint.getDelay());
                        functions.add(action);
                    }
                }
                else if (lcMacroPoint instanceof ServoPortPoint) {
                    ServoPortPoint servoPortPoint = (ServoPortPoint) lcMacroPoint;
                    // search the servo port
                    ServoPort port =
                        NodeUtils.getPort(portsProvider.getServoPorts(), servoPortPoint.getOutputNumber(),
                            macro.isFlatPortModel(), nodeIsFlatPortModel);
                    if (port == null) {
                        LOGGER.warn("No ServoPort available with output number: {}", servoPortPoint.getOutputNumber());
                        addEmptyFunction(functions);
                    }
                    else {
                        ServoPortAction action =
                            new ServoPortAction(
                                ServoPortStatus.valueOf(servoPortPoint.getServoPortActionType().getAction().name()),
                                port, servoPortPoint.getDelay(),
                                servoPortPoint.getServoPortActionType().getDestination());
                        functions.add(action);
                    }
                }
                else if (lcMacroPoint instanceof AnalogPortPoint) {
                    AnalogPortPoint analogPortPoint = (AnalogPortPoint) lcMacroPoint;
                    // search the analog port
                    AnalogPort port =
                        NodeUtils.getPort(portsProvider.getAnalogPorts(), analogPortPoint.getOutputNumber(),
                            macro.isFlatPortModel(), nodeIsFlatPortModel);
                    if (port == null) {
                        LOGGER.warn("No AnalogPort available with output number: {}",
                            analogPortPoint.getOutputNumber());
                        addEmptyFunction(functions);
                    }
                    else {
                        AnalogPortAction action =
                            new AnalogPortAction(
                                AnalogPortStatus.valueOf(analogPortPoint.getAnalogPortActionType().getAction().name()),
                                port, analogPortPoint.getDelay(), analogPortPoint.getAnalogPortActionType().getValue());
                        functions.add(action);
                    }
                }
                // TODO add tests ...
                else if (lcMacroPoint instanceof SwitchPortPoint) {
                    SwitchPortPoint switchPortPoint = (SwitchPortPoint) lcMacroPoint;
                    // search the switch port
                    SwitchPort port =
                        NodeUtils.getPort(portsProvider.getEnabledSwitchPorts(), switchPortPoint.getOutputNumber(),
                            macro.isFlatPortModel(), nodeIsFlatPortModel);
                    if (port == null) {
                        LOGGER.warn("No SwitchPort available with output number: {}",
                            switchPortPoint.getOutputNumber());
                        // addEmptyFunction(functions);
                        port = SwitchPort.NONE;
                    }
                    // else {
                    SwitchPortAction action =
                        new SwitchPortAction(SwitchPortStatus.valueOf(switchPortPoint.getSwitchPortActionType().name()),
                            port, switchPortPoint.getDelay());
                    functions.add(action);
                    // }
                }
                else if (lcMacroPoint instanceof SwitchPairPortPoint) {
                    SwitchPairPortPoint switchPairPortPoint = (SwitchPairPortPoint) lcMacroPoint;
                    // search the switchPair port
                    SwitchPairPort port =
                        NodeUtils.getPort(portsProvider.getEnabledSwitchPairPorts(),
                            switchPairPortPoint.getOutputNumber(), macro.isFlatPortModel(), nodeIsFlatPortModel);
                    if (port == null) {
                        LOGGER.warn("No SwitchPairPort available with output number: {}",
                            switchPairPortPoint.getOutputNumber());
                        // addEmptyFunction(functions);
                        port = SwitchPairPort.NONE;
                    }
                    // else {
                    SwitchPairPortAction action =
                        new SwitchPairPortAction(
                            SwitchPortStatus.valueOf(switchPairPortPoint.getSwitchPortActionType().name()), port,
                            switchPairPortPoint.getDelay());
                    functions.add(action);
                    // }
                }
                else if (lcMacroPoint instanceof SoundPortPoint) {
                    SoundPortPoint soundPortPoint = (SoundPortPoint) lcMacroPoint;
                    // search the switch port
                    SoundPort port =
                        NodeUtils.getPort(portsProvider.getSoundPorts(), soundPortPoint.getOutputNumber(),
                            macro.isFlatPortModel(), nodeIsFlatPortModel);
                    if (port == null) {
                        LOGGER.warn("No SwitchPort available with output number: {}", soundPortPoint.getOutputNumber());
                        addEmptyFunction(functions);
                    }
                    else {
                        SoundPortAction action =
                            new SoundPortAction(
                                SoundPortStatus.valueOf(soundPortPoint.getSoundPortActionType().getAction().name()),
                                port, soundPortPoint.getDelay(), soundPortPoint.getSoundPortActionType().getValue());
                        functions.add(action);
                    }
                }
                else if (lcMacroPoint instanceof MotorPortPoint) {
                    MotorPortPoint motorPortPoint = (MotorPortPoint) lcMacroPoint;
                    // search the motor port
                    MotorPort port =
                        NodeUtils.getPort(portsProvider.getMotorPorts(), motorPortPoint.getOutputNumber(),
                            macro.isFlatPortModel(), nodeIsFlatPortModel);
                    if (port == null) {
                        LOGGER.warn("No MotorPort available with output number: {}", motorPortPoint.getOutputNumber());
                        addEmptyFunction(functions);
                    }
                    else {
                        MotorPortAction action =
                            new MotorPortAction(
                                MotorPortStatus.valueOf(motorPortPoint.getMotorPortActionType().getAction().name()),
                                port, motorPortPoint.getDelay(), motorPortPoint.getMotorPortActionType().getValue());
                        functions.add(action);
                    }
                }
                else if (lcMacroPoint instanceof DelayPoint) {
                    DelayPoint delayPoint = (DelayPoint) lcMacroPoint;
                    DelayFunction function = new DelayFunction();
                    function.setDelay(delayPoint.getDelayActionType());
                    functions.add(function);
                }
                else if (lcMacroPoint instanceof RandomDelayPoint) {
                    RandomDelayPoint randomDelayPoint = (RandomDelayPoint) lcMacroPoint;
                    RandomDelayFunction function = new RandomDelayFunction();
                    function.setMaximumValue(randomDelayPoint.getRandomDelayActionType());
                    functions.add(function);
                }
                else if (lcMacroPoint instanceof CriticalSectionPoint) {
                    CriticalSectionPoint criticalSectionPoint = (CriticalSectionPoint) lcMacroPoint;
                    CriticalFunction function =
                        new CriticalFunction(
                            CriticalFunctionStatus.valueOf(criticalSectionPoint.getCriticalSectionActionType().name()));
                    functions.add(function);
                }
                else if (lcMacroPoint instanceof MacroActionPoint) {
                    MacroActionPoint macroActionPoint = (MacroActionPoint) lcMacroPoint;
                    MacroFunction function =
                        new MacroFunction(
                            MacroStatus.valueOf(macroActionPoint.getMacroActionType().getOperation().name()),
                            macroActionPoint.getMacroActionType().getMacroNumber());
                    functions.add(function);
                }
                else if (lcMacroPoint instanceof FlagPoint) {
                    FlagPoint flagPoint = (FlagPoint) lcMacroPoint;
                    FlagFunction function =
                        new FlagFunction(FlagStatus.valueOf(flagPoint.getFlagActionType().getOperation().name()),
                            new Flag(flagPoint.getFlagActionType().getFlagNumber()));
                    functions.add(function);
                }
                else if (lcMacroPoint instanceof InputPortPoint) {
                    InputPortPoint inputPortPoint = (InputPortPoint) lcMacroPoint;
                    // search the input port
                    InputPort port =
                        NodeUtils.getPort(portsProvider.getEnabledInputPorts(), inputPortPoint.getInputNumber(),
                            macro.isFlatPortModel(), nodeIsFlatPortModel);
                    if (port == null) {
                        LOGGER.warn("No InputPort available with port number: {}", inputPortPoint.getInputNumber());
                        port = InputPort.NONE;

                        // addEmptyFunction(functions);
                    }
                    // else {
                    InputStatus inputStatus = null;
                    switch (inputPortPoint.getInputPortActionType()) {
                        case QUERY_1:
                            inputStatus = InputStatus.QUERY1;
                            break;
                        default:
                            inputStatus = InputStatus.QUERY0;
                            break;
                    }
                    InputFunction function = new InputFunction(inputStatus, port);
                    functions.add(function);
                    // }
                }
                // TODO support more port point types
            }
            macro.setFunctions(functions);

            // handle startClk ...
            TimeStartCondition timeStartCondition = new TimeStartCondition();
            StartClkType startClk = lcMacro.getStartClk();
            if (startClk != null) {
                LOGGER.info("Apply start condition: {}", startClk);
                if (startClk.getRepeatPeriod() != null) {
                    switch (startClk.getRepeatPeriod()) {
                        case EVERY_MINUTE:
                            timeStartCondition.setRepeatTime(MacroRepeatTime.MINUTELY);
                            timeStartCondition.setMinutely(true);
                            break;
                        case EVERY_QUARTER_HOUR:
                            timeStartCondition.setRepeatTime(MacroRepeatTime.QUARTER_HOURLY);
                            break;
                        case EVERY_HALF_HOUR:
                            timeStartCondition.setRepeatTime(MacroRepeatTime.HALF_HOURLY);
                            break;
                        case EVERY_HOUR:
                            timeStartCondition.setRepeatTime(MacroRepeatTime.HOURLY);
                            timeStartCondition.setHourly(true);
                            break;
                        default:
                            LOGGER.warn("Unsupported repeat period: {}", startClk.getRepeatPeriod());
                            break;
                    }
                }
                if (startClk.getWeekday() != null) {
                    timeStartCondition.setRepeatDay(MacroRepeatDay.valueOf(startClk.getWeekday().name()));
                }
                Calendar cal = GregorianCalendar.getInstance();
                cal.set(Calendar.HOUR_OF_DAY, startClk.getHour());
                cal.set(Calendar.MINUTE, startClk.getMinute());
                timeStartCondition.setTime(cal);
                LinkedList<StartCondition> startConditions = new LinkedList<StartCondition>();
                startConditions.add(timeStartCondition);
                macro.setStartConditions(startConditions);
            }
            else {
                LOGGER.info("No start condition available.");
            }
        }
        return macro;
    }

    // private <P extends Port<?>> P getPort(
    // List<P> ports, int portNumber, boolean macroIsflatPortModel, boolean nodeIsFlatPortModel) {
    //
    // if (macroIsflatPortModel != nodeIsFlatPortModel) {
    // // the models are different
    // if (!macroIsflatPortModel) {
    // // the macro is type-oriented and the node is flat
    // try {
    // return ports.get(portNumber);
    // }
    // catch (IndexOutOfBoundsException ex) {
    // LOGGER.warn("Port not available, portNumber: {}, message: {}", portNumber, ex.getMessage());
    // }
    // }
    // else {
    // LOGGER.warn("Cannot calculate port from flat to type-oriented model.");
    // }
    // }
    // else {
    // for (P port : ports) {
    // if (port.getId() == portNumber) {
    // return port;
    // }
    // }
    // }
    // return null;
    // }

    /**
     * Replace the port references in the provided macro.
     * <p>
     * This method is temporary and should be removed later after NodeState is loaded via a factory!
     * </p>
     * 
     * @param macro
     *            the macro
     * @param portsProvider
     *            the ports provider
     */
    @Deprecated
    public static void replacePortReferences(
        Macro macro, PortsProvider portsProvider, final Map<String, Object> params) {
        new MacroFactory().replaceReferences(macro, portsProvider, params);
    }

    private void replaceReferences(Macro macro, PortsProvider portsProvider, final Map<String, Object> params) {
        LOGGER.debug("Replace references in macro: {}", macro);
        if (macro != null) {
            List<Function<?>> macroFunctions = macro.getFunctions();

            boolean nodeIsFlatPortModel = portsProvider.isFlatPortModel();
            boolean macroIsFlatPortModel = macro.isFlatPortModel();

            // TODO add support for conversion from type-oriented to flat model
            LOGGER.info("replaceReferences, nodeIsFlatPortModel: {}, macroIsFlatPortModel: {}", nodeIsFlatPortModel,
                macroIsFlatPortModel);

            if (macroFunctions != null) {
                List<Function<?>> newMacroFunctions = new ArrayList<Function<?>>();

                for (Function<?> function : macroFunctions) {
                    LOGGER.info("Process current macro step: {}", function);

                    if (function instanceof AnalogPortAction) {
                        AnalogPort port = ((AnalogPortAction) function).getPort();

                        if (port != null) {
                            // get the replaced port
                            int id = port.getId();
                            port =
                                NodeUtils.getPort(portsProvider.getAnalogPorts(), port.getId(), macroIsFlatPortModel,
                                    nodeIsFlatPortModel);
                            if (port != null) {
                                ((AnalogPortAction) function).setPort(port);
                            }
                            else {
                                LOGGER.warn("No analogport found with id: {}", id);
                                addImportError(params, "No analogport found with port number: " + id);
                                function = new EmptyFunction();
                            }
                        }
                    }
                    else if (function instanceof BacklightPortAction) {
                        BacklightPort port = ((BacklightPortAction) function).getPort();

                        if (port != null) {
                            // get the replaced port
                            int id = port.getId();
                            port =
                                NodeUtils.getPort(portsProvider.getBacklightPorts(), port.getId(), macroIsFlatPortModel,
                                    nodeIsFlatPortModel);
                            if (port != null) {
                                ((BacklightPortAction) function).setPort(port);
                            }
                            else {
                                LOGGER.warn("No backlight port found with id: {}", id);
                                addImportError(params, "No backlightport found with port number: " + id);
                                function = new EmptyFunction();
                            }
                        }
                    }
                    else if (function instanceof FlagFunction) {
                        Flag flag = ((FlagFunction) function).getFlag();

                        if (flag != null) {
                            int id = flag.getId();
                            List<Flag> flags = portsProvider.getFlags();

                            if (id == -1) {
                                id = 0;
                            }
                            if (id >= 0 && id < flags.size()) {
                                ((FlagFunction) function).setFlag(flags.get(id));
                            }
                        }
                    }
                    else if (function instanceof AccessoryOkayFunction) {
                        InputPort port = ((AccessoryOkayFunction) function).getInput();
                        if (port != null) {
                            // get the replaced port
                            int id = port.getId();
                            port =
                                NodeUtils.getPort(portsProvider.getInputPorts(), id, macroIsFlatPortModel,
                                    nodeIsFlatPortModel);
                            if (port != null) {
                                ((AccessoryOkayFunction) function).setInput(port);
                            }
                            else {
                                LOGGER.warn("No inputport found with id: {}", id);
                                addImportError(params, "No inputport found with port number: " + id);
                                // function = new EmptyFunction();
                                ((AccessoryOkayFunction) function).setInput(InputPort.NONE);
                            }
                        }
                    }

                    else if (function instanceof InputFunction) {
                        InputPort port = ((InputFunction) function).getInput();

                        if (port != null) {
                            // get the replaced port
                            int id = port.getId();
                            port =
                                NodeUtils.getPort(portsProvider.getInputPorts(), port.getId(), macroIsFlatPortModel,
                                    nodeIsFlatPortModel);
                            if (port != null) {
                                ((InputFunction) function).setInput(port);
                            }
                            else {
                                LOGGER.warn("No inputport found with id: {}", id);
                                addImportError(params, "No inputport found with port number: " + id);
                                // function = new EmptyFunction();
                                ((InputFunction) function).setInput(InputPort.NONE);
                            }
                        }
                    }
                    else if (function instanceof LightPortAction) {
                        LightPort port = ((LightPortAction) function).getPort();

                        if (port != null) {
                            // get the replaced port
                            int id = port.getId();
                            port =
                                NodeUtils.getPort(portsProvider.getLightPorts(), port.getId(), macroIsFlatPortModel,
                                    nodeIsFlatPortModel);
                            if (port != null) {
                                ((LightPortAction) function).setPort(port);
                            }
                            else {
                                LOGGER.warn("No lightport found with id: {}", id);
                                addImportError(params, "No lightport found with port number: " + id);
                                function = new EmptyFunction();
                            }
                        }
                    }
                    else if (function instanceof MacroFunction) {
                        ((MacroFunction) function).setMacroId(((MacroFunction) function).getMacroId());
                    }
                    else if (function instanceof MotorPortAction) {
                        MotorPort port = ((MotorPortAction) function).getPort();

                        if (port != null) {
                            // get the replaced port
                            int id = port.getId();
                            port =
                                NodeUtils.getPort(portsProvider.getMotorPorts(), port.getId(), macroIsFlatPortModel,
                                    nodeIsFlatPortModel);
                            if (port != null) {
                                ((MotorPortAction) function).setPort(port);
                            }
                            else {
                                LOGGER.warn("No motorport found with id: {}", id);
                                addImportError(params, "No motorport found with port number: " + id);
                                function = new EmptyFunction();
                            }
                        }
                    }
                    else if (function instanceof ServoPortAction) {
                        ServoPort port = ((ServoPortAction) function).getPort();

                        if (port != null) {
                            // get the replaced port
                            int id = port.getId();
                            LOGGER.info("Found servo port to replace: {}", port);
                            port =
                                NodeUtils.getPort(portsProvider.getServoPorts(), port.getId(), macroIsFlatPortModel,
                                    nodeIsFlatPortModel);
                            LOGGER.info("Replaced port: {}", port);
                            if (port != null) {
                                ((ServoPortAction) function).setPort(port);
                            }
                            else {
                                LOGGER.warn("No servoport found with id: {}", id);
                                addImportError(params, "No servoport found with port number: " + id);

                                function = new EmptyFunction();
                            }
                        }
                    }
                    else if (function instanceof SoundPortAction) {
                        SoundPort port = ((SoundPortAction) function).getPort();

                        if (port != null) {
                            // get the replaced port
                            int id = port.getId();
                            port =
                                NodeUtils.getPort(portsProvider.getSoundPorts(), port.getId(), macroIsFlatPortModel,
                                    nodeIsFlatPortModel);
                            if (port != null) {
                                ((SoundPortAction) function).setPort(port);
                            }
                            else {
                                LOGGER.warn("No soundport found with id: {}", id);
                                addImportError(params, "No soundport found with port number: " + id);
                                function = new EmptyFunction();
                            }
                        }
                    }
                    else if (function instanceof SwitchPortAction) {
                        SwitchPort port = ((SwitchPortAction) function).getPort();

                        if (port != null) {
                            // get the replaced port
                            int id = port.getId();
                            LOGGER.info(" Found switch port to replace: {}", port);
                            port =
                                NodeUtils.getPort(portsProvider.getSwitchPorts(), port.getId(), macroIsFlatPortModel,
                                    nodeIsFlatPortModel);
                            LOGGER.info("Replaced port: {}", port);
                            if (port != null) {
                                ((SwitchPortAction) function).setPort(port);
                            }
                            else {
                                LOGGER.warn("No switchport found with id: {}", id);

                                addImportError(params, "No switchport found with port number: " + id);
                                // function = new EmptyFunction();
                                ((SwitchPortAction) function).setPort(SwitchPort.NONE);
                            }
                        }
                    }
                    newMacroFunctions.add(function);
                }
                macro.setFunctions(newMacroFunctions);
            }
        }
    }

    private void addImportError(Map<String, Object> params, String message) {
        List<String> saveErrors = (List<String>) params.get(NodeUtils.IMPORT_ERRORS);
        if (saveErrors == null) {
            saveErrors = new LinkedList<String>();
            params.put(NodeUtils.IMPORT_ERRORS, saveErrors);
        }
        saveErrors.add(message);
    }
}
