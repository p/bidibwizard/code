package org.bidib.wizard.mvc.railcomplus.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.collections4.Predicate;
import org.bidib.jbidibc.core.AddressData;
import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.DecoderIdAddressData;
import org.bidib.jbidibc.core.DefaultMessageListener;
import org.bidib.jbidibc.core.Feature;
import org.bidib.jbidibc.core.RcPlusBindData;
import org.bidib.jbidibc.core.RcPlusDecoderAnswerData;
import org.bidib.jbidibc.core.RcPlusFeedbackBindData;
import org.bidib.jbidibc.core.DecoderUniqueIdData;
import org.bidib.jbidibc.core.TidData;
import org.bidib.jbidibc.core.enumeration.AddressMode;
import org.bidib.jbidibc.core.enumeration.AddressTypeEnum;
import org.bidib.jbidibc.core.enumeration.CommandStationPom;
import org.bidib.jbidibc.core.enumeration.PomAcknowledge;
import org.bidib.jbidibc.core.enumeration.RcPlusAcknowledge;
import org.bidib.jbidibc.core.enumeration.RcPlusDecoderType;
import org.bidib.jbidibc.core.enumeration.RcPlusPhase;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.wizard.comm.Communication;
import org.bidib.wizard.comm.CommunicationFactory;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.common.view.DockKeys;
import org.bidib.wizard.mvc.main.controller.MainControllerInterface;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.model.listener.NodeListListener;
import org.bidib.wizard.mvc.railcomplus.controller.listener.RailcomPlusControllerListener;
import org.bidib.wizard.mvc.railcomplus.model.RailcomPlusDecoderModel;
import org.bidib.wizard.mvc.railcomplus.model.RailcomPlusModel;
import org.bidib.wizard.mvc.railcomplus.model.listener.DecoderAddressListener;
import org.bidib.wizard.mvc.railcomplus.view.RailcomPlusView;
import org.bidib.wizard.mvc.railcomplus.view.listener.RailcomPlusViewListener;
import org.bidib.wizard.utils.DockUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vlsolutions.swing.docking.Dockable;
import com.vlsolutions.swing.docking.DockableState;
import com.vlsolutions.swing.docking.DockingConstants;
import com.vlsolutions.swing.docking.DockingDesktop;
import com.vlsolutions.swing.docking.DockingUtilities;
import com.vlsolutions.swing.docking.RelativeDockablePosition;
import com.vlsolutions.swing.docking.TabbedDockableContainer;

public class RailcomPlusController implements NodeListListener, RailcomPlusControllerListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(RailcomPlusController.class);

    private static final int CV29 = 29;

    private static final int CV1 = 1;

    private static final int CV17 = 17;

    private static final int CV18 = 18;

    private DefaultMessageListener messageListener;

    private RailcomPlusViewListener railcomPlusViewListener;

    private RailcomPlusModel railcomPlusModel;

    private RailcomPlusView railcomPlusView;

    private final DockingDesktop desktop;

    private final Node node;

    private final MainModel mainModel;

    public RailcomPlusController(final MainModel mainModel, final DockingDesktop desktop, final Node node) {
        this.mainModel = mainModel;
        this.desktop = desktop;
        this.node = node;
    }

    public void start(final MainControllerInterface mainController) {

        // check if the railcom view is already opened
        String searchKey = DockKeys.RAILCOM_PLUS_VIEW;
        LOGGER.info("Search for view with key: {}", searchKey);
        Dockable view = desktop.getContext().getDockableByKey(searchKey);
        if (view != null) {
            LOGGER.info("Select the existing railcom plus view instead of open a new one.");
            DockUtils.selectWindow(view);
            return;
        }

        LOGGER.info("Create new RailcomPlusView.");
        railcomPlusModel = new RailcomPlusModel();

        railcomPlusView = new RailcomPlusView(desktop, railcomPlusModel, this);

        DockableState[] dockables = desktop.getDockables();
        LOGGER.info("Current dockables: {}", new Object[] { dockables });
        if (dockables.length > 1) {

            DockableState tabPanelNodeDetails = null;
            // search the node details tab panel
            for (DockableState dockable : dockables) {

                if (DockKeys.DOCKKEY_TAB_PANEL.equals(dockable.getDockable().getDockKey())) {
                    LOGGER.info("Found the tab panel dockable.");
                    tabPanelNodeDetails = dockable;

                    break;
                }
            }

            Dockable dock = desktop.getDockables()[1].getDockable();
            if (tabPanelNodeDetails != null) {
                LOGGER.info("Add the railcom plus view to the node details panel.");
                dock = tabPanelNodeDetails.getDockable();

                TabbedDockableContainer container = DockingUtilities.findTabbedDockableContainer(dock);
                int order = 0;
                if (container != null) {
                    order = container.getTabCount();
                }
                LOGGER.info("Add new railcomPlusView at order: {}", order);

                desktop.createTab(dock, railcomPlusView, order, true);
            }
            else {
                desktop.split(dock, railcomPlusView, DockingConstants.SPLIT_RIGHT);
            }
        }
        else {
            desktop.addDockable(railcomPlusView, RelativeDockablePosition.RIGHT);
        }

        final Communication communication = CommunicationFactory.getInstance();

        railcomPlusView.addRailcomPlusViewListener(railcomPlusViewListener = new RailcomPlusViewListener() {

            private Node getCommandStationNode() {

                // search the command station node
                Collection<Node> nodes = mainController.getNodes();
                Node commandStationNode = null;
                for (Node node : nodes) {
                    if (node.isCommandStation()) {
                        commandStationNode = node;
                        LOGGER.info("Found a command station node: {}", commandStationNode);

                        break;
                    }
                }
                return commandStationNode;
            }

            @Override
            public boolean isFeatureRcplusAvailable() {
                Node commandStationNode = getCommandStationNode();
                if (commandStationNode == null) {
                    LOGGER.warn("No command station node available! Operation aborted!");

                    // show dialog
                    JOptionPane.showMessageDialog(null,
                        Resources.getString(RailcomPlusController.class, "no_commandstation_available"));
                    return false;
                }

                Feature featureGenRcPlusAvailable =
                    IterableUtils.find(node.getNode().getFeatures(), new Predicate<Feature>() {
                        @Override
                        public boolean evaluate(Feature feature) {
                            return feature.isRequestedFeature(BidibLibrary.FEATURE_GEN_RCPLUS_AVAILABLE);
                        }
                    });
                if (featureGenRcPlusAvailable != null && featureGenRcPlusAvailable.getValue() > 0) {
                    LOGGER.info("The feature FEATURE_GEN_RCPLUS_AVAILABLE is available.");
                    return true;
                }

                LOGGER.warn("The feature FEATURE_GEN_RCPLUS_AVAILABLE is not available.");
                return false;
            }

            @Override
            public void readTid() {
                LOGGER.info("Query the TID.");

                Node commandStationNode = getCommandStationNode();
                if (commandStationNode == null) {
                    LOGGER.warn("No command station node available! Operation aborted!");

                    // show dialog
                    JOptionPane.showMessageDialog(null,
                        Resources.getString(RailcomPlusController.class, "no_commandstation_available"));
                    return;
                }

                // clear the current TID
                railcomPlusModel.setTid(null);

                railcomPlusView.addLog("Query the TID");

                communication.getRcPlusTid(commandStationNode.getNode());
            }

            @Override
            public void updateSid() {

                TidData tidData = railcomPlusModel.getTid();
                if (tidData != null) {

                    int sid = tidData.getSid();
                    sid++;
                    LOGGER.info("Incremented the SID: {}", sid);

                    Node commandStationNode = getCommandStationNode();
                    if (commandStationNode == null) {
                        LOGGER.warn("No command station node available! Operation aborted!");

                        // show dialog
                        JOptionPane.showMessageDialog(null,
                            Resources.getString(RailcomPlusController.class, "no_commandstation_available"));
                        return;
                    }

                    railcomPlusView.addLog("Set the new SID: " + sid + ", TID: " + tidData);

                    communication.setRcPlusTid(commandStationNode.getNode(), new TidData(tidData.getUniqueId(), sid));

                }
                else {
                    LOGGER.warn("No TID available! Operation aborted!");

                    // TODO add I18N
                    JOptionPane.showMessageDialog(null, "No TID available! Read TID before update SID!");
                }
            }

            @Override
            public void updateTid() {

                TidData tidData = railcomPlusModel.getTid();
                if (tidData != null) {
                    int sid = tidData.getSid();
                    DecoderUniqueIdData uniqueId = tidData.getUniqueId();
                    uniqueId = new DecoderUniqueIdData(/* uniqueId.getMun() + 1 */0x0D0C0B0A, uniqueId.getMid());
                    LOGGER.info("Incremented the TID: {}", uniqueId);

                    Node commandStationNode = getCommandStationNode();
                    if (commandStationNode == null) {
                        LOGGER.warn("No command station node available! Operation aborted!");

                        // show dialog
                        JOptionPane.showMessageDialog(null,
                            Resources.getString(RailcomPlusController.class, "no_commandstation_available"));
                        return;
                    }

                    tidData = new TidData(uniqueId, sid);

                    railcomPlusView.addLog("Set the new TID: " + tidData);

                    communication.setRcPlusTid(commandStationNode.getNode(), tidData);
                }
                else {
                    LOGGER.warn("No TID available! Operation aborted!");

                    // TODO add I18N
                    JOptionPane.showMessageDialog(null, "No TID available! Read TID before update SID!");
                }
            }

            public void pingOnce(RcPlusPhase phase) {
                Node commandStationNode = getCommandStationNode();
                if (commandStationNode == null) {
                    LOGGER.warn("No command station node available! Operation aborted!");

                    // show dialog
                    JOptionPane.showMessageDialog(null,
                        Resources.getString(RailcomPlusController.class, "no_commandstation_available"));
                    return;
                }

                railcomPlusView.addLog("Send PING ONCE with phase: " + phase);

                communication.sendPingOnce(commandStationNode.getNode(), phase);
            }

            @Override
            public void ping(int interval) {
                Node commandStationNode = getCommandStationNode();
                if (commandStationNode == null) {
                    LOGGER.warn("No command station node available! Operation aborted!");

                    // show dialog
                    JOptionPane.showMessageDialog(null,
                        Resources.getString(RailcomPlusController.class, "no_commandstation_available"));
                    return;
                }

                railcomPlusView.addLog("Send PING with interval: " + interval);

                communication.sendPing(commandStationNode.getNode(), interval);
            }

            @Override
            public void find(RcPlusPhase phase, DecoderUniqueIdData decoder) {
                Node commandStationNode = getCommandStationNode();
                if (commandStationNode == null) {
                    LOGGER.warn("No command station node available! Operation aborted!");

                    // show dialog
                    JOptionPane.showMessageDialog(null,
                        Resources.getString(RailcomPlusController.class, "no_commandstation_available"));
                    return;
                }

                railcomPlusView.addLog("Send FIND for decoder: " + decoder + ", phase: " + phase);

                communication.sendFind(commandStationNode.getNode(), phase, decoder);
            }

            @Override
            public void bind(RcPlusBindData bindData) {
                Node commandStationNode = getCommandStationNode();
                if (commandStationNode == null) {
                    LOGGER.warn("No command station node available! Operation aborted!");

                    // show dialog
                    JOptionPane.showMessageDialog(null,
                        Resources.getString(RailcomPlusController.class, "no_commandstation_available"));
                    return;
                }

                railcomPlusView.addLog("Send BIND with data: " + bindData);

                communication.sendBind(commandStationNode.getNode(), bindData);
            }

            @Override
            public void queryAddressMode(final RailcomPlusDecoderModel decoder) {

                LOGGER.info("Send POM request to query CV29 for decoder: {}", decoder);

                Node commandStationNode = getCommandStationNode();
                if (commandStationNode == null) {
                    LOGGER.warn("No command station node available! Operation aborted!");

                    // show dialog
                    JOptionPane.showMessageDialog(null,
                        Resources.getString(RailcomPlusController.class, "no_commandstation_available"));
                    return;
                }

                int cvNumber = CV29;
                DecoderIdAddressData addressData =
                    new DecoderIdAddressData(decoder.getDecoderMun(), decoder.getDecoderManufacturer());

                railcomPlusView.addLog("Query CV29 from decoder: " + decoder);

                PomAcknowledge pomAck =
                    communication.sendReadCvPomRequest(commandStationNode.getNode(), addressData,
                        CommandStationPom.RD_BYTE, cvNumber);
                LOGGER.info("Received pomAck: {}", pomAck);
            }

            @Override
            public void queryAddress(final RailcomPlusDecoderModel decoder, final List<Integer> cvNumbers) {

                LOGGER.info("Send POM request to query address for decoder: {}, cvNumbers: {}", decoder, cvNumbers);

                Node commandStationNode = getCommandStationNode();
                if (commandStationNode == null) {
                    LOGGER.warn("No command station node available! Operation aborted!");

                    // show dialog
                    JOptionPane.showMessageDialog(null,
                        Resources.getString(RailcomPlusController.class, "no_commandstation_available"));
                    return;
                }

                for (int cvNumber : cvNumbers) {
                    DecoderIdAddressData addressData =
                        new DecoderIdAddressData(decoder.getDecoderMun(), decoder.getDecoderManufacturer());
                    LOGGER.info("Query CV{} from decoder: {}", cvNumber, decoder);

                    railcomPlusView.addLog("Query CV" + cvNumber + " from decoder: " + decoder);

                    PomAcknowledge pomAck =
                        communication.sendReadCvPomRequest(commandStationNode.getNode(), addressData,
                            CommandStationPom.RD_BYTE, cvNumber);
                    LOGGER.info("Received pomAck: {}", pomAck);
                }
            }
        });

        try {
            // Create the message listener for the railcom plus messages
            messageListener = new DefaultMessageListener() {

                @Override
                public void csRcPlusTid(final byte[] address, final TidData tid) {
                    LOGGER.info("Received the TID: {}, address: {}", tid, address);

                    railcomPlusView.addLog("Received TID: " + tid);

                    if (SwingUtilities.isEventDispatchThread()) {
                        railcomPlusModel.setTid(tid);
                    }
                    else {
                        SwingUtilities.invokeLater(new Runnable() {

                            @Override
                            public void run() {
                                railcomPlusModel.setTid(tid);
                            }
                        });
                    }
                }

                @Override
                public void csRcPlusPingAcknState(byte[] address, RcPlusPhase phase, RcPlusAcknowledge acknState) {
                    LOGGER.info("Received the ping ackn state: {}, phase: {}, address:{}", acknState, phase, address);
                    // no implementation

                    railcomPlusView.addLog("Received PING ACK state, phase: " + phase + ", acknState: " + acknState);
                }

                @Override
                public void csRcPlusBindAnswer(byte[] address, RcPlusDecoderAnswerData decoderAnswer) {
                    LOGGER.info("Received the bind answer: {}, address:{}", decoderAnswer, address);
                    // no implementation

                    railcomPlusView.addLog("Received BIND answer for decoder: " + decoderAnswer);
                }

                @Override
                public void csRcPlusFindAnswer(
                    byte[] address, RcPlusPhase phase, RcPlusDecoderAnswerData decoderAnswer) {
                    LOGGER.info("Received the find answer: {}, phase: {}, address:{}", decoderAnswer, phase, address);
                    // no implementation

                    railcomPlusView.addLog("Received FIND answer for phase: " + phase + ", decoder: " + decoderAnswer);

                }

                @Override
                public void feedbackRcPlusBindAccepted(
                    byte[] address, int detectorNum, RcPlusDecoderType decoderType,
                    RcPlusFeedbackBindData rcPlusBindAccepted) {

                    LOGGER.info(
                        "Received the feedback bind accepted answer: {}, detectorNum: {}, decoderType: {}, address:{}",
                        rcPlusBindAccepted, detectorNum, decoderType, address);

                    railcomPlusView.addLog("Received BIND ACCEPTED, decoderType: " + decoderType
                        + ", rcPlusBindAccepted: " + rcPlusBindAccepted);

                }

                @Override
                public void feedbackRcPlusPongNew(
                    byte[] address, int detectorNum, RcPlusPhase phase, RcPlusDecoderType decoderType,
                    DecoderUniqueIdData uniqueId) {

                    LOGGER.info(
                        "Received the feedback PONG NEW, uniqueId: {}, detectorNum: {}, decoderType: {}, address:{}",
                        uniqueId, detectorNum, decoderType, address);

                    RailcomPlusDecoderModel decoder = new RailcomPlusDecoderModel();
                    decoder.setDecoderMun(uniqueId.getMun());
                    decoder.setDecoderManufacturer(uniqueId.getMid());

                    railcomPlusView.addLog("Received PONG NEW for decoder: " + decoder);

                    railcomPlusModel.addDecoder(decoder);

                    railcomPlusView.addLog("Send query address for decoder: " + decoder);

                    // get the current assigned address if the decoder is new
                    fireQueryAddressMode(decoder);
                }

                @Override
                public void feedbackRcPlusPongOkay(
                    byte[] address, int detectorNum, RcPlusPhase phase, RcPlusDecoderType decoderType,
                    DecoderUniqueIdData uniqueId) {

                    LOGGER.info(
                        "Received the feedback PONG OKAY, uniqueId: {}, detectorNum: {}, decoderType: {}, address:{}",
                        uniqueId, detectorNum, decoderType, address);

                    RailcomPlusDecoderModel decoder = new RailcomPlusDecoderModel();
                    decoder.setDecoderMun(uniqueId.getMun());
                    decoder.setDecoderManufacturer(uniqueId.getMid());

                    railcomPlusView.addLog("Received PONG OKAY for decoder: " + decoder);

                    railcomPlusModel.addDecoder(decoder);

                    // fireQueryAddress(decoder);
                }

                @Override
                public void feedbackXPom(byte[] address, AddressData decoderAddress, int cvNumber, int[] dat) {
                    LOGGER.info(
                        "CV is signaled from feedback device, address: {} , decoderAddress: {}, cvNumber: {}, dat: {}",
                        address, decoderAddress, cvNumber, dat);

                    railcomPlusView.addLog("Received XPOM for decoderAddress: " + decoderAddress + ", CV: " + cvNumber
                        + ", value: " + ByteUtils.bytesToHex(dat));
                }

                @Override
                public void feedbackXPom(byte[] address, DecoderIdAddressData did, int cvNumber, int[] dat) {
                    LOGGER.info("CV is signaled from feedback device, address: {} , did: {}, cvNumber: {}, dat: {}",
                        address, did, cvNumber, dat);

                    railcomPlusView.addLog("Received XPOM for did: " + did + ", CV: " + cvNumber + ", value: "
                        + ByteUtils.bytesToHex(dat));

                    if (cvNumber == CV29) {
                        LOGGER.info("Received CV29, value: {}, bit 5: {}", ByteUtils.bytesToHex(dat, 1),
                            ByteUtils.getBit(dat[0], 5));

                        AddressMode addressMode = AddressMode.valueOf(ByteUtils.getBit(dat[0], 5));
                        LOGGER.info("Current addressMode: {}", addressMode);

                        final List<Integer> cvNumbers = new ArrayList<>();
                        switch (addressMode) {
                            case LONG:
                                cvNumbers.add(CV17);
                                cvNumbers.add(CV18);
                                break;
                            default:
                                cvNumbers.add(CV1);
                                break;
                        }

                        RailcomPlusDecoderModel decoder = railcomPlusModel.findDecoder(did);

                        if (decoder != null) {
                            decoder.setDecoderAddressMode(addressMode);

                            fireQueryAddress(decoder, cvNumbers);
                        }
                        else {
                            LOGGER.error("Get address is skipped because no matching decoder found for did: {}", did);

                            railcomPlusView
                                .addLog("Get address is skipped because no matching decoder is found for did: " + did);
                        }
                    }
                    else {
                        RailcomPlusDecoderModel decoder = railcomPlusModel.findDecoder(did);

                        if (cvNumber == CV1 || cvNumber == CV17) {
                            int cvValue = dat[0];
                            // low byte of address
                            LOGGER.info("Received CV{}", cvNumber, cvValue);

                            if (decoder != null) {
                                decoder.setDecoderAddressLowValue(cvValue);
                            }
                        }
                        else if (cvNumber == CV18) {
                            int cvValue = dat[0];
                            // high byte of address
                            LOGGER.info("Received CV{}", cvNumber, cvValue);

                            if (decoder != null) {
                                decoder.setDecoderAddressHighValue(cvValue);
                            }
                        }

                        if (decoder != null && decoder.isAddressComplete()) {
                            LOGGER.info("The address is complete: {}", decoder.getDecoderAddress());

                            railcomPlusModel.decoderChanged(decoder);
                        }
                    }
                }
            };

            CommunicationFactory.addMessageListener(messageListener);

        }
        catch (Exception ex) {
            LOGGER.warn("Add message listener for railcom+ messages failed.", ex);
        }

        railcomPlusView.addDecoderAddressListener(new DecoderAddressListener() {

            private Node getCommandStationNode() {

                // search the command station node
                Collection<Node> nodes = mainController.getNodes();
                Node commandStationNode = null;
                for (Node node : nodes) {
                    if (node.isCommandStation()) {
                        commandStationNode = node;
                        LOGGER.info("Found a command station node: {}", commandStationNode);

                        break;
                    }
                }
                return commandStationNode;
            }

            @Override
            public void setAddress(RailcomPlusDecoderModel decoder) {

                LOGGER.info("Set address of decoder: {}", decoder);

                Node commandStationNode = getCommandStationNode();
                if (commandStationNode == null) {
                    LOGGER.warn("No command station node available! Operation aborted!");

                    // show dialog
                    JOptionPane.showMessageDialog(null,
                        Resources.getString(RailcomPlusController.class, "no_commandstation_available"));
                    return;
                }

                if (decoder.getDecoderAddress() != null && decoder.getDecoderAddress() > 0) {

                    try {
                        DecoderUniqueIdData uniqueIdData =
                            new DecoderUniqueIdData(decoder.getDecoderMun(), decoder.getDecoderManufacturer());
                        AddressData address =
                            new AddressData(decoder.getDecoderAddress(), AddressTypeEnum.LOCOMOTIVE_FORWARD);

                        RcPlusBindData bindData = new RcPlusBindData(uniqueIdData, address);

                        railcomPlusView.addLog("Send BIND for decoder: " + decoder + ", bindData: " + bindData);

                        communication.sendBind(commandStationNode.getNode(), bindData);
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Set decoder address failed.", ex);
                    }
                }
                else {
                    LOGGER.warn("No address specified for decoder: {}", decoder);

                    // show dialog
                    JOptionPane.showMessageDialog(null,
                        Resources.getString(RailcomPlusController.class, "no_address_specified"));
                }

            }

            @Override
            public void queryAddress(RailcomPlusDecoderModel decoder) {

                LOGGER.info("Query address of decoder: {}", decoder);

                fireQueryAddressMode(decoder);
            }
        });

        // add the node list listener
        mainModel.addNodeListListener(this);

        railcomPlusView.initialize();
    }

    private void fireQueryAddressMode(final RailcomPlusDecoderModel decoder) {
        LOGGER.info("Query the decoder address for decoder: {}", decoder);

        if (railcomPlusViewListener != null) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {

                    LOGGER.info("Query decoder address mode.");
                    railcomPlusViewListener.queryAddressMode(decoder);
                }
            });
        }
    }

    private void fireQueryAddress(final RailcomPlusDecoderModel decoder, final List<Integer> cvNumbers) {
        LOGGER.info("Query the decoder address for decoder: {}, cvNumbers: {}", decoder, cvNumbers);

        if (railcomPlusViewListener != null) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {

                    LOGGER.info("Query decoder address.");
                    railcomPlusViewListener.queryAddress(decoder, cvNumbers);
                }
            });
        }
    }

    @Override
    public void listChanged() {
        LOGGER.info("The node list has changed.");
    }

    @Override
    public void nodeChanged() {
        LOGGER.debug("The node has changed, current node in model: {}", node);
        if (SwingUtilities.isEventDispatchThread()) {
            internalNodeChanged();
        }
        else {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    internalNodeChanged();
                }
            });
        }
    }

    private void internalNodeChanged() {

        LOGGER.debug("handle node has changed, node: {}", node);

        if (node != null && node.equals(mainModel.getSelectedNode())) {
            LOGGER.debug("The node in the model has not changed.");
            return;
        }

        if (railcomPlusView != null) {
            LOGGER.info("Close the railcomPlusView panel: {}", railcomPlusView);

            desktop.close(railcomPlusView);
        }

        // unregister node list listener
        mainModel.removeNodeListListener(this);
    }

    @Override
    public void nodeStateChanged() {
    }

    @Override
    public void listNodeAdded(Node node) {
    }

    @Override
    public void listNodeRemoved(Node node) {
    }

    @Override
    public void viewClosed() {
        LOGGER.info("The railcom plus view was closed. Cleanup the listeners.");

        if (messageListener != null) {
            LOGGER.info("Remove the message listener from communication factory.");
            CommunicationFactory.removeMessageListener(messageListener);

            messageListener = null;
        }
    }

    @Override
    public void nodeWillChange() {

    }
}
