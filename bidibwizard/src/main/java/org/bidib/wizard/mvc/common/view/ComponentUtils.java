package org.bidib.wizard.mvc.common.view;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;

public class ComponentUtils {

    public static <T extends JComponent> List<T> harvestComponents(Container c, Class<T> clazz) {
        List<T> jLabels = new ArrayList<T>();
        harvestComponents(c, clazz, jLabels);
        return jLabels;
    }

    public static <T extends JComponent> void harvestComponents(Container c, Class<T> clazz, List<T> l) {
        Component[] components = c.getComponents();
        for (Component com : components) {
            if (clazz.isAssignableFrom(com.getClass())) {
                l.add((T) com);
            }
            else if (com instanceof Container) {
                harvestComponents((Container) com, clazz, l);
            }
        }
    }

    /**
     * Find the first component of the specified class in the container.
     * 
     * @param c
     *            the container
     * @param clazz
     *            the class
     * @return the first found component or null if none was found
     */
    public static <T> T harvestComponent(Container c, Class<T> clazz) {
        Component[] components = c.getComponents();
        for (Component com : components) {
            if (clazz.isAssignableFrom(com.getClass())) {
                return ((T) com);
            }
            else if (com instanceof Container) {
                T result = harvestComponent((Container) com, clazz);
                if (result != null) {
                    return result;
                }
            }
        }
        return null;
    }

    public static void centerComponentOnScreen(Component component) {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension d = toolkit.getScreenSize();

        Point p = new Point();
        p.x += ((d.width - component.getWidth()) / 2);
        p.y += ((d.height - component.getHeight()) / 2);

        if (p.x < 0) {
            p.x = 0;
        }

        if (p.y < 0) {
            p.y = 0;
        }

        component.setLocation(p);
    }

}
