package org.bidib.wizard.mvc.railcomplus.controller.listener;

public interface RailcomPlusControllerListener {
    /**
     * The view was closed.
     */
    void viewClosed();
}
