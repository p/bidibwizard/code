package org.bidib.wizard.mvc.main.view.panel;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.TableColumn;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.bidib.cvexchange.CVSType;
import org.bidib.cvexchange.SaveCV;
import org.bidib.jbidibc.core.helpers.Context;
import org.bidib.jbidibc.core.helpers.DefaultContext;
import org.bidib.jbidibc.core.node.ConfigurationVariable;
import org.bidib.jbidibc.exchange.vendorcv.CVType;
import org.bidib.jbidibc.exchange.vendorcv.DataType;
import org.bidib.jbidibc.exchange.vendorcv.ModeType;
import org.bidib.jbidibc.exchange.vendorcv.VendorCV;
import org.bidib.jbidibc.exchange.vendorcv.VendorCvError;
import org.bidib.jbidibc.exchange.vendorcv.VendorCvFactory;
import org.bidib.jbidibc.exchange.vendorcv.VersionInfoType;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.common.utils.ImageUtils;
import org.bidib.wizard.dialog.FileDialog;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.bidib.wizard.mvc.common.view.cvdefinition.CvDefinitionTreeTableModel;
import org.bidib.wizard.mvc.common.view.cvdefinition.CvDefintionPanelProvider;
import org.bidib.wizard.mvc.common.view.cvdefinition.CvValueUtils;
import org.bidib.wizard.mvc.common.view.validation.IconFeedbackPanel;
import org.bidib.wizard.mvc.common.view.validation.PropertyValidationI18NSupport;
import org.bidib.wizard.mvc.main.controller.MainController;
import org.bidib.wizard.mvc.main.controller.MainControllerInterface;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.model.listener.CvDefinitionListener;
import org.bidib.wizard.mvc.main.model.listener.CvDefinitionRequestListener;
import org.bidib.wizard.mvc.main.view.component.LabeledDisplayItems;
import org.bidib.wizard.mvc.main.view.cvdef.CvBitfieldValueEditor;
import org.bidib.wizard.mvc.main.view.cvdef.CvByteValueEditor;
import org.bidib.wizard.mvc.main.view.cvdef.CvDccLongAddrValueEditor;
import org.bidib.wizard.mvc.main.view.cvdef.CvIntegerValueEditor;
import org.bidib.wizard.mvc.main.view.cvdef.CvLongValueEditor;
import org.bidib.wizard.mvc.main.view.cvdef.CvNode;
import org.bidib.wizard.mvc.main.view.cvdef.CvRadioValueEditor;
import org.bidib.wizard.mvc.main.view.cvdef.CvSignedCharValueEditor;
import org.bidib.wizard.mvc.main.view.cvdef.CvValueEditor;
import org.bidib.wizard.mvc.main.view.cvdef.CvValueNumberEditor;
import org.bidib.wizard.mvc.main.view.cvdef.CvValueStringEditor;
import org.bidib.wizard.mvc.main.view.cvdef.DccAddrRGEditor;
import org.bidib.wizard.mvc.main.view.cvdef.DeviceNode;
import org.bidib.wizard.mvc.main.view.cvdef.GBM16TReverserEditor;
import org.bidib.wizard.mvc.main.view.cvdef.LongCvNode;
import org.bidib.wizard.mvc.main.view.cvdef.LongNodeNode;
import org.bidib.wizard.mvc.main.view.cvdef.NodeNode;
import org.bidib.wizard.mvc.main.view.panel.listener.TabSelectionListener;
import org.bidib.wizard.mvc.main.view.panel.listener.TabStatusListener;
import org.bidib.wizard.mvc.main.view.statusbar.StatusBar;
import org.bidib.wizard.mvc.main.view.table.CvDefinitionJideTreeTable;
import org.bidib.wizard.mvc.preferences.model.Preferences;
import org.bidib.wizard.utils.CvExchangeFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.beans.Model;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.value.Trigger;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.builder.ButtonBarBuilder;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.debug.FormDebugPanel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.DefaultComponentFactory;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.validation.ValidationResultModel;
import com.jgoodies.validation.util.DefaultValidationResultModel;
import com.jgoodies.validation.util.PropertyValidationSupport;
import com.jgoodies.validation.view.ValidationComponentUtils;
import com.jidesoft.grid.DefaultExpandableRow;
import com.vlsolutions.swing.docking.DockingDesktop;
import com.vlsolutions.swing.toolbars.ToolBarConstraints;
import com.vlsolutions.swing.toolbars.ToolBarPanel;
import com.vlsolutions.swing.toolbars.VLToolBar;

public class CvDefinitionPanel extends JPanel
    implements TabSelectionListener, CvDefintionPanelProvider, CvDefinitionRequestListenerAware {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(CvDefinitionPanel.class);

    private static final String NONE = "none";

    private List<ConfigurationVariable> configVariables;

    private CvDefinitionTreeTableModel treeModel;

    private JTextField textXmlFileName;

    private JTextField textVersion;

    private JTextField textLastUpdate;

    private JTextField textAuthor;

    private JTextField textDescription;

    private final CvDefinitionJideTreeTable cvTreeTable;

    private JButton saveButton;

    private CardLayout cardLayout;

    private EditorPanels editorPanels;

    private Map<String, CvNode> cvNumberToNodeMap = new LinkedHashMap<String, CvNode>();

    private static final String READ = "read";

    private static final String WRITE = "write";

    private static final String LOAD = "loadFromFile";

    private static final String SAVE = "saveToFile";

    private JButton readButton;

    private JButton writeButton;

    private JButton loadFromFileButton;

    private JButton saveToFileButton;

    private VLToolBar toolbarCvDefinition;

    private final TabStatusListener tabStatusListener;

    private final MainModel mainModel;

    private List<CvDefinitionRequestListener> cvDefinitionRequestListeners =
        new LinkedList<CvDefinitionRequestListener>();

    public static final class CvDefinitionFileValueBean extends Model {
        private static final long serialVersionUID = 1L;

        public static final String PROPERTYNAME_FILENAME = "fileName";

        public static final String PROPERTYNAME_FILEPATH = "filePath";

        private String fileName;

        private String filePath;

        /**
         * @return the fileName
         */
        public String getFileName() {
            return fileName;
        }

        /**
         * @param fileName
         *            the fileName to set
         */
        public void setFileName(String fileName) {
            String oldValue = this.fileName;
            this.fileName = fileName;

            firePropertyChange(PROPERTYNAME_FILENAME, oldValue, fileName);
        }

        /**
         * @return the filePath
         */
        public String getFilePath() {
            return filePath;
        }

        /**
         * @param filePath
         *            the filePath to set
         */
        public void setFilePath(String filePath) {
            String oldValue = this.filePath;
            this.filePath = filePath;
            firePropertyChange(PROPERTYNAME_FILEPATH, oldValue, filePath);
        }

        public boolean isUserDefinedFile() {
            return StringUtils.isNotEmpty(filePath) && !filePath.startsWith("/bidib/");
        }
    }

    public CvDefinitionPanel(final MainModel mainModel, final TabStatusListener tabStatusListener) {
        this.tabStatusListener = tabStatusListener;
        this.mainModel = mainModel;

        setLayout(new BorderLayout());
        setBorder(new EmptyBorder(5, 5, 5, 5));

        LOGGER.debug("Create the CV tree.");

        cvTreeTable = new CvDefinitionJideTreeTable();
        // cvTreeTable.setShowsRootHandles(false);
        cvTreeTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        cvTreeTable.getTableHeader().setReorderingAllowed(false);

        final ListSelectionModel listSelectionModel = cvTreeTable.getSelectionModel();
        listSelectionModel.addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent e) {

                LOGGER.info("list selection, e: {}", e);
                if (e.getValueIsAdjusting()) {
                    return;
                }

                CvNode cvNode = null;

                final int index = cvTreeTable.getSelectedRow();
                DefaultExpandableRow row = (DefaultExpandableRow) cvTreeTable.getRowAt(index);

                LOGGER.info("Selection has changed, current index: {}, row: {}", index, row);

                // special handling for JideLongNodeNode
                if (row != null) {
                    if (row instanceof LongNodeNode) {
                        LongNodeNode longNodeNode = (LongNodeNode) row;
                        LongCvNode masterNode = longNodeNode.getSubCvMasterNode();
                        if (masterNode != null) {
                            LOGGER.info("Found masterNode to select: {}", masterNode);
                            cvNode = masterNode;

                            // expand the master node
                            SwingUtilities.invokeLater(new Runnable() {
                                @Override
                                public void run() {
                                    cvTreeTable.expandRow(index, true);
                                }
                            });
                        }
                    }
                    else if (row instanceof CvNode) {
                        cvNode = (CvNode) row;
                    }
                }

                if (cvNode != null) {
                    LOGGER.info("Show node in editor: {}", cvNode);

                    DataType dataTypeName = cvNode.getCV().getType();
                    editorPanels.show(dataTypeName.name());

                    // the save button is disabled by default
                    saveButton.setEnabled(false);
                    switch (dataTypeName) {
                        case INT:
                            PropertyConnector.connect(integerEditor.getSaveButtonEnabledModel(), "value", saveButton,
                                "enabled");
                            integerEditor.setValue(cvNode, cvNumberToNodeMap);
                            break;
                        case BYTE:
                            PropertyConnector.connect(byteEditor.getSaveButtonEnabledModel(), "value", saveButton,
                                "enabled");
                            byteEditor.setValue(cvNode, cvNumberToNodeMap);
                            break;
                        case BIT:
                            PropertyConnector.connect(bitfieldEditor.getSaveButtonEnabledModel(), "value", saveButton,
                                "enabled");
                            bitfieldEditor.setValue(cvNode, cvNumberToNodeMap);
                            break;
                        case SIGNED_CHAR:
                            PropertyConnector.connect(signedCharEditor.getSaveButtonEnabledModel(), "value", saveButton,
                                "enabled");
                            signedCharEditor.setValue(cvNode, cvNumberToNodeMap);
                            break;
                        case DCC_ADDR_RG:
                            PropertyConnector.connect(dccAddrRGEditor.getSaveButtonEnabledModel(), "value", saveButton,
                                "enabled");
                            dccAddrRGEditor.setValue(cvNode, cvNumberToNodeMap);
                            break;
                        case GBM_16_T_REVERSER:
                            PropertyConnector.connect(gbm16TReverserEditor.getSaveButtonEnabledModel(), "value",
                                saveButton, "enabled");
                            gbm16TReverserEditor.setValue(cvNode, cvNumberToNodeMap);
                            break;
                        case RADIO:
                            PropertyConnector.connect(radioEditor.getSaveButtonEnabledModel(), "value", saveButton,
                                "enabled");
                            radioEditor.setValue(cvNode, cvNumberToNodeMap);
                            break;
                        case LONG:
                            PropertyConnector.connect(longEditor.getSaveButtonEnabledModel(), "value", saveButton,
                                "enabled");
                            longEditor.setValue(cvNode, cvNumberToNodeMap);
                            break;
                        case DCC_LONG_ADDR:
                            PropertyConnector.connect(dccLongAddrEditor.getSaveButtonEnabledModel(), "value",
                                saveButton, "enabled");
                            dccLongAddrEditor.setValue(cvNode, cvNumberToNodeMap);
                            break;
                        case STRING:
                            PropertyConnector.connect(stringEditor.getSaveButtonEnabledModel(), "value", saveButton,
                                "enabled");
                            stringEditor.setValue(cvNode, cvNumberToNodeMap);
                            break;
                        default:
                            break;
                    }

                }
                else {
                    LOGGER.info("Non cvNode was selected.");
                    editorPanels.show(NONE);
                }
            }
        });
        cvTreeTable.addMouseListener(new MouseAdapter() {
            public void mouseReleased(MouseEvent e) {
                potentiallyShowPopup(e);
            }

            public void mousePressed(MouseEvent e) {
                potentiallyShowPopup(e);
            }
        });

        mainModel.addCvDefinitionListener(new CvDefinitionListener() {

            @Override
            public void cvDefinitionChanged() {
                // clear and release stored cv list
                if (configVariables != null) {
                    configVariables.clear();
                    configVariables = null;
                }

                editorPanels.show(NONE);

                if (readButton != null) {
                    readButton.setEnabled(false);
                }
                if (writeButton != null) {
                    writeButton.setEnabled(false);
                }

                if (loadFromFileButton != null) {
                    loadFromFileButton.setEnabled(false);
                }

                cvNumberToNodeMap.clear();

                Node node = mainModel.getSelectedNode();
                if (node == null) {
                    LOGGER.info("No node available.");
                    return;
                }

                CvDefinitionTreeTableModel cvDefinitionTreeTableModel = node.getCvDefinitionTreeTableModel();
                treeModel = cvDefinitionTreeTableModel;

                if (treeModel == null) {
                    return;
                }

                cvTreeTable.setModel(treeModel);

                for (int columnIndex = 0; columnIndex < cvTreeTable.getColumnCount(); columnIndex++) {

                    TableColumn column = cvTreeTable.getColumnModel().getColumn(columnIndex);
                    int modelIndex = column.getModelIndex();
                    switch (modelIndex) {
                        case CvNode.COLUMN_NUMBER:
                            column.setPreferredWidth(20);
                            break;
                        case CvNode.COLUMN_DESCRIPTION:
                            column.setPreferredWidth(250);
                            break;
                        default:
                            break;
                    }
                }

                cvTreeTable.expandAll();

                // fetch the current map from the node
                cvNumberToNodeMap = node.getCvNumberToJideNodeMap();
                configVariables = node.getConfigVariables();

                // clear the xml-file info
                // textXmlFileName.setText(null);
                textVersion.setText(null);
                textLastUpdate.setText(null);
                textAuthor.setText(null);
                textDescription.setText(null);

                definitionFileValueBean.setFileName(null);
                definitionFileValueBean.setFilePath(null);

                // set the config variables in the main model
                // this triggers the CvDefinitionListener.cvDefinitionValuesChanged()
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        LOGGER.info("Set the configVariables in the main model.");
                        mainModel.setConfigurationVariables(configVariables);

                        // create the tree nodes based on the provided vendor configuration
                        if (mainModel.getVendorCV() != null && mainModel.getVendorCV().getVendorCV() != null) {
                            VendorCV vendorCV = mainModel.getVendorCV().getVendorCV();
                            VersionInfoType versionInfoType = vendorCV.getVersion();
                            LOGGER.debug("versionInfoType: {}", versionInfoType);

                            String lastUpdate = versionInfoType.getLastupdate();
                            if (StringUtils.isNotBlank(lastUpdate)) {
                                try {
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                                    Date lastUpdateDate = sdf.parse(lastUpdate);
                                    lastUpdate = new SimpleDateFormat("dd.MM.yyyy").format(lastUpdateDate);
                                }
                                catch (ParseException ex) {
                                    LOGGER.warn("Parse last update date failed.", ex);
                                }
                            }

                            String fileName = "?";
                            try {
                                fileName = mainModel.getVendorCV().getFilename();
                            }
                            catch (Exception ex) {
                                LOGGER.warn("Prepare XML filename failed.", ex);
                            }
                            String filePath = "?";
                            try {
                                filePath = node.getVendorCV().getFilePath();
                            }
                            catch (Exception ex) {
                                LOGGER.warn("Prepare XML filePath failed.", ex);
                            }

                            // textXmlFileName.setText(fileName);
                            textXmlFileName.setToolTipText(filePath);
                            textVersion.setText(versionInfoType.getVersion());
                            textLastUpdate.setText(lastUpdate);
                            textAuthor.setText(versionInfoType.getAuthor());
                            textDescription.setText(versionInfoType.getDescription());

                            definitionFileValueBean.setFileName(fileName);
                            definitionFileValueBean.setFilePath(filePath);

                            LOGGER.info("Number of configuration variables for this node: {}", configVariables.size());

                            if (readButton != null) {
                                // enable the read button to get the values of the configuration variables for this node
                                // on request
                                readButton.setEnabled(configVariables.size() > 0);
                            }
                            if (writeButton != null) {
                                // enable the write button to write the values of the configuration variables for this
                                // node on request
                                writeButton.setEnabled(configVariables.size() > 0);
                            }

                            if (loadFromFileButton != null) {
                                loadFromFileButton.setEnabled(configVariables.size() > 0);
                            }

                            if (cvValidationModel != null) {
                                PropertyValidationSupport support =
                                    new PropertyValidationI18NSupport(definitionFileValueBean, "validation");

                                if (definitionFileValueBean.isUserDefinedFile()) {

                                    support.addWarning("xmlFileName_key",
                                        "filename_is_user_defined;" + definitionFileValueBean.filePath);
                                }
                                cvValidationModel.setResult(support.getResult());
                            }
                        }
                        else {
                            LOGGER.info("No vendor CV available.");
                        }
                    }
                });

            }

            @Override
            public void cvDefinitionValuesChanged(final boolean read) {
                LOGGER.info("The CV defintion values have changed, read: {}", read);

                // The new values are currently the same instances as we provided to fetch.
                // Thus the nodes have the new values already and only the tree refresh must be triggered.

                // refresh the tree with the new values
                // TreeTableNode root = treeModel.getRoot();
                // treeModel.refreshTreeTable(root);

                if (editorPanels.getCurrentPanel() instanceof CvValueEditor) {
                    // update the current values
                    CvValueEditor cvValueEditor = (CvValueEditor) editorPanels.getCurrentPanel();
                    cvValueEditor.refreshValueFromSource();
                }
            }
        });

        JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        JScrollPane scrollTree = new JScrollPane(cvTreeTable);
        splitPane.add(scrollTree);

        JPanel westPanel = new JPanel(new BorderLayout());
        westPanel.add(createXmFileInfoPanel(), BorderLayout.NORTH);
        westPanel.add(createCvEditorPanel(mainModel), BorderLayout.CENTER);

        JScrollPane scrollCvEditors = new JScrollPane(westPanel);
        splitPane.add(scrollCvEditors);

        add(splitPane, BorderLayout.CENTER);

        splitPane.setDividerLocation(800);

        DockingDesktop desktop =
            (DockingDesktop) DefaultApplicationContext.getInstance().get(DefaultApplicationContext.KEY_DESKTOP);
        toolbarCvDefinition = new VLToolBar("cvDefinition");
        addButtons(toolbarCvDefinition);
        addToolBar(toolbarCvDefinition, new ToolBarConstraints(0, 2));
        // initially invisible
        toolbarCvDefinition.setVisible(false);

        LabeledDisplayItems<Node> tree =
            DefaultApplicationContext.getInstance().get(DefaultApplicationContext.KEY_NODE_TREE,
                LabeledDisplayItems.class);
        if (tree instanceof NodeTree) {
            NodeTree nodeTree = (NodeTree) tree;
            LOGGER.info("Add VetoableChangeListener to nodeTree.");
            nodeTree.addVetoableChangeListener(new VetoableChangeListener() {

                @Override
                public void vetoableChange(PropertyChangeEvent evt) throws PropertyVetoException {
                    LOGGER.info("vetoableChange, evt: {}", evt);

                    if ("selectedNode".equals(evt.getPropertyName())) {
                        LOGGER.info("The selected node will change.");
                        boolean pendingChanges = CvDefinitionTreeHelper.hasPendingChanges(treeModel);

                        // show a dialog to allow discard the changes
                        if (pendingChanges) {
                            int result =
                                JOptionPane.showConfirmDialog(CvDefinitionPanel.this,
                                    Resources.getString(CvDefinitionPanel.class, "discardPendingChanges.text"),
                                    Resources.getString(CvDefinitionPanel.class, "discardPendingChanges.title"),
                                    JOptionPane.OK_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE);

                            if (result != JOptionPane.OK_OPTION) {
                                throw new PropertyVetoException("Pending changes in CV definition detected.", evt);
                            }

                            resetPendingChanges();
                        }
                    }

                }
            });
        }
        else {
            LOGGER.warn("The NodeTree is not an instance of NodeTree class!!!!");
        }
    }

    public void resetPendingChanges() {
        // reset the pending changes
        if (treeModel != null) {
            LOGGER.info("Reset pending changes on root node.");
            DefaultExpandableRow root = (DefaultExpandableRow) treeModel.getRoot();
            resetPendingChanges(root);
        }
    }

    private void addToolBar(final VLToolBar toolBar, ToolBarConstraints constraints) {
        ToolBarPanel topToolBarPanel =
            (ToolBarPanel) DefaultApplicationContext.getInstance().get(DefaultApplicationContext.KEY_TOPTOOLBARPANEL);
        topToolBarPanel.add(toolBar, constraints);
    }

    private void removeToolBar(final VLToolBar toolBar) {
        ToolBarPanel topToolBarPanel =
            (ToolBarPanel) DefaultApplicationContext.getInstance().get(DefaultApplicationContext.KEY_TOPTOOLBARPANEL);
        topToolBarPanel.remove(toolBar);
    }

    private boolean formDebugPanel;

    protected ValidationResultModel cvValidationModel;

    private CvDefinitionFileValueBean definitionFileValueBean = new CvDefinitionFileValueBean();

    private ImageIcon userDefinedImportIcon;

    private JPanel createXmFileInfoPanel() {

        FormLayout layout =
            new FormLayout("pref, 3dlu, min(30dlu;pref), 3dlu, pref, 3dlu, max(30dlu;pref), 3dlu, pref");

        DefaultFormBuilder builder = null;
        if (formDebugPanel) {
            builder = new DefaultFormBuilder(layout, new FormDebugPanel());
        }
        else {
            builder = new DefaultFormBuilder(layout);
        }

        // init the components

        ValueModel modeModel =
            new PropertyAdapter<CvDefinitionFileValueBean>(definitionFileValueBean,
                CvDefinitionFileValueBean.PROPERTYNAME_FILENAME, true);
        textXmlFileName = BasicComponentFactory.createTextField(modeModel, false);
        textXmlFileName.setColumns(20);
        textXmlFileName.setEditable(false);
        textVersion = new JTextField(7);
        textVersion.setEditable(false);
        textLastUpdate = new JTextField(20);
        textLastUpdate.setEditable(false);
        textAuthor = new JTextField(20);
        textAuthor.setEditable(false);
        textDescription = new JTextField(30);
        textDescription.setEditable(false);

        userDefinedImportIcon = ImageUtils.createImageIcon(NodeTree.class, "/icons/16x16/user-defined.png");

        final JButton importCvDefinitionButton = new JButton(userDefinedImportIcon);
        importCvDefinitionButton.setToolTipText(Resources.getString(getClass(), "import.tooltip"));
        importCvDefinitionButton.addActionListener(evt -> {
            LOGGER.info("Import user-defined CV definition.");
            importUserDefinedCvDefinition();
        });

        builder.appendSeparator(Resources.getString(getClass(), "fileinfo.title"));
        builder.append(Resources.getString(getClass(), "fileinfo.xmlfile"), textXmlFileName, 5);
        builder.append(importCvDefinitionButton);
        builder.nextLine();
        builder.append(Resources.getString(getClass(), "fileinfo.version"), textVersion);
        builder.append(Resources.getString(getClass(), "fileinfo.lastchange"), textLastUpdate, 3);
        builder.nextLine();
        builder.append(Resources.getString(getClass(), "fileinfo.author"), textAuthor, 7);
        builder.nextLine();
        builder.append(Resources.getString(getClass(), "fileinfo.description"), textDescription, 7);
        builder.nextLine();

        // add icons that show if user-defined cv definition is used
        ValidationComponentUtils.setMandatory(textXmlFileName, true);
        ValidationComponentUtils.setMessageKey(textXmlFileName, "validation.xmlFileName_key");

        // make insets
        builder.border(Borders.DIALOG);

        FormLayout feedbackLayout = new FormLayout("p:g");
        DefaultFormBuilder feedbackBuilder = new DefaultFormBuilder(feedbackLayout);

        cvValidationModel = new DefaultValidationResultModel();

        PropertyValidationSupport support = new PropertyValidationI18NSupport(definitionFileValueBean, "validation");

        // support.addWarning("xmlFileName_key", "filename is user defined");
        cvValidationModel.setResult(support.getResult());

        // Padding for overlay icon
        JComponent cvIconPanel = new IconFeedbackPanel(this.cvValidationModel, builder.getPanel());

        feedbackBuilder.appendRow("p");
        feedbackBuilder.add(cvIconPanel);

        return feedbackBuilder.getPanel();
    }

    private class EditorPanels extends JPanel {
        private static final long serialVersionUID = 1L;

        private Map<Object, Component> panels = new LinkedHashMap<Object, Component>();

        private String currentPanelName;

        public EditorPanels(CardLayout layout) {
            super(layout);
        }

        @Override
        public void add(Component comp, Object constraints) {
            LOGGER.debug("Add new panel to editor panels, name: {}, panel: {}", constraints, comp);
            panels.put(constraints, comp);
            if (currentPanelName == null) {
                currentPanelName = constraints.toString();
            }
            super.add(comp, constraints);
        }

        public void show(Object constraints) {
            LOGGER.debug("Show: {}", constraints);

            String newPanelName = constraints.toString();

            // detect if a panel is not available and show an empty panel in this case
            if (panels.containsKey(newPanelName)) {
                if (getLayout() instanceof CardLayout) {
                    ((CardLayout) getLayout()).show(this, constraints.toString());
                }
                currentPanelName = newPanelName;
            }
            else {
                LOGGER.warn("No panel available for constraint: {}", newPanelName);
                show(NONE);
            }
        }

        public JPanel getCurrentPanel() {
            if (currentPanelName != null) {
                return (JPanel) panels.get(currentPanelName);
            }
            return null;
        }
    }

    private JPanel createCvEditorPanel(MainModel mainModel) {
        JPanel editorContainer = new JPanel(new BorderLayout());
        editorContainer.setBorder(Borders.DIALOG);
        editorContainer.add(
            DefaultComponentFactory.getInstance().createSeparator(Resources.getString(getClass(), "editor.cveditor")),
            BorderLayout.NORTH);

        cardLayout = new CardLayout();
        // editorPanels = new EditorPanels(cardLayout);
        // editorContainer.add(editorPanels, BorderLayout.CENTER);

        saveButton = new JButton(Resources.getString(getClass(), "editor.save"));
        saveButton.setEnabled(false);
        saveButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                LOGGER.info("save pressed.");
                try {
                    Trigger trigger = getTrigger();
                    if (trigger == null) {
                        LOGGER.info("No trigger available. Do not save!");
                        return;
                    }
                    trigger.triggerCommit();

                    JPanel panel = editorPanels.getCurrentPanel();
                    if (panel instanceof CvValueEditor) {
                        CvValueEditor valueEditor = (CvValueEditor) panel;

                        CvNode cvNode = valueEditor.getCvNode();
                        LOGGER.debug("Current selected cvNode: {}", cvNode);
                        if (cvNode != null) {
                            // let the editor update the CV value(s) ...
                            valueEditor.updateCvValues(cvNumberToNodeMap, treeModel);

                            LOGGER.info("Refresh the treeModel.");

                            cvTreeTable.updateUI();

                            LOGGER.info("Refresh the treeModel finished.");

                            // hook in here and set the pending changes flag on the tab
                            if (CvDefinitionTreeHelper.hasPendingChanges(treeModel)) {
                                tabStatusListener.updatePendingChanges(CvDefinitionPanel.this, true);
                            }
                        }
                    }
                }
                catch (Exception ex) {
                    LOGGER.warn("Commit failed.", ex);
                }
            }
        });
        JButton resetButton = new JButton(Resources.getString(getClass(), "editor.reset"));
        resetButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                LOGGER.info("Reset was pressed.");
                Trigger trigger = getTrigger();
                if (trigger != null) {
                    trigger.triggerFlush();

                    // call 2x
                    trigger.triggerFlush();
                }
            }
        });
        // create the button panel
        JPanel buttonBar = new ButtonBarBuilder().addButton(saveButton, resetButton).build();
        editorContainer.add(buttonBar, BorderLayout.NORTH);

        // show the editor panels below the save and reset button
        editorPanels = new EditorPanels(cardLayout);
        editorContainer.add(editorPanels, BorderLayout.CENTER);

        // add the editor panels ...
        editorPanels.add(new JPanel(), NONE);
        editorPanels.add(createBitfieldEditor(), DataType.BIT.name());
        editorPanels.add(createByteEditor(), DataType.BYTE.name());
        editorPanels.add(createIntegerEditor(), DataType.INT.name());
        editorPanels.add(createSignedCharEditor(), DataType.SIGNED_CHAR.name());
        editorPanels.add(createRadioEditor(), DataType.RADIO.name());
        editorPanels.add(createLongEditor(), DataType.LONG.name());
        editorPanels.add(createDccLongAddrEditor(), DataType.DCC_LONG_ADDR.name());
        editorPanels.add(createStringEditor(), DataType.STRING.name());

        // add specific editor panels ...
        editorPanels.add(createDccAddrRGEditor(), DataType.DCC_ADDR_RG.name());
        editorPanels.add(createGBM16TReverserEditor(mainModel), DataType.GBM_16_T_REVERSER.name());

        return editorContainer;
    }

    private Trigger getTrigger() {
        JPanel currentEditor = editorPanels.getCurrentPanel();
        if (currentEditor instanceof CvValueEditor) {
            return ((CvValueEditor) currentEditor).getTrigger();
        }
        return null;
    }

    private GBM16TReverserEditor gbm16TReverserEditor;

    private JPanel createGBM16TReverserEditor(MainModel mainModel) {
        gbm16TReverserEditor = new GBM16TReverserEditor(mainModel);
        gbm16TReverserEditor.create(new Trigger());
        return gbm16TReverserEditor;
    }

    private DccAddrRGEditor dccAddrRGEditor;

    private JPanel createDccAddrRGEditor() {
        dccAddrRGEditor = new DccAddrRGEditor();
        dccAddrRGEditor.create(new Trigger());
        return dccAddrRGEditor;
    }

    private CvValueNumberEditor<Byte> byteEditor;

    private JPanel createByteEditor() {
        byteEditor = new CvByteValueEditor();
        byteEditor.setValueLabelPrefix("Byte");
        byteEditor.create(new Trigger());
        return byteEditor;
    }

    private CvValueNumberEditor<Byte> bitfieldEditor;

    private JPanel createBitfieldEditor() {
        bitfieldEditor = new CvBitfieldValueEditor();
        bitfieldEditor.create(new Trigger());
        return bitfieldEditor;
    }

    private CvValueNumberEditor<Byte> radioEditor;

    private JPanel createRadioEditor() {
        radioEditor = new CvRadioValueEditor();
        radioEditor.create(new Trigger());
        return radioEditor;
    }

    private CvValueNumberEditor<Integer> integerEditor;

    private JPanel createIntegerEditor() {
        integerEditor = new CvIntegerValueEditor();
        integerEditor.setValueLabelPrefix("Integer");
        integerEditor.create(new Trigger());
        return integerEditor;
    }

    private CvValueNumberEditor<Long> longEditor;

    private JPanel createLongEditor() {
        longEditor = new CvLongValueEditor();
        longEditor.setValueLabelPrefix("Long");
        longEditor.create(new Trigger());
        return longEditor;
    }

    private CvValueNumberEditor<Integer> dccLongAddrEditor;

    private JPanel createDccLongAddrEditor() {
        dccLongAddrEditor = new CvDccLongAddrValueEditor();
        dccLongAddrEditor.setValueLabelPrefix("DccLongAddr");
        dccLongAddrEditor.create(new Trigger());
        return dccLongAddrEditor;
    }

    private CvSignedCharValueEditor signedCharEditor;

    private JPanel createSignedCharEditor() {
        signedCharEditor = new CvSignedCharValueEditor();
        signedCharEditor.setValueLabelPrefix("Signed Char");
        signedCharEditor.create(new Trigger());
        return signedCharEditor;
    }

    private CvValueStringEditor stringEditor;

    private JPanel createStringEditor() {
        stringEditor = new CvValueStringEditor();
        stringEditor.setValueLabelPrefix("String");
        stringEditor.create(new Trigger());
        return stringEditor;
    }

    public String getName() {
        return Resources.getString(getClass(), "name");
    }

    private void readCvValues() {
        LOGGER.debug("Read all configured CV values.");

        // Get all configured CV values from the node
        readCvValues(configVariables);
    }

    private void readCvValues(List<ConfigurationVariable> configVariables) {
        LOGGER.debug("Read the CV values.");

        // Get the CV values from the node
        if (configVariables != null && configVariables.size() > 0) {
            LOGGER.info("Get the CV values for configuration variables, count: {}", configVariables.size());
            fireLoadConfigVariables(configVariables);
        }
        else {
            LOGGER.warn("No configuration variables available.");
        }
    }

    @Override
    public void checkPendingChanges() {
        boolean hasPendingChanges = CvDefinitionTreeHelper.hasPendingChanges(treeModel);
        if (!hasPendingChanges) {
            tabStatusListener.updatePendingChanges(this, false);
        }
    }

    private void resetPendingChanges(DefaultExpandableRow node) {
        if (node == null) {
            return;
        }
        for (int childIndex = 0; childIndex < node.getChildrenCount(); childIndex++) {
            DefaultExpandableRow child = (DefaultExpandableRow) node.getChildAt(childIndex);
            if (child instanceof CvNode) {
                CvNode cvNode = (CvNode) child;
                if (cvNode.getNewValue() != null) {
                    LOGGER.debug("Reset the pending changes on node: {}", cvNode);
                    cvNode.resetNewValue();
                }
            }
            else {
                resetPendingChanges(child);
            }
        }
    }

    /**
     * Add a new {@link ConfigurationVariable} item to the list if the new value is available.
     * 
     * @param cvNode
     *            the cvNode
     * @param cvList
     *            the list
     */
    private void addNewValueToList(CvNode cvNode, List<ConfigurationVariable> cvList) {
        if (cvNode.getNewValue() != null) {
            // we have a new value to write to the node.
            String cvNumber = cvNode.getConfigVar().getName();
            String cvValue = Objects.toString(cvNode.getNewValue(), null);

            ConfigurationVariable cv = new ConfigurationVariable(cvNumber, cvValue);
            cvList.add(cv);
        }
    }

    private void addButtons(VLToolBar toolBar) {

        // read button
        readButton =
            makeNavigationButton("load-from-node", "/32x32", READ, Resources.getString(getClass(), "toolbar.readallcv"),
                Resources.getString(getClass(), "toolbar.readallcv.alttext"));
        readButton.setEnabled(false);
        toolBar.add(readButton);

        readButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                readCvValues();
            }
        });

        // write button
        writeButton =
            makeNavigationButton("save-to-node", "/32x32", WRITE, Resources.getString(getClass(), "toolbar.writeallcv"),
                Resources.getString(getClass(), "toolbar.writeallcv.alttext"));
        toolBar.add(writeButton);
        writeButton.setEnabled(false);

        writeButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                LOGGER.info("Write the CV values.");

                // collect the new values from the tree
                DefaultExpandableRow root = (DefaultExpandableRow) treeModel.getRoot();

                final Node selectedNode = mainModel.getSelectedNode();

                // write the values to the node
                CvValueUtils.writeCvValues(selectedNode, root, cvNumberToNodeMap, CvDefinitionPanel.this);
            }
        });

        // load button
        loadFromFileButton =
            makeNavigationButton("loadfromfile", "/32x32", LOAD,
                Resources.getString(getClass(), "toolbar.loadfromfile"),
                Resources.getString(getClass(), "toolbar.loadfromfile.alttext"));
        toolBar.add(loadFromFileButton);
        loadFromFileButton.setEnabled(false);

        loadFromFileButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                loadFromFileCvValues();
            }
        });
        // save button
        saveToFileButton =
            makeNavigationButton("savetofile", "/32x32", SAVE.toLowerCase(),
                Resources.getString(getClass(), "toolbar.savetofile"),
                Resources.getString(getClass(), "toolbar.savetofile.alttext"));
        toolBar.add(saveToFileButton);
        saveToFileButton.setEnabled(false);

        saveToFileButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                saveToFileCvValues();
            }
        });

    }

    private JButton makeNavigationButton(
        String imageName, String pathExt, String actionCommand, String toolTipText, String altText) {
        // Look for the image.
        String imgLocation = "/icons/" + imageName + ".png";
        if (pathExt != null) {
            imgLocation = "/icons" + pathExt + "/" + imageName + ".png";
        }
        URL imageURL = CvDefinitionPanel.class.getResource(imgLocation);

        // Create and initialize the button.
        JButton button = new JButton();
        button.setActionCommand(actionCommand);
        button.setToolTipText(toolTipText);

        if (imageURL != null) { // image found
            button.setIcon(new ImageIcon(imageURL, altText));
        }
        else { // no image found
            button.setText(altText);
            LOGGER.warn("Resource not found: {}", imgLocation);
        }

        return button;
    }

    @Override
    public void addCvDefinitionRequestListener(CvDefinitionRequestListener l) {
        cvDefinitionRequestListeners.add(l);
    }

    private void fireLoadConfigVariables(List<ConfigurationVariable> configVariables) {
        // TODO decouple the AWT-thread from this work?

        for (CvDefinitionRequestListener l : cvDefinitionRequestListeners) {
            l.loadCvValues(configVariables);
        }
    }

    @Override
    public void writeConfigVariables(List<ConfigurationVariable> cvList) {
        fireWriteConfigVariables(cvList);
    }

    private void fireWriteConfigVariables(List<ConfigurationVariable> cvList) {
        // TODO decouple the AWT-thread from this work?

        for (CvDefinitionRequestListener l : cvDefinitionRequestListeners) {
            l.writeCvValues(cvList);
        }
    }

    private void potentiallyShowPopup(MouseEvent e) {
        JPopupMenu popupMenu = null;

        if (e.isPopupTrigger()) {
            int index = cvTreeTable.getSelectedRow();
            if (index > -1) {

                DefaultExpandableRow row = (DefaultExpandableRow) cvTreeTable.getRowAt(index);

                popupMenu = buildPopup(row);

                // Only show popup if we have any menu items in it
                if (popupMenu.getComponentCount() > 0) {
                    popupMenu.show((Component) e.getSource(), e.getX(), e.getY());
                }

            }
        }
    }

    private JPopupMenu buildPopup(final DefaultExpandableRow selectedComponent) {
        JPopupMenu pm = new JPopupMenu();
        // Based on selection context, install actions...
        if (selectedComponent instanceof CvNode) {
            CvNode cvNode = (CvNode) selectedComponent;
            JMenuItem readCv =
                new JMenuItem(new CvAction(ActionType.READ, Resources.getString(getClass(), "menu.readcv"), cvNode));
            pm.add(readCv);
            if (!ModeType.RO.equals(cvNode.getCV().getMode()) && !ModeType.H.equals(cvNode.getCV().getMode())) {
                JMenuItem writeCv =
                    new JMenuItem(
                        new CvAction(ActionType.WRITE, Resources.getString(getClass(), "menu.writecv"), cvNode));
                pm.add(writeCv);
            }
        }
        else if (selectedComponent instanceof NodeNode) {
            NodeNode nodeNode = (NodeNode) selectedComponent;
            JMenuItem readCv =
                new JMenuItem(
                    new NodeCvAction(ActionType.READ, Resources.getString(getClass(), "menu.readcvs"), nodeNode));
            pm.add(readCv);

            JMenuItem writeCv =
                new JMenuItem(
                    new NodeCvAction(ActionType.WRITE, Resources.getString(getClass(), "menu.writecvs"), nodeNode));
            pm.add(writeCv);
        }
        else if (selectedComponent instanceof DeviceNode) {
            DeviceNode deviceNode = (DeviceNode) selectedComponent;
            JMenuItem readCv =
                new JMenuItem(
                    new NodeCvAction(ActionType.READ, Resources.getString(getClass(), "menu.readcvs"), deviceNode));
            pm.add(readCv);

            JMenuItem writeCv =
                new JMenuItem(
                    new NodeCvAction(ActionType.WRITE, Resources.getString(getClass(), "menu.writecvs"), deviceNode));
            pm.add(writeCv);
        }

        return pm;
    }

    public enum ActionType {
        READ, WRITE
    };

    private class CvAction extends AbstractAction {
        private static final long serialVersionUID = 1L;

        private CvNode cvNode;

        private ActionType actionType;

        public CvAction(ActionType actionType, String name, CvNode cvNode) {
            super(name);
            this.actionType = actionType;
            this.cvNode = cvNode;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            LOGGER.debug("Add config var of selected cvNode: {}", cvNode);

            List<CvNode> cvNodes = new LinkedList<>();
            CVType cv = cvNode.getCV();
            switch (cv.getType()) {
                case INT:
                    CvNode highCvNode = cvNumberToNodeMap.get(cvNode.getCV().getHigh());
                    CvNode lowCvNode = cvNumberToNodeMap.get(cvNode.getCV().getLow());
                    cvNodes.add(highCvNode);
                    cvNodes.add(lowCvNode);
                    break;
                case LONG:
                    if (cvNode instanceof LongCvNode) {
                        LongCvNode masterCvNode = ((LongCvNode) cvNode).getMasterNode();
                        cvNodes.add(masterCvNode);
                        for (CvNode slaveNode : masterCvNode.getSlaveNodes()) {
                            cvNodes.add(slaveNode);
                        }
                    }
                    break;
                default:
                    cvNodes.add(cvNode);
                    break;
            }

            LOGGER.debug("Nodes to update: {}", cvNodes);

            switch (actionType) {
                case READ:
                    List<ConfigurationVariable> cvSet = new LinkedList<ConfigurationVariable>();
                    for (CvNode node : cvNodes) {
                        cvSet.add(node.getConfigVar());
                    }
                    readCvValues(cvSet);
                    break;
                case WRITE:
                    List<ConfigurationVariable> cvList = new LinkedList<ConfigurationVariable>();
                    for (CvNode node : cvNodes) {
                        if (!ModeType.RO.equals(node.getCV().getMode()) && !ModeType.H.equals(node.getCV().getMode())) {
                            addNewValueToList(node, cvList);
                        }
                    }
                    final Node selectedNode = mainModel.getSelectedNode();
                    CvValueUtils.writeCvValues(selectedNode, cvList, cvNumberToNodeMap, CvDefinitionPanel.this);

                    break;
                default:
                    break;
            }
        }
    }

    private class NodeCvAction extends AbstractAction {
        private static final long serialVersionUID = 1L;

        private DefaultExpandableRow nodeNode;

        private ActionType actionType;

        public NodeCvAction(ActionType actionType, String name, DefaultExpandableRow nodeNode) {
            super(name);
            this.actionType = actionType;
            this.nodeNode = nodeNode;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            LOGGER.debug("Add config var of selected cvNode: {}", nodeNode);

            List<CvNode> cvNodes = new LinkedList<CvNode>();
            addCvOfSubnode(nodeNode, cvNodes);

            LOGGER.debug("Nodes to update: {}", cvNodes);

            switch (actionType) {
                case READ:
                    List<ConfigurationVariable> cvSet = new LinkedList<ConfigurationVariable>();
                    for (CvNode node : cvNodes) {
                        cvSet.add(node.getConfigVar());
                    }
                    readCvValues(cvSet);
                    break;
                case WRITE:
                    List<ConfigurationVariable> cvList = new LinkedList<ConfigurationVariable>();
                    for (CvNode node : cvNodes) {
                        if (!ModeType.RO.equals(node.getCV().getMode()) && !ModeType.H.equals(node.getCV().getMode())) {
                            addNewValueToList(node, cvList);
                        }
                    }

                    final Node selectedNode = mainModel.getSelectedNode();
                    CvValueUtils.writeCvValues(selectedNode, cvList, cvNumberToNodeMap, CvDefinitionPanel.this);

                    break;
                default:
                    break;
            }

        }

        private void addCvOfSubnode(DefaultExpandableRow nodeNode, List<CvNode> cvNodes) {
            LOGGER.info("Process children of nodeNode: {}", nodeNode.getValueAt(0));
            // process the child nodes
            for (int index = 0; index < nodeNode.getChildrenCount(); index++) {

                DefaultExpandableRow childNode = (DefaultExpandableRow) nodeNode.getChildAt(index);
                if (childNode instanceof CvNode) {
                    CvNode cvNode = (CvNode) childNode;
                    CVType cv = cvNode.getCV();
                    switch (cv.getType()) {
                        case INT:
                            CvNode highCvNode = cvNumberToNodeMap.get(cvNode.getCV().getHigh());
                            CvNode lowCvNode = cvNumberToNodeMap.get(cvNode.getCV().getLow());
                            cvNodes.add(highCvNode);
                            cvNodes.add(lowCvNode);
                            break;
                        default:
                            cvNodes.add(cvNode);
                            break;
                    }
                }
                else if (childNode instanceof NodeNode) {
                    NodeNode currentNode = (NodeNode) childNode;
                    LOGGER.info("Found a NodeNode as child: {}", currentNode.getValueAt(0));
                    addCvOfSubnode(currentNode, cvNodes);
                }
            }
        }
    }

    private static FileFilter cvExchangeFilter;

    private static final String CV_EXCHANGE_EXTENSION = "xml";

    // description, suffix for node files
    private static String saveCvDescription;

    private void loadFromFileCvValues() {
        saveCvDescription = Resources.getString(CvDefinitionPanel.class, "saveCvDescription");
        cvExchangeFilter = new FileNameExtensionFilter(saveCvDescription, CV_EXCHANGE_EXTENSION);

        FileDialog dialog = new FileDialog(cvTreeTable, FileDialog.OPEN, null, cvExchangeFilter) {

            @Override
            public void approve(String fileName) {
                try {
                    // setWaitCursor();
                    LOGGER.info("Start importing saved CV, fileName: {}", fileName);

                    SaveCV saveCV = CvExchangeFactory.loadSaveCV(fileName);
                    LOGGER.info("Loaded saveCV from file: {}", fileName);

                    if (saveCV != null) {
                        List<CVSType> cvList = saveCV.getCVS();
                        if (CollectionUtils.isNotEmpty(cvList)) {

                            for (CVSType cvsType : cvList) {

                                int cvNumber = cvsType.getCV();
                                String cvValue = cvsType.getValue();

                                setCV(cvNumber, cvValue);
                            }

                        }
                    }
                }
                catch (Exception ex) {
                    LOGGER.warn("Load saved CV failed.", ex);

                    throw new RuntimeException("Load saved CV failed.");
                }
                finally {
                    // setDefaultCursor();
                }

                // treeModel.refreshTreeTable(treeModel.getRoot());
            }
        };
        dialog.showDialog();
    }

    private void setCV(final int cvNumber, final String cvValue) {

        if ("---".equals(cvValue)) {
            LOGGER.debug("Skip invalid value.");
            return;
        }

        final String cvNum = Integer.toString(cvNumber);
        ConfigurationVariable cv = IterableUtils.find(configVariables, new Predicate<ConfigurationVariable>() {

            @Override
            public boolean evaluate(ConfigurationVariable cv) {
                if (cv.getName().equals(cvNum)) {
                    return true;
                }
                return false;
            }
        });

        if (cv != null) {
            cv.setValue(cvValue);
        }
        else {
            LOGGER.warn("No CV found to update with cvNumber: {}, cvValue: {}", cvNumber, cvValue);
        }

    }

    private void saveToFileCvValues() {
        // TODO implement
    }

    @Override
    public void tabSelected(boolean selected) {
        LOGGER.info("Tab is selected: {}", selected);

        toolbarCvDefinition.setVisible(selected);
    }

    @Override
    public void refreshDisplayedValues() {
        // TreeTableNode root = treeModel.getRoot();
        //
        // // refresh the tree with the cleared new values
        // treeModel.refreshTreeTable(root);
    }

    private void importUserDefinedCvDefinition() {

        FileFilter ff = new XmlFileFilter();

        FileDialog dialog = new FileDialog(this, FileDialog.OPEN, null, ff) {
            @Override
            public void approve(String selectedFile) {
                File file = new File(selectedFile);

                selectedFile = file.getName();
                LOGGER.info("Selected file to import: {}", selectedFile);

                String labelPath = Preferences.getInstance().getLabelV2Path();
                StringBuilder sb = new StringBuilder(labelPath);
                sb.append("/data/BiDiBNodeVendorData/");

                LOGGER.info("Store file at path: {}", sb.toString());

                File target = new File(sb.toString(), selectedFile);

                try {
                    if (target.exists()) {
                        LOGGER.info("Delete existing file: {}", target);
                        FileUtils.deleteQuietly(target);

                    }
                    FileUtils.copyFile(file, target, false);

                    // TODO add dialog that shows the result

                    StopWatch sw = new StopWatch();
                    sw.start();

                    MainControllerInterface mainController =
                        DefaultApplicationContext.getInstance().get(DefaultApplicationContext.KEY_MAIN_CONTROLLER,
                            MainControllerInterface.class);

                    Node node = mainModel.getSelectedNode();
                    final Context context = new DefaultContext();
                    mainController.reloadCvDefinition(context, node);

                    sw.stop();

                    List<VendorCvError> vendorCVErrors =
                        context.get(VendorCvFactory.VENDORCV_ERRORS, List.class, Collections.emptyList());

                    StatusBar statusBar =
                        DefaultApplicationContext.getInstance().get(DefaultApplicationContext.KEY_STATUS_BAR,
                            StatusBar.class);

                    if (CollectionUtils.isNotEmpty(vendorCVErrors)) {
                        LOGGER.warn("Load vendor CV data from files failed: {}", vendorCVErrors);
                        statusBar.setStatus(String.format(
                            Resources.getString(MainController.class, "load-config-finished-load-vendorcv-failed"),
                            node, sw), StatusBar.DISPLAY_ERROR);
                    }
                    else {
                        statusBar.setStatus(
                            String.format(Resources.getString(MainController.class, "load-config-finished-no-vendorcv"),
                                node, sw),
                            StatusBar.DISPLAY_NORMAL);
                    }

                }
                catch (IOException ex) {
                    LOGGER.warn("Copy file failed.", ex);

                    throw new RuntimeException("Copy file failed.");
                }

            }
        };
        dialog.showDialog();

    }

    public static final String PATTERN_CV_FILENAME = "^BiDiBCV-([0-9]{1,2})-.+\\.xml$";

    private final class XmlFileFilter extends FileFilter {

        public static final String SUFFIX_XML = "xml";

        @Override
        public boolean accept(File file) {
            boolean result = false;

            if (file != null) {
                if (file.isDirectory()) {
                    result = true;
                }
                else if (file.toString() != null) {
                    String extension = FilenameUtils.getExtension(file.toString());
                    if (SUFFIX_XML.equalsIgnoreCase(extension)) {

                        // check if the pattern matches
                        Matcher m = Pattern.compile(PATTERN_CV_FILENAME).matcher(file.getName());
                        if (m.matches()) {
                            LOGGER.info("The pattern matches!");

                            result = true;
                        }
                    }
                }
            }
            return result;
        }

        @Override
        public String getDescription() {
            return Resources.getString(CvDefinitionPanel.class, "filter") + " (*." + SUFFIX_XML + ")";
        }

    }
}
