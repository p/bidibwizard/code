package org.bidib.wizard.mvc.main.model.listener;

public interface CvDefinitionListener {
    /**
     * Signal that the cv definition has changed.
     */
    void cvDefinitionChanged();

    /**
     * The values of the cv defintion have changed.
     * 
     * @param read
     *            the update was forced by a read operation
     */
    void cvDefinitionValuesChanged(boolean read);
}
