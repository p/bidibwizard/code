package org.bidib.wizard.mvc.pt.view.panel;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import org.bidib.jbidibc.core.enumeration.CommandStationProgState;
import org.bidib.jbidibc.core.enumeration.PtOperation;
import org.bidib.jbidibc.core.utils.CollectionUtils;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.common.utils.ImageUtils;
import org.bidib.wizard.mvc.common.view.validation.IconFeedbackPanel;
import org.bidib.wizard.mvc.main.view.panel.NodeTree;
import org.bidib.wizard.mvc.pt.model.PtProgrammerModel;
import org.bidib.wizard.mvc.pt.model.listener.ProgCommandListener;
import org.bidib.wizard.mvc.pt.view.CommandStationProgStateConverter;
import org.bidib.wizard.mvc.pt.view.command.PtOperationCommand;
import org.bidib.wizard.mvc.pt.view.command.PtOperationIfElseCommand;
import org.bidib.wizard.mvc.pt.view.panel.ProgCommandAwareBeanModel.ExecutionType;
import org.bidib.wizard.mvc.pt.view.panel.listener.PtRequestListener;
import org.bidib.wizard.mvc.pt.view.panel.listener.PtResultListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.helpers.MessageFormatter;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.value.ConverterValueModel;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.debug.FormDebugPanel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.validation.ValidationResultModel;

public abstract class AbstractPtPanel<M extends ProgCommandAwareBeanModel> implements PtResultListener {
    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());

    private static final String ENCODED_COLUMN_SPECS = "pref, 3dlu, pref, 3dlu, pref, 3dlu, fill:50dlu:grow";

    private final List<PtRequestListener> listeners = new LinkedList<PtRequestListener>();

    protected final PtProgrammerModel cvProgrammerModel;

    protected final PtResultProxyModel ptResultProxyModel;

    protected final JButton readButton = new JButton(Resources.getString(AbstractPtPanel.class, "read"));

    protected final JButton writeButton = new JButton(Resources.getString(AbstractPtPanel.class, "write"));

    protected ValueModel readButtonEnabled;

    protected ValueModel writeButtonEnabled;

    protected ValueModel currentOperationModel;

    protected JLabel currentOperationLabel;

    private ImageIcon progOperationErrorIcon;

    private ImageIcon progOperationSuccessfulIcon;

    private ImageIcon progOperationWaitIcon;

    protected ImageIcon progOperationUnknownIcon;

    private ImageIcon progOperationEmptyIcon;

    private JTextArea loggerArea;

    private final static String NEWLINE = "\n";

    protected boolean activeTab;

    private M progCommandAwareBeanModel;

    public AbstractPtPanel(final PtProgrammerModel cvProgrammerModel) {
        this.cvProgrammerModel = cvProgrammerModel;

        // create a proxy model that receives updates from the programmer model but does not update values if not active
        ptResultProxyModel = new PtResultProxyModel() {
            private static final long serialVersionUID = 1L;

            @Override
            public boolean isAllowUpdate() {
                // this method is overriden to take the flag of this panel into account
                return AbstractPtPanel.this.isActive();
            }
        };

        initializeIcons();
    }

    /**
     * Initialize the icons
     */
    private void initializeIcons() {
        // Load the icons
        progOperationErrorIcon = ImageUtils.createImageIcon(NodeTree.class, "/icons/accessory-error.png");
        progOperationSuccessfulIcon = ImageUtils.createImageIcon(NodeTree.class, "/icons/accessory-successful.png");
        progOperationWaitIcon = ImageUtils.createImageIcon(NodeTree.class, "/icons/accessory-wait.png");
        progOperationUnknownIcon = ImageUtils.createImageIcon(NodeTree.class, "/icons/information.png");
        progOperationEmptyIcon = ImageUtils.createImageIcon(NodeTree.class, "/icons/empty.png");
    }

    /**
     * @return the progCommandAwareBeanModel
     */
    public ProgCommandAwareBeanModel getProgCommandAwareBeanModel() {
        return progCommandAwareBeanModel;
    }

    /**
     * @param progCommandAwareBeanModel
     *            the progCommandAwareBeanModel
     */
    public void setProgCommandAwareBeanModel(M progCommandAwareBeanModel) {
        this.progCommandAwareBeanModel = progCommandAwareBeanModel;
    }

    public void addPtRequestListener(PtRequestListener l) {
        listeners.add(l);
    }

    protected List<PtRequestListener> getPtRequestListeners() {
        return listeners;
    }

    @Override
    public void setActive(boolean active) {
        LOGGER.info("Set the active flag: {}", active);
        this.activeTab = active;
    }

    @Override
    public boolean isActive() {
        return activeTab;
    }

    protected abstract void createWorkerPanel(DefaultFormBuilder builder);

    public JPanel createPanel() {

        cvProgrammerModel.addPropertyChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                switch (evt.getPropertyName()) {
                    case PtProgrammerModel.PROPERTYNAME_COMMANDSTATIONPROGSTATE:
                        ptResultProxyModel.setCommandStationProgState((CommandStationProgState) evt.getNewValue());
                        break;
                    case PtProgrammerModel.PROPERTYNAME_CVNUMBER:
                        ptResultProxyModel.setCvNumber((int) evt.getNewValue());
                        break;
                    case PtProgrammerModel.PROPERTYNAME_CVVALUE:
                        ptResultProxyModel.setCvValue((Integer) evt.getNewValue());
                        break;
                    default:
                        break;
                }
            }
        });

        DefaultFormBuilder builder = null;
        boolean debug = false;
        if (debug) {
            builder = new DefaultFormBuilder(new FormLayout(ENCODED_COLUMN_SPECS), new FormDebugPanel());
        }
        else {
            builder = new DefaultFormBuilder(new FormLayout(ENCODED_COLUMN_SPECS), new JPanel());
        }
        builder.border(Borders.TABBED_DIALOG);

        // create the worker panel content
        createWorkerPanel(builder);

        // prepare the operation verdict
        currentOperationModel =
            new PropertyAdapter<PtResultProxyModel>(ptResultProxyModel,
                PtResultProxyModel.PROPERTYNAME_COMMANDSTATIONPROGSTATE, true);
        ValueModel valueConverterModel =
            new ConverterValueModel(currentOperationModel, new CommandStationProgStateConverter());
        currentOperationLabel = BasicComponentFactory.createLabel(valueConverterModel);
        currentOperationLabel.setIcon(progOperationEmptyIcon);
        builder.append(Resources.getString(AbstractPtPanel.class, "prog-result"));
        builder.append(currentOperationLabel, 5);
        builder.appendRow("5dlu");
        builder.nextLine(2);

        // prepare the logger area
        loggerArea = new JTextArea(20, 45);
        loggerArea.setFont(UIManager.getDefaults().getFont("Label.font"));
        // loggerArea.setLineWrap(true);
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.getViewport().add(loggerArea);
        loggerArea.setEditable(false);

        builder.appendRow("fill:200px:grow");
        builder.append(scrollPane, 7);

        doBindButtons();

        // handle the finished prog commands
        cvProgrammerModel.addProgCommandListener(new ProgCommandListener() {

            @Override
            public void progCommandStationFinished(CommandStationProgState commandStationProgState) {
                LOGGER.info("The prog command has finished, commandStationProgState: {}", commandStationProgState);
                if (!isActive()) {
                    return;
                }

                if (!CommandStationProgState.PROG_START.equals(commandStationProgState)
                    && !CommandStationProgState.PROG_RUNNING.equals(commandStationProgState)) {

                    PtOperationCommand<M> executingProgCommand =
                        (PtOperationCommand<M>) progCommandAwareBeanModel.getExecutingProgCommand();
                    if (executingProgCommand != null) {
                        LOGGER.info("The executingProgCommand: {}", executingProgCommand);
                        executingProgCommand.setProgStateResult(commandStationProgState);

                        boolean ignoreError = false;

                        if (executingProgCommand instanceof PtOperationIfElseCommand) {
                            PtOperationIfElseCommand<M> ifElseCommand =
                                (PtOperationIfElseCommand<M>) executingProgCommand;
                            LOGGER.info("The executing command is a if-else-command: {}", ifElseCommand);

                            // we must check if the result is successful or not
                            if (!CommandStationProgState.PROG_OKAY.equals(ifElseCommand.getProgStateResult())) {
                                LOGGER.info("The command was not successful executed!");
                                List<PtOperationCommand<M>> failureCommands = ifElseCommand.getProgCommandsFailure();
                                if (CollectionUtils.hasElements(failureCommands)) {
                                    LOGGER.info("Found failure commands to be executed: {}", failureCommands);
                                    // add the new commands to process
                                    List<PtOperationCommand<? extends ProgCommandAwareBeanModel>> progCommands =
                                        progCommandAwareBeanModel.getProgCommands();
                                    progCommands.clear();
                                    progCommands.addAll(failureCommands);

                                    // ignore the error
                                    ignoreError = true;
                                }
                            }
                            else {
                                LOGGER.info("The command was successful executed!");
                                List<PtOperationCommand<M>> successCommands = ifElseCommand.getProgCommandsSuccess();
                                if (CollectionUtils.hasElements(successCommands)) {
                                    LOGGER.info("Found success commands to be executed: {}", successCommands);
                                    // add the new commands to process
                                    List<PtOperationCommand<? extends ProgCommandAwareBeanModel>> progCommands =
                                        progCommandAwareBeanModel.getProgCommands();
                                    progCommands.clear();
                                    progCommands.addAll(successCommands);
                                }

                            }
                        }

                        // we must check if the result is successful or not
                        if (CommandStationProgState.PROG_OKAY.equals(executingProgCommand.getProgStateResult())) {
                            LOGGER.info("PostExecute the command.");
                            executingProgCommand.postExecute(progCommandAwareBeanModel);
                        }
                        else if (!ignoreError) {
                            // if an error occurs we must stop processing!!!!

                            LOGGER.warn("Clear remaining prog commands because command has finished with an error: {}",
                                executingProgCommand);
                            if (CollectionUtils.hasElements(progCommandAwareBeanModel.getProgCommands())) {
                                progCommandAwareBeanModel.getProgCommands().clear();
                            }

                            addLogText("Error detected. Please try again.");
                        }

                        // keep the executed commands
                        progCommandAwareBeanModel.getExecutedProgCommands().add(executingProgCommand);
                        executingProgCommand = null;
                    }

                    if (CollectionUtils.hasElements(progCommandAwareBeanModel.getProgCommands())) {
                        LOGGER.info("Prepare the next command.");

                        fireNextCommand();
                    }
                    else {
                        LOGGER.info("No more commands to send.");
                    }
                }
            }
        });

        // react on changes of CV value
        cvProgrammerModel.addPropertyChangeListener(PtProgrammerModel.PROPERTYNAME_CVVALUE,
            new PropertyChangeListener() {

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    if (!isActive()) {
                        return;
                    }

                    LOGGER.info("The CV value has been changed: {}", evt.getNewValue());

                    if (progCommandAwareBeanModel.getCurrentOperation() != null) {
                        switch (progCommandAwareBeanModel.getCurrentOperation()) {
                            case RD_BYTE:
                                progCommandAwareBeanModel.getExecutingProgCommand().setCvValueResult(
                                    (Integer) evt.getNewValue());

                                if (evt.getNewValue() != null) {
                                    addLogText("Read byte returned: {}", evt.getNewValue());
                                }
                                break;
                            case WR_BYTE:
                                break;
                            case RD_BIT:
                            case WR_BIT:
                                if (ExecutionType.READ.equals(progCommandAwareBeanModel.getExecution())) {
                                    Object value = evt.getNewValue();
                                    LOGGER.info("Returned read bit value: {}", value);
                                    if (value != null) {
                                        int val = ((Integer) value).intValue();
                                        int bitValue = (val & 0x08) == 0x08 ? 1 : 0;
                                        progCommandAwareBeanModel.getExecutingProgCommand().setCvValueResult(bitValue);

                                        addLogText("Read bit returned: {}", bitValue);
                                    }
                                }
                                else {
                                    // nothing to do
                                }
                                break;
                            default:
                                break;
                        }
                    }
                    else {
                        LOGGER.info("No operation performed.");
                    }
                }
            });

        // check if we have validation enabled
        if (getValidationResultModel() != null) {
            LOGGER.info("Create iconfeedback panel.");
            JComponent cvIconPanel = new IconFeedbackPanel(getValidationResultModel(), builder.build());
            DefaultFormBuilder feedbackBuilder = null;
            feedbackBuilder = new DefaultFormBuilder(new FormLayout("p:g"));

            feedbackBuilder.appendRow("fill:p:grow");
            feedbackBuilder.add(cvIconPanel);
            // feedbackBuilder.border(new EmptyBorder(10, 0, 0, 0));

            JPanel panel = feedbackBuilder.build();
            triggerValidation();
            return panel;
        }

        return builder.build();
    }

    protected void doBindButtons() {
        // add bindings for enable/disable the write button
        PropertyConnector.connect(readButtonEnabled, "value", readButton, "enabled");
        PropertyConnector.connect(writeButtonEnabled, "value", writeButton, "enabled");
    }

    protected void triggerValidation() {

    }

    protected ValidationResultModel getValidationResultModel() {
        return null;
    }

    @Override
    public void addLogText(String logLine, Object... args) {
        LOGGER.info("Add text to loggerArea, logLine: {}, args: {}", logLine, args);

        if (args != null) {
            logLine = MessageFormatter.arrayFormat(logLine, args).getMessage();
        }
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SSS - ");
        final StringBuffer message = new StringBuffer(sdf.format(new Date()));
        message.append(logLine).append(NEWLINE);

        if (SwingUtilities.isEventDispatchThread()) {

            loggerArea.append(message.toString());
            loggerArea.setCaretPosition(loggerArea.getDocument().getLength());
        }
        else {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    loggerArea.append(message.toString());
                    loggerArea.setCaretPosition(loggerArea.getDocument().getLength());
                }
            });
        }
    }

    protected abstract Object getCurrentOperation();

    public void signalCommandStationProgStateChanged(CommandStationProgState commandStationProgState) {

        if (!isActive()) {
            LOGGER.info("Do not process result because this is not the active tab.");
            return;
        }

        switch (commandStationProgState) {
        // finished state
            case PROG_OKAY:
                currentOperationLabel.setIcon(progOperationSuccessfulIcon);
                addLogText("Prog operation passed: " + getCurrentOperation());
                // enable the input elements
                enableInputElements();
                break;
            case PROG_STOPPED:
            case PROG_NO_LOCO:
            case PROG_NO_ANSWER:
            case PROG_SHORT:
            case PROG_VERIFY_FAILED:
                currentOperationLabel.setIcon(progOperationErrorIcon);
                addLogText("Prog operation failed: " + commandStationProgState.name());
                // enable the input elements
                enableInputElements();
                break;
            // pending
            case PROG_START:
                currentOperationLabel.setIcon(progOperationUnknownIcon);
                addLogText("Prog operation started: " + getCurrentOperation());
                // disableInputElements();
                break;
            // pending
            case PROG_RUNNING:
                currentOperationLabel.setIcon(progOperationWaitIcon);
                addLogText("Prog operation is running ...");
                break;
        }
    }

    protected void sendRequest(PtOperation operation, int cvNumber, int cvValue) {

        // send to bidib
        for (PtRequestListener listener : getPtRequestListeners()) {
            listener.sendRequest(this, operation, cvNumber, cvValue);
        }
    }

    protected void disableInputElements() {
        if (readButtonEnabled != null) {
            readButtonEnabled.setValue(false);
        }
        if (writeButtonEnabled != null) {
            writeButtonEnabled.setValue(false);
        }
    }

    protected void enableInputElements() {
    }

    protected void fireNextCommand() {
        // disable the input elements
        disableInputElements();

        List<PtOperationCommand<? extends ProgCommandAwareBeanModel>> progCommands =
            progCommandAwareBeanModel.getProgCommands();
        LOGGER.info("Prepared commands for addressing the decoder: {}", progCommands);

        // remove the executing command from the prog commands list ...
        PtOperationCommand<?> progCommand = progCommands.remove(0);

        progCommandAwareBeanModel.setCurrentOperation(progCommand.getPtOperation());
        switch (progCommand.getPtOperation()) {
            case WR_BIT:
            case WR_BYTE:
                progCommandAwareBeanModel.setExecution(ExecutionType.WRITE);
                break;
            default:
                progCommandAwareBeanModel.setExecution(ExecutionType.READ);
                break;
        }
        progCommandAwareBeanModel.setExecutingProgCommand(progCommand);

        addLogText("Prog write request: {}", progCommand);

        // send to bidib
        sendRequest(progCommand.getPtOperation(), progCommand.getCvNumber(), progCommand.getCvValue());
    }
}
