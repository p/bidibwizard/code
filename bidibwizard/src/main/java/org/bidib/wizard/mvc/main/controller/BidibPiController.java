package org.bidib.wizard.mvc.main.controller;

public interface BidibPiController {

    /**
     * Reset the BiDiB-Pi.
     */
    void reset();

    /**
     * Shutdown the BiDiB-Pi integration.
     */
    void shutdown();
}
