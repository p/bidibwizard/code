package org.bidib.wizard.mvc.main.model;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.util.LinkedHashMap;
import java.util.Map;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.ServoPortEnum;
import org.bidib.jbidibc.core.port.BytePortConfigValue;
import org.bidib.jbidibc.core.port.PortConfigValue;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.wizard.comm.ServoPortStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServoPort extends Port<ServoPortStatus> {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ServoPort.class);

    static {
        try {
            // Q: why is this needed? A: export of beans with XMLDecoder
            PropertyDescriptor[] descriptor = Introspector.getBeanInfo(ServoPort.class).getPropertyDescriptors();

            for (int i = 0; i < descriptor.length; i++) {
                PropertyDescriptor propertyDescriptor = descriptor[i];
                if (propertyDescriptor.getName().equals("value")) {
                    propertyDescriptor.setValue("transient", Boolean.TRUE);
                }
                else if (propertyDescriptor.getName().equals("relativeValue")) {
                    propertyDescriptor.setValue("transient", Boolean.TRUE);
                }
            }
        }
        catch (IntrospectionException e) {
            throw new RuntimeException(e);
        }
    }

    public static final ServoPort NONE = new ServoPort.Builder(-1).setLabel("<none>").build();

    private int speed;

    private int trimDown;

    private int trimUp;

    /**
     * The absolute value of the position.
     */
    private int value;

    public ServoPort() {
        super(null);
        setStatus(ServoPortStatus.START);
    }

    public ServoPort(GenericPort genericPort) {
        super(genericPort);
        setStatus(ServoPortStatus.START);
    }

    private ServoPort(Builder builder) {
        super(null);
        setId(builder.id);
        setLabel(builder.label);
    }

    public int getSpeed() {
        if (genericPort != null) {
            Number servoSpeed = genericPort.getPortConfigValue(BidibLibrary.BIDIB_PCFG_SERVO_SPEED);
            if (servoSpeed != null) {
                this.speed = ByteUtils.getInt(servoSpeed.byteValue());
            }
            else {
                LOGGER.warn("No value received from generic port for BIDIB_PCFG_SERVO_SPEED!");
            }
        }
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
        if (genericPort != null) {
            genericPort.setPortConfigValue(BidibLibrary.BIDIB_PCFG_SERVO_SPEED,
                new BytePortConfigValue(ByteUtils.getLowByte(speed)));
        }
    }

    public int getTrimDown() {
        if (genericPort != null) {
            Number servoTrimDown = genericPort.getPortConfigValue(BidibLibrary.BIDIB_PCFG_SERVO_ADJ_L);
            if (servoTrimDown != null) {
                this.trimDown = ByteUtils.getInt(servoTrimDown.byteValue());
            }
            else {
                LOGGER.warn("No value received from generic port for BIDIB_PCFG_SERVO_ADJ_L!");
            }
        }
        return trimDown;
    }

    public void setTrimDown(int trimDown) {
        this.trimDown = trimDown;
        if (genericPort != null) {
            genericPort.setPortConfigValue(BidibLibrary.BIDIB_PCFG_SERVO_ADJ_L,
                new BytePortConfigValue(ByteUtils.getLowByte(trimDown)));
        }
    }

    public int getTrimUp() {
        if (genericPort != null) {
            Number servoTrimUp = genericPort.getPortConfigValue(BidibLibrary.BIDIB_PCFG_SERVO_ADJ_H);
            if (servoTrimUp != null) {
                this.trimUp = ByteUtils.getInt(servoTrimUp.byteValue());
            }
            else {
                LOGGER.warn("No value received from generic port for BIDIB_PCFG_SERVO_ADJ_H!");
            }
        }
        return trimUp;
    }

    public void setTrimUp(int trimUp) {
        this.trimUp = trimUp;
        if (genericPort != null) {
            genericPort.setPortConfigValue(BidibLibrary.BIDIB_PCFG_SERVO_ADJ_H,
                new BytePortConfigValue(ByteUtils.getLowByte(trimUp)));
        }
    }

    /**
     * Get the absolute servo destination value.
     * 
     * @return absolute servo destination value
     */
    public int getValue() {
        if (genericPort != null) {
            Integer portVal = genericPort.getPortValue();
            if (portVal != null) {
                value = portVal;
            }
            else {
                value = 0;
            }
        }
        return value;
    }

    /**
     * Set the absolute servo destination value.
     * 
     * @param value
     *            absolute servo destination value
     */
    public void setValue(int value) {
        if (genericPort != null) {
            genericPort.setPortValue(value);
        }
        this.value = value;
    }

    /**
     * Get the absolute destination value from the provided relative value in %. The minimum value 0% is the PORTSTAT
     * value 0, the maximum value 100% is the PORTSTAT value 255, 50% is the PORTSTAT value 127.
     * 
     * @param value
     *            relative servo destination value in %
     * 
     * @return absolute servo destination value
     */
    public int getAbsoluteValue(int value) {
        if (value < 0 || value > 100) {
            throw new IllegalArgumentException("The allowed range is 0-100. Provided value: " + value);
        }
        int stepWidth = (value * 255) / 100;
        return stepWidth;
    }

    /**
     * Get the relative servo destination value in % from the absolute value. The minimum value 0% is the PORTSTAT value
     * 0, the maximum value 100% is the PORTSTAT value 255, 50% is the PORTSTAT value 127.
     * 
     * @param value
     *            absolute servo destination value
     * 
     * @return relative servo destination value in %
     */
    public int getRelativeValue(int value) {
        if (value < 0 || value > 255) {
            throw new IllegalArgumentException("The allowed range is 0-255. Provided value: " + value);
        }
        long result = Math.round(((double) value * 100) / 255);
        return (int) result;
    }

    /**
     * Get the relative servo destination value in % from the absolute value. The minimum value 0% is the PORTSTAT value
     * 0, the maximum value 100% is the PORTSTAT value 255, 50% is the PORTSTAT value 127.
     * 
     * @return relative servo destination value in %
     */
    public int getRelativeValue() {
        long result = Math.round(((double) getValue() * 100) / 255);
        return (int) result;
    }

    /**
     * Set the relative servo destination value in %.
     * 
     * @param value
     *            relative servo destination value in %
     */
    public void setRelativeValue(int value) {
        setValue(getAbsoluteValue(value));
    }

    public String getDebugString() {
        return getClass().getSimpleName() + "[speed=" + speed + ",trimDown=" + trimDown + ",trimUp=" + trimUp
            + ",value=" + getValue() + "]";
    }

    @Override
    public byte[] getPortConfig() {
        return new byte[] { ByteUtils.getLowByte(trimDown), ByteUtils.getLowByte(trimUp), ByteUtils.getLowByte(speed),
            0 };
    }

    public void setPortConfig(byte[] portConfig) {
        LOGGER.info("Set the servo port parameters: {}", ByteUtils.bytesToHex(portConfig));

        setPortConfigEnabled(true);
        setTrimDown(ByteUtils.getInt(portConfig[0]));
        getKnownPortConfigKeys().add(Byte.valueOf(BidibLibrary.BIDIB_PCFG_SERVO_ADJ_L));
        setTrimUp(ByteUtils.getInt(portConfig[1]));
        getKnownPortConfigKeys().add(Byte.valueOf(BidibLibrary.BIDIB_PCFG_SERVO_ADJ_H));
        setSpeed(ByteUtils.getInt(portConfig[2]));
        getKnownPortConfigKeys().add(Byte.valueOf(BidibLibrary.BIDIB_PCFG_SERVO_SPEED));

    }

    @Override
    public void setPortConfigX(Map<Byte, PortConfigValue<?>> portConfig) {

        Number trimmDown = getPortConfigValue(BidibLibrary.BIDIB_PCFG_SERVO_ADJ_L, portConfig);
        if (trimmDown != null) {
            setTrimDown(ByteUtils.getInt(trimmDown.byteValue()));
        }
        else {
            LOGGER.warn("No value received for BIDIB_PCFG_SERVO_ADJ_L!");
        }
        Number trimUp = getPortConfigValue(BidibLibrary.BIDIB_PCFG_SERVO_ADJ_H, portConfig);
        if (trimUp != null) {
            setTrimUp(ByteUtils.getInt(trimUp.byteValue()));
        }
        else {
            LOGGER.warn("No value received for BIDIB_PCFG_SERVO_ADJ_H!");
        }
        Number speed = getPortConfigValue(BidibLibrary.BIDIB_PCFG_SERVO_SPEED, portConfig);
        if (speed != null) {
            setSpeed(ByteUtils.getInt(speed.byteValue()));
        }
        else {
            LOGGER.warn("No value received for BIDIB_PCFG_SERVO_SPEED!");
        }

        // call the super class
        super.setPortConfigX(portConfig);
    }

    @Override
    public Map<Byte, PortConfigValue<?>> getPortConfigX() {

        if (genericPort != null) {
            return genericPort.getPortConfig();
        }

        Map<Byte, PortConfigValue<?>> portConfigX = new LinkedHashMap<Byte, PortConfigValue<?>>();

        // add the trim down
        if (isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_SERVO_ADJ_L)) {
            portConfigX.put(BidibLibrary.BIDIB_PCFG_SERVO_ADJ_L,
                new BytePortConfigValue(ByteUtils.getLowByte(trimDown)));
        }

        // add the trim up
        if (isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_SERVO_ADJ_H)) {
            portConfigX.put(BidibLibrary.BIDIB_PCFG_SERVO_ADJ_H, new BytePortConfigValue(ByteUtils.getLowByte(trimUp)));
        }

        // add the speed
        if (isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_SERVO_SPEED)) {
            portConfigX.put(BidibLibrary.BIDIB_PCFG_SERVO_SPEED, new BytePortConfigValue(ByteUtils.getLowByte(speed)));
        }
        return portConfigX;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ServoPort) {
            return ((ServoPort) obj).getId() == getId();
        }
        return false;
    }

    @Override
    public int hashCode() {
        return getId();
    }

    @Override
    protected LcOutputType getPortType() {
        return LcOutputType.SERVOPORT;
    }

    @Override
    protected ServoPortStatus internalGetStatus() {
        return ServoPortStatus.valueOf(ServoPortEnum.valueOf(genericPort.getPortStatus()));
    }

    public static class Builder {
        private final int id;

        private String label;

        public Builder(int id) {
            this.id = id;
        }

        public Builder setLabel(String label) {
            this.label = label;
            return this;
        }

        public ServoPort build() {
            return new ServoPort(this);
        }
    }

}
