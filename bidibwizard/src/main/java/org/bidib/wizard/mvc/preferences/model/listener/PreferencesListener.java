package org.bidib.wizard.mvc.preferences.model.listener;

import java.util.Date;

public interface PreferencesListener {

    /**
     * @param prevSelectedComPort
     *            the prev selected com port has changed
     */
    void prevSelectedComPortChanged(String prevSelectedComPort);

    /**
     * @param prevSelectedSerialSymLink
     *            the prev selected serial symlink has changed
     */
    void prevSelectedSerialSymLinkChanged(String prevSelectedSerialSymLink);

    /**
     * @param prevSelectedUdpHost
     *            the prev selected udp host has changed
     */
    void prevSelectedUdpHostChanged(String prevSelectedUdpHost);

    /**
     * @param prevSelectedTcpHost
     *            the prev selected tcp host has changed
     */
    void prevSelectedTcpHostChanged(String prevSelectedTcpHost);

    /**
     * The serialEnabled flag has changed.
     * 
     * @param serialEnabled
     *            the new serialEnabled flag
     */
    void serialEnabledChanged(boolean serialEnabled);

    /**
     * The udpEnabled flag has changed.
     * 
     * @param udpEnabled
     *            the new udpEnabled flag
     */
    void udpEnabledChanged(boolean udpEnabled);

    /**
     * The tcpEnabled flag has changed.
     * 
     * @param tcpEnabled
     *            the new tcpEnabled flag
     */
    void tcpEnabledChanged(boolean tcpEnabled);

    /**
     * The plainTcpEnabled flag has changed.
     * 
     * @param plainTcpEnabled
     *            the new plainTcpEnabled flag
     */
    void plainTcpEnabledChanged(boolean plainTcpEnabled);

    /**
     * The start time has changed.
     * 
     * @param startTime
     *            the new start time
     */
    void startTimeChanged(Date startTime);

    /**
     * The time factor has changed.
     * 
     * @param timeFactor
     *            the new time factor
     */
    void timeFactorChanged(int timeFactor);

    /**
     * The reset reconnect delay has changed.
     * 
     * @param resetReconnectDelay
     *            the new reset reconnect delay value
     */
    void resetReconnectDelayChanged(int resetReconnectDelay);

    /**
     * The response timeout has changed.
     * 
     * @param responseTimeout
     *            the new response timeout value
     */
    void responseTimeoutChanged(int responseTimeout);

    /**
     * The ptModeDoNotConfirmSwitch flag has changed.
     * 
     * @param ptModeDoNotConfirmSwitch
     *            the ptModeDoNotConfirmSwitch flag
     */
    void ptModeDoNotConfirmSwitchChanged(boolean ptModeDoNotConfirmSwitch);

    /**
     * The showBoosterTable flag has changed.
     * 
     * @param showBoosterTable
     *            the showBoosterTable flag
     */
    void showBoosterTableChanged(boolean showBoosterTable);

    /**
     * A property has changed.
     * 
     * @param propertyName
     *            the property name
     * @param oldValue
     *            the old property value
     * @param newValue
     *            the new property value
     */
    void propertyChanged(String propertyName, Object oldValue, Object newValue);
}
