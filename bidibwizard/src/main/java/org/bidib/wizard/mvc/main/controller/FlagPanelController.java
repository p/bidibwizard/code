package org.bidib.wizard.mvc.main.controller;

import org.bidib.wizard.labels.FlagLabelFactory;
import org.bidib.wizard.labels.Labels;
import org.bidib.wizard.labels.PortLabelUtils;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.view.panel.FlagListPanel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FlagPanelController {
    private static final Logger LOGGER = LoggerFactory.getLogger(FlagPanelController.class);

    private Labels flagLabels;

    private final MainModel mainModel;

    public FlagPanelController(final MainModel mainModel) {

        this.mainModel = mainModel;

        flagLabels =
            DefaultApplicationContext.getInstance().get(DefaultApplicationContext.KEY_FLAG_LABELS, Labels.class);
    }

    public FlagListPanel createFlagListPanel() {
        FlagListPanel flagListPanel = new FlagListPanel(this, mainModel);

        return flagListPanel;
    }

    public void setFlagLabel(int flagId, String label) {

        if (flagLabels == null) {
            LOGGER.info("No flag labels avaialble.");
            return;
        }
        LOGGER.info("Set the flag label: {}", label);

        boolean save = true;
        long uniqueId = mainModel.getSelectedNode().getUniqueId();

        PortLabelUtils.replaceLabel(flagLabels, uniqueId, flagId, label, FlagLabelFactory.DEFAULT_LABELTYPE);

        if (save) {
            try {
                new FlagLabelFactory().saveLabels(uniqueId, flagLabels);
            }
            catch (Exception e) {
                LOGGER.warn("Save flag labels failed.", e);
                throw new RuntimeException(e);
            }
        }
    }

}
