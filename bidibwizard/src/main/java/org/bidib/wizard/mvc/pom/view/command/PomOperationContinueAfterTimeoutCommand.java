package org.bidib.wizard.mvc.pom.view.command;

/**
 * This is a marker interface that allows to continue after a timeout has occured.
 */
public interface PomOperationContinueAfterTimeoutCommand {

}
