package org.bidib.wizard.mvc.main.view.panel;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.AbstractAction;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;

import org.apache.commons.collections4.CollectionUtils;
import org.bidib.wizard.comm.ServoPortStatus;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.model.Port;
import org.bidib.wizard.mvc.main.model.ServoPort;
import org.bidib.wizard.mvc.main.model.ServoPortTableModel;
import org.bidib.wizard.mvc.main.model.SimplePortTableModel;
import org.bidib.wizard.mvc.main.model.listener.PortValueListener;
import org.bidib.wizard.mvc.main.model.listener.ServoPortListener;
import org.bidib.wizard.mvc.main.view.table.PortConfigErrorAwareEditor;
import org.bidib.wizard.mvc.main.view.table.PortConfigErrorAwareRenderer;
import org.bidib.wizard.mvc.main.view.table.ServoPortTable;
import org.bidib.wizard.mvc.main.view.table.ServoPortTableCellRenderer;
import org.bidib.wizard.mvc.main.view.table.SliderEditor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jidesoft.grid.TableColumnChooser;

public class ServoPortListPanel
    extends SimplePortListPanel<ServoPortStatus, ServoPort, ServoPortListener<ServoPortStatus>>
    implements PortValueListener<ServoPortStatus> {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ServoPortListPanel.class);

    private final Collection<ServoPortListener<ServoPortStatus>> servoPortListeners =
        new LinkedList<ServoPortListener<ServoPortStatus>>();

    private final MainModel mainModel;

    private AtomicBoolean updateModelInProgress = new AtomicBoolean();

    // TODO move to controller
    private Node selectedNode;

    public ServoPortListPanel(MainModel mainModel) {
        super(new ServoPortTableModel(mainModel), mainModel.getServoPorts(),
            Resources.getString(ServoPortListPanel.class, "emptyTable"));
        this.mainModel = mainModel;
    }

    @Override
    protected void createTable(
        final SimplePortTableModel<ServoPortStatus, ServoPort, ServoPortListener<ServoPortStatus>> tableModel,
        String emptyTableText) {

        table = new ServoPortTable(tableModel, emptyTableText) {
            private static final long serialVersionUID = 1L;

            @Override
            protected void sliderStateChanged(ServoPort servoPort) {
                if (!updateModelInProgress.get()) {
                    fireTestButtonPressed(servoPort);
                }
                else {
                    LOGGER.debug("Do not send new port status to servo because the model was updated: {}", servoPort);
                }
            }
        };

        // do not allow drag columns to other position
        table.getTableHeader().setReorderingAllowed(false);

        table.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);

        // disabled sorting
        table.setSortable(false);

        // let the left and right key change the slider value and <numeric block +> and <numeric block ->
        table.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(
            KeyStroke.getKeyStroke(KeyEvent.VK_SUBTRACT, 0), "left");
        table.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0),
            "left");
        table.getActionMap().put("left", new AbstractAction() {
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {
                JTable table = (JTable) e.getSource();
                TableCellEditor editor = table.getCellEditor(table.getSelectedRow(), table.getSelectedColumn());

                if (editor instanceof SliderEditor) {
                    table.editCellAt(table.getSelectedRow(), table.getSelectedColumn());
                    ((SliderEditor) editor).setDown();
                }
            }
        });
        // slider value up with <numeric block +> and <cursor right>
        table.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_ADD, 0),
            "right");
        table.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0),
            "right");
        table.getActionMap().put("right", new AbstractAction() {
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {
                JTable table = (JTable) e.getSource();
                TableCellEditor editor = table.getCellEditor(table.getSelectedRow(), table.getSelectedColumn());

                if (editor instanceof SliderEditor) {
                    table.editCellAt(table.getSelectedRow(), table.getSelectedColumn());
                    ((SliderEditor) editor).setUp();
                }
            }
        });

        // TODO the servo ports are moved to node
        ((ServoPortTableModel) tableModel).getMainModel().addServoPortValueListener(this);

        TableColumn tc = table.getColumnModel().getColumn(ServoPortTableModel.COLUMN_LABEL);
        // tc.setCellRenderer(new ServoPortTableCellRenderer(ServoPortTableModel.COLUMN_LABEL));
        tc.setCellRenderer(new PortConfigErrorAwareRenderer(ServoPortTableModel.COLUMN_LABEL));
        tc.setCellEditor(new PortConfigErrorAwareEditor(ServoPortTableModel.COLUMN_PORT_INSTANCE));
        tc.setIdentifier(Integer.valueOf(ServoPortTableModel.COLUMN_LABEL));

        tc = table.getColumnModel().getColumn(ServoPortTableModel.COLUMN_SPEED);
        tc.setCellRenderer(new ServoPortTableCellRenderer(ServoPortTableModel.COLUMN_SPEED));
        tc.setIdentifier(Integer.valueOf(ServoPortTableModel.COLUMN_SPEED));

        tc = table.getColumnModel().getColumn(ServoPortTableModel.COLUMN_TRIM_DOWN);
        tc.setCellRenderer(new ServoPortTableCellRenderer(ServoPortTableModel.COLUMN_TRIM_DOWN));
        tc.setIdentifier(Integer.valueOf(ServoPortTableModel.COLUMN_TRIM_DOWN));

        tc = table.getColumnModel().getColumn(ServoPortTableModel.COLUMN_TRIM_UP);
        tc.setCellRenderer(new ServoPortTableCellRenderer(ServoPortTableModel.COLUMN_TRIM_UP));
        tc.setIdentifier(Integer.valueOf(ServoPortTableModel.COLUMN_TRIM_UP));

        TableColumnChooser.hideColumn(table, ServoPortTableModel.COLUMN_PORT_INSTANCE);

    }

    public void addServoPortListener(ServoPortListener<ServoPortStatus> listener) {
        portListener = listener;

        ((ServoPortTableModel) tableModel).addServoPortListener(listener);
        servoPortListeners.add(listener);
    }

    private void fireTestButtonPressed(ServoPort servoPort) {
        for (ServoPortListener<ServoPortStatus> l : servoPortListeners) {
            l.testButtonPressed(servoPort);
        }
    }

    @Override
    public void listChanged() {

        if (selectedNode != null) {
            selectedNode.removePortListListener(ServoPort.class, this);
        }

        selectedNode = mainModel.getSelectedNode();

        try {
            updateModelInProgress.set(true);
            LOGGER.info("Update model has started, call clearTable.");

            table.clearTable();
            if (selectedNode != null) {
                List<ServoPort> servoPorts = new LinkedList<>();
                servoPorts.addAll(selectedNode.getServoPorts());
                for (ServoPort servoPort : servoPorts) {
                    tableModel.addRow(servoPort);
                }
            }
            table.packColumn(table.getColumnCount() - 1, 2);
        }
        finally {
            LOGGER.debug("Update model has finished.");
            updateModelInProgress.set(false);
        }

        if (selectedNode != null) {
            selectedNode.addPortListListener(ServoPort.class, this);
        }
    }

    @Override
    public void valueChanged(Port<ServoPortStatus> port) {
        LOGGER.debug("The value of a servo port has changed: {}, value: {}", port, ((ServoPort) port).getValue());

        try {
            // do not trigger sending new configuration to port ...
            updateModelInProgress.set(true);
            LOGGER.debug("Update model has started.");

            int rowCount = tableModel.getRowCount();
            for (int row = 0; row < rowCount; row++) {
                ServoPort servoPort = (ServoPort) tableModel.getValueAt(row, ServoPortTableModel.COLUMN_PORT_INSTANCE);
                if (servoPort.getId() == port.getId()) {
                    LOGGER.debug("Found the matching port: {}", servoPort);
                    ((ServoPortTable) table).updateSliderPosition(row, ServoPortTableModel.COLUMN_VALUE,
                        servoPort.getRelativeValue());
                    break;
                }
            }
        }
        finally {
            LOGGER.debug("Update model has finished.");
            updateModelInProgress.set(false);
        }
    }

    @Override
    protected void refreshPorts() {

        Node node = mainModel.getSelectedNode();
        if (node != null) {
            if (node.getNode().isPortFlatModelAvailable()) {
                if (CollectionUtils.isNotEmpty(node.getGenericPorts())) {
                    mainModel.getServoPorts();
                }
                else {
                    LOGGER.info(
                        "The node supports flat port model but no generic ports are available. Skip get servo ports.");
                }
            }
            else {
                mainModel.getServoPorts();
            }
        }
    }

    // TODO make this method abstract after all ports are moved to Node
    protected List<ServoPort> getPorts() {
        LOGGER.info("Get ports returns: {}", ports);
        return selectedNode.getServoPorts();
    }

}
