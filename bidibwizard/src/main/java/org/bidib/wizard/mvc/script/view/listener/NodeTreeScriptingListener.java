package org.bidib.wizard.mvc.script.view.listener;

import org.bidib.wizard.mvc.main.model.Node;

public interface NodeTreeScriptingListener {
    /**
     * Set the selected node in the node list.
     * 
     * @param node
     *            the new selected node
     */
    void setSelectedNode(Node node);
}
