package org.bidib.wizard.mvc.main.view.panel;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.AbstractAction;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;

import org.apache.commons.collections4.CollectionUtils;
import org.bidib.wizard.comm.MotorPortStatus;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.main.controller.MotorPortPanelController;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.MotorPort;
import org.bidib.wizard.mvc.main.model.MotorPortTableModel;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.model.Port;
import org.bidib.wizard.mvc.main.model.SimplePortTableModel;
import org.bidib.wizard.mvc.main.model.listener.MotorPortListener;
import org.bidib.wizard.mvc.main.model.listener.PortValueListener;
import org.bidib.wizard.mvc.main.view.table.MotorPortTable;
import org.bidib.wizard.mvc.main.view.table.MotorSliderEditor;
import org.bidib.wizard.mvc.main.view.table.PortConfigErrorAwareEditor;
import org.bidib.wizard.mvc.main.view.table.PortConfigErrorAwareRenderer;
import org.bidib.wizard.mvc.main.view.table.SliderEditor;

import com.jidesoft.grid.TableColumnChooser;

public class MotorPortListPanel
    extends SimplePortListPanel<MotorPortStatus, MotorPort, MotorPortListener<MotorPortStatus>>
    implements PortValueListener<MotorPortStatus> {
    private static final long serialVersionUID = 1L;

    private final Collection<MotorPortListener<MotorPortStatus>> motorPortListeners = new LinkedList<>();

    private final MainModel mainModel;

    private AtomicBoolean updateModelInProgress = new AtomicBoolean();

    // TODO move to controller
    private Node selectedNode;

    public MotorPortListPanel(final MotorPortPanelController controller, MainModel mainModel) {
        super(new MotorPortTableModel(mainModel), mainModel.getMotorPorts(),
            Resources.getString(MotorPortListPanel.class, "emptyTable"));

        this.mainModel = mainModel;
    }

    @Override
    protected void createTable(
        final SimplePortTableModel<MotorPortStatus, MotorPort, MotorPortListener<MotorPortStatus>> tableModel,
        String emptyTableText) {

        table = new MotorPortTable(tableModel, emptyTableText) {
            private static final long serialVersionUID = 1L;

            @Override
            protected void sliderStateChanged(MotorPort motorPort) {
                if (!updateModelInProgress.get()) {
                    fireTestButtonPressed(motorPort);
                }
                else {
                    LOGGER.info("Do not send new port status to motor because the model was updated: {}", motorPort);
                }
            }
        };

        // do not allow drag columns to other position
        table.getTableHeader().setReorderingAllowed(false);

        table.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);

        // disabled sorting
        table.setSortable(false);

        // let the left and right key change the slider value and <numeric block +> and <numeric block ->
        table.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(
            KeyStroke.getKeyStroke(KeyEvent.VK_SUBTRACT, 0), "left");
        table.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0),
            "left");
        table.getActionMap().put("left", new AbstractAction() {
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {
                JTable table = (JTable) e.getSource();
                TableCellEditor editor = table.getCellEditor(table.getSelectedRow(), table.getSelectedColumn());

                if (editor instanceof SliderEditor) {
                    table.editCellAt(table.getSelectedRow(), table.getSelectedColumn());
                    ((SliderEditor) editor).setDown();
                }
            }
        });
        // slider value up with <numeric block +> and <cursor right>
        table.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_ADD, 0),
            "right");
        table.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0),
            "right");
        table.getActionMap().put("right", new AbstractAction() {
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {
                JTable table = (JTable) e.getSource();
                TableCellEditor editor = table.getCellEditor(table.getSelectedRow(), table.getSelectedColumn());

                if (editor instanceof SliderEditor) {
                    table.editCellAt(table.getSelectedRow(), table.getSelectedColumn());
                    ((SliderEditor) editor).setUp();
                }
            }
        });

        // slider stop with <numeric block 0>
        table.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_NUMPAD0, 0),
            "stop");
        table.getActionMap().put("stop", new AbstractAction() {
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {
                JTable table = (JTable) e.getSource();
                TableCellEditor editor = table.getCellEditor(table.getSelectedRow(), table.getSelectedColumn());

                if (editor instanceof SliderEditor) {
                    table.editCellAt(table.getSelectedRow(), table.getSelectedColumn());
                    ((MotorSliderEditor) editor).stop();
                }
            }
        });

        // TODO the motor ports are moved to node
        ((MotorPortTableModel) tableModel).getMainModel().addMotorPortValueListener(this);

        TableColumn tc = table.getColumnModel().getColumn(MotorPortTableModel.COLUMN_LABEL);
        tc.setCellRenderer(new PortConfigErrorAwareRenderer(MotorPortTableModel.COLUMN_LABEL));
        tc.setCellEditor(new PortConfigErrorAwareEditor(MotorPortTableModel.COLUMN_PORT_INSTANCE));
        tc.setIdentifier(Integer.valueOf(MotorPortTableModel.COLUMN_LABEL));

        TableColumnChooser.hideColumn(table, MotorPortTableModel.COLUMN_PORT_INSTANCE);

    }

    private void fireTestButtonPressed(MotorPort motorPort) {
        for (MotorPortListener<MotorPortStatus> l : motorPortListeners) {
            l.testButtonPressed(motorPort);
        }
    }

    @Override
    public void listChanged() {

        if (selectedNode != null) {
            selectedNode.removePortListListener(MotorPort.class, this);
        }

        // update the selected node
        selectedNode = mainModel.getSelectedNode();

        try {
            updateModelInProgress.set(true);
            LOGGER.info("Update model has started, call clearTable.");

            table.clearTable();
            if (selectedNode != null) {
                List<MotorPort> motorPorts = new LinkedList<>();
                motorPorts.addAll(selectedNode.getMotorPorts());
                for (MotorPort motorPort : motorPorts) {
                    tableModel.addRow(motorPort);
                }
            }
            table.packColumn(table.getColumnCount() - 1, 2);
        }
        finally {
            LOGGER.debug("Update model has finished.");
            updateModelInProgress.set(false);
        }

        if (selectedNode != null) {
            selectedNode.addPortListListener(MotorPort.class, this);
        }
    }

    @Override
    public void valueChanged(Port<MotorPortStatus> port) {
        LOGGER.info("The value of a motor port has changed: {}, value: {}", port, ((MotorPort) port).getValue());

        try {
            // do not trigger sending new configuration to port ...
            updateModelInProgress.set(true);
            LOGGER.debug("Update model has started.");

            int rowCount = tableModel.getRowCount();
            for (int row = 0; row < rowCount; row++) {
                MotorPort motorPort = (MotorPort) tableModel.getValueAt(row, MotorPortTableModel.COLUMN_PORT_INSTANCE);
                if (motorPort.getId() == port.getId()) {
                    LOGGER.debug("Found the matching port: {}", motorPort);
                    ((MotorPortTable) table).updateSliderPosition(row, MotorPortTableModel.COLUMN_VALUE,
                        motorPort.getValue());
                    break;
                }
            }
        }
        finally {
            LOGGER.info("Update model has finished.");
            updateModelInProgress.set(false);
        }
    }

    @Override
    protected void refreshPorts() {

        Node node = mainModel.getSelectedNode();
        if (node != null) {
            if (node.getNode().isPortFlatModelAvailable()) {
                if (CollectionUtils.isNotEmpty(node.getGenericPorts())) {
                    mainModel.getMotorPorts();
                }
                else {
                    LOGGER.info(
                        "The node supports flat port model but no generic ports are available. Skip get motor ports.");
                }
            }
            else {
                mainModel.getMotorPorts();
            }
        }
    }

    // TODO make this method abstract after all ports are moved to Node
    protected List<MotorPort> getPorts() {
        LOGGER.info("Get ports returns: {}", ports);
        return selectedNode.getMotorPorts();
    }

    public void addPortListener(final MotorPortListener<MotorPortStatus> listener) {
        LOGGER.info("Add motor port listener: {}", listener);
        // portListener = listener;

        ((MotorPortTableModel) tableModel).addMotorPortListener(listener);
        motorPortListeners.add(listener);
        super.addPortListener(listener);
    }
}
