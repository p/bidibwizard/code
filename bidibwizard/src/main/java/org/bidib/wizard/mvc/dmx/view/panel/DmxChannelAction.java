package org.bidib.wizard.mvc.dmx.view.panel;

import java.awt.Point;
import java.awt.event.ActionEvent;

import org.bidib.wizard.mvc.dmx.model.DmxChannel;
import org.bidib.wizard.mvc.main.model.Port;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The <code>DmxChannelAction</code> creates a point for the dmxChannel without a port assigned.
 */
public class DmxChannelAction extends LocationAwareAction<DmxChannel> {

    private static final Logger LOGGER = LoggerFactory.getLogger(DmxChannelAction.class);

    private static final long serialVersionUID = 1L;

    public DmxChannelAction(DmxChannel dmxChannel, DmxChartPanel dmxChartPanel) {
        super(dmxChannel.toString(), dmxChannel, dmxChartPanel);
    }

    @Override
    public void actionPerformed(ActionEvent event) {

        Point currentMousePoint = dmxChartPanel.getCurrentMousePoint();

        String seriesKey = Integer.toString(getActionObject().getChannelId());
        LOGGER.info("Selected series key: {}, currentMousePoint: {}", seriesKey, currentMousePoint);

        final int currentX = (int) Math.round(currentMousePoint.getX());
        final int currentY = (int) Math.round(currentMousePoint.getY());

        DmxChartPanel.LOGGER.info("Add new point at X: {}, Y: {}", currentX, currentY);

        LOGGER.info("Create a new point without a port at X: {}, Y: {}", currentX, currentY);

        dmxChartPanel.createDataItem(seriesKey, currentX, currentY, (Port<?>) null, getActionObject(), null);
    }
}
