package org.bidib.wizard.mvc.main.model;

import java.beans.IndexedPropertyChangeEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import javax.swing.SwingUtilities;

import org.bidib.wizard.comm.FeedbackPortStatus;
import org.bidib.wizard.mvc.main.controller.FeedbackPortStatusChangeProvider;
import org.bidib.wizard.mvc.main.model.listener.FeedbackPortListener;
import org.bidib.wizard.mvc.main.model.listener.PortListListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.beans.Model;

public class FeedbackPortModel extends Model implements FeedbackPortStatusChangeProvider {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(FeedbackPortModel.class);

    private Node selectedNode;

    private PropertyChangeListener pclFeedbackPorts;

    private PropertyChangeListener pclFeedbackPort;

    private final List<FeedbackPortListener<FeedbackPortStatus>> feedbackPortListeners = new LinkedList<>();

    private final List<PortListListener> portListListeners = new LinkedList<>();

    public FeedbackPortModel() {

        pclFeedbackPort = new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                LOGGER.debug("Property has changed, evt: {}", evt);

                if (evt instanceof IndexedPropertyChangeEvent) {
                    final IndexedPropertyChangeEvent ipce = (IndexedPropertyChangeEvent) evt;

                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            final int portId = ipce.getIndex();
                            final Node node = (Node) ipce.getSource();
                            LOGGER.debug("The port status has changed: {}, emitting node: {}", portId, node);

                            if (/* selectedNode != null && */node.equals(selectedNode)) {
                                List<FeedbackPort> feedbackPorts = selectedNode.getFeedbackPorts();
                                FeedbackPort port = feedbackPorts.get(portId);
                                if (port != null) {
                                    for (FeedbackPortListener<FeedbackPortStatus> feedbackPortListener : feedbackPortListeners) {
                                        switch (ipce.getPropertyName()) {
                                            case Node.PROPERTY_FEEDBACKPORT_STATUS:
                                                FeedbackPortStatus status = port.getStatus();
                                                feedbackPortListener.statusChanged(port, status);
                                                break;
                                            case Node.PROPERTY_FEEDBACKPORT_ADDRESSES:
                                                List<FeedbackAddressData> addresses = port.getAddresses();
                                                feedbackPortListener.addressesChanged(port, addresses);
                                                break;
                                            case Node.PROPERTY_FEEDBACKPORT_CONFIDENCE:
                                                FeedbackConfidenceData confidence = port.getConfidence();
                                                feedbackPortListener.confidenceChanged(port, confidence);
                                                break;
                                            case Node.PROPERTY_FEEDBACKPORT_DYNSTATES:
                                                Collection<FeedbackDynStateData> dynStates = port.getDynStates();
                                                feedbackPortListener.dynStatesChanged(port, dynStates);
                                                break;
                                            default:
                                                break;
                                        }
                                    }
                                }
                                else {
                                    LOGGER.warn("No feedback port found for portId: {}", portId);
                                }
                            }
                        }
                    });

                }
            }
        };
        pclFeedbackPorts = new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                LOGGER.info("Property has changed, evt: {}", evt);
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {

                        for (PortListListener listener : portListListeners) {
                            LOGGER.info("Notify listener that the ports have changed: {}", listener);
                            listener.listChanged();
                        }
                    }
                });
            }
        };

    }

    @Override
    public Node getSelectedNode() {
        return selectedNode;
    }

    /**
     * @param selectedNode
     *            the selectedNode to set
     */
    public void setSelectedNode(Node selectedNode) {

        if (this.selectedNode != null) {
            // unregister node listener
            LOGGER.info("Remove pcl from previous selected node: {}", selectedNode);
            this.selectedNode.removePropertyChangeListener(Node.PROPERTY_FEEDBACKPORTS, pclFeedbackPorts);
            this.selectedNode.removePropertyChangeListener(Node.PROPERTY_FEEDBACKPORT_STATUS, pclFeedbackPort);
            this.selectedNode.removePropertyChangeListener(Node.PROPERTY_FEEDBACKPORT_ADDRESSES, pclFeedbackPort);
            this.selectedNode.removePropertyChangeListener(Node.PROPERTY_FEEDBACKPORT_CONFIDENCE, pclFeedbackPort);
            this.selectedNode.removePropertyChangeListener(Node.PROPERTY_FEEDBACKPORT_DYNSTATES, pclFeedbackPort);
        }

        this.selectedNode = selectedNode;

        if (this.selectedNode != null) {
            // register node listener
            LOGGER.info("Add pcl to currently selected node: {}", selectedNode);
            this.selectedNode.addPropertyChangeListener(Node.PROPERTY_FEEDBACKPORTS, pclFeedbackPorts);
            this.selectedNode.addPropertyChangeListener(Node.PROPERTY_FEEDBACKPORT_STATUS, pclFeedbackPort);
            this.selectedNode.addPropertyChangeListener(Node.PROPERTY_FEEDBACKPORT_ADDRESSES, pclFeedbackPort);
            this.selectedNode.addPropertyChangeListener(Node.PROPERTY_FEEDBACKPORT_CONFIDENCE, pclFeedbackPort);
            this.selectedNode.addPropertyChangeListener(Node.PROPERTY_FEEDBACKPORT_DYNSTATES, pclFeedbackPort);
        }

    }

    @Override
    public void addFeedbackPortListener(final FeedbackPortListener<FeedbackPortStatus> feedbackPortListener) {
        feedbackPortListeners.add(feedbackPortListener);
    }

    @Override
    public void removeFeedbackPortListener(final FeedbackPortListener<FeedbackPortStatus> feedbackPortListener) {
        feedbackPortListeners.remove(feedbackPortListener);
    }

    @Override
    public void addPortListListener(PortListListener portListListener) {
        portListListeners.add(portListListener);
    }

    @Override
    public void removePortListListener(PortListListener portListListener) {
        portListListeners.remove(portListListener);
    }
}
