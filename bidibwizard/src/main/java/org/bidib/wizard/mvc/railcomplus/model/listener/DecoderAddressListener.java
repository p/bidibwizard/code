package org.bidib.wizard.mvc.railcomplus.model.listener;

import org.bidib.wizard.mvc.railcomplus.model.RailcomPlusDecoderModel;

public interface DecoderAddressListener {

    void setAddress(RailcomPlusDecoderModel decoder);

    void queryAddress(RailcomPlusDecoderModel decoder);
}
