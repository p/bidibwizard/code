package org.bidib.wizard.mvc.pomupdate.view.listener;

public interface DecoderInfoStatusListener {

    /**
     * Status update notification.
     * 
     * @param progress
     *            the new progress value
     */
    void updateStatus(int progress);

    /**
     * Signal that the update has finished.
     */
    void finished();
}
