package org.bidib.wizard.mvc.main.view.menu;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.Collection;
import java.util.LinkedList;

import javax.swing.Box;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

import org.bidib.jbidibc.core.enumeration.IdentifyState;
import org.bidib.jbidibc.core.enumeration.SysErrorEnum;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.model.listener.DefaultNodeListListener;
import org.bidib.wizard.mvc.main.model.listener.NodeListener;
import org.bidib.wizard.mvc.main.view.menu.listener.MainMenuListener;
import org.bidib.wizard.mvc.main.view.panel.NodeListPanel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vlsolutions.swing.docking.DockKey;
import com.vlsolutions.swing.docking.Dockable;
import com.vlsolutions.swing.docking.DockableState;
import com.vlsolutions.swing.docking.DockingDesktop;
import com.vlsolutions.swing.docking.event.DockableStateChangeEvent;
import com.vlsolutions.swing.docking.event.DockableStateChangeListener;

public class MainMenuBar extends JMenuBar implements NodeListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(MainMenuBar.class);

    private static final long serialVersionUID = 1L;

    private final Collection<MainMenuListener> menuListeners = new LinkedList<MainMenuListener>();

    private final NodeListMenu nodeListMenu;

    private final WindowListMenu windowListMenu;

    private Node selectedNode;

    private DockingFramesRegistry dockingFramesRegistry;

    private JMenuItem connectItem;

    private JMenuItem disconnectItem;

    private JMenuItem railcomPlusItem;

    // private JMenuItem pomUpdateItem;

    private JMenuItem debugInterfaceItem;

    public MainMenuBar(final MainModel model, final DockingDesktop desktop) {

        addDockableStateChangeListener(desktop);
        windowListMenu = new WindowListMenu(dockingFramesRegistry);
        nodeListMenu = new NodeListMenu(model);

        JMenu fileMenu = new JMenu(Resources.getString(getClass(), "file"));
        connectItem = new JMenuItem(Resources.getString(getClass(), "connect"));

        connectItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.ALT_MASK));
        connectItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireConnect();
            }
        });
        fileMenu.add(connectItem);
        disconnectItem = new JMenuItem(Resources.getString(getClass(), "disconnect"));

        disconnectItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, ActionEvent.ALT_MASK));
        disconnectItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireDisconnect();
            }
        });
        disconnectItem.setEnabled(false);
        fileMenu.add(disconnectItem);

        fileMenu.addSeparator();

        JMenuItem exitItem = new JMenuItem(Resources.getString(getClass(), "quit"));

        exitItem.setAccelerator(
            KeyStroke.getKeyStroke(KeyEvent.VK_Q, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
        exitItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireExit();
            }
        });
        fileMenu.add(exitItem);
        add(fileMenu);

        JMenu editMenu = new JMenu(Resources.getString(getClass(), "edit"));
        JMenuItem preferencesItem = new JMenuItem(Resources.getString(getClass(), "preferences") + " ...");

        preferencesItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                firePreferences();
            }
        });
        editMenu.add(preferencesItem);
        add(editMenu);

        final JMenu nodeMenu = nodeListMenu.getJMenu();

        nodeMenu.addMenuListener(new MenuListener() {
            @Override
            public void menuCanceled(MenuEvent e) {
            }

            @Override
            public void menuDeselected(MenuEvent e) {
            }

            @Override
            public void menuSelected(MenuEvent e) {

                if (e.getSource() instanceof JMenu) {
                    if (!((JMenu) e.getSource()).isEnabled()) {
                        LOGGER.debug("The menu is not enabled, skip processing.");
                        return;
                    }
                }
                // update the menu items
                try {
                    NodeListPanel nodeListPanel =
                        DefaultApplicationContext.getInstance().get("nodeListPanel", NodeListPanel.class);
                    Node node = nodeListPanel.getSelectedItem();
                    // Node node = model.getSelectedNode();
                    if (node != null) {
                        nodeListMenu.updateMenuItems(node);
                    }
                }
                catch (Exception ex) {
                    LOGGER.warn("Update menu items of nodeListMenu failed.", ex);
                    JOptionPane.showMessageDialog(JOptionPane.getFrameForComponent(null),
                        Resources.getString(MainMenuBar.class, "menuitemserror.message"),
                        Resources.getString(MainMenuBar.class, "menuitemserror.title"), JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        nodeMenu.setEnabled(false);
        nodeMenu.setText(Resources.getString(getClass(), "node"));
        add(nodeMenu);

        model.addNodeListListener(new DefaultNodeListListener() {
            @Override
            public void nodeChanged() {

                if (SwingUtilities.isEventDispatchThread()) {
                    internalNodeChanged(model, nodeMenu);
                }
                else {
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            internalNodeChanged(model, nodeMenu);
                        }
                    });
                }
            }
        });

        final JMenu framesMenu = windowListMenu.getJMenu();

        final JMenuItem boosterTableItem = new JMenuItem(Resources.getString(getClass(), "boosterTable"));

        boosterTableItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireBoosterTable();
            }
        });

        // always add booster table at first position
        String dockKeyKey = "BoosterTableView";
        windowListMenu.addStickyItem(dockKeyKey, boosterTableItem);

        final JMenuItem consoleItem = new JMenuItem(Resources.getString(getClass(), "console"));

        consoleItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireConsole();
            }
        });

        // always add booster table at first position
        dockKeyKey = "ConsoleView";
        windowListMenu.addStickyItem(dockKeyKey, consoleItem);

        framesMenu.addMenuListener(new MenuListener() {

            @Override
            public void menuSelected(MenuEvent e) {
                windowListMenu.prepareMenu();
            }

            @Override
            public void menuDeselected(MenuEvent e) {
            }

            @Override
            public void menuCanceled(MenuEvent e) {
            }
        });
        // framesMenu.setEnabled(false);
        framesMenu.setText(Resources.getString(getClass(), "window"));
        add(framesMenu);

        JMenu toolsMenu = new JMenu(Resources.getString(getClass(), "tools"));
        JMenuItem nodeScriptItem = new JMenuItem(Resources.getString(getClass(), "nodeScript"));

        nodeScriptItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, ActionEvent.ALT_MASK));
        nodeScriptItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireNodeScript(model);
            }
        });
        toolsMenu.add(nodeScriptItem);

        railcomPlusItem = new JMenuItem(Resources.getString(getClass(), "railcomPlus"));

        railcomPlusItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.ALT_MASK));
        railcomPlusItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireRailcomPlus(model);
            }
        });

        // the POM update menu item is enabled depending on the feature of the command station!
        railcomPlusItem.setEnabled(false);
        toolsMenu.add(railcomPlusItem);

        // pomUpdateItem = new JMenuItem(Resources.getString(getClass(), "pomUpdate"));
        //
        // pomUpdateItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, ActionEvent.ALT_MASK));
        // pomUpdateItem.addActionListener(new ActionListener() {
        // @Override
        // public void actionPerformed(ActionEvent e) {
        // firePomUpdate(model);
        // }
        // });
        //
        // // the POM update menu item is enabled depending on the feature of the command station!
        // pomUpdateItem.setEnabled(false);
        // toolsMenu.add(pomUpdateItem);

        // the developer tools
        final JMenu developerToolsMenu = new JMenu(Resources.getString(getClass(), "developerTools"));
        toolsMenu.add(developerToolsMenu);

        // the debug interface
        debugInterfaceItem = new JMenuItem(Resources.getString(getClass(), "debugInterface"));

        debugInterfaceItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, ActionEvent.ALT_MASK));
        debugInterfaceItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireDebugInterface();
            }
        });

        // the debug interface menu item is always enabled
        debugInterfaceItem.setEnabled(true);
        developerToolsMenu.add(debugInterfaceItem);

        // the ping table
        final JMenuItem pingTableItem = new JMenuItem(Resources.getString(getClass(), "pingTable"));

        pingTableItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                firePingTable();
            }
        });

        developerToolsMenu.add(pingTableItem);

        // add tools sub menu to menubar
        add(toolsMenu);

        add(Box.createHorizontalGlue());

        JMenu helpMenu = new JMenu(Resources.getString(getClass(), "help"));
        JMenuItem aboutItem = new JMenuItem(Resources.getString(getClass(), "about") + " ...");

        aboutItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireAbout();
            }
        });
        helpMenu.add(aboutItem);

        JMenuItem logPanelItem = new JMenuItem(Resources.getString(getClass(), "logPanel") + " ...");

        logPanelItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireLogPanel();
            }
        });
        helpMenu.add(logPanelItem);

        JMenuItem logFilesItem = new JMenuItem(Resources.getString(getClass(), "logFiles") + " ...");

        logFilesItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireCollectLogFiles();
            }
        });
        helpMenu.add(logFilesItem);
        add(helpMenu);
    }

    private void internalNodeChanged(MainModel model, JMenu nodeMenu) {
        if (getSelectedNode() != null) {
            getSelectedNode().removeNodeListener(MainMenuBar.this);
        }

        Node newNode = model.getSelectedNode();

        nodeMenu.setEnabled(newNode != null);
        if (newNode != null) {
            // add as listener to the selected node
            newNode.addNodeListener(MainMenuBar.this);
        }

        LOGGER.info("Set the selected node: {}", newNode);
        setSelectedNode(newNode);
    }

    private void addDockableStateChangeListener(final DockingDesktop desktop) {

        dockingFramesRegistry = new DockingFramesRegistry();

        DockableStateChangeListener listener = new DockableStateChangeListener() {
            @Override
            public void dockableStateChanged(DockableStateChangeEvent event) {
                DockableState newState = event.getNewState();
                LOGGER.info("The state of the dockable has changed: {}", event);
                if (newState.isDocked()) {
                    // a new frame is docked
                    DockKey dockKey = newState.getDockable().getDockKey();
                    Dockable dockable = dockingFramesRegistry.getDockable(dockKey);
                    if (dockable != null) {
                        LOGGER.debug("Dockable already registered: {}", dockKey);
                    }
                    else {
                        dockable = newState.getDockable();
                        LOGGER.info("Register new dockable: {}", dockable);
                        dockingFramesRegistry.addDockable(dockKey, dockable);
                    }

                }
                if (newState.isClosed()) {
                    DockKey dockKey = newState.getDockable().getDockKey();
                    dockingFramesRegistry.removeDockable(dockKey);

                    // the dockable has been closed
                    if (newState.getDockable() instanceof DockableStateChangeListener) {
                        desktop.removeDockableStateChangeListener((DockableStateChangeListener) newState.getDockable());
                    }
                    desktop.unregisterDockable(newState.getDockable()); // forget about it !
                }
            }
        };
        desktop.addDockableStateChangeListener(listener);
    }

    @Override
    public void addressMessagesEnabledChanged(Boolean isAddressMessagesEnabled) {
        nodeListMenu.setAddressMessagesEnabled(isAddressMessagesEnabled);
    }

    public void addMenuListener(MainMenuListener l) {
        menuListeners.add(l);
    }

    @Override
    public void dccStartEnabledChanged(Boolean isDccStartEnabled) {
        nodeListMenu.setDccStartEnabled(isDccStartEnabled);
    }

    @Override
    public void railComPlusAvailableChanged(Boolean railComPlusAvailable) {
        LOGGER.info("Set the railComPlus item available: {}", railComPlusAvailable);
        railcomPlusItem.setEnabled(railComPlusAvailable != null ? railComPlusAvailable : false);
    }

    @Override
    public void pomUpdateAvailableChanged(Boolean pomUpdateAvailable) {
        LOGGER.info("Set the pomUpdate item available: {}", pomUpdateAvailable);
        // pomUpdateItem.setEnabled(pomUpdateAvailable != null ? pomUpdateAvailable : false);
    }

    @Override
    public void externalStartEnabledChanged(Boolean isExternalStartEnabled) {
        nodeListMenu.setExternalStartEnabled(isExternalStartEnabled);
    }

    @Override
    public void feedbackMessagesEnabledChanged(Boolean isFeedbackMessagesEnabled) {
        nodeListMenu.setFeedbackMessagesEnabled(isFeedbackMessagesEnabled);
    }

    @Override
    public void feedbackMirrorDisabledChanged(Boolean isFeedbackMirrorDisabled) {
        nodeListMenu.setFeedbackMirrorDisabled(isFeedbackMirrorDisabled);
    }

    private void fireBoosterTable() {
        for (MainMenuListener l : menuListeners) {
            l.boosterTable();
        }
    }

    private void firePingTable() {
        for (MainMenuListener l : menuListeners) {
            l.pingTable();
        }
    }

    private void fireLogPanel() {
        for (MainMenuListener l : menuListeners) {
            l.logPanel();
        }
    }

    private void fireConsole() {
        for (MainMenuListener l : menuListeners) {
            l.console();
        }
    }

    private void fireCollectLogFiles() {
        for (MainMenuListener l : menuListeners) {
            l.collectLogFiles();
        }
    }

    private void fireAbout() {
        for (MainMenuListener l : menuListeners) {
            l.about();
        }
    }

    private void fireExit() {
        for (MainMenuListener l : menuListeners) {
            l.exit();
        }
    }

    private void fireConnect() {
        for (MainMenuListener l : menuListeners) {
            l.connect();
        }
    }

    private void fireDisconnect() {
        for (MainMenuListener l : menuListeners) {
            l.disconnect();
        }
    }

    private void firePreferences() {
        for (MainMenuListener l : menuListeners) {
            l.preferences();
        }
    }

    private void fireNodeScript(final MainModel mainModel) {
        for (MainMenuListener l : menuListeners) {
            l.nodeScript(mainModel);
        }
    }

    private void fireRailcomPlus(final MainModel mainModel) {
        for (MainMenuListener l : menuListeners) {
            l.railcomPlus(mainModel);
        }
    }

    private void firePomUpdate(final MainModel mainModel) {
        for (MainMenuListener l : menuListeners) {
            l.pomUpdate(mainModel);
        }
    }

    private void fireDebugInterface() {
        for (MainMenuListener l : menuListeners) {
            l.debugInterface();
        }
    }

    private Node getSelectedNode() {
        return selectedNode;
    }

    @Override
    public void identifyStateChanged(IdentifyState identifyState) {
        // identify state of the selected node has changed
        nodeListMenu.setIdentifyStateSelected(identifyState);
    }

    @Override
    public void keyMessagesEnabledChanged(Boolean isKeyMessagesEnabled) {
        nodeListMenu.setKeyMessagesEnabled(isKeyMessagesEnabled);
    }

    @Override
    public void labelChanged(String label) {
    }

    private void setSelectedNode(Node node) {
        this.selectedNode = node;

        if (selectedNode != null) {
            railComPlusAvailableChanged(
                selectedNode.isRailComPlusAvailable() != null ? selectedNode.isRailComPlusAvailable() : false);
            pomUpdateAvailableChanged(
                selectedNode.isPomUpdateAvailable() != null ? selectedNode.isPomUpdateAvailable() : false);
        }
        else {
            railComPlusAvailableChanged(false);
            pomUpdateAvailableChanged(false);
        }
    }

    public void opening() {
        LOGGER.info("Port is opening.");
        connectItem.setEnabled(false);
        // disconnectItem.setEnabled(true);
    }

    public void opened(String port) {
        connectItem.setEnabled(false);
        disconnectItem.setEnabled(true);
    }

    public void closed(String port) {
        connectItem.setEnabled(true);
        disconnectItem.setEnabled(false);
    }

    @Override
    public void sysErrorChanged(SysErrorEnum sysError) {

    }
}
