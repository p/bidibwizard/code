package org.bidib.wizard.mvc.main.controller;

import java.util.Collection;
import java.util.Map;

import org.bidib.jbidibc.core.enumeration.CommandStationState;
import org.bidib.jbidibc.core.helpers.Context;
import org.bidib.jbidibc.core.port.PortConfigValue;
import org.bidib.wizard.mvc.main.model.Accessory;
import org.bidib.wizard.mvc.main.model.Macro;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.model.listener.NodeListListener;
import org.bidib.wizard.mvc.main.model.listener.NodeSelectionListener;
import org.bidib.wizard.mvc.script.view.NodeScripting;
import org.bidib.wizard.script.node.types.TargetType;

public interface MainControllerInterface {

    void stop();

    void openConnection();

    void closeConnection();

    void clearNodes();

    Collection<Node> getNodes();

    void addNodeListListener(NodeListListener nodeListListener);

    void removeNodeListListener(NodeListListener nodeListListener);

    void addNodeSelectionListListener(NodeSelectionListener l);

    void removeNodeSelectionListener(NodeSelectionListener l);

    NodeScripting getNodeScripting();

    /**
     * Switch all boosters off.
     */
    void allBoosterOff();

    /**
     * Switch all boosters on.
     * 
     * @param boosterAndCommandStation
     *            switch all booster and command station on
     * @param requestedCommandStationState
     *            the requested command station state
     */
    void allBoosterOn(boolean boosterAndCommandStation, CommandStationState requestedCommandStationState);

    void replaceMacro(Macro macro, boolean saveOnNode);

    void resetNode(Node node);

    void replaceAccessory(Accessory accessory, boolean saveOnNode);

    /**
     * Replace the port config.
     * 
     * @param portType
     *            the port type
     * @param portConfig
     *            the new port config
     */
    void replacePortConfig(TargetType portType, final Map<Byte, PortConfigValue<?>> portConfig);

    /**
     * Reload the CV definition of the node.
     * 
     * @param context
     *            the context
     * @param node
     *            the node
     */
    void reloadCvDefinition(final Context context, final Node node);
}
