package org.bidib.wizard.mvc.cvprogrammer.view.panel.listener;

public interface CvReadListener {
    void read(int cvNumber);
}
