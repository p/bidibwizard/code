package org.bidib.wizard.mvc.main.model.listener;

import org.bidib.wizard.mvc.main.model.Node;

public interface NodeSelectionListener {

    /**
     * Signals that the selected node has changed.
     * 
     * @param selectedNode
     *            the selected node
     */
    void selectedNodeChanged(final Node selectedNode);
}
