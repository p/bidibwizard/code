package org.bidib.wizard.mvc.accessory.view.panel.listener;

import org.bidib.jbidibc.core.enumeration.AccessoryAcknowledge;

public interface AccessoryResultListener {

    /**
     * Sets the listener active or inactive.
     * 
     * @param active
     *            the active flag
     */
    void setActive(boolean active);

    /**
     * @return listener is active
     */
    boolean isActive();

    /**
     * Add a new line to the logger area.
     * 
     * @param logLine
     *            the message to log. Use {} as placeholders for args.
     * @param args
     *            the args
     */
    void addLogText(String logLine, Object... args);

    void signalAcknowledgeChanged(AccessoryAcknowledge acknowledge);
}
