package org.bidib.wizard.mvc.main.model.listener;

public interface PortListListener {
    /**
     * The port list has been changed.
     */
    void listChanged();
}
