package org.bidib.wizard.mvc.main.view.component;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.UIManager;

import org.apache.commons.lang.StringUtils;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.dialog.EscapeDialog;

import com.jgoodies.forms.builder.ButtonBarBuilder;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.debug.FormDebugPanel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.FormLayout;
import com.jidesoft.swing.MultilineLabel;

public class NodeErrorsDialog extends EscapeDialog {

    private static final long serialVersionUID = 1L;

    private JTextArea loggerArea;

    private final JButton closeButton = new JButton(Resources.getString(getClass(), "close"));

    private final JButton cancelButton = new JButton(Resources.getString(getClass(), "cancel"));

    private final static String NEWLINE = "\n";

    private MultilineLabel descriptionLabel;

    private Icon errorIcon;

    private int closeOption = JOptionPane.CANCEL_OPTION;

    public NodeErrorsDialog(Frame frame, String title, boolean modal) {
        super(frame, (StringUtils.isNotBlank(title) ? title : Resources.getString(NodeErrorsDialog.class, "title")),
            modal);

        // Load the icons
        errorIcon = UIManager.getDefaults().getIcon("OptionPane.errorIcon");

        DefaultFormBuilder builder = null;
        boolean debug = false;
        if (debug) {
            JPanel panel = new FormDebugPanel();
            builder = new DefaultFormBuilder(new FormLayout("pref, 3dlu, fill:50dlu:grow"), panel);
        }
        else {
            builder = new DefaultFormBuilder(new FormLayout("pref, 3dlu, fill:50dlu:grow"));
        }
        builder.border(Borders.DIALOG);

        builder.append(new JLabel(errorIcon));
        descriptionLabel = new MultilineLabel();
        builder.append(descriptionLabel);

        // prepare the logger area
        loggerArea = new JTextArea(20, 60);
        loggerArea.setFont(UIManager.getDefaults().getFont("Label.font"));

        loggerArea.setLineWrap(true);
        JScrollPane scrollPane = new JScrollPane(loggerArea);
        loggerArea.setEditable(false);

        builder.append(scrollPane, 3);

        closeButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                close();
            }
        });
        cancelButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                cancel();
            }
        });
        cancelButton.setEnabled(false);

        // prepare the close button
        JPanel buttons = new ButtonBarBuilder().addGlue().addButton(closeButton, cancelButton).build();
        builder.append(buttons, 3);

        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());
        contentPane.add(builder.build(), BorderLayout.CENTER);
    }

    private void close() {
        closeOption = JOptionPane.OK_OPTION;
        setVisible(false);

        dispose();
    }

    private void cancel() {
        closeOption = JOptionPane.CANCEL_OPTION;
        setVisible(false);

        dispose();
    }

    public void setErrors(String description, List<String> errors) {
        // clear text
        loggerArea.setText(null);
        if (StringUtils.isNotBlank(description)) {
            descriptionLabel.setText(description);
        }

        // set the errors
        for (String logLine : errors) {
            loggerArea.append(logLine + NEWLINE);
        }
        loggerArea.setCaretPosition(loggerArea.getDocument().getLength());
    }

    public void showDialog() {

        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    public void setCancelEnabled(boolean enabled) {
        if (cancelButton != null) {
            cancelButton.setEnabled(enabled);
        }
    }

    public void setCloseButtonText(String closeButtonText) {
        if (closeButton != null) {
            closeButton.setText(closeButtonText);
        }
    }

    public int getCloseOption() {
        return closeOption;
    }
}
