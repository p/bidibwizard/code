package org.bidib.wizard.mvc.dmx.view.panel.renderer;

import java.awt.Color;
import java.awt.Paint;

import org.jfree.chart.event.RendererChangeEvent;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;

/**
 * A custom renderer to provide mouseover highlights.
 */
public class DmxLineRenderer extends XYLineAndShapeRenderer {

    private static final long serialVersionUID = 1L;

    /** The row to highlight (-1 for none). */
    private int highlightRow = -1;

    /** The column to highlight (-1 for none). */
    private int highlightColumn = -1;

    /**
     * Sets the item to be highlighted (use (-1, -1) for no highlight).
     * 
     * @param row
     *            the row index.
     * @param column
     *            the column index.
     */
    public void setHighlightedItem(int row, int column) {
        if (this.highlightRow == row && this.highlightColumn == column) {
            return; // nothing to do
        }
        this.highlightRow = row;
        this.highlightColumn = column;
        notifyListeners(new RendererChangeEvent(this));
    }

    /**
     * Return a special colour for the highlighted item.
     * 
     * @param row
     *            the row index, the series in this case
     * @param column
     *            the column index, the item index in this case
     * 
     * @return The outline paint.
     */
    public Paint getItemOutlinePaint(int row, int column) {
        if (row == this.highlightRow && column == this.highlightColumn) {
            return Color.yellow;
        }
        return super.getItemOutlinePaint(row, column);
    }

    public boolean isHighlighted() {
        // TODO implement
        return (this.highlightRow > -1) && (this.highlightColumn > -1);
    }

    public int getRow() {
        return highlightRow;
    }

    public int getColumn() {
        return highlightColumn;
    }

}