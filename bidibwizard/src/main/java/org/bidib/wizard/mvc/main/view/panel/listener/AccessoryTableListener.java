package org.bidib.wizard.mvc.main.view.panel.listener;

import org.bidib.wizard.mvc.main.model.MacroRef;

public interface AccessoryTableListener {
    void delete(int[] rows);

    void insertEmptyAfter(int row);

    void insertEmptyBefore(int row);

    void copy(MacroRef[] macros);

    void cut(int[] rows, MacroRef[] macros);

    void pasteAfter(int row);

    void testButtonPressed(int aspect);
}
