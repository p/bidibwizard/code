package org.bidib.wizard.mvc.main.view.panel.listener;

import java.awt.Component;

public interface TabVisibilityListener {

    /**
     * Set the component visible or invisible.
     * 
     * @param component
     *            the component
     * @param visible
     *            the visible flag
     */
    void setTabVisible(final Component component, final boolean visible);
}
