package org.bidib.wizard.mvc.main.model;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class FeedbackConfidenceData {

    private boolean freeze;

    private boolean signal;

    private boolean valid;

    public FeedbackConfidenceData(boolean freeze, boolean signal, boolean valid) {
        this.freeze = freeze;
        this.signal = signal;
        this.valid = valid;
    }

    /**
     * @return the freeze
     */
    public boolean isFreeze() {
        return freeze;
    }

    /**
     * @param freeze
     *            the freeze to set
     */
    public void setFreeze(boolean freeze) {
        this.freeze = freeze;
    }

    /**
     * @return the signal flag
     */
    public boolean isSignal() {
        return signal;
    }

    /**
     * @return the no-signal flag
     */
    public boolean isNoSignal() {
        return !signal;
    }

    /**
     * @param signal
     *            the signal to set
     */
    public void setSignal(boolean signal) {
        this.signal = signal;
    }

    /**
     * @return the valid
     */
    public boolean isValid() {
        return valid;
    }

    /**
     * @return the void flag
     */
    public boolean isVoid() {
        return !valid;
    }

    /**
     * @param valid
     *            the valid to set
     */
    public void setValid(boolean valid) {
        this.valid = valid;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
