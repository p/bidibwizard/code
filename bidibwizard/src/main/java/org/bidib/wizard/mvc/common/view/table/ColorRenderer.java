package org.bidib.wizard.mvc.common.view.table;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class ColorRenderer extends DefaultTableCellRenderer {
    private static final long serialVersionUID = 1L;

    // private Border unselectedBorder;
    //
    // private Border selectedBorder;

    // private boolean isBordered;

    public ColorRenderer(/* boolean isBordered */) {
        // this.isBordered = isBordered;
    }

    public Component getTableCellRendererComponent(
        JTable table, Object color, boolean isSelected, boolean hasFocus, int row, int column) {

        // don't set the background color because we use the highlighter to set the color

        super.getTableCellRendererComponent(table, color, isSelected, hasFocus, row, column);
        // Component comp = super.getTableCellRendererComponent(table, color, isSelected, hasFocus, row, column);

        // if (isBordered) {
        // if (isSelected) {
        // if (selectedBorder == null) {
        // selectedBorder = BorderFactory.createMatteBorder(2, 5, 2, 5, table.getSelectionBackground());
        // }
        // setBorder(selectedBorder);
        // }
        // else {
        // if (unselectedBorder == null) {
        // unselectedBorder = BorderFactory.createMatteBorder(2, 5, 2, 5, table.getBackground());
        // }
        // setBorder(unselectedBorder);
        // }
        // }

        if (color instanceof Integer) {
            Integer colValue = (Integer) color;
            Color col = new Color(colValue.intValue());
            super.setForeground(col);
            super.setBackground(col);
        }
        else if (color instanceof Color) {
            Color col = (Color) color;
            super.setForeground(col);
            super.setBackground(col);
        }

        return this;
    }
}