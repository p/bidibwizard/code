package org.bidib.wizard.mvc.main.view.table;

import java.awt.Color;
import java.awt.Component;

import javax.swing.UIManager;

import org.bidib.wizard.mvc.main.model.Port;
import org.jdesktop.swingx.decorator.AbstractHighlighter;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.util.PaintUtils;

/**
 * The PortHighlighter is only applied if a port is disabled.
 * 
 */
public class PortHighlighter extends AbstractHighlighter {

    private final Color disabledBackground;

    public PortHighlighter() {
        disabledBackground = UIManager.getColor("Label.background");
    }

    @Override
    protected Component doHighlight(Component renderer, ComponentAdapter adapter) {

        applyBackground(renderer, adapter);

        return renderer;
    }

    @Override
    protected boolean canHighlight(Component component, ComponentAdapter adapter) {
        Object value = adapter.getFilteredValueAt(adapter.row, adapter.column);

        if (value instanceof Port<?>) {
            if (!((Port<?>) value).isEnabled()) {
                // only use this highlighter if the port is disabled.
                return true;
            }
        }
        return false;
    }

    protected void applyBackground(Component renderer, ComponentAdapter adapter) {

        Color background = disabledBackground;
        if (adapter.isSelected()) {
            background = background.darker();
        }

        renderer.setBackground(PaintUtils.blend(renderer.getBackground(), background));
    }

}
