package org.bidib.wizard.mvc.pt.view.listener;

import org.bidib.jbidibc.core.enumeration.CommandStationState;
import org.bidib.jbidibc.core.enumeration.PtOperation;

public interface PtProgrammerViewListener {
    /**
     * Close the dialog.
     */
    void close();

    /**
     * Send the PT request.
     * 
     * @param operation
     *            the operation
     * @param cvNumber
     *            the CV number
     * @param cvValue
     *            the CV value
     */
    void sendRequest(PtOperation operation, int cvNumber, int cvValue);

    /**
     * Send the command station state request.
     * 
     * @param activateProgMode
     *            activate the PT programming mode
     */
    void sendCommandStationStateRequest(boolean activateProgMode);

    /**
     * Get the current state of the command station.
     * 
     * @return the current command station state
     */
    CommandStationState getCurrentCommandStationState();
}
