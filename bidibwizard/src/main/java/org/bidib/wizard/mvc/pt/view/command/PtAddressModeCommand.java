package org.bidib.wizard.mvc.pt.view.command;

import org.bidib.jbidibc.core.enumeration.AddressMode;
import org.bidib.jbidibc.core.enumeration.PtOperation;
import org.bidib.wizard.mvc.pt.view.panel.AddressProgBeanModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PtAddressModeCommand extends PtOperationIfElseCommand<AddressProgBeanModel> {
    private static final Logger LOGGER = LoggerFactory.getLogger(PtAddressModeCommand.class);

    private AddressMode addressMode;

    public PtAddressModeCommand(PtOperation ptOperation, int cvNumber, int cvValue) {
        super(ptOperation, cvNumber, cvValue);
    }

    /**
     * @param cvValueResult
     *            the cvValueResult to set
     */
    public void setCvValueResult(Integer cvValueResult) {
        super.setCvValueResult(cvValueResult);

        if (cvValueResult != null) {
            // set the address mode
            setAddressMode(AddressMode.valueOf(cvValueResult.intValue()));
        }
    }

    public void setAddressMode(AddressMode addressMode) {
        this.addressMode = addressMode;
    }

    public AddressMode getAddressMode() {
        return addressMode;
    }

    @Override
    public void postExecute(final AddressProgBeanModel addressProgBeanModel) {
        super.postExecute(addressProgBeanModel);

        // update the address mode
        if (addressMode != null) {
            LOGGER.debug("Set the addressMode: {}", addressMode);
            addressProgBeanModel.setAddressMode(addressMode);
        }
    }

    @Override
    public boolean isExpectedResult() {
        try {
            LOGGER.info("isExpectedResult, compare values, expected: {}, received: {}", getCvValue(),
                getCvValueResult());
            return (getCvValue() == getCvValueResult());
        }
        catch (Exception ex) {
            LOGGER.warn("Compare expected and result cv value failed.", ex);
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer(getClass().getSimpleName());
        sb.append(", ").append(super.toString());
        sb.append(",addressMode=").append(addressMode);
        return sb.toString();
    }
}
