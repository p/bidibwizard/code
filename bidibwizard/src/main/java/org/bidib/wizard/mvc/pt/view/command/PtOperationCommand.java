package org.bidib.wizard.mvc.pt.view.command;

import org.bidib.jbidibc.core.enumeration.CommandStationProgState;
import org.bidib.jbidibc.core.enumeration.PtOperation;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.wizard.mvc.pt.view.panel.ProgCommandAwareBeanModel;

public class PtOperationCommand<M extends ProgCommandAwareBeanModel> {

    private PtOperation ptOperation;

    private int cvNumber;

    private int cvValue;

    private Integer cvValueResult;

    private CommandStationProgState progStateResult;

    public PtOperationCommand(PtOperation ptOperation, int cvNumber, int cvValue) {
        this.ptOperation = ptOperation;
        this.cvNumber = cvNumber;
        this.cvValue = cvValue;
    }

    /**
     * @return the ptOperation
     */
    public PtOperation getPtOperation() {
        return ptOperation;
    }

    /**
     * @param ptOperation
     *            the ptOperation to set
     */
    public void setPtOperation(PtOperation ptOperation) {
        this.ptOperation = ptOperation;
    }

    /**
     * @return the cvNumber
     */
    public int getCvNumber() {
        return cvNumber;
    }

    /**
     * @param cvNumber
     *            the cvNumber to set
     */
    public void setCvNumber(int cvNumber) {
        this.cvNumber = cvNumber;
    }

    /**
     * @return the cvValue
     */
    public int getCvValue() {
        return cvValue;
    }

    /**
     * @param cvValue
     *            the cvValue to set
     */
    public void setCvValue(int cvValue) {
        this.cvValue = cvValue;
    }

    /**
     * @return the cvValueResult
     */
    public Integer getCvValueResult() {
        return cvValueResult;
    }

    /**
     * @param cvValueResult
     *            the cvValueResult to set
     */
    public void setCvValueResult(Integer cvValueResult) {
        this.cvValueResult = cvValueResult;
    }

    /**
     * @return the progStateResult
     */
    public CommandStationProgState getProgStateResult() {
        return progStateResult;
    }

    /**
     * @param progStateResult
     *            the progStateResult to set
     */
    public void setProgStateResult(CommandStationProgState progStateResult) {
        this.progStateResult = progStateResult;
    }

    public void postExecute(final M progComamndAwareBeanModel) {
        // do nothing
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("ptOperation=");
        sb.append(ptOperation).append(",cvNumber=").append(cvNumber).append(",cvValue=0x")
            .append(ByteUtils.byteToHex(cvValue));
        if (cvValueResult != null) {
            sb.append(",cvValueResult=").append(ByteUtils.byteToHex(cvValueResult));
        }
        if (progStateResult != null) {
            sb.append(",progStateResult=").append(progStateResult);
        }
        return sb.toString();
    }
}
