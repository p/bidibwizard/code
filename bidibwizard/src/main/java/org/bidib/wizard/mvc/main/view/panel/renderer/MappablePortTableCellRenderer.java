package org.bidib.wizard.mvc.main.view.panel.renderer;

import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import org.bidib.wizard.common.utils.ImageUtils;
import org.bidib.wizard.mvc.main.model.Port;

public class MappablePortTableCellRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = 1L;

    private final ImageIcon mappingEnabledIcon;

    public MappablePortTableCellRenderer() {

        mappingEnabledIcon = ImageUtils.createImageIcon(MappablePortTableCellRenderer.class, "/icons/arrow_switch.png");
    }

    public Component getTableCellRendererComponent(
        JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

        // call super to set the correct color if selected
        super.getTableCellRendererComponent(table, null, isSelected, hasFocus, row, column);

        // renderer only handles Ports
        if (value instanceof Port<?>) {
            Port<?> port = (Port<?>) value;
            setEnabled(port.isEnabled());
            setIcon(null);
            setText(port.getPortIdentifier());
            if (port.isRemappingEnabled()) {
                setIcon(mappingEnabledIcon);
            }
        }
        else {
            setEnabled(false);
            setText(null);
        }
        return this;
    }
}
