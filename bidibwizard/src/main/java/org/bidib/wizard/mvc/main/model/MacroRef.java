package org.bidib.wizard.mvc.main.model;

import java.io.Serializable;

public class MacroRef implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer id;

    /**
     * Creates a new instance of MacroRef. The MacroRef stores the id of a macro.
     */
    public MacroRef() {
        this.id = 0;
    }

    public MacroRef(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String toString() {
        return "MacroRef, id: " + id;
    }
}
