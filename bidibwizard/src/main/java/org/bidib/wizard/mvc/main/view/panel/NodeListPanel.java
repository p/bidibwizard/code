package org.bidib.wizard.mvc.main.view.panel;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Frame;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.commons.lang3.StringUtils;
import org.bidib.jbidibc.core.StringData;
import org.bidib.jbidibc.core.enumeration.IdentifyState;
import org.bidib.jbidibc.core.schema.bidib2.BiDiB;
import org.bidib.jbidibc.experimental.excel.ExcelExportFactory;
import org.bidib.wizard.comm.Communication;
import org.bidib.wizard.comm.CommunicationFactory;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.dialog.EscapeDialog;
import org.bidib.wizard.dialog.FileDialog;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.bidib.wizard.mvc.main.controller.MainController;
import org.bidib.wizard.mvc.main.model.Macro;
import org.bidib.wizard.mvc.main.model.MacroSaveState;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.model.listener.DefaultNodeListListener;
import org.bidib.wizard.mvc.main.view.component.BulkSwitchNodeOperationsDialog;
import org.bidib.wizard.mvc.main.view.component.BusyFrame;
import org.bidib.wizard.mvc.main.view.component.LabeledDisplayItems;
import org.bidib.wizard.mvc.main.view.component.NodeErrorsDialog;
import org.bidib.wizard.mvc.main.view.component.SaveNodeConfigurationDialog;
import org.bidib.wizard.mvc.main.view.menu.NodeListMenu;
import org.bidib.wizard.mvc.main.view.menu.listener.NodeListMenuListener;
import org.bidib.wizard.mvc.main.view.panel.listener.LabelChangedListener;
import org.bidib.wizard.mvc.main.view.panel.listener.NodeListActionListener;
import org.bidib.wizard.mvc.main.view.statusbar.StatusBar;
import org.bidib.wizard.mvc.script.view.listener.NodeTreeScriptingListener;
import org.bidib.wizard.utils.NodeUtils;
import org.oxbow.swingbits.dialog.task.TaskDialogs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.forms.builder.ButtonBarBuilder;
import com.jgoodies.forms.factories.Borders;
import com.vlsolutions.swing.docking.DockKey;
import com.vlsolutions.swing.docking.Dockable;

public class NodeListPanel extends LabelListPanel<Node>
    implements NodeListMenuListener, Dockable, NodeTreeScriptingListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(NodeListPanel.class);

    private final DockKey DOCKKEY = new DockKey("nodeListPanel");

    private final Collection<NodeListActionListener> nodeListListeners = new LinkedList<NodeListActionListener>();

    private final NodeListMenu nodeListMenu;

    private final JPopupMenu popupMenu;

    private JPanel contentPanel;

    private FileFilter bidibNodesFilter;

    private static final String BIDIBFILE_EXTENSION = "xlsx";

    private String bidibNodeFilesDescription;

    private static LabeledDisplayItems<Node> createNodeTree() {

        LabeledDisplayItems<Node> nodeTree = null;
        nodeTree = new NodeTree();

        return nodeTree;
    }

    public NodeListPanel(final MainModel model) {
        super(createNodeTree());

        // register the nodeTree in the context
        LOGGER.info("Register the nodeTree in applicationContext.");
        DefaultApplicationContext.getInstance().register(DefaultApplicationContext.KEY_NODE_TREE, getLabelList());

        nodeListMenu = new NodeListMenu(model);
        popupMenu = nodeListMenu.getPopupMenu();

        DOCKKEY.setName(Resources.getString(getClass(), "title"));
        // turn off autohide and close features
        DOCKKEY.setCloseEnabled(false);
        DOCKKEY.setAutoHideEnabled(false);

        getLabelList().addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                int index = getLabelList().getIndex(e.getPoint());

                if (index > -1) {
                    // get the selected node
                    Node node = getLabelList().getElementAt(index);
                    LOGGER.debug("Node was selected: {}", node);

                    if (node != null) {

                        if (e.getClickCount() == 1 && e.isPopupTrigger()) {
                            // update the menu items based on the features of the node
                            nodeListMenu.setAddressMessagesEnabled(node.isAddressMessagesEnabled());
                            nodeListMenu.setDccStartEnabled(node.isDccStartEnabled());
                            nodeListMenu.setExternalStartEnabled(node.isExternalStartEnabled());
                            nodeListMenu.setFeedbackMessagesEnabled(node.isFeedbackMessagesEnabled());
                            nodeListMenu.setKeyMessagesEnabled(node.isKeyMessagesEnabled());
                            nodeListMenu.setClearErrorEnabled(node.isNodeHasError());
                            handleMouseEvent(e, popupMenu);
                        }
                        else if (e.getClickCount() == 2) {
                            // select the node
                            LOGGER.info("Select the node: {}", node);
                            getLabelList().selectedValueChanged(index);
                        }
                    }
                }
            }

            public void mouseReleased(MouseEvent e) {
                LOGGER.debug("Mouse released.");
                if (e.isPopupTrigger()) {
                    mousePressed(e);
                }
            }
        });
        getLabelList().setItems(model.getNodes().toArray(new Node[0]));
        getLabelList().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        model.addNodeListListener(new DefaultNodeListListener() {
            @Override
            public void listChanged() {
                LOGGER.debug("List changed, use the AWT-thread to set the nodes in the label list.");
                if (SwingUtilities.isEventDispatchThread()) {
                    internalListChanged(model);
                }
                else {
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            internalListChanged(model);
                        }
                    });
                }
            }

            @Override
            public void nodeStateChanged() {
                LOGGER.debug("Node state changed, use the AWT-thread to refresh the labellist.");
                if (SwingUtilities.isEventDispatchThread()) {
                    getLabelList().refreshView();
                }
                else {
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            getLabelList().refreshView();
                        }
                    });
                }
            }
        });
        nodeListMenu.addMenuListener(this);
    }

    private JPanel createPanel() {
        LOGGER.info("Create the content panel.");

        JPanel contentPanel = new JPanel();
        contentPanel.setLayout(new BorderLayout());
        contentPanel.setBorder(Borders.DLU2);

        JScrollPane scrollPane = new JScrollPane();

        LabeledDisplayItems<Node> labelList = getLabelList();

        final NodeTree nodeTree = (NodeTree) labelList;
        scrollPane.setViewportView(nodeTree);

        addLabelChangedListener(new LabelChangedListener<Node>() {

            @Override
            public void labelChanged(Node node, String label) {
                LOGGER.debug("The label was changed on node: {}, new label: {}", node, label);
                nodeTree.labelChanged(node, label);
            }
        });

        contentPanel.add(scrollPane, BorderLayout.CENTER);

        return contentPanel;
    }

    @Override
    public Component getComponent() {
        if (contentPanel == null) {
            contentPanel = createPanel();
        }
        return contentPanel;
    }

    @Override
    public DockKey getDockKey() {
        return DOCKKEY;
    }

    private void internalListChanged(final MainModel model) {
        Node selectedValue = getLabelList().getSelectedItem();
        LOGGER.debug("The list has changed, current selected value: {}", selectedValue);

        // set the current items in the label list
        getLabelList().setItems(model.getNodes().toArray(new Node[0]));
        LOGGER.debug("The list has changed, set the new nodes has finished.");

        // TODO we should set the previous selected value but this does not work currently
        // if (selectedValue != null && model.getNodes().contains(selectedValue)) {
        // LOGGER.debug("Set the previous selected value: {}", selectedValue);
        // getLabelList().setSelectedValue(selectedValue, true);
        // }
    }

    @Override
    public void addressMessagesEnabled(boolean isSelected) {
        Node node = (Node) getLabelList().getSelectedItem();

        if (node != null) {
            node.setAddressMessagesEnabled(isSelected);
            for (NodeListActionListener l : nodeListListeners) {
                l.enableAddressMessages(node);
            }
        }
    }

    public void addNodeListListener(NodeListActionListener l) {
        addLabelChangedListener(l);
        nodeListListeners.add(l);
    }

    @Override
    public void dccStartEnabled(boolean isSelected) {
        Node node = (Node) getLabelList().getSelectedItem();

        if (node != null) {
            node.setDccStartEnabled(isSelected);
            for (NodeListActionListener l : nodeListListeners) {
                l.enableDccStart(node);
            }
        }
    }

    @Override
    public void importNode() {
        Node node = (Node) getLabelList().getSelectedItem();

        if (node != null) {
            for (NodeListActionListener l : nodeListListeners) {
                l.importNode(node);
            }
        }
    }

    @Override
    public void exportNode() {
        Node node = (Node) getLabelList().getSelectedItem();

        if (node != null) {
            for (NodeListActionListener l : nodeListListeners) {
                l.exportNode(node);
            }
        }
    }

    @Override
    public void externalStartEnabled(boolean isSelected) {
        Node node = (Node) getLabelList().getSelectedItem();

        if (node != null) {
            node.setExternalStartEnabled(isSelected);
            for (NodeListActionListener l : nodeListListeners) {
                l.enableExternalStart(node);
            }
        }
    }

    @Override
    public void feedbackMessagesEnabled(boolean isSelected) {
        Node node = (Node) getLabelList().getSelectedItem();

        if (node != null) {
            node.setFeedbackMessagesEnabled(isSelected);
            for (NodeListActionListener l : nodeListListeners) {
                l.enableFeedbackMessages(node);
            }
        }
    }

    @Override
    public void feedbackMirrorDisabled(boolean disable) {
        Node node = (Node) getLabelList().getSelectedItem();

        if (node != null) {
            node.setFeedbackMirrorDisabled(disable);
            for (NodeListActionListener l : nodeListListeners) {
                l.disableFeedbackMirror(node, disable);
            }
        }
    }

    @Override
    public void firmwareUpdate() {
        Point itemPosition = selectedIndexToLocation();

        for (NodeListActionListener l : nodeListListeners) {
            l.firmwareUpdate((Node) getLabelList().getSelectedItem(), itemPosition.x, itemPosition.y);
        }
    }

    @Override
    public void identify(boolean isSelected) {
        Node node = (Node) getLabelList().getSelectedItem();

        if (node != null) {
            node.setIdentifyState(isSelected ? IdentifyState.START : IdentifyState.STOP);
            for (NodeListActionListener l : nodeListListeners) {
                l.identify(node);
            }
        }
    }

    @Override
    public void keyMessagesEnabled(boolean isSelected) {
        Node node = (Node) getLabelList().getSelectedItem();

        if (node != null) {
            node.setKeyMessagesEnabled(isSelected);
            for (NodeListActionListener l : nodeListListeners) {
                l.enableKeyMessages(node);
            }
        }
    }

    @Override
    public void features() {
        Node node = (Node) getLabelList().getSelectedItem();
        LOGGER.debug("Open the features dialog on node: {}", node);
        if (node != null) {
            Point itemPosition = selectedIndexToLocation();

            for (NodeListActionListener l : nodeListListeners) {
                l.features(node, itemPosition.x, itemPosition.y);
            }
        }
    }

    @Override
    public void reset() {
        Node node = (Node) getLabelList().getSelectedItem();
        LOGGER.debug("Open the reset dialog on node: {}", node);
        if (node != null) {
            // show the reset dialog for the selected node
            Frame parent = JOptionPane.getFrameForComponent(contentPanel);
            new ResetDialog(parent, node, true);
        }
    }

    @Override
    public void ping() {
        Node node = (Node) getLabelList().getSelectedItem();
        LOGGER.info("Send ping to node: {}", node);

        if (node != null) {
            for (NodeListActionListener l : nodeListListeners) {
                l.ping(node, (byte) 0x01);
            }
        }
    }

    @Override
    public void readUniqueId() {
        Node node = (Node) getLabelList().getSelectedItem();
        LOGGER.info("Read the uniqueId from the node: {}", node);
        long uniqueId = 0;

        if (node != null) {
            for (NodeListActionListener l : nodeListListeners) {
                uniqueId = l.readUniqueId(node);
            }
        }

        StringBuilder message = new StringBuilder("The UniqueId from the current node: ");
        message.append(org.bidib.jbidibc.core.utils.NodeUtils.getUniqueIdAsString(uniqueId));
        Frame parent = JOptionPane.getFrameForComponent(contentPanel);
        JOptionPane.showMessageDialog(parent, message);
    }

    @Override
    public void dmxModeler() {
        Node node = (Node) getLabelList().getSelectedItem();
        LOGGER.debug("Open the dmxModeler on node: {}", node);

        if (node != null) {
            for (NodeListActionListener l : nodeListListeners) {
                l.dmxModeler(node);
            }
        }
    }

    @Override
    protected void fireLabelChanged(Node object, String label) {
        super.fireLabelChanged(object, label);
        getLabelList().refreshView();
    }

    private static final class ResetDialog extends EscapeDialog {
        private static final long serialVersionUID = 1L;

        public ResetDialog(final Frame parent, final Node node, boolean modal) {
            super(parent, Resources.getString(ResetDialog.class, "title"), modal);
            getContentPane().setLayout(new BorderLayout());

            JPanel mainPanel = new JPanel(new BorderLayout());

            mainPanel.add(new JLabel(Resources.getString(getClass(), "message"),
                UIManager.getIcon("OptionPane.warningIcon"), SwingConstants.LEADING), BorderLayout.CENTER);
            mainPanel.setBorder(new EmptyBorder(5, 5, 5, 5));

            // buttons
            JButton reset = new JButton(Resources.getString(getClass(), "reset"));

            reset.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    setVisible(false);
                    fireReset(node);
                }
            });

            JButton cancel = new JButton(Resources.getString(getClass(), "cancel"));

            cancel.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    setVisible(false);
                    fireCancel();
                }
            });

            JPanel buttons =
                new ButtonBarBuilder().addGlue().addButton(reset, cancel).border(Borders.BUTTON_BAR_PAD).build();

            mainPanel.add(buttons, BorderLayout.SOUTH);

            getContentPane().add(mainPanel);
            pack();

            setLocationRelativeTo(parent);
            setMinimumSize(getSize());
            setVisible(true);
        }

        private void fireReset(Node node) {
            LOGGER.info("Reset the current node: {}", node);
            CommunicationFactory.getInstance().reset(node.getNode());
        }

        private void fireCancel() {

        }
    }

    @Override
    public void loco() {
        Point itemPosition = selectedIndexToLocation();
        Node node = (Node) getLabelList().getSelectedItem();

        if (node != null && org.bidib.jbidibc.core.utils.NodeUtils.hasCommandStationFunctions(node.getUniqueId())) {
            LOGGER.info("Open the loco dialog.");
            for (NodeListActionListener l : nodeListListeners) {
                l.loco(node, itemPosition.x, itemPosition.y);
            }
        }
    }

    @Override
    public void locoList() {
        Node node = (Node) getLabelList().getSelectedItem();

        if (node != null && org.bidib.jbidibc.core.utils.NodeUtils.hasCommandStationFunctions(node.getUniqueId())) {
            LOGGER.info("Open the loco table.");
            for (NodeListActionListener l : nodeListListeners) {
                l.locoList(node);
            }
        }
    }

    @Override
    public void dccAccessory() {
        Point itemPosition = selectedIndexToLocation();
        Node node = (Node) getLabelList().getSelectedItem();

        if (node != null) {
            for (NodeListActionListener l : nodeListListeners) {
                l.dccAccessory(node, itemPosition.x, itemPosition.y);
            }
        }
    }

    @Override
    public void locoCv() {
        Point itemPosition = selectedIndexToLocation();
        Node node = (Node) getLabelList().getSelectedItem();

        if (node != null) {
            for (NodeListActionListener l : nodeListListeners) {
                l.locoCv(node, itemPosition.x, itemPosition.y);
            }
        }
    }

    @Override
    public void locoCvPt() {
        Point itemPosition = selectedIndexToLocation();
        Node node = (Node) getLabelList().getSelectedItem();

        if (node != null) {
            for (NodeListActionListener l : nodeListListeners) {
                l.locoCvPt(node, itemPosition.x, itemPosition.y);
            }
        }
    }

    @Override
    public void saveNode() {
        Node node = (Node) getLabelList().getSelectedItem();

        if (node != null) {
            BusyFrame busyFrame = null;
            try {
                Frame frame = JOptionPane.getFrameForComponent(contentPanel);
                if (frame instanceof BusyFrame) {
                    busyFrame = (BusyFrame) frame;
                    busyFrame.setBusy(true);
                }

                final Map<String, Object> params = new HashMap<String, Object>();

                // show a dialog with some checkboxes to allow the user to select all options (port config, macros,
                // accessories) and move this to saveNode ...
                SaveNodeConfigurationDialog saveNodeConfigurationDialog =
                    new SaveNodeConfigurationDialog(frame, null, null, node, params, true) {
                        private static final long serialVersionUID = 1L;

                        @Override
                        protected void fireContinue(Node node) {
                            LOGGER.info("User wants to save the configuration to node.");

                            params.put(SAVE_MACROS, isSaveMacros());
                            params.put(SAVE_ACCESSORIES, isSaveAccessories());
                            params.put(SAVE_BACKLIGHTPORTS, isSaveBacklightPorts());
                            params.put(SAVE_LIGHTPORTS, isSaveLightPorts());
                            params.put(SAVE_SERVOPORTS, isSaveServoPorts());
                            params.put(SAVE_SWITCHPORTS, isSaveSwitchPorts());

                            params.put(SAVE_FEATURES, isSaveFeatures());
                            params.put(SAVE_CVS, isSaveConfigurationVariables());

                            for (NodeListActionListener l : nodeListListeners) {
                                l.saveNode(node, params);
                            }

                            // check if errors were detected
                            if (params.containsKey(SAVE_ERRORS)) {
                                LOGGER.warn("The save on node operation has finished with errors!!!");
                                // show an error dialog with the list of save errors during update node
                                NodeErrorsDialog nodeErrorsDialog =
                                    new NodeErrorsDialog(JOptionPane.getFrameForComponent(this), null, true);

                                // set the error information
                                nodeErrorsDialog.setErrors(
                                    Resources.getString(MainController.class, "save-values-on-node-failed"),
                                    (List<String>) params.get(SAVE_ERRORS));
                                nodeErrorsDialog.showDialog();
                            }
                        }

                        @Override
                        protected void fireCancel(Node node) {
                        }
                    };
                saveNodeConfigurationDialog.showDialog();
            }
            finally {
                if (busyFrame != null) {
                    busyFrame.setBusy(false);
                }
            }
        }
    }

    @Override
    public void showDetails() {
        Point itemPosition = selectedIndexToLocation();

        for (NodeListActionListener l : nodeListListeners) {
            l.nodeDetails((Node) getLabelList().getSelectedItem(), itemPosition.x, itemPosition.y);
        }
    }

    @Override
    public void bulkSwitchNode() {
        Node node = (Node) getLabelList().getSelectedItem();
        LOGGER.debug("Open the bulk switch node dialog on node: {}", node);
        if (node != null) {
            Point itemPosition = selectedIndexToLocation();

            // show the reset dialog for the selected node
            BulkSwitchNodeOperationsDialog dialog = new BulkSwitchNodeOperationsDialog(node, true);
            dialog.showDialog(itemPosition);
        }
    }

    @Override
    public void clearErrors() {
        Node node = (Node) getLabelList().getSelectedItem();
        LOGGER.debug("Clear errors on node: {}", node);
        if (node != null) {
            node.setNodeHasError(false);
        }
    }

    @Override
    public void setSelectedNode(Node node) {
        LOGGER.info("Set the selected node: {}", node);

        getLabelList().setSelectedItem(node);
    }

    @Override
    public Node getSelectedItem() {
        return getLabelList().getSelectedItem();
    }

    @Override
    public void generateDocumentation() {

        LOGGER.info("Generate documentation.");
        try {
            Node node = getSelectedItem();
            if (node != null) {

                // get the main model
                MainModel mainModel =
                    DefaultApplicationContext.getInstance().get(DefaultApplicationContext.KEY_PORTS_PROVIDER,
                        MainModel.class);
                LOGGER.info("Fetched mainModel from context: {}", mainModel);

                bidibNodeFilesDescription = Resources.getString(getClass(), "bidibNodeFilesDescription");
                bidibNodesFilter = new FileNameExtensionFilter(bidibNodeFilesDescription, BIDIBFILE_EXTENSION);

                String defaultFileName = node.toString() + "." + BIDIBFILE_EXTENSION;
                if (StringUtils.isNotBlank(node.getNode().getStoredString(StringData.INDEX_USERNAME))) {
                    defaultFileName =
                        node.getNode().getStoredString(StringData.INDEX_USERNAME) + "." + BIDIBFILE_EXTENSION;
                }

                final FileDialog dialog =
                    new FileDialog(contentPanel, FileDialog.SAVE, defaultFileName, bidibNodesFilter) {
                        @Override
                        public void approve(String selectedFile) {
                            File file = new File(selectedFile);

                            String fileName = file.getPath();

                            BusyFrame busyFrame =
                                DefaultApplicationContext.getInstance().get(DefaultApplicationContext.KEY_MAIN_FRAME,
                                    BusyFrame.class);

                            try {
                                busyFrame.setBusy(true);

                                // collect information of the node
                                // TODO fetch all macros content from the node
                                LOGGER.info("Load the macro content of the node before export: {}", node);

                                boolean nodeHasFlatPortModel = node.getNode().isPortFlatModelAvailable();
                                LOGGER.info("Prepare the nodeState, nodeHasFlatPortModel: {}", nodeHasFlatPortModel);

                                List<Macro> macroList = new LinkedList<Macro>();
                                Communication communication = CommunicationFactory.getInstance();
                                for (Macro macro : mainModel.getMacros()) {
                                    Macro macroWithContent = communication.getMacroContent(node, macro);
                                    LOGGER.info("Load macro content: {}", macroWithContent);

                                    // reset the changed flag on the macro
                                    macroWithContent.setMacroSaveState(MacroSaveState.PERMANENTLY_STORED_ON_NODE);
                                    macroWithContent.setFlatPortModel(nodeHasFlatPortModel);

                                    macroList.add(macroWithContent);
                                }
                                LOGGER.info("Set the new macros for the node: {}", macroList);
                                mainModel.setMacros(macroList);

                                final BiDiB bidib = NodeUtils.convertToBiDiB(node, mainModel);
                                LOGGER.info("Converted bidib: {}", bidib);

                                ExcelExportFactory.exportDocumentation(bidib, fileName);

                                StatusBar statusBar =
                                    DefaultApplicationContext
                                        .getInstance().get(DefaultApplicationContext.KEY_STATUS_BAR, StatusBar.class);
                                statusBar.setStatus(
                                    Resources.getString(NodeListPanel.class, "export-documentation-passed", fileName),
                                    StatusBar.DISPLAY_NORMAL);
                            }
                            catch (Exception ex) {
                                LOGGER.warn("Generate documentation failed.", ex);

                                TaskDialogs
                                    .build(JOptionPane.getFrameForComponent(contentPanel),
                                        Resources.getString(NodeListPanel.class,
                                            "export-documentation-failed.instruction"),
                                        Resources.getString(NodeListPanel.class, "export-documentation-failed"))
                                    .title(Resources.getString(NodeListPanel.class, "export-documentation.title"))
                                    .exception(ex);
                            }
                            finally {
                                busyFrame.setBusy(false);
                            }
                        }
                    };
                dialog.showDialog();

            }
        }
        catch (Exception ex) {
            LOGGER.warn("Generate documentation failed.", ex);
        }
    }
}
