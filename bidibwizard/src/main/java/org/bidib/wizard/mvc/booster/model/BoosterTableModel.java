package org.bidib.wizard.mvc.booster.model;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.bidib.jbidibc.core.enumeration.BoosterState;
import org.bidib.jbidibc.core.enumeration.CommandStationState;
import org.bidib.wizard.mvc.main.model.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.beans.Model;
import com.jgoodies.common.collect.ArrayListModel;

public class BoosterTableModel extends Model {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(BoosterTableModel.class);

    public static final String PROPERTY_BOOSTERS = "boosters";

    private ArrayListModel<BoosterModel> boosterList = new ArrayListModel<>();

    private final PropertyChangeListener boosterListener;

    public BoosterTableModel() {

        // create the propertyChangeListener to refresh the booster list
        boosterListener = new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (BoosterModel.PROPERTY_BOOSTER_LABEL.equals(evt.getPropertyName())) {
                    LOGGER.debug("The booster label has been changed.");
                    BoosterModel booster = (BoosterModel) evt.getSource();
                    int index = boosterList.indexOf(booster);
                    boosterList.fireContentsChanged(index);
                }
            }
        };

    }

    public void addBooster(Node node) {
        synchronized (boosterList) {
            BoosterModel booster = new BoosterModel(node, boosterListener);
            if (!boosterList.contains(booster)) {
                LOGGER.info("Add booster to booster list: {}", node);
                booster.registerNode();

                String nodeLabel = booster.prepareNodeLabel();
                booster.setNodeLabel(nodeLabel);

                List<BoosterModel> oldValue = new LinkedList<>(boosterList);
                boosterList.add(booster);

                firePropertyChange(PROPERTY_BOOSTERS, oldValue, boosterList);
            }
            else {
                LOGGER.warn("Node is already in booster list: {}", node);
            }
        }
    }

    public void removeBooster(Node node) {
        synchronized (boosterList) {
            LOGGER.info("Remove booster from booster list: {}", node);

            List<BoosterModel> oldValue = new LinkedList<>(boosterList);
            int index = boosterList.indexOf(new BoosterModel(node, null));
            if (index > -1) {
                BoosterModel removed = boosterList.remove(index);
                if (removed != null) {
                    removed.freeNode();
                }

                firePropertyChange(PROPERTY_BOOSTERS, oldValue, boosterList);
            }
        }
    }

    public ArrayListModel<BoosterModel> getBoosterListModel() {
        return boosterList;
    }

    public List<BoosterModel> getBoosters() {
        return Collections.unmodifiableList(boosterList);
    }

    public void setBoosterState(byte[] address, BoosterState state) {
        synchronized (boosterList) {
            for (BoosterModel booster : boosterList) {
                if (Arrays.equals(booster.getNodeAddress(), address)) {
                    LOGGER.trace("Found booster to update: {}", booster);
                    booster.setState(state);

                    int index = boosterList.indexOf(booster);
                    boosterList.fireContentsChanged(index);
                    break;
                }
            }
        }
    }

    public void setCommandStationState(byte[] address, CommandStationState state) {
        synchronized (boosterList) {
            for (BoosterModel booster : boosterList) {
                if (Arrays.equals(booster.getNodeAddress(), address)) {
                    LOGGER.trace("Found command station to update: {}", booster);
                    booster.setCommandStationState(state);

                    int index = boosterList.indexOf(booster);
                    boosterList.fireContentsChanged(index);
                    break;
                }
            }
        }
    }

    public void setBoosterCurrent(byte[] address, int current) {
        synchronized (boosterList) {
            for (BoosterModel booster : boosterList) {
                if (Arrays.equals(booster.getNodeAddress(), address)) {
                    LOGGER.trace("Found booster to update: {}", booster);
                    booster.setCurrent(current);

                    int index = boosterList.indexOf(booster);
                    boosterList.fireContentsChanged(index);
                    break;
                }
            }
        }
    }

    public void setBoosterMaxCurrent(byte[] address, int maxCurrent) {
        synchronized (boosterList) {
            for (BoosterModel booster : boosterList) {
                if (Arrays.equals(booster.getNodeAddress(), address)) {
                    LOGGER.trace("Found booster to update: {}", booster);
                    booster.setMaxCurrent(maxCurrent);

                    int index = boosterList.indexOf(booster);
                    boosterList.fireContentsChanged(index);
                    break;
                }
            }
        }
    }

    public void setBoosterVoltage(byte[] address, int voltage) {
        synchronized (boosterList) {
            for (BoosterModel booster : boosterList) {
                if (Arrays.equals(booster.getNodeAddress(), address)) {
                    LOGGER.trace("Found booster to update: {}", booster);
                    booster.setVoltage(voltage);

                    int index = boosterList.indexOf(booster);
                    boosterList.fireContentsChanged(index);
                    break;
                }
            }
        }
    }

    public void setBoosterTemperature(byte[] address, int temperature) {
        synchronized (boosterList) {
            for (BoosterModel booster : boosterList) {
                if (Arrays.equals(booster.getNodeAddress(), address)) {
                    LOGGER.trace("Found booster to update: {}", booster);
                    booster.setTemperature(temperature);

                    int index = boosterList.indexOf(booster);
                    boosterList.fireContentsChanged(index);
                    break;
                }
            }
        }
    }
}
