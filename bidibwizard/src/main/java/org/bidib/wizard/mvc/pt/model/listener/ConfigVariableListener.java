package org.bidib.wizard.mvc.pt.model.listener;

import org.bidib.jbidibc.core.enumeration.CommandStationProgState;
import org.bidib.jbidibc.core.enumeration.CommandStationState;

public interface ConfigVariableListener {
    /**
     * The state of the command station has changed.
     * 
     * @param commandStationState
     *            the new command station state
     */
    void commandStationStateChanged(CommandStationState commandStationState);

    /**
     * The programming state of the command station has changed.
     * 
     * @param commandStationProgState
     *            the new programming state
     */
    void commandStationProgStateChanged(CommandStationProgState commandStationProgState);
}
