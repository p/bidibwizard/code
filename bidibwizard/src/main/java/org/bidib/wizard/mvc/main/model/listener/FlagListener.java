package org.bidib.wizard.mvc.main.model.listener;

public interface FlagListener {

    /**
     * The label of the flag was changed.
     * 
     * @param flagId
     *            the flag number
     */
    void labelChanged(int flagId);
}
