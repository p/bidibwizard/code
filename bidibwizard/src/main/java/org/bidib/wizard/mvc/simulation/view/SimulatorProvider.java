package org.bidib.wizard.mvc.simulation.view;

import org.bidib.jbidibc.simulation.SimulatorNode;
import org.bidib.wizard.mvc.main.model.Node;

public interface SimulatorProvider {

    SimulatorNode getSimulator(Node node);

    void restoreSimulationView();
}
