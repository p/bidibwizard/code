package org.bidib.wizard.mvc.pt.view.command;

import org.bidib.jbidibc.core.enumeration.PtOperation;
import org.bidib.wizard.mvc.pt.view.panel.DirectAccessProgBeanModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PtDirectAccessCommand extends PtOperationCommand<DirectAccessProgBeanModel> {
    private static final Logger LOGGER = LoggerFactory.getLogger(PtDirectAccessCommand.class);

    public PtDirectAccessCommand(PtOperation ptOperation, int cvNumber, int cvValue) {
        super(ptOperation, cvNumber, cvValue);
    }

    @Override
    public void postExecute(final DirectAccessProgBeanModel directAccessProgBeanModel) {
        super.postExecute(directAccessProgBeanModel);

        // update the railcom config
        if (getCvValueResult() != null) {
            LOGGER.debug("Set the cvValueResult: {}", getCvValueResult());
            directAccessProgBeanModel.setCvValue(getCvValueResult());
        }
    }
}
