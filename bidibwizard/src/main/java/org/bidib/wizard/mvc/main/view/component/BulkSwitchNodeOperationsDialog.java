package org.bidib.wizard.mvc.main.view.component;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.commons.lang.StringUtils;
import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.comm.CommunicationFactory;
import org.bidib.wizard.comm.SwitchPortStatus;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.dialog.EscapeDialog;
import org.bidib.wizard.dialog.FileDialog;
import org.bidib.wizard.mvc.loco.view.ScriptPanel;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.script.ScriptCommand;
import org.bidib.wizard.script.ScriptEngineListener;
import org.bidib.wizard.script.engine.ScriptEngine;
import org.bidib.wizard.script.engine.ScriptEngine.ScriptStatus;
import org.bidib.wizard.script.switching.SwitchFunctionsScriptCommandFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.value.ValueHolder;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.builder.ButtonBarBuilder;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.FormLayout;

public class BulkSwitchNodeOperationsDialog extends EscapeDialog
    implements BulkSwitchFunctionsScripting, ScriptEngineListener<BulkSwitchFunctionsScripting> {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(BulkSwitchNodeOperationsDialog.class);

    // description, suffix for script files
    private static String scriptDescription;

    private static final String SCRIPT_EXTENSION = "bidibt";

    private static FileFilter scriptFilter;

    private ValueModel selectedScriptModel = new ValueHolder();

    private ValueModel currentCommandModel = new ValueHolder();

    private JButton startScript;

    private JButton stopScript;

    private ValueModel checkRepeatingModel = new ValueHolder();

    private AtomicBoolean scriptRepeating = new AtomicBoolean(false);

    // private final JTextArea switchCommands;

    private final Node node;

    private ScriptEngine<BulkSwitchFunctionsScripting> scriptEngine;

    public BulkSwitchNodeOperationsDialog(final Node node, boolean modal) {
        super(null, Resources.getString(BulkSwitchNodeOperationsDialog.class, "title"), modal);
        this.node = node;

        scriptDescription = Resources.getString(ScriptPanel.class, "scriptDescription");
        scriptFilter = new FileNameExtensionFilter(scriptDescription, SCRIPT_EXTENSION);

        DefaultFormBuilder formBuilder = new DefaultFormBuilder(new FormLayout("60dlu, 3dlu, 60dlu, 3dlu, 0dlu:grow"));

        JButton selectScript = new JButton(Resources.getString(ScriptPanel.class, "selectScript"));
        selectScript.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                // select the script file
                FileDialog dialog =
                    new FileDialog(BulkSwitchNodeOperationsDialog.this, FileDialog.OPEN, "*." + SCRIPT_EXTENSION,
                        scriptFilter) {

                        @Override
                        public void approve(String fileName) {
                            LOGGER.info("Load script: {}", fileName);
                            try {
                                File file = new File(fileName);
                                if (file.exists()) {
                                    LOGGER.info("The script file exists: {}", file);
                                    selectedScriptModel.setValue(file.getName());

                                    prepareScript(fileName);

                                    startScript.setEnabled(true);
                                }
                                else {
                                    selectedScriptModel.setValue("no script selected");
                                    startScript.setEnabled(false);
                                }
                            }
                            catch (IOException ex) {
                                LOGGER.info("Load and process script file failed.", ex);
                                startScript.setEnabled(false);
                            }
                        }
                    };
                dialog.showDialog();
            }
        });
        formBuilder.append(selectScript);

        JLabel scriptLabel = BasicComponentFactory.createLabel(selectedScriptModel);
        formBuilder.append(scriptLabel, 3);

        JCheckBox repeatingCheck =
            BasicComponentFactory.createCheckBox(checkRepeatingModel,
                Resources.getString(ScriptPanel.class, "repeating"));
        formBuilder.nextLine();
        formBuilder.append(repeatingCheck, 3);

        formBuilder.nextLine();

        startScript = new JButton(Resources.getString(ScriptPanel.class, "startScript"));
        startScript.setEnabled(false);
        startScript.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                scriptEngine.startScript();
            }
        });
        formBuilder.append(startScript);

        stopScript = new JButton(Resources.getString(ScriptPanel.class, "stopScript"));
        stopScript.setEnabled(false);
        stopScript.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                stopScript();
            }
        });
        formBuilder.append(stopScript);

        JLabel currentCommandLabel = BasicComponentFactory.createLabel(currentCommandModel);
        formBuilder.append(currentCommandLabel);

        checkRepeatingModel.addValueChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                LOGGER.info("Repeating has changed: {}", checkRepeatingModel.getValue());
                Boolean repeating = (Boolean) checkRepeatingModel.getValue();

                scriptRepeating.set(repeating);
                scriptEngine.setScriptRepeating(repeating);
            }
        });

        JPanel mainPanel = formBuilder.border(Borders.DIALOG).build();

        JButton cancelButton = new JButton(Resources.getString(BulkSwitchNodeOperationsDialog.class, "cancel"));
        cancelButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                fireCancel(node);
            }
        });

        JPanel buttonBar = new ButtonBarBuilder().addButton(cancelButton).build();
        JPanel southPanel = new JPanel(new BorderLayout());
        southPanel.add(buttonBar, BorderLayout.EAST);
        southPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().setLayout(new BorderLayout());
        getContentPane().add(mainPanel, BorderLayout.CENTER);
        getContentPane().add(southPanel, BorderLayout.SOUTH);

        getContentPane().setPreferredSize(new Dimension(300, 400));

        // create the script engine
        scriptEngine = new ScriptEngine<BulkSwitchFunctionsScripting>(this, new HashMap<String, Object>());
        scriptEngine.addScriptEngineListener(this);

    }

    public void showDialog(Point itemPosition) {
        pack();
        setLocation(itemPosition);
        setMinimumSize(getSize());
        setVisible(true);
    }

    // private void firePerform(final Node node) {
    // // SPORT 1 ON
    // // SPORT 1 OFF
    // String totalText = switchCommands.getText().trim();
    // LOGGER.info("Total text: {}", totalText);
    //
    // List<ScriptCommand<BulkSwitchFunctionsScripting>> scriptCommands = prepareScript(totalText);
    //
    // if (scriptCommands != null) {
    // scriptEngine.setScriptCommands(scriptCommands);
    //
    // scriptEngine.startScript();
    // }
    //
    // LOGGER.info("Execute commands finished.");
    // }

    private void fireCancel(final Node node) {
        LOGGER.info("Stop the script on press cancel button.");
        stopScript();
    }

    private void stopScript() {
        LOGGER.info("Stop the script.");
        scriptEngine.stopScript();
    }

    @Override
    public void scriptStatusChanged(final ScriptStatus scriptStatus) {
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                switch (scriptStatus) {
                    case RUNNING:
                        startScript.setEnabled(false);
                        stopScript.setEnabled(true);
                        break;
                    case STOPPED:
                    case FINISHED:
                        startScript.setEnabled(true);
                        stopScript.setEnabled(false);
                        break;
                }
                currentCommandChanged(null);
            }
        });
    }

    private final static Charset ENCODING = StandardCharsets.UTF_8;

    private void prepareScript(String fileName) throws IOException {
        Path fFilePath = Paths.get(fileName);

        SwitchFunctionsScriptCommandFactory<BulkSwitchFunctionsScripting> factory =
            new SwitchFunctionsScriptCommandFactory<BulkSwitchFunctionsScripting>();
        List<ScriptCommand<BulkSwitchFunctionsScripting>> scriptCommands =
            new LinkedList<ScriptCommand<BulkSwitchFunctionsScripting>>();

        try (Scanner scanner = new Scanner(fFilePath, ENCODING.name())) {
            while (scanner.hasNextLine()) {
                processLine(scanner.nextLine().trim(), factory, scriptCommands);
            }
        }

        LOGGER.info("Prepared list of commands: {}", scriptCommands);

        scriptEngine.setScriptCommands(scriptCommands);
    }

    // private List<ScriptCommand<BulkSwitchFunctionsScripting>> prepareScript(String scriptContent) {
    //
    // SwitchFunctionsScriptCommandFactory<BulkSwitchFunctionsScripting> factory = new
    // SwitchFunctionsScriptCommandFactory<BulkSwitchFunctionsScripting>();
    // List<ScriptCommand<BulkSwitchFunctionsScripting>> scriptCommands = new
    // LinkedList<ScriptCommand<BulkSwitchFunctionsScripting>>();
    //
    // // try (Scanner scanner = new Scanner(fFilePath, ENCODING.name())){
    // try (Scanner scanner = new Scanner(scriptContent)){
    // while (scanner.hasNextLine()){
    // processLine(scanner.nextLine().trim(), factory, scriptCommands);
    // }
    // }
    //
    // LOGGER.info("Prepared list of commands: {}", scriptCommands);
    //
    // // scriptEngine.setScriptCommands(scriptCommands);
    //
    // return scriptCommands;
    // }

    private void processLine(
        String line, SwitchFunctionsScriptCommandFactory<BulkSwitchFunctionsScripting> factory,
        List<ScriptCommand<BulkSwitchFunctionsScripting>> scriptCommands) {
        LOGGER.info("Process line: {}", line);

        if (line.startsWith("#") || StringUtils.isBlank(line)) {
            LOGGER.info("Skip comment or empty line.");
        }
        else {
            LOGGER.info("Current line: {}", line);
            ScriptCommand<BulkSwitchFunctionsScripting> command = factory.parse(line);
            if (command != null) {
                scriptCommands.add(command);
            }
        }
    }

    @Override
    public void sendPortStatusAction(int port, BidibStatus status) {
        SwitchPortStatus switchPortStatus = (SwitchPortStatus) status;
        CommunicationFactory.getInstance().activateSwitchPort(node.getNode(), port, switchPortStatus);
    }

    @Override
    public void sendPortValueAction(int port, int portValue) {
        CommunicationFactory.getInstance().activateServoPort(node.getNode(), port, portValue);
    }

    @Override
    public void currentCommandChanged(final ScriptCommand<BulkSwitchFunctionsScripting> command) {

        if (SwingUtilities.isEventDispatchThread()) {
            currentCommandModel.setValue((command != null ? command.toString() : null));
        }
        else {
            try {
                SwingUtilities.invokeAndWait(new Runnable() {

                    @Override
                    public void run() {
                        currentCommandModel.setValue((command != null ? command.toString() : null));
                    }
                });
            }
            catch (InvocationTargetException | InterruptedException e) {
                LOGGER.warn("Update current command failed.", e);
            }
        }
    }
}
