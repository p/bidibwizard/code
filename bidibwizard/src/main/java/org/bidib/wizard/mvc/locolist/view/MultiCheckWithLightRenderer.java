package org.bidib.wizard.mvc.locolist.view;

import java.awt.Graphics2D;

import javax.swing.ImageIcon;

import org.bidib.wizard.common.utils.ImageUtils;
import org.bidib.wizard.mvc.booster.view.BoosterStateCellRenderer;

public class MultiCheckWithLightRenderer extends AbstractMaskedIconRenderer {

    private static final long serialVersionUID = 1L;

    protected final ImageIcon off;

    protected final ImageIcon on;

    public MultiCheckWithLightRenderer(int mask) {
        super(mask);

        off = ImageUtils.createImageIcon(BoosterStateCellRenderer.class, "/icons/locolist/check_unselected.png");
        on = ImageUtils.createImageIcon(BoosterStateCellRenderer.class, "/icons/locolist/check_selected.png");
    }

    @Override
    protected void renderImages(Graphics2D g2) {

        // iterate over the bits of the mask
        int index = 0;
        for (int i = 0; i < 8; i++) {
            if ((mask >> i & 0x01) == 0x00) {
                // skip unused bits
                continue;
            }
            g2.drawImage(((value >> i) & 0x01) == 0x00 ? off.getImage() : on.getImage(), 2 + (index * 18), 2, this);
            index++;
        }

    }

}
