package org.bidib.wizard.mvc.main.view.panel;

import org.bidib.wizard.script.node.types.TargetType;

public interface ChangeLabelSupport {

    void changeLabel(TargetType portType);
}
