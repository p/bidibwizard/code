package org.bidib.wizard.mvc.main.model;

public enum AccessorySaveState {
    PENDING_CHANGES, PERMANENTLY_STORED_ON_NODE;
}
