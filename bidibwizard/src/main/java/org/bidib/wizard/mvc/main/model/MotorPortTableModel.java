package org.bidib.wizard.mvc.main.model;

import java.util.Collection;
import java.util.LinkedList;

import org.bidib.wizard.comm.MotorPortStatus;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.main.model.listener.MotorPortListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MotorPortTableModel
    extends SimplePortTableModel<MotorPortStatus, MotorPort, MotorPortListener<MotorPortStatus>> {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(MotorPortTableModel.class);

    private final Collection<MotorPortListener<MotorPortStatus>> motorPortListeners = new LinkedList<>();

    private final MainModel mainModel;

    public static final int COLUMN_LABEL = 0;

    public static final int COLUMN_VALUE = 1;

    public static final int COLUMN_PORT_INSTANCE = 2;

    private static final String[] COLUMN_NAMES =
        { Resources.getString(MotorPortTableModel.class, "label"),
            Resources.getString(MotorPortTableModel.class, "value"), null };

    public MotorPortTableModel(final MainModel model) {
        this.mainModel = model;
        setColumnIdentifiers(COLUMN_NAMES);
    }

    @Override
    protected int getColumnPortInstance() {
        return COLUMN_PORT_INSTANCE;
    }

    public MainModel getMainModel() {
        return mainModel;
    }

    public void addMotorPortListener(MotorPortListener<MotorPortStatus> l) {
        motorPortListeners.add(l);
    }

    public void addRow(MotorPort motorPort) {
        if (motorPort != null) {
            Object[] rowData = new Object[COLUMN_NAMES.length];

            rowData[COLUMN_LABEL] = motorPort.toString();
            rowData[COLUMN_VALUE] = motorPort.getValue();
            rowData[COLUMN_PORT_INSTANCE] = motorPort;
            addRow(rowData);
        }
        else {
            LOGGER.warn("Performed addRow without servo port instance.");
        }
    }

    private void fireLabelChanged(MotorPort motorPort, String label) {
        for (MotorPortListener<MotorPortStatus> l : motorPortListeners) {
            l.labelChanged(motorPort, label);
        }
    }

    private void fireConfigValuesChanged(MotorPort motorPort) {
        for (MotorPortListener<MotorPortStatus> l : motorPortListeners) {
            l.valuesChanged(motorPort);
        }
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        boolean isEditable = false;
        MotorPort motorPort = (MotorPort) getValueAt(row, COLUMN_PORT_INSTANCE);
        switch (column) {
            default:
                if (motorPort.isEnabled()) {
                    isEditable = true;
                }
                break;
        }
        return isEditable;
    }

    @Override
    public Class<?> getColumnClass(int column) {
        switch (column) {
            case COLUMN_LABEL:
                return String.class;
            default:
                return Integer.class;
        }
    }

    @Override
    public Object getValueAt(int row, int column) {
        Object result = null;
        switch (column) {
            case COLUMN_LABEL:
                column = COLUMN_PORT_INSTANCE;
                break;
            case COLUMN_VALUE:
                MotorPort port = (MotorPort) super.getValueAt(row, COLUMN_PORT_INSTANCE);

                if (port != null) {
                    // return the value for the target position
                    result = port.getValue();
                    LOGGER.info("Get the port value: {}, row: {}", result, row);
                }
                break;
            default:
                column = COLUMN_PORT_INSTANCE;
                break;
        }
        if (result == null) {
            result = super.getValueAt(row, column);
        }
        return result;
    }

    @Override
    public void setValueAt(Object value, int row, int column) {
        final MotorPort port = (MotorPort) getValueAt(row, COLUMN_PORT_INSTANCE);

        switch (column) {
            case COLUMN_LABEL:
                port.setLabel((String) value);
                super.setValueAt(port.toString(), row, column);
                fireLabelChanged(port, port.getLabel());
                break;
            case COLUMN_VALUE:
                // set the value for the target position
                int intValue = ((Integer) value);
                LOGGER.info("Set the value, new value: {}, port.value: {}", value, port.getValue());
                if (port.getValue() != intValue) {
                    port.setValue(intValue);
                    super.setValueAt(intValue, row, column);
                }
                else {
                    LOGGER.info("Ignore set the new value");
                }
                break;
        }
    }
}
