package org.bidib.wizard.mvc.main.view.panel;

import java.awt.BorderLayout;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.main.model.Port;
import org.bidib.wizard.mvc.main.model.SimplePortTableModel;
import org.bidib.wizard.mvc.main.model.listener.PortListListener;
import org.bidib.wizard.mvc.main.model.listener.PortListener;
import org.bidib.wizard.mvc.main.view.table.PortTable;
import org.bidib.wizard.script.node.types.TargetType;
import org.bidib.wizard.utils.PortListUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class SimplePortListPanel<S extends BidibStatus, P extends Port<S>, L extends PortListener<S>>
    extends JPanel implements PortListListener, ChangeLabelSupport {
    private static final long serialVersionUID = 1L;

    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());

    protected SimplePortTableModel<S, P, L> tableModel;

    protected List<P> ports;

    protected PortTable table;

    protected L portListener;

    public SimplePortListPanel(final SimplePortTableModel<S, P, L> tableModel, List<P> ports, String emptyTableText) {
        this.tableModel = tableModel;

        this.ports = ports;
        setLayout(new BorderLayout());

        setBorder(new EmptyBorder(5, 5, 5, 5));

        createTable(tableModel, emptyTableText);

        JScrollPane scrollPane = new JScrollPane(table);
        scrollPane.setPreferredSize(table.getPreferredSize());
        add(scrollPane, BorderLayout.CENTER);
    }

    protected void createTable(final SimplePortTableModel<S, P, L> tableModel, String emptyTableText) {

        table = createPortTable(tableModel, emptyTableText);

        // do not allow drag columns to other position
        table.getTableHeader().setReorderingAllowed(false);

        table.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);

        // disabled sorting
        table.setSortable(false);
    }

    protected PortTable createPortTable(final SimplePortTableModel<S, P, L> tableModel, String emptyTableText) {
        return new PortTable(tableModel, emptyTableText) {
            private static final long serialVersionUID = 1L;

            @Override
            public void clearTable() {
            }
        };
    }

    public void addPortListener(final L listener) {
        portListener = listener;
        tableModel.addPortListener(listener);
    }

    public String getName() {
        return Resources.getString(getClass(), "name");
    }

    @Override
    public void listChanged() {
        if (tableModel == null) {
            return;
        }

        LOGGER.info("The port list has changed in SimplePortListPanel.");

        refreshPorts();

        tableModel.setRowCount(0);

        // List<P> ports = getPorts();
        List<P> ports = new LinkedList<>();
        ports.addAll(getPorts());
        synchronized (ports) {
            for (P port : ports) {
                LOGGER.debug("Adding row for port: {}", port);
                tableModel.addRow(port);
            }
        }

        if (isPackLastColumn()) {
            if (table.getColumnCount() > 1) {
                table.packColumn(table.getColumnCount() - 1, 2);
            }
        }

        LOGGER.info("The port list has changed has finished in SimplePortListPanel.");
    }

    /**
     * @return the last column must be packed after all rows are added.
     */
    protected boolean isPackLastColumn() {
        return true;
    }

    @Override
    public void changeLabel(TargetType portType) {
        int portNum = portType.getPortNum();
        String label = portType.getLabel();

        P port = null;
        List<P> ports = getPorts();
        synchronized (ports) {
            port = PortListUtils.findPortByPortNumber(ports, portNum);
        }

        if (port != null) {
            try {
                portListener.labelChanged(port, label);
            }
            catch (Exception ex) {
                LOGGER.warn("Change port label failed.", ex);
            }
        }
    }

    protected void refreshPorts() {

    }

    // TODO make this method abstract after all ports are moved to Node
    protected List<P> getPorts() {
        LOGGER.info("Get ports returns: {}", ports);
        return ports;
    }
}
