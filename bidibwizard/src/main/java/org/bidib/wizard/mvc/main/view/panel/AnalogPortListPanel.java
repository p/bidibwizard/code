package org.bidib.wizard.mvc.main.view.panel;

import javax.swing.table.TableColumn;

import org.apache.commons.collections4.CollectionUtils;
import org.bidib.wizard.comm.AnalogPortStatus;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.main.model.AnalogPort;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.model.SimplePortTableModel;
import org.bidib.wizard.mvc.main.model.listener.OutputListener;
import org.bidib.wizard.mvc.main.view.table.ComboBoxWithButtonEditor;
import org.bidib.wizard.mvc.main.view.table.ComboBoxWithButtonRenderer;

public class AnalogPortListPanel
    extends SimplePortListPanel<AnalogPortStatus, AnalogPort, OutputListener<AnalogPortStatus>> {
    private static final long serialVersionUID = 1L;

    private final MainModel mainModel;

    public AnalogPortListPanel(MainModel model) {
        super(new SimplePortTableModel<AnalogPortStatus, AnalogPort, OutputListener<AnalogPortStatus>>() {
            private static final long serialVersionUID = 1L;

            @Override
            protected int getColumnPortInstance() {
                return SimplePortTableModel.COLUMN_PORT_INSTANCE;
            }
        }, model.getAnalogPorts(), Resources.getString(AnalogPortListPanel.class, "emptyTable"));

        this.mainModel = model;
        mainModel.addAnalogPortListListener(this);

        TableColumn buttonColumn = table.getColumnModel().getColumn(table.getColumnCount() - 1);

        buttonColumn.setCellRenderer(new ComboBoxWithButtonRenderer<>(table.getActions(AnalogPortStatus.START), ">"));
        buttonColumn.setCellEditor(new ComboBoxWithButtonEditor(table.getActions(AnalogPortStatus.START), ">"));
    }

    @Override
    protected void refreshPorts() {

        Node node = mainModel.getSelectedNode();
        if (node != null) {
            if (node.getNode().isPortFlatModelAvailable()) {
                if (CollectionUtils.isNotEmpty(node.getGenericPorts())) {
                    mainModel.getAnalogPorts();
                }
                else {
                    LOGGER
                        .info("The node supports flat port model but no generic ports are available. Skip get analog ports.");
                }
            }
            else {
                mainModel.getAnalogPorts();
            }
        }
    }
}
