package org.bidib.wizard.mvc.preferences.view.panel;

import java.awt.BorderLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Collection;
import java.util.LinkedList;

import javax.swing.JCheckBox;
import javax.swing.JPanel;

import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.preferences.model.PreferencesModel;
import org.bidib.wizard.mvc.preferences.view.listener.PreferencesViewListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.debug.FormDebugPanel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.FormLayout;

public class WizardSettingsPanel {

    private static final Logger LOGGER = LoggerFactory.getLogger(WizardSettingsPanel.class);

    private static final String ENCODED_DIALOG_COLUMN_SPECS = "pref, 3dlu, fill:50dlu:grow";

    private final Collection<PreferencesViewListener> listeners = new LinkedList<PreferencesViewListener>();

    private final PreferencesModel model;

    private JPanel contentPanel;

    public WizardSettingsPanel(final PreferencesModel model) {
        this.model = model;
    }

    public JPanel createPanel() {

        DefaultFormBuilder dialogBuilder = null;
        boolean debugDialog = false;
        if (debugDialog) {
            JPanel panel = new FormDebugPanel();
            dialogBuilder = new DefaultFormBuilder(new FormLayout(ENCODED_DIALOG_COLUMN_SPECS), panel);
        }
        else {
            JPanel panel = new JPanel(new BorderLayout());
            dialogBuilder = new DefaultFormBuilder(new FormLayout(ENCODED_DIALOG_COLUMN_SPECS), panel);
        }
        dialogBuilder.border(Borders.TABBED_DIALOG);

        // dialogBuilder.append(Resources.getString(getClass(), "labelPath") + ":");

        JCheckBox showTipOfDay = new JCheckBox(Resources.getString(getClass(), "showTipOfDay"), model.isShowTipOfDay());
        showTipOfDay.setContentAreaFilled(false);
        showTipOfDay.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                final boolean showTipOfDay = e.getStateChange() == ItemEvent.SELECTED;
                firePropertyChanged("showTipOfDay", showTipOfDay);
            }
        });
        dialogBuilder.append(showTipOfDay, 3);

        JCheckBox csQueryEnabled =
            new JCheckBox(Resources.getString(getClass(), "csQueryEnabled"), model.isCsQueryEnabled());
        csQueryEnabled.setContentAreaFilled(false);
        csQueryEnabled.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                final boolean csQueryEnabled = e.getStateChange() == ItemEvent.SELECTED;
                firePropertyChanged("csQueryEnabled", csQueryEnabled);
            }
        });
        dialogBuilder.append(csQueryEnabled, 3);

        JCheckBox showActionInLastTab =
            new JCheckBox(Resources.getString(getClass(), "showActionInLastTab"), model.isShowActionInLastTab());
        showActionInLastTab.setContentAreaFilled(false);
        showActionInLastTab.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                final boolean showActionInLastTab = e.getStateChange() == ItemEvent.SELECTED;
                firePropertyChanged("showActionInLastTab", showActionInLastTab);
            }
        });
        dialogBuilder.append(showActionInLastTab, 3);

        JCheckBox allBoosterOnDoNotConfirmSwitch =
            new JCheckBox(Resources.getString(getClass(), "allBoosterOnDoNotConfirmSwitch"),
                model.isAllBoosterOnDoNotConfirmSwitch());
        allBoosterOnDoNotConfirmSwitch.setContentAreaFilled(false);
        allBoosterOnDoNotConfirmSwitch.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                final boolean allBoosterOnDoNotConfirmSwitch = e.getStateChange() == ItemEvent.SELECTED;
                firePropertyChanged("allBoosterOnDoNotConfirmSwitch", allBoosterOnDoNotConfirmSwitch);
            }
        });
        dialogBuilder.append(allBoosterOnDoNotConfirmSwitch, 3);

        JCheckBox m4SupportEnabled =
            new JCheckBox(Resources.getString(getClass(), "m4SupportEnabled"), model.isM4SupportEnabled());
        m4SupportEnabled.setContentAreaFilled(false);
        m4SupportEnabled.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                final boolean m4SupportEnabled = e.getStateChange() == ItemEvent.SELECTED;
                firePropertyChanged("m4SupportEnabled", m4SupportEnabled);
            }
        });
        dialogBuilder.append(m4SupportEnabled, 3);

        contentPanel = dialogBuilder.build();
        return contentPanel;
    }

    public void addPreferencesViewListener(PreferencesViewListener l) {
        listeners.add(l);
    }

    private void firePropertyChanged(String propertyName, boolean value) {
        for (PreferencesViewListener l : listeners) {
            l.propertyChanged(propertyName, value);
        }
    }

}
