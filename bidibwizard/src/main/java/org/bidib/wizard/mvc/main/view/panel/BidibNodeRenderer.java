package org.bidib.wizard.mvc.main.view.panel;

import java.awt.Component;

import javax.swing.Icon;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

import org.bidib.jbidibc.core.ProtocolVersion;
import org.bidib.jbidibc.core.enumeration.IdentifyState;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.view.panel.NodeTree.NodeTreeNode;
import org.bidib.wizard.mvc.main.view.panel.renderer.BidibNodeNameUtils;
import org.bidib.wizard.mvc.preferences.model.Preferences;
import org.bidib.wizard.mvc.preferences.model.listener.PreferencesAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BidibNodeRenderer extends DefaultTreeCellRenderer {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(BidibNodeRenderer.class);

    private Icon bidibLeafIcon;

    private Icon bidibIdentifyIcon;

    private Icon bidibNodeIcon;

    private Icon bidibErrorIcon;

    private Icon bidibRestartIcon;

    private Icon bidibLeafWarnIcon;

    private Icon bidibNodeWarnIcon;

    private boolean alwaysShowProductNameInTree;

    private String messageUnsupportedProtocol;

    public BidibNodeRenderer(final NodeTree nodeTree, Icon leafIcon, Icon nodeIcon, Icon identifyIcon, Icon errorIcon,
        Icon restartPendingIcon, Icon leafWarnIcon, Icon nodeWarnIcon, String messageUnsupportedProtocol) {
        bidibLeafIcon = leafIcon;
        bidibNodeIcon = nodeIcon;
        bidibIdentifyIcon = identifyIcon;
        bidibErrorIcon = errorIcon;
        bidibRestartIcon = restartPendingIcon;
        bidibLeafWarnIcon = leafWarnIcon;
        bidibNodeWarnIcon = nodeWarnIcon;
        this.messageUnsupportedProtocol = messageUnsupportedProtocol;

        alwaysShowProductNameInTree = Preferences.getInstance().isAlwaysShowProductNameInTree();
        LOGGER.info("alwaysShowProductNameInTree: {}", alwaysShowProductNameInTree);

        Preferences.getInstance().addPreferencesListener(new PreferencesAdapter() {

            @Override
            public void propertyChanged(String propertyName, Object oldValue, Object newValue) {

                if (Preferences.PROPERTY_ALWAYS_SHOW_PRODUCTNAME_IN_TREE.equals(propertyName)) {
                    if (newValue instanceof Boolean) {
                        alwaysShowProductNameInTree = (Boolean) newValue;
                        LOGGER.info("Refresh the tree.");
                        nodeTree.refreshTree();
                    }
                }
            }
        });
    }

    public Component getTreeCellRendererComponent(
        JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {

        super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);

        NodeTreeNode nodeTreeNode = (NodeTreeNode) value;
        Node node = (Node) (nodeTreeNode.getUserObject());
        if (isNodeHasError(node)) {
            if (isNodeHasRestartPendingError(node)) {
                setIcon(bidibRestartIcon);
            }
            else {
                setIcon(bidibErrorIcon);
            }
        }
        else if (isIdentifyNode(node)) {
            setIcon(bidibIdentifyIcon);
        }
        else if (isUnsupportedProtocolVersionNode(node)) {
            if (leaf) {
                setIcon(bidibLeafWarnIcon);
            }
            else {
                setIcon(bidibNodeWarnIcon);
            }
        }
        else if (isUpdateableNode(node)) {
            if (leaf) {
                setIcon(bidibLeafIcon);
            }
            else {
                setIcon(bidibNodeIcon);
            }
        }
        else {
            if (leaf) {
                setIcon(bidibLeafIcon);
            }
            else {
                setIcon(bidibNodeIcon);
            }
        }
        prepareLabel(node);
        return this;
    }

    protected void prepareLabel(final Node node) {
        BidibNodeNameUtils.NodeLabelData labelData =
            BidibNodeNameUtils.prepareLabel(node, alwaysShowProductNameInTree, true);

        setText(labelData.getNodeLabel());

        if (isUnsupportedProtocolVersionNode(node)) {
            setToolTipText(messageUnsupportedProtocol);
        }
        else {
            setToolTipText(labelData.getNodeToolTipText());
        }
    }

    protected boolean isNodeHasError(final Node node) {
        if (node != null) {
            return node.isNodeHasError();
        }
        return false;
    }

    protected boolean isNodeHasRestartPendingError(final Node node) {
        if (node != null) {
            return node.isNodeHasRestartPendingError();
        }
        return false;
    }

    protected boolean isUpdateableNode(final Node node) {
        if (node != null) {
            return node.isUpdatable();
        }
        return false;
    }

    protected boolean isIdentifyNode(final Node node) {
        if (node != null && IdentifyState.START.equals(node.getIdentifyState())) {
            return true;
        }
        return false;
    }

    protected boolean isUnsupportedProtocolVersionNode(final Node node) {
        if (node != null && ProtocolVersion.isSupportedProtocolVersion(node.getNode().getProtocolVersion())) {
            return true;
        }
        return false;
    }
}
