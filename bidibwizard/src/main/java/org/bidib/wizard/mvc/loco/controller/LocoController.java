package org.bidib.wizard.mvc.loco.controller;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.BitSet;

import javax.swing.JFrame;

import org.bidib.jbidibc.core.AddressData;
import org.bidib.jbidibc.core.DefaultMessageListener;
import org.bidib.jbidibc.core.DriveState;
import org.bidib.jbidibc.core.MessageListener;
import org.bidib.jbidibc.core.enumeration.CommandStationState;
import org.bidib.jbidibc.core.exception.NoAnswerException;
import org.bidib.jbidibc.core.utils.ProductUtils;
import org.bidib.wizard.comm.Communication;
import org.bidib.wizard.comm.CommunicationFactory;
import org.bidib.wizard.comm.Direction;
import org.bidib.wizard.comm.SpeedSteps;
import org.bidib.wizard.comm.listener.CommunicationListener;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.bidib.wizard.mvc.common.view.ViewCloseListener;
import org.bidib.wizard.mvc.loco.model.LocoModel;
import org.bidib.wizard.mvc.loco.model.listener.LocoModelListener;
import org.bidib.wizard.mvc.loco.view.LocoView;
import org.bidib.wizard.mvc.main.controller.CommandStationService;
import org.bidib.wizard.mvc.main.model.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LocoController {
    private static final Logger LOGGER = LoggerFactory.getLogger(LocoController.class);

    private final Node node;

    private final JFrame parent;

    private final int x;

    private final int y;

    private final LocoModel model = new LocoModel();

    private LocoView locoView;

    private LocoModelListener locoModelListener;

    public LocoController(Node node, JFrame parent, int x, int y) {
        this.node = node;
        this.parent = parent;
        this.x = x;
        this.y = y;
    }

    /**
     * Start the loco controller.
     */
    public void start() {
        if (ProductUtils.isRFBasisNode(node.getUniqueId())) {
            LOGGER.info("The default speed steps for the decoders on the RF Basis Node is 28.");
            model.setSpeedSteps(SpeedSteps.DCC28);
        }
        else {
            model.setSpeedSteps(SpeedSteps.DCC128);
        }

        final Communication communication = CommunicationFactory.getInstance();

        final MessageListener messageListener = new DefaultMessageListener() {
            @Override
            public void speed(byte[] address, AddressData addressData, int speed) {

                // TODO must check the address and the address of the selected node and make sure the speed is delivered
                // from the correct decoder
                // because we can have multiple domains

                if (addressData.getAddress() == model.getAddress()) {
                    model.setReportedSpeed(speed);
                }
            }

            @Override
            public void dynState(
                byte[] address, int detectorNumber, AddressData decoderAddress, int dynNumber, int dynValue) {

                // TODO must check the address and the address of the selected node

                // TODO the dynNumber is for OpenCar decoder
                LOGGER.info("dynState, decoderAddress: {}, dynNumber: {}, dynValue: {}", decoderAddress, dynNumber,
                    dynValue);

                if (decoderAddress.getAddress() == model.getAddress() && dynNumber == 3) {
                    // LOGGER.info("The dynState is forwarded to model");
                    model.setDynStateEnergy(dynValue);
                }
                else {
                    LOGGER.debug("The dynState ist not forwarded to model");
                }

            }

            @Override
            public void csDriveManual(byte[] address, DriveState driveState) {
                LOGGER.info("Received csDriveManual, address: {}, driveState: {}", address, driveState);
                // TODO process csDriveManual message
            }
        };
        communication.addMessageListener(messageListener);

        communication.addCommunicationListener(new CommunicationListener() {

            @Override
            public void status(String statusText, int displayDuration) {
            }

            @Override
            public void opening() {

            }

            @Override
            public void opened(String port) {
            }

            @Override
            public void initialized() {
            }

            @Override
            public void closed(String port) {

                LOGGER.info("The communication port was closed, close the locoView: {}", locoView);

                if (locoView != null) {
                    locoView.close();

                    locoView = null;
                }

                try {
                    communication.removeCommunicationListener(this);
                }
                catch (Exception ex) {
                    LOGGER.warn("Remove communication listener failed.", ex);
                }
            }
        });

        locoModelListener = new LocoModelListener() {
            @Override
            public void addressChanged(int address) {
            }

            @Override
            public void directionChanged(Direction direction) {
            }

            @Override
            public void functionChanged(int index, boolean value) {

                // Prepare the function group index
                int functionGroupIndex = 0;

                if (index < 5) {
                    functionGroupIndex = 0;
                }
                else if (index < 9) {
                    functionGroupIndex = 1;
                }
                else if (index < 13) {
                    functionGroupIndex = 2;
                }
                else if (index < 21) {
                    functionGroupIndex = 3;
                }
                else {
                    functionGroupIndex = 4;
                }

                BitSet activeFunctions = new BitSet(functionGroupIndex + 1);
                LOGGER.debug("functions have changed, functionGroupIndex: {}", functionGroupIndex);
                activeFunctions.set(functionGroupIndex, true);
                communication.setSpeed(node.getNode(), model.getAddress(), model.getSpeedSteps(), null,
                    model.getDirection(), activeFunctions, model.getFunctions());
            }

            @Override
            public void speedStepsChanged(SpeedSteps speedSteps) {
            }

            @Override
            public void dynStateEnergyChanged(int dynStateEnergy) {
            }
        };
        model.addLocoModelListener(locoModelListener);

        model.addPropertyChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                switch (evt.getPropertyName()) {
                    case LocoModel.PROPERTYNAME_SPEED:
                        try {
                            int speed = (Integer) evt.getNewValue();
                            communication.setSpeed(node.getNode(), model.getAddress(), model.getSpeedSteps(), speed,
                                model.getDirection(), null, null);
                        }
                        catch (NoAnswerException ex) {
                            LOGGER.warn("Set speed failed.", ex);
                        }
                        catch (Exception ex) {
                            LOGGER.warn("Set speed failed.", ex);
                        }
                        break;
                    default:
                        break;
                }

            }
        });

        LOGGER.info("Create new instance of LocoView.");
        locoView = new LocoView(parent, model, x, y);

        locoView.addViewCloseListener(new ViewCloseListener() {

            @Override
            public void close() {
                LOGGER.info("The locoView is closed.");
                unregisterMessageListener(messageListener, communication);

                // release locoView
                if (locoView != null) {
                    locoView = null;
                }

                if (locoModelListener != null) {
                    LOGGER.info("Remove locoModelListener from model.");
                    model.removeLocoModelListener(locoModelListener);
                    locoModelListener = null;
                }
            }
        });

        // query the current state of the command station
        try {
            CommandStationState csState = communication.queryCommandStationState(node.getNode());
            LOGGER.info("Current command station state: {}", csState);
            if (CommandStationState.isOffState(csState)) {
                LOGGER.info("Start the watchdog.");
                CommandStationService csService =
                    DefaultApplicationContext.getInstance().get(DefaultApplicationContext.KEY_COMMAND_STATION_SERVICE,
                        CommandStationService.class);
                csService.switchOn(node, false);
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Get command station state failed.", ex);
        }

    }

    private void unregisterMessageListener(MessageListener messageListener, final Communication communication) {
        LOGGER.info("Unregister message listener.");
        if (messageListener != null) {
            LOGGER.info("Remove the LocoController as message listener.");
            try {
                communication.removeMessageListener(messageListener);
            }
            catch (Exception ex) {
                LOGGER.warn("Remove the LocoController as message listener failed.", ex);
            }

            messageListener = null;
        }
    }
}
