package org.bidib.wizard.mvc.main.model.listener;

import org.bidib.jbidibc.core.enumeration.IdentifyState;
import org.bidib.jbidibc.core.enumeration.SysErrorEnum;

/**
 * This listener signals changes of the selected node.
 */
public interface NodeListener {
    /**
     * @param isAddressMessagesEnabled
     *            the new addressMessagesEnabled flag
     */
    void addressMessagesEnabledChanged(Boolean isAddressMessagesEnabled);

    /**
     * @param isDccStartEnabled
     *            the new dccStartEnabled flag
     */
    void dccStartEnabledChanged(Boolean isDccStartEnabled);

    /**
     * @param railComPlusAvailable
     *            the new railComPlusAvailable flag
     */
    void railComPlusAvailableChanged(Boolean railComPlusAvailable);

    /**
     * @param pomUpdateAvailable
     *            the new pomUpdateAvailable flag
     */
    void pomUpdateAvailableChanged(Boolean pomUpdateAvailable);

    /**
     * @param isExternalStartEnabled
     *            the new externalStartEnabled flag
     */
    void externalStartEnabledChanged(Boolean isExternalStartEnabled);

    /**
     * @param isFeedbackMessagesEnabled
     *            the new feedbackMessagesEnabled flag
     */
    void feedbackMessagesEnabledChanged(Boolean isFeedbackMessagesEnabled);

    /**
     * @param isFeedbackMirrorDisabled
     *            the new feedbackMirrorDisabled flag
     */
    void feedbackMirrorDisabledChanged(Boolean isFeedbackMirrorDisabled);

    /**
     * The identify state of the selected node has changed.
     * 
     * @param identifyState
     *            the new identifyState
     */
    void identifyStateChanged(IdentifyState identifyState);

    /**
     * @param isKeyMessagesEnabled
     *            the new keyMessagesEnabled flag
     */
    void keyMessagesEnabledChanged(Boolean isKeyMessagesEnabled);

    /**
     * @param label
     *            the new label
     */
    void labelChanged(String label);

    /**
     * The sysError of the selected node has changed.
     * 
     * @param sysError
     *            the new sysError
     */
    void sysErrorChanged(SysErrorEnum sysError);
}
