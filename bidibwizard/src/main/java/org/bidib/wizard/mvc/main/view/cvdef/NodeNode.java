package org.bidib.wizard.mvc.main.view.cvdef;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.bidib.jbidibc.exchange.vendorcv.NodeType;
import org.bidib.jbidibc.exchange.vendorcv.NodetextType;
import org.bidib.jbidibc.exchange.vendorcv.VendorCVUtils;
import org.bidib.wizard.utils.XmlLocaleUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jidesoft.grid.DefaultExpandableRow;

public class NodeNode extends DefaultExpandableRow {
    private static final Logger LOGGER = LoggerFactory.getLogger(NodeNode.class);

    public static final int COLUMN_KEY = -1;

    protected int index = -1;

    protected String lang;

    private NodeType node;

    public NodeNode(NodeType node, int index, String lang) {
        // super(node);
        this.node = node;
        this.index = index;
        this.lang = lang;
    }

    // @Override
    // public int getColumnCount() {
    // return 1;
    // }

    public NodeType getNode() {
        return node;
    }

    @Override
    public Object getValueAt(int column) {
        NodeType nodeType = getNode();
        LOGGER.trace("Get value for column: {}", column);
        switch (column) {
            case 0:
                // we must check the language ...
                NodetextType nodetext = VendorCVUtils.getNodetextOfLanguage(nodeType, lang);

                if (nodetext != null) {
                    String value = nodetext.getText();
                    String replacementD = Integer.toString(index);
                    value = value.replaceAll("%%d", replacementD);
                    String replacementP = Integer.toString(index + 1);
                    value = value.replaceAll("%%p", replacementP);
                    LOGGER.trace("Prepared value: {}", value);
                    return value;
                }
                break;
            case COLUMN_KEY:
                NodetextType nodetextDef =
                    VendorCVUtils.getNodetextOfLanguage(nodeType, XmlLocaleUtils.DEFAULT_LOCALE_VENDOR_CV);
                if (nodetextDef != null) {
                    String value = nodetextDef.getText();
                    return value;
                }
                break;
            default:
                break;
        }

        return null;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
