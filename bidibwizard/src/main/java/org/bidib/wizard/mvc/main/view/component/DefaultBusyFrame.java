package org.bidib.wizard.mvc.main.view.component;

import java.awt.Cursor;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultBusyFrame extends JFrame implements BusyFrame {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultBusyFrame.class);

    protected JPanel glassPane;

    public DefaultBusyFrame() {
        initBusyJFrame();
    }

    protected void initBusyJFrame() {
        setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        // Create our own glass pane which says it manages focus. This is
        // part of the key to capturing keyboard events.
        glassPane = new JPanel() {
            private static final long serialVersionUID = 1L;

            @Override
            public boolean getFocusTraversalKeysEnabled() {
                return true;
            }
        };
        // Add a no-op MouseAdapter so that we enable mouse events
        glassPane.addMouseListener(new MouseAdapter() {
        });
        // Eat keystrokes so they don't go to other components
        glassPane.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                e.consume();
            }
        });
        glassPane.setOpaque(false);
        this.setGlassPane(glassPane);
    }

    private final Cursor waitCursor = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);

    private final Cursor defaultCursor = Cursor.getDefaultCursor();

    @Override
    public boolean setBusy(boolean busy) {
        if (busy) {
            LOGGER.info("Set wait cursor");

            if (waitCursor.equals(getCursor())) {
                return false;
            }

            // Setting the frame cursor AND glass pane cursor in this order
            // works around the Win32 problem where you have to move the mouse 1
            // pixel to get the Cursor to change.
            this.setCursor(waitCursor /* Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR) */);

            glassPane.setVisible(true);
            // Force glass pane to get focus so that we consume KeyEvents
            glassPane.requestFocus();
            glassPane.setCursor(waitCursor /* Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR) */);
            // Turn off the close button
            setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        }
        else {
            LOGGER.info("Set default cursor");

            if (defaultCursor.equals(getCursor())) {
                return false;
            }

            glassPane.setCursor(defaultCursor /* Cursor.getDefaultCursor() */);
            glassPane.setVisible(false);
            this.requestFocus();

            this.setCursor(defaultCursor /* Cursor.getDefaultCursor() */);
            setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        }

        return true;
    }
}
