package org.bidib.wizard.mvc.main.view.cvdef;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.jdesktop.swingx.treetable.AbstractMutableTreeTableNode;

@Deprecated
public class AboutNode extends AbstractMutableTreeTableNode {

    public AboutNode(Object[] data) {
        super(data);
    }

    @Override
    public Object getValueAt(int column) {
        if (column > 4) {
            return null;
        }
        return null;
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
