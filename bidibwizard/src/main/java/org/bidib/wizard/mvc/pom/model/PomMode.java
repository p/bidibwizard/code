package org.bidib.wizard.mvc.pom.model;

/**
 * The programming mode used for the programming track.
 */
public enum PomMode {
    BYTE("byte"), BIT("bit");

    private final String key;

    private PomMode(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }
}
