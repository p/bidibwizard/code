package org.bidib.wizard.mvc.dmx.view;

import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.apache.commons.lang.StringUtils;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.dmx.model.DmxModel;
import org.bidib.wizard.mvc.dmx.model.DmxScenery;
import org.bidib.wizard.mvc.dmx.model.DmxSceneryModel;
import org.bidib.wizard.mvc.dmx.view.panel.DmxModelerPanel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vlsolutions.swing.docking.DockKey;
import com.vlsolutions.swing.docking.Dockable;
import com.vlsolutions.swing.docking.DockableState;
import com.vlsolutions.swing.docking.DockingDesktop;
import com.vlsolutions.swing.docking.event.DockableStateChangeEvent;
import com.vlsolutions.swing.docking.event.DockableStateChangeListener;

/**
 * The <code>DmxModelerView</code> is the view that shows the chart for a <code>DmxScenery</code>.
 */
public class DmxModelerView implements Dockable {
    private static final Logger LOGGER = LoggerFactory.getLogger(DmxModelerView.class);

    private final DockKey DOCKKEY = new DockKey("DmxModelerView");

    private DmxModelerPanel contentPanel;

    private final DockableStateChangeListener dockableStateChangeListener;

    public DmxModelerView(final DockingDesktop desktop, final DmxScenery dmxScenery,
        final DmxSceneryModel dmxSceneryModel) {

        DOCKKEY.setName(prepareTitle(dmxScenery));
        // turn off autohide and enable close and float-enabled features
        DOCKKEY.setCloseEnabled(true);
        DOCKKEY.setAutoHideEnabled(false);
        DOCKKEY.setFloatEnabled(true);

        DOCKKEY.setKey(prepareKey(dmxScenery));
        LOGGER.info("Created new DOCKKEY: {}", DOCKKEY.getKey());

        dockableStateChangeListener = new DockableStateChangeListener() {

            @Override
            public void dockableStateChanged(DockableStateChangeEvent event) {
                LOGGER.info("The state has changed, newState: {}, prevState: {}", event.getNewState(),
                    event.getPreviousState());

                DockableState newState = event.getNewState();
                if (newState.getDockable().equals(DmxModelerView.this) && newState.isClosed()) {
                    LOGGER.info("The DmxModelerView is closed.");
                    // we are closed
                    desktop.removeDockableStateChangeListener(dockableStateChangeListener);

                    if (contentPanel != null) {
                        contentPanel.cleanup();
                    }
                }

            }
        };
        desktop.addDockableStateChangeListener(dockableStateChangeListener);

        final DmxModel dmxModel = new DmxModel(dmxScenery, dmxSceneryModel);
        contentPanel = new DmxModelerPanel(dmxModel);

        // if the name of the scenery is changed we must update the dockkey name and the title
        dmxScenery.addPropertyChangeListener(DmxScenery.PROPERTY_NAME, new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                // scenery name has changed
                String sceneryName = (String) evt.getNewValue();
                if (StringUtils.isNotBlank(sceneryName)) {
                    // update the window title
                    DOCKKEY.setName(prepareTitle(dmxScenery));
                }
            }
        });

    }

    public static String prepareKey(final DmxScenery dmxScenery) {
        return Resources.getString(DmxModelerView.class, "title") + " - " + dmxScenery.getId();
    }

    public static String prepareTitle(final DmxScenery dmxScenery) {
        return Resources.getString(DmxModelerView.class, "title") + " - " + dmxScenery.getName();
    }

    @Override
    public DockKey getDockKey() {
        return DOCKKEY;
    }

    @Override
    public Component getComponent() {
        return contentPanel;
    }

    public void loadSceneryPoints() {
        contentPanel.loadSceneryPoints();
    }

    public void storeSceneryPoints() {
        contentPanel.storeSceneryPoints();
    }
}
