package org.bidib.wizard.mvc.pt.controller.listener;

import org.bidib.jbidibc.core.enumeration.CommandStationPt;
import org.bidib.jbidibc.core.enumeration.CommandStationState;
import org.bidib.wizard.mvc.main.model.Node;

public interface PtProgrammerControllerListener {
    void close();

    void sendRequest(Node node, CommandStationPt opCode, int cvNumber, int cvValue);

    void sendCommandStationStateRequest(Node node, CommandStationState commandStationState);

    /**
     * Get the current state of the command station.
     * 
     * @param node
     *            the node
     * @return the current command station state
     */
    CommandStationState getCurrentCommandStationState(Node node);
}
