package org.bidib.wizard.mvc.locolist.view;

import java.awt.Graphics2D;

import javax.swing.ImageIcon;

import org.bidib.wizard.common.utils.ImageUtils;
import org.bidib.wizard.mvc.booster.view.BoosterStateCellRenderer;

public class LocoLightRenderer extends AbstractMaskedIconRenderer {

    private static final long serialVersionUID = 1L;

    protected final ImageIcon lightOff;

    protected final ImageIcon lightOn;

    public LocoLightRenderer(int mask) {
        super(mask);

        lightOn = ImageUtils.createImageIcon(BoosterStateCellRenderer.class, "/icons/16x16/lightbulb.png");
        lightOff = ImageUtils.createImageIcon(BoosterStateCellRenderer.class, "/icons/16x16/lightbulb_off.png");
    }

    @Override
    protected void renderImages(Graphics2D g2) {

        int i = 0;
        g2.drawImage(((value >> 4) & 0x01) == 0x00 ? lightOff.getImage() : lightOn.getImage(), 2 + (i * 18), 2, this);
    }

}
