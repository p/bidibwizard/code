package org.bidib.wizard.mvc.common.view.list;

import javax.swing.DefaultListModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// @author Santhosh Kumar T - santhosh@in.fiorano.com
public class DefaultMutableListModel<E> extends DefaultListModel<E> implements MutableListModel<E> {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultMutableListModel.class);

    private static final long serialVersionUID = 1L;

    public boolean isCellEditable(int index) {
        return true;
    }

    public void setValueAt(E value, int index) {
        LOGGER.info("Set the new value: {}, index: {}", value, index);

        super.setElementAt(value, index);
    }
}
