package org.bidib.wizard.mvc.stepcontrol.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.bidib.wizard.mvc.main.model.MotorPort;
import org.bidib.wizard.mvc.main.model.SoundPort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.beans.Model;
import com.jgoodies.common.collect.ArrayListModel;

public class StepControlModel extends Model {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(StepControlModel.class);

    public static final String PROPERTYNAME_SELECTED_ASPECT = "selectedAspect";

    public static final String PROPERTYNAME_STEPCONTROL_ASPECTS = "stepControlAspects";

    public static final String PROPERTYNAME_CURRENT_POSITION = "currentPosition";

    public static final String PROPERTYNAME_TURNTABLE_TYPE = "turnTableType";

    public static final String PROPERTYNAME_SPEED = "speed";

    public static final String PROPERTYNAME_ACCEL = "accel";

    public static final String PROPERTYNAME_DECEL = "decel";

    public static final String PROPERTYNAME_PUSH_INTERVAL = "pushInterval";

    public static final String PROPERTYNAME_OPERATIONAL_MODE = "operationalMode";

    public static final String PROPERTYNAME_CURRENT_DEGREES = "currentDegrees";

    public static final String PROPERTYNAME_TARGET_DEGREES = "targetDegrees";

    public static final String PROPERTYNAME_MOTOR_PORT = "motorPort";

    public static final String PROPERTYNAME_SOUND_PORTS = "soundPorts";

    private ArrayListModel<StepControlAspect> stepControlAspects = new ArrayListModel<>();

    private StepControlAspect selectedAspect;

    private TurnTableType turnTableType;

    private Long currentPosition;

    private int speed = 1;

    private int accel = 1;

    private int decel = 1;

    private Integer pushInterval;

    private boolean operationalMode;

    private double currentDegrees;

    private double targetDegrees;

    private MotorPort motorPort;

    private List<SoundPort> soundPorts;

    /**
     * @return the selectedAspect
     */
    public StepControlAspect getSelectedAspect() {
        return selectedAspect;
    }

    /**
     * @param selectedAspect
     *            the selectedAspect to set
     */
    public void setSelectedAspect(StepControlAspect selectedAspect) {
        StepControlAspect oldValue = this.selectedAspect;
        this.selectedAspect = selectedAspect;

        firePropertyChange(PROPERTYNAME_SELECTED_ASPECT, oldValue, selectedAspect);
    }

    /**
     * @return the stepControlAspects list model
     */
    public ArrayListModel<StepControlAspect> getStepControlAspectsListModel() {
        return stepControlAspects;
    }

    /**
     * @return the stepControlAspects
     */
    public List<StepControlAspect> getStepControlAspects() {
        return stepControlAspects;
    }

    /**
     * @param stepControlAspect
     *            the single stepControlAspect to add
     */
    public void addStepControlAspect(StepControlAspect stepControlAspect) {
        List<StepControlAspect> oldValue = new ArrayList<>();
        oldValue.addAll(this.stepControlAspects);

        // TODO fix this hack
        if (stepControlAspect.getAspectNumber() == null) {
            int size = stepControlAspects.size();
            stepControlAspect.setAspectNumber(size + 1);
        }

        this.stepControlAspects.add(stepControlAspect);

        firePropertyChange(PROPERTYNAME_STEPCONTROL_ASPECTS, oldValue, stepControlAspects);
    }

    /**
     * @param stepControlAspect
     *            the single stepControlAspect to remove
     */
    public void removeStepControlAspect(StepControlAspect stepControlAspect) {
        List<StepControlAspect> oldValue = new ArrayList<>();
        oldValue.addAll(this.stepControlAspects);

        this.stepControlAspects.remove(stepControlAspect);

        firePropertyChange(PROPERTYNAME_STEPCONTROL_ASPECTS, oldValue, stepControlAspects);
    }

    /**
     * @param stepControlAspects
     *            the stepControlAspects to set
     */
    public void setStepControlAspects(List<StepControlAspect> stepControlAspects) {
        List<StepControlAspect> oldValue = new ArrayList<>();
        oldValue.addAll(this.stepControlAspects);

        this.stepControlAspects.clear();
        if (CollectionUtils.isNotEmpty(stepControlAspects)) {
            this.stepControlAspects.addAll(stepControlAspects);
        }

        firePropertyChange(PROPERTYNAME_STEPCONTROL_ASPECTS, oldValue, this.stepControlAspects);
    }

    /**
     * @return the turnTableType
     */
    public TurnTableType getTurnTableType() {
        return turnTableType;
    }

    /**
     * @param turnTableType
     *            the turnTableType to set
     */
    public void setTurnTableType(TurnTableType turnTableType) {
        LOGGER.info("Set the turnTableType: {}", turnTableType);

        TurnTableType oldValue = this.turnTableType;
        this.turnTableType = turnTableType;

        firePropertyChange(PROPERTYNAME_TURNTABLE_TYPE, oldValue, turnTableType);
    }

    /**
     * @return the currentPosition
     */
    public Long getCurrentPosition() {
        return currentPosition;
    }

    /**
     * @param currentPosition
     *            the currentPosition to set
     */
    public void setCurrentPosition(Long currentPosition) {
        LOGGER.info("Set the current position: {}", currentPosition);

        Long oldValue = this.currentPosition;
        this.currentPosition = currentPosition;

        firePropertyChange(PROPERTYNAME_CURRENT_POSITION, oldValue, currentPosition);
    }

    /**
     * @return the speed
     */
    public int getSpeed() {
        return speed;
    }

    /**
     * @param speed
     *            the speed to set
     */
    public void setSpeed(int speed) {
        LOGGER.info("Set speed: {}", speed);

        int oldValue = this.speed;
        this.speed = speed;
        firePropertyChange(PROPERTYNAME_SPEED, oldValue, speed);
    }

    /**
     * @return the accel
     */
    public int getAccel() {
        return accel;
    }

    /**
     * @param accel
     *            the accel to set
     */
    public void setAccel(int accel) {
        int oldValue = this.accel;
        this.accel = accel;
        firePropertyChange(PROPERTYNAME_ACCEL, oldValue, accel);
    }

    /**
     * @return the decel
     */
    public int getDecel() {
        return decel;
    }

    /**
     * @param decel
     *            the decel to set
     */
    public void setDecel(int decel) {
        int oldValue = this.decel;
        this.decel = decel;
        firePropertyChange(PROPERTYNAME_DECEL, oldValue, decel);
    }

    /**
     * @return the operationalMode
     */
    public boolean getOperationalMode() {
        return operationalMode;
    }

    /**
     * @param operationalMode
     *            the operationalMode to set
     */
    public void setOperationalMode(boolean operationalMode) {
        boolean oldValue = this.operationalMode;
        this.operationalMode = operationalMode;

        firePropertyChange(PROPERTYNAME_OPERATIONAL_MODE, oldValue, operationalMode);
    }

    /**
     * Clear all values from the model.
     */
    public void clearModel() {
        setStepControlAspects(null);
        setCurrentPosition(0L);
        setSelectedAspect(null);
        setTurnTableType(null);
        setSpeed(1);
        setAccel(1);
        setDecel(1);
        setOperationalMode(false);
    }

    public void setTurntableCurrentDegrees(double degrees) {
        double oldValue = currentDegrees;
        currentDegrees = degrees;

        firePropertyChange(PROPERTYNAME_CURRENT_DEGREES, oldValue, currentDegrees);
    }

    public double getTurntableCurrentDegrees() {
        return currentDegrees;
    }

    public void setTurntableTargetDegrees(double degrees) {
        double oldValue = targetDegrees;
        targetDegrees = degrees;

        firePropertyChange(PROPERTYNAME_TARGET_DEGREES, oldValue, targetDegrees);
    }

    public double getTurntableTargetDegrees() {
        return targetDegrees;
    }

    /**
     * @return the motorPort
     */
    public MotorPort getMotorPort() {
        return motorPort;
    }

    /**
     * @param motorPort
     *            the motorPort to set
     */
    public void setMotorPort(MotorPort motorPort) {
        MotorPort oldValue = this.motorPort;

        this.motorPort = motorPort;

        firePropertyChange(PROPERTYNAME_MOTOR_PORT, oldValue, motorPort);
    }

    /**
     * @return the soundPorts
     */
    public List<SoundPort> getSoundPorts() {
        return soundPorts;
    }

    /**
     * @param soundPorts
     *            the soundPorts to set
     */
    public void setSoundPorts(List<SoundPort> soundPorts) {
        List<SoundPort> oldValue = this.soundPorts;

        this.soundPorts = soundPorts;

        firePropertyChange(PROPERTYNAME_SOUND_PORTS, oldValue, soundPorts);
    }

    /**
     * @param pushInterval
     *            the push interval to set
     */
    public void setPushInterval(Integer pushInterval) {
        Integer oldValue = this.pushInterval;

        this.pushInterval = pushInterval;
        firePropertyChange(PROPERTYNAME_PUSH_INTERVAL, oldValue, pushInterval);
    }

    /**
     * @return the push interval
     */
    public Integer getPushInterval() {
        return pushInterval;
    }

}
