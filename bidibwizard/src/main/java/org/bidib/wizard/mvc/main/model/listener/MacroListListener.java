package org.bidib.wizard.mvc.main.model.listener;

public interface MacroListListener {
    /**
     * The macros have been changed.
     */
    void listChanged();

    /**
     * The selected macro has changed.
     */
    void macroChanged();

    /**
     * The pending changes flag has been changed.
     */
    void pendingChangesChanged();
}
