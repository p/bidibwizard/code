package org.bidib.wizard.mvc.main.view.panel.glazed;

import org.bidib.wizard.mvc.main.model.FeedbackPosition;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.TransformedList;
import ca.odell.glazedlists.event.ListEvent;

public class FeedbackPositionsToDecoderList extends TransformedList<FeedbackPosition, String> {

    /**
     * Construct an FeedbackPositionsToDecoderList from an EventList that contains only FeedbackPosition objects.
     */
    public FeedbackPositionsToDecoderList(EventList<FeedbackPosition> source) {
        super(source);
        source.addListEventListener(this);
    }

    /**
     * Gets the decoder address at the specified index.
     */
    @Override
    public String get(int index) {
        FeedbackPosition position = source.get(index);
        return Integer.toString(position.getDecoderAddress());
    }

    /**
     * When the source position list changes, propogate the exact same changes for the positions list.
     */
    @Override
    public void listChanged(ListEvent<FeedbackPosition> listChanges) {
        updates.forwardEvent(listChanges);
    }

    @Override
    protected boolean isWritable() {
        return false;
    }

}
