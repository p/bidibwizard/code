package org.bidib.wizard.mvc.firmware.controller.listener;

public interface FirmwareControllerListener {
    void close();
}
