package org.bidib.wizard.mvc.dmx.view.scenery;

import com.jgoodies.binding.beans.Model;

public class ApplyButtonEnabledModel extends Model {
    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_BUTTON_ENABLED = "buttonEnabled";

    public static final String PROPERTY_MODEL_BUFFERING = "modelBuffering";

    private boolean modelValid;

    private boolean modelBuffering;

    private boolean buttonEnabled;

    /**
     * @return the modelValid
     */
    public boolean isModelValid() {
        return modelValid;
    }

    /**
     * @param modelValid
     *            the modelValid to set
     */
    public void setModelValid(boolean modelValid) {
        this.modelValid = modelValid;

        setButtonEnabled(modelBuffering && this.modelValid);
    }

    /**
     * @return the modelBuffering
     */
    public boolean isModelBuffering() {
        return modelBuffering;
    }

    /**
     * @param modelBuffering
     *            the modelBuffering to set
     */
    public void setModelBuffering(boolean modelBuffering) {
        this.modelBuffering = modelBuffering;

        setButtonEnabled(this.modelBuffering && modelValid);
    }

    /**
     * @return the buttonEnabled
     */
    public boolean isButtonEnabled() {
        return buttonEnabled;
    }

    /**
     * @param buttonEnabled
     *            the buttonEnabled to set
     */
    public void setButtonEnabled(boolean buttonEnabled) {
        boolean oldValue = this.buttonEnabled;

        this.buttonEnabled = buttonEnabled;

        firePropertyChange(PROPERTY_BUTTON_ENABLED, oldValue, buttonEnabled);
    }
}
