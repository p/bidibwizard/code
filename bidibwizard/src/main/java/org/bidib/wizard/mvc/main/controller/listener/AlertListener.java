package org.bidib.wizard.mvc.main.controller.listener;

import com.jidesoft.alert.Alert;

public interface AlertListener {

    public enum AlertAction {
        DEVICE_ADDED, DEVICE_REMOVED;
    }

    /**
     * An alert was added.
     * 
     * @param alert
     *            the alert
     * @param alertAction
     *            the alert action
     */
    void alertAdded(final Alert alert, AlertAction alertAction);

    /**
     * An alert was removed.
     * 
     * @param alert
     *            the alert
     */
    void alertRemoved(final Alert alert);
}
