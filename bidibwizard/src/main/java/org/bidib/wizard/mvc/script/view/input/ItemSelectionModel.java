package org.bidib.wizard.mvc.script.view.input;

import com.jgoodies.binding.beans.Model;

public class ItemSelectionModel<T> extends Model {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_SELECTED_ITEM = "selectedItem";

    private T selectedItem;

    /**
     * @return the selectedAccessory
     */
    public T getSelectedItem() {
        return selectedItem;
    }

    /**
     * @param selectedItem
     *            the selectedItem to set
     */
    public void setSelectedItem(T selectedItem) {
        T oldValue = this.selectedItem;

        this.selectedItem = selectedItem;
        firePropertyChange(PROPERTY_SELECTED_ITEM, oldValue, selectedItem);
    }
}
