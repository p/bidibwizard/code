package org.bidib.wizard.mvc.main.view.table;

import java.awt.Color;
import java.awt.Component;

import org.jdesktop.swingx.decorator.AbstractHighlighter;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The RgbColorHighlighter is only applied if a color is available.
 * 
 */
public class RgbColorHighlighter extends AbstractHighlighter {
    private static final Logger LOGGER = LoggerFactory.getLogger(RgbColorHighlighter.class);

    public RgbColorHighlighter() {
    }

    @Override
    protected Component doHighlight(Component renderer, ComponentAdapter adapter) {

        applyBackground(renderer, adapter);

        return renderer;
    }

    @Override
    protected boolean canHighlight(Component component, ComponentAdapter adapter) {
        Object value = adapter.getFilteredValueAt(adapter.row, adapter.column);

        if (value instanceof Color /* && component instanceof ColorRenderer */) {
            // only use this highlighter if the Color is processed.
            return true;
        }
        return false;
    }

    protected void applyBackground(Component renderer, ComponentAdapter adapter) {

        Object value = adapter.getFilteredValueAt(adapter.row, adapter.column);

        Color background = (Color) value;
        if (background != null) {
            if (adapter.isSelected()) {
                background = background.darker();
            }
            LOGGER.info("Set background, row: {}, column: {}, color: {}", adapter.row, adapter.column, background);
            renderer.setBackground(background);
        }
    }

}
