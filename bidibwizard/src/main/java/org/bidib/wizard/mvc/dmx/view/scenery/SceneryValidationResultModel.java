package org.bidib.wizard.mvc.dmx.view.scenery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.util.DefaultValidationResultModel;

public class SceneryValidationResultModel extends DefaultValidationResultModel {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(SceneryValidationResultModel.class);

    public static final String PROPERTY_VALID_STATE = "validState";

    public static final String PROPERTY_VALID_STATE_NO_WARN_OR_ERRORS = "validStateNoWarnOrErrors";

    private boolean validState;

    private boolean validStateNoWarnOrErrors;

    public SceneryValidationResultModel() {
    }

    @Override
    public void setResult(ValidationResult newResult) {

        boolean oldValidState = getResult().isEmpty();
        boolean oldValidStateNoWarnErrors = !(getResult().hasWarnings() || getResult().hasErrors());

        super.setResult(newResult);

        boolean newValidState = newResult.isEmpty();
        boolean newValidStateNoWarnErrors = !(newResult.hasWarnings() || newResult.hasErrors());

        LOGGER
            .debug(
                "Set the newValidState: {}, oldValidState: {}, newValidStateNoWarnErrors: {}, oldValidStateNoWarnErrors: {}",
                newValidState, oldValidState, newValidStateNoWarnErrors, oldValidStateNoWarnErrors);

        validState = newValidState;
        validStateNoWarnOrErrors = newValidStateNoWarnErrors;

        firePropertyChange(PROPERTY_VALID_STATE, oldValidState, newValidState);
        firePropertyChange(PROPERTY_VALID_STATE_NO_WARN_OR_ERRORS, oldValidStateNoWarnErrors, newValidStateNoWarnErrors);
    }

    public boolean getValidState() {
        return validState;
    }

    public boolean getValidStateNoWarnOrErrors() {
        return validStateNoWarnOrErrors;
    }
};
