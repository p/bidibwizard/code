package org.bidib.wizard.mvc.dmx.model;

import java.util.List;

import org.bidib.wizard.mvc.main.model.BacklightPort;

public interface DmxEnvironmentProvider {

    /**
     * @return the configured backlightPorts
     */
    List<BacklightPort> getBacklightPorts();

    /**
     * @return the configured lightPorts
     */
    List<DmxLightPort> getLightPorts();

    /**
     * @return the configured dmxChannels
     */
    List<DmxChannel> getDmxChannels();
}
