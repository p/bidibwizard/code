package org.bidib.wizard.mvc.main.model;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.LoadTypeEnum;
import org.bidib.jbidibc.core.enumeration.PortConfigKeys;
import org.bidib.wizard.comm.SwitchPortStatus;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.main.model.listener.PortListener;
import org.bidib.wizard.mvc.main.model.listener.SwitchPairPortListener;
import org.bidib.wizard.mvc.main.view.table.listener.ButtonListener;
import org.bidib.wizard.utils.PortUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SwitchPairPortTableModel
    extends SimplePortTableModel<SwitchPortStatus, SwitchPairPort, SwitchPairPortListener<SwitchPortStatus>>
    implements ButtonListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(SwitchPairPortTableModel.class);

    private static final long serialVersionUID = 1L;

    public static final int COLUMN_LABEL = 0;

    public static final int COLUMN_SWITCH_OFF_TIME = 1;

    public static final int COLUMN_LOAD_TYPE = 2;

    public static final int COLUMN_PORT_IDENTIFIER = 3;

    public static final int COLUMN_STATUS = 4;

    public static final int COLUMN_TEST = 5;

    public static final int COLUMN_PORT_INSTANCE = 6;

    public SwitchPairPortTableModel(final MainModel model) {
        super();

        model.addSwitchPairPortListener(new PortListener<SwitchPortStatus>() {
            @Override
            public void labelChanged(Port<SwitchPortStatus> port, String label) {
            }

            @Override
            public void statusChanged(Port<SwitchPortStatus> port, SwitchPortStatus status) {
                LOGGER.info("The port status has changed, port: {}, port.status: {}", status, port, port.getStatus());
                updatePortStatus(port);
            }

            @Override
            public void configChanged(Port<SwitchPortStatus> port) {
            }
        });
    }

    @Override
    protected int getColumnPortInstance() {
        return COLUMN_PORT_INSTANCE;
    }

    private void updatePortStatus(Port<SwitchPortStatus> port) {
        // the port status is signaled from the node
        for (int row = 0; row < getRowCount(); row++) {
            if (port.equals(getValueAt(row, COLUMN_PORT_INSTANCE))) {

                LOGGER.info("The port state has changed: {}", port.getStatus());
                super.setValueAt(port.getStatus(), row, COLUMN_STATUS);

                // get the opposite status and set it
                SwitchPortStatus oppositeStatus = PortUtils.getOppositeStatus(port.getStatus());

                LOGGER.info("Set the port status, oppositeStatus: {}", oppositeStatus);

                setValueAt(oppositeStatus, row, COLUMN_TEST);
                break;
            }
        }
    }

    @Override
    protected void initialize() {
        columnNames =
            new String[] { Resources.getString(getClass(), "label"), Resources.getString(getClass(), "switchOffTime"),
                Resources.getString(getClass(), "loadType"), Resources.getString(getClass(), "portIdentifier"),
                Resources.getString(getClass(), "status"), Resources.getString(getClass(), "test"), null };
    }

    public void addRow(SwitchPairPort port) {
        if (port != null) {
            Object[] rowData = new Object[columnNames.length];

            rowData[COLUMN_LABEL] = port.toString();
            rowData[COLUMN_SWITCH_OFF_TIME] = port.getSwitchOffTime();
            rowData[COLUMN_LOAD_TYPE] = port.getLoadType();
            rowData[COLUMN_PORT_IDENTIFIER] = port.getPortIdentifier();
            rowData[COLUMN_STATUS] = port.getStatus();

            SwitchPortStatus oppositeStatus = (SwitchPortStatus) PortUtils.getOppositeStatus(port.getStatus());
            rowData[COLUMN_TEST] = oppositeStatus;
            rowData[COLUMN_PORT_INSTANCE] = port;
            addRow(rowData);
        }
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        boolean isEditable = false;
        SwitchPairPort switchPairPort = (SwitchPairPort) getValueAt(row, COLUMN_PORT_INSTANCE);
        switch (column) {
            case COLUMN_LABEL:
                isEditable = true;
                break;
            case COLUMN_STATUS:
                // the status can never be changed.
                isEditable = false;
            case COLUMN_TEST:
                if (switchPairPort.isEnabled()) {
                    isEditable = true;
                }
                break;
            case COLUMN_SWITCH_OFF_TIME:
                if (switchPairPort.isEnabled()
                    && switchPairPort.isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_TICKS)) {
                    isEditable = true;
                }
                break;
            case COLUMN_LOAD_TYPE:
                if (switchPairPort.isEnabled()
                    && switchPairPort.isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_LOAD_TYPE)) {
                    isEditable = true;
                }
                break;
            default:
                break;
        }
        return isEditable;
    }

    @Override
    public Class<?> getColumnClass(int column) {
        switch (column) {
            case COLUMN_LABEL:
                return String.class;
            case COLUMN_PORT_INSTANCE:
            case COLUMN_PORT_IDENTIFIER:
                return Object.class;
            case COLUMN_LOAD_TYPE:
                return LoadTypeEnum.class;
            case COLUMN_STATUS:
                return Object.class;
            case COLUMN_TEST:
                return Object.class;

            default:
                return Object.class;
        }
    }

    @Override
    public void setValueAt(Object value, int row, int column) {
        final Object o = getValueAt(row, COLUMN_PORT_INSTANCE);

        if (o instanceof SwitchPairPort) {
            final SwitchPairPort port = (SwitchPairPort) o;

            switch (column) {
                case COLUMN_LABEL:
                    port.setLabel((String) value);
                    super.setValueAt(port.toString(), row, column);
                    fireLabelChanged(port, port.getLabel());
                    break;
                case COLUMN_SWITCH_OFF_TIME:
                    int switchOffTime = (Integer) value;
                    if (port.getSwitchOffTime() != switchOffTime) {
                        port.setSwitchOffTime(switchOffTime);
                        super.setValueAt(value, row, column);
                        fireValuesChanged(port, PortConfigKeys.BIDIB_PCFG_TICKS);
                    }
                    else {
                        LOGGER.debug("The switchOff time has not been changed.");
                    }
                    break;
                case COLUMN_LOAD_TYPE:
                    LoadTypeEnum loadType = (LoadTypeEnum) value;
                    if (loadType != port.getLoadType()) {
                        port.setLoadType(loadType);
                        super.setValueAt(value, row, column);
                        fireValuesChanged(port, PortConfigKeys.BIDIB_PCFG_LOAD_TYPE);
                    }
                    else {
                        LOGGER.debug("The load type has not been changed.");
                    }
                    break;
                case COLUMN_STATUS:
                    port.setStatus((SwitchPortStatus) value);
                    super.setValueAt(value, row, column);
                    break;
                case COLUMN_TEST:
                    LOGGER.debug("Status of switch port is updated: {}, port: {}", value, port);
                    if (value instanceof SwitchPortStatus) {
                        SwitchPortStatus oppositeStatus = (SwitchPortStatus) value;

                        port.setStatus(oppositeStatus);
                        super.setValueAt(oppositeStatus, row, column);
                    }
                    else {
                        LOGGER.warn("Set an invalid value: {}", value);
                        super.setValueAt(value, row, column);
                    }
                    break;
                default:
                    super.setValueAt(value, row, column);
                    break;
            }
        }
        else {
            super.setValueAt(value, row, column);
        }
    }

    @Override
    public Object getValueAt(int row, int column) {
        switch (column) {
            case COLUMN_PORT_IDENTIFIER:
            case COLUMN_LABEL:
            case COLUMN_TEST:
            case COLUMN_SWITCH_OFF_TIME:
                column = COLUMN_PORT_INSTANCE;
                break;
            case COLUMN_LOAD_TYPE:
                break;
            default:
                break;
        }
        return super.getValueAt(row, column);
    }

    private void fireValuesChanged(SwitchPairPort port, PortConfigKeys... portConfigKeys) {

        LOGGER.info("The values of the port have changed: {}", port);
        for (SwitchPairPortListener<SwitchPortStatus> l : portListeners) {
            l.valuesChanged(port, portConfigKeys);
        }
    }

    @Override
    public void buttonPressed(int row, int column) {
        LOGGER.info("The button was pressed, row: {}, column: {}", row, column);

        // be careful: if we check the column we must evaluate if the SPORT configuration columns are displayed or not
        // ...
        final Object portInstance = getValueAt(row, COLUMN_PORT_INSTANCE);
        if (portInstance instanceof SwitchPairPort) {
            final SwitchPairPort port = (SwitchPairPort) portInstance;
            fireTestButtonPressed(port);
        }
        else {
            LOGGER.warn("The current portInstance is not a SwitchPairPort: {}", portInstance);
        }
    }

    /**
     * Change the port type.
     * 
     * @param portType
     *            the new port type
     * @param port
     *            the port
     */
    public void changePortType(LcOutputType portType, SwitchPairPort port) {

        for (SwitchPairPortListener<SwitchPortStatus> l : portListeners) {
            l.changePortType(portType, port);
        }
    }
}
