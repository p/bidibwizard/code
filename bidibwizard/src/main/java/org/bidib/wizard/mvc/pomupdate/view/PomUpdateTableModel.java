package org.bidib.wizard.mvc.pomupdate.view;

import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.pomupdate.model.Decoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.adapter.AbstractTableAdapter;
import com.jgoodies.binding.list.SelectionInList;

public class PomUpdateTableModel extends AbstractTableAdapter<Decoder> {

    private static final Logger LOGGER = LoggerFactory.getLogger(PomUpdateTableModel.class);

    private static final long serialVersionUID = 1L;

    public static final int COLUMN_ADDRESS = 0;

    public static final int COLUMN_VENDOR = 1;

    public static final int COLUMN_VERSION_MODEL = 2;

    public static final int COLUMN_PREPARE_PROGRESS = 3;

    public static final int COLUMN_PERFORM_PROGRESS = 4;

    private static final String[] COLUMNNAMES = new String[] {
        Resources.getString(PomUpdateTableModel.class, "address"),
        Resources.getString(PomUpdateTableModel.class, "vendor"),
        Resources.getString(PomUpdateTableModel.class, "model"),
        Resources.getString(PomUpdateTableModel.class, "prepareProgess"),
        Resources.getString(PomUpdateTableModel.class, "performProgess") };

    public PomUpdateTableModel(SelectionInList<Decoder> decoderList) {
        super(decoderList, COLUMNNAMES);

        LOGGER.info("Current listModel: {}", getListModel());
    }

    @Override
    public Class<?> getColumnClass(int column) {
        switch (column) {
            case COLUMN_ADDRESS:
            case COLUMN_PREPARE_PROGRESS:
            case COLUMN_PERFORM_PROGRESS:
                return Integer.class;
            default:
                return Object.class;
        }
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        if (column == COLUMN_ADDRESS) {
            return true;
        }
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Decoder decoder = getRow(rowIndex);
        switch (columnIndex) {
            case COLUMN_ADDRESS:
                return decoder.getAddress();
            case COLUMN_VENDOR:
                return decoder.getVendorId();
            case COLUMN_VERSION_MODEL:
                return decoder.getDecoderVersionAndModel();
            case COLUMN_PREPARE_PROGRESS:
                return decoder.getPrepareProgress();
            case COLUMN_PERFORM_PROGRESS:
                return decoder.getPerformProgress();
            default:
                return null;
        }
    }

    @Override
    public void setValueAt(Object value, int row, int column) {
        LOGGER.debug("Set the value, value: {}, row: {}, column: {}", value, row, column);
        Decoder decoder = getRow(row);
        switch (column) {
            case COLUMN_ADDRESS:
                if (value instanceof Integer) {
                    decoder.setAddress((Integer) value);
                }
                break;
            case COLUMN_PREPARE_PROGRESS:
                if (value instanceof Float) {
                    decoder.setPrepareProgress((Float) value);
                }
                break;
            case COLUMN_PERFORM_PROGRESS:
                if (value instanceof Float) {
                    decoder.setPerformProgress((Float) value);
                }
                break;
            default:
                break;
        }
    }

    public void updatePrepareStatus(Decoder decoder, int progress) {
        updateStatus(decoder, progress, COLUMN_PREPARE_PROGRESS);
    }

    public void updatePerformStatus(Decoder decoder, int progress) {
        updateStatus(decoder, progress, COLUMN_PERFORM_PROGRESS);
    }

    private void updateStatus(Decoder decoder, int progress, int column) {
        LOGGER.info("Update status for decoder: {}, progress: {}", decoder, progress);

        int rowCount = getRowCount();
        for (int row = 0; row < rowCount; row++) {
            if (decoder != null) {
                Decoder currentDecoder = getRow(row);
                if (currentDecoder.equals(decoder)) {
                    LOGGER.debug("Found decoder to update in row: {}", row);
                    float p = (float) progress / 100f;
                    setValueAt(p, row, column);
                    fireTableCellUpdated(row, column);

                    break;
                }
            }
            else {
                float p = (float) progress / 100f;
                setValueAt(p, row, column);
                fireTableCellUpdated(row, column);
            }
        }
    }
}
