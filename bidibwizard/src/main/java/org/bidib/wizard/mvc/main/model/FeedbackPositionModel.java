package org.bidib.wizard.mvc.main.model;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.bidib.wizard.mvc.main.controller.FeedbackPositionStatusChangeProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.beans.Model;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;

public class FeedbackPositionModel extends Model implements FeedbackPositionStatusChangeProvider {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(FeedbackPositionModel.class);

    private final int MAX_SIZE = 3000;

    private Node selectedNode;

    private PropertyChangeListener pclFeedbackPositions;

    private EventList<FeedbackPosition> positionsEventList = new BasicEventList<FeedbackPosition>();

    public FeedbackPositionModel() {
        pclFeedbackPositions = new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                LOGGER.debug("Property has changed, evt: {}", evt);
                FeedbackPosition position = (FeedbackPosition) evt.getNewValue();

                positionsEventList.getReadWriteLock().writeLock().lock();
                try {
                    // access list here
                    if (positionsEventList.size() > MAX_SIZE) {
                        int lastIndex = positionsEventList.size() - 1;
                        while (lastIndex > MAX_SIZE) {
                            positionsEventList.remove(0);
                            LOGGER.debug("Removed item.");
                            lastIndex--;
                        }
                    }
                    positionsEventList.add(position);
                }
                finally {
                    positionsEventList.getReadWriteLock().writeLock().unlock();
                }
            }
        };
    }

    @Override
    public Node getSelectedNode() {
        return selectedNode;
    }

    /**
     * @param selectedNode
     *            the selectedNode to set
     */
    public void setSelectedNode(Node selectedNode) {

        if (this.selectedNode != null) {
            // unregister node listener
            LOGGER.info("Remove pcl from previous selected node: {}", selectedNode);
            this.selectedNode.removePropertyChangeListener(Node.PROPERTY_FEEDBACKPOSITIONS, pclFeedbackPositions);
        }

        this.selectedNode = selectedNode;

        if (this.selectedNode != null) {
            // register node listener
            LOGGER.info("Add pcl to currently selected node: {}", selectedNode);
            this.selectedNode.addPropertyChangeListener(Node.PROPERTY_FEEDBACKPOSITIONS, pclFeedbackPositions);
        }

    }

    /**
     * @return the positionsEventList
     */
    public EventList<FeedbackPosition> getPositionsEventList() {
        return positionsEventList;
    }
}
