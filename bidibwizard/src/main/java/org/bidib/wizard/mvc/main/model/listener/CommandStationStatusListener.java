package org.bidib.wizard.mvc.main.model.listener;

import org.bidib.jbidibc.core.enumeration.CommandStationState;
import org.bidib.wizard.mvc.main.model.Node;

public interface CommandStationStatusListener {

    void stateChanged(Node node, CommandStationState status);
}
