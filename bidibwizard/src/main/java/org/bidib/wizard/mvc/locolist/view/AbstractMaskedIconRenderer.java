package org.bidib.wizard.mvc.locolist.view;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.geom.Rectangle2D;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public abstract class AbstractMaskedIconRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = 1L;

    protected int value;

    protected final int mask;

    public AbstractMaskedIconRenderer(int mask) {
        super();
        this.mask = mask;
    }

    @Override
    public Component getTableCellRendererComponent(
        JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

        if (value instanceof Integer) {
            this.value = ((Integer) value).intValue();
        }

        return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        Graphics2D g2 = (Graphics2D) g.create();

        // first determine the size of the chart rendering area...
        Dimension size = getSize();
        Insets insets = getInsets();
        Rectangle2D available =
            new Rectangle2D.Double(insets.left, insets.top, size.getWidth() - insets.left - insets.right,
                size.getHeight() - insets.top - insets.bottom);

        // work out if scaling is required...
        boolean scale = false;
        double drawWidth = available.getWidth();
        double drawHeight = available.getHeight();
        // this.scaleX = 1.0;
        // this.scaleY = 1.0;

        renderImages(g2);
    }

    protected abstract void renderImages(Graphics2D g2);

    @Override
    public String getText() {
        return null;
    }
}
