package org.bidib.wizard.mvc.main.model.listener;

import java.util.Collection;

import org.bidib.wizard.comm.FeedbackPortStatus;
import org.bidib.wizard.mvc.main.model.FeedbackAddressData;
import org.bidib.wizard.mvc.main.model.FeedbackConfidenceData;
import org.bidib.wizard.mvc.main.model.FeedbackDynStateData;
import org.bidib.wizard.mvc.main.model.FeedbackPort;
import org.bidib.wizard.mvc.main.model.Port;

/**
 * The default implementation of the feedback port listener with empty implementations. Use this class as base if you
 * don't need all events.
 */
public abstract class DefaultFeedbackPortListener implements FeedbackPortListener<FeedbackPortStatus> {

    @Override
    public void statusChanged(Port<FeedbackPortStatus> port, FeedbackPortStatus status) {
    }

    @Override
    public void labelChanged(Port<FeedbackPortStatus> port, String label) {
    }

    @Override
    public void speedChanged(FeedbackPort port, int address, int speed) {
    }

    @Override
    public void confidenceChanged(FeedbackPort port, FeedbackConfidenceData confidence) {
    }

    @Override
    public void addressesChanged(FeedbackPort port, Collection<FeedbackAddressData> addresses) {
    }

    @Override
    public void dynStatesChanged(FeedbackPort port, Collection<FeedbackDynStateData> dynStates) {
    }

    @Override
    public void configChanged(Port<FeedbackPortStatus> port) {
    }
}
