package org.bidib.wizard.mvc.common.view.wizard;

import javax.swing.JPanel;

public class JWizardPanel {

    private JWizardComponents wizardComponents;

    private String panelTitle;

    protected JPanel panel;

    public JWizardPanel(JWizardComponents wizardComponents) {
        this(wizardComponents, null);
    }

    public JWizardPanel(JWizardComponents wizardComponents, String title) {
        this.wizardComponents = wizardComponents;
        this.panelTitle = title;
    }

    /**
     * This method is called when the panel is added to JWizardComponents. Do not call this method directly.
     */
    protected void initPanel() {
    }

    public void update() {
    }

    public void next() {
        goNext();
    }

    public void back() {
        goBack();
    }

    public JPanel getPanel() {
        return panel;
    }

    public JWizardComponents getWizardComponents() {
        return wizardComponents;
    }

    public void setWizardComponents(JWizardComponents awizardComponents) {
        wizardComponents = awizardComponents;
    }

    public String getPanelTitle() {
        return panelTitle;
    }

    public void setPanelTitle(String title) {
        panelTitle = title;
    }

    protected boolean goNext() {
        if (wizardComponents.getWizardPanelList().size() > wizardComponents.getCurrentIndex() + 1) {
            wizardComponents.setCurrentIndex(wizardComponents.getCurrentIndex() + 1);
            wizardComponents.updateComponents();
            return true;
        }
        else {
            return false;
        }
    }

    protected boolean goBack() {
        if (wizardComponents.getCurrentIndex() - 1 >= 0) {
            wizardComponents.setCurrentIndex(wizardComponents.getCurrentIndex() - 1);
            wizardComponents.updateComponents();
            return true;
        }
        else {
            return false;
        }
    }

    protected void switchPanel(int panelIndex) {
        getWizardComponents().setCurrentIndex(panelIndex);
        getWizardComponents().updateComponents();
    }

    protected void setBackButtonEnabled(boolean set) {
        wizardComponents.getBackButton().setEnabled(set);
    }

    protected void setNextButtonEnabled(boolean set) {
        wizardComponents.getNextButton().setEnabled(set);
    }

    protected void setFinishButtonEnabled(boolean set) {
        wizardComponents.getFinishButton().setEnabled(set);
    }
}
