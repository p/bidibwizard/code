package org.bidib.wizard.mvc.pom.view.panel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.bidib.jbidibc.core.AddressData;
import org.bidib.jbidibc.core.enumeration.PomOperation;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.common.view.converter.StringConverter;
import org.bidib.wizard.mvc.common.view.validation.PropertyValidationI18NSupport;
import org.bidib.wizard.mvc.pom.model.PomMode;
import org.bidib.wizard.mvc.pom.model.PomProgrammerModel;
import org.bidib.wizard.mvc.pom.view.CurrentAddressBeanModel;
import org.bidib.wizard.mvc.pom.view.command.PomDirectAccessCommand;
import org.bidib.wizard.mvc.pom.view.command.PomOperationCommand;
import org.bidib.wizard.mvc.pom.view.panel.ProgCommandAwareBeanModel.ExecutionType;
import org.bidib.wizard.utils.InputValidationDocument;

import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.binding.value.ConverterValueModel;
import com.jgoodies.binding.value.ValueHolder;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.builder.ButtonBarBuilder;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.ValidationResultModel;
import com.jgoodies.validation.util.PropertyValidationSupport;
import com.jgoodies.validation.view.ValidationComponentUtils;

public class ResetDecoderPanel extends AbstractPomPanel<DirectAccessProgBeanModel> {

    private static final int CV8 = 8;

    private final DirectAccessProgBeanModel directAccessProgBeanModel;

    private ValueModel cvNumberValueModel;

    private ValueModel cvValueValueModel;

    private JTextField cvNumber;

    private JTextField cvValue;

    private InputValidationDocument cvValueDocument;

    private PomValidationResultModel directValidationModel;

    public ResetDecoderPanel(final PomProgrammerModel cvProgrammerModel,
        final CurrentAddressBeanModel currentAddressBeanModel) {
        super(cvProgrammerModel, currentAddressBeanModel);

        directAccessProgBeanModel = new DirectAccessProgBeanModel();
        directAccessProgBeanModel.setCvNumber(CV8);
        directAccessProgBeanModel.setCvValue(8);
        directAccessProgBeanModel.setMode(PomMode.BYTE);
        setProgCommandAwareBeanModel(directAccessProgBeanModel);
    }

    @Override
    protected void createWorkerPanel(DefaultFormBuilder builder, final PomValidationResultModel parentValidationModel) {

        builder.append(new JLabel(Resources.getString(getClass(), "reset-decoder-message")), 7);

        builder.append(Resources.getString(getClass(), "cv-number"));

        cvNumberValueModel =
            new PropertyAdapter<DirectAccessProgBeanModel>(directAccessProgBeanModel,
                DirectAccessProgBeanModel.PROPERTYNAME_CV_NUMBER, true);

        final ValueModel cvNumberConverterModel =
            new ConverterValueModel(cvNumberValueModel, new StringConverter(new DecimalFormat("#")));

        // create the textfield for the CV number
        cvNumber = new JTextField();
        cvNumber.setDocument(new InputValidationDocument(4, InputValidationDocument.NUMERIC));
        cvNumber.setColumns(4);

        // bind manually because we changed the document of the textfield
        Bindings.bind(cvNumber, cvNumberConverterModel, false);
        builder.append(cvNumber);

        // always disabled
        cvNumber.setEditable(false);

        // ValidationComponentUtils.setMandatory(cvNumber, true);
        ValidationComponentUtils.setMessageKey(cvNumber, "validation.cvnumber_key");

        // add a validation model that can trigger a button state with the validState property
        directValidationModel = new PomValidationResultModel(parentValidationModel);

        builder.nextLine();

        builder.append(Resources.getString(getClass(), "cv-value"));

        cvValueValueModel =
            new PropertyAdapter<DirectAccessProgBeanModel>(directAccessProgBeanModel,
                DirectAccessProgBeanModel.PROPERTYNAME_CV_VALUE, true);

        final ValueModel cvValueConverterModel =
            new ConverterValueModel(cvValueValueModel, new StringConverter(new DecimalFormat("#")));

        // create the textfield for the CV value
        cvValue = new JTextField();
        cvValueDocument = new InputValidationDocument(3, InputValidationDocument.NUMERIC);
        cvValue.setDocument(cvValueDocument);
        cvValue.setColumns(3);

        // bind manually because we changed the document of the textfield
        Bindings.bind(cvValue, cvValueConverterModel, false);
        builder.append(cvValue);

        ValidationComponentUtils.setMandatory(cvValue, true);
        ValidationComponentUtils.setMessageKey(cvValue, "validation.cvvalue_key");

        builder.nextLine();

        directAccessProgBeanModel.addPropertyChangeListener(DirectAccessProgBeanModel.PROPERTYNAME_CV_VALUE,
            new PropertyChangeListener() {

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    LOGGER.debug("CV value has changed: {}", directAccessProgBeanModel.getCvValue());
                    triggerValidation();
                }
            });

        writeButtonEnabled = new ValueHolder(false);
        writeButton.setEnabled(false);
        writeButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                fireWrite();
            }
        });

        // prepare the read and write buttons
        JPanel progActionButtons = new ButtonBarBuilder().addGlue().addButton(writeButton).build();
        builder.append(progActionButtons, 7);
    }

    @Override
    protected ValidationResultModel getValidationResultModel() {
        return directValidationModel;
    }

    private ValidationResult validate() {
        PropertyValidationSupport support = new PropertyValidationI18NSupport(cvNumberValueModel, "validation");

        // only CV numbers up to 1023 are valid
        if (directAccessProgBeanModel.getCvNumber() == null) {
            support.addError("cvnumber_key", "not_empty");
        }
        else if (directAccessProgBeanModel.getCvNumber().intValue() < 1
            || directAccessProgBeanModel.getCvNumber().intValue() > 1023) {
            support.addError("cvnumber_key", "invalid_value;min=1,max=1023");
        }

        if (directAccessProgBeanModel.getCvValue() == null) {
            support.addError("cvvalue_key", "not_empty_for_write");
        }
        else if (directAccessProgBeanModel.getCvValue().intValue() < 0
            || directAccessProgBeanModel.getCvValue().intValue() > 255) {
            support.addError("cvvalue_key", "invalid_value;min=0,max=255");
        }

        ValidationResult validationResult = support.getResult();
        LOGGER.info("Prepared validationResult: {}", validationResult);
        return validationResult;
    }

    @Override
    protected void triggerValidation() {
        ValidationResult validationResult = validate();
        directValidationModel.setResult(validationResult);

        // enable or disable the buttons
        LOGGER.info("Set the writeButtonEnabled, validStateNoWarnOrErrors: {}, validState: {}",
            directValidationModel.getValidStateNoWarnOrErrors(), directValidationModel.getValidState());

        writeButtonEnabled.setValue(directValidationModel.getValidState());
    }

    @Override
    protected void disableInputElements() {

        cvValue.setEnabled(false);

        super.disableInputElements();
    }

    @Override
    protected void enableInputElements() {

        cvValue.setEnabled(true);

        // check the validation model ...
        triggerValidation();
    }

    private int prepareByteCvValue() {
        // set integer value from bit values
        int cvValue = (directAccessProgBeanModel.getCvValue() != null ? directAccessProgBeanModel.getCvValue() : 0);
        LOGGER.info("Prepared byte-based cvValue: {}", ByteUtils.byteToHex(cvValue));
        return cvValue;
    }

    private void fireWrite() {
        // disable the input elements
        disableInputElements();

        // perform operation
        LOGGER.info("Prepare the write request for model: {}", directAccessProgBeanModel);

        // clear the executed commands
        directAccessProgBeanModel.getExecutedProgCommands().clear();

        // prepare the list of commands that must be executed
        List<PomOperationCommand<? extends ProgCommandAwareBeanModel>> progCommands =
            directAccessProgBeanModel.getProgCommands();
        progCommands.clear();

        int cvValue = prepareByteCvValue();

        int cvNumber = directAccessProgBeanModel.getCvNumber();
        AddressData addressData =
            new AddressData(currentAddressBeanModel.getDccAddress(), currentAddressBeanModel.getAddressType());
        // new AddressData(currentAddressBeanModel.getAddress(), AddressTypeEnum.LOCOMOTIVE_FORWARD);

        PomOperation operation =
            (PomMode.BIT.equals(directAccessProgBeanModel.getMode()) ? PomOperation.WR_BIT : PomOperation.WR_BYTE);
        directAccessProgBeanModel.setCurrentOperation(operation);
        directAccessProgBeanModel.setExecution(ExecutionType.WRITE);

        progCommands.add(new PomDirectAccessCommand(addressData, operation, cvNumber, cvValue));

        startTimeoutControl(DEFAULT_TIMEOUT);

        fireNextCommand();
    }

    @Override
    protected Object getCurrentOperation() {
        return directAccessProgBeanModel.getCurrentOperation();
    }

}
