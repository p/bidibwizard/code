package org.bidib.wizard.mvc.accessory.model;

import org.bidib.jbidibc.core.enumeration.AccessoryAcknowledge;
import org.bidib.jbidibc.core.enumeration.AccessoryAddressingEnum;
import org.bidib.jbidibc.core.enumeration.TimeBaseUnitEnum;
import org.bidib.jbidibc.core.enumeration.TimingControlEnum;

import com.jgoodies.binding.beans.Model;

public class AccessoryModel extends Model {
    private static final long serialVersionUID = 1L;

    public static final String PROPERTYNAME_DCC_ADDRESS = "dccAddress";

    public static final String PROPERTYNAME_ACCESSORY_ADDRESSING = "accessoryAddressing";

    public static final String PROPERTYNAME_SWITCH_TIME = "switchTime";

    public static final String PROPERTYNAME_TIME_BASE_UNIT = "timeBaseUnit";

    public static final String PROPERTYNAME_TIMING_CONTROL = "timingControl";

    public static final String PROPERTYNAME_ASPECT = "aspect";

    public static final String PROPERTYNAME_ACKNOWLEDGE = "acknowledge";

    private Integer dccAddress;

    private AccessoryAddressingEnum accessoryAddressing = AccessoryAddressingEnum.RCN_213;

    private Integer switchTime = 5;

    private TimeBaseUnitEnum timeBaseUnit = TimeBaseUnitEnum.UNIT_100MS;

    private TimingControlEnum timingControl = TimingControlEnum.OUTPUT_UNIT;

    private Integer aspect;

    private AccessoryAcknowledge acknowledge;

    /**
     * @return the DCC address
     */
    public Integer getDccAddress() {
        return dccAddress;
    }

    /**
     * @param dccAddress
     *            the DCC address to set
     */
    public void setDccAddress(Integer dccAddress) {
        Integer oldValue = this.dccAddress;
        this.dccAddress = dccAddress;

        firePropertyChange(PROPERTYNAME_DCC_ADDRESS, oldValue, dccAddress);
    }

    /**
     * @return the accessoryAddressing
     */
    public AccessoryAddressingEnum getAccessoryAddressing() {
        return accessoryAddressing;
    }

    /**
     * @param accessoryAddressing
     *            the accessoryAddressing to set
     */
    public void setAccessoryAddressing(AccessoryAddressingEnum accessoryAddressing) {
        AccessoryAddressingEnum oldValue = this.accessoryAddressing;
        this.accessoryAddressing = accessoryAddressing;

        firePropertyChange(PROPERTYNAME_ACCESSORY_ADDRESSING, oldValue, accessoryAddressing);
    }

    /**
     * @return the switchTime
     */
    public Integer getSwitchTime() {
        return switchTime;
    }

    /**
     * @param switchTime
     *            the switchTime to set
     */
    public void setSwitchTime(Integer switchTime) {
        Integer oldValue = this.switchTime;
        this.switchTime = switchTime;

        firePropertyChange(PROPERTYNAME_SWITCH_TIME, oldValue, switchTime);
    }

    /**
     * @return the timeBaseUnit
     */
    public TimeBaseUnitEnum getTimeBaseUnit() {
        return timeBaseUnit;
    }

    /**
     * @param timeBaseUnit
     *            the timeBaseUnit to set
     */
    public void setTimeBaseUnit(TimeBaseUnitEnum timeBaseUnit) {
        TimeBaseUnitEnum oldValue = this.timeBaseUnit;
        this.timeBaseUnit = timeBaseUnit;

        firePropertyChange(PROPERTYNAME_TIME_BASE_UNIT, oldValue, timeBaseUnit);
    }

    /**
     * @return the timingControl
     */
    public TimingControlEnum getTimingControl() {
        return timingControl;
    }

    /**
     * @param timingControl
     *            the timingControl to set
     */
    public void setTimingControl(TimingControlEnum timingControl) {
        TimingControlEnum oldValue = this.timingControl;
        this.timingControl = timingControl;

        firePropertyChange(PROPERTYNAME_TIMING_CONTROL, oldValue, timingControl);
    }

    /**
     * @return the aspect
     */
    public Integer getAspect() {
        return aspect;
    }

    /**
     * @param aspect
     *            the aspect to set
     */
    public void setAspect(Integer aspect) {
        Integer oldValue = this.aspect;
        this.aspect = aspect;

        firePropertyChange(PROPERTYNAME_ASPECT, oldValue, aspect);
    }

    /**
     * @return the acknowledge
     */
    public AccessoryAcknowledge getAcknowledge() {
        return acknowledge;
    }

    /**
     * @param acknowledge
     *            the acknowledge to set
     */
    public void setAcknowledge(AccessoryAcknowledge acknowledge) {
        AccessoryAcknowledge oldValue = this.acknowledge;
        this.acknowledge = acknowledge;

        firePropertyChange(PROPERTYNAME_ACKNOWLEDGE, oldValue, acknowledge);
    }

}
