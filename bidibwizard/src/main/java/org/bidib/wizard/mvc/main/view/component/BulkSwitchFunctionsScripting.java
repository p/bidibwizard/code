package org.bidib.wizard.mvc.main.view.component;

import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.script.Scripting;

public interface BulkSwitchFunctionsScripting extends Scripting {

    /**
     * Send a port status action.
     * 
     * @param port
     *            the port number
     * @param portStatus
     *            the port status
     */
    void sendPortStatusAction(int port, BidibStatus portStatus);

    /**
     * Send a port value action.
     * 
     * @param port
     *            the port number
     * @param portValue
     *            the port value
     */
    void sendPortValueAction(int port, int portValue);
}
