package org.bidib.wizard.mvc.pom.view.command;

import java.util.LinkedList;
import java.util.List;

import org.bidib.jbidibc.core.AddressData;
import org.bidib.jbidibc.core.enumeration.PomOperation;
import org.bidib.wizard.mvc.pom.view.panel.ProgCommandAwareBeanModel;

public abstract class PomOperationIfElseCommand<M extends ProgCommandAwareBeanModel> extends PomOperationCommand<M> {

    private List<PomOperationCommand<M>> progCommandsSuccess = new LinkedList<PomOperationCommand<M>>();

    private List<PomOperationCommand<M>> progCommandsFailure = new LinkedList<PomOperationCommand<M>>();

    public PomOperationIfElseCommand(AddressData decoderAddress, PomOperation pomOperation, int cvNumber, int cvValue) {
        super(decoderAddress, pomOperation, cvNumber, cvValue);
    }

    /**
     * @return the progCommandsSuccess
     */
    public List<PomOperationCommand<M>> getProgCommandsSuccess() {
        return progCommandsSuccess;
    }

    /**
     * @param progCommandsSuccess
     *            the progCommandsSuccess to set
     */
    public void setProgCommandsSuccess(List<PomOperationCommand<M>> progCommandsSuccess) {
        this.progCommandsSuccess = progCommandsSuccess;
    }

    /**
     * @param progCommandSuccess
     *            the progCommandSuccess to add
     */
    public void addProgCommandSuccess(PomOperationCommand<M> progCommandSuccess) {
        if (progCommandsSuccess == null) {
            progCommandsSuccess = new LinkedList<>();
        }
        progCommandsSuccess.add(progCommandSuccess);
    }

    /**
     * @return the progCommandsFailure
     */
    public List<PomOperationCommand<M>> getProgCommandsFailure() {
        return progCommandsFailure;
    }

    /**
     * @param progCommandsFailure
     *            the progCommandsFailure to set
     */
    public void setProgCommandsFailure(List<PomOperationCommand<M>> progCommandsFailure) {
        this.progCommandsFailure = progCommandsFailure;
    }

    /**
     * Verify the result.
     * 
     * @return true: expected result, false: result is different from expected result
     */
    public abstract boolean isExpectedResult();

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer(super.toString());
        if (progCommandsSuccess != null) {
            sb.append(",progCommandsSuccess=").append(progCommandsSuccess);
        }
        if (progCommandsFailure != null) {
            sb.append(",progCommandsFailure=").append(progCommandsFailure);
        }
        return sb.toString();
    }
}
