package org.bidib.wizard.mvc.pt.view.panel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import org.bidib.jbidibc.core.enumeration.AddressMode;
import org.bidib.jbidibc.core.enumeration.PtOperation;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.core.utils.CvUtils;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.common.view.converter.StringConverter;
import org.bidib.wizard.mvc.common.view.validation.PropertyValidationI18NSupport;
import org.bidib.wizard.mvc.pt.model.PtProgrammerModel;
import org.bidib.wizard.mvc.pt.view.command.PtAddressModeCommand;
import org.bidib.wizard.mvc.pt.view.command.PtAddressValueCommand;
import org.bidib.wizard.mvc.pt.view.command.PtAddressValueCommand.ValueType;
import org.bidib.wizard.mvc.pt.view.command.PtOperationCommand;
import org.bidib.wizard.mvc.pt.view.command.PtOperationIfElseCommand;
import org.bidib.wizard.utils.InputValidationDocument;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.value.ConverterValueModel;
import com.jgoodies.binding.value.ValueHolder;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.builder.ButtonBarBuilder;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.ValidationResultModel;
import com.jgoodies.validation.util.PropertyValidationSupport;
import com.jgoodies.validation.view.ValidationComponentUtils;

public class AddressPanel extends AbstractPtPanel<AddressProgBeanModel> {
    private final AddressProgBeanModel addressProgBeanModel;

    private ValueModel addressValueModel;

    private JTextField address;

    private InputValidationDocument addressDocument;

    private JComponent[] modeButtons;

    private ValidationResultModel addressValidationModel;

    public AddressPanel(final PtProgrammerModel cvProgrammerModel) {
        super(cvProgrammerModel);
        addressProgBeanModel = new AddressProgBeanModel();
        setProgCommandAwareBeanModel(addressProgBeanModel);
    }

    @Override
    protected void createWorkerPanel(DefaultFormBuilder builder) {

        builder.append(new JLabel(Resources.getString(getClass(), "address-message")), 7);

        // create the panel content
        builder.append(Resources.getString(getClass(), "address-mode"));

        ValueModel modeModel =
            new PropertyAdapter<AddressProgBeanModel>(addressProgBeanModel,
                AddressProgBeanModel.PROPERTYNAME_ADDRESS_MODE, true);
        modeButtons = new JComponent[AddressMode.values().length];
        int index = 0;
        for (AddressMode mode : AddressMode.values()) {

            JRadioButton radio =
                BasicComponentFactory.createRadioButton(modeModel, mode,
                    Resources.getString(AddressMode.class, mode.getKey()));
            modeButtons[index++] = radio;

            // add radio button
            builder.append(radio);
        }

        builder.nextLine();

        builder.append(Resources.getString(getClass(), "address"));

        addressValueModel =
            new PropertyAdapter<AddressProgBeanModel>(addressProgBeanModel, AddressProgBeanModel.PROPERTYNAME_ADDRESS,
                true);

        final ValueModel addressConverterModel =
            new ConverterValueModel(addressValueModel, new StringConverter(new DecimalFormat("#")));

        // create the textfield for the CV number
        address = new JTextField();
        addressDocument = new InputValidationDocument(5, InputValidationDocument.NUMERIC);
        address.setDocument(addressDocument);
        address.setColumns(5);

        // bind manually because we changed the document of the textfield
        Bindings.bind(address, addressConverterModel, false);
        builder.append(address);

        ValidationComponentUtils.setMandatory(address, true);
        ValidationComponentUtils.setMessageKeys(address, "validation.address_key", "validation.address_short_key",
            "validation.address_long_key");

        // add a validation model that can trigger a button state with the validState property
        addressValidationModel = new PtValidationResultModel();

        addressProgBeanModel.addPropertyChangeListener(AddressProgBeanModel.PROPERTYNAME_ADDRESS_MODE,
            new PropertyChangeListener() {

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    LOGGER.debug("Address mode has changed: {}", addressProgBeanModel.getAddressMode());
                    triggerValidation();
                }
            });
        addressProgBeanModel.addPropertyChangeListener(AddressProgBeanModel.PROPERTYNAME_ADDRESS,
            new PropertyChangeListener() {

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    LOGGER.debug("Address has changed: {}", addressProgBeanModel.getAddress());
                    triggerValidation();
                }
            });

        builder.nextLine();

        readButtonEnabled = new ValueHolder(false);

        readButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                fireRead();
            }
        });

        writeButtonEnabled = new ValueHolder(false);
        writeButton.setEnabled(false);
        writeButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                fireWrite();
            }
        });

        // if the model is valid, the write button is enabled.
        PropertyConnector.connect(addressValidationModel, PtValidationResultModel.PROPERTY_VALID_STATE, writeButton,
            "enabled");

        // prepare the read and write buttons
        JPanel progActionButtons = new ButtonBarBuilder().addGlue().addButton(readButton, writeButton).build();
        builder.append(progActionButtons, 7);

    }

    @Override
    protected void doBindButtons() {
        // add bindings for enable/disable the read button
        PropertyConnector.connect(readButtonEnabled, "value", readButton, "enabled");
        PropertyConnector.connect(writeButtonEnabled, "value", writeButton, "enabled");
    }

    @Override
    protected ValidationResultModel getValidationResultModel() {
        return addressValidationModel;
    }

    private static final int MIN_SHORT_ADDRESS = 1;

    private static final int MAX_SHORT_ADDRESS = 112;

    private static final int MIN_LONG_ADDRESS = MAX_SHORT_ADDRESS + 1;

    private static final int MAX_LONG_ADDRESS = 10239;

    private ValidationResult validate() {
        PropertyValidationSupport support = new PropertyValidationI18NSupport(addressProgBeanModel, "validation");

        if (AddressMode.SHORT.equals(addressProgBeanModel.getAddressMode())) {
            // only addresses between 1 and 112 are valid
            if (addressProgBeanModel.getAddress() == null) {
                support.addWarning("address_key", "not_empty_for_write");
            }
            else if (addressProgBeanModel.getAddress().intValue() < MIN_SHORT_ADDRESS
                || addressProgBeanModel.getAddress().intValue() > MAX_SHORT_ADDRESS) {
                support.addError("address_short_key", "invalid_value;min=" + MIN_SHORT_ADDRESS + ",max="
                    + MAX_SHORT_ADDRESS);
            }
        }
        else {
            // only addresses between 113 and 10239 are valid
            if (addressProgBeanModel.getAddress() == null) {
                support.addWarning("address_key", "not_empty_for_write");
            }
            else if (addressProgBeanModel.getAddress().intValue() < MIN_LONG_ADDRESS
                || addressProgBeanModel.getAddress().intValue() > MAX_LONG_ADDRESS) {
                support.addError("address_long_key", "invalid_value;min=" + MIN_LONG_ADDRESS + ",max="
                    + MAX_LONG_ADDRESS);
            }
        }
        ValidationResult validationResult = support.getResult();
        LOGGER.info("Prepared validationResult: {}", validationResult);
        return validationResult;
    }

    @Override
    protected void triggerValidation() {
        ValidationResult validationResult = validate();
        addressValidationModel.setResult(validationResult);
    }

    @Override
    protected void disableInputElements() {
        LOGGER.info("+++ disableInputElements");

        address.setEnabled(false);

        for (JComponent comp : modeButtons) {
            comp.setEnabled(false);
        }

        super.disableInputElements();
    }

    @Override
    protected void enableInputElements() {
        LOGGER.info("+++ enableInputElements");

        address.setEnabled(true);

        for (JComponent comp : modeButtons) {
            comp.setEnabled(true);
        }

        if (addressValueModel.getValue() != null) {
            writeButtonEnabled.setValue(true);
        }
        readButtonEnabled.setValue(true);
    }

    private void fireWrite() {
        // disable the input elements
        disableInputElements();

        // perform operation
        LOGGER.info("Prepare the write request for model: {}", addressProgBeanModel);

        // clear the executed commands
        addressProgBeanModel.getExecutedProgCommands().clear();

        // prepare the list of commands that must be executed, e.g. for long address: WR CV29 B5 V1,
        // WR CV17 V${lowAddr}, WR CV18 V${hiAddr}
        List<PtOperationCommand<? extends ProgCommandAwareBeanModel>> progCommands =
            addressProgBeanModel.getProgCommands();
        progCommands.clear();

        int address = addressProgBeanModel.getAddress();

        // see http://forum.opendcc.de/viewtopic.php?f=50&t=1630&p=16168#p16162
        // lowAddress = 0xC0 | Math.round((address / 256) - 0.5))
        int lowAddress = (int) (192 + Math.round((address / 256) - 0.5));
        int highAddress = address - (lowAddress - 192) * 256;

        switch (addressProgBeanModel.getAddressMode()) {
            case LONG:
                LOGGER.info("Prepare address mode commands for long address.");
                progCommands.add(new PtAddressModeCommand(PtOperation.WR_BIT, 29, CvUtils.preparePtBitCvValue(true, 5,
                    1)));
                progCommands.add(new PtAddressValueCommand(PtOperation.WR_BYTE, 17, ByteUtils
                    .getIntLowByteValue(lowAddress), ValueType.low));
                progCommands.add(new PtAddressValueCommand(PtOperation.WR_BYTE, 18, ByteUtils
                    .getIntLowByteValue(highAddress), ValueType.high));
                break;
            default:
                LOGGER.info("Prepare address mode commands for short address.");
                progCommands.add(new PtAddressModeCommand(PtOperation.WR_BIT, 29, CvUtils.preparePtBitCvValue(true, 5,
                    1)));
                progCommands.add(new PtAddressValueCommand(PtOperation.WR_BYTE, 1, ByteUtils
                    .getIntLowByteValue(address), ValueType.low));
                break;
        }

        fireNextCommand();
    }

    private void fireRead() {
        // disable the input elements
        disableInputElements();

        // clear the address
        addressProgBeanModel.setAddress(null);

        // clear the executed commands
        addressProgBeanModel.getExecutedProgCommands().clear();

        List<PtOperationCommand<? extends ProgCommandAwareBeanModel>> progCommands =
            addressProgBeanModel.getProgCommands();
        progCommands.clear();

        // this is more complicated because we must detect the state of CV 29 bit 5 and then use either CV 1 or
        // CV 17/18 to get the address

        // prepare the read long address command ... and add as failure commands to read short address command
        PtOperationIfElseCommand<AddressProgBeanModel> readLongCommmand =
            new PtAddressModeCommand(PtOperation.RD_BIT, 29, CvUtils.preparePtBitCvValue(false, 5, 1));
        progCommands.add(readLongCommmand);
        List<PtOperationCommand<AddressProgBeanModel>> progCommandsSuccess = new LinkedList<>();
        progCommandsSuccess.add(new PtAddressValueCommand(PtOperation.RD_BYTE, 17, ByteUtils.getLowByte(0),
            ValueType.low));
        progCommandsSuccess.add(new PtAddressValueCommand(PtOperation.RD_BYTE, 18, ByteUtils.getHighByte(0),
            ValueType.high));
        readLongCommmand.setProgCommandsSuccess(progCommandsSuccess);

        // prepare the read short address command
        List<PtOperationCommand<AddressProgBeanModel>> progCommandsFailure = new LinkedList<>();
        PtOperationIfElseCommand<AddressProgBeanModel> readShortCommmand =
            new PtAddressModeCommand(PtOperation.RD_BIT, 29, CvUtils.preparePtBitCvValue(false, 5, 0));
        progCommandsFailure.add(readShortCommmand);
        readShortCommmand.addProgCommandSuccess(new PtAddressValueCommand(PtOperation.RD_BYTE, 1, ByteUtils
            .getLowByte(0), ValueType.low));
        readLongCommmand.setProgCommandsFailure(progCommandsFailure);

        fireNextCommand();
    }

    @Override
    protected Object getCurrentOperation() {
        return addressProgBeanModel.getCurrentOperation();
    }
}
