package org.bidib.wizard.mvc.features.controller;

import java.util.Collection;
import java.util.HashSet;

import javax.swing.JFrame;

import org.bidib.jbidibc.core.Feature;
import org.bidib.wizard.mvc.features.controller.listener.FeaturesControllerListener;
import org.bidib.wizard.mvc.features.model.FeaturesModel;
import org.bidib.wizard.mvc.features.view.FeaturesView;
import org.bidib.wizard.mvc.features.view.listener.FeaturesViewListener;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FeaturesController {
    private static final Logger LOGGER = LoggerFactory.getLogger(FeaturesController.class);

    private final Collection<FeaturesControllerListener> listeners = new HashSet<FeaturesControllerListener>();

    private final JFrame parent;

    private final Node node;

    private final int x;

    private final int y;

    private final FeaturesModel model;

    private final MainModel mainModel;

    private FeaturesView view;

    public FeaturesController(JFrame parent, MainModel mainModel, Node node, int x, int y) {
        this.parent = parent;
        this.mainModel = mainModel;
        this.node = node;
        this.x = x;
        this.y = y;

        model = new FeaturesModel(node);
    }

    public void addFeaturesControllerListener(FeaturesControllerListener listener) {
        listeners.add(listener);
    }

    private void fireClose() {
        for (FeaturesControllerListener listener : listeners) {
            listener.close();
        }
    }

    private void fireReadAll() {
        LOGGER.debug("Read all features.");

        // TODO read the features from the mainModel ...

        for (FeaturesControllerListener listener : listeners) {
            listener.readAll(node);
        }
    }

    /**
     * Write a feature to the node.
     * 
     * @param feature
     *            the feature to write
     */
    private void fireWrite(Feature feature) {
        LOGGER.debug("Write feature to node, feature: {}", feature);
        for (FeaturesControllerListener listener : listeners) {
            listener.write(node, feature);
        }
    }

    public void start() {
        view = new FeaturesView(parent, model, x, y);
        view.addFeaturesViewListener(new FeaturesViewListener() {
            @Override
            public void close() {
                fireClose();
            }

            @Override
            public void write(Collection<Feature> features) {

                // iterate over the features and write the values
                for (Feature feature : features) {
                    // TODO what happens if an error occurs?
                    try {
                        fireWrite(feature);
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Write feature failed.", ex);
                    }
                }
            }
        });

        // initially read all features
        fireReadAll();

        // make the view visible
        view.setVisible(true);
    }

    public void setFeatures(Collection<Feature> features) {
        LOGGER.info("Set the features in the model.");

        model.setFeatures(features);
    }

}
