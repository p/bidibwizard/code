package org.bidib.wizard.mvc.main.view.table;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.table.TableCellRenderer;

import org.bidib.wizard.comm.InputPortStatus;
import org.bidib.wizard.mvc.main.model.InputPort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InputPortTableStatusCellRenderer implements TableCellRenderer {
    private static final Logger LOGGER = LoggerFactory.getLogger(InputPortTableStatusCellRenderer.class);

    private JPanel panel;

    private JLabel label;

    public InputPortTableStatusCellRenderer() {

        panel = new JPanel();
        panel.setOpaque(true);

        GridBagLayout layout = new GridBagLayout();
        GridBagConstraints c = new GridBagConstraints();

        c.anchor = GridBagConstraints.CENTER;
        c.fill = GridBagConstraints.CENTER;
        panel.setLayout(layout);

        label = new JLabel();
        Border b = BorderFactory.createRaisedBevelBorder();

        label.setBorder(b);
        label.setOpaque(true);
        label.setPreferredSize(new Dimension(15, 15));
        panel.add(label, c);
    }

    public Component getTableCellRendererComponent(
        JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

        // TODO check what happens here ...
        InputPortStatus status = null;
        boolean enabled = false;
        if (value instanceof InputPort) {
            InputPort inputPort = (InputPort) value;

            enabled = inputPort.isEnabled();
            label.setEnabled(enabled);
            panel.setEnabled(enabled);
            // panel.setOpaque(!enabled);
            if (enabled) {
                // get the status only if enabled
                status = inputPort.getStatus();
            }
        }
        else {
            // TODO this is the normal operation case, check what happens with feedback ports ...
            LOGGER.warn("Found invalid configuration.");
            // status = (InputPortStatus) value;

            label.setEnabled(false);
            panel.setEnabled(false);
        }

        if (enabled && InputPortStatus.ON.equals(status)) {
            label.setBackground(Color.RED);
        }
        else {
            label.setBackground(Color.LIGHT_GRAY);
        }

        return panel;
    }
}