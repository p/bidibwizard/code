package org.bidib.wizard.mvc.pt.view;

import org.apache.commons.lang.StringUtils;
import org.bidib.jbidibc.core.enumeration.CommandStationProgState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.value.BindingConverter;

/**
 * Converts Values to Strings and vice-versa using a given Format.
 */
public final class CommandStationProgStateConverter implements BindingConverter<CommandStationProgState, Object> {
    private static final Logger LOGGER = LoggerFactory.getLogger(CommandStationProgStateConverter.class);

    // Implementing Abstract Behavior *************************************

    /**
     * Formats the source value and returns a String representation.
     * 
     * @param sourceValue
     *            the source value
     * @return the formatted sourceValue
     */
    @Override
    public Object targetValue(CommandStationProgState sourceValue) {
        if (sourceValue != null) {
            CommandStationProgState commandStationProgState = (CommandStationProgState) sourceValue;
            return commandStationProgState.name();
        }
        return null;
    }

    /**
     * Parses the given String encoding and sets it as the subject's new value. Silently catches {@code ParseException}.
     * 
     * @param targetValue
     *            the value to be converted and set as new subject value
     */
    @Override
    public CommandStationProgState sourceValue(Object targetValue) {
        try {
            if (StringUtils.isNotBlank((String) targetValue)) {
                CommandStationProgState.valueOf((String) targetValue);
            }
        }
        catch (Exception e) {
            LOGGER.warn("Cannot convert the target value to CommandStationProgState: {}", targetValue);
        }
        return null;
    }
}
