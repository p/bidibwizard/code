package org.bidib.wizard.mvc.common.view.renderer;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import org.apache.commons.lang.StringUtils;
import org.bidib.wizard.mvc.main.model.Macro;
import org.bidib.wizard.mvc.main.model.MacroRef;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MacroRefRenderer extends JLabel implements ListCellRenderer<MacroRef> {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(MacroRefRenderer.class);

    private final Macro[] macros;

    public MacroRefRenderer(Macro[] macros) {
        setOpaque(true);
        this.macros = macros;
    }

    public Component getListCellRendererComponent(
        JList<? extends MacroRef> list, MacroRef value, int index, boolean isSelected, boolean cellHasFocus) {

        if (isSelected) {
            setBackground(list.getSelectionBackground());
            setForeground(list.getSelectionForeground());
        }
        else {
            setBackground(list.getBackground());
            setForeground(list.getForeground());
        }

        LOGGER.info("Current macroRef: {}, index: {}", value, index);

        if (value != null) {
            String text = null;
            Macro macro = null;

            // get the macro id from the macroRef
            Integer macroId = value.getId();
            LOGGER.info("Current macroId: {}", macroId);
            if (macroId != null) {
                // search for matching macro
                for (Macro currentMacro : macros) {
                    if (currentMacro.getId() == macroId.intValue()) {
                        LOGGER.debug("Found matching macro: {}", currentMacro);
                        macro = currentMacro;
                        break;
                    }
                }
            }

            LOGGER.info("Matching macro: {}", macro);
            if (macro != null) {
                if (StringUtils.isNotBlank(macro.getLabel())) {
                    // text = macro.getLabel();
                    text = String.format("%1$02d : %2$s", macro.getId(), macro.getLabel());
                }
                else {
                    // text = Resources.getString(Macro.class, "label") + "_" + macro.getId();
                    text = String.format("%1$02d : ", macro.getId());
                }
            }

            LOGGER.info("Current text: {}", text);

            setText(text);
        }
        else {
            setText(null);
        }
        return this;
    }
}
