package org.bidib.wizard.mvc.main.view.panel;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;

import org.apache.commons.collections4.CollectionUtils;
import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.Feature;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.LoadTypeEnum;
import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.comm.SwitchPortStatus;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.dialog.LabelDialog;
import org.bidib.wizard.mvc.common.view.renderer.PortIdentifierTableCellRenderer;
import org.bidib.wizard.mvc.main.controller.SwitchPairPortPanelController;
import org.bidib.wizard.mvc.main.model.ConfigurablePort;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.model.Port;
import org.bidib.wizard.mvc.main.model.SimplePortTableModel;
import org.bidib.wizard.mvc.main.model.SwitchPairPort;
import org.bidib.wizard.mvc.main.model.SwitchPairPortTableModel;
import org.bidib.wizard.mvc.main.model.listener.SwitchPairPortListener;
import org.bidib.wizard.mvc.main.view.menu.listener.PortListMenuListener;
import org.bidib.wizard.mvc.main.view.table.ConfigurablePortComboBoxEditor;
import org.bidib.wizard.mvc.main.view.table.ConfigurablePortComboBoxRenderer;
import org.bidib.wizard.mvc.main.view.table.DefaultPortListMenuListener;
import org.bidib.wizard.mvc.main.view.table.PortComboBoxWithButtonEditor;
import org.bidib.wizard.mvc.main.view.table.PortComboBoxWithButtonRenderer;
import org.bidib.wizard.mvc.main.view.table.PortConfigErrorAwareEditor;
import org.bidib.wizard.mvc.main.view.table.PortConfigErrorAwareRenderer;
import org.bidib.wizard.mvc.main.view.table.PortTable;
import org.bidib.wizard.mvc.main.view.table.PortTicksEditor;
import org.bidib.wizard.mvc.main.view.table.PortTicksRenderer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jidesoft.grid.TableColumnChooser;

public class SwitchPairPortListPanel
    extends SimplePortListPanel<SwitchPortStatus, SwitchPairPort, SwitchPairPortListener<SwitchPortStatus>> {
    private static final Logger LOGGER = LoggerFactory.getLogger(SwitchPairPortListPanel.class);

    private static final long serialVersionUID = 1L;

    private final MainModel mainModel;

    private final class LoadTypeComboBoxRenderer extends ConfigurablePortComboBoxRenderer<LoadTypeEnum> {
        private static final long serialVersionUID = 1L;

        public LoadTypeComboBoxRenderer(LoadTypeEnum[] items, byte... pcfgType) {
            super(SwitchPairPortTableModel.COLUMN_PORT_INSTANCE, items, pcfgType);
        }

        @Override
        protected Object getCurrentValue(ConfigurablePort<?> port) {
            LoadTypeEnum value = ((SwitchPairPort) port).getLoadType();
            if (LoadTypeEnum.UNKNOWN == value) {
                value = null;
            }
            return value;
        }
    }

    public SwitchPairPortListPanel(final SwitchPairPortPanelController controller, MainModel model) {
        super(new SwitchPairPortTableModel(model), model.getSwitchPairPorts(),
            Resources.getString(SwitchPairPortListPanel.class, "emptyTable"));
        mainModel = model;

        mainModel.addSwitchPortListListener(this);

        TableColumn tc = table.getColumnModel().getColumn(SwitchPairPortTableModel.COLUMN_LABEL);
        tc.setCellRenderer(new PortConfigErrorAwareRenderer(SwitchPairPortTableModel.COLUMN_LABEL));
        tc.setCellEditor(new PortConfigErrorAwareEditor(SwitchPairPortTableModel.COLUMN_PORT_INSTANCE));
        tc.setIdentifier(Integer.valueOf(SwitchPairPortTableModel.COLUMN_LABEL));

        tc = table.getColumnModel().getColumn(SwitchPairPortTableModel.COLUMN_SWITCH_OFF_TIME);
        tc.setCellRenderer(new PortTicksRenderer());
        tc.setCellEditor(new PortTicksEditor(0, 255));
        tc.setIdentifier(Integer.valueOf(SwitchPairPortTableModel.COLUMN_SWITCH_OFF_TIME));

        LoadTypeComboBoxRenderer tableCellRendererLoadType =
            new LoadTypeComboBoxRenderer(LoadTypeEnum.getValues(), BidibLibrary.BIDIB_PCFG_LOAD_TYPE);

        LoadTypeEnum[] loadTypes = Arrays.copyOf(LoadTypeEnum.getValues(), LoadTypeEnum.getValues().length - 1);
        TableCellEditor tableCellEditorLoadType =
            new ConfigurablePortComboBoxEditor<LoadTypeEnum>(SwitchPairPortTableModel.COLUMN_PORT_INSTANCE,
                LoadTypeEnum.getValues(), loadTypes);

        tc = table.getColumnModel().getColumn(SwitchPairPortTableModel.COLUMN_LOAD_TYPE);
        tc.setCellRenderer(tableCellRendererLoadType);
        tc.setCellEditor(tableCellEditorLoadType);
        tc.setIdentifier(Integer.valueOf(SwitchPairPortTableModel.COLUMN_LOAD_TYPE));

        tc = table.getColumnModel().getColumn(SwitchPairPortTableModel.COLUMN_PORT_IDENTIFIER);
        tc.setCellRenderer(new PortIdentifierTableCellRenderer());
        tc.setMaxWidth(80);
        tc.setIdentifier(Integer.valueOf(SwitchPairPortTableModel.COLUMN_PORT_IDENTIFIER));

        // Set the status renderer
        tc = table.getColumnModel().getColumn(SwitchPairPortTableModel.COLUMN_STATUS);
        tc.setIdentifier(Integer.valueOf(SwitchPairPortTableModel.COLUMN_STATUS));
        tc.setCellRenderer(new DefaultTableCellRenderer());
        tc.setMaxWidth(80);

        TableColumn buttonColumn = table.getColumnModel().getColumn(SwitchPairPortTableModel.COLUMN_TEST);
        buttonColumn.setIdentifier(Integer.valueOf(SwitchPairPortTableModel.COLUMN_TEST));

        buttonColumn.setCellRenderer(new PortComboBoxWithButtonRenderer<BidibStatus, SwitchPortStatus>(
            table.getActions(SwitchPortStatus.ON), ">") {
            private static final long serialVersionUID = 1L;

            @Override
            protected void setSelectedValue(Port<?> port) {
                SwitchPortStatus oppositeStatus = null;
                if (port != null) {
                    oppositeStatus = (SwitchPortStatus) port.getStatus();
                }
                comboBox.setSelectedItem(oppositeStatus);
            }
        });
        PortComboBoxWithButtonEditor editor =
            new PortComboBoxWithButtonEditor(table.getActions(SwitchPortStatus.ON), ">") {
                private static final long serialVersionUID = 1L;

                @Override
                protected void setSelectedValue(Port<?> port) {
                    SwitchPortStatus oppositeStatus = null;
                    if (port != null) {
                        oppositeStatus = (SwitchPortStatus) port.getStatus();
                    }
                    comboBox.setSelectedItem(oppositeStatus);
                }
            };
        editor.addButtonListener((SwitchPairPortTableModel) tableModel);
        buttonColumn.setCellEditor(editor);

        TableColumnChooser.hideColumn(table, SwitchPairPortTableModel.COLUMN_PORT_INSTANCE);
    }

    @Override
    protected PortTable createPortTable(
        final SimplePortTableModel<SwitchPortStatus, SwitchPairPort, SwitchPairPortListener<SwitchPortStatus>> tableModel,
        String emptyTableText) {

        PortTable portTable = new PortTable(tableModel, emptyTableText) {
            private static final long serialVersionUID = 1L;

            @Override
            public void clearTable() {
            }

            @Override
            protected PortListMenuListener createMenuListener() {
                // create the port list menu
                return new DefaultPortListMenuListener() {
                    @Override
                    public void editLabel() {
                        final int row = getRow(popupEvent.getPoint());
                        if (row > -1) {
                            Object val = getValueAt(row, 0);
                            if (val instanceof Port<?>) {
                                val = ((Port<?>) val).toString();
                            }
                            final Object value = val;
                            if (value instanceof String) {
                                // show the port name editor
                                new LabelDialog((String) value, popupEvent.getXOnScreen(), popupEvent.getYOnScreen()) {
                                    @Override
                                    public void labelChanged(String label) {
                                        setValueAt(label, row, 0);
                                    }
                                };
                            }
                        }
                        else {
                            LOGGER.warn("The row is not available!");
                        }
                    }

                    @Override
                    public void mapPort() {

                        final int row = getRow(popupEvent.getPoint());
                        if (row > -1) {
                            Object val = getValueAt(row, 0);
                            if (val instanceof SwitchPairPort) {
                                SwitchPairPort switchPairPort = (SwitchPairPort) val;
                                LOGGER.info("Change mapping for port: {}", switchPairPort);

                                // confirm switch to switchPair port
                                int result =
                                    JOptionPane.showConfirmDialog(
                                        JOptionPane.getFrameForComponent(SwitchPairPortListPanel.this),
                                        Resources.getString(SwitchPairPortListPanel.class, "switch-port-confirm"),
                                        Resources.getString(SwitchPairPortListPanel.class, "switch-port-title"),
                                        JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
                                if (result == JOptionPane.OK_OPTION) {
                                    LOGGER.info("Change the port to an switch port.");

                                    SwitchPairPortTableModel switchPairPortTableModel =
                                        (SwitchPairPortTableModel) getModel();
                                    switchPairPortTableModel.changePortType(LcOutputType.SWITCHPAIRPORT,
                                        switchPairPort);
                                }
                            }
                        }
                    }
                };
            }

        };

        return portTable;
    }

    @Override
    public void listChanged() {
        LOGGER.info("The port list has changed.");

        super.listChanged();

        boolean hasPortIdentifiers = false;

        List<SwitchPairPort> ports = new LinkedList<>();
        ports.addAll(getPorts());
        synchronized (ports) {
            for (SwitchPairPort port : ports) {
                if (port.isRemappingEnabled()) {
                    hasPortIdentifiers = true;
                    break;
                }
            }
        }

        if (mainModel.getSelectedNode() != null) {
            LOGGER.info("A node is selected.");

            boolean hasSwitchPortConfigTicks = false;
            boolean hasSwitchPortConfigLoadType = false;

            Node node = mainModel.getSelectedNode();
            if (node.getNode().isPortFlatModelAvailable() && CollectionUtils.isNotEmpty(node.getSwitchPairPorts())) {
                LOGGER.info("Check if at least one switch port has the switchPair port config available.");
                for (SwitchPairPort port : node.getSwitchPairPorts()) {
                    if (!hasSwitchPortConfigTicks) {
                        hasSwitchPortConfigTicks = port.isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_TICKS);
                    }
                    if (!hasSwitchPortConfigLoadType) {
                        hasSwitchPortConfigLoadType = port.isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_LOAD_TYPE);
                    }

                    if (hasSwitchPortConfigTicks && hasSwitchPortConfigLoadType) {
                        // found one -> show the column
                        break;
                    }
                }
            }
            else {
                Feature switchPortConfigAvailable =
                    Feature.findFeature(node.getNode().getFeatures(), BidibLibrary.FEATURE_SWITCH_CONFIG_AVAILABLE);
                if (switchPortConfigAvailable != null) {
                    hasSwitchPortConfigTicks = (switchPortConfigAvailable.getValue() > 0);
                }
            }
            LOGGER.info("List has changed, hasPortIdentifiers: {}, hasSwitchPortConfigTicks: {}", hasPortIdentifiers,
                hasSwitchPortConfigTicks);

            // keep the column index of the column to insert in the view
            int viewColumnIndex = SwitchPairPortTableModel.COLUMN_SWITCH_OFF_TIME;

            // show/hide the ticks column
            viewColumnIndex =
                table.setColumnVisible(SwitchPairPortTableModel.COLUMN_SWITCH_OFF_TIME, viewColumnIndex,
                    hasSwitchPortConfigTicks);

            // show/hide the load type column
            viewColumnIndex =
                table.setColumnVisible(SwitchPairPortTableModel.COLUMN_LOAD_TYPE, viewColumnIndex,
                    hasSwitchPortConfigLoadType);

            // show/hide the port identifier column
            viewColumnIndex =
                table.setColumnVisible(SwitchPairPortTableModel.COLUMN_PORT_IDENTIFIER, viewColumnIndex,
                    hasPortIdentifiers);
        }
    }

    @Override
    protected void refreshPorts() {
        LOGGER.info("refresh the ports.");

        Node node = mainModel.getSelectedNode();
        if (node != null) {
            if (node.getNode().isPortFlatModelAvailable()) {
                if (CollectionUtils.isNotEmpty(node.getGenericPorts())) {
                    mainModel.getSwitchPairPorts();
                }
                else {
                    LOGGER.info(
                        "The node supports flat port model but no generic ports are available. Skip get switchPair ports.");
                }
            }
            else {
                mainModel.getSwitchPairPorts();
            }
        }
        LOGGER.info("refresh the switchPair ports has finished.");
    }
}
