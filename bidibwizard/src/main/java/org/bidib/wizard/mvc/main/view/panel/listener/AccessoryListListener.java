package org.bidib.wizard.mvc.main.view.panel.listener;

import org.bidib.wizard.mvc.main.model.Accessory;

public interface AccessoryListListener extends LabelChangedListener<Accessory> {
    void exportAccessory(Accessory accessory);

    void importAccessory(Accessory accessory);

    void reloadAccessory(Accessory accessory);

    void saveAccessory(Accessory accessory);

    void initializeAccessory(Accessory selectedAccessory);
}
