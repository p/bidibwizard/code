package org.bidib.wizard.mvc.main.view.panel;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyEvent;
import java.util.LinkedList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.event.ChangeEvent;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;

import org.apache.commons.collections4.CollectionUtils;
import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.comm.LightPortStatus;
import org.bidib.wizard.comm.SwitchPortStatus;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.dialog.LabelDialog;
import org.bidib.wizard.mvc.common.view.slider.ConfigXAwareSliderRenderer;
import org.bidib.wizard.mvc.common.view.slider.SliderEditor;
import org.bidib.wizard.mvc.common.view.slider.SliderValueChangeListener;
import org.bidib.wizard.mvc.common.view.table.ColorEditor;
import org.bidib.wizard.mvc.common.view.table.ColorRenderer;
import org.bidib.wizard.mvc.main.controller.LightPortPanelController;
import org.bidib.wizard.mvc.main.model.LightPort;
import org.bidib.wizard.mvc.main.model.LightPortTableModel;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.model.Port;
import org.bidib.wizard.mvc.main.model.SimplePortTableModel;
import org.bidib.wizard.mvc.main.model.listener.LightPortListener;
import org.bidib.wizard.mvc.main.view.menu.listener.PortListMenuListener;
import org.bidib.wizard.mvc.main.view.panel.renderer.MappablePortTableCellRenderer;
import org.bidib.wizard.mvc.main.view.table.PortComboBoxWithButtonEditor;
import org.bidib.wizard.mvc.main.view.table.PortComboBoxWithButtonRenderer;
import org.bidib.wizard.mvc.main.view.table.PortConfigErrorAwareEditor;
import org.bidib.wizard.mvc.main.view.table.PortConfigErrorAwareRenderer;
import org.bidib.wizard.mvc.main.view.table.PortTable;
import org.bidib.wizard.utils.NumberRangeEditor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jidesoft.grid.CellStyle;
import com.jidesoft.grid.RowStripeTableStyleProvider;
import com.jidesoft.grid.TableColumnChooser;
import com.jidesoft.grid.TableStyleProvider;

public class LightPortListPanel
    extends SimplePortListPanel<LightPortStatus, LightPort, LightPortListener<LightPortStatus>> {
    private static final Logger LOGGER = LoggerFactory.getLogger(LightPortListPanel.class);

    private static final class RgbColorEditor extends ColorEditor {
        private static final long serialVersionUID = 1L;

        private int row;

        private int column;

        public RgbColorEditor(LightPortTableModel lightPortTableModel) {

            setColorChooserValueUpdate((color) -> {
                LOGGER.info("Update the current color: {}, row: {}, column: {}", color, row, column);

                lightPortTableModel.setValueAt(color, getRow(), getColumn());

            });

        }

        protected int getRow() {
            return row;
        }

        protected int getColumn() {
            return column;
        }

        @Override
        public Component getTableCellEditorComponent(
            JTable table, Object value, boolean isSelected, int row, int column) {
            this.row = row;
            this.column = column;

            return super.getTableCellEditorComponent(table, value, isSelected, row, column);
        }

    }

    private static final long serialVersionUID = 1L;

    private final MainModel mainModel;

    // private boolean hasColorValue;

    private ConfigXAwareSliderRenderer sliderRendererDimmMin;

    private SliderEditor sliderEditorDimmMin;

    private ConfigXAwareSliderRenderer sliderRendererDimmMax;

    private SliderEditor sliderEditorDimmMax;

    private final LightPortPanelController controller;

    public LightPortListPanel(final LightPortPanelController controller, final MainModel model) {
        super(new LightPortTableModel(model), model.getLightPorts(),
            Resources.getString(LightPortListPanel.class, "emptyTable"));
        this.controller = controller;

        mainModel = model;
        mainModel.addLightPortListListener(this);

        TableColumn tc = table.getColumnModel().getColumn(LightPortTableModel.COLUMN_LABEL);
        tc.setCellRenderer(new PortConfigErrorAwareRenderer(LightPortTableModel.COLUMN_LABEL));
        tc.setCellEditor(new PortConfigErrorAwareEditor(LightPortTableModel.COLUMN_PORT_INSTANCE));
        tc.setIdentifier(Integer.valueOf(LightPortTableModel.COLUMN_LABEL));

        tc = table.getColumnModel().getColumn(LightPortTableModel.COLUMN_PWM_MIN);
        tc.setIdentifier(Integer.valueOf(LightPortTableModel.COLUMN_PWM_MIN));
        tc.setCellRenderer(new ConfigXAwareSliderRenderer(0, 255, 10, BidibLibrary.BIDIB_PCFG_LEVEL_PORT_OFF));
        SliderEditor sliderEditor = new SliderEditor(0, 255, 10);
        tc.setCellEditor(sliderEditor);

        final SliderValueChangeListener sliderValueChangeListener = new SliderValueChangeListener() {

            @Override
            public void stateChanged(ChangeEvent e, boolean isAdjusting, int value) {
                // only handle if not adjusting
                // LOGGER.info("The state of the slider has changed, e: {}, value: {}", e, value);

                int editingColumn = table.getEditingColumn();
                int editingRow = table.getEditingRow();

                LOGGER.info("editingColumn: {}, editingRow: {}, value: {}, isAdjusting: {}", editingColumn, editingRow,
                    value, isAdjusting);
            }
        };
        sliderEditor.setSliderValueChangeListener(sliderValueChangeListener);

        tc = table.getColumnModel().getColumn(LightPortTableModel.COLUMN_PWM_MAX);
        tc.setIdentifier(Integer.valueOf(LightPortTableModel.COLUMN_PWM_MAX));
        sliderEditor = new SliderEditor(0, 255, 10);
        tc.setCellRenderer(new ConfigXAwareSliderRenderer(0, 255, 10, BidibLibrary.BIDIB_PCFG_LEVEL_PORT_ON));
        tc.setCellEditor(sliderEditor);

        tc = table.getColumnModel().getColumn(LightPortTableModel.COLUMN_DIM_MIN);
        tc.setIdentifier(Integer.valueOf(LightPortTableModel.COLUMN_DIM_MIN));

        sliderEditorDimmMin = new SliderEditor(1, 255, 10, true);
        tc.setCellEditor(sliderEditorDimmMin);
        sliderRendererDimmMin =
            new ConfigXAwareSliderRenderer(1, 255, 10, true, BidibLibrary.BIDIB_PCFG_DIMM_DOWN,
                BidibLibrary.BIDIB_PCFG_DIMM_DOWN_8_8);
        tc.setCellRenderer(sliderRendererDimmMin);

        tc = table.getColumnModel().getColumn(LightPortTableModel.COLUMN_DIM_MAX);
        tc.setIdentifier(Integer.valueOf(LightPortTableModel.COLUMN_DIM_MAX));
        sliderEditorDimmMax = new SliderEditor(1, 255, 10, true);
        tc.setCellEditor(sliderEditorDimmMax);
        sliderRendererDimmMax =
            new ConfigXAwareSliderRenderer(1, 255, 10, true, BidibLibrary.BIDIB_PCFG_DIMM_UP,
                BidibLibrary.BIDIB_PCFG_DIMM_UP_8_8);
        tc.setCellRenderer(sliderRendererDimmMax);

        // Set the color renderer
        tc = table.getColumnModel().getColumn(LightPortTableModel.COLUMN_RGB);
        tc.setCellRenderer(new ColorRenderer(/* false */));
        tc.setMaxWidth(120);
        tc.setIdentifier(Integer.valueOf(LightPortTableModel.COLUMN_RGB));
        tc.setCellEditor(new RgbColorEditor((LightPortTableModel) tableModel));

        tc = table.getColumnModel().getColumn(LightPortTableModel.COLUMN_TRANSITION_TIME);
        tc.setIdentifier(Integer.valueOf(LightPortTableModel.COLUMN_TRANSITION_TIME));
        tc.setCellEditor(new NumberRangeEditor(0, 0xFFFF));

        tc = table.getColumnModel().getColumn(LightPortTableModel.COLUMN_PORT_IDENTIFIER);
        tc.setCellRenderer(new MappablePortTableCellRenderer());
        tc.setMaxWidth(80);
        tc.setIdentifier(Integer.valueOf(LightPortTableModel.COLUMN_PORT_IDENTIFIER));

        // Set the status renderer
        tc = table.getColumnModel().getColumn(LightPortTableModel.COLUMN_STATUS);
        tc.setIdentifier(Integer.valueOf(LightPortTableModel.COLUMN_STATUS));
        tc.setCellRenderer(new DefaultTableCellRenderer());
        tc.setMinWidth(100);
        tc.setMaxWidth(120);

        TableColumn buttonColumn = table.getColumnModel().getColumn(LightPortTableModel.COLUMN_TEST);
        buttonColumn.setIdentifier(Integer.valueOf(LightPortTableModel.COLUMN_TEST));
        buttonColumn.setMinWidth(180);
        buttonColumn.setMaxWidth(200);

        buttonColumn.setCellRenderer(new PortComboBoxWithButtonRenderer<BidibStatus, SwitchPortStatus>(
            table.getActions(LightPortStatus.ON), ">") {
            private static final long serialVersionUID = 1L;

            @Override
            protected void setSelectedValue(Port<?> port) {
                LightPortStatus oppositeStatus = null;
                if (port != null) {
                    oppositeStatus = (LightPortStatus) port.getStatus();
                }
                comboBox.setSelectedItem(oppositeStatus);
            }
        });
        PortComboBoxWithButtonEditor editor =
            new PortComboBoxWithButtonEditor(table.getActions(LightPortStatus.ON), ">") {
                private static final long serialVersionUID = 1L;

                @Override
                protected void setSelectedValue(Port<?> port) {
                    LightPortStatus oppositeStatus = null;
                    if (port != null) {
                        oppositeStatus = (LightPortStatus) port.getStatus();
                    }
                    comboBox.setSelectedItem(oppositeStatus);
                }
            };
        editor.addButtonListener((LightPortTableModel) tableModel);
        buttonColumn.setCellEditor(editor);

        TableColumnChooser.hideColumn(table, LightPortTableModel.COLUMN_PORT_INSTANCE);

        // force repaint of slider renderers after resize panel
        addComponentListener(new ComponentAdapter() {

            @Override
            public void componentResized(ComponentEvent e) {
                LOGGER.trace("Component is resized: {}", e.getComponent());
                table.repaint();
            }
        });

        // let the left and right key change the slider value
        table.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0),
            "left");
        table.getActionMap().put("left", new AbstractAction() {
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {
                JTable table = (JTable) e.getSource();
                TableCellEditor editor = table.getCellEditor(table.getSelectedRow(), table.getSelectedColumn());

                if (editor instanceof SliderEditor) {
                    table.editCellAt(table.getSelectedRow(), table.getSelectedColumn());
                    ((SliderEditor) editor).stepDown();
                }
            }
        });
        table.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0),
            "right");
        table.getActionMap().put("right", new AbstractAction() {
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {
                JTable table = (JTable) e.getSource();
                TableCellEditor editor = table.getCellEditor(table.getSelectedRow(), table.getSelectedColumn());

                if (editor instanceof SliderEditor) {
                    table.editCellAt(table.getSelectedRow(), table.getSelectedColumn());
                    ((SliderEditor) editor).stepUp();
                }
            }
        });
    }

    @Override
    protected PortTable createPortTable(
        final SimplePortTableModel<LightPortStatus, LightPort, LightPortListener<LightPortStatus>> tableModel,
        String emptyTableText) {

        final PortTable portTable = new PortTable(tableModel, Resources.getString(getClass(), "emptyTable")) {
            private static final long serialVersionUID = 1L;

            @Override
            public void clearTable() {
            }

            @Override
            public TableStyleProvider getTableStyleProvider() {

                return new RowStripeTableStyleProvider(Color.WHITE, new Color(209, 224, 239)) {
                    @Override
                    public CellStyle getCellStyleAt(JTable table, int rowIndex, int columnIndex) {
                        // TODO Auto-generated method stub
                        if (columnIndex == LightPortTableModel.COLUMN_RGB) {
                            CellStyle cs = new CellStyle();

                            Color color = (Color) tableModel.getValueAt(rowIndex, columnIndex);
                            if (color != null) {
                                cs.setBackground(color);
                                return cs;
                            }
                        }
                        return super.getCellStyleAt(table, rowIndex, columnIndex);
                    }
                };
            }

            @Override
            protected boolean isInsertPortsVisible() {
                // light ports supports insert ports
                return true;
            }

            @Override
            protected PortListMenuListener createMenuListener() {
                // create the port list menu
                return new PortListMenuListener() {
                    @Override
                    public void editLabel() {
                        final int row = getRow(popupEvent.getPoint());
                        if (row > -1) {
                            Object val = getValueAt(row, 0);
                            if (val instanceof Port<?>) {
                                val = ((Port<?>) val).toString();
                            }
                            final Object value = val;
                            if (value instanceof String) {
                                // show the port name editor
                                new LabelDialog((String) value, popupEvent.getXOnScreen(), popupEvent.getYOnScreen()) {
                                    @Override
                                    public void labelChanged(String label) {
                                        setValueAt(label, row, 0);
                                    }
                                };
                            }
                        }
                        else {
                            LOGGER.warn("The row is not available!");
                        }
                    }

                    @Override
                    public void mapPort() {

                        final int row = getRow(popupEvent.getPoint());
                        if (row > -1) {
                            Object val = getValueAt(row, 0);
                            if (val instanceof LightPort) {
                                LightPort lightPort = (LightPort) val;
                                LOGGER.info("Change mapping for port: {}", lightPort);

                                // confirm switch to switch port
                                int result =
                                    JOptionPane.showConfirmDialog(
                                        JOptionPane.getFrameForComponent(LightPortListPanel.this),
                                        Resources.getString(LightPortListPanel.class, "switch-port-confirm"),
                                        Resources.getString(LightPortListPanel.class, "switch-port-title"),
                                        JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
                                if (result == JOptionPane.OK_OPTION) {
                                    LOGGER.info("Change the port to an light port.");

                                    LightPortTableModel lightPortTableModel = (LightPortTableModel) getModel();
                                    lightPortTableModel.changePortType(LcOutputType.LIGHTPORT, lightPort);
                                }
                            }
                        }
                    }

                    @Override
                    public void insertPorts() {

                        if (getSelectedRow() > -1) {
                            LightPortTableModel lightPortTableModel = (LightPortTableModel) getModel();
                            controller.insertPorts(getSelectedRow(), lightPortTableModel, portListener);
                        }
                    }

                };
            }

        };

        return portTable;
    }

    @Override
    public void listChanged() {
        LOGGER.info("The port list has changed.");
        super.listChanged();

        boolean hasPortIdentifiers = false;

        List<LightPort> ports = new LinkedList<>();
        ports.addAll(getPorts());
        synchronized (ports) {
            for (LightPort port : ports) {
                if (port.isRemappingEnabled()) {
                    hasPortIdentifiers = true;
                    break;
                }
            }
        }

        boolean hasColorValue = false;
        boolean hasTransitionTime = false;

        if (CollectionUtils.isNotEmpty(ports)) {
            for (LightPort lightPort : ports) {
                if (lightPort.getRgbValue() != null) {
                    hasColorValue = true;
                }
                if (lightPort.getTransitionTime() != null) {
                    hasTransitionTime = true;
                }
            }

            // get the max dimm value
            try {
                LightPort lightPort = ports.get(0);
                if (lightPort != null) {
                    int dimmStretchMin = lightPort.getDimStretchMin();
                    int dimmStretchMax = lightPort.getDimStretchMax();
                    LOGGER.info("Set the values for dimmStretchMin: {}, dimmStretchMax: {}", dimmStretchMin,
                        dimmStretchMax);
                    sliderRendererDimmMin.setMaxValue(dimmStretchMin);
                    sliderEditorDimmMin.setMaxValue(dimmStretchMin);
                    sliderRendererDimmMax.setMaxValue(dimmStretchMax);
                    sliderEditorDimmMax.setMaxValue(dimmStretchMax);
                }
            }
            catch (Exception ex) {
                LOGGER.warn("Get the dimm stretch values failed.", ex);
            }
        }

        LOGGER.info("hasColorValue: {}, hasPortIdentifiers: {}, hasTransitionTime: {}", hasColorValue,
            hasPortIdentifiers, hasTransitionTime);

        // keep the column index of the column to insert in the view
        int viewColumnIndex = LightPortTableModel.COLUMN_RGB;

        // show/hide the RGB column
        viewColumnIndex = table.setColumnVisible(LightPortTableModel.COLUMN_RGB, viewColumnIndex, hasColorValue);

        // show/hide the transition time column
        viewColumnIndex =
            table.setColumnVisible(LightPortTableModel.COLUMN_TRANSITION_TIME, viewColumnIndex, hasTransitionTime);

        // show/hide the port identifiers
        table.setColumnVisible(LightPortTableModel.COLUMN_PORT_IDENTIFIER, viewColumnIndex, hasPortIdentifiers);
    }

    @Override
    protected void refreshPorts() {
        LOGGER.info("refresh the ports.");

        Node node = mainModel.getSelectedNode();
        if (node != null) {
            if (node.getNode().isPortFlatModelAvailable()) {
                if (CollectionUtils.isNotEmpty(node.getGenericPorts())) {
                    mainModel.getLightPorts();
                }
                else {
                    LOGGER.info(
                        "The node supports flat port model but no generic ports are available. Skip get light ports.");
                }
            }
            else {
                mainModel.getLightPorts();
            }
        }
    }
}
