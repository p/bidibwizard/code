package org.bidib.wizard.mvc.pom.view.panel;

public class RailcomProgBeanModel extends ProgCommandAwareBeanModel {
    private static final long serialVersionUID = 1L;

    public static final String PROPERTYNAME_CHANNEL_1 = "channel1";

    public static final String PROPERTYNAME_CHANNEL_2 = "channel2";

    public static final String PROPERTYNAME_CHANNEL_USAGE = "channelUsage";

    public static final String PROPERTYNAME_RAILCOM_PLUS = "railcomPlus";

    private boolean channel1;

    private boolean channel2;

    private boolean channelUsage;

    private boolean railcomPlus;

    /**
     * @return the channel1
     */
    public boolean isChannel1() {
        return channel1;
    }

    /**
     * @param channel1
     *            the channel1 to set
     */
    public void setChannel1(boolean channel1) {
        boolean oldValue = this.channel1;
        this.channel1 = channel1;
        firePropertyChange(PROPERTYNAME_CHANNEL_1, oldValue, channel1);
    }

    /**
     * @return the channel2
     */
    public boolean isChannel2() {
        return channel2;
    }

    /**
     * @param channel2
     *            the channel2 to set
     */
    public void setChannel2(boolean channel2) {
        boolean oldValue = this.channel2;
        this.channel2 = channel2;
        firePropertyChange(PROPERTYNAME_CHANNEL_2, oldValue, channel2);
    }

    /**
     * @return the channelUsage
     */
    public boolean isChannelUsage() {
        return channelUsage;
    }

    /**
     * @param channelUsage
     *            the channelUsage to set
     */
    public void setChannelUsage(boolean channelUsage) {
        boolean oldValue = this.channelUsage;
        this.channelUsage = channelUsage;
        firePropertyChange(PROPERTYNAME_CHANNEL_USAGE, oldValue, channelUsage);
    }

    /**
     * @return the railcomPlus
     */
    public boolean isRailcomPlus() {
        return railcomPlus;
    }

    /**
     * @param railcomPlus
     *            the railcomPlus to set
     */
    public void setRailcomPlus(boolean railcomPlus) {
        boolean oldValue = this.railcomPlus;
        this.railcomPlus = railcomPlus;
        firePropertyChange(PROPERTYNAME_RAILCOM_PLUS, oldValue, railcomPlus);
    }
}
