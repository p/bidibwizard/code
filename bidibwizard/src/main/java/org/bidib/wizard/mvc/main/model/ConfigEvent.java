package org.bidib.wizard.mvc.main.model;

import java.util.Objects;

public class ConfigEvent {

    private int portNumber;

    private Object value;

    /**
     * @param portNumber
     *            the portNumber to set
     * @param value
     *            the value to set
     */
    public ConfigEvent(int portNumber, Object value) {
        this.portNumber = portNumber;
        this.value = value;
    }

    /**
     * @return the portNumber
     */
    public int getPortNumber() {
        return portNumber;
    }

    /**
     * @return the value
     */
    public Object getValue() {
        return value;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ConfigEvent) {
            ConfigEvent other = (ConfigEvent) obj;
            if (other.portNumber != portNumber) {
                return false;
            }

            return Objects.equals(other.value, value);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return portNumber;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("ConfigEvent, portNum: ");
        sb.append(portNumber).append(", value: ").append(value);
        return sb.toString();
    }
}
