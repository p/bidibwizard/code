package org.bidib.wizard.mvc.main.view.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import org.bidib.wizard.comm.CommunicationFactory;
import org.bidib.wizard.comm.listener.CommunicationListener;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.bidib.wizard.mvc.common.model.PreferencesPortType;
import org.bidib.wizard.mvc.main.view.menu.listener.MainMenuListener;
import org.bidib.wizard.mvc.preferences.model.Preferences;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.debug.FormDebugPanel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.DefaultComponentFactory;
import com.jgoodies.forms.layout.FormLayout;

/**
 * EmptyPanel
 * 
 */
public class EmptyPanel {
    private static final Logger LOGGER = LoggerFactory.getLogger(EmptyPanel.class);

    private static final String ENCODED_DIALOG_COLUMN_SPECS = "pref, 3dlu, pref, 3dlu, 20dlu:grow:fill";

    private JPanel content;

    private JLabel infoLabel;

    private JButton configureButton;

    public EmptyPanel() {
        LOGGER.debug("Create new EmptyPanel.");

        final Preferences prefs = Preferences.getInstance();

        // create form builder
        DefaultFormBuilder dialogBuilder = null;
        boolean debugDialog = false;
        if (debugDialog) {
            JPanel panel = new FormDebugPanel();
            dialogBuilder = new DefaultFormBuilder(new FormLayout(ENCODED_DIALOG_COLUMN_SPECS), panel);
        }
        else {
            JPanel panel = new JPanel(new BorderLayout());
            dialogBuilder = new DefaultFormBuilder(new FormLayout(ENCODED_DIALOG_COLUMN_SPECS), panel);
        }
        dialogBuilder.border(Borders.DIALOG);

        JLabel welcomeLabel =
            DefaultComponentFactory.getInstance().createHeaderLabel(
                "<html><b>" + Resources.getString(getClass(), "welcome") + "</b></html>");
        Font labelFont = welcomeLabel.getFont();
        Font welcomeFont = labelFont.deriveFont(Font.BOLD, labelFont.getSize() * 2.0f);
        welcomeLabel.setFont(welcomeFont);
        welcomeLabel.setForeground(Color.BLUE);
        welcomeLabel.setHorizontalAlignment(SwingConstants.LEFT);

        dialogBuilder.append(welcomeLabel, 5);
        dialogBuilder.appendParagraphGapRow();
        dialogBuilder.appendRow("p");
        dialogBuilder.nextLine(2);

        // open preferences button
        configureButton =
            DefaultComponentFactory.getInstance().createButton(
                new AbstractAction(Resources.getString("mvc.main.view.menu.MainMenuBar", "preferences")) {

                    private static final long serialVersionUID = 1L;

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        firePreferences();
                    }

                    @Override
                    public boolean isEnabled() {
                        return true;
                    }
                });

        infoLabel = DefaultComponentFactory.getInstance().createLabel("");

        updatePanelText(prefs);

        infoLabel.setHorizontalAlignment(SwingConstants.LEFT);
        dialogBuilder.append(infoLabel);

        dialogBuilder.append(configureButton);

        content = dialogBuilder.build();

        if (configureButton != null) {
            prefs.addPropertyChangeListener(Preferences.PROPERTY_SELECTED_PORTTYPE, new PropertyChangeListener() {
                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    // listen to changes
                    if (Preferences.PROPERTY_SELECTED_PORTTYPE.equals(evt.getPropertyName())) {
                        LOGGER.info("The selected port type has changed.");

                        updatePanelText(prefs);
                    }
                }
            });
        }

        CommunicationFactory.addCommunicationListener(new CommunicationListener() {

            @Override
            public void opening() {

            }

            @Override
            public void opened(String port) {
                LOGGER.info("Port was opened: {}", port);

                if (!SwingUtilities.isEventDispatchThread()) {
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            setLabelText("<html><b>" + Resources.getString(EmptyPanel.class, "selectNodeFromList")
                                + "</b></html>");
                            configureButton.setVisible(false);
                        }
                    });
                    return;
                }

                setLabelText("<html><b>" + Resources.getString(EmptyPanel.class, "selectNodeFromList") + "</b></html>");
                configureButton.setVisible(false);
            }

            @Override
            public void closed(String port) {
                LOGGER.info("Port was closed: {}", port);

                if (!SwingUtilities.isEventDispatchThread()) {
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            updatePanelText(prefs);
                        }
                    });
                    return;
                }
                updatePanelText(prefs);
            }

            @Override
            public void initialized() {
            }

            @Override
            public void status(String statusText, int displayDuration) {
            }
        });
    }

    public JPanel getContent() {
        return content;
    }

    private void updatePanelText(final Preferences prefs) {

        PreferencesPortType preferencesPortType = prefs.getSelectedPortType();
        if (PreferencesPortType.isSimulation(preferencesPortType)) {
            // change the text on the info label
            infoLabel.setText(
                "<html>" + Resources.getString(EmptyPanel.class, "simulation-connection-configured") + "</html>");

            infoLabel.setForeground(Color.RED);
            configureButton.setVisible(true);
        }
        else if (PreferencesPortType.isValidConnectionConfiguration(preferencesPortType)) {
            infoLabel.setText("<html><b>" + Resources.getString(EmptyPanel.class, "connect-to-system") + "</b></html>");

            infoLabel.setForeground(Color.BLACK);
            configureButton.setVisible(false);
        }
        else {
            // change the text on the info label
            infoLabel.setText("<html><b>" + Resources.getString(EmptyPanel.class, "no-connection-to-system-configured")
                + "</b></html>");

            infoLabel.setForeground(Color.RED);
            configureButton.setVisible(true);
        }
    }

    private void setLabelText(String labelText) {
        LOGGER.info("Set the label text: {}", labelText);

        infoLabel.setText(labelText);
        infoLabel.setForeground(Color.BLACK);
    }

    public String getName() {
        return Resources.getString(getClass(), "name");
    }

    private void firePreferences() {

        MainMenuListener mainMenuListener =
            DefaultApplicationContext.getInstance().get(DefaultApplicationContext.KEY_MAINMENU_LISTENER,
                MainMenuListener.class);

        if (mainMenuListener != null) {
            mainMenuListener.preferences();
        }
        else {
            LOGGER.warn("No main menu listener available.");
        }
    }

}
