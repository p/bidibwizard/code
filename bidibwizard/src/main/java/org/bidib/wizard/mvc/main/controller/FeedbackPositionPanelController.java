package org.bidib.wizard.mvc.main.controller;

import javax.swing.SwingUtilities;

import org.bidib.wizard.mvc.main.model.FeedbackPositionModel;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.model.listener.DefaultNodeListListener;
import org.bidib.wizard.mvc.main.model.listener.NodeListListener;
import org.bidib.wizard.mvc.main.view.panel.FeedbackPositionListPanel;
import org.bidib.wizard.mvc.main.view.panel.listener.TabVisibilityListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FeedbackPositionPanelController {

    private static final Logger LOGGER = LoggerFactory.getLogger(FeedbackPositionPanelController.class);

    private final MainModel mainModel;

    private final NodeListListener nodeListListener;

    private FeedbackPositionModel feedbackPositionModel;

    private FeedbackPositionListPanel feedbackPositionListPanel;

    private final TabVisibilityListener tabVisibilityListener;

    public FeedbackPositionPanelController(final MainModel mainModel,
        final TabVisibilityListener tabVisibilityListener) {
        this.mainModel = mainModel;
        this.tabVisibilityListener = tabVisibilityListener;

        feedbackPositionModel = new FeedbackPositionModel();

        nodeListListener = new DefaultNodeListListener() {
            @Override
            public void nodeChanged() {
                LOGGER.info("The node has changed.");

                Node selectedNode = mainModel.getSelectedNode();
                // update the selected node
                feedbackPositionModel.setSelectedNode(selectedNode);

                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        LOGGER.info("Notify that the selected port was changed.");
                        if (feedbackPositionListPanel != null) {
                            feedbackPositionListPanel.listChanged();
                        }
                        else {
                            LOGGER.warn("The feedbackPortListPanel is not available.");
                        }
                    }
                });
            }
        };
        this.mainModel.addNodeListListener(nodeListListener);

    }

    public Node getSelectedNode() {
        return feedbackPositionModel.getSelectedNode();
    }

    public FeedbackPositionListPanel createFeedbackPositionListPanel() {
        LOGGER.info("Create new feedbackPositionListPanel.");
        feedbackPositionListPanel =
            new FeedbackPositionListPanel(this, feedbackPositionModel, mainModel, tabVisibilityListener);

        return feedbackPositionListPanel;
    }

}
