package org.bidib.wizard.mvc.ping.view;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import org.bidib.wizard.mvc.ping.model.NodePingState;

public class PingStateCellRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = 1L;

    public PingStateCellRenderer() {

    }

    public Component getTableCellRendererComponent(
        JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        // provide value == null because default processing is setText(value.toString())
        super.getTableCellRendererComponent(table, null, isSelected, hasFocus, row, column);

        if (value instanceof NodePingState) {
            NodePingState pingState = (NodePingState) value;

            // TODO
            // Icon icon = boosterStateIcons.get(boosterState);
            // setIcon(icon);
            // TODO change to use icon only
            setText(pingState.toString());

            setToolTipText(pingState.toString());
        }
        else {
            setIcon(null);
            setToolTipText(null);
        }

        return this;
    }
}
