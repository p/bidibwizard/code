package org.bidib.wizard.mvc.dmx.view.panel;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicLong;

import org.bidib.wizard.mvc.main.model.Port;
import org.bidib.wizard.mvc.main.model.function.MacroFunction;
import org.jfree.chart.util.ParamChecks;
import org.jfree.data.general.SeriesChangeEvent;
import org.jfree.data.general.SeriesException;
import org.jfree.data.xy.XYDataItem;
import org.jfree.data.xy.XYSeries;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DmxSeries extends XYSeries {
    private static final Logger LOGGER = LoggerFactory.getLogger(DmxSeries.class);

    private static final long serialVersionUID = 1L;

    public static final int BRIGHTNESS_MIN = 0;

    public static final int BRIGHTNESS_MAX = 100;

    private static AtomicLong UNIQUEID = new AtomicLong(0);

    public DmxSeries(Comparable<String> key, boolean autoSort) {
        super(key, autoSort, /* false */true /* allowDuplicateXValues */);
    }

    public DmxDataItem getNewDmxDataItem(double x, double y) {
        // generate a new uniqueId
        long uniqueId = UNIQUEID.incrementAndGet();
        return new DmxDataItem(x, y, uniqueId, (Port<?>) null);
    }

    public void add(Number x, Number y, Port<?> port) {
        if (y.intValue() < BRIGHTNESS_MIN) {
            LOGGER.info("Y value is out of range!");
            y = BRIGHTNESS_MIN;
        }
        else if (y.intValue() > BRIGHTNESS_MAX) {
            LOGGER.info("Y value is out of range!");
            y = BRIGHTNESS_MAX;
        }

        if (x.intValue() < 0) {
            x = 0;
        }

        // generate a new uniqueId
        long uniqueId = UNIQUEID.incrementAndGet();
        super.add(new DmxDataItem(x, y, uniqueId, port));
    }

    public DmxDataItem add(int x, int y, Port<?> port) {
        if (y < BRIGHTNESS_MIN) {
            LOGGER.info("Y value is out of range!");
            y = BRIGHTNESS_MIN;
        }
        else if (y > BRIGHTNESS_MAX) {
            LOGGER.info("Y value is out of range!");
            y = BRIGHTNESS_MAX;
        }

        if (x < 0) {
            x = 0;
        }
        // generate a new uniqueId
        long uniqueId = UNIQUEID.incrementAndGet();
        DmxDataItem dmxDataItem = new DmxDataItem(x, y, uniqueId, port);
        super.add(dmxDataItem);

        return dmxDataItem;
    }

    public void add(int x, int y, MacroFunction macro) {
        if (y < BRIGHTNESS_MIN) {
            LOGGER.info("Y value is out of range!");
            y = BRIGHTNESS_MIN;
        }
        else if (y > BRIGHTNESS_MAX) {
            LOGGER.info("Y value is out of range!");
            y = BRIGHTNESS_MAX;
        }

        if (x < 0) {
            x = 0;
        }
        // generate a new uniqueId
        long uniqueId = UNIQUEID.incrementAndGet();
        super.add(new DmxDataItem(x, y, uniqueId, macro));
    }

    public void add(double x, double y, Port<?> port) {
        if (y < BRIGHTNESS_MIN) {
            LOGGER.info("Y value is out of range!");
            y = BRIGHTNESS_MIN;
        }
        else if (y > BRIGHTNESS_MAX) {
            LOGGER.info("Y value is out of range!");
            y = BRIGHTNESS_MAX;
        }

        if (x < 0) {
            x = 0;
        }
        // generate a new uniqueId
        long uniqueId = UNIQUEID.incrementAndGet();
        super.add(new DmxDataItem(x, y, uniqueId, port));
    }

    public XYDataItem addOrUpdate(Number x, Number y, DmxDataItem item) {
        if (y.intValue() < BRIGHTNESS_MIN) {
            LOGGER.info("Y value is out of range!");
            y = BRIGHTNESS_MIN;
        }
        else if (y.intValue() > BRIGHTNESS_MAX) {
            LOGGER.info("Y value is out of range!");
            y = BRIGHTNESS_MAX;
        }

        if (x.intValue() < 0) {
            x = 0;
        }

        // TODO check if this is really clever ... maybe better solution is to overwrite the method ...
        item.setTimeOffset(x.intValue());
        item.setBrightness(y.intValue());
        DmxDataItem overwritten = (DmxDataItem) addOrUpdate(item);

        // if (overwritten != null) {
        // int index = indexOf(overwritten.getX());
        // DmxDataItem original = (DmxDataItem) data.get(index);
        // // set the port
        // original.setPort(port);
        // overwritten.setPort(port);
        //
        // // fireSeriesChanged();
        // }

        return overwritten;
    }

    // public XYDataItem addOrUpdate(Number x, Number y, MacroFunction macro) {
    // if (y.intValue() < BRIGHTNESS_MIN) {
    // LOGGER.info("Y value is out of range!");
    // y = BRIGHTNESS_MIN;
    // }
    // else if (y.intValue() > BRIGHTNESS_MAX) {
    // LOGGER.info("Y value is out of range!");
    // y = BRIGHTNESS_MAX;
    // }
    //
    // if (x.intValue() < 0) {
    // x = 0;
    // }
    //
    // // TODO check if this is really clever ... maybe better solution is to overwrite the method ...
    // item.setTimeOffset(x.intValue());
    // item.setBrightness(y.intValue());
    // DmxDataItem overwritten = (DmxDataItem) addOrUpdate(item);
    //
    // // DmxDataItem overwritten = (DmxDataItem) addOrUpdate(new DmxDataItem(x.intValue(), y.intValue(), macro));
    // //
    // // if (overwritten != null) {
    // // int index = indexOf(overwritten.getX());
    // // DmxDataItem original = (DmxDataItem) data.get(index);
    // // // set the port
    // // original.setMacro(macro);
    // // overwritten.setMacro(macro);
    // //
    // // // fireSeriesChanged();
    // // }
    //
    // return overwritten;
    // }

    public int indexOf(Number x) {

        LOGGER.error("Called old indexOf!!!!");

        if (getAutoSort()) {
            return Collections.binarySearch(this.data, new DmxDataItem(x, null, -1, (Port<?>) null));
        }
        else {

            for (int i = 0; i < this.data.size(); i++) {
                DmxDataItem item = (DmxDataItem) this.data.get(i);
                // LOGGER.info("indexOf, i: {}, x: {}, current timeOffset: {}", i, x.intValue(), item.getTimeOffset());
                if (item.getTimeOffset() == x.intValue()) {
                    return i;
                }
            }
            return -1;
        }
    }

    public int indexOf(Number x, final long uniqueId) {
        if (getAutoSort()) {
            int index = Collections.binarySearch(this.data, new DmxDataItem(x, null, uniqueId, (Port<?>) null));
            LOGGER.info("indexOf, index: {}", index);
            return index;
        }
        else {

            LOGGER.info("Search index for unique id: {}", uniqueId);

            for (int i = 0; i < this.data.size(); i++) {
                DmxDataItem item = (DmxDataItem) this.data.get(i);
                // LOGGER.info("indexOf, i: {}, x: {}, current timeOffset: {}", i, x.intValue(), item.getTimeOffset());
                // if (item.getTimeOffset() == x.intValue()) {
                if (item.getUniqueId() == uniqueId) {
                    LOGGER.info("The unique id matches: {}", item);
                    return i;
                }
            }
            return -1;
        }
    }

    public void moveItem(DmxDataItem dmxDataItem, int newX, int newY) {
        // search the data item
        if (dmxDataItem != null) {
            int index = indexOf(dmxDataItem.getX(), dmxDataItem.getUniqueId());
            LOGGER.info("moveItem, index of item: {}, dmxDataItem: {}", index, dmxDataItem);
            if (index > -1) {

                {
                    DmxDataItem removed = (DmxDataItem) data.remove(index);
                }
                // DmxDataItem item = (DmxDataItem) data.get(index);
                // set the new x and y value
                // removed.setTimeOffset(newX);
                // removed.setBrightness(newY);

                dmxDataItem.setTimeOffset(newX);
                dmxDataItem.setBrightness(newY);

                index = Collections.binarySearch(this.data, dmxDataItem);

                LOGGER.info("binary search returned index: {}", index);
                if (index < 0) {
                    data.add(-index - 1, dmxDataItem);
                }
                else if (getAllowDuplicateXValues()) {
                    for (int size = data.size(); index < size && dmxDataItem.compareTo(data.get(index)) == 0; index++) {
                        ;
                    }
                    LOGGER.info("index after iterate over existing: {}", index);
                    if (index < data.size()) {
                        data.add(index, dmxDataItem);
                    }
                    else {
                        data.add(dmxDataItem);
                    }
                }
                else {
                    throw new SeriesException("X-value already exists.");
                }

                // dmxDataItem.setTimeOffset(newX);
                // dmxDataItem.setBrightness(newY);

                doUpdateBoundsForAddedItem(dmxDataItem);

                fireSeriesChanged();

                // DmxDataItem item = (DmxDataItem) remove(index);
                // // this is the original data item
                // LOGGER.debug("Found original item to update: {}", item);
                //
                // double prevX = dmxDataItem.getXValue();
                // // double prevY = dmxDataItem.getYValue();
                //
                // // set the new x and y value
                // dmxDataItem.setTimeOffset(newX);
                // dmxDataItem.setBrightness(newY);
                // try {
                // add(dmxDataItem);
                // }
                // catch (SeriesException ex) {
                // LOGGER.warn("Move item to new position failed, restore old position.", ex);
                // // restore the previous x and y value
                // dmxDataItem.setTimeOffset((int) prevX);
                // dmxDataItem.setBrightness(newY);
                // // dmxDataItem.setY(Double.valueOf(prevY));
                // add(dmxDataItem);
                // }
            }
        }
    }

    /**
     * Adds or updates an item in the series and sends a {@link SeriesChangeEvent} to all registered listeners.
     * 
     * @param item
     *            the data item (<code>null</code> not permitted).
     * 
     * @return A copy of the overwritten data item, or <code>null</code> if no item was overwritten.
     * 
     * @since 1.0.14
     */
    public XYDataItem addOrUpdate(DmxDataItem item) {
        ParamChecks.nullNotPermitted(item, "item");
        if (getAllowDuplicateXValues()) {
            add(item);
            return null;
        }

        // if we get to here, we know that duplicate X values are not permitted
        XYDataItem overwritten = null;
        int index = indexOf(item.getX(), item.getUniqueId());
        if (index >= 0) {
            DmxDataItem existing = (DmxDataItem) this.data.get(index);
            overwritten = (XYDataItem) existing.clone();
            // figure out if we need to iterate through all the y-values
            boolean iterate = false;
            double oldY = existing.getYValue();
            if (!Double.isNaN(oldY)) {
                iterate = oldY <= getMinY() || oldY >= getMaxY();
            }
            existing.setY(item.getY());
            existing.setTimeOffset(item.getTimeOffset());

            if (iterate) {
                doFindBoundsByIteration();
            }
            else if (item.getY() != null) {
                double yy = item.getY().doubleValue();
                setMinY(minIgnoreNaN(getMinY(), yy));
                setMaxY(maxIgnoreNaN(getMaxY(), yy));
            }
        }
        else {
            // if the series is sorted, the negative index is a result from
            // Collections.binarySearch() and tells us where to insert the
            // new item...otherwise it will be just -1 and we should just
            // append the value to the list...
            item = (DmxDataItem) item.clone();
            if (getAutoSort()) {
                this.data.add(-index - 1, item);
            }
            else {
                this.data.add(item);
            }
            doUpdateBoundsForAddedItem(item);

            // check if this addition will exceed the maximum item count...
            if (getItemCount() > getMaximumItemCount()) {
                XYDataItem removed = (XYDataItem) this.data.remove(0);
                doUpdateBoundsForRemovedItem(removed);
            }
        }
        fireSeriesChanged();
        return overwritten;
    }

    private void doFindBoundsByIteration() {
        try {
            Method method = XYSeries.class.getDeclaredMethod("findBoundsByIteration", null);
            method.setAccessible(true);

            method.invoke(this, null);
        }
        catch (Exception ex) {
            LOGGER.warn("Invoke findBoundsByIteration failed.", ex);
        }
    }

    private void doUpdateBoundsForAddedItem(XYDataItem item) {
        try {
            Method method = XYSeries.class.getDeclaredMethod("updateBoundsForAddedItem", XYDataItem.class);
            method.setAccessible(true);

            method.invoke(this, item);
        }
        catch (Exception ex) {
            LOGGER.warn("Invoke updateBoundsForAddedItem failed.", ex);
        }
    }

    private void doUpdateBoundsForRemovedItem(XYDataItem item) {
        try {
            Method method = XYSeries.class.getDeclaredMethod("updateBoundsForRemovedItem", XYDataItem.class);
            method.setAccessible(true);

            method.invoke(this, item);
        }
        catch (Exception ex) {
            LOGGER.warn("Invoke updateBoundsForRemovedItem failed.", ex);
        }
    }

    /**
     * A function to find the maximum of two values, but ignoring any Double.NaN values.
     * 
     * @param a
     *            the first value.
     * @param b
     *            the second value.
     * 
     * @return The maximum of the two values.
     */
    private double maxIgnoreNaN(double a, double b) {
        if (Double.isNaN(a)) {
            return b;
        }
        if (Double.isNaN(b)) {
            return a;
        }
        return Math.max(a, b);
    }

    /**
     * A function to find the minimum of two values, but ignoring any Double.NaN values.
     * 
     * @param a
     *            the first value.
     * @param b
     *            the second value.
     * 
     * @return The minimum of the two values.
     */
    private double minIgnoreNaN(double a, double b) {
        if (Double.isNaN(a)) {
            return b;
        }
        if (Double.isNaN(b)) {
            return a;
        }
        return Math.min(a, b);
    }

    private void setMinY(double minY) {
        LOGGER.debug("Set the new minY value: {}", minY);
        try {
            Field fieldMinY = XYSeries.class.getDeclaredField("minY");
            fieldMinY.setAccessible(true);

            fieldMinY.set(this, minY);
        }
        catch (Exception ex) {
            LOGGER.warn("Set the minY value failed.", ex);
        }
    }

    private void setMaxY(double maxY) {
        LOGGER.debug("Set the new maxY value: {}", maxY);
        try {
            Field fieldMaxY = XYSeries.class.getDeclaredField("maxY");
            fieldMaxY.setAccessible(true);

            fieldMaxY.set(this, maxY);
        }
        catch (Exception ex) {
            LOGGER.warn("Set the maxY value failed.", ex);
        }
    }

    /**
     * Adds a data item to the series and, if requested, sends a {@link SeriesChangeEvent} to all registered listeners.
     * 
     * @param item
     *            the (x, y) item (<code>null</code> not permitted).
     * @param notify
     *            a flag that controls whether or not a {@link SeriesChangeEvent} is sent to all registered listeners.
     */
    public void add(XYDataItem item, boolean notify) {
        ParamChecks.nullNotPermitted(item, "item");
        item = (XYDataItem) item.clone();
        if (getAutoSort()) {
            int index = Collections.binarySearch(this.data, item);
            if (index < 0) {
                this.data.add(-index - 1, item);
            }
            else {
                if (getAllowDuplicateXValues()) {
                    // need to make sure we are adding *after* any duplicates
                    int size = this.data.size();
                    while (index < size && item.compareTo(this.data.get(index)) == 0) {
                        index++;
                    }
                    if (index < this.data.size()) {
                        this.data.add(index, item);
                    }
                    else {
                        this.data.add(item);
                    }
                }
                else {
                    throw new SeriesException("X-value already exists.");
                }
            }
        }
        else {
            if (!getAllowDuplicateXValues()) {
                // can't allow duplicate values, so we need to check whether
                // there is an item with the given x-value already
                DmxDataItem dmxDataItem = (DmxDataItem) item;
                int index = indexOf(item.getX(), dmxDataItem.getUniqueId());
                if (index >= 0) {
                    throw new SeriesException("X-value already exists.");
                }
            }
            this.data.add(item);
        }
        doUpdateBoundsForAddedItem(item);
        if (getItemCount() > getMaximumItemCount()) {
            XYDataItem removed = (XYDataItem) this.data.remove(0);
            doUpdateBoundsForRemovedItem(removed);
        }
        if (notify) {
            fireSeriesChanged();
        }
    }

}
