package org.bidib.wizard.mvc.main.model;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.UnsupportedEncodingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class XmlLoader {
    private static final Logger LOGGER = LoggerFactory.getLogger(XmlLoader.class);

    private XmlLoader() {
    }

    /*
     * // TODO use this if compiled with JDK 7 ... public static Reader changePackage(Reader reader) { StringBuilder
     * result = new StringBuilder(); BufferedReader buffer = new BufferedReader(reader); String line;
     * 
     * try { while ((line = buffer.readLine()) != null) { line = line.replace("\"mvc.main.model.",
     * "\"org.bidib.wizard.mvc.main.model."); line = line.replace("\"comm.", "\"org.bidib.wizard.comm.");
     * result.append(line); result.append('\n'); } } catch (IOException e) { LOGGER.error(e.toString(), e); } finally {
     * try { buffer.close(); } catch (IOException e) { } } return new StringReader(result.toString()); }
     */

    /**
     * Change the package names. Use this if compiled with JDK 6 ...
     * 
     * @param reader
     *            the reader
     * @return the inputstream
     * @throws UnsupportedEncodingException
     */
    public static InputStream changePackage(Reader reader) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        BufferedReader buffer = new BufferedReader(reader);
        String line;

        try {
            while ((line = buffer.readLine()) != null) {
                line = line.replace("\"mvc.main.model.", "\"org.bidib.wizard.mvc.main.model.");
                line = line.replace("\"comm.", "\"org.bidib.wizard.comm.");
                result.append(line);
                result.append('\n');
            }
        }
        catch (IOException e) {
            LOGGER.error("Change package failed.", e);
        }
        finally {
            try {
                buffer.close();
            }
            catch (IOException e) {
            }
        }
        return new ByteArrayInputStream(result.toString().getBytes());
    }
}
