package org.bidib.wizard.mvc.main.model;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;

import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.MotorPortEnum;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.wizard.comm.MotorPortStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MotorPort extends Port<MotorPortStatus> {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(MotorPort.class);

    public static final String PROPERTY_PORT_VALUE = "value";

    static {
        try {
            // Q: why is this needed? A: export of beans with XMLDecoder
            PropertyDescriptor[] descriptor = Introspector.getBeanInfo(SwitchPort.class).getPropertyDescriptors();

            for (int i = 0; i < descriptor.length; i++) {
                PropertyDescriptor propertyDescriptor = descriptor[i];
                if (propertyDescriptor.getName().equals("value")) {
                    propertyDescriptor.setValue("transient", Boolean.TRUE);
                }
            }
        }
        catch (IntrospectionException e) {
            throw new RuntimeException(e);
        }
    }

    public static final MotorPort NONE = new MotorPort.Builder(-1).setLabel("<none>").build();

    /**
     * The absolute speed value.
     */
    private int value;

    public MotorPort() {
        this((GenericPort) null);
    }

    public MotorPort(GenericPort genericPort) {
        super(genericPort);
        setStatus(MotorPortStatus.FORWARD);
    }

    private MotorPort(Builder builder) {
        super(null);
        setId(builder.id);
        setLabel(builder.label);
    }

    /**
     * Get the absolute motor speed value.
     * 
     * @return absolute motor speed value
     */
    public int getValue() {
        if (genericPort != null) {
            Integer portVal = genericPort.getPortValue();
            if (portVal != null) {
                value = portVal;
            }
            else {
                value = 0;
            }
        }
        return value;
    }

    /**
     * Set the absolute motor speed value.
     * 
     * @param value
     *            absolute motor speed value
     */
    public void setValue(int value) {
        LOGGER.info("Set the value: {}", value);

        int oldValue = this.value;

        if (genericPort != null) {
            genericPort.setPortValue(value);
        }
        this.value = value;
        firePropertyChange(PROPERTY_PORT_VALUE, oldValue, value);
    }

    public String getDebugString() {
        return getClass().getSimpleName() + "[value=" + getValue() + "]";
    }

    @Override
    public byte[] getPortConfig() {
        return new byte[] { 0, 0, 0, 0 };
    }

    public void setPortConfig(byte[] portConfig) {
        LOGGER.info("Set the motor port parameters: {}", ByteUtils.bytesToHex(portConfig));

        setPortConfigEnabled(true);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof MotorPort) {
            return ((MotorPort) obj).getId() == getId();
        }
        return false;
    }

    @Override
    public int hashCode() {
        return getId();
    }

    @Override
    protected LcOutputType getPortType() {
        return LcOutputType.MOTORPORT;
    }

    @Override
    protected MotorPortStatus internalGetStatus() {
        return MotorPortStatus.valueOf(MotorPortEnum.valueOf(genericPort.getPortStatus()));
    }

    public static class Builder {
        private final int id;

        private String label;

        public Builder(int id) {
            this.id = id;
        }

        public Builder setLabel(String label) {
            this.label = label;
            return this;
        }

        public MotorPort build() {
            return new MotorPort(this);
        }
    }
}
