package org.bidib.wizard.mvc.main.model;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.enumeration.IoBehaviourEnum;
import org.bidib.jbidibc.core.enumeration.IoBehaviourInputEnum;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.PortConfigKeys;
import org.bidib.wizard.comm.InputPortStatus;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.main.model.listener.InputPortListener;
import org.bidib.wizard.mvc.main.model.listener.PortListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InputPortTableModel
    extends SimplePortTableModel<InputPortStatus, InputPort, InputPortListener<InputPortStatus>> {
    private static final Logger LOGGER = LoggerFactory.getLogger(InputPortTableModel.class);

    private static final long serialVersionUID = 1L;

    public static final int COLUMN_LABEL = 0;

    public static final int COLUMN_IO_BEHAVIOUR = 1;

    public static final int COLUMN_SWITCH_OFF_TIME = 2;

    public static final int COLUMN_PORT_IDENTIFIER = 3;

    public static final int COLUMN_STATUS = 4;

    public static final int COLUMN_PORT_INSTANCE = 5;

    public InputPortTableModel(MainModel model) {
        super();
        model.addInputPortListener(new PortListener<InputPortStatus>() {
            @Override
            public void labelChanged(Port<InputPortStatus> port, String label) {
                LOGGER.info("Label changed, port: {}, label: {}", port, label);

                fireTableCellUpdated(port.getId(), COLUMN_LABEL);
            }

            @Override
            public void statusChanged(Port<InputPortStatus> port, InputPortStatus status) {
                LOGGER.info("The port status has changed: {}, port: {}", status, port);
                for (int row = 0; row < getRowCount(); row++) {
                    if (port.equals(getValueAt(row, COLUMN_PORT_INSTANCE))) {
                        setValueAt(port.getStatus(), row, COLUMN_STATUS);
                        break;
                    }
                }
            }

            @Override
            public void configChanged(Port<InputPortStatus> port) {
            }
        });
    }

    @Override
    protected int getColumnPortInstance() {
        return COLUMN_PORT_INSTANCE;
    }

    @Override
    protected void initialize() {
        columnNames =
            new String[] { Resources.getString(getClass(), "label"), Resources.getString(getClass(), "ioBehaviour"),
                Resources.getString(getClass(), "switchOffTime"), Resources.getString(getClass(), "portIdentifier"),
                Resources.getString(getClass(), "status"), null };
    }

    public void addRow(InputPort port) {
        if (port != null) {
            Object[] rowData = new Object[columnNames.length];

            rowData[COLUMN_LABEL] = port.toString();
            rowData[COLUMN_IO_BEHAVIOUR] = port.getInputBehaviour();
            rowData[COLUMN_SWITCH_OFF_TIME] = port.getSwitchOffTime();
            rowData[COLUMN_PORT_IDENTIFIER] = port.getPortIdentifier();
            rowData[COLUMN_STATUS] = port.getStatus();
            rowData[COLUMN_PORT_INSTANCE] = port;
            addRow(rowData);
        }
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        boolean isEditable = false;
        InputPort inputPort = (InputPort) getValueAt(row, COLUMN_PORT_INSTANCE);
        switch (column) {
            case COLUMN_LABEL:
                isEditable = true;
                break;
            case COLUMN_IO_BEHAVIOUR:
                if (inputPort.isEnabled() && inputPort.isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_INPUT_CTRL)) {
                    isEditable = true;
                }
                // LOGGER.debug("Input port has BIDIB_PCFG_INPUT_CTRL: {}, row: {}", isEditable, row);
                break;
            case COLUMN_SWITCH_OFF_TIME:
                if (inputPort.isEnabled() && inputPort.isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_TICKS)) {
                    isEditable = true;
                }
                break;
            default:
                break;
        }
        return isEditable;
    }

    @Override
    public Class<?> getColumnClass(int column) {
        switch (column) {
            case COLUMN_LABEL:
                return String.class;
            case COLUMN_IO_BEHAVIOUR:
                return IoBehaviourEnum.class;
            default:
                return Object.class;
        }
    }

    @Override
    public void setValueAt(Object value, int row, int column) {
        final Object o = getValueAt(row, COLUMN_PORT_INSTANCE);

        if (o instanceof InputPort) {
            final InputPort port = (InputPort) o;

            switch (column) {
                case COLUMN_LABEL:
                    // if (value instanceof String) {
                    port.setLabel((String) value);
                    super.setValueAt(port.toString(), row, column);
                    fireLabelChanged(port, port.getLabel());
                    // }
                    // else {
                    // super.setValueAt(value, row, column);
                    // }
                    break;
                case COLUMN_IO_BEHAVIOUR:
                    IoBehaviourInputEnum ioBehaviour = (IoBehaviourInputEnum) value;

                    if (ioBehaviour != port.getInputBehaviour()) {
                        port.setInputBehaviour(ioBehaviour);
                        super.setValueAt(value, row, column);
                        fireValuesChanged(port, PortConfigKeys.BIDIB_PCFG_INPUT_CTRL);
                    }
                    else {
                        LOGGER.debug("The IOBehaviour has not been changed.");
                    }
                    break;
                case COLUMN_SWITCH_OFF_TIME:
                    int switchOffTime = (Integer) value;
                    if (port.getSwitchOffTime() != switchOffTime) {
                        port.setSwitchOffTime(switchOffTime);
                        super.setValueAt(value, row, column);
                        fireValuesChanged(port, PortConfigKeys.BIDIB_PCFG_TICKS);
                    }
                    else {
                        LOGGER.debug("The switchOff time has not been changed.");
                    }
                    break;
                case COLUMN_STATUS:
                    LOGGER.debug("Status of input port is updated: {}, port: {}", value, port);
                    if (value instanceof InputPortStatus) {
                        super.setValueAt(value, row, column);
                    }
                    else {
                        LOGGER.warn("Set an invalid value: {}", value);
                        super.setValueAt(value, row, column);
                    }
                    break;
                default:
                    super.setValueAt(value, row, column);
                    break;
            }
        }
        else {
            super.setValueAt(value, row, column);
        }
    }

    @Override
    public Object getValueAt(int row, int column) {
        switch (column) {
            case COLUMN_PORT_IDENTIFIER:
            case COLUMN_LABEL:
            case COLUMN_STATUS:
            case COLUMN_SWITCH_OFF_TIME:
            case COLUMN_IO_BEHAVIOUR:
                column = COLUMN_PORT_INSTANCE;
                break;
            default:
                break;
        }
        return super.getValueAt(row, column);
    }

    private void fireValuesChanged(InputPort port, PortConfigKeys... portConfigKeys) {
        for (InputPortListener<InputPortStatus> listener : portListeners) {
            listener.valuesChanged(port, portConfigKeys);
        }
    }

    public void changePortType(LcOutputType portType, InputPort port) {

        for (InputPortListener<InputPortStatus> l : portListeners) {
            l.changePortType(portType, port);
        }
    }
}
