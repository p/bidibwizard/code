package org.bidib.wizard.mvc.main.view.cvdef;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import org.apache.commons.lang.StringUtils;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.exchange.vendorcv.ModeType;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.common.utils.ImageUtils;
import org.bidib.wizard.mvc.common.view.converter.StringConverter;
import org.bidib.wizard.mvc.common.view.cvdefinition.CvDefinitionTreeTableModel;
import org.bidib.wizard.mvc.common.view.validation.PropertyValidationI18NSupport;
import org.bidib.wizard.mvc.main.view.panel.NodeTree;
import org.bidib.wizard.utils.InputValidationDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.value.ConverterValueModel;
import com.jgoodies.binding.value.ValueHolder;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.util.PropertyValidationSupport;
import com.jgoodies.validation.view.ValidationComponentUtils;

public class CvDccLongAddrValueEditor extends CvValueNumberEditor<Integer> {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(CvDccLongAddrValueEditor.class);

    private ValueModel cvHighValueModel = new ValueHolder();

    private ValueModel cvLowValueModel = new ValueHolder();

    private ValueModel converterHighValueModel;

    private ValueModel converterLowValueModel;

    private boolean adjustingInProgress;

    private int minValue = 1;

    private int maxValue = 10239;

    private ValueModel cvNumberHighModel = new ValueHolder();

    private ValueModel cvNumberLowModel = new ValueHolder();

    private ValueModel writeEnabled;

    // if the range of the low value is limited, this value is the maximum
    private Integer lowValueMax = 0x3F;

    // if the range of the high value is limited, this value is the maximum
    private Integer highValueMax = 0xFF;

    @Override
    protected ValidationResult validateModel(
        final PresentationModel<CvValueBean<Integer>> model, CvValueBean<Integer> cvValueBean) {

        PropertyValidationSupport support = new PropertyValidationI18NSupport(cvValueBean, "validation");

        if (Boolean.TRUE.equals(writeEnabled.getValue())) {
            Object val = model.getBufferedModel("cvValue").getValue();
            if (val instanceof Number) {
                int value = ((Number) val).intValue();
                if (value < minValue || value > maxValue) {
                    support.addError("cvvalue_key", "invalid_value;min=" + minValue + ",max=" + maxValue);
                }
            }
            else if (val == null) {
                support.addError("cvvalue_key", "not_empty");
            }

            Object valHigh = cvHighValueModel.getValue();
            if (valHigh instanceof Number) {
                int value = ((Number) valHigh).intValue();

                int maxHighValue = 255;
                if (highValueMax != null) {
                    maxHighValue = highValueMax;
                }

                if (value < 0 || value > maxHighValue) {
                    support.addError("cvhighvalue_key", "invalid_value;min=0,max=" + maxHighValue);
                }
            }
            else if (valHigh == null) {
                support.addError("cvhighvalue_key", "not_empty");
            }

            Object valLow = cvLowValueModel.getValue();
            if (valLow instanceof Number) {
                int value = ((Number) valLow).intValue();

                int maxLowValue = 255;
                if (lowValueMax != null) {
                    maxLowValue = lowValueMax + 192;
                }
                if (value < 0 || value > maxLowValue) {
                    support.addError("cvlowvalue_key", "invalid_value;min=0,max=" + maxLowValue);
                }
            }
            else if (valLow == null) {
                support.addError("cvlowvalue_key", "not_empty");
            }
        }
        else {
            LOGGER.debug("Validation is disabled!");
        }

        return support.getResult();
    }

    private boolean isAdjusting() {
        return adjustingInProgress;
    }

    private void setAdjusting(boolean adjusting) {
        this.adjustingInProgress = adjusting;
    }

    @Override
    protected DefaultFormBuilder prepareFormPanel() {

        writeEnabled = new ValueHolder(false);

        // Get buffered model objects.
        cvValueModel = cvAdapter.getBufferedModel("cvValue");
        valueConverterModel = new ConverterValueModel(cvValueModel, new StringConverter(new DecimalFormat("#")));

        DefaultFormBuilder formBuilder =
            new DefaultFormBuilder(new FormLayout("3dlu, max(40dlu;pref), 3dlu, max(30dlu;pref), 0dlu:grow"));
        JTextField textCvValue = BasicComponentFactory.createTextField(valueConverterModel, false);
        textCvValue.setDocument(new InputValidationDocument(InputValidationDocument.NUMERIC));
        textCvValue.setEditable(false);

        // the needs reboot Icon is invisible by default
        ImageIcon warnIcon = ImageUtils.createImageIcon(NodeTree.class, "/icons/warn.png");
        JLabel needsRebootIcon =
            new JLabel(Resources.getString(getClass(), "rebootrequired"), warnIcon, SwingConstants.LEADING);
        needsRebootIcon.setVisible(false);

        // init the components
        ValidationComponentUtils.setMandatory(textCvValue, true);
        ValidationComponentUtils.setMessageKey(textCvValue, "validation.cvvalue_key");
        formBuilder.leadingColumnOffset(1);
        formBuilder.nextLine(0);

        formBuilder.append(BasicComponentFactory.createLabel(new ValueHolder(
            getValueLabelPrefix() + " " + Resources.getString(CvDccLongAddrValueEditor.class, "value"))), 4);
        formBuilder.nextLine();
        formBuilder.append(textCvValue, 1);
        formBuilder.append(needsRebootIcon);

        // the 'byte' fields
        converterHighValueModel =
            new ConverterValueModel(cvHighValueModel, new StringConverter(new DecimalFormat("#")));
        converterLowValueModel = new ConverterValueModel(cvLowValueModel, new StringConverter(new DecimalFormat("#")));

        JTextField textCvHighValue = BasicComponentFactory.createTextField(converterHighValueModel, false);
        textCvHighValue.setDocument(new InputValidationDocument(InputValidationDocument.NUMERIC));
        textCvHighValue.setEditable(false);
        ValidationComponentUtils.setMessageKey(textCvHighValue, "validation.cvhighvalue_key");

        formBuilder.nextLine();
        formBuilder.append(BasicComponentFactory.createLabel(cvNumberHighModel, new CvNodeMessageFormat("{0} (CV{1})")),
            4);
        formBuilder.nextLine();
        formBuilder.append(textCvHighValue);

        JTextField textCvLowValue = BasicComponentFactory.createTextField(converterLowValueModel, false);
        textCvLowValue.setDocument(new InputValidationDocument(InputValidationDocument.NUMERIC));
        textCvLowValue.setEditable(false);
        ValidationComponentUtils.setMessageKey(textCvLowValue, "validation.cvlowvalue_key");

        formBuilder.nextLine();
        formBuilder.append(BasicComponentFactory.createLabel(cvNumberLowModel, new CvNodeMessageFormat("{0} (CV{1})")),
            4);
        formBuilder.nextLine();
        formBuilder.append(textCvLowValue);

        // add bindings for enable/disable the textfields
        PropertyConnector.connect(writeEnabled, "value", textCvValue, "editable");
        // PropertyConnector.connect(writeEnabled, "value", textCvHighValue, "editable");
        // PropertyConnector.connect(writeEnabled, "value", textCvLowValue, "editable");
        PropertyConnector.connect(needsRebootModel, "value", needsRebootIcon, "visible");

        cvHighValueModel.addValueChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                LOGGER.debug("high value has changed: {}", evt.getNewValue());
                if (isAdjusting()) {
                    LOGGER.debug("updateChangeInProgress, skip processing.");
                    return;
                }

                try {
                    setAdjusting(true);

                    Object lowVal = cvLowValueModel.getValue();
                    byte lowValByte = 0;
                    if (lowVal instanceof Number) {
                        lowValByte = ((Number) lowVal).byteValue();
                    }

                    if (evt.getNewValue() instanceof Number) {
                        Number num = (Number) evt.getNewValue();
                        byte val = num.byteValue();
                        LOGGER.debug("The new high value is: {}", val);
                        Integer newVal = ByteUtils.getInt(lowValByte, val);
                        valueConverterModel.setValue(String.valueOf(newVal));
                    }
                    else {
                        // handle empty or invalid value ..
                        Integer newVal = ByteUtils.getInt(lowValByte, (byte) 0);
                        valueConverterModel.setValue(String.valueOf(newVal));
                    }

                }
                finally {
                    setAdjusting(false);
                }
                triggerValidation(null);
            }
        });
        cvLowValueModel.addValueChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                LOGGER.debug("low value has changed: {}", evt.getNewValue());
                if (isAdjusting()) {
                    LOGGER.debug("updateChangeInProgress, skip processing.");
                    return;
                }

                try {
                    setAdjusting(true);

                    Object highVal = cvHighValueModel.getValue();
                    byte highValByte = 0;
                    if (highVal instanceof Number) {
                        highValByte = ((Number) highVal).byteValue();
                    }

                    if (evt.getNewValue() instanceof Number) {
                        Number num = (Number) evt.getNewValue();
                        byte val = num.byteValue();
                        LOGGER.debug("The new low value is: {}", val);
                        Integer newVal = ByteUtils.getInt(val, highValByte);
                        valueConverterModel.setValue(String.valueOf(newVal));
                    }
                    else {
                        // handle empty or invalid value ..
                        Integer newVal = ByteUtils.getInt((byte) 0, highValByte);
                        valueConverterModel.setValue(String.valueOf(newVal));
                    }

                }
                finally {
                    setAdjusting(false);
                }
                triggerValidation(null);
            }
        });
        cvValueModel.addValueChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                LOGGER.debug("The value has changed: {}", evt.getNewValue());

                if (isAdjusting()) {
                    LOGGER.debug("updateChangeInProgress, skip processing.");
                    return;
                }

                try {
                    setAdjusting(true);

                    if (evt.getNewValue() instanceof Number) {
                        Number num = (Number) evt.getNewValue();
                        int address = num.intValue();
                        LOGGER.debug("The new address value is: {}", address);

                        // see http://forum.opendcc.de/viewtopic.php?f=50&t=1630&p=16168#p16162
                        // lowAddress = 0xC0 | Math.round((address / 256) - 0.5))
                        int lowAddress = (int) (192 + Math.round((address / 256) - 0.5));
                        int highAddress = address - (lowAddress - 192) * 256;

                        converterHighValueModel.setValue(String.valueOf(highAddress));
                        converterLowValueModel.setValue(String.valueOf(lowAddress));
                    }
                    else {
                        converterHighValueModel.setValue(null);
                        converterLowValueModel.setValue(null);
                    }

                }
                finally {
                    setAdjusting(false);
                }
                triggerValidation(null);
            }
        });
        return formBuilder;
    }

    @Override
    public void setValue(CvNode cvNode, Map<String, CvNode> cvNumberToNodeMap) {

        boolean timeout = cvNode.getConfigVar().isTimeout();

        // set textfield editable before the value is set because the validation is triggered
        writeEnabled.setValue(!(ModeType.RO.equals(cvNode.getCV().getMode()) || timeout));

        LOGGER.debug("Set the value from cvNode: {}", cvNode);
        // clear the value models
        converterHighValueModel.setValue(null);
        converterLowValueModel.setValue(null);

        // set the value ... triggers validation
        super.setValue(cvNode, cvNumberToNodeMap);

        String highCvNumber = cvValueBean.getCvNode().getCV().getHigh();
        String lowCvNumber = cvValueBean.getCvNode().getCV().getLow();

        // get the low and high value nodes
        CvNode lowNode = cvNumberToNodeMap.get(lowCvNumber);
        CvNode highNode = cvNumberToNodeMap.get(highCvNumber);

        LOGGER.debug("Prepare form panel, high cv: {}, low cv: {}", highCvNumber, lowCvNumber);
        cvNumberHighModel.setValue(highNode);
        cvNumberLowModel.setValue(lowNode);

        // prepare the integer value
        Number lowValue = null;
        // this must be updated in the text field ...
        if (lowNode.getNewValue() instanceof Number) {
            lowValue = (Number) lowNode.getNewValue();
        }
        else if (StringUtils.isNotBlank(lowNode.getConfigVar().getValue())) {
            String value = lowNode.getConfigVar().getValue();
            try {
                lowValue = Integer.valueOf(value);
            }
            catch (NumberFormatException ex) {
                LOGGER.warn("Parse value failed: {}", value);
            }
        }
        cvLowValueModel.setValue(lowValue);

        Number highValue = null;
        // this must be updated in the text field ...
        if (highNode.getNewValue() instanceof Number) {
            highValue = (Number) highNode.getNewValue();
        }
        else if (StringUtils.isNotBlank(highNode.getConfigVar().getValue())) {
            String value = highNode.getConfigVar().getValue();
            try {
                highValue = Integer.valueOf(value);
            }
            catch (NumberFormatException ex) {
                LOGGER.warn("Parse value failed: {}", value);
            }
        }
        cvHighValueModel.setValue(highValue);

        checkNeedsReboot();

        triggerValidation(null);
    }

    protected void checkNeedsReboot() {
        CvNode highNode = (CvNode) cvNumberHighModel.getValue();
        CvNode lowNode = (CvNode) cvNumberLowModel.getValue();
        Boolean needsReboot = Boolean.FALSE;
        if (highNode != null && highNode.getCV() != null && highNode.getCV().isRebootneeded() != null) {
            needsReboot = highNode.getCV().isRebootneeded();
        }
        if (needsReboot.equals(Boolean.FALSE) && lowNode != null && lowNode.getCV() != null
            && lowNode.getCV().isRebootneeded() != null) {
            needsReboot = lowNode.getCV().isRebootneeded();
        }

        needsRebootModel.setValue(needsReboot);
    }

    @Override
    public void updateCvValues(Map<String, CvNode> cvNumberToNodeMap, CvDefinitionTreeTableModel treeModel) {
        CvNode cvNodeHigh = (CvNode) cvNumberHighModel.getValue();
        cvNodeHigh.setValueAt(cvHighValueModel.getValue(), CvNode.COLUMN_NEW_VALUE);

        CvNode cvNodeLow = (CvNode) cvNumberLowModel.getValue();
        cvNodeLow.setValueAt(cvLowValueModel.getValue(), CvNode.COLUMN_NEW_VALUE);

        // treeModel.refreshTreeTable(cvNodeLow);
        // treeModel.refreshTreeTable(cvNodeHigh);
    }

    /**
     * Refresh the displayed value from the node.
     */
    public void refreshValue() {
        super.refreshValue();

        CvNode cvNodeHigh = (CvNode) cvNumberHighModel.getValue();
        LOGGER.debug("Refresh values form cvNodeHigh: {}", cvNodeHigh);
        if (cvNodeHigh != null) {
            setValueInternally(cvNodeHigh, false);
        }
        CvNode cvNodeLow = (CvNode) cvNumberLowModel.getValue();
        LOGGER.debug("Refresh values form cvNodeLow: {}", cvNodeLow);
        if (cvNodeLow != null) {
            setValueInternally(cvNodeLow, false);
        }
    }

}
