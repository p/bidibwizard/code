package org.bidib.wizard.mvc.main.model;

public interface IdAware {
    int getId();
}
