package org.bidib.wizard.mvc.main.view.table;

import java.awt.Color;
import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

public class ComboBoxRenderer<E> extends JComboBox<E> implements TableCellRenderer {
    private static final long serialVersionUID = 1L;

    public ComboBoxRenderer(E[] items) {
        super(items);
    }

    public Component getTableCellRendererComponent(
        JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        if (isSelected) {
            // use table.foreground because under windows the foreground color is white and background is lightgray
            setForeground(hasFocus ? table.getForeground().brighter() : table.getForeground());
            setBackground(hasFocus ? table.getSelectionBackground().brighter() : table.getSelectionBackground());
            Color borderColor =
                hasFocus ? table.getSelectionBackground()/* .brighter() */ : table.getSelectionBackground();
            setBorder(BorderFactory.createLineBorder(borderColor));
        }
        else {
            setForeground(table.getForeground());
            setBackground(table.getBackground());
            setBorder(null);
        }
        setSelectedItem(value);
        return this;
    }
}
