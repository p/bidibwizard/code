package org.bidib.wizard.mvc.pom.view;

public class OperationAbortedException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public OperationAbortedException(String message) {
        super(message);
    }

}
