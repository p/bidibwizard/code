package org.bidib.wizard.mvc.main.view.panel.listener;

import java.util.Map;

import org.bidib.wizard.mvc.main.model.Node;

/**
 * The <code>NodeListActionListener</code> is the listener for actions performed on the node list or node tree.
 */
public interface NodeListActionListener extends LabelChangedListener<Node> {

    /**
     * Enable delivery of address messages on the node.
     * 
     * @param node
     *            the node
     */
    void enableAddressMessages(Node node);

    /**
     * Enable start macros from DCC on the node.
     * 
     * @param node
     *            the node
     */
    void enableDccStart(Node node);

    /**
     * Enable start macros from inputs on the node.
     * 
     * @param node
     *            the node
     */
    void enableExternalStart(Node node);

    /**
     * Enable delivery of feedback messages on the node.
     * 
     * @param node
     *            the node
     */
    void enableFeedbackMessages(Node node);

    /**
     * Disable delivery of feedback mirror on the node.
     * 
     * @param node
     *            the node
     * @param disable
     *            the enable flag
     */
    void disableFeedbackMirror(Node node, boolean disable);

    /**
     * Enable delivery of key messages on the node.
     * 
     * @param node
     *            the node
     */
    void enableKeyMessages(Node node);

    /**
     * Show the node features dialog.
     * 
     * @param node
     *            the node
     * @param x
     *            the X position of the label component that displays the node
     * @param y
     *            the Y position of the label component that displays the node
     */
    void features(Node node, int x, int y);

    /**
     * Export the node configuration to xml file.
     * 
     * @param node
     *            the node
     */
    void exportNode(Node node);

    /**
     * Open the firmware update dialog.
     * 
     * @param node
     *            the node
     * @param x
     *            the x position
     * @param y
     *            the y position
     */
    void firmwareUpdate(Node node, int x, int y);

    /**
     * Update the identify state on the node.
     * 
     * @param node
     *            the node
     */
    void identify(Node node);

    /**
     * Send a ping to the node.
     * 
     * @param node
     *            the node
     * @param data
     *            the data
     */
    void ping(Node node, byte data);

    /**
     * Read the uniqueId from the node.
     * 
     * @param node
     *            the node
     * @return the uniqueId
     */
    Long readUniqueId(Node node);

    /**
     * Import the node configuration from xml file.
     * 
     * @param node
     *            the node
     */
    void importNode(Node node);

    /**
     * Open the loco dialog.
     * 
     * @param node
     *            the node
     * @param x
     *            the x position
     * @param y
     *            the y position
     */
    void loco(Node node, int x, int y);

    /**
     * Open the loco table.
     * 
     * @param node
     *            the node
     * @param x
     *            the x position
     * @param y
     *            the y position
     */
    void locoList(Node node);

    /**
     * Open the DCC accessory dialog.
     * 
     * @param node
     *            the node
     * @param x
     *            the x position
     * @param y
     *            the y position
     */
    void dccAccessory(Node node, int x, int y);

    /**
     * Open the loco CV dialog.
     * 
     * @param node
     *            the node
     * @param x
     *            the x position
     * @param y
     *            the y position
     */
    void locoCv(Node node, int x, int y);

    /**
     * Open the loco CV dialog for programming track.
     * 
     * @param node
     *            the node
     * @param x
     *            the x position
     * @param y
     *            the y position
     */
    void locoCvPt(Node node, int x, int y);

    /**
     * Show the node details.
     * 
     * @param node
     *            the node
     * @param posX
     *            the X position of the label component that displays the node
     * @param posY
     *            the Y position of the label component that displays the node
     */
    void nodeDetails(Node node, int posX, int posY);

    /**
     * Save configurable values (accessories, macros, servo, switch and light port configuration) to the node.
     * 
     * @param node
     *            the node
     * @param params
     *            the parameters
     */
    void saveNode(Node node, Map<String, Object> params);

    /**
     * Open the DMX modeler.
     * 
     * @param node
     *            the node
     */
    void dmxModeler(Node node);
}
