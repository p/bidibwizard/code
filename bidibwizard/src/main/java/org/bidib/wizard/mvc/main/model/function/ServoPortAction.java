package org.bidib.wizard.mvc.main.model.function;

import org.bidib.jbidibc.exchange.lcmacro.LcMacroPointType;
import org.bidib.jbidibc.exchange.lcmacro.ServoActionType;
import org.bidib.jbidibc.exchange.lcmacro.ServoPortActionType;
import org.bidib.jbidibc.exchange.lcmacro.ServoPortPoint;
import org.bidib.wizard.comm.ServoPortStatus;
import org.bidib.wizard.mvc.main.model.ServoPort;

public class ServoPortAction extends SimplePortAction<ServoPortStatus, ServoPort> {
    /**
     * Creates a new instance of ServoPortAction and set the action to {@link ServoPortStatus#START}.
     */
    public ServoPortAction() {
        this(ServoPortStatus.START);
    }

    public ServoPortAction(ServoPortStatus action) {
        this(action, null, 0, 0);
    }

    /**
     * Creates a new instance of ServoPortAction and set the action to {@link ServoPortStatus#START}.
     * 
     * @param delay
     *            the delay value
     * @param servoPort
     *            the servoPort instance
     * @param value
     *            the target value of the servo
     */
    public ServoPortAction(ServoPortStatus action, ServoPort servoPort, int delay, int value) {
        super(action, KEY_SERVO, servoPort, delay, value);
    }

    public String getDebugString() {
        int id = 0;

        if (getPort() != null) {
            id = getPort().getId();
        }
        return "@" + getDelay() + " Servo:" + String.format("%02d", id) + "->" + getValue();
    }

    @Override
    public LcMacroPointType toLcMacroPoint() {
        ServoPortPoint servoPortPoint = new ServoPortPoint();
        servoPortPoint.setDelay(getDelay());
        servoPortPoint.setOutputNumber(getPort().getId());
        ServoPortActionType servoPortActionType = new ServoPortActionType();
        servoPortActionType.setAction(ServoActionType.fromValue(getAction().name()));
        servoPortActionType.setDestination(getValue());
        servoPortPoint.setServoPortActionType(servoPortActionType);
        return servoPortPoint;
    }
}
