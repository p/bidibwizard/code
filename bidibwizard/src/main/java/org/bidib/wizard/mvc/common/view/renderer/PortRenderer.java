package org.bidib.wizard.mvc.common.view.renderer;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import org.apache.commons.lang.StringUtils;
import org.bidib.wizard.mvc.main.model.Port;

public class PortRenderer extends JLabel implements ListCellRenderer<Port<?>> {

    private static final long serialVersionUID = 1L;

    public PortRenderer() {
        setOpaque(true);
    }

    public Component getListCellRendererComponent(
        JList<? extends Port<?>> list, Port<?> port, int index, boolean isSelected, boolean cellHasFocus) {

        if (isSelected) {
            setBackground(list.getSelectionBackground());
            setForeground(list.getSelectionForeground());
        }
        else {
            setBackground(list.getBackground());
            setForeground(list.getForeground());
        }

        if (port != null) {
            String text = null;
            if (StringUtils.isNotBlank(port.getLabel())) {
                text = String.format("%1$02d : %2$s", port.getId(), port.getLabel());
            }
            else if (port.getId() > -1) {
                text = String.format("%1$02d :", port.getId());
            }
            else {
                // must be empty space to show in combo!
                text = " ";
            }

            setText(text);

            if (port.isInactive() || !port.isEnabled()) {
                setBackground(Color.red.darker());
            }
        }
        else {
            setText(null);
        }
        return this;
    }
}
