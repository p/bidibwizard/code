package org.bidib.wizard.mvc.stepcontrol.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.EventObject;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BoundedRangeModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableModel;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.bidib.jbidibc.core.AccessoryState;
import org.bidib.jbidibc.core.Feature;
import org.bidib.jbidibc.core.enumeration.FeatureEnum;
import org.bidib.jbidibc.core.node.ConfigurationVariable;
import org.bidib.jbidibc.core.utils.AccessoryStateUtils.ErrorAccessoryState.AccessoryExecutionState;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.core.utils.ProductUtils;
import org.bidib.jbidibc.exchange.vendorcv.CVType;
import org.bidib.jbidibc.exchange.vendorcv.DataType;
import org.bidib.jbidibc.ui.LogarithmicJSlider;
import org.bidib.wizard.comm.FeedbackPortStatus;
import org.bidib.wizard.comm.MotorPortStatus;
import org.bidib.wizard.comm.SoundPortStatus;
import org.bidib.wizard.comm.SpeedSteps;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.common.utils.ImageUtils;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.bidib.wizard.mvc.common.view.converter.StringToUnsignedLongConverter;
import org.bidib.wizard.mvc.common.view.cvdefinition.CvDefinitionTreeTableModel;
import org.bidib.wizard.mvc.common.view.cvdefinition.CvDefintionPanelProvider;
import org.bidib.wizard.mvc.common.view.cvdefinition.CvValueUtils;
import org.bidib.wizard.mvc.common.view.icon.RotateLabel;
import org.bidib.wizard.mvc.common.view.icon.RotatedIcon;
import org.bidib.wizard.mvc.common.view.slider.SliderValueChangeListener;
import org.bidib.wizard.mvc.common.view.table.ButtonColumn;
import org.bidib.wizard.mvc.common.view.table.CustomBooleanCellEditor;
import org.bidib.wizard.mvc.common.view.table.CustomBooleanCellRenderer;
import org.bidib.wizard.mvc.common.view.table.LineNumberTableRowHeader;
import org.bidib.wizard.mvc.main.controller.FeedbackPortStatusChangeProvider;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.MotorPort;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.model.Port;
import org.bidib.wizard.mvc.main.model.SoundPort;
import org.bidib.wizard.mvc.main.model.listener.CvDefinitionRequestListener;
import org.bidib.wizard.mvc.main.model.listener.DefaultFeedbackPortListener;
import org.bidib.wizard.mvc.main.model.listener.PortValueListener;
import org.bidib.wizard.mvc.main.view.component.DefaultTabSelectionPanel;
import org.bidib.wizard.mvc.main.view.cvdef.CvNode;
import org.bidib.wizard.mvc.main.view.cvdef.CvNodeUtils;
import org.bidib.wizard.mvc.main.view.cvdef.LongCvNode;
import org.bidib.wizard.mvc.main.view.menu.BasicPopupMenu;
import org.bidib.wizard.mvc.main.view.panel.NodeTree;
import org.bidib.wizard.mvc.main.view.panel.listener.TabSelectionListener;
import org.bidib.wizard.mvc.main.view.panel.listener.TabStatusListener;
import org.bidib.wizard.mvc.main.view.statusbar.StatusBar;
import org.bidib.wizard.mvc.main.view.table.MotorSliderEditor;
import org.bidib.wizard.mvc.main.view.table.PortHighlighter;
import org.bidib.wizard.mvc.stepcontrol.controller.StepControlControllerInterface;
import org.bidib.wizard.mvc.stepcontrol.model.ConfigurationWizardModel;
import org.bidib.wizard.mvc.stepcontrol.model.ConfigurationWizardModel.WizardStatus;
import org.bidib.wizard.mvc.stepcontrol.model.CvConsoleModel;
import org.bidib.wizard.mvc.stepcontrol.model.Gearing;
import org.bidib.wizard.mvc.stepcontrol.model.MotorSizeType;
import org.bidib.wizard.mvc.stepcontrol.model.StepControlAspect;
import org.bidib.wizard.mvc.stepcontrol.model.StepControlAspect.Polarity;
import org.bidib.wizard.mvc.stepcontrol.model.StepControlModel;
import org.bidib.wizard.mvc.stepcontrol.model.TurnTableType;
import org.bidib.wizard.utils.IntegerEditor;
import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.Highlighter;
import org.jdesktop.swingx.decorator.HighlighterFactory;
import org.jdesktop.swingx.error.ErrorInfo;
import org.jdesktop.swingx.sort.TableSortController;
import org.oxbow.swingbits.dialog.task.TaskDialogs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.adapter.BoundedRangeAdapter;
import com.jgoodies.binding.adapter.SingleListSelectionAdapter;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.binding.value.ConverterFactory;
import com.jgoodies.binding.value.ConverterValueModel;
import com.jgoodies.binding.value.ValueHolder;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.builder.ButtonBarBuilder;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.debug.FormDebugPanel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.FormLayout;
import com.jidesoft.grid.DefaultExpandableRow;
import com.jidesoft.swing.JideButton;
import com.vlsolutions.swing.toolbars.ToolBarConstraints;
import com.vlsolutions.swing.toolbars.ToolBarPanel;
import com.vlsolutions.swing.toolbars.VLToolBar;

import eu.hansolo.steelseries.extras.Led;

public class StepControlPanel
    implements TabSelectionListener, CvDefintionPanelProvider, PortValueListener<MotorPortStatus> {
    private static final Logger LOGGER = LoggerFactory.getLogger(StepControlPanel.class);

    private static final String EMERGENCY_STOP = "<html><font color='red'><b>EMERGENCY STOP</b></font></html>";

    private static final String OPERATING = "<html><b>OPERATING</b></html>";

    private static final String READ = "read";

    private static final String WRITE = "write";

    private static final String KEYWORD_PATTERN_POLARITY = "target_polarity_%d";

    private static final String KEYWORD_PATTERN_POSITION = "target_position_%d";

    private static final String KEYWORD_CONFIGURED_ASPECTS = "configured_aspects";

    private static final String KEYWORD_TABLETYPE = "tabletype";

    private static final String KEYWORD_NEMATYPE = "nematype";

    private static final String KEYWORD_CURRENT_MOVING = "current_moving";

    private static final String KEYWORD_CURRENT_STOPPED = "current_stopped";

    private static final String KEYWORD_STEPCOUNT = "stepcount";

    private static final String KEYWORD_GEAR_PRIMARY = "gear_primary";

    private static final String KEYWORD_GEAR_SECONDARY = "gear_secondary";

    private static final String KEYWORD_TOTALSTEPS = "totalsteps";

    private static final String KEYWORD_BACKLASH = "backlash";

    private static final String KEYWORD_MICROSTEPPING = "microstepping";

    private static final String KEYWORD_SPEED = "speed";

    private static final String KEYWORD_ACCEL = "accel";

    private static final String KEYWORD_DECEL = "decel";

    private static final String KEYWORD_PUSH_INTERVAL = "push_interval";

    private static final String CVKEY_CURRENT_POSITION = "STEPPOS";

    private static final int EXTRA_VERTICAL_SPACE = 4;

    private static final int MAX_CONFIGURED_ASPECTS = 48;

    private static final long INACTIVE_POSITION_VALUE = 0xFFFFFFFFL;

    private final StepControlModel stepControlModel;

    private final TabStatusListener tabStatusListener;

    private final FeedbackPortStatusChangeProvider feedbackPortStatusChangeProvider;

    private JXTable table;

    private JPanel component;

    private Node selectedNode;

    private SelectionInList<StepControlAspect> aspectSelection;

    private PresentationModel<StepControlModel> stepControlPresentationModel;

    private ImageIcon accessoryErrorIcon;

    private ImageIcon accessorySuccessfulIcon;

    private ImageIcon accessoryWaitIcon;

    private ImageIcon accessoryUnknownIcon;

    private ImageIcon emergencyStopIcon;

    private JLabel operationalStateIconLabel = new JLabel();

    private JLabel executionStateIconLabel = new JLabel();

    private List<CvDefinitionRequestListener> cvDefinitionRequestListeners = new LinkedList<>();

    private Map<String, CvNode> mapKeywordToNode;

    private List<ConfigurationVariable> requiredConfigVariables = new LinkedList<>();

    private final MainModel mainModel;

    private boolean initialized;

    private StatusBar statusBar;

    private List<String> fieldsToUpdate = new LinkedList<>();

    private List<Led> occupancyLeds = new LinkedList<>();

    private JButton readCvButton;

    private JButton writeCvButton;

    private VLToolBar toolbarCvDefinition;

    // turntable
    private double degree;

    private RotatedIcon turntableIcon;

    private JComponent turntableComponent;

    private AngleRenderer angleRenderer;

    private MotorSlider motorSliderEditor;

    private MotorPort selectedMotorPort;

    private StepControlControllerInterface stepControlController;

    private Map<String, JideButton> functionButtonMap = new HashMap<>();

    private JPanel functionButtonParentPanel;

    // flag to change sliders to logarithmic scale
    private boolean useLogarithmicSliderSpeed = false;

    private boolean useLogarithmicSlider = true;

    private JSlider speedSlider;

    private ValueModel speedModel;

    private static final class MotorSlider extends MotorSliderEditor implements PropertyChangeListener {

        private static final long serialVersionUID = 1L;

        public MotorSlider(MotorPort motorPort, int minValue, int maxValue) {
            super(motorPort, minValue, maxValue);
        }

        public JPanel getComponent() {
            return panel;
        }

        public void addSliderValueChangeListener(final SliderValueChangeListener listener) {

            slider.addChangeListener(new ChangeListener() {

                @Override
                public void stateChanged(ChangeEvent e) {
                    JSlider source = (JSlider) e.getSource();

                    LOGGER.info("The slider state has changed, source value: {}", source.getValue());

                    listener.stateChanged(e, source.getValueIsAdjusting(), source.getValue());
                }
            });
        }

        @Override
        public void setPort(MotorPort port) {
            if (getPort() != null) {
                getPort().removePropertyChangeListener(MotorPort.PROPERTY_PORT_VALUE, this);
            }

            super.setPort(port);

            if (port != null) {
                port.addPropertyChangeListener(MotorPort.PROPERTY_PORT_VALUE, this);
            }

        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            LOGGER.info("The value of the motor port has changed: {}", evt);

            if (MotorPort.PROPERTY_PORT_VALUE.equals(evt.getPropertyName())) {
                if (getPort() != null) {
                    int value = getPort().getValue();
                    LOGGER.info("Set the new value: {}", value);
                    BoundedRangeModel m = slider.getModel();
                    m.setValue(value);
                }
            }
        }

    }

    public StepControlPanel(final MainModel mainModel, final StepControlModel stepControlModel,
        final TabStatusListener tabStatusListener,
        final FeedbackPortStatusChangeProvider feedbackPortStatusChangeProvider,
        StepControlControllerInterface stepControlController) {
        this.tabStatusListener = tabStatusListener;
        this.mainModel = mainModel;
        this.feedbackPortStatusChangeProvider = feedbackPortStatusChangeProvider;
        this.stepControlController = stepControlController;

        this.stepControlModel = stepControlModel;

        statusBar = DefaultApplicationContext.getInstance().get("statusBar", StatusBar.class);
    }

    public void createComponent() {

        DefaultTabSelectionPanel container = new DefaultTabSelectionPanel() {
            private static final long serialVersionUID = 1L;

            @Override
            public void tabSelected(boolean selected) {
                LOGGER.debug("Select tab, current component: is StepControlPanel.");

                StepControlPanel.this.tabSelected(selected);
            }
        };

        DefaultFormBuilder defaultFormBuilder = new DefaultFormBuilder(new FormLayout("p, 3dlu, p:g"), container);
        defaultFormBuilder.border(Borders.TABBED_DIALOG);

        // load the icons
        initializeAccessoryStateIcons();

        ImageIcon iconCreateNewAspect =
            ImageUtils.createImageIcon(StepControlPanel.class, "/icons/stepcontrol/aspect_insert.png");
        ImageIcon iconUpdateAspect =
            ImageUtils.createImageIcon(StepControlPanel.class, "/icons/stepcontrol/aspect_edit.png");
        ImageIcon iconConfigurationWizard =
            ImageUtils.createImageIcon(StepControlPanel.class, "/icons/16x16/wrench.png");

        aspectSelection =
            new SelectionInList<>((ListModel<StepControlAspect>) stepControlModel.getStepControlAspectsListModel());

        stepControlPresentationModel = new PresentationModel<>(new ValueHolder(stepControlModel, true));

        operationalStateIconLabel = new JLabel(EMERGENCY_STOP);
        defaultFormBuilder.append(Resources.getString(StepControlPanel.class, "operational_state"),
            operationalStateIconLabel);

        executionStateIconLabel = new JLabel();
        defaultFormBuilder.append(Resources.getString(StepControlPanel.class, "execution_state"),
            executionStateIconLabel);

        defaultFormBuilder.appendRow("5dlu");

        table =
            new JXTable(new AspectTableAdapter(aspectSelection,
                new String[] { Resources.getString(StepControlPanel.class, "position"),
                    Resources.getString(StepControlPanel.class, "angle"),
                    Resources.getString(StepControlPanel.class, "polarity"),
                    Resources.getString(StepControlPanel.class, "action") }) {

                private static final long serialVersionUID = 1L;

                @Override
                public void fireTableDataChanged() {
                    // preserve selection calling fireTableDataChanged()
                    LOGGER.info("The table data has changed.");

                    int[] sel = null;
                    int[] index = null;
                    if (table != null) {
                        sel = table.getSelectedRows();
                        index = new int[1];
                        index[0] = table.getRowSorter().convertRowIndexToModel(sel[0]);
                        LOGGER.info("tableDataChanged, sel: {}, index: {}", sel, index);
                    }

                    super.fireTableDataChanged();

                    if (index != null) {
                        // write the changed cv value to the node
                        prepareAndWriteCvValuesToNode(true);

                        LOGGER.info("Set the selection interval on index: {}", index[0]);
                        // keep selection
                        // getSelectionModel().setSelectionInterval(row, row);

                        setSelectedIndex(index[0]);
                    }
                }
            }) {

                private static final long serialVersionUID = 1L;

                @Override
                public boolean editCellAt(final int row, int column, EventObject e) {
                    boolean isEditing = super.editCellAt(row, column, e);
                    LOGGER.info("isEditing: {}", isEditing);

                    // this is necessary for the polarity toggle button to work correct
                    if (column == AspectTableAdapter.COLUMN_POLARITY) {
                        SwingUtilities.invokeLater(new Runnable() {
                            public void run() {
                                if (getCellEditor() != null) {
                                    LOGGER.info("Stop the cell editing.");
                                    getCellEditor().stopCellEditing();

                                    // // write the changed cv value to the node
                                    // prepareAndWriteCvValuesToNode(true);
                                    //
                                    // LOGGER.info("Set the selection interval on row: {}", row);
                                    // // keep selection
                                    // getSelectionModel().setSelectionInterval(row, row);
                                }
                            }
                        });
                    }
                    return isEditing;
                }

                protected RowSorter<? extends TableModel> createDefaultRowSorter() {
                    return new TableSortController<TableModel>(getModel()) {
                        @Override
                        public void toggleSortOrder(int column) {
                            LOGGER.debug("toggleSortOrder on column: {}", column);
                            // do nothing to prevent toggle sort order
                        }
                    };
                }
            };
        table.setSelectionModel(new SingleListSelectionAdapter(aspectSelection.getSelectionIndexHolder()));
        table.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);

        table.setRowHeight(table.getRowHeight() + EXTRA_VERTICAL_SPACE);
        table.getColumn(AspectTableAdapter.COLUMN_POSITION).setPreferredWidth(80);
        table.getColumn(AspectTableAdapter.COLUMN_POLARITY).setPreferredWidth(70);
        table.getColumn(AspectTableAdapter.COLUMN_PERFORM_ASPECT).setPreferredWidth(100);

        DecimalFormatSymbols symbols = new DecimalFormatSymbols(Locale.ENGLISH);
        angleRenderer = new AngleRenderer(new DecimalFormat("0.0", symbols));
        table.getColumn(AspectTableAdapter.COLUMN_ANGLE).setCellRenderer(angleRenderer);

        // add highlighter that evaluate if the port is enabled
        PortHighlighter portHighlighter = new PortHighlighter();
        Highlighter simpleStriping = HighlighterFactory.createSimpleStriping();
        table.setHighlighters(simpleStriping, portHighlighter);

        JScrollPane scrollPane = new JScrollPane(table);
        scrollPane.setPreferredSize(table.getPreferredSize());

        LineNumberTableRowHeader tableLineNumber = new LineNumberTableRowHeader(scrollPane, table);
        tableLineNumber.setBackground(Color.LIGHT_GRAY);
        tableLineNumber.setVerticalOffset(EXTRA_VERTICAL_SPACE);
        scrollPane.setRowHeaderView(tableLineNumber);

        table.getModel().addTableModelListener(new TableModelListener() {

            @Override
            public void tableChanged(TableModelEvent e) {
                LOGGER.info("The table has changed, e: {}", e);

                tabStatusListener.updatePendingChanges(component, true);
            }
        });

        defaultFormBuilder.appendRow("fill:200dlu:g");
        defaultFormBuilder.nextLine(2);
        defaultFormBuilder.append(scrollPane);

        // create the 'detail panel'
        DefaultFormBuilder detailFormBuilder = null;
        boolean debug = false;
        detailFormBuilder =
            new DefaultFormBuilder(new FormLayout("p, 3dlu, p, 3dlu, p, 3dlu, p, 3dlu, p, 3dlu, p:g"),
                (debug ? new FormDebugPanel() : new JPanel()));
        detailFormBuilder.border(Borders.DLU4);

        // prepare the configuration wizard button
        JButton startConfigurationWizard =
            new JButton(Resources.getString(StepControlPanel.class, "button.wizard"), iconConfigurationWizard);
        startConfigurationWizard.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                LOGGER.info("Open configuration wizard.");
                fireConfiguration();
            }
        });

        detailFormBuilder.append(buildLeftAlignedButtonBar(startConfigurationWizard), 9);
        detailFormBuilder.nextLine();

        final ValueModel currentPositionConverterModel =
            new ConverterValueModel(
                stepControlPresentationModel.getModel(StepControlModel.PROPERTYNAME_CURRENT_POSITION),
                new StringToUnsignedLongConverter());
        JTextField currentPosition = BasicComponentFactory.createTextField(currentPositionConverterModel);
        detailFormBuilder.append(Resources.getString(StepControlPanel.class, "position"), currentPosition);

        // add button for setpos and getpos
        JButton setPosition = new JButton(Resources.getString(StepControlPanel.class, "set_position"));
        setPosition.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                LOGGER.info("Set the position value.");
                fireSetPosition();
            }
        });
        detailFormBuilder.append(setPosition);

        JButton getPosition = new JButton(Resources.getString(StepControlPanel.class, "get_position"));
        getPosition.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                LOGGER.info("Get the position value.");
                fireGetPosition();
            }
        });
        detailFormBuilder.append(getPosition);

        // MotorPort motorPort = null;
        motorSliderEditor = new MotorSlider(null, -SpeedSteps.DCC128.getSteps(), SpeedSteps.DCC128.getSteps());
        // create the component
        motorSliderEditor.createComponent(0);

        // disable the motor slider by default
        motorSliderEditor.setEnabled(false);

        JPanel sliderComponent = motorSliderEditor.getComponent();

        detailFormBuilder.append("Motor", sliderComponent);

        detailFormBuilder.nextLine();

        // lower part of the detail form

        JButton createNewAspect = new JButton(iconCreateNewAspect);
        createNewAspect.setMargin(new Insets(0, 0, 0, 0));
        createNewAspect.setToolTipText(Resources.getString(StepControlPanel.class, "create_new_aspect"));
        createNewAspect.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                LOGGER.info("Create new aspect from data in form.");

                fireCreateNewAspect();
            }
        });

        // append the buttons
        // detailFormBuilder.append(buildLeftAlignedButtonBar(createNewAspect/* , readCurrentPos */), 11);

        JButton updatePosOfCurrentAspect = new JButton(iconUpdateAspect);
        updatePosOfCurrentAspect.setMargin(new Insets(0, 0, 0, 0));
        updatePosOfCurrentAspect.setToolTipText(Resources.getString(StepControlPanel.class, "update_aspect"));
        updatePosOfCurrentAspect.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                LOGGER.info("Update seleceted aspect with current position.");

                fireUpdateAspect();
            }
        });
        updatePosOfCurrentAspect.setEnabled(false);

        // append the buttons
        detailFormBuilder.append(Resources.getString(StepControlPanel.class, "aspect"),
            buildLeftAlignedButtonBar(createNewAspect, updatePosOfCurrentAspect), 9);

        detailFormBuilder.nextLine();

        // add the occupancy leds
        Led occupancy1 = new Led();
        occupancy1.setPreferredSize(new Dimension(32, 32));
        Led occupancy2 = new Led();
        occupancy2.setPreferredSize(new Dimension(32, 32));
        Led occupancy3 = new Led();
        occupancy3.setPreferredSize(new Dimension(32, 32));
        Led occupancy4 = new Led();
        occupancy4.setPreferredSize(new Dimension(32, 32));

        occupancyLeds.add(occupancy1);
        occupancyLeds.add(occupancy2);
        occupancyLeds.add(occupancy3);
        occupancyLeds.add(occupancy4);

        detailFormBuilder.append(Resources.getString(StepControlPanel.class, "occupancy"),
            buildLeftAlignedButtonBar(occupancy1, occupancy2, occupancy3, occupancy4), 9);
        detailFormBuilder.nextLine();

        // slider for speed
        speedModel = stepControlPresentationModel.getModel(StepControlModel.PROPERTYNAME_SPEED);
        speedSlider = (useLogarithmicSliderSpeed ? new LogarithmicJSlider() : new JSlider());
        // JSlider speedSlider = new LogarithmicJSlider();

        int maxSpeed = 2001;
        int minSpeed = 1;
        LOGGER.info("Use initial speed range, min: {}, max: {}", minSpeed, maxSpeed);

        BoundedRangeAdapter boundedRangeAdapter = new BoundedRangeAdapter(speedModel, 1, minSpeed, maxSpeed);
        speedSlider.setModel(boundedRangeAdapter);

        JLabel speedLabel =
            BasicComponentFactory
                .createLabel(ConverterFactory.createStringConverter(speedModel, new DecimalFormat("#####")));
        speedLabel.setPreferredSize(new Dimension(40, speedLabel.getPreferredSize().height));
        detailFormBuilder.append(Resources.getString(StepControlPanel.class, "speed"), speedLabel);
        detailFormBuilder.append(speedSlider, 7);

        detailFormBuilder.nextLine();

        // slider for accel
        ValueModel accelModel = stepControlPresentationModel.getModel(StepControlModel.PROPERTYNAME_ACCEL);
        JSlider accelSlider = (useLogarithmicSlider ? new LogarithmicJSlider() : new JSlider());

        BoundedRangeAdapter accelBoundedRangeAdapter = new BoundedRangeAdapter(accelModel, 1, 1, 65536);
        accelSlider.setModel(accelBoundedRangeAdapter);

        JLabel accelLabel =
            BasicComponentFactory
                .createLabel(ConverterFactory.createStringConverter(accelModel, new DecimalFormat("#####")));
        detailFormBuilder.append(Resources.getString(StepControlPanel.class, "accel"), accelLabel);
        detailFormBuilder.append(accelSlider, 7);

        detailFormBuilder.nextLine();

        // slider for decel
        ValueModel decelModel = stepControlPresentationModel.getModel(StepControlModel.PROPERTYNAME_DECEL);
        JSlider decelSlider = (useLogarithmicSlider ? new LogarithmicJSlider() : new JSlider());

        BoundedRangeAdapter decelBoundedRangeAdapter = new BoundedRangeAdapter(decelModel, 1, 1, 65536);
        decelSlider.setModel(decelBoundedRangeAdapter);

        JLabel decelLabel =
            BasicComponentFactory
                .createLabel(ConverterFactory.createStringConverter(decelModel, new DecimalFormat("#####")));
        detailFormBuilder.append(Resources.getString(StepControlPanel.class, "decel"), decelLabel);
        detailFormBuilder.append(decelSlider, 7);

        // add the lower part with animation and sound buttons
        detailFormBuilder.appendRow("3dlu");
        detailFormBuilder.appendRow("top:p:g");
        detailFormBuilder.nextLine(2);

        // add the turntable image
        ImageIcon turntableBackgroundIcon =
            ImageUtils.loadImageIcon(NodeTree.class, "/icons/stepcontrol/turntable-bg.png", 120, 120);
        ImageIcon turntableBasicIcon =
            ImageUtils.loadImageIcon(NodeTree.class, "/icons/stepcontrol/turntable-platform.png", 110, 110);

        JLayeredPane layeredPane = new JLayeredPane();
        layeredPane.setPreferredSize(new Dimension(140, 140));

        JLabel backgroundLabel = new JLabel(turntableBackgroundIcon);
        backgroundLabel.setOpaque(true);

        // backgroundLabel.setBackground(Color.GREEN);

        backgroundLabel.setBounds(0, 0, turntableBackgroundIcon.getIconWidth(),
            turntableBackgroundIcon.getIconHeight());
        layeredPane.add(backgroundLabel, new Integer(10));

        turntableIcon = new RotatedIcon(turntableBasicIcon, degree, true) {
            public int getIconWidth() {
                return getIconHeight();
            };
        };

        turntableComponent = new RotateLabel(turntableIcon);

        turntableComponent.setBounds(5, 5, turntableBackgroundIcon.getIconWidth() - 10,
            turntableBackgroundIcon.getIconHeight() - 10);
        turntableComponent.setOpaque(false);

        layeredPane.add(turntableComponent, new Integer(20));

        // create a panel with the turntable animation
        JPanel contentPanel = new JPanel(new BorderLayout());
        contentPanel.setOpaque(true);
        contentPanel.add(layeredPane, BorderLayout.CENTER);

        detailFormBuilder.append(contentPanel, 5);

        // create the panel for the sound function buttons
        functionButtonParentPanel = new JPanel();
        detailFormBuilder.append(functionButtonParentPanel, 5);

        defaultFormBuilder.append(new JScrollPane(detailFormBuilder.build()));

        component = defaultFormBuilder.build();

        // the name is displayed on the tab
        component.setName(getName());

        final AspectTablePopupMenu menu = new AspectTablePopupMenu();

        table.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                handleMouseEvent(e, menu);
            }

            public void mouseReleased(MouseEvent e) {
                handleMouseEvent(e, menu);
            }
        });
        // do not allow drag columns to other position
        table.getTableHeader().setReorderingAllowed(false);

        table.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);

        table.getColumn(AspectTableAdapter.COLUMN_POLARITY).setCellEditor(new CustomBooleanCellEditor(
            "/icons/stepcontrol/arrow_straight.png", "/icons/stepcontrol/arrow_switch.png"));
        table.getColumn(AspectTableAdapter.COLUMN_POLARITY).setCellRenderer(new CustomBooleanCellRenderer(
            "/icons/stepcontrol/arrow_straight.png", "/icons/stepcontrol/arrow_switch.png"));

        table.getColumn(AspectTableAdapter.COLUMN_POSITION).setCellRenderer(new LongRenderer());
        table.getColumn(AspectTableAdapter.COLUMN_POSITION).setCellEditor(new IntegerEditor(0, Integer.MAX_VALUE));

        table.getColumnExt(AspectTableAdapter.COLUMN_POLARITY).setSortable(false);
        table.getColumnExt(AspectTableAdapter.COLUMN_PERFORM_ASPECT).setSortable(false);
        table.setSortOrder(AspectTableAdapter.COLUMN_POSITION, SortOrder.ASCENDING);

        Action performAspect = new AbstractAction() {
            private static final long serialVersionUID = 1L;

            public void actionPerformed(ActionEvent e) {
                int modelRow = Integer.valueOf(e.getActionCommand());
                LOGGER.info("Perform aspect of row: {}", modelRow);
                firePerformAspect(modelRow);
            }
        };

        // add the button renderer to the column
        new ButtonColumn(table, performAspect, AspectTableAdapter.COLUMN_PERFORM_ASPECT);

        // enable the button if an aspect is selected
        aspectSelection.addValueChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                updatePosOfCurrentAspect.setEnabled(evt.getNewValue() != null);

            }
        });

        // add the pending changes listener to the stepControlModel
        stepControlModel.addPropertyChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                LOGGER.info("Property has changed, evt: {}", evt);

                if (StepControlModel.PROPERTYNAME_OPERATIONAL_MODE.equals(evt.getPropertyName())) {
                    if (Boolean.FALSE == evt.getNewValue()) {
                        LOGGER.info("Set the operational state label to emergency mode.");
                        operationalStateIconLabel.setIcon(emergencyStopIcon);
                        operationalStateIconLabel.setText(EMERGENCY_STOP);
                    }
                    else {
                        LOGGER.info("Set the operational state label to operating mode.");
                        operationalStateIconLabel.setIcon(null);
                        operationalStateIconLabel.setText(OPERATING);
                    }
                }
                else if (StepControlModel.PROPERTYNAME_CURRENT_DEGREES.equals(evt.getPropertyName())) {
                    // no update of pending changes
                }
                else if (StepControlModel.PROPERTYNAME_TARGET_DEGREES.equals(evt.getPropertyName())) {
                    // no update of pending changes
                }
                else if (StepControlModel.PROPERTYNAME_MOTOR_PORT.equals(evt.getPropertyName())) {
                    // no update of pending changes

                    selectedMotorPort = stepControlModel.getMotorPort();
                    LOGGER.info("The selected motor port has changed, selectedMotorPort: {}", selectedMotorPort);

                    motorSliderEditor.setPort(selectedMotorPort);
                    motorSliderEditor.setEnabled(selectedMotorPort != null);
                }
                else if (StepControlModel.PROPERTYNAME_SOUND_PORTS.equals(evt.getPropertyName())) {
                    // no update of pending changes

                    List<SoundPort> soundPorts = stepControlModel.getSoundPorts();
                    LOGGER.info("The sound ports have changed, soundPorts: {}", soundPorts);

                    functionButtonParentPanel.removeAll();

                    if (CollectionUtils.isNotEmpty(soundPorts)) {
                        final List<JideButton> functionButtons = new LinkedList<>();
                        createFunctionButtonPanel(functionButtonParentPanel, functionButtons);
                    }
                    functionButtonParentPanel.revalidate();
                }
                else {
                    tabStatusListener.updatePendingChanges(component, true);
                }
            }
        });

        speedSlider.addChangeListener(new ChangeListener() {

            @Override
            public void stateChanged(ChangeEvent e) {

                // the slider must not fire when setting the values
                if (!initialized || selectedNode == null) {
                    LOGGER.info(
                        "The panel is not initialized or no node is selected, discard the state change of speed.");
                    return;
                }

                JSlider source = (JSlider) e.getSource();
                if (!source.getValueIsAdjusting() && initialized) {
                    firePerformSpeed();
                }
            }
        });

        accelSlider.addChangeListener(new ChangeListener() {

            @Override
            public void stateChanged(ChangeEvent e) {

                // the slider must not fire when setting the values
                if (!initialized || selectedNode == null) {
                    LOGGER.info(
                        "The panel is not initialized or no node is selected, discard the state change of accel.");
                    return;
                }

                JSlider source = (JSlider) e.getSource();
                if (!source.getValueIsAdjusting() && initialized) {
                    firePerformAccel();
                }
            }
        });

        decelSlider.addChangeListener(new ChangeListener() {

            @Override
            public void stateChanged(ChangeEvent e) {

                // the slider must not fire when setting the values
                if (!initialized || selectedNode == null) {
                    LOGGER.info(
                        "The panel is not initialized or no node is selected, discard the state change of decel.");
                    return;
                }

                JSlider source = (JSlider) e.getSource();
                if (!source.getValueIsAdjusting() && initialized) {
                    firePerformDecel();
                }
            }
        });

        motorSliderEditor.addSliderValueChangeListener(new SliderValueChangeListener() {

            @Override
            public void stateChanged(ChangeEvent e, boolean isAdjusting, int value) {
                // only handle if not adjusting
                if (!isAdjusting) {
                    MotorPort motorPort = stepControlModel.getMotorPort();

                    if (motorPort != null) {
                        LOGGER.info("Update motor port: {}, new value: {}", motorPort.getDebugString(), value);

                        motorPort.setValue(value);

                        stepControlController.setMotorPortValue(motorPort);
                    }

                }
            }
        });

        toolbarCvDefinition = new VLToolBar("stepControlCvDefinition");
        addButtons(toolbarCvDefinition);
        addToolBar(toolbarCvDefinition, new ToolBarConstraints(0, 2));
        // initially invisible
        toolbarCvDefinition.setVisible(false);

        feedbackPortStatusChangeProvider.addFeedbackPortListener(new DefaultFeedbackPortListener() {

            @Override
            public void statusChanged(Port<FeedbackPortStatus> port, FeedbackPortStatus status) {
                if (selectedNode != null && ProductUtils.isStepControl(selectedNode.getUniqueId())) {

                    if (port.getId() < occupancyLeds.size()) {
                        LOGGER.info("The status of the feedback port has changed: {}, status: {}", port, status);
                        Led occupancyLed = occupancyLeds.get(port.getId());
                        if (occupancyLed != null) {

                            occupancyLed.setLedOn(status == FeedbackPortStatus.OCCUPIED ? true : false);
                        }
                    }
                    else {
                        LOGGER.debug("Port number of feedback port is out of range: {}", port.getId());
                    }
                }
                else {
                    LOGGER.info("Do not update occupancy leds because the selected node is not a StepControl.");
                }
            }
        });
    }

    protected void fireGetPosition() {
        LOGGER.info("Read the current position value: {}");

        try {
            final List<ConfigurationVariable> cvList = new LinkedList<>();

            prepareGetCurrentPosition(cvList);

            fireLoadConfigVariables(cvList);
        }
        catch (Exception ex) {
            LOGGER.warn("Get the new {} value failed.", CVKEY_CURRENT_POSITION, ex);
        }
    }

    private void prepareGetCurrentPosition(final List<ConfigurationVariable> cvList) {

        // add the current position
        ConfigurationVariable cv = new ConfigurationVariable(CVKEY_CURRENT_POSITION, null /* "" */);
        cvList.add(cv);

        fieldsToUpdate.add(CVKEY_CURRENT_POSITION);

        CvNode cvNode = selectedNode.getCvNumberToJideNodeMap().get(CVKEY_CURRENT_POSITION);
        if (cvNode == null) {
            CVType cvType = new CVType();
            cvType.setNumber(CVKEY_CURRENT_POSITION);
            cvType.setType(DataType.STRING);
            cvNode = new CvNode(cvType, cv);
            LOGGER.info("Add the transient CvNode for current position: {}", cvNode);

            selectedNode.getCvNumberToJideNodeMap().put(CVKEY_CURRENT_POSITION, cvNode);
        }

    }

    protected void fireSetPosition() {
        Long value = stepControlModel.getCurrentPosition();

        if (value == null) {
            LOGGER.warn("The position value is null.");
            return;
        }
        int currentPosition = (int) value.intValue();

        LOGGER.info("Write the current position value: {}", currentPosition);

        try {
            final List<ConfigurationVariable> cvList = new LinkedList<>();
            // add the current position
            ConfigurationVariable cv =
                new ConfigurationVariable(CVKEY_CURRENT_POSITION, Integer.toString(currentPosition));
            cvList.add(cv);
            fieldsToUpdate.add(CVKEY_CURRENT_POSITION);

            CvNode cvNode = selectedNode.getCvNumberToJideNodeMap().get(CVKEY_CURRENT_POSITION);

            if (cvNode == null) {
                CVType cvType = new CVType();
                cvType.setNumber(CVKEY_CURRENT_POSITION);
                cvType.setType(DataType.STRING);
                cvNode = new CvNode(cvType, cv);
                LOGGER.info("Add the transient CvNode for current position: {}", cvNode);

                selectedNode.getCvNumberToJideNodeMap().put(CVKEY_CURRENT_POSITION, cvNode);
            }
            else {
                cvNode.setNewValue(Integer.toString(currentPosition));
            }

            // write the config variables to the node
            fireWriteConfigVariables(cvList);
        }
        catch (Exception ex) {
            LOGGER.warn("Set the new {} value failed.", CVKEY_CURRENT_POSITION, ex);
        }
    }

    public void setTurntableDegrees(double degree) {
        // degree = time * 360;

        LOGGER.info("Set degree: {}", degree);

        if (turntableIcon != null) {
            turntableIcon.setDegrees(degree);
            turntableComponent.repaint();
        }
    }

    /**
     * Initialize the accessory state icons
     */
    private void initializeAccessoryStateIcons() {
        // Set the icon for leaf nodes.
        accessoryErrorIcon = ImageUtils.createImageIcon(StepControlPanel.class, "/icons/accessory-error.png");
        accessorySuccessfulIcon = ImageUtils.createImageIcon(StepControlPanel.class, "/icons/accessory-successful.png");
        accessoryWaitIcon = ImageUtils.createImageIcon(StepControlPanel.class, "/icons/accessory-wait.png");
        accessoryUnknownIcon = ImageUtils.createImageIcon(StepControlPanel.class, "/icons/accessory-unknown.png");

        emergencyStopIcon = ImageUtils.createImageIcon(StepControlPanel.class, "/icons/stepcontrol/emergency-stop.png");
    }

    private void firePerformAspect(int row) {
        LOGGER.info("Aspect is performed on row: {}", row);

        for (CvDefinitionRequestListener l : cvDefinitionRequestListeners) {
            l.activateAspect(row);
        }
    }

    private void firePerformSpeed() {
        LOGGER.info("Write the speed value.");
        int speed = stepControlModel.getSpeed();
        writeCvValue(KEYWORD_SPEED, speed);
    }

    private void firePerformAccel() {
        int accel = stepControlModel.getAccel();
        writeCvValue(KEYWORD_ACCEL, accel);
    }

    private void firePerformDecel() {
        int decel = stepControlModel.getDecel();
        writeCvValue(KEYWORD_DECEL, decel);
    }

    private void writeCvValue(String keyword, int modelValue) {
        try {
            final List<ConfigurationVariable> cvList = new LinkedList<>();

            // check the value
            CvNode speedNode = getNode(keyword);

            CvValueUtils.compareAndAddNewValue(speedNode, Integer.toString(modelValue), cvList,
                selectedNode.getCvNumberToJideNodeMap());

            LOGGER.info("Write CV value, keyword: {}, modelValue: {}, cvList: {}", keyword, modelValue, cvList);

            if (CollectionUtils.isNotEmpty(cvList)) {
                // write the config variables to the node
                fireWriteConfigVariables(cvList);
            }
            else {
                LOGGER.warn("No CV values to write found.");
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Set the new {} value failed.", keyword, ex);
        }
    }

    private void readCurrentValuesFromCV(final Node node) {
        LOGGER.info("Read the values from CV for node: {}", node);

        if (node == null || !ProductUtils.isStepControl(node.getUniqueId())) {
            LOGGER.warn("No node available or not a StepControl: {}", node);

            if (mapKeywordToNode != null) {
                mapKeywordToNode.clear();
            }

            return;
        }
        LOGGER.info("Get the cvDefinitionTreeTableModel from the node: {}", node);
        CvDefinitionTreeTableModel cvDefinitionTreeTableModel = node.getCvDefinitionTreeTableModel();
        if (cvDefinitionTreeTableModel != null) {
            // search the keywords
            mapKeywordToNode = new LinkedHashMap<>();

            DefaultExpandableRow rootNode = (DefaultExpandableRow) cvDefinitionTreeTableModel.getRoot();
            if (rootNode != null) {
                CvNodeUtils.harvestKeywordNodes(rootNode, mapKeywordToNode);
            }

            LOGGER.info("Found keywords in nodes: {}", mapKeywordToNode.keySet());

            // TODO load the speed range
            CvNode cvNodeSpeed = getNode(KEYWORD_SPEED);
            int maxSpeed = 2001;
            int minSpeed = 1;
            if (cvNodeSpeed != null) {
                try {
                    maxSpeed = Integer.parseInt(cvNodeSpeed.getCV().getMax()) + 1;
                }
                catch (Exception ex) {
                    LOGGER.warn("Get the max speed from CV defintion failed.", ex);
                }

                try {
                    minSpeed = Integer.parseInt(cvNodeSpeed.getCV().getMin());
                }
                catch (Exception ex) {
                    LOGGER.warn("Get the min speed from CV defintion failed.", ex);
                }
            }
            LOGGER.info("Use speed range, min: {}, max: {}", minSpeed, maxSpeed);

            BoundedRangeAdapter boundedRangeAdapter = new BoundedRangeAdapter(speedModel, 1, minSpeed, maxSpeed);
            speedSlider.setModel(boundedRangeAdapter);

        }
        else {
            LOGGER.warn("No cvDefinitionTreeTableModel available for node: {}", node);
        }

        // TODO make this more clever? only load values that are not loaded already? is this clever? how about changed
        // values?

        // read the values from the node
        if (MapUtils.isNotEmpty(mapKeywordToNode)) {
            List<ConfigurationVariable> configVariables = new LinkedList<>();
            for (CvNode cvNode : mapKeywordToNode.values()) {
                // LOGGER.info("Process cvNode: {}", cvNode);

                prepareConfigVariables(cvNode, configVariables, node.getCvNumberToJideNodeMap());
            }

            // keep the list of config variables
            this.requiredConfigVariables.clear();
            if (CollectionUtils.isNotEmpty(configVariables)) {
                this.requiredConfigVariables.addAll(configVariables);
            }

            // add the current position with the special CV
            prepareGetCurrentPosition(configVariables);

            fireLoadConfigVariables(configVariables);
        }
        else {
            LOGGER.warn("No values available in mapKeywordToNode!");
        }
    }

    private void prepareConfigVariables(
        final CvNode cvNode, final List<ConfigurationVariable> configVariables,
        final Map<String, CvNode> cvNumberToNodeMap) {

        try {
            switch (cvNode.getCV().getType()) {
                case LONG:
                    // LONG nodes are processed
                    LongCvNode masterNode = ((LongCvNode) cvNode).getMasterNode();
                    configVariables.add(masterNode.getConfigVar());
                    for (CvNode slaveNode : masterNode.getSlaveNodes()) {
                        configVariables.add(slaveNode.getConfigVar());
                    }
                    break;
                case INT:
                    // INT nodes are processed
                    configVariables.add(cvNode.getConfigVar());
                    int highCvNum = Integer.parseInt(cvNode.getCV().getHigh());
                    int cvNumber = Integer.parseInt(cvNode.getCV().getNumber());

                    if (highCvNum == cvNumber) {
                        // search the low CV
                        CvNode lowCvNode = cvNumberToNodeMap.get(cvNode.getCV().getLow());
                        configVariables.add(lowCvNode.getConfigVar());
                    }
                    else {
                        // search the high CV
                        CvNode highCvNode = cvNumberToNodeMap.get(cvNode.getCV().getHigh());
                        if (highCvNode == null) {
                            LOGGER.warn("The highCvNode is not available: {}", cvNode.getCV());
                        }
                        configVariables.add(highCvNode.getConfigVar());
                    }
                    break;
                default:
                    configVariables.add(cvNode.getConfigVar());
                    break;
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Prepare config variables to read from node failed.", ex);
        }

    }

    public void addCvDefinitionRequestListener(CvDefinitionRequestListener l) {
        cvDefinitionRequestListeners.add(l);
    }

    private void fireLoadConfigVariables(List<ConfigurationVariable> configVariables) {
        // TODO decouple the AWT-thread from this work?
        LOGGER.info("Load the config variables.");
        for (CvDefinitionRequestListener l : cvDefinitionRequestListeners) {
            l.loadCvValues(configVariables);
        }
    }

    private JComponent buildButtonBar(JButton... button) {
        return new ButtonBarBuilder().addGlue().addButton(button).build();
    }

    private JComponent buildLeftAlignedButtonBar(JComponent... button) {
        return new ButtonBarBuilder().addButton(button).addGlue().build();
    }

    public JPanel getComponent() {
        return component;
    }

    public String getName() {
        return Resources.getString(getClass(), "name");
    }

    @Override
    public void tabSelected(boolean selected) {
        LOGGER.info("Tab is selected: {}, initialized: {}", selected, initialized);
        // show the toolbar
        toolbarCvDefinition.setVisible(selected);

        // if the tab is selected and the initialized flag is false we must load the CV defintions
        if (selected) {
            triggerLoadCvValues();
        }

        if (!selected && mainModel.getSelectedNode() == null) {
            LOGGER.info("The tab is no longer selected and the selected node is null, reset the selected node!");

            selectedNode = null;
        }
    }

    public void triggerLoadCvValues() {
        final Node node = selectedNode;
        LOGGER.info("Load the CV values for node: {}, initialized: {}", node, initialized);

        if (!initialized && selectedNode != null) {

            statusBar.setStatus(Resources.getString(StepControlPanel.class, "loading_cvvalues"));

            // call this method after node was selected
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    readCurrentValuesFromCV(node);
                }
            });
        }
        else {
            LOGGER.info("Skip loading CV values because the initialized is true.");
        }
    }

    private void handleMouseEvent(MouseEvent e, AspectTablePopupMenu aspectTableMenu) {
        if (e.isPopupTrigger()) {
            int row = table.rowAtPoint(e.getPoint());
            int column = table.columnAtPoint(e.getPoint());
            showAspectTableMenu(e, aspectTableMenu, row, column);
        }
    }

    private void showAspectTableMenu(MouseEvent e, AspectTablePopupMenu aspectTableMenu, int row, int column) {

        Object value = null;
        if (row > -1) {
            table.setRowSelectionInterval(row, row);
            value = table.getValueAt(row, -1);
        }
        aspectTableMenu.setDeleteEnabled(value instanceof StepControlAspect);
        aspectTableMenu.setDeleteAllEnabled(table.getRowCount() > 0);

        aspectTableMenu.show(e.getComponent(), e.getX(), e.getY());
    }

    private final class AspectTablePopupMenu extends BasicPopupMenu {
        private static final long serialVersionUID = 1L;

        private JMenuItem newLabel;

        private JMenuItem deleteLabel;

        private JMenuItem deleteAllLabel;

        public AspectTablePopupMenu() {
            newLabel = new JMenuItem(Resources.getString(StepControlPanel.class, "newAspect") + " ...");
            newLabel.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    if (stepControlModel.getStepControlAspects().size() < MAX_CONFIGURED_ASPECTS) {
                        int stepNumber = table.getRowCount() + 1;
                        StepControlAspect aspect = new StepControlAspect(stepNumber, 0, Polarity.normal);

                        stepControlModel.addStepControlAspect(aspect);
                    }
                    else {
                        LOGGER.warn("Maximum number of aspects reached.");
                        JOptionPane.showMessageDialog(component,
                            Resources.getString(StepControlPanel.class, "max_aspect_message"),
                            Resources.getString(StepControlPanel.class, "max_aspect_title"), JOptionPane.ERROR_MESSAGE);
                    }

                }
            });
            add(newLabel);

            deleteLabel = new JMenuItem(Resources.getString(StepControlPanel.class, "deleteAspect") + " ...");
            deleteLabel.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    int selectedRow = table.getSelectedRow();
                    if (selectedRow > -1) {

                        Object aspect = table.getValueAt(selectedRow, -1);
                        if (aspect instanceof StepControlAspect) {
                            stepControlModel.removeStepControlAspect((StepControlAspect) aspect);
                        }
                    }
                }
            });
            add(deleteLabel);

            deleteAllLabel = new JMenuItem(Resources.getString(StepControlPanel.class, "deleteAllAspects") + " ...");
            deleteAllLabel.setToolTipText(Resources.getString(StepControlPanel.class, "deleteAllAspects.tooltip"));
            deleteAllLabel.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    int totalRows = table.getRowCount();
                    if (totalRows > 0) {

                        // show a messagebox to check if the user really wants to delete all aspects
                        boolean continueDeleteAspects =
                            TaskDialogs
                                .build(JOptionPane.getFrameForComponent(component),
                                    Resources.getString(StepControlPanel.class, "deleteAllAspects-confirm.instruction"),
                                    Resources.getString(StepControlPanel.class, "deleteAllAspects-confirm"))
                                .title(Resources.getString(StepControlPanel.class, "deleteAllAspects-confirm.title"))
                                .isConfirmed();
                        LOGGER.info("The user confirmed to continue delete all aspects: {}", continueDeleteAspects);

                        if (continueDeleteAspects) {
                            while (totalRows > 0) {
                                Object aspect = table.getValueAt(totalRows - 1, -1);
                                LOGGER.info("Fetched row: {}, aspect: {}", totalRows, aspect);
                                if (aspect instanceof StepControlAspect) {
                                    stepControlModel.removeStepControlAspect((StepControlAspect) aspect);
                                }
                                totalRows--;
                            }
                        }
                    }
                }
            });
            add(deleteAllLabel);
        }

        public void setDeleteEnabled(boolean enabled) {
            deleteLabel.setEnabled(enabled);
        }

        public void setDeleteAllEnabled(boolean enabled) {
            deleteAllLabel.setEnabled(enabled);
        }
    }

    private void fireCreateNewAspect() {

        if (stepControlModel.getStepControlAspects().size() < MAX_CONFIGURED_ASPECTS) {

            // create new aspect using the 'actual position'
            Long position =
                (Long) stepControlPresentationModel.getValue(StepControlModel.PROPERTYNAME_CURRENT_POSITION);
            if (position == null) {
                position = 0L;
            }
            StepControlAspect stepControlAspect = new StepControlAspect(null, position, Polarity.normal);

            stepControlModel.addStepControlAspect(stepControlAspect);

            writeAndReselectAspect(stepControlAspect);
        }
        else {
            LOGGER.warn("Maximum number of aspects reached.");
            JOptionPane.showMessageDialog(component, Resources.getString(StepControlPanel.class, "max_aspect_message"),
                Resources.getString(StepControlPanel.class, "max_aspect_title"), JOptionPane.ERROR_MESSAGE);
        }
    }

    private void fireUpdateAspect() {

        int selectedRow = table.getSelectedRow();
        LOGGER.info("The selected row: {}", selectedRow);

        if (selectedRow > -1) {
            int index = table.getRowSorter().convertRowIndexToModel(selectedRow);
            LOGGER.info("The selected index: {}", index);

            StepControlAspect stepControlAspect = aspectSelection.getElementAt(index);

            LOGGER.info("Current selected aspect: {}", stepControlAspect);

            if (stepControlAspect != null) {
                Long position =
                    (Long) stepControlPresentationModel.getValue(StepControlModel.PROPERTYNAME_CURRENT_POSITION);
                if (position == null) {
                    position = 0L;
                }
                stepControlAspect.setPosition(position);

                writeAndReselectAspect(stepControlAspect);

                table.getRowSorter().allRowsChanged();
            }
            else {
                LOGGER.info("No aspect selected.");
            }
        }
        else {
            LOGGER.info("No row selected.");
        }
    }

    private void writeAndReselectAspect(final StepControlAspect stepControlAspect) {
        aspectSelection.setSelection(stepControlAspect);

        prepareAndWriteCvValuesToNode(true);

        // The list was loaded completely new. Get the new aspect from the aspectSelection.
        StepControlAspect createdAspect =
            stepControlModel
                .getStepControlAspects().stream()
                .filter(aspect -> aspect.getPosition() == stepControlAspect.getPosition()).findFirst().orElse(null);
        LOGGER.info("Fetched createdAspect from model: {}", createdAspect);
        aspectSelection.setSelection(createdAspect);

        int index = aspectSelection.getSelectionIndex();
        LOGGER.info("The new aspect has index: {}", index);

        LOGGER.info("Select the current aspect after write CV values to node, index: {}", index);
        setSelectedIndex(index);
    }

    private int setSelectedIndex(int index) {
        index = table.getRowSorter().convertRowIndexToView(index);
        LOGGER.info("Set the selection interval, index: {}", index);
        table.getSelectionModel().setSelectionInterval(index, index);

        if (true) {
            Rectangle cellRect = table.getCellRect(index, 0, false);
            if (cellRect != null) {
                table.scrollRectToVisible(cellRect);
            }
        }

        return index;
    }

    private void fireConfiguration() {
        LOGGER.info("Open the configuration wizard.");

        try {
            ConfigurationWizardModel configurationWizardModel = new ConfigurationWizardModel();
            configurationWizardModel.setTurnTableType(TurnTableType.linear);

            prepareConfigurationModel(configurationWizardModel);
            ConfigurationWizard wizard = new ConfigurationWizard(configurationWizardModel);

            wizard.showWizard();

            if (configurationWizardModel.getWizardStatus() == WizardStatus.finished) {
                // transfer the new values to the node
                writeConfigurationValues(configurationWizardModel);
            }
            else {
                LOGGER.info("The configurationWizardModel is not available.");
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Show configuration wizard failed.", ex);
            ErrorInfo info =
                new ErrorInfo("BiDiB-Wizard", "Open configuration dialog for Step Control failed.", null, "ERROR", ex,
                    Level.WARNING, null);
            JXErrorPane.showDialog(null, info);
        }
    }

    /**
     * The CV definition of the node has changed.
     */
    public void cvDefinitionChanged() {

        // the cvDefinitionChanged is misused to change the selected node

        LOGGER.info("The cv definition has changed, selected node: {}", selectedNode);

        if (selectedNode != null && selectedNode.equals(mainModel.getSelectedNode())) {
            LOGGER.info("The node in the model has not changed.");
            return;
        }

        // TODO the motor ports are moved to node
        mainModel.removeMotorPortValueListener(this);

        // force reload of CV values
        initialized = false;

        if (readCvButton != null) {
            readCvButton.setEnabled(false);
        }
        if (writeCvButton != null) {
            writeCvButton.setEnabled(false);
        }

        // set the selected node
        selectedNode = mainModel.getSelectedNode();

        // clear the aspects
        stepControlModel.clearModel();
        requiredConfigVariables.clear();

        if (mapKeywordToNode != null) {
            LOGGER.info("Clear the mapKeywordToNode.");
            mapKeywordToNode.clear();
        }
        //
        // // force reload of CV values
        // initialized = false;

        // reset the pending changes flag on the tab
        resetPendingChanges();

        if (selectedNode != null) {
            if (readCvButton != null) {
                // enable the read button to get the values of the configuration variables for this node on
                // request
                readCvButton.setEnabled(CollectionUtils.isNotEmpty(selectedNode.getConfigVariables()));
            }
            if (writeCvButton != null) {
                // enable the write button to write the values of the configuration variables for this node on
                // request
                writeCvButton.setEnabled(CollectionUtils.isNotEmpty(selectedNode.getConfigVariables()));
            }
        }

        // load the aspects initially
        if (selectedNode != null && ProductUtils.isStepControl(selectedNode.getUniqueId())) {
            LOGGER.info("The currently selected node is a StepControl.");

            // TODO the motor ports are moved to node
            mainModel.addMotorPortValueListener(this);

            fieldsToUpdate.add(KEYWORD_CONFIGURED_ASPECTS);
        }
    }

    /**
     * The values of the current CV definition have changed. Get the cv values and update the stepControlModel.
     */
    public void cvDefinitionValuesChanged(final boolean read) {
        LOGGER.info("The cv defintion values have changed, read: {}", read);

        boolean errorDetected = false;

        if (selectedNode == null || !ProductUtils.isStepControl(selectedNode.getUniqueId())) {
            LOGGER.info("The currently selected node is not a StepControl.");
            return;
        }

        if (fieldsToUpdate.contains(CVKEY_CURRENT_POSITION)) {
            try {
                fieldsToUpdate.remove(CVKEY_CURRENT_POSITION);

                long currentPosition = getConfigVarLongValueFromStringByCvKey(CVKEY_CURRENT_POSITION);
                stepControlModel.setCurrentPosition(currentPosition);

                LOGGER.info(
                    "The current position was read and no more CV values are updated! The current position is: {}",
                    currentPosition);

            }
            catch (Exception ex) {
                LOGGER.warn("Update the current position failed.", ex);
            }
            // return;
        }

        if (MapUtils.isEmpty(mapKeywordToNode)) {
            LOGGER.info("No mapKeywordToNode value available.");
            return;
        }

        try {
            Long totalSteps = getConfigVarLongValue(KEYWORD_TOTALSTEPS);
            if (angleRenderer != null) {
                angleRenderer.setTotalSteps(totalSteps);
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Set the total step count failed.", ex);
            errorDetected = true;
        }

        try {
            Integer value = getConfigVarIntValue(KEYWORD_TABLETYPE);
            TurnTableType turnTableType = TurnTableType.fromValue(ByteUtils.getLowByte(value));
            stepControlModel.setTurnTableType(turnTableType);
        }
        catch (Exception ex) {
            LOGGER.warn("Set the turnTableType failed.", ex);
            errorDetected = true;
        }

        if (read) {
            LOGGER.info("The values were read, update the aspects in the table.");
            fieldsToUpdate.add(KEYWORD_CONFIGURED_ASPECTS);
        }

        if (fieldsToUpdate.contains(KEYWORD_CONFIGURED_ASPECTS)) {
            LOGGER.info("Prepare the configured aspects.");

            // create the list of aspects with the values from the config variables
            List<StepControlAspect> configuredAspects = new LinkedList<>();
            for (int index = 0; index < MAX_CONFIGURED_ASPECTS; index++) {
                try {
                    Integer targetPolarity = getConfigVarIntValue(String.format(KEYWORD_PATTERN_POLARITY, index));
                    Long targetPosition = getConfigVarLongValue(String.format(KEYWORD_PATTERN_POSITION, index));

                    if (targetPosition < INACTIVE_POSITION_VALUE) {

                        StepControlAspect stepControlAspect =
                            new StepControlAspect(null, targetPosition.longValue(),
                                Polarity.valueOf(targetPolarity.intValue()));
                        LOGGER.info("Adding new stepControlAspect: {}", stepControlAspect);
                        configuredAspects.add(stepControlAspect);
                    }
                    else {
                        LOGGER.info("No active position at index {}, skip further creation of aspects, position: {}",
                            index, targetPosition);
                        break;
                    }
                }
                catch (IllegalArgumentException ex) {
                    LOGGER.warn("Prepare configured step control aspect failed: {}", ex.getMessage());
                    errorDetected = true;
                }
                catch (Exception ex) {
                    LOGGER.warn("Prepare configured step control aspect failed.", ex);
                    errorDetected = true;
                }
            }
            stepControlModel.setStepControlAspects(configuredAspects);
            fieldsToUpdate.remove(KEYWORD_CONFIGURED_ASPECTS);
        }

        try {
            Integer speed = getConfigVarIntValue(KEYWORD_SPEED);
            stepControlModel.setSpeed(speed);
        }
        catch (Exception ex) {
            LOGGER.warn("Set the speed failed.", ex);
            errorDetected = true;
        }

        try {
            Integer accel = getConfigVarIntValue(KEYWORD_ACCEL);
            stepControlModel.setAccel(accel);
        }
        catch (Exception ex) {
            LOGGER.warn("Set the accel failed.", ex);
            errorDetected = true;
        }

        try {
            Integer decel = getConfigVarIntValue(KEYWORD_DECEL);
            stepControlModel.setDecel(decel);
        }
        catch (Exception ex) {
            LOGGER.warn("Set the decel failed.", ex);
            errorDetected = true;
        }

        try {
            Integer pushInterval = getConfigVarByteValue(KEYWORD_PUSH_INTERVAL);
            stepControlModel.setPushInterval(pushInterval);
        }
        catch (Exception ex) {
            LOGGER.warn("Set the push interval failed.", ex);
            errorDetected = true;
        }

        if (!initialized && !errorDetected) {

            initialized = true;

            LOGGER.info("The panel was initialized. Reset the pending changes flag on the tab.");
            tabStatusListener.updatePendingChanges(component, false);

            // TODO re-create the list of aspects
        }
        else if (read) {
            LOGGER.info("The CV values were read from the node. Reset the pending changes flag on the tab.");
            tabStatusListener.updatePendingChanges(component, false);

            // TODO re-create the list of aspects
        }

    }

    private CvNode getNode(String keyword) {
        CvNode cvNode = mapKeywordToNode.get(keyword);
        return cvNode;
    }

    private Integer getConfigVarByteValue(String keyword) {
        CvNode cvNode = getNode(keyword);

        Integer value = CvValueUtils.getConfigVarByteValue(cvNode, selectedNode.getCvNumberToJideNodeMap());
        return value;
    }

    private Integer getConfigVarIntValue(String keyword) {
        CvNode cvNode = getNode(keyword);
        Integer value = CvValueUtils.getConfigVarIntValue(cvNode, selectedNode.getCvNumberToJideNodeMap());
        return value;
    }

    private Long getConfigVarLongValue(String keyword) {
        CvNode cvNode = getNode(keyword);
        Long value = CvValueUtils.getConfigVarLongValue(cvNode);
        return value;
    }

    private Long getConfigVarLongValueFromStringByCvKey(String cvKey) {
        LOGGER.info("Get the cvNode for CV key: {}", cvKey);

        CvNode cvNode = selectedNode.getCvNumberToJideNodeMap().get(cvKey);

        // get the value as string and parse the value to long
        String value = CvValueUtils.getConfigVarStringValue(cvNode);
        long longValue = Long.parseLong(value);
        return longValue;
    }

    public void executionStateChanged(
        AccessoryExecutionState executionState, int accessoryId, int aspect, AccessoryState accessoryState) {
        LOGGER.info("The execution state has changed: {}, accessoryId: {}, aspect: {}", executionState, accessoryId,
            aspect);
        if (aspect == -1 || executionState == null) {
            executionState = AccessoryExecutionState.IDLE;
        }

        executionStateIconLabel.setToolTipText(null);
        switch (executionState) {
            case ERROR:
                executionStateIconLabel.setIcon(accessoryErrorIcon);
                executionStateIconLabel
                    .setText(Resources.getString(StepControlPanel.class, "unknown_error_aspect", aspect));
                if (accessoryState != null) {
                    executionStateIconLabel.setToolTipText(accessoryState.getErrorInformation());
                }
                break;
            case RUNNING:
                executionStateIconLabel.setIcon(accessoryWaitIcon);
                executionStateIconLabel
                    .setText(Resources.getString(StepControlPanel.class, "executing_aspect", aspect));
                break;
            case SUCCESSFUL:
                executionStateIconLabel.setIcon(accessorySuccessfulIcon);
                executionStateIconLabel.setText(Resources.getString(StepControlPanel.class, "active_aspect", aspect));
                break;
            case UNKNOWN:
                executionStateIconLabel.setIcon(accessoryUnknownIcon);
                executionStateIconLabel.setText(null);
                break;
            default:
                executionStateIconLabel.setIcon(null);
                executionStateIconLabel.setText(null);
                break;
        }

    }

    static class LongRenderer extends DefaultTableCellRenderer.UIResource {
        private static final long serialVersionUID = 1L;

        private NumberFormat formatter;

        public LongRenderer() {
            super();
            setHorizontalAlignment(JLabel.RIGHT);
        }

        public void setValue(Object value) {
            if (formatter == null) {
                formatter = new DecimalFormat("#");
            }

            if (value instanceof Number) {
                long sourceValue = ((Number) value).longValue();
                long lValue = ((long) sourceValue) & 0xffffffffL;

                setText(formatter.format(lValue));
            }
            else {
                setText(null);
            }
        }
    }

    private void addButtons(VLToolBar toolBar) {

        // read CV button
        readCvButton =
            makeNavigationButton("load-from-node", "/32x32", READ, Resources.getString(getClass(), "toolbar.readallcv"),
                Resources.getString(getClass(), "toolbar.readallcv.alttext"));
        readCvButton.setEnabled(false);
        toolBar.add(readCvButton);

        readCvButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                readCurrentValuesFromCV(selectedNode);
            }
        });

        // write CV button
        writeCvButton =
            makeNavigationButton("save-to-node", "/32x32", WRITE, Resources.getString(getClass(), "toolbar.writeallcv"),
                Resources.getString(getClass(), "toolbar.writeallcv.alttext"));
        toolBar.add(writeCvButton);
        writeCvButton.setEnabled(false);

        writeCvButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                prepareAndWriteCvValuesToNode(false);
            }
        });
    }

    private void prepareAndWriteCvValuesToNode(boolean writeOnlyAspects) {

        LOGGER.info("Write the CV values.");

        // prepare the cv list to write
        final List<ConfigurationVariable> cvList = new LinkedList<>();

        // collect the new values
        int aspectIndex = 0;

        List<StepControlAspect> aspects = new LinkedList<>();
        // get the aspects from the model
        aspects.addAll(stepControlModel.getStepControlAspects());

        Collections.sort(aspects, new Comparator<StepControlAspect>() {

            @Override
            public int compare(StepControlAspect o1, StepControlAspect o2) {
                return Long.compare(o1.getPosition(), o2.getPosition());
            }
        });

        // check the aspects
        for (StepControlAspect currentAspect : aspects) {
            LOGGER.info("Prepare aspect to save: {}", currentAspect);

            prepareCvValues(aspectIndex, currentAspect, cvList);

            aspectIndex++;
        }

        // add the terminating aspects by setting 0xFFFF as position
        if (aspectIndex < MAX_CONFIGURED_ASPECTS) {
            StepControlAspect currentAspect = new StepControlAspect(null, INACTIVE_POSITION_VALUE, Polarity.normal);
            prepareCvValues(aspectIndex, currentAspect, cvList);

            aspectIndex++;
        }

        if (!writeOnlyAspects) {
            // check the turntable type
            CvNode tableTypeNode = getNode(KEYWORD_TABLETYPE);
            TurnTableType turnTableType = stepControlModel.getTurnTableType();
            CvValueUtils.compareAndAddNewValue(tableTypeNode, Integer.toString(turnTableType.getCvValue()), cvList,
                selectedNode.getCvNumberToJideNodeMap());

            // check the speed
            CvNode speedNode = getNode(KEYWORD_SPEED);
            int speed = stepControlModel.getSpeed();
            CvValueUtils.compareAndAddNewValue(speedNode, Integer.toString(speed), cvList,
                selectedNode.getCvNumberToJideNodeMap());

            // check the accel
            CvNode accelNode = getNode(KEYWORD_ACCEL);
            int accel = stepControlModel.getAccel();
            CvValueUtils.compareAndAddNewValue(accelNode, Integer.toString(accel), cvList,
                selectedNode.getCvNumberToJideNodeMap());

            // check the decel
            CvNode decelNode = getNode(KEYWORD_DECEL);
            int decel = stepControlModel.getDecel();
            CvValueUtils.compareAndAddNewValue(decelNode, Integer.toString(decel), cvList,
                selectedNode.getCvNumberToJideNodeMap());
        }
        else {
            LOGGER.info("Only write aspectst to node.");
        }

        // write the values on the node
        CvValueUtils.writeCvValues(selectedNode, cvList, selectedNode.getCvNumberToJideNodeMap(),
            StepControlPanel.this);
    }

    private void prepareConfigurationModel(final ConfigurationWizardModel configurationWizardModel) {

        configurationWizardModel.setTurnTableType(stepControlModel.getTurnTableType());

        Integer nemaType = getConfigVarByteValue(KEYWORD_NEMATYPE);
        MotorSizeType motorSizeType = MotorSizeType.fromValue(ByteUtils.getLowByte(nemaType));
        configurationWizardModel.setMotorSizeType(motorSizeType);

        Integer stepCount = getConfigVarIntValue(KEYWORD_STEPCOUNT);
        configurationWizardModel.setStepCount(stepCount);

        Gearing gearing = null;
        Integer gearPrimary = getConfigVarByteValue(KEYWORD_GEAR_PRIMARY);
        Integer gearSecondary = getConfigVarIntValue(KEYWORD_GEAR_SECONDARY);
        if (gearPrimary == null || gearPrimary.intValue() < 1) {
            LOGGER.info("No gearing defined because primary value = 0.");
        }
        else if ((gearPrimary != null && gearPrimary.intValue() > 1)
            || (gearSecondary != null && gearSecondary.intValue() > 1)) {
            gearing = new Gearing(Gearing.YES);

            gearing.setGearRatioPrimary(gearPrimary != null ? gearPrimary : 1);
            gearing.setGearRatioSecondary(gearSecondary != null ? gearSecondary : 1);
            gearing.setBackLash(0);
        }

        Integer backlash = getConfigVarIntValue(KEYWORD_BACKLASH);
        if (backlash != null && backlash.intValue() > 0) {
            // LOGGER.info("Create gearing because the backlash value is available.");
            // if (gearing == null) {
            // gearing = new Gearing(Gearing.YES);
            // gearing.setGearRatioPrimary(1);
            // gearing.setGearRatioSecondary(1);
            // }
            if (gearing != null) {
                LOGGER.info("Set the backlash value.");
                gearing.setBackLash(backlash);
            }
            else {
                LOGGER.info("No gearing available to set the backlash value.");
            }
        }

        if (gearing == null) {
            LOGGER.info("No gearing defined.");
            gearing = new Gearing(Gearing.NO);
        }
        configurationWizardModel.setGearing(gearing);

        Integer microStepping = getConfigVarByteValue(KEYWORD_MICROSTEPPING);
        if (microStepping == null || microStepping.intValue() < 64) {
            LOGGER.warn("Set the default microstepping value of 64!");
            microStepping = Integer.valueOf(64);
        }
        configurationWizardModel.setMicroStepping(microStepping);

    }

    /**
     * Write the CV values provided by the configurationWizardModel.
     * 
     * @param configurationWizardModel
     *            the configuration wizard model
     */
    private void writeConfigurationValues(final ConfigurationWizardModel configurationWizardModel) {
        LOGGER.info("Write the values from the configuration wizard model to the node.");

        // prepare the cv list to write
        final List<ConfigurationVariable> cvList = new LinkedList<>();

        // check the turntable type
        CvNode tableTypeNode = getNode(KEYWORD_TABLETYPE);
        TurnTableType turnTableType = configurationWizardModel.getTurnTableType();
        stepControlModel.setTurnTableType(turnTableType);
        CvValueUtils.compareAndAddNewValue(tableTypeNode, Integer.toString(turnTableType.getCvValue()), cvList,
            selectedNode.getCvNumberToJideNodeMap());

        // check the motor type
        CvNode nemaTypeNode = getNode(KEYWORD_NEMATYPE);
        MotorSizeType motorSizeType = configurationWizardModel.getMotorSizeType();
        CvValueUtils.compareAndAddNewValue(nemaTypeNode, Integer.toString(motorSizeType.getCvValue()), cvList,
            selectedNode.getCvNumberToJideNodeMap());

        // prepare the current moving and stopped values
        CvNode currentMovingNode = getNode(KEYWORD_CURRENT_MOVING);
        // the values are stored in 10mA units
        int currentMoving = motorSizeType.getCurrentMoving();
        CvValueUtils.compareAndAddNewValue(currentMovingNode, Integer.toString(currentMoving / 10), cvList,
            selectedNode.getCvNumberToJideNodeMap());
        // prepare the current moving and stopped values
        CvNode currentStoppedNode = getNode(KEYWORD_CURRENT_STOPPED);
        // the values are stored in 10mA units
        int currentStopped = motorSizeType.getCurrentStopped();
        CvValueUtils.compareAndAddNewValue(currentStoppedNode, Integer.toString(currentStopped / 10), cvList,
            selectedNode.getCvNumberToJideNodeMap());

        // check the step count
        CvNode stepCountNode = getNode(KEYWORD_STEPCOUNT);
        Integer stepCount = configurationWizardModel.getStepCount();
        CvValueUtils.compareAndAddNewValue(stepCountNode, Integer.toString(stepCount), cvList,
            selectedNode.getCvNumberToJideNodeMap());

        // check the gear primary
        CvNode gearPrimaryNode = getNode(KEYWORD_GEAR_PRIMARY);
        Integer gearRatioPrimary = configurationWizardModel.getGearing().getGearRatioPrimary();
        CvValueUtils.compareAndAddNewValue(gearPrimaryNode, Integer.toString(gearRatioPrimary), cvList,
            selectedNode.getCvNumberToJideNodeMap());

        // check the gear secondary
        CvNode gearSecondaryNode = getNode(KEYWORD_GEAR_SECONDARY);
        Integer gearRatioSecondary = configurationWizardModel.getGearing().getGearRatioSecondary();
        CvValueUtils.compareAndAddNewValue(gearSecondaryNode, Integer.toString(gearRatioSecondary), cvList,
            selectedNode.getCvNumberToJideNodeMap());

        // check the total steps
        LongCvNode totalStepsNode = (LongCvNode) getNode(KEYWORD_TOTALSTEPS);
        Integer totalStepCount = configurationWizardModel.getTotalStepCount();
        CvValueUtils.compareAndAddNewValue(totalStepsNode, totalStepCount, cvList);

        // prepare the gearing
        if (Gearing.YES.equals(configurationWizardModel.getGearing().getKey())) {
            CvNode backlashNode = getNode(KEYWORD_BACKLASH);
            Integer backlash = configurationWizardModel.getGearing().getBackLash();
            CvValueUtils.compareAndAddNewValue(backlashNode, Integer.toString(backlash), cvList,
                selectedNode.getCvNumberToJideNodeMap());
        }
        else {
            LOGGER.info("No gearing selected.");

            CvNode backlashNode = getNode(KEYWORD_BACKLASH);
            Integer backlash = 0;
            CvValueUtils.compareAndAddNewValue(backlashNode, Integer.toString(backlash), cvList,
                selectedNode.getCvNumberToJideNodeMap());
        }

        // check the microstepping
        CvNode microSteppingNode = getNode(KEYWORD_MICROSTEPPING);
        Integer microStepping = configurationWizardModel.getMicroStepping();
        CvValueUtils.compareAndAddNewValue(microSteppingNode, Integer.toString(microStepping), cvList,
            selectedNode.getCvNumberToJideNodeMap());

        // write the values on the node
        CvValueUtils.writeCvValues(selectedNode, cvList, selectedNode.getCvNumberToJideNodeMap(),
            StepControlPanel.this);

        // update the renderer
        try {
            Long totalSteps = getConfigVarLongValue(KEYWORD_TOTALSTEPS);
            if (angleRenderer != null) {
                angleRenderer.setTotalSteps(totalSteps);
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Set the total step count failed.", ex);
        }

    }

    private void prepareCvValues(
        int aspectIndex, StepControlAspect currentAspect, final List<ConfigurationVariable> cvList) {

        // prepare the keyword to search
        String keywordPolarity = String.format(KEYWORD_PATTERN_POLARITY, aspectIndex);
        CvNode polarityNode = getNode(keywordPolarity);

        Polarity polarity = currentAspect.getPolarity();
        CvValueUtils.compareAndAddNewValue(polarityNode, Integer.toString(polarity.getCvValue()), cvList,
            selectedNode.getCvNumberToJideNodeMap());

        // the target position is a long type
        String keywordPosition = String.format(KEYWORD_PATTERN_POSITION, aspectIndex);
        // get the master
        LongCvNode positionNode = (LongCvNode) getNode(keywordPosition);

        long position = currentAspect.getPosition();
        // prepare the master value
        LOGGER.info("The new position is: {}", position);

        CvValueUtils.compareAndAddNewValue(positionNode, Long.toString(position), cvList,
            selectedNode.getCvNumberToJideNodeMap());
    }

    private JButton makeNavigationButton(
        String imageName, String pathExt, String actionCommand, String toolTipText, String altText) {
        // Look for the image.
        String imgLocation = "/icons/" + imageName + ".png";
        if (pathExt != null) {
            imgLocation = "/icons" + pathExt + "/" + imageName + ".png";
        }
        URL imageURL = StepControlPanel.class.getResource(imgLocation);

        // Create and initialize the button.
        JButton button = new JButton();
        button.setActionCommand(actionCommand);
        button.setToolTipText(toolTipText);

        if (imageURL != null) { // image found
            button.setIcon(new ImageIcon(imageURL, altText));
        }
        else { // no image found
            button.setText(altText);
            LOGGER.warn("Resource not found: {}", imgLocation);
        }

        return button;
    }

    private void addToolBar(final VLToolBar toolBar, ToolBarConstraints constraints) {
        ToolBarPanel topToolBarPanel =
            (ToolBarPanel) DefaultApplicationContext.getInstance().get(DefaultApplicationContext.KEY_TOPTOOLBARPANEL);
        topToolBarPanel.add(toolBar, constraints);
    }

    private void removeToolBar(final VLToolBar toolBar) {
        ToolBarPanel topToolBarPanel =
            (ToolBarPanel) DefaultApplicationContext.getInstance().get(DefaultApplicationContext.KEY_TOPTOOLBARPANEL);
        topToolBarPanel.remove(toolBar);
    }

    @Override
    public void checkPendingChanges() {
        boolean hasPendingChanges = hasPendingChanges();
        if (!hasPendingChanges) {
            // tabStatusListener.updatePendingChanges(component, false);
            resetPendingChanges();
        }
    }

    private void resetPendingChanges() {
        LOGGER.info("Reset pending changes.");
        tabStatusListener.updatePendingChanges(component, false);
    }

    private boolean hasPendingChanges() {
        // TODO implement hasPendingChanges()
        return false;
    }

    @Override
    public void writeConfigVariables(List<ConfigurationVariable> cvList) {
        fireWriteConfigVariables(cvList);
    }

    private void fireWriteConfigVariables(List<ConfigurationVariable> cvList) {
        // TODO decouple the AWT-thread from this work?

        // add line
        CvConsoleModel cvConsoleModel = CvConsoleModel.getConsoleModel();
        for (ConfigurationVariable cv : cvList) {
            cvConsoleModel.addConsoleLine(Color.black, String.format(">> CV %s : %s", cv.getName(), cv.getValue()));
        }

        for (CvDefinitionRequestListener l : cvDefinitionRequestListeners) {
            l.writeCvValues(cvList);
        }
    }

    @Override
    public void refreshDisplayedValues() {

    }

    @Override
    public void valueChanged(Port<MotorPortStatus> port) {
        // TODO Auto-generated method stub
        LOGGER.info("The motor port value has changed: {}", port);

        if (port.getId() == 0) {
            MotorPort motorPort = (MotorPort) port;
            motorSliderEditor.setValue(motorPort.getValue());
        }
    }

    private JPanel createFunctionButtonPanel(final JPanel parentPanel, final List<JideButton> functionButtons) {

        DefaultFormBuilder formBuilder =
            new DefaultFormBuilder(new FormLayout(
                "pref, 8dlu, pref, 8dlu, pref, 8dlu, pref, 8dlu, pref, 8dlu, pref, 8dlu, pref, 8dlu, pref, 8dlu, pref, 8dlu, pref"),
                parentPanel);

        formBuilder.appendSeparator(Resources.getString(getClass(), "additionalFunctions"));

        int totalSoundPorts = 0;
        if (selectedNode != null) {
            Feature feature =
                Feature.findFeature(selectedNode.getNode().getFeatures(),
                    FeatureEnum.FEATURE_CTRL_SOUND_COUNT.getNumber());
            if (feature != null) {
                totalSoundPorts = feature.getValue();
            }
        }
        LOGGER.info("Total number of sound ports: {}", totalSoundPorts);

        final int columns = 10;
        int currentSoundPortId = 0;
        for (int row = 0; row < 4; row++) {

            for (int column = 0; column < columns; column++) {
                final int functionIndex = row * columns + (column);
                final String buttonText = "F" + functionIndex;
                final JideButton functionButton = new JideButton(buttonText);
                functionButton.setButtonStyle(JideButton.TOOLBOX_STYLE);

                functionButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        LOGGER.info("Trigger sound port with index: {}", functionIndex);
                        SoundPortStatus soundPortStatus = SoundPortStatus.PLAY;

                        stepControlController.triggerSoundPort(functionIndex, soundPortStatus);
                    }
                });
                functionButtons.add(functionButton);

                functionButtonMap.put(buttonText, functionButton);

                formBuilder.append(functionButton);

                currentSoundPortId++;
                if (currentSoundPortId >= totalSoundPorts) {
                    LOGGER.info("Maximum number of sound ports reached.");
                    break;
                }
            }

            if (currentSoundPortId >= totalSoundPorts) {
                LOGGER.info("Maximum number of sound ports reached.");
                break;
            }
        }

        return formBuilder.build();
    }

}
