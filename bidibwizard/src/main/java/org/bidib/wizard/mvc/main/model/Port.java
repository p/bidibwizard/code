package org.bidib.wizard.mvc.main.model;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.PortConfigStatus;
import org.bidib.jbidibc.core.port.PortConfigValue;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.common.locale.Resources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.beans.Model;

public abstract class Port<T extends BidibStatus> extends Model {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(Port.class);

    static {
        try {
            // Q: why is this needed? A: export of beans with XMLDecoder
            PropertyDescriptor[] descriptor = Introspector.getBeanInfo(Port.class).getPropertyDescriptors();

            for (int i = 0; i < descriptor.length; i++) {
                PropertyDescriptor propertyDescriptor = descriptor[i];
                if (propertyDescriptor.getName().equals("status")) {
                    propertyDescriptor.setValue("transient", Boolean.TRUE);
                }
                else if (propertyDescriptor.getName().equals("enabed")) {
                    propertyDescriptor.setValue("transient", Boolean.TRUE);
                }
                else if (propertyDescriptor.getName().equals("genericPort")) {
                    propertyDescriptor.setValue("transient", Boolean.TRUE);
                }
                else if (propertyDescriptor.getName().equals("portConfigEnabled")) {
                    propertyDescriptor.setValue("transient", Boolean.TRUE);
                }
                else if (propertyDescriptor.getName().equals("portConfig")) {
                    propertyDescriptor.setValue("transient", Boolean.TRUE);
                }
                else if (propertyDescriptor.getName().equals("portConfigX")) {
                    propertyDescriptor.setValue("transient", Boolean.TRUE);
                }
                else if (propertyDescriptor.getName().equals("configStatus")) {
                    propertyDescriptor.setValue("transient", Boolean.TRUE);
                }
                else if (propertyDescriptor.getName().equals("remappingEnabled")) {
                    propertyDescriptor.setValue("transient", Boolean.TRUE);
                }
                else if (propertyDescriptor.getName().equals("isInactive")) {
                    propertyDescriptor.setValue("transient", Boolean.TRUE);
                }
                else if (propertyDescriptor.getName().equals("knownPortConfigKeys")) {
                    propertyDescriptor.setValue("transient", Boolean.TRUE);
                }
            }
        }
        catch (IntrospectionException e) {
            throw new RuntimeException(e);
        }
    }

    public static final String PROPERTY_CONFIG_LOADED = "configLoaded";

    public static final String PROPERTY_PORT_CONFIG_ERRORCODE = "portConfigErrorCode";

    private int id;

    private boolean enabled = true;

    private boolean remappingEnabled = false;

    private String portIdentifier;

    private String label;

    private T status;

    private PortConfigStatus configStatus = PortConfigStatus.CONFIG_PENDING;

    private Integer portConfigErrorCode;

    protected transient GenericPort genericPort;

    private transient Set<Byte> knownPortConfigKeys = new LinkedHashSet<>();

    // TODO: the port config is enabled by default, change to false
    private boolean portConfigEnabled = true;

    private boolean isInactive;

    protected Port(GenericPort genericPort) {
        this.genericPort = genericPort;
    }

    public GenericPort getGenericPort() {
        return genericPort;
    }

    /**
     * @return the port type
     */
    protected abstract LcOutputType getPortType();

    /**
     * @param port
     *            the port
     * @return the port type
     */
    public static LcOutputType getPortType(Port<?> port) {
        return port.getPortType();
    }

    /**
     * @return the knownPortConfigKeys
     */
    public Set<Byte> getKnownPortConfigKeys() {
        if (genericPort != null) {
            return genericPort.getKnownPortConfigKeys();
        }
        return knownPortConfigKeys;
    }

    /**
     * @param knownPortConfigKeys
     *            the knownPortConfigKeys to set
     */
    public void setKnownPortConfigKeys(Set<Byte> knownPortConfigKeys) {

        // the known port config keys are added in setPortConfigX()

        if (genericPort != null) {
            genericPort.setKnownPortConfigKeys(knownPortConfigKeys);
            return;
        }

        // clear the current keys
        this.knownPortConfigKeys.clear();

        // add all new keys
        this.knownPortConfigKeys.addAll(knownPortConfigKeys);
    }

    /**
     * @param knownPortConfigKeys
     *            the knownPortConfigKeys to set
     */
    public void addKnownPortConfigKeys(Set<Byte> knownPortConfigKeys) {

        // the known port config keys are added in setPortConfigX()
        Collections.addAll(getKnownPortConfigKeys(), knownPortConfigKeys.toArray(new Byte[0]));
    }

    /**
     * @param key
     *            the key to search
     * @return {@code true}: the key is supported by the node, {@code false}: the key is not supported by the node
     */
    public boolean isPortConfigKeySupported(Byte key) {
        if (genericPort != null) {
            return genericPort.isPortConfigKeySupported(key);
        }
        return knownPortConfigKeys.contains(key);
    }

    /**
     * @param keys
     *            the keys to search
     * @return {@code true}: at least one key is supported by the node, {@code false}: none of the provided keys is not
     *         supported by the node
     */
    public boolean isPortConfigKeySupported(byte[] keys) {
        if (genericPort != null) {
            for (Byte key : keys) {
                if (genericPort.isPortConfigKeySupported(key)) {
                    // found matching key
                    return true;
                }
            }
            return false;
        }
        for (Byte key : keys) {
            if (knownPortConfigKeys.contains(key)) {
                // found matching key
                return true;
            }
        }
        return false;
    }

    /**
     * @return the port number
     */
    public int getId() {
        if (genericPort != null) {
            return genericPort.getPortNumber();
        }
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the enabled
     */
    public boolean isEnabled() {

        if (genericPort != null) {
            return genericPort.isMatchingPortType(getPortType());
        }

        if (isInactive()) {
            return false;
        }

        // if the portIdentifier is available we must evaluate the enabled flag
        if (StringUtils.isNotBlank(portIdentifier)) {
            return enabled;
        }
        return true;
    }

    /**
     * @param enabled
     *            the enabled to set
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * @return the mappingEnabled
     */
    public boolean isRemappingEnabled() {
        if (genericPort != null) {
            return genericPort.isRemappingEnabled();
        }

        return remappingEnabled;
    }

    /**
     * @param remappingEnabled
     *            the mappingEnabled to set
     */
    public void setRemappingEnabled(boolean remappingEnabled) {
        this.remappingEnabled = remappingEnabled;
    }

    /**
     * @return the isInactive flag
     */
    public boolean isInactive() {
        if (genericPort != null) {
            return genericPort.isInactive();
        }

        return isInactive;
    }

    /**
     * The port can be inactive after the port type was changed.
     * 
     * @param isInactive
     *            the isInactive flag to set
     */
    public void setInactive(boolean isInactive) {
        LOGGER.info("Set port inactive: {}, port: {}", isInactive, this);
        this.isInactive = isInactive;
    }

    /**
     * @return the portIdentifier
     */
    public String getPortIdentifier() {
        return portIdentifier;
    }

    /**
     * @param portIdentifier
     *            the portIdentifier to set
     */
    public void setPortIdentifier(String portIdentifier) {
        this.portIdentifier = portIdentifier;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        LOGGER.debug("Set label: {}", label);
        this.label = label;
    }

    public T getStatus() {
        if (genericPort != null) {

            status = internalGetStatus();

        }
        return status;
    }

    protected T internalGetStatus() {
        return status;
    }

    public void setStatus(T status) {

        if (genericPort != null) {
            genericPort.setPortStatus(status.getType().getType());
        }

        this.status = status;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Port) {

            if (!obj.getClass().equals(getClass())) {
                return false;
            }

            return ((Port<?>) obj).getId() == getId();
        }
        return false;
    }

    @Override
    public int hashCode() {
        return getId();
    }

    public String toString() {
        String result = null;

        if (id != -1) {
            if (StringUtils.isNotBlank(label)) {
                result = label;
            }
            else {
                result = Resources.getString(getClass(), "label") + "_" + id;
            }
        }
        else {
            result = "";
        }
        return result;
    }

    /**
     * @return the port configuration in a byte array according to MSG_LC_CONFIG_SET (data[2]-data[5])
     */
    public abstract byte[] getPortConfig();

    /**
     * @return the portConfigEnabled
     */
    public boolean isPortConfigEnabled() {
        return portConfigEnabled;
    }

    /**
     * @param portConfigEnabled
     *            the portConfigEnabled to set
     */
    public void setPortConfigEnabled(boolean portConfigEnabled) {
        this.portConfigEnabled = portConfigEnabled;
    }

    /**
     * @return the configLoaded
     */
    public PortConfigStatus getConfigStatus() {
        if (genericPort != null) {
            configStatus = genericPort.getConfigStatus();
        }
        return configStatus;
    }

    /**
     * @param configStatus
     *            the configStatus to set
     */
    public void setConfigStatus(PortConfigStatus configStatus) {
        PortConfigStatus oldValue = this.configStatus;
        this.configStatus = configStatus;

        firePropertyChange(PROPERTY_CONFIG_LOADED, oldValue, this.configStatus);
    }

    /**
     * @return the portConfigErrorCode
     */
    public Integer getPortConfigErrorCode() {

        if (genericPort != null) {
            return genericPort.getPortConfigErrorCode();
        }

        return portConfigErrorCode;
    }

    /**
     * @param portConfigErrorCode
     *            the portConfigErrorCode to set
     */
    public void setPortConfigErrorCode(Integer portConfigErrorCode) {
        Integer oldValue = this.portConfigErrorCode;

        LOGGER.info("Set the portConfigErrorCode: {}, port id: {}", portConfigErrorCode, getId());
        this.portConfigErrorCode = portConfigErrorCode;
        firePropertyChange(PROPERTY_PORT_CONFIG_ERRORCODE, oldValue, this.portConfigErrorCode);
    }

    /**
     * @param portConfig
     *            the portConfig to set
     */
    public void setPortConfigX(Map<Byte, PortConfigValue<?>> portConfig) {
        LOGGER.info("Set the port config: {}", portConfig);

        // keep the received config keys because we must only send the known keys to the node
        addKnownPortConfigKeys(portConfig.keySet());

        // an additional message will follow
        boolean moreToContinue = portConfig.containsKey(BidibLibrary.BIDIB_PCFG_CONTINUE);
        if (moreToContinue) {
            LOGGER.info("An additional message with more config will follow.");
            setConfigStatus(PortConfigStatus.CONFIG_PENDING);
        }
        else {
            setConfigStatus(PortConfigStatus.CONFIG_PASSED);
        }

        // no parameters available / error code
        Number noneErrorCode = getPortConfigValue(BidibLibrary.BIDIB_PCFG_NONE, portConfig);
        if (noneErrorCode != null) {
            int errorCode = ByteUtils.getInt(noneErrorCode.byteValue());
            if (errorCode != BidibLibrary.BIDIB_ERR_LC_PORT_NONE) {
                setPortConfigErrorCode(errorCode);
                setConfigStatus(PortConfigStatus.CONFIG_ERROR);
            }
        }
    }

    /**
     * @return the current portConfig
     */
    public Map<Byte, PortConfigValue<?>> getPortConfigX() {
        return Collections.emptyMap();
    }

    protected <T> T getPortConfigValue(Byte key, Map<Byte, PortConfigValue<?>> portConfig) {
        try {
            PortConfigValue<?> portConfigValue = portConfig.get(key);
            if (portConfigValue != null) {
                return (T) portConfig.get(key).getValue();
            }
        }
        catch (ClassCastException ex) {
            LOGGER.warn("Cast value of key: {} to target type failed.", key, ex);
        }
        return null;
    }

}
