package org.bidib.wizard.mvc.main.controller;

import java.util.List;

import org.bidib.wizard.labels.AccessoryLabelFactory;
import org.bidib.wizard.labels.AccessoryLabelUtils;
import org.bidib.wizard.labels.LabelType;
import org.bidib.wizard.labels.Labels;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.bidib.wizard.mvc.main.model.Accessory;
import org.bidib.wizard.mvc.main.model.AccessoryStartupAspectModel;
import org.bidib.wizard.mvc.main.model.AccessorySwitchTimeModel;
import org.bidib.wizard.mvc.main.model.Macro;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.view.panel.AccessoryListPanel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AccessoryPanelController {
    private static final Logger LOGGER = LoggerFactory.getLogger(AccessoryPanelController.class);

    private final MainModel mainModel;

    private final AccessoryStartupAspectModel accessoryStartupAspectModel;

    private final AccessorySwitchTimeModel accessorySwitchTimeModel;

    private Labels accessoryLabels;

    public AccessoryPanelController(final MainModel mainModel) {
        this.mainModel = mainModel;

        accessoryLabels =
            DefaultApplicationContext.getInstance().get(DefaultApplicationContext.KEY_ACCESSORY_LABELS, Labels.class);

        // create the startup aspect model
        accessoryStartupAspectModel = new AccessoryStartupAspectModel();

        // create the switch time model
        accessorySwitchTimeModel = new AccessorySwitchTimeModel();
    }

    public AccessoryListPanel createAccessoryListPanel() {
        AccessoryListPanel accessoryListPanel =
            new AccessoryListPanel(this, mainModel, accessoryStartupAspectModel, accessorySwitchTimeModel);

        return accessoryListPanel;
    }

    public List<Macro> getMacros() {
        return mainModel.getMacros();
    }

    public Node getSelectedNode() {
        return mainModel.getSelectedNode();
    }

    public LabelType getAccessoryAspectsLabels(int accessoryId) {

        if (accessoryLabels == null) {
            LOGGER.info("No accessory labels avaialble.");
            return null;
        }

        long uniqueId = mainModel.getSelectedNode().getUniqueId();

        return AccessoryLabelUtils.getAccessoryLabel(accessoryLabels, uniqueId, accessoryId);
    }

    public void setAccessoryAspectLabel(int aspectId, String label) {

        if (accessoryLabels == null) {
            LOGGER.info("No accessory labels avaialble.");
            return;
        }

        boolean save = true;

        long uniqueId = mainModel.getSelectedNode().getUniqueId();
        final Accessory selectedAccessory = mainModel.getSelectedAccessory();

        int accessoryId = selectedAccessory.getId();

        AccessoryLabelUtils.replaceLabel(accessoryLabels, uniqueId, accessoryId, aspectId, label);

        if (save) {
            try {
                // accessoryLabels.save();

                new AccessoryLabelFactory().saveLabels(uniqueId, accessoryLabels);
            }
            catch (Exception e) {
                LOGGER.warn("Save accessory labels failed.", e);
                throw new RuntimeException(e);
            }
        }
    }

    public void saveAccessoryLabel(final Accessory accessory, String label) {

        accessory.setLabel(label);

        long uniqueId = mainModel.getSelectedNode().getNode().getUniqueId();
        AccessoryLabelUtils.replaceLabel(accessoryLabels, uniqueId, accessory.getId(), label);

        try {

            new AccessoryLabelFactory().saveLabels(uniqueId, accessoryLabels);
        }
        catch (Exception e) {
            LOGGER.warn("Save accessory labels failed.", e);
            throw new RuntimeException(e);
        }
    }
}
