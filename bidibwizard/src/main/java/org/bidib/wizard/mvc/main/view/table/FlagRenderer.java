package org.bidib.wizard.mvc.main.view.table;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import org.apache.commons.lang.StringUtils;

public class FlagRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = 1L;

    private final String defaultPrefix;

    public FlagRenderer(String defaultPrefix) {
        this.defaultPrefix = defaultPrefix;
    }

    public Component getTableCellRendererComponent(
        JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        String text = null;
        if (value != null && StringUtils.isNotBlank(value.toString())) {
            text = String.format("%1$02d : %2$s", row, value.toString());
        }
        else {
            text = String.format("%1$02d : %2$s", row, defaultPrefix + row);
        }
        setText(text);

        return this;
    }
}
