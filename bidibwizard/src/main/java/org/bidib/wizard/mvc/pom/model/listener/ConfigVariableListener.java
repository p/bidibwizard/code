package org.bidib.wizard.mvc.pom.model.listener;

import org.bidib.jbidibc.core.enumeration.CommandStationState;

public interface ConfigVariableListener {
    /**
     * The state of the command station has changed.
     * 
     * @param commandStationState
     *            the new command station state
     */
    void commandStationStateChanged(CommandStationState commandStationState);
}
