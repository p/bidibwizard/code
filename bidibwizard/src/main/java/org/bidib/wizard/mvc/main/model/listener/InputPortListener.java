package org.bidib.wizard.mvc.main.model.listener;

import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.PortConfigKeys;
import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.mvc.main.model.InputPort;

public interface InputPortListener<S extends BidibStatus> extends OutputListener<S> {
    /**
     * The values of the port have changed.
     * 
     * @param port
     *            the port
     * @param portConfigKeys
     *            the port config keys
     */
    void valuesChanged(InputPort port, PortConfigKeys... portConfigKeys);

    /**
     * Change the port type.
     * 
     * @param portType
     *            the new port type
     * @param port
     *            the port
     */
    void changePortType(LcOutputType portType, InputPort port);
}
