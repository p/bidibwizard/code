package org.bidib.wizard.mvc.main.model.function;

import org.bidib.jbidibc.exchange.lcmacro.LcMacroPointType;
import org.bidib.jbidibc.exchange.lcmacro.ServoMoveQueryPoint;
import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.mvc.main.model.ServoPort;

public class ServoMoveQueryFunction extends SystemFunction<BidibStatus> implements PortAware<ServoPort> {
    private ServoPort port;

    public ServoMoveQueryFunction() {
        this(null);
    }

    public ServoMoveQueryFunction(ServoPort port) {
        super(null, KEY_SERVO_MOVE_QUERY);
        this.port = port;
    }

    public ServoPort getPort() {
        return port;
    }

    public void setPort(ServoPort port) {
        this.port = port;
    }

    public String getDebugString() {
        int id = 0;

        if (getPort() != null) {
            id = getPort().getId();
        }
        return "ServoModeQuery, Port=" + id;
    }

    @Override
    public LcMacroPointType toLcMacroPoint() {
        // prepare macro point
        ServoMoveQueryPoint servoMoveQueryPoint = new ServoMoveQueryPoint().withOutputNumber(getPort().getId());
        return servoMoveQueryPoint;
    }
}
