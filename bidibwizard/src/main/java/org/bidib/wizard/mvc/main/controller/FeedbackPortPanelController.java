package org.bidib.wizard.mvc.main.controller;

import javax.swing.SwingUtilities;

import org.bidib.wizard.main.DefaultApplicationContext;
import org.bidib.wizard.mvc.main.model.FeedbackPortModel;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.model.listener.DefaultNodeListListener;
import org.bidib.wizard.mvc.main.model.listener.NodeListListener;
import org.bidib.wizard.mvc.main.view.panel.FeedbackPortListPanel;
import org.bidib.wizard.mvc.main.view.panel.listener.TabVisibilityListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FeedbackPortPanelController {

    private static final Logger LOGGER = LoggerFactory.getLogger(FeedbackPortPanelController.class);

    private final MainModel mainModel;

    private final NodeListListener nodeListListener;

    private FeedbackPortListPanel feedbackPortListPanel;

    private FeedbackPortModel feedbackPortModel;

    private final TabVisibilityListener tabVisibilityListener;

    public FeedbackPortPanelController(final MainModel mainModel, final TabVisibilityListener tabVisibilityListener) {
        this.mainModel = mainModel;
        this.tabVisibilityListener = tabVisibilityListener;

        feedbackPortModel = new FeedbackPortModel();

        DefaultApplicationContext.getInstance().register(
            DefaultApplicationContext.KEY_FEEDBACKPORTSTATUSCHANGEPROVIDER, feedbackPortModel);

        nodeListListener = new DefaultNodeListListener() {
            @Override
            public void nodeChanged() {
                LOGGER.info("The node has changed.");

                Node selectedNode = mainModel.getSelectedNode();
                // update the selected node
                feedbackPortModel.setSelectedNode(selectedNode);

                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        LOGGER.info("Notify that the selected port was changed.");
                        if (feedbackPortListPanel != null) {
                            feedbackPortListPanel.listChanged();
                        }
                        else {
                            LOGGER.warn("The feedbackPortListPanel is not available.");
                        }
                    }
                });
            }
        };
        this.mainModel.addNodeListListener(nodeListListener);
    }

    public Node getSelectedNode() {
        return feedbackPortModel.getSelectedNode();
    }

    public FeedbackPortListPanel createFeedbackPortListPanel() {
        LOGGER.info("Create new feedbackPortListPanel.");
        feedbackPortListPanel = new FeedbackPortListPanel(this, feedbackPortModel, mainModel, tabVisibilityListener);

        return feedbackPortListPanel;
    }
}
