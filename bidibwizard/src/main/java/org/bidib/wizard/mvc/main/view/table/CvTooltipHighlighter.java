package org.bidib.wizard.mvc.main.view.table;

import java.awt.Component;

import javax.swing.JComponent;

import org.jdesktop.swingx.decorator.AbstractHighlighter;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.renderer.StringValue;

public class CvTooltipHighlighter extends AbstractHighlighter {

    private StringValue toolTipValue;

    /**
     * Instantiates a CvTooltipHighlighter with null StringValue. The Highlighter is applied always.
     */
    public CvTooltipHighlighter() {
        this((HighlightPredicate) null);
    }

    /**
     * Instantiates a CvTooltipHighlighter with the specified StringValue. The Highlighter is applied always.
     * 
     * @param toolTipValue
     *            the StringValue used to create the tool tip
     */
    public CvTooltipHighlighter(StringValue toolTipValue) {
        this(null, toolTipValue);
    }

    /**
     * Instantiates a CvTooltipHighlighter with the specified HighlightPredicate and a null StringValue.
     * 
     * @param predicate
     *            the HighlightPredicate to use, may be null to default to ALWAYS.
     */
    public CvTooltipHighlighter(HighlightPredicate predicate) {
        this(predicate, null);
    }

    /**
     * Instantiates a CvTooltipHighlighter with the specified HighlightPredicate and StringValue.
     * 
     * @param predicate
     *            the HighlightPredicate to use, may be null to default to ALWAYS.
     * @param toolTipValue
     *            the StringValue used to create the tool tip
     */
    public CvTooltipHighlighter(HighlightPredicate predicate, StringValue toolTipValue) {
        super(predicate);

        this.toolTipValue = toolTipValue;
    }

    /**
     * Returns the StringValue used for decoration.
     * 
     * @return the StringValue used for decoration
     * 
     * @see #setToolTipValue(StringValue)
     */
    public StringValue getToolTipValue() {
        return toolTipValue;
    }

    /**
     * Sets the StringValue used for decoration. May be null to use default decoration.
     * 
     * @param font
     *            the Font used for decoration, may be null to use default decoration.
     * 
     * @see #getToolTipValue()
     */
    public void setToolTipValue(StringValue toolTipValue) {
        if (areEqual(toolTipValue, getToolTipValue()))
            return;
        this.toolTipValue = toolTipValue;
        fireStateChanged();
    }

    /**
     * {@inheritDoc}
     * <p>
     * 
     * Implemented to return false if the component is not a JComponent.
     */
    @Override
    protected boolean canHighlight(Component component, ComponentAdapter adapter) {
        return component instanceof JComponent;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Component doHighlight(Component component, ComponentAdapter adapter) {
        String toolTipText = null;

        if (toolTipValue == null) {
            if (adapter instanceof TooltipProvider) {
                toolTipText = ((TooltipProvider) adapter).getToolTip();
            }
            else {
                toolTipText = adapter.getString();
            }
        }
        else {
            toolTipText = toolTipValue.getString(adapter.getValue());
        }

        ((JComponent) component).setToolTipText(toolTipText);

        return component;
    }
}
