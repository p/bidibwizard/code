package org.bidib.wizard.mvc.main.view.table;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.LinkedList;

import javax.swing.AbstractCellEditor;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.TableCellEditor;

import org.apache.commons.lang.StringUtils;
import org.bidib.wizard.mvc.main.view.table.listener.ButtonListener;
import org.bidib.wizard.utils.InputValidationDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NumberWithButtonEditor extends AbstractCellEditor implements TableCellEditor {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(NumberWithButtonEditor.class);

    private final Collection<ButtonListener> buttonListeners = new LinkedList<ButtonListener>();

    private final JPanel panel = new JPanel();

    private final JFormattedTextField textField = new JFormattedTextField();

    private final JButton button;

    private int row = 0;

    private int column = 0;

    public NumberWithButtonEditor(String buttonText, int maximum) {
        textField.setHorizontalAlignment(SwingConstants.RIGHT);

        InputValidationDocument numericDocument = new InputValidationDocument(4, InputValidationDocument.NUMERIC);
        textField.setDocument(numericDocument);
        textField.setColumns(4);

        button = new JButton(buttonText);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireEditingStopped();
                fireButtonPressed();
            }
        });
        panel.setLayout(new GridBagLayout());
        panel.setFocusCycleRoot(true);

        GridBagConstraints c = new GridBagConstraints();

        c.anchor = GridBagConstraints.FIRST_LINE_START;
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0;
        c.insets = new Insets(0, 0, 2, 2);
        c.weightx = 1;
        panel.add(textField, c);
        c.anchor = GridBagConstraints.FIRST_LINE_END;
        c.gridx++;
        c.weightx = 0;
        panel.add(button, c);
    }

    public NumberWithButtonEditor(String buttonText) {
        this(buttonText, 255);
    }

    public void addButtonListener(ButtonListener l) {
        buttonListeners.add(l);
    }

    private void fireButtonPressed() {
        for (ButtonListener l : buttonListeners) {
            l.buttonPressed(row, column);
        }
    }

    @Override
    public Object getCellEditorValue() {
        try {
            if (StringUtils.isNotBlank(textField.getText())) {
                return Integer.parseInt(textField.getText());
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Parse textfield value failed.", ex);
        }
        return Integer.valueOf(0);
    }

    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        this.row = row;
        this.column = column;
        if (isSelected) {
            panel.setForeground(table.getSelectionForeground());
            panel.setBackground(table.getSelectionBackground());
        }
        else {
            panel.setForeground(table.getForeground());
            panel.setBackground(table.getBackground());
        }
        textField.setText(value != null ? value.toString() : "");
        return panel;
    }

    @Override
    public boolean stopCellEditing() {
        try {
            if (StringUtils.isNotBlank(textField.getText())) {
                int value = Integer.valueOf(textField.getText());

                LOGGER.debug("Current value: {}", value);
            }
            else {
                LOGGER.info("Allow empty string in text field.");
            }
        }
        catch (Exception e) {
            LOGGER.warn("Parse text value failed: {}", e.getMessage());
            return false;
        }
        return super.stopCellEditing();
    }
}
