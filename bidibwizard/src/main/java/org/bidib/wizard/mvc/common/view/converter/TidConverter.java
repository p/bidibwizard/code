package org.bidib.wizard.mvc.common.view.converter;

import org.bidib.jbidibc.core.TidData;

import com.jgoodies.binding.value.BindingConverter;

/**
 * Converts Values to Strings and vice-versa using a given Format.
 */
public final class TidConverter implements BindingConverter<TidData, String> {
    // Instance Creation **************************************************

    /**
     * Constructs a {@code TidConverter}.
     */
    public TidConverter() {
    }

    // Implementing Abstract Behavior *************************************

    /**
     * Formats the source value and returns a String representation.
     * 
     * @param sourceValue
     *            the source value
     * @return the formatted sourceValue
     */
    @Override
    public String targetValue(TidData sourceValue) {
        if (sourceValue != null) {
            return String.format("MUN=%08X:MID=%02X:SID=%02X", sourceValue.getUniqueId().getMun(), sourceValue
                .getUniqueId().getMid(), sourceValue.getSid());
        }
        return null;
    }

    /**
     * Parses the given String encoding and sets it as the subject's new value. Silently catches {@code ParseException}.
     * 
     * @param targetValue
     *            the value to be converted and set as new subject value
     */
    @Override
    public TidData sourceValue(String targetValue) {
        // no implementation
        return null;
    }
}
