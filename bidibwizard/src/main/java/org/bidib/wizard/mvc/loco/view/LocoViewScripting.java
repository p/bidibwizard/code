package org.bidib.wizard.mvc.loco.view;

import org.bidib.wizard.comm.SpeedSteps;
import org.bidib.wizard.script.Scripting;

public interface LocoViewScripting extends Scripting {

    void selectDecoderAddress(int dccAddress);

    void setSpeedSteps(SpeedSteps speedSteps);

    void setSpeed(int speed);

    void setFunction(int function);

    void setStop();

    void setStopEmergency();
}
