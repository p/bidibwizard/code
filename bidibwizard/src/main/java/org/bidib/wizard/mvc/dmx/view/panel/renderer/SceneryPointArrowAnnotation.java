package org.bidib.wizard.mvc.dmx.view.panel.renderer;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Stroke;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;

import org.jfree.chart.annotations.AbstractXYAnnotation;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.event.AnnotationChangeEvent;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.PlotRenderingInfo;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.util.ParamChecks;
import org.jfree.ui.RectangleEdge;
import org.jfree.util.PublicCloneable;

public class SceneryPointArrowAnnotation extends AbstractXYAnnotation implements PublicCloneable {

    private static final long serialVersionUID = 1L;

    /** The default tip radius (in Java2D units). */
    public static final double DEFAULT_BASE_RADIUS = 10.0;

    /** The x-coordinate. */
    private double x;

    /** The y-coordinate. */
    private double y;

    private double duration;

    private double volumeDelta;

    /** The arrow stroke. */
    private transient Stroke arrowStroke;

    /** The arrow paint. */
    private transient Paint arrowPaint;

    /**
     * The radius from the (x, y) point to the tip of the arrow (in Java2D units).
     */
    private double baseRadius;

    private Comparable<String> seriesKey;

    public SceneryPointArrowAnnotation(Comparable<String> seriesKey, double x, double y, int duration, int volumeDelta) {
        super();
        this.seriesKey = seriesKey;
        this.x = x;
        this.y = y;

        this.duration = duration;
        this.volumeDelta = volumeDelta;

        this.arrowStroke = new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[] { 1, 2 }, 0);
        // this.arrowStroke = new BasicStroke(1.0f);
        this.arrowPaint = Color.black;

        this.baseRadius = DEFAULT_BASE_RADIUS;
    }

    public Comparable<String> getSeriesKey() {
        return seriesKey;
    }

    /**
     * Returns the x coordinate for the text anchor point (measured against the domain axis).
     * 
     * @return The x coordinate (in data space).
     * 
     * @see #setX(double)
     */
    public double getX() {
        return this.x;
    }

    /**
     * Sets the x coordinate for the text anchor point (measured against the domain axis) and sends an
     * {@link AnnotationChangeEvent} to all registered listeners.
     * 
     * @param x
     *            the x coordinate (in data space).
     * 
     * @see #getX()
     */
    public void setX(double x) {
        this.x = x;
        fireAnnotationChanged();
    }

    /**
     * Returns the y coordinate for the text anchor point (measured against the range axis).
     * 
     * @return The y coordinate (in data space).
     * 
     * @see #setY(double)
     */
    public double getY() {
        return this.y;
    }

    /**
     * Sets the y coordinate for the text anchor point (measured against the range axis) and sends an
     * {@link AnnotationChangeEvent} to all registered listeners.
     * 
     * @param y
     *            the y coordinate.
     * 
     * @see #getY()
     */
    public void setY(double y) {
        this.y = y;
        fireAnnotationChanged();
    }

    /**
     * @return the duration
     */
    public double getDuration() {
        return duration;
    }

    /**
     * @param duration
     *            the duration to set
     */
    public void setDuration(double duration) {
        this.duration = duration;
    }

    /**
     * @return the volumeDelta
     */
    public double getVolumeDelta() {
        return volumeDelta;
    }

    /**
     * @param volumeDelta
     *            the volumeDelta to set
     */
    public void setVolumeDelta(double volumeDelta) {
        this.volumeDelta = volumeDelta;
    }

    /**
     * Returns the stroke used to draw the arrow line.
     * 
     * @return The arrow stroke (never <code>null</code>).
     * 
     * @see #setArrowStroke(Stroke)
     */
    public Stroke getArrowStroke() {
        return this.arrowStroke;
    }

    /**
     * Sets the stroke used to draw the arrow line and sends an {@link AnnotationChangeEvent} to all registered
     * listeners.
     * 
     * @param stroke
     *            the stroke (<code>null</code> not permitted).
     * 
     * @see #getArrowStroke()
     */
    public void setArrowStroke(Stroke stroke) {
        ParamChecks.nullNotPermitted(stroke, "stroke");
        this.arrowStroke = stroke;
        fireAnnotationChanged();
    }

    /**
     * Returns the paint used for the arrow.
     * 
     * @return The arrow paint (never <code>null</code>).
     * 
     * @see #setArrowPaint(Paint)
     */
    public Paint getArrowPaint() {
        return this.arrowPaint;
    }

    /**
     * Sets the paint used for the arrow and sends an {@link AnnotationChangeEvent} to all registered listeners.
     * 
     * @param paint
     *            the arrow paint (<code>null</code> not permitted).
     * 
     * @see #getArrowPaint()
     */
    public void setArrowPaint(Paint paint) {
        ParamChecks.nullNotPermitted(paint, "paint");
        this.arrowPaint = paint;
        fireAnnotationChanged();
    }

    /**
     * Returns the base radius.
     * 
     * @return The base radius (in Java2D units).
     * 
     * @see #setBaseRadius(double)
     */
    public double getBaseRadius() {
        return this.baseRadius;
    }

    /**
     * Sets the base radius and sends an {@link AnnotationChangeEvent} to all registered listeners.
     * 
     * @param radius
     *            the radius (in Java2D units).
     */
    public void setBaseRadius(double radius) {
        this.baseRadius = radius;
        fireAnnotationChanged();
    }

    /**
     * Draws the annotation.
     * 
     * @param g2
     *            the graphics device.
     * @param plot
     *            the plot.
     * @param dataArea
     *            the data area.
     * @param domainAxis
     *            the domain axis.
     * @param rangeAxis
     *            the range axis.
     * @param rendererIndex
     *            the renderer index.
     * @param info
     *            the plot rendering info.
     */
    @Override
    public void draw(
        Graphics2D g2, XYPlot plot, Rectangle2D dataArea, ValueAxis domainAxis, ValueAxis rangeAxis, int rendererIndex,
        PlotRenderingInfo info) {

        PlotOrientation orientation = plot.getOrientation();
        RectangleEdge domainEdge = Plot.resolveDomainAxisLocation(plot.getDomainAxisLocation(), orientation);
        RectangleEdge rangeEdge = Plot.resolveRangeAxisLocation(plot.getRangeAxisLocation(), orientation);

        double j2DX = domainAxis.valueToJava2D(getX(), dataArea, domainEdge);
        double j2DY = rangeAxis.valueToJava2D(getY(), dataArea, rangeEdge);
        if (orientation == PlotOrientation.HORIZONTAL) {
            double temp = j2DX;
            j2DX = j2DY;
            j2DY = temp;
        }

        double startX = j2DX + getBaseRadius();
        double startY = j2DY + getBaseRadius();

        double endX = domainAxis.valueToJava2D(getX() + duration, dataArea, domainEdge);
        double endY = rangeAxis.valueToJava2D(getY() + volumeDelta, dataArea, rangeEdge);

        g2.setStroke(getArrowStroke());
        g2.setPaint(getArrowPaint());
        Line2D line = new Line2D.Double(startX, startY, endX, endY);
        g2.draw(line);

    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof SceneryPointArrowAnnotation)) {
            return false;
        }
        SceneryPointArrowAnnotation that = (SceneryPointArrowAnnotation) obj;
        if (this.x != that.x) {
            return false;
        }
        if (this.y != that.y) {
            return false;
        }
        if (!this.seriesKey.equals(that.seriesKey)) {
            return false;
        }

        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        int result = 193;
        result = 37 * result + this.seriesKey.hashCode();
        long temp = Double.doubleToLongBits(this.x);
        result = 37 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(this.y);
        result = 37 * result + (int) (temp ^ (temp >>> 32));

        return result;
    }
}
