package org.bidib.wizard.mvc.script.view.utils;

import org.bidib.jbidibc.core.Node;
import org.bidib.jbidibc.core.utils.NodeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NodeRequirement {

    private static final Logger LOGGER = LoggerFactory.getLogger(NodeRequirement.class);

    private final String vid;

    private final String pid;

    public NodeRequirement(String vid, String pid) {
        this.vid = vid;
        this.pid = pid;
    }

    public String getPid() {
        return pid;
    }

    public String getVid() {
        return vid;
    }

    public boolean matches(final Node node) {
        int nodeVid = NodeUtils.getVendorId(node.getUniqueId());
        long nodePid = NodeUtils.getPid(node.getUniqueId(), node.getRelevantPidBits());

        LOGGER.info("Current node, vid: {}, pid: {}", nodeVid, nodePid);

        boolean matchingVid = false;
        String[] tokens = vid.split(",");
        for (String token : tokens) {
            try {
                if (Integer.parseInt(token.trim()) == nodeVid) {
                    // found matching VID
                    matchingVid = true;
                    break;
                }
            }
            catch (NumberFormatException ex) {
                LOGGER.warn("Parse VID token to integer value failed.", ex);
            }
        }

        LOGGER.info("Checked if the VID is matching: {}", matchingVid);

        boolean matchingPid = false;
        if (matchingVid) {
            tokens = pid.split(",");
            for (String token : tokens) {
                try {
                    if (Long.parseLong(token.trim()) == nodePid) {
                        // found matching PID
                        matchingPid = true;
                        break;
                    }
                }
                catch (NumberFormatException ex) {
                    LOGGER.warn("Parse PID token to integer value failed.", ex);
                }
            }
        }

        LOGGER.info("Checked if the PID is matching: {}", matchingPid);

        return matchingPid;
    }
}
