package org.bidib.wizard.mvc.main.view.cvdef;

import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.beans.Model;

public class DualBitfieldBean extends Model {
    private static final Logger LOGGER = LoggerFactory.getLogger(DualBitfieldBean.class);

    private static final long serialVersionUID = 1L;

    public final static String BIT_PROPERTY = "bit";

    public final static String BIT0_PROPERTY = "bit0";

    public final static String BIT1_PROPERTY = "bit1";

    public final static String BIT2_PROPERTY = "bit2";

    public final static String BIT3_PROPERTY = "bit3";

    public final static String BIT4_PROPERTY = "bit4";

    public final static String BIT5_PROPERTY = "bit5";

    public final static String BIT6_PROPERTY = "bit6";

    public final static String BIT7_PROPERTY = "bit7";

    private Byte bitfield = new Byte((byte) 0);

    private Integer radioBits = new Integer(0);

    public Byte getBitfield() {
        return bitfield;
    }

    public boolean getBit(int index) {
        index++;
        return (bitfield & index) == index;
    }

    public boolean getBit0LeftSide() {
        return isBitSet(0, false);
    }

    public boolean getBit1LeftSide() {
        return isBitSet(1, false);
    }

    public boolean getBit2LeftSide() {
        return isBitSet(2, false);
    }

    public boolean getBit3LeftSide() {
        return isBitSet(3, false);
    }

    public boolean getBit4LeftSide() {
        return isBitSet(4, false);
    }

    public boolean getBit5LeftSide() {
        return isBitSet(5, false);
    }

    public boolean getBit6LeftSide() {
        return isBitSet(6, false);
    }

    public boolean getBit7LeftSide() {
        return isBitSet(7, false);
    }

    // right side
    public boolean getBit0RightSide() {
        return isBitSet(0, true);
    }

    public boolean getBit1RightSide() {
        return isBitSet(1, true);
    }

    public boolean getBit2RightSide() {
        return isBitSet(2, true);
    }

    public boolean getBit3RightSide() {
        return isBitSet(3, true);
    }

    public boolean getBit4RightSide() {
        return isBitSet(4, true);
    }

    public boolean getBit5RightSide() {
        return isBitSet(5, true);
    }

    public boolean getBit6RightSide() {
        return isBitSet(6, true);
    }

    public boolean getBit7RightSide() {
        return isBitSet(7, true);
    }

    private boolean isBitSet(int bitPos, boolean rightSide) {
        // support for Radiobits, if the bit is a radio bit it must be set exclusively in its group
        if (radioBits != null) {
            byte radioBitsValue = radioBits.byteValue();
            if ((radioBitsValue & (1 << bitPos)) == (1 << bitPos)) {

                LOGGER.debug("The current bit is a radio bit: {}", bitPos);
                if (!rightSide) {
                    return false;
                }
            }
            else {
                if (rightSide) {
                    return false;
                }
            }
        }

        return (bitfield & (1 << bitPos)) == (1 << bitPos);
    }

    public void setBit0(boolean bit) {
        setBit(bit, 0);
    }

    public void setBit1(boolean bit) {
        setBit(bit, 1);
    }

    public void setBit2(boolean bit) {
        setBit(bit, 2);
    }

    public void setBit3(boolean bit) {
        setBit(bit, 3);
    }

    public void setBit4(boolean bit) {
        setBit(bit, 4);
    }

    public void setBit5(boolean bit) {
        setBit(bit, 5);
    }

    public void setBit6(boolean bit) {
        setBit(bit, 6);
    }

    public void setBit7(boolean bit) {
        setBit(bit, 7);
    }

    private void setBit(boolean bit, int bitpos) {
        Integer newValue = null;
        if (bit) {

            // support for Radiobits, if the bit is a radio bit it must be set exclusively in its group
            if (radioBits != null) {
                if ((radioBits.byteValue() & (1 << bitpos)) == (1 << bitpos)) {

                    LOGGER.debug("The current bit is a radio bit: {}", bitpos);
                    int clearedRadioBitsValue = (bitfield & ~(radioBits.byteValue()));
                    // set the value
                    newValue = (1 << bitpos) | clearedRadioBitsValue;
                }
                else {
                    newValue = (bitfield | (1 << bitpos));
                }
            }
            else {
                newValue = (bitfield | (1 << bitpos));
            }
        }
        else {
            newValue = (bitfield & ~(1 << bitpos));
        }

        setBitfield(ByteUtils.getLowByte(newValue.intValue()));
    }

    public void setBitfield(Number bitfieldNum) {
        LOGGER.debug("Set the bitfield value: {}", (bitfieldNum != null ? (bitfieldNum.intValue() & 0xFF) : null));
        Byte newValue = null;
        if (bitfieldNum == null) {
            newValue = new Byte((byte) 0);
        }
        else {
            newValue = ByteUtils.getLowByte(bitfieldNum.intValue());
        }
        this.bitfield = newValue;

        fireMultiplePropertiesChanged();
    }

    /**
     * @return the radioBits
     */
    public Integer getRadioBits() {
        return radioBits;
    }

    /**
     * @param radioBits
     *            the radioBits to set
     */
    public void setRadioBits(Integer radioBits) {
        this.radioBits = radioBits;
    }
}