package org.bidib.wizard.mvc.main.model.listener;

import org.bidib.wizard.comm.BoosterStatus;

public interface BoosterStatusListener {
    void currentChanged(int current);

    void maximumCurrentChanged(int maximumCurrent);

    void stateChanged(BoosterStatus status);

    void temperatureChanged(int temperature);

    void voltageChanged(int voltage);
}
