package org.bidib.wizard.mvc.main.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionListener;

import org.apache.commons.lang3.StringUtils;
import org.bidib.wizard.comm.AnalogPortStatus;
import org.bidib.wizard.comm.CommunicationFactory;
import org.bidib.wizard.comm.ServoPortStatus;
import org.bidib.wizard.comm.listener.CommunicationListener;
import org.bidib.wizard.common.context.ApplicationContext;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.bidib.wizard.mvc.common.model.PreferencesPortType;
import org.bidib.wizard.mvc.console.model.ConsoleModel;
import org.bidib.wizard.mvc.debug.controller.DebugInterfaceController;
import org.bidib.wizard.mvc.main.controller.BidibPiController;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.listener.BacklightPortListener;
import org.bidib.wizard.mvc.main.model.listener.CvDefinitionRequestListener;
import org.bidib.wizard.mvc.main.model.listener.DefaultNodeListListener;
import org.bidib.wizard.mvc.main.model.listener.OutputListener;
import org.bidib.wizard.mvc.main.model.listener.ServoPortListener;
import org.bidib.wizard.mvc.main.view.component.DefaultBusyFrame;
import org.bidib.wizard.mvc.main.view.menu.MainMenuBar;
import org.bidib.wizard.mvc.main.view.menu.listener.MainMenuListener;
import org.bidib.wizard.mvc.main.view.panel.MainPanel;
import org.bidib.wizard.mvc.main.view.panel.listener.AccessoryListListener;
import org.bidib.wizard.mvc.main.view.panel.listener.AccessoryTableListener;
import org.bidib.wizard.mvc.main.view.panel.listener.MacroListListener;
import org.bidib.wizard.mvc.main.view.panel.listener.MacroTableListener;
import org.bidib.wizard.mvc.main.view.panel.listener.NodeListActionListener;
import org.bidib.wizard.mvc.main.view.panel.listener.StatusListener;
import org.bidib.wizard.mvc.main.view.statusbar.StatusBar;
import org.bidib.wizard.mvc.preferences.model.Preferences;
import org.bidib.wizard.mvc.simulation.controller.SimulationController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vlsolutions.swing.docking.DockingDesktop;
import com.vlsolutions.swing.toolbars.ToolBarConstraints;
import com.vlsolutions.swing.toolbars.ToolBarContainer;
import com.vlsolutions.swing.toolbars.ToolBarPanel;
import com.vlsolutions.swing.toolbars.VLToolBar;

public class MainView extends DefaultBusyFrame {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(MainView.class);

    private MainMenuBar mainMenuBar;

    private MainPanel mainPanel;

    private StatusBar statusBar;

    private final Collection<MainMenuListener> toolbarListeners = new LinkedList<MainMenuListener>();

    private JButton connectButton;

    private JButton disconnectButton;

    private JButton allBoosterOffButton;

    private JButton allBoosterOnButton;

    private JButton simulationButton;

    private JButton developerDebugInterfaceButton;

    private JButton bidibPiResetButton;

    private JButton nodeScriptingButton;

    private DockingDesktop desktop;

    private MainModel mainModel;

    private VLToolBar toolbarSimulation;

    private VLToolBar toolbarDeveloper;

    private VLToolBar toolbarBidibPi;

    private ToolBarPanel topToolBarPanel;

    public MainView(MainModel model) {
        this.mainModel = model;
        setTitle(Resources.getString(getClass(), "title"));

    }

    public void createComponents() {

        desktop = new DockingDesktop();

        mainMenuBar = new MainMenuBar(mainModel, desktop);
        setJMenuBar(mainMenuBar);

        // create and install the container
        ToolBarContainer container = ToolBarContainer.createDefaultContainer(true, false, false, false);
        topToolBarPanel = container.getToolBarPanelAt(BorderLayout.NORTH);

        DefaultApplicationContext.getInstance().register(DefaultApplicationContext.KEY_TOPTOOLBARPANEL,
            topToolBarPanel);
        DefaultApplicationContext.getInstance().register(DefaultApplicationContext.KEY_DESKTOP, desktop);

        // create and register the toolbars
        VLToolBar toolbarBidib = new VLToolBar("bidib");
        topToolBarPanel.add(toolbarBidib, new ToolBarConstraints(0, 0)); // first row, first column
        addBidibButtons(toolbarBidib);

        VLToolBar toolbarTools = new VLToolBar("tools");
        topToolBarPanel.add(toolbarTools, new ToolBarConstraints(0, 1)); // first row, first column
        addToolsButtons(toolbarTools, desktop);

        toolbarSimulation = new VLToolBar("simulation");
        topToolBarPanel.add(toolbarSimulation, new ToolBarConstraints(0, 2)); // first row, first column
        addSimulationButtons(toolbarSimulation, desktop);

        // only show simulation toolbar in simulation mode
        boolean simulationMode = PreferencesPortType.isSimulation(Preferences.getInstance().getSelectedPortType());
        toolbarSimulation.setVisible(simulationMode);

        toolbarDeveloper = new VLToolBar("developer");
        topToolBarPanel.add(toolbarDeveloper, new ToolBarConstraints(0, 3)); // first row, first column
        addDeveloperButtons(toolbarDeveloper, desktop);

        // only show developer toolbar in power user mode
        boolean powerUserMode = Preferences.getInstance().isPowerUser();
        toolbarDeveloper.setVisible(powerUserMode);

        ApplicationContext applicationContext = DefaultApplicationContext.getInstance();

        BidibPiController bidibPiController =
            applicationContext.get(ApplicationContext.KEY_BIDIB_PI_CONTROLLER, BidibPiController.class);
        if (bidibPiController != null) {
            toolbarBidibPi = new VLToolBar("bidibpi");
            topToolBarPanel.add(toolbarBidibPi, new ToolBarConstraints(0, 4)); // first row, first column
            addBidibPiButtons(toolbarBidibPi, desktop);

            toolbarBidibPi.setVisible(true);
        }

        // add the desktop to the center
        container.add(desktop, BorderLayout.CENTER);

        // toolbar container becomes the only one component
        getContentPane().add(container);

        // create and register the console model
        ConsoleModel.getConsoleModel();

        // create the status bar
        statusBar = new StatusBar(mainModel);
        applicationContext.register(DefaultApplicationContext.KEY_STATUS_BAR, statusBar);

        // create the main panel
        mainPanel = new MainPanel(mainModel);
        mainPanel.create(desktop);

        // Add the status bar.
        getContentPane().add(statusBar, BorderLayout.SOUTH);

        CommunicationFactory.addCommunicationListener(new CommunicationListener() {

            @Override
            public void status(String statusText, int displayDuration) {
            }

            @Override
            public void opening() {
                LOGGER.info("The port is opening. Disable the buttons.");
                connectButton.setEnabled(false);
                disconnectButton.setEnabled(false);
                allBoosterOffButton.setEnabled(false);
                allBoosterOnButton.setEnabled(false);
                mainMenuBar.opening();
            }

            @Override
            public void opened(String port) {
                disconnectButton.setEnabled(true);
                allBoosterOffButton.setEnabled(true);
                allBoosterOnButton.setEnabled(true);
                connectButton.setEnabled(false);

                mainMenuBar.opened(port);
            }

            @Override
            public void initialized() {
            }

            @Override
            public void closed(final String port) {
                if (SwingUtilities.isEventDispatchThread()) {
                    disconnectButton.setEnabled(false);
                    allBoosterOffButton.setEnabled(false);
                    allBoosterOnButton.setEnabled(false);
                    connectButton.setEnabled(true);

                    mainMenuBar.closed(port);
                }
                else {
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            disconnectButton.setEnabled(false);
                            allBoosterOffButton.setEnabled(false);
                            allBoosterOnButton.setEnabled(false);
                            connectButton.setEnabled(true);

                            mainMenuBar.closed(port);
                        }
                    });
                }
            }
        });

        Preferences.getInstance().addPropertyChangeListener(Preferences.PROPERTY_SELECTED_PORTTYPE,
            new PropertyChangeListener() {

                @Override
                public void propertyChange(PropertyChangeEvent evt) {

                    if (toolbarSimulation != null) {
                        try {
                            PreferencesPortType portType = Preferences.getInstance().getSelectedPortType();
                            LOGGER.info(
                                "The selected port type has changed, enable/disable the simulation toolbar, portType: {}",
                                portType);
                            if (portType != null) {
                                toolbarSimulation.setVisible(PreferencesPortType.isSimulation(portType));
                            }
                        }
                        catch (Exception ex) {
                            LOGGER.warn("Change visibility of simulation toolbar failed.", ex);
                        }
                    }
                }
            });
        Preferences.getInstance().addPropertyChangeListener(Preferences.PROPERTY_POWER_USER,
            new PropertyChangeListener() {

                @Override
                public void propertyChange(PropertyChangeEvent evt) {

                    if (toolbarDeveloper != null) {
                        try {
                            boolean isPowerUser = Preferences.getInstance().isPowerUser();
                            LOGGER.info("The power user property has changed, set the developer toolbar visible: {}",
                                isPowerUser);
                            toolbarDeveloper.setVisible(isPowerUser);
                        }
                        catch (Exception ex) {
                            LOGGER.warn("Change visibility of developer toolbar failed.", ex);
                        }
                    }
                }
            });

        mainModel.addNodeListListener(new DefaultNodeListListener() {

            @Override
            public void nodeChanged() {

                final boolean nodeScriptWizardEnabled = (mainModel.getSelectedNode() != null);
                LOGGER.info("The selected node has changed, nodeScriptWizardEnabled: {}", nodeScriptWizardEnabled);
                if (SwingUtilities.isEventDispatchThread()) {
                    nodeScriptingButton.setEnabled(nodeScriptWizardEnabled);
                }
                else {
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            nodeScriptingButton.setEnabled(nodeScriptWizardEnabled);
                        }
                    });
                }
            }

            @Override
            public void listChanged() {
                // TODO Auto-generated method stub

            }
        });
    }

    public void addToolBarListener(MainMenuListener l) {
        toolbarListeners.add(l);
    }

    private void fireConnect() {
        connectButton.setEnabled(false);
        for (MainMenuListener l : toolbarListeners) {
            l.connect();
        }
    }

    private void fireDisconnect() {
        for (MainMenuListener l : toolbarListeners) {
            l.disconnect();
        }
    }

    private void fireAllBoosterOff() {
        for (MainMenuListener l : toolbarListeners) {
            l.allBoosterOff();
        }
    }

    private void fireAllBoosterOn() {
        for (MainMenuListener l : toolbarListeners) {
            l.allBoosterOn();
        }
    }

    private void fireShowNodeScriptWizard() {
        for (MainMenuListener l : toolbarListeners) {
            l.showNodeScriptWizard(mainModel);
        }
    }

    /**
     * @return the desktop
     */
    public DockingDesktop getDesktop() {
        return desktop;
    }

    private void addBidibButtons(VLToolBar toolbar) {
        connectButton =
            Resources.makeNavigationButton("32x32/connect.png",
                Resources.getString(getClass(), "toolbar.bidib.connect"),
                Resources.getString(getClass(), "toolbar.bidib.connect.alttext"));
        connectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireConnect();
            }
        });
        toolbar.add(connectButton);

        disconnectButton =
            Resources.makeNavigationButton("32x32/disconnect.png",
                Resources.getString(getClass(), "toolbar.bidib.disconnect"),
                Resources.getString(getClass(), "toolbar.bidib.disconnect.alttext"));
        disconnectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireDisconnect();
            }
        });
        disconnectButton.setEnabled(false);
        toolbar.add(disconnectButton);

        toolbar.addSeparator();

        allBoosterOffButton =
            Resources.makeNavigationButton("32x32/boosterOff.png",
                Resources.getString(getClass(), "toolbar.bidib.all-booster-off"),
                Resources.getString(getClass(), "toolbar.bidib.all-booster-off.alttext"));
        allBoosterOffButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireAllBoosterOff();
            }
        });
        allBoosterOffButton.setEnabled(false);
        toolbar.add(allBoosterOffButton);

        allBoosterOnButton =
            Resources.makeNavigationButton("32x32/boosterOn.png",
                Resources.getString(getClass(), "toolbar.bidib.all-booster-on"),
                Resources.getString(getClass(), "toolbar.bidib.all-booster-on.alttext"));
        allBoosterOnButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireAllBoosterOn();
            }
        });
        allBoosterOnButton.setEnabled(false);
        toolbar.add(allBoosterOnButton);
    }

    private void addToolsButtons(VLToolBar toolbar, final DockingDesktop desktop) {
        nodeScriptingButton =
            Resources.makeNavigationButton("32x32/nodescripting.png",
                Resources.getString(getClass(), "toolbar.nodescripting.open"),
                Resources.getString(getClass(), "toolbar.nodescripting.open.alttext"));
        nodeScriptingButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // TODO create the nodeScript controller

                fireShowNodeScriptWizard();
            }
        });
        toolbar.add(nodeScriptingButton);

        nodeScriptingButton.setEnabled(false);
    }

    private void addSimulationButtons(VLToolBar toolbar, final DockingDesktop desktop) {
        simulationButton =
            Resources.makeNavigationButton("32x32/sim-mode.png",
                Resources.getString(getClass(), "toolbar.simulation.open"),
                Resources.getString(getClass(), "toolbar.simulation.open.alttext"));
        simulationButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // create the simulation controller
                if (mainModel.getSelectedNode() != null) {
                    SimulationController simulationController = new SimulationController(desktop);
                    simulationController.start(mainModel);

                    simulationController.activate(mainModel.getSelectedNode());
                }
            }
        });
        toolbar.add(simulationButton);
    }

    private void addDeveloperButtons(VLToolBar toolbar, final DockingDesktop desktop) {
        developerDebugInterfaceButton =
            Resources.makeNavigationButton("32x32/console.png",
                Resources.getString(getClass(), "toolbar.debuginterface.open"),
                Resources.getString(getClass(), "toolbar.debuginterface.open.alttext"));
        developerDebugInterfaceButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // create the debug interface controller
                DebugInterfaceController debugInterfaceController = new DebugInterfaceController(desktop);
                debugInterfaceController.start();
            }
        });
        toolbar.add(developerDebugInterfaceButton);
    }

    private void addBidibPiButtons(VLToolBar toolbar, final DockingDesktop desktop) {
        bidibPiResetButton =
            Resources.makeNavigationButton("32x32/lightning.png",
                Resources.getString(getClass(), "toolbar.bidibpi.reset"),
                Resources.getString(getClass(), "toolbar.bidibpi.reset.alttext"));
        bidibPiResetButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // reset the Bidib-Pi
                BidibPiController bidibPiController =
                    DefaultApplicationContext.getInstance().get(ApplicationContext.KEY_BIDIB_PI_CONTROLLER,
                        BidibPiController.class);
                if (bidibPiController != null) {
                    LOGGER.info("Call reset on the Bidib-Pi");
                    bidibPiController.reset();
                }
                else {
                    LOGGER.info("No bidib-pi available.");
                }
            }
        });
        toolbar.add(bidibPiResetButton);
    }

    public void addAccessoryListListener(AccessoryListListener l) {
        mainPanel.addAccessoryListListener(l);
    }

    public void addAccessoryListSelectionListener(ListSelectionListener l) {
        mainPanel.addAccessoryListSelectionListener(l);
    }

    public void addAccessoryTableListener(AccessoryTableListener l) {
        mainPanel.addAccessoryTableListener(l);
    }

    public void addAnalogPortListener(OutputListener<AnalogPortStatus> l) {
        mainPanel.addAnalogPortListener(l);
    }

    public void addBacklightPortListener(BacklightPortListener l) {
        mainPanel.addBacklightPortListener(l);
    }

    public void addMacroListListener(MacroListListener l) {
        mainPanel.addMacroListListener(l);
    }

    public void addMacroListSelectionListener(ListSelectionListener l) {
        mainPanel.addMacroListSelectionListener(l);
    }

    public void addMacroTableListener(MacroTableListener l) {
        mainPanel.addMacroTableListener(l);
    }

    public void addMainMenuListener(MainMenuListener l) {
        mainMenuBar.addMenuListener(l);
    }

    // public void addMotorPortListener(MotorPortListener<MotorPortStatus> l) {
    // mainPanel.addMotorPortListener(l);
    // }

    public void addNodeListListener(NodeListActionListener l) {
        mainPanel.addNodeListListener(l);
    }

    public void addNodeListSelectionListener(ListSelectionListener l) {
        mainPanel.addNodeListSelectionListener(l);
    }

    public void addServoPortListener(ServoPortListener<ServoPortStatus> l) {
        mainPanel.addServoPortListener(l);
    }

    // public void addSoundPortListener(OutputListener<SoundPortStatus> l) {
    // mainPanel.addSoundPortListener(l);
    // }

    public void addStatusListener(StatusListener l) {
        statusBar.addStatusListener(l);
    }

    public void addCvDefinitionRequestListener(CvDefinitionRequestListener l) {
        mainPanel.addCvDefinitionRequestListener(l);
    }

    public void prepareFrame() {

        setMinimumSize(new Dimension(1024, 600));

        String encodedWindowPosition = Preferences.getInstance().getWindowPosition();
        Dimension maxSize = Toolkit.getDefaultToolkit().getScreenSize();
        if (StringUtils.isBlank(encodedWindowPosition)) {

            setPreferredSize(new Dimension(Math.min(maxSize.width, 1400), Math.min(maxSize.height, 800)));
        }
        else {
            // Restore the window position from settings
            Point windowPos = Preferences.getWindowPos(encodedWindowPosition);
            Dimension windowSize = Preferences.getWindowSize(encodedWindowPosition);
            int extendedState = Preferences.getExtendedState(encodedWindowPosition);
            LOGGER.info("Restore frame at pos: {}, size: {}, extendedState: {}", windowPos, windowSize, extendedState);

            setExtendedState(extendedState);
            if (windowPos != null) {
                if (windowPos.x > (maxSize.width - 20)) {
                    windowPos.x = maxSize.width - 20;
                }
                if (windowPos.y > (maxSize.height - 20)) {
                    windowPos.y = maxSize.height - 20;
                }

                if (windowPos.x < 0) {
                    windowPos.x = 0;
                }
                if (windowPos.y < 0) {
                    windowPos.y = 0;
                }

                setLocation(windowPos);
            }

            if (windowSize != null) {
                setPreferredSize(windowSize);
            }
            else {
                setPreferredSize(new Dimension(Math.min(maxSize.width, 1400), Math.min(maxSize.height, 800)));
            }
        }

        pack();
    }

    public void saveWindowPosition() {
        LOGGER.info("Save the window position.");
        try {
            Preferences.getInstance().setWindowPosition(
                Preferences.encodedWindowPosition(getLocation(), getSize(), getExtendedState()));
            Preferences.getInstance().save(null);
        }
        catch (Exception ex) {
            LOGGER.warn("Save window position during shutdown failed.", ex);
        }
    }

    public void setWindowListener(WindowListener l) {
        addWindowListener(l);
    }

    /**
     * Display a text in the statusbar.
     * 
     * @param statusText
     *            the text to display
     * @param displayDuration
     *            Time wait for clearing the message (in seconds). Any value lesser than 1 disable this functionality.
     */
    public void setStatusText(String statusText, int displayDuration) {
        if (statusBar != null) {
            statusBar.setStatus(statusText, displayDuration);
        }
    }
}
