package org.bidib.wizard.mvc.main.view.cvdef;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.bidib.jbidibc.core.node.ConfigurationVariable;
import org.bidib.jbidibc.exchange.vendorcv.CVType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LongCvNode extends CvNode {
    private static final Logger LOGGER = LoggerFactory.getLogger(LongCvNode.class);

    private List<LongCvNode> slaveNodes = new LinkedList<>();

    private LongCvNode masterNode;

    public LongCvNode(CVType cv, ConfigurationVariable configVar) {
        super(cv, configVar);
    }

    public void addSlaveNode(LongCvNode slaveNode) {
        LOGGER.trace("Add new slave node: {}", slaveNode);
        slaveNodes.add(slaveNode);
    }

    public void setMasterNode(LongCvNode masterNode) {
        LOGGER.trace("Set the master node: {}", masterNode);
        this.masterNode = masterNode;
    }

    public LongCvNode getMasterNode() {
        if (masterNode == null) {
            // if the master is not set, we are the master himself
            return this;
        }
        return masterNode;
    }

    public List<LongCvNode> getSlaveNodes() {
        return Collections.unmodifiableList(slaveNodes);
    }

}
