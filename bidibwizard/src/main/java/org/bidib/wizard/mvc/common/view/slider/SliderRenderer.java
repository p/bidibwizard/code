package org.bidib.wizard.mvc.common.view.slider;

import java.awt.Component;
import java.awt.Dimension;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.bidib.jbidibc.ui.LogarithmicJSlider;
import org.bidib.wizard.mvc.main.model.ConfigurablePort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SliderRenderer extends JPanel implements TableCellRenderer {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(SliderRenderer.class);

    private JSlider slider;

    private JLabel sliderValue;

    private int labelWidth;

    public SliderRenderer(int min, int max, int value) {
        this(min, max, value, false);
    }

    public SliderRenderer(int min, int max, int value, boolean useLogarithmicSlider) {

        if (!useLogarithmicSlider) {
            slider = new JSlider(min, max, value);
        }
        else {
            slider = new LogarithmicJSlider(min, max, value);
        }

        JLabel tempLabel = new JLabel(Integer.toString(65535/* max */));
        tempLabel.doLayout();
        final Dimension prefSize = tempLabel.getPreferredSize();

        sliderValue = new JLabel();

        // keep the label width
        labelWidth = prefSize.width;
        sliderValue.setHorizontalAlignment(JLabel.RIGHT);
        sliderValue.setPreferredSize(prefSize);
        sliderValue.setMinimumSize(prefSize);

        slider.setMinimumSize(prefSize);

        setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
        add(slider);
        add(Box.createRigidArea(new Dimension(0, 5)));
        add(sliderValue);
    }

    public Component getTableCellRendererComponent(
        JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

        if (isSelected) {
            setForeground(table.getSelectionForeground());
            setBackground(table.getSelectionBackground());
        }
        else {
            setForeground(table.getForeground());
            setBackground(table.getBackground());
        }

        TableColumnModel columnModel = table.getColumnModel();
        TableColumn selectedColumn = columnModel.getColumn(column);
        int columnWidth = selectedColumn.getWidth();
        int columnHeight = table.getRowHeight();

        if (columnWidth > labelWidth) {
            slider.setSize(new Dimension(columnWidth - labelWidth, columnHeight));
        }

        setValue(value);

        // get value at column 0 must return the port
        try {
            Object portValue = table.getModel().getValueAt(row, 0);

            if (portValue instanceof ConfigurablePort<?>) {

                ConfigurablePort<?> port = (ConfigurablePort<?>) portValue;
                boolean enabled = port.isEnabled();
                slider.setEnabled(enabled);
                sliderValue.setEnabled(enabled);

                slider.setOpaque(!enabled);
                sliderValue.setOpaque(!enabled);
            }
            else {
                slider.setEnabled(true);
                sliderValue.setEnabled(true);
                slider.setOpaque(false);
                sliderValue.setOpaque(false);
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Enable or disable slider and slider value failed.");
        }

        slider.updateUI();

        return this;
    }

    /**
     * Sets the <code>String</code> object for the cell being rendered to <code>value</code>.
     * 
     * @param value
     *            the string value for this cell; if value is <code>null</code> it sets the text value to an empty
     *            string
     * @see JLabel#setText
     * 
     */
    protected void setValue(Object value) {
        slider.setValue(((Integer) value).intValue());

        updateSliderTextValue(slider.getValue());
    }

    /**
     * @param value
     *            the relative value to set
     */
    protected void updateSliderTextValue(int value) {

        String newValue = Integer.toString(value);
        LOGGER.trace("Set the new value: {}", newValue);
        sliderValue.setText(newValue);
    }

    public void setMaxValue(int maxValue) {
        if (slider != null) {
            slider.setMaximum(maxValue);
        }
    }

    public void setMinValue(int minValue) {
        if (slider != null) {
            slider.setMinimum(minValue);
        }
    }
}
