package org.bidib.wizard.mvc.railcomplus.model;

import java.util.Objects;

import org.bidib.jbidibc.core.enumeration.AddressMode;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.beans.Model;

public class RailcomPlusDecoderModel extends Model {

    private static final Logger LOGGER = LoggerFactory.getLogger(RailcomPlusDecoderModel.class);

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_DECODER_MANUFACTURER = "decoderManufacturer";

    public static final String PROPERTY_DECODER_ADDRESS = "decoderAddress";

    private Integer decoderManufacturer;

    private Integer decoderAddress;

    private Long decoderMun;

    private AddressMode decoderAddressMode;

    private boolean[] addressPartsSet = new boolean[2];

    public RailcomPlusDecoderModel() {

    }

    /**
     * @return the decoderManufacturer
     */
    public Integer getDecoderManufacturer() {
        return decoderManufacturer;
    }

    /**
     * @param decoderManufacturer
     *            the decoderManufacturer to set
     */
    public void setDecoderManufacturer(Integer decoderManufacturer) {
        LOGGER.info("Set the decoder manufacturer: {}", decoderManufacturer);
        this.decoderManufacturer = decoderManufacturer;
    }

    /**
     * @return the decoderAddress
     */
    public Integer getDecoderAddress() {
        return decoderAddress;
    }

    /**
     * @param decoderAddress
     *            the decoderAddress to set
     */
    public void setDecoderAddress(Integer decoderAddress) {
        LOGGER.info("Set the decoder address: {}", decoderAddress);
        this.decoderAddress = decoderAddress;

        if (decoderAddress == null) {
            addressPartsSet[0] = true;
            addressPartsSet[1] = true;
        }
    }

    /**
     * @param decoderAddressLowValue
     *            the decoderAddress low value to set
     */
    public void setDecoderAddressLowValue(Integer decoderAddressLowValue) {
        LOGGER.info("Set the decoder address low value: {}", decoderAddressLowValue);

        if (decoderAddress == null) {
            decoderAddress = 0;
        }

        decoderAddress = ByteUtils.toInt((decoderAddress >> 8), decoderAddressLowValue);
        addressPartsSet[0] = true;
    }

    /**
     * @param decoderAddressHighValue
     *            the decoderAddress high value to set
     */
    public void setDecoderAddressHighValue(Integer decoderAddressHighValue) {
        LOGGER.info("Set the decoder address high value: {}", decoderAddressHighValue);

        if (decoderAddress == null) {
            decoderAddress = 0;
        }

        decoderAddress = ByteUtils.toInt(decoderAddressHighValue, decoderAddress);
        addressPartsSet[1] = true;
    }

    public boolean isAddressComplete() {
        boolean addressComplete = false;
        if (AddressMode.SHORT == decoderAddressMode) {
            // short address only needs low byte
            addressComplete = addressPartsSet[0];
        }
        else {
            addressComplete = (addressPartsSet[0] && addressPartsSet[1]);
        }
        return addressComplete;
    }

    /**
     * @return the decoderMun
     */
    public Long getDecoderMun() {
        return decoderMun;
    }

    /**
     * @param decoderMun
     *            the decoderMun to set
     */
    public void setDecoderMun(Long decoderMun) {
        this.decoderMun = decoderMun;
    }

    /**
     * @param addressMode
     *            the addressMode to set
     */
    public void setDecoderAddressMode(AddressMode addressMode) {
        this.decoderAddressMode = addressMode;
    }

    /**
     * @return the addressMode
     */
    public AddressMode getDecoderAddressMode() {
        return decoderAddressMode;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof RailcomPlusDecoderModel) {
            RailcomPlusDecoderModel other = (RailcomPlusDecoderModel) obj;
            if (Objects.equals(decoderManufacturer, other.decoderManufacturer)
                && Objects.equals(decoderMun, other.decoderMun)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hashCode = super.hashCode();
        if (decoderManufacturer != null) {
            hashCode += decoderManufacturer.hashCode();
        }
        if (decoderMun != null) {
            hashCode += decoderMun.hashCode();
        }
        // ignore decoder address
        return hashCode;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("RailcomPlusDecoderModel[decoderManufacturer=");
        sb.append(decoderManufacturer).append(",decoderMun=");
        if (decoderMun != null) {
            sb.append(ByteUtils.longToHex((long) decoderMun & 0xFFFFFFFFL));
        }
        else {
            sb.append(decoderMun);
        }
        sb.append(",decoderAddress=").append(decoderAddress).append("]");
        return sb.toString();
    }
}
