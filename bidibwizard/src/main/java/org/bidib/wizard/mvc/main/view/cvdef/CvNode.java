package org.bidib.wizard.mvc.main.view.cvdef;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import org.bidib.jbidibc.core.node.ConfigurationVariable;
import org.bidib.jbidibc.exchange.vendorcv.CVType;
import org.bidib.jbidibc.exchange.vendorcv.DescriptionType;
import org.bidib.wizard.utils.XmlLocaleUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jidesoft.grid.DefaultExpandableRow;

public class CvNode extends DefaultExpandableRow implements PropertyChangeListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(CvNode.class);

    public static final int COLUMN_KEY = -1;

    public static final int COLUMN_DESCRIPTION = 0;

    public static final int COLUMN_NUMBER = 1;

    public static final int COLUMN_VALUE = 2;

    public static final int COLUMN_NEW_VALUE = 3;

    public static final int COLUMN_MODE = 4;

    private Object newValue;

    private String lang;

    private CVType cv;

    private final ConfigurationVariable configVar;

    public CvNode(CVType cv, ConfigurationVariable configVar) {
        this.cv = cv;
        this.configVar = configVar;
        lang = XmlLocaleUtils.getXmlLocaleVendorCV();

        configVar.addPropertyChangeListener(this);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        LOGGER.trace("The property has changed: {}", evt);

        switch (evt.getPropertyName()) {
            case ConfigurationVariable.PROPERTY_VALUE:
                LOGGER.info("Reset the new value.");
                setNewValue(null);
                break;
            default:
                break;
        }

    }

    @Override
    public Object getValueAt(int columnIndex) {
        switch (columnIndex) {
            case COLUMN_NUMBER:
                return getCV().getNumber();
            case COLUMN_DESCRIPTION:
                return getDescription(getCV().getDescription(), lang);
            case COLUMN_VALUE:
                return configVar.getValue();
            case COLUMN_NEW_VALUE:
                // TODO provide an editor with the allowed values
                return (newValue == null ? "-" : newValue);
            case COLUMN_MODE:
                return getCV().getMode();
            case COLUMN_KEY:
                return getDescription(getCV().getDescription(), XmlLocaleUtils.DEFAULT_LOCALE);
            default:
                return getCV();
        }
    }

    @Override
    public void setValueAt(Object value, int column) {
        switch (column) {
            case COLUMN_NEW_VALUE:
                LOGGER.debug("Set the new value: {}", value);
                if (value instanceof Number) {

                    if (getCV() != null) {
                        switch (getCV().getType()) {
                            case SIGNED_CHAR:
                                newValue = ((Number) value).byteValue();
                                break;
                            default:
                                newValue = ((Number) value).intValue() & 0xFF;
                                break;
                        }
                    }
                }
                else {
                    // assume the value is a string
                    newValue = value;
                }
                break;
            default:
                break;
        }
    }

    public CVType getCV() {
        return cv;
    }

    public Object getNewValue() {
        return newValue;
    }

    public void resetNewValue() {
        newValue = null;
    }

    public void setNewValue(Object value) {

        if (getCV() != null) {
            if (value == null) {
                newValue = null;
            }
            else {
                switch (getCV().getType()) {
                    case STRING:
                        newValue = value;
                        break;
                    case SIGNED_CHAR:
                        if (value instanceof String) {
                            // convert value to int
                            value = Integer.parseInt((String) value);
                        }
                        newValue = ((Number) value).byteValue();
                        break;
                    default:
                        if (value instanceof String) {
                            // convert value to int
                            value = Integer.parseInt((String) value);
                        }
                        newValue = ((Number) value).intValue() & 0xFF;
                        break;
                }
            }
        }
    }

    public ConfigurationVariable getConfigVar() {
        return configVar;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("cv number: ").append(cv.getNumber()).append(", new value: ").append(newValue);
        return sb.toString();
    }

    public static String getDescription(List<DescriptionType> descriptions, String lang) {

        if (descriptions != null) {
            for (DescriptionType description : descriptions) {
                if (lang.equals(description.getLang())) {
                    return description.getText();
                }
            }
        }
        return "<unknown/unbekannt>";
    }

    public static String getDescriptionHelp(List<DescriptionType> descriptions, String lang) {

        if (descriptions != null) {
            for (DescriptionType description : descriptions) {
                if (lang.equals(description.getLang())) {
                    return description.getHelp();
                }
            }
        }
        return null;
    }

}
