package org.bidib.wizard.mvc.firmware.view.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.filechooser.FileFilter;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.bidib.jbidibc.core.utils.ProductUtils;
import org.bidib.jbidibc.exchange.bidib.FirmwareFactory;
import org.bidib.jbidibc.exchange.bidib.FirmwareFactory.NodetextUtils;
import org.bidib.jbidibc.exchange.firmware.DeviceNode;
import org.bidib.jbidibc.exchange.firmware.Firmware;
import org.bidib.jbidibc.exchange.firmware.FirmwareDefinitionType;
import org.bidib.jbidibc.exchange.firmware.FirmwareNode;
import org.bidib.jbidibc.exchange.firmware.NodeType;
import org.bidib.jbidibc.exchange.firmware.SimpleNode;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.dialog.FileDialog;
import org.bidib.wizard.mvc.common.view.checkboxtree.AddCheckBoxToTree;
import org.bidib.wizard.mvc.common.view.checkboxtree.IconCellRenderer;
import org.bidib.wizard.mvc.common.view.checkboxtree.IconData;
import org.bidib.wizard.mvc.common.view.checkboxtree.InvisibleNode;
import org.bidib.wizard.mvc.common.view.checkboxtree.InvisibleTreeModel;
import org.bidib.wizard.mvc.firmware.model.FirmwareModel;
import org.bidib.wizard.mvc.firmware.model.FirmwareUpdateModel;
import org.bidib.wizard.mvc.firmware.model.UpdateStatus;
import org.bidib.wizard.mvc.firmware.model.listener.FirmwareModelListener;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.preferences.model.Preferences;
import org.bidib.wizard.utils.XmlLocaleUtils;
import org.oxbow.swingbits.dialog.task.IContentDesign;
import org.oxbow.swingbits.dialog.task.TaskDialog;
import org.oxbow.swingbits.dialog.task.TaskDialog.StandardCommand;
import org.oxbow.swingbits.list.CheckList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.helpers.MessageFormatter;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.value.ConverterValueModel;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.builder.ButtonBarBuilder;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.debug.FormDebugPanel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.FormLayout;

public class FirmwareUpdatePanel {
    private static final Logger LOGGER = LoggerFactory.getLogger(FirmwareUpdatePanel.class);

    private static final String ENCODED_DIALOG_COLUMN_SPECS = "pref, 3dlu, fill:50dlu:grow, 3dlu, pref";

    private final static String NEWLINE = "\n";

    private ValueModel nodeNameValueModel;

    private ValueModel productNameValueModel;

    private ValueModel uuidValueModel;

    private ValueModel versionValueModel;

    private ValueModel selectedFileValueModel;

    private final JButton selectFirmwareButton = new JButton(Resources.getString(getClass(), "select-firmware"));

    private final JProgressBar progressBar = new JProgressBar(0, 100);

    private final JButton startButton = new JButton(Resources.getString(getClass(), "startUpdate"));

    private final JButton clearButton = new JButton(Resources.getString(getClass(), "clearLogArea"));

    private JButton closeButton;

    public static final ImageIcon ICON_FIRMWARE = new ImageIcon("");

    private ValueModel currentOperationModel;

    private JLabel currentOperationLabel;

    private JTree firmwareTree;

    private DefaultTreeModel firmwareTreeModel;

    private AddCheckBoxToTree addCheckBoxToTree = new AddCheckBoxToTree();

    private AddCheckBoxToTree.CheckTreeManager checkTreeManager;

    private JTextPane loggerArea;

    private SimpleAttributeSet[] attrs;

    private FirmwareUpdateModel firmwareUpdateModel = new FirmwareUpdateModel();

    private FirmwareModel firmwareModel;

    private JPanel contentPanel;

    public FirmwareUpdatePanel() {

    }

    /**
     * @return the firmwareModel
     */
    public FirmwareModel getFirmwareModel() {
        return firmwareModel;
    }

    /**
     * @param firmwareModel
     *            the firmwareModel to set
     */
    public void setFirmwareModel(final FirmwareModel firmwareModel) {
        this.firmwareModel = firmwareModel;

        firmwareUpdateModel.setNodeName(firmwareModel.getNodeName());
        firmwareUpdateModel.setProductName(firmwareModel.getProductName());
        firmwareUpdateModel.setUuid(firmwareModel.getUniqueId());
        firmwareUpdateModel.setInstalledVersion(firmwareModel.getNodeCurrentVersion());

        firmwareModel.addPropertyChangeListener(FirmwareModel.PROPERTYNAME_IN_PROGRESS, new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                LOGGER.info("The inProgress property has been changed: {}", firmwareModel.isInProgress());
                firmwareUpdateModel.setFirmwareUpdateInProgress(firmwareModel.isInProgress());
            }
        });

        firmwareModel.addPropertyChangeListener(FirmwareModel.PROPERTYNAME_UPDATE_STATUS, new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                LOGGER.info("The update status property has been changed: {}", firmwareModel.getUpdateStatus());
                if (UpdateStatus.NODE_LOST.equals(firmwareModel.getUpdateStatus())) {
                    firmwareUpdateModel.setNodeLost(true);
                }
            }
        });

        firmwareModel.addFirmwareModelListener(new FirmwareModelListener() {

            @Override
            public void progressValueChanged(final int progressValue) {
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        progressBar.setValue(progressValue);
                    }
                });
            }

            @Override
            public void firmwareFileChanged() {
                LOGGER.info("The firmware files have been changed.");
            }

            @Override
            public void processingStatusChanged(final String processingStatus, final int style, final Object... args) {
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        addLogText(processingStatus, style, args);
                    }
                });
            }
        });
    }

    public void setCloseButton(final JButton closeButton) {
        this.closeButton = closeButton;
    }

    public JPanel createPanel() {

        DefaultFormBuilder builder = null;
        boolean debugDialog = false;
        if (debugDialog) {
            JPanel panel = new FormDebugPanel();
            builder = new DefaultFormBuilder(new FormLayout(ENCODED_DIALOG_COLUMN_SPECS), panel);
        }
        else {
            JPanel panel = new JPanel(new BorderLayout());
            builder = new DefaultFormBuilder(new FormLayout(ENCODED_DIALOG_COLUMN_SPECS), panel);
        }
        builder.border(Borders.DIALOG);

        builder.appendSeparator(Resources.getString(getClass(), "node-info"));

        nodeNameValueModel =
            new PropertyAdapter<FirmwareUpdateModel>(firmwareUpdateModel, FirmwareUpdateModel.PROPERTYNAME_NODE_NAME,
                true);
        JTextField nodeNameText = BasicComponentFactory.createTextField(nodeNameValueModel, true);
        nodeNameText.setEditable(false);
        builder.append(Resources.getString(getClass(), "name"), nodeNameText, 3);
        builder.nextLine();

        productNameValueModel =
            new PropertyAdapter<FirmwareUpdateModel>(firmwareUpdateModel, FirmwareUpdateModel.PROPERTYNAME_PRODUCT_NAME,
                true);
        JTextField productNameText = BasicComponentFactory.createTextField(productNameValueModel, true);
        productNameText.setEditable(false);
        builder.append(Resources.getString(getClass(), "productname"), productNameText, 3);
        builder.nextLine();

        uuidValueModel =
            new PropertyAdapter<FirmwareUpdateModel>(firmwareUpdateModel, FirmwareUpdateModel.PROPERTYNAME_UUID, true);
        JTextField uuidText = BasicComponentFactory.createTextField(uuidValueModel, true);
        uuidText.setEditable(false);
        builder.append(Resources.getString(getClass(), "uuid"), uuidText, 3);
        builder.nextLine();

        versionValueModel =
            new PropertyAdapter<FirmwareUpdateModel>(firmwareUpdateModel,
                FirmwareUpdateModel.PROPERTYNAME_INSTALLED_VERSION, true);
        JTextField versionText = BasicComponentFactory.createTextField(versionValueModel, true);
        versionText.setEditable(false);
        builder.append(Resources.getString(getClass(), "firmware"), versionText, 3);
        builder.nextLine();

        // select firmware
        builder.appendSeparator(Resources.getString(getClass(), "firmware-update"));

        selectedFileValueModel =
            new PropertyAdapter<FirmwareUpdateModel>(firmwareUpdateModel,
                FirmwareUpdateModel.PROPERTYNAME_SELECTED_ARCHIVE_NAME, true);
        JTextField selectedFileText = BasicComponentFactory.createTextField(selectedFileValueModel, true);
        selectedFileText.setEditable(false);
        builder.append(Resources.getString(getClass(), "file"), selectedFileText);

        builder.append(selectFirmwareButton);

        builder.appendRow("3dlu");
        builder.nextLine();

        DefaultMutableTreeNode top = new InvisibleNode(new IconData(ICON_FIRMWARE, null, "Firmware"));

        firmwareTreeModel = new InvisibleTreeModel(top);

        firmwareTree = new JTree(firmwareTreeModel) {
            private static final long serialVersionUID = 1L;

            protected void setExpandedState(TreePath path, boolean state) {
                if (state) {
                    super.setExpandedState(path, state);
                }
            }
        };

        firmwareTree.putClientProperty("JTree.lineStyle", "Angled");

        TreeCellRenderer renderer = new IconCellRenderer();
        firmwareTree.setCellRenderer(renderer);

        firmwareTree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        firmwareTree.setShowsRootHandles(false);
        firmwareTree.setEditable(false);

        checkTreeManager =
            addCheckBoxToTree.new CheckTreeManager(
                firmwareTree,
                null);

        JScrollPane scrollTree = new JScrollPane();
        scrollTree.getViewport().add(firmwareTree);

        builder.appendRow("100dlu");
        builder.nextLine();
        builder.append(scrollTree, 5);

        ValueModel exportModeValueModel =
            new PropertyAdapter<FirmwareUpdateModel>(firmwareUpdateModel, FirmwareUpdateModel.PROPERTYNAME_EXPERT_MODE,
                true);

        JCheckBox checkExpertMode =
            BasicComponentFactory.createCheckBox(exportModeValueModel, Resources.getString(getClass(), "expert-mode"));
        builder.append(checkExpertMode, 5);
        builder.nextLine();

        // select firmware
        builder.appendSeparator(Resources.getString(getClass(), "progress"));

        // prepare the operation verdict
        currentOperationModel =
            new PropertyAdapter<FirmwareModel>(firmwareModel, FirmwareModel.PROPERTYNAME_UPDATE_STATUS, true);
        ValueModel valueConverterModel =
            new ConverterValueModel(currentOperationModel, new FirmwareUpdateStateConverter());
        currentOperationLabel = BasicComponentFactory.createLabel(valueConverterModel);
        ValueModel iconConverterModel =
            new ConverterValueModel(currentOperationModel, new FirmwareUpdateIconConverter());
        Bindings.bind(currentOperationLabel, "icon", iconConverterModel);
        // currentOperationLabel.setIcon(updateOperationEmptyIcon);
        builder.append(currentOperationLabel, 5);
        builder.appendRow("3dlu");
        builder.nextLine(2);

        builder.append(progressBar, 3);
        progressBar.setStringPainted(true);
        startButton.setEnabled(false);
        builder.append(startButton);

        builder.appendRow("3dlu");
        builder.nextLine();

        // prepare the logger area
        loggerArea = new JTextPane();
        loggerArea.setPreferredSize(new Dimension(450, 100));
        Font font = UIManager.getDefaults().getFont("Label.font");
        loggerArea.setFont(font);
        loggerArea.setEditable(false);

        attrs = new SimpleAttributeSet[2];

        attrs[0] = new SimpleAttributeSet();

        attrs[1] = new SimpleAttributeSet(attrs[0]);
        // StyleConstants.setBold(attrs[1], true);
        StyleConstants.setForeground(attrs[1], Color.red);

        JScrollPane scrollLog = new JScrollPane();
        scrollLog.getViewport().add(loggerArea);

        builder.appendRow("fill:100dlu:grow");
        builder.nextLine();
        builder.append(scrollLog, 5);

        builder.appendRow("3dlu");

        // prepare the close button
        JPanel buttons = new ButtonBarBuilder().addButton(clearButton).addGlue().addButton(closeButton).build();

        builder.appendRow("p");
        builder.nextLine(2);
        builder.append(buttons, 5);

        selectFirmwareButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                selectFirmware();
            }
        });

        startButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                fireStart();
            }

        });
        clearButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                fireClear();
            }

        });

        // if the model is valid, the start button is enabled.
        PropertyConnector.connect(firmwareUpdateModel, FirmwareUpdateModel.PROPERTYNAME_CAN_UPDATE, startButton,
            "enabled");
        PropertyConnector.connect(firmwareModel, FirmwareModel.PROPERTYNAME_IDLE, closeButton, "enabled");
        PropertyConnector.connect(firmwareModel, FirmwareModel.PROPERTYNAME_IDLE_AND_VALID, selectFirmwareButton,
            "enabled");

        exportModeValueModel.addValueChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                LOGGER.info("The expert mode has changed: {}", evt.getNewValue());
                updateTreeElementsVisibility();
            }
        });

        checkTreeManager.addPropertyChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                LOGGER.info("The tree selectionPaths have changed, new value: {}", evt.getNewValue());

                boolean firmwareFilesSelected = false;
                if (evt.getNewValue() instanceof TreePath[]) {
                    TreePath[] treePaths = (TreePath[]) evt.getNewValue();
                    firmwareFilesSelected = treePaths.length > 0;
                }
                firmwareUpdateModel.setFirmwareFilesSelected(firmwareFilesSelected);
            }
        });

        contentPanel = builder.build();
        return contentPanel;
    }

    private void hideChildren(DefaultMutableTreeNode parent, boolean expertMode) {

        Enumeration<?> e = parent.children();
        while (e.hasMoreElements()) {
            DefaultMutableTreeNode childNode = (DefaultMutableTreeNode) e.nextElement();
            if (childNode instanceof InvisibleNode) {
                InvisibleNode node = (InvisibleNode) childNode;
                LOGGER.info("Found InvisibleNode instance: {}, expertMode: {}", node, expertMode);
                if (!(node.getUserObject() instanceof DeviceData)) {
                    node.setVisible(expertMode);
                }
            }
            hideChildren(childNode, expertMode);
        }
    }

    private void updateTreeElementsVisibility() {
        DefaultMutableTreeNode root = (DefaultMutableTreeNode) firmwareTreeModel.getRoot();

        boolean expertMode = firmwareUpdateModel.isExpertMode();
        LOGGER.info("Update the visibility of the tree elements, expertMode: {}", expertMode);

        hideChildren(root, expertMode);

        // force refresh tree
        ((InvisibleTreeModel) firmwareTreeModel).activateFilter(!expertMode);
        firmwareTreeModel.reload();

        for (int i = 0; i < firmwareTree.getRowCount(); i++) {
            firmwareTree.expandRow(i);
        }
    }

    public AddCheckBoxToTree.CheckTreeManager getCheckTreeManager() {
        return checkTreeManager;
    }

    private void selectFirmware() {
        FileFilter ff = new ZipAndHexWithTargetFileFilter();

        FileDialog dialog = new FileDialog(contentPanel, FileDialog.OPEN, null, ff) {
            @Override
            public void approve(String selectedFile) {
                File file = new File(selectedFile);

                selectedFile = file.getName();

                String extension = FilenameUtils.getExtension(selectedFile);
                if (!ZipAndHexWithTargetFileFilter.SUFFIX_ZIP.equalsIgnoreCase(extension)) {

                    String[] parts = selectedFile.split("\\.");

                    // if this fails with an exception the dialog is not closed
                    // check if the destination identifier is valid
                    // int destIdentifier = Integer.parseInt(parts[parts.length - 2]);
                    int destIdentifier = -1;
                    try {
                        destIdentifier = Integer.parseInt(parts[parts.length - 2]);
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Auto-detect destination for firmware failed.", ex);
                    }
                    if (destIdentifier < 0 || destIdentifier > 127) {
                        LOGGER.warn("Invalid destination detected: {}", destIdentifier);

                        // TODO let the user select the destination
                        Object[] options = { "Flash", "Eeprom" };
                        int n =
                            JOptionPane.showOptionDialog(contentPanel, "Select the destination.", "Firmware Update",
                                JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options,
                                options[0]);

                        switch (n) {
                            case JOptionPane.YES_OPTION:
                                destIdentifier = 0;
                                break;
                            case JOptionPane.NO_OPTION:
                                destIdentifier = 1;
                                break;
                            default:
                                throw new RuntimeException("Invalid destination detected: " + destIdentifier);
                        }

                    }

                    loadFirmware(file, destIdentifier);

                }
                // handle ZIP files
                else {
                    // load from ZIP
                    loadFirmware(file.toString());
                }

                firmwareUpdateModel.setFirmwareArchivePath(file.toString());
                firmwareModel.setInProgress(false);
                firmwareModel.setProgressValue(0);
                // firmwareModel.setIdle(true);
                firmwareModel.setUpdateStatus(UpdateStatus.NONE);

                firmwareModel.addProcessingStatus(
                    Resources.getString(FirmwareUpdatePanel.class, "status.file-selected"), 0, file.toString());

                // update the visibility of the tree elements
                updateTreeElementsVisibility();

                // if (CollectionUtils.isNotEmpty(firmwareModel.getCvDefinitionFiles())) {
                // // Ask the user to import the cvDefintion files
                // int result =
                // JOptionPane.showConfirmDialog(JOptionPane.getFrameForComponent(contentPanel),
                // Resources.getString(FirmwareUpdatePanel.class, "cvdefinition-files.message"),
                // Resources.getString(FirmwareUpdatePanel.class, "cvdefinition-files.title"),
                // JOptionPane.YES_NO_OPTION);
                // if (result == JOptionPane.YES_OPTION) {
                //
                // LOGGER.info("Import the cv definition files: {}", firmwareModel.getCvDefinitionFiles());
                // // TODO
                // firmwareUpdateModel.getFirmwareArchivePath()
                //
                //
                // }
                // }
            }
        };
        dialog.showDialog();

        if (CollectionUtils.isNotEmpty(firmwareUpdateModel.getCvDefinitionFiles())) {
            // Ask the user to import the cvDefintion files
            int result =
                JOptionPane.showConfirmDialog(JOptionPane.getFrameForComponent(contentPanel),
                    Resources.getString(FirmwareUpdatePanel.class, "cvdefinition-files.message"),
                    Resources.getString(FirmwareUpdatePanel.class, "cvdefinition-files.title"),
                    JOptionPane.YES_NO_OPTION);
            if (result == JOptionPane.YES_OPTION) {

                // clear the cv definition files to get the new files trigger the change listener even with the same
                // files
                firmwareModel.setCvDefinitionFiles(firmwareUpdateModel.getFirmwareArchivePath(), null);

                List<String> cvDefinitionFiles = firmwareUpdateModel.getCvDefinitionFiles();

                LOGGER.info("Set the CV definition files in the firmware model.");

                // set the update version before the files are set
                firmwareModel.setUpdateVersion(firmwareUpdateModel.getUpdateVersion());

                // set the value to trigger the import
                firmwareModel.setCvDefinitionFiles(firmwareUpdateModel.getFirmwareArchivePath(), cvDefinitionFiles);
            }
        }

    }

    private void fireClear() {
        loggerArea.setText(null);
    }

    private static TaskDialog questionDialog(Window parent, String title, Icon icon, String instruction, String text) {

        TaskDialog dlg = new TaskDialog(parent, title);
        dlg.setInstruction(instruction);
        dlg.setText(text);

        dlg.setIcon(icon);
        dlg.setCommands(StandardCommand.OK.derive(TaskDialog.makeKey("Ok")),
            StandardCommand.CANCEL.derive(TaskDialog.makeKey("Cancel")));
        return dlg;
    }

    private void fireStart() {
        LOGGER.info("Start was pressed.");

        TreePath[] paths = getCheckTreeManager().getSelectionModel().getSelectionPaths();
        if (paths != null) {

            // show a dialog
            Node nodeToUpdate = firmwareModel.getNode();

            boolean showUnplugServoWarning = Preferences.getInstance().isShowFirmwareUpdateUnplugServoWarning();
            if (showUnplugServoWarning && nodeToUpdate != null) {
                StringBuilder sb = new StringBuilder();

                if ((nodeToUpdate.hasServoPorts() || ProductUtils.isOneBootloader(nodeToUpdate.getUniqueId()))) {
                    LOGGER.info("The node to update has servo ports. Show the info to unplug the servo before update!");

                    sb.append(Resources.getString(FirmwareUpdatePanel.class, "unplug-servo.message"));
                    sb.append("<br/><br/>");
                }
                sb.append(Resources.getString(FirmwareUpdatePanel.class, "continue.message"));

                TaskDialog dlg =
                    questionDialog(JOptionPane.getFrameForComponent(contentPanel),
                        Resources.getString(FirmwareUpdatePanel.class, "unplug-servo.title"),
                        TaskDialog.StandardIcon.QUESTION,
                        Resources.getString(FirmwareUpdatePanel.class, "backup-node.message"),
                        sb.toString().replaceAll("\r\n", "<br/>"));

                JList<String> list = new JList<>();
                CheckList<String> checkList = new CheckList.Builder(list).build();
                checkList.setData(
                    Arrays.asList(Resources.getString(FirmwareUpdatePanel.class, "show-unplug-servo-warning.choice")));

                // blend list color with dialog
                Color listColor = UIManager.getColor(IContentDesign.COLOR_MESSAGE_BACKGROUND);
                list.setBackground(listColor);
                list.setSelectionBackground(listColor);
                list.setSelectionForeground(list.getForeground());
                dlg.setFixedComponent(list);

                boolean result = dlg.show().equals(StandardCommand.OK);

                Collection<String> checkedItems = checkList.getCheckedItems();
                if (!result) {
                    LOGGER.info("User cancelled update operation.");
                    return;
                }
                if (CollectionUtils.isNotEmpty(checkedItems)) {
                    LOGGER.info("User selected to no longer show the warning dialog.");
                    Preferences.getInstance().setShowFirmwareUpdateUnplugServoWarning(false);
                    Preferences.getInstance().save(null);
                }
            }

            List<FirmwareNode> selectedNodes = new LinkedList<FirmwareNode>();
            for (TreePath tp : paths) {
                LOGGER.info("Selected path is: {}", tp);

                // collect the selected firmware files
                TreeNode treeNode = (TreeNode) tp.getLastPathComponent();

                collectSelectedFirmwareFiles(treeNode, selectedNodes);
            }

            LOGGER.info("Selected firmware nodes: {}", selectedNodes);
            for (FirmwareNode node : selectedNodes) {
                addLogText(Resources.getString(FirmwareUpdatePanel.class, "status.selected-dest"), 0,
                    node.getDestinationNumber(), node.getFilename());
            }

            if (CollectionUtils.isNotEmpty(selectedNodes)) {
                LOGGER.info("Set the firmware update files in the firmware model.");

                firmwareModel.setInProgress(true);

                firmwareModel.setFirmwareFiles(firmwareUpdateModel.getFirmwareArchivePath(), selectedNodes);
            }
        }

    }

    private void collectSelectedFirmwareFiles(TreeNode treeNode, List<FirmwareNode> selectedNodes) {
        if (!treeNode.isLeaf()) {
            LOGGER.info("The current treeNode is not a leaf: {}", treeNode);
            for (int index = 0; index < treeNode.getChildCount(); index++) {
                TreeNode child = treeNode.getChildAt(index);
                LOGGER.info("The current child: {}", child);

                if (child instanceof DefaultMutableTreeNode) {
                    DefaultMutableTreeNode mutableChild = (DefaultMutableTreeNode) child;
                    if (mutableChild.getUserObject() instanceof FirmwareData) {
                        FirmwareData firmwareData = (FirmwareData) mutableChild.getUserObject();

                        LOGGER.info("The current node has firmware data: {}", firmwareData.getFirmwareNode());
                        selectedNodes.add(firmwareData.getFirmwareNode());
                    }
                    else {
                        collectSelectedFirmwareFiles(child, selectedNodes);
                    }
                }
                else {
                    collectSelectedFirmwareFiles(child, selectedNodes);
                }
            }
        }
        else {
            if (treeNode instanceof DefaultMutableTreeNode) {
                DefaultMutableTreeNode mutableChild = (DefaultMutableTreeNode) treeNode;
                if (mutableChild.getUserObject() instanceof FirmwareData) {
                    FirmwareData firmwareData = (FirmwareData) mutableChild.getUserObject();

                    LOGGER.info("The current node has firmware data: {}", firmwareData.getFirmwareNode());
                    selectedNodes.add(firmwareData.getFirmwareNode());
                }
            }
        }
    }

    public void addLogText(String logLine, final int style, Object... args) {
        LOGGER.info("Add text to loggerArea, logLine: {}, args: {}", logLine, args);

        if (args != null) {
            logLine = MessageFormatter.arrayFormat(logLine, args).getMessage();
        }
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SSS - ");
        final StringBuffer message = new StringBuffer(sdf.format(new Date()));
        message.append(logLine).append(NEWLINE);

        if (SwingUtilities.isEventDispatchThread()) {
            StyledDocument doc = loggerArea.getStyledDocument();
            try {
                doc.insertString(doc.getLength(), message.toString(), attrs[style]);
            }
            catch (Exception ex) {
                LOGGER.warn("Add new log text failed.");
            }

            loggerArea.setCaretPosition(loggerArea.getDocument().getLength());
        }
        else {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    StyledDocument doc = loggerArea.getStyledDocument();
                    try {
                        doc.insertString(doc.getLength(), message.toString(), attrs[style]);
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Add new log text failed.");
                    }

                    loggerArea.setCaretPosition(loggerArea.getDocument().getLength());
                }
            });
        }
    }

    public void loadFirmware(String path) {
        LOGGER.info("Load firmware from path: {}", path);

        String lang = XmlLocaleUtils.getXmlLocaleVendorCV();

        DefaultMutableTreeNode top = (DefaultMutableTreeNode) firmwareTreeModel.getRoot();
        // remove all existing children
        top.removeAllChildren();

        Firmware firmware = FirmwareFactory.getFirmware(path);
        if (firmware != null && firmware.getFirmwareDefinition() != null) {

            firmwareUpdateModel.setUpdateVersion(null);

            FirmwareDefinitionType firmwareDefinition = firmware.getFirmwareDefinition();

            // get the nodes
            List<NodeType> nodes = firmwareDefinition.getNode();
            for (NodeType node : nodes) {

                if (node instanceof DeviceNode) {
                    DeviceNode deviceNode = (DeviceNode) node;
                    DefaultMutableTreeNode deviceNodeItem = createDeviceNode(top, deviceNode, lang);
                    top.add(deviceNodeItem);

                    // try to get the version from the filename
                    for (NodeType firmwareNode : deviceNode.getNode()) {
                        if (firmwareNode instanceof FirmwareNode) {
                            FirmwareNode fwNode = (FirmwareNode) firmwareNode;
                            String filename = fwNode.getFilename();
                            try {

                                String[] parts = filename.split("[-]|[_]|[ ]");
                                LOGGER.info("Current parts: {}, from filename: {}", new Object[] { parts }, filename);

                                // care about the last part
                                // the last part can contain a .000 or .001

                                if (parts[parts.length - 1].indexOf(".000") < 0) {
                                    // ignore if the last part does not end with '.000'
                                    continue;
                                }

                                String version =
                                    parts[parts.length - 1].substring(0, parts[parts.length - 1].indexOf(".000"));
                                LOGGER.info("Found update version: {}", version);

                                firmwareUpdateModel.setUpdateVersion(version);

                                break;
                            }
                            catch (Exception ex) {
                                LOGGER.warn("Find update version in name of firmware file failed.", ex);
                            }
                        }
                    }

                }
            }

            // check if cv definitions are available
            List<String> cvFilenames = firmwareDefinition.getCvFilename();
            if (CollectionUtils.isNotEmpty(cvFilenames)) {

                // keep the cv definitions
                firmwareUpdateModel.setCvDefinitionFiles(cvFilenames);
            }

            if (StringUtils.isBlank(firmwareUpdateModel.getUpdateVersion()) && firmware.getVersion() != null) {
                LOGGER.info("Set the update version: {}", firmware.getVersion());
                firmwareUpdateModel.setUpdateVersion(firmware.getVersion().getVersion());
            }
        }
        // force refresh tree
        firmwareTreeModel.reload(top);

        for (int i = 0; i < firmwareTree.getRowCount(); i++) {
            firmwareTree.expandRow(i);
        }
    }

    public void loadFirmware(File firmwareFile, int destination) {
        LOGGER.info("Load firmware from single file: {}, destination: {}", firmwareFile, destination);

        String lang = XmlLocaleUtils.getXmlLocaleVendorCV();

        DefaultMutableTreeNode top = (DefaultMutableTreeNode) firmwareTreeModel.getRoot();
        // remove all existing children
        top.removeAllChildren();

        String filename = firmwareFile.getName();
        String[] parts = filename.split("[-]|[_]|[ ]");

        LOGGER.info("prepared parts of filename: {}", (Object[]) parts);

        // prepare a temporary firmware description
        StringBuffer firmwareDescription = new StringBuffer("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        firmwareDescription.append("<Firmware xsi:schemaLocation=\"http://www.bidib.org/schema/firmware firmware.xsd\" "
            + "xmlns:firmware=\"http://www.bidib.org/schema/firmware\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "
            + "xmlns=\"http://www.bidib.org/schema/firmware\">");
        firmwareDescription.append(
            "<Version Version=\"0.1\" Lastupdate=\"20140411\" Author=\"BiDiB.org\" Pid=\"000\" Vendor=\"013\" Description=\"Firmware Definition\"/>");
        firmwareDescription.append("<FirmwareDefinition>");
        firmwareDescription
            .append("<Node xsi:type=\"DeviceNode\" Comment=\"Temporary Node\" VID=\"013\" PID=\"000\" >");
        if (parts != null && parts.length > 0) {
            firmwareDescription.append("<Nodetext Lang=\"de-DE\" Text=\"");
            if (parts.length > 1) {
                for (int index = 0; index < parts.length - 1; index++) {
                    firmwareDescription.append(parts[index]).append(" ");
                }
            }
            else {
                firmwareDescription.append(parts[0]).append(" -> ");
                switch (destination) {
                    case 0:
                        firmwareDescription.append("FLASH");
                        break;
                    case 1:
                        firmwareDescription.append("EEPROM");
                        break;
                    default:
                        firmwareDescription.append(destination);
                        break;
                }
            }
            firmwareDescription.append("\"/>");

            firmwareDescription.append("<Nodetext Lang=\"en-EN\" Text=\"");
            for (int index = 0; index < parts.length - 1; index++) {
                firmwareDescription.append(parts[index]).append(" ");
            }
            firmwareDescription.append("\"/>");
        }
        else {
            firmwareDescription.append("<Nodetext Lang=\"de-DE\" Text=\"Nicht erkannte Firmware\"/>");
            firmwareDescription.append("<Nodetext Lang=\"en-EN\" Text=\"Unknown Firmware\"/>");
        }

        String destinationName = null;
        switch (destination) {
            case 0:
            case 1:
                destinationName = Resources.getString(FirmwareUpdatePanel.class, "memoryType-" + destination);
                break;
            default:
                destinationName = Resources.getString(FirmwareUpdatePanel.class, "memoryType-other");
                break;
        }

        firmwareDescription
            .append("<Node xsi:type=\"FirmwareNode\" Comment=\"The Temporary Firmware\" DestinationNumber=\"")
            .append(destination).append("\" >");
        firmwareDescription
            .append("<Nodetext Lang=\"de-DE\" Text=\"Firmware Destination: ").append(destinationName).append("\"/>");
        firmwareDescription
            .append("<Nodetext Lang=\"en-EN\" Text=\"Firmware Destination: ").append(destinationName).append("\"/>");
        firmwareDescription.append("<Filename>").append(firmwareFile.getName()).append("</Filename>");
        firmwareDescription.append("</Node>");
        firmwareDescription.append("</Node>");
        firmwareDescription.append("</FirmwareDefinition>");
        firmwareDescription.append("</Firmware>");

        Firmware firmware = FirmwareFactory.getFirmware(firmwareDescription);
        if (firmware != null && firmware.getFirmwareDefinition() != null) {
            List<NodeType> nodes = firmware.getFirmwareDefinition().getNode();
            for (NodeType node : nodes) {

                if (node instanceof DeviceNode) {
                    DeviceNode deviceNode = (DeviceNode) node;
                    DefaultMutableTreeNode deviceNodeItem = createDeviceNode(top, deviceNode, lang);
                    top.add(deviceNodeItem);
                }
            }
        }
        else {
            LOGGER.warn("No valid firmware description available.");
        }

        // force refresh tree
        firmwareTreeModel.reload(top);

        for (int i = 0; i < firmwareTree.getRowCount(); i++) {
            firmwareTree.expandRow(i);
        }
    }

    protected DefaultMutableTreeNode createDeviceNode(
        DefaultMutableTreeNode parent, DeviceNode deviceNode, String lang) {

        DefaultMutableTreeNode deviceNodeItem =
            new InvisibleNode(new DeviceData(ICON_FIRMWARE, null,
                NodetextUtils.getText(deviceNode.getNodetext(), lang /* "de-DE" */)));
        List<NodeType> subNodes = deviceNode.getNode();
        for (NodeType subNode : subNodes) {
            if (subNode instanceof FirmwareNode) {
                FirmwareNode firmwareNode = (FirmwareNode) subNode;
                DefaultMutableTreeNode firmwareNodeItem = createFirmwareNode(deviceNodeItem, firmwareNode, lang);
                deviceNodeItem.add(firmwareNodeItem);
            }
            else if (subNode instanceof SimpleNode) {
                SimpleNode simpleNode = (SimpleNode) subNode;
                DefaultMutableTreeNode simpleNodeItem = createSimpleNode(deviceNodeItem, simpleNode, lang);
                deviceNodeItem.add(simpleNodeItem);
            }
        }
        return deviceNodeItem;
    }

    protected DefaultMutableTreeNode createSimpleNode(
        DefaultMutableTreeNode parent, SimpleNode simpleNode, String lang) {

        DefaultMutableTreeNode simpleNodeItem =
            new InvisibleNode(
                new IconData(ICON_FIRMWARE, null, NodetextUtils.getText(simpleNode.getNodetext(), lang /* "de-DE" */)));

        List<NodeType> subNodes = simpleNode.getNode();
        for (NodeType subNode : subNodes) {
            if (subNode instanceof FirmwareNode) {
                FirmwareNode firmwareNode = (FirmwareNode) subNode;
                DefaultMutableTreeNode firmwareNodeItem = createFirmwareNode(simpleNodeItem, firmwareNode, lang);
                simpleNodeItem.add(firmwareNodeItem);
            }
            else if (subNode instanceof SimpleNode) {
                SimpleNode simpleSubNode = (SimpleNode) subNode;
                DefaultMutableTreeNode simpleSubNodeItem = createSimpleNode(simpleNodeItem, simpleSubNode, lang);
                simpleNodeItem.add(simpleSubNodeItem);
            }
        }

        return simpleNodeItem;
    }

    protected DefaultMutableTreeNode createFirmwareNode(
        DefaultMutableTreeNode parent, FirmwareNode firmwareNode, String lang) {

        DefaultMutableTreeNode firmwareNodeItem =
            new InvisibleNode(new FirmwareData(ICON_FIRMWARE, null,
                NodetextUtils.getText(firmwareNode.getNodetext(), lang /* "de-DE" */), firmwareNode));
        return firmwareNodeItem;
    }
}
