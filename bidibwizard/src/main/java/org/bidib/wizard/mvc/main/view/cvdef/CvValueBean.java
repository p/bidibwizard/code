package org.bidib.wizard.mvc.main.view.cvdef;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.common.bean.Bean;

public class CvValueBean<T> extends Bean {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(CvValueBean.class);

    private T cvValue;

    private String cvNumber;

    private CvNode cvNode;

    public CvValueBean() {

    }

    public T getCvValue() {
        return cvValue;
    }

    public void setCvValue(T cvValue) {
        LOGGER.debug("Set the value: {}", cvValue);
        T oldValue = this.cvValue;
        this.cvValue = cvValue;

        firePropertyChange("cvValue", oldValue, this.cvValue);
    }

    /**
     * @return the cvNumber
     */
    public String getCvNumber() {
        return cvNumber;
    }

    /**
     * @param cvNumber
     *            the cvNumber to set
     */
    public void setCvNumber(String cvNumber) {
        this.cvNumber = cvNumber;
    }

    /**
     * @return the cvNode
     */
    public CvNode getCvNode() {
        return cvNode;
    }

    /**
     * @param cvNode
     *            the cvNode to set
     */
    public void setCvNode(CvNode cvNode) {
        this.cvNode = cvNode;

        setCvNumber(cvNode.getCV().getNumber());
    }
}
