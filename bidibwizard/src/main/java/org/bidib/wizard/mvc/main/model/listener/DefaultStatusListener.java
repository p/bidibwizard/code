package org.bidib.wizard.mvc.main.model.listener;

/**
 * Empty default implementation of the StatusListener interface.
 */
public abstract class DefaultStatusListener implements StatusListener {

    public void cdChanged(boolean cd) {

    }

    public void runningChanged(boolean running) {

    }

    public void rxChanged(boolean rx) {

    }

    public void txChanged(boolean tx) {

    }

    public void ctsChanged(boolean cts) {

    }
}
