package org.bidib.wizard.mvc.railcomplus.view;

import java.util.LinkedList;
import java.util.List;

import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.main.view.table.listener.ButtonListener;
import org.bidib.wizard.mvc.railcomplus.model.RailcomPlusDecoderModel;
import org.bidib.wizard.mvc.railcomplus.model.listener.DecoderAddressListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.adapter.AbstractTableAdapter;
import com.jgoodies.binding.list.SelectionInList;

public class RailcomPlusTableTableModel extends AbstractTableAdapter<RailcomPlusDecoderModel> implements ButtonListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(RailcomPlusTableTableModel.class);

    private static final long serialVersionUID = 1L;

    public static final int COLUMN_MANUFACTURER = 0;

    public static final int COLUMN_DECMUN = 1;

    public static final int COLUMN_ADDRESS = 2;

    private List<DecoderAddressListener> addressListeners = new LinkedList<>();

    private static final String[] COLUMNNAMES = new String[] {
        Resources.getString(RailcomPlusTableTableModel.class, "manufacturer"),
        Resources.getString(RailcomPlusTableTableModel.class, "decmun"),
        Resources.getString(RailcomPlusTableTableModel.class, "address") };

    public RailcomPlusTableTableModel(SelectionInList<RailcomPlusDecoderModel> decoderList) {
        super(decoderList, COLUMNNAMES);

        LOGGER.info("Current listModel: {}", getListModel());
    }

    @Override
    public Class<?> getColumnClass(int column) {
        switch (column) {
            case COLUMN_MANUFACTURER:
            case COLUMN_DECMUN:
                return Integer.class;
            case COLUMN_ADDRESS:
                return Integer.class;
            default:
                return Object.class;
        }
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        if (column == COLUMN_ADDRESS) {
            return true;
        }
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        RailcomPlusDecoderModel decoder = (RailcomPlusDecoderModel) getRow(rowIndex);
        switch (columnIndex) {
            case COLUMN_MANUFACTURER:
                return decoder.getDecoderManufacturer();
            case COLUMN_DECMUN:
                return decoder.getDecoderMun();
            case COLUMN_ADDRESS:
                return decoder.getDecoderAddress();
            default:
                return null;
        }
    }

    @Override
    public void setValueAt(Object value, int row, int column) {
        LOGGER.info("Set the value, value: {}, row: {}, column: {}", value, row, column);
        RailcomPlusDecoderModel decoder = (RailcomPlusDecoderModel) getRow(row);
        switch (column) {
            case COLUMN_ADDRESS:
                decoder.setDecoderAddress((Integer) value);
                break;
            default:
                break;
        }
    }

    @Override
    public void buttonPressed(int row, int column) {

        RailcomPlusDecoderModel decoder = (RailcomPlusDecoderModel) getRow(row);

        LOGGER.info("Set address for decoder: {}", decoder);
        fireAddressButtonPressed(decoder);
    }

    public void addDecoderAddressListener(DecoderAddressListener listener) {
        addressListeners.add(listener);
    }

    private void fireAddressButtonPressed(RailcomPlusDecoderModel decoder) {
        for (DecoderAddressListener listener : addressListeners) {
            listener.setAddress(decoder);
        }
    }

}
