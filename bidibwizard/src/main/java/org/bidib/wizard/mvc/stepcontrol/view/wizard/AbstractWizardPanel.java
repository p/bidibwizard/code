package org.bidib.wizard.mvc.stepcontrol.view.wizard;

import javax.swing.SwingUtilities;

import org.bidib.wizard.mvc.common.view.wizard.JWizardComponents;
import org.bidib.wizard.mvc.common.view.wizard.JWizardPanel;

public abstract class AbstractWizardPanel extends JWizardPanel {

    public AbstractWizardPanel(JWizardComponents wizardComponents) {
        super(wizardComponents);

        if (!getWizardComponents().onLastPanel()) {
            getWizardComponents().getFinishButton().setVisible(false);
        }
    }

    @Override
    protected void initPanel() {
    }

    public void setStepValid(boolean valid) {
        setNextButtonEnabled(valid);
    }

    public boolean isStepValid() {
        // return motorCharacteristicsValidationModel.getValidStateNoWarnOrErrors();
        return false;
    }

    @Override
    public void next() {
        super.next();
        if (getWizardComponents().onLastPanel()) {
            getWizardComponents().getNextButton().setVisible(false);
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    getWizardComponents().getFinishButton().setVisible(true);
                    getWizardComponents().getFinishButton().requestFocus();
                }
            });
        }
        else {
            getWizardComponents().getNextButton().setVisible(true);
            getWizardComponents().getFinishButton().setVisible(false);
        }
    }

    @Override
    public void back() {
        super.back();
        if (getWizardComponents().onLastPanel()) {
            getWizardComponents().getNextButton().setVisible(false);
            getWizardComponents().getFinishButton().setVisible(true);
        }
        else {
            getWizardComponents().getNextButton().setVisible(true);
            getWizardComponents().getFinishButton().setVisible(false);
        }
    }
}
