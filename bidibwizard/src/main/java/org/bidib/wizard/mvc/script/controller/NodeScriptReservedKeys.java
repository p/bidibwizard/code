package org.bidib.wizard.mvc.script.controller;

public interface NodeScriptReservedKeys {

    public static final String KEY_UNIQUEID = "uniqueid";

    public static final String KEY_POWER_USER = "power_user";

    public static final String KEY_PREVENT_REPLACE_LABELS = "prevent_replace_labels";

    public static final String KEY_ACCESSORY_COUNT = "node_accessory_count";

    public static final String KEY_ACCESSORY_MACRO_MAPPED = "node_accessory_macro_mapped";

    public static final String KEY_MACRO_COUNT = "node_macro_count";

    public static final String KEY_MACRO_SIZE = "node_macro_size";

    public static final String KEY_DIMMRANGE = "dimm_range";

    public static final String KEY_NODE_CV = "cv";

    public static final String KEY_NODE_VID = "vid";

    public static final String KEY_NODE_PID = "pid";

    public static final String KEY_NODE_FIRMWARE_VERSION = "node_firmware_version";

    public static final String KEY_BACKLIGHT_COUNT = "node_backlight_count";

    public static final String KEY_INPUT_COUNT = "node_input_count";

    public static final String KEY_LIGHT_COUNT = "node_light_count";

    public static final String KEY_SERVO_COUNT = "node_servo_count";

    public static final String KEY_SWITCH_COUNT = "node_switch_count";
}
