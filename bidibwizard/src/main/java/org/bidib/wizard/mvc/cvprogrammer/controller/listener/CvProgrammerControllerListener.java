package org.bidib.wizard.mvc.cvprogrammer.controller.listener;

import org.bidib.jbidibc.core.AddressData;
import org.bidib.jbidibc.core.enumeration.CommandStationPom;
import org.bidib.wizard.mvc.main.model.Node;

public interface CvProgrammerControllerListener {
    void close();

    // void read(Node node, AddressData locoAddress, CommandStationPom opCode, int cvNumber);

    void sendRequest(Node node, AddressData locoAddress, CommandStationPom opCode, int cvNumber, int cvValue);
}
