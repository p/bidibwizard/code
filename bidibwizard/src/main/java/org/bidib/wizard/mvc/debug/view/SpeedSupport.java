package org.bidib.wizard.mvc.debug.view;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SpeedSupport {
    private static final Logger LOGGER = LoggerFactory.getLogger(SpeedSupport.class);

    public static final String PATTERN_SPEED = "^([0-9]{1,3})";

    private static Pattern speedPattern = Pattern.compile(PATTERN_SPEED);

    public static String parseSpeed(StringBuilder input, int start) {

        try {
            if (input.substring(start).startsWith("time =")) {

                // // time = 1234; length =345*87; speed =100km/h
                int startParseSpeed = input.substring(start).indexOf("; speed") + 8 + start;

                Matcher m = speedPattern.matcher(input.substring(startParseSpeed + 1));

                while (m.find()) {
                    String speed = m.group(0);
                    LOGGER.info("Detected speed: {}", speed);

                    return speed;
                }

            }
        }
        catch (Exception ex) {
            LOGGER.warn("Prepare speed message for TC9 and copy to clipboard failed.", ex);
        }

        return null;
    }

}
