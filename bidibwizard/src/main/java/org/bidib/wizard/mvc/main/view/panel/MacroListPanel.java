package org.bidib.wizard.mvc.main.view.panel;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.LinkedList;

import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.common.utils.ImageUtils;
import org.bidib.wizard.labels.Labels;
import org.bidib.wizard.labels.MacroLabelFactory;
import org.bidib.wizard.labels.PortLabelUtils;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.bidib.wizard.mvc.common.view.list.DefaultListCellEditor;
import org.bidib.wizard.mvc.common.view.list.DefaultMutableListModel;
import org.bidib.wizard.mvc.common.view.list.JListMutable;
import org.bidib.wizard.mvc.common.view.panel.DisabledPanel;
import org.bidib.wizard.mvc.main.model.Macro;
import org.bidib.wizard.mvc.main.model.MacroSaveState;
import org.bidib.wizard.mvc.main.model.MacroUtils;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.function.Function;
import org.bidib.wizard.mvc.main.model.listener.MacroListener;
import org.bidib.wizard.mvc.main.view.component.LabelList;
import org.bidib.wizard.mvc.main.view.menu.MacroListMenu;
import org.bidib.wizard.mvc.main.view.menu.listener.MacroListMenuListener;
import org.bidib.wizard.mvc.main.view.panel.listener.MacroListListener;
import org.bidib.wizard.mvc.main.view.panel.listener.MacroTableListener;
import org.bidib.wizard.mvc.preferences.model.Preferences;
import org.bidib.wizard.script.node.types.TargetType;
import org.bidib.wizard.utils.MacroListUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MacroListPanel extends LabelListPanel<Macro>
    implements MacroListMenuListener, MacroListener, org.bidib.wizard.mvc.main.model.listener.MacroListListener,
    ChangeLabelSupport {
    private static final Logger LOGGER = LoggerFactory.getLogger(MacroListPanel.class);

    private static final String EMPTY_LABEL = " ";

    private final Collection<MacroListListener> macroListListeners = new LinkedList<MacroListListener>();

    private final MainModel model;

    private final MacroListMenu macroListMenu = new MacroListMenu();

    private final MacroParameterPanel macroParameterPanel;

    private final MacroContentPanel macroContentPanel;

    private final JPanel borderPanel = new JPanel();

    private final TitledBorder border =
        BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), EMPTY_LABEL);

    private final DisabledPanel disabledBorderPanel;

    private Macro macro;

    private JScrollPane scrollMacroList;

    private JPanel buttonPanel;

    private boolean showPowerUser;

    private final JPanel contentPanel;

    private final static class MacroListRenderer extends DefaultListCellRenderer {
        private static final long serialVersionUID = 1L;

        private ImageIcon bidibErrorIcon;

        private ImageIcon pendingChangesIcon;

        private ImageIcon savedChangesIcon;

        private ImageIcon permanentlyStoredIcon;

        private ImageIcon notLoadedFromNodeIcon;

        public MacroListRenderer() {
            bidibErrorIcon = ImageUtils.createImageIcon(NodeTree.class, "/icons/error-leaf.png");
            pendingChangesIcon = ImageUtils.createImageIcon(NodeTree.class, "/icons/16x16/write.png");
            savedChangesIcon = ImageUtils.createImageIcon(NodeTree.class, "/icons/16x16/savetonode.png");
            permanentlyStoredIcon = ImageUtils.createImageIcon(NodeTree.class, "/icons/16x16/lock.png");
            notLoadedFromNodeIcon = ImageUtils.createImageIcon(NodeTree.class, "/icons/16x16/load-from-node.png");
        }

        @Override
        public Component getListCellRendererComponent(
            JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            // prepare the default renderer
            Component renderer = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            if (value instanceof Macro) {
                Macro macro = (Macro) value;
                if (macro.isContainsError()) {
                    LOGGER.info("The current macro contains an error: {}", macro);
                    // set the error icon
                    setIcon(bidibErrorIcon);
                }
                else {
                    switch (macro.getMacroSaveState()) {
                        case NOT_LOADED_FROM_NODE:
                            // set the not loaded from icon
                            setIcon(notLoadedFromNodeIcon);
                            break;
                        case PENDING_CHANGES:
                            // set the pending changes icon
                            setIcon(pendingChangesIcon);
                            break;
                        case SAVED_ON_NODE:
                            setIcon(savedChangesIcon);
                            break;
                        case PERMANENTLY_STORED_ON_NODE:
                            setIcon(permanentlyStoredIcon);
                            break;
                        default:
                            LOGGER.error("Unknown macro save state detected: {}", macro.getMacroSaveState());
                            break;
                    }
                }

                String text = null;
                if (StringUtils.isNotBlank(macro.getLabel())) {
                    text = String.format("%1$02d : %2$s", macro.getId(), macro.getLabel());
                }
                else if (macro.getId() > -1) {
                    text = String.format("%1$02d :", macro.getId());
                }
                else {
                    text = " ";
                }

                setText(text);

            }
            return renderer;
        }
    }

    public MacroListPanel(final MainModel model) {
        // create the list for the macros
        super(new LabelList<Macro>(new DefaultMutableListModel() {
            private static final long serialVersionUID = 1L;

            @Override
            public void setValueAt(Object value, int index) {
                LOGGER.info("Set the new value: {}, index: {}", value, index);

                if (value instanceof String) {
                    long uniqueId = model.getSelectedNode().getUniqueId();

                    Labels macroLabels =
                        DefaultApplicationContext.getInstance().get(DefaultApplicationContext.KEY_MACRO_LABELS,
                            Labels.class);

                    // the name of the macro was changed
                    Macro macro = (Macro) super.get(index);
                    String label = (String) value;

                    macro.setLabel(label);

                    MacroLabelFactory factory = new MacroLabelFactory();
                    PortLabelUtils.replaceLabel(macroLabels, uniqueId, macro.getId(), label,
                        MacroLabelFactory.DEFAULT_LABELTYPE);
                    factory.saveLabels(uniqueId, macroLabels);
                    return;
                }

                super.setValueAt(value, index);
            }
        }));
        this.model = model;
        model.addMacroListListener(this);

        contentPanel = new JPanel() {
            private static final long serialVersionUID = 1L;

            @Override
            public String getName() {
                // this is used as tab title
                return Resources.getString(MacroListPanel.class, "name");
            }
        };
        contentPanel.setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));

        getLabelList().addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                handleMouseEvent(e, macroListMenu);
            }

            public void mouseReleased(MouseEvent e) {
                handleMouseEvent(e, macroListMenu);
            }
        });
        getLabelList().setItems(model.getMacros().toArray(new Macro[0]));
        getLabelList().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        getLabelList().setCellRenderer(new MacroListRenderer());

        ((JListMutable) getLabelList()).setListCellEditor(new DefaultListCellEditor(new JTextField()));

        macroListMenu.addMenuListener(this);

        borderPanel.setLayout(new BorderLayout());
        borderPanel.setBorder(border);

        macroParameterPanel = new MacroParameterPanel(model);

        borderPanel.add(macroParameterPanel, BorderLayout.NORTH);

        macroContentPanel = new MacroContentPanel(model);
        borderPanel.add(macroContentPanel, BorderLayout.CENTER);

        createAndAddButtonPanel(Preferences.getInstance().isPowerUser());

        disabledBorderPanel = new DisabledPanel(borderPanel);

        scrollMacroList = new JScrollPane((Component) getLabelList());
        JScrollPane scrollMacroContent = new JScrollPane(disabledBorderPanel);

        JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, scrollMacroList, scrollMacroContent);

        splitPane.setContinuousLayout(true);
        splitPane.setOneTouchExpandable(true);
        splitPane.setResizeWeight(0.2);

        contentPanel.add(splitPane, BorderLayout.CENTER);
        setEnabled(false);

        Preferences.getInstance().addPropertyChangeListener(Preferences.PROPERTY_POWER_USER,
            new PropertyChangeListener() {

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    boolean isPowerUser = Preferences.getInstance().isPowerUser();
                    LOGGER.info("The power user flag has changed, isPowerUser: {}", isPowerUser);
                    createAndAddButtonPanel(isPowerUser);
                }
            });
    }

    public JPanel getComponent() {
        return contentPanel;
    }

    private void createAndAddButtonPanel(boolean isPowerUser) {
        if (buttonPanel != null && showPowerUser == isPowerUser) {
            LOGGER.info("Power user flag has not changed.");
            return;
        }

        if (buttonPanel != null) {
            borderPanel.remove(buttonPanel);
        }

        if (isPowerUser) {
            buttonPanel = preparePowerUserButtonPanel();
        }
        else {
            buttonPanel = prepareNormalUserButtonPanel();
        }
        showPowerUser = isPowerUser;

        borderPanel.add(buttonPanel, BorderLayout.SOUTH);
    }

    private JPanel prepareNormalUserButtonPanel() {

        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridLayout(1, 0, 5, 0));

        JButton reloadButton = new JButton(Resources.getString(getClass(), "load"));
        reloadButton.setToolTipText(Resources.getString(getClass(), "load.tooltip"));

        reloadButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireReloadMacro();
            }
        });
        buttonPanel.add(reloadButton);

        JButton saveButton = new JButton(Resources.getString(getClass(), "save"));
        saveButton.setToolTipText(Resources.getString(getClass(), "save.tooltip"));

        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireSaveMacro();
            }
        });
        buttonPanel.add(saveButton);

        JButton testButton = new JButton(Resources.getString(getClass(), "test"));
        testButton.setToolTipText(Resources.getString(getClass(), "test.tooltip"));

        // test macro
        testButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireTestMacro(false);
            }
        });
        buttonPanel.add(testButton);

        JButton stopButton = new JButton(Resources.getString(getClass(), "stop"));
        stopButton.setToolTipText(Resources.getString(getClass(), "stop.tooltip"));

        stopButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireStopMacro();
            }
        });
        buttonPanel.add(stopButton);

        return buttonPanel;
    }

    private JPanel preparePowerUserButtonPanel() {

        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridLayout(1, 0, 5, 0));

        JButton reloadButton = new JButton(Resources.getString(getClass(), "reload"));
        reloadButton.setToolTipText(Resources.getString(getClass(), "reload.tooltip"));

        reloadButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireReloadMacro();
            }
        });
        buttonPanel.add(reloadButton);

        JButton transferButton = new JButton(Resources.getString(getClass(), "transfer"));
        transferButton.setToolTipText(Resources.getString(getClass(), "transfer.tooltip"));

        transferButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                LOGGER.info("Transfer button was pressed.");
                fireTransferMacro();
            }
        });
        buttonPanel.add(transferButton);

        JButton testButton = new JButton(Resources.getString(getClass(), "testPower"));
        testButton.setToolTipText(Resources.getString(getClass(), "testPower.tooltip"));

        // test macro
        testButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireTestMacro(true);
            }
        });
        buttonPanel.add(testButton);

        JButton stopButton = new JButton(Resources.getString(getClass(), "stop"));
        stopButton.setToolTipText(Resources.getString(getClass(), "stop.tooltip"));

        stopButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireStopMacro();
            }
        });
        buttonPanel.add(stopButton);

        JButton saveButton = new JButton(Resources.getString(getClass(), "save"));
        saveButton.setToolTipText(Resources.getString(getClass(), "save.tooltip"));

        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireSaveMacro();
            }
        });
        buttonPanel.add(saveButton);

        JButton remoteStartButton = new JButton(Resources.getString(getClass(), "remoteStart"));
        remoteStartButton.setToolTipText(Resources.getString(getClass(), "remoteStart.tooltip"));

        remoteStartButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireRemoteStartMacro();
            }
        });
        buttonPanel.add(remoteStartButton);

        return buttonPanel;
    }

    public void addMacroListListener(MacroListListener l) {
        addLabelChangedListener(l);
        macroListListeners.add(l);
    }

    public void addMacroTableListener(MacroTableListener l) {
        macroContentPanel.addTableListener(l);
    }

    public void setEnabled(boolean enabled) {
        disabledBorderPanel.setEnabled(enabled);
    }

    private void fireExportMacro() {
        Macro macro = model.getSelectedMacro();

        if (!validateMacro(macro, "export", "ask_continue_macro_remove_empty_step", "ask_empty_macro")) {
            LOGGER.info("User aborted export macro: {}", macro);
            return;
        }

        if (macro.isValid()) {
            for (MacroListListener l : macroListListeners) {
                l.exportMacro(model.getSelectedMacro());
            }
        }
        else {
            // show warning dialog
            JOptionPane.showMessageDialog(contentPanel, Resources.getString(getClass(), "macro_content_not_valid"),
                Resources.getString(getClass(), "export"), JOptionPane.WARNING_MESSAGE);
        }
    }

    private void fireImportMacro() {
        for (MacroListListener l : macroListListeners) {
            l.importMacro();
        }
    }

    private void fireReloadMacro() {
        for (MacroListListener l : macroListListeners) {
            l.reloadMacro(model.getSelectedMacro());
        }
    }

    private void fireSaveMacro() {
        Macro macro = model.getSelectedMacro();

        if (!validateMacro(macro, "save", "ask_continue_macro_remove_empty_step", "ask_empty_macro")) {
            LOGGER.info("User aborted save macro: {}", macro);
            return;
        }

        if (macro.isValid()) {
            for (MacroListListener l : macroListListeners) {
                l.saveMacro(macro);
            }
        }
        else {
            // show warning dialog
            JOptionPane.showMessageDialog(contentPanel, Resources.getString(getClass(), "macro_content_not_valid"),
                Resources.getString(getClass(), "save"), JOptionPane.WARNING_MESSAGE);
        }
    }

    private void fireTestMacro(boolean transferBeforeStart) {
        Macro macro = model.getSelectedMacro();

        if (!validateMacro(macro, "start", "ask_continue_macro_remove_empty_step", "ask_empty_macro")) {
            LOGGER.info("User aborted start macro: {}", macro);
            return;
        }

        if (macro.isValid()) {

            if (!transferBeforeStart && macro.getMacroSaveState().equals(MacroSaveState.PENDING_CHANGES)) {
                // show a hint to transfer the macro to the node
                int result =
                    JOptionPane.showConfirmDialog(contentPanel,
                        Resources.getString(getClass(), "macro_transfer_pending_changes_before_test"),
                        Resources.getString(getClass(), "start"), JOptionPane.YES_NO_OPTION,
                        JOptionPane.WARNING_MESSAGE);

                if (result == JOptionPane.YES_OPTION) {
                    transferBeforeStart = true;
                }
            }

            for (MacroListListener l : macroListListeners) {
                l.startMacro(macro, transferBeforeStart);
            }
        }
        else {
            // show warning dialog
            JOptionPane.showMessageDialog(contentPanel, Resources.getString(getClass(), "macro_content_not_valid"),
                Resources.getString(getClass(), "start"), JOptionPane.WARNING_MESSAGE);
        }
    }

    private void fireRemoteStartMacro() {
        Macro macro = model.getSelectedMacro();
        for (MacroListListener l : macroListListeners) {
            l.startMacro(macro, false);
        }
    }

    private void fireStopMacro() {
        for (MacroListListener l : macroListListeners) {
            l.stopMacro(model.getSelectedMacro());
        }
    }

    private void fireInitializeMacro() {
        for (MacroListListener l : macroListListeners) {
            l.initializeMacro(model.getSelectedMacro());
        }
    }

    private void fireTransferMacro() {
        Macro macro = model.getSelectedMacro();

        if (!validateMacro(macro, "transfer", "ask_continue_macro_remove_empty_step", "ask_empty_macro")) {
            LOGGER.info("User aborted transfer macro: {}", macro);
            return;
        }

        if (macro.isValid()) {
            for (MacroListListener l : macroListListeners) {
                l.transferMacro(model.getSelectedMacro());
            }
        }
        else {
            // show warning dialog
            JOptionPane.showMessageDialog(contentPanel, Resources.getString(getClass(), "macro_content_not_valid"),
                Resources.getString(getClass(), "transfer"), JOptionPane.WARNING_MESSAGE);
        }
    }

    @Override
    public void exportMacro() {
        fireExportMacro();
    }

    @Override
    public void importMacro() {
        fireImportMacro();
    }

    @Override
    public void startMacro() {
        boolean powerUser = Preferences.getInstance().isPowerUser();
        fireTestMacro(powerUser);
    }

    @Override
    public void stopMacro() {
        fireStopMacro();
    }

    @Override
    public void initializeMacro() {
        fireInitializeMacro();
    }

    @Override
    public void listChanged() {
        LOGGER.info("Macro list has been changed.");
        getLabelList().setItems(model.getMacros().toArray(new Macro[0]));
        resetMacroScrollPane();
        setEnabled(false);
    }

    private void resetMacroScrollPane() {
        JScrollBar verticalScrollBar = scrollMacroList.getVerticalScrollBar();
        JScrollBar horizontalScrollBar = scrollMacroList.getHorizontalScrollBar();
        verticalScrollBar.setValue(verticalScrollBar.getMinimum());
        horizontalScrollBar.setValue(horizontalScrollBar.getMinimum());
    }

    @Override
    public void macroChanged() {
        if (macro != null) {
            macro.removeMacroListener(this);
        }

        macro = model.getSelectedMacro();
        LOGGER.info("The selected macro has been changed, macro: {}", macro);

        if (macro != null) {
            macro.addMacroListener(this);
            labelChanged(macro.toString());
        }
        else {
            labelChanged(EMPTY_LABEL);
        }
        setEnabled(macro != null);

        // transfer the focus back to the macro list
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                // SwingUtilities.updateComponentTreeUI((Component) getLabelList());
                ((JListMutable) getLabelList()).requestFocusInWindow();
            }
        });
    }

    @Override
    public void pendingChangesChanged() {

        getLabelList().refreshView();
    }

    @Override
    public void functionsAdded(int row, Function<? extends BidibStatus>[] functions) {
    }

    @Override
    public void functionRemoved(int row) {
    }

    @Override
    public void functionsRemoved() {
    }

    @Override
    public void labelChanged(String label) {
        if (label != null && !label.equals(EMPTY_LABEL)) {
            label = macro.toString();
        }
        if (label != null && !label.equals(EMPTY_LABEL)) {
            border.setTitle(label + ":");
        }
        else {
            border.setTitle("");
        }
        borderPanel.repaint();
        // fireLabelChanged(model.getSelectedMacro(), label);
    }

    @Override
    public void changeLabel(TargetType portType) {
        int portNum = portType.getPortNum();
        String label = portType.getLabel();

        Macro macro = MacroListUtils.findMacroByMacroNumber(model.getMacros(), portNum);
        if (macro != null) {
            LOGGER.info("Current macro: {}, new label: {}", macro, label);
            macro.setLabel(label);
            if (macro.equals(model.getSelectedMacro())) {
                labelChanged(label);
            }
            // else {
            fireLabelChanged(macro, label);
            // }
        }
    }

    @Override
    public void functionMoved(int fromIndex, int toIndex, Function<? extends BidibStatus> fromFunction) {
    }

    @Override
    public void startConditionChanged() {
    }

    @Override
    public void slowdownFactorChanged() {
    }

    @Override
    public void cyclesChanged() {
    }

    /**
     * @param macro
     *            the macro
     * @param titleKey
     *            the title key for the dialog
     * @param messageRemoveEmptyStepsKey
     *            the key for the remove empty steps message
     * @param messageEmptyMacroKey
     *            the key for the empty macro message
     * @return {@code true} : continue, {@code false} : abort
     */
    private boolean validateMacro(
        final Macro macro, String titleKey, String messageRemoveEmptyStepsKey, String messageEmptyMacroKey) {

        if (!macro.isValid()) {

            if (CollectionUtils.isNotEmpty(macro.getFunctions()) && MacroUtils.hasEmptySteps(macro)) {
                // step with no function in macro, ask the user to export empty macro
                // show dialog
                int result =
                    JOptionPane.showConfirmDialog(contentPanel,
                        Resources.getString(MacroListPanel.class, messageRemoveEmptyStepsKey),
                        Resources.getString(MacroListPanel.class, titleKey), JOptionPane.OK_CANCEL_OPTION,
                        JOptionPane.WARNING_MESSAGE);
                if (result != JOptionPane.OK_OPTION) {
                    LOGGER.info("User canceled processing macro with empty steps.");
                    return false;
                }

                LOGGER.info("Remove empty macro steps.");
                MacroUtils.removeEmptySteps(macro);

            }

            if (CollectionUtils.isEmpty(macro.getFunctions())) {
                // no functions in macro, ask the user to export empty macro
                // show dialog
                int result =
                    JOptionPane.showConfirmDialog(contentPanel,
                        Resources.getString(MacroListPanel.class, messageEmptyMacroKey),
                        Resources.getString(MacroListPanel.class, titleKey), JOptionPane.OK_CANCEL_OPTION);
                if (result != JOptionPane.OK_OPTION) {
                    LOGGER.info("User canceled processing empty macro.");
                    return false;
                }
            }
        }
        return true;
    }
}
