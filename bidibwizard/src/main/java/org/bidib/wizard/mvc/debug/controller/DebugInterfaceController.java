package org.bidib.wizard.mvc.debug.controller;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.bidib.jbidibc.core.ConnectionListener;
import org.bidib.jbidibc.core.exception.PortNotFoundException;
import org.bidib.jbidibc.core.exception.PortNotOpenedException;
import org.bidib.jbidibc.debug.DebugInterface;
import org.bidib.jbidibc.debug.DebugMessageListener;
import org.bidib.jbidibc.debug.DebugMessageReceiver;
import org.bidib.jbidibc.debug.DebugReaderFactory;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.common.model.CommPort;
import org.bidib.wizard.mvc.common.view.DockKeys;
import org.bidib.wizard.mvc.debug.controller.listener.DebugInterfaceControllerListener;
import org.bidib.wizard.mvc.debug.model.DebugInterfaceModel;
import org.bidib.wizard.mvc.debug.view.DebugInterfaceView;
import org.bidib.wizard.mvc.debug.view.listener.DebugInterfaceViewListener;
import org.bidib.wizard.mvc.debug.view.listener.ProgressStatusCallback;
import org.bidib.wizard.mvc.logger.view.BidibLogsAppender;
import org.bidib.wizard.mvc.preferences.model.Preferences;
import org.bidib.wizard.utils.DockUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vlsolutions.swing.docking.Dockable;
import com.vlsolutions.swing.docking.DockableState;
import com.vlsolutions.swing.docking.DockingConstants;
import com.vlsolutions.swing.docking.DockingDesktop;
import com.vlsolutions.swing.docking.DockingUtilities;
import com.vlsolutions.swing.docking.RelativeDockablePosition;
import com.vlsolutions.swing.docking.TabbedDockableContainer;
import com.vlsolutions.swing.docking.event.DockableStateChangeEvent;
import com.vlsolutions.swing.docking.event.DockableStateChangeListener;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Appender;
import ch.qos.logback.core.CoreConstants;
import ch.qos.logback.core.FileAppender;
import ch.qos.logback.core.status.Status;

public class DebugInterfaceController implements DebugInterfaceControllerListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(DebugInterfaceController.class);

    private static final String APPENDER_NAME = DebugInterfaceController.class.getName();

    private final DockingDesktop desktop;

    private DebugInterfaceModel debugInterfaceModel;

    private DebugInterfaceView debugInterfaceView;

    private DebugInterface debugReader;

    private DebugMessageReceiver messageReceiver;

    private DebugMessageListener messageListener;

    private List<byte[]> firmwareContent;

    protected final ScheduledExecutorService sendFileWorker = Executors.newScheduledThreadPool(1);

    public DebugInterfaceController(final DockingDesktop desktop) {
        this.desktop = desktop;
    }

    public void start() {
        // check if the debug interface view is already opened
        String searchKey = DockKeys.DEBUG_INTERFACE_VIEW;
        LOGGER.info("Search for view with key: {}", searchKey);
        Dockable view = desktop.getContext().getDockableByKey(searchKey);
        if (view != null) {
            LOGGER.info("Select the existing debug interface view instead of open a new one.");
            DockUtils.selectWindow(view);
            return;
        }

        LOGGER.info("Create new DebugInterfaceView.");
        debugInterfaceModel = new DebugInterfaceModel();

        debugInterfaceView = new DebugInterfaceView(desktop, this, debugInterfaceModel);

        DockableState[] dockables = desktop.getDockables();
        LOGGER.info("Current dockables: {}", new Object[] { dockables });
        if (dockables.length > 1) {

            DockableState tabPanelNodeDetails = null;
            // search the node details tab panel
            for (DockableState dockable : dockables) {

                if (DockKeys.DOCKKEY_TAB_PANEL.equals(dockable.getDockable().getDockKey())) {
                    LOGGER.info("Found the tab panel dockable.");
                    tabPanelNodeDetails = dockable;

                    break;
                }
            }

            Dockable dock = desktop.getDockables()[1].getDockable();
            if (tabPanelNodeDetails != null) {
                LOGGER.info("Add the debug interface view next to the node details panel.");
                dock = tabPanelNodeDetails.getDockable();

                TabbedDockableContainer container = DockingUtilities.findTabbedDockableContainer(dock);
                int order = 0;
                if (container != null) {
                    order = container.getTabCount();
                }
                LOGGER.info("Add new debugInterfaceView at order: {}", order);

                desktop.createTab(dock, debugInterfaceView, order, true);
            }
            else {
                desktop.split(dock, debugInterfaceView, DockingConstants.SPLIT_RIGHT);
            }
        }
        else {
            desktop.addDockable(debugInterfaceView, RelativeDockablePosition.RIGHT);
        }

        debugInterfaceView.addDebugInterfaceViewListener(new DebugInterfaceViewListener() {

            @Override
            public void openConnection() {

                Integer baudRate = debugInterfaceModel.getBaudRate();

                if (baudRate == null) {
                    LOGGER.warn("No baudrate selected!");

                    JOptionPane.showMessageDialog(debugInterfaceView.getComponent(),
                        Resources.getString(DebugInterfaceController.class, "select-baudrate"), "Debug Interface",
                        JOptionPane.ERROR_MESSAGE);
                    return;
                }
                // PreferencesPortType debugPort = Preferences.getInstance().getPreviousSelectedDebugPort();
                CommPort debugPort = debugInterfaceModel.getSelectedPort();
                if (debugPort == null || StringUtils.isBlank(debugPort.getName())) {
                    LOGGER.warn("No debugPort selected!");

                    JOptionPane.showMessageDialog(debugInterfaceView.getComponent(),
                        Resources.getString(DebugInterfaceController.class, "select-port"), "Debug Interface",
                        JOptionPane.ERROR_MESSAGE);

                    return;
                }
                LOGGER.info("Current debugPort: {}", debugPort);
                String portName = debugPort.getName();

                if (debugReader == null) {
                    LOGGER.info("Create new instance of debug reader.");

                    try {
                        messageListener = new DebugMessageListener() {

                            @Override
                            public void debugMessage(final String message) {
                                LOGGER.info("debug message received size: {}", message.length());
                                debugInterfaceView.addLog(message);
                            }
                        };
                        messageReceiver = new DebugMessageReceiver();
                        messageReceiver.addMessageListener(messageListener);

                        // create the debug reader
                        String serialPortProvider = Preferences.getInstance().getSerialPortProvider().toLowerCase();
                        LOGGER.info("Selected serial port provider: {}", serialPortProvider);
                        DebugReaderFactory.SerialImpl serialImpl = null;
                        switch (serialPortProvider) {
                            case "scm":
                                serialImpl = DebugReaderFactory.SerialImpl.SCM;
                                break;
                            default:
                                serialImpl = DebugReaderFactory.SerialImpl.RXTX;
                                break;
                        }

                        debugReader = DebugReaderFactory.getDebugReader(serialImpl, messageReceiver);
                        LOGGER.info("Created debugReader: {}", debugReader);
                        // debugReader = new DebugReader(messageReceiver);
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Open debug port failed.", ex);
                    }
                }

                try {
                    debugReader.open(portName, baudRate, new ConnectionListener() {

                        @Override
                        public void opened(String port) {
                            LOGGER.info("Port opened: {}", port);
                            SwingUtilities.invokeLater(new Runnable() {
                                public void run() {
                                    debugInterfaceModel.setConnected(true);
                                }
                            });
                        }

                        @Override
                        public void closed(String port) {
                            LOGGER.info("Port closed: {}", port);
                            SwingUtilities.invokeLater(new Runnable() {
                                public void run() {
                                    debugInterfaceModel.setConnected(false);
                                }
                            });
                        }

                        @Override
                        public void status(String messageKey) {
                            // no implementation
                        }

                    }, null);
                }
                catch (PortNotFoundException | PortNotOpenedException ex) {
                    LOGGER.warn("Open debug port failed.", ex);

                    JOptionPane.showMessageDialog(debugInterfaceView.getComponent(),
                        "Open debug Port failed. Reason: " + ex.getReason(), "Debug Interface",
                        JOptionPane.ERROR_MESSAGE);
                }
                catch (Exception ex) {
                    LOGGER.warn("Open debug port failed.", ex);
                }
            }

            @Override
            public void closeConnection() {
                LOGGER.info("Close the debug connection.");

                fireCloseConnection();
            }

            @Override
            public void transmit() {
                String sendText = debugInterfaceModel.getSendText();
                
                if (StringUtils.isEmpty(sendText)) {
                	LOGGER.info("No data to send!");
                	return;
                }
                
                LOGGER.info("Send text to debugReader: {}, lineEnding: {}", sendText,
                    debugInterfaceModel.getLineEnding());

                if (debugReader != null) {
                    debugReader.send(sendText, debugInterfaceModel.getLineEnding());
                }
            }

            @Override
            public void transmitFile(final AtomicBoolean continueTransmit, final ProgressStatusCallback callback) {
                fireTransmitFile(continueTransmit, callback);
            }
        });

        desktop.addDockableStateChangeListener(new DockableStateChangeListener() {

            @Override
            public void dockableStateChanged(DockableStateChangeEvent event) {
                if (event.getNewState().getDockable().equals(debugInterfaceView) && event.getNewState().isClosed()) {
                    LOGGER.info("LogPanelView was closed, free resources.");

                    // debugInterfaceView.close();

                    try {
                        // assume SLF4J is bound to logback in the current environment
                        LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();

                        // ch.qos.logback.classic.Logger log = context.getLogger("org.bidib");
                        ch.qos.logback.classic.Logger logTX = context.getLogger("TX");
                        ch.qos.logback.classic.Logger logRX = context.getLogger("RX");
                        Appender<ILoggingEvent> appender = logTX.getAppender(BidibLogsAppender.APPENDER_NAME);
                        LOGGER.info("Current BidibLogsAppender: {}", appender);

                        if (appender != null) {
                            LOGGER.info("Detach and stop the appender.");
                            logTX.detachAppender(appender);
                            logRX.detachAppender(appender);

                            appender.stop();
                        }
                    }
                    catch (Exception ex) {
                        LOGGER.info("Remove BidibLogsAppender failed.");
                    }
                }
            }
        });

        debugInterfaceModel.addPropertyChangeListener(DebugInterfaceModel.PROPERTY_LOG_TO_FILE,
            new PropertyChangeListener() {

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    fireLogToFileChanged();
                }
            });

        debugInterfaceModel.addPropertyChangeListener(DebugInterfaceModel.PROPERTY_LOGFILE_NAME,
            new PropertyChangeListener() {

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    fireLogToFileChanged();
                }
            });
    }

    private void fireTransmitFile(final AtomicBoolean continueTransmit, final ProgressStatusCallback callback) {
        File sendFile = debugInterfaceModel.getSendFile();
        LOGGER.info("Send file content to debugReader: {}", sendFile);

        if (debugReader != null) {
            // prepare the data to send
            this.firmwareContent = null;

            InputStream input = null;
            List<byte[]> firmwareContent = new ArrayList<>();
            try {
                byte[] buffer = new byte[256];
                input = new FileInputStream(sendFile);

                int readBytes = IOUtils.read(input, buffer);
                while (readBytes > 0) {
                    LOGGER.info("Number of bytes read: {}", readBytes);
                    byte[] packet = Arrays.copyOf(buffer, readBytes);
                    firmwareContent.add(packet);

                    readBytes = IOUtils.read(input, buffer);
                }

                // keep the firmware content
                this.firmwareContent = firmwareContent;
            }
            catch (IOException ex) {
                LOGGER.info("No firmware content file found.");
            }
            finally {
                if (input != null) {
                    try {
                        input.close();
                    }
                    catch (Exception e) {
                        LOGGER.warn("Close input stream failed.", e);
                    }
                    input = null;
                }
            }

            if (CollectionUtils.isNotEmpty(this.firmwareContent)) {
                LOGGER.info("Send first packet to debug reader.");

                debugInterfaceModel.setTransferInProgress(true);

                sendFirmwarePackets(continueTransmit, callback);
            }
        }
    }

    private void sendFirmwarePackets(final AtomicBoolean continueTransmit, final ProgressStatusCallback callback) {

        Runnable runnable = new Runnable() {
            public void run() {
                LOGGER.info("Start sending firmware packets.");

                int packetsSent = 0;
                int totalPackets = firmwareContent.size();
                try {
                    for (byte[] content : firmwareContent) {

                        if (!continueTransmit.get()) {
                            LOGGER.warn("Transfer firmware was aborted by user.");
                            break;
                        }

                        debugReader.send(content);

                        packetsSent++;
                        if (callback != null) {
                            callback.statusChanged((packetsSent * 100) / totalPackets);
                        }
                    }
                }
                catch (Exception ex) {
                    LOGGER.warn("Send firmware content to debug reader failed.", ex);
                }

                debugInterfaceModel.setTransferInProgress(false);

                if (callback != null) {
                    callback.transferFinished();
                }
            }
        };
        // Start the send firmware files process
        sendFileWorker.execute(runnable);

    }

    private void fireCloseConnection() {
        if (debugReader != null) {
            LOGGER.info("Close the debug reader.");
            debugReader.close();

            debugInterfaceModel.setConnected(false);

            messageReceiver.removeMessageListener(messageListener);
            messageListener = null;
            messageReceiver = null;

            debugReader = null;
        }
        else {
            LOGGER.info("debug reader not available.");
        }
    }

    @Override
    public void viewClosed() {
        LOGGER.info("The view is closed.");

        fireCloseConnection();
    }

    protected void fireLogToFileChanged() {

        if (debugInterfaceModel.isLogToFile() && StringUtils.isNotBlank(debugInterfaceModel.getLogFileName())) {
            LOGGER.info("Add logger for debug interface: {}", debugInterfaceModel.getLogFileName());

            try {
                // assume SLF4J is bound to logback in the current environment
                LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();

                ch.qos.logback.classic.Logger logTX = context.getLogger(DebugInterfaceView.LOGGER_PANE_NAME);
                Appender<ILoggingEvent> appender = logTX.getAppender(APPENDER_NAME);

                if (appender == null) {

                    // clear all status messages
                    context.getStatusManager().clear();

                    PatternLayoutEncoder ple = new PatternLayoutEncoder();

                    ple.setPattern("%msg%n");
                    ple.setContext(context);
                    ple.start();

                    LOGGER.info("Create new appender.");
                    FileAppender<ILoggingEvent> fileAppender = new FileAppender<>();
                    fileAppender.setFile(debugInterfaceModel.getLogFileName());
                    fileAppender.setAppend(false);
                    fileAppender.setEncoder(ple);
                    fileAppender.setContext(context);
                    fileAppender.setName(APPENDER_NAME);
                    fileAppender.start();

                    logTX.setAdditive(false);
                    logTX.setLevel(Level.INFO);
                    logTX.addAppender(fileAppender);

                    List<Status> statusList = context.getStatusManager().getCopyOfStatusList();
                    if (CollectionUtils.isNotEmpty(statusList)) {
                        for (Status status : statusList) {
                            LOGGER.warn("Current status entry: {}", status);
                        }
                    }
                }
                else {
                    LOGGER.info("{} appender is already assigned.", APPENDER_NAME);
                }
            }
            catch (Exception ex) {
                LOGGER.info("Add debug interface logger failed.");
            }
        }
        else if (!debugInterfaceModel.isLogToFile() || StringUtils.isBlank(debugInterfaceModel.getLogFileName())) {
            LOGGER.info("Check if we must stop the logger.");
            try {
                // assume SLF4J is bound to logback in the current environment
                LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();

                // ch.qos.logback.classic.Logger log = context.getLogger("org.bidib");
                ch.qos.logback.classic.Logger logTX = context.getLogger(DebugInterfaceView.LOGGER_PANE_NAME);
                Appender<ILoggingEvent> appender = logTX.getAppender(APPENDER_NAME);
                LOGGER.info("Current debug interface appender: {}", appender);

                if (appender != null) {
                    // clear all status messages
                    context.getStatusManager().clear();

                    LOGGER.info("Detach and stop the appender.");
                    logTX.detachAppender(appender);

                    ((FileAppender<ILoggingEvent>) appender).stop();

                    // this is an ugly hack ... :-(
                    Map<String, String> map =
                        (Map<String, String>) context.getObject(CoreConstants.RFA_FILENAME_PATTERN_COLLISION_MAP);
                    map.remove(APPENDER_NAME);

                    List<Status> statusList = context.getStatusManager().getCopyOfStatusList();
                    if (CollectionUtils.isNotEmpty(statusList)) {
                        for (Status status : statusList) {
                            LOGGER.warn("Current status entry: {}", status);
                        }
                    }
                }
                else {
                    LOGGER.warn("Appender was not attached.");
                }
            }
            catch (Exception ex) {
                LOGGER.info("Remove debug interface logger failed.");
            }
        }
    }

}
