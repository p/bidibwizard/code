package org.bidib.wizard.mvc.main.view.cvdef;

import org.bidib.jbidibc.core.utils.ByteUtils;

public class OffsetValue implements Comparable<OffsetValue> {

    private final byte value;

    private String description;

    public OffsetValue(byte value) {

        this.value = value;
    }

    public byte getValue() {
        return value;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(description);
        return sb.append(" (").append(ByteUtils.getInt(value)).append(")").toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof OffsetValue) {
            OffsetValue other = (OffsetValue) obj;

            return value == other.getValue();
        }
        return false;
    }

    @Override
    public int hashCode() {
        return ByteUtils.getInt(value);
    }

    @Override
    public int compareTo(OffsetValue other) {

        int myValue = ByteUtils.getInt(value);
        int otherValue = ByteUtils.getInt(other.value);
        if (myValue > otherValue) {
            return 1;
        }
        else if (myValue < otherValue) {
            return -1;
        }
        return 0;
    }
}
