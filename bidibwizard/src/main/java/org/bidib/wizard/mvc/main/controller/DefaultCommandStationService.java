package org.bidib.wizard.mvc.main.controller;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.collections4.Predicate;
import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.DefaultMessageListener;
import org.bidib.jbidibc.core.Feature;
import org.bidib.jbidibc.core.MessageListener;
import org.bidib.jbidibc.core.enumeration.CommandStationState;
import org.bidib.jbidibc.core.utils.NodeUtils;
import org.bidib.wizard.comm.Communication;
import org.bidib.wizard.comm.CommunicationFactory;
import org.bidib.wizard.mvc.common.model.PreferencesPortType;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.preferences.model.Preferences;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultCommandStationService implements CommandStationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultCommandStationService.class);

    // use core pool size of 1 thread, if more threads are needed they will be claimed dynamically
    private final ScheduledExecutorService commandStationWatchdogWorker = Executors.newScheduledThreadPool(1);

    private final Map<org.bidib.jbidibc.core.Node, NodeContainer> watchdogTaskRegistry = new LinkedHashMap<>();

    private final MessageListener messageListener;

    private boolean preventWatchdogTask;

    private static final class NodeContainer {

        ScheduledFuture<?> watchdogTask;

        boolean responsePending;

        public NodeContainer(ScheduledFuture<?> watchdogTask) {
            this.watchdogTask = watchdogTask;
        }
    }

    public DefaultCommandStationService() {

        messageListener = new DefaultMessageListener() {

            @Override
            public void csState(byte[] address, CommandStationState commandStationState) {
                // TODO Auto-generated method stub
                LOGGER.info("Received csState message: {}", commandStationState);

                // find matching node
                org.bidib.jbidibc.core.Node commandStationNode = null;
                for (Entry<org.bidib.jbidibc.core.Node, NodeContainer> entry : watchdogTaskRegistry.entrySet()) {
                    if (Arrays.equals(address, entry.getKey().getAddr())) {
                        LOGGER.info("Found node entry, reset the response pending flag for node: {}", entry.getKey());
                        entry.getValue().responsePending = false;
                        commandStationNode = entry.getKey();
                        break;
                    }
                }

                if (commandStationNode != null) {
                    signalStateChanged(commandStationNode, commandStationState);
                }
                else {
                    LOGGER.info("No matching command station node found with address: {}",
                        NodeUtils.formatAddress(address));
                }
            }

        };
        CommunicationFactory.addMessageListener(messageListener);

        final Preferences preferences = Preferences.getInstance();

        checkSelectedPortType(preferences);

        preferences.addPropertyChangeListener(Preferences.PROPERTY_SELECTED_PORTTYPE, new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                checkSelectedPortType(preferences);
            }
        });
    }

    private void checkSelectedPortType(final Preferences preferences) {
        PreferencesPortType portType = preferences.getSelectedPortType();
        if (portType != null
            && PreferencesPortType.ConnectionPortType.SerialOverTcpPort.equals(portType.getConnectionPortType())) {
            LOGGER.info("The current selected port type is: {}. Do not start watchdog tasks.", portType);

            preventWatchdogTask = true;
        }
    }

    @Override
    public void switchOn(final Node wizardNode, boolean ignoreWatchDog) {

        LOGGER.info("Switch command station on, wizardNode: {}, ignoreWatchDog: {}", wizardNode, ignoreWatchDog);

        setCommandStationState(wizardNode, (ignoreWatchDog ? CommandStationState.GO_IGN_WD : CommandStationState.GO));
    }

    @Override
    public void switchOff(final Node wizardNode) {

        LOGGER.info("Switch command station off, wizardNode: {}", wizardNode);

        // stop the watchdog
        final org.bidib.jbidibc.core.Node node = wizardNode.getNode();
        stopWatchDogTask(node);

        setCommandStationState(wizardNode, CommandStationState.OFF);
    }

    @Override
    public void switchStop(final Node wizardNode, boolean softStop) {

        LOGGER.info("Switch command station on, wizardNode: {}, softStop: {}", wizardNode, softStop);

        final org.bidib.jbidibc.core.Node node = wizardNode.getNode();

        stopWatchDogTask(node);

        CommandStationState commandStationState = CommandStationState.STOP;
        if (softStop) {
            commandStationState = CommandStationState.SOFTSTOP;
        }
        node.setRequestedCommandStationState(commandStationState);

        CommunicationFactory.getInstance().setCommandStationState(node, commandStationState);
    }

    @Override
    public void signalStateChanged(final Node wizardNode, CommandStationState status) {
        LOGGER.info("Status of command station has changed: {}, node: {}", status, wizardNode);
        final org.bidib.jbidibc.core.Node node = wizardNode.getNode();
        signalStateChanged(node, status);
    }

    private void signalStateChanged(final org.bidib.jbidibc.core.Node node, CommandStationState status) {
        LOGGER.debug("Status of command station has changed: {}, node: {}", status, node);

        // if command station is off or PT is active stop the watchdog task
        if (CommandStationState.isOffState(status) || CommandStationState.isPtProgState(status)) {
            stopWatchDogTask(node);
        }
        else {
            LOGGER.info("State has changed, current RequestedCommandStationState: {}, preventWatchdogTask: {}",
                node.getRequestedCommandStationState(), preventWatchdogTask);

            if (!preventWatchdogTask && !watchdogTaskRegistry.containsKey(node)) {
                // we handle no requested command station state as using watchdog by default
                if (node.getRequestedCommandStationState() == null) {
                    LOGGER.info("Set the default requested commandstation state to: {}", CommandStationState.GO_IGN_WD);
                    node.setRequestedCommandStationState(CommandStationState.GO_IGN_WD);
                }

                if (CommandStationState.GO.equals(node.getRequestedCommandStationState())) {
                    LOGGER.warn("Start watchdog task for the current node after state GO was signalled: {}", node);

                    setCommandStationState(node, node.getRequestedCommandStationState());
                }
            }
            else {
                LOGGER.info(
                    "Do not handle state change of command station because the watchdog task is prevented by configuration or watchdog is registered already.");
            }
        }
    }

    @Override
    public void signalNodeRemoved(final Node wizardNode) {

        if (wizardNode.isCommandStation()) {
            // a command station node was removed
            LOGGER.info("A command station node was removed.");
            final org.bidib.jbidibc.core.Node node = wizardNode.getNode();
            stopWatchDogTask(node);
        }
    }

    @Override
    public void stopAllWatchDogTasks() {

        List<org.bidib.jbidibc.core.Node> nodes = new ArrayList<>();
        nodes.addAll(watchdogTaskRegistry.keySet());

        for (org.bidib.jbidibc.core.Node node : nodes) {
            try {
                stopWatchDogTask(node);
            }
            catch (Exception ex) {
                LOGGER.warn("Stop watchdog task failed for node: {}", node, ex);
            }
        }
    }

    private void stopWatchDogTask(final org.bidib.jbidibc.core.Node node) {
        NodeContainer container = watchdogTaskRegistry.get(node);
        if (container != null) {
            LOGGER.info("Stop watchdog task for node: {}", node);
            ScheduledFuture<?> scheduledFuture = container.watchdogTask;
            scheduledFuture.cancel(false);

            LOGGER.info("Stop watchdog task finished for node: {}", node);
            watchdogTaskRegistry.remove(node);
        }
        else {
            LOGGER.info("Stop watchdog task is ignored because no nodeContainer registered for node: {}", node);
        }
    }

    @Override
    public void setCommandStationState(final Node wizardNode, CommandStationState status) {

        final org.bidib.jbidibc.core.Node node = wizardNode.getNode();
        setCommandStationState(node, status);
    }

    private void setCommandStationState(final org.bidib.jbidibc.core.Node node, CommandStationState status) {

        LOGGER.info("Set the command station state for wizardNode: {}, status: {}", node, status);

        final Communication communication = CommunicationFactory.getInstance();

        // if command station is off or PT is active then stop the watchdog task
        if (CommandStationState.isOffState(status) || CommandStationState.isPtProgState(status)) {
            stopWatchDogTask(node);

            LOGGER.info("Set the new requested status: {}, and send status to node: {}", status, node);
            if (CommandStationState.isOffState(status)) {
                // update the state only if status is an OFF status
                node.setRequestedCommandStationState(status);
            }
            communication.setCommandStationState(node, status);
        }
        else {
            LOGGER.info("Switch command station to GO state, status: {}, preventWatchdogTask: {}", status,
                preventWatchdogTask);

            int watchdogTimeout = 0;
            if (CommandStationState.GO == status) {
                // read the watchdog timeout
                Feature genWatchdog = IterableUtils.find(node.getFeatures(), new Predicate<Feature>() {
                    @Override
                    public boolean evaluate(Feature feature) {
                        return feature.isRequestedFeature(BidibLibrary.FEATURE_GEN_WATCHDOG);
                    }
                });

                if (genWatchdog != null) {
                    watchdogTimeout = genWatchdog.getValue();
                    LOGGER.info("The command station has the following watchdog timeout configured: {}",
                        watchdogTimeout);

                    if (watchdogTimeout > 0) {
                        // convert the timeout to milliseconds
                        watchdogTimeout = watchdogTimeout * 100; /* ms */
                    }
                    else {
                        LOGGER.info("No watchdog timeout configured.");
                    }
                }
                else {
                    LOGGER.info("No watchdog feature available.");
                }
            }

            if (watchdogTimeout > 0) {

                if (watchdogTaskRegistry.containsKey(node)) {
                    LOGGER.warn("A watchdog task for the current node already exists: {}", node);

                    return;
                }

                if (!preventWatchdogTask) {

                    int initialDelay = 50;
                    int period = watchdogTimeout - 20;
                    LOGGER.info("Start watchdog task with period: {}, watchdogTimeout: {}", period, watchdogTimeout);

                    // must start task to repeat the MSG_CS_SET_STATE(GO) if supporting watchdog
                    ScheduledFuture<?> scheduledFuture =
                        commandStationWatchdogWorker.scheduleAtFixedRate(new Runnable() {
                            public void run() {
                                LOGGER.info("Watchdog timer fired, send BIDIB_CS_STATE_GO.");

                                if (communication.isOpened()) {
                                    internalSetCommandStationState(communication, node, CommandStationState.GO);
                                }
                                else {
                                    LOGGER.warn("Do not send BIDIB_CS_STATE_GO because the communication is closed.");
                                }
                            }
                        }, initialDelay, period, TimeUnit.MILLISECONDS);

                    // put scheduledFuture into watchdog task registry
                    NodeContainer container = new NodeContainer(scheduledFuture);
                    watchdogTaskRegistry.put(node, container);

                    node.setRequestedCommandStationState(CommandStationState.GO);
                }
                else {
                    LOGGER.warn(
                        "The command station GO state was requested but the watchdog is prevented by the current configuration. The watchdog task is not started!");

                    node.setRequestedCommandStationState(CommandStationState.GO);

                    LOGGER.info("Send the GO to the command station.");
                    internalSetCommandStationState(communication, node, CommandStationState.GO);
                }
            }
            else {
                LOGGER.info("Start command station with ignore watchdog.");
                node.setRequestedCommandStationState(CommandStationState.GO_IGN_WD);

                // communication.setCommandStationState(node, CommandStationState.GO_IGN_WD);
                internalSetCommandStationState(communication, node, CommandStationState.GO_IGN_WD);

                // make sure the watchdog is stopped
                stopWatchDogTask(node);
            }
        }
    }

    private void internalSetCommandStationState(
        final Communication communication, final org.bidib.jbidibc.core.Node node, CommandStationState state) {
        LOGGER.debug("Set the command station state: {}, node: {}", state, node);
        try {
            NodeContainer container = watchdogTaskRegistry.get(node);
            if (container != null && container.responsePending) {

                LOGGER.warn(
                    "The last request to set the command station state was acknowledged! This might be a communication problem. Send stop command to command station.");
                state = CommandStationState.STOP;

                communication.setCommandStationState(node, state);

                stopWatchDogTask(node);
            }
            else {
                if (container != null) {
                    LOGGER.info("Set the response pending flag for node: {}", node);
                    container.responsePending = true;
                }

                communication.setCommandStationState(node, state);

            }
        }
        catch (Exception ex) {
            LOGGER.warn("Set the command station state failed.", ex);
        }
    }

    @Override
    public void clearLocoBuffer(Node node) {

        LOGGER.info("Clear the complete loco buffer of the command station, node: {}", node);

        try {
            final Communication communication = CommunicationFactory.getInstance();
            communication.clearLocoBuffer(node.getNode(), 0);
        }
        catch (Exception ex) {
            LOGGER.warn("Clear the complete loco buffer failed.", ex);
        }

    }
}
