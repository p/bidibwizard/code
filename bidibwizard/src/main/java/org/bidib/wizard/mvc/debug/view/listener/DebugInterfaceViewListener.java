package org.bidib.wizard.mvc.debug.view.listener;

import java.util.concurrent.atomic.AtomicBoolean;

public interface DebugInterfaceViewListener {

    /**
     * Open connection.
     */
    void openConnection();

    /**
     * Close connection.
     */
    void closeConnection();

    /**
     * Transmit data in textfield.
     */
    void transmit();

    /**
     * Transmit the content of a file.
     */
    void transmitFile(final AtomicBoolean continueTransmit, final ProgressStatusCallback callback);
}
