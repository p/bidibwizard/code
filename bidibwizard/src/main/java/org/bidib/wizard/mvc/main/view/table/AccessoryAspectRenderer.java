package org.bidib.wizard.mvc.main.view.table;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class AccessoryAspectRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = 1L;

    private final String defaultPrefix;

    public AccessoryAspectRenderer(String defaultPrefix) {
        this.defaultPrefix = defaultPrefix;
    }

    public Component getTableCellRendererComponent(
        JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        if (value != null) {
            setText(value.toString());
        }
        else {
            setText(defaultPrefix + row);
        }

        return this;
    }
}
