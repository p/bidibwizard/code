package org.bidib.wizard.mvc.pomupdate.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.apache.commons.lang.StringUtils;
import org.bidib.jbidibc.pomupdate.DecoderInformation;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.dialog.EscapeDialog;

import com.jgoodies.forms.builder.ButtonBarBuilder;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.debug.FormDebugPanel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.FormLayout;

public class DecoderFirmwareInfoDialog extends EscapeDialog {

    private static final long serialVersionUID = 1L;

    private final JButton closeButton = new JButton(Resources.getString(getClass(), "close"));

    private JLabel vendorIdLabel;

    private JLabel decoderIdLabel;

    private JLabel decoderTypeLabel;

    private JLabel versionLabel;

    private JLabel releaseDateLabel;

    public DecoderFirmwareInfoDialog(Frame frame, String title, boolean modal) {
        super(frame, (StringUtils.isNotBlank(title) ? title : Resources.getString(DecoderFirmwareInfoDialog.class,
            "title")), modal);
        DefaultFormBuilder builder = null;
        boolean debug = false;
        if (debug) {
            JPanel panel = new FormDebugPanel();
            builder = new DefaultFormBuilder(new FormLayout("100dlu:grow, 3dlu, fill:50dlu:grow"), panel);
        }
        else {
            builder = new DefaultFormBuilder(new FormLayout("100dlu:grow, 3dlu, fill:50dlu:grow"));
        }
        builder.border(Borders.DIALOG);

        closeButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                close();
            }
        });

        vendorIdLabel = new JLabel();
        builder.append("Vendor Id", vendorIdLabel);
        decoderIdLabel = new JLabel();
        builder.append("Decoder Id", decoderIdLabel);
        decoderTypeLabel = new JLabel();
        builder.append("Decoder Type", decoderTypeLabel);

        versionLabel = new JLabel();
        builder.append("Firmware version", versionLabel);
        releaseDateLabel = new JLabel();
        builder.append("Release date", releaseDateLabel);

        // prepare the close button
        JPanel buttons = new ButtonBarBuilder().addGlue().addButton(closeButton).build();
        builder.append(buttons, 3);

        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());
        contentPane.add(builder.build(), BorderLayout.CENTER);
    }

    private void close() {
        setVisible(false);

        dispose();
    }

    public void setDecoderInfo(DecoderInformation information) {
        vendorIdLabel.setText(Integer.toString(information.getVendorId()));
        decoderIdLabel.setText(Integer.toString(information.getDecoderId()));
        decoderTypeLabel.setText(information.getDecoderType().toString());
        versionLabel.setText(information.getFirmwareVersion());
        releaseDateLabel.setText(information.getFirmwareReleaseDate());
    }

    public void showDialog(Component comp) {
        pack();
        setLocationRelativeTo(comp);
        setVisible(true);
    }
}
