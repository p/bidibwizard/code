package org.bidib.wizard.mvc.logger.view;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.SimpleDateFormat;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.text.BadLocationException;

import org.apache.commons.lang.SystemUtils;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.main.view.menu.BasicPopupMenu;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Appender;
import ch.qos.logback.core.spi.AppenderAttachable;

public class LogsPane implements Observer {
    private static final Logger LOGGER = LoggerFactory.getLogger(LogsPane.class);

    private final JScrollPane logsPane = new JScrollPane();

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss.SSS");

    private final JTextArea logsArea = new JTextArea();

    private BidibLogsAppender bidibAppender;

    private final JPopupMenu popupMenu;

    private JMenuItem autoScrollsItem;

    private JMenuItem clearItem;

    /**
     * Creates the text area, sets it as non-editable and sets an observer to intercept logs.
     */
    public LogsPane() {
        logsArea.setEditable(false);
        // logsArea.setFont(UIManager.getDefaults().getFont("Label.font"));
        logsArea.setFont(new Font("Monospaced", Font.PLAIN, 11));

        logsPane.getViewport().add(logsArea, null);
        addObserverToBidibLogAppender();

        logsPane.setAutoscrolls(true);
        popupMenu = new BasicPopupMenu() {
            private static final long serialVersionUID = 1L;
        };
        prepareMenuItems(popupMenu);

        logsArea.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                if (e.getClickCount() == 1 && e.isPopupTrigger()) {
                    LOGGER.debug("Show the popup menu.");

                    e.consume();

                    popupMenu.show(logsArea, e.getX(), e.getY());
                }
            }

            public void mouseReleased(MouseEvent e) {
                LOGGER.debug("Mouse released.");
                if (e.isPopupTrigger()) {
                    mousePressed(e);
                }
            }
        });
    }

    private void prepareMenuItems(JPopupMenu menu) {
        autoScrollsItem = new JMenuItem(Resources.getString(getClass(), "autoScrolls"));

        autoScrollsItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                logsArea.setCaretPosition(logsArea.getDocument().getLength());
                logsArea.setAutoscrolls(true);
            }
        });
        addMenuItem(menu, autoScrollsItem);

        clearItem = new JMenuItem(Resources.getString(getClass(), "clear"));

        clearItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    logsArea.setText(null);
                    logsArea.setCaretPosition(logsArea.getDocument().getLength());
                    logsArea.setAutoscrolls(true);
                }
                catch (Exception ex) {
                    LOGGER.warn("Clear logsarea failed.", ex);
                }
            }
        });
        addMenuItem(menu, clearItem);
    }

    private void addMenuItem(Object menu, JMenuItem menuItem) {
        if (menu instanceof JMenu) {
            ((JMenu) menu).add(menuItem);
        }
        else if (menu instanceof JPopupMenu) {
            ((JPopupMenu) menu).add(menuItem);
        }
    }

    /**
     * Returns the JScrollPane object.
     * 
     * @return the JScrollPane object.
     */
    public JScrollPane get() {
        return logsPane;
    }

    /**
     * Adds this object to the Bidib logs appender observable, to intercept logs.
     * <p>
     * The goal is to be informed when the log appender will received some RX/TX logs.<br>
     * When a log is written, the appender will notify this class which will display it in the text area.
     * </p>
     */
    @SuppressWarnings("unchecked")
    private void addObserverToBidibLogAppender() {
        String appenderName = BidibLogsAppender.APPENDER_NAME;

        LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
        for (Logger logger : context.getLoggerList()) {

            Appender<ILoggingEvent> appender = ((AppenderAttachable<ILoggingEvent>) logger).getAppender(appenderName);
            if (appender instanceof BidibLogsAppender) {
                bidibAppender = (BidibLogsAppender) appender;
                break;
            }
        }

        if (bidibAppender == null) {
            LoggerFactory.getLogger(LogsPane.class).error("Can't find appender with name: {}", appenderName);
        }
        else {
            bidibAppender.getObservable().addObserver(this);
        }
    }

    /**
     * Updates the content of the text area.
     * <p>
     * This method will be called by an observable element.
     * </p>
     * <ul>
     * <li>If the observable is a LogsObservable object, the text area will display the received log.</li>
     * </ul>
     * 
     * @param o
     *            the observable element which will notify this class.
     * @param log
     *            optional parameter (a {@code String} object, when the observable is a {@code LogsObservable} object,
     *            which will contain the log).
     */
    @Override
    public void update(Observable o, final Object log) {
        if (o instanceof BidibLogsObservable) {

            SwingUtilities.invokeLater(new Runnable() {

                @Override
                public void run() {
                    try {
                        int lines = logsArea.getLineCount();
                        if (lines > 200) {
                            // remove the first 50 lines
                            int end = logsArea.getLineEndOffset(/* lines - */50);
                            logsArea.getDocument().remove(0, end);
                        }
                    }
                    catch (BadLocationException ex) {
                        LOGGER.warn("Remove some lines from logsArea failed.", ex);
                    }
                    // Update and scroll pane to the bottom
                    ILoggingEvent loggingEvent = (ILoggingEvent) log;

                    logsArea.append(dateFormat.format(loggingEvent.getTimeStamp()));
                    logsArea.append(" - ");
                    logsArea.append(loggingEvent.getFormattedMessage());
                    logsArea.append(SystemUtils.LINE_SEPARATOR);
                    // logsArea.setCaretPosition(logsArea.getDocument().getLength());
                    logsArea.invalidate();
                }
            });
        }
    }

    public void close() {
        if (bidibAppender != null) {

            LOGGER.info("Remove observer from appender.");
            try {
                bidibAppender.getObservable().deleteObserver(this);
            }
            catch (Exception ex) {
                LOGGER.warn("Remove observer from appender failed.", ex);
            }

            bidibAppender = null;
        }
    }
}
