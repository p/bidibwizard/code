package org.bidib.wizard.mvc.main.controller;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.ConnectionListener;
import org.bidib.jbidibc.core.Feature;
import org.bidib.jbidibc.core.NodeListener;
import org.bidib.jbidibc.core.ProtocolVersion;
import org.bidib.jbidibc.core.SoftwareVersion;
import org.bidib.jbidibc.core.StringData;
import org.bidib.jbidibc.core.enumeration.CommandStationState;
import org.bidib.jbidibc.core.enumeration.LcMacroState;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.PortConfigStatus;
import org.bidib.jbidibc.core.enumeration.SysErrorEnum;
import org.bidib.jbidibc.core.exception.InvalidConfigurationException;
import org.bidib.jbidibc.core.exception.NoAnswerException;
import org.bidib.jbidibc.core.exception.PortNotFoundException;
import org.bidib.jbidibc.core.exception.PortNotOpenedException;
import org.bidib.jbidibc.core.exception.ReasonAware;
import org.bidib.jbidibc.core.helpers.Context;
import org.bidib.jbidibc.core.helpers.DefaultContext;
import org.bidib.jbidibc.core.node.BidibNode;
import org.bidib.jbidibc.core.node.ConfigurationVariable;
import org.bidib.jbidibc.core.node.RootNode;
import org.bidib.jbidibc.core.port.PortConfigValue;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.core.utils.NodeUtils;
import org.bidib.jbidibc.core.utils.ProductUtils;
import org.bidib.jbidibc.debug.DebugMessageListener;
import org.bidib.jbidibc.debug.DebugMessageReceiver;
import org.bidib.jbidibc.debug.LineEndingEnum;
import org.bidib.jbidibc.debug.rxtx.DebugReader;
import org.bidib.jbidibc.exchange.vendorcv.VendorCvData;
import org.bidib.jbidibc.exchange.vendorcv.VendorCvError;
import org.bidib.jbidibc.exchange.vendorcv.VendorCvFactory;
import org.bidib.jbidibc.ui.OS;
import org.bidib.wizard.comm.AnalogPortStatus;
import org.bidib.wizard.comm.BacklightPortStatus;
import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.comm.Communication;
import org.bidib.wizard.comm.CommunicationFactory;
import org.bidib.wizard.comm.ServoPortStatus;
import org.bidib.wizard.comm.exception.InternalStartupException;
import org.bidib.wizard.comm.listener.CommunicationListener;
import org.bidib.wizard.common.context.ApplicationContext;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.common.utils.ImageUtils;
import org.bidib.wizard.dialog.FileDialog;
import org.bidib.wizard.labels.AccessoryLabelFactory;
import org.bidib.wizard.labels.AccessoryLabelUtils;
import org.bidib.wizard.labels.AnalogPortLabelFactory;
import org.bidib.wizard.labels.BacklightPortLabelFactory;
import org.bidib.wizard.labels.FeedbackPortLabelFactory;
import org.bidib.wizard.labels.FlagLabelFactory;
import org.bidib.wizard.labels.InputPortLabelFactory;
import org.bidib.wizard.labels.LabelType;
import org.bidib.wizard.labels.Labels;
import org.bidib.wizard.labels.LightPortLabelFactory;
import org.bidib.wizard.labels.MacroLabelFactory;
import org.bidib.wizard.labels.MotorPortLabelFactory;
import org.bidib.wizard.labels.NodeLabelFactory;
import org.bidib.wizard.labels.PortLabelUtils;
import org.bidib.wizard.labels.ServoPortLabelFactory;
import org.bidib.wizard.labels.SoundPortLabelFactory;
import org.bidib.wizard.labels.SwitchPairPortLabelFactory;
import org.bidib.wizard.labels.SwitchPortLabelFactory;
import org.bidib.wizard.labels.WizardLabelFactory;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.bidib.wizard.main.WizardStartupParams;
import org.bidib.wizard.mvc.common.model.PreferencesPortType;
import org.bidib.wizard.mvc.main.controller.listener.AlertListener;
import org.bidib.wizard.mvc.main.model.Accessory;
import org.bidib.wizard.mvc.main.model.AccessoryFactory;
import org.bidib.wizard.mvc.main.model.AccessorySaveState;
import org.bidib.wizard.mvc.main.model.AnalogPort;
import org.bidib.wizard.mvc.main.model.BacklightPort;
import org.bidib.wizard.mvc.main.model.DefaultTransferListener;
import org.bidib.wizard.mvc.main.model.FeedbackPort;
import org.bidib.wizard.mvc.main.model.Flag;
import org.bidib.wizard.mvc.main.model.GenericPort;
import org.bidib.wizard.mvc.main.model.InputPort;
import org.bidib.wizard.mvc.main.model.LightPort;
import org.bidib.wizard.mvc.main.model.Macro;
import org.bidib.wizard.mvc.main.model.MacroFactory;
import org.bidib.wizard.mvc.main.model.MacroFactory.ExportFormat;
import org.bidib.wizard.mvc.main.model.MacroRef;
import org.bidib.wizard.mvc.main.model.MacroSaveState;
import org.bidib.wizard.mvc.main.model.MacroUtils;
import org.bidib.wizard.mvc.main.model.MainMessageListener;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.MotorPort;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.model.Port;
import org.bidib.wizard.mvc.main.model.ServoPort;
import org.bidib.wizard.mvc.main.model.SoundPort;
import org.bidib.wizard.mvc.main.model.SwitchPairPort;
import org.bidib.wizard.mvc.main.model.SwitchPort;
import org.bidib.wizard.mvc.main.model.function.Function;
import org.bidib.wizard.mvc.main.model.listener.BacklightPortListener;
import org.bidib.wizard.mvc.main.model.listener.CommandStationStatusListener;
import org.bidib.wizard.mvc.main.model.listener.CvDefinitionRequestListener;
import org.bidib.wizard.mvc.main.model.listener.DefaultNodeListListener;
import org.bidib.wizard.mvc.main.model.listener.NodeListListener;
import org.bidib.wizard.mvc.main.model.listener.NodeSelectionListener;
import org.bidib.wizard.mvc.main.model.listener.OutputListener;
import org.bidib.wizard.mvc.main.model.listener.ServoPortListener;
import org.bidib.wizard.mvc.main.view.MainNodeListActionListener;
import org.bidib.wizard.mvc.main.view.MainView;
import org.bidib.wizard.mvc.main.view.component.AccessoryFileDialog;
import org.bidib.wizard.mvc.main.view.component.LabeledDisplayItems;
import org.bidib.wizard.mvc.main.view.component.MacroFileDialog;
import org.bidib.wizard.mvc.main.view.menu.listener.MainMenuListener;
import org.bidib.wizard.mvc.main.view.panel.listener.AccessoryListListener;
import org.bidib.wizard.mvc.main.view.panel.listener.AccessoryTableListener;
import org.bidib.wizard.mvc.main.view.panel.listener.MacroListListener;
import org.bidib.wizard.mvc.main.view.panel.listener.MacroTableListener;
import org.bidib.wizard.mvc.main.view.panel.listener.StatusListener;
import org.bidib.wizard.mvc.main.view.statusbar.StatusBar;
import org.bidib.wizard.mvc.preferences.model.Preferences;
import org.bidib.wizard.mvc.script.view.NodeScripting;
import org.bidib.wizard.mvc.stepcontrol.view.StepControlPanel;
import org.bidib.wizard.mvc.tips.controller.TipOfDayClosedListener;
import org.bidib.wizard.mvc.tips.controller.TipOfDayController;
import org.bidib.wizard.script.node.types.TargetType;
import org.bidib.wizard.utils.DockUtils;
import org.bidib.wizard.utils.NodeStateUtils;
import org.bidib.wizard.utils.PortListUtils;
import org.oxbow.swingbits.dialog.task.TaskDialogs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jidesoft.alert.Alert;
import com.jidesoft.swing.FolderChooser;
import com.vlsolutions.swing.docking.Dockable;

public class MainController implements MainControllerInterface {
    private static final Logger LOGGER = LoggerFactory.getLogger(MainController.class);

    private final MainModel model = new MainModel();

    private MainView view;

    private MainNodeListActionListener mainNodeListActionListener;

    private Labels accessoryLabels;

    private Labels analogPortLabels;

    private Labels feedbackPortLabels;

    private Labels inputPortLabels;

    private Labels lightPortLabels;

    private Labels backlightPortLabels;

    private Labels macroLabels;

    private Labels motorPortLabels;

    private Labels nodeLabels;

    private Labels servoPortLabels;

    private Labels soundPortLabels;

    private Labels switchPortLabels;

    private Labels switchPairPortLabels;

    private Labels flagLabels;

    private boolean ignoreWaitTimeout;

    private final Map<org.bidib.jbidibc.core.Node, Thread> newNodeCreatedThreadRegistry = new LinkedHashMap<>();

    private final ScheduledExecutorService selectedNodeChangeWorker = Executors.newScheduledThreadPool(1);

    private CommandStationService commandStationService;

    private UsbHotPlugController usbHotPlugController;

    public MainController() {
        ignoreWaitTimeout = Preferences.getInstance().isIgnoreWaitTimeout();
        LOGGER.info("Created MainController, ignoreWaitTimeout: {}", ignoreWaitTimeout);
    }

    /**
     * Create a new wizard node and return the initialized wizard node.
     */
    protected Node createNode(Communication communication, org.bidib.jbidibc.core.Node node) {
        Node wizardNode = null;
        LOGGER.info("Create new 'wizard' node from jbidibc.Node: {}", node);

        if (communication != null && node != null) {
            wizardNode = new Node(node);

            communication.clearCachedData(node);
            if (!communication.isNodeRegistered(node)) {
                communication.registerNode(node);
            }

            // TODO cleanup this hack an solve it better
            // load the labels of the node
            Long uniqueId = Long.valueOf(node.getUniqueId());

            NodeLabelFactory factory = new NodeLabelFactory();
            factory.getLabels(uniqueId);

            boolean limitedNode = true;
            int magic = BidibNode.BIDIB_MAGIC_UNKNOWN;
            try {
                magic = communication.getMagic(node);
            }
            catch (NoAnswerException ex) {
                // the magic stays as BIDIB_MAGIC_UNKNOWN and this signals an error
                LOGGER.warn("The node did not answer to the first getMagic request: {}", node, ex);

                try {
                    Thread.sleep(500);
                    magic = communication.getMagic(node);
                }
                catch (NoAnswerException ex1) {
                    // the magic stays as BIDIB_MAGIC_UNKNOWN and this signals an error
                    LOGGER.warn("The node did not answer to the second getMagic request: {}", node, ex1);
                }
                catch (InterruptedException ex1) {
                    LOGGER.warn(
                        "The current thread was interrupted before the second getMagic request to node was sent: {}",
                        node, ex1);
                }
            }

            List<Feature> features = Collections.emptyList();

            switch (magic) {
                case BidibNode.BIDIB_MAGIC_UNKNOWN:
                    // the node has an error
                    wizardNode.setNodeHasError(true);
                    break;
                case BidibLibrary.BIDIB_BOOT_MAGIC:
                    LOGGER.info(
                        "Node returned boot magic: {}. This is a limited node that does not answer to feature requests!",
                        magic);
                    wizardNode.setBootloaderNode(true);

                    // get the firmware version
                    SoftwareVersion softwareVersion = communication.getSoftwareVersion(node);
                    LOGGER.info("SW version of new node: {}", softwareVersion);
                    wizardNode.getNode().setSoftwareVersion(softwareVersion);

                    // get the protocol version
                    ProtocolVersion protocolVersion = communication.getProtocolVersion(node);
                    LOGGER.info("Protocol version of new node: {}", protocolVersion);
                    wizardNode.getNode().setProtocolVersion(protocolVersion);

                    // the node supports FW update by specification
                    wizardNode.setUpdatable(true);
                    break;
                default:
                    LOGGER.info("Node returned magic: {}", magic);

                    // the node has no limitations
                    limitedNode = false;

                    // get the firmware version
                    softwareVersion = communication.getSoftwareVersion(node);
                    LOGGER.info("SW version of new node: {}", softwareVersion);
                    wizardNode.getNode().setSoftwareVersion(softwareVersion);

                    // get the protocol version
                    protocolVersion = communication.getProtocolVersion(node);
                    LOGGER.info("Protocol version of new node: {}", protocolVersion);
                    wizardNode.getNode().setProtocolVersion(protocolVersion);

                    // query all features of the node
                    features = communication.getFeatures(node, true);

                    // after we fetch the magic we must try to get the FEATURE_RELEVANT_PID_BITS

                    Feature relevantPidBits = Feature.findFeature(features, BidibLibrary.FEATURE_RELEVANT_PID_BITS);

                    if (relevantPidBits != null) {
                        LOGGER.info("Set the relevant PID bits: {}", relevantPidBits);
                        node.setFeature(relevantPidBits);
                        wizardNode.getNode().setRelevantPidBits(relevantPidBits.getValue());
                    }

                    // get the FEATURE_STRING_SIZE
                    Feature stringSize = Feature.findFeature(features, BidibLibrary.FEATURE_STRING_SIZE);

                    if (stringSize != null && stringSize.getValue() > 0) {
                        LOGGER.info("Node supports FEATURE_STRING_SIZE: {}", stringSize.getValue());
                        node.setFeature(stringSize);

                        // the OneControl-2.00.06 has a problem with a user string > 21 characters
                        if (ProductUtils.isOneControl(node.getUniqueId())
                            && node.getSoftwareVersion().equals(new SoftwareVersion(2, 0, 6))) {
                            LOGGER.info("The string size for OneControl-2.00.06 is limited to 20.");
                            wizardNode.getNode().setStringSize(20);
                        }
                        else {
                            wizardNode.getNode().setStringSize(stringSize.getValue());
                        }

                        int namespace = StringData.NAMESPACE_NODE;
                        int index = 0;
                        String storedString = null;
                        try {
                            storedString = communication.getString(node, namespace, index);
                        }
                        catch (RuntimeException ex) {
                            LOGGER.warn("Fetch stored string failed, namespace: {}, index: {}", namespace, index, ex);
                        }
                        LOGGER.info("Fetched storedString[{}:{}]: {}", namespace, index, storedString);
                        wizardNode.getNode().setStoredString(index, storedString);
                        index++;
                        try {
                            storedString = communication.getString(node, namespace, index);
                        }
                        catch (RuntimeException ex) {
                            LOGGER.warn("Fetch stored string failed, namespace: {}, index: {}", namespace, index, ex);
                        }
                        LOGGER.info("Fetched storedString[{}:{}]: {}", namespace, index, storedString);
                        wizardNode.getNode().setStoredString(index, storedString);
                    }
                    else {
                        LOGGER.info("Node has not FEATURE_STRING_SIZE available or string size is 0.");
                    }

                    // check if the node supports FW update
                    wizardNode.setUpdatable(communication.isUpdatable(node));

                    if (ProductUtils.isOneBootloader(node.getUniqueId())) {
                        LOGGER.info("Found a OneBootloader node: {}", node);
                    }
                    break;
            }

            if (!wizardNode.isNodeHasError()) {
                // collect some configuration of the node
                wizardNode.setCommandStation(communication.isCommandStation(node));

                if (wizardNode.isCommandStation()) {
                    LOGGER.info("Ask the commandstation node if the RailCom+ feature is available.");
                    wizardNode.setRailComPlusAvailable(communication.isRailComPlusAvailable(node));

                    LOGGER.info("Set the POM Update available for the command station node.");
                    wizardNode.setPomUpdateAvailable(true);
                }
                wizardNode.setBooster(communication.isBooster(node));

                // update the labels of the node
                AnalogPortLabelFactory analogPortLabelFactory = new AnalogPortLabelFactory();
                analogPortLabelFactory.getLabels(uniqueId);

                BacklightPortLabelFactory backlightPortLabelFactory = new BacklightPortLabelFactory();
                backlightPortLabelFactory.getLabels(uniqueId);

                InputPortLabelFactory inputPortLabelFactory = new InputPortLabelFactory();
                inputPortLabelFactory.getLabels(uniqueId);

                LightPortLabelFactory lightPortLabelFactory = new LightPortLabelFactory();
                lightPortLabelFactory.getLabels(uniqueId);

                MotorPortLabelFactory motorPortLabelFactory = new MotorPortLabelFactory();
                motorPortLabelFactory.getLabels(uniqueId);

                ServoPortLabelFactory servoPortLabelFactory = new ServoPortLabelFactory();
                servoPortLabelFactory.getLabels(uniqueId);

                SoundPortLabelFactory soundPortLabelFactory = new SoundPortLabelFactory();
                soundPortLabelFactory.getLabels(uniqueId);

                SwitchPortLabelFactory switchPortLabelFactory = new SwitchPortLabelFactory();
                switchPortLabelFactory.getLabels(uniqueId);

                FeedbackPortLabelFactory feedbackPortLabelFactory = new FeedbackPortLabelFactory();
                feedbackPortLabelFactory.getLabels(uniqueId);

                // check if the node has feedback ports to know its a feedback device
                if (!limitedNode && NodeUtils.hasFeedbackFunctions(node.getUniqueId())) {
                    int numFeedbackPorts = communication.getFeedbackPorts(node).size();
                    LOGGER.info("Number of feedback ports: {}", numFeedbackPorts);
                    if (numFeedbackPorts > 0) {
                        LOGGER.debug("The new node has feedback ports.");
                        wizardNode.setAddressMessagesEnabled(communication.isAddressMessagesEnabled(node));
                        wizardNode.setFeedbackMessagesEnabled(communication.isFeedbackMessagesEnabled(node));
                        wizardNode.setFeedbackMirrorDisabled(communication.isFeedbackMirrorDisabled(node));
                    }
                }
                else {
                    LOGGER.info("The new node has no feedback functions.");
                }

                // check if the node has switch functions
                if (!limitedNode && NodeUtils.hasSwitchFunctions(node.getUniqueId())) {

                    Feature portFlatModel = Feature.findFeature(features, BidibLibrary.FEATURE_CTRL_PORT_FLAT_MODEL);
                    Integer flatPortModelPortCount = null;
                    if (portFlatModel != null) {
                        LOGGER.info("The feature FEATURE_CTRL_PORT_FLAT_MODEL is available: {}", portFlatModel);
                        flatPortModelPortCount = portFlatModel.getValue();
                    }
                    else {
                        LOGGER.info(
                            "The feature FEATURE_CTRL_PORT_FLAT_MODEL is not available. Check for extended flat port model.");
                    }

                    // check for extended flat port model
                    Feature portFlatModelExtended =
                        Feature.findFeature(features, BidibLibrary.FEATURE_CTRL_PORT_FLAT_MODEL_EXTENDED);
                    if (portFlatModelExtended != null) {
                        LOGGER.info("The feature FEATURE_CTRL_PORT_FLAT_MODEL_EXTENDED is available: {}",
                            portFlatModelExtended);
                        if (flatPortModelPortCount != null) {
                            int extendedPortCount =
                                256 * portFlatModelExtended.getValue() + flatPortModelPortCount.intValue();
                            flatPortModelPortCount = extendedPortCount;
                        }
                        else {
                            flatPortModelPortCount = 256 * portFlatModelExtended.getValue();
                        }
                    }
                    else {
                        LOGGER.info("The feature FEATURE_CTRL_PORT_FLAT_MODEL_EXTENDED is not available.");
                    }

                    if (flatPortModelPortCount != null) {
                        wizardNode.getNode().setPortFlatModel(flatPortModelPortCount.intValue());
                    }

                    int numInputPorts = communication.getInputPorts(node).size();
                    LOGGER.debug("Number of input ports: {}", numInputPorts);
                    if (numInputPorts > 0) {
                        LOGGER.debug("The new node has input ports.");
                        wizardNode.setKeyMessagesEnabled(communication.isKeyMessagesEnabled(node));
                    }
                    int macroLength = communication.getMaxMacroLength(node);
                    LOGGER.debug("Supported macro lenght: {}", macroLength);

                    wizardNode.setMaxMacroSteps(macroLength);

                    if (macroLength > 0) {
                        LOGGER.debug("The new node has macros.");

                        // check from the features
                        wizardNode.setDccStartEnabled(communication.isDccStartEnabled(node));
                        wizardNode.setExternalStartEnabled(communication.isExternalStartMacroEnabled(node));
                    }
                    wizardNode.setStorableMacroCount(communication.getStorableMacroCount(node));

                    // load labels for macros and accessories
                    MacroLabelFactory macroLabelFactory = new MacroLabelFactory();
                    macroLabelFactory.getLabels(uniqueId);

                    AccessoryLabelFactory accessoryLabelFactory = new AccessoryLabelFactory();
                    accessoryLabelFactory.getLabels(uniqueId);

                    // add the flags
                    FlagLabelFactory flagLabelFactory = new FlagLabelFactory();
                    flagLabelFactory.getLabels(uniqueId);

                    // TODO prepare the flags
                    final List<Flag> flags = new LinkedList<Flag>();
                    for (int id = 0; id < 16; id++) {
                        Flag flag = new Flag();

                        flag.setId(id);
                        flags.add(flag);
                    }
                    wizardNode.setFlags(flags);

                }
                else {
                    LOGGER.debug("The new node has no accessory functions.");
                }
            }
            else {
                LOGGER.error("The new node has errors: {}", wizardNode);
            }

            wizardNode.setLabel(PortLabelUtils
                .getLabel(nodeLabels, node.getUniqueId(), 0, NodeLabelFactory.DEFAULT_LABELTYPE).getLabelString());
            LOGGER.info("Prepared new 'wizard' node: {}", wizardNode);
        }
        return wizardNode;
    }

    @Override
    public void clearNodes() {
        LOGGER.info("Clear the nodes from the model.");

        model.getStatusModel().setCd(false);
        model.getStatusModel().setRx(false);
        model.getStatusModel().setTx(false);
        LOGGER.warn("Communication factory is not available!");
        model.getStatusModel().setCd(false);

        model.clearNodes();
    }

    @Override
    public Collection<Node> getNodes() {
        return model.getNodes();
    }

    private Node findNode(org.bidib.jbidibc.core.Node commNode) {
        Node result = null;

        if (commNode != null) {
            for (Node node : model.getNodes()) {
                if (node.getNode().getUniqueId() == commNode.getUniqueId()) {
                    result = node;
                    break;
                }
            }
        }
        return result;
    }

    private void loadLabels() {

        // make sure the labels directory is available and write enabled
        String labelPath = Preferences.getInstance().getLabelV2Path();
        LOGGER.info("Check if the directory for labels exists: {}", labelPath);
        boolean labelPathIsValid = false;

        do {
            try {
                Path dir = Paths.get(labelPath);

                if (!Files.exists(dir)) {
                    LOGGER.info("Try to create the directory for labels: {}", labelPath);

                    try {
                        dir = Files.createDirectories(dir);

                        LOGGER.info("Created new directory for labels: {}", dir);
                    }
                    catch (IOException ioex) {
                        LOGGER.warn("Create new directory for labels failed.", ioex);

                        if (OS.isLinux() && labelPath.startsWith("/home/")) {
                            String userName = System.getProperty("user.name");
                            // check if the name is correct
                            int beginIndex = 6;
                            int endIndex = labelPath.indexOf("/", beginIndex);
                            String configuredUserName = labelPath.substring(beginIndex, endIndex);
                            if (!userName.equals(configuredUserName)) {

                                LOGGER.warn(
                                    "The current username '{}' does not match the configured username '{}' for the label path.",
                                    userName, configuredUserName);

                                Object[] options =
                                    { Resources.getString(Labels.class, "labeldirerror.correct"),
                                        Resources.getString(Labels.class, "labeldirerror.ignore") };

                                int answer =
                                    JOptionPane.showOptionDialog(JOptionPane.getFrameForComponent(null),
                                        Resources.getString(Labels.class, "labeldirerror.message_username_mismatch",
                                            new Object[] { userName, configuredUserName, labelPath }),
                                        Resources.getString(Labels.class, "labeldirerror.title"),
                                        JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE, null, options,
                                        options[0]);

                                if (answer == JOptionPane.YES_OPTION) {
                                    labelPath = "/home/" + userName + labelPath.substring(endIndex);

                                    LOGGER.info("The user selected to correct the label path: {}", labelPath);
                                    Preferences.getInstance().setLabelV2Path(labelPath);
                                    Preferences.getInstance().save(null);

                                    dir = Paths.get(labelPath);
                                    if (!Files.exists(dir)) {
                                        LOGGER.info("Try to create the directory for labels: {}", labelPath);
                                        try {
                                            dir = Files.createDirectories(dir);
                                            LOGGER.info("Created new directory for labels: {}", dir);
                                        }
                                        catch (IOException ioex2) {
                                            LOGGER.warn("Create new directory for labels failed.", ioex);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if (!Files.exists(dir)) {
                    LOGGER.warn("The directory for labels is not available. Check permission on path: {}", labelPath);

                    throw new IllegalStateException(
                        "The directory for labels is not available. Check permission on path: " + labelPath);
                }

                if (!Files.isWritable(dir)) {
                    LOGGER.warn("The directory for labels is not write enabled. Check permission on path: {}",
                        labelPath);

                    throw new IllegalStateException(
                        "The directory for labels is not write enabled. Check permission on path: " + labelPath);
                }

                LOGGER.info("The label directory is write enabled: {}", labelPath);

                labelPathIsValid = true;
            }
            catch (Exception ex) {
                LOGGER.warn(
                    "The directory for labels is not available or is not write enabled. Check permission on path: {}",
                    labelPath, ex);

                int choice =
                    JOptionPane.showOptionDialog(JOptionPane.getFrameForComponent(null),
                        Resources.getString(Labels.class, "labeldirerror.message", new Object[] { labelPath }),
                        Resources.getString(Labels.class, "labeldirerror.title"), JOptionPane.OK_CANCEL_OPTION,
                        JOptionPane.ERROR_MESSAGE, null, null, null);

                // interpret the user's choice
                if (choice == JOptionPane.CANCEL_OPTION) {
                    System.exit(0);
                }
                else {
                    // let the user change the label directory
                    FolderChooser chooser = new FolderChooser();
                    int returnVal = chooser.showOpenDialog(JOptionPane.getFrameForComponent(null));
                    if (returnVal == FolderChooser.APPROVE_OPTION) {
                        LOGGER.info("You chose to open this folder: {}", chooser.getSelectedFile().getPath());

                        labelPath = chooser.getSelectedFile().getPath();
                    }
                    else {
                        System.exit(0);
                    }
                }
            }
        }
        while (!labelPathIsValid);

        // check if the label path was changed
        if (!labelPath.equals(Preferences.getInstance().getLabelV2Path())) {
            LOGGER.info("Set the new label path: {}", labelPath);
            Preferences.getInstance().setLabelV2Path(labelPath);
            Preferences.getInstance().save(null);
        }

        startupWorkers.submit(new Runnable() {

            @Override
            public void run() {
                // Check if we need to migrate to new labels format
                LOGGER.info("Check if we need to migrate to new labels format.");

                // prepare the WizardLabelFactory instance
                try {
                    WizardLabelFactory.getInstance();
                }
                catch (IllegalArgumentException ex) {
                    // handle exception
                    JOptionPane.showMessageDialog(null,
                        Resources.getString(MainController.class, "mandatory-directory-missing.text"),
                        Resources.getString(MainController.class, "mandatory-directory-missing.title"),
                        JOptionPane.ERROR_MESSAGE);
                    System.exit(1);
                }
                LOGGER.info("Check to migrate to new labels format has finished.");

                loadAndRegisterLabels();

                LOGGER.info("Load and register labels passed. Invoke open UI.");

                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        prepareAndOpenUI();

                        LOGGER.info("Open the UI has finished.");
                    }
                });
            }

        });
    }

    /**
     * the migration worker
     */
    private final ScheduledExecutorService startupWorkers = Executors.newScheduledThreadPool(1);

    private void loadAndRegisterLabels() {
        LOGGER.info("Start load and register label factories.");

        AnalogPortLabelFactory aplf = new AnalogPortLabelFactory();
        analogPortLabels = aplf.getLabels(null);

        BacklightPortLabelFactory bplf = new BacklightPortLabelFactory();
        backlightPortLabels = bplf.getLabels(null);

        FeedbackPortLabelFactory fbplf = new FeedbackPortLabelFactory();
        feedbackPortLabels = fbplf.getLabels(null);

        InputPortLabelFactory iplf = new InputPortLabelFactory();
        inputPortLabels = iplf.getLabels(null);

        LightPortLabelFactory lplf = new LightPortLabelFactory();
        lightPortLabels = lplf.getLabels(null);

        MotorPortLabelFactory mplf = new MotorPortLabelFactory();
        motorPortLabels = mplf.getLabels(null);

        ServoPortLabelFactory servoplf = new ServoPortLabelFactory();
        servoPortLabels = servoplf.getLabels(null);

        SoundPortLabelFactory soundplf = new SoundPortLabelFactory();
        soundPortLabels = soundplf.getLabels(null);

        SwitchPortLabelFactory splf = new SwitchPortLabelFactory();
        switchPortLabels = splf.getLabels(null);

        SwitchPairPortLabelFactory spplf = new SwitchPairPortLabelFactory();
        switchPairPortLabels = spplf.getLabels(null);

        AccessoryLabelFactory factory = new AccessoryLabelFactory();
        accessoryLabels = factory.getLabels(null);

        // create the MacroLabelFactory
        MacroLabelFactory mlf = new MacroLabelFactory();
        macroLabels = mlf.getLabels(null);

        FlagLabelFactory flf = new FlagLabelFactory();
        flagLabels = flf.getLabels(null);

        // create the NodeLabelFactory
        NodeLabelFactory nlf = new NodeLabelFactory();
        nodeLabels = nlf.getLabels(null);

        DefaultApplicationContext.getInstance().register(DefaultApplicationContext.KEY_ANALOGPORT_LABELS,
            analogPortLabels);
        DefaultApplicationContext.getInstance().register(DefaultApplicationContext.KEY_BACKLIGHTPORT_LABELS,
            backlightPortLabels);
        DefaultApplicationContext.getInstance().register(DefaultApplicationContext.KEY_FEEDBACKPORT_LABELS,
            feedbackPortLabels);
        DefaultApplicationContext.getInstance().register(DefaultApplicationContext.KEY_INPUTPORT_LABELS,
            inputPortLabels);
        DefaultApplicationContext.getInstance().register(DefaultApplicationContext.KEY_LIGHTPORT_LABELS,
            lightPortLabels);
        DefaultApplicationContext.getInstance().register(DefaultApplicationContext.KEY_MOTORPORT_LABELS,
            motorPortLabels);
        DefaultApplicationContext.getInstance().register(DefaultApplicationContext.KEY_SERVOPORT_LABELS,
            servoPortLabels);
        DefaultApplicationContext.getInstance().register(DefaultApplicationContext.KEY_SOUNDPORT_LABELS,
            soundPortLabels);
        DefaultApplicationContext.getInstance().register(DefaultApplicationContext.KEY_SWITCHPORT_LABELS,
            switchPortLabels);
        DefaultApplicationContext.getInstance().register(DefaultApplicationContext.KEY_SWITCHPAIRPORT_LABELS,
            switchPairPortLabels);

        DefaultApplicationContext.getInstance().register(DefaultApplicationContext.KEY_NODE_LABELS, nodeLabels);
        DefaultApplicationContext.getInstance().register(DefaultApplicationContext.KEY_MACRO_LABELS, macroLabels);
        DefaultApplicationContext.getInstance().register(DefaultApplicationContext.KEY_ACCESSORY_LABELS,
            accessoryLabels);

        DefaultApplicationContext.getInstance().register(DefaultApplicationContext.KEY_FLAG_LABELS, flagLabels);
    }

    private boolean setDefaultCursor() {
        LOGGER.info("Set default cursor");
        return view.setBusy(false);
    }

    private boolean setWaitCursor() {
        LOGGER.info("Set wait cursor");
        return view.setBusy(true);
    }

    /**
     * Start the main controller.
     */
    public void start() {

        LOGGER.info("Start the main controller");

        DefaultApplicationContext.getInstance().register(DefaultApplicationContext.KEY_PORTS_PROVIDER, model);

        // initialize and register the command station service
        commandStationService = new DefaultCommandStationService();
        DefaultApplicationContext.getInstance().register(DefaultApplicationContext.KEY_COMMAND_STATION_SERVICE,
            commandStationService);

        loadLabels();
    }

    private void prepareAndOpenUI() {
        LOGGER.info("Open the UI.");

        // this initializes Bidib ...
        CommunicationFactory.addTransferListener(new DefaultTransferListener(model));

        CommunicationFactory.addMessageListener(new MainMessageListener(model));

        CommunicationFactory.addNodeListener(new NodeListener() {
            @Override
            public void nodeLost(org.bidib.jbidibc.core.Node commNode) {
                Node node = findNode(commNode);

                LOGGER.info("Node lost, commNode: {}, node: {}", commNode, node);
                if (node != null) {
                    LOGGER.info("Remove lost node from communication: {}", node);
                    Communication communication = CommunicationFactory.getInstance();
                    communication.removeNode(node.getNode());
                    model.removeNode(node);

                    // remove the node from the newNodeCreated-thread registry
                    synchronized (newNodeCreatedThreadRegistry) {
                        Thread newNodeCreatedThread = newNodeCreatedThreadRegistry.remove(commNode);
                        if (newNodeCreatedThread != null) {
                            try {
                                LOGGER.warn("Interrupt the newNodeCreatedThread: {}", newNodeCreatedThread);
                                if (newNodeCreatedThread.isAlive()) {
                                    newNodeCreatedThread.interrupt();
                                }
                            }
                            catch (Exception ex) {
                                LOGGER.warn("Interrupt newNodeCreatedThread failed.", ex);
                            }
                        }
                        else {
                            LOGGER.info("The lost node is not in the registry of new created nodes threads.");
                        }
                    }

                    view.setStatusText(String.format(Resources.getString(MainController.class, "node-lost"), commNode),
                        StatusBar.DISPLAY_ERROR);
                }
                else {
                    LOGGER.warn("Node not found.");
                }
            }

            @Override
            public void nodeNew(final org.bidib.jbidibc.core.Node commNode) {
                LOGGER.info("New node in system: {}", commNode);

                // check if the root node is initialized ...
                if (Arrays.equals(commNode.getAddr(), RootNode.ROOTNODE_ADDR)) {
                    LOGGER.info("Process the root node: {}", commNode);
                }
                else if (!CollectionUtils.isNotEmpty(getNodes())) {
                    LOGGER.warn("A new node is signaled but the root node was not processed yet. Skip the new node.");
                    return;
                }

                // TODO if an existing node is added with a new version this will not detect the problem!
                // TODO change the logic here
                LOGGER.info("Current newNodeCreatedThreadRegistry: {}", newNodeCreatedThreadRegistry);
                Thread newNodeCreatedThread = null;

                synchronized (newNodeCreatedThreadRegistry) {

                    org.bidib.jbidibc.core.Node foundKey =
                        IterableUtils.find(newNodeCreatedThreadRegistry.keySet(),
                            new Predicate<org.bidib.jbidibc.core.Node>() {

                                @Override
                                public boolean evaluate(org.bidib.jbidibc.core.Node node) {
                                    if (Arrays.equals(node.getAddr(), commNode.getAddr())
                                        && node.getUniqueId() == commNode.getUniqueId()) {
                                        LOGGER.debug("Found equal node: {}", node);
                                        return true;
                                    }
                                    return false;
                                }

                            });
                    if (foundKey != null) {
                        LOGGER.warn("The new node is already in the registration process: {}", commNode);
                        return;
                    }

                    // TODO we should better use a queue and let the network be read from only 1 thread
                    newNodeCreatedThread =
                        new Thread(
                            new NewNodeReader(MainController.this, view, model, newNodeCreatedThreadRegistry, commNode),
                            "newNode-created-thread-" + String.valueOf(System.currentTimeMillis()));

                    // add the node to the newNodeCreated-thread registry
                    LOGGER.info("Add new node to newNodeCreatedThreadRegistry and start thread, commNode: {}",
                        commNode);
                    // synchronized (newNodeCreatedThreadRegistry) {
                    newNodeCreatedThreadRegistry.put(commNode, newNodeCreatedThread);
                }
                newNodeCreatedThread.start();
            }
        });

        // create the main view
        view = new MainView(model);
        DefaultApplicationContext.getInstance().register(DefaultApplicationContext.KEY_MAIN_FRAME, view);

        view.createComponents();
        view.setIconImage(ImageUtils.createImageIcon(getClass(), "/icons/frameIcon_48x48.png").getImage());

        // layout the frame content
        view.prepareFrame();

        WizardStartupParams startupParams =
            DefaultApplicationContext.getInstance().get(DefaultApplicationContext.KEY_STARTUP_PROPERTIES,
                WizardStartupParams.class);
        LOGGER.info("Fetched startup params: {}", startupParams);

        // check the startup mode
        if (startupParams != null) {
            String startupMode = startupParams.getStartupMode();
            if (WizardStartupParams.MODE_ICONIFIED.equalsIgnoreCase(startupMode)) {
                view.setExtendedState(JFrame.ICONIFIED);
            }
        }

        // show the frame
        view.setVisible(true);

        CommunicationFactory.addCommunicationListener(new CommunicationListener() {

            @Override
            public void opening() {
                view.setStatusText(Resources.getString(CommunicationFactory.class, "opening-port"),
                    StatusBar.DISPLAY_NORMAL);
            }

            @Override
            public void opened(String port) {
                view.setStatusText(Resources.getString(CommunicationFactory.class, "open-port-passed") + " " + port,
                    StatusBar.DISPLAY_NORMAL);

                model.getStatusModel().setCd(true);
            }

            @Override
            public void closed(final String port) {

                if (openThread != null) {
                    try {
                        LOGGER.info("Interrupt the open thread to free waiting thread.");
                        synchronized (openThread) {
                            openThread.interrupt();
                        }
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Interrupt open thread failed.", ex);
                    }
                }

                // clear all nodes
                if (SwingUtilities.isEventDispatchThread()) {
                    handleClosed(port);
                }
                else {
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            handleClosed(port);
                        }
                    });
                }
            }

            private void handleClosed(final String port) {
                LOGGER.info("Port was closed, set the status text and clear the nodes.");
                if (StringUtils.isNotBlank(port)) {
                    view.setStatusText(
                        Resources.getString(CommunicationFactory.class, "close-port-passed") + " " + port,
                        StatusBar.DISPLAY_NORMAL);
                }
                else {
                    view.setStatusText(null, StatusBar.DISPLAY_NORMAL);
                }

                // commandStationService must stop all watchdog tasks
                if (commandStationService != null) {
                    commandStationService.stopAllWatchDogTasks();
                }

                clearNodes();

                model.getStatusModel().setCd(false);

                model.signalResetInitialLoadFinished();
                // release the cv definition
                model.setCvDefinition(null);
            }

            @Override
            public void initialized() {
            }

            @Override
            public void status(String statusText, int displayDuration) {
                view.setStatusText(statusText, displayDuration);
            }
        });

        // display the connect hint in the status bar
        view.setStatusText(Resources.getString(getClass(), "connectHint"), StatusBar.DISPLAY_NORMAL);

        // add the node list listener
        mainNodeListActionListener = new MainNodeListActionListener(view, model);
        view.addNodeListListener(mainNodeListActionListener);

        DefaultApplicationContext.getInstance().register(DefaultApplicationContext.KEY_MAINNODELISTLISTENER,
            mainNodeListActionListener);

        // add the node selection listener
        view.addNodeListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(final ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    LOGGER.info("The list selection has changed on the node list: {}", e.getSource());

                    final LabeledDisplayItems<Node> nodeList = (LabeledDisplayItems<Node>) e.getSource();
                    final Node node = nodeList.getSelectedItem();

                    // do not load the node if it is already selected
                    if (node != null && node.equals(model.getSelectedNode())) {
                        LOGGER.info("The node is already selected!");
                        return;
                    }

                    // make sure the initial loading of the tree structure has finished
                    if (!model.isInitialLoadFinished()) {
                        LOGGER.warn(
                            "The initial load of the nodeTab has not finished yet. Cancel selection and load of node details.");
                        return;
                    }

                    // select the node details tab
                    try {
                        String searchKey = "tabPanel";
                        LOGGER.info("Search for view with key: {}", searchKey);
                        Dockable tabPanel = view.getDesktop().getContext().getDockableByKey(searchKey);
                        DockUtils.selectWindow(tabPanel);
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Select tabPanel failed.", ex);
                    }

                    LOGGER.info("Set the new selected node in the model: {}", node);
                    model.setSelectedNode(node);

                    // use an executor
                    selectedNodeChangeWorker.schedule(new Runnable() {

                        @Override
                        public void run() {

                            LOGGER.info("Process node changed starting, Node-changed-thread-{}",
                                System.currentTimeMillis());

                            try {
                                SwingUtilities.invokeLater(() -> setWaitCursor());

                                StopWatch sw = new StopWatch();
                                sw.start();

                                view.setStatusText(
                                    String.format(Resources.getString(MainController.class, "load-config"), node),
                                    StatusBar.DISPLAY_NORMAL);

                                final Context context = new DefaultContext();

                                // fetch the cv definition
                                VendorCvData vendorCV = loadCvDefinition(context, node);

                                boolean nodeHasLimitations = true;
                                if (node != null) {
                                    // ignore sys errors
                                    nodeHasLimitations = node.isBootloaderNode() || node.isNodeHasError(true);

                                    LOGGER.info("The node was checked for limitiations: {}", nodeHasLimitations);
                                }

                                final Communication communication = CommunicationFactory.getInstance();
                                if (!nodeHasLimitations && communication != null) {
                                    getNodeConfiguration(communication, node);

                                    // this will wait until the port status values are loaded if needed
                                    queryNodeStatus(communication, node);
                                }
                                else {
                                    // update all port lists with the values that are now available
                                    model.updatePortLists();
                                }

                                sw.stop();
                                LOGGER.info("Process node changed took: {}", sw);

                                if (vendorCV != null) {
                                    view.setStatusText(
                                        String.format(Resources.getString(MainController.class, "load-config-finished"),
                                            node, sw),
                                        StatusBar.DISPLAY_NORMAL);
                                }
                                else {

                                    List<VendorCvError> vendorCVErrors =
                                        context.get(VendorCvFactory.VENDORCV_ERRORS, List.class,
                                            Collections.emptyList());

                                    if (CollectionUtils.isNotEmpty(vendorCVErrors)) {
                                        LOGGER.warn("Load vendor CV data from files failed: {}", vendorCVErrors);
                                        view.setStatusText(
                                            String.format(Resources.getString(MainController.class,
                                                "load-config-finished-load-vendorcv-failed"), node, sw),
                                            StatusBar.DISPLAY_ERROR);
                                    }
                                    else {
                                        view.setStatusText(
                                            String.format(Resources.getString(MainController.class,
                                                "load-config-finished-no-vendorcv"), node, sw),
                                            StatusBar.DISPLAY_NORMAL);
                                    }
                                }

                            }
                            catch (Exception ex) {
                                LOGGER.warn("Process node change caused an error: {}", ex);
                            }
                            finally {
                                SwingUtilities.invokeLater(() -> setDefaultCursor());
                                LOGGER.info("Process node changed finished.");
                            }
                        }
                    }, 0, TimeUnit.MILLISECONDS);
                }
            }
        });

        view.addAccessoryListListener(new AccessoryListListener() {
            @Override
            public void exportAccessory(final Accessory accessory) {
                // save the accessory
                AccessoryFileDialog dialog =
                    new AccessoryFileDialog(view,
                        // default is legacy filter
                        FileDialog.SAVE, accessory, false) {
                        @Override
                        public void approve(String fileName) {
                            try {
                                ExportFormat exportFormat =
                                    (getCheckUseLegacyFormat() != null && getCheckUseLegacyFormat().isSelected()
                                        ? ExportFormat.serialization : ExportFormat.jaxb);

                                final ApplicationContext context = new DefaultApplicationContext();
                                context.register("uniqueId", model.getSelectedNode().getUniqueId());

                                Labels labels =
                                    new AccessoryLabelFactory().getLabels(model.getSelectedNode().getUniqueId());
                                context.register("labels", labels);

                                AccessoryFactory.saveAccessory(fileName, accessory, exportFormat, context);

                                // update the status bar
                                view.setStatusText(String
                                    .format(Resources.getString(MainController.class, "exportedAccessory"), fileName),
                                    StatusBar.DISPLAY_NORMAL);
                            }
                            catch (IOException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    };
                dialog.showDialog();
            }

            @Override
            public void importAccessory(final Accessory accessory) {
                AccessoryFileDialog dialog = new AccessoryFileDialog(view, FileDialog.OPEN, accessory, false) {

                    @Override
                    public void approve(String fileName) {
                        ExportFormat exportFormat =
                            (getCheckUseLegacyFormat().isSelected() ? ExportFormat.serialization : ExportFormat.jaxb);

                        final ApplicationContext context = new DefaultApplicationContext();
                        context.register("uniqueId", model.getSelectedNode().getUniqueId());

                        final AccessoryLabelFactory accessoryLabelFactory = new AccessoryLabelFactory();
                        long uniqueId = model.getSelectedNode().getUniqueId();

                        Labels labels = accessoryLabelFactory.getLabels(uniqueId);
                        context.register("labels", labels);

                        Accessory accessory = AccessoryFactory.loadAccessory(fileName, exportFormat, model, context);

                        if (accessory != null) {

                            Accessory selectedAccessory = model.getSelectedAccessory();

                            accessory.setId(selectedAccessory.getId());
                            // set the total number possible macros for this accessory
                            accessory.setMacroSize(CommunicationFactory
                                .getInstance().getAccessoryLength(model.getSelectedNode().getNode()));

                            accessory.setMacroMapped(selectedAccessory.isMacroMapped());
                            if (accessory.getStartupState() == null) {
                                LOGGER.info(
                                    "The imported accessory has no startup state assigned. Use the state from the currently selected accessory.");
                                accessory.setStartupState(selectedAccessory.getStartupState());
                            }

                            model.replaceAccessory(accessory);
                        }

                        accessoryLabelFactory.saveLabels(uniqueId, labels);
                    }
                };
                dialog.showDialog();
            }

            @Override
            public void labelChanged(final Accessory accessory, String label) {

                long uniqueId = model.getSelectedNode().getNode().getUniqueId();
                AccessoryLabelUtils.replaceLabel(accessoryLabels, uniqueId, accessory.getId(), label);

                try {

                    new AccessoryLabelFactory().saveLabels(uniqueId, accessoryLabels);
                }
                catch (Exception e) {
                    LOGGER.warn("Save accessory labels failed.", e);
                    throw new RuntimeException(e);
                }
            }

            @Override
            public void reloadAccessory(Accessory accessory) {
                if (accessory != null
                    && (AccessorySaveState.PERMANENTLY_STORED_ON_NODE != accessory.getAccessorySaveState())) {

                    // show dialog
                    int result =
                        JOptionPane.showConfirmDialog(view,
                            Resources.getString(MainController.class, "accessory_has_pending_changes"),
                            Resources.getString(MainController.class, "pending_changes"), JOptionPane.OK_CANCEL_OPTION);
                    if (result != JOptionPane.OK_OPTION) {
                        LOGGER.info("User canceled discard pending changes.");
                        return;
                    }

                    // reset pending changes
                    // accessory.setPendingChanges(false);
                    accessory.setAccessorySaveState(AccessorySaveState.PERMANENTLY_STORED_ON_NODE);

                }

                CommunicationFactory.getInstance().reloadAccessory(model.getSelectedNode().getNode(), accessory);
                // accessory.setPendingChanges(false);
                accessory.setAccessorySaveState(AccessorySaveState.PERMANENTLY_STORED_ON_NODE);
            }

            @Override
            public void saveAccessory(Accessory accessory) {
                LOGGER.info("Save the accessory: {}", accessory);
                CommunicationFactory.getInstance().saveAccessory(model.getSelectedNode().getNode(), accessory);
            }

            @Override
            public void initializeAccessory(Accessory accessory) {
                // and force the panels to reload
                try {
                    if (model.getSelectedAccessory() != null && model.getSelectedAccessory().equals(accessory) && model
                        .getSelectedAccessory().getAccessorySaveState().equals(AccessorySaveState.PENDING_CHANGES)) {
                        // show dialog
                        int result =
                            JOptionPane.showConfirmDialog(view,
                                Resources.getString(MainController.class, "accessory_has_pending_changes"),
                                Resources.getString(MainController.class, "pending_changes"),
                                JOptionPane.OK_CANCEL_OPTION);
                        if (result != JOptionPane.OK_OPTION) {
                            LOGGER.info("User canceled discard pending changes.");
                            return;
                        }

                        // reset pending changes
                        model.getSelectedAccessory().setAccessorySaveState(
                            AccessorySaveState.PERMANENTLY_STORED_ON_NODE);
                    }
                    // initialize the macro
                    accessory.initialize();
                    accessory.setAccessorySaveState(AccessorySaveState.PENDING_CHANGES);

                    model.setSelectedAccessory(accessory);
                }
                catch (Exception ex) {
                    LOGGER.warn("Set the new selcted macro failed.", ex);
                }
            }

        });

        // add a list selection listener for the accessory list
        view.addAccessoryListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(final ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    try {

                        // the selected accessory has changed
                        setWaitCursor();

                        final JList<Accessory> accessoryList = (JList<Accessory>) e.getSource();
                        Accessory accessory = accessoryList.getSelectedValue();

                        if (accessory != null) {
                            model.setSelectedAccessory(accessory);
                        }
                    }
                    finally {
                        setDefaultCursor();
                    }
                }
            }
        });
        view.addAccessoryTableListener(new AccessoryTableListener() {

            private MacroRef[] macrosClipBoard;

            private void setMacrosToClipboard(MacroRef[] macros) {
                if (macros != null) {
                    try {
                        this.macrosClipBoard = new MacroRef[macros.length];
                        for (int index = 0; index < macros.length; index++) {
                            if (macros[index] != null) {
                                this.macrosClipBoard[index] = new MacroRef(macros[index].getId());
                            }
                        }
                    }
                    catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                }
                else {
                    this.macrosClipBoard = null;
                }
            }

            private MacroRef[] getMacrosFromClipboard() {
                MacroRef[] result = null;

                if (macrosClipBoard != null) {
                    try {
                        result = new MacroRef[macrosClipBoard.length];
                        for (int index = 0; index < macrosClipBoard.length; index++) {
                            if (macrosClipBoard[index] != null) {
                                result[index] = new MacroRef(macrosClipBoard[index].getId());
                            }
                        }
                    }
                    catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                }
                return result;
            }

            @Override
            public void delete(int[] rows) {
                for (int index = rows.length - 1; index >= 0; index--) {
                    model.getSelectedAccessory().removeMacro(rows[index]);
                }
            }

            private MacroRef findNextFreeMacro() {

                SortedSet<Integer> macroIds = new TreeSet<>();
                for (Accessory accessory : model.getAccessories()) {

                    for (MacroRef aspect : accessory.getAspects()) {

                        macroIds.add(aspect.getId());
                    }
                }

                // macro id starts from 0
                int macroId = 0;
                for (Integer currentId : macroIds) {
                    if (currentId.intValue() == macroId) {
                        macroId++;
                    }
                    else {
                        LOGGER.info("Found free macroId: {}", macroId);
                    }
                }
                MacroRef macroRef = null;
                if (macroId < model.getMacros().size()) {
                    LOGGER.info("Create new MacroRef with macroId: {}", macroId);
                    macroRef = new MacroRef(macroId);
                }
                else {
                    macroRef = new MacroRef();
                }
                return macroRef;
            }

            @Override
            public void insertEmptyAfter(int row) {
                MacroRef macroRef = findNextFreeMacro();
                model.getSelectedAccessory().addAspectAfter(row, macroRef);
            }

            @Override
            public void insertEmptyBefore(int row) {
                MacroRef macroRef = findNextFreeMacro();
                model.getSelectedAccessory().addAspectBefore(row >= 0 ? row : 0, macroRef);
            }

            @Override
            public void testButtonPressed(int aspect) {
                // check if the accessory has pending changes
                Accessory accessory = model.getSelectedAccessory();
                if (accessory != null
                    && (AccessorySaveState.PERMANENTLY_STORED_ON_NODE != accessory.getAccessorySaveState())) {
                    // show a hint to transfer the macro to the node
                    int result =
                        JOptionPane.showConfirmDialog(view,
                            Resources.getString(AccessoryTableListener.class,
                                "accessory_transfer_pending_changes_before_test"),
                            Resources.getString(AccessoryTableListener.class, "test"), JOptionPane.YES_NO_OPTION,
                            JOptionPane.WARNING_MESSAGE);

                    if (result == JOptionPane.YES_OPTION) {
                        CommunicationFactory.getInstance().saveAccessory(model.getSelectedNode().getNode(), accessory);
                    }
                }

                CommunicationFactory.getInstance().startAccessory(model.getSelectedNode().getNode(),
                    model.getSelectedAccessory(), aspect);
            }

            @Override
            public void copy(MacroRef[] macros) {
                setMacrosToClipboard(macros);
            }

            @Override
            public void cut(int[] rows, MacroRef[] macros) {
                setMacrosToClipboard(macros);
                for (int index = rows.length - 1; index >= 0; index--) {
                    model.getSelectedAccessory().removeMacro(rows[index]);
                }
            }

            @Override
            public void pasteAfter(int row) {
                MacroRef[] macros = getMacrosFromClipboard();
                if (macros != null) {
                    model.getSelectedAccessory().addAspectsAfter(row, macros);
                }
                else {
                    MacroRef macroRef = findNextFreeMacro();
                    model.getSelectedAccessory().addAspectAfter(row, macroRef);
                }
            }
        });
        view.addAnalogPortListener(new OutputListener<AnalogPortStatus>() {
            @Override
            public void labelChanged(Port<AnalogPortStatus> port, String label) {
                port.setLabel(label);

                try {
                    AnalogPortLabelFactory factory = new AnalogPortLabelFactory();
                    long uniqueId = model.getSelectedNode().getNode().getUniqueId();
                    factory.replaceLabel(analogPortLabels, uniqueId, port.getId(), label);

                    factory.saveLabels(uniqueId, analogPortLabels);
                }
                catch (InvalidConfigurationException ex) {
                    LOGGER.warn("Save analog port labels failed.", ex);

                    String labelPath = ex.getReason();
                    JOptionPane.showMessageDialog(JOptionPane.getFrameForComponent(null),
                        Resources.getString(Labels.class, "labelfileerror.message", new Object[] { labelPath }),
                        Resources.getString(Labels.class, "labelfileerror.title"), JOptionPane.ERROR_MESSAGE);
                }
            }

            @Override
            public void statusChanged(Port<AnalogPortStatus> port, AnalogPortStatus status) {
            }

            @Override
            public void testButtonPressed(Port<AnalogPortStatus> port) {
                CommunicationFactory.getInstance().activateAnalogPort(model.getSelectedNode().getNode(), port.getId(),
                    (AnalogPortStatus) port.getStatus());
            }

            @Override
            public void configChanged(Port<AnalogPortStatus> port) {
            }
        });

        final CommandStationStatusListener commandStationStatusListener = new CommandStationStatusListener() {

            @Override
            public void stateChanged(final Node node, final CommandStationState status) {
                if (SwingUtilities.isEventDispatchThread()) {
                    internalStateChanged(node, status);
                }
                else {
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            internalStateChanged(node, status);
                        }
                    });
                }
            }

            private void internalStateChanged(Node wizardNode, CommandStationState status) {
                LOGGER.debug("Status of command station has changed: {}, node: {}", status, wizardNode);
                commandStationService.signalStateChanged(wizardNode, status);
            }
        };
        model.addCommandStationStatusListener(commandStationStatusListener);
        model.addNodeListListener(new DefaultNodeListListener() {
            @Override
            public void listNodeRemoved(Node node) {
                // command station service must remove watchdog task if command station node is removed
                commandStationService.signalNodeRemoved(node);
            }
        });

        // handle backlight
        view.addBacklightPortListener(new BacklightPortListener() {
            @Override
            public void labelChanged(Port<BacklightPortStatus> port, String label) {
                port.setLabel(label);

                try {
                    BacklightPortLabelFactory factory = new BacklightPortLabelFactory();
                    long uniqueId = model.getSelectedNode().getNode().getUniqueId();
                    factory.replaceLabel(backlightPortLabels, uniqueId, port.getId(), label);

                    factory.saveLabels(uniqueId, backlightPortLabels);
                }
                catch (InvalidConfigurationException ex) {
                    LOGGER.warn("Save backlight port labels failed.", ex);

                    String labelPath = ex.getReason();
                    JOptionPane.showMessageDialog(JOptionPane.getFrameForComponent(null),
                        Resources.getString(Labels.class, "labelfileerror.message", new Object[] { labelPath }),
                        Resources.getString(Labels.class, "labelfileerror.title"), JOptionPane.ERROR_MESSAGE);
                }
            }

            @Override
            public void statusChanged(Port<BacklightPortStatus> port, BacklightPortStatus status) {
                LOGGER.debug("Status of backlight port has changed, port: {}, status: {}", port, status);
            }

            @Override
            public void testButtonPressed(Port<BacklightPortStatus> port) {

                BacklightPort backlightPort = (BacklightPort) port;
                LOGGER.info("Test pressed for port: {}, port.value: {}", port, backlightPort.getValue());

                CommunicationFactory.getInstance().activateBacklightPort(model.getSelectedNode().getNode(),
                    backlightPort, backlightPort.getValue());
            }

            @Override
            public void valuesChanged(BacklightPort port) {
                LOGGER.debug(
                    "Values have changed for backlight port: {}. Set the new backlight port parameters, dimSlopeDown: {}, dimSlopeUp: {}, dmxMapping: {}.",
                    port, port.getDimSlopeUp(), port.getDimSlopeDown(), port.getDmxMapping());
                CommunicationFactory.getInstance().setBacklightPortParameters(model.getSelectedNode().getNode(), port,
                    port.getDimSlopeDown(), port.getDimSlopeUp(), port.getDmxMapping());
            }

            @Override
            public void configChanged(Port<BacklightPortStatus> port) {
            }
        });

        view.addCvDefinitionRequestListener(new CvDefinitionRequestListener() {

            @Override
            public void loadCvValues(List<ConfigurationVariable> configVariables) {
                LOGGER.info("Load CV definition values!");
                StopWatch sw = new StopWatch();
                sw.start();
                boolean cursorChanged = false;
                try {
                    cursorChanged = setWaitCursor();

                    CommunicationFactory.getInstance().getConfigurationVariables(model.getSelectedNode().getNode(),
                        configVariables);

                    LOGGER.debug("Update model with configuration variables: {}", configVariables);
                    model.updateConfigurationVariableValues(configVariables, true);
                }
                finally {
                    if (cursorChanged) {
                        setDefaultCursor();
                    }
                }
                sw.stop();

                LOGGER.info("Load CV values has finished! Total loading duration: {}", sw);
                view.setStatusText(String.format(Resources.getString(MainController.class, "loadCvFinished"), sw),
                    StatusBar.DISPLAY_NORMAL);
            }

            @Override
            public void writeCvValues(List<ConfigurationVariable> cvList) {
                LOGGER.info("Write CV definition values!");
                StopWatch sw = new StopWatch();
                sw.start();
                try {
                    setWaitCursor();

                    List<ConfigurationVariable> configVars =
                        CommunicationFactory
                            .getInstance().writeConfigurationVariables(model.getSelectedNode().getNode(), cvList);
                    // iterate over the collection of stored variables in the model and update the values.
                    // After that notify the tree and delete the new values that are now stored in the node
                    model.updateConfigurationVariableValues(configVars, false);
                }
                finally {
                    setDefaultCursor();
                }
                sw.stop();

                LOGGER.info("Write CV values has finished! Total writing duration: {}", sw);
                view.setStatusText(String.format(Resources.getString(MainController.class, "writeCvFinished"), sw),
                    StatusBar.DISPLAY_NORMAL);
            }

            @Override
            public void activateAspect(int aspectNumber) {
                LOGGER.info("Start accessory with aspect: {}", aspectNumber);

                try {
                    setWaitCursor();

                    // TODO fix this to use the correct accessory
                    Accessory accessory = new Accessory();
                    accessory.setId(0);

                    CommunicationFactory.getInstance().startAccessory(model.getSelectedNode().getNode(), accessory,
                        aspectNumber);
                }
                finally {
                    setDefaultCursor();
                }
            }
        });

        view.addMacroListListener(new MacroListListener() {
            @Override
            public void exportMacro(final Macro macro) {

                if (CollectionUtils.isNotEmpty(macro.getFunctions()) && MacroUtils.hasEmptySteps(macro)) {
                    // step with no function in macro, ask the user to export empty macro
                    // show dialog
                    int result =
                        JOptionPane.showConfirmDialog(view,
                            Resources.getString(MainController.class, "ask_continue_export_macro_with_empty_step"),
                            Resources.getString(MainController.class, "export_macro.title"),
                            JOptionPane.OK_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE);
                    if (result != JOptionPane.OK_OPTION) {
                        LOGGER.info("User canceled export empty macro.");
                        return;
                    }

                    LOGGER.info("Remove empty macro steps.");
                    MacroUtils.removeEmptySteps(macro);

                }

                if (CollectionUtils.isEmpty(macro.getFunctions())) {
                    // no functions in macro, ask the user to export empty macro
                    // show dialog
                    int result =
                        JOptionPane.showConfirmDialog(view,
                            Resources.getString(MainController.class, "ask_export_empty_macro"),
                            Resources.getString(MainController.class, "export_macro.title"),
                            JOptionPane.OK_CANCEL_OPTION);
                    if (result != JOptionPane.OK_OPTION) {
                        LOGGER.info("User canceled export empty macro.");
                        return;
                    }
                }

                FileDialog dialog =
                    new MacroFileDialog(view,
                        // default is legacy filter
                        FileDialog.SAVE, macro) {

                        @Override
                        public void approve(String fileName) {
                            try {
                                boolean flatPortModel = false;
                                if (model.getSelectedNode() != null
                                    && model.getSelectedNode().getNode().isPortFlatModelAvailable()) {
                                    flatPortModel = true;
                                }
                                LOGGER.info("Set the flat port model flag: {}", flatPortModel);
                                macro.setFlatPortModel(flatPortModel);

                                MacroFactory.saveMacro(fileName, macro, ExportFormat.jaxb);

                                // update the status bar
                                view.setStatusText(
                                    String.format(Resources.getString(MainController.class, "exportedMacro"), fileName),
                                    StatusBar.DISPLAY_NORMAL);
                            }
                            catch (Exception ex) {
                                LOGGER.warn("Export macro failed.", ex);
                                // throw new RuntimeException(ex);

                                TaskDialogs
                                    .build(view,
                                        Resources.getString(MainController.class, "export_macro_failed.instruction"),
                                        Resources.getString(MainController.class, "export_macro_failed",
                                            macro.toString()))
                                    .title(Resources.getString(MainController.class, "export_macro.title"))
                                    .exception(ex);
                                ;
                            }
                        }

                    };
                dialog.showDialog();
            }

            @Override
            public void importMacro() {

                if (model.getSelectedMacro() != null
                    && model.getSelectedMacro().getMacroSaveState().equals(MacroSaveState.PENDING_CHANGES)) {

                    // show dialog
                    int result =
                        JOptionPane.showConfirmDialog(view,
                            Resources.getString(MainController.class, "macro_has_pending_changes"),
                            Resources.getString(MainController.class, "pending_changes"), JOptionPane.OK_CANCEL_OPTION);
                    if (result != JOptionPane.OK_OPTION) {
                        LOGGER.info("User canceled discard pending changes.");
                        return;
                    }

                    // reset pending changes
                    model.getSelectedMacro().setMacroSaveState(MacroSaveState.SAVED_ON_NODE);
                }

                FileDialog dialog = new MacroFileDialog(view, FileDialog.OPEN, null) {

                    @Override
                    public void approve(String fileName) {
                        ExportFormat exportFormat =
                            (getCheckUseLegacyFormat().isSelected() ? ExportFormat.serialization : ExportFormat.jaxb);

                        // a new macro instance is created
                        Macro macro = MacroFactory.loadMacro(fileName, exportFormat, model);

                        LOGGER.info("Loaded macro, flatPortModel: {}", macro.isFlatPortModel());
                        // set the new macro id
                        macro.setId(model.getSelectedMacro().getId());
                        // set pending changes flag
                        macro.setMacroSaveState(MacroSaveState.PENDING_CHANGES);

                        // replace the imported macro
                        replaceMacro(macro, false);
                    }
                };
                dialog.showDialog();
            }

            @Override
            public void labelChanged(final Macro macro, String label) {
                LOGGER.info("Label of macro has changed, macro: {}, label: {}", macro, label);
                NodeStateUtils.replaceMacroLabel(macroLabels, model.getSelectedNode().getNode().getUniqueId(), macro,
                    label, true);
            }

            @Override
            public void reloadMacro(Macro macro) {

                if (macro != null && macro.getMacroSaveState().equals(MacroSaveState.PENDING_CHANGES)) {

                    // show dialog
                    int result =
                        JOptionPane.showConfirmDialog(view,
                            Resources.getString(MainController.class, "macro_has_pending_changes"),
                            Resources.getString(MainController.class, "pending_changes"), JOptionPane.OK_CANCEL_OPTION);
                    if (result != JOptionPane.OK_OPTION) {
                        LOGGER.info("User canceled discard pending changes.");
                        return;
                    }

                    // reset pending changes
                    macro.setMacroSaveState(MacroSaveState.SAVED_ON_NODE);
                }

                try {
                    LcMacroState lcMacroState =
                        CommunicationFactory.getInstance().reloadMacro(model.getSelectedNode(), macro);
                    LOGGER.info("Reload macro returned: {}", lcMacroState);

                    model.getSelectedMacro().setMacroSaveState(MacroSaveState.PERMANENTLY_STORED_ON_NODE);
                    macro.setContainsError(false);
                }
                catch (InvalidConfigurationException ex) {
                    LOGGER.warn("Restore macro content failed.", ex);

                    model.setNodeHasError(model.getSelectedNode(), true);
                }
            }

            @Override
            public void saveMacro(Macro macro) {
                LOGGER.info("Save macro: {}", macro);
                try {
                    // the macro save state is reset by the saveMacro call

                    LcMacroState lcMacroState =
                        CommunicationFactory.getInstance().saveMacro(model.getSelectedNode(), macro);
                    LOGGER.info("Save macro returned: {}", lcMacroState);

                    macro.setContainsError(false);
                }
                catch (InvalidConfigurationException ex) {
                    LOGGER.warn("Save macro content failed.", ex);

                    model.setNodeHasError(model.getSelectedNode(), true);
                }
            }

            @Override
            public void startMacro(Macro macro, boolean transferBeforeStart) {
                LOGGER.info("Start macro: {}, transferBeforeStart: {}", macro, transferBeforeStart);
                LcMacroState lcMacroState =
                    CommunicationFactory.getInstance().startMacro(model.getSelectedNode(), macro, transferBeforeStart);
                LOGGER.info("Start macro returned: {}", lcMacroState);
            }

            @Override
            public void stopMacro(Macro macro) {
                LOGGER.info("Stop macro: {}", macro);
                LcMacroState lcMacroState =
                    CommunicationFactory.getInstance().stopMacro(model.getSelectedNode().getNode(), macro);
                LOGGER.info("Stop macro returned: {}", lcMacroState);
            }

            @Override
            public void transferMacro(Macro macro) {
                LOGGER.info("Transfer macro: {}", macro);
                try {
                    CommunicationFactory.getInstance().transferMacro(model.getSelectedNode(), macro);
                    LOGGER.info("Transfer macro passed.");
                }
                catch (InvalidConfigurationException ex) {
                    LOGGER.warn("Transfer macro content failed.", ex);

                    model.setNodeHasError(model.getSelectedNode(), true);
                }
            }

            @Override
            public void initializeMacro(Macro macro) {
                // and force the panels to reload
                try {
                    if (model.getSelectedMacro() != null && model.getSelectedMacro().equals(macro)
                        && model.getSelectedMacro().getMacroSaveState().equals(MacroSaveState.PENDING_CHANGES)) {
                        // show dialog
                        int result =
                            JOptionPane.showConfirmDialog(view,
                                Resources.getString(MainController.class, "macro_has_pending_changes"),
                                Resources.getString(MainController.class, "pending_changes"),
                                JOptionPane.OK_CANCEL_OPTION);
                        if (result != JOptionPane.OK_OPTION) {
                            LOGGER.info("User canceled discard pending changes.");
                            return;
                        }

                        // reset pending changes
                        model.getSelectedMacro().setMacroSaveState(MacroSaveState.SAVED_ON_NODE);
                    }
                    // initialize the macro
                    macro.initialize();
                    macro.setMacroSaveState(MacroSaveState.PENDING_CHANGES);

                    model.setSelectedMacro(macro);
                }
                catch (Exception ex) {
                    LOGGER.warn("Set the new selcted macro failed.", ex);
                }
            }

        });
        view.addMacroListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(final ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    final JList<Macro> macroList = (JList<Macro>) e.getSource();
                    try {
                        setWaitCursor();
                        if (!(macroList.getSelectedValue() instanceof Macro)) {
                            // do not react on change of label
                            return;
                        }

                        Macro macro = macroList.getSelectedValue();

                        if (macro != null) {
                            // This loads the macro content effectively ...
                            LOGGER.info("Load the macro content from the node.");

                            try {
                                macro =
                                    CommunicationFactory.getInstance().getMacroContent(model.getSelectedNode(), macro);
                                model.setSelectedMacro(macro);
                            }
                            catch (InvalidConfigurationException ex) {
                                LOGGER.warn("Restore macro content failed.", ex);

                                // set an empty macro
                                macro.setContainsError(true);
                                model.setSelectedMacro(macro);

                                model.setNodeHasError(model.getSelectedNode(), true);
                            }
                            catch (Exception ex) {
                                LOGGER.warn("Get the content of the selected macro failed.", ex);
                            }
                        }
                    }
                    finally {
                        setDefaultCursor();
                    }
                }
            }
        });
        view.addMacroTableListener(new MacroTableListener() {

            private Function<?>[] functionsClipBoard;

            private void setFunctionsToClipboard(Function<?>[] functions) {
                if (functions != null) {
                    try {
                        this.functionsClipBoard = new Function[functions.length];
                        for (int index = 0; index < functions.length; index++) {
                            if (functions[index] != null) {
                                this.functionsClipBoard[index] = ((Function<?>) functions[index].clone());
                            }
                        }
                    }
                    catch (CloneNotSupportedException e) {
                        throw new RuntimeException(e);
                    }
                }
                else {
                    this.functionsClipBoard = null;
                }
            }

            private Function<?>[] getFunctionsFromClipboard() {
                Function<?>[] result = null;

                if (functionsClipBoard != null) {
                    try {
                        result = new Function[functionsClipBoard.length];
                        for (int index = 0; index < functionsClipBoard.length; index++) {
                            if (functionsClipBoard[index] != null) {
                                result[index] = ((Function<?>) functionsClipBoard[index].clone());
                            }
                        }
                    }
                    catch (CloneNotSupportedException e) {
                        throw new RuntimeException(e);
                    }
                }
                return result;
            }

            @Override
            public void copy(Function<? extends BidibStatus>[] functions) {
                setFunctionsToClipboard(functions);
            }

            @Override
            public void cut(int[] rows, Function<? extends BidibStatus>[] functions) {
                setFunctionsToClipboard(functions);
                for (int index = rows.length - 1; index >= 0; index--) {
                    model.getSelectedMacro().removeFunction(rows[index]);
                }
            }

            @Override
            public void delete(int[] rows) {
                for (int index = rows.length - 1; index >= 0; index--) {
                    model.getSelectedMacro().removeFunction(rows[index]);
                }
            }

            @Override
            public void insertEmptyAfter(int row) {
                model.getSelectedMacro().addFunctionsAfter(row, null);
            }

            @Override
            public void insertEmptyBefore(int row) {
                model.getSelectedMacro().addFunctionsBefore(row >= 0 ? row : 0, null);
            }

            @Override
            public void pasteAfter(int row) {
                model.getSelectedMacro().addFunctionsAfter(row, getFunctionsFromClipboard());
            }

            @Override
            public void pasteBefore(int row) {
                model.getSelectedMacro().addFunctionsBefore(row >= 0 ? row : 0, getFunctionsFromClipboard());
            }

            @Override
            public void pasteInvertedAfter(int row) {
                model.getSelectedMacro().addFunctionsInvertedAfter(row, getFunctionsFromClipboard());
            }
        });

        MainMenuListener mainMenuListener = new DefaultMainMenuListener(view, this);

        DefaultApplicationContext.getInstance().register(DefaultApplicationContext.KEY_MAINMENU_LISTENER,
            mainMenuListener);

        view.addMainMenuListener(mainMenuListener);
        view.addToolBarListener(mainMenuListener);

        view.addServoPortListener(new ServoPortListener<ServoPortStatus>() {
            @Override
            public void labelChanged(Port<ServoPortStatus> port, String label) {
                port.setLabel(label);

                try {
                    ServoPortLabelFactory factory = new ServoPortLabelFactory();
                    long uniqueId = model.getSelectedNode().getNode().getUniqueId();
                    factory.replaceLabel(servoPortLabels, uniqueId, port.getId(), label);

                    factory.saveLabels(uniqueId, servoPortLabels);
                }
                catch (InvalidConfigurationException ex) {
                    LOGGER.warn("Save servo port labels failed.", ex);

                    String labelPath = ex.getReason();
                    JOptionPane.showMessageDialog(JOptionPane.getFrameForComponent(null),
                        Resources.getString(Labels.class, "labelfileerror.message", new Object[] { labelPath }),
                        Resources.getString(Labels.class, "labelfileerror.title"), JOptionPane.ERROR_MESSAGE);
                }
            }

            @Override
            public void statusChanged(Port<ServoPortStatus> port, ServoPortStatus status) {
                LOGGER.debug("Status of servo port has changed, port: {}, status: {}", port, status);
            }

            @Override
            public void testButtonPressed(Port<ServoPortStatus> port) {
                LOGGER.info("Test pressed for port: {}", port);
                CommunicationFactory.getInstance().activateServoPort(model.getSelectedNode().getNode(), port.getId(),
                    ((ServoPort) port).getValue());
            }

            @Override
            public void valuesChanged(ServoPort port) {
                LOGGER.info("Values have changed for servo port: {}. Set the new servo parameters.", port);
                CommunicationFactory.getInstance().setServoPortParameters(model.getSelectedNode().getNode(), port,
                    port.getTrimDown(), port.getTrimUp(), port.getSpeed());
            }

            @Override
            public void configChanged(Port<ServoPortStatus> port) {
            }
        });

        view.addStatusListener(new StatusListener() {
            @Override
            public void switchedOff() {
                model.getStatusModel().setModelClockStartEnabled(false);
                model.getStatusModel().setRunning(false);
            }

            @Override
            public void switchedOn() {
                model.getStatusModel().setModelClockStartEnabled(true);
                model.getStatusModel().setRunning(true);
            }

            @Override
            public void switchedCommandStationOn(boolean ignoreWatchDog) {
            }

            @Override
            public void switchedCommandStationOff() {
            }

            @Override
            public void switchedCommandStationSoftStop() {
            }

            @Override
            public void switchedCommandStationStop() {
            }

            @Override
            public void queryBoosterState() {
            }
        });

        // add the window listener
        view.setWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                stop();
            }
        });

        if (Preferences.getInstance().isShowBoosterTable()) {
            LOGGER.info("Show the booster table is selected in preferences.");

            // open the booster table ...
            try {
                mainMenuListener.boosterTable();
            }
            catch (Exception ex) {
                LOGGER.warn("Open booster table failed.", ex);
            }
        }

        boolean isAutoConnect = false;
        if (startupParams != null) {
            isAutoConnect = startupParams.isAutoConnect();
        }

        if (isAutoConnect) {
            LOGGER.info("Queue the autoConnect task.");
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    // trigger the autoConnect
                    try {
                        MainMenuListener mainMenuListener =
                            DefaultApplicationContext.getInstance().get(DefaultApplicationContext.KEY_MAINMENU_LISTENER,
                                MainMenuListener.class);

                        LOGGER.info("Try to autoconnect.");
                        mainMenuListener.connect();
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Autoconnect failed: {}", ex);
                    }

                }
            });
        }
        else {
            if (Preferences.getInstance().isUseHotPlugController()) {
                try {
                    LOGGER.info("Create and start UsbHotPlugController.");
                    usbHotPlugController = new UsbHotPlugController();
                    usbHotPlugController.start();

                    LOGGER.info("Create and start UsbHotPlugController has finished.");
                }
                catch (Exception ex) {
                    LOGGER.warn("Start UsbHotPlugController for USB devices failed.", ex);
                }
                catch (Error err) {
                    LOGGER.warn("Start UsbHotPlugController for USB devices failed.", err);
                }
            }
            else {
                LOGGER.info("The usage of hotplug controller is disabled in preferences.");
            }
        }

        if (Preferences.getInstance().isShowTipOfDay()) {
            LOGGER.info("Show tip of days.");

            final List<Alert> alertList = new LinkedList<>();

            final AlertListener alertListener = new AlertListener() {

                @Override
                public void alertAdded(final Alert alert, AlertAction alertAction) {
                    LOGGER.info("Alert added: {}", alert);

                    switch (alertAction) {
                        case DEVICE_ADDED:
                            alertList.add(alert);
                            break;
                        default:
                            alertList.remove(alert);
                            break;
                    }
                }

                @Override
                public void alertRemoved(final Alert alert) {
                    LOGGER.info("Alert removed: {}", alert);
                }

            };

            if (usbHotPlugController != null) {
                usbHotPlugController.addAlertListener(alertListener);
            }

            final TipOfDayClosedListener listener = new TipOfDayClosedListener() {

                @Override
                public void closed() {
                    LOGGER.info("The tips of day dialog was closed. Show an alert if available.");

                    for (Alert alert : alertList) {
                        LOGGER.info("Show alert: {}", alert);
                        usbHotPlugController.showAlert(alert, view);
                    }

                    // remove the alerts from the list
                    alertList.clear();
                }
            };
            try {
                TipOfDayController tipOfDayController = new TipOfDayController();
                tipOfDayController.start(listener);
            }
            catch (Exception ex) {
                LOGGER.warn("Start the tip of day controller failed.", ex);
            }
        }
        else {
            LOGGER.info("Show tip of days is disabled in settings.");
        }

    }

    private void getNodeConfiguration(final Communication communication, final Node node) {
        LOGGER.info("Get the node configuration for node: {}, protocol version: {}", node,
            node.getNode().getProtocolVersion());

        List<FeedbackPort> feedbackPorts = new LinkedList<FeedbackPort>();
        List<AnalogPort> analogPorts = new LinkedList<AnalogPort>();
        List<InputPort> inputPorts = new LinkedList<InputPort>();
        List<LightPort> lightPorts = new LinkedList<LightPort>();
        List<BacklightPort> backlightPorts = new LinkedList<BacklightPort>();
        List<MotorPort> motorPorts = new LinkedList<MotorPort>();
        List<ServoPort> servoPorts = new LinkedList<ServoPort>();
        List<SoundPort> soundPorts = new LinkedList<SoundPort>();
        final List<SwitchPort> switchPorts = new LinkedList<SwitchPort>();
        final List<SwitchPairPort> switchPairPorts = new LinkedList<SwitchPairPort>();

        // check if we have flat or type-oriented port model
        if (node.getNode().isPortFlatModelAvailable()) {
            LOGGER.info("Get the port information from flat port model.");

            if (NodeUtils.hasFeedbackFunctions(node.getUniqueId())) {
                LOGGER.debug("Get the feedback ports from the node.");
                for (FeedbackPort feedbackPort : communication.getFeedbackPorts(node.getNode())) {
                    feedbackPort.setLabel(PortLabelUtils
                        .getLabel(feedbackPortLabels, node.getNode().getUniqueId(), feedbackPort.getId(),
                            FeedbackPortLabelFactory.DEFAULT_LABELTYPE)
                        .getLabelString());
                    feedbackPorts.add(feedbackPort);
                }

                // model.setFeedbackPorts(feedbackPorts);
                node.setFeedbackPorts(feedbackPorts);
            }
            else {
                LOGGER.debug("Node has no feedback ports configured.");
                // model.setFeedbackPorts(feedbackPorts);
                node.setFeedbackPorts(feedbackPorts);
            }

            int totalPortCount = node.getNode().getPortFlatModel();
            LOGGER.info("Create total number of ports: {}", totalPortCount);
            List<GenericPort> ports = new LinkedList<>();
            for (int portId = 0; portId < totalPortCount; portId++) {
                GenericPort port = new GenericPort(portId);
                ports.add(port);
            }

            // set the generic ports on the node
            node.setGenericPorts(ports);

            // clear the ports in the model to force usage of generic ports
            model.setAnalogPorts(analogPorts);
            model.setBacklightPorts(backlightPorts);
            model.setLightPorts(lightPorts);
            model.setInputPorts(inputPorts);
            // model.setMotorPorts(motorPorts);
            node.setMotorPorts(motorPorts);
            // model.setServoPorts(servoPorts);
            node.setServoPorts(servoPorts);
            model.setSoundPorts(soundPorts);
            model.setSwitchPorts(switchPorts);
            model.setSwitchPairPorts(switchPairPorts);

            // trigger query of port config
            LOGGER.info("Trigger query of port config for node: {}", node);

            // check for the protocol version
            if (node.getNode().getProtocolVersion().isHigherThan(ProtocolVersion.VERSION_0_5)) {
                LOGGER.info("The current node uses queryAllPortConfig to get the port config from the node.");

                // query the port config
                communication.queryAllPortConfig(node.getNode(), null, null, null);

                try {
                    boolean configPendingFound = false;
                    for (GenericPort port : node.getGenericPorts()) {
                        if (PortConfigStatus.CONFIG_PENDING.equals(port.getConfigStatus())) {
                            // found a port with config pending status
                            LOGGER.info("Found a port with config pending status: {}", port);

                            configPendingFound = true;
                            break;
                        }
                    }

                    if (configPendingFound) {
                        LOGGER.warn(
                            "Found config pending status found in generic ports, clear the port cache on the node and update the port lists after queryAllPortConfig.");
                        node.setNodeHasError(true);
                        node.setErrorState(SysErrorEnum.BIDIB_ERR_TXT,
                            ByteUtils.bstr(Resources.getString(MainController.class, "missing-port-config")));
                        node.clearPortCache();

                        LOGGER.info("Force update of port lists.");
                        model.updatePortLists();
                    }
                    else {
                        LOGGER.info("No pending config found, don't update of ports list.");
                    }
                }
                catch (Exception ex) {
                    LOGGER.warn("Check if all generic ports have been configured failed.", ex);
                }

            }
            else {
                communication.queryPortConfig(node.getNode(), node.getGenericPorts());
            }
        }
        else {
            // process port-type oriented model
            LOGGER.info("Get the port information from port-type oriented model.");

            if (NodeUtils.hasFeedbackFunctions(node.getUniqueId())) {
                LOGGER.debug("Get the feedback ports from the node.");
                for (FeedbackPort feedbackPort : communication.getFeedbackPorts(node.getNode())) {
                    feedbackPort.setLabel(PortLabelUtils
                        .getLabel(feedbackPortLabels, node.getNode().getUniqueId(), feedbackPort.getId(),
                            FeedbackPortLabelFactory.DEFAULT_LABELTYPE)
                        .getLabelString());
                    feedbackPorts.add(feedbackPort);
                }
                // set the feedback ports on the node
                node.setFeedbackPorts(feedbackPorts);

            }
            else {
                LOGGER.debug("Node has no feedback ports configured.");
                node.setFeedbackPorts(feedbackPorts);
                // model.setFeedbackPorts(feedbackPorts);
            }

            // if we use asynchronous initialization of ports we must create the ports
            // first, add to model and trigger initialize afterwards

            if (NodeUtils.hasSwitchFunctions(node.getUniqueId())) {
                LOGGER.info("Get the switch functions from the node.");

                // check for the protocol version
                boolean queryAllPortConfigSupported = false;
                if (node.getNode().getProtocolVersion().isHigherThan(ProtocolVersion.VERSION_0_5)) {

                    // the OneDMX 2.0.0 is a special case
                    if (ProductUtils.isOneDMX(node.getUniqueId())
                        && node.getNode().getSoftwareVersion().equals(new SoftwareVersion(2, 0, 0))) {

                        LOGGER.info("The current OneDMX-2.00.00 has no support for queryAllPortConfig.");

                        queryAllPortConfigSupported = false;
                    }
                    // the STu-0.01.04 is a special case
                    else if (ProductUtils.isSTu(node.getUniqueId())
                        && node.getNode().getSoftwareVersion().equals(new SoftwareVersion(0, 1, 4))) {

                        LOGGER.info("The current STu-0.01.04 has no support for queryAllPortConfig.");

                        queryAllPortConfigSupported = false;
                    }
                    else {
                        LOGGER.info("The current node has support for queryAllPortConfig.");

                        queryAllPortConfigSupported = true;
                    }
                }

                LOGGER.debug("Get the analog ports from the node.");
                for (AnalogPort analogPort : communication.getAnalogPorts(node.getNode())) {
                    analogPort.setLabel(PortLabelUtils
                        .getLabel(analogPortLabels, node.getNode().getUniqueId(), analogPort.getId(),
                            AnalogPortLabelFactory.DEFAULT_LABELTYPE)
                        .getLabelString());
                    analogPorts.add(analogPort);
                }
                model.setAnalogPorts(analogPorts);

                if (CollectionUtils.isNotEmpty(analogPorts)) {
                    if (queryAllPortConfigSupported) {
                        communication.queryAllPortConfig(node.getNode(), LcOutputType.ANALOGPORT, 0,
                            analogPorts.size());
                    }
                }

                LOGGER.info("Get the input ports from the node.");
                for (InputPort inputPort : communication.getInputPorts(node.getNode())) {
                    inputPort.setLabel(PortLabelUtils
                        .getLabel(inputPortLabels, node.getNode().getUniqueId(), inputPort.getId(),
                            InputPortLabelFactory.DEFAULT_LABELTYPE)
                        .getLabelString());
                    inputPorts.add(inputPort);
                }
                model.setInputPorts(inputPorts);

                // TODO check if this would work
                LOGGER.info("Number of input ports from the node: {}", inputPorts.size());
                if (CollectionUtils.isNotEmpty(inputPorts)) {
                    if (!queryAllPortConfigSupported) {
                        // trigger query of input port config
                        communication.queryInputPortConfig(node.getNode());
                    }
                    else {
                        communication.queryAllPortConfig(node.getNode(), LcOutputType.INPUTPORT, 0, inputPorts.size());
                    }
                }

                LOGGER.debug("Get the switch ports from the node.");
                for (SwitchPort switchPort : communication.getSwitchPorts(node.getNode())) {
                    // set the label
                    switchPort.setLabel(PortLabelUtils
                        .getLabel(switchPortLabels, node.getNode().getUniqueId(), switchPort.getId(),
                            SwitchPortLabelFactory.DEFAULT_LABELTYPE)
                        .getLabelString());
                    switchPorts.add(switchPort);
                }
                model.setSwitchPorts(switchPorts);

                LOGGER.info("Number of switch ports from the node: {}", switchPorts.size());
                if (CollectionUtils.isNotEmpty(switchPorts)) {
                    if (!queryAllPortConfigSupported) {
                        // trigger query of switch port config
                        communication.querySwitchPortConfig(node.getNode());
                    }
                    else {
                        communication.queryAllPortConfig(node.getNode(), LcOutputType.SWITCHPORT, 0,
                            switchPorts.size());
                    }
                }

                LOGGER.info("Get the light ports from the node.");
                for (LightPort lightPort : communication.getLightPorts(node.getNode())) {
                    lightPort.setLabel(PortLabelUtils
                        .getLabel(lightPortLabels, node.getNode().getUniqueId(), lightPort.getId(),
                            LightPortLabelFactory.DEFAULT_LABELTYPE)
                        .getLabelString());
                    lightPorts.add(lightPort);
                }
                model.setLightPorts(lightPorts);

                if (CollectionUtils.isNotEmpty(lightPorts)) {
                    if (!queryAllPortConfigSupported) {
                        // trigger query of light port config
                        communication.queryLightPortConfig(node.getNode());
                    }
                    else {
                        communication.queryAllPortConfig(node.getNode(), LcOutputType.LIGHTPORT, 0, lightPorts.size());
                    }
                }
                else {
                    LOGGER.info("No lightports on node.");
                }

                LOGGER.debug("Get the backlight ports from the node.");
                for (BacklightPort backlightPort : communication.getBacklightPorts(node.getNode())) {
                    backlightPort.setLabel(PortLabelUtils
                        .getLabel(backlightPortLabels, node.getNode().getUniqueId(), backlightPort.getId(),
                            BacklightPortLabelFactory.DEFAULT_LABELTYPE)
                        .getLabelString());
                    backlightPorts.add(backlightPort);
                }
                model.setBacklightPorts(backlightPorts);

                if (CollectionUtils.isNotEmpty(backlightPorts)) {
                    if (!queryAllPortConfigSupported) {
                        // trigger query of backlight port config
                        communication.queryBacklightPortConfig(node.getNode());
                    }
                    else {
                        communication.queryAllPortConfig(node.getNode(), LcOutputType.BACKLIGHTPORT, 0,
                            backlightPorts.size());
                    }
                }

                Collection<MotorPort> nodeMotorPorts = communication.getMotorPorts(node.getNode());
                if (CollectionUtils.isNotEmpty(nodeMotorPorts)) {
                    LOGGER.debug("Get the motor ports from the node.");
                    for (MotorPort motorPort : nodeMotorPorts) {
                        motorPort.setLabel(PortLabelUtils
                            .getLabel(motorPortLabels, node.getNode().getUniqueId(), motorPort.getId(),
                                MotorPortLabelFactory.DEFAULT_LABELTYPE)
                            .getLabelString());
                        motorPorts.add(motorPort);
                    }
                }
                else {
                    LOGGER.debug("The current node has no motor ports.");
                }
                // model.setMotorPorts(motorPorts);
                node.setMotorPorts(motorPorts);

                if (CollectionUtils.isNotEmpty(motorPorts)) {
                    if (queryAllPortConfigSupported) {
                        // query the configuration of the motor ports
                        communication.queryAllPortConfig(node.getNode(), LcOutputType.MOTORPORT, 0, motorPorts.size());
                    }
                }

                LOGGER.info("Get the servo ports from the node.");
                for (ServoPort servoPort : communication.getServoPorts(node.getNode())) {
                    servoPort.setLabel(PortLabelUtils
                        .getLabel(servoPortLabels, node.getNode().getUniqueId(), servoPort.getId(),
                            MotorPortLabelFactory.DEFAULT_LABELTYPE)
                        .getLabelString());
                    servoPorts.add(servoPort);
                }

                LOGGER.info("Set the servo ports on the node.");
                node.setServoPorts(servoPorts);

                if (CollectionUtils.isNotEmpty(servoPorts)) {
                    if (!queryAllPortConfigSupported) {
                        // trigger query of servo port config
                        communication.queryServoPortConfig(node.getNode());
                    }
                    else {
                        LOGGER.info("Query the port config of the servo ports.");
                        communication.queryAllPortConfig(node.getNode(), LcOutputType.SERVOPORT, 0, servoPorts.size());
                    }
                }

                LOGGER.debug("Get the sound ports from the node.");
                for (SoundPort soundPort : communication.getSoundPorts(node.getNode())) {
                    soundPort.setLabel(PortLabelUtils
                        .getLabel(soundPortLabels, node.getNode().getUniqueId(), soundPort.getId(),
                            MotorPortLabelFactory.DEFAULT_LABELTYPE)
                        .getLabelString());
                    soundPorts.add(soundPort);
                }
                model.setSoundPorts(soundPorts);

                if (CollectionUtils.isNotEmpty(soundPorts)) {
                    if (queryAllPortConfigSupported) {
                        // query the configuration of the sound ports
                        communication.queryAllPortConfig(node.getNode(), LcOutputType.SOUNDPORT, 0, soundPorts.size());
                    }
                }

            }
            else {
                LOGGER.info("Node has no switch functions configured.");
                model.setAnalogPorts(analogPorts);
                model.setInputPorts(inputPorts);
                model.setSwitchPorts(switchPorts);
                model.setLightPorts(lightPorts);
                model.setBacklightPorts(backlightPorts);
                // model.setMotorPorts(motorPorts);
                node.setMotorPorts(motorPorts);
                // model.setServoPorts(servoPorts);
                node.setServoPorts(servoPorts);
                model.setSoundPorts(soundPorts);
                model.setSwitchPairPorts(switchPairPorts);
            }

            // update all port lists with the values that are now available
            model.updatePortLists();
        }

        List<Flag> flags = new LinkedList<Flag>();
        List<Macro> macros = new LinkedList<Macro>();
        List<Accessory> accessories = new LinkedList<Accessory>();

        if (NodeUtils.hasAccessoryFunctions(node.getUniqueId())) {
            LOGGER.debug("Get the flags.");

            // flags.addAll(node.getFlags());
            for (Flag flag : node.getFlags()) {
                LabelType flagLabel =
                    PortLabelUtils.getLabel(flagLabels, node.getUniqueId(), flag.getId(),
                        FlagLabelFactory.DEFAULT_LABELTYPE);
                flag.setLabel(flagLabel.getLabelString());
                flags.add(flag);
            }
            model.setFlags(flags);

            // check if the node has macros
            Feature macroLevel = Feature.findFeature(node.getNode().getFeatures(), BidibLibrary.FEATURE_CTRL_MAC_LEVEL);

            int macroLevelValue = 0;
            if (macroLevel != null) {
                macroLevelValue = macroLevel.getValue();
            }

            if (macroLevelValue > 0) {
                LOGGER.info("Get the macros from the node.");
                try {
                    for (Macro macro : communication.getMacros(node.getNode())) {
                        // set the user-defined label

                        LabelType macroLabel =
                            PortLabelUtils.getLabel(macroLabels, node.getUniqueId(), macro.getId(),
                                MacroLabelFactory.DEFAULT_LABELTYPE);
                        macro.setLabel(macroLabel.getLabelString());

                        // reset the changed flag on the macros
                        macro.setMacroSaveState(MacroSaveState.NOT_LOADED_FROM_NODE);

                        // add the current macro to the list
                        macros.add(macro);
                    }
                }
                catch (RuntimeException e) {
                    LOGGER.warn("Get the macros from the node failed.", e);
                    model.setNodeHasError(node, true);
                }
            }
            else {
                LOGGER.info("The current node has no macros.");
            }
            model.setMacros(macros);

            LOGGER.info("Get the accessories from the node.");
            try {
                Collection<Accessory> nodeAccessories = communication.getAccessories(node.getNode());
                if (nodeAccessories != null) {
                    boolean isStepControl = ProductUtils.isStepControl(node.getUniqueId());

                    for (Accessory accessory : nodeAccessories) {
                        if (isStepControl && accessory.getId() < 3) {
                            accessory.setEditable(false);
                            accessory.setLabel(
                                Resources.getString(StepControlPanel.class, "accessory." + accessory.getId()));
                        }
                        else {

                            LabelType label =
                                AccessoryLabelUtils.getAccessoryLabel(accessoryLabels, node.getNode().getUniqueId(),
                                    accessory.getId());

                            accessory.setLabel(label.getLabelString());
                        }
                        // reset the changed flag on the accessory
                        accessory.setAccessorySaveState(AccessorySaveState.PERMANENTLY_STORED_ON_NODE);

                        accessories.add(accessory);
                    }
                }
                else {
                    LOGGER.debug("Node has no accessories configured.");
                }
            }
            catch (NoAnswerException e) {
                LOGGER.warn("Get accessories from node failed.", e);

                model.setNodeHasError(node, true, "Get accessories from node failed.");
            }
            model.setAccessories(accessories);

            if (CollectionUtils.isNotEmpty(accessories)) {

                LOGGER.info("Query the accessory state from the node.");
                try {
                    communication.queryAccessoryState(node.getNode(), accessories.toArray(new Accessory[0]));
                }
                catch (Exception ex) {
                    LOGGER.warn("Query accessory state from node failed.", ex);
                }
            }
            else {
                LOGGER.info("No accessories on node available.");
            }
        }
        else {
            model.setFlags(flags);
            model.setMacros(macros);
            model.setAccessories(accessories);
        }
    }

    private void queryNodeStatus(final Communication communication, final Node node) {

        // initialize the current values
        if (NodeUtils.hasBoosterFunctions(node.getUniqueId())) {
            LOGGER.info("The node is a booster, set the booster maximum value.");
            // set the max booster current
            int boosterMaxCurrent = communication.getBoosterMaximumCurrent(node.getNode());
            LOGGER.info("Set the booster maximum current value: {}", boosterMaxCurrent);

            model.setBoosterMaximumCurrent(boosterMaxCurrent);
            model.setBoosterStatus(null);

            // trigger fetch the current booster values
            communication.boosterQuery(node.getNode());
        }
        else {
            LOGGER.info("Current node has no booster functions: {}", node);
            model.setBoosterMaximumCurrent(0);
            model.setBoosterStatus(null);
        }

        if (NodeUtils.hasCommandStationFunctions(node.getUniqueId())) {
            LOGGER.info("The node is a command station, query the state.");
            model.setCommandStationState(node, null);

            // trigger fetch the current command station status
            communication.queryCommandStationState(node.getNode());
        }
        else {
            LOGGER.info("Current node has no booster functions: {}", node);
            model.setCommandStationState(node, null);
        }

        if (node.getFeedbackPorts().size() > 0) {
            int numPorts = node.getFeedbackPorts().size();

            int queryEnd = ((numPorts + 7) / 8 * 8);
            LOGGER.info("Query the feedback ports, numPorts: {}, queryEnd: {}", numPorts, queryEnd);
            communication.getFeedbackPortStatus(node.getNode(), 0, queryEnd);
        }
        else {
            LOGGER.info("No feedback ports to query available.");
        }

        // check if the query port status feature is enabled
        if (NodeUtils.hasSwitchFunctions(node.getUniqueId())) {
            LOGGER.info("The node has switching functions.");

            boolean queryPortStatusEnabled = communication.isQueryPortStatusEnabled(node.getNode());
            if (queryPortStatusEnabled
                && node.getNode().getProtocolVersion().isHigherThan(ProtocolVersion.VERSION_0_6)) {
                // use MSG_LC_PORT_GET_ALL to get the status of the ports
                LOGGER.warn("Query all port status from the node: {}", node);

                int portTypeMask = 0xFFFF;
                int rangeFrom = 0;
                int rangeTo = 0xFFFF;
                communication.queryPortStatusAll(node.getNode(), portTypeMask, rangeFrom, rangeTo);

                // wait until the node status is loaded
                try {
                    Object lock = node.getInitialLoadFinishedLock();
                    LOGGER.info("Wait for complete loading of node status.");
                    synchronized (lock) {
                        lock.wait(8000);
                    }
                    LOGGER.info("Wait for complete loading of node status passed.");
                }
                catch (Exception ex) {
                    LOGGER.warn("Wait for complete loading of node status failed.", ex);
                }

            }
            else {

                if (CollectionUtils.isNotEmpty(model.getInputPorts())) {
                    LOGGER.info("Query the status of the input ports.");

                    List<Integer> portIds = new LinkedList<Integer>();
                    for (InputPort inputPort : model.getInputPorts()) {

                        if (inputPort.isEnabled()) {
                            portIds.add(inputPort.getId());
                        }
                        else {
                            LOGGER.info("Skip query port status of disabled input port with portId: {}",
                                inputPort.getId());
                        }
                    }
                    communication.getInputPortStatus(node.getNode(), portIds);
                }

                LOGGER.info("Query port status is enabled: {}", queryPortStatusEnabled);
                if (queryPortStatusEnabled) {
                    try {
                        List<ServoPort> servoPorts = new LinkedList<ServoPort>(model.getServoPorts());
                        LOGGER.info("Query the port status for the servo ports: {}", servoPorts);

                        List<Integer> portIds = new LinkedList<Integer>();
                        for (ServoPort servoPort : servoPorts) {
                            if (servoPort.isEnabled()) {
                                // query the port status
                                portIds.add(servoPort.getId());
                            }
                            else {
                                LOGGER.info("Skip query port status of disabled servo port with portId: {}",
                                    servoPort.getId());
                            }
                        }
                        communication.queryPortStatus(LcOutputType.SERVOPORT, node.getNode(), portIds);
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Query the port status for the servo ports failed.", ex);
                    }

                    try {
                        // TODO check if we must get the enabled switch ports from the node and no longer from model
                        List<SwitchPort> switchPorts = new LinkedList<SwitchPort>(model.getEnabledSwitchPorts());
                        LOGGER.info("Query the port status for the switch ports: {}", switchPorts);

                        List<Integer> portIds = new LinkedList<Integer>();
                        for (SwitchPort switchPort : switchPorts) {

                            if (switchPort.isEnabled()) {
                                // query the port status
                                portIds.add(switchPort.getId());
                            }
                            else {
                                LOGGER.info("Skip query port status of disabled switch port with portId: {}",
                                    switchPort.getId());
                            }
                        }
                        communication.queryPortStatus(LcOutputType.SWITCHPORT, node.getNode(), portIds);
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Query the port status for the switch ports failed.", ex);
                    }

                    try {
                        List<SoundPort> soundPorts = new LinkedList<>(model.getEnabledSoundPorts());
                        LOGGER.info("Query the port status for the sound ports: {}", soundPorts);

                        List<Integer> portIds = new LinkedList<Integer>();
                        for (SoundPort soundPort : soundPorts) {

                            if (soundPort.isEnabled()) {
                                // query the port status
                                portIds.add(soundPort.getId());
                            }
                            else {
                                LOGGER.info("Skip query port status of disabled sound port with portId: {}",
                                    soundPort.getId());
                            }
                        }
                        communication.queryPortStatus(LcOutputType.SOUNDPORT, node.getNode(), portIds);
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Query the port status for the sound ports failed.", ex);
                    }

                    try {
                        List<BacklightPort> backlightPorts = new LinkedList<BacklightPort>(model.getBacklightPorts());
                        LOGGER.info("Query the port status for the backlight ports: {}", backlightPorts);

                        if (CollectionUtils.isNotEmpty(backlightPorts)) {
                            List<Integer> portIds = new LinkedList<Integer>();
                            for (BacklightPort backlightPort : backlightPorts) {

                                if (backlightPort.isEnabled()) {
                                    // query the port status
                                    portIds.add(backlightPort.getId());
                                }
                                else {
                                    LOGGER.info("Skip query port status of disabled backlight port with portId: {}",
                                        backlightPort.getId());
                                }
                            }
                            communication.queryPortStatus(LcOutputType.BACKLIGHTPORT, node.getNode(), portIds);
                        }
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Query the port status for the backlight ports failed.", ex);
                    }

                    try {
                        List<LightPort> lightPorts = new LinkedList<LightPort>(model.getLightPorts());
                        LOGGER.info("Query the port status for the light ports: {}", lightPorts);

                        if (CollectionUtils.isNotEmpty(lightPorts)) {
                            List<Integer> portIds = new LinkedList<Integer>();
                            for (LightPort lightPort : lightPorts) {

                                if (lightPort.isEnabled()) {
                                    // query the port status
                                    portIds.add(lightPort.getId());
                                }
                                else {
                                    LOGGER.info("Skip query port status of disabled light port with portId: {}",
                                        lightPort.getId());
                                }
                            }

                            communication.queryPortStatus(LcOutputType.LIGHTPORT, node.getNode(), portIds);
                        }
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Query the port status for the light ports failed.", ex);
                    }

                    try {
                        List<MotorPort> motorPorts = new LinkedList<>(node.getMotorPorts());
                        LOGGER.info("Query the port status for the motor ports: {}", motorPorts);

                        if (CollectionUtils.isNotEmpty(motorPorts)) {
                            List<Integer> portIds = new LinkedList<Integer>();
                            for (MotorPort motorPort : motorPorts) {

                                if (motorPort.isEnabled()) {
                                    // query the port status
                                    portIds.add(motorPort.getId());
                                }
                                else {
                                    LOGGER.info("Skip query port status of disabled motor port with portId: {}",
                                        motorPort.getId());
                                }
                            }

                            communication.queryPortStatus(LcOutputType.MOTORPORT, node.getNode(), portIds);
                        }
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Query the port status for the motor ports failed.", ex);
                    }

                }
                else {
                    LOGGER.info("Query port status is not enabled for node: {}", node);
                }
            }
        }
        else {
            LOGGER.info("Node has no switch port functions.");
        }
    }

    @Override
    public void stop() {
        LOGGER.info("Stop the main controller.");
        selectedNodeChangeWorker.shutdownNow();

        view.saveWindowPosition();

        if (usbHotPlugController != null) {
            try {
                usbHotPlugController.stopWatcher();
            }
            catch (Exception ex) {
                LOGGER.warn("Stop usbHotPlugController failed.", ex);
            }
            usbHotPlugController = null;
        }

        view.setVisible(false);

        System.exit(0);
    }

    @Override
    public void addNodeListListener(NodeListListener nodeListListener) {
        LOGGER.info("Add new nodeListListener: {}", nodeListListener);
        model.addNodeListListener(nodeListListener);
    }

    @Override
    public void removeNodeListListener(NodeListListener nodeListListener) {
        LOGGER.info("Remove nodeListListener: {}", nodeListListener);
        model.removeNodeListListener(nodeListListener);
    }

    @Override
    public void addNodeSelectionListListener(NodeSelectionListener l) {
        model.addNodeSelectionListListener(l);
    }

    @Override
    public void removeNodeSelectionListener(NodeSelectionListener l) {
        model.removeNodeSelectionListener(l);
    }

    private NodeScripting nodeScripting;

    @Override
    public synchronized NodeScripting getNodeScripting() {
        if (nodeScripting == null) {
            LOGGER.info("Create new NodeScripting.");
            nodeScripting = new DefaultNodeScripting(model, this);
        }

        return nodeScripting;
    }

    private Object openThreadLock = new Object();

    private Thread openThread;

    @Override
    public void openConnection() {

        Communication communication = CommunicationFactory.getInstance();
        if (communication == null) {
            LOGGER.warn("The communication factory is not configured correct. Skip open connection.");

            return;
        }

        if (communication.isOpened()) {
            LOGGER.warn("The communication factory has opened the port already. Skip open connection.");

            return;
        }

        synchronized (openThreadLock) {

            // use a thread to open the port
            LOGGER.info("Start a thread to open the port.");

            if (openThread != null) {

                LOGGER.warn("An openThread is already running!!!!");
                return;
            }

            // TODO keep the openThread as an instance variable
            openThread = new Thread(new Runnable() {

                @Override
                public void run() {
                    LOGGER.info("The open thread is starting.");

                    final Context context = new DefaultContext();

                    // connect to system
                    view.setStatusText(Resources.getString(DefaultMainMenuListener.class, "open-port"),
                        StatusBar.DISPLAY_NORMAL);
                    try {

                        // get the value from the preferences
                        context.register("ignoreWrongReceiveMessageNumber",
                            Boolean.valueOf(Preferences.getInstance().isIgnoreWrongReceiveMessageNumber()));
                        context.register("ignoreFlowControl",
                            Boolean.valueOf(Preferences.getInstance().isIgnoreFlowControl()));

                        Communication communication = CommunicationFactory.getInstance();
                        LOGGER.info("Fetched instance of communication: {}", communication);

                        communication.open(null, context);
                        LOGGER.info("Opened communication.");
                    }
                    catch (InvalidConfigurationException ex) {
                        LOGGER.warn("Open port and create nodes failed.", ex);
                        if (ex.getCause() instanceof ReasonAware) {
                            ReasonAware reasonAware = (ReasonAware) ex.getCause();
                            String causeTemplate = Resources.getString(ReasonAware.class, "causeTemplate");
                            String reason = Resources.getString(reasonAware.getClass(), reasonAware.getReason());
                            view.setStatusText(String.format(causeTemplate, ex.getCause().getMessage(), reason),
                                StatusBar.DISPLAY_NORMAL);

                            showErrorDialog(String.format(causeTemplate, ex.getCause().getMessage(), reason),
                                Resources.getString(MainController.class, "error.title"));
                        }
                        else {
                            String causeTemplate =
                                Resources.getString(InvalidConfigurationException.class, "causeTemplate");
                            String message = null;
                            if (StringUtils.isNotBlank(ex.getReason())) {
                                message = Resources.getString(ex.getClass(), ex.getReason());
                            }
                            else {
                                message = (ex.getCause() != null ? ex.getCause().getMessage() : ex.getMessage());
                            }
                            view.setStatusText(String.format(causeTemplate, message), StatusBar.DISPLAY_NORMAL);

                            showErrorDialog(String.format(causeTemplate, message),
                                Resources.getString(MainController.class, "error.title"));
                        }
                    }
                    catch (NoAnswerException ex) {
                        LOGGER.warn("Open port and establish communication failed.", ex);
                        view.setStatusText(Resources.getString(ex.getClass(), "bidib-communication-failure"),
                            StatusBar.DISPLAY_NORMAL);

                        context.register("testIsDebugIfActive", Boolean.TRUE);
                        context.register("noAnswerEx", ex);
                    }
                    catch (InternalStartupException ex) {
                        LOGGER.warn("Open port and create nodes was aborted.", ex);

                        view.setStatusText(Resources.getString(ex.getClass(), "status", ex.getMessage()),
                            StatusBar.DISPLAY_NORMAL);

                        showErrorDialog(Resources.getString(ex.getClass(), "message", ex.getMessage()),
                            Resources.getString(MainController.class, "error.title"));
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Open port and create nodes failed.", ex);

                        if (ex.getCause() != null) {
                            view.setStatusText(Resources.getString((Class<?>) null, ex.getCause().getClass().getName())
                                + " " + ex.getCause().getMessage(), StatusBar.DISPLAY_NORMAL);

                            showErrorDialog(
                                Resources.getString((Class<?>) null, ex.getCause().getClass().getName()) + " "
                                    + ex.getCause().getMessage(),
                                Resources.getString(MainController.class, "error.title"));
                        }
                        else {
                            view.setStatusText(
                                Resources.getString((Class<?>) null, ex.getClass().getName()) + " " + ex.getMessage(),
                                StatusBar.DISPLAY_NORMAL);

                            showErrorDialog(
                                Resources.getString((Class<?>) null, ex.getClass().getName()) + " " + ex.getMessage(),
                                Resources.getString(MainController.class, "error.title"));
                        }
                    }
                    LOGGER.info("The open thread has finished.");

                    if (context.get("testIsDebugIfActive", Boolean.class, Boolean.FALSE) == Boolean.TRUE) {
                        LOGGER.info("Test if the debug interface is active.");

                        // use the debug controller to check the interface
                        SwingUtilities.invokeLater(new Runnable() {
                            public void run() {
                                PreferencesPortType portType = Preferences.getInstance().getSelectedPortType();
                                String commPort = portType.getConnectionName();
                                String result = sendInfoRequest(commPort);

                                NoAnswerException ex = context.get("noAnswerEx", NoAnswerException.class, null);
                                if (StringUtils.isNotBlank(result)) {

                                    showErrorDialog(Resources.getString(ex.getClass(),
                                        "bidib-communication-failure-debug-interface-active", new Object[] { result }),
                                        Resources.getString(MainController.class, "error.title"));
                                }
                                else {
                                    // display the original error message
                                    showErrorDialog(Resources.getString(ex.getClass(), "bidib-communication-failure"),
                                        Resources.getString(MainController.class, "error.title"));
                                }
                            }
                        });
                    }

                    LOGGER.info("+++ The openThread has finished.");

                    synchronized (openThreadLock) {
                        LOGGER.info("+++ Release the openThread instance variable.");
                        synchronized (openThread) {
                            openThread.notifyAll();
                        }
                        openThread = null;
                    }
                }
            }, "openThread");

            CommunicationFactory.getInstance().notifyOpening();

            LOGGER.info("Start the open thread.");
            openThread.start();
        }
    }

    @Override
    public void closeConnection() {
        LOGGER.info("### Close the connection to BiDiB.");

        // TODO check if a thread is running that reads the structure
        Thread copyOfOpenTread = null;
        synchronized (openThreadLock) {
            LOGGER.info("Check if openThread instance variable is set before release communication, openThread: {}",
                openThread);
            copyOfOpenTread = openThread;
        }
        if (copyOfOpenTread != null) {
            LOGGER.warn("The openThread is running!!!! Wait for termination.");

            try {
                LOGGER.info("### Interrupt the open thread.");
                copyOfOpenTread.interrupt();

                synchronized (copyOfOpenTread) {
                    LOGGER.info("### Wait for the open thread termination.");
                    copyOfOpenTread.join(5000);
                }

                LOGGER.info("### The open thread has finished.");
            }
            catch (Exception ex) {
                LOGGER.warn("### Wait for termination of openThread failed.", ex);
            }
        }

        synchronized (newNodeCreatedThreadRegistry) {
            if (!newNodeCreatedThreadRegistry.isEmpty()) {
                LOGGER.info("Pending workers in newNodeCreatedThreadRegistry found.");

                for (Entry<org.bidib.jbidibc.core.Node, Thread> entry : newNodeCreatedThreadRegistry.entrySet()) {
                    LOGGER.warn("Terminate running thread for node: {}", entry);

                    Thread worker = entry.getValue();
                    try {
                        synchronized (worker) {
                            worker.interrupt();

                            worker.join(2000);
                        }
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Wait for termination of worker thread failed: {}", entry, ex);
                    }
                }

                LOGGER.info("All workers have finished,");
            }
            else {
                LOGGER.info("No newNodeCreatedThreads registered.");
            }
        }

        // disconnect from bidib
        CommunicationFactory.disconnect();

        LOGGER.info("The communication factory was disconnected.");
    }

    private String sendInfoRequest(String portName) {
        LOGGER.warn("Send the info request to the root node, portName: {}", portName);

        final StringBuilder receivedMessages = new StringBuilder();

        try {
            final Object resultLock = new Object();

            int baudRate = 19200;
            DebugMessageListener messageListener = null;
            DebugMessageReceiver messageReceiver = null;
            DebugReader debugReader = null;
            if (debugReader == null) {
                LOGGER.info("Create new instance of debug reader.");

                try {
                    messageListener = new DebugMessageListener() {

                        @Override
                        public void debugMessage(final String message) {
                            LOGGER.info("debug message received: {}", message);

                            receivedMessages.append(message);

                            if (message.indexOf('\n') > -1) {
                                LOGGER.info("Received CR, notify the waiting thread.");
                                synchronized (resultLock) {
                                    resultLock.notify();
                                }
                            }
                        }
                    };
                    messageReceiver = new DebugMessageReceiver();
                    messageReceiver.addMessageListener(messageListener);
                    debugReader = new DebugReader(messageReceiver);
                }
                catch (Exception ex) {
                    LOGGER.warn("Open debug port failed.", ex);
                }
            }

            try {
                debugReader.open(portName, baudRate, new ConnectionListener() {

                    @Override
                    public void opened(String port) {
                        LOGGER.info("Port opened: {}", port);
                    }

                    @Override
                    public void closed(String port) {
                        LOGGER.info("Port closed: {}", port);
                    }

                    @Override
                    public void status(String messageKey) {
                    }
                }, null);
            }
            catch (PortNotFoundException | PortNotOpenedException ex) {
                LOGGER.warn("Open debug port failed.", ex);
            }
            catch (Exception ex) {
                LOGGER.warn("Open debug port failed.", ex);
            }

            String infoRequest = "?";

            // send twice because the first message might not be evaluated correct because of data in buffer
            // on the receiving side because the debug interface expects the line feed.
            debugReader.send(infoRequest, LineEndingEnum.CRLF);
            debugReader.send(infoRequest, LineEndingEnum.CRLF);

            synchronized (resultLock) {
                resultLock.wait(1000);
            }

            debugReader.close();

            messageReceiver.removeMessageListener(messageListener);
            messageListener = null;
            messageReceiver = null;

            debugReader = null;
        }
        catch (Exception ex) {
            LOGGER.error("Send info request failed.", ex);
        }
        finally {
        }

        return receivedMessages.toString();
    }

    private void showErrorDialog(final String message, final String title) {
        if (SwingUtilities.isEventDispatchThread()) {
            JOptionPane.showMessageDialog(view, message, title, JOptionPane.ERROR_MESSAGE);
        }
        else {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    JOptionPane.showMessageDialog(view, message, title, JOptionPane.ERROR_MESSAGE);
                }
            });
        }
    }

    @Override
    public void allBoosterOff() {

        for (Node node : model.getNodes()) {
            if (NodeUtils.hasBoosterFunctions(node.getUniqueId())) {
                LOGGER.info("Switch booster off: {}", node);
                try {
                    CommunicationFactory.getInstance().boosterOff(node.getNode());
                }
                catch (Exception ex) {
                    LOGGER.warn("Switch booster off failed for node: {}", node, ex);
                }
            }
        }
    }

    @Override
    public void allBoosterOn(boolean boosterAndCommandStation, CommandStationState requestedCommandStationState) {
        LOGGER.info("Switch all boosters on, boosterAndCommandStation: {}, requestedCommandStationState: {}",
            boosterAndCommandStation, requestedCommandStationState);

        if (boosterAndCommandStation) {
            LOGGER.info("Switch the command stations on before the boosters will be switched on.");

            // use the command station service to witch the command station on
            CommandStationService commandStationService =
                DefaultApplicationContext.getInstance().get(DefaultApplicationContext.KEY_COMMAND_STATION_SERVICE,
                    CommandStationService.class);

            for (Node node : model.getNodes()) {
                if (NodeUtils.hasCommandStationFunctions(node.getUniqueId())) {

                    try {
                        LOGGER.info("Clear the loco buffer before switch on the command station, node: {}", node);
                        commandStationService.clearLocoBuffer(node);

                        Thread.sleep(300);

                        LOGGER.info("Switch command station on: {}", node);
                        commandStationService.setCommandStationState(node,
                            requestedCommandStationState /* CommandStationState.GO */);
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Switch command station on failed for node: {}", node, ex);
                    }
                }
            }

            LOGGER.info("Wait a little bit to let the command station send the DCC signal.");

            try {
                Thread.sleep(300);
            }
            catch (Exception ex) {
                LOGGER.warn("Wait a little bit to let the command station send the DCC signal was interrupted.", ex);
            }
        }

        LOGGER.info("Switch all boosters on.");

        for (Node node : model.getNodes()) {
            if (NodeUtils.hasBoosterFunctions(node.getUniqueId())) {
                LOGGER.info("Switch booster on: {}", node);
                try {
                    CommunicationFactory.getInstance().boosterOn(node.getNode());
                }
                catch (Exception ex) {
                    LOGGER.warn("Switch booster on failed for node: {}", node, ex);
                }
            }
        }
    }

    @Override
    public void replaceMacro(Macro macro, boolean saveOnNode) {
        LOGGER.info("Replace the macro: {}", macro);
        if (macro != null) {
            // set the total number possible functions for this macro
            macro.setFunctionSize(
                CommunicationFactory.getInstance().getMaxMacroLength(model.getSelectedNode().getNode()));

            if (model.getSelectedMacro() != null
                && model.getSelectedMacro().getMacroSaveState().equals(MacroSaveState.PENDING_CHANGES)) {
                // show dialog
                int result =
                    JOptionPane.showConfirmDialog(view,
                        Resources.getString(MainController.class, "macro_has_pending_changes"),
                        Resources.getString(MainController.class, "pending_changes"), JOptionPane.OK_CANCEL_OPTION);
                if (result != JOptionPane.OK_OPTION) {
                    LOGGER.info("User canceled discard pending changes.");
                    return;
                }
            }

            // replace the macro in the main model
            model.replaceMacro(macro);
        }

        if (saveOnNode) {
            LOGGER.info("Transfer the macro to node and save permanently: {}", macro);
            try {
                CommunicationFactory.getInstance().saveMacro(model.getSelectedNode(), macro);
                LOGGER.info("Transfer and save macro passed.");
                // reset pending changes
                // macro.setMacroSaveState(false);
            }
            catch (InvalidConfigurationException ex) {
                LOGGER.warn("Transfer macro content or save macro failed.", ex);

                model.setNodeHasError(model.getSelectedNode(), true);
            }
        }
    }

    @Override
    public void resetNode(Node node) {
        LOGGER.info("Reset the current node: {}", node);
        CommunicationFactory.getInstance().reset(node.getNode());
    }

    @Override
    public void replaceAccessory(Accessory accessory, boolean saveOnNode) {
        LOGGER.info("Replace the accessory: {}", (accessory != null ? accessory.getDebugString() : null));

        if (accessory != null) {
            model.replaceAccessory(accessory);
        }

        if (saveOnNode) {
            LOGGER.info("Transfer the accessory to node and save permanently.");
            try {
                CommunicationFactory.getInstance().saveAccessory(model.getSelectedNode().getNode(), accessory);
                LOGGER.info("Transfer and save accessory passed.");
            }
            catch (InvalidConfigurationException ex) {
                LOGGER.warn("Transfer accessory or save accessory failed.", ex);

                model.setNodeHasError(model.getSelectedNode(), true);

                throw ex;
            }
        }
    }

    @Override
    public void replacePortConfig(TargetType portType, final Map<Byte, PortConfigValue<?>> portConfig) {
        LOGGER.info("Replace the port config, portType: {}, portConfig: {}", portType, portConfig);
        try {
            Node node = model.getSelectedNode();

            switch (portType.getScriptingTargetType()) {
                case BACKLIGHTPORT: {
                    Number lightDimmDown = (Number) portConfig.get(BidibLibrary.BIDIB_PCFG_DIMM_DOWN_8_8).getValue();
                    int dimmDown = lightDimmDown.intValue();
                    Number lightDimmUp = (Number) portConfig.get(BidibLibrary.BIDIB_PCFG_DIMM_UP_8_8).getValue();
                    int dimmUp = lightDimmUp.intValue();

                    BacklightPort backlightPort =
                        PortListUtils.findPortByPortNumber(model.getBacklightPorts(), portType.getPortNum().intValue());

                    int dmxMapping = 1;

                    // TODO make this wait for the response from the node
                    CommunicationFactory.getInstance().setBacklightPortParameters(node.getNode(), backlightPort,
                        dimmDown, dimmUp, dmxMapping);
                }
                    break;
                case LIGHTPORT: {
                    Number lightLevelOff = (Number) portConfig.get(BidibLibrary.BIDIB_PCFG_LEVEL_PORT_OFF).getValue();
                    int levelOff = lightLevelOff.intValue();
                    Number lightLevelOn = (Number) portConfig.get(BidibLibrary.BIDIB_PCFG_LEVEL_PORT_ON).getValue();
                    int levelOn = lightLevelOn.intValue();

                    Number lightDimmDown = null;
                    if (portConfig.containsKey(BidibLibrary.BIDIB_PCFG_DIMM_DOWN_8_8)) {
                        lightDimmDown = (Number) portConfig.get(BidibLibrary.BIDIB_PCFG_DIMM_DOWN_8_8).getValue();
                    }
                    else if (portConfig.containsKey(BidibLibrary.BIDIB_PCFG_DIMM_DOWN)) {
                        lightDimmDown = (Number) portConfig.get(BidibLibrary.BIDIB_PCFG_DIMM_DOWN).getValue();
                    }

                    int dimmDown = 0;
                    if (lightDimmDown != null) {
                        dimmDown = lightDimmDown.intValue();
                    }
                    Number lightDimmUp = null;
                    if (portConfig.containsKey(BidibLibrary.BIDIB_PCFG_DIMM_UP_8_8)) {
                        lightDimmUp = (Number) portConfig.get(BidibLibrary.BIDIB_PCFG_DIMM_UP_8_8).getValue();
                    }
                    else if (portConfig.containsKey(BidibLibrary.BIDIB_PCFG_DIMM_UP)) {
                        lightDimmUp = (Number) portConfig.get(BidibLibrary.BIDIB_PCFG_DIMM_UP).getValue();
                    }
                    int dimmUp = 0;
                    if (lightDimmUp != null) {
                        dimmUp = lightDimmUp.intValue();
                    }

                    Integer lightRgb = null;
                    PortConfigValue<?> rgbValue = portConfig.get(BidibLibrary.BIDIB_PCFG_RGB);
                    if (rgbValue != null) {
                        lightRgb = (Integer) rgbValue.getValue();
                    }

                    LightPort lightPort =
                        PortListUtils.findPortByPortNumber(model.getLightPorts(), portType.getPortNum().intValue());

                    // TODO make this wait for the response from the node
                    // CommunicationFactory.getInstance().setLightPortParameters(node.getNode(), lightPort, levelOff,
                    // levelOn, dimmDown, dimmUp, lightRgb);
                    setPortParameters(node, lightPort, portConfig);
                }
                    break;
                case SERVOPORT: {
                    Number servoTrimDown = (Number) portConfig.get(BidibLibrary.BIDIB_PCFG_SERVO_ADJ_L).getValue();
                    int trimDown = servoTrimDown.intValue();
                    Number servoTrimUp = (Number) portConfig.get(BidibLibrary.BIDIB_PCFG_SERVO_ADJ_H).getValue();
                    int trimUp = servoTrimUp.intValue();
                    Number servoSpeed = (Number) portConfig.get(BidibLibrary.BIDIB_PCFG_SERVO_SPEED).getValue();
                    int speed = servoSpeed.intValue();

                    ServoPort servoPort =
                        PortListUtils.findPortByPortNumber(model.getServoPorts(), portType.getPortNum().intValue());

                    // TODO make this wait for the response from the node
                    CommunicationFactory.getInstance().setServoPortParameters(node.getNode(), servoPort, trimDown,
                        trimUp, speed);
                }
                    break;
                case SWITCHPORT: {

                    SwitchPort switchPort =
                        PortListUtils.findPortByPortNumber(model.getSwitchPorts(), portType.getPortNum().intValue());

                    // if (ProductUtils.isOneControl(node.getUniqueId())) {
                    // if (portConfig.containsKey(BidibLibrary.BIDIB_PCFG_SWITCH_CTRL)) {
                    // Number switchIoType =
                    // (Number) portConfig.get(BidibLibrary.BIDIB_PCFG_SWITCH_CTRL).getValue();
                    // int ioType = switchIoType.intValue();
                    //
                    // portConfig.remove(BidibLibrary.BIDIB_PCFG_SWITCH_CTRL);
                    // LOGGER.warn("Replace the BIDIB_PCFG_SWITCH_CTRL and convert to BIDIB_PCFG_IO_CTRL: {}",
                    // ioType);
                    //
                    // IoBehaviourEnum ioBehaviour = IoBehaviourEnum.valueOf(ByteUtils.getLowByte(ioType));
                    // portConfig.put(BidibLibrary.BIDIB_PCFG_IO_CTRL,
                    // new BytePortConfigValue(ioBehaviour.getType()));
                    // }
                    // }

                    setPortParameters(node, switchPort, portConfig);
                }
                    break;
                default:
                    LOGGER.warn("Unsupported port type detected: {}", portType);
                    break;
            }

        }
        catch (Exception ex) {
            LOGGER.warn("Replace port config on node failed.", ex);
        }
    }

    private void setPortParameters(
        final Node node, final Port<?> port, final Map<Byte, PortConfigValue<?>> portConfig) {

        Communication comm = CommunicationFactory.getInstance();

        // final Object continueLock = new Object();
        //
        // final MessageListener msgListener = new DefaultMessageListener() {
        // @Override
        // public void lcConfigX(byte[] address, LcConfigX lcConfigX) {
        // LOGGER.info("Received the LcConfigX response, address: {}, lcConfigX: {}",
        // ByteUtils.bytesToHex(address), lcConfigX);
        // synchronized (continueLock) {
        // continueLock.notifyAll();
        // }
        // }
        //
        // @Override
        // public void lcNa(byte[] address, BidibPort bidibPort, Integer errorCode) {
        // LOGGER.info("Received the LcNa response, address: {}, port: {}", ByteUtils.bytesToHex(address),
        // bidibPort);
        //
        // synchronized (continueLock) {
        // continueLock.notifyAll();
        // }
        // }
        //
        // };
        // try {
        // comm.addMessageListener(msgListener);
        comm.setPortParameters(node.getNode(), port, null, portConfig);

        // LOGGER.info("Sent the new port parameters and wait for response before continue.");
        // synchronized (continueLock) {
        // continueLock.wait(3000);
        // }
        // LOGGER.info("Wait for response before continue has finished.");
        // }
        // catch (InterruptedException e) {
        // LOGGER.warn("Wait for response before continue was interrupted.");
        // }
        // finally {
        // comm.removeMessageListener(msgListener);
        // }
    }

    private VendorCvData loadCvDefinition(final Context context, final Node node) {
        VendorCvData vendorCV = null;

        if (node != null) {

            try {

                File file = new File("");
                file = new File(file.getAbsoluteFile(), "data/BiDiBNodeVendorData");

                String userHome = System.getProperty("user.home");
                File searchPathUserHomeWizard = new File(userHome, ".BiDiBWizard/data/BiDiBNodeVendorData");

                String labelPath = Preferences.getInstance().getLabelV2Path();
                File searchPathLabelPath = new File(labelPath, "data/BiDiBNodeVendorData");

                vendorCV =
                    VendorCvFactory.getCvDefinition(node.getNode(), context, searchPathLabelPath.getAbsolutePath(),
                        file.getAbsolutePath(), "classpath:/bidib", searchPathUserHomeWizard.getAbsolutePath());
            }
            catch (Exception ex) {
                LOGGER.warn("Get CV definition for node failed.", ex);
            }

            if (vendorCV != null) {
                node.setVendorCV(vendorCV);
                node.prepareVendorCVTree();
            }
            else if (ProductUtils.isOneBootloader(node.getUniqueId())) {
                LOGGER.info("The current node is a OneBootloader.");
            }
            else {
                LOGGER.warn("No CV definition available for node: {}", node);
            }
        }
        model.setCvDefinition(vendorCV);

        return vendorCV;
    }

    @Override
    public void reloadCvDefinition(final Context context, Node node) {
        // TODO Auto-generated method stub
        loadCvDefinition(context, node);
    }
}
