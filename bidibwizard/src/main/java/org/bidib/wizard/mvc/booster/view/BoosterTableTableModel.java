package org.bidib.wizard.mvc.booster.view;

import org.bidib.jbidibc.core.enumeration.BoosterState;
import org.bidib.jbidibc.core.enumeration.CommandStationState;
import org.bidib.jbidibc.core.utils.NodeUtils;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.booster.model.BoosterModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.adapter.AbstractTableAdapter;
import com.jgoodies.binding.list.SelectionInList;

public class BoosterTableTableModel extends AbstractTableAdapter<BoosterModel> {
    private static final Logger LOGGER = LoggerFactory.getLogger(BoosterTableTableModel.class);

    private static final long serialVersionUID = 1L;

    public static final int COLUMN_UNIQUE_ID = 0;

    public static final int COLUMN_DESCRIPTION = 1;

    public static final int COLUMN_STATUS = 2;

    public static final int COLUMN_CS_STATUS = 3;

    public static final int COLUMN_TEMPERATURE = 4;

    public static final int COLUMN_VOLTAGE = 5;

    public static final int COLUMN_MAX_CURRENT = 6;

    public static final int COLUMN_CURRENT = 7;

    public static final int COLUMN_PORT_INSTANCE = 8;

    private static final String[] COLUMNNAMES = new String[] {
        Resources.getString(BoosterTableTableModel.class, "uniqueId"),
        Resources.getString(BoosterTableTableModel.class, "description"),
        Resources.getString(BoosterTableTableModel.class, "status"),
        Resources.getString(BoosterTableTableModel.class, "csStatus"),
        Resources.getString(BoosterTableTableModel.class, "temperature"),
        Resources.getString(BoosterTableTableModel.class, "voltage"),
        Resources.getString(BoosterTableTableModel.class, "maxCurrent"),
        Resources.getString(BoosterTableTableModel.class, "current") };

    public BoosterTableTableModel(SelectionInList<BoosterModel> boosterList) {
        super(boosterList, COLUMNNAMES);

        LOGGER.info("Current listModel: {}", getListModel());
    }

    @Override
    public Class<?> getColumnClass(int column) {
        switch (column) {
            case COLUMN_UNIQUE_ID:
                return String.class;
            case COLUMN_STATUS:
                return BoosterState.class;
            case COLUMN_CS_STATUS:
                return CommandStationState.class;
            default:
                return Object.class;
        }
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        BoosterModel booster = (BoosterModel) getRow(rowIndex);
        switch (columnIndex) {
            case COLUMN_UNIQUE_ID:
                return NodeUtils.getUniqueIdAsString(booster.getBooster().getUniqueId());
            case COLUMN_DESCRIPTION:
                return booster.getNodeLabel();
            case COLUMN_STATUS:
                return booster.getState();
            case COLUMN_CS_STATUS:
                return booster.getCommandStationState();
            case COLUMN_TEMPERATURE:
                return booster.getTemperature();
            case COLUMN_VOLTAGE:
                return booster.getVoltage();
            case COLUMN_MAX_CURRENT:
                return booster.getMaxCurrent();
            case COLUMN_CURRENT:
                return booster.getCurrent();
            default:
                return null;
        }
    }
}
