package org.bidib.wizard.mvc.script.view.utils;

public class NodeRequirementException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public NodeRequirementException(String message) {
        super(message);
    }
}
