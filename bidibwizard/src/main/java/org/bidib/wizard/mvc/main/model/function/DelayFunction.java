package org.bidib.wizard.mvc.main.model.function;

import org.bidib.jbidibc.exchange.lcmacro.DelayPoint;
import org.bidib.jbidibc.exchange.lcmacro.LcMacroPointType;
import org.bidib.wizard.comm.BidibStatus;

/**
 * The delay function stores the delay value that is used as outputNumber attribute when sent to the node.
 */
public class DelayFunction extends SystemFunction<BidibStatus> implements Delayable {
    private int delay;

    public DelayFunction() {
        super(null, KEY_DELAY);
    }

    @Override
    public int getDelay() {
        return delay;
    }

    @Override
    public void setDelay(int delay) {
        this.delay = delay;
    }

    public String getDebugString() {
        return "@" + getDelay() + " Delay";
    }

    @Override
    public LcMacroPointType toLcMacroPoint() {
        DelayPoint delayPoint = new DelayPoint();
        delayPoint.setDelayActionType(getDelay());
        return delayPoint;
    }

    public static class DelayFunctionBuilder extends DelayFunctionBuilderBase<DelayFunctionBuilder> {
        public static DelayFunctionBuilder delayFunction() {
            return new DelayFunctionBuilder();
        }

        public DelayFunctionBuilder() {
            super(new DelayFunction());
        }

        public DelayFunction build() {
            return getInstance();
        }
    }

    static class DelayFunctionBuilderBase<GeneratorT extends DelayFunctionBuilderBase<GeneratorT>> {
        private DelayFunction instance;

        protected DelayFunctionBuilderBase(DelayFunction aInstance) {
            instance = aInstance;
        }

        protected DelayFunction getInstance() {
            return instance;
        }

        @SuppressWarnings("unchecked")
        public GeneratorT withDelay(int aValue) {
            instance.setDelay(aValue);

            return (GeneratorT) this;
        }
    }

}
