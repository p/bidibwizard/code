package org.bidib.wizard.mvc.firmware.controller;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JFrame;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.bidib.jbidibc.core.FirmwareUpdateStat;
import org.bidib.jbidibc.core.StringData;
import org.bidib.jbidibc.core.enumeration.FirmwareUpdateOperation;
import org.bidib.jbidibc.core.enumeration.FirmwareUpdateState;
import org.bidib.jbidibc.core.exception.NoAnswerException;
import org.bidib.jbidibc.core.node.RootNode;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.core.utils.NodeUtils;
import org.bidib.jbidibc.exchange.bidib.FirmwareFactory;
import org.bidib.jbidibc.exchange.firmware.FirmwareNode;
import org.bidib.wizard.comm.Communication;
import org.bidib.wizard.comm.CommunicationFactory;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.dialog.FileDialog;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.bidib.wizard.mvc.firmware.controller.listener.FirmwareControllerListener;
import org.bidib.wizard.mvc.firmware.model.FirmwareModel;
import org.bidib.wizard.mvc.firmware.model.FirmwareUpdatePart;
import org.bidib.wizard.mvc.firmware.model.UpdateStatus;
import org.bidib.wizard.mvc.firmware.view.FirmwareView;
import org.bidib.wizard.mvc.firmware.view.listener.FirmwareViewListener;
import org.bidib.wizard.mvc.main.controller.CommandStationService;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.model.listener.DefaultNodeListListener;
import org.bidib.wizard.mvc.main.model.listener.NodeListListener;
import org.bidib.wizard.mvc.preferences.model.Preferences;
import org.bidib.wizard.utils.FirmwareUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FirmwareController {
    private static final Logger LOGGER = LoggerFactory.getLogger(FirmwareController.class);

    private final Collection<FirmwareControllerListener> listeners = new LinkedList<FirmwareControllerListener>();

    private final Node node;

    private final JFrame parent;

    private final int x;

    private final int y;

    private final FirmwareModel firmwareModel = new FirmwareModel();

    private final MainModel mainModel;

    private NodeListListener nodeListListener;

    public FirmwareController(JFrame parent, Node node, int x, int y, final MainModel mainModel) {
        this.parent = parent;
        this.node = node;
        this.x = x;
        this.y = y;
        this.mainModel = mainModel;
    }

    public void addFirmwareControllerListener(FirmwareControllerListener l) {
        listeners.add(l);
    }

    private void fireClose() {
        LOGGER.info("Close the firmware controller.");
        if (nodeListListener != null) {
            LOGGER.info("Remove the nodelist listener.");
            mainModel.removeNodeListListener(nodeListListener);
            nodeListListener = null;
        }

        for (FirmwareControllerListener l : listeners) {
            l.close();
        }
    }

    /**
     * Start the firmware update controller.
     */
    public void start() {
        final Communication communication = CommunicationFactory.getInstance();

        // get current values
        firmwareModel.setNode(node);
        firmwareModel
            .setNodeName(node.getLabel() != null ? node.getLabel() : NodeUtils.prepareNodeLabel(node.getNode()));
        firmwareModel.setProductName(node.getNode().getStoredString(StringData.INDEX_PRODUCTNAME));
        long uniqueId = node.getNode().getUniqueId();
        firmwareModel.setUniqueId(NodeUtils.getUniqueIdAsString(uniqueId));
        firmwareModel.setNodeCurrentVersion(communication.getSoftwareVersion(node.getNode()).toString());

        // check for ClassID1 Bit 7 and addr = 0
        boolean isBootloaderRootNode = false;
        isBootloaderRootNode =
            /* NodeUtils.hasSubNodesFunctions(uniqueId) && */ node.isUpdatable()
                && Arrays.equals(node.getNode().getAddr(), RootNode.ROOTNODE_ADDR);
        LOGGER.info("The current node is a bootloader rootnode: {}", isBootloaderRootNode);
        firmwareModel.setBootloaderRootNode(isBootloaderRootNode);

        nodeListListener = new DefaultNodeListListener() {

            @Override
            public void listNodeRemoved(Node node) {
                LOGGER.info("Node was removed. Verify that it's not the node to be updated: {}", node);

                if (node.equals(FirmwareController.this.node)) {
                    LOGGER.error("The node that is updated was removed! Cancel all firmware update operations.");

                    firmwareModel.setUpdateStatus(UpdateStatus.NODE_LOST);

                    firmwareModel.addProcessingStatus(Resources.getString(FirmwareController.class, "status.node-lost"),
                        1);
                }
            }
        };
        mainModel.addNodeListListener(nodeListListener);

        // create the firmware update view
        final FirmwareView view = new FirmwareView(parent, firmwareModel);
        if (x > -1 && y > -1) {
            view.setLocation(x + 20, y + 20);
        }
        else {
            view.setLocationRelativeTo(null);
        }

        firmwareModel.addPropertyChangeListener(FirmwareModel.PROPERTYNAME_CV_DEFINITION_FILES,
            new PropertyChangeListener() {

                @Override
                public void propertyChange(PropertyChangeEvent evt) {

                    if (CollectionUtils.isNotEmpty(firmwareModel.getCvDefinitionFiles())) {
                        List<String> cvDefinitionFiles = firmwareModel.getCvDefinitionFiles();
                        String version = firmwareModel.getUpdateVersion();
                        LOGGER.info("Import the CV definition files: {}, version: {}", cvDefinitionFiles, version);

                        String labelPath = Preferences.getInstance().getLabelV2Path();
                        File searchPathLabelPath = new File(labelPath, "data/BiDiBNodeVendorData");

                        // make sure the path exists
                        searchPathLabelPath.mkdirs();

                        String firmwareArchivePath = firmwareModel.getCvDefinitionArchivePath();
                        File firmwareFile = new File(firmwareArchivePath);

                        for (String cvDefinitionFile : cvDefinitionFiles) {

                            List<String> firmwareContent = new ArrayList<String>();
                            try {
                                LOGGER.info("Load CV definition from archive file into buffer: {}",
                                    firmwareArchivePath);

                                firmwareContent =
                                    FirmwareFactory.getCvDefinitionContent(firmwareFile, cvDefinitionFile);

                                LOGGER.info(
                                    "Load CV definition from file into buffer passed. Total number of lines: {}",
                                    firmwareContent.size());

                                String targetFileName = cvDefinitionFile;

                                // the cvDefinition filename must be versioned ...
                                if (!FirmwareUtils.hasVersionInFilename(cvDefinitionFile)) {
                                    // if (!cvDefinitionFile.contains(version)) {
                                    targetFileName =
                                        cvDefinitionFile.substring(0, cvDefinitionFile.indexOf(".xml")) + "-" + version
                                            + ".xml";
                                    LOGGER.info("Prepared cvDefinitionFile with the version: {}", targetFileName);
                                }
                                else {
                                    LOGGER.info("The cvDefinitionFile already contains a version: {}", targetFileName);
                                }
                                // LOGGER.info("Current targetFileName: {}", targetFileName);

                                FileOutputStream fos = null;
                                try {
                                    File file = new File(searchPathLabelPath, targetFileName);

                                    if (file.exists()) {
                                        // ask user to overwrite the file
                                        boolean override = FileDialog.askOverrideExisting(parent, file);
                                        if (!override) {
                                            LOGGER.info("User decided to not overwrite the existing file.");
                                            continue;
                                        }
                                    }

                                    fos = new FileOutputStream(file);
                                    IOUtils.writeLines(firmwareContent, null, fos, Charset.forName("UTF-8"));
                                    fos.flush();
                                }
                                catch (Exception ex1) {
                                    LOGGER.warn("Write CV definition file failed: {}", cvDefinitionFile, ex1);
                                }
                                finally {
                                    if (fos != null) {
                                        try {
                                            fos.close();
                                        }
                                        catch (Exception e1) {
                                            LOGGER.warn("Close fos failed.", e1);
                                        }
                                    }
                                }

                            }
                            catch (Exception e) {
                                LOGGER.warn("Load CV definition from file into buffer failed", e);

                                return;
                            }
                        }
                    }

                }
            });

        view.addFirmwareViewListener(new FirmwareViewListener() {

            @Override
            public void close() {
                if (UpdateStatus.NONE.equals(firmwareModel.getUpdateStatus())
                    || UpdateStatus.NODE_LOST.equals(firmwareModel.getUpdateStatus())) {
                    LOGGER.info("No software update was started or node was lost: {}", firmwareModel.getUpdateStatus());
                }
                else {
                    LOGGER.info("Send the firmware update operation EXIT command.");
                    try {
                        Communication communication = CommunicationFactory.getInstance();
                        sendCommand(communication, FirmwareUpdateOperation.EXIT);
                        LOGGER.info("Send the EXIT command to the node passed.");

                        if (firmwareModel.isBootloaderRootNode()) {
                            LOGGER.info(
                                "A bootloader root node was updated. We must release the root node and get the root and all children again.");

                            // trigger release the root node
                            communication.releaseAndReloadRootNode(node.getNode());
                        }
                    }
                    catch (InterruptedException e) {
                        LOGGER.warn("Send exit operation failed.", e);
                        throw new RuntimeException(e);
                    }
                }
                LOGGER.info("Close the dialog.");
                fireClose();
            }

            @Override
            public void updateFirmware() {
                // start the firmware update with the selected files
                final List<FirmwareNode> firmwareFiles = firmwareModel.getFirmwareFiles();
                final String firmwareArchivePath = firmwareModel.getFirmwareArchivePath();

                LOGGER.info("Start the firmware update process, firmwareArchivePath: {}", firmwareArchivePath);

                if (CollectionUtils.isNotEmpty(firmwareFiles)) {

                    if (firmwareModel.isBootloaderRootNode()) {
                        final CommandStationService commandStationService =
                            DefaultApplicationContext.getInstance().get(
                                DefaultApplicationContext.KEY_COMMAND_STATION_SERVICE, CommandStationService.class);
                        if (commandStationService != null) {
                            LOGGER.info(
                                "Stop all watchdog tasks before update because we update the bootloader root node.");
                            commandStationService.stopAllWatchDogTasks();
                        }
                    }
                    // start a new thread to send the firmware to the node
                    new Thread() {
                        @Override
                        public void run() {
                            List<FirmwareUpdatePart> firmwareUpdateParts = new LinkedList<>();
                            int fileSize = 0;
                            // Load the firmware into memory
                            for (FirmwareNode firmwareNode : firmwareFiles) {

                                File firmwareFile = new File(firmwareArchivePath);
                                int destination = firmwareNode.getDestinationNumber();

                                List<String> firmwareContent = null;
                                try {
                                    LOGGER.info("Load firmware from file into buffer: {}", firmwareArchivePath);

                                    firmwareContent =
                                        FirmwareFactory.getFirmwareContent(firmwareFile, firmwareNode.getFilename());

                                    if (CollectionUtils.isNotEmpty(firmwareContent)) {
                                        for (String line : firmwareContent) {
                                            fileSize += line.length();
                                        }
                                    }
                                    LOGGER.info(
                                        "Load firmware from file into buffer passed. Total number of packets to transfer: {}",
                                        firmwareContent.size());

                                    firmwareModel.addProcessingStatus(
                                        Resources.getString(FirmwareController.class, "status.load-firmware-passed"), 0,
                                        firmwareNode.getFilename());
                                }
                                catch (Exception e) {
                                    LOGGER.warn("Load firmware from file into buffer failed", e);

                                    firmwareModel.addProcessingStatus(
                                        Resources.getString(FirmwareController.class, "status.load-firmware-failed"), 1,
                                        firmwareNode.getFilename());
                                    firmwareModel.setUpdateStatus(UpdateStatus.PREPARE_FAILED);
                                    firmwareModel.setInProgress(false);
                                    return;
                                }

                                FirmwareUpdatePart firmwareUpdatePart =
                                    new FirmwareUpdatePart(firmwareNode.getFilename(), firmwareContent, destination);
                                firmwareUpdateParts.add(firmwareUpdatePart);
                            }

                            try {
                                LOGGER.info("Transfer file with length: {}", fileSize);
                                firmwareModel.setUpdateStatus(UpdateStatus.PREPARE);
                                boolean enterFwUpdateModePassed =
                                    sendCommand(communication, FirmwareUpdateOperation.ENTER,
                                        ByteUtils.convertLongToUniqueId(node.getNode().getUniqueId()));
                                if (enterFwUpdateModePassed) {
                                    firmwareModel.setUpdateStatus(UpdateStatus.ENTRY_PASSED);

                                    int currentSize = 0;
                                    boolean errorDetected = false;
                                    // transfer all selected parts
                                    for (FirmwareUpdatePart part : firmwareUpdateParts) {
                                        int destIdentifier = part.getDestination();

                                        LOGGER.info("Set the destination for the firmware: {}", destIdentifier);
                                        if (sendCommand(communication, FirmwareUpdateOperation.SETDEST,
                                            ByteUtils.getLowByte(destIdentifier))) {

                                            LOGGER.info("Set the destination for the firmware passed: {}",
                                                ByteUtils.getLowByte(destIdentifier));

                                            firmwareModel.setUpdateStatus(UpdateStatus.DATA_TRANSFER);
                                            firmwareModel.addProcessingStatus(
                                                Resources.getString(FirmwareController.class, "status.start-transfer"),
                                                0, part.getFilename());

                                            int block = 0;
                                            for (String line : part.getFirmwareContent()) {
                                                currentSize += line.length();
                                                firmwareModel.setProgressValue(currentSize * 100 / fileSize);
                                                LOGGER.trace("Send block: {}, line: {}", block, line);

                                                if (!sendCommand(communication, FirmwareUpdateOperation.DATA,
                                                    line.getBytes())) {
                                                    LOGGER.warn(
                                                        "Unexpected answer from node while sending fw data (block#: {}, line: {}).",
                                                        block, line);

                                                    errorDetected = true;
                                                    firmwareModel.addProcessingStatus(Resources.getString(
                                                        FirmwareController.class, "status.unexpected-answer"), 1);
                                                    firmwareModel.addProcessingStatus(Resources
                                                        .getString(FirmwareController.class, "status.transfer-aborted"),
                                                        1, part.getFilename());

                                                    break;
                                                }
                                                block++;
                                            }

                                            if (!errorDetected) {
                                                LOGGER.info("Send the firmware update done command.");
                                                sendCommand(communication, FirmwareUpdateOperation.DONE);
                                                firmwareModel.addProcessingStatus(Resources
                                                    .getString(FirmwareController.class, "status.transfer-finished"), 0,
                                                    part.getFilename());
                                            }
                                            else {
                                                LOGGER.warn("There was an error detected during firmware update.");
                                            }
                                        }

                                        if (errorDetected) {
                                            LOGGER
                                                .info("Error detected during firmware update. Cancel update process.");

                                            firmwareModel.setUpdateStatus(UpdateStatus.DATA_TRANSFER_FAILED);
                                            break;
                                        }
                                    }

                                    if (!errorDetected) {
                                        // all parts were transfered
                                        firmwareModel.setUpdateStatus(UpdateStatus.DATA_TRANSFER_PASSED);
                                        LOGGER.info("The firmware update has passed.");
                                    }
                                    else {
                                        LOGGER.warn("The firmware update has not passed.");
                                    }

                                    firmwareModel.setProgressValue(100);
                                    firmwareModel.setInProgress(false);
                                }
                                else {
                                    LOGGER.warn("Enter firmware update operation failed.");
                                    firmwareModel.setProgressValue(0);
                                    firmwareModel.setInProgress(false);
                                    firmwareModel.addProcessingStatus(Resources.getString(FirmwareController.class,
                                        "status.enter-firmware-update-mode-failed"), 1);
                                }
                            }
                            catch (Exception e) {
                                LOGGER.warn("Transfer firmware udpate to node failed.", e);

                                firmwareModel.addProcessingStatus(
                                    Resources.getString(FirmwareController.class, "status.transfer-firmware-failed"),
                                    1);
                                firmwareModel.setUpdateStatus(UpdateStatus.DATA_TRANSFER_FAILED);
                                firmwareModel.setInProgress(false);
                            }
                        }
                    }.start();
                }
                else {
                    firmwareModel.setInProgress(false);
                }
            }
        });

        view.setVisible(true);
    }

    private boolean sendCommand(Communication communication, FirmwareUpdateOperation operation, byte... data)
        throws InterruptedException {
        boolean result = false;

        FirmwareUpdateStat updateStat = null;
        // handle firmware update operations
        // send firmware operation to node and wait for result
        try {
            updateStat = communication.sendFirmwareUpdateOperation(node.getNode(), operation, data);
        }
        catch (NoAnswerException ex) {
            switch (operation) {
                case ENTER:
                    LOGGER.warn("No answer received during enter firmware update mode.", ex);
                    break;
                case EXIT:
                    LOGGER.warn("No answer received during exit firmware update mode.", ex);
                    break;
                default:
                    LOGGER.warn("No answer received during firmware update, try send data again.", ex);
                    traceTimeout(operation);
                    break;
            }
        }

        if (updateStat != null) {
            LOGGER.info("Received update stat, timeout: {}, state: {}, last operation: {}", updateStat.getTimeout(),
                updateStat.getState(), operation);

            if (updateStat.getTimeout() > 0) {
                int extendedTime = updateStat.getTimeout() * 10;
                LOGGER.warn("The node requested a wait to complete: {} ms", extendedTime);
                traceMoreTimeRequested(extendedTime);
                Thread.sleep(extendedTime);
                traceContinueAfterMoreTimeRequested(extendedTime);
            }

            FirmwareUpdateState state = updateStat.getState();

            if ((operation == FirmwareUpdateOperation.ENTER && state == FirmwareUpdateState.READY)
                || (operation == FirmwareUpdateOperation.SETDEST && state == FirmwareUpdateState.DATA)
                || (operation == FirmwareUpdateOperation.DATA && state == FirmwareUpdateState.DATA)) {
                result = true;
            }
        }
        else {
            LOGGER.warn("No updateStat received for operation: {}", operation);
        }
        LOGGER.debug("sendCommand return result: {}", result);
        return result;
    }

    private void traceTimeout(final FirmwareUpdateOperation operation) {
        firmwareModel.addProcessingStatus(
            Resources.getString(FirmwareController.class, "status.transfer-firmware-timeout"), 1, operation);
    }

    private void traceMoreTimeRequested(int extendedTime) {
        firmwareModel.addProcessingStatus(
            Resources.getString(FirmwareController.class, "status.transfer-firmware-more-time-requested"), 1,
            extendedTime);
    }

    private void traceContinueAfterMoreTimeRequested(int extendedTime) {
        firmwareModel.addProcessingStatus(
            Resources.getString(FirmwareController.class, "status.transfer-firmware-continue-after-more-time-wait"), 1,
            extendedTime);
    }
}
