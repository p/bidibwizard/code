package org.bidib.wizard.mvc.common.view.renderer;

import java.text.DecimalFormat;

import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

public class DecRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = 1L;

    private DecimalFormat df;

    public DecRenderer(DecimalFormat df) {
        this.df = df;
        this.setHorizontalAlignment(SwingConstants.RIGHT);
    }

    @Override
    protected void setValue(Object value) {
        setText((value == null) ? "" : df.format(value));
    }
}
