package org.bidib.wizard.mvc.main.view.table;

import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import org.apache.commons.lang.StringUtils;
import org.bidib.jbidibc.core.enumeration.PortConfigStatus;
import org.bidib.wizard.common.utils.ImageUtils;
import org.bidib.wizard.mvc.main.model.Port;

public class PortConfigErrorAwareRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = 1L;

    private final int portColumnIndex;

    private ImageIcon errorIcon;

    private ImageIcon inactiveIcon;

    public PortConfigErrorAwareRenderer(int portColumnIndex) {
        this.portColumnIndex = portColumnIndex;

        errorIcon = ImageUtils.createImageIcon(PortConfigErrorAwareRenderer.class, "/icons/error-leaf.png");
        inactiveIcon = ImageUtils.createImageIcon(PortConfigErrorAwareRenderer.class, "/icons/cancel.png");
    }

    public Component getTableCellRendererComponent(
        JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        setIcon(null);
        setToolTipText(null);

        Port<?> port = (Port<?>) table.getModel().getValueAt(row, portColumnIndex);
        if (port != null) {
            PortConfigStatus configStatus = port.getConfigStatus();
            setEnabled(port.isEnabled());
            if (port.isInactive()) {
                // the port is inactive
                setIcon(inactiveIcon);
                setToolTipText("This port is inactive");
            }
            else if (port.isEnabled()) {
                if (PortConfigStatus.CONFIG_ERROR.equals(configStatus)) {
                    // error detected
                    setIcon(errorIcon);
                    setToolTipText("This port has an error: " + port.getPortConfigErrorCode());
                }
            }

            String text = null;
            if (StringUtils.isNotBlank(port.getLabel())) {
                text = String.format("%1$02d : %2$s", port.getId(), port.getLabel());
            }
            else if (port.getId() > -1) {
                text = String.format("%1$02d :", port.getId());
            }
            else {
                text = " ";
            }

            setText(text);
        }

        return this;
    }
}
