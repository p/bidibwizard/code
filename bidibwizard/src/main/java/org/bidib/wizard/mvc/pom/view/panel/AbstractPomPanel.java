package org.bidib.wizard.mvc.pom.view.panel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.UIManager;

import org.bidib.jbidibc.core.AddressData;
import org.bidib.jbidibc.core.enumeration.PomOperation;
import org.bidib.jbidibc.core.enumeration.PomProgState;
import org.bidib.jbidibc.core.utils.CollectionUtils;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.common.utils.ImageUtils;
import org.bidib.wizard.mvc.common.view.validation.IconFeedbackPanel;
import org.bidib.wizard.mvc.main.view.panel.NodeTree;
import org.bidib.wizard.mvc.pom.model.PomProgrammerModel;
import org.bidib.wizard.mvc.pom.model.listener.ProgCommandListener;
import org.bidib.wizard.mvc.pom.view.CurrentAddressBeanModel;
import org.bidib.wizard.mvc.pom.view.command.PomOperationCommand;
import org.bidib.wizard.mvc.pom.view.command.PomOperationContinueAfterTimeoutCommand;
import org.bidib.wizard.mvc.pom.view.command.PomOperationIfElseCommand;
import org.bidib.wizard.mvc.pom.view.panel.ProgCommandAwareBeanModel.ExecutionType;
import org.bidib.wizard.mvc.pom.view.panel.listener.PomRequestListener;
import org.bidib.wizard.mvc.pom.view.panel.listener.PomResultListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.helpers.MessageFormatter;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.value.ConverterValueModel;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.debug.FormDebugPanel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.validation.ValidationResultModel;

public abstract class AbstractPomPanel<M extends ProgCommandAwareBeanModel> implements PomResultListener {
    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());

    public static final int DEFAULT_TIMEOUT = 1000;

    public static final int DEFAULT_SUSI_READ_FIRST_TIMEOUT = 300;

    private static final String ENCODED_COLUMN_SPECS = "pref, 3dlu, pref, 3dlu, pref, 3dlu, fill:50dlu:grow";

    private final List<PomRequestListener> listeners = new LinkedList<PomRequestListener>();

    protected final PomProgrammerModel cvProgrammerModel;

    protected final PomResultProxyModel pomResultProxyModel;

    protected final JButton readButton = new JButton(Resources.getString(AbstractPomPanel.class, "read"));

    protected final JButton writeButton = new JButton(Resources.getString(AbstractPomPanel.class, "write"));

    protected ValueModel readButtonEnabled;

    protected ValueModel writeButtonEnabled;

    protected ValueModel currentOperationModel;

    protected JLabel currentOperationLabel;

    private ImageIcon progOperationErrorIcon;

    private ImageIcon progOperationSuccessfulIcon;

    private ImageIcon progOperationWaitIcon;

    protected ImageIcon progOperationUnknownIcon;

    private ImageIcon progOperationEmptyIcon;

    private JTextArea loggerArea;

    private final static String NEWLINE = "\n";

    protected boolean activeTab;

    private M progCommandAwareBeanModel;

    protected CurrentAddressBeanModel currentAddressBeanModel;

    private Timer readTimeout;

    private Object readTimeoutLock = new Object();

    /**
     * Creates a new instance of the AbstractPomPanel.
     * 
     * @param cvProgrammerModel
     *            the POM programmer model
     * @param currentAddressBeanModel
     *            the address bean model
     */
    public AbstractPomPanel(final PomProgrammerModel cvProgrammerModel,
        final CurrentAddressBeanModel currentAddressBeanModel) {
        this.cvProgrammerModel = cvProgrammerModel;
        this.currentAddressBeanModel = currentAddressBeanModel;

        // create a proxy model that receives updates from the programmer model but does not update values if not active
        pomResultProxyModel = new PomResultProxyModel() {
            private static final long serialVersionUID = 1L;

            @Override
            public boolean isAllowUpdate() {
                // this method is overriden to take the flag of this panel into account
                return AbstractPomPanel.this.isActive();
            }
        };

        initializeIcons();
    }

    /**
     * Initialize the icons
     */
    private void initializeIcons() {
        // Load the icons
        progOperationErrorIcon = ImageUtils.createImageIcon(NodeTree.class, "/icons/accessory-error.png");
        progOperationSuccessfulIcon = ImageUtils.createImageIcon(NodeTree.class, "/icons/accessory-successful.png");
        progOperationWaitIcon = ImageUtils.createImageIcon(NodeTree.class, "/icons/accessory-wait.png");
        progOperationUnknownIcon = ImageUtils.createImageIcon(NodeTree.class, "/icons/information.png");
        progOperationEmptyIcon = ImageUtils.createImageIcon(NodeTree.class, "/icons/empty.png");
    }

    /**
     * @return the progCommandAwareBeanModel
     */
    public ProgCommandAwareBeanModel getProgCommandAwareBeanModel() {
        return progCommandAwareBeanModel;
    }

    /**
     * @param progCommandAwareBeanModel
     *            the progCommandAwareBeanModel
     */
    public void setProgCommandAwareBeanModel(M progCommandAwareBeanModel) {
        this.progCommandAwareBeanModel = progCommandAwareBeanModel;
    }

    public void addPomRequestListener(PomRequestListener l) {
        listeners.add(l);
    }

    protected List<PomRequestListener> getPomRequestListeners() {
        return listeners;
    }

    @Override
    public void setActive(boolean active) {
        LOGGER.info("Set the active flag: {}", active);
        this.activeTab = active;
    }

    @Override
    public boolean isActive() {
        return activeTab;
    }

    protected abstract void createWorkerPanel(
        DefaultFormBuilder builder, final PomValidationResultModel parentValidationModel);

    private static class LogAreaAwarePanel extends JPanel implements LogAreaAware {
        private static final long serialVersionUID = 1L;

        @Override
        public void clearLogArea() {
        }
    }

    private static class LogAreaAwareDebugPanel extends FormDebugPanel implements LogAreaAware {
        private static final long serialVersionUID = 1L;

        @Override
        public void clearLogArea() {
        }
    }

    public JPanel createPanel(final PomValidationResultModel parentValidationModel) {

        cvProgrammerModel.addPropertyChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                switch (evt.getPropertyName()) {
                    case PomProgrammerModel.PROPERTYNAME_POMPROGSTATE:
                        pomResultProxyModel.setPomProgState((PomProgState) evt.getNewValue());
                        break;
                    case PomProgrammerModel.PROPERTYNAME_CVNUMBER:
                        pomResultProxyModel.setCvNumber((int) evt.getNewValue());
                        break;
                    case PomProgrammerModel.PROPERTYNAME_CVVALUE:
                        pomResultProxyModel.setCvValue((Integer) evt.getNewValue());
                        break;
                    default:
                        break;
                }
            }
        });

        DefaultFormBuilder builder = null;
        boolean debug = false;
        if (debug) {
            builder = new DefaultFormBuilder(new FormLayout(ENCODED_COLUMN_SPECS), new LogAreaAwareDebugPanel() {
                private static final long serialVersionUID = 1L;

                @Override
                public void clearLogArea() {
                    AbstractPomPanel.this.clearLogArea();
                }
            });
        }
        else {
            builder = new DefaultFormBuilder(new FormLayout(ENCODED_COLUMN_SPECS), new LogAreaAwarePanel() {
                private static final long serialVersionUID = 1L;

                @Override
                public void clearLogArea() {
                    AbstractPomPanel.this.clearLogArea();
                }
            });
        }
        builder.border(Borders.TABBED_DIALOG);

        // create the worker panel content
        createWorkerPanel(builder, parentValidationModel);

        // prepare the operation verdict
        currentOperationModel =
            new PropertyAdapter<PomResultProxyModel>(pomResultProxyModel,
                PomResultProxyModel.PROPERTYNAME_POMPROGSTATE, true);
        ValueModel valueConverterModel = new ConverterValueModel(currentOperationModel, new PomProgStateConverter());
        currentOperationLabel = BasicComponentFactory.createLabel(valueConverterModel);
        currentOperationLabel.setIcon(progOperationEmptyIcon);
        builder.append(Resources.getString(AbstractPomPanel.class, "prog-result"));
        builder.append(currentOperationLabel, 5);
        builder.appendRow("5dlu");
        builder.nextLine(2);

        // prepare the logger area
        loggerArea = new JTextArea(20, 45);
        loggerArea.setFont(UIManager.getDefaults().getFont("Label.font"));
        // loggerArea.setLineWrap(true);
        JScrollPane scrollPane =
            new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        scrollPane.getViewport().add(loggerArea);
        loggerArea.setEditable(false);

        builder.appendRow("fill:200px:grow");
        builder.append(scrollPane, 7);

        doBindButtons();

        // handle the finished prog commands
        cvProgrammerModel.addProgCommandListener(new ProgCommandListener() {

            @Override
            public void progPomFinished(PomProgState pomProgState) {
                LOGGER.info("The prog command has finished, pomProgState: {}", pomProgState);
                if (!isActive()) {
                    return;
                }

                if (!PomProgState.POM_PROG_START.equals(pomProgState)
                    && !PomProgState.POM_PROG_RUNNING.equals(pomProgState)) {

                    PomOperationCommand<M> executingProgCommand =
                        (PomOperationCommand<M>) progCommandAwareBeanModel.getExecutingProgCommand();
                    if (executingProgCommand != null) {
                        LOGGER.info("The executingProgCommand: {}", executingProgCommand);
                        executingProgCommand.setProgStateResult(pomProgState);

                        boolean ignoreError = false;

                        if (executingProgCommand instanceof PomOperationIfElseCommand) {
                            PomOperationIfElseCommand<M> ifElseCommand =
                                (PomOperationIfElseCommand<M>) executingProgCommand;
                            LOGGER.info("The executing command is a if-else-command: {}", ifElseCommand);

                            // we must check if the result is successful or not
                            if (!PomProgState.POM_PROG_OKAY.equals(ifElseCommand.getProgStateResult())
                                || !ifElseCommand.isExpectedResult()) {
                                LOGGER
                                    .info("The command was not successful executed or expected result was not received!");
                                List<PomOperationCommand<M>> failureCommands = ifElseCommand.getProgCommandsFailure();
                                if (CollectionUtils.hasElements(failureCommands)) {
                                    LOGGER.info("Found failure commands to be executed: {}", failureCommands);
                                    // add the new commands to process
                                    List<PomOperationCommand<? extends ProgCommandAwareBeanModel>> progCommands =
                                        progCommandAwareBeanModel.getProgCommands();
                                    progCommands.clear();
                                    progCommands.addAll(failureCommands);

                                    // ignore the error
                                    ignoreError = true;
                                }
                            }
                            else {
                                LOGGER.info("The command was successful executed!");
                                List<PomOperationCommand<M>> successCommands = ifElseCommand.getProgCommandsSuccess();
                                if (CollectionUtils.hasElements(successCommands)) {
                                    LOGGER.info("Found success commands to be executed: {}", successCommands);
                                    // add the new commands to process
                                    List<PomOperationCommand<? extends ProgCommandAwareBeanModel>> progCommands =
                                        progCommandAwareBeanModel.getProgCommands();
                                    progCommands.clear();
                                    progCommands.addAll(successCommands);
                                }

                            }
                        }

                        // we must check if the result is successful or not
                        if (PomProgState.POM_PROG_OKAY.equals(executingProgCommand.getProgStateResult())) {
                            LOGGER.info("PostExecute the command.");
                            executingProgCommand.postExecute(progCommandAwareBeanModel);
                        }
                        else if (!ignoreError) {
                            // if an error occurs we must stop processing!!!!

                            LOGGER.warn("Clear remaining prog commands because command has finished with an error: {}",
                                executingProgCommand);
                            if (CollectionUtils.hasElements(progCommandAwareBeanModel.getProgCommands())) {
                                progCommandAwareBeanModel.getProgCommands().clear();
                            }

                            addLogText("Error detected. Please try again.");
                        }

                        // keep the executed commands
                        progCommandAwareBeanModel.getExecutedProgCommands().add(executingProgCommand);
                        executingProgCommand = null;
                    }

                    if (CollectionUtils.hasElements(progCommandAwareBeanModel.getProgCommands())) {
                        LOGGER.info("Prepare the next command.");

                        startTimeoutControl(DEFAULT_TIMEOUT);
                        LOGGER.info("Fire the next command.");
                        fireNextCommand();
                    }
                    else {
                        LOGGER.info("No more commands to send, enable the input elements.");
                        stopTimeoutControl();
                        enableInputElements();
                    }
                }
            }
        });

        // react on changes of CV value
        cvProgrammerModel.addPropertyChangeListener(PomProgrammerModel.PROPERTYNAME_CVVALUE,
            new PropertyChangeListener() {

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    if (!isActive()) {
                        return;
                    }

                    LOGGER.info("The CV value has been changed: {}", evt.getNewValue());

                    if (progCommandAwareBeanModel.getCurrentOperation() != null) {
                        switch (progCommandAwareBeanModel.getCurrentOperation()) {
                            case RD_BIT:
                            case RD_BYTE:
                                progCommandAwareBeanModel.getExecutingProgCommand().setCvValueResult(
                                    (Integer) evt.getNewValue());

                                if (evt.getNewValue() != null) {
                                    addLogText("Read operation returned: {}", progCommandAwareBeanModel
                                        .getExecutingProgCommand().getCvValueResult());
                                    // addLogText("Read byte returned: {}", evt.getNewValue());
                                }
                                break;
                            case WR_BYTE:
                                break;
                            // case RD_BIT:
                            case WR_BIT:
                                /*
                                 * if (ExecutionType.READ.equals(progCommandAwareBeanModel.getExecution())) { Object
                                 * value = evt.getNewValue(); LOGGER.info("Returned read bit value: {}", value); if
                                 * (value != null) { int val = ((Integer) value).intValue(); int bitValue = (val & 0x08)
                                 * == 0x08 ? 1 : 0;
                                 * progCommandAwareBeanModel.getExecutingProgCommand().setCvValueResult(bitValue);
                                 * 
                                 * addLogText("Read bit returned: {}", bitValue); } } else { // nothing to do }
                                 */
                                break;
                            default:
                                break;
                        }
                    }
                    else {
                        LOGGER.info("No operation performed.");
                    }
                }
            });

        // listen on validation changes of parent validation model
        if (parentValidationModel != null) {
            parentValidationModel.addPropertyChangeListener(PomValidationResultModel.PROPERTY_VALID_STATE,
                new PropertyChangeListener() {

                    @Override
                    public void propertyChange(PropertyChangeEvent evt) {
                        triggerValidation();
                    }
                });
            parentValidationModel.addPropertyChangeListener(
                PomValidationResultModel.PROPERTY_VALID_STATE_NO_WARN_OR_ERRORS, new PropertyChangeListener() {

                    @Override
                    public void propertyChange(PropertyChangeEvent evt) {
                        triggerValidation();
                    }
                });
        }

        JPanel panel = null;
        // check if we have validation enabled
        if (getValidationResultModel() != null) {
            LOGGER.info("Create iconfeedback panel.");
            JComponent cvIconPanel = new IconFeedbackPanel(getValidationResultModel(), builder.build());
            DefaultFormBuilder feedbackBuilder = null;
            feedbackBuilder = new DefaultFormBuilder(new FormLayout("p:g"));

            feedbackBuilder.appendRow("fill:p:grow");
            feedbackBuilder.add(cvIconPanel);

            panel = feedbackBuilder.build();

            // TODO validation is triggered by PomProgrammerView
            // triggerValidation();
        }
        else {
            panel = builder.build();
        }

        return panel;
    }

    protected void doBindButtons() {
        // add bindings for enable/disable the write button
        if (readButtonEnabled != null) {
            PropertyConnector.connect(readButtonEnabled, "value", readButton, "enabled");
        }
        PropertyConnector.connect(writeButtonEnabled, "value", writeButton, "enabled");
    }

    protected abstract void triggerValidation();

    protected ValidationResultModel getValidationResultModel() {
        return null;
    }

    @Override
    public void addLogText(String logLine, Object... args) {
        LOGGER.info("Add text to loggerArea, logLine: {}, args: {}", logLine, args);

        if (args != null) {
            logLine = MessageFormatter.arrayFormat(logLine, args).getMessage();
        }
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SSS - ");
        final StringBuffer message = new StringBuffer(sdf.format(new Date()));
        message.append(logLine).append(NEWLINE);

        if (SwingUtilities.isEventDispatchThread()) {

            loggerArea.append(message.toString());
            loggerArea.setCaretPosition(loggerArea.getDocument().getLength());
        }
        else {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    loggerArea.append(message.toString());
                    loggerArea.setCaretPosition(loggerArea.getDocument().getLength());
                }
            });
        }
    }

    public void clearLogArea() {
        if (loggerArea != null) {
            loggerArea.setText(null);
        }
    }

    protected abstract Object getCurrentOperation();

    @Override
    public void signalPomProgStateChanged(PomProgState pomProgState) {

        if (!isActive()) {
            LOGGER.info("Do not process result because this is not the active tab.");
            return;
        }

        switch (pomProgState) {
        // finished state
            case POM_PROG_OKAY:
                currentOperationLabel.setIcon(progOperationSuccessfulIcon);
                addLogText("Prog operation passed: " + getCurrentOperation());
                // enable the input elements
                enableInputElements();
                break;
            case POM_PROG_STOPPED:
                // case PROG_NO_LOCO:
            case POM_PROG_NO_ANSWER:
                // case PROG_SHORT:
                // case PROG_VERIFY_FAILED:
                currentOperationLabel.setIcon(progOperationErrorIcon);
                addLogText("Prog operation failed: {}", pomProgState.name());
                // enable the input elements
                enableInputElements();
                break;
            // pending
            case POM_PROG_START:
                currentOperationLabel.setIcon(progOperationUnknownIcon);
                addLogText("Prog operation started: " + getCurrentOperation());
                // disableInputElements();
                break;
            // pending
            case POM_PROG_RUNNING:
                currentOperationLabel.setIcon(progOperationWaitIcon);
                addLogText("Prog operation is running ...");
                break;
        }
    }

    protected void sendRequest(AddressData decoderAddress, PomOperation operation, int cvNumber, int cvValue) {

        // send to bidib
        for (PomRequestListener listener : getPomRequestListeners()) {
            listener.sendRequest(this, decoderAddress, operation, cvNumber, cvValue);
        }
    }

    protected void disableInputElements() {
        LOGGER.info("Disable input elements.");
        if (readButtonEnabled != null) {
            readButtonEnabled.setValue(false);
        }
        if (writeButtonEnabled != null) {
            writeButtonEnabled.setValue(false);
        }
    }

    protected void enableInputElements() {
        LOGGER.info("Enable input elements.");
    }

    protected void fireNextCommand() {
        // disable the input elements
        disableInputElements();

        List<PomOperationCommand<? extends ProgCommandAwareBeanModel>> progCommands =
            progCommandAwareBeanModel.getProgCommands();
        LOGGER.info("Prepared commands for addressing the decoder: {}", progCommands);

        // remove the executing command from the prog commands list ...
        PomOperationCommand<?> progCommand = progCommands.remove(0);

        progCommandAwareBeanModel.setCurrentOperation(progCommand.getPomOperation());
        switch (progCommand.getPomOperation()) {
            case WR_BIT:
            case WR_BYTE:
                progCommandAwareBeanModel.setExecution(ExecutionType.WRITE);
                break;
            default:
                progCommandAwareBeanModel.setExecution(ExecutionType.READ);
                break;
        }
        progCommandAwareBeanModel.setExecutingProgCommand(progCommand);

        addLogText("Prog write request: {}", progCommand);

        // send to bidib
        sendRequest(progCommand.getDecoderAddress(), progCommand.getPomOperation(), progCommand.getCvNumber(),
            progCommand.getCvValue());
    }

    protected void startTimeoutControl(int timeout) {
        LOGGER.info("Timeout control is started, timeout: {}.");

        synchronized (readTimeoutLock) {
            if (readTimeout != null) {
                LOGGER.info("The timeout control is already assigned: {}", readTimeout);
                if (readTimeout.isRunning()) {
                    LOGGER.info("The timeout control is already running and will be stopped.");
                    readTimeout.stop();
                }
                readTimeout = null;
            }

            readTimeout = new Timer(timeout, new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {

                    LOGGER.info("Timeout control has expired.");
                    addLogText("Timeout control has expired. No answer from decoder received.");

                    // TODO check if we have more commands
                    if (CollectionUtils.hasElements(progCommandAwareBeanModel.getProgCommands())) {
                        LOGGER.info("Prepare the next command to send.");
                        startTimeoutControl(DEFAULT_TIMEOUT);
                        fireNextCommand();
                    }
                    else if (progCommandAwareBeanModel.getExecutingProgCommand() instanceof PomOperationIfElseCommand) {
                        boolean ignoreError = false;

                        PomOperationCommand<M> executingProgCommand =
                            (PomOperationCommand<M>) progCommandAwareBeanModel.getExecutingProgCommand();
                        LOGGER.info("The executingProgCommand: {}", executingProgCommand);

                        if (executingProgCommand instanceof PomOperationIfElseCommand) {
                            PomOperationIfElseCommand<M> ifElseCommand =
                                (PomOperationIfElseCommand<M>) executingProgCommand;
                            LOGGER.info("The executing command is a if-else-command: {}", ifElseCommand);

                            List<PomOperationCommand<M>> failureCommands = ifElseCommand.getProgCommandsFailure();
                            if (CollectionUtils.hasElements(failureCommands)) {
                                LOGGER.info("Found failure commands to be executed: {}", failureCommands);

                                // TODO make this more generic, e.g. add a marker interface ...
                                if (failureCommands.get(0) instanceof PomOperationContinueAfterTimeoutCommand) {
                                    LOGGER
                                        .info("The first failure command is a PomOperationContinueAfterTimeoutCommand. This is no error, we can proceed.");

                                    // add the new commands to process
                                    List<PomOperationCommand<? extends ProgCommandAwareBeanModel>> progCommands =
                                        progCommandAwareBeanModel.getProgCommands();
                                    progCommands.clear();
                                    progCommands.addAll(failureCommands);

                                    // ignore the error
                                    ignoreError = true;
                                }
                            }
                        }
                        if (!ignoreError) {
                            LOGGER.info("No more commands to send, enable the input elements.");

                            // show an error and enable input elements
                            cvProgrammerModel.setPomProgState(PomProgState.POM_PROG_NO_ANSWER);
                        }
                        else {
                            LOGGER.info("Start the timeout control and fire the next command.");
                            startTimeoutControl(DEFAULT_TIMEOUT);
                            fireNextCommand();
                        }
                    }
                    else {
                        LOGGER.info("No more commands to send, enable the input elements.");
                        // show an error and enable input elements
                        cvProgrammerModel.setPomProgState(PomProgState.POM_PROG_NO_ANSWER);
                    }
                }
            });
            readTimeout.setRepeats(false);
            readTimeout.start();
        }
    }

    protected void stopTimeoutControl() {
        LOGGER.info("Timeout control is stopped.");

        synchronized (readTimeoutLock) {
            if (readTimeout != null) {
                LOGGER.info("The timeout control is assigned and will be stopped: {}", readTimeout);
                readTimeout.stop();

                readTimeout = null;
            }
        }
    }
}
