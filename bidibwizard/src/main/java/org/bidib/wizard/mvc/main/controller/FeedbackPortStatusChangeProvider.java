package org.bidib.wizard.mvc.main.controller;

import org.bidib.wizard.comm.FeedbackPortStatus;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.model.listener.FeedbackPortListener;
import org.bidib.wizard.mvc.main.model.listener.PortListListener;

public interface FeedbackPortStatusChangeProvider {

    /**
     * Add a feedback port listener.
     * 
     * @param feedbackPortListener
     *            the feedback port listener to add
     */
    void addFeedbackPortListener(final FeedbackPortListener<FeedbackPortStatus> feedbackPortListener);

    /**
     * Remove a feedback port listener.
     * 
     * @param feedbackPortListener
     *            the feedback port listener to remove
     */
    void removeFeedbackPortListener(final FeedbackPortListener<FeedbackPortStatus> feedbackPortListener);

    /**
     * Add a port list listener.
     * 
     * @param portListListener
     *            the port list listener to add
     */
    void addPortListListener(PortListListener portListListener);

    /**
     * Remove a port list listener.
     * 
     * @param portListListener
     *            the port list listener to remove
     */
    void removePortListListener(PortListListener portListListener);

    /**
     * @return the selectedNode
     */
    Node getSelectedNode();
}
