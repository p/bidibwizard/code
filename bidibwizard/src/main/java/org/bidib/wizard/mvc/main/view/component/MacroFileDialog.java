package org.bidib.wizard.mvc.main.view.component;

import java.awt.Component;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.commons.lang3.StringUtils;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.dialog.FileDialog;
import org.bidib.wizard.mvc.main.model.Macro;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class MacroFileDialog extends FileDialog {
    private static final Logger LOGGER = LoggerFactory.getLogger(MacroFileDialog.class);

    private final String MACRO_EXTENSION_LEGACY = "macro";

    private final String MACRO_EXTENSION = "mxml";

    private JCheckBox checkUseLegacyFormat;

    private FileFilter macroFilter;

    private FileFilter macroFilterLegacy;

    private String macroDescription;

    private String macroDescriptionLegacy;

    private Macro macro;

    public MacroFileDialog(Component parent, int dialogType, final Macro macro) {
        super(parent, dialogType, null, null);
        this.macro = macro;

        macroDescription = Resources.getString(MacroFileDialog.class, "macroDescription");
        macroDescriptionLegacy = Resources.getString(MacroFileDialog.class, "macroDescriptionLegacy");
        macroFilter = new FileNameExtensionFilter(macroDescription, MACRO_EXTENSION);
        macroFilterLegacy = new FileNameExtensionFilter(macroDescriptionLegacy, MACRO_EXTENSION_LEGACY);

        String fileName = null;
        if (macro != null) {
            // must make a valid file name
            fileName = macro.toString();

            // replace invalid characters
            fileName = fileName.replaceAll("[^a-zA-Z0-9.+-]", "_");
        }

        if (StringUtils.isNotBlank(fileName)) {
            fileName = fileName + "." + MACRO_EXTENSION;
            LOGGER.info("Update the file filter for macro extension, fileName: {}", fileName);
        }
        if (FileDialog.SAVE != dialogType) {
            // import macro
            updateFileFilter(new FileFilter[] { macroFilter, macroFilterLegacy }, fileName);
        }
        else {
            updateFileFilter(new FileFilter[] { macroFilter }, fileName);
        }
    }

    protected JCheckBox getCheckUseLegacyFormat() {
        return checkUseLegacyFormat;
    }

    @Override
    protected Component getAdditionalPanel() {
        // prepare a panel with checkboxes for loading macro content before export
        JPanel additionalPanel = new JPanel();
        additionalPanel.setLayout(new BoxLayout(additionalPanel, BoxLayout.PAGE_AXIS));

        if (FileDialog.SAVE != dialogType) {
            // import macro
            final FileDialog fileDialog = this;
            checkUseLegacyFormat =
                new JCheckBox(Resources.getString(MacroFileDialog.class, "checkUseLegacyFormat"), false);
            checkUseLegacyFormat.addItemListener(new ItemListener() {

                @Override
                public void itemStateChanged(ItemEvent e) {
                    String fileName = null;
                    FileFilter accessoryFileFilter = null;
                    switch (e.getStateChange()) {
                        case ItemEvent.SELECTED:
                            LOGGER.info("checkUseLegacyFormat is selected.");
                            accessoryFileFilter = macroFilterLegacy;
                            if (macro != null) {
                                fileName = macro.toString() + "." + MACRO_EXTENSION_LEGACY;
                            }
                            break;
                        default:
                            LOGGER.info("checkUseLegacyFormat is unselected.");
                            accessoryFileFilter = macroFilter;
                            if (macro != null) {
                                fileName = macro.toString() + "." + MACRO_EXTENSION;
                            }
                            break;
                    }
                    fileDialog.updateFileFilter(accessoryFileFilter, fileName);
                }
            });
            additionalPanel.add(checkUseLegacyFormat);
        }
        else {
            LOGGER.info("Save dialog does no longer support legacy format!");
        }
        return additionalPanel;
    }
}
