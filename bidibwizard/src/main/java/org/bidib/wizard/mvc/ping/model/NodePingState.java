package org.bidib.wizard.mvc.ping.model;

public enum NodePingState {
    OFF, ON;
}
