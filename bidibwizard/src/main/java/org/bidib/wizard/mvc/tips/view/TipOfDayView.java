package org.bidib.wizard.mvc.tips.view;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.URL;
import java.util.ResourceBundle;

import javax.swing.AbstractAction;
import javax.swing.JCheckBox;
import javax.swing.JDialog;

import org.bidib.wizard.mvc.preferences.model.Preferences;
import org.bidib.wizard.mvc.tips.controller.TipOfDayClosedListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jidesoft.swing.JideSwingUtilities;
import com.jidesoft.tipoftheday.ResourceBundleTipOfTheDaySource;
import com.jidesoft.tipoftheday.TipOfTheDayDialog;

public class TipOfDayView {

    private static final Logger LOGGER = LoggerFactory.getLogger(TipOfDayView.class);

    public void showTipsOfTheDay(final TipOfDayClosedListener listener) {
        final ResourceBundleTipOfTheDaySource tipOfTheDaySource =
            new ResourceBundleTipOfTheDaySource(ResourceBundle.getBundle("tips/tips"));
        tipOfTheDaySource.setCurrentTipIndex(getLastTipOfDayIndex());
        URL styleSheet = TipOfTheDayDialog.class.getResource("/tips.css");
        TipOfTheDayDialog dialog =
            new TipOfTheDayDialog((Frame) null, tipOfTheDaySource, new AbstractAction("Show Tips on startup") {
                private static final long serialVersionUID = 3919739150082321631L;

                public void actionPerformed(ActionEvent e) {
                    if (e.getSource() instanceof JCheckBox) {
                        JCheckBox checkBox = (JCheckBox) e.getSource();
                        setPrefBooleanValue(checkBox.isSelected());
                    }
                    // change your user preference
                }
            }, styleSheet);

        dialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                LOGGER.info("Window was closed.");
                super.windowClosed(e);

                int currentTipIndex = tipOfTheDaySource.getCurrentTipIndex();
                if (currentTipIndex > -1) {
                    LOGGER.info("Save the currentTipIndex: {}", currentTipIndex);
                    setLastTipOfDayIndex(currentTipIndex);
                }

                if (listener != null) {
                    listener.closed();
                }
            }
        });

        dialog.setShowTooltip(getPrefBooleanValue());
        dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        dialog.setResizable(false);
        dialog.pack();
        JideSwingUtilities.globalCenterWindow(dialog);
        dialog.setVisible(true);
    }

    private boolean getPrefBooleanValue() {
        Preferences prefs = Preferences.getInstance();
        return prefs.isShowTipOfDay();
    }

    private void setPrefBooleanValue(boolean showTipOfDay) {
        Preferences prefs = Preferences.getInstance();
        prefs.setShowTipOfDay(showTipOfDay);
    }

    private int getLastTipOfDayIndex() {
        Preferences prefs = Preferences.getInstance();
        return prefs.getLastTipOfDayIndex();
    }

    private void setLastTipOfDayIndex(int currentTipIndex) {
        Preferences prefs = Preferences.getInstance();
        prefs.setLastTipOfDayIndex(currentTipIndex);
        prefs.save(null);
    }
}
