package org.bidib.wizard.mvc.main.model;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.util.LinkedHashMap;
import java.util.Map;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.LightPortEnum;
import org.bidib.jbidibc.core.port.BytePortConfigValue;
import org.bidib.jbidibc.core.port.Int16PortConfigValue;
import org.bidib.jbidibc.core.port.PortConfigValue;
import org.bidib.jbidibc.core.port.RgbPortConfigValue;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.wizard.comm.LightPortStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LightPort extends Port<LightPortStatus> implements ConfigurablePort<LightPortStatus> {
    private static final Logger LOGGER = LoggerFactory.getLogger(LightPort.class);

    private static final long serialVersionUID = 1L;

    static {
        try {
            // Q: why is this needed? A: export of beans with XMLDecoder
            PropertyDescriptor[] descriptor = Introspector.getBeanInfo(LightPort.class).getPropertyDescriptors();

            for (int i = 0; i < descriptor.length; i++) {
                PropertyDescriptor propertyDescriptor = descriptor[i];
                if (propertyDescriptor.getName().equals("status")) {
                    propertyDescriptor.setValue("transient", Boolean.TRUE);
                }
            }
        }
        catch (IntrospectionException e) {
            throw new RuntimeException(e);
        }
    }

    public static final String PROPERTY_DIM_MAX = "dimMax";

    public static final String PROPERTY_DIM_MIN = "dimMin";

    public static final String PROPERTY_DIM_STRETCH_MAX = "dimStretchMax";

    public static final String PROPERTY_DIM_STRETCH_MIN = "dimStretchMin";

    public static final String PROPERTY_PWM_MAX = "pwmMax";

    public static final String PROPERTY_PWM_MIN = "pwmMin";

    public static final String PROPERTY_RGB_VALUE = "rgbValue";

    public static final String PROPERTY_TRANSITION_TIME = "transitionTime";

    public static final int DIMM_STRETCHMAX8_8 = 65535;

    public static final int DIMM_STRETCHMAX8 = 255;

    private int dimMax;

    private int dimStretchMax = DIMM_STRETCHMAX8;

    private int dimMin;

    private int dimStretchMin = DIMM_STRETCHMAX8;

    private int pwmMax;

    private int pwmMin;

    private Integer rgbValue;

    private Integer transitionTime;

    public static final LightPort NONE = new LightPort.Builder(-1).setLabel("<none>").build();

    public LightPort() {
        super(null);
        setStatus(LightPortStatus.UNKNOWN);
    }

    public LightPort(GenericPort genericPort) {
        super(genericPort);
    }

    private LightPort(Builder builder) {
        this();
        setId(builder.id);
        setLabel(builder.label);
    }

    public int getDimMax() {

        if (genericPort != null) {
            // get the dimm up value
            Number dimMax = null;
            if (isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_DIMM_UP_8_8)) {
                dimMax = genericPort.getPortConfigValue(BidibLibrary.BIDIB_PCFG_DIMM_UP_8_8);
            }
            else if (isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_DIMM_UP)) {
                dimMax = genericPort.getPortConfigValue(BidibLibrary.BIDIB_PCFG_DIMM_UP);
            }
            else {
                LOGGER.warn("No value received from generic port for BIDIB_PCFG_DIMM_UP!");
            }
            if (dimMax != null) {
                if (dimMax instanceof Byte) {
                    this.dimMax = ByteUtils.getInt(dimMax.byteValue());
                }
                else {
                    this.dimMax = dimMax.intValue();
                }
            }
        }
        return dimMax;
    }

    public void setDimMax(int dimMax) {
        int oldValue = getDimMax();
        LOGGER.info("Set new dimMax: {}, oldValue: {}", dimMax, oldValue);
        this.dimMax = dimMax;
        if (genericPort != null) {

            if (isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_DIMM_UP_8_8)) {
                genericPort.setPortConfigValue(BidibLibrary.BIDIB_PCFG_DIMM_UP_8_8,
                    new BytePortConfigValue(ByteUtils.getLowByte(dimMax)));
            }
            else {
                genericPort.setPortConfigValue(BidibLibrary.BIDIB_PCFG_DIMM_UP,
                    new BytePortConfigValue(ByteUtils.getLowByte(dimMax)));
            }
        }

        firePropertyChange(PROPERTY_DIM_MAX, oldValue, this.dimMax);
    }

    private void setDimMax8_8(int dimMax) {
        int oldValue = getDimMax();
        LOGGER.info("Set new dimMax8_8: {}, oldValue: {}", dimMax, oldValue);
        this.dimMax = dimMax;
        if (genericPort != null) {
            genericPort.setPortConfigValue(BidibLibrary.BIDIB_PCFG_DIMM_UP_8_8, new Int16PortConfigValue(dimMax));
        }

        firePropertyChange(PROPERTY_DIM_MAX, oldValue, this.dimMax);
    }

    public int getDimMin() {
        if (genericPort != null) {
            // get the dimm down value
            Number dimMin = null;
            if (isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_DIMM_DOWN_8_8)) {
                dimMin = genericPort.getPortConfigValue(BidibLibrary.BIDIB_PCFG_DIMM_DOWN_8_8);
            }
            else if (isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_DIMM_DOWN)) {
                dimMin = genericPort.getPortConfigValue(BidibLibrary.BIDIB_PCFG_DIMM_DOWN);
            }
            else {
                LOGGER.warn("No value received from generic port for BIDIB_PCFG_DIMM_DOWN!");
            }
            if (dimMin != null) {
                if (dimMin instanceof Byte) {
                    this.dimMin = ByteUtils.getInt(dimMin.byteValue());
                }
                else {
                    this.dimMin = dimMin.intValue();
                }
            }
        }
        return dimMin;
    }

    public void setDimMin(int dimMin) {
        int oldValue = getDimMin();
        LOGGER.trace("Set new dimMin: {}, oldValue: {}", dimMin, oldValue);
        this.dimMin = dimMin;
        if (genericPort != null) {
            genericPort.setPortConfigValue(BidibLibrary.BIDIB_PCFG_DIMM_DOWN,
                new BytePortConfigValue(ByteUtils.getLowByte(dimMin)));
        }

        firePropertyChange(PROPERTY_DIM_MIN, oldValue, this.dimMin);
    }

    private void setDimMin8_8(int dimMin) {
        int oldValue = getDimMin();
        LOGGER.trace("Set new dimMin: {}, oldValue: {}", dimMin, oldValue);
        this.dimMin = dimMin;
        if (genericPort != null) {
            genericPort.setPortConfigValue(BidibLibrary.BIDIB_PCFG_DIMM_DOWN_8_8, new Int16PortConfigValue(dimMin));
        }

        firePropertyChange(PROPERTY_DIM_MIN, oldValue, this.dimMin);
    }

    /**
     * @return the dimStretchMax
     */
    public int getDimStretchMax() {
        return dimStretchMax;
    }

    /**
     * @param dimStretchMax
     *            the dimStretchMax to set
     */
    public void setDimStretchMax(int dimStretchMax) {
        int oldValue = this.dimStretchMax;
        this.dimStretchMax = dimStretchMax;
        firePropertyChange(PROPERTY_DIM_STRETCH_MAX, oldValue, this.dimStretchMax);
    }

    /**
     * @return the dimStretchMin
     */
    public int getDimStretchMin() {
        return dimStretchMin;
    }

    /**
     * @param dimStretchMin
     *            the dimStretchMin to set
     */
    public void setDimStretchMin(int dimStretchMin) {
        int oldValue = this.dimStretchMin;
        this.dimStretchMin = dimStretchMin;
        firePropertyChange(PROPERTY_DIM_STRETCH_MIN, oldValue, this.dimStretchMin);
    }

    public int getPwmMax() {
        if (genericPort != null) {
            Number pwmMax = genericPort.getPortConfigValue(BidibLibrary.BIDIB_PCFG_LEVEL_PORT_ON);
            if (pwmMax != null) {
                this.pwmMax = ByteUtils.getInt(pwmMax.byteValue());
            }
            else {
                LOGGER.warn("No value received from generic for BIDIB_PCFG_LEVEL_PORT_ON!");
            }
        }
        return pwmMax;
    }

    public void setPwmMax(int pwmMax) {
        int oldValue = getPwmMax();
        this.pwmMax = pwmMax;
        if (genericPort != null) {
            genericPort.setPortConfigValue(BidibLibrary.BIDIB_PCFG_LEVEL_PORT_ON,
                new BytePortConfigValue(ByteUtils.getLowByte(pwmMax)));
        }

        firePropertyChange(PROPERTY_PWM_MAX, oldValue, this.pwmMax);
    }

    public int getPwmMin() {
        if (genericPort != null) {
            Number pwmMin = genericPort.getPortConfigValue(BidibLibrary.BIDIB_PCFG_LEVEL_PORT_OFF);
            if (pwmMin != null) {
                this.pwmMin = ByteUtils.getInt(pwmMin.byteValue());
            }
            else {
                LOGGER.warn("No value received from generic for BIDIB_PCFG_LEVEL_PORT_OFF!");
            }
        }
        return pwmMin;
    }

    public void setPwmMin(int pwmMin) {
        int oldValue = getPwmMin();
        this.pwmMin = pwmMin;
        if (genericPort != null) {
            genericPort.setPortConfigValue(BidibLibrary.BIDIB_PCFG_LEVEL_PORT_OFF,
                new BytePortConfigValue(ByteUtils.getLowByte(pwmMin)));
        }

        firePropertyChange(PROPERTY_PWM_MIN, oldValue, this.pwmMin);
    }

    /**
     * @return the rgbValue
     */
    public Integer getRgbValue() {
        if (genericPort != null) {
            Number rgbValue = genericPort.getPortConfigValue(BidibLibrary.BIDIB_PCFG_RGB);
            if (rgbValue != null) {
                this.rgbValue = rgbValue.intValue();
            }
            else {
                LOGGER.warn("No value received from generic for BIDIB_PCFG_RGB!");
            }
        }
        return rgbValue;
    }

    /**
     * @param rgbValue
     *            the rgbValue to set
     */
    public void setRgbValue(Integer rgbValue) {
        Integer oldValue = this.rgbValue;
        this.rgbValue = rgbValue;
        if (genericPort != null) {
            genericPort.setPortConfigValue(BidibLibrary.BIDIB_PCFG_RGB, new RgbPortConfigValue(rgbValue));
        }

        firePropertyChange(PROPERTY_RGB_VALUE, oldValue, this.rgbValue);
    }

    /**
     * @return the transitionTime
     */
    public Integer getTransitionTime() {
        if (genericPort != null) {
            Number transitionTime = genericPort.getPortConfigValue(BidibLibrary.BIDIB_PCFG_TRANSITION_TIME);
            if (transitionTime != null) {
                this.transitionTime = transitionTime.intValue();
            }
            else {
                LOGGER.warn("No value received from generic for BIDIB_PCFG_TRANSITION_TIME!");
            }
        }
        return transitionTime;
    }

    /**
     * @param transitionTime
     *            the transitionTime to set
     */
    public void setTransitionTime(Integer transitionTime) {
        Integer oldValue = this.transitionTime;
        this.transitionTime = transitionTime;
        if (genericPort != null) {
            genericPort.setPortConfigValue(BidibLibrary.BIDIB_PCFG_TRANSITION_TIME,
                new Int16PortConfigValue(transitionTime));
        }

        firePropertyChange(PROPERTY_TRANSITION_TIME, oldValue, this.transitionTime);
    }

    public String getDebugString() {
        return getClass().getSimpleName() + "[pwmMin=" + pwmMin + ",pwmMax=" + pwmMax + ",dimMin=" + dimMin + ",dimMax="
            + dimMax + ",dimStretchMax=" + dimStretchMax + ",dimStretchMin=" + dimStretchMin + ",rgbValue="
            + (rgbValue != null ? rgbValue : "<null>") + ",status=" + getStatus() + "]";
    }

    @Override
    public byte[] getPortConfig() {
        return new byte[] { ByteUtils.getLowByte(pwmMin), ByteUtils.getLowByte(pwmMax), ByteUtils.getLowByte(dimMin),
            ByteUtils.getLowByte(dimMax) };
    }

    public void setPortConfig(byte[] portConfig) {
        LOGGER.info("Set the light port parameters: {}", ByteUtils.bytesToHex(portConfig));

        setPortConfigEnabled(true);
        setPwmMin(ByteUtils.getInt(portConfig[0]));
        getKnownPortConfigKeys().add(Byte.valueOf(BidibLibrary.BIDIB_PCFG_LEVEL_PORT_OFF));
        setPwmMax(ByteUtils.getInt(portConfig[1]));
        getKnownPortConfigKeys().add(Byte.valueOf(BidibLibrary.BIDIB_PCFG_LEVEL_PORT_ON));
        setDimMin(ByteUtils.getInt(portConfig[2]));
        getKnownPortConfigKeys().add(Byte.valueOf(BidibLibrary.BIDIB_PCFG_DIMM_DOWN_8_8));
        setDimMax(ByteUtils.getInt(portConfig[3]));
        getKnownPortConfigKeys().add(Byte.valueOf(BidibLibrary.BIDIB_PCFG_DIMM_UP_8_8));

        // set the rgb value is not available
        setDimStretchMin(DIMM_STRETCHMAX8);
        setDimStretchMax(DIMM_STRETCHMAX8);
    }

    @Override
    public void setPortConfigX(Map<Byte, PortConfigValue<?>> portConfig) {
        LOGGER.info("Set the port config for LightPort: {}", portConfig);

        Number pwmMax = getPortConfigValue(BidibLibrary.BIDIB_PCFG_LEVEL_PORT_ON, portConfig);
        if (pwmMax != null) {
            setPwmMax(ByteUtils.getInt(pwmMax.byteValue()));
        }
        else {
            LOGGER.warn("No value received for BIDIB_PCFG_LEVEL_PORT_ON!");
            // setPwmMax(0);
        }

        Number pwmMin = getPortConfigValue(BidibLibrary.BIDIB_PCFG_LEVEL_PORT_OFF, portConfig);
        if (pwmMin != null) {
            setPwmMin(ByteUtils.getInt(pwmMin.byteValue()));
        }
        else {
            LOGGER.warn("No value received for BIDIB_PCFG_LEVEL_PORT_OFF!");
            // setPwmMin(0);
        }
        Number dimMax = getPortConfigValue(BidibLibrary.BIDIB_PCFG_DIMM_UP_8_8, portConfig);
        if (dimMax != null) {
            setDimMax8_8(ByteUtils.getWORD(dimMax.intValue()));
            setDimStretchMax(DIMM_STRETCHMAX8_8);
        }
        else {
            dimMax = getPortConfigValue(BidibLibrary.BIDIB_PCFG_DIMM_UP, portConfig);
            if (dimMax != null) {
                setDimMax(ByteUtils.getInt(dimMax.byteValue()));
                setDimStretchMax(DIMM_STRETCHMAX8);
            }
            else {
                LOGGER.warn("No value received for BIDIB_PCFG_DIMM_UP or BIDIB_PCFG_DIMM_UP_8_8!");
                // setDimMax(0);
                setDimStretchMax(DIMM_STRETCHMAX8);
            }
        }
        Number dimMin = getPortConfigValue(BidibLibrary.BIDIB_PCFG_DIMM_DOWN_8_8, portConfig);
        if (dimMin != null) {
            setDimMin8_8(ByteUtils.getWORD(dimMin.intValue()));
            setDimStretchMin(DIMM_STRETCHMAX8_8);
        }
        else {
            dimMin = getPortConfigValue(BidibLibrary.BIDIB_PCFG_DIMM_DOWN, portConfig);
            if (dimMin != null) {
                setDimMin(ByteUtils.getInt(dimMin.byteValue()));
                setDimStretchMin(DIMM_STRETCHMAX8);
            }
            else {
                LOGGER.warn("No value received for BIDIB_PCFG_DIMM_DOWN or BIDIB_PCFG_DIMM_DOWN_8_8!");
                // setDimMin(0);
                setDimStretchMin(DIMM_STRETCHMAX8);
            }
        }
        Number rgbValue = getPortConfigValue(BidibLibrary.BIDIB_PCFG_RGB, portConfig);
        if (rgbValue != null) {
            setRgbValue(ByteUtils.getRGB(rgbValue.intValue()));
        }
        else {
            // setRgbValue(null);
            LOGGER.info("No value received for BIDIB_PCFG_RGB!");
        }

        Number transitionTime = getPortConfigValue(BidibLibrary.BIDIB_PCFG_TRANSITION_TIME, portConfig);
        if (transitionTime != null) {
            setTransitionTime(transitionTime.intValue());
        }
        else {
            // setTransitionTime(null);
            LOGGER.info("No value received for BIDIB_PCFG_TRANSITION_TIME!");
        }

        // call the super class
        super.setPortConfigX(portConfig);
    }

    @Override
    public Map<Byte, PortConfigValue<?>> getPortConfigX() {

        if (genericPort != null) {
            return genericPort.getPortConfig();
        }

        Map<Byte, PortConfigValue<?>> portConfigX = new LinkedHashMap<Byte, PortConfigValue<?>>();

        // add the dimm up value
        if (isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_DIMM_UP_8_8)) {
            portConfigX.put(BidibLibrary.BIDIB_PCFG_DIMM_UP_8_8, new Int16PortConfigValue(dimMax));
        }
        else if (isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_DIMM_UP)) {
            portConfigX.put(BidibLibrary.BIDIB_PCFG_DIMM_UP, new BytePortConfigValue(ByteUtils.getLowByte(dimMax)));
        }
        else {
            LOGGER.warn("No DIMM_UP config available.");
        }

        // add the dimm down value
        if (isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_DIMM_DOWN_8_8)) {
            portConfigX.put(BidibLibrary.BIDIB_PCFG_DIMM_DOWN_8_8, new Int16PortConfigValue(dimMin));
        }
        else if (isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_DIMM_DOWN)) {
            portConfigX.put(BidibLibrary.BIDIB_PCFG_DIMM_DOWN, new BytePortConfigValue(ByteUtils.getLowByte(dimMin)));
        }
        else {
            LOGGER.warn("No DIMM_DOWN config available.");
        }

        // add the port on
        if (isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_LEVEL_PORT_ON)) {
            portConfigX.put(BidibLibrary.BIDIB_PCFG_LEVEL_PORT_ON,
                new BytePortConfigValue(ByteUtils.getLowByte(pwmMax)));
        }

        // add the port off
        if (isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_LEVEL_PORT_OFF)) {
            portConfigX.put(BidibLibrary.BIDIB_PCFG_LEVEL_PORT_OFF,
                new BytePortConfigValue(ByteUtils.getLowByte(pwmMin)));
        }

        if (isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_RGB) && getRgbValue() != null) {
            portConfigX.put(BidibLibrary.BIDIB_PCFG_RGB, new RgbPortConfigValue(getRgbValue()));
        }

        if (isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_TRANSITION_TIME) && getTransitionTime() != null) {
            portConfigX.put(BidibLibrary.BIDIB_PCFG_TRANSITION_TIME, new Int16PortConfigValue(getTransitionTime()));
        }

        return portConfigX;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof LightPort) {
            return ((LightPort) obj).getId() == getId();
        }
        return false;
    }

    @Override
    public int hashCode() {
        return getId();
    }

    @Override
    protected LcOutputType getPortType() {
        return LcOutputType.LIGHTPORT;
    }

    @Override
    protected LightPortStatus internalGetStatus() {
        return LightPortStatus.valueOf(LightPortEnum.valueOf(genericPort.getPortStatus()));
    }

    public static class Builder {
        private final int id;

        private String label;

        public Builder(int id) {
            this.id = id;
        }

        public Builder setLabel(String label) {
            this.label = label;
            return this;
        }

        public LightPort build() {
            return new LightPort(this);
        }
    }
}
