package org.bidib.wizard.mvc.main.view.panel.listener;

public interface TabVisibilityProvider {

    /**
     * @return the tab visible state.
     */
    boolean isTabVisible();
}
