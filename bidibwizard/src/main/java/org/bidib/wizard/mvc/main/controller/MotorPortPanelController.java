package org.bidib.wizard.mvc.main.controller;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.JOptionPane;

import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.PortConfigKeys;
import org.bidib.jbidibc.core.exception.InvalidConfigurationException;
import org.bidib.jbidibc.core.port.PortConfigValue;
import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.comm.CommunicationFactory;
import org.bidib.wizard.comm.MotorPortStatus;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.labels.Labels;
import org.bidib.wizard.labels.MotorPortLabelFactory;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.bidib.wizard.mvc.main.controller.wrapper.NodePortWrapper;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.MotorPort;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.model.Port;
import org.bidib.wizard.mvc.main.model.listener.DefaultNodeListListener;
import org.bidib.wizard.mvc.main.model.listener.MotorPortListener;
import org.bidib.wizard.mvc.main.view.panel.MotorPortListPanel;
import org.bidib.wizard.mvc.script.view.ScriptParser;
import org.bidib.wizard.script.ScriptCommand;
import org.bidib.wizard.script.engine.ScriptEngine;
import org.bidib.wizard.script.switching.MotorPortCommand;
import org.bidib.wizard.script.switching.WaitCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MotorPortPanelController implements PortScripting {

    private static final Logger LOGGER = LoggerFactory.getLogger(MotorPortPanelController.class);

    private final MainModel mainModel;

    private final Map<Node, NodePortWrapper> testToggleRegistry = new LinkedHashMap<>();

    public MotorPortPanelController(final MainModel mainModel) {
        this.mainModel = mainModel;
    }

    public MotorPortListPanel createPanel() {
        final Labels motorPortLabels =
            DefaultApplicationContext.getInstance().get(DefaultApplicationContext.KEY_MOTORPORT_LABELS, Labels.class);

        final MotorPortListPanel motorPortListPanel = new MotorPortListPanel(this, mainModel);

        motorPortListPanel.addPortListener(new MotorPortListener<MotorPortStatus>() {
            @Override
            public void labelChanged(Port<MotorPortStatus> port, String label) {
                port.setLabel(label);

                try {
                    MotorPortLabelFactory factory = new MotorPortLabelFactory();
                    long uniqueId = mainModel.getSelectedNode().getNode().getUniqueId();
                    factory.replaceLabel(motorPortLabels, uniqueId, port.getId(), label);

                    factory.saveLabels(uniqueId, motorPortLabels);
                }
                catch (InvalidConfigurationException ex) {
                    LOGGER.warn("Save motor port labels failed.", ex);

                    String labelPath = ex.getReason();
                    JOptionPane.showMessageDialog(JOptionPane.getFrameForComponent(null),
                        Resources.getString(Labels.class, "labelfileerror.message", new Object[] { labelPath }),
                        Resources.getString(Labels.class, "labelfileerror.title"), JOptionPane.ERROR_MESSAGE);
                }

                motorPortListPanel.repaint();
            }

            @Override
            public void statusChanged(Port<MotorPortStatus> port, MotorPortStatus status) {
                LOGGER.info("Status of motor port has changed, port: {}", port);
            }

            @Override
            public void valuesChanged(MotorPort port, PortConfigKeys... portConfigKeys) {

                LOGGER.info("The port value are changed for port: {}", port);

                Map<Byte, PortConfigValue<?>> values = new LinkedHashMap<>();

                for (PortConfigKeys key : portConfigKeys) {
                    switch (key) {
                        default:
                            LOGGER.warn("Unsupported port config key detected: {}", key);
                            break;
                    }
                }

                // don't set the port type param to not send BIDIB_PCFG_RECONFIG
                CommunicationFactory.getInstance().setPortParameters(mainModel.getSelectedNode().getNode(), port, null,
                    values);
            }

            @Override
            public void testButtonPressed(Port<MotorPortStatus> port) {
                LOGGER.info("The test button was pressed for port: {}", port);

                Node node = mainModel.getSelectedNode();
                if (MotorPortStatus.TEST != port.getStatus()) {
                    stopTestToggleTask(node, port);

                    MotorPort motorPort = (MotorPort) port;
                    CommunicationFactory.getInstance().activateMotorPort(node.getNode(), motorPort.getId(),
                        motorPort.getValue());
                }
                else {
                    addTestToggleTask(node, port);
                }
            }

            @Override
            public void configChanged(Port<MotorPortStatus> port) {
            }

            @Override
            public void changePortType(LcOutputType portType, MotorPort port) {
                LOGGER.info("The port type will change to: {}, port: {}", portType, port);

                Map<Byte, PortConfigValue<?>> values = new LinkedHashMap<>();

                CommunicationFactory.getInstance().setPortParameters(mainModel.getSelectedNode().getNode(), port,
                    portType, values);
            }
        });

        mainModel.addNodeListListener(new DefaultNodeListListener() {
            @Override
            public void nodeWillChange() {
                LOGGER.info("The selected node will change!");
                try {
                    List<Node> nodes = new LinkedList<>();
                    for (Node node : testToggleRegistry.keySet()) {
                        nodes.add(node);
                    }
                    LOGGER.info("Found nodes to stop the test toggle task: {}", nodes);
                    for (Node node : nodes) {
                        stopTestToggleTask(node, null);
                    }
                    LOGGER.info("Stop the test toggle task passed for nodes: {}", nodes);
                }
                catch (Exception ex) {
                    LOGGER.warn("Stop test toggle tasks failed.", ex);
                }
            }
        });

        return motorPortListPanel;
    }

    public void addTestToggleTask(final Node node, final Port<?> port) {
        LOGGER.info("Add test toggle task for node: {}, port: {}", node, port);

        NodePortWrapper nodePortWrapper = testToggleRegistry.remove(node);
        ScriptEngine<PortScripting> scriptEngine = null;
        if (nodePortWrapper != null) {
            scriptEngine = nodePortWrapper.removePort(port);
        }

        if (scriptEngine != null) {
            LOGGER.info("Found a node scripting engine in the registry: {}", scriptEngine);
            try {
                scriptEngine.stopScript(Long.valueOf(2000));
            }
            catch (Exception ex) {
                LOGGER.warn("Stop script failed.", ex);
            }
        }

        HashMap<String, Object> context = new LinkedHashMap<>();
        context.put(ScriptParser.KEY_SELECTED_NODE, node);
        context.put(ScriptParser.KEY_MAIN_MODEL, mainModel);

        scriptEngine = new ScriptEngine<PortScripting>(this, context);

        List<ScriptCommand<PortScripting>> scriptCommands = new LinkedList<ScriptCommand<PortScripting>>();
        MotorPortCommand<PortScripting> spc = new MotorPortCommand<>();
        spc.parse("MOTOR " + port.getId() + " 10");
        scriptCommands.add(spc);
        WaitCommand<PortScripting> wc = new WaitCommand<>();
        wc.parse("WAIT 2000");
        scriptCommands.add(wc);
        spc = new MotorPortCommand<>();
        spc.parse("MOTOR " + port.getId() + " 0");
        scriptCommands.add(spc);
        wc = new WaitCommand<>();
        wc.parse("WAIT 2000");
        scriptCommands.add(wc);

        LOGGER.info("Prepared list of commands: {}", scriptCommands);

        scriptEngine.setScriptCommands(scriptCommands);
        // repeating
        scriptEngine.setScriptRepeating(true);

        if (nodePortWrapper == null) {
            LOGGER.info("Create new NodePortWrapper for node: {}", node);
            nodePortWrapper = new NodePortWrapper(node);
        }

        LOGGER.info("Put script engine in registry for node: {}", node);
        nodePortWrapper.addPort(port, scriptEngine);

        testToggleRegistry.put(node, nodePortWrapper);

        scriptEngine.startScript();
    }

    public void stopTestToggleTask(final Node node, final Port<?> port) {
        LOGGER.info("Stop test toggle task for node: {}, port: {}", node, port);

        NodePortWrapper nodePortWrapper = testToggleRegistry.get(node);

        if (nodePortWrapper != null) {
            Set<Port<?>> toRemove = new HashSet<>();
            if (port != null) {
                toRemove.add(port);
            }
            else {
                toRemove.addAll(nodePortWrapper.getKeySet());
            }

            for (Port<?> removePort : toRemove) {
                ScriptEngine<PortScripting> engine = nodePortWrapper.removePort(removePort);

                if (engine != null) {
                    LOGGER.info("Found a node scripting engine in the registry: {}", engine);
                    try {
                        engine.stopScript(Long.valueOf(2000));
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Stop script failed.", ex);
                    }
                }
                else {
                    LOGGER.warn("No scripting engine found for node: {}", node);
                }
            }

            if (nodePortWrapper.isEmpty()) {
                LOGGER.info("No more ports registered for node: {}", node);
                testToggleRegistry.remove(node);
            }
        }
    }

    @Override
    public void sendPortStatusAction(int port, BidibStatus portStatus) {

    }

    @Override
    public void sendPortValueAction(int port, int portValue) {
        LOGGER.info("Send motor value on the port: {}, portValue: {}", port, portValue);
        try {
            Node node = mainModel.getSelectedNode();
            CommunicationFactory.getInstance().activateMotorPort(node.getNode(), port, portValue);
        }
        catch (Exception ex) {
            LOGGER.warn("Activate motor port failed.", ex);
        }
    }
}
