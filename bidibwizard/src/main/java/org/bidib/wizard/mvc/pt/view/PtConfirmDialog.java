package org.bidib.wizard.mvc.pt.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;

import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.dialog.EscapeDialog;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.preferences.model.Preferences;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.value.ValueHolder;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.builder.ButtonBarBuilder;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

public class PtConfirmDialog extends EscapeDialog {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(PtConfirmDialog.class);

    private static final String ENCODED_DIALOG_COLUMN_SPECS = "pref, 10dlu, min(300dlu;pref)";

    private int result = JOptionPane.CANCEL_OPTION;

    private ValueModel checkDoNotAskInFutureValueModel;

    public PtConfirmDialog(final Node node, boolean modal, Point itemPosition) {
        super(null, Resources.getString(PtConfirmDialog.class, "title"), modal);
        getContentPane().setLayout(new BorderLayout());

        // JPanel panel = new FormDebugPanel();
        DefaultFormBuilder builder =
            new DefaultFormBuilder(new FormLayout(ENCODED_DIALOG_COLUMN_SPECS,
                "pref, 10dlu, top:35dlu, 5dlu, pref, 5dlu, pref"));
        builder.border(Borders.DIALOG);

        JLabel iconLabel = new JLabel(UIManager.getIcon("OptionPane.warningIcon"));

        int row = 1;
        CellConstraints cc = new CellConstraints();
        builder.add(iconLabel, cc.xywh(1, row, 1, 2));

        JLabel messageLabel = new JLabel(Resources.getString(getClass(), "message-warn"));
        Font font = messageLabel.getFont();
        font = font.deriveFont(16.0f);
        messageLabel.setFont(font);
        messageLabel.setForeground(Color.RED);

        builder.add(messageLabel, cc.xy(3, row));
        row += 2;

        JLabel messageInfoLabel = new JLabel(Resources.getString(getClass(), "message"));
        builder.add(messageInfoLabel, cc.xy(3, row));
        row += 2;

        checkDoNotAskInFutureValueModel = new ValueHolder(Preferences.getInstance().isPtModeDoNotConfirmSwitch());
        if (!((Boolean) checkDoNotAskInFutureValueModel.getValue()).booleanValue()) {
            JCheckBox checkDoNotAskInFuture =
                BasicComponentFactory.createCheckBox(checkDoNotAskInFutureValueModel,
                    Resources.getString(getClass(), "do-not-ask"));
            builder.add(checkDoNotAskInFuture, cc.xy(3, row));
            row += 2;
        }
        else {
            // we do not use it
            checkDoNotAskInFutureValueModel = null;
        }

        // buttons
        JButton continueButton = new JButton(Resources.getString(getClass(), "continue"));

        continueButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                fireContinue(node);
            }
        });

        JButton cancel = new JButton(Resources.getString(getClass(), "cancel"));

        cancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                fireCancel();
            }
        });

        JPanel buttons = new ButtonBarBuilder().addGlue().addButton(continueButton, cancel).build();

        builder.add(buttons, cc.xy(3, row));

        getContentPane().add(builder.build());

        pack();
        setLocation(itemPosition);
        setMinimumSize(getSize());
        setVisible(true);
    }

    private void fireContinue(Node node) {
        LOGGER.info("Continue operation");

        result = JOptionPane.YES_OPTION;

        if (checkDoNotAskInFutureValueModel != null
            && ((Boolean) checkDoNotAskInFutureValueModel.getValue()).booleanValue()) {
            LOGGER.info("Do not show confirm dialog in the future.");
            Preferences.getInstance().setPtModeDoNotConfirmSwitch(true);
            Preferences.getInstance().save(null);
        }
    }

    private void fireCancel() {

    }

    public int getResult() {
        return result;
    }

}
