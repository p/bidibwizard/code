package org.bidib.wizard.mvc.main.view.cvdef;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jidesoft.grid.DefaultExpandableRow;

public final class CvNodeUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(CvNodeUtils.class);

    /**
     * Harvest the CV nodes with keywords from the provided tree structure and store the CV nodes in the map under the
     * keyword.
     * 
     * @param parentNode
     *            the parent node
     * @param mapKeywordToNode
     *            the map to store the harvested data
     */
    public static void harvestKeywordNodes(DefaultExpandableRow parentNode, Map<String, CvNode> mapKeywordToNode) {

        if (parentNode != null) {
            for (int index = 0; index < parentNode.getChildrenCount(); index++) {
                DefaultExpandableRow childNode = (DefaultExpandableRow) parentNode.getChildAt(index);
                if (childNode instanceof CvNode) {
                    CvNode cvNode = (CvNode) childNode;
                    if (StringUtils.isNotBlank(cvNode.getCV().getKeyword())) {
                        LOGGER.info("The current node has a keyword: {}, cvNumber: {}", cvNode.getCV().getKeyword(),
                            cvNode.getCV().getNumber());

                        mapKeywordToNode.put(cvNode.getCV().getKeyword(), cvNode);
                    }

                }
                harvestKeywordNodes(childNode, mapKeywordToNode);
            }
        }
    }

}
