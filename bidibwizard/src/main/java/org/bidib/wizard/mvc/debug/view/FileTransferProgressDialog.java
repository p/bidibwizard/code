package org.bidib.wizard.mvc.debug.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;

import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.dialog.EscapeDialog;
import org.bidib.wizard.mvc.debug.view.listener.DebugInterfaceViewListener;
import org.bidib.wizard.mvc.debug.view.listener.ProgressStatusCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.forms.builder.ButtonBarBuilder;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.FormLayout;

public class FileTransferProgressDialog extends EscapeDialog {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(FileTransferProgressDialog.class);

    private static final String ENCODED_DIALOG_COLUMN_SPECS = "pref, 10dlu, min(300dlu;pref)";

    private int result = JOptionPane.CANCEL_OPTION;

    private final AtomicBoolean continueTransmit = new AtomicBoolean(true);

    private final ProgressStatusCallback callback;

    private final JProgressBar progressBar = new JProgressBar(0, 100);

    private final ScheduledExecutorService worker = Executors.newScheduledThreadPool(1);

    public FileTransferProgressDialog(final Component parent, boolean modal,
        final Collection<DebugInterfaceViewListener> listeners) {
        super(JOptionPane.getFrameForComponent(parent), Resources.getString(FileTransferProgressDialog.class, "title"),
            modal);
        getContentPane().setLayout(new BorderLayout());

        DefaultFormBuilder builder = new DefaultFormBuilder(new FormLayout(ENCODED_DIALOG_COLUMN_SPECS));
        builder.border(Borders.DIALOG);

        builder.append(Resources.getString(getClass(), "progress"), progressBar);
        progressBar.setStringPainted(true);

        JButton cancel = new JButton(Resources.getString(getClass(), "cancel"));

        cancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fireCancel();
            }
        });

        JPanel buttons = new ButtonBarBuilder().addGlue().addButton(cancel).build();

        builder.nextLine();
        builder.append(buttons, 3);

        getContentPane().add(builder.build());

        pack();
        setLocationRelativeTo(parent);
        setMinimumSize(getSize());

        callback = new ProgressStatusCallback() {

            @Override
            public void statusChanged(final int progressValue) {
                LOGGER.info("The status has changed, progress: {}", progressValue);

                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        progressBar.setValue(progressValue);
                    }
                });
            }

            @Override
            public void transferFinished() {
                fireClose();
            }
        };

        Runnable runnable = new Runnable() {

            @Override
            public void run() {
                LOGGER.info("Transmit file.");
                try {
                    for (DebugInterfaceViewListener listener : listeners) {
                        listener.transmitFile(continueTransmit, callback);
                    }
                }
                catch (Exception ex) {
                    LOGGER.warn("Transmit file failed.", ex);
                }
            }
        };
        worker.execute(runnable);

        setVisible(true);
    }

    private void fireCancel() {
        continueTransmit.set(false);

        fireClose();
    }

    private void fireClose() {
        LOGGER.info("Close the dialog.");

        if (SwingUtilities.isEventDispatchThread()) {
            try {
                worker.shutdown();
                worker.awaitTermination(5, TimeUnit.SECONDS);
            }
            catch (InterruptedException ex) {
                LOGGER.warn("Wait for termination of worker was interrupted.", ex);
            }

            setVisible(false);
        }
        else {
            SwingUtilities.invokeLater(new Runnable() {

                @Override
                public void run() {
                    LOGGER.info("Close the dialog from AWT thread.");
                    try {
                        worker.shutdown();
                        worker.awaitTermination(5, TimeUnit.SECONDS);
                    }
                    catch (InterruptedException ex) {
                        LOGGER.warn("Wait for termination of worker was interrupted.", ex);
                    }

                    setVisible(false);
                }
            });
        }
    }

    public int getResult() {
        return result;
    }

}
