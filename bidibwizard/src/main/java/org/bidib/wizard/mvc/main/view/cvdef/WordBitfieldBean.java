package org.bidib.wizard.mvc.main.view.cvdef;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.binding.beans.Model;

public class WordBitfieldBean extends Model {
    private static final Logger LOGGER = LoggerFactory.getLogger(WordBitfieldBean.class);

    private static final long serialVersionUID = 1L;

    public final static int TOTAL_BITS = 16;

    public final static String BIT_PROPERTY = "bit";

    public final static String INVERTEDBIT_PROPERTY = "invertedBit";

    public final static String BIT0_PROPERTY = "bit0";

    public final static String BIT1_PROPERTY = "bit1";

    public final static String BIT2_PROPERTY = "bit2";

    public final static String BIT3_PROPERTY = "bit3";

    public final static String BIT4_PROPERTY = "bit4";

    public final static String BIT5_PROPERTY = "bit5";

    public final static String BIT6_PROPERTY = "bit6";

    public final static String BIT7_PROPERTY = "bit7";

    private Integer bitfield = new Integer(0);

    public Integer getBitfield() {
        return bitfield;
    }

    public boolean getBit0() {
        return (bitfield & 0x01) == 0x01;
    }

    public boolean getInvertedBit0() {
        return !getBit0();
    }

    public boolean getBit1() {
        return (bitfield & 0x02) == 0x02;
    }

    public boolean getInvertedBit1() {
        return !getBit1();
    }

    public boolean getBit2() {
        return (bitfield & 0x04) == 0x04;
    }

    public boolean getInvertedBit2() {
        return !getBit2();
    }

    public boolean getBit3() {
        return (bitfield & 0x08) == 0x08;
    }

    public boolean getInvertedBit3() {
        return !getBit3();
    }

    public boolean getBit4() {
        return (bitfield & 0x10) == 0x10;
    }

    public boolean getInvertedBit4() {
        return !getBit4();
    }

    public boolean getBit5() {
        return (bitfield & 0x20) == 0x20;
    }

    public boolean getInvertedBit5() {
        return !getBit5();
    }

    public boolean getBit6() {
        return (bitfield & 0x40) == 0x40;
    }

    public boolean getInvertedBit6() {
        return !getBit6();
    }

    public boolean getBit7() {
        return (bitfield & 0x80) == 0x80;
    }

    public boolean getInvertedBit7() {
        return !getBit7();
    }

    public boolean getBit8() {
        return (bitfield & 0x0100) == 0x0100;
    }

    public boolean getInvertedBit8() {
        return !getBit8();
    }

    public boolean getBit9() {
        return (bitfield & 0x0200) == 0x0200;
    }

    public boolean getInvertedBit9() {
        return !getBit9();
    }

    public boolean getBit10() {
        return (bitfield & 0x0400) == 0x0400;
    }

    public boolean getInvertedBit10() {
        return !getBit10();
    }

    public boolean getBit11() {
        return (bitfield & 0x0800) == 0x0800;
    }

    public boolean getInvertedBit11() {
        return !getBit11();
    }

    public boolean getBit12() {
        return (bitfield & 0x1000) == 0x1000;
    }

    public boolean getInvertedBit12() {
        return !getBit12();
    }

    public boolean getBit13() {
        return (bitfield & 0x2000) == 0x2000;
    }

    public boolean getInvertedBit13() {
        return !getBit13();
    }

    public boolean getBit14() {
        return (bitfield & 0x4000) == 0x4000;
    }

    public boolean getInvertedBit14() {
        return !getBit14();
    }

    public boolean getBit15() {
        return (bitfield & 0x8000) == 0x8000;
    }

    public boolean getInvertedBit15() {
        return !getBit15();
    }

    //

    public void setBit0(boolean bit) {
        setBit(bit, 0);
    }

    public void setBit1(boolean bit) {
        setBit(bit, 1);
    }

    public void setBit2(boolean bit) {
        setBit(bit, 2);
    }

    public void setBit3(boolean bit) {
        setBit(bit, 3);
    }

    public void setBit4(boolean bit) {
        setBit(bit, 4);
    }

    public void setBit5(boolean bit) {
        setBit(bit, 5);
    }

    public void setBit6(boolean bit) {
        setBit(bit, 6);
    }

    public void setBit7(boolean bit) {
        setBit(bit, 7);
    }

    public void setBit8(boolean bit) {
        setBit(bit, 8);
    }

    public void setBit9(boolean bit) {
        setBit(bit, 9);
    }

    public void setBit10(boolean bit) {
        setBit(bit, 10);
    }

    public void setBit11(boolean bit) {
        setBit(bit, 11);
    }

    public void setBit12(boolean bit) {
        setBit(bit, 12);
    }

    public void setBit13(boolean bit) {
        setBit(bit, 13);
    }

    public void setBit14(boolean bit) {
        setBit(bit, 14);
    }

    public void setBit15(boolean bit) {
        setBit(bit, 15);
    }

    private void setBit(boolean bit, int bitpos) {
        Integer newValue = null;
        if (bit) {
            newValue = (bitfield | (1 << bitpos));
        }
        else {
            newValue = (bitfield & ~(1 << bitpos));
        }

        setBitfield(newValue);
    }

    public void setBitfield(final Integer bitfieldNum) {
        LOGGER.debug("Set the bitfield value: {}", bitfieldNum);
        Integer newValue = null;
        if (bitfieldNum == null) {
            newValue = new Integer(0);
        }
        else {
            newValue = bitfieldNum;
        }
        Integer oldValue = this.bitfield;
        this.bitfield = newValue;

        for (int index = 0; index < TOTAL_BITS; index++) {
            int pos = (0x01 << index);
            Boolean oldBitValue = (oldValue & pos) == pos;
            Boolean newBitValue = (bitfield & pos) == pos;

            firePropertyChange(BIT_PROPERTY + index, oldBitValue, newBitValue);
        }
        for (int index = 0; index < TOTAL_BITS; index++) {
            int pos = (0x01 << index);
            Boolean oldBitValue = !((oldValue & pos) == pos);
            Boolean newBitValue = !((bitfield & pos) == pos);

            firePropertyChange(INVERTEDBIT_PROPERTY + index, oldBitValue, newBitValue);
        }
    }
}