package org.bidib.wizard.mvc.pomupdate.model;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class Decoder {

    private int address;

    private String decoderVersionAndModel;

    private String vendorId;

    private float prepareProgress;

    private float performProgress;

    public Decoder(int address) {
        this.address = address;
    }

    /**
     * @return the decoder address
     */
    public int getAddress() {
        return address;
    }

    /**
     * @param address
     *            the address to set
     */
    public void setAddress(int address) {
        this.address = address;
    }

    /**
     * @return the decoderVersionAndModel
     */
    public String getDecoderVersionAndModel() {
        return decoderVersionAndModel;
    }

    /**
     * @param decoderVersionAndModel
     *            the decoderVersionAndModel to set
     */
    public void setDecoderVersionAndModel(String decoderVersionAndModel) {
        this.decoderVersionAndModel = decoderVersionAndModel;
    }

    /**
     * @return the vendor id
     */
    public String getVendorId() {
        return vendorId;
    }

    /**
     * @param vendorId
     *            the vendor id to set
     */
    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    /**
     * @return the prepareProgress
     */
    public float getPrepareProgress() {
        return prepareProgress;
    }

    /**
     * @param prepareProgress
     *            the prepareProgress to set
     */
    public void setPrepareProgress(float prepareProgress) {
        this.prepareProgress = prepareProgress;
    }

    public boolean isPrepareUpdateDone() {
        return prepareProgress >= 1.0f;
    }

    /**
     * @return the performProgress
     */
    public float getPerformProgress() {
        return performProgress;
    }

    /**
     * @param performProgress
     *            the performProgress to set
     */
    public void setPerformProgress(float performProgress) {
        this.performProgress = performProgress;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Decoder) {
            Decoder other = (Decoder) obj;
            return other.address == address;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return address;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
