package org.bidib.wizard.mvc.script.view.wizard;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import org.apache.commons.collections4.CollectionUtils;
import org.bidib.schema.nodescriptsources.CategoryType;
import org.bidib.schema.nodescriptsources.DescriptionType;
import org.bidib.schema.nodescriptsources.NodeScriptSourceType;
import org.bidib.schema.nodescriptsources.NodeScriptSources;
import org.bidib.wizard.common.context.ApplicationContext;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.script.model.NodeScriptSource;
import org.bidib.wizard.script.node.NodeScriptContextKeys;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jidesoft.dialog.ButtonEvent;
import com.jidesoft.dialog.ButtonNames;
import com.jidesoft.swing.JideSwingUtilities;
import com.jidesoft.wizard.DefaultWizardPage;

public class SelectCategoryPage extends DefaultWizardPage {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(SelectCategoryPage.class);

    private ButtonGroup group = new ButtonGroup();

    List<JRadioButton> buttons = new ArrayList<>();

    private String selectedCategory;

    private final ApplicationContext scriptContext;

    private final Map<NodeScriptSource, NodeScriptSources> mapNodeScriptSources;

    public SelectCategoryPage(Icon icon, final ApplicationContext scriptContext,
        final Map<NodeScriptSource, NodeScriptSources> mapNodeScriptSources) {
        super(Resources.getString(SelectCategoryPage.class, "title"),
            Resources.getString(SelectCategoryPage.class, "description"), icon);

        this.scriptContext = scriptContext;
        this.mapNodeScriptSources = mapNodeScriptSources;
    }

    @Override
    public int getSelectedStepIndex() {
        return 1;
    }

    @Override
    protected void initContentPane() {
        super.initContentPane();

        final String lang = scriptContext.get(NodeScriptContextKeys.KEY_LANG, String.class);

        SortedSet<String> categories = new TreeSet<>();

        // TODO this should be a Map<String,List<CategoryType>> ... because we could have multiple sources
        Map<String, List<CategoryType>> categoryMap = new HashMap<>();

        NodeScriptSources nodeScriptSources = null;
        for (Entry<NodeScriptSource, NodeScriptSources> entry : mapNodeScriptSources.entrySet()) {
            nodeScriptSources = entry.getValue();
            NodeScriptSource key = entry.getKey();
            LOGGER.info("Fetched nodeScriptSource for key: {}", key);

            if (CollectionUtils.isNotEmpty(nodeScriptSources.getNodeScriptSource())) {

                // get the categories
                for (NodeScriptSourceType nodeScriptSource : nodeScriptSources.getNodeScriptSource()) {

                    if (CollectionUtils.isNotEmpty(nodeScriptSource.getCategory())) {
                        for (CategoryType category : nodeScriptSource.getCategory()) {
                            String categoryName = category.getApplicationCategory().name();
                            LOGGER.info("Add category with name: {}", categoryName);
                            categories.add(categoryName);

                            if (!categoryMap.containsKey(categoryName)) {
                                categoryMap.put(categoryName, new ArrayList<CategoryType>());
                            }

                            List<CategoryType> categoriesList = categoryMap.get(categoryName);
                            categoriesList.add(category);
                        }
                    }
                }
            }

        }
        if (CollectionUtils.isNotEmpty(categories)) {

            for (String categoryName : categories) {

                List<CategoryType> categoriesList = categoryMap.get(categoryName);
                if (CollectionUtils.isNotEmpty(categoriesList)) {
                    CategoryType category = categoriesList.get(0);

                    if (CollectionUtils.isNotEmpty(category.getDescription())) {
                        for (DescriptionType description : category.getDescription()) {
                            if (lang.equals(description.getLanguage())) {
                                LOGGER.info("Found description of category: {}", description);

                                JRadioButton button = new JRadioButton(description.getValue());
                                button.addItemListener(new ItemListener() {
                                    public void itemStateChanged(ItemEvent e) {
                                        if (e.getStateChange() == ItemEvent.SELECTED) {
                                            selectedCategory = category.getSubDirectory();
                                            updateNextPage();
                                        }
                                    }
                                });
                                buttons.add(button);
                                break;
                            }
                        }
                    }
                }
            }

            // add the buttons to the panel
            int buttonCount = buttons.size();

            JPanel panel = null;
            if (buttonCount > 5) {
                panel = new JPanel(new GridLayout(5, 2));
            }
            else {
                panel = new JPanel(new GridLayout(5, 1));
            }

            for (JRadioButton button : buttons) {
                LOGGER.info("Add button: {}", button);
                panel.add(button);
                group.add(button);
            }

            addComponent(panel);
            JideSwingUtilities.setOpaqueRecursively(panel, false);
            return;
        }
        // }

        JPanel emptyPanel = new JPanel(new BorderLayout());
        emptyPanel.add(new JLabel(Resources.getString(SelectCategoryPage.class, "configuration.missing")));
        addComponent(emptyPanel);
    }

    @Override
    public void setupWizardButtons() {
        fireButtonEvent(ButtonEvent.ENABLE_BUTTON, ButtonNames.BACK);
        fireButtonEvent(ButtonEvent.DISABLE_BUTTON, ButtonNames.NEXT);
        fireButtonEvent(ButtonEvent.SET_DEFAULT_BUTTON, ButtonNames.NEXT);
        fireButtonEvent(ButtonEvent.HIDE_BUTTON, ButtonNames.FINISH);
        updateNextPage();
    }

    public String getSelectedCategory() {

        return selectedCategory;
    }

    private void updateNextPage() {

        for (JRadioButton button : buttons) {
            if (button.isSelected()) {

                fireButtonEvent(ButtonEvent.ENABLE_BUTTON, ButtonNames.NEXT);
                return;
            }
        }
    }
}