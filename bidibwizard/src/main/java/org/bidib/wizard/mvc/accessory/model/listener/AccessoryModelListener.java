package org.bidib.wizard.mvc.accessory.model.listener;

public interface AccessoryModelListener {

    /**
     * The selected address has changed.
     * 
     * @param address
     *            the new address
     */
    void addressChanged(int address);
}
