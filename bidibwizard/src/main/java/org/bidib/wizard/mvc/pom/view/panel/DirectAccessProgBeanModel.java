package org.bidib.wizard.mvc.pom.view.panel;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.bidib.wizard.mvc.pom.model.PomMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DirectAccessProgBeanModel extends ProgCommandAwareBeanModel {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(DirectAccessProgBeanModel.class);

    public static final String PROPERTYNAME_MODE = "mode";

    public static final String PROPERTYNAME_CV_NUMBER = "cvNumber";

    public static final String PROPERTYNAME_CV_VALUE = "cvValue";

    private PomMode mode = PomMode.BYTE;

    private Integer cvNumber = 1;

    private Integer cvValue;

    public DirectAccessProgBeanModel() {
    }

    /**
     * @return the mode
     */
    public PomMode getMode() {
        return mode;
    }

    /**
     * @param mode
     *            the mode to set
     */
    public void setMode(PomMode mode) {
        PomMode oldMode = this.mode;
        this.mode = mode;
        firePropertyChange(PROPERTYNAME_MODE, oldMode, mode);
    }

    /**
     * @return the cvNumber
     */
    public Integer getCvNumber() {
        return cvNumber;
    }

    /**
     * @param cvNumber
     *            the cvNumber to set
     */
    public void setCvNumber(Integer cvNumber) {
        Integer oldCvNumber = this.cvNumber;
        this.cvNumber = cvNumber;
        firePropertyChange(PROPERTYNAME_CV_NUMBER, oldCvNumber, cvNumber);
    }

    /**
     * @return the cvValue
     */
    public Integer getCvValue() {
        return cvValue;
    }

    /**
     * @param cvValue
     *            the cvValue to set
     */
    public void setCvValue(Integer cvValue) {
        Integer oldCvValue = this.cvValue;

        LOGGER.info("+++ Set the new CV value: {}, old: {}", cvValue, oldCvValue);

        this.cvValue = cvValue;
        firePropertyChange(PROPERTYNAME_CV_VALUE, oldCvValue, cvValue);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}