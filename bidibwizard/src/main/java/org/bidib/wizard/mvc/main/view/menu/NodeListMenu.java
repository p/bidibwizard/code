package org.bidib.wizard.mvc.main.view.menu;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.LinkedList;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;

import org.bidib.jbidibc.core.enumeration.IdentifyState;
import org.bidib.jbidibc.core.utils.NodeUtils;
import org.bidib.jbidibc.core.utils.ProductUtils;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.view.component.LabeledDisplayItems;
import org.bidib.wizard.mvc.main.view.menu.listener.NodeListMenuListener;
import org.bidib.wizard.mvc.preferences.model.Preferences;
import org.bidib.wizard.mvc.pt.controller.PtProgrammerController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NodeListMenu {

    private static final Logger LOGGER = LoggerFactory.getLogger(NodeListMenu.class);

    private static final Collection<NodeListMenuListener> menuListeners = new LinkedList<NodeListMenuListener>();

    private final MainModel model;

    private final JMenuItem clearError = new JMenuItem(Resources.getString(getClass(), "clearError") + " ...");

    private final JCheckBoxMenuItem identify = new JCheckBoxMenuItem(Resources.getString(getClass(), "identify"));

    private final JMenuItem firmwareUpdate = new JMenuItem(Resources.getString(getClass(), "firmwareUpdate") + " ...");

    private final JMenuItem features = new JMenuItem(Resources.getString(getClass(), "features") + " ...");

    private final JMenuItem reset = new JMenuItem(Resources.getString(getClass(), "reset") + " ...");

    private final JMenuItem loco = new JMenuItem(Resources.getString(getClass(), "loco") + " ...");

    private JSeparator locoListSeparator;

    private final JMenuItem locoList = new JMenuItem(Resources.getString(getClass(), "locoList") + " ...");

    private final JMenuItem dccAccessory = new JMenuItem(Resources.getString(getClass(), "dccAccessory") + " ...");

    private final JMenuItem locoCv = new JMenuItem(Resources.getString(getClass(), "locoCv") + " ...");

    private final JMenuItem locoCvPt = new JMenuItem(Resources.getString(getClass(), "locoCvPt") + " ...");

    private final JMenuItem ping = new JMenuItem(Resources.getString(getClass(), "ping"));

    private final JMenuItem uniqueId = new JMenuItem(Resources.getString(getClass(), "uniqueId"));

    private final JMenuItem dmxModeler = new JMenuItem(Resources.getString(getClass(), "dmxModeler") + " ...");

    private final JCheckBoxMenuItem addressMessagesEnabled =
        new JCheckBoxMenuItem(Resources.getString(getClass(), "addressMessages"));

    private final JCheckBoxMenuItem feedbackMessagesEnabled =
        new JCheckBoxMenuItem(Resources.getString(getClass(), "feedbackMessages"));

    private final JCheckBoxMenuItem keyMessagesEnabled =
        new JCheckBoxMenuItem(Resources.getString(getClass(), "keyMessages"));

    private final JCheckBoxMenuItem externalStartEnabled =
        new JCheckBoxMenuItem(Resources.getString(getClass(), "externalStart"));

    private final JCheckBoxMenuItem dccStartEnabled =
        new JCheckBoxMenuItem(Resources.getString(getClass(), "dccStart"));

    private final JCheckBoxMenuItem feedbackMirrorDisabled =
        new JCheckBoxMenuItem(Resources.getString(getClass(), "feedbackMirrorDisabled"));

    private final JMenuItem importNode = new JMenuItem(Resources.getString(getClass(), "import") + " ...");

    private final JMenuItem exportNode = new JMenuItem(Resources.getString(getClass(), "export") + " ...");

    private final JMenuItem generateNodeDocumentation =
        new JMenuItem(Resources.getString(getClass(), "documentation") + " ...");

    private final JMenuItem saveNode = new JMenuItem(Resources.getString(getClass(), "saveNode"));

    private final JMenuItem bulkSwitchNode = new JMenuItem(Resources.getString(getClass(), "bulkSwitch"));

    private JSeparator bulkSwitchSeparator;

    private JSeparator keyMessagesExtMacroStartSeparator;

    public NodeListMenu(MainModel model) {
        this.model = model;

        exportNode.setToolTipText(Resources.getString(getClass(), "export.tooltip"));
        saveNode.setToolTipText(Resources.getString(getClass(), "saveNode.tooltip"));
        clearError.setToolTipText(Resources.getString(getClass(), "clearError.tooltip"));
    }

    private void addMenuItem(Object menu, JMenuItem menuItem) {
        if (menu instanceof JMenu) {
            ((JMenu) menu).add(menuItem);
        }
        else if (menu instanceof JPopupMenu) {
            ((JPopupMenu) menu).add(menuItem);
        }
    }

    private void addNodeListMenu(Object menu) {
        JMenuItem editLabel = new JMenuItem(Resources.getString(getClass(), "editLabel") + " ...");

        editLabel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fireEditLabel();
            }
        });
        addMenuItem(menu, editLabel);

        clearError.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fireClearError();
            }
        });
        addMenuItem(menu, clearError);

        identify.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fireIdentify(identify.isSelected());
            }
        });
        addMenuItem(menu, identify);

        JMenuItem details = new JMenuItem(Resources.getString(getClass(), "details") + " ...");

        details.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fireDetails();
            }
        });
        addMenuItem(menu, details);

        firmwareUpdate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireFirmwareUpdate();
            }
        });
        addMenuItem(menu, firmwareUpdate);

        features.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireFeatures();
            }
        });
        addMenuItem(menu, features);

        reset.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireReset();
            }
        });
        addMenuItem(menu, reset);

        dmxModeler.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                fireDmxModeler();
            }
        });
        addMenuItem(menu, dmxModeler);

        loco.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireLoco();
            }
        });
        addMenuItem(menu, loco);

        dccAccessory.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireDccAccessory();
            }
        });
        addMenuItem(menu, dccAccessory);

        // programming on main track
        locoCv.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireLocoCv();
            }
        });
        addMenuItem(menu, locoCv);
        // TODO enable
        // locoCv.setEnabled(false);

        // programming on programming track
        locoCvPt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireLocoCvPt();
            }
        });
        addMenuItem(menu, locoCvPt);

        ping.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                firePing();
            }
        });
        addMenuItem(menu, ping);

        uniqueId.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fireReadUniqueId();
            }
        });
        addMenuItem(menu, uniqueId);
        boolean isPowerUser = Preferences.getInstance().isPowerUser();
        uniqueId.setVisible(isPowerUser);

        boolean isLocoListEnabled = Preferences.getInstance().isCsQueryEnabled();

        // if (isLocoListEnabled) {
        locoListSeparator = addSeparator(menu);
        locoListSeparator.setVisible(isLocoListEnabled);

        locoList.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireLocoList();
            }
        });
        addMenuItem(menu, locoList);
        locoList.setVisible(isLocoListEnabled);
        // }

        addSeparator(menu);

        addressMessagesEnabled.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireAddressMessagesEnabled(addressMessagesEnabled.isSelected());
            }
        });
        addMenuItem(menu, addressMessagesEnabled);

        feedbackMessagesEnabled.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireFeedbackMessagesEnabled(feedbackMessagesEnabled.isSelected());
            }
        });
        addMenuItem(menu, feedbackMessagesEnabled);

        // power users can disable sending feedback mirror messages
        feedbackMirrorDisabled.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireFeedbackMirrorDisabled(feedbackMirrorDisabled.isSelected());
            }
        });
        addMenuItem(menu, feedbackMirrorDisabled);
        feedbackMirrorDisabled.setVisible(isPowerUser);

        keyMessagesEnabled.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireKeyMessagesEnabled(keyMessagesEnabled.isSelected());
            }
        });
        addMenuItem(menu, keyMessagesEnabled);

        dccStartEnabled.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireDccStartEnabled(dccStartEnabled.isSelected());
            }
        });
        addMenuItem(menu, dccStartEnabled);

        externalStartEnabled.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fireExternalStartEnabled(externalStartEnabled.isSelected());
            }
        });
        addMenuItem(menu, externalStartEnabled);

        keyMessagesExtMacroStartSeparator = addSeparator(menu);

        importNode.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fireImport();
            }
        });
        addMenuItem(menu, importNode);

        exportNode.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fireExport();
            }
        });
        addMenuItem(menu, exportNode);

        generateNodeDocumentation.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fireGenerateDocumentation();
            }
        });
        addMenuItem(menu, generateNodeDocumentation);

        addSeparator(menu);

        saveNode.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fireSave();
            }
        });
        addMenuItem(menu, saveNode);

        bulkSwitchSeparator = addSeparator(menu);

        bulkSwitchNode.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fireBulkSwitch();
            }
        });
        addMenuItem(menu, bulkSwitchNode);
    }

    public void addMenuListener(NodeListMenuListener l) {
        menuListeners.add(l);
    }

    private JSeparator addSeparator(Object menu) {

        JSeparator separator = new JPopupMenu.Separator();
        if (menu instanceof JMenu) {
            ((JMenu) menu).add(separator);
        }
        else if (menu instanceof JPopupMenu) {
            ((JPopupMenu) menu).add(separator);
        }
        return separator;
    }

    private void fireAddressMessagesEnabled(boolean isSelected) {
        for (NodeListMenuListener l : menuListeners) {
            l.addressMessagesEnabled(isSelected);
        }
    }

    private void fireDccStartEnabled(boolean isSelected) {
        for (NodeListMenuListener l : menuListeners) {
            l.dccStartEnabled(isSelected);
        }
    }

    private void fireDetails() {
        for (NodeListMenuListener l : menuListeners) {
            l.showDetails();
        }
    }

    private void fireEditLabel() {
        for (NodeListMenuListener l : menuListeners) {
            l.editLabel();
        }
    }

    private void fireClearError() {
        for (NodeListMenuListener l : menuListeners) {
            l.clearErrors();
        }
    }

    private void fireExport() {
        for (NodeListMenuListener l : menuListeners) {
            l.exportNode();
        }
    }

    private void fireExternalStartEnabled(boolean isSelected) {
        for (NodeListMenuListener l : menuListeners) {
            l.externalStartEnabled(isSelected);
        }
    }

    private void fireFeedbackMessagesEnabled(boolean isSelected) {
        for (NodeListMenuListener l : menuListeners) {
            l.feedbackMessagesEnabled(isSelected);
        }
    }

    private void fireFeedbackMirrorDisabled(boolean disable) {
        for (NodeListMenuListener l : menuListeners) {
            l.feedbackMirrorDisabled(disable);
        }
    }

    private void fireFirmwareUpdate() {
        for (NodeListMenuListener l : menuListeners) {
            l.firmwareUpdate();
        }
    }

    private void fireIdentify(boolean isSelected) {
        for (NodeListMenuListener l : menuListeners) {
            l.identify(isSelected);
        }
    }

    private void fireImport() {
        for (NodeListMenuListener l : menuListeners) {
            l.importNode();
        }
    }

    private void fireKeyMessagesEnabled(boolean isSelected) {
        for (NodeListMenuListener l : menuListeners) {
            l.keyMessagesEnabled(isSelected);
        }
    }

    private void fireFeatures() {
        for (NodeListMenuListener l : menuListeners) {
            l.features();
        }
    }

    private void fireReset() {
        for (NodeListMenuListener l : menuListeners) {
            l.reset();
        }
    }

    private void firePing() {
        for (NodeListMenuListener l : menuListeners) {
            l.ping();
        }
    }

    private void fireReadUniqueId() {
        for (NodeListMenuListener l : menuListeners) {
            l.readUniqueId();
        }
    }

    private void fireDmxModeler() {
        for (NodeListMenuListener l : menuListeners) {
            l.dmxModeler();
        }
    }

    private void fireLoco() {
        for (NodeListMenuListener l : menuListeners) {
            l.loco();
        }
    }

    private void fireLocoList() {
        for (NodeListMenuListener l : menuListeners) {
            l.locoList();
        }
    }

    private void fireDccAccessory() {
        for (NodeListMenuListener l : menuListeners) {
            l.dccAccessory();
        }
    }

    private void fireLocoCv() {
        for (NodeListMenuListener l : menuListeners) {
            l.locoCv();
        }
    }

    private void fireLocoCvPt() {
        for (NodeListMenuListener l : menuListeners) {
            l.locoCvPt();
        }
    }

    private void fireSave() {
        for (NodeListMenuListener l : menuListeners) {
            l.saveNode();
        }
    }

    private void fireBulkSwitch() {
        for (NodeListMenuListener l : menuListeners) {
            l.bulkSwitchNode();
        }
    }

    private void fireGenerateDocumentation() {
        for (NodeListMenuListener l : menuListeners) {
            l.generateDocumentation();
        }
    }

    public JMenu getJMenu() {
        JMenu menu = new JMenu();

        addNodeListMenu(menu);
        return menu;
    }

    public JPopupMenu getPopupMenu() {
        JPopupMenu popupMenu = new BasicPopupMenu() {
            private static final long serialVersionUID = 1L;

            @Override
            public void show(Component invoker, int x, int y) {
                Node node = ((LabeledDisplayItems<Node>) invoker).getSelectedItem();
                if (node != null) {
                    updateMenuItems(node);
                    super.show(invoker, x, y);
                }
            }
        };

        addNodeListMenu(popupMenu);
        return popupMenu;
    }

    public void updateMenuItems(Node node) {
        boolean nodeIsSelected = node.equals(model.getSelectedNode());

        setAddressMessagesEnabled(node.isAddressMessagesEnabled());
        setDccStartEnabled(node.isDccStartEnabled());
        setExternalStartEnabled(node.isExternalStartEnabled());
        setFeedbackMessagesEnabled(node.isFeedbackMessagesEnabled());

        // feedback mirror disabled is only available for power users
        setFeedbackMirrorDisabled(node.isFeedbackMirrorDisabled());
        boolean isPowerUser = Preferences.getInstance().isPowerUser();
        feedbackMirrorDisabled.setVisible(node.hasFeedbackPorts() && isPowerUser);

        uniqueId.setVisible(isPowerUser);

        if (keyMessagesEnabled.isVisible() || dccStartEnabled.isVisible() || externalStartEnabled.isVisible()) {
            keyMessagesExtMacroStartSeparator.setVisible(true);
        }
        else {
            keyMessagesExtMacroStartSeparator.setVisible(false);
        }

        setClearErrorEnabled(node.isNodeHasError());
        setFeaturesEnabled(!node.isBootloaderNode());
        setFirmwareUpdateEnabled(node.isUpdatable());
        setLocoEnabled(node.isCommandStation(), nodeIsSelected);
        setDccAccessoryEnabled(node.isCommandStation(), nodeIsSelected);

        // must check bit 3 of class
        setLocoCvEnabled(NodeUtils.hasCommandStationFunctions(node.getUniqueId()),
            NodeUtils.hasCommandStationProgrammingFunctions(node.getUniqueId()), nodeIsSelected);
        setIdentifyStateSelected(node.getIdentifyState());
        setBulkSwitchEnabled(NodeUtils.hasSwitchFunctions(node.getUniqueId()));

        boolean hasData = hasExchangeableData(node);
        setImportEnabled(hasData);
        setExportEnabled(hasData);
        setSaveEnabled(hasData);

        // currently the DMX modeler is enabled for OneDMX only
        boolean isOneDMX = ProductUtils.isOneDMX(node.getUniqueId());
        setDmxModelerEnabled(NodeUtils.hasSwitchFunctions(node.getUniqueId()) && isOneDMX, nodeIsSelected);
    }

    public void setClearErrorEnabled(Boolean isClearErrorEnabled) {
        clearError.setVisible(isClearErrorEnabled != null && isClearErrorEnabled);
        clearError.setEnabled(isClearErrorEnabled != null && isClearErrorEnabled);
    }

    // TODO ??? why is this needed, what about switch and other port types ???
    // TODO what about features and CV values?
    private boolean hasExchangeableData(Node node) {
        if (node != null && node.equals(model.getSelectedNode())) {
            boolean hasVendorCV = node.getVendorCV() != null;
            boolean hasFeatures = !node.isBootloaderNode();
            return hasVendorCV || hasFeatures || model.getAccessories().size() > 0 || model.getLightPorts().size() > 0
                || model.getMacros().size() > 0 || model.getServoPorts().size() > 0
                || model.getFeedbackPorts().size() > 0;
        }
        return false;
    }

    public void setAddressMessagesEnabled(Boolean isAddressMessagesEnabled) {
        addressMessagesEnabled.setVisible(isAddressMessagesEnabled != null);
        addressMessagesEnabled.setSelected(isAddressMessagesEnabled != null && isAddressMessagesEnabled);
    }

    public void setDccStartEnabled(Boolean isDccStartEnabled) {
        dccStartEnabled.setVisible(isDccStartEnabled != null);
        dccStartEnabled.setSelected(isDccStartEnabled != null && isDccStartEnabled);
    }

    public void setExportEnabled(Boolean exportEnabled) {
        exportNode.setEnabled(exportEnabled != null && exportEnabled);
        generateNodeDocumentation.setEnabled(exportEnabled != null && exportEnabled);
    }

    public void setExternalStartEnabled(Boolean isExternalStartEnabled) {
        externalStartEnabled.setVisible(isExternalStartEnabled != null);
        externalStartEnabled.setSelected(isExternalStartEnabled != null && isExternalStartEnabled);
    }

    public void setFeedbackMessagesEnabled(Boolean isFeedbackMessagesEnabled) {
        feedbackMessagesEnabled.setVisible(isFeedbackMessagesEnabled != null);
        feedbackMessagesEnabled.setSelected(isFeedbackMessagesEnabled != null && isFeedbackMessagesEnabled);
    }

    public void setFeedbackMirrorDisabled(Boolean isFeedbackMirrorDisabled) {
        feedbackMirrorDisabled.setVisible(isFeedbackMirrorDisabled != null);
        feedbackMirrorDisabled.setSelected(isFeedbackMirrorDisabled != null && isFeedbackMirrorDisabled);
    }

    public void setFirmwareUpdateEnabled(boolean isFirmwareUpdateEnabled) {
        firmwareUpdate.setEnabled(isFirmwareUpdateEnabled);
    }

    public void setFeaturesEnabled(boolean isFeaturesEnabled) {
        features.setEnabled(isFeaturesEnabled);
    }

    public void setIdentifyStateSelected(IdentifyState identifyState) {
        identify.setSelected(identifyState == IdentifyState.START);
    }

    public void setImportEnabled(Boolean importEnabled) {
        importNode.setEnabled(importEnabled != null && importEnabled);
    }

    public void setKeyMessagesEnabled(Boolean isKeyMessagesEnabled) {
        keyMessagesEnabled.setVisible(isKeyMessagesEnabled != null);
        keyMessagesEnabled.setSelected(isKeyMessagesEnabled != null && isKeyMessagesEnabled);
    }

    private void setLocoEnabled(Boolean isLocoEnabled, Boolean nodeIsSelected) {
        loco.setEnabled(isLocoEnabled != null && isLocoEnabled && nodeIsSelected != null && nodeIsSelected);
        loco.setVisible(isLocoEnabled != null && isLocoEnabled);

        boolean isLocoListEnabled = Preferences.getInstance().isCsQueryEnabled();

        locoList.setEnabled(
            isLocoEnabled != null && isLocoEnabled && nodeIsSelected != null && nodeIsSelected && isLocoListEnabled);
        locoList.setVisible(isLocoEnabled != null && isLocoEnabled && isLocoListEnabled);
        locoListSeparator.setVisible(isLocoEnabled != null && isLocoEnabled && isLocoListEnabled);
    }

    private void setDccAccessoryEnabled(Boolean isDccAccessoryEnabled, Boolean nodeIsSelected) {
        dccAccessory.setEnabled(
            isDccAccessoryEnabled != null && isDccAccessoryEnabled && nodeIsSelected != null && nodeIsSelected);
        dccAccessory.setVisible(isDccAccessoryEnabled != null && isDccAccessoryEnabled);
    }

    private void setLocoCvEnabled(Boolean isDccGenerator, Boolean isLocoCvEnabled, Boolean nodeIsSelected) {

        // TODO find a better solution for this hack ...
        boolean isLocoCvOpened = false;
        if (PtProgrammerController.isOpened()) {
            LOGGER.info("The dialog is already opened.");
            isLocoCvOpened = true;
        }

        // enable the menu items: DCC generator must always support POM
        locoCv.setEnabled(isDccGenerator != null && isDccGenerator && nodeIsSelected != null && nodeIsSelected);
        locoCvPt.setEnabled(
            isLocoCvEnabled != null && isLocoCvEnabled && !isLocoCvOpened && nodeIsSelected != null && nodeIsSelected);

        locoCv.setVisible(isDccGenerator != null && isDccGenerator);
        locoCvPt.setVisible(isLocoCvEnabled != null && isLocoCvEnabled);
    }

    private void setDmxModelerEnabled(Boolean isDmxModelerEnabled, Boolean nodeIsSelected) {
        dmxModeler
            .setEnabled(isDmxModelerEnabled != null && isDmxModelerEnabled && nodeIsSelected != null && nodeIsSelected);
        dmxModeler.setVisible(isDmxModelerEnabled != null && isDmxModelerEnabled);
    }

    private void setSaveEnabled(Boolean isSaveEnabled) {

        // TODO fix and enable
        isSaveEnabled = false;

        saveNode.setEnabled(isSaveEnabled != null && isSaveEnabled);
    }

    private void setBulkSwitchEnabled(Boolean isBulkSwitchEnabled) {
        boolean isPowerUser = Preferences.getInstance().isPowerUser();

        // show bulk switch operations only for power users
        bulkSwitchNode.setEnabled(isBulkSwitchEnabled);
        bulkSwitchNode.setVisible(isPowerUser);
        bulkSwitchSeparator.setVisible(isPowerUser);
    }
}
