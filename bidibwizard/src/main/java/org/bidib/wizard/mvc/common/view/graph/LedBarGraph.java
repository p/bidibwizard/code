package org.bidib.wizard.mvc.common.view.graph;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.plaf.basic.BasicGraphicsUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LedBarGraph extends JPanel {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(LedBarGraph.class);

    private final int totalLedsCount;

    private int totalHeight;

    private Color[] colors;

    private Color[] colorsInactive;

    private int value;

    private int singleLedHeight = 7;

    private int barWidth = 10;

    private int spaceLeft = 3;

    private int spaceRight = 3;

    private int textHeight = 20;

    private int totalWidth;

    private final Font labelFont;

    public LedBarGraph(int totalLedsCount) {

        this.totalLedsCount = totalLedsCount;

        totalHeight = this.totalLedsCount * singleLedHeight + textHeight;

        colors = new Color[totalLedsCount];
        colorsInactive = new Color[totalLedsCount];

        for (int index = 0; index < colors.length; index++) {

            if (index < 2) {
                colors[index] = Color.RED;
            }
            else if (index < 3) {
                colors[index] = Color.ORANGE;
            }
            else {
                colors[index] = Color.GREEN;
            }
            colorsInactive[index] = colors[index].darker().darker().darker();
        }

        totalWidth = barWidth + spaceLeft + spaceRight;

        labelFont = UIManager.getDefaults().getFont("Label.font");
        LOGGER.info("Current labelFont: {}", labelFont);
    }

    public void setValue(int value) {
        LOGGER.info("Set value: {}", value);
        this.value = value;

        repaint();
    }

    @Override
    public void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);

        int indexedValue = value / totalLedsCount;
        LOGGER.info("new indexed value: {}", indexedValue);

        for (int index = 0; index < totalLedsCount; index++) {

            if (indexedValue > index) {
                graphics.setColor(colors[index]);
            }
            else {
                graphics.setColor(colorsInactive[index]);
            }
            graphics.fillRect(spaceLeft, totalHeight - (index * singleLedHeight + textHeight), totalWidth,
                singleLedHeight - 1);
        }

        Graphics2D g2d = (Graphics2D) graphics;
        g2d.setColor(Color.BLACK);
        g2d.setFont(labelFont);

        // must set the rendering hint for proper rendering
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        // g2d.drawString(String.format("%2d%%", value), spaceLeft, totalHeight);
        BasicGraphicsUtils.drawString(g2d, String.format("%2d%%", value), -1, spaceLeft, totalHeight);
    }

    @Override
    public Dimension getMinimumSize() {
        return new Dimension(totalWidth + 10, totalHeight + singleLedHeight);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(totalWidth + 10, totalHeight + singleLedHeight);
    }

    public int getTotalLedsCount() {
        return totalLedsCount;
    }
}
