package org.bidib.wizard.mvc.main.view.panel;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import org.apache.commons.collections4.CollectionUtils;
import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.enumeration.IoBehaviourInputEnum;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.PortConfigKeys;
import org.bidib.jbidibc.core.exception.InvalidConfigurationException;
import org.bidib.jbidibc.core.port.BytePortConfigValue;
import org.bidib.jbidibc.core.port.PortConfigValue;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.wizard.comm.CommunicationFactory;
import org.bidib.wizard.comm.InputPortStatus;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.dialog.LabelDialog;
import org.bidib.wizard.labels.InputPortLabelFactory;
import org.bidib.wizard.labels.Labels;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.bidib.wizard.mvc.common.view.renderer.PortIdentifierTableCellRenderer;
import org.bidib.wizard.mvc.main.model.ConfigurablePort;
import org.bidib.wizard.mvc.main.model.InputPort;
import org.bidib.wizard.mvc.main.model.InputPortTableModel;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.model.Port;
import org.bidib.wizard.mvc.main.model.SimplePortTableModel;
import org.bidib.wizard.mvc.main.model.listener.InputPortListener;
import org.bidib.wizard.mvc.main.view.menu.listener.PortListMenuListener;
import org.bidib.wizard.mvc.main.view.table.ComboBoxEditor;
import org.bidib.wizard.mvc.main.view.table.ConfigurablePortComboBoxRenderer;
import org.bidib.wizard.mvc.main.view.table.DefaultPortListMenuListener;
import org.bidib.wizard.mvc.main.view.table.InputPortTableStatusCellRenderer;
import org.bidib.wizard.mvc.main.view.table.PortConfigErrorAwareEditor;
import org.bidib.wizard.mvc.main.view.table.PortConfigErrorAwareRenderer;
import org.bidib.wizard.mvc.main.view.table.PortTable;
import org.bidib.wizard.mvc.main.view.table.PortTicksEditor;
import org.bidib.wizard.mvc.main.view.table.PortTicksRenderer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jidesoft.grid.TableColumnChooser;

public class InputPortListPanel
    extends SimplePortListPanel<InputPortStatus, InputPort, InputPortListener<InputPortStatus>> {
    private static final Logger LOGGER = LoggerFactory.getLogger(InputPortListPanel.class);

    private static final long serialVersionUID = 1L;

    private final MainModel mainModel;

    private final class IoBehaviourComboBoxRenderer extends ConfigurablePortComboBoxRenderer<IoBehaviourInputEnum> {
        private static final long serialVersionUID = 1L;

        public IoBehaviourComboBoxRenderer(IoBehaviourInputEnum[] items, byte... pcfgType) {
            super(InputPortTableModel.COLUMN_PORT_INSTANCE, items, pcfgType);
        }

        @Override
        protected Object getCurrentValue(ConfigurablePort<?> port) {
            IoBehaviourInputEnum value = ((InputPort) port).getInputBehaviour();
            if (IoBehaviourInputEnum.UNKNOWN == value) {
                value = null;
            }
            return value;
        }
    }

    public InputPortListPanel(final MainModel model) {
        super(new InputPortTableModel(model), model.getInputPorts(),
            Resources.getString(InputPortListPanel.class, "emptyTable"));

        mainModel = model;
        mainModel.addInputPortListListener(this);

        // initialize the table columns with an identifier to allow show/hide later ...
        TableColumn tc = table.getColumnModel().getColumn(InputPortTableModel.COLUMN_LABEL);
        tc.setCellRenderer(new PortConfigErrorAwareRenderer(InputPortTableModel.COLUMN_LABEL));
        tc.setCellEditor(new PortConfigErrorAwareEditor(InputPortTableModel.COLUMN_PORT_INSTANCE));
        tc.setIdentifier(Integer.valueOf(InputPortTableModel.COLUMN_LABEL));

        TableCellRenderer tableCellRenderer =
            new IoBehaviourComboBoxRenderer(IoBehaviourInputEnum.getValues(), BidibLibrary.BIDIB_PCFG_INPUT_CTRL);
        TableCellEditor tableCellEditor = new ComboBoxEditor<IoBehaviourInputEnum>(IoBehaviourInputEnum.getValues());
        tc = table.getColumnModel().getColumn(InputPortTableModel.COLUMN_IO_BEHAVIOUR);
        tc.setCellRenderer(tableCellRenderer);
        tc.setCellEditor(tableCellEditor);
        tc.setIdentifier(Integer.valueOf(InputPortTableModel.COLUMN_IO_BEHAVIOUR));

        tc = table.getColumnModel().getColumn(InputPortTableModel.COLUMN_SWITCH_OFF_TIME);
        tc.setCellRenderer(new PortTicksRenderer());
        tc.setCellEditor(new PortTicksEditor(0, 255));
        tc.setIdentifier(Integer.valueOf(InputPortTableModel.COLUMN_SWITCH_OFF_TIME));

        tc = table.getColumnModel().getColumn(InputPortTableModel.COLUMN_PORT_IDENTIFIER);
        tc.setCellRenderer(new PortIdentifierTableCellRenderer());
        tc.setMaxWidth(80);
        tc.setIdentifier(Integer.valueOf(InputPortTableModel.COLUMN_PORT_IDENTIFIER));

        tc = table.getColumnModel().getColumn(InputPortTableModel.COLUMN_STATUS);
        tc.setCellRenderer(new InputPortTableStatusCellRenderer());
        tc.setIdentifier(Integer.valueOf(InputPortTableModel.COLUMN_STATUS));

        TableColumnChooser.hideColumn(table, InputPortTableModel.COLUMN_PORT_INSTANCE);

        registerPortListener();
    }

    @Override
    protected PortTable createPortTable(
        SimplePortTableModel<InputPortStatus, InputPort, InputPortListener<InputPortStatus>> tableModel,
        String emptyTableText) {

        PortTable portTable = new PortTable(tableModel, emptyTableText) {
            private static final long serialVersionUID = 1L;

            @Override
            public void clearTable() {
            }

            @Override
            protected PortListMenuListener createMenuListener() {
                // create the port list menu
                return new DefaultPortListMenuListener() {
                    @Override
                    public void editLabel() {
                        final int row = getRow(popupEvent.getPoint());
                        if (row > -1) {
                            Object val = getValueAt(row, 0);
                            if (val instanceof Port<?>) {
                                val = ((Port<?>) val).toString();
                            }
                            final Object value = val;
                            if (value instanceof String) {
                                // show the port name editor
                                new LabelDialog((String) value, popupEvent.getXOnScreen(), popupEvent.getYOnScreen()) {
                                    @Override
                                    public void labelChanged(String label) {
                                        setValueAt(label, row, 0);
                                    }
                                };
                            }
                        }
                        else {
                            LOGGER.warn("The row is not available!");
                        }
                    }

                    @Override
                    public void mapPort() {

                        final int row = getRow(popupEvent.getPoint());
                        if (row > -1) {
                            Object val = getValueAt(row, 0);
                            if (val instanceof InputPort) {
                                InputPort inputPort = (InputPort) val;
                                LOGGER.info("Change mapping for port: {}", inputPort);

                                // TODO check if the port supports switch to input port
                                int result =
                                    JOptionPane.showConfirmDialog(
                                        JOptionPane.getFrameForComponent(InputPortListPanel.this),
                                        Resources.getString(InputPortListPanel.class, "switch-port-confirm"),
                                        Resources.getString(InputPortListPanel.class, "switch-port-title"),
                                        JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
                                if (result == JOptionPane.OK_OPTION) {
                                    LOGGER.info("Change the port to an input port.");

                                    InputPortTableModel inputPortTableModel = (InputPortTableModel) getModel();
                                    inputPortTableModel.changePortType(LcOutputType.INPUTPORT, inputPort);

                                }
                            }
                        }
                    }
                };
            }

        };

        return portTable;
    }

    private void registerPortListener() {

        final Labels inputPortLabels =
            DefaultApplicationContext.getInstance().get(DefaultApplicationContext.KEY_INPUTPORT_LABELS, Labels.class);

        // add an input port listener that handles some method calls and delegates labelChanged to the provided listener
        super.addPortListener(new InputPortListener<InputPortStatus>() {
            @Override
            public void labelChanged(Port<InputPortStatus> port, String label) {
                // listener.labelChanged(port, label);

                port.setLabel(label);

                try {
                    InputPortLabelFactory factory = new InputPortLabelFactory();
                    long uniqueId = mainModel.getSelectedNode().getNode().getUniqueId();
                    factory.replaceLabel(inputPortLabels, uniqueId, port.getId(), label);

                    factory.saveLabels(uniqueId, inputPortLabels);
                }
                catch (InvalidConfigurationException ex) {
                    LOGGER.warn("Save input port labels failed.", ex);

                    String labelPath = ex.getReason();
                    JOptionPane.showMessageDialog(JOptionPane.getFrameForComponent(null),
                        Resources.getString(Labels.class, "labelfileerror.message", new Object[] { labelPath }),
                        Resources.getString(Labels.class, "labelfileerror.title"), JOptionPane.ERROR_MESSAGE);
                }

            }

            @Override
            public void statusChanged(Port<InputPortStatus> port, InputPortStatus status) {
            }

            @Override
            public void testButtonPressed(Port<InputPortStatus> output) {
            }

            @Override
            public void configChanged(Port<InputPortStatus> port) {

            }

            @Override
            public void changePortType(LcOutputType portType, InputPort port) {

                LOGGER.info("The port type will change to: {}, port: {}", portType, port);

                Map<Byte, PortConfigValue<?>> values = new LinkedHashMap<>();

                CommunicationFactory.getInstance().setPortParameters(mainModel.getSelectedNode().getNode(), port,
                    portType, values);

            }

            @Override
            public void valuesChanged(InputPort port, PortConfigKeys... portConfigKeys) {

                LOGGER.info("The port value are changed for port: {}, portConfigKeys: {}", port, portConfigKeys);

                Map<Byte, PortConfigValue<?>> values = new LinkedHashMap<>();

                for (PortConfigKeys key : portConfigKeys) {
                    switch (key) {
                        case BIDIB_PCFG_INPUT_CTRL:
                            IoBehaviourInputEnum inputBehaviour = port.getInputBehaviour();
                            values.put(BidibLibrary.BIDIB_PCFG_INPUT_CTRL,
                                new BytePortConfigValue(inputBehaviour.getType()));
                            break;
                        case BIDIB_PCFG_TICKS:
                            int switchOffTime = port.getSwitchOffTime();
                            values.put(BidibLibrary.BIDIB_PCFG_TICKS,
                                new BytePortConfigValue(ByteUtils.getLowByte(switchOffTime)));
                            break;
                        default:
                            LOGGER.warn("Unsupported port config key detected: {}", key);
                            break;
                    }
                }

                // don't set the port type param to not send BIDIB_PCFG_RECONFIG
                CommunicationFactory.getInstance().setPortParameters(mainModel.getSelectedNode().getNode(), port, null,
                    values);
            }
        });
    }

    @Override
    public void listChanged() {
        LOGGER.debug("The port list has changed.");
        super.listChanged();

        boolean hasPortIdentifiers = false;

        List<InputPort> ports = new LinkedList<>();
        ports.addAll(getPorts());

        LOGGER.info("Number of ports: {}", ports.size());

        synchronized (ports) {
            for (InputPort port : ports) {
                if (port.isRemappingEnabled()) {
                    hasPortIdentifiers = true;
                    break;
                }
            }
        }

        // keep the column index of the column to insert in the view
        int viewColumnIndex = InputPortTableModel.COLUMN_IO_BEHAVIOUR;

        if (mainModel.getSelectedNode() != null) {
            LOGGER.info("A node is selected.");
            boolean hasInputPortConfig = false;
            Node node = mainModel.getSelectedNode();
            if (node.getNode().isPortFlatModelAvailable() && CollectionUtils.isNotEmpty(node.getEnabledInputPorts())) {
                LOGGER.info("Check if the at least one input port has the input port config available.");
                for (InputPort port : node.getEnabledInputPorts()) {
                    hasInputPortConfig = port.isHasInputPortConfig();
                    if (hasInputPortConfig) {
                        // found one -> show the column
                        break;
                    }
                }
            }
            LOGGER.info("List has changed, hasPortIdentifiers: {}, hasInputPortConfig: {}", hasPortIdentifiers,
                hasInputPortConfig);

            viewColumnIndex =
                table.setColumnVisible(InputPortTableModel.COLUMN_IO_BEHAVIOUR, viewColumnIndex, hasPortIdentifiers);
            viewColumnIndex =
                table.setColumnVisible(InputPortTableModel.COLUMN_SWITCH_OFF_TIME, viewColumnIndex, hasPortIdentifiers);
        }

        // show/hide the port identifiers
        table.setColumnVisible(InputPortTableModel.COLUMN_PORT_IDENTIFIER, viewColumnIndex, hasPortIdentifiers);
    }

    @Override
    protected void refreshPorts() {
        LOGGER.info("refresh the ports.");

        Node node = mainModel.getSelectedNode();
        if (node != null) {
            if (node.getNode().isPortFlatModelAvailable()) {
                LOGGER.debug("The current node supports the flat port model.");
                if (CollectionUtils.isNotEmpty(node.getGenericPorts())) {
                    LOGGER.debug("Get the input ports in the model.");
                    mainModel.getInputPorts();
                }
                else {
                    LOGGER.info(
                        "The node supports flat port model but no generic ports are available. Skip get input ports.");
                }
            }
            else {
                mainModel.getInputPorts();
            }
        }
    }
}
