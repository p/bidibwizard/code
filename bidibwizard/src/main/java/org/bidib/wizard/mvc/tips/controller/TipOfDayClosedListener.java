package org.bidib.wizard.mvc.tips.controller;

public interface TipOfDayClosedListener {

    /**
     * The tip of day was closed.
     */
    void closed();

}
