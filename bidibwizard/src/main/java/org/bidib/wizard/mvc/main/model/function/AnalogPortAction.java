package org.bidib.wizard.mvc.main.model.function;

import org.bidib.jbidibc.exchange.lcmacro.AnalogActionType;
import org.bidib.jbidibc.exchange.lcmacro.AnalogPortActionType;
import org.bidib.jbidibc.exchange.lcmacro.AnalogPortPoint;
import org.bidib.jbidibc.exchange.lcmacro.LcMacroPointType;
import org.bidib.wizard.comm.AnalogPortStatus;
import org.bidib.wizard.mvc.main.model.AnalogPort;

public class AnalogPortAction extends SimplePortAction<AnalogPortStatus, AnalogPort> {
    public AnalogPortAction() {
        this(AnalogPortStatus.START);
    }

    public AnalogPortAction(AnalogPortStatus action) {
        this(action, null, 0, 0);
    }

    public AnalogPortAction(AnalogPort port, int delay, int value) {
        this(AnalogPortStatus.START, port, delay, value);
    }

    public AnalogPortAction(AnalogPortStatus action, AnalogPort port, int delay, int value) {
        super(action, KEY_ANALOG, port, delay, value);
    }

    public String getDebugString() {
        int id = 0;

        if (getPort() != null) {
            id = getPort().getId();
        }
        return "@" + getDelay() + " Analog:" + String.format("%02d", id) + "->" + getValue();
    }

    @Override
    public LcMacroPointType toLcMacroPoint() {
        AnalogPortPoint analogPortPoint = new AnalogPortPoint();
        analogPortPoint.setDelay(getDelay());
        AnalogPortActionType analogPortAction = new AnalogPortActionType();
        analogPortAction.setAction(AnalogActionType.fromValue(getAction().name()));
        analogPortAction.setValue(getValue());
        analogPortPoint.setAnalogPortActionType(analogPortAction);
        analogPortPoint.setOutputNumber(getPort().getId());
        return analogPortPoint;
    }
}
