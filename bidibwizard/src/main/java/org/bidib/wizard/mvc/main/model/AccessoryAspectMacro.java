package org.bidib.wizard.mvc.main.model;

import org.bidib.wizard.common.locale.Resources;

public class AccessoryAspectMacro implements AccessoryAspect {

    private MacroRef macro;

    private final int index;

    private String label;

    private boolean immutableAccessory;

    public AccessoryAspectMacro(int index, MacroRef macro) {
        this.index = index;
        this.macro = macro;
    }

    /**
     * @return the index
     */
    public int getIndex() {
        return index;
    }

    /**
     * @return the macro
     */
    public MacroRef getMacro() {
        return macro;
    }

    /**
     * @param macro
     *            the macro to set
     */
    public void setMacro(MacroRef macro) {
        this.macro = macro;
    }

    /**
     * @return the label
     */
    public String getLabel() {
        return label;
    }

    /**
     * @param label
     *            the label to set
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * @return the immutableAccessory
     */
    public boolean isImmutableAccessory() {
        return immutableAccessory;
    }

    /**
     * @param immutableAccessory
     *            the immutableAccessory to set
     */
    public void setImmutableAccessory(boolean immutableAccessory) {
        this.immutableAccessory = immutableAccessory;
    }

    @Override
    public String toString() {
        if (label != null) {
            return label;
        }

        return Resources.getString(AccessoryTableModel.class, "aspect") + "_" + index;
    }
}
