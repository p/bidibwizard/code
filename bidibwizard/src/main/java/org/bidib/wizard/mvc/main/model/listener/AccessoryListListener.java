package org.bidib.wizard.mvc.main.model.listener;

public interface AccessoryListListener {
    /**
     * The selected accessory has changed.
     * 
     * @param accessoryId
     *            the id of the changed accessory
     */
    void accessoryChanged(int accessoryId);

    /**
     * The accessories have been changed.
     */
    void listChanged();

    /**
     * The pending changes flag has been changed.
     */
    void pendingChangesChanged();
}
