package org.bidib.wizard.mvc.main.controller;

import org.bidib.jbidibc.core.enumeration.CommandStationState;
import org.bidib.wizard.mvc.main.model.Node;

public interface CommandStationService {

    /**
     * Switch command station on.
     * 
     * @param node
     *            the node
     * @param ignoreWatchDog
     *            if <code>true</code> the watchdog is ignored, if <code>false</code> the watchdog task is started
     */
    void switchOn(final Node node, boolean ignoreWatchDog);

    /**
     * Switch the command station off.
     * 
     * @param node
     *            the node
     */
    void switchOff(final Node node);

    /**
     * Switch the command station to stop state.
     * 
     * @param node
     *            the node
     * @param softStop
     *            if <code>true</code> the softstop is performed (send FS 0 to all decoders, turn DCC off), if
     *            <code>false</code> the command station turns DCC off immediately
     */
    void switchStop(final Node node, boolean softStop);

    /**
     * Set the new command station state.
     * 
     * @param node
     *            the node
     * @param commandStationState
     *            the new command station state
     */
    void setCommandStationState(final Node node, CommandStationState commandStationState);

    /**
     * Clear the complete loco buffer.
     * 
     * @param node
     *            the node
     */
    void clearLocoBuffer(final Node node);

    /**
     * The command station state has changed.
     * 
     * @param node
     *            the node
     * @param status
     *            the signalled status
     */
    void signalStateChanged(final Node node, CommandStationState status);

    /**
     * A node was removed.
     * 
     * @param node
     *            the node
     */
    void signalNodeRemoved(final Node node);

    /**
     * Stop all watchdog tasks.
     */
    void stopAllWatchDogTasks();
}
