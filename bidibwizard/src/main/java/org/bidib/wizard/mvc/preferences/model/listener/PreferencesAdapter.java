package org.bidib.wizard.mvc.preferences.model.listener;

import java.util.Date;

/**
 * The PreferencesAdapter is the default implementation of PresenceListener. Use this class as base class and only
 * implement the required methods.
 */
public abstract class PreferencesAdapter implements PreferencesListener {

    @Override
    public void prevSelectedSerialSymLinkChanged(String prevSelectedSerialSymLink) {
        // intentionally empty implementation
    }

    @Override
    public void prevSelectedComPortChanged(String prevSelectedComPort) {
        // intentionally empty implementation
    }

    @Override
    public void prevSelectedUdpHostChanged(String prevSelectedUdpHost) {
        // intentionally empty implementation
    }

    @Override
    public void prevSelectedTcpHostChanged(String prevSelectedTcpHost) {
        // intentionally empty implementation
    }

    @Override
    public void serialEnabledChanged(boolean serialEnabled) {
        // intentionally empty implementation
    }

    @Override
    public void udpEnabledChanged(boolean udpEnabled) {
        // intentionally empty implementation
    }

    @Override
    public void tcpEnabledChanged(boolean tcpEnabled) {
        // intentionally empty implementation
    }

    @Override
    public void plainTcpEnabledChanged(boolean tcpEnabled) {
        // intentionally empty implementation
    }

    @Override
    public void startTimeChanged(Date startTime) {
        // intentionally empty implementation
    }

    @Override
    public void timeFactorChanged(int timeFactor) {
        // intentionally empty implementation
    }

    @Override
    public void resetReconnectDelayChanged(int resetReconnectDelay) {
        // intentionally empty implementation
    }

    @Override
    public void responseTimeoutChanged(int responseTimeout) {
        // intentionally empty implementation
    }

    @Override
    public void ptModeDoNotConfirmSwitchChanged(boolean ptModeDoNotConfirmSwitch) {
        // intentionally empty implementation
    }

    @Override
    public void showBoosterTableChanged(boolean powerUser) {
        // intentionally empty implementation
    }

    @Override
    public void propertyChanged(String propertyName, Object oldValue, Object newValue) {
        // intentionally empty implementation
    }
}
