package org.bidib.wizard.mvc.firmware.model;

import java.util.List;

public class FirmwareUpdatePart {
    private List<String> firmwareContent;

    private int destination;

    private String filename;

    /**
     * @param filename
     *            the firmware filename
     * @param firmwareContent
     * @param destination
     */
    public FirmwareUpdatePart(String filename, List<String> firmwareContent, int destination) {
        this.filename = filename;
        this.firmwareContent = firmwareContent;
        this.destination = destination;
    }

    /**
     * @return the firmwareContent
     */
    public List<String> getFirmwareContent() {
        return firmwareContent;
    }

    /**
     * @param firmwareContent
     *            the firmwareContent to set
     */
    public void setFirmwareContent(List<String> firmwareContent) {
        this.firmwareContent = firmwareContent;
    }

    /**
     * @return the destination
     */
    public int getDestination() {
        return destination;
    }

    /**
     * @param destination
     *            the destination to set
     */
    public void setDestination(int destination) {
        this.destination = destination;
    }

    /**
     * @return the filename
     */
    public String getFilename() {
        return filename;
    }

    /**
     * @param filename
     *            the filename to set
     */
    public void setFilename(String filename) {
        this.filename = filename;
    }
}
