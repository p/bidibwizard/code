package org.bidib.wizard.mvc.error.model;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Collection;
import java.util.LinkedList;

import org.bidib.wizard.mvc.error.model.listener.ErrorListener;

public class ErrorModel {
    private final Collection<ErrorListener> errorListeners = new LinkedList<ErrorListener>();

    private Throwable error = null;

    private String stackTrace = " ";

    public void addErrorListener(ErrorListener l) {
        errorListeners.add(l);
    }

    private void fireErrorChanged(String errorMessage, String stackTrace) {
        for (ErrorListener l : errorListeners) {
            l.errorChanged(errorMessage, stackTrace);
        }
    }

    public Throwable getError() {
        return error;
    }

    public String getStackTrace(Throwable throwable) {
        Writer result = new StringWriter();
        PrintWriter printWriter = new PrintWriter(result);

        throwable.printStackTrace(printWriter);
        return result.toString();
    }

    public void setError(Throwable error) {
        this.error = error;

        String errorStackTrace = getStackTrace(error);

        if ((errorStackTrace != null && !errorStackTrace.equals(stackTrace)) || errorStackTrace == null
            && stackTrace != null) {
            stackTrace = errorStackTrace;

            Throwable cause = error.getCause();

            fireErrorChanged((cause != null ? cause.toString() : error.toString()), errorStackTrace);
        }
    }
}
