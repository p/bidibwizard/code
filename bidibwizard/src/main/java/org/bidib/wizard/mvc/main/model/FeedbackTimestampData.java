package org.bidib.wizard.mvc.main.model;

public class FeedbackTimestampData {
    private long timestamp;

    /**
     * @param timestamp
     *            the timestamp to set
     */
    public FeedbackTimestampData(long timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * @return the timestamp
     */
    public long getTimestamp() {
        return timestamp;
    }

    /**
     * @param timestamp
     *            the timestamp to set
     */
    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String toString() {
        StringBuilder result = new StringBuilder();

        result.append("[").append(timestamp).append("]");
        return result.toString();
    }
}
