package org.bidib.wizard.script;

import org.bidib.wizard.script.engine.ScriptEngine.ScriptStatus;

public interface ScriptEngineListener<T extends Scripting> {

    /**
     * @param command
     *            the currently executed command
     */
    void currentCommandChanged(final ScriptCommand<T> command);

    /**
     * @param scriptStatus
     *            the new script status
     */
    void scriptStatusChanged(final ScriptStatus scriptStatus);
}
