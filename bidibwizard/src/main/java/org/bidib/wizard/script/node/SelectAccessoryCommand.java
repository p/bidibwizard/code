package org.bidib.wizard.script.node;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.bidib.jbidibc.core.Feature;
import org.bidib.jbidibc.core.enumeration.FeatureEnum;
import org.bidib.jbidibc.core.utils.NodeUtils;
import org.bidib.wizard.highlight.BidibScriptScanner;
import org.bidib.wizard.highlight.Scanner;
import org.bidib.wizard.highlight.Token;
import org.bidib.wizard.highlight.TokenTypes;
import org.bidib.wizard.mvc.main.model.Accessory;
import org.bidib.wizard.mvc.main.model.MacroRef;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.script.view.NodeScripting;
import org.bidib.wizard.mvc.script.view.ScriptParser;
import org.bidib.wizard.script.AbstractScriptCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SelectAccessoryCommand extends AbstractScriptCommand<NodeScripting> {

    private static final Logger LOGGER = LoggerFactory.getLogger(SelectAccessoryCommand.class);

    public static final String KEY = "selectAccessory";

    private Long uuid;

    private String label;

    private Integer accessoryNumber;

    private ConfigAccessoryCommand configAccessory;

    private List<AddAspectCommand> aspects;

    protected SelectAccessoryCommand() {
        super(KEY);
    }

    public SelectAccessoryCommand(Long uuid) {
        super(KEY);
        this.uuid = uuid;
    }

    /**
     * @return the accessoryNumber
     */
    public Integer getAccessoryNumber() {
        return accessoryNumber;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * @param accessoryNumber
     *            the accessoryNumber to set
     */
    public void setAccessoryNumber(Integer accessoryNumber) {
        this.accessoryNumber = accessoryNumber;
    }

    /**
     * @return the configAccessory
     */
    public ConfigAccessoryCommand getConfigAccessory() {
        return configAccessory;
    }

    /**
     * @param configAccessory
     *            the configAccessory to set
     */
    public void setConfigAccessory(ConfigAccessoryCommand configAccessory) {
        this.configAccessory = configAccessory;
    }

    public void addAspect(AddAspectCommand aspect) {
        if (aspects == null) {
            aspects = new LinkedList<>();
        }
        LOGGER.info("Add new aspect: {}", aspect);
        aspects.add(aspect);
    }

    public List<AddAspectCommand> getAspects() {
        return aspects;
    }

    @Override
    public void parse(String commandLine) {
        LOGGER.info("Parse the command line: {}", commandLine);
    }

    public void scan(Scanner scanner, int index, final Map<String, Object> context) {

        for (int i = index + 1; i < scanner.size(); i++) {
            Token token = scanner.getToken(i);
            LOGGER.info("scan, current index: {}, token symbol: {}, name: {}", i, token.symbol.type, token.symbol.name);
            switch (token.symbol.type) {
                case TokenTypes.KEYWORD2:
                    switch (token.symbol.name) {
                        // case BidibScriptScanner.KEY2_LABEL:
                        case BidibScriptScanner.KEY2_NAME:
                            // label detected
                            StringValueCallback labelAware = new StringValueCallback() {

                                @Override
                                public void setString(String value) {
                                    LOGGER.info("Set the accessory name: {}", value);
                                    setLabel(value);
                                }

                                @Override
                                public void appendString(String value) {
                                    String label = getLabel();
                                    if (label == null) {
                                        setLabel(value);
                                    }
                                    else {
                                        setLabel(label + value);
                                    }
                                }
                            };
                            i += NodeScriptUtils.parseLabel(scanner, i, labelAware, context);

                            // get the accessory id from the accessory name from the map of accessory labels
                            Object value = getLabel();
                            if (value instanceof String) {
                                String accessoryName = (String) value;
                                // get the corresponding accessory number
                                Map<Integer, String> accessoryLabels =
                                    (Map<Integer, String>) context.get(ScriptParser.KEY_ACCESSORY_LABELS);

                                Integer accessoryId = MapUtils.invertMap(accessoryLabels).get(accessoryName);
                                if (accessoryId != null) {
                                    value = accessoryId;
                                }
                                else {
                                    LOGGER.warn("No corresponding accessory number found for accessory name: {}",
                                        accessoryName);
                                }
                            }

                            LOGGER.info("Set the accessory number: {}", value);
                            if (value != null) {
                                setAccessoryNumber(Integer.valueOf(value.toString()));
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case TokenTypes.NUMBER:
                    // set the accessory number
                    setAccessoryNumber(Integer.valueOf(token.symbol.name));
                    break;
                case TokenTypes.VARIABLE:
                    if (i == 2) {
                        // set the accessory number
                        LOGGER.info("Set the accessory number based on a variable: {}", token.symbol.name);
                        String variable = token.symbol.name;
                        Object value = context.get(variable);
                        if (value == null) {
                            // not found -> use the variable
                            value = variable;
                        }

                        // get the accessory id from the accessory name from the map of accessory labels
                        if (value instanceof String) {
                            String accessoryName = (String) value;
                            // get the corresponding accessory number
                            Map<Integer, String> accessoryLabels =
                                (Map<Integer, String>) context.get(ScriptParser.KEY_ACCESSORY_LABELS);

                            Integer accessoryId = MapUtils.invertMap(accessoryLabels).get(accessoryName);
                            if (accessoryId != null) {
                                value = accessoryId;
                            }
                            else {
                                LOGGER.warn("No corresponding accessory number found for accessory name: {}",
                                    accessoryName);
                            }
                        }

                        setAccessoryNumber(Integer.valueOf(value.toString()));
                    }
                default:
                    break;
            }
        }
    }

    @Override
    protected void internalExecute(final NodeScripting scripting, final Map<String, Object> context) {
        LOGGER.info("Select the accessory: {}", this);

        // create a accessory from the provided data
        Node selectedNode = (Node) context.get(ScriptParser.KEY_SELECTED_NODE);

        Map<Integer, String> accessoryLabels = (Map<Integer, String>) context.get(ScriptParser.KEY_ACCESSORY_LABELS);

        Feature feature =
            NodeUtils.getFeature(FeatureEnum.FEATURE_ACCESSORY_MACROMAPPED, selectedNode.getNode().getFeatures());
        // set the macrosize from the device
        Accessory accessory = new Accessory();
        boolean accessoryMacroMapped = false;
        if (feature != null) {
            accessory.setMacroSize(feature.getValue());
            accessoryMacroMapped = (feature.getValue() > 0);
        }
        else {
            LOGGER.info("Set default macro size of 16 for accessoryNumber: {}", accessoryNumber);
            accessory.setMacroSize(16);
        }
        accessory.setLabel(accessoryLabels.get(accessoryNumber));
        accessory.setId(accessoryNumber);

        if (CollectionUtils.isNotEmpty(getAspects())) {
            for (AddAspectCommand command : getAspects()) {

                // replace all placeholders in the aspects
                command.prepareAccessory(context);

                // check if the aspect is valid
                if (command.getMacroNumber() == null && accessoryMacroMapped) {
                    LOGGER.warn("The aspect is not valid because the macro number is not available.");
                    throw new IllegalArgumentException(
                        "The aspect is not valid because the macro number is not available.");
                }
                accessory.addAspect(new MacroRef(command.getMacroNumber()));
            }
        }
        else {
            LOGGER.info("No aspects provided.");
        }

        if (configAccessory != null) {
            LOGGER.info("The accessory has config to set: {}", configAccessory);
            Integer startup = configAccessory.getAccessoryConfig().get("startup");
            if (startup != null) {
                accessory.setStartupState(startup);
            }
        }

        scripting.setAccessory(uuid, accessory);
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[");
        sb.append(getClass().getSimpleName());
        sb.append(", uuid: ").append(uuid);
        sb.append(", accessoryNumber: ").append(accessoryNumber);
        sb.append(", aspects: ");
        if (CollectionUtils.isNotEmpty(aspects)) {
            for (AddAspectCommand step : aspects) {
                sb.append("\r\n").append(step);
            }
        }
        else {
            sb.append("<none>");
        }
        sb.append("]");
        return sb.toString();
    }
}
