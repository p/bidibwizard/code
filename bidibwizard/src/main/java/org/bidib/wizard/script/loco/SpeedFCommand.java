package org.bidib.wizard.script.loco;

import java.util.Map;
import java.util.Scanner;

import org.bidib.wizard.mvc.loco.view.LocoViewScripting;
import org.bidib.wizard.script.AbstractScriptCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The speed forward command.
 */
public class SpeedFCommand extends AbstractScriptCommand<LocoViewScripting> {
    private static final Logger LOGGER = LoggerFactory.getLogger(SpeedFCommand.class);

    public static final String KEY = "SPEEDV";

    private int speed;

    public SpeedFCommand() {
        super(KEY);
    }

    @Override
    public void parse(String commandLine) {
        Scanner scanner = new Scanner(commandLine);
        if (!getKey().equals(scanner.next())) {
            LOGGER.info("Invalid command is scanned, key does not match.");
        }
        line = commandLine.trim();

        speed = scanner.nextInt();
        scanner.close();

        LOGGER.info("Parsed command, speed: {}", speed);
    }

    public int getSpeed() {
        return speed;
    }

    @Override
    protected void internalExecute(final LocoViewScripting scripting, final Map<String, Object> context) {
        LOGGER.info("Set the speed: {}", speed);
        scripting.setSpeed(speed);
    }
}
