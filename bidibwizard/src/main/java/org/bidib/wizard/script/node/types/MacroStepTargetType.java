package org.bidib.wizard.script.node.types;

public class MacroStepTargetType extends TargetType {
    private static final long serialVersionUID = 1L;

    public MacroStepTargetType() {
        super(ScriptingTargetType.MACROSTEP);
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[MacroStepTargetType, ");
        sb.append(super.toString());
        sb.append("]");
        return sb.toString();
    }
}
