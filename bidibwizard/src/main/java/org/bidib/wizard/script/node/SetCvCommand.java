package org.bidib.wizard.script.node;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.bidib.wizard.mvc.script.view.NodeScripting;
import org.bidib.wizard.script.AbstractScriptCommand;
import org.bidib.wizard.script.node.types.CvType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SetCvCommand extends AbstractScriptCommand<NodeScripting> {
    private static final Logger LOGGER = LoggerFactory.getLogger(SetCvCommand.class);

    public static final String KEY = "setCv";

    private Long uuid;

    private CvType cvType;

    private List<CvType> cvTypes;

    protected SetCvCommand() {
        super(KEY);
    }

    public SetCvCommand(Long uuid, CvType cvType) {
        super(KEY);
        this.uuid = uuid;
        this.cvType = cvType;
    }

    /**
     * Append a CV to the existing command
     * 
     * @param cvType
     *            the new CV
     */
    public void appendCV(CvType cvType) {

        if (cvTypes == null) {
            cvTypes = new ArrayList<>();
            if (this.cvType != null) {
                // move the stored CV to the list
                cvTypes.add(this.cvType);
                this.cvType = null;
            }
        }

        cvTypes.add(cvType);
    }

    @Override
    public void parse(String commandLine) {
        LOGGER.info("Parse the command line: {}", commandLine);
    }

    @Override
    protected void internalExecute(final NodeScripting scripting, final Map<String, Object> context) {
        if (cvTypes == null) {
            LOGGER.info("Set the CV.");
            scripting.setCv(uuid, cvType);
        }
        else {
            LOGGER.info("Set the CV values.");
            scripting.setCv(uuid, cvTypes.toArray(new CvType[0]));
        }
    }

}
