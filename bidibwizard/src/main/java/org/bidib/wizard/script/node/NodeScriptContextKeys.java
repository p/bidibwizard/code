package org.bidib.wizard.script.node;

public interface NodeScriptContextKeys {

    static final String KEY_LANG = "lang";

    static final String KEY_NODESCRIPTPROCESSOR = "nodeScriptProcessor";

    static final String KEY_SELECTED_NODESCRIPT_PATH = "selectedNodescriptPath";

    static final String KEY_SCRIPTEXECUTIONSTATUS = "scriptExecutionStatus";

    static final String KEY_SCRIPTEXECUTIONSTATUSLOCK = "scriptExecutionStatusLock";

    static final String KEY_SELECTED_CORE_NODE = "selectedCoreNode";
}
