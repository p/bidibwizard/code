package org.bidib.wizard.script.node;

import java.util.Map;

import org.bidib.wizard.mvc.script.view.NodeScripting;
import org.bidib.wizard.script.AbstractScriptCommand;
import org.bidib.wizard.script.node.types.TargetType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SetLabelCommand extends AbstractScriptCommand<NodeScripting> {
    private static final Logger LOGGER = LoggerFactory.getLogger(SetLabelCommand.class);

    public static final String KEY = "setLabel";

    private Long uuid;

    private TargetType portType;

    protected SetLabelCommand() {
        super(KEY);
    }

    public SetLabelCommand(Long uuid, TargetType portType) {
        super(KEY);
        this.uuid = uuid;
        this.portType = portType;
    }

    @Override
    public void parse(String commandLine) {
        LOGGER.info("Parse the command line: {}", commandLine);
    }

    @Override
    protected void internalExecute(final NodeScripting scripting, final Map<String, Object> context) {
        LOGGER.info("Set the label.");
        scripting.setLabel(uuid, portType);
    }

}
