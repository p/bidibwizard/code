package org.bidib.wizard.script;

public interface InitializingCommand {
    /**
     * initialize the command after the properties are set.
     */
    void afterPropertiesSet();
}
