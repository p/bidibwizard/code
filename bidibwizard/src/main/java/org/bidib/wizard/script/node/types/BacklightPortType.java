package org.bidib.wizard.script.node.types;

public class BacklightPortType extends TargetType {
    private static final long serialVersionUID = 1L;

    public BacklightPortType() {
        super(ScriptingTargetType.BACKLIGHTPORT);
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[BacklightPortType, ");
        sb.append(super.toString());
        sb.append("]");
        return sb.toString();
    }
}
