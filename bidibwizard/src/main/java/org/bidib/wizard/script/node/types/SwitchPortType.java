package org.bidib.wizard.script.node.types;

public class SwitchPortType extends TargetType {
    private static final long serialVersionUID = 1L;

    public SwitchPortType() {
        super(ScriptingTargetType.SWITCHPORT);
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[SwitchPortType, ");
        sb.append(super.toString());
        sb.append("]");
        return sb.toString();
    }
}
