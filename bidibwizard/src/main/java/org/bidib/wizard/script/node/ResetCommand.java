package org.bidib.wizard.script.node;

import java.util.Map;

import org.bidib.wizard.mvc.script.view.NodeScripting;
import org.bidib.wizard.script.AbstractScriptCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ResetCommand extends AbstractScriptCommand<NodeScripting> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ResetCommand.class);

    public static final String KEY = "reset";

    private Long uuid;

    protected ResetCommand() {
        super(KEY);
    }

    public ResetCommand(Long uuid) {
        super(KEY);
        this.uuid = uuid;
    }

    @Override
    public void parse(String commandLine) {
        LOGGER.info("Set the reset: {}", commandLine);
        // label = commandLine;
    }

    @Override
    public void execute(final NodeScripting scripting, final Map<String, Object> context) {
        LOGGER.info("Reset node with uuid: {}", uuid);

        try {
            scripting.resetNode(uuid);
        }
        catch (Exception ex) {
            LOGGER.warn("Reset node failed, uuid: {}", uuid, ex);
        }
    }

    @Override
    protected void internalExecute(NodeScripting scripting, final Map<String, Object> context) {
    }
}
