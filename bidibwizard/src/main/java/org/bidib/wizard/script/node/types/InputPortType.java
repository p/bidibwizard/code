package org.bidib.wizard.script.node.types;

public class InputPortType extends TargetType {
    private static final long serialVersionUID = 1L;

    public InputPortType() {
        super(ScriptingTargetType.INPUTPORT);
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[InputPortType, ");
        sb.append(super.toString());
        sb.append("]");
        return sb.toString();
    }
}
