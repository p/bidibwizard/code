package org.bidib.wizard.script.node.types;

public class AnalogPortType extends TargetType {
    private static final long serialVersionUID = 1L;

    public AnalogPortType() {
        super(ScriptingTargetType.ANALOGPORT);
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[AnalogPortType, ");
        sb.append(super.toString());
        sb.append("]");
        return sb.toString();
    }
}
