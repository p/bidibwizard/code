package org.bidib.wizard.script.node;

import org.slf4j.LoggerFactory;

public interface StringValueCallback {

    void setString(String value);

    default void appendString(String value) {
        LoggerFactory.getLogger(StringValueCallback.class).warn("appendStrign is called in default method, value: {}",
            value);
    }
}
