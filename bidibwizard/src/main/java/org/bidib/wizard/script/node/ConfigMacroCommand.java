package org.bidib.wizard.script.node;

import java.util.HashMap;
import java.util.Map;

import org.bidib.wizard.highlight.BidibScriptScanner;
import org.bidib.wizard.highlight.Scanner;
import org.bidib.wizard.highlight.Token;
import org.bidib.wizard.highlight.TokenTypes;
import org.bidib.wizard.mvc.script.view.NodeScripting;
import org.bidib.wizard.script.AbstractScriptCommand;
import org.bidib.wizard.script.InitializingCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConfigMacroCommand extends AbstractScriptCommand<NodeScripting> implements InitializingCommand {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigMacroCommand.class);

    public static final String KEY = "configMacro";

    private Long uuid;

    private Map<String, Integer> macroConfig = new HashMap<String, Integer>();

    protected ConfigMacroCommand() {
        super(KEY);
    }

    public ConfigMacroCommand(Long uuid) {
        super(KEY);
        this.uuid = uuid;
    }

    public Map<String, Integer> getMacroConfig() {
        return macroConfig;
    }

    @Override
    public void parse(String commandLine) {
        LOGGER.info("Parse the command line: {}", commandLine);
    }

    public int scan(Scanner scanner, int index, final Map<String, Object> context) {

        int i = index + 1;
        for (; i < scanner.size(); i++) {
            Token token = scanner.getToken(i);
            LOGGER.info("scan, current index: {}, token symbol: {}, name: {}", i, token.symbol.type, token.symbol.name);
            switch (token.symbol.type) {
                case TokenTypes.KEYWORD2:
                    switch (token.symbol.name) {

                        case BidibScriptScanner.KEY2_REPEAT:
                            LOGGER.info("Repeat detected: {}", token.symbol.name);

                            NumberAware repeatCallback = new NumberAware() {
                                @Override
                                public void setNumber(Integer number) {
                                    LOGGER.info("Set the repeat: {}", number);

                                    macroConfig.put("repeat", number);
                                }
                            };
                            i = NodeScriptUtils.parseNumber(scanner, i, repeatCallback, context);

                            break;
                        case BidibScriptScanner.KEY2_SLOWDOWN:
                            LOGGER.info("Slowdown detected: {}", token.symbol.name);

                            NumberAware slowdownCallback = new NumberAware() {
                                @Override
                                public void setNumber(Integer number) {
                                    LOGGER.info("Set the slowdown: {}", number);

                                    macroConfig.put("slowdown", number);
                                }
                            };
                            i = NodeScriptUtils.parseNumber(scanner, i, slowdownCallback, context);
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }

            LOGGER.info("scan, i after parse: {}, last symbol name: {}", i, token.symbol.name);
        }

        return i;
    }

    @Override
    protected void internalExecute(final NodeScripting scripting, final Map<String, Object> context) {
        LOGGER.info("Set the macro config: {}", this);

    }

    public void afterPropertiesSet() {
        LOGGER.info("afterPropertiesSet is called: {}", this);

    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[");
        sb.append(getClass().getSimpleName());
        sb.append(", uuid: ").append(uuid);
        sb.append("]");
        return sb.toString();
    }
}
