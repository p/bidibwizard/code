package org.bidib.wizard.script.node.types;

public class AccessoryTargetType extends TargetType {
    private static final long serialVersionUID = 1L;

    public AccessoryTargetType() {
        super(ScriptingTargetType.ACCESSORY);
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[AccessoryTargetType, ");
        sb.append(super.toString());
        sb.append("]");
        return sb.toString();
    }
}
