package org.bidib.wizard.script.loco;

import java.util.Map;
import java.util.Scanner;

import org.bidib.wizard.mvc.loco.view.LocoViewScripting;
import org.bidib.wizard.script.AbstractScriptCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The speed backward command.
 */
public class WaitCommand extends AbstractScriptCommand<LocoViewScripting> {
    private static final Logger LOGGER = LoggerFactory.getLogger(WaitCommand.class);

    public static final String KEY = "WAIT";

    private int wait;

    public WaitCommand() {
        super(KEY);
    }

    @Override
    public void parse(String commandLine) {
        Scanner scanner = new Scanner(commandLine);
        if (!getKey().equals(scanner.next())) {
            LOGGER.info("Invalid command is scanned, key does not match.");
        }
        line = commandLine.trim();

        wait = scanner.nextInt();
        scanner.close();

        LOGGER.info("Parsed command, wait: {}", wait);
    }

    public int getWait() {
        return wait;
    }

    @Override
    public void execute(LocoViewScripting scripting, final Map<String, Object> context) {

        try {
            Thread.sleep(wait);
        }
        catch (InterruptedException e) {
            LOGGER.warn("Sleep was interrupted.");
        }
    }

    @Override
    protected void internalExecute(LocoViewScripting scripting, final Map<String, Object> context) {
    }
}
