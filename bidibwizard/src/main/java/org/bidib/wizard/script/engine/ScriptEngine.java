package org.bidib.wizard.script.engine;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import org.bidib.wizard.mvc.script.view.ScriptParser;
import org.bidib.wizard.script.ScriptCommand;
import org.bidib.wizard.script.ScriptEngineListener;
import org.bidib.wizard.script.Scripting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ScriptEngine<T extends Scripting> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ScriptEngine.class);

    private final Object scriptWorkerLock = new Object();

    private Thread scriptWorker;

    private AtomicBoolean scriptRunning = new AtomicBoolean(false);

    private AtomicBoolean scriptRepeating = new AtomicBoolean(false);

    private List<ScriptCommand<T>> scriptCommands;

    private T scripting;

    private final Map<String, Object> context;

    private List<ScriptEngineListener<T>> engineListeners = new LinkedList<ScriptEngineListener<T>>();

    public enum ScriptStatus {
        STOPPED, RUNNING, FINISHED, ABORTED, FINISHED_WITH_ERRORS
    }

    public ScriptEngine(T scripting, final Map<String, Object> context) {
        this.scripting = scripting;
        this.context = context;
    }

    public void addScriptEngineListener(ScriptEngineListener<T> listener) {
        engineListeners.add(listener);
    }

    public void removeScriptEngineListener(ScriptEngineListener<T> listener) {
        engineListeners.remove(listener);
    }

    public void setScriptRepeating(boolean repeating) {
        this.scriptRepeating.set(repeating);
    }

    public void setScriptCommands(List<ScriptCommand<T>> scriptCommands) {
        LOGGER.info("Set the new script commands.");
        synchronized (scriptWorkerLock) {
            if (scriptRunning.get()) {
                LOGGER.warn(
                    "The script engine is currently processing commands. Stop the engine before setting new commands.");
                throw new IllegalStateException(
                    "The script engine is currently processing commands. Stop the engine before setting new commands.");
            }
            this.scriptCommands = scriptCommands;
        }
    }

    public void startScript() {

        synchronized (scriptWorkerLock) {
            if (scriptCommands != null && scriptWorker == null) {
                LOGGER.info("Create and start scriptWorker.");
                scriptRunning.set(true);

                signalScriptStatus(ScriptStatus.RUNNING);

                scriptWorker = new Thread(new Runnable() {

                    @Override
                    public void run() {
                        LOGGER.info("Start execution of script commands.");

                        try {
                            do {
                                for (ScriptCommand<T> command : scriptCommands) {

                                    if (!scriptRunning.get()) {
                                        LOGGER.info("Script execution is stopped.");
                                        break;
                                    }

                                    updateCurrentCommand(command);
                                    command.execute(scripting, context);
                                }
                            }
                            while (scriptRepeating.get() && scriptRunning.get());

                            if (context.containsKey(ScriptParser.KEY_SCRIPT_ERRORS)) {
                                LOGGER.warn("Script errors detected!");
                                signalScriptStatus(ScriptStatus.FINISHED_WITH_ERRORS);
                            }
                            else {
                                signalScriptStatus(ScriptStatus.FINISHED);
                            }
                        }
                        catch (Exception ex) {
                            LOGGER.warn("Executing script command has failed. Set the scriptStatus to aborted.", ex);

                            signalScriptStatus(ScriptStatus.ABORTED);
                        }
                        finally {
                            LOGGER.info("Script worker has finished.");

                            // the script has finished
                            scriptRunning.set(false);
                        }
                    }
                });
                scriptWorker.start();
                LOGGER.info("Start script worker has passed.");
            }
            else if (scriptWorker != null) {
                LOGGER.warn("Script worker is still running.");
            }
        }
    }

    /**
     * Initiate stop the script worker but don't wait for termination.
     */
    public void stopScript() {
        stopScript(null);
    }

    /**
     * Initiate stop the script worker and wait at most <code>waitForTermination</code> for termination.
     * 
     * @param waitForTermination
     *            the time to wait in milliseconds
     */
    public void stopScript(Long waitForTermination) {
        LOGGER.info("Stop the script.");

        scriptRunning.set(false);

        synchronized (scriptWorkerLock) {
            if (scriptWorker != null) {
                LOGGER.info("Interrupt the script worker.");
                scriptWorker.interrupt();

                if (waitForTermination != null) {
                    long waitTime = waitForTermination.longValue();
                    LOGGER.info("Wait for termination of script worker for {}ms", waitTime);
                    try {
                        scriptWorker.join(waitTime);
                    }
                    catch (InterruptedException ex) {
                        LOGGER.warn("Wait for termination of script worker was interrupted.", ex);
                    }

                    // release the script worker instance
                    scriptWorker = null;
                }
            }
            else {
                LOGGER.info("Script worker is not available.");
            }
        }
    }

    private void signalScriptStatus(ScriptStatus scriptStatus) {
        for (ScriptEngineListener<T> listener : engineListeners) {
            listener.scriptStatusChanged(scriptStatus);
        }
    }

    private void updateCurrentCommand(final ScriptCommand<T> command) {
        for (ScriptEngineListener<T> listener : engineListeners) {
            listener.currentCommandChanged(command);
        }
    }
}
