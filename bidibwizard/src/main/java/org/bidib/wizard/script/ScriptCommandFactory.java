package org.bidib.wizard.script;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.bidib.wizard.script.loco.AddrCommand;
import org.bidib.wizard.script.loco.KeyCommand;
import org.bidib.wizard.script.loco.KeyShortCommand;
import org.bidib.wizard.script.loco.SpeedBCommand;
import org.bidib.wizard.script.loco.SpeedFCommand;
import org.bidib.wizard.script.loco.SpeedStepsCommand;
import org.bidib.wizard.script.loco.StopCommand;
import org.bidib.wizard.script.loco.StopEmergencyCommand;
import org.bidib.wizard.script.loco.WaitCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ScriptCommandFactory<T extends Scripting> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ScriptCommandFactory.class);

    private Map<String, Class<? extends ScriptCommand<?>>> scriptCommands =
        new HashMap<String, Class<? extends ScriptCommand<?>>>();

    public ScriptCommandFactory() {

        scriptCommands.put(AddrCommand.KEY, AddrCommand.class);
        scriptCommands.put(SpeedStepsCommand.KEY, SpeedStepsCommand.class);
        scriptCommands.put(SpeedFCommand.KEY, SpeedFCommand.class);
        scriptCommands.put(SpeedBCommand.KEY, SpeedBCommand.class);
        scriptCommands.put(WaitCommand.KEY, WaitCommand.class);
        scriptCommands.put(KeyCommand.KEY, KeyCommand.class);
        scriptCommands.put(KeyShortCommand.KEY, KeyShortCommand.class);
        scriptCommands.put(StopCommand.KEY, StopCommand.class);
        scriptCommands.put(StopEmergencyCommand.KEY, StopEmergencyCommand.class);
    }

    public ScriptCommand<T> parse(String line) {
        Scanner scanner = new Scanner(line);
        String key = scanner.next();
        ScriptCommand<T> command = getScriptCommand(key);
        if (command != null) {
            command.parse(line);
        }
        scanner.close();
        return command;
    }

    private ScriptCommand<T> getScriptCommand(String key) {
        Class<?> clazz = scriptCommands.get(key);
        if (clazz == null) {
            LOGGER.warn("Command is not registered: {}", key);
            return null;
        }
        try {
            return (ScriptCommand<T>) clazz.newInstance();
        }
        catch (InstantiationException | IllegalAccessException e) {
            LOGGER.warn("Create script command class failed.", e);
        }
        return null;
    }
}
