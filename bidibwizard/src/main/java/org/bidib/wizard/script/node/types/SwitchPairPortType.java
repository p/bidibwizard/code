package org.bidib.wizard.script.node.types;

public class SwitchPairPortType extends TargetType {
    private static final long serialVersionUID = 1L;

    public SwitchPairPortType() {
        super(ScriptingTargetType.SWITCHPAIRPORT);
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[SwitchPairPortType, ");
        sb.append(super.toString());
        sb.append("]");
        return sb.toString();
    }
}
