package org.bidib.wizard.script.switching;

import java.util.Map;
import java.util.Scanner;

import org.bidib.wizard.mvc.main.view.component.BulkSwitchFunctionsScripting;
import org.bidib.wizard.script.AbstractScriptCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The motor port command.
 */
public class MotorPortCommand<T extends BulkSwitchFunctionsScripting> extends AbstractScriptCommand<T> {
    private static final Logger LOGGER = LoggerFactory.getLogger(MotorPortCommand.class);

    public static final String KEY = "MOTOR";

    private int portNum;

    private int speed;

    public MotorPortCommand() {
        super(KEY);
    }

    @Override
    public void parse(String commandLine) {
        Scanner scanner = new Scanner(commandLine);
        if (!getKey().equals(scanner.next())) {
            LOGGER.info("Invalid command is scanned, key does not match.");
        }
        line = commandLine.trim();

        portNum = scanner.nextInt();

        speed = scanner.nextInt();

        scanner.close();

        LOGGER.info("Parsed command, portNum: {}, speed: {}", portNum, speed);
    }

    public int getPortNum() {
        return portNum;
    }

    @Override
    protected void internalExecute(final T scripting, final Map<String, Object> context) {
        LOGGER.info("Set the value, portNum: {}, speed: {}", portNum, speed);
        scripting.sendPortValueAction(portNum, speed);
    }
}
