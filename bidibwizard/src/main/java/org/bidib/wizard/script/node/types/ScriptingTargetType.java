package org.bidib.wizard.script.node.types;

public enum ScriptingTargetType {

    // @formatter:off
    SWITCHPORT("switch"), INPUTPORT("input"), SERVOPORT("servo"), ANALOGPORT("analog"), 
    BACKLIGHTPORT("backlight"), FEEDBACKPORT("feedback"), FEEDBACKPOSITION("feedbackPosition"), 
    LIGHTPORT("light"), MOTORPORT("motor"), SOUNDPORT("sound"), SWITCHPAIRPORT("switchPair"), MACRO("macro"), 
    MACROSTEP("macrostep"), ACCESSORY("accessory"), CV("cv"), FLAG("flag");
    // @formatter:on

    private final String key;

    ScriptingTargetType(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }
}
