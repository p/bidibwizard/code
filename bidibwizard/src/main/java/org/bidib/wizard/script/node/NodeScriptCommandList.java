package org.bidib.wizard.script.node;

import java.util.List;

import org.bidib.wizard.mvc.script.view.NodeScripting;
import org.bidib.wizard.script.ScriptCommand;

import com.jgoodies.binding.beans.Model;

public class NodeScriptCommandList extends Model {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTYNAME_NODE_SCRIPT_COMMANDS = "nodeScriptCommands";

    public static final String PROPERTYNAME_EXECUTION_STATUS = "executionStatus";

    private List<ScriptCommand<NodeScripting>> nodeScriptCommands;

    public enum ExecutionStatus {
        pending, running, finished, aborted, finishedWithErrors;
    }

    private ExecutionStatus executionStatus;

    /**
     * @return the nodeScriptCommands
     */
    public List<ScriptCommand<NodeScripting>> getNodeScriptCommands() {
        return nodeScriptCommands;
    }

    /**
     * @param nodeScriptCommands
     *            the nodeScriptCommands to set
     */
    public void setNodeScriptCommands(List<ScriptCommand<NodeScripting>> nodeScriptCommands) {
        List<ScriptCommand<NodeScripting>> oldValue = this.nodeScriptCommands;

        this.nodeScriptCommands = nodeScriptCommands;
        firePropertyChange(PROPERTYNAME_NODE_SCRIPT_COMMANDS, oldValue, nodeScriptCommands);
    }

    /**
     * @return the executionStatus
     */
    public ExecutionStatus getExecutionStatus() {
        return executionStatus;
    }

    /**
     * @param executionStatus
     *            the executionStatus to set
     */
    public void setExecutionStatus(ExecutionStatus executionStatus) {
        ExecutionStatus oldValue = this.executionStatus;

        this.executionStatus = executionStatus;
        firePropertyChange(PROPERTYNAME_EXECUTION_STATUS, oldValue, executionStatus);
    }

    @Override
    public String toString() {
        return "NodeScriptCommandList, executionStatus: " + executionStatus;
    }
}
