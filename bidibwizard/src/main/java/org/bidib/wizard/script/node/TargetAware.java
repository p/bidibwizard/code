package org.bidib.wizard.script.node;

public interface TargetAware {

    void setTarget(Integer target);
}
