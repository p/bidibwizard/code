package org.bidib.wizard.script.loco;

import java.util.Map;
import java.util.Scanner;

import org.bidib.wizard.comm.SpeedSteps;
import org.bidib.wizard.mvc.loco.view.LocoViewScripting;
import org.bidib.wizard.script.AbstractScriptCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SpeedStepsCommand extends AbstractScriptCommand<LocoViewScripting> {
    private static final Logger LOGGER = LoggerFactory.getLogger(SpeedStepsCommand.class);

    public static final String KEY = "SPEEDSTEPS";

    private SpeedSteps speedSteps;

    public SpeedStepsCommand() {
        super(KEY);
    }

    @Override
    public void parse(String commandLine) {
        Scanner scanner = new Scanner(commandLine);
        if (!getKey().equals(scanner.next())) {
            LOGGER.info("Invalid command is scanned, key does not match.");
        }
        line = commandLine.trim();

        try {
            String dccSteps = "DCC" + scanner.next();
            speedSteps = SpeedSteps.valueOf(dccSteps);
        }
        catch (IllegalArgumentException ex) {
            LOGGER.warn("Parse speedSteps failed.", ex);
        }
        scanner.close();

        LOGGER.info("Parsed SpeedStepsCommand, speedSteps: {}", speedSteps);
    }

    public SpeedSteps getSpeedSteps() {
        return speedSteps;
    }

    @Override
    protected void internalExecute(final LocoViewScripting scripting, final Map<String, Object> context) {
        LOGGER.info("Set the speedSteps: {}", speedSteps);
        scripting.setSpeedSteps(speedSteps);
    }
}
