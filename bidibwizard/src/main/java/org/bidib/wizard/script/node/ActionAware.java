package org.bidib.wizard.script.node;

import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.mvc.main.model.function.Function;

public interface ActionAware {

    void setAction(BidibStatus action);

    Function<? extends BidibStatus> getFunction();
}
