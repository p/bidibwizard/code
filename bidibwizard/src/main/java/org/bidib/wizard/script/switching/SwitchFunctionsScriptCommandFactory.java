package org.bidib.wizard.script.switching;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.bidib.wizard.script.ScriptCommand;
import org.bidib.wizard.script.Scripting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SwitchFunctionsScriptCommandFactory<T extends Scripting> {
    private static final Logger LOGGER = LoggerFactory.getLogger(SwitchFunctionsScriptCommandFactory.class);

    private Map<String, Class<?>> scriptCommands = new HashMap<>();

    public SwitchFunctionsScriptCommandFactory() {

        scriptCommands.put(LightPortCommand.KEY, LightPortCommand.class);
        scriptCommands.put(SwitchPortCommand.KEY, SwitchPortCommand.class);
        scriptCommands.put(ServoPortCommand.KEY, ServoPortCommand.class);
        scriptCommands.put(WaitCommand.KEY, WaitCommand.class);
    }

    public ScriptCommand<T> parse(String line) {
        Scanner scanner = new Scanner(line);
        String key = scanner.next();
        ScriptCommand<T> command = getScriptCommand(key);
        if (command != null) {
            command.parse(line);
        }
        scanner.close();
        return command;
    }

    private ScriptCommand<T> getScriptCommand(String key) {
        Class<?> clazz = scriptCommands.get(key);
        if (clazz == null) {
            LOGGER.warn("Command is not registered: {}", key);
            return null;
        }
        try {
            return (ScriptCommand<T>) clazz.newInstance();
        }
        catch (InstantiationException | IllegalAccessException e) {
            LOGGER.warn("Create switch function script command class failed.", e);
        }
        return null;
    }
}
