package org.bidib.wizard.script.node.types;

public class MotorPortType extends TargetType {
    private static final long serialVersionUID = 1L;

    public MotorPortType() {
        super(ScriptingTargetType.MOTORPORT);
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[MotorPortType, ");
        sb.append(super.toString());
        sb.append("]");
        return sb.toString();
    }
}
