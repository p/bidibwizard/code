package org.bidib.wizard.script.node.types;

public class CvType {

    private Integer cvNumber;

    private String cvValue;

    /**
     * @return the cvNumber
     */
    public Integer getCvNumber() {
        return cvNumber;
    }

    /**
     * @param cvNumber
     *            the cvNumber to set
     */
    public void setCvNumber(Integer cvNumber) {
        this.cvNumber = cvNumber;
    }

    /**
     * @return the cvValue
     */
    public String getCvValue() {
        return cvValue;
    }

    /**
     * @param cvValue
     *            the cvValue to set
     */
    public void setCvValue(String cvValue) {
        this.cvValue = cvValue;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[CvType, cvNumber: ");
        sb.append(cvNumber).append(", cvValue: ").append(cvValue).append("]");
        return sb.toString();
    }
}
