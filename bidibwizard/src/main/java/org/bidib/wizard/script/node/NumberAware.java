package org.bidib.wizard.script.node;

public interface NumberAware {

    void setNumber(Integer number);
}
