package org.bidib.wizard.script;

import java.util.LinkedHashMap;
import java.util.Map;

import org.bidib.wizard.common.context.ApplicationContext;

public class DefaultScriptContext implements ApplicationContext {

    private Map<String, Object> registry = new LinkedHashMap<>();

    @Override
    public Object register(String key, Object content) {
        synchronized (registry) {
            return registry.put(key, content);
        }
    }

    @Override
    public Object unregister(String key) {
        synchronized (registry) {
            return registry.remove(key);
        }
    }

    @Override
    public Object get(String key) {
        synchronized (registry) {
            return registry.get(key);
        }
    }

    @Override
    public <T> T get(String key, Class<T> type) {
        return get(type, get(key));
    }

    protected <T> T get(Class<T> type, Object body) {
        // if same type
        if (type.isInstance(body)) {
            return type.cast(body);
        }
        return null;
    }

    /**
     * Get the inner map for legacy purposes.
     * 
     * @return the inner map
     */
    public Map<String, Object> getRegistry() {
        return registry;
    }
}
