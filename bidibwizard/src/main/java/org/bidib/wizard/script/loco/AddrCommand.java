package org.bidib.wizard.script.loco;

import java.util.Map;
import java.util.Scanner;

import org.bidib.wizard.mvc.loco.view.LocoViewScripting;
import org.bidib.wizard.script.AbstractScriptCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AddrCommand extends AbstractScriptCommand<LocoViewScripting> {
    private static final Logger LOGGER = LoggerFactory.getLogger(AddrCommand.class);

    public static final String KEY = "ADDR";

    private int decoderAddress;

    public AddrCommand() {
        super(KEY);
    }

    @Override
    public void parse(String commandLine) {
        Scanner scanner = new Scanner(commandLine);
        if (!getKey().equals(scanner.next())) {
            LOGGER.info("Invalid command is scanned, key does not match.");
        }
        line = commandLine.trim();

        decoderAddress = scanner.nextInt();
        scanner.close();

        LOGGER.info("Parsed SelectDecoderCommand, decoderAddress: {}", decoderAddress);
    }

    public int getDecoderAddress() {
        return decoderAddress;
    }

    @Override
    protected void internalExecute(final LocoViewScripting scripting, final Map<String, Object> context) {
        LOGGER.info("Set the decoder address: {}", decoderAddress);
        scripting.selectDecoderAddress(decoderAddress);
    }
}
