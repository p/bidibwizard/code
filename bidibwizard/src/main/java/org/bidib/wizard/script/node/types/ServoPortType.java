package org.bidib.wizard.script.node.types;

public class ServoPortType extends TargetType {
    private static final long serialVersionUID = 1L;

    public ServoPortType() {
        super(ScriptingTargetType.SERVOPORT);
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[ServoPortType, ");
        sb.append(super.toString());
        sb.append("]");
        return sb.toString();
    }
}
