package org.bidib.wizard.script.node.types;

public class LightPortType extends TargetType {
    private static final long serialVersionUID = 1L;

    public LightPortType() {
        super(ScriptingTargetType.LIGHTPORT);
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[LightPortType, ");
        sb.append(super.toString());
        sb.append("]");
        return sb.toString();
    }
}
