package org.bidib.wizard.script.node.types;

public class SoundPortType extends TargetType {
    private static final long serialVersionUID = 1L;

    public SoundPortType() {
        super(ScriptingTargetType.SOUNDPORT);
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[SoundPortType, ");
        sb.append(super.toString());
        sb.append("]");
        return sb.toString();
    }
}
