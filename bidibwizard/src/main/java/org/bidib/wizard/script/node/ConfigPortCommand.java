package org.bidib.wizard.script.node;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.port.BytePortConfigValue;
import org.bidib.jbidibc.core.port.Int16PortConfigValue;
import org.bidib.jbidibc.core.port.PortConfigValue;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.highlight.BidibScriptScanner;
import org.bidib.wizard.highlight.Scanner;
import org.bidib.wizard.highlight.Token;
import org.bidib.wizard.highlight.TokenTypes;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.bidib.wizard.mvc.main.model.Port;
import org.bidib.wizard.mvc.main.model.PortsProvider;
import org.bidib.wizard.mvc.main.model.function.Function;
import org.bidib.wizard.mvc.main.model.function.PortAction;
import org.bidib.wizard.mvc.script.view.NodeScripting;
import org.bidib.wizard.mvc.script.view.ScriptParser;
import org.bidib.wizard.script.AbstractScriptCommand;
import org.bidib.wizard.script.InitializingCommand;
import org.bidib.wizard.script.node.types.BacklightPortType;
import org.bidib.wizard.script.node.types.LightPortType;
import org.bidib.wizard.script.node.types.ServoPortType;
import org.bidib.wizard.script.node.types.SwitchPairPortType;
import org.bidib.wizard.script.node.types.TargetType;
import org.bidib.wizard.utils.PortListUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConfigPortCommand extends AbstractScriptCommand<NodeScripting>
    implements FunctionAware, InitializingCommand {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigPortCommand.class);

    public static final String KEY = "configPort";

    private Long uuid;

    private Function<? extends BidibStatus> function;

    // the port label is only used if the port number is not provided directly
    private String portLabel;

    private Integer portNumber;

    private Map<Byte, PortConfigValue<?>> portConfig = new HashMap<Byte, PortConfigValue<?>>();

    protected ConfigPortCommand() {
        super(KEY);
    }

    public ConfigPortCommand(Long uuid) {
        super(KEY);
        this.uuid = uuid;
    }

    @Override
    public void setFunction(Function<BidibStatus> function) {
        this.function = function;
    }

    public Function<? extends BidibStatus> getFunction() {
        return function;
    }

    /**
     * @return the portLabel
     */
    public String getPortLabel() {
        return portLabel;
    }

    /**
     * @param portLabel
     *            the portLabel to set
     */
    public void setPortLabel(String portLabel) {
        this.portLabel = portLabel;
    }

    /**
     * @return the portNumber
     */
    public Integer getPortNumber() {
        return portNumber;
    }

    /**
     * @param portNumber
     *            the portNumber to set
     */
    public void setPortNumber(Integer portNumber) {
        this.portNumber = portNumber;
    }

    @Override
    public void parse(String commandLine) {
        LOGGER.info("Parse the command line: {}", commandLine);
    }

    public int scan(Scanner scanner, int index, final Map<String, Object> context) {

        int i = index + 1;
        for (; i < scanner.size(); i++) {
            Token token = scanner.getToken(i);
            LOGGER.info("scan, current index: {}, token symbol: {}, name: {}", i, token.symbol.type, token.symbol.name);
            switch (token.symbol.type) {
                case TokenTypes.KEYWORD2:
                    switch (token.symbol.name) {
                        case BidibScriptScanner.KEY2_NAME:
                            // label detected
                            StringValueCallback labelAware = new StringValueCallback() {
                                @Override
                                public void setString(String label) {
                                    LOGGER.info("Set the port label: {}", label);
                                    portLabel = label;
                                }
                            };
                            i = NodeScriptUtils.parseLabel(scanner, i, labelAware, context);
                            break;
                        case BidibScriptScanner.KEY2_PTYPE:
                            // ptype detected
                            i = NodeScriptUtils.parsePtype(scanner, i, context, this);
                            break;

                        // add the case KEY2_LOWERLIMIT: etc.
                        case BidibScriptScanner.KEY2_LOWERLIMIT:
                            LOGGER.info("LowerLimit detected: {}", token.symbol.name);

                            NumberAware lowerLimitCallback = new NumberAware() {
                                @Override
                                public void setNumber(Integer number) {
                                    LOGGER.info("Set the lower limit: {}", number);
                                    portConfig.put(BidibLibrary.BIDIB_PCFG_SERVO_ADJ_L,
                                        new BytePortConfigValue(ByteUtils.getLowByte(number)));
                                }
                            };
                            i = NodeScriptUtils.parseNumber(scanner, i, lowerLimitCallback, context);
                            break;
                        case BidibScriptScanner.KEY2_UPPERLIMIT:
                            LOGGER.info("UpperLimit detected: {}", token.symbol.name);

                            NumberAware upperLimitCallback = new NumberAware() {
                                @Override
                                public void setNumber(Integer number) {
                                    LOGGER.info("Set the upper limit: {}", number);
                                    portConfig.put(BidibLibrary.BIDIB_PCFG_SERVO_ADJ_H,
                                        new BytePortConfigValue(ByteUtils.getLowByte(number)));
                                }
                            };
                            i = NodeScriptUtils.parseNumber(scanner, i, upperLimitCallback, context);
                            break;
                        case BidibScriptScanner.KEY2_TURNTIME:
                            LOGGER.info("TurnTime detected: {}", token.symbol.name);

                            NumberAware turnTimeCallback = new NumberAware() {
                                @Override
                                public void setNumber(Integer number) {
                                    LOGGER.info("Set the turntime: {}", number);
                                    portConfig.put(BidibLibrary.BIDIB_PCFG_SERVO_SPEED,
                                        new BytePortConfigValue(ByteUtils.getLowByte(number)));
                                }
                            };
                            i = NodeScriptUtils.parseNumber(scanner, i, turnTimeCallback, context);
                            break;
                        case BidibScriptScanner.KEY2_VALUEOFF:
                            LOGGER.info("valueOff detected: {}", token.symbol.name);

                            NumberAware valueOffCallback = new NumberAware() {
                                @Override
                                public void setNumber(Integer number) {
                                    LOGGER.info("Set the valueOff: {}", number);
                                    portConfig.put(BidibLibrary.BIDIB_PCFG_LEVEL_PORT_OFF,
                                        new BytePortConfigValue(ByteUtils.getLowByte(number)));
                                }
                            };
                            i = NodeScriptUtils.parseNumber(scanner, i, valueOffCallback, context);
                            break;
                        case BidibScriptScanner.KEY2_VALUEON:
                            LOGGER.info("valueOn detected: {}", token.symbol.name);

                            NumberAware valueOnCallback = new NumberAware() {
                                @Override
                                public void setNumber(Integer number) {
                                    LOGGER.info("Set the valueOn: {}", number);
                                    portConfig.put(BidibLibrary.BIDIB_PCFG_LEVEL_PORT_ON,
                                        new BytePortConfigValue(ByteUtils.getLowByte(number)));
                                }
                            };
                            i = NodeScriptUtils.parseNumber(scanner, i, valueOnCallback, context);
                            break;
                        case BidibScriptScanner.KEY2_DIMMOFF:
                            LOGGER.info("dimmOff detected: {}", token.symbol.name);

                            NumberAware dimmOffCallback = new NumberAware() {
                                @Override
                                public void setNumber(Integer number) {
                                    LOGGER.info("Set the dimmOff: {}", number);
                                    portConfig.put(BidibLibrary.BIDIB_PCFG_DIMM_DOWN,
                                        new BytePortConfigValue(ByteUtils.getLowByte(number)));
                                }
                            };
                            i = NodeScriptUtils.parseNumber(scanner, i, dimmOffCallback, context);
                            break;
                        case BidibScriptScanner.KEY2_DIMMON:
                            LOGGER.info("dimmOn detected: {}", token.symbol.name);

                            NumberAware dimmOnCallback = new NumberAware() {
                                @Override
                                public void setNumber(Integer number) {
                                    LOGGER.info("Set the dimmOn: {}", number);
                                    portConfig.put(BidibLibrary.BIDIB_PCFG_DIMM_UP,
                                        new BytePortConfigValue(ByteUtils.getLowByte(number)));
                                }
                            };
                            i = NodeScriptUtils.parseNumber(scanner, i, dimmOnCallback, context);
                            break;
                        case BidibScriptScanner.KEY2_DIMMOFF88:
                            LOGGER.info("dimmOff88 detected: {}", token.symbol.name);

                            NumberAware dimmOff88Callback = new NumberAware() {
                                @Override
                                public void setNumber(Integer number) {
                                    LOGGER.info("Set the dimmOff: {}", number);
                                    portConfig.put(BidibLibrary.BIDIB_PCFG_DIMM_DOWN_8_8,
                                        new Int16PortConfigValue(ByteUtils.getWORD(number)));
                                }
                            };
                            i = NodeScriptUtils.parseNumber(scanner, i, dimmOff88Callback, context);
                            break;
                        case BidibScriptScanner.KEY2_DIMMON88:
                            LOGGER.info("dimmOn88 detected: {}", token.symbol.name);

                            NumberAware dimmOn88Callback = new NumberAware() {
                                @Override
                                public void setNumber(Integer number) {
                                    LOGGER.info("Set the dimmOn: {}", number);
                                    portConfig.put(BidibLibrary.BIDIB_PCFG_DIMM_UP_8_8,
                                        new Int16PortConfigValue(ByteUtils.getWORD(number)));
                                }
                            };
                            i = NodeScriptUtils.parseNumber(scanner, i, dimmOn88Callback, context);
                            break;
                        case BidibScriptScanner.KEY2_MAPPING:
                            LOGGER.info("mapping detected: {}", token.symbol.name);

                            NumberAware mappingCallback = new NumberAware() {
                                @Override
                                public void setNumber(Integer number) {
                                    LOGGER.info("Set the mapping: {}", number);
                                    portConfig.put(BidibLibrary.BIDIB_PCFG_OUTPUT_MAP,
                                        new BytePortConfigValue(ByteUtils.getLowByte(number)));
                                }
                            };
                            i = NodeScriptUtils.parseNumber(scanner, i, mappingCallback, context);
                            break;

                        case BidibScriptScanner.KEY2_IOTYPE:
                            LOGGER.info("iotype detected: {}", token.symbol.name);
                            NumberAware iotypeCallback = new NumberAware() {
                                @Override
                                public void setNumber(Integer number) {
                                    LOGGER.info("Set the iotype: {}", number);
                                    portConfig.put(BidibLibrary.BIDIB_PCFG_SWITCH_CTRL,
                                        new BytePortConfigValue(ByteUtils.getLowByte(number)));
                                }
                            };
                            i = NodeScriptUtils.parseNumber(scanner, i, iotypeCallback, context);
                            break;
                        case BidibScriptScanner.KEY2_TICKS:
                            LOGGER.info("ticks detected: {}", token.symbol.name);
                            NumberAware ticksCallback = new NumberAware() {
                                @Override
                                public void setNumber(Integer number) {
                                    LOGGER.info("Set the ticks: {}", number);
                                    portConfig.put(BidibLibrary.BIDIB_PCFG_TICKS,
                                        new BytePortConfigValue(ByteUtils.getLowByte(number)));
                                }
                            };
                            i = NodeScriptUtils.parseNumber(scanner, i, ticksCallback, context);
                            break;

                        case BidibScriptScanner.KEY2_NUMBER:
                            // number detected
                            NumberAware numberAware = new NumberAware() {

                                @Override
                                public void setNumber(Integer number) {
                                    // if the current function is a macro function we must set the macro id
                                    if (function != null) {
                                        if (function instanceof PortAction) {
                                            PortAction portAction = (PortAction) function;
                                            String actionKey = portAction.getKey();
                                            LOGGER.info("Set the port id: {}, actionKey: {}", number, actionKey);

                                            Integer portId = number;
                                            // set the port with the provided number
                                            PortsProvider portsProvider =
                                                DefaultApplicationContext.getInstance().get(
                                                    DefaultApplicationContext.KEY_PORTS_PROVIDER, PortsProvider.class);
                                            Port<?> port = null;
                                            switch (portAction.getKey()) {
                                                case Function.KEY_ANALOG:
                                                    port =
                                                        PortListUtils.findPortByPortNumber(
                                                            portsProvider.getAnalogPorts(), portId);
                                                    break;
                                                case Function.KEY_BACKLIGHT:
                                                    port =
                                                        PortListUtils.findPortByPortNumber(
                                                            portsProvider.getBacklightPorts(), portId);
                                                    break;
                                                case Function.KEY_FEEDBACK:
                                                    port =
                                                        PortListUtils.findPortByPortNumber(
                                                            portsProvider.getFeedbackPorts(), portId);
                                                    break;
                                                case Function.KEY_INPUT:
                                                    port =
                                                        PortListUtils.findPortByPortNumber(
                                                            portsProvider.getEnabledInputPorts(), portId);
                                                    break;
                                                case Function.KEY_LIGHT:
                                                    port =
                                                        PortListUtils.findPortByPortNumber(
                                                            portsProvider.getLightPorts(), portId);
                                                    break;
                                                case Function.KEY_MOTOR:
                                                    port =
                                                        PortListUtils.findPortByPortNumber(
                                                            portsProvider.getMotorPorts(), portId);
                                                    break;
                                                case Function.KEY_SERVO:
                                                    port =
                                                        PortListUtils.findPortByPortNumber(
                                                            portsProvider.getServoPorts(), portId);
                                                    break;
                                                case Function.KEY_SOUND:
                                                    port =
                                                        PortListUtils.findPortByPortNumber(
                                                            portsProvider.getSoundPorts(), portId);
                                                    break;
                                                case Function.KEY_SWITCH:
                                                    port =
                                                        PortListUtils.findPortByPortNumber(
                                                            portsProvider.getEnabledSwitchPorts(), portId);
                                                    break;
                                                case Function.KEY_SWITCHPAIR:
                                                    port =
                                                        PortListUtils.findPortByPortNumber(
                                                            portsProvider.getEnabledSwitchPairPorts(), portId);
                                                    break;
                                                default:
                                                    break;
                                            }

                                            if (port != null) {
                                                portAction.setPort(port);
                                            }
                                            else {
                                                LOGGER.warn("No port found for port number: {}, actionKey: {}", portId,
                                                    actionKey);
                                            }
                                        }
                                    }
                                    else {
                                        LOGGER.warn("Unknown parameter number detected, value: {}", number);
                                    }
                                }
                            };
                            i = NodeScriptUtils.parseNumber(scanner, i, numberAware, context);
                            break;
                        default:
                            break;
                    }
                    break;
                case TokenTypes.NUMBER:
                    LOGGER.info("NUMBER detected: {}", token.symbol.name);

                    // set the port number
                    setPortNumber(Integer.valueOf(token.symbol.name));
                    break;
                case TokenTypes.IDENTIFIER:
                    LOGGER.info("IDENTIFIER detected: {}", token.symbol.name);

                    break;
                default:
                    break;
            }

            LOGGER.info("scan, i after parse: {}, last symbol name: {}", i, token.symbol.name);
        }

        return i;
    }

    @Override
    protected void internalExecute(final NodeScripting scripting, final Map<String, Object> context) {
        LOGGER.info("Set the port config: {}", this);

        if (function == null) {

            throw new IllegalArgumentException("The function must not be null.");
        }

        // TODO change for other port types
        String labelKey = null;
        TargetType portType = null;
        switch (function./* getAction(). */getKey()) {
            case Function.KEY_SERVO:
                labelKey = ScriptParser.KEY_SERVO_LABELS;
                portType = new ServoPortType();
                break;
            case Function.KEY_SWITCH:
                labelKey = ScriptParser.KEY_SWITCH_LABELS;
                portType = new SwitchPortType();
                break;
            case Function.KEY_SWITCHPAIR:
                labelKey = ScriptParser.KEY_SWITCHPAIR_LABELS;
                portType = new SwitchPairPortType();
                break;
            case Function.KEY_BACKLIGHT:
                labelKey = ScriptParser.KEY_BACKLIGHT_LABELS;
                portType = new BacklightPortType();
                break;
            case Function.KEY_LIGHT:
                labelKey = ScriptParser.KEY_LIGHT_LABELS;
                portType = new LightPortType();
                break;
            default:
                LOGGER.warn("Unsupported port type for port config: {}", function.getAction().getKey());
                break;
        }

        if (function instanceof PortAction) {
            PortAction portAction = (PortAction) function;
            if (portAction.getPort() != null) {
                portNumber = portAction.getPort().getId();
            }
        }

        if (portNumber != null) {
            portType.setPortNum(portNumber);

            // set the port config
            scripting.setPortConfig(uuid, portType, portConfig);
        }
        else if (StringUtils.isNotBlank(labelKey) && StringUtils.isNotBlank(portLabel)) {
            Map<Integer, String> labelMap = (Map<Integer, String>) context.get(labelKey);
            Integer portNumber = MapUtils.invertMap(labelMap).get(portLabel);

            if (portNumber != null) {
                portType.setPortNum(portNumber);

                // set the port config
                scripting.setPortConfig(uuid, portType, portConfig);
            }
            else {
                LOGGER.warn("No port number found for portLabel: {}", portLabel);
            }
        }
        else if (portType.getPortNum() != null) {
            LOGGER.info("Set the port config for portType: {}, portConfig: {}", portType, portConfig);
            // set the port config
            scripting.setPortConfig(uuid, portType, portConfig);
        }
        else {
            LOGGER.warn(
                "Set port config is skipped because the key for labels or no port label is not avaiable, function: {}",
                function);
        }
    }

    public void afterPropertiesSet() {
        LOGGER.info("afterPropertiesSet is called: {}", this);

    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[");
        sb.append(getClass().getSimpleName());
        sb.append(", uuid: ").append(uuid);
        sb.append(", function: ").append(function);
        sb.append(", portLabel: ").append(portLabel).append(", portConfig: ").append(portConfig);
        sb.append("]");
        return sb.toString();
    }
}
