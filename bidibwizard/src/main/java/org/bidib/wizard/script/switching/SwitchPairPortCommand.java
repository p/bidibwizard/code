package org.bidib.wizard.script.switching;

import java.util.Map;
import java.util.Scanner;

import org.bidib.wizard.comm.SwitchPortStatus;
import org.bidib.wizard.mvc.main.view.component.BulkSwitchFunctionsScripting;
import org.bidib.wizard.script.AbstractScriptCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The switchPair port command.
 */
public class SwitchPairPortCommand<T extends BulkSwitchFunctionsScripting> extends AbstractScriptCommand<T> {
    private static final Logger LOGGER = LoggerFactory.getLogger(SwitchPairPortCommand.class);

    public static final String KEY = "SPPORT";

    private int portNum;

    private SwitchPortStatus portStatus;

    public SwitchPairPortCommand() {
        super(KEY);
    }

    @Override
    public void parse(String commandLine) {
        Scanner scanner = new Scanner(commandLine);
        if (!getKey().equals(scanner.next())) {
            LOGGER.info("Invalid command is scanned, key does not match.");
        }
        line = commandLine.trim();

        portNum = scanner.nextInt();

        portStatus = SwitchPortStatus.valueOf(scanner.next());

        scanner.close();

        LOGGER.info("Parsed command, portNum: {}, portStatus: {}", portNum, portStatus);
    }

    public int getPortNum() {
        return portNum;
    }

    @Override
    protected void internalExecute(final T scripting, final Map<String, Object> context) {
        LOGGER.info("Set the status, portNum: {}, portStatus: {}", portNum, portStatus);
        scripting.sendPortStatusAction(portNum, portStatus);
    }
}
