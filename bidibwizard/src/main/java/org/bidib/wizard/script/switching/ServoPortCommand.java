package org.bidib.wizard.script.switching;

import java.util.Map;
import java.util.Scanner;

import org.bidib.wizard.mvc.main.view.component.BulkSwitchFunctionsScripting;
import org.bidib.wizard.script.AbstractScriptCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The switch port command.
 */
public class ServoPortCommand extends AbstractScriptCommand<BulkSwitchFunctionsScripting> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ServoPortCommand.class);

    public static final String KEY = "SERVO";

    private int portNum;

    private int position;

    public ServoPortCommand() {
        super(KEY);
    }

    @Override
    public void parse(String commandLine) {
        Scanner scanner = new Scanner(commandLine);
        if (!getKey().equals(scanner.next())) {
            LOGGER.info("Invalid command is scanned, key does not match.");
        }
        line = commandLine.trim();

        portNum = scanner.nextInt();

        position = scanner.nextInt();

        scanner.close();

        LOGGER.info("Parsed command, portNum: {}, position: {}", portNum, position);
    }

    public int getPortNum() {
        return portNum;
    }

    @Override
    protected void internalExecute(final BulkSwitchFunctionsScripting scripting, final Map<String, Object> context) {
        LOGGER.info("Set the status, portNum: {}, position: {}", portNum, position);
        scripting.sendPortValueAction(portNum, position);
    }
}
