package org.bidib.wizard.script;

import java.util.Map;

import javax.swing.SwingUtilities;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractScriptCommand<T extends Scripting> implements ScriptCommand<T> {
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractScriptCommand.class);

    private final String key;

    protected String line;

    protected AbstractScriptCommand(String key) {
        this.key = key;
    }

    @Override
    public String getKey() {
        return key;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getLine() {
        return line;
    }

    @Override
    public void execute(final T scripting, final Map<String, Object> context) {
        LOGGER.info("Execute the command: {}", this);
        try {
            if (SwingUtilities.isEventDispatchThread()) {
                internalExecute(scripting, context);
            }
            else {
                // SwingUtilities.invokeAndWait(new Runnable() {
                //
                // @Override
                // public void run() {
                // LOGGER.info("Execute command in AWT-thread.");
                // try {
                internalExecute(scripting, context);
                // }
                // catch (Exception ex) {
                // LOGGER.warn("Execute command in AWT-thread failed.", ex);
                // }
                // }
                // });
            }
        }
        // catch (InvocationTargetException ex) {
        // // TODO: handle exception
        // LOGGER.warn("Execute command in AWT-thread failed.", ex);
        //
        // Throwable targetEx = ex.getTargetException();
        // if (targetEx instanceof RuntimeException) {
        // throw (RuntimeException) targetEx;
        // }
        //
        // throw new RuntimeException("Execute script command failed.", ex.getTargetException());
        // }
        catch (Exception ex) {
            LOGGER.warn("Execute command in AWT-thread failed.", ex);

            throw new RuntimeException("Execute script command failed.", ex);
        }
    }

    protected abstract void internalExecute(T scripting, final Map<String, Object> context);

    @Override
    public String toString() {
        if (StringUtils.isNotBlank(line)) {
            return line;
        }
        return ToStringBuilder.reflectionToString(this);
    }
}
