package org.bidib.wizard.script.node;

import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import org.bidib.wizard.mvc.script.view.NodeScripting;
import org.bidib.wizard.script.AbstractScriptCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReselectCommand extends AbstractScriptCommand<NodeScripting> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ReselectCommand.class);

    public static final String KEY = "reselect";

    private Long uuid;

    private AtomicBoolean selectNodePassed = new AtomicBoolean();

    protected ReselectCommand() {
        super(KEY);
    }

    public ReselectCommand(Long uuid) {
        super(KEY);
        this.uuid = uuid;
    }

    @Override
    public void parse(String commandLine) {
        LOGGER.info("Set the reselect: {}", commandLine);
        // label = commandLine;
    }

    @Override
    protected void internalExecute(NodeScripting scripting, final Map<String, Object> context) {
        LOGGER.info("Reselect node with uuid: {}", uuid);

        try {
            scripting.reselectNode(uuid);

            selectNodePassed.set(true);
        }
        catch (Exception ex) {
            LOGGER.warn("Reselect node failed, uuid: {}", uuid, ex);
        }
    }

    @Override
    public void execute(NodeScripting scripting, final Map<String, Object> context) {
        super.execute(scripting, context);

        if (selectNodePassed.get()) {
            LOGGER.info("The select node operation has passed, wait a couple of seconds before continue.");

            try {
                Thread.sleep(5000);
            }
            catch (InterruptedException ex) {
                LOGGER.warn("Sleep while waiting for node initialization was interrupted.", ex);
            }
        }
        // TODO if the command has not passed we should handle the error
    }
}
