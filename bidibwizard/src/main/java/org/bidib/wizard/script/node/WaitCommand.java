package org.bidib.wizard.script.node;

import java.util.Map;

import org.bidib.wizard.mvc.script.view.NodeScripting;
import org.bidib.wizard.script.AbstractScriptCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WaitCommand extends AbstractScriptCommand<NodeScripting> {
    private static final Logger LOGGER = LoggerFactory.getLogger(WaitCommand.class);

    public static final String KEY = "wait";

    private Long uuid;

    private Integer waitTime;

    protected WaitCommand() {
        super(KEY);
    }

    public WaitCommand(Long uuid, Integer waitTime) {
        super(KEY);
        this.uuid = uuid;
        this.waitTime = waitTime;
    }

    @Override
    public void parse(String commandLine) {
        LOGGER.info("Set the wait time: {}", commandLine);
    }

    @Override
    public void execute(final NodeScripting scripting, final Map<String, Object> context) {
        LOGGER.info("Wait for {}ms, uuid: {}", waitTime, uuid);

        try {
            Thread.sleep(waitTime);
        }
        catch (InterruptedException ex) {
            LOGGER.warn("The sleep was interrupted.");
        }
    }

    @Override
    protected void internalExecute(NodeScripting scripting, final Map<String, Object> context) {
    }
}
