package org.bidib.wizard.script.node;

import java.util.HashMap;
import java.util.Map;

import org.bidib.wizard.highlight.BidibScriptScanner;
import org.bidib.wizard.highlight.Scanner;
import org.bidib.wizard.highlight.Token;
import org.bidib.wizard.highlight.TokenTypes;
import org.bidib.wizard.mvc.script.view.NodeScripting;
import org.bidib.wizard.script.AbstractScriptCommand;
import org.bidib.wizard.script.InitializingCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConfigAccessoryCommand extends AbstractScriptCommand<NodeScripting> implements InitializingCommand {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigAccessoryCommand.class);

    public static final String KEY = "configAccessory";

    private Long uuid;

    private Map<String, Integer> accessoryConfig = new HashMap<String, Integer>();

    protected ConfigAccessoryCommand() {
        super(KEY);
    }

    public ConfigAccessoryCommand(Long uuid) {
        super(KEY);
        this.uuid = uuid;
    }

    public Map<String, Integer> getAccessoryConfig() {
        return accessoryConfig;
    }

    @Override
    public void parse(String commandLine) {
        LOGGER.info("Parse the command line: {}", commandLine);
    }

    public int scan(Scanner scanner, int index, final Map<String, Object> context) {

        int i = index + 1;
        for (; i < scanner.size(); i++) {
            Token token = scanner.getToken(i);
            LOGGER.info("scan, current index: {}, token symbol: {}, name: {}", i, token.symbol.type, token.symbol.name);
            switch (token.symbol.type) {
                case TokenTypes.KEYWORD2:
                    switch (token.symbol.name) {

                        case BidibScriptScanner.KEY2_STARTUP:
                            LOGGER.info("Startup detected: {}", token.symbol.name);

                            // NumberAware repeatCallback = new NumberAware() {
                            // @Override
                            // public void setNumber(Integer number) {
                            // LOGGER.info("Set the repeat: {}", number);
                            //
                            // macroConfig.put("repeat", number);
                            // }
                            // };
                            // i = NodeScriptUtils.parseNumber(scanner, i, repeatCallback, context);
                            i = parseStartup(scanner, i, context);

                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }

            LOGGER.info("scan, i after parse: {}, last symbol name: {}", i, token.symbol.name);
        }

        return i;
    }

    protected static final int ASPECT_PARAM_UNCHANGED = 255;

    protected static final int ASPECT_PARAM_RESTORE = 254;

    private int parseStartup(Scanner scanner, int index, final Map<String, Object> context) {
        int i = index + 1;

        for (; i < scanner.size(); i++) {
            Token token = scanner.getToken(i);
            LOGGER.info("scan startup, current index: {}, token symbol: {}, name: {}", i, token.symbol.type,
                token.symbol.name);
            switch (token.symbol.type) {
                case TokenTypes.KEYWORD2:
                    switch (token.symbol.name) {

                        case BidibScriptScanner.KEY2_ASPECT:
                            LOGGER.info("aspect detected: {}", token.symbol.name);

                            NumberAware repeatCallback = new NumberAware() {
                                @Override
                                public void setNumber(Integer number) {
                                    LOGGER.info("Set the repeat: {}", number);

                                    accessoryConfig.put("startup", number);
                                }
                            };
                            i = NodeScriptUtils.parseNumber(scanner, i, repeatCallback, context);
                            break;
                        case BidibScriptScanner.KEY2_RESTORE:
                            LOGGER.info("restore detected: {}", token.symbol.name);
                            accessoryConfig.put("startup", ASPECT_PARAM_RESTORE);
                            break;
                        case BidibScriptScanner.KEY2_NONE:
                            LOGGER.info("none detected: {}", token.symbol.name);
                            accessoryConfig.put("startup", ASPECT_PARAM_UNCHANGED);
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }

            LOGGER.info("scan startup, i after parse: {}, last symbol name: {}", i, token.symbol.name);
        }
        return i;
    }

    @Override
    protected void internalExecute(final NodeScripting scripting, final Map<String, Object> context) {
        LOGGER.info("Set the accessory config: {}", this);

    }

    public void afterPropertiesSet() {
        LOGGER.info("afterPropertiesSet is called: {}", this);

    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[");
        sb.append(getClass().getSimpleName());
        sb.append(", uuid: ").append(uuid);
        sb.append("]");
        return sb.toString();
    }
}
