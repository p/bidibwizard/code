package org.bidib.wizard.script.loco;

import java.util.Map;
import java.util.Scanner;

import org.bidib.wizard.mvc.loco.view.LocoViewScripting;
import org.bidib.wizard.script.AbstractScriptCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The speed forward command.
 */
public class KeyShortCommand extends AbstractScriptCommand<LocoViewScripting> {
    private static final Logger LOGGER = LoggerFactory.getLogger(KeyShortCommand.class);

    public static final String KEY = "KEYSHORT";

    private int function;

    private int wait = 100;

    public KeyShortCommand() {
        super(KEY);
    }

    @Override
    public void parse(String commandLine) {
        Scanner scanner = new Scanner(commandLine);
        if (!getKey().equals(scanner.next())) {
            LOGGER.info("Invalid command is scanned, key does not match.");
        }
        line = commandLine.trim();

        function = scanner.nextInt();

        if (scanner.hasNext()) {
            wait = scanner.nextInt();
        }

        scanner.close();

        LOGGER.info("Parsed command, function: {}", function);
    }

    public int getFunction() {
        return function;
    }

    public int getWait() {
        return wait;
    }

    @Override
    protected void internalExecute(final LocoViewScripting scripting, final Map<String, Object> context) {
        LOGGER.info("Set the function: {}", function);
        scripting.setFunction(function);

        LOGGER.info("Delay between set function again: {}", wait);
        try {
            Thread.sleep(wait);
        }
        catch (InterruptedException e) {
            LOGGER.warn("Wait between send function was interrupted.", e);
        }
        LOGGER.info("Set the function again: {}", function);
        scripting.setFunction(function);
    }
}
