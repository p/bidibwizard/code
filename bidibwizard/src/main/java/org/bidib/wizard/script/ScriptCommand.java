package org.bidib.wizard.script;

import java.util.Map;

public interface ScriptCommand<T extends Scripting> {

    /**
     * @return the command key
     */
    String getKey();

    /**
     * @param commandLine
     *            the command line to parse
     */
    void parse(String commandLine);

    /**
     * Execute the command
     */
    void execute(final T scripting, Map<String, Object> context);
}
