package org.bidib.wizard.script.node.types;

/**
 * The <code>FlagTargetType</code> is used to change the label of a flag.
 */
public class FlagTargetType extends TargetType {
    private static final long serialVersionUID = 1L;

    public FlagTargetType() {
        super(ScriptingTargetType.FLAG);
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[FlagTargetType, ");
        sb.append(super.toString());
        sb.append("]");
        return sb.toString();
    }
}
