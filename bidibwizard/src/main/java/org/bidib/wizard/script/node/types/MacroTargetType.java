package org.bidib.wizard.script.node.types;

/**
 * The <code>MacroTargetType</code> is used to change the label of a macro.
 */
public class MacroTargetType extends TargetType {
    private static final long serialVersionUID = 1L;

    public MacroTargetType() {
        super(ScriptingTargetType.MACRO);
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[MacroTargetType, ");
        sb.append(super.toString());
        sb.append("]");
        return sb.toString();
    }
}
