package org.bidib.wizard.script.node;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.highlight.Scanner;
import org.bidib.wizard.highlight.Token;
import org.bidib.wizard.highlight.TokenTypes;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.function.Delayable;
import org.bidib.wizard.mvc.main.model.function.Function;
import org.bidib.wizard.mvc.script.view.ScriptParser;
import org.bidib.wizard.utils.MacroListUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NodeScriptUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(NodeScriptUtils.class);

    public static int parsePtype(
        Scanner scanner, int index, final Map<String, Object> context, final FunctionAware command) {
        Token token = scanner.getToken(index + 1);
        if (token.symbol.type == TokenTypes.OPERATOR) {
            LOGGER.info("Current symbol name: {}", token.symbol.name);

            String value = null;
            token = scanner.getToken(index + 2);
            switch (token.symbol.type) {
                case TokenTypes.VARIABLE:
                    String variable = token.symbol.name;
                    value = (String) context.get(variable);
                    if (value == null) {
                        // not found -> use the variable
                        value = variable;
                    }
                    break;
                default:
                    value = token.symbol.name;
                    break;
            }
            LOGGER.info("Set the port type value: {}", value);

            Function<BidibStatus> selectedFunction = null;
            MainModel mainModel = (MainModel) context.get(ScriptParser.KEY_MAIN_MODEL);
            Function<BidibStatus>[] functions =
                (Function<BidibStatus>[]) MacroListUtils.prepareAvailableFunctions(mainModel);
            for (Function<BidibStatus> function : functions) {
                if (function.getKey().equalsIgnoreCase(value)) {
                    LOGGER.info("Found matching function: {}", function);
                    selectedFunction = (Function<BidibStatus>) function;
                    break;
                }
            }

            if (selectedFunction != null) {
                LOGGER.info("Set the selected function: {}", selectedFunction);
                command.setFunction(selectedFunction);
            }

            return index + 2;
        }
        return index;
    }

    /**
     * Parse the label value and set it on the provided labelAware instance.
     * 
     * @param scanner
     *            the scanner instance
     * @param index
     *            the token index
     * @param labelAware
     *            the target type
     * @param context
     *            the context
     * @return the next token index to use
     */
    public static int parseLabel(
        Scanner scanner, int index, StringValueCallback labelAware, final Map<String, Object> context) {
        Token token = scanner.getToken(index + 1);
        if (token.symbol.type == TokenTypes.OPERATOR) {
            token = scanner.getToken(index + 2);

            do {
                String value = null;
                // token = scanner.getToken(index + 2);

                if (token == null) {
                    break;
                }

                LOGGER.info("Current token.symbol.type: {}, scanner.size: {}, index: {}", token.symbol.type,
                    scanner.size(), index);

                switch (token.symbol.type) {
                    case TokenTypes.VARIABLE:
                        String variable = token.symbol.name;
                        value = (String) context.get(variable);
                        if (value == null) {
                            // not found -> use the variable
                            value = variable;
                        }
                        break;
                    default:
                        // value = token.symbol.name;

                        value = StringUtils.strip(token.symbol.name, "\"'");
                        break;
                }
                LOGGER.info("Append the label value: {}", value);
                labelAware.appendString(value);

                index += 2;

                if (index < (scanner.size() - 2)) {
                    token = scanner.getToken(index + 2);
                }
                else {
                    break;
                }
            }
            while (token.symbol.type != TokenTypes.KEYWORD && token.symbol.type != TokenTypes.KEYWORD2
                && token.symbol.type != TokenTypes.OPERATOR && token.symbol.type != TokenTypes.WHITESPACE
            /* && index < (scanner.size() - 2) */);

            return index /* + 2 */;
        }
        return index;
    }

    /**
     * Parse the delay value and set it on the provided delayable instance.
     * 
     * @param scanner
     *            the scanner instance
     * @param index
     *            the token index
     * @param delayable
     *            the delayable instance
     * @param context
     *            the context
     * @return the next token index to use
     */
    public static int parseDelay(Scanner scanner, int index, Delayable delayable, final Map<String, Object> context) {
        Token token = scanner.getToken(index + 1);
        if (token.symbol.type == TokenTypes.OPERATOR) {

            String value = null;
            token = scanner.getToken(index + 2);
            switch (token.symbol.type) {
                case TokenTypes.VARIABLE:
                    String variable = token.symbol.name;
                    value = (String) context.get(variable);
                    if (value == null) {
                        // not found -> use the variable
                        value = variable;
                    }
                    break;
                default:
                    value = token.symbol.name;
                    break;
            }
            LOGGER.info("Set the delay value: {}", value);
            delayable.setDelay(Integer.valueOf(value));

            return index + 2;
        }
        return index;
    }

    /**
     * Parse the action value and set it on the provided actionAware instance.
     * 
     * @param scanner
     *            the scanner instance
     * @param index
     *            the token index
     * @param actionAware
     *            the actionAware instance
     * @param context
     *            the context
     * @return the next token index to use
     */
    public static int parseAction(
        Scanner scanner, int index, ActionAware actionAware, final Map<String, Object> context) {
        LOGGER.info("Parse action, index: {}", index);
        Token token = scanner.getToken(index + 1);
        if (token.symbol.type == TokenTypes.OPERATOR) {

            String value = null;
            token = scanner.getToken(index + 2);
            switch (token.symbol.type) {
                case TokenTypes.VARIABLE:
                    String variable = token.symbol.name;
                    value = (String) context.get(variable);
                    if (value == null) {
                        // not found -> use the variable
                        value = variable;
                    }
                    break;
                default:
                    value = token.symbol.name;
                    break;
            }
            LOGGER.info("Set the action value: {}", value);

            Function<? extends BidibStatus> selectedFunction = actionAware.getFunction();

            if (selectedFunction != null) {

                BidibStatus[] actions = selectedFunction.getAction().getValues();
                for (BidibStatus action : actions) {
                    LOGGER.info("Current action: {}", action);

                    if (action.getKey().equalsIgnoreCase(value)) {
                        LOGGER.info("Found matching action: {}", action);

                        actionAware.setAction(action);
                        break;
                    }
                }
            }

            return index + 2;
        }
        return index;
    }

    public BidibStatus[] getActions(BidibStatus status) {
        if (status != null) {
            BidibStatus[] actions = status.getValues();
            return actions;
        }
        else {
            LOGGER.debug("No BidibStatus available.");
        }
        return new BidibStatus[0];
    }

    /**
     * Parse the target value and set it on the provided target aware instance.
     * 
     * @param scanner
     *            the scanner instance
     * @param index
     *            the token index
     * @param targetAware
     *            the target aware instance
     * @param context
     *            the context
     * @return the next token index to use
     */
    public static int parseTarget(
        Scanner scanner, int index, TargetAware targetAware, final Map<String, Object> context) {
        Token token = scanner.getToken(index + 1);
        if (token.symbol.type == TokenTypes.OPERATOR) {

            String value = null;
            token = scanner.getToken(index + 2);
            switch (token.symbol.type) {
                case TokenTypes.VARIABLE:
                    String variable = token.symbol.name;
                    value = (String) context.get(variable);
                    if (value == null) {
                        // not found -> use the variable
                        value = variable;
                    }
                    break;
                default:
                    value = token.symbol.name;
                    break;
            }
            LOGGER.info("Set the delay value: {}", value);
            targetAware.setTarget(Integer.valueOf(value));

            return index + 2;
        }
        return index;
    }

    /**
     * Parse the macro number value and set it on the provided target aware instance.
     * 
     * @param scanner
     *            the scanner instance
     * @param index
     *            the token index
     * @param numberAware
     *            the number aware instance
     * @param context
     *            the context
     * @return the next token index to use
     */
    public static int parseNumber(
        Scanner scanner, int index, NumberAware numberAware, final Map<String, Object> context) {
        Token token = scanner.getToken(index + 1);
        if (token.symbol.type == TokenTypes.OPERATOR) {

            String value = null;
            token = scanner.getToken(index + 2);
            switch (token.symbol.type) {
                case TokenTypes.VARIABLE:
                    String variable = token.symbol.name;
                    LOGGER.info("parseNumber will try to replace variable: {}", variable);
                    value = (String) context.get(variable);
                    if (value == null) {
                        // not found -> use the variable
                        LOGGER.warn("No substitution for variable found.");
                        value = variable;
                    }
                    break;
                case TokenTypes.IDENTIFIER:
                    LOGGER.info("Found identifier: {}", token.symbol.name);
                    break;
                default:
                    value = token.symbol.name;
                    break;
            }
            LOGGER.info("Set the number value: {}", value);
            numberAware.setNumber(Integer.valueOf(value));

            return index + 2;
        }
        return index;
    }

    /**
     * Parse the macro time hour value and set it on the provided target aware instance.
     * 
     * @param scanner
     *            the scanner instance
     * @param index
     *            the token index
     * @param numberAware
     *            the number aware instance
     * @param context
     *            the context
     * @return the next token index to use
     */
    public static int parseMacroTimeHour(
        Scanner scanner, int index, NumberAware numberAware, final Map<String, Object> context) {
        Token token = scanner.getToken(index + 1);
        if (token.symbol.type == TokenTypes.OPERATOR) {

            String value = null;
            token = scanner.getToken(index + 2);
            switch (token.symbol.type) {
                case TokenTypes.VARIABLE:
                    String variable = token.symbol.name;
                    value = (String) context.get(variable);
                    if (value == null) {
                        // not found -> use the variable
                        LOGGER.warn("No substitution found for variable: {}", variable);
                        value = variable;
                    }
                    break;
                case TokenTypes.IDENTIFIER:
                    value = token.symbol.name;
                    LOGGER.debug("Found identifier: {}", value);
                    switch (value.toLowerCase()) {
                        case "everyfull":
                            value = "24";
                            break;
                        case "everyfullatday":
                            value = "25";
                            break;
                        default:
                            LOGGER.warn("No replacement found for value: {}", value);
                            break;
                    }
                    break;
                default:
                    value = token.symbol.name;
                    break;
            }
            LOGGER.info("Set the macro time hour number value: {}", value);
            numberAware.setNumber(Integer.valueOf(value));

            return index + 2;
        }
        return index;
    }

    /**
     * Parse the macro time minute value and set it on the provided target aware instance.
     * 
     * @param scanner
     *            the scanner instance
     * @param index
     *            the token index
     * @param numberAware
     *            the number aware instance
     * @param context
     *            the context
     * @return the next token index to use
     */
    public static int parseMacroTimeMinute(
        Scanner scanner, int index, NumberAware numberAware, final Map<String, Object> context) {
        Token token = scanner.getToken(index + 1);
        if (token.symbol.type == TokenTypes.OPERATOR) {

            String value = null;
            token = scanner.getToken(index + 2);
            switch (token.symbol.type) {
                case TokenTypes.VARIABLE:
                    String variable = token.symbol.name;
                    value = (String) context.get(variable);
                    if (value == null) {
                        // not found -> use the variable
                        LOGGER.warn("No substitution found for variable: {}", variable);
                        value = variable;
                    }
                    break;
                case TokenTypes.IDENTIFIER:
                    value = token.symbol.name;
                    LOGGER.debug("Found identifier: {}", value);
                    switch (value.toLowerCase()) {
                        case "everyminute":
                            value = "60";
                            break;
                        case "everyhalfhour":
                            value = "61";
                            break;
                        case "everyquarter":
                            value = "62";
                            break;
                        default:
                            LOGGER.warn("No replacement found for value: {}", value);
                            break;
                    }
                    break;
                default:
                    value = token.symbol.name;
                    break;
            }
            LOGGER.info("Set the macro time minute number value: {}", value);
            numberAware.setNumber(Integer.valueOf(value));

            return index + 2;
        }
        return index;
    }

    /**
     * Parse the macro time day value and set it on the provided target aware instance.
     * 
     * @param scanner
     *            the scanner instance
     * @param index
     *            the token index
     * @param numberAware
     *            the number aware instance
     * @param context
     *            the context
     * @return the next token index to use
     */
    public static int parseMacroTimeDay(
        Scanner scanner, int index, NumberAware numberAware, final Map<String, Object> context) {
        Token token = scanner.getToken(index + 1);
        if (token.symbol.type == TokenTypes.OPERATOR) {

            String value = null;
            token = scanner.getToken(index + 2);
            switch (token.symbol.type) {
                case TokenTypes.VARIABLE:
                    String variable = token.symbol.name;
                    value = (String) context.get(variable);
                    if (value == null) {
                        // not found -> use the variable
                        LOGGER.warn("No substitution found for variable: {}", variable);
                        value = variable;
                    }
                    break;
                case TokenTypes.IDENTIFIER:
                    value = token.symbol.name;
                    LOGGER.debug("Found identifier: {}", value);
                    switch (value.toLowerCase()) {
                        case "everyday":
                            value = "7";
                            break;
                        default:
                            LOGGER.warn("No replacement found for value: {}", value);
                            break;
                    }
                    break;
                default:
                    value = token.symbol.name;
                    break;
            }
            LOGGER.info("Set the macro time hour number value: {}", value);
            numberAware.setNumber(Integer.valueOf(value));

            return index + 2;
        }
        return index;
    }

    /**
     * Parse the variable value and set it on the provided target aware instance.
     * 
     * @param scanner
     *            the scanner instance
     * @param index
     *            the token index
     * @param labelAware
     *            the label aware instance
     * @param context
     *            the context
     * @return the next token index to use
     */
    public static int parseVariable(
        Scanner scanner, int index, StringValueCallback labelAware, final Map<String, Object> context) {

        Token token = scanner.getToken(index + 1);

        LOGGER.info("parse variable, current index: {}, token symbol: {}, name: {}", index, token.symbol.type,
            token.symbol.name);

        String value = null;
        switch (token.symbol.type) {
            case TokenTypes.VARIABLE:
                String variable = token.symbol.name;
                value = (String) context.get("%" + variable);
                if (value == null) {
                    // not found -> use the variable
                    value = variable;
                }
                break;
            default:
                value = token.symbol.name;
                break;
        }

        if (value != null) {
            LOGGER.info("Set the variable value: {}", value);
            labelAware.setString(value);
        }

        return 1;
    }

}
