package org.bidib.wizard.script.switching;

import java.util.Map;
import java.util.Scanner;

import org.bidib.wizard.comm.LightPortStatus;
import org.bidib.wizard.mvc.main.view.component.BulkSwitchFunctionsScripting;
import org.bidib.wizard.script.AbstractScriptCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The switch port command.
 */
public class LightPortCommand<T extends BulkSwitchFunctionsScripting> extends AbstractScriptCommand<T> {
    private static final Logger LOGGER = LoggerFactory.getLogger(LightPortCommand.class);

    public static final String KEY = "LIGHT";

    private int portNum;

    private LightPortStatus portStatus;

    public LightPortCommand() {
        super(KEY);
    }

    @Override
    public void parse(String commandLine) {
        Scanner scanner = new Scanner(commandLine);
        if (!getKey().equals(scanner.next())) {
            LOGGER.info("Invalid command is scanned, key does not match.");
        }
        line = commandLine.trim();

        portNum = scanner.nextInt();

        portStatus = LightPortStatus.valueOf(scanner.next());

        scanner.close();

        LOGGER.info("Parsed command, portNum: {}, portStatus: {}", portNum, portStatus);
    }

    public int getPortNum() {
        return portNum;
    }

    @Override
    protected void internalExecute(final T scripting, final Map<String, Object> context) {
        LOGGER.info("Set the status, portNum: {}, portStatus: {}", portNum, portStatus);
        scripting.sendPortStatusAction(portNum, portStatus);
    }
}
