package org.bidib.wizard.script.node.types;

import org.bidib.wizard.mvc.main.model.LabelAware;

public abstract class TargetType implements LabelAware {
    private static final long serialVersionUID = 1L;

    private final ScriptingTargetType scriptingTargetType;

    private Integer portNum;

    private String label;

    public TargetType(ScriptingTargetType scriptingTargetType) {
        this.scriptingTargetType = scriptingTargetType;
    }

    /**
     * @return the ScriptingTargetType
     */
    public ScriptingTargetType getScriptingTargetType() {
        return scriptingTargetType;
    }

    /**
     * @return the portNum
     */
    public Integer getPortNum() {
        return portNum;
    }

    /**
     * @param portNum
     *            the portNum to set
     */
    public void setPortNum(Integer portNum) {
        this.portNum = portNum;
    }

    /**
     * @return the label
     */
    public String getLabel() {
        return label;
    }

    /**
     * @param label
     *            the label to set
     */
    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[PortType, scriptingTargetType: ");
        sb.append(scriptingTargetType).append(", portNum: ").append(portNum);
        if (label != null) {
            sb.append(", label: ").append(label);
        }
        sb.append("]");
        return sb.toString();
    }
}
