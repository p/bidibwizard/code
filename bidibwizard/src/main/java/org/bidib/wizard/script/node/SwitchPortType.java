package org.bidib.wizard.script.node;

import org.bidib.wizard.script.node.types.ScriptingTargetType;
import org.bidib.wizard.script.node.types.TargetType;

public class SwitchPortType extends TargetType {
    private static final long serialVersionUID = 1L;

    public SwitchPortType() {
        super(ScriptingTargetType.SWITCHPORT);
    }

}
