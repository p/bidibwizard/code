package org.bidib.wizard.dialog;

import javax.swing.JDialog;
import javax.swing.JOptionPane;

import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.view.panel.renderer.BidibNodeNameUtils;

public abstract class NodeLabelDialog {

    public NodeLabelDialog(Node node, int x, int y) {

        String label = BidibNodeNameUtils.prepareLabel(node, false, false).getNodeLabel();

        JOptionPane pane =
            new JOptionPane(Resources.getString(LabelDialog.class, "label") + ":", JOptionPane.QUESTION_MESSAGE);

        pane.setInitialSelectionValue(label);
        pane.setOptionType(JOptionPane.OK_CANCEL_OPTION);
        pane.setWantsInput(true);

        JDialog dialog = pane.createDialog(null, Resources.getString(LabelDialog.class, "title"));

        dialog.setLocation(x, y);
        dialog.setVisible(true);
        dialog.dispose();

        Object selectedValue = pane.getInputValue();

        if (selectedValue != JOptionPane.UNINITIALIZED_VALUE && selectedValue != null) {
            labelChanged(selectedValue.toString());
        }
    }

    public abstract void labelChanged(String value);
}
