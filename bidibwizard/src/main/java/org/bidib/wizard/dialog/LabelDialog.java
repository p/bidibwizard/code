package org.bidib.wizard.dialog;

import javax.swing.JDialog;
import javax.swing.JOptionPane;

import org.bidib.wizard.common.locale.Resources;

public abstract class LabelDialog {

    public LabelDialog(String value, int x, int y) {
        JOptionPane pane =
            new JOptionPane(Resources.getString(LabelDialog.class, "label") + ":", JOptionPane.QUESTION_MESSAGE);

        pane.setInitialSelectionValue(value);
        pane.setOptionType(JOptionPane.OK_CANCEL_OPTION);
        pane.setWantsInput(true);

        JDialog dialog = pane.createDialog(null, Resources.getString(LabelDialog.class, "title"));

        dialog.setLocation(x, y);
        dialog.setVisible(true);

        Object selectedValue = pane.getInputValue();

        if (selectedValue != JOptionPane.UNINITIALIZED_VALUE && selectedValue != null) {
            labelChanged(selectedValue.toString());
        }
    }

    public abstract void labelChanged(String value);
}
