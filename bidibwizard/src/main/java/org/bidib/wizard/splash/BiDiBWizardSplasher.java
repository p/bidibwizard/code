package org.bidib.wizard.splash;

/**
 * BiDiBWizardSplasher
 */
public class BiDiBWizardSplasher {

    /**
     * Shows the splash screen, launches the application and then disposes the splash screen.
     * 
     * @param args
     *            the command line arguments
     */
    public static void main(String[] args) {

        String className = "org.bidib.wizard.main.BiDiBWizard";
        String[] args2 = null;

        System.out.println("number of arguments: " + args.length);

        if (args.length > 0) {
            className = args[0];

            args2 = new String[args.length - 1];
            for (int index = 1; index < args.length; index++) {
                args2[index - 1] = args[index];
            }
        }
        else {
            args2 = args;
        }
        System.out.println("main class: " + className);

        SplashWindow.splash(BiDiBWizardSplasher.class.getResource("/icons/BiDiBWizardSplash.png"));
        SplashWindow.invokeMain(className, args2);
        SplashWindow.disposeSplash();
    }
}
