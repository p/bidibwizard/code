package org.bidib.wizard.labels;

import org.bidib.wizard.labels.WizardLabelFactory.LabelTypes;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NodeLabelFactory extends AbstractLabelFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(NodeLabelFactory.class);

    public static final String DEFAULT_LABELTYPE = "node";

    @Override
    protected String getLabelFileName() {
        return "NodeLabels.labels";
    }

    @Override
    protected String getLabelTypeName() {
        return DEFAULT_LABELTYPE;
    }

    @Override
    protected LabelTypes getLabelType() {
        return LabelTypes.node;
    }

    @Override
    protected String getApplicationContextKey() {
        // TODO Auto-generated method stub
        return DefaultApplicationContext.KEY_NODE_LABELS;
    }

    // @Override
    // protected NodeLabels getLegacyLabels() {
    // return new NodeLabels();
    // }

    // protected Labels migrateLabels() {
    // LOGGER.info("Try to migrate exisiting port labels.");
    //
    // Labels labels = null;
    // try {
    // NodeLabels legacyLabels = getLegacyLabels();
    // if (legacyLabels == null) {
    // return labels;
    // }
    // // load the legacy labels
    // legacyLabels.load();
    //
    // if (MapUtils.isEmpty(legacyLabels.getLabelMap())) {
    // LOGGER.info("No legacy labels to migrate found.");
    // return labels;
    // }
    //
    // labels = new Labels();
    //
    // for (Entry<Long, String> entry : legacyLabels.getLabelMap().entrySet()) {
    // long currentUniqueId = entry.getKey();
    //
    // LabelNodeType labelNode = new LabelNodeType();
    // labelNode.setType("node");
    // labelNode.setUniqueId(currentUniqueId);
    //
    // String labelString = entry.getValue();
    // if (StringUtils.isEmpty(labelString)) {
    // LOGGER.info("No legacy child label to migrate found for current uniqueId: {}", currentUniqueId);
    // continue;
    // }
    //
    // labels.getLabelNode().add(labelNode);
    //
    // int portId = 0;
    // // create default label
    // LabelType labelType = new LabelType();
    // labelType.setType(getLabelTypeName());
    // labelType.setIndex(portId);
    // labelType.setLabelString(labelString);
    //
    // labelNode.getLabel().add(labelType);
    // }
    // LOGGER.info("Migration of legacy labels passed.");
    // }
    // catch (Exception ex) {
    // LOGGER.warn("Load legacy node labels failed.", ex);
    // }
    //
    // return labels;
    // }

    @Override
    protected void saveLabelsToBiDiB(Long uniqueId, Labels labels) {
        LOGGER.info("Save labels to BiDiB, uniqueId: {}", uniqueId);

        // WizardLabelFactory bidibLabelFactory = WizardLabelFactory.getInstance();
        // bidibLabelFactory.updateLabels(uniqueId, labels, WizardLabelFactory.LabelTypes.node);
        // bidibLabelFactory.saveLabels(uniqueId);
        updateLabels(uniqueId, WizardLabelFactory.LabelTypes.node);
    }

    @Override
    protected void loadLabelsFromBiDiB(Long uniqueId, final Labels labels) {

        LOGGER.info("Load labels from BiDiB, uniqueId: {}", uniqueId);

        mergeLabels(uniqueId, WizardLabelFactory.LabelTypes.node);
    }
}
