package org.bidib.wizard.labels;

import org.bidib.wizard.labels.WizardLabelFactory.LabelTypes;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FlagLabelFactory extends AbstractLabelFactory {
    private static final Logger LOGGER = LoggerFactory.getLogger(FlagLabelFactory.class);

    public static final String DEFAULT_LABELTYPE = "flag";

    @Override
    protected String getLabelFileName() {
        return "FlagLabels.labels";
    }

    @Override
    protected String getLabelTypeName() {
        return DEFAULT_LABELTYPE;
    }

    @Override
    protected LabelTypes getLabelType() {
        return LabelTypes.macro;
    }

    @Override
    protected String getApplicationContextKey() {
        return DefaultApplicationContext.KEY_FLAG_LABELS;
    }

    @Override
    protected void saveLabelsToBiDiB(Long uniqueId, Labels labels) {
        LOGGER.info("Save labels to BiDiB, uniqueId: {}", uniqueId);

        updateLabels(uniqueId, WizardLabelFactory.LabelTypes.flag);
    }

    @Override
    protected void loadLabelsFromBiDiB(Long uniqueId, Labels labels) {
        LOGGER.info("Load labels from BiDiB, uniqueId: {}", uniqueId);

        mergeLabels(uniqueId, WizardLabelFactory.LabelTypes.flag);
    }
}
