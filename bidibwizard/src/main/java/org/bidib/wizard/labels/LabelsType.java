package org.bidib.wizard.labels;

import java.util.List;

public interface LabelsType {

    List<LabelNodeType> getLabelNode();
}
