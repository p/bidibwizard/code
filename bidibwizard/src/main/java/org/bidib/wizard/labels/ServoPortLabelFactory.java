package org.bidib.wizard.labels;

import org.bidib.wizard.labels.WizardLabelFactory.LabelTypes;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServoPortLabelFactory extends AbstractLabelFactory {
    private static final Logger LOGGER = LoggerFactory.getLogger(ServoPortLabelFactory.class);

    public static final String DEFAULT_LABELTYPE = "servoPort";

    @Override
    protected String getLabelFileName() {
        return "ServoPortLabels.labels";
    }

    @Override
    protected String getLabelTypeName() {
        return DEFAULT_LABELTYPE;
    }

    @Override
    protected LabelTypes getLabelType() {
        return LabelTypes.servoPort;
    }

    @Override
    protected String getApplicationContextKey() {
        // TODO Auto-generated method stub
        return DefaultApplicationContext.KEY_SERVOPORT_LABELS;
    }

    // @Override
    // protected ChildLabels getLegacyLabels() {
    // return new ServoPortLabels();
    // }

    @Override
    protected void saveLabelsToBiDiB(Long uniqueId, Labels labels) {
        LOGGER.info("Save labels to BiDiB, uniqueId: {}", uniqueId);

        // WizardLabelFactory bidibLabelFactory = WizardLabelFactory.getInstance();
        // bidibLabelFactory.updateLabels(uniqueId, labels, WizardLabelFactory.LabelTypes.servoPort);
        // bidibLabelFactory.saveLabels(uniqueId);
        updateLabels(uniqueId, WizardLabelFactory.LabelTypes.servoPort);
    }

    @Override
    protected void loadLabelsFromBiDiB(Long uniqueId, Labels labels) {
        LOGGER.info("Load labels from BiDiB, uniqueId: {}", uniqueId);

        mergeLabels(uniqueId, WizardLabelFactory.LabelTypes.servoPort);
    }
}
