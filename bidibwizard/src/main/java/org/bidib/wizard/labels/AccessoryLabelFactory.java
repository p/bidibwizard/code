package org.bidib.wizard.labels;

import java.util.Map.Entry;

import org.apache.commons.collections4.MapUtils;
import org.bidib.wizard.labels.WizardLabelFactory.LabelTypes;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.bidib.wizard.mvc.main.model.AccessoryLabels;
import org.bidib.wizard.mvc.main.model.ChildLabelMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AccessoryLabelFactory extends AbstractLabelFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccessoryLabelFactory.class);

    public static final String DEFAULT_LABELTYPE = "accessory";

    // private static final String JAXB_PACKAGE = "org.bidib.wizard.labels";

    public static final String XSD_LOCATION = "/xsd/wizard-labels.xsd";

    public static final String JAXB_SCHEMA_LOCATION = "http://www.bidib.org/wizard/labels xsd/wizard-labels.xsd";

    // private Labels accessoryLabels;

    // private Object lock = new Object();

    @Override
    protected String getLabelFileName() {
        return "AccessoryLabels.labels";
    }

    @Override
    protected String getLabelTypeName() {
        return DEFAULT_LABELTYPE;
    }

    @Override
    protected LabelTypes getLabelType() {
        return LabelTypes.accessory;
    }

    @Override
    protected String getApplicationContextKey() {
        return DefaultApplicationContext.KEY_ACCESSORY_LABELS;
    }

    protected Labels migrateLabels() {
        LOGGER.info("Try to migrate exisiting accessory labels.");

        Labels labels = null;
        try {
            AccessoryLabels legacyLabels = new AccessoryLabels();
            legacyLabels.load();

            if (MapUtils.isEmpty(legacyLabels.getLabelMap())) {
                LOGGER.info("No legacy labels to migrate found.");
                return labels;
            }

            labels = new Labels();

            for (Entry<Long, ChildLabelMap> entry : legacyLabels.getLabelMap().entrySet()) {
                long currentUniqueId = entry.getKey();

                LabelNodeType labelNode = new LabelNodeType();
                labelNode.setType("node");
                labelNode.setUniqueId(currentUniqueId);

                labels.getLabelNode().add(labelNode);

                ChildLabelMap childLabels = entry.getValue();
                if (MapUtils.isEmpty(childLabels)) {
                    LOGGER.info("No legacy child labels to migrate found.");
                    continue;
                }

                for (Entry<Integer, String> currentChildLabel : childLabels.entrySet()) {
                    int accessoryId = currentChildLabel.getKey();
                    String labelString = currentChildLabel.getValue();

                    // create default label
                    LabelType accessoryLabel = new LabelType();
                    accessoryLabel.setType("accessory");
                    accessoryLabel.setIndex(accessoryId);
                    accessoryLabel.setLabelString(labelString);

                    labelNode.getLabel().add(accessoryLabel);
                }
            }
            LOGGER.info("Migration of legacy labels passed.");
        }
        catch (Exception ex) {
            LOGGER.warn("Load legacy accessory labels failed.", ex);
        }
        return labels;
    }

    @Override
    protected void saveLabelsToBiDiB(Long uniqueId, Labels labels) {
        LOGGER.info("Save labels to BiDiB, uniqueId: {}", uniqueId);

        // WizardLabelFactory bidibLabelFactory = WizardLabelFactory.getInstance();
        // bidibLabelFactory.updateLabels(uniqueId, labels, WizardLabelFactory.LabelTypes.accessory);
        // bidibLabelFactory.saveLabels(uniqueId);
        updateLabels(uniqueId, WizardLabelFactory.LabelTypes.accessory);

    }

    @Override
    protected void loadLabelsFromBiDiB(Long uniqueId, Labels labels) {

        LOGGER.info("Load labels from BiDiB, uniqueId: {}", uniqueId);

        mergeLabels(uniqueId, WizardLabelFactory.LabelTypes.accessory);
    }
}
