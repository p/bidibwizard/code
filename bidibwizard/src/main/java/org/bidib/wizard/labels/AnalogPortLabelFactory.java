package org.bidib.wizard.labels;

import org.bidib.wizard.labels.WizardLabelFactory.LabelTypes;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AnalogPortLabelFactory extends AbstractLabelFactory {
    private static final Logger LOGGER = LoggerFactory.getLogger(AnalogPortLabelFactory.class);

    public static final String DEFAULT_LABELTYPE = "analogPort";

    @Override
    protected String getLabelFileName() {
        return "AnalogPortLabels.labels";
    }

    @Override
    protected String getLabelTypeName() {
        return DEFAULT_LABELTYPE;
    }

    @Override
    protected LabelTypes getLabelType() {
        return LabelTypes.analogPort;
    }

    @Override
    protected String getApplicationContextKey() {
        // TODO Auto-generated method stub
        return DefaultApplicationContext.KEY_ANALOGPORT_LABELS;
    }

    // @Override
    // protected ChildLabels getLegacyLabels() {
    // return new AnalogPortLabels();
    // }

    @Override
    protected void saveLabelsToBiDiB(Long uniqueId, Labels labels) {
        LOGGER.info("Save labels to BiDiB, uniqueId: {}", uniqueId);

        // BidibLabelFactory bidibLabelFactory = BidibLabelFactory.getInstance();
        // bidibLabelFactory.updateLabels(uniqueId, labels, BidibLabelFactory.LabelTypes.analogPort);
        // bidibLabelFactory.saveLabels(uniqueId);
        updateLabels(uniqueId, WizardLabelFactory.LabelTypes.analogPort);
    }

    @Override
    protected void loadLabelsFromBiDiB(Long uniqueId, Labels labels) {
        LOGGER.info("Load labels from BiDiB, uniqueId: {}", uniqueId);

        mergeLabels(uniqueId, WizardLabelFactory.LabelTypes.analogPort);
    }
}
