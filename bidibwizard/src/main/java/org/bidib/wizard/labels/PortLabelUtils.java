package org.bidib.wizard.labels;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;

import org.apache.commons.lang3.StringUtils;
import org.bidib.jbidibc.core.exception.InvalidConfigurationException;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.bidib.wizard.mvc.main.model.ChildLabels;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.model.Port;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PortLabelUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(PortLabelUtils.class);

    /**
     * Get the labels for the node with the provided uniqueId and the provided label id.
     * 
     * @param uniqueId
     *            the unique id
     * @param labelId
     *            the label id
     * @param defaultLabelType
     *            the default labelType name
     * @return the label
     */
    public static LabelType getLabel(Labels labels, long uniqueId, int labelId, final String defaultLabelType) {
        LabelType labelType = null;

        if (labels != null) {
            for (LabelNodeType labelNode : labels.getLabelNode()) {

                if (labelNode.getUniqueId() == uniqueId) {
                    // found the node -> search the label
                    for (LabelType label : labelNode.getLabel()) {

                        if (label.getIndex() == labelId) {
                            // found the label
                            labelType = label;
                            break;
                        }

                    }
                    break;
                }
            }
        }

        if (labelType == null) {
            // create default value
            labelType = new LabelType();
            labelType.setType(defaultLabelType);
            labelType.setIndex(labelId);
        }

        return labelType;
    }

    /**
     * Get the labelNode for the provided uniqueId.
     * 
     * @param labels
     *            the labels to search
     * @param uniqueId
     *            the uniqueId
     * @return the labelNode or <code>null</code> if none is found
     */
    public static LabelNodeType getLabelNode(Labels labels, long uniqueId) {
        if (labels != null) {
            for (LabelNodeType labelNode : labels.getLabelNode()) {

                if (labelNode.getUniqueId() == uniqueId) {
                    // found the node -> search the label
                    return labelNode;
                }
            }
        }
        return null;
    }

    /**
     * Get the label of the provided id.
     * 
     * @param labelNode
     *            the labelNode
     * @param labelId
     *            the label id
     * @return the label or <code>null</code> if none is found
     */
    public static LabelType getLabel(LabelNodeType labelNode, int labelId) {

        if (labelNode == null) {
            return null;
        }

        LabelType label = null;
        // search the label
        for (LabelType currentLabel : labelNode.getLabel()) {

            if (currentLabel.getIndex() == labelId) {
                // found the label
                label = currentLabel;
                break;
            }
        }
        return label;
    }

    /**
     * Replace the label.
     * 
     * @param labels
     *            the labels
     * @param uniqueId
     *            the unique id of the node
     * @param labelId
     *            the id of the label
     * @param labelString
     *            the new label string
     */
    public static void replaceLabel(
        Labels labels, long uniqueId, int labelId, final String labelString, final String defaultLabelType) {
        LabelType labelType = null;
        LabelNodeType labelNode = null;

        for (LabelNodeType currentLabelNode : labels.getLabelNode()) {

            if (currentLabelNode.getUniqueId() == uniqueId) {
                // found the node -> search the label
                labelNode = currentLabelNode;

                // search the accessory
                for (LabelType label : currentLabelNode.getLabel()) {

                    if (label.getIndex() == labelId) {
                        // found the label
                        labelType = label;
                        break;
                    }

                }
                break;
            }
        }

        // make sure we have a labelNode
        if (labelNode == null) {
            // create new labelNode
            labelNode = new LabelNodeType();
            labelNode.setType("node");
            labelNode.setUniqueId(uniqueId);

            labels.getLabelNode().add(labelNode);
        }

        if (labelType == null) {
            // create default label
            labelType = new LabelType();
            labelType.setType(defaultLabelType);
            labelType.setIndex(labelId);
            labelType.setLabelString(labelString);

            labelNode.getLabel().add(labelType);
        }
        else {
            // update the label
            labelType.setLabelString(labelString);
        }
    }

    public static <T extends Port<?>> void applyPortLabels(
        Node node, String labelKey, String defaultLabelType, List<T> ports) {

        org.bidib.wizard.labels.Labels labels =
            DefaultApplicationContext.getInstance().get(labelKey, org.bidib.wizard.labels.Labels.class);

        if (labels == null) {
            ChildLabels portLabels = (ChildLabels) DefaultApplicationContext.getInstance().get(labelKey);
            LOGGER.info("Fetched legacy ChildLabels.");
            if (portLabels != null) {
                for (T port : ports) {
                    // set the label
                    port.setLabel(portLabels.getLabel(node.getNode().getUniqueId(), port.getId()));
                }
            }
            else {
                LOGGER.warn("No port labels available for labelKey: {}", labelKey);
            }
        }
        else {
            for (T port : ports) {
                // set the label
                port.setLabel(PortLabelUtils
                    .getLabel(labels, node.getNode().getUniqueId(), port.getId(), defaultLabelType).getLabelString());
            }
        }
    }

    public static Map<Integer, String> getCopyOfMap(final Labels inMap, long uniqueId) {

        Map<Integer, String> labelMap = new HashMap<Integer, String>();

        for (LabelNodeType labelNode : inMap.getLabelNode()) {

            if (labelNode.getUniqueId() == uniqueId) {
                // found the node -> search the label

                for (LabelType label : labelNode.getLabel()) {

                    if (StringUtils.isNotBlank(label.getLabelString())) {
                        labelMap.put(label.getIndex(), label.getLabelString());
                    }
                }
                break;
            }
        }

        return labelMap;
    }

    public static Integer getIdByLabel(Labels labels, long uniqueId, String labelValue) {
        Integer id = null;

        for (LabelNodeType labelNode : labels.getLabelNode()) {

            if (labelNode.getUniqueId() == uniqueId) {
                // found the node -> search the label

                for (LabelType label : labelNode.getLabel()) {

                    if (StringUtils.isNotBlank(label.getLabelString())
                        && label.getLabelString().equalsIgnoreCase(labelValue)) {
                        LOGGER.info("Found corresponding label: {}", label);
                        id = label.getIndex();

                        break;
                    }
                }
                break;
            }
        }
        return id;
    }

    public static <E extends Port<?>> void saveLabels(
        final LabelFactory labelFactory, final Labels lightPortLabels, final Node node) {
        try {
            long uniqueId = node.getUniqueId();
            labelFactory.saveLabels(uniqueId, lightPortLabels);
        }
        catch (InvalidConfigurationException ex) {
            LOGGER.warn("Save port labels failed.", ex);

            String labelPath = ex.getReason();
            JOptionPane.showMessageDialog(JOptionPane.getFrameForComponent(null),
                Resources.getString(Labels.class, "labelfileerror.message", new Object[] { labelPath }),
                Resources.getString(Labels.class, "labelfileerror.title"), JOptionPane.ERROR_MESSAGE);
        }
        catch (Exception ex) {
            LOGGER.warn("Save port labels failed.", ex);

            String message = ex.getMessage();
            JOptionPane.showMessageDialog(JOptionPane.getFrameForComponent(null),
                Resources.getString(Labels.class, "labelfileerror.message", new Object[] { message }),
                Resources.getString(Labels.class, "labelfileerror.title"), JOptionPane.ERROR_MESSAGE);
        }
    }

}
