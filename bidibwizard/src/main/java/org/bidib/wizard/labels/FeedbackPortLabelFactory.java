package org.bidib.wizard.labels;

import org.bidib.wizard.labels.WizardLabelFactory.LabelTypes;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FeedbackPortLabelFactory extends AbstractLabelFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(FeedbackPortLabelFactory.class);

    public static final String DEFAULT_LABELTYPE = "feedbackPort";

    @Override
    protected String getLabelFileName() {
        return "FeedbackPortLabels.labels";
    }

    @Override
    protected String getLabelTypeName() {
        return DEFAULT_LABELTYPE;
    }

    @Override
    protected LabelTypes getLabelType() {
        return LabelTypes.feedbackPort;
    }

    @Override
    protected String getApplicationContextKey() {
        // TODO Auto-generated method stub
        return DefaultApplicationContext.KEY_FEEDBACKPORT_LABELS;
    }

    protected Labels migrateLabels() {
        LOGGER.info("Try to migrate exisiting feedback port labels.");

        return null;
    }

    // @Override
    // protected ChildLabels getLegacyLabels() {
    // return null;
    // }

    @Override
    protected void saveLabelsToBiDiB(Long uniqueId, Labels labels) {
        LOGGER.info("Save labels to BiDiB, uniqueId: {}", uniqueId);

        // WizardLabelFactory bidibLabelFactory = WizardLabelFactory.getInstance();
        // bidibLabelFactory.updateLabels(uniqueId, labels, WizardLabelFactory.LabelTypes.feedbackPort);
        // bidibLabelFactory.saveLabels(uniqueId);
        updateLabels(uniqueId, WizardLabelFactory.LabelTypes.feedbackPort);
    }

    @Override
    protected void loadLabelsFromBiDiB(Long uniqueId, Labels labels) {
        LOGGER.info("Load labels from BiDiB, uniqueId: {}", uniqueId);

        mergeLabels(uniqueId, WizardLabelFactory.LabelTypes.feedbackPort);
    }
}
