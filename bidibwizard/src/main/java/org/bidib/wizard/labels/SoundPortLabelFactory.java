package org.bidib.wizard.labels;

import org.bidib.wizard.labels.WizardLabelFactory.LabelTypes;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SoundPortLabelFactory extends AbstractLabelFactory {
    private static final Logger LOGGER = LoggerFactory.getLogger(SoundPortLabelFactory.class);

    public static final String DEFAULT_LABELTYPE = "soundPort";

    @Override
    protected String getLabelFileName() {
        return "SoundPortLabels.labels";
    }

    @Override
    protected String getLabelTypeName() {
        return DEFAULT_LABELTYPE;
    }

    @Override
    protected LabelTypes getLabelType() {
        return LabelTypes.soundPort;
    }

    @Override
    protected String getApplicationContextKey() {
        // TODO Auto-generated method stub
        return DefaultApplicationContext.KEY_SOUNDPORT_LABELS;
    }

    // @Override
    // protected ChildLabels getLegacyLabels() {
    // return new SoundPortLabels();
    // }

    @Override
    protected void saveLabelsToBiDiB(Long uniqueId, Labels labels) {
        LOGGER.info("Save labels to BiDiB, uniqueId: {}", uniqueId);

        // WizardLabelFactory bidibLabelFactory = WizardLabelFactory.getInstance();
        // bidibLabelFactory.updateLabels(uniqueId, labels, WizardLabelFactory.LabelTypes.soundPort);
        // bidibLabelFactory.saveLabels(uniqueId);
        updateLabels(uniqueId, WizardLabelFactory.LabelTypes.soundPort);
    }

    @Override
    protected void loadLabelsFromBiDiB(Long uniqueId, Labels labels) {
        LOGGER.info("Load labels from BiDiB, uniqueId: {}", uniqueId);

        mergeLabels(uniqueId, WizardLabelFactory.LabelTypes.soundPort);
    }
}
