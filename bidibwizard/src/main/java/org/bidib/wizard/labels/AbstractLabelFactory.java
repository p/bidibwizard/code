package org.bidib.wizard.labels;

import java.io.File;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.collections4.Predicate;
import org.bidib.wizard.labels.WizardLabelFactory.LabelTypes;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.bidib.wizard.mvc.preferences.model.Preferences;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractLabelFactory implements LabelFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractLabelFactory.class);

    public static final String XSD_LOCATION = "/xsd/wizard-labels.xsd";

    private Labels labels;

    private Object lock = new Object();

    protected abstract String getLabelFileName();

    protected abstract String getLabelTypeName();

    protected abstract String getApplicationContextKey();

    protected String getDefaultSearchPath() {
        return null;
    }

    @Override
    public Labels getLabels(Long uniqueId) {
        return getLabels(uniqueId, null);
    }

    /**
     * @return the type of labels to load
     */
    protected abstract LabelTypes getLabelType();

    /**
     * Load the labels from the provided file.
     * 
     * @param uniqueId
     *            the uniqueId of the node
     * @param filePath
     *            the file
     * @return the labels
     */
    public Labels getLabels(Long uniqueId, String filePath) {

        LOGGER.info("Get labels for uniqueId: {}, filePath: {}", uniqueId, filePath);
        synchronized (lock) {
            if (labels == null) {

                // get the existing labels from the applicationContext first because the factory is no singleton
                labels = DefaultApplicationContext.getInstance().get(getApplicationContextKey(), Labels.class);
                if (labels == null) {
                    labels = new Labels();

                    LOGGER.info("Register labels in applicationContext under key: {}", getApplicationContextKey());
                    DefaultApplicationContext.getInstance().register(getApplicationContextKey(), labels);
                }

                if (uniqueId != null) {
                    LOGGER.info("Load and merge labels for uniqueId: {}", uniqueId);

                    loadLabelsFromBiDiB(uniqueId, labels);
                }
                else {
                    LOGGER.info("Do not load labels if no uniqueId available.");
                }
            }
            else {
                if (uniqueId != null) {
                    // check if the node labels are already loaded
                    for (LabelNodeType labelNode : labels.getLabelNode()) {
                        if (labelNode.getUniqueId() == uniqueId) {
                            LOGGER.info("Labels for new node are loaded already, uniqueId: {}", uniqueId);
                            return labels;
                        }
                    }

                    LOGGER.info("Load and merge labels for uniqueId: {}", uniqueId);
                    // load labels from bidib node file
                    loadLabelsFromBiDiB(uniqueId, labels);
                }
            }

        }
        LOGGER.debug("Return labels: {}", labels);
        return labels;
    }

    @Override
    public void saveLabels(Long uniqueId, final Labels labels) {
        saveLabels(uniqueId, labels, getDefaultFileName(), true);
    }

    /**
     * @return the default filename of the labels
     */
    public String getDefaultFileName() {
        String result = null;
        File baseDir = new File(Preferences.getInstance().getLabelV2Path());

        if (!baseDir.exists()) {
            baseDir.mkdir();
        }
        if (baseDir.exists()) {
            result = new File(baseDir, getLabelFileName()).toString();
        }
        return result;
    }

    /**
     * Save the labels to bidib.xsd format.
     * 
     * @param uniqueId
     *            the uniqueId of the node
     * @param labels
     *            the labels
     */
    protected abstract void saveLabelsToBiDiB(Long uniqueId, final Labels labels);

    /**
     * Load the labels from bidib.xsd format.
     * 
     * @param uniqueId
     *            the uniqueId of the node
     * @param labels
     *            the labels
     */
    protected abstract void loadLabelsFromBiDiB(Long uniqueId, final Labels labels);

    /**
     * Save the labels.
     * 
     * @param uniqueId
     *            the uniqueId of the node
     * @param labels
     *            the labels
     * @param fileName
     *            the filename
     * @param gzip
     *            use gzip
     */
    public void saveLabels(Long uniqueId, final Labels labels, final String fileName, boolean gzip) {

        // TODO remove gzip = false
        gzip = false;

        LOGGER.info("Save labels content to file: {}, uniqueId: {}", fileName, uniqueId);

        if (this.labels == null) {
            LOGGER.debug("Set the provided labels: {}", labels);
            this.labels = labels;
        }

        if (uniqueId != null) {
            // save the labels in bidib.xsd format
            LOGGER.info("Save the labels to bidib.xsd format, uniqueId: {}", uniqueId);
            try {
                saveLabelsToBiDiB(uniqueId, labels);
            }
            catch (Exception ex) {
                LOGGER.warn("Save the labels to bidib.xsd format failed, uniqueId: {}", uniqueId, ex);
            }

            LOGGER.info("Save labels to bidib passed.");
            return;
        }
    }

    /**
     * Replace the label.
     * 
     * @param labels
     *            the labels
     * @param uniqueId
     *            the unique id of the node
     * @param labelId
     *            the id of the label
     * @param labelString
     *            the new label string
     */
    public void replaceLabel(Labels labels, long uniqueId, int labelId, final String labelString) {
        LabelType labelType = null;
        LabelNodeType labelNode = null;

        for (LabelNodeType currentLabelNode : labels.getLabelNode()) {

            if (currentLabelNode.getUniqueId() == uniqueId) {
                // found the node -> search the label
                labelNode = currentLabelNode;

                // search the accessory
                for (LabelType label : currentLabelNode.getLabel()) {

                    if (label.getIndex() == labelId) {
                        // found the label
                        labelType = label;
                        break;
                    }

                }
                break;
            }
        }

        // make sure we have a labelNode
        if (labelNode == null) {
            // create new labelNode
            labelNode = new LabelNodeType();
            labelNode.setType("node");
            labelNode.setUniqueId(uniqueId);

            labels.getLabelNode().add(labelNode);
        }

        if (labelType == null) {
            // create default label
            labelType = new LabelType();
            labelType.setType(getLabelTypeName());
            labelType.setIndex(labelId);
            labelType.setLabelString(labelString);

            labelNode.getLabel().add(labelType);
        }
        else {
            // update the label
            labelType.setLabelString(labelString);
        }
    }

    /**
     * Get the labels for the node with the provided uniqueId and the provided label id.
     * 
     * @param uniqueId
     *            the unique id
     * @param labelId
     *            the label id
     * @return the label
     */
    public LabelType getLabel(Labels labels, long uniqueId, int labelId) {
        LabelType labelType = null;

        for (LabelNodeType labelNode : labels.getLabelNode()) {

            if (labelNode.getUniqueId() == uniqueId) {
                // found the node -> search the label
                for (LabelType label : labelNode.getLabel()) {

                    if (label.getIndex() == labelId) {
                        // found the label
                        labelType = label;
                        break;
                    }

                }
                break;
            }
        }

        if (labelType == null) {
            // create default value
            labelType = new LabelType();
            labelType.setType(getLabelTypeName());
            labelType.setIndex(labelId);
        }

        return labelType;
    }

    protected void updateLabels(Long uniqueId, final WizardLabelFactory.LabelTypes type) {

        WizardLabelFactory bidibLabelFactory = WizardLabelFactory.getInstance();
        bidibLabelFactory.updateLabels(uniqueId, labels, type);
        bidibLabelFactory.saveLabels(uniqueId, getDefaultSearchPath());
    }

    protected void mergeLabels(Long uniqueId, final WizardLabelFactory.LabelTypes type) {

        WizardLabelFactory bidibLabelFactory = WizardLabelFactory.getInstance();
        Labels loadedLabels = bidibLabelFactory.loadLabels(uniqueId, type, getDefaultSearchPath());

        LOGGER.debug("Loaded labels from WizardLabelFactory: {}", loadedLabels);

        if (loadedLabels != null && CollectionUtils.isNotEmpty(loadedLabels.getLabelNode())) {
            // merge the labels
            LabelNodeType labelNode = loadedLabels.getLabelNode().get(0);
            LOGGER.debug("Merge the node label: {}", labelNode);

            // merge the labels ...

            if (CollectionUtils.isNotEmpty(labels.getLabelNode())) {
                LabelNodeType toMerge = null;
                for (LabelNodeType currentLabelNode : labels.getLabelNode()) {
                    if (currentLabelNode.getUniqueId() == uniqueId) {
                        toMerge = currentLabelNode;
                        break;
                    }
                }

                if (toMerge != null && CollectionUtils.isNotEmpty(loadedLabels.getLabelNode())) {
                    LOGGER.debug("Merge in labelNode: {}", toMerge);

                    LabelNodeType labelNodeToMerge = loadedLabels.getLabelNode().get(0);
                    for (LabelType labelToMerge : labelNodeToMerge.getLabel()) {
                        LOGGER.debug("Merge new labelNode: {}", labelNodeToMerge);
                        final long indexLabel = labelToMerge.getIndex();
                        LabelType labelFound = IterableUtils.find(toMerge.getLabel(), new Predicate<LabelType>() {

                            @Override
                            public boolean evaluate(LabelType label) {
                                return label.getIndex() == indexLabel;
                            }
                        });

                        if (labelFound != null) {
                            LOGGER.debug("Found label to remove: {}", labelFound);
                            toMerge.getLabel().remove(labelFound);
                        }
                        LOGGER.debug("Add labelToMerge: {}", labelToMerge);
                        toMerge.getLabel().add(labelToMerge);
                    }
                }
                else {
                    LOGGER.info("No label to merge found.");
                    // add new node
                    labels.getLabelNode().add(labelNode);
                }
            }
            else {
                // add new node
                labels.getLabelNode().add(labelNode);
            }
        }
    }
}
