package org.bidib.wizard.labels;

import java.awt.Frame;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.bidib.jbidibc.core.schema.bidib2.Accessory;
import org.bidib.jbidibc.core.schema.bidib2.FeedbackPort;
import org.bidib.jbidibc.core.schema.bidib2.InputKey;
import org.bidib.jbidibc.core.schema.bidib2.Macro;
import org.bidib.jbidibc.core.schema.bidib2.Node;
import org.bidib.jbidibc.core.schema.bidib2.OutputAnalog;
import org.bidib.jbidibc.core.schema.bidib2.OutputBacklight;
import org.bidib.jbidibc.core.schema.bidib2.OutputLight;
import org.bidib.jbidibc.core.schema.bidib2.OutputMotor;
import org.bidib.jbidibc.core.schema.bidib2.OutputServo;
import org.bidib.jbidibc.core.schema.bidib2.OutputSound;
import org.bidib.jbidibc.core.schema.bidib2.OutputSwitch;
import org.bidib.jbidibc.core.schema.bidib2.OutputSwitchPair;
import org.bidib.jbidibc.core.schema.bidibbase.BaseLabel;
import org.bidib.jbidibc.core.schema.bidibbase.PortType;
import org.bidib.jbidibc.core.schema.bidiblabels.AccessoryLabel;
import org.bidib.jbidibc.core.schema.bidiblabels.AccessoryLabels;
import org.bidib.jbidibc.core.schema.bidiblabels.FeedbackPortLabels;
import org.bidib.jbidibc.core.schema.bidiblabels.FlagLabels;
import org.bidib.jbidibc.core.schema.bidiblabels.LabelFactory;
import org.bidib.jbidibc.core.schema.bidiblabels.MacroLabels;
import org.bidib.jbidibc.core.schema.bidiblabels.NodeLabel;
import org.bidib.jbidibc.core.schema.bidiblabels.NodeLabels;
import org.bidib.jbidibc.core.schema.bidiblabels.PortLabel;
import org.bidib.jbidibc.core.schema.bidiblabels.PortLabels;
import org.bidib.jbidibc.core.schema.exception.NodeNotAvailableException;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.dialog.FileDialog;
import org.bidib.wizard.migration.migrator.WizardMigrator;
import org.bidib.wizard.mvc.main.model.Flag;
import org.bidib.wizard.mvc.preferences.model.Preferences;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WizardLabelFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(WizardLabelFactory.class);

    private static final String BIDIB_DEFAULT_LABEL_SUBDIR = "labels";

    // private static final String MIGRATION_FILE = "wizard-migration.log";

    public enum LabelTypes {
        // @formatter:off
        accessory(Accessory.class), analogPort(OutputAnalog.class, PortType.ANALOGOUT), 
        backlightPort(OutputBacklight.class, PortType.BACKLIGHT), feedbackPort(FeedbackPort.class), 
        inputPort(InputKey.class, PortType.INPUT), lightPort(OutputLight.class, PortType.LIGHT), 
        macro(Macro.class), motorPort(OutputMotor.class, PortType.MOTOR), node(Node.class), 
        servoPort(OutputServo.class, PortType.SERVO), soundPort(OutputSound.class, PortType.SOUND), 
        switchPort(OutputSwitch.class, PortType.SWITCH), switchPairPort(OutputSwitchPair.class, PortType.SWITCHPAIR), 
        flag(Flag.class);
        
        // @formatter:on

        private final Class<?> portClazz;

        private final PortType portType;

        LabelTypes(final Class<?> portClazz) {
            this(portClazz, null);
        }

        LabelTypes(final Class<?> portClazz, PortType portType) {
            this.portClazz = portClazz;
            this.portType = portType;
        }

        public Class<?> getPortClazz() {
            return portClazz;
        }

        public PortType getPortType() {
            return portType;
        }
    }

    private Object portLabelMapLock = new Object();

    private Map<Long, NodeLabels> portLabelMap = new HashMap<>();

    private final LabelFactory labelFactory;

    private static WizardLabelFactory INSTANCE;

    private static boolean skipMigration;

    /**
     * @return the skipMigration
     */
    public static boolean isSkipMigration() {
        return skipMigration;
    }

    /**
     * @param skipMigration
     *            the skipMigration to set
     */
    public static void setSkipMigration(boolean skipMigration) {
        WizardLabelFactory.skipMigration = skipMigration;

        LOGGER.warn("The instance is released!");
        INSTANCE = null;
    }

    /**
     * Private constructor to prevent create local instances.
     */
    private WizardLabelFactory() {

        LOGGER.info("Create the WizardLabelFactory instance.");

        // prepare the instance of labelFactory
        labelFactory = new LabelFactory();
    }

    /**
     * @return the instance of {@code BidibLabelFactory}.
     */
    public static synchronized WizardLabelFactory getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new WizardLabelFactory();

            LOGGER.warn("Call check and perform migration.");
            try {
                // check if migration is pending
                INSTANCE.checkAndPerformMigration();
            }
            catch (IllegalArgumentException ex) {
                LOGGER.warn("Check and perform migration failed.", ex);

                throw ex;
            }
            catch (Exception ex) {
                LOGGER.warn("Check and perform migration failed.", ex);
            }
        }

        return INSTANCE;
    }

    public Labels loadLabels(Long uniqueId, LabelTypes type, String... searchRoot) {
        LOGGER.info("Load labels for uniqueId: {}, type: {}, searchRoot: {}", uniqueId, type, searchRoot);

        // lookup the portLabelMap if the labels of the node are loaded already
        synchronized (portLabelMapLock) {
            NodeLabels nodeLabels = portLabelMap.get(uniqueId);

            if (nodeLabels == null) {

                // prepare the node file location from the uniqueId
                File labelsFile = null;
                FileInputStream fis = null;
                try {
                    // prepare the filename
                    String nodeFileName = LabelFactory.prepareNodeFilename(uniqueId);

                    LOGGER.info("Search for labels of nodeFileName: {}, searchRoot: {}", nodeFileName, searchRoot);

                    String path =
                        (ArrayUtils.isNotEmpty(searchRoot) && StringUtils.isNotBlank(searchRoot[0]) ? searchRoot[0]
                            : getDefaultSearchRoot()[0]);
                    LOGGER.info("Prepared path: {}", path);
                    labelsFile = new File(path, nodeFileName);

                    fis = new FileInputStream(labelsFile);
                    nodeLabels = labelFactory.loadLabels(fis);

                    // store the labels in the map
                    portLabelMap.put(uniqueId, nodeLabels);
                }
                catch (FileNotFoundException ex) {
                    LOGGER.warn("Prepare file to load labels failed because file is not available: {}",
                        ex.getMessage());
                }
                catch (Exception ex) {
                    LOGGER.warn("Prepare file to load labels failed.", ex);
                }
                finally {
                    if (fis != null) {
                        try {
                            fis.close();
                        }
                        catch (Exception ex) {
                            LOGGER.warn("Close file input stream failed.", ex);
                        }
                    }
                }
            }

            if (nodeLabels != null) {
                LOGGER.info("Use cached labels.");
                return loadLabels(nodeLabels, type);
            }
        }

        LOGGER.warn("No label available for node with uniqueId: {}", uniqueId);
        return null;
    }

    public void updateLabels(final Long uniqueId, final Labels labels, final LabelTypes type) {

        synchronized (portLabelMapLock) {
            NodeLabels nodeLabels = portLabelMap.get(uniqueId);

            if (nodeLabels == null) {
                LOGGER.warn(
                    "No cached nodeLabels available, because no node in portLabel map for uniqueId: {}. Create new nodeLabels.",
                    uniqueId);

                nodeLabels = new NodeLabels().withNodeLabel(new NodeLabel().withUniqueId(uniqueId));
                portLabelMap.put(uniqueId, nodeLabels);
            }

            // update the labels
            try {

                switch (type) {
                    case node:
                        updateNodeLabels(uniqueId, nodeLabels, labels, type);
                        break;
                    case accessory:
                        updateAccessoryLabels(uniqueId, nodeLabels, labels, type);
                        break;
                    case macro:
                        updateMacroLabels(uniqueId, nodeLabels, labels, type);
                        break;
                    case flag:
                        updateFlagLabels(uniqueId, nodeLabels, labels, type);
                        break;
                    case feedbackPort:
                        updateFeedbackPortLabels(uniqueId, nodeLabels, labels, type);
                        break;
                    default:
                        updatePortLabels(uniqueId, nodeLabels, labels, type);
                        break;
                }
            }
            catch (Exception ex) {
                LOGGER.warn("Update nodeLabels from provided labels failed.", ex);
            }
        }
    }

    /**
     * Save the labels.
     * 
     * @param uniqueId
     *            the uniqueId of the node
     */
    public void saveLabels(Long uniqueId, String... searchRoot) {

        synchronized (portLabelMapLock) {

            NodeLabels nodeLabels = portLabelMap.get(uniqueId);
            if (nodeLabels != null) {
                LOGGER.info("Found nodeLabels to save for uniqueId: {}", uniqueId);

                String path =
                    (ArrayUtils.isNotEmpty(searchRoot) && StringUtils.isNotBlank(searchRoot[0]) ? searchRoot[0]
                        : getDefaultSearchRoot()[0]);

                // prepare the filename
                // TODO better provide a file to saveNodeLabels
                File file = new File(path, LabelFactory.prepareNodeFilename(uniqueId));

                LOGGER.info("Save nodeLabels to file: {}", file);

                labelFactory.saveNodeLabel(nodeLabels, file, false);
            }
            else {
                LOGGER.warn("Save node labels failed for node with uniqueId: {}", uniqueId);
            }
        }
    }

    protected Labels loadLabels(final NodeLabels nodeLabels, final LabelTypes type) {

        Labels labels = null;

        try {
            switch (type) {
                case node:
                    labels = getNodeLabels(nodeLabels);
                    break;
                case accessory:
                    labels = getAccessoryLabels(nodeLabels);
                    break;
                case macro:
                    labels = getMacroLabels(nodeLabels);
                    break;
                case flag:
                    labels = getFlagLabels(nodeLabels);
                    break;
                case feedbackPort:
                    labels = getFeedbackPortLabels(nodeLabels);
                    break;
                default:
                    labels = getPortLabels(nodeLabels, type);
                    break;
            }
        }
        catch (NodeNotAvailableException ex) {
            LOGGER.warn("Get node from saved labels failed.", ex);
        }

        return labels;
    }

    protected String[] getDefaultSearchRoot() {

        final Preferences prefs = Preferences.getInstance();

        File[] bidibBaseDir = new File[1];
        bidibBaseDir[0] = new File(prefs.getLabelV2Path(), BIDIB_DEFAULT_LABEL_SUBDIR);

        String searchRoot = bidibBaseDir[0].getPath();
        LOGGER.debug("Default search root: {}", searchRoot);

        // check if the target path exists
        if (!bidibBaseDir[0].exists()) {
            LOGGER.warn("The target path for migration does not exist. Ask user to provide new directory!");

            Frame frame = JOptionPane.getFrameForComponent(null);

            final FileFilter[] ff = null;
            FileDialog dialog = new FileDialog(frame, FileDialog.SAVE, (String) null, ff) {
                @Override
                public void approve(String selectedFile) {
                    LOGGER.info("Selected file: {}", selectedFile);
                    File file = new File(selectedFile);

                    try {
                        FileUtils.forceMkdir(new File(file, BIDIB_DEFAULT_LABEL_SUBDIR));
                        bidibBaseDir[0] = new File(file, BIDIB_DEFAULT_LABEL_SUBDIR);

                        prefs.setLabelV2Path(file.getPath());
                        prefs.save(null);
                    }
                    catch (Exception ex) {
                        LOGGER.warn("The selected directory could not be created: {}", selectedFile);

                        throw new RuntimeException("The selected directory could not be created: " + selectedFile);
                    }

                }
            };
            dialog.setApproveButtonText(Resources.getString(WizardLabelFactory.class, "choose-xml-location"));
            dialog.showDialog();

            if (bidibBaseDir[0] == null || !bidibBaseDir[0].exists()) {
                // force valid directory
                throw new IllegalArgumentException("The selected directory could not be created: " + bidibBaseDir);
            }

            searchRoot = bidibBaseDir[0].getPath();

            LOGGER.info("Corrected target path for migration: {}", searchRoot);
        }

        String labelPath2 = Preferences.getInstance().getLabelPath();
        if (StringUtils.isBlank(labelPath2)) {
            labelPath2 = Preferences.getInstance().getLabelV2Path();
        }

        File fallbackBaseDir = new File(labelPath2);

        if (fallbackBaseDir.exists()) {

            String searchFallback = fallbackBaseDir.getPath();
            LOGGER.debug("Retired search root: {}", searchFallback);

            return new String[] { searchRoot, searchFallback };
        }

        return new String[] { searchRoot };
    }

    private void checkAndPerformMigration() {

        if (skipMigration) {
            LOGGER.warn("The check for migration is skipped by configuration!!!");
            return;
        }

        // check if the migration file exists
        String[] searchRoots = getDefaultSearchRoot();

        // check if the target path exists
        if (!new File(searchRoots[0]).exists()) {
            LOGGER.warn("The target path for migration does not exist. Skip migration!");
            return;
        }

        WizardMigrator migrator = new WizardMigrator();
        File migrationLogFile = migrator.getMigrationLogfile(searchRoots[0]);

        LOGGER.info("Check if the migration log exists under: {}", migrationLogFile);
        if (migrationLogFile.exists()) {
            LOGGER.info("The migration was performed already, the migration log exists under: {}", migrationLogFile);

            return;
        }

        String targetPath = searchRoots[0];
        LOGGER.info("Prepared targetPath: {}, all searchRoots: {}", targetPath, searchRoots);

        // perform the migration
        String migrationTargetPath = migrator.checkAndPerformMigration(targetPath, migrationLogFile, searchRoots);

        // update the preferences if necessary
        Preferences preferences = Preferences.getInstance();
        if (StringUtils.isNotBlank(migrationTargetPath) && !preferences.isLabelV2PathSet()) {
            LOGGER.info("Migration returned migrationTargetPath: {}", migrationTargetPath);

            File file = new File(migrationTargetPath);
            migrationTargetPath = file.getParent();
            LOGGER.info("Update the LabelV2Path in preferences with labelPath: {}", migrationTargetPath);

            preferences.setLabelV2Path(migrationTargetPath);
            preferences.save(null);
        }
    }

    private Labels getNodeLabels(final NodeLabels nodeLabels) {
        NodeLabel nodeLabel = nodeLabels.getNodeLabel();

        // prepare the labels of the wizard
        Labels labels = new Labels();
        long currentUniqueId = nodeLabel.getUniqueId();

        final LabelNodeType labelNode = new LabelNodeType();
        labelNode.setType("node");
        labelNode.setUniqueId(currentUniqueId);
        // labelNode.setLabelString(nodeLabel.getUserName());

        LabelType label = new LabelType();
        labelNode.withLabel(label);
        label.setType("node");
        label.setIndex(0);
        label.setLabelString(nodeLabel.getUserName());

        labels.withLabelNode(labelNode);

        return labels;
    }

    private Labels getAccessoryLabels(final NodeLabels nodeLabels) {
        NodeLabel nodeLabel = nodeLabels.getNodeLabel();
        AccessoryLabels accessoryLabels = nodeLabels.getAccessoryLabels();

        // prepare the labels of the wizard
        Labels labels = new Labels();
        long currentUniqueId = nodeLabel.getUniqueId();

        final LabelNodeType labelNode = new LabelNodeType();
        labelNode.setType("node");
        labelNode.setUniqueId(currentUniqueId);

        labels.withLabelNode(labelNode);

        if (accessoryLabels != null && CollectionUtils.isNotEmpty(accessoryLabels.getAccessoryLabel())) {

            convertAccessoryLabels(accessoryLabels.getAccessoryLabel(), labelNode);
        }
        return labels;
    }

    private void convertAccessoryLabels(final List<AccessoryLabel> accessories, final LabelNodeType labelNode) {
        for (AccessoryLabel accessoryLabel : accessories) {

            // create default label
            LabelType labelType = new LabelType();
            labelType.setType(LabelTypes.accessory.name());
            labelType.setIndex(accessoryLabel.getIndex());
            labelType.setLabelString(accessoryLabel.getLabel());

            labelNode.getLabel().add(labelType);

            if (accessoryLabel.getAspectLabel() != null
                && CollectionUtils.isNotEmpty(accessoryLabel.getAspectLabel())) {

                // add child labels element
                labelType.withChildLabels(new ChildLabelType());

                // convert the aspects
                for (BaseLabel aspect : accessoryLabel.getAspectLabel()) {
                    LabelType labelTypeAspect = new LabelType();
                    labelTypeAspect.setType("aspect");
                    labelTypeAspect.setIndex(aspect.getIndex());
                    labelTypeAspect.setLabelString(aspect.getLabel());

                    labelType.getChildLabels().withLabel(labelTypeAspect);
                }
            }
        }
    }

    private Labels getMacroLabels(final NodeLabels nodeLabels) {
        NodeLabel nodeLabel = nodeLabels.getNodeLabel();

        // prepare the labels of the wizard
        Labels labels = new Labels();
        long currentUniqueId = nodeLabel.getUniqueId();

        final LabelNodeType labelNode = new LabelNodeType();
        labelNode.setType("node");
        labelNode.setUniqueId(currentUniqueId);

        labels.withLabelNode(labelNode);

        if (nodeLabels.getMacroLabels() != null
            && CollectionUtils.isNotEmpty(nodeLabels.getMacroLabels().getMacroLabel())) {

            convertLabels(nodeLabels.getMacroLabels().getMacroLabel(), labelNode, LabelTypes.macro);
        }

        return labels;
    }

    private Labels getFlagLabels(final NodeLabels nodeLabels) {
        NodeLabel nodeLabel = nodeLabels.getNodeLabel();

        // prepare the labels of the wizard
        Labels labels = new Labels();
        long currentUniqueId = nodeLabel.getUniqueId();

        final LabelNodeType labelNode = new LabelNodeType();
        labelNode.setType("node");
        labelNode.setUniqueId(currentUniqueId);

        labels.withLabelNode(labelNode);

        if (nodeLabels.getFlagLabels() != null
            && CollectionUtils.isNotEmpty(nodeLabels.getFlagLabels().getFlagLabel())) {

            convertLabels(nodeLabels.getFlagLabels().getFlagLabel(), labelNode, LabelTypes.flag);
        }

        return labels;
    }

    private void convertLabels(final List<BaseLabel> macros, final LabelNodeType labelNode, final LabelTypes type) {
        for (BaseLabel macro : macros) {

            // create default label
            LabelType labelType = new LabelType();
            labelType.setType(type.name());
            labelType.setIndex(macro.getIndex());
            labelType.setLabelString(macro.getLabel());

            labelNode.getLabel().add(labelType);
        }
    }

    private Labels getFeedbackPortLabels(final NodeLabels nodeLabels) {
        NodeLabel nodeLabel = nodeLabels.getNodeLabel();

        // prepare the labels of the wizard
        Labels labels = new Labels();
        long currentUniqueId = nodeLabel.getUniqueId();

        final LabelNodeType labelNode = new LabelNodeType();
        labelNode.setType("node");
        labelNode.setUniqueId(currentUniqueId);

        labels.withLabelNode(labelNode);

        if (nodeLabels.getFeedbackPortLabels() != null
            && CollectionUtils.isNotEmpty(nodeLabels.getFeedbackPortLabels().getPortLabel())) {
            convertLabels(nodeLabels.getFeedbackPortLabels().getPortLabel(), labelNode, LabelTypes.feedbackPort);
        }

        return labels;
    }

    private Labels getPortLabels(final NodeLabels nodeLabels, final LabelTypes type) {
        NodeLabel nodeLabel = nodeLabels.getNodeLabel();

        // prepare the labels of the wizard
        Labels labels = new Labels();
        long currentUniqueId = nodeLabel.getUniqueId();

        final LabelNodeType labelNode = new LabelNodeType();
        labelNode.setType("node");
        labelNode.setUniqueId(currentUniqueId);

        labels.withLabelNode(labelNode);

        if (nodeLabels.getPortLabels() != null
            && CollectionUtils.isNotEmpty(nodeLabels.getPortLabels().getPortLabel())) {

            List<PortLabel> ports =
                LabelFactory.getPortsOfType(nodeLabels.getPortLabels().getPortLabel(), type.getPortType());

            convertPortLabels(ports, labelNode, type);
        }
        return labels;
    }

    private void convertPortLabels(final List<PortLabel> ports, final LabelNodeType labelNode, final LabelTypes type) {
        for (PortLabel port : ports) {

            // create default label
            LabelType labelType = new LabelType();
            labelType.setType(type.name());
            labelType.setIndex(port.getIndex());
            labelType.setLabelString(port.getLabel());

            labelNode.getLabel().add(labelType);
        }
    }

    private void updateNodeLabels(
        Long uniqueId, final NodeLabels nodeLabels, final Labels labels, final LabelTypes type) {

        NodeLabel nodeLabel = nodeLabels.getNodeLabel();

        // add the accessories from labels
        for (LabelNodeType labelNode : labels.getLabelNode()) {
            if (labelNode.getUniqueId() == uniqueId) {
                // found node
                String labelString = labelNode.getLabel().get(0).getLabelString();
                nodeLabel.setUserName(labelString);
                LOGGER.info("Found node to update: {}", nodeLabel);
                break;
            }
        }
    }

    private void updateAccessoryLabels(
        Long uniqueId, final NodeLabels nodeLabels, final Labels labels, final LabelTypes type) {

        // add the accessories from labels
        for (LabelNodeType labelNode : labels.getLabelNode()) {
            if (labelNode.getUniqueId() == uniqueId) {
                // found node

                // check if accessory labels available
                if (CollectionUtils.isNotEmpty(labelNode.getLabel())) {

                    // check the accessoryLabels
                    if (nodeLabels.getAccessoryLabels() == null) {
                        nodeLabels.withAccessoryLabels(new AccessoryLabels());
                    }

                    List<AccessoryLabel> accessories = nodeLabels.getAccessoryLabels().getAccessoryLabel();
                    accessories.clear();

                    for (LabelType labelType : labelNode.getLabel()) {
                        AccessoryLabel accessory =
                            new AccessoryLabel().withIndex(labelType.getIndex()).withLabel(labelType.getLabelString());
                        accessories.add(accessory);

                        if (labelType.getChildLabels() != null
                            && !IterableUtils.isEmpty(labelType.getChildLabels().getLabel())) {

                            // process the aspects
                            for (LabelType aspectType : labelType.getChildLabels().getLabel()) {
                                BaseLabel aspect =
                                    new BaseLabel()
                                        .withIndex(aspectType.getIndex()).withLabel(aspectType.getLabelString());
                                accessory.getAspectLabel().add(aspect);
                            }
                        }
                    }
                    break;
                }
            }
        }
    }

    private void updateMacroLabels(
        Long uniqueId, final NodeLabels nodeLabels, final Labels labels, final LabelTypes type) {

        // add the macros from labels
        for (LabelNodeType labelNode : labels.getLabelNode()) {
            if (labelNode.getUniqueId() == uniqueId) {
                // found node
                // check if accessory labels available
                if (CollectionUtils.isNotEmpty(labelNode.getLabel())) {

                    // check the macroLabels
                    if (nodeLabels.getMacroLabels() == null) {
                        nodeLabels.withMacroLabels(new MacroLabels());
                    }

                    List<BaseLabel> macros = nodeLabels.getMacroLabels().getMacroLabel();
                    macros.clear();

                    for (LabelType labelType : labelNode.getLabel()) {
                        BaseLabel macro =
                            new BaseLabel().withIndex(labelType.getIndex()).withLabel(labelType.getLabelString());
                        macros.add(macro);
                    }
                }
                break;
            }
        }
    }

    private void updateFlagLabels(
        Long uniqueId, final NodeLabels nodeLabels, final Labels labels, final LabelTypes type) {

        // add the flags from labels
        for (LabelNodeType labelNode : labels.getLabelNode()) {
            if (labelNode.getUniqueId() == uniqueId) {
                // found node
                // check if flag labels available
                if (CollectionUtils.isNotEmpty(labelNode.getLabel())) {

                    // check the flagLabels
                    if (nodeLabels.getFlagLabels() == null) {
                        nodeLabels.withFlagLabels(new FlagLabels());
                    }

                    List<BaseLabel> flags = nodeLabels.getFlagLabels().getFlagLabel();
                    flags.clear();

                    for (LabelType labelType : labelNode.getLabel()) {
                        BaseLabel macro =
                            new BaseLabel().withIndex(labelType.getIndex()).withLabel(labelType.getLabelString());
                        flags.add(macro);
                    }
                }
                break;
            }
        }
    }

    private void updateFeedbackPortLabels(
        Long uniqueId, final NodeLabels nodeLabels, final Labels labels, final LabelTypes type) {

        // add the ports from labels
        for (LabelNodeType labelNode : labels.getLabelNode()) {
            if (labelNode.getUniqueId() == uniqueId) {
                // found node
                // check if accessory labels available
                if (CollectionUtils.isNotEmpty(labelNode.getLabel())) {

                    // check the feedbackPortLabels
                    if (nodeLabels.getFeedbackPortLabels() == null) {
                        nodeLabels.withFeedbackPortLabels(new FeedbackPortLabels());
                    }

                    List<BaseLabel> feedbackPorts = nodeLabels.getFeedbackPortLabels().getPortLabel();
                    feedbackPorts.clear();

                    for (LabelType labelType : labelNode.getLabel()) {
                        BaseLabel feedbackPort =
                            new BaseLabel().withIndex(labelType.getIndex()).withLabel(labelType.getLabelString());
                        feedbackPorts.add(feedbackPort);
                    }
                }
                break;
            }
        }
    }

    private void updatePortLabels(
        Long uniqueId, final NodeLabels nodeLabels, final Labels labels, final LabelTypes type) {

        // add the ports from labels
        for (LabelNodeType labelNode : labels.getLabelNode()) {
            if (labelNode.getUniqueId() == uniqueId) {
                // found node
                // check if accessory labels available
                if (CollectionUtils.isNotEmpty(labelNode.getLabel())) {

                    // check the portLabels
                    if (nodeLabels.getPortLabels() == null) {
                        nodeLabels.withPortLabels(new PortLabels());
                    }

                    // filter out ports of correct type
                    List<PortLabel> ports = nodeLabels.getPortLabels().getPortLabel();
                    filterDropPorts(ports, type);

                    for (LabelType labelType : labelNode.getLabel()) {
                        try {
                            PortLabel port =
                                new PortLabel()
                                    .withIndex(labelType.getIndex()).withLabel(labelType.getLabelString())
                                    .withType(type.getPortType());
                            ports.add(port);
                        }
                        catch (Exception ex) {
                            LOGGER.warn("Create new instance of port failed. Current labelType: {}", labelType, ex);
                        }
                    }
                }
                break;
            }
        }
    }

    /**
     * Filter out all ports of the provided type class.
     * 
     * @param ports
     *            the ports collection
     * @param type
     *            the port type to drop
     */
    private void filterDropPorts(List<PortLabel> ports, final LabelTypes type) {

        final PortType portType = type.getPortType();

        CollectionUtils.filterInverse(ports, new Predicate<PortLabel>() {

            @Override
            public boolean evaluate(PortLabel port) {
                return port.getType() == portType;
            }
        });
    }

}
