package org.bidib.wizard.labels;

public interface LabelFactory {

    /**
     * Load the labels from the default file path.
     * 
     * @param uniqueId
     *            the uniqueId of the node
     * @return the labels
     */
    Labels getLabels(Long uniqueId);

    /**
     * Save the labels.
     * 
     * @param uniqueId
     *            the uniqueId of the node
     * @param labels
     *            the labels
     */
    void saveLabels(Long uniqueId, final Labels labels);
}
