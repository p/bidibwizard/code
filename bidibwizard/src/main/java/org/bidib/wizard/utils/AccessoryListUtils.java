package org.bidib.wizard.utils;

import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.collections4.Predicate;
import org.bidib.wizard.mvc.main.model.Accessory;

public class AccessoryListUtils {

    /**
     * Find the accessory by the provided accessory number.
     * 
     * @param accessories
     *            the list of accessories
     * @param accessoryNumber
     *            the accessory number
     * @return the matching accessory or {@code null} if no matching accessory was found
     */
    public static Accessory findAccessoryByAccessoryNumber(Iterable<Accessory> accessories, final int accessoryNumber) {
        Accessory accessory = IterableUtils.find(accessories, new Predicate<Accessory>() {

            @Override
            public boolean evaluate(Accessory accessory) {
                if (accessory.getId() == accessoryNumber) {
                    return true;
                }
                return false;
            }
        });
        return accessory;
    }
}
