package org.bidib.wizard.utils;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.lang.StringUtils;
import org.bidib.jbidibc.core.Feature;
import org.bidib.jbidibc.core.StringData;
import org.bidib.jbidibc.core.enumeration.IoBehaviourInputEnum;
import org.bidib.jbidibc.core.enumeration.IoBehaviourSwitchEnum;
import org.bidib.jbidibc.core.schema.bidib2.Accessories;
import org.bidib.jbidibc.core.schema.bidib2.Aspect;
import org.bidib.jbidibc.core.schema.bidib2.Aspects;
import org.bidib.jbidibc.core.schema.bidib2.BiDiB;
import org.bidib.jbidibc.core.schema.bidib2.Features;
import org.bidib.jbidibc.core.schema.bidib2.FeedbackPorts;
import org.bidib.jbidibc.core.schema.bidib2.HourExtension;
import org.bidib.jbidibc.core.schema.bidib2.IOInputBehaviour;
import org.bidib.jbidibc.core.schema.bidib2.IOSwitchBehaviour;
import org.bidib.jbidibc.core.schema.bidib2.InputKey;
import org.bidib.jbidibc.core.schema.bidib2.MacroParameterClockStart;
import org.bidib.jbidibc.core.schema.bidib2.MacroParameterRepeat;
import org.bidib.jbidibc.core.schema.bidib2.MacroParameterSlowdown;
import org.bidib.jbidibc.core.schema.bidib2.MacroParameters;
import org.bidib.jbidibc.core.schema.bidib2.MacroPoint;
import org.bidib.jbidibc.core.schema.bidib2.MacroPoints;
import org.bidib.jbidibc.core.schema.bidib2.Macros;
import org.bidib.jbidibc.core.schema.bidib2.MinuteExtension;
import org.bidib.jbidibc.core.schema.bidib2.Nodes;
import org.bidib.jbidibc.core.schema.bidib2.OutputBacklight;
import org.bidib.jbidibc.core.schema.bidib2.OutputLight;
import org.bidib.jbidibc.core.schema.bidib2.OutputServo;
import org.bidib.jbidibc.core.schema.bidib2.OutputSwitch;
import org.bidib.jbidibc.core.schema.bidib2.Ports;
import org.bidib.jbidibc.core.schema.bidib2.Weekday;
import org.bidib.jbidibc.core.schema.bidib2.WeekdayExtension;
import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.common.context.ApplicationContext;
import org.bidib.wizard.labels.LabelNodeType;
import org.bidib.wizard.labels.LabelType;
import org.bidib.wizard.labels.Labels;
import org.bidib.wizard.labels.PortLabelUtils;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.bidib.wizard.mvc.main.model.Accessory;
import org.bidib.wizard.mvc.main.model.AnalogPort;
import org.bidib.wizard.mvc.main.model.BacklightPort;
import org.bidib.wizard.mvc.main.model.FeedbackPort;
import org.bidib.wizard.mvc.main.model.InputPort;
import org.bidib.wizard.mvc.main.model.LightPort;
import org.bidib.wizard.mvc.main.model.Macro;
import org.bidib.wizard.mvc.main.model.MacroRef;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.model.Port;
import org.bidib.wizard.mvc.main.model.ServoPort;
import org.bidib.wizard.mvc.main.model.StartCondition;
import org.bidib.wizard.mvc.main.model.SwitchPort;
import org.bidib.wizard.mvc.main.model.TimeStartCondition;
import org.bidib.wizard.mvc.main.model.function.Function;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NodeUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(NodeUtils.class);

    public static final String IMPORT_ERRORS = "importErrors";

    /**
     * Find a node by its node uuid in the provided list of nodes.
     * 
     * @param nodes
     *            the list of nodes
     * @param uuid
     *            the uuid of the node to find
     * @return the found node or <code>null</code> if no node was found with the provided node uuid
     */
    public static Node findNodeByUuid(Collection<Node> nodes, final long uuid) {
        Node node = IterableUtils.find(nodes, new Predicate<Node>() {

            @Override
            public boolean evaluate(final Node node) {
                if (node.getUniqueId() == uuid) {
                    LOGGER.debug("Found node: {}", node);
                    return true;
                }
                return false;
            }
        });
        return node;
    }

    /**
     * Find a node by its node address in the provided list of nodes.
     * 
     * @param nodes
     *            the list of nodes
     * @param address
     *            the address of the node to find
     * @return the found node or <code>null</code> if no node was found with the provided node address
     */
    public static Node findNodeByAddress(Collection<Node> nodes, final byte[] address) {
        Node node = IterableUtils.find(nodes, new Predicate<Node>() {

            @Override
            public boolean evaluate(final Node node) {
                if (Arrays.equals(node.getNode().getAddr(), address)) {
                    LOGGER.debug("Found node: {}", node);
                    return true;
                }
                return false;
            }
        });
        return node;
    }

    /**
     * Prepare the node label based on the user string with fallback to node.toString().
     * 
     * @param node
     *            the node
     * @return the node label
     */
    public static String prepareLabel(Node node) {
        String nodeLabel = null;

        if (node != null) {
            if (node.getNode().hasStoredStrings()) {
                // the node has stored strings, prepare the label with the stored strings
                String userString = node.getNode().getStoredString(StringData.INDEX_USERNAME);
                if (StringUtils.isNotBlank(userString)) {
                    // user string is available
                    String label = node.toString();
                    if (label.startsWith(userString)) {
                        nodeLabel = label;
                    }
                    else {
                        nodeLabel = userString;
                    }
                }
            }
            if (StringUtils.isBlank(nodeLabel)) {
                nodeLabel = node.toString();
            }
        }
        return nodeLabel;
    }

    public static Node configureFromBiDiB(final Node node, final BiDiB bidib) {
        if (bidib == null) {
            throw new IllegalArgumentException("A bidib must be provided.");
        }

        Nodes nodes = bidib.getNodes();

        if (nodes != null && CollectionUtils.isNotEmpty(nodes.getNode())) {
            List<org.bidib.jbidibc.core.schema.bidib2.Node> schemaNodes = nodes.getNode();
            for (org.bidib.jbidibc.core.schema.bidib2.Node schemaNode : schemaNodes) {

                // set the node name
                if (StringUtils.isNotBlank(schemaNode.getUserName())) {
                    node.setLabel(schemaNode.getUserName());
                    node.getNode().setStoredString(StringData.INDEX_USERNAME, schemaNode.getUserName());
                }

            }
        }
        return node;
    }

    public static BiDiB convertToBiDiB(final Node node, final MainModel mainModel) {

        if (node == null) {
            throw new IllegalArgumentException("A node must be provided.");
        }
        if (mainModel == null) {
            throw new IllegalArgumentException("The mainModel must be provided.");
        }

        ApplicationContext context = DefaultApplicationContext.getInstance();

        org.bidib.jbidibc.core.Node coreNode = node.getNode();

        long uniqueId = coreNode.getUniqueId();

        BiDiB bidib = new BiDiB();
        bidib.withSchemaVersion("1.0");

        org.bidib.jbidibc.core.schema.bidib2.Node schemaNode = new org.bidib.jbidibc.core.schema.bidib2.Node();
        schemaNode
            .withUserName(coreNode.getStoredString(StringData.INDEX_USERNAME))
            .withProductName(coreNode.getStoredString(StringData.INDEX_PRODUCTNAME))
            .withManufacturerId(org.bidib.jbidibc.core.utils.NodeUtils.getVendorId(uniqueId))
            .withProductId((int) org.bidib.jbidibc.core.utils.NodeUtils.getPid(uniqueId, coreNode.getRelevantPidBits()))
            .withUniqueId(uniqueId).withFirmwareRelease(coreNode.getSoftwareVersion().toString())
            .withProtocol(coreNode.getProtocolVersion().toString());

        List<org.bidib.jbidibc.core.schema.bidib2.Node> nodes = bidib.withNodes(new Nodes()).getNodes().getNode();
        nodes.add(schemaNode);

        // features
        List<Feature> features = coreNode.getFeatures();
        if (CollectionUtils.isNotEmpty(features)) {
            schemaNode.withFeatures(new Features());
            List<org.bidib.jbidibc.core.schema.bidib2.Feature> schemaFeatures = schemaNode.getFeatures().getFeature();
            for (Feature feature : features) {

                org.bidib.jbidibc.core.schema.bidib2.Feature schemaFeature =
                    new org.bidib.jbidibc.core.schema.bidib2.Feature();
                schemaFeature.withFeatureCodeId(feature.getType()).withValue(feature.getValue());

                schemaFeatures.add(schemaFeature);
            }

        }

        // feedback ports
        schemaNode.withFeedbackPorts(new FeedbackPorts());
        List<org.bidib.jbidibc.core.schema.bidib2.FeedbackPort> schemaFeedbackPports =
            schemaNode.getFeedbackPorts().getFeedbackPort();

        List<FeedbackPort> feedbackPorts = mainModel.getFeedbackPorts();
        if (CollectionUtils.isNotEmpty(feedbackPorts)) {
            Labels feedbackPortLabels = context.get(DefaultApplicationContext.KEY_FEEDBACKPORT_LABELS, Labels.class);

            LabelNodeType labelNode = PortLabelUtils.getLabelNode(feedbackPortLabels, coreNode.getUniqueId());

            // prepare the feedback ports
            for (FeedbackPort feedbackPort : feedbackPorts) {

                org.bidib.jbidibc.core.schema.bidib2.FeedbackPort port =
                    new org.bidib.jbidibc.core.schema.bidib2.FeedbackPort();
                port.setNumber(feedbackPort.getId());

                LabelType label = PortLabelUtils.getLabel(labelNode, feedbackPort.getId());
                if (label != null) {
                    port.setName(label.getLabelString());
                }
                else {
                    // set the default name
                    port.setName(feedbackPort.toString());
                }
                schemaFeedbackPports.add(port);
            }
        }

        // ports
        schemaNode.withPorts(new Ports());
        List<org.bidib.jbidibc.core.schema.bidib2.Port> ports = schemaNode.getPorts().getPort();

        // analog ports
        List<AnalogPort> analogPorts = mainModel.getAnalogPorts();
        if (CollectionUtils.isNotEmpty(analogPorts)) {
            // prepare the analog ports
        }

        // input ports
        List<InputPort> inputPorts = mainModel.getInputPorts();
        if (CollectionUtils.isNotEmpty(inputPorts)) {
            Labels inputPortLabels = context.get(DefaultApplicationContext.KEY_INPUTPORT_LABELS, Labels.class);

            LabelNodeType labelNode = PortLabelUtils.getLabelNode(inputPortLabels, coreNode.getUniqueId());

            // prepare the input ports
            for (InputPort inputPort : inputPorts) {

                InputKey inputKey = new InputKey();
                inputKey.setNumber(inputPort.getId());
                inputKey.setDisabled(!inputPort.isEnabled());

                try {
                    if (inputPort.getInputBehaviour() != IoBehaviourInputEnum.UNKNOWN) {
                        inputKey.setIoInputBehaviour(IOInputBehaviour.valueOf(inputPort.getInputBehaviour().name()));
                    }
                }
                catch (Exception ex) {
                    LOGGER.warn("Set the ioBehaviour for inputKey failed.", ex);
                }

                LabelType label = PortLabelUtils.getLabel(labelNode, inputPort.getId());
                if (label != null) {
                    inputKey.setName(label.getLabelString());
                }
                else {
                    // set the default name
                    inputKey.setName(inputPort.toString());
                }
                ports.add(inputKey);
            }
        }

        // backlight ports
        List<BacklightPort> backlightPorts = mainModel.getBacklightPorts();
        if (CollectionUtils.isNotEmpty(backlightPorts)) {
            Labels backlightLabels = context.get(DefaultApplicationContext.KEY_BACKLIGHTPORT_LABELS, Labels.class);

            LabelNodeType labelNode = PortLabelUtils.getLabelNode(backlightLabels, uniqueId);

            // prepare the backlight ports
            for (BacklightPort backlightPort : backlightPorts) {

                OutputBacklight backlight = new OutputBacklight();
                backlight.setNumber(backlightPort.getId());
                backlight.setDisabled(!backlightPort.isEnabled());
                backlight.setDimmingDownSpeed(backlightPort.getDimSlopeDown());
                backlight.setDimmingUpSpeed(backlightPort.getDimSlopeUp());
                backlight.setDmxChannel(backlightPort.getDmxMapping());

                LabelType label = PortLabelUtils.getLabel(labelNode, backlightPort.getId());
                if (label != null) {
                    backlight.setName(label.getLabelString());
                }
                else {
                    // set the default name
                    backlight.setName(backlightPort.toString());
                }
                ports.add(backlight);
            }
        }

        // light ports
        List<LightPort> lightPorts = mainModel.getLightPorts();
        if (CollectionUtils.isNotEmpty(lightPorts)) {
            Labels lightLabels = context.get(DefaultApplicationContext.KEY_LIGHTPORT_LABELS, Labels.class);

            LabelNodeType labelNode = PortLabelUtils.getLabelNode(lightLabels, uniqueId);

            // prepare the light ports
            for (LightPort lightPort : lightPorts) {

                OutputLight light = new OutputLight();
                light.setNumber(lightPort.getId());
                light.setDisabled(!lightPort.isEnabled());
                light.setBrightnessOff(lightPort.getPwmMin());
                light.setBrightnessOn(lightPort.getPwmMax());
                light.setDimmingDownSpeed(lightPort.getDimMin());
                light.setDimmingUpSpeed(lightPort.getDimMax());
                light.setRgbValue(lightPort.getRgbValue());

                LabelType label = PortLabelUtils.getLabel(labelNode, lightPort.getId());
                if (label != null) {
                    light.setName(label.getLabelString());
                }
                else {
                    // set the default name
                    light.setName(lightPort.toString());
                }
                ports.add(light);
            }
        }

        // servo ports
        List<ServoPort> servoPorts = mainModel.getServoPorts();
        if (CollectionUtils.isNotEmpty(servoPorts)) {
            Labels servoLabels = context.get(DefaultApplicationContext.KEY_SERVOPORT_LABELS, Labels.class);

            LabelNodeType labelNode = PortLabelUtils.getLabelNode(servoLabels, uniqueId);

            // prepare the servo ports
            for (ServoPort servoPort : servoPorts) {

                OutputServo servo = new OutputServo();
                servo.setDisabled(!servoPort.isEnabled());
                servo.setLowerLimit(servoPort.getTrimDown());
                servo.setUpperLimit(servoPort.getTrimUp());
                servo.setMovingTime(servoPort.getSpeed());
                servo.setNumber(servoPort.getId());
                LabelType label = PortLabelUtils.getLabel(labelNode, servoPort.getId());
                if (label != null) {
                    servo.setName(label.getLabelString());
                }
                else {
                    // set the default name
                    servo.setName(servoPort.toString());
                }
                ports.add(servo);
            }
        }

        // switch ports
        List<SwitchPort> switchPorts = mainModel.getSwitchPorts();
        if (CollectionUtils.isNotEmpty(switchPorts)) {
            Labels switchPortLabels = context.get(DefaultApplicationContext.KEY_SWITCHPORT_LABELS, Labels.class);

            LabelNodeType labelNode = PortLabelUtils.getLabelNode(switchPortLabels, coreNode.getUniqueId());

            // prepare the switch ports
            for (SwitchPort switchPort : switchPorts) {

                OutputSwitch outputSwitch = new OutputSwitch();
                outputSwitch.setNumber(switchPort.getId());
                outputSwitch.setDisabled(!switchPort.isEnabled());
                try {
                    if (switchPort.getOutputBehaviour() != IoBehaviourSwitchEnum.UNKNOWN) {
                        outputSwitch
                            .setIoSwitchBehaviour(IOSwitchBehaviour.valueOf(switchPort.getOutputBehaviour().name()));
                    }
                }
                catch (Exception ex) {
                    LOGGER.warn("Set the ioBehaviour for outputSwitch failed.", ex);
                }
                outputSwitch.setSwitchOffTime(switchPort.getSwitchOffTime());
                LabelType label = PortLabelUtils.getLabel(labelNode, switchPort.getId());
                if (label != null) {
                    outputSwitch.setName(label.getLabelString());
                }
                else {
                    // set the default name
                    outputSwitch.setName(switchPort.toString());
                }
                ports.add(outputSwitch);
            }
        }

        // macros
        List<Macro> macros = mainModel.getMacros();
        if (CollectionUtils.isNotEmpty(macros)) {

            Labels macroLabels = context.get(DefaultApplicationContext.KEY_MACRO_LABELS, Labels.class);
            LabelNodeType labelNode = PortLabelUtils.getLabelNode(macroLabels, coreNode.getUniqueId());

            FunctionConversionFactory functionConversionFactory = new FunctionConversionFactory();

            Macros schemaMacros = new Macros();

            for (Macro macro : macros) {

                org.bidib.jbidibc.core.schema.bidib2.Macro schemaMacro =
                    new org.bidib.jbidibc.core.schema.bidib2.Macro();
                schemaMacro.setNumber(macro.getId());
                // schemaMacro.setName(macro.getLabel());
                LabelType label = PortLabelUtils.getLabel(labelNode, macro.getId());
                if (label != null) {
                    schemaMacro.setName(label.getLabelString());
                }

                // prepare macro parameters
                MacroParameters macroParameters = createMacroParameters(macro);
                schemaMacro.setMacroParameters(macroParameters);

                // prepare macro points
                List<Function<? extends BidibStatus>> functions = macro.getFunctions();
                if (CollectionUtils.isNotEmpty(functions)) {

                    MacroPoints macroPoints = new MacroPoints();
                    schemaMacro.withMacroPoints(macroPoints);

                    for (Function<? extends BidibStatus> function : functions) {

                        LOGGER.info("Convert function: {}", function);
                        // prepare macro point
                        MacroPoint macroPoint = functionConversionFactory.convert(function);

                        macroPoints.withMacroPoint(macroPoint);
                    }
                }

                schemaMacros.withMacro(schemaMacro);
            }

            schemaNode.setMacros(schemaMacros);
        }

        // accessories
        List<Accessory> accessories = mainModel.getAccessories();
        if (CollectionUtils.isNotEmpty(accessories)) {
            Labels accessoryLabels = context.get(DefaultApplicationContext.KEY_ACCESSORY_LABELS, Labels.class);
            LabelNodeType labelNode = PortLabelUtils.getLabelNode(accessoryLabels, coreNode.getUniqueId());

            Accessories schemaAccessories = new Accessories();

            for (Accessory accessory : accessories) {
                org.bidib.jbidibc.core.schema.bidib2.Accessory schemaAccessory =
                    new org.bidib.jbidibc.core.schema.bidib2.Accessory();
                schemaAccessory.setNumber(accessory.getId());
                // schemaAccessory.setName(accessory.getLabel());
                LabelType label = PortLabelUtils.getLabel(labelNode, accessory.getId());
                if (label != null) {
                    schemaAccessory.setName(label.getLabelString());
                }

                // prepare the aspects
                Collection<MacroRef> aspects = accessory.getAspects();
                if (CollectionUtils.isNotEmpty(aspects)) {

                    Aspects schemaAspects = new Aspects();
                    schemaAccessory.withAspects(schemaAspects);

                    int aspectNumber = 0;
                    for (MacroRef macroRef : aspects) {
                        // prepare aspect
                        Aspect aspect = new Aspect();
                        aspect.setMacroNumber(macroRef.getId());
                        aspect.setNumber(aspectNumber++);

                        // TODO prepare the aspect label
                        // aspect.setName(macroRef.getLabel());

                        schemaAspects.withAspect(aspect);
                    }
                }

                // set the startup accessory
                if (accessory.getStartupState() != null) {
                    schemaAccessory.setStartupState(accessory.getStartupState());
                }
                schemaAccessories.withAccessory(schemaAccessory);
            }

            schemaNode.setAccessories(schemaAccessories);
        }

        return bidib;
    }

    private static MacroParameters createMacroParameters(Macro macro) {
        MacroParameters macroParameters = new MacroParameters();

        MacroParameterSlowdown macroParameterSlowdown = new MacroParameterSlowdown();
        macroParameterSlowdown.setSpeed(macro.getSpeed());
        macroParameters.withMacroParameter(macroParameterSlowdown);

        MacroParameterRepeat macroParameterRepeat = new MacroParameterRepeat();
        macroParameterRepeat.setRepetitions(macro.getCycles());
        macroParameters.withMacroParameter(macroParameterRepeat);

        MacroParameterClockStart macroParameterClockStart = new MacroParameterClockStart();

        if (CollectionUtils.isNotEmpty(macro.getStartConditions())) {
            List<StartCondition> startConditions = new LinkedList<>(macro.getStartConditions());
            if (startConditions.get(0) instanceof TimeStartCondition) {
                TimeStartCondition timeStartCondition = (TimeStartCondition) startConditions.get(0);

                macroParameterClockStart.setHour(timeStartCondition.getTime().get(Calendar.HOUR_OF_DAY));
                macroParameterClockStart.setMinute(timeStartCondition.getTime().get(Calendar.MINUTE));
                macroParameterClockStart.setIsEnabled(true);

                // set the repeat day
                if (timeStartCondition.getRepeatDay() != null) {
                    switch (timeStartCondition.getRepeatDay()) {
                        case ALL:
                            macroParameterClockStart.setWeekday(WeekdayExtension.EVERY_DAY.value());
                            break;
                        case MONDAY:
                            macroParameterClockStart.setWeekday(Weekday.MONDAY.value());
                            break;
                        case TUESDAY:
                            macroParameterClockStart.setWeekday(Weekday.TUESDAY.value());
                            break;
                        case WEDNESDAY:
                            macroParameterClockStart.setWeekday(Weekday.WEDNESDAY.value());
                            break;
                        case THURSDAY:
                            macroParameterClockStart.setWeekday(Weekday.THURSDAY.value());
                            break;
                        case FRIDAY:
                            macroParameterClockStart.setWeekday(Weekday.FRIDAY.value());
                            break;
                        case SATURDAY:
                            macroParameterClockStart.setWeekday(Weekday.SATURDAY.value());
                            break;
                        case SUNDAY:
                            macroParameterClockStart.setWeekday(Weekday.SUNDAY.value());
                            break;
                        default:
                            LOGGER.warn("Unknown repeat day: {}", timeStartCondition.getRepeatDay());
                            break;
                    }
                }
                else {
                    LOGGER.info("No repeat day specified for macro: {}", macro.getLabel());
                }

                // set the repeat time
                if (timeStartCondition.getRepeatTime() != null) {
                    switch (timeStartCondition.getRepeatTime()) {
                        case HOURLY:
                            macroParameterClockStart.setPeriodicalRepetition(HourExtension.EVERY_HOUR.value());
                            // macroParameterClockStart.setMinute(null);
                            break;
                        case WORKING_HOURLY:
                            macroParameterClockStart
                                .setPeriodicalRepetition(HourExtension.EVERY_HOUR_IN_DAYTIME.value());
                            // macroParameterClockStart.setMinute(null);
                            break;
                        case HALF_HOURLY:
                            macroParameterClockStart.setPeriodicalRepetition(MinuteExtension.EVERY_30_MINUTES.value());
                            // macroParameterClockStart.setHour(null);
                            break;
                        case QUARTER_HOURLY:
                            macroParameterClockStart.setPeriodicalRepetition(MinuteExtension.EVERY_15_MINUTES.value());
                            // macroParameterClockStart.setHour(null);
                            break;
                        case MINUTELY:
                            macroParameterClockStart.setPeriodicalRepetition(MinuteExtension.EVERY_MINUTE.value());
                            // macroParameterClockStart.setHour(null);
                            break;
                        case NONE:
                            break;
                    }
                }
                else {
                    LOGGER.info("No repeat time specified for macro: {}", macro.getLabel());
                }
            }
        }
        else {
            macroParameterClockStart.setIsEnabled(false);
        }
        macroParameters.withMacroParameter(macroParameterClockStart);

        return macroParameters;
    }

    public static <P extends Port<?>> P getPort(
        List<P> ports, int portNumber, boolean sourceIsFlatPortModel, boolean targetIsFlatPortModel) {
        LOGGER.info("Get port with portNumber: {}, sourceIsFlatPortModel: {}, targetIsFlatPortModel: {}", portNumber,
            sourceIsFlatPortModel, targetIsFlatPortModel);

        if (sourceIsFlatPortModel != targetIsFlatPortModel) {
            // the models are different
            if (!sourceIsFlatPortModel) {
                // the source is type-oriented and the target is flat
                try {
                    P port = ports.get(portNumber);
                    if (!port.isEnabled()) {
                        LOGGER.warn("Found disabled port at index: {}, port: {}", portNumber, port);
                        return null;
                    }
                    else {
                        LOGGER.info("Searched port at index: {}, return found port: {}", portNumber, port);
                        return port;
                    }
                }
                catch (IndexOutOfBoundsException ex) {
                    LOGGER.warn("Port not available, portNumber: {}, message: {}", portNumber, ex.getMessage());
                }
            }
            else {
                LOGGER.warn("Cannot calculate port from flat to type-oriented model.");
            }
        }
        else {
            LOGGER.info("Get the port from the portNumber: {}", portNumber);
            for (P port : ports) {
                if (port.getId() == portNumber) {
                    LOGGER.info("Return found port: {}", port);
                    return port;
                }
            }
        }
        return null;
    }

}
