package org.bidib.wizard.utils;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOCase;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SearchPathUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(SearchPathUtils.class);

    public static List<File> findFiles(String fileName, String... searchPaths) {

        LOGGER.info("Find files in searchPaths: {}", new Object[] { searchPaths });

        final List<File> files = new LinkedList<>();

        for (String searchPath : searchPaths) {

            LOGGER.info("Prepared filename to load vendorCv: {}", fileName.toString());
            if (searchPath.startsWith("classpath:")) {
                int beginIndex = "classpath:".length();
                String lookup = "/" + searchPath.substring(beginIndex);
                lookup = lookup.replaceAll("\\\\", "/");
                LOGGER.info("Lookup file internally: {}", lookup);

                URL pathString = SearchPathUtils.class.getResource(lookup);
                LOGGER.info("Prepared pathString: {}", pathString);

                if (pathString == null) {
                    LOGGER.info("No resource for lookup '{}' found.", lookup);
                    continue;
                }

                FileSystem fs = null;
                try {

                    URI lookupURI = pathString.toURI();
                    LOGGER.info("Prepared lookupURI: {}", lookupURI);

                    final String[] array = lookupURI.toString().split("!");

                    Path path = null;
                    if (array.length > 1) {
                        final Map<String, String> env = new HashMap<>();
                        LOGGER.info("Create new filesystem for: {}", array[0]);
                        fs = FileSystems.newFileSystem(URI.create(array[0]), env);
                        path = fs.getPath(array[1]);
                        LOGGER.info("Prepared path: {}", path);
                    }
                    else {
                        path = Paths.get(lookupURI);
                    }

                    final FileSystem fsInner = fs;

                    Files.walkFileTree(path.getParent(), new SimpleFileVisitor<Path>() {
                        @Override
                        public FileVisitResult visitFile(Path path, BasicFileAttributes attrs) throws IOException {
                            LOGGER.info("Current file: {}", path);

                            if (FilenameUtils.wildcardMatch(path.toString(), fileName, IOCase.INSENSITIVE)) {
                                LOGGER.info("Found matching path: {}, absolutePath: {}", path, path.toAbsolutePath());

                                File file = null;
                                if (fsInner != null) {
                                    String filePath = array[0] + "!" + path.toAbsolutePath();
                                    file = new File(filePath);
                                }
                                else {

                                    file = path.toFile();
                                }

                                LOGGER.info("Add matching file: {}", file);

                                files.add(file);
                            }

                            return FileVisitResult.CONTINUE;
                        }

                        @Override
                        public FileVisitResult postVisitDirectory(Path dir, IOException e) throws IOException {
                            if (e == null) {
                                LOGGER.info("Current directory: {}", dir);
                                return FileVisitResult.CONTINUE;
                            }
                            else {
                                // directory iteration failed
                                throw e;
                            }
                        }
                    });
                }
                catch (Exception e) {
                    LOGGER.warn("Convert uri to path failed.", e);
                }
                finally {
                    if (fs != null) {
                        try {
                            fs.close();
                        }
                        catch (Exception ex) {
                            LOGGER.warn("Close filesystem failed.", ex);
                        }
                    }
                }

                LOGGER.info("Found matching files: {}", files);
            }
            else {
                LOGGER.info("Search for files in searchPath: {}", searchPath);
                File vendorCvFile = new File(searchPath, fileName);
                File searchDirectory = vendorCvFile.getParentFile();
                if (searchDirectory.exists()) {
                    // use filename with wildcards
                    IOFileFilter fileFilter = new WildcardFileFilter(fileName, IOCase.INSENSITIVE);
                    files.addAll(FileUtils.listFiles(searchDirectory, fileFilter, TrueFileFilter.INSTANCE));

                    LOGGER.info("Found matching files: {}", files);
                }
                else {
                    LOGGER.info("The directory to search does not exist: {}", searchDirectory.toString());
                }
            }
        }
        return files;
    }

}
