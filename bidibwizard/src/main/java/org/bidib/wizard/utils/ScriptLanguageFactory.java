package org.bidib.wizard.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.bidib.script.BiDiBScript;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

public class ScriptLanguageFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(ScriptLanguageFactory.class);

    private static final String JAXB_PACKAGE = "org.bidib.script";

    public static final String XSD_LOCATION = "/xsd/scriptlanguage.xsd";

    public static BiDiBScript loadBiDiBScript(String filePath) throws FileNotFoundException {

        InputStream is = null;
        try {
            if (filePath.startsWith("/")) {
                is = ScriptLanguageFactory.class.getResourceAsStream(filePath);
            }
            else {
                File file = new File(filePath);
                is = new FileInputStream(file);
            }
        }
        catch (FileNotFoundException ex) {
            LOGGER.warn("Load saved CV from file failed.", ex);

            throw ex;
        }

        BiDiBScript biDiBScript = new ScriptLanguageFactory().loadBiDiBScriptFile(is);
        LOGGER.trace("Loaded biDiBScript: {}", biDiBScript);

        return biDiBScript;
    }

    private BiDiBScript loadBiDiBScriptFile(InputStream is) {

        BiDiBScript biDiBScript = null;
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(JAXB_PACKAGE);

            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            StreamSource streamSource = new StreamSource(ScriptLanguageFactory.class.getResourceAsStream(XSD_LOCATION));
            Schema schema = schemaFactory.newSchema(streamSource);
            unmarshaller.setSchema(schema);

            biDiBScript = (BiDiBScript) unmarshaller.unmarshal(is);
        }
        catch (JAXBException | SAXException ex) {
            LOGGER.warn("Load BiDiBScript from file failed.", ex);

            throw new RuntimeException("Load BiDiBScript from file failed.");
        }
        return biDiBScript;
    }
}
