package org.bidib.wizard.utils;

import java.util.List;

import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.collections4.Predicate;
import org.bidib.wizard.mvc.main.model.GenericPort;
import org.bidib.wizard.mvc.main.model.Port;

public class PortListUtils {

    /**
     * Iterate over the ports and find the matching port by comparing the port number.
     * 
     * @param ports
     *            the ports list
     * @param portNum
     *            the port number
     * @return the matching port or {@code null} if no matching port was found
     */
    public static <T extends Port<?>> T findPortByPortNumber(List<T> ports, final int portNum) {
        T port = IterableUtils.find(ports, new Predicate<T>() {

            @Override
            public boolean evaluate(T port) {
                if (port.getId() == portNum) {
                    return true;
                }
                return false;
            }
        });
        return port;
    }

    /**
     * Iterate over the generic ports and find the matching port by comparing the port number.
     * 
     * @param ports
     *            the ports list
     * @param portNum
     *            the port number
     * @return the matching port or {@code null} if no matching port was found
     */
    public static GenericPort findGenericPortByPortNumber(List<GenericPort> ports, final int portNum) {
        GenericPort port = IterableUtils.find(ports, new Predicate<GenericPort>() {

            @Override
            public boolean evaluate(GenericPort port) {
                if (port.getPortNumber() != null && port.getPortNumber().intValue() == portNum) {
                    return true;
                }
                return false;
            }
        });
        return port;
    }
}
