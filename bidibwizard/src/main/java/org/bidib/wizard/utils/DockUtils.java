package org.bidib.wizard.utils;

import java.awt.Component;
import java.awt.Container;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vlsolutions.swing.docking.Dockable;
import com.vlsolutions.swing.docking.DockingUtilities;
import com.vlsolutions.swing.docking.SingleDockableContainer;
import com.vlsolutions.swing.docking.TabbedDockableContainer;

public class DockUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(DockUtils.class);

    // public static void selectWindow(Dockable dockable) {
    //
    // if (dockable == null) {
    // LOGGER.warn("Select window is skipped because no dockable provided.");
    // return;
    // }
    //
    // // TODO this should also work with non-tabbed windows
    // TabbedDockableContainer container = DockingUtilities.findTabbedDockableContainer(dockable);
    // if (container != null) {
    // container.setSelectedDockable(dockable);
    // }
    // else {
    // LOGGER.info("Container not available, select component directly.");
    // dockable.getComponent().requestFocusInWindow();
    // }
    // }

    public static void selectWindow(Dockable dockable) {

        // TODO this should also work with non-tabbed windows
        TabbedDockableContainer container = DockingUtilities.findTabbedDockableContainer(dockable);
        if (container != null) {
            container.setSelectedDockable(dockable);
        }
        else {
            LOGGER.warn("Container not available, select component directly.");

            SingleDockableContainer singleContainer = DockingUtilities.findSingleDockableContainer(dockable);
            if (singleContainer != null) {
                try {
                    Component comp = (Component) singleContainer;

                    Container container2 = comp.getParent();
                    if (container2 != null) {
                        container2 = container2.getParent();
                    }

                    if (container2 != null) {
                        LOGGER.info("Request focus: {}", container2);
                        container2.requestFocus();
                    }
                }
                catch (Exception ex) {
                    LOGGER.warn("Request focus in single container failed.", ex);
                }
            }
            else {
                dockable.getComponent().requestFocusInWindow();
            }
        }
    }

}
