package org.bidib.wizard.utils;

import java.util.List;

import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.collections4.Predicate;
import org.bidib.wizard.mvc.main.model.IdAware;

public class IdAwareListUtils {

    public static <T extends IdAware> T findItemById(List<T> items, final int portNum) {
        T port = IterableUtils.find(items, new Predicate<T>() {

            @Override
            public boolean evaluate(T item) {
                if (item.getId() == portNum) {
                    return true;
                }
                return false;
            }
        });
        return port;
    }
}
