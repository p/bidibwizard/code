package org.bidib.wizard.utils;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class XmlLocaleUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(XmlLocaleUtils.class);

    public static final String DEFAULT_LOCALE = "en-US";

    public static final String DEFAULT_LOCALE_VENDOR_CV = "en-EN";

    public static String getXmlLocale() {
        Locale locale = Locale.getDefault();
        LOGGER.trace("Current locale: {}", locale);

        String xmlLocale = null;
        xmlLocale = locale.getLanguage();
        if (xmlLocale.equals("de")) {
            xmlLocale = "de-DE";
        }
        else {
            xmlLocale = DEFAULT_LOCALE;
        }
        return xmlLocale;
    }

    public static String getXmlLocaleVendorCV() {
        Locale locale = Locale.getDefault();
        LOGGER.trace("Current locale: {}", locale);

        String xmlLocale = null;
        xmlLocale = locale.getLanguage();
        if (xmlLocale.equals("de")) {
            xmlLocale = "de-DE";
        }
        else {
            xmlLocale = DEFAULT_LOCALE_VENDOR_CV;
        }
        return xmlLocale;
    }

}
