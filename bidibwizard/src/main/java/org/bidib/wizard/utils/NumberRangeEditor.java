package org.bidib.wizard.utils;

import java.awt.Component;

import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.TableCellEditor;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NumberRangeEditor extends AbstractCellEditor implements TableCellEditor {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(NumberRangeEditor.class);

    private final JTextField textField = new JTextField();

    public NumberRangeEditor(int minimum, int maximum) {
        textField.setHorizontalAlignment(SwingConstants.RIGHT);

        InputValidationDocument numericDocument = new InputValidationDocument(8, InputValidationDocument.NUMERIC);
        numericDocument.setDocumentFilter(new IntegerRangeFilter(minimum, maximum));
        textField.setDocument(numericDocument);
        textField.setColumns(4);
    }

    public NumberRangeEditor() {
        this(0, 255);
    }

    @Override
    public Object getCellEditorValue() {
        try {
            if (StringUtils.isNotBlank(textField.getText())) {
                return Integer.parseInt(textField.getText());
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Parse textfield value failed.", ex);
        }
        return Integer.valueOf(0);
    }

    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        textField.setText(value != null ? value.toString() : "");
        return textField;
    }

    @Override
    public boolean stopCellEditing() {
        try {
            if (StringUtils.isNotBlank(textField.getText())) {
                int value = Integer.valueOf(textField.getText());

                LOGGER.debug("Current value: {}", value);
            }
            else {
                LOGGER.info("Allow empty string in text field.");
            }
        }
        catch (Exception e) {
            LOGGER.warn("Parse text value failed: {}", e.getMessage());
            return false;
        }
        return super.stopCellEditing();
    }
}
