package org.bidib.wizard.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.UnmarshalException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.apache.commons.lang3.StringUtils;
import org.bidib.jbidibc.core.schema.exception.InvalidSchemaException;
import org.bidib.schema.nodescriptsources.NodeScriptSources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class NodeScriptSourcesFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(NodeScriptSourcesFactory.class);

    private static JAXBContext jaxbContext;

    private static final String JAXB_PACKAGE = "org.bidib.schema.nodescriptsources";

    public static final String XSD_LOCATION = "/xsd/nodeScriptSources.xsd";

    /**
     * Load the nodeScript sources from the provided path.
     * 
     * @param filePath
     *            the file path
     * @return the nodeScriptSources instance
     * @throws FileNotFoundException
     */
    public static NodeScriptSources loadNodeScriptSources(String filePath) throws FileNotFoundException {

        LOGGER.info("Try to load nodeScriptSources from filePath: {}", filePath);

        if (StringUtils.isBlank(filePath)) {
            throw new IllegalArgumentException("No file path provided.");
        }

        NodeScriptSources nodeScriptSources = null;
        InputStream is = null;
        try {
            if (filePath.startsWith("classpath:")) {
                String internalPath = "/" + filePath.substring("classpath:".length());
                LOGGER.info("Look for internal file at path: {}", internalPath);
                is = NodeScriptSourcesFactory.class.getResourceAsStream(internalPath);
            }
            else {
                File file = new File(filePath);
                if (file.exists()) {
                    is = new FileInputStream(file);
                }
                else {
                    LOGGER.info("The requested file does not exist: {}", filePath);
                }
            }

            if (is == null) {
                LOGGER.info("No file found at location: {}", filePath);
            }
            else {
                nodeScriptSources = new NodeScriptSourcesFactory().loadNodeScriptSourcesFile(is);
                LOGGER.info("Loaded nodeScriptSources: {}", nodeScriptSources);
            }

        }
        catch (FileNotFoundException ex) {
            LOGGER.warn("Load saved NodeScriptSources from file failed.", ex);
        }
        catch (Exception ex) {
            LOGGER.warn("Load saved NodeScriptSources from file failed.", ex);
        }
        finally {
            if (is != null) {
                try {
                    is.close();
                }
                catch (IOException ex) {
                    LOGGER.warn("Close input stream failed.", ex);
                }
            }
        }
        return nodeScriptSources;
    }

    private NodeScriptSources loadNodeScriptSourcesFile(InputStream is) {

        NodeScriptSources nodeScriptSources = null;
        try {
            if (jaxbContext == null) {
                LOGGER.info("Create the jaxb context for NodeScriptSources.");
                jaxbContext = JAXBContext.newInstance(JAXB_PACKAGE);
            }

            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

            StreamSource streamSource =
                new StreamSource(NodeScriptSourcesFactory.class.getResourceAsStream(XSD_LOCATION));
            Schema schema = schemaFactory.newSchema(streamSource);
            unmarshaller.setSchema(schema);

            nodeScriptSources = (NodeScriptSources) unmarshaller.unmarshal(is);
        }
        catch (UnmarshalException ex) {
            LOGGER.warn("Load NodeScriptSources from file failed with UnmarshalException.", ex);
            if (ex.getCause() instanceof SAXParseException) {
                throw new InvalidSchemaException("Load NodeScriptSources from file failed");
            }

            throw new IllegalArgumentException("Load NodeScriptSources from file failed with UnmarshalException.");
        }
        catch (JAXBException | SAXException ex) {
            LOGGER.warn("Load NodeScriptSources from file failed.", ex);

            throw new RuntimeException("Load NodeScriptSources from file failed.");
        }
        catch (Exception ex) {
            LOGGER.warn("Load NodeScriptSources from file failed.", ex);
            throw new RuntimeException("Load NodeScriptSources from file failed.");
        }
        return nodeScriptSources;
    }
}
