package org.bidib.wizard.utils;

import java.awt.Component;
import java.awt.Frame;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.Feature;
import org.bidib.jbidibc.core.StringData;
import org.bidib.jbidibc.core.exception.InvalidConfigurationException;
import org.bidib.jbidibc.core.node.ConfigurationVariable;
import org.bidib.jbidibc.core.port.BytePortConfigValue;
import org.bidib.jbidibc.core.port.PortConfigValue;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.exchange.vendorcv.ModeType;
import org.bidib.wizard.comm.Communication;
import org.bidib.wizard.comm.CommunicationFactory;
import org.bidib.wizard.common.context.ApplicationContext;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.labels.AccessoryLabelFactory;
import org.bidib.wizard.labels.AccessoryLabelUtils;
import org.bidib.wizard.labels.AnalogPortLabelFactory;
import org.bidib.wizard.labels.BacklightPortLabelFactory;
import org.bidib.wizard.labels.FeedbackPortLabelFactory;
import org.bidib.wizard.labels.InputPortLabelFactory;
import org.bidib.wizard.labels.Labels;
import org.bidib.wizard.labels.LightPortLabelFactory;
import org.bidib.wizard.labels.MacroLabelFactory;
import org.bidib.wizard.labels.MotorPortLabelFactory;
import org.bidib.wizard.labels.NodeLabelFactory;
import org.bidib.wizard.labels.PortLabelUtils;
import org.bidib.wizard.labels.ServoPortLabelFactory;
import org.bidib.wizard.labels.SoundPortLabelFactory;
import org.bidib.wizard.labels.SwitchPortLabelFactory;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.bidib.wizard.mvc.common.view.cvdefinition.CvDefinitionTreeTableModel;
import org.bidib.wizard.mvc.main.controller.MainController;
import org.bidib.wizard.mvc.main.model.Accessory;
import org.bidib.wizard.mvc.main.model.AnalogPort;
import org.bidib.wizard.mvc.main.model.BacklightPort;
import org.bidib.wizard.mvc.main.model.FeedbackPort;
import org.bidib.wizard.mvc.main.model.InputPort;
import org.bidib.wizard.mvc.main.model.LightPort;
import org.bidib.wizard.mvc.main.model.Macro;
import org.bidib.wizard.mvc.main.model.MacroFactory;
import org.bidib.wizard.mvc.main.model.MacroSaveState;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.MotorPort;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.model.NodeState;
import org.bidib.wizard.mvc.main.model.Port;
import org.bidib.wizard.mvc.main.model.PortsProvider;
import org.bidib.wizard.mvc.main.model.ServoPort;
import org.bidib.wizard.mvc.main.model.SoundPort;
import org.bidib.wizard.mvc.main.model.SwitchPairPort;
import org.bidib.wizard.mvc.main.model.SwitchPort;
import org.bidib.wizard.mvc.main.view.component.NodeErrorsDialog;
import org.bidib.wizard.mvc.main.view.component.SaveNodeConfigurationDialog;
import org.bidib.wizard.mvc.main.view.cvdef.CvNode;
import org.bidib.wizard.mvc.main.view.panel.CvDefinitionTreeHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NodeStateUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(NodeStateUtils.class);

    /**
     * Prepare the nodeState from the provided node data.
     * 
     * @param model
     *            the main model instance
     * @param communication
     *            the communication instance
     * @param node
     *            the node
     * @param checkLoadMacroContent
     *            load macro content flag. If {@code true} the macro content is loaded from the node.
     * @param checkLoadFeatures
     *            load features flag. If {@code true} the features are loaded from the node.
     * @param checkLoadCVs
     *            load CV values flag. If {@code true} the CV values are loaded from the node.
     * @return the node state
     */
    public static NodeState prepareNodeState(
        final MainModel model, final Communication communication, final Node node, final Boolean checkLoadMacroContent,
        final Boolean checkLoadFeatures, final Boolean checkLoadCVs) {

        boolean nodeHasFlatPortModel = node.getNode().isPortFlatModelAvailable();
        LOGGER.info("Prepare the nodeState, nodeHasFlatPortModel: {}", nodeHasFlatPortModel);

        if (checkLoadMacroContent != null && checkLoadMacroContent.booleanValue()) {
            LOGGER.info("Load the macro content of the node before export: {}", node);
            List<Macro> macroList = new LinkedList<Macro>();
            for (Macro macro : model.getMacros()) {
                Macro macroWithContent = communication.getMacroContent(node, macro);
                LOGGER.info("Load macro content: {}", macroWithContent);

                // reset the changed flag on the macro
                macroWithContent.setMacroSaveState(MacroSaveState.PERMANENTLY_STORED_ON_NODE);
                macroWithContent.setFlatPortModel(nodeHasFlatPortModel);

                macroList.add(macroWithContent);
            }
            LOGGER.info("Set the new macros for the node: {}", macroList);
            model.setMacros(macroList);

            // TODO check if the accessories are loaded !!!
        }

        List<Feature> features = null;
        if (checkLoadFeatures.booleanValue()) {
            LOGGER.info("Load the features of the node before export: {}", node);
            boolean discardCache = true;
            features = new ArrayList<Feature>();
            features.addAll(communication.getFeatures(node.getNode(), discardCache));
            features = Collections.unmodifiableList(features);
        }

        // load CVs
        List<ConfigurationVariable> configurationVariables = null;
        if (checkLoadCVs.booleanValue()) {
            LOGGER.info("Load the CVs of the node before export: {}", node);

            // use the CV list from the model
            configurationVariables = model.getConfigurationVariables();

            configurationVariables = communication.getConfigurationVariables(node.getNode(), configurationVariables);

            LOGGER.info("Current configurationVariables: {}", configurationVariables);
        }
        else {
            LOGGER.warn("The CV values of the node are not exported!");
        }

        // get the name of the node
        List<StringData> nodeStrings = new LinkedList<>();
        if (node.getNode().hasStoredStrings()) {
            StringData productName = new StringData();
            productName.setNamespace(StringData.NAMESPACE_NODE);
            productName.setIndex(StringData.INDEX_PRODUCTNAME);
            productName.setValue(node.getNode().getStoredString(StringData.INDEX_PRODUCTNAME));
            LOGGER.info("Add productName: {}", productName);
            nodeStrings.add(productName);

            StringData userName = new StringData();
            userName.setNamespace(StringData.NAMESPACE_NODE);
            userName.setIndex(StringData.INDEX_USERNAME);
            userName.setValue(node.getNode().getStoredString(StringData.INDEX_USERNAME));
            LOGGER.info("Add userName: {}", userName);
            nodeStrings.add(userName);
        }

        NodeState nodeState =
            new NodeState(model.getAccessories(), model.getAnalogPorts(), model.getBacklightPorts(),
                model.getFeedbackPorts(), model.getLightPorts(), model.getMacros(), model.getServoPorts(),
                model.getInputPorts(), model.getSwitchPorts(), model.getSwitchPairPorts(), model.getSoundPorts(),
                model.getMotorPorts(), features, configurationVariables, nodeStrings);
        nodeState.setFlatPortModel(nodeHasFlatPortModel);

        return nodeState;
    }

    private static List<Port<?>> toSortedList(Collection<? extends Port<?>> source) {

        List<Port<?>> list = new ArrayList<>();
        list.addAll(source);

        Collections.sort(list, new Comparator<Port<?>>() {
            public int compare(Port<?> i1, Port<?> i2) {
                return (i2.getId() > i1.getId()) ? -1 : 1;
            }
        });

        LOGGER.info("Return sorted list: {}", list);
        return list;
    }

    public static void loadNodeState(
        final Component parent, String fileName, final MainModel model, final Node node,
        final Map<String, Object> importParams, final ApplicationContext applicationContext,
        final Boolean checkRestoreNodeString, final Boolean checkRestoreMacroContent,
        final Boolean checkRestoreFeatures, final Boolean checkRestoreCVs) {

        LOGGER.info("Load nodeState from file: {}", fileName);

        NodeState nodeState = NodeState.load(fileName);

        // check the port model!!!
        boolean sourceIsFlatModel = nodeState.isFlatPortModel();
        boolean targetIsFlatModel = node.getNode().isPortFlatModelAvailable();
        LOGGER.info("The imported data supports flat port model: {}, selected node supports flat port model: {}",
            sourceIsFlatModel, targetIsFlatModel);

        PortsProvider portsProvider = model;

        if (!sourceIsFlatModel) {
            LOGGER.warn(
                "Maybe the flag sourceIsFlatModel was not detected in imported node file! Try detect from nodeState data.");

            List<?>[] ports =
                new List<?>[] { toSortedList(nodeState.getAnalogPorts()), toSortedList(nodeState.getBacklightPorts()),
                    toSortedList(nodeState.getInputPorts()), toSortedList(nodeState.getLightPorts()),
                    toSortedList(nodeState.getMotorPorts()), toSortedList(nodeState.getServoPorts()),
                    toSortedList(nodeState.getSoundPorts()), toSortedList(nodeState.getSwitchPorts()) };

            // Check if any ports are available. If more than one port type is available and the first port
            // does not start with 0 we have flat port model

            for (List<?> portList : ports) {
                if (CollectionUtils.isNotEmpty(portList)) {
                    Port<?> port = (Port<?>) portList.get(0);
                    if (port.getId() > 0) {
                        sourceIsFlatModel = Boolean.TRUE;
                        LOGGER.info("Detected flat port model from port list of port: {}", port);
                        break;
                    }
                }
            }
        }

        LOGGER.info("sourceIsFlatModel: {}", sourceIsFlatModel);
        if (importParams != null) {
            importParams.put("NodeState_sourceIsFlatModel", sourceIsFlatModel);
        }

        long uniqueId = node.getNode().getUniqueId();

        if (CollectionUtils.isNotEmpty(nodeState.getFeedbackPorts())) {
            List<FeedbackPort> feedbackPorts = portsProvider.getFeedbackPorts();

            final Labels feedbackPortLabels =
                applicationContext.get(DefaultApplicationContext.KEY_FEEDBACKPORT_LABELS, Labels.class);

            for (FeedbackPort feedbackPort : nodeState.getFeedbackPorts()) {
                // feedback ports are special ports, not dependent on port model
                FeedbackPort port = PortListUtils.findPortByPortNumber(feedbackPorts, feedbackPort.getId());

                if (port != null) {
                    port.setLabel(feedbackPort.getLabel());

                    PortLabelUtils.replaceLabel(feedbackPortLabels, uniqueId, feedbackPort.getId(),
                        feedbackPort.getLabel(), FeedbackPortLabelFactory.DEFAULT_LABELTYPE);
                }
            }
        }

        // set the ports with special configuration
        if (CollectionUtils.isNotEmpty(nodeState.getAnalogPorts())) {

            List<AnalogPort> analogPorts = portsProvider.getAnalogPorts();

            final Labels analogPortLabels =
                applicationContext.get(DefaultApplicationContext.KEY_ANALOGPORT_LABELS, Labels.class);

            for (AnalogPort analogPort : nodeState.getAnalogPorts()) {
                AnalogPort port =
                    org.bidib.wizard.utils.NodeUtils.getPort(analogPorts, analogPort.getId(), sourceIsFlatModel,
                        targetIsFlatModel);
                if (port != null) {
                    port.setLabel(analogPort.getLabel());

                    // TODO must set the supported configx keys ...

                    port.setPortConfigX(analogPort.getPortConfigX());

                    PortLabelUtils.replaceLabel(analogPortLabels, uniqueId, analogPort.getId(), analogPort.getLabel(),
                        AnalogPortLabelFactory.DEFAULT_LABELTYPE);
                }
                else {
                    LOGGER.warn("No analog port found with port id: {}", analogPort.getId());
                }
            }
        }

        if (CollectionUtils.isNotEmpty(nodeState.getLightPorts())) {

            List<LightPort> lightPorts = portsProvider.getLightPorts();

            final Labels lightPortLabels =
                applicationContext.get(DefaultApplicationContext.KEY_LIGHTPORT_LABELS, Labels.class);

            for (LightPort lightPort : nodeState.getLightPorts()) {

                LightPort port =
                    org.bidib.wizard.utils.NodeUtils.getPort(lightPorts, lightPort.getId(), sourceIsFlatModel,
                        targetIsFlatModel);
                if (port != null) {
                    port.setLabel(lightPort.getLabel());
                    // migrate the missing configuration

                    // must set the supported configx keys ...
                    Map<Byte, PortConfigValue<?>> portConfigX = new LinkedHashMap<Byte, PortConfigValue<?>>();

                    if (port.getDimStretchMax() == LightPort.DIMM_STRETCHMAX8_8) {
                        LOGGER.info("Detected DIMM_STRETCHMAX8_8 for lightPort with id: {}", port.getId());
                        portConfigX.put(BidibLibrary.BIDIB_PCFG_DIMM_DOWN_8_8,
                            new BytePortConfigValue(ByteUtils.getLowByte(lightPort.getDimMin())));
                        portConfigX.put(BidibLibrary.BIDIB_PCFG_DIMM_UP_8_8,
                            new BytePortConfigValue(ByteUtils.getLowByte(lightPort.getDimMax())));
                    }
                    else {
                        LOGGER.info("Detected DIMM_STRETCHMAX8 for lightPort with id: {}", port.getId());
                        portConfigX.put(BidibLibrary.BIDIB_PCFG_DIMM_DOWN,
                            new BytePortConfigValue(ByteUtils.getLowByte(lightPort.getDimMin())));
                        portConfigX.put(BidibLibrary.BIDIB_PCFG_DIMM_UP,
                            new BytePortConfigValue(ByteUtils.getLowByte(lightPort.getDimMax())));
                    }
                    portConfigX.put(BidibLibrary.BIDIB_PCFG_LEVEL_PORT_OFF,
                        new BytePortConfigValue(ByteUtils.getLowByte(lightPort.getPwmMin())));
                    portConfigX.put(BidibLibrary.BIDIB_PCFG_LEVEL_PORT_ON,
                        new BytePortConfigValue(ByteUtils.getLowByte(lightPort.getPwmMax())));

                    port.setKnownPortConfigKeys(portConfigX.keySet());

                    port.setPortConfigX(portConfigX);

                    PortLabelUtils.replaceLabel(lightPortLabels, uniqueId, lightPort.getId(), lightPort.getLabel(),
                        LightPortLabelFactory.DEFAULT_LABELTYPE);
                }
                else {
                    LOGGER.warn("No light port found with port id: {}", lightPort.getId());
                }
            }
        }

        if (CollectionUtils.isNotEmpty(nodeState.getServoPorts())) {

            List<ServoPort> servoPorts = portsProvider.getServoPorts();

            final Labels servoPortLabels =
                applicationContext.get(DefaultApplicationContext.KEY_SERVOPORT_LABELS, Labels.class);

            for (ServoPort servoPort : nodeState.getServoPorts()) {

                ServoPort port =
                    org.bidib.wizard.utils.NodeUtils.getPort(servoPorts, servoPort.getId(), sourceIsFlatModel,
                        targetIsFlatModel);
                if (port != null) {
                    port.setLabel(servoPort.getLabel());

                    LOGGER.info("Set the configX for the servo port: {}", servoPort.getPortConfigX());
                    // migrate the missing configuration

                    // must set the supported configx keys ...
                    Map<Byte, PortConfigValue<?>> portConfigX = new LinkedHashMap<Byte, PortConfigValue<?>>();
                    portConfigX.put(BidibLibrary.BIDIB_PCFG_SERVO_SPEED,
                        new BytePortConfigValue(ByteUtils.getLowByte(servoPort.getSpeed())));
                    portConfigX.put(BidibLibrary.BIDIB_PCFG_SERVO_ADJ_L,
                        new BytePortConfigValue(ByteUtils.getLowByte(servoPort.getTrimDown())));
                    portConfigX.put(BidibLibrary.BIDIB_PCFG_SERVO_ADJ_H,
                        new BytePortConfigValue(ByteUtils.getLowByte(servoPort.getTrimUp())));

                    port.setKnownPortConfigKeys(portConfigX.keySet());

                    port.setPortConfigX(portConfigX);

                    PortLabelUtils.replaceLabel(servoPortLabels, uniqueId, port.getId(), servoPort.getLabel(),
                        ServoPortLabelFactory.DEFAULT_LABELTYPE);
                }
                else {
                    LOGGER.warn("No servo port found with port id: {}", servoPort.getId());
                }
            }
        }
        if (CollectionUtils.isNotEmpty(nodeState.getBacklightPorts())) {

            List<BacklightPort> backlightPorts = portsProvider.getBacklightPorts();

            final Labels backlightPortLabels =
                applicationContext.get(DefaultApplicationContext.KEY_BACKLIGHTPORT_LABELS, Labels.class);

            for (BacklightPort backlightPort : nodeState.getBacklightPorts()) {

                BacklightPort port =
                    org.bidib.wizard.utils.NodeUtils.getPort(backlightPorts, backlightPort.getId(), sourceIsFlatModel,
                        targetIsFlatModel);
                if (port != null) {
                    port.setLabel(backlightPort.getLabel());
                    // migrate the missing configuration

                    // must set the supported configx keys ...
                    Map<Byte, PortConfigValue<?>> portConfigX = new LinkedHashMap<Byte, PortConfigValue<?>>();

                    portConfigX.put(BidibLibrary.BIDIB_PCFG_DIMM_DOWN_8_8,
                        new BytePortConfigValue(ByteUtils.getLowByte(backlightPort.getDimSlopeDown())));
                    portConfigX.put(BidibLibrary.BIDIB_PCFG_DIMM_UP_8_8,
                        new BytePortConfigValue(ByteUtils.getLowByte(backlightPort.getDimSlopeUp())));

                    portConfigX.put(BidibLibrary.BIDIB_PCFG_OUTPUT_MAP,
                        new BytePortConfigValue(ByteUtils.getLowByte(backlightPort.getDmxMapping())));

                    port.setKnownPortConfigKeys(portConfigX.keySet());

                    port.setPortConfigX(portConfigX);

                    PortLabelUtils.replaceLabel(backlightPortLabels, uniqueId, backlightPort.getId(),
                        backlightPort.getLabel(), BacklightPortLabelFactory.DEFAULT_LABELTYPE);
                }
                else {
                    LOGGER.warn("No backlight port found with port id: {}", backlightPort.getId());
                }
            }
        }
        if (CollectionUtils.isNotEmpty(nodeState.getInputPorts())) {

            List<InputPort> inputPorts = portsProvider.getInputPorts();

            final Labels inputPortLabels =
                applicationContext.get(DefaultApplicationContext.KEY_INPUTPORT_LABELS, Labels.class);

            for (InputPort inputPort : nodeState.getInputPorts()) {
                LOGGER.info("Process inputPort from import: {}", inputPort);
                InputPort port =
                    org.bidib.wizard.utils.NodeUtils.getPort(inputPorts, inputPort.getId(), sourceIsFlatModel,
                        targetIsFlatModel);

                if (port != null) {
                    LOGGER.info("Apply configuration to real input port: {}, port number: {}", port, port.getId());

                    // found matching port on node
                    port.setLabel(inputPort.getLabel());

                    // must set the supported configx keys ...
                    Map<Byte, PortConfigValue<?>> portConfigX = new LinkedHashMap<Byte, PortConfigValue<?>>();

                    portConfigX.put(BidibLibrary.BIDIB_PCFG_TICKS,
                        new BytePortConfigValue(ByteUtils.getLowByte(inputPort.getSwitchOffTime())));

                    port.setKnownPortConfigKeys(portConfigX.keySet());

                    port.setPortConfigX(portConfigX);

                    PortLabelUtils.replaceLabel(inputPortLabels, uniqueId, inputPort.getId(), inputPort.getLabel(),
                        InputPortLabelFactory.DEFAULT_LABELTYPE);
                }
            }
        }
        if (CollectionUtils.isNotEmpty(nodeState.getMotorPorts())) {

            List<MotorPort> motorPorts = portsProvider.getMotorPorts();

            final Labels motorPortLabels =
                applicationContext.get(DefaultApplicationContext.KEY_MOTORPORT_LABELS, Labels.class);

            for (MotorPort motorPort : nodeState.getMotorPorts()) {
                MotorPort port =
                    org.bidib.wizard.utils.NodeUtils.getPort(motorPorts, motorPort.getId(), sourceIsFlatModel,
                        targetIsFlatModel);

                if (port != null) {
                    port.setLabel(motorPort.getLabel());
                    // migrate the missing configuration

                    // TODO must set the supported configx keys ...

                    port.setPortConfigX(motorPort.getPortConfigX());

                    PortLabelUtils.replaceLabel(motorPortLabels, uniqueId, motorPort.getId(), motorPort.getLabel(),
                        MotorPortLabelFactory.DEFAULT_LABELTYPE);
                }
                else {
                    LOGGER.warn("No motor port found with port id: {}", motorPort.getId());
                }
            }
        }
        if (CollectionUtils.isNotEmpty(nodeState.getSoundPorts())) {

            List<SoundPort> soundPorts = portsProvider.getSoundPorts();

            final Labels soundPortLabels =
                applicationContext.get(DefaultApplicationContext.KEY_SOUNDPORT_LABELS, Labels.class);

            for (SoundPort soundPort : nodeState.getSoundPorts()) {
                SoundPort port =
                    org.bidib.wizard.utils.NodeUtils.getPort(soundPorts, soundPort.getId(), sourceIsFlatModel,
                        targetIsFlatModel);

                if (port != null) {
                    port.setLabel(soundPort.getLabel());
                    // migrate the missing configuration

                    // must set the supported configx keys ...
                    Map<Byte, PortConfigValue<?>> portConfigX = new LinkedHashMap<Byte, PortConfigValue<?>>();

                    portConfigX.put(BidibLibrary.BIDIB_PCFG_TICKS,
                        new BytePortConfigValue(ByteUtils.getLowByte(soundPort.getPulseTime())));

                    port.setKnownPortConfigKeys(portConfigX.keySet());

                    port.setPortConfigX(portConfigX);

                    PortLabelUtils.replaceLabel(soundPortLabels, uniqueId, soundPort.getId(), soundPort.getLabel(),
                        SoundPortLabelFactory.DEFAULT_LABELTYPE);
                }
                else {
                    LOGGER.warn("No sound port found with port id: {}", soundPort.getId());
                }
            }
        }
        if (CollectionUtils.isNotEmpty(nodeState.getSwitchPorts())) {
            LOGGER.info("Set the switch ports.");

            List<SwitchPort> switchPorts = portsProvider.getEnabledSwitchPorts();

            final Labels switchPortLabels =
                applicationContext.get(DefaultApplicationContext.KEY_SWITCHPORT_LABELS, Labels.class);

            for (SwitchPort switchPort : nodeState.getSwitchPorts()) {
                LOGGER.info("Process switchPort from import: {}", switchPort);

                SwitchPort port =
                    org.bidib.wizard.utils.NodeUtils.getPort(switchPorts, switchPort.getId(), sourceIsFlatModel,
                        targetIsFlatModel);

                if (port != null) {
                    LOGGER.info("Apply configuration to real switch port: {}, port number: {}", port, port.getId());
                    port.setLabel(switchPort.getLabel());
                    // migrate the missing configuration

                    // must set the supported configx keys ...
                    Map<Byte, PortConfigValue<?>> portConfigX = new LinkedHashMap<Byte, PortConfigValue<?>>();

                    portConfigX.put(BidibLibrary.BIDIB_PCFG_TICKS,
                        new BytePortConfigValue(ByteUtils.getLowByte(switchPort.getSwitchOffTime())));

                    portConfigX.put(BidibLibrary.BIDIB_PCFG_SWITCH_CTRL,
                        new BytePortConfigValue(ByteUtils.getLowByte(switchPort.getOutputBehaviour().getType())));

                    port.setKnownPortConfigKeys(portConfigX.keySet());

                    port.setPortConfigX(portConfigX);

                    PortLabelUtils.replaceLabel(switchPortLabels, uniqueId, port.getId(), switchPort.getLabel(),
                        SwitchPortLabelFactory.DEFAULT_LABELTYPE);
                }
                else {
                    LOGGER.warn("No switch port found with port id: {}", switchPort.getId());
                }
            }
        }

        if (CollectionUtils.isNotEmpty(nodeState.getSwitchPairPorts())) {
            LOGGER.info("Set the switchPair ports.");

            List<SwitchPairPort> switchPairPorts = portsProvider.getEnabledSwitchPairPorts();

            final Labels switchPortLabels =
                applicationContext.get(DefaultApplicationContext.KEY_SWITCHPORT_LABELS, Labels.class);

            for (SwitchPairPort switchPairPort : nodeState.getSwitchPairPorts()) {
                LOGGER.info("Process switchPort from import: {}", switchPairPort);

                SwitchPairPort port =
                    org.bidib.wizard.utils.NodeUtils.getPort(switchPairPorts, switchPairPort.getId(), sourceIsFlatModel,
                        targetIsFlatModel);

                if (port != null) {
                    LOGGER.info("Apply configuration to real switchPair port: {}, port number: {}", port, port.getId());
                    port.setLabel(switchPairPort.getLabel());
                    // migrate the missing configuration

                    // must set the supported configx keys ...
                    Map<Byte, PortConfigValue<?>> portConfigX = new LinkedHashMap<Byte, PortConfigValue<?>>();

                    portConfigX.put(BidibLibrary.BIDIB_PCFG_TICKS,
                        new BytePortConfigValue(ByteUtils.getLowByte(switchPairPort.getSwitchOffTime())));

                    // portConfigX.put(BidibLibrary.BIDIB_PCFG_SWITCH_CTRL,
                    // new BytePortConfigValue(ByteUtils.getLowByte(switchPairPort.getOutputBehaviour().getType())));

                    port.setKnownPortConfigKeys(portConfigX.keySet());

                    port.setPortConfigX(portConfigX);

                    PortLabelUtils.replaceLabel(switchPortLabels, uniqueId, port.getId(), switchPairPort.getLabel(),
                        SwitchPortLabelFactory.DEFAULT_LABELTYPE);
                }
                else {
                    LOGGER.warn("No switch port found with port id: {}", switchPairPort.getId());
                }
            }
        }

        // restore the node strings
        if (checkRestoreNodeString != null && checkRestoreNodeString.booleanValue()) {
            LOGGER.info("Restore the node name from strings.");
            if (CollectionUtils.isNotEmpty(nodeState.getNodeStrings())) {
                // long uniqueId = node.getNode().getUniqueId();

                final Labels nodeLabels =
                    applicationContext.get(DefaultApplicationContext.KEY_NODE_LABELS, Labels.class);

                for (StringData stringData : nodeState.getNodeStrings()) {
                    if (stringData.getNamespace() == StringData.NAMESPACE_NODE) {
                        LOGGER.info("Set the node string: {}", stringData);
                        model.getSelectedNode().getNode().setStoredString(stringData.getIndex(), stringData.getValue());

                        if (StringData.INDEX_USERNAME == stringData.getIndex()) {
                            PortLabelUtils.replaceLabel(nodeLabels, uniqueId, 0, stringData.getValue(),
                                NodeLabelFactory.DEFAULT_LABELTYPE);
                        }
                    }
                    else {
                        LOGGER.warn("Unknown namespace detected: {}", stringData);
                    }
                }

                try {
                    new NodeLabelFactory().saveLabels(uniqueId, nodeLabels);
                }
                catch (InvalidConfigurationException ex) {
                    LOGGER.warn("Save node labels failed.", ex);

                    String labelPath = ex.getReason();
                    JOptionPane.showMessageDialog(JOptionPane.getFrameForComponent(null),
                        Resources.getString(Labels.class, "labelfileerror.message", new Object[] { labelPath }),
                        Resources.getString(Labels.class, "labelfileerror.title"), JOptionPane.ERROR_MESSAGE);
                }
            }
        }
        else {
            LOGGER.info("Node names from strings are not restored!");
        }

        if (checkRestoreMacroContent != null && checkRestoreMacroContent.booleanValue()) {
            LOGGER.info("Restore the macros and accessories.");

            final Labels macroLabels = applicationContext.get(DefaultApplicationContext.KEY_MACRO_LABELS, Labels.class);

            int maxMacroLength =
                CommunicationFactory.getInstance().getMaxMacroLength(model.getSelectedNode().getNode());
            for (Macro macro : nodeState.getMacros()) {

                // TODO the replacement of port references causes problems if ports can be mapped
                macro.setFlatPortModel(sourceIsFlatModel);

                // TODO this does not work!!!
                MacroFactory.replacePortReferences(macro, model, importParams);
                macro.setFunctionSize(maxMacroLength);
                // set pending changes flag
                macro.setMacroSaveState(MacroSaveState.PENDING_CHANGES);

                // replace the macro label
                replaceMacroLabel(macroLabels, uniqueId, macro, macro.getLabel(), false);
            }
            model.setMacros(nodeState.getMacros());

            // save the imported labels of the ports and macros
            try {
                // save the macro labels locally
                new MacroLabelFactory().saveLabels(uniqueId, macroLabels);
            }
            catch (InvalidConfigurationException ex) {
                LOGGER.warn("Save macro labels failed.", ex);

                String labelPath = ex.getReason();
                JOptionPane.showMessageDialog(JOptionPane.getFrameForComponent(null),
                    Resources.getString(Labels.class, "labelfileerror.message", new Object[] { labelPath }),
                    Resources.getString(Labels.class, "labelfileerror.title"), JOptionPane.ERROR_MESSAGE);
            }

            model.setAccessories(nodeState.getAccessories());
            int maxAccessoryLength =
                CommunicationFactory.getInstance().getAccessoryLength(model.getSelectedNode().getNode());

            final Labels accessoryLabels =
                applicationContext.get(DefaultApplicationContext.KEY_ACCESSORY_LABELS, Labels.class);

            for (Accessory accessory : nodeState.getAccessories()) {
                LOGGER.info("Current accessory: {}", accessory);

                accessory.setMacroSize(maxAccessoryLength);

                // replace the accessory label
                AccessoryLabelUtils.replaceLabel(accessoryLabels, uniqueId, accessory.getId(), accessory.getLabel());
            }
            try {
                // save the accessory labels locally
                new AccessoryLabelFactory().saveLabels(uniqueId, accessoryLabels);
            }
            catch (Exception e) {
                LOGGER.warn("Save accessory labels failed.", e);
            }

            importParams.put(SaveNodeConfigurationDialog.MACRO_AND_ACCESSORIES_RESTORED, Boolean.TRUE);
        }
        else {
            LOGGER.info("Macros and accessories are not restored!");
        }

        // restore features and CVs ...

        if (checkRestoreFeatures != null && checkRestoreFeatures.booleanValue()) {
            LOGGER.info("Restore the features.");

            if (CollectionUtils.isNotEmpty(nodeState.getFeatures())) {
                // keep the features in the model ... not stored on node!
                node.getNode().setFeatures(
                    org.bidib.jbidibc.core.utils.CollectionUtils.newLinkedList(nodeState.getFeatures()));

                importParams.put(SaveNodeConfigurationDialog.FEATURES_RESTORED, Boolean.TRUE);
            }
            else {
                LOGGER.warn("No features in import file found!");
            }
        }
        else {
            LOGGER.info("Features are not restored.");
        }

        if (checkRestoreCVs != null && checkRestoreCVs.booleanValue()) {
            LOGGER.info("Restore the CV values.");
            if (CollectionUtils.isNotEmpty(nodeState.getConfigurationVariables())) {
                // keep the CVs in the model ... not stored on node!
                List<ConfigurationVariable> loadedCVs =
                    org.bidib.jbidibc.core.utils.CollectionUtils.newLinkedList(nodeState.getConfigurationVariables());
                LOGGER.info("Initial loadedCVs: {}", loadedCVs);

                CollectionUtils.filter(loadedCVs, new Predicate<ConfigurationVariable>() {

                    @Override
                    public boolean evaluate(ConfigurationVariable cv) {
                        boolean timeoutDetected = cv.isTimeout();

                        if (timeoutDetected) {
                            LOGGER.warn("Skip restore CV with timeout: {}", cv);
                        }

                        return !timeoutDetected;
                    }
                });
                LOGGER.info("filtered CVs: {}", loadedCVs);

                // update the CV values in the CV map of the node and not on the mainModel

                // Use the cvNumberToNode map to set the new values
                Map<String, CvNode> map = node.getCvNumberToJideNodeMap();
                for (ConfigurationVariable cv : loadedCVs) {
                    CvNode cvNode = map.get(cv.getName());
                    if (cvNode != null && !(ModeType.RO.equals(cvNode.getCV().getMode())
                        || ModeType.H.equals(cvNode.getCV().getMode()))) {

                        cvNode.setNewValue(cv.getValue());
                    }
                }

                CvDefinitionTreeTableModel treeModel = node.getCvDefinitionTreeTableModel();
                if (treeModel != null) {

                    boolean hasPendingCVChanges = CvDefinitionTreeHelper.hasPendingChanges(treeModel);
                    LOGGER.info("After import of CV values, hasPendingCVChanges: {}", hasPendingCVChanges);
                }

                importParams.put(SaveNodeConfigurationDialog.CV_VALUES_RESTORED, Boolean.TRUE);
            }
            else {
                LOGGER.warn("No CV values in import file found!");
            }
        }
        else {
            LOGGER.info("CV values are not restored.");
        }

        if (importParams.containsKey(org.bidib.wizard.utils.NodeUtils.IMPORT_ERRORS)) {
            LOGGER.warn("The import node operation has finished with errors!!!");

            Frame frame = JOptionPane.getFrameForComponent(parent);
            // show an error dialog with the list of save errors during update node
            NodeErrorsDialog nodeErrorsDialog =
                new NodeErrorsDialog(frame,
                    Resources.getString(MainController.class, "import-prepare-values-failed.title"), true);

            // set the error information
            nodeErrorsDialog.setErrors(
                Resources.getString(MainController.class, "import-prepare-values-failed.message"),
                (List<String>) importParams.get(org.bidib.wizard.utils.NodeUtils.IMPORT_ERRORS));
            nodeErrorsDialog.setCancelEnabled(true);
            nodeErrorsDialog
                .setCloseButtonText(Resources.getString(MainController.class, "import-prepare-values-failed.close"));
            nodeErrorsDialog.pack();
            nodeErrorsDialog.showDialog();

            // TODO allow the user to canel the import
            if (JOptionPane.CANCEL_OPTION == nodeErrorsDialog.getCloseOption()) {
                LOGGER.info("User selected cancel operation in nodeErrorsDialog. Abort import of node data.");

                return;
            }
        }
    }

    public static void replaceMacroLabel(
        final Labels macroLabels, long uniqueId, Macro macro, String label, boolean save) {
        LOGGER.info("Replace the macro label, new label: {}, macro: {}", label, macro);

        PortLabelUtils.replaceLabel(macroLabels, uniqueId, macro.getId(), label, MacroLabelFactory.DEFAULT_LABELTYPE);

        if (save) {
            try {
                new MacroLabelFactory().saveLabels(uniqueId, macroLabels);
                // macroLabels.save();
            }
            catch (InvalidConfigurationException ex) {
                LOGGER.warn("Save macro labels failed.", ex);

                String labelPath = ex.getReason();
                JOptionPane.showMessageDialog(JOptionPane.getFrameForComponent(null),
                    Resources.getString(Labels.class, "labelfileerror.message", new Object[] { labelPath }),
                    Resources.getString(Labels.class, "labelfileerror.title"), JOptionPane.ERROR_MESSAGE);
            }
        }
    }
}
