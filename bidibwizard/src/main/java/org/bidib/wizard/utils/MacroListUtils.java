package org.bidib.wizard.utils;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.collections4.Predicate;
import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.comm.LightPortStatus;
import org.bidib.wizard.comm.MacroStatus;
import org.bidib.wizard.comm.SwitchPortStatus;
import org.bidib.wizard.mvc.main.model.Macro;
import org.bidib.wizard.mvc.main.model.PortsProvider;
import org.bidib.wizard.mvc.main.model.function.AccessoryOkayFunction;
import org.bidib.wizard.mvc.main.model.function.AnalogPortAction;
import org.bidib.wizard.mvc.main.model.function.BacklightPortAction;
import org.bidib.wizard.mvc.main.model.function.CriticalFunction;
import org.bidib.wizard.mvc.main.model.function.DelayFunction;
import org.bidib.wizard.mvc.main.model.function.EmptyFunction;
import org.bidib.wizard.mvc.main.model.function.FlagFunction;
import org.bidib.wizard.mvc.main.model.function.Function;
import org.bidib.wizard.mvc.main.model.function.InputFunction;
import org.bidib.wizard.mvc.main.model.function.LightPortAction;
import org.bidib.wizard.mvc.main.model.function.MacroFunction;
import org.bidib.wizard.mvc.main.model.function.MotorPortAction;
import org.bidib.wizard.mvc.main.model.function.RandomDelayFunction;
import org.bidib.wizard.mvc.main.model.function.ServoMoveQueryFunction;
import org.bidib.wizard.mvc.main.model.function.ServoPortAction;
import org.bidib.wizard.mvc.main.model.function.SoundPortAction;
import org.bidib.wizard.mvc.main.model.function.SwitchPairPortAction;
import org.bidib.wizard.mvc.main.model.function.SwitchPortAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MacroListUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(MacroListUtils.class);

    public static Macro findMacroByMacroNumber(List<Macro> ports, final int portNum) {
        Macro port = IterableUtils.find(ports, new Predicate<Macro>() {

            @Override
            public boolean evaluate(Macro port) {
                if (port.getId() == portNum) {
                    return true;
                }
                return false;
            }
        });
        return port;
    }

    /**
     * Prepare the available functions for the node.
     * 
     * @param portsProvider
     *            the ports provider
     * @return the functions
     */
    public static Function<? extends BidibStatus>[] prepareAvailableFunctions(final PortsProvider portsProvider) {
        List<Function<? extends BidibStatus>> functions = new LinkedList<Function<? extends BidibStatus>>();

        if (portsProvider.getAnalogPorts().size() > 0) {
            functions.add(new AnalogPortAction());
        }
        functions.add(new CriticalFunction());
        functions.add(new FlagFunction());
        if (portsProvider.getInputPorts().size() > 0) {
            functions.add(new InputFunction());
        }
        if (portsProvider.getLightPorts().size() > 0) {
            functions.add(new LightPortAction());
        }
        functions.add(new MacroFunction());
        if (portsProvider.getMotorPorts().size() > 0) {
            functions.add(new MotorPortAction());
        }
        if (portsProvider.getServoPorts().size() > 0) {
            functions.add(new ServoPortAction());
            functions.add(new ServoMoveQueryFunction());
        }
        if (portsProvider.getSoundPorts().size() > 0) {
            functions.add(new SoundPortAction());
        }
        if (portsProvider.getSwitchPorts().size() > 0) {
            functions.add(new SwitchPortAction());
        }
        if (portsProvider.getSwitchPairPorts().size() > 0) {
            functions.add(new SwitchPairPortAction());
        }
        if (portsProvider.getAccessories().size() > 0) {
            functions.add(new AccessoryOkayFunction());
        }
        if (portsProvider.getBacklightPorts().size() > 0) {
            functions.add(new BacklightPortAction());
        }
        functions.add(new DelayFunction());
        functions.add(new RandomDelayFunction());
        functions.add(new EmptyFunction());
        return functions.toArray(new Function<?>[0]);
    }

    public static Function<? extends BidibStatus> getInvertedFunction(Function<? extends BidibStatus> function) {
        Function<? extends BidibStatus> currentFunction = null;
        try {
            // manipulate the function
            if (function instanceof SwitchPortAction) {
                currentFunction = (Function<? extends BidibStatus>) function.clone();
                // invert the switch action
                SwitchPortAction switchPortAction = (SwitchPortAction) currentFunction;
                SwitchPortStatus switchPortStatus = switchPortAction.getAction();

                switchPortStatus = PortUtils.getOppositeStatus(switchPortStatus);
                switchPortAction.setAction(switchPortStatus);
            }
            else if (function instanceof LightPortAction) {
                currentFunction = (Function<? extends BidibStatus>) function.clone();
                // change the light action
                LightPortAction lightPortAction = (LightPortAction) currentFunction;
                LightPortStatus lightPortStatus = lightPortAction.getAction();
                lightPortStatus = PortUtils.getOppositeStatus(lightPortStatus);
                lightPortAction.setAction(lightPortStatus);
            }
            else if (function instanceof ServoPortAction) {
                currentFunction = (Function<? extends BidibStatus>) function.clone();
                // invert the servo target value
                ServoPortAction servoPortAction = (ServoPortAction) currentFunction;
                int currentValue = servoPortAction.getValue();
                int targetValue = 255 - currentValue;
                servoPortAction.setValue(targetValue);
            }
            else if (function instanceof MacroFunction) {
                currentFunction = (Function<? extends BidibStatus>) function.clone();
                // switch the macro function
                MacroFunction macroFunction = (MacroFunction) currentFunction;
                if (MacroStatus.START.equals(macroFunction.getAction())) {
                    macroFunction.setAction(MacroStatus.STOP);
                }
                else if (MacroStatus.STOP.equals(macroFunction.getAction())) {
                    macroFunction.setAction(MacroStatus.START);
                }
            }
        }
        catch (CloneNotSupportedException ex) {
            LOGGER.warn("Clone of function is not supported: {}", function, ex);
        }
        return currentFunction;
    }
}
