package org.bidib.wizard.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FirmwareUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(FirmwareUtils.class);

    // private static final String REGEX_FILENAME = "BiDiBCV-[0-9]{1,3}-[0-9]{1,5}\\.xml";
    private static final String REGEX_FILENAME = "(\\-|\\.)[0-9]+";

    /**
     * Check if the filename has a version encoded.
     * <p>
     * The filename schema is: BiDiBCV-<vid>-<pid>[-jj[.ii[.cc]]].xml with jj = major, ii = minor, cc = micro.
     * </p>
     * 
     * @param filename
     *            the filename
     * @return version encoded
     */
    public static boolean hasVersionInFilename(String filename) {

        Matcher m = Pattern.compile(REGEX_FILENAME).matcher(filename);

        int start = m.regionStart();
        int end = m.regionEnd();

        LOGGER.info("Region start: {}, end: {}", start, end);
        int totalFound = 0;

        while (m.find()) {

            LOGGER.info("groupCount: {}", m.groupCount());

            if (m.groupCount() > 0 && m.group(1) != null) {
                String text = m.group(0);
                LOGGER.info("1. Found text: [{}]", text);

                totalFound++;
            }
            else {
                String text = m.group(0);
                LOGGER.info("Plain [{}]", text);
            }
        }

        return totalFound > 2;

    }

}
