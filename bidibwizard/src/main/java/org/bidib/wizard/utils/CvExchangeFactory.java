package org.bidib.wizard.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.bidib.cvexchange.SaveCV;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

public class CvExchangeFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(CvExchangeFactory.class);

    private static final String JAXB_PACKAGE = "org.bidib.cvexchange";

    public static final String XSD_LOCATION = "/xsd/cv-exchange.xsd";

    public static SaveCV loadSaveCV(String filePath) throws FileNotFoundException {

        InputStream is = null;
        try {
            if (filePath.startsWith("/")) {
                is = CvExchangeFactory.class.getResourceAsStream(filePath);
            }
            else {
                File file = new File(filePath);
                is = new FileInputStream(file);
            }
        }
        catch (FileNotFoundException ex) {
            LOGGER.warn("Load saved CV from file failed.", ex);

            throw ex;
        }

        SaveCV saveCV = new CvExchangeFactory().loadSaveCVFile(is);
        LOGGER.trace("Loaded saveCV: {}", saveCV);

        return saveCV;
    }

    private SaveCV loadSaveCVFile(InputStream is) {

        SaveCV saveCV = null;
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(JAXB_PACKAGE);

            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            StreamSource streamSource = new StreamSource(CvExchangeFactory.class.getResourceAsStream(XSD_LOCATION));
            Schema schema = schemaFactory.newSchema(streamSource);
            unmarshaller.setSchema(schema);

            saveCV = (SaveCV) unmarshaller.unmarshal(is);
        }
        catch (JAXBException | SAXException ex) {
            LOGGER.warn("Load SaveCV from file failed.", ex);

            throw new RuntimeException("Load SaveCV from file failed.");
        }
        return saveCV;
    }
}
