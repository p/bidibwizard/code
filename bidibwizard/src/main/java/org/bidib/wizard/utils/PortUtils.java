package org.bidib.wizard.utils;

import org.bidib.jbidibc.core.enumeration.BacklightPortEnum;
import org.bidib.jbidibc.core.enumeration.LightPortEnum;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.exchange.dmxscenery.BacklightPortType;
import org.bidib.jbidibc.exchange.dmxscenery.LightPortType;
import org.bidib.jbidibc.exchange.dmxscenery.PortType;
import org.bidib.wizard.comm.BacklightPortStatus;
import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.comm.LightPortStatus;
import org.bidib.wizard.comm.SoundPortStatus;
import org.bidib.wizard.comm.SwitchPortStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PortUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(PortUtils.class);

    /**
     * Calculate the dimm duration in ms from a volumeFrom to a new volumeTo.
     * 
     * @param volumeFrom
     *            the start volume value
     * @param volumeTo
     *            the end volume value
     * @param delta
     *            the delta value (dimmUp, dimmDown)
     * @return the dimm duration in ms
     */
    public static int calculateDimmDuration(int volumeFrom, int volumeTo, int delta) {
        int volumeDelta = volumeTo - volumeFrom;

        return calculateDimmDuration((volumeDelta < 0 ? -volumeDelta : volumeDelta), delta);
    }

    /**
     * Calculate the dimm duration in ms for the volumeDelta value.
     * 
     * @param volumeDelta
     *            the volume delta value
     * @param delta
     *            the delta value (dimmUp, dimmDown)
     * @return the dimm duration in ms
     */
    public static int calculateDimmDuration(double volumeDelta, int delta) {
        LOGGER.debug("Calculate dimm duration, volumeDelta: {}", volumeDelta);
        if (delta < 1) {
            return 0;
        }

        int t = (int) ((volumeDelta * 256) * 10 /* ms */ / delta);

        return t;
    }

    public static int calculateDelayTicks(int timeDelta, int macroParaSlowdown) {
        LOGGER.debug("Calculate the delay, timeDelta: {}, macroParaSlowdown: {}", timeDelta, macroParaSlowdown);
        if (macroParaSlowdown < 1) {
            return 0;
        }
        int ticks = timeDelta / (macroParaSlowdown * 20 /* ms */);

        return ticks;
    }

    /**
     * @param portType
     *            the port type
     * @param action
     *            the action number
     * @return the action as BidibStatus
     */
    public static BidibStatus actionFromPortType(PortType portType, Integer action) {
        BidibStatus bidibStatus = null;
        if (portType != null && action != null) {
            if (portType instanceof LightPortType) {
                LightPortEnum lightPortEnum = LightPortEnum.valueOf(ByteUtils.getLowByte(action));

                bidibStatus = LightPortStatus.valueOf(lightPortEnum);
            }
            else if (portType instanceof BacklightPortType) {
                BacklightPortEnum backlightPortEnum = BacklightPortEnum.valueOf(ByteUtils.getLowByte(action));

                bidibStatus = BacklightPortStatus.valueOf(backlightPortEnum);
            }
            else {
                LOGGER.warn("Unsupported port type detected: {}", portType);
            }

            LOGGER.info("Return bidibStatus: {}", bidibStatus);
        }

        return bidibStatus;
    }

    @SuppressWarnings("unchecked")
    public static <T extends BidibStatus> T getOppositeStatus(T bidibStatus) {

        if (bidibStatus instanceof LightPortStatus) {
            LightPortStatus lightPortStatus = (LightPortStatus) bidibStatus;
            switch (lightPortStatus) {
                case DOWN:
                    bidibStatus = (T) LightPortStatus.UP;
                    break;
                case UP:
                    bidibStatus = (T) LightPortStatus.DOWN;
                    break;
                case ON:
                    bidibStatus = (T) LightPortStatus.OFF;
                    break;
                case OFF:
                    bidibStatus = (T) LightPortStatus.ON;
                    break;
                default:
                    break;
            }
        }
        else if (bidibStatus instanceof SwitchPortStatus) {
            SwitchPortStatus switchPortStatus = (SwitchPortStatus) bidibStatus;
            switch (switchPortStatus) {
                case ON:
                    bidibStatus = (T) SwitchPortStatus.OFF;
                    break;
                case OFF:
                    bidibStatus = (T) SwitchPortStatus.ON;
                    break;
                default:
                    // bidibStatus = bidibStatus;
                    break;
            }
        }
        else if (bidibStatus instanceof SoundPortStatus) {
            SoundPortStatus soundPortStatus = (SoundPortStatus) bidibStatus;
            switch (soundPortStatus) {
                case PLAY:
                    bidibStatus = (T) SoundPortStatus.STOP;
                    break;
                case STOP:
                    bidibStatus = (T) SoundPortStatus.PLAY;
                    break;
                default:
                    // bidibStatus = bidibStatus;
                    break;
            }
        }
        return bidibStatus;
    }
}
