package org.bidib.wizard.utils;

import org.bidib.jbidibc.core.schema.bidib2.FunctionAccessoryNotification;
import org.bidib.jbidibc.core.schema.bidib2.FunctionCriticalSection;
import org.bidib.jbidibc.core.schema.bidib2.FunctionDelay;
import org.bidib.jbidibc.core.schema.bidib2.FunctionFlag;
import org.bidib.jbidibc.core.schema.bidib2.FunctionInput;
import org.bidib.jbidibc.core.schema.bidib2.FunctionMacro;
import org.bidib.jbidibc.core.schema.bidib2.FunctionOutputLight;
import org.bidib.jbidibc.core.schema.bidib2.FunctionOutputSwitch;
import org.bidib.jbidibc.core.schema.bidib2.MacroPoint;
import org.bidib.jbidibc.core.schema.bidib2.MacroPointAccessoryNotification;
import org.bidib.jbidibc.core.schema.bidib2.MacroPointCriticalSection;
import org.bidib.jbidibc.core.schema.bidib2.MacroPointDelay;
import org.bidib.jbidibc.core.schema.bidib2.MacroPointFlag;
import org.bidib.jbidibc.core.schema.bidib2.MacroPointInput;
import org.bidib.jbidibc.core.schema.bidib2.MacroPointMacro;
import org.bidib.jbidibc.core.schema.bidib2.MacroPointOutputAnalog;
import org.bidib.jbidibc.core.schema.bidib2.MacroPointOutputBacklight;
import org.bidib.jbidibc.core.schema.bidib2.MacroPointOutputLight;
import org.bidib.jbidibc.core.schema.bidib2.MacroPointOutputMotor;
import org.bidib.jbidibc.core.schema.bidib2.MacroPointOutputServo;
import org.bidib.jbidibc.core.schema.bidib2.MacroPointOutputSound;
import org.bidib.jbidibc.core.schema.bidib2.MacroPointOutputSwitch;
import org.bidib.jbidibc.core.schema.bidib2.MacroPointOutputSwitchPair;
import org.bidib.jbidibc.core.schema.bidib2.MacroPointServoMoveQuery;
import org.bidib.wizard.comm.BidibStatus;
import org.bidib.wizard.mvc.main.model.function.AccessoryOkayFunction;
import org.bidib.wizard.mvc.main.model.function.AnalogPortAction;
import org.bidib.wizard.mvc.main.model.function.BacklightPortAction;
import org.bidib.wizard.mvc.main.model.function.CriticalFunction;
import org.bidib.wizard.mvc.main.model.function.DelayFunction;
import org.bidib.wizard.mvc.main.model.function.FlagFunction;
import org.bidib.wizard.mvc.main.model.function.Function;
import org.bidib.wizard.mvc.main.model.function.InputFunction;
import org.bidib.wizard.mvc.main.model.function.LightPortAction;
import org.bidib.wizard.mvc.main.model.function.MacroFunction;
import org.bidib.wizard.mvc.main.model.function.MotorPortAction;
import org.bidib.wizard.mvc.main.model.function.RandomDelayFunction;
import org.bidib.wizard.mvc.main.model.function.ServoMoveQueryFunction;
import org.bidib.wizard.mvc.main.model.function.ServoPortAction;
import org.bidib.wizard.mvc.main.model.function.SoundPortAction;
import org.bidib.wizard.mvc.main.model.function.SwitchPairPortAction;
import org.bidib.wizard.mvc.main.model.function.SwitchPortAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FunctionConversionFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(FunctionConversionFactory.class);

    public MacroPoint convert(Function<? extends BidibStatus> function) {
        MacroPoint macroPoint = null;

        LOGGER.info("Current key: {}", function.getKey());
        switch (function.getKey()) {
            case Function.KEY_ACCESSORY_OKAY:
                macroPoint = convertAccessoryAction((AccessoryOkayFunction) function);
                break;
            case Function.KEY_ANALOG:
                macroPoint = convertAnalogPortAction((AnalogPortAction) function);
                break;
            case Function.KEY_BACKLIGHT:
                macroPoint = convertBacklightPortAction((BacklightPortAction) function);
                break;
            case Function.KEY_CRITICAL:
                macroPoint = convertCriticalAction((CriticalFunction) function);
                break;
            case Function.KEY_DELAY:
                macroPoint = convertDelayAction((DelayFunction) function);
                break;
            case Function.KEY_FLAG:
                macroPoint = convertFlagAction((FlagFunction) function);
                break;
            case Function.KEY_INPUT:
                macroPoint = convertInputAction((InputFunction) function);
                break;
            case Function.KEY_LIGHT:
                macroPoint = convertLightPortAction((LightPortAction) function);
                break;
            case Function.KEY_MACRO:
                macroPoint = convertMacroAction((MacroFunction) function);
                break;
            case Function.KEY_MOTOR:
                macroPoint = convertMotorPortAction((MotorPortAction) function);
                break;
            case Function.KEY_RANDOM_DELAY:
                macroPoint = convertRandomDelayAction((RandomDelayFunction) function);
                break;
            case Function.KEY_SERVO:
                macroPoint = convertServoPortAction((ServoPortAction) function);
                break;
            case Function.KEY_SERVO_MOVE_QUERY:
                macroPoint = convertServoMoveQueryAction((ServoMoveQueryFunction) function);
                break;
            case Function.KEY_SOUND:
                macroPoint = convertSoundPortAction((SoundPortAction) function);
                break;
            case Function.KEY_SWITCH:
                macroPoint = convertSwitchPortAction((SwitchPortAction) function);
                break;
            case Function.KEY_SWITCHPAIR:
                macroPoint = convertSwitchPairPortAction((SwitchPairPortAction) function);
                break;
            default:
                LOGGER.warn("Unknown function detected: {}", function);
                break;
        }

        return macroPoint;
    }

    protected MacroPoint convertInputAction(InputFunction function) {
        MacroPointInput macroPoint = new MacroPointInput();

        switch (function.getAction()) {
            case QUERY0:
                macroPoint.setFunction(FunctionInput.WAIT_FOR_0);
                break;
            case QUERY1:
                macroPoint.setFunction(FunctionInput.WAIT_FOR_1);
                break;
            default:
                LOGGER.warn("Unknown input action detected: {}", function);
                break;
        }

        macroPoint.setInputNumber(function.getInput().getId());

        return macroPoint;
    }

    protected MacroPoint convertMacroAction(MacroFunction function) {
        MacroPointMacro macroPoint = new MacroPointMacro();

        switch (function.getAction()) {
            case START:
                macroPoint.setFunction(FunctionMacro.START);
                break;
            case STOP:
                macroPoint.setFunction(FunctionMacro.STOP);
                break;
            default:
                LOGGER.warn("Unknown macro action detected: {}", function);
                break;
        }
        macroPoint.setMacroNumber(function.getMacroId());

        return macroPoint;
    }

    protected MacroPoint convertFlagAction(FlagFunction function) {
        MacroPointFlag macroPoint = new MacroPointFlag();

        switch (function.getAction()) {
            case CLEAR:
                macroPoint.setFunction(FunctionFlag.RESET);
                break;
            case QUERY_0:
                macroPoint.setFunction(FunctionFlag.QUERY_0);
                break;
            case QUERY_1:
                macroPoint.setFunction(FunctionFlag.QUERY_1);
                break;
            case SET:
                macroPoint.setFunction(FunctionFlag.SET);
                break;
            default:
                LOGGER.warn("Unknown flag action detected: {}", function);
                break;
        }

        macroPoint.setFlagNumber(function.getFlag().getId());

        return macroPoint;
    }

    protected MacroPoint convertDelayAction(DelayFunction function) {
        MacroPointDelay macroPoint = new MacroPointDelay();

        macroPoint.setFunction(FunctionDelay.FIXED);
        macroPoint.setDelay(function.getDelay());

        return macroPoint;
    }

    protected MacroPoint convertRandomDelayAction(RandomDelayFunction function) {
        MacroPointDelay macroPoint = new MacroPointDelay();

        macroPoint.setFunction(FunctionDelay.RANDOM);
        macroPoint.setDelay(function.getMaximumValue());

        return macroPoint;
    }

    protected MacroPoint convertCriticalAction(CriticalFunction function) {
        MacroPointCriticalSection macroPoint = new MacroPointCriticalSection();

        switch (function.getAction()) {
            case BEGIN:
                macroPoint.setFunction(FunctionCriticalSection.BEGIN);
                break;
            case END:
                macroPoint.setFunction(FunctionCriticalSection.END);
                break;
            default:
                LOGGER.warn("Unknown criticalSection action detected: {}", function);
                break;
        }

        return macroPoint;
    }

    protected MacroPoint convertAccessoryAction(AccessoryOkayFunction function) {
        MacroPointAccessoryNotification macroPoint = new MacroPointAccessoryNotification();

        switch (function.getAction()) {
            case QUERY0:
                macroPoint.setFunction(FunctionAccessoryNotification.OKAY_IF_INPUT_0);
                macroPoint.setInputNumber(function.getInput().getId());
                break;
            case QUERY1:
                macroPoint.setFunction(FunctionAccessoryNotification.OKAY_IF_INPUT_1);
                macroPoint.setInputNumber(function.getInput().getId());
                break;
            case NO_FEEDBACK:
                macroPoint.setFunction(FunctionAccessoryNotification.OKAY);
                break;
            default:
                LOGGER.warn("Unknown accessoryOkay action detected: {}", function.getAction());
                break;
        }

        return macroPoint;
    }

    protected MacroPoint convertServoMoveQueryAction(ServoMoveQueryFunction function) {
        MacroPointServoMoveQuery macroPoint = new MacroPointServoMoveQuery();
        macroPoint.setOutputNumber(function.getPort().getId());
        return macroPoint;
    }

    protected MacroPoint convertServoPortAction(ServoPortAction action) {

        MacroPointOutputServo macroPoint = new MacroPointOutputServo();
        macroPoint.setDelay(action.getDelay());
        macroPoint.setOutputNumber(action.getPort().getId());
        macroPoint.setPosition(action.getValue());

        return macroPoint;
    }

    protected MacroPoint convertSwitchPortAction(SwitchPortAction action) {

        MacroPointOutputSwitch macroPoint = new MacroPointOutputSwitch();
        macroPoint.setDelay(action.getDelay());
        macroPoint.setOutputNumber(action.getPort().getId());

        switch (action.getAction()) {
            case OFF:
                macroPoint.setFunction(FunctionOutputSwitch.OFF);
                break;
            case ON:
                macroPoint.setFunction(FunctionOutputSwitch.ON);
                break;
            default:
                LOGGER.warn("Unknown switchport action detected: {}", action.getAction());
                break;
        }
        return macroPoint;
    }

    protected MacroPoint convertSwitchPairPortAction(SwitchPairPortAction action) {

        MacroPointOutputSwitchPair macroPoint = new MacroPointOutputSwitchPair();
        macroPoint.setDelay(action.getDelay());
        macroPoint.setOutputNumber(action.getPort().getId());

        switch (action.getAction()) {
            case OFF:
                macroPoint.setFunction(FunctionOutputSwitch.OFF);
                break;
            case ON:
                macroPoint.setFunction(FunctionOutputSwitch.ON);
                break;
            default:
                LOGGER.warn("Unknown switchport action detected: {}", action.getAction());
                break;
        }
        return macroPoint;
    }

    protected MacroPoint convertLightPortAction(LightPortAction action) {
        MacroPointOutputLight macroPoint = new MacroPointOutputLight();
        macroPoint.setDelay(action.getDelay());
        macroPoint.setOutputNumber(action.getPort().getId());

        switch (action.getAction()) {
            case BLINKA:
                macroPoint.setFunction(FunctionOutputLight.BLINK_A);
                break;
            case BLINKB:
                macroPoint.setFunction(FunctionOutputLight.BLINK_B);
                break;
            case DOUBLEFLASH:
                macroPoint.setFunction(FunctionOutputLight.DOUBLE_FLASH);
                break;
            case DOWN:
                macroPoint.setFunction(FunctionOutputLight.DIM_DOWN);
                break;
            case FLASHA:
                macroPoint.setFunction(FunctionOutputLight.FLASH_A);
                break;
            case FLASHB:
                macroPoint.setFunction(FunctionOutputLight.FLASH_B);
                break;
            case NEON:
                macroPoint.setFunction(FunctionOutputLight.NEON_FLICKER);
                break;
            case OFF:
                macroPoint.setFunction(FunctionOutputLight.TURN_OFF);
                break;
            case ON:
                macroPoint.setFunction(FunctionOutputLight.TURN_ON);
                break;
            case UP:
                macroPoint.setFunction(FunctionOutputLight.DIM_UP);
                break;
            default:
                LOGGER.warn("Unknown lightport action detected: {}", action.getAction());
                break;
        }

        return macroPoint;
    }

    protected MacroPoint convertBacklightPortAction(BacklightPortAction action) {
        MacroPointOutputBacklight macroPoint = new MacroPointOutputBacklight();
        macroPoint.setDelay(action.getDelay());
        macroPoint.setOutputNumber(action.getPort().getId());
        macroPoint.setBrightness(action.getValue());

        return macroPoint;
    }

    protected MacroPoint convertAnalogPortAction(AnalogPortAction function) {
        MacroPointOutputAnalog macroPoint = new MacroPointOutputAnalog();
        macroPoint.setDelay(function.getDelay());
        macroPoint.setOutputNumber(function.getPort().getId());

        return macroPoint;
    }

    protected MacroPoint convertMotorPortAction(MotorPortAction function) {
        MacroPointOutputMotor macroPoint = new MacroPointOutputMotor();
        macroPoint.setDelay(function.getDelay());
        macroPoint.setOutputNumber(function.getPort().getId());

        return macroPoint;
    }

    protected MacroPoint convertSoundPortAction(SoundPortAction function) {
        MacroPointOutputSound macroPoint = new MacroPointOutputSound();
        macroPoint.setDelay(function.getDelay());
        macroPoint.setOutputNumber(function.getPort().getId());

        return macroPoint;
    }
}
