package org.bidib.wizard.comm;

import org.bidib.jbidibc.core.enumeration.LightPortEnum;
import org.bidib.wizard.common.locale.Resources;

public enum LightPortStatus implements BidibStatus {
    // @formatter:off
    ON(LightPortEnum.ON, "on"), OFF(LightPortEnum.OFF, "off"), UP(LightPortEnum.UP, "up"), DOWN(LightPortEnum.DOWN,
        "down"), NEON(LightPortEnum.NEON, "neon"), BLINKA(LightPortEnum.BLINKA, "blinka"), BLINKB(LightPortEnum.BLINKB,
        "blinkb"), FLASHA(LightPortEnum.FLASHA, "flasha"), FLASHB(LightPortEnum.FLASHB, "flashb"), DOUBLEFLASH(
        LightPortEnum.DOUBLEFLASH, "doubleflash"), UNKNOWN(LightPortEnum.UNKNOWN, "unknown"), TEST(LightPortEnum.TEST,
        "test");
    // @formatter:on

    private final LightPortEnum type;

    private final String key;

    private final String label;

    LightPortStatus(LightPortEnum type, String key) {
        this.type = type;
        this.key = key;
        this.label = Resources.getString(LightPortStatus.class, key);
    }

    public LightPortEnum getType() {
        return type;
    }

    public String getKey() {
        return key;
    }

    public String toString() {
        return label;
    }

    public static LightPortStatus valueOf(LightPortEnum type) {
        LightPortStatus result = null;

        for (LightPortStatus e : values()) {
            if (e.type == type) {
                result = e;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("cannot map " + type + " to a light port status");
        }
        return result;
    }

    @Override
    public BidibStatus[] getValues() {
        return new BidibStatus[] { ON, OFF, UP, DOWN, NEON, BLINKA, BLINKB, FLASHA, FLASHB, DOUBLEFLASH, TEST };
    }
}
