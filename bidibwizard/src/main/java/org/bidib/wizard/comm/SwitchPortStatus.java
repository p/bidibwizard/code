package org.bidib.wizard.comm;

import org.bidib.jbidibc.core.enumeration.SwitchPortEnum;
import org.bidib.wizard.common.locale.Resources;

public enum SwitchPortStatus implements BidibStatus {
    // @formatter:off
    ON(SwitchPortEnum.ON, "on"), OFF(SwitchPortEnum.OFF, "off"), TEST(SwitchPortEnum.TEST, "test");
    // @formatter:on

    private final SwitchPortEnum type;

    private final String key;

    private final String label;

    SwitchPortStatus(SwitchPortEnum type, String key) {
        this.type = type;
        this.key = key;
        this.label = Resources.getString(SwitchPortStatus.class, key);
    }

    public SwitchPortEnum getType() {
        return type;
    }

    public String getKey() {
        return key;
    }

    public String toString() {
        return label;
    }

    public static SwitchPortStatus valueOf(SwitchPortEnum type) {
        SwitchPortStatus result = null;

        for (SwitchPortStatus e : values()) {
            if (e.type == type) {
                result = e;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("cannot map " + type + " to a switch port status");
        }
        return result;
    }

    @Override
    public BidibStatus[] getValues() {
        return new BidibStatus[] { ON, OFF, TEST };
    }
}
