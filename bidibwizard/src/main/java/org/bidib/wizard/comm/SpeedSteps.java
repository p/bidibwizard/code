package org.bidib.wizard.comm;

import org.bidib.jbidibc.core.DriveState;
import org.bidib.jbidibc.core.enumeration.SpeedStepsEnum;
import org.bidib.wizard.common.locale.Resources;

public enum SpeedSteps implements BidibStatus {
    // @formatter:off
    DCC14(14, "14", SpeedStepsEnum.DCC14), DCC28(28, "28", SpeedStepsEnum.DCC28), 
    DCC128(126, "128", SpeedStepsEnum.DCC128), MM14(14, "MM14", SpeedStepsEnum.MM14), 
    MM27a(27, "MM27a", SpeedStepsEnum.MM27a), MM27b(27, "MM27b", SpeedStepsEnum.MM27b), 
    M4(128, "M4", SpeedStepsEnum.M4);
    // @formatter:on

    private final int steps;

    private final String label;

    private final String key;

    private final SpeedStepsEnum type;

    SpeedSteps(int steps, String key, SpeedStepsEnum type) {
        this.steps = steps;
        this.key = key;
        this.label = Resources.getString(SpeedSteps.class, key);
        this.type = type;
    }

    public int getSteps() {
        return steps;
    }

    public SpeedStepsEnum getType() {
        return type;
    }

    public String getKey() {
        return key;
    }

    public String toString() {
        return label;
    }

    public static SpeedSteps valueOf(SpeedStepsEnum type) {
        SpeedSteps result = null;

        for (SpeedSteps e : values()) {
            if (e.type == type) {
                result = e;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("cannot map " + type + " to a speed step");
        }
        return result;
    }

    public static SpeedSteps valueOf(int speedSteps) {
        SpeedSteps result = null;

        // change 128 --> 126
        if (speedSteps == 128) {
            speedSteps = 126;
        }

        for (SpeedSteps e : values()) {
            if (e.steps == speedSteps) {
                result = e;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("cannot map " + speedSteps + " to a speed step");
        }
        return result;
    }

    public static SpeedSteps fromBidibFormat(int format) {
        SpeedSteps result = null;
        switch (format) {
            case DriveState.DRIVE_ADDRESS_FORMAT_DCC14:
                result = SpeedSteps.DCC14;
                break;
            case DriveState.DRIVE_ADDRESS_FORMAT_DCC28:
                result = SpeedSteps.DCC28;
                break;
            case DriveState.DRIVE_ADDRESS_FORMAT_DCC128:
                result = SpeedSteps.DCC128;
                break;
            case DriveState.DRIVE_ADDRESS_FORMAT_MM14:
                result = SpeedSteps.MM14;
                break;
            case DriveState.DRIVE_ADDRESS_FORMAT_MM27a:
                result = SpeedSteps.MM27a;
                break;
            case DriveState.DRIVE_ADDRESS_FORMAT_MM27b:
                result = SpeedSteps.MM27b;
                break;
            case DriveState.DRIVE_ADDRESS_FORMAT_M4:
                result = SpeedSteps.M4;
                break;
            default:
                break;
        }
        return result;
    }

    @Override
    public BidibStatus[] getValues() {
        return new BidibStatus[] { DCC14, DCC28, DCC128 };
    }
}
