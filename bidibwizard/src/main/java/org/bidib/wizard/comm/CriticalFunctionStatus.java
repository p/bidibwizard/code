package org.bidib.wizard.comm;

import org.bidib.jbidibc.core.enumeration.CriticalFunctionEnum;
import org.bidib.wizard.common.locale.Resources;

public enum CriticalFunctionStatus implements BidibStatus {
    // @formatter:off
    BEGIN(CriticalFunctionEnum.BEGIN, "begin"), END(CriticalFunctionEnum.END, "end");
    // @formatter:on

    private final CriticalFunctionEnum type;

    private final String key;

    private final String label;

    CriticalFunctionStatus(CriticalFunctionEnum type, String key) {
        this.type = type;
        this.key = key;
        this.label = Resources.getString(CriticalFunctionStatus.class, key);
    }

    public CriticalFunctionEnum getType() {
        return type;
    }

    public String getKey() {
        return key;
    }

    public String toString() {
        return label;
    }

    public static CriticalFunctionStatus valueOf(CriticalFunctionEnum type) {
        CriticalFunctionStatus result = null;

        for (CriticalFunctionStatus e : values()) {
            if (e.type == type) {
                result = e;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("cannot map " + type + " to a critical function status");
        }
        return result;
    }

    @Override
    public BidibStatus[] getValues() {
        return new BidibStatus[] { BEGIN, END };
    }
}
