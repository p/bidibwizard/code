package org.bidib.wizard.comm;

import org.bidib.jbidibc.core.enumeration.MacroEnum;
import org.bidib.wizard.common.locale.Resources;

public enum MacroStatus implements BidibStatus {
    // @formatter:off
    START(MacroEnum.START, "start"), STOP(MacroEnum.STOP, "stop"), END(MacroEnum.END, "end");
    // @formatter:on

    private final MacroEnum type;

    private final String key;

    private final String label;

    MacroStatus(MacroEnum type, String key) {
        this.type = type;
        this.key = key;
        this.label = Resources.getString(MacroStatus.class, key);
    }

    public MacroEnum getType() {
        return type;
    }

    public String getKey() {
        return key;
    }

    public String toString() {
        return label;
    }

    public static MacroStatus valueOf(MacroEnum type) {
        MacroStatus result = null;

        for (MacroStatus e : values()) {
            if (e.type == type) {
                result = e;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("cannot map " + type + " to a macro status");
        }
        return result;
    }

    @Override
    public BidibStatus[] getValues() {
        return new BidibStatus[] { START, STOP /* , END */ };
    }
}
