package org.bidib.wizard.comm.bidib;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.bidib.jbidibc.core.AddressData;
import org.bidib.jbidibc.core.BidibFactory;
import org.bidib.jbidibc.core.BidibInterface;
import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.BidibMessageProcessor;
import org.bidib.jbidibc.core.BidibPort;
import org.bidib.jbidibc.core.ConnectionListener;
import org.bidib.jbidibc.core.DecoderIdAddressData;
import org.bidib.jbidibc.core.DecoderUniqueIdData;
import org.bidib.jbidibc.core.Feature;
import org.bidib.jbidibc.core.FirmwareUpdateStat;
import org.bidib.jbidibc.core.LcConfig;
import org.bidib.jbidibc.core.LcConfigX;
import org.bidib.jbidibc.core.LcMacro;
import org.bidib.jbidibc.core.LcMacroParaValue;
import org.bidib.jbidibc.core.MessageListener;
import org.bidib.jbidibc.core.Node;
import org.bidib.jbidibc.core.NodeListener;
import org.bidib.jbidibc.core.ProtocolVersion;
import org.bidib.jbidibc.core.RcPlusBindData;
import org.bidib.jbidibc.core.SoftwareVersion;
import org.bidib.jbidibc.core.StringData;
import org.bidib.jbidibc.core.TidData;
import org.bidib.jbidibc.core.VendorData;
import org.bidib.jbidibc.core.enumeration.AccessoryAcknowledge;
import org.bidib.jbidibc.core.enumeration.ActivateCoilEnum;
import org.bidib.jbidibc.core.enumeration.BoosterState;
import org.bidib.jbidibc.core.enumeration.CommandStationPom;
import org.bidib.jbidibc.core.enumeration.CommandStationPt;
import org.bidib.jbidibc.core.enumeration.CommandStationState;
import org.bidib.jbidibc.core.enumeration.CsQueryTypeEnum;
import org.bidib.jbidibc.core.enumeration.DirectionEnum;
import org.bidib.jbidibc.core.enumeration.FeatureEnum;
import org.bidib.jbidibc.core.enumeration.FirmwareUpdateOperation;
import org.bidib.jbidibc.core.enumeration.IdentifyState;
import org.bidib.jbidibc.core.enumeration.IoBehaviourSwitchEnum;
import org.bidib.jbidibc.core.enumeration.LcMacroOperationCode;
import org.bidib.jbidibc.core.enumeration.LcMacroState;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.PomAcknowledge;
import org.bidib.jbidibc.core.enumeration.PortModelEnum;
import org.bidib.jbidibc.core.enumeration.RcPlusPhase;
import org.bidib.jbidibc.core.enumeration.TimeBaseUnitEnum;
import org.bidib.jbidibc.core.enumeration.TimingControlEnum;
import org.bidib.jbidibc.core.exception.InvalidConfigurationException;
import org.bidib.jbidibc.core.exception.NoAnswerException;
import org.bidib.jbidibc.core.exception.PortNotFoundException;
import org.bidib.jbidibc.core.exception.PortNotOpenedException;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.exception.ProtocolInvalidParamException;
import org.bidib.jbidibc.core.exception.ProtocolNoAnswerException;
import org.bidib.jbidibc.core.helpers.Context;
import org.bidib.jbidibc.core.message.AccessoryParaResponse;
import org.bidib.jbidibc.core.message.BoostOnMessage;
import org.bidib.jbidibc.core.message.SysClockMessage;
import org.bidib.jbidibc.core.node.AccessoryNode;
import org.bidib.jbidibc.core.node.BidibNode;
import org.bidib.jbidibc.core.node.CommandStationNode;
import org.bidib.jbidibc.core.node.ConfigurationVariable;
import org.bidib.jbidibc.core.node.RootNode;
import org.bidib.jbidibc.core.node.listener.TransferListener;
import org.bidib.jbidibc.core.port.BytePortConfigValue;
import org.bidib.jbidibc.core.port.Int16PortConfigValue;
import org.bidib.jbidibc.core.port.PortConfigValue;
import org.bidib.jbidibc.core.port.ReconfigPortConfigValue;
import org.bidib.jbidibc.core.port.RgbPortConfigValue;
import org.bidib.jbidibc.core.utils.AccessoryStateUtils.ErrorAccessoryState.AccessoryExecutionState;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.jbidibc.core.utils.MessageUtils;
import org.bidib.jbidibc.core.utils.NodeUtils;
import org.bidib.wizard.comm.AccessoryOkayStatus;
import org.bidib.wizard.comm.AnalogPortStatus;
import org.bidib.wizard.comm.Communication;
import org.bidib.wizard.comm.CommunicationFactory;
import org.bidib.wizard.comm.ConversionUtils;
import org.bidib.wizard.comm.Direction;
import org.bidib.wizard.comm.LightPortStatus;
import org.bidib.wizard.comm.MacroConversionHelper;
import org.bidib.wizard.comm.SoundPortStatus;
import org.bidib.wizard.comm.SpeedSteps;
import org.bidib.wizard.comm.SwitchPortStatus;
import org.bidib.wizard.comm.exception.InternalStartupException;
import org.bidib.wizard.comm.listener.CommunicationListener;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.common.model.PreferencesPortType;
import org.bidib.wizard.mvc.main.model.Accessory;
import org.bidib.wizard.mvc.main.model.AccessorySaveState;
import org.bidib.wizard.mvc.main.model.AnalogPort;
import org.bidib.wizard.mvc.main.model.BacklightPort;
import org.bidib.wizard.mvc.main.model.FeedbackPort;
import org.bidib.wizard.mvc.main.model.Flag;
import org.bidib.wizard.mvc.main.model.GenericPort;
import org.bidib.wizard.mvc.main.model.InputPort;
import org.bidib.wizard.mvc.main.model.LightPort;
import org.bidib.wizard.mvc.main.model.Macro;
import org.bidib.wizard.mvc.main.model.MacroRef;
import org.bidib.wizard.mvc.main.model.MacroRepeatDay;
import org.bidib.wizard.mvc.main.model.MacroRepeatTime;
import org.bidib.wizard.mvc.main.model.MacroSaveState;
import org.bidib.wizard.mvc.main.model.MacroUtils;
import org.bidib.wizard.mvc.main.model.MotorPort;
import org.bidib.wizard.mvc.main.model.Port;
import org.bidib.wizard.mvc.main.model.ServoPort;
import org.bidib.wizard.mvc.main.model.SoundPort;
import org.bidib.wizard.mvc.main.model.StartCondition;
import org.bidib.wizard.mvc.main.model.SwitchPort;
import org.bidib.wizard.mvc.main.model.TimeStartCondition;
import org.bidib.wizard.mvc.main.model.function.AccessoryOkayFunction;
import org.bidib.wizard.mvc.main.model.function.CriticalFunction;
import org.bidib.wizard.mvc.main.model.function.DelayFunction;
import org.bidib.wizard.mvc.main.model.function.Delayable;
import org.bidib.wizard.mvc.main.model.function.FlagFunction;
import org.bidib.wizard.mvc.main.model.function.Function;
import org.bidib.wizard.mvc.main.model.function.InputFunction;
import org.bidib.wizard.mvc.main.model.function.MacroFunction;
import org.bidib.wizard.mvc.main.model.function.PortAction;
import org.bidib.wizard.mvc.main.model.function.PortAware;
import org.bidib.wizard.mvc.main.model.function.PortValueAware;
import org.bidib.wizard.mvc.main.model.function.RandomDelayFunction;
import org.bidib.wizard.mvc.main.model.function.ServoMoveQueryFunction;
import org.bidib.wizard.mvc.main.view.statusbar.StatusBar;
import org.bidib.wizard.mvc.preferences.model.Preferences;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BidibCommunication implements Communication {
    private static final Logger LOGGER = LoggerFactory.getLogger(BidibCommunication.class);

    private static final int DMX_MAPPING_OFFSET = 1;

    private final Set<CommunicationListener> communicationListeners = new LinkedHashSet<CommunicationListener>();

    private final Set<TransferListener> transferListeners = new LinkedHashSet<TransferListener>();

    private final Set<MessageListener> messageListeners = new LinkedHashSet<MessageListener>();

    private final Set<NodeListener> nodeListeners = new LinkedHashSet<NodeListener>();

    private final Map<Long, List<ConfigurationVariable>> configurationVariables =
        new HashMap<Long, List<ConfigurationVariable>>();

    private final Map<Long, List<Accessory>> accessories = new HashMap<Long, List<Accessory>>();

    private final Map<Long, List<AnalogPort>> analogPorts = new HashMap<Long, List<AnalogPort>>();

    private final Map<Long, List<FeedbackPort>> feedbackPorts = new HashMap<Long, List<FeedbackPort>>();

    // private final List<Flag> flags = new LinkedList<Flag>();

    private final Map<Long, List<InputPort>> inputPorts = new HashMap<Long, List<InputPort>>();

    private final Map<Long, List<LightPort>> lightPorts = new HashMap<Long, List<LightPort>>();

    private final Map<Long, List<BacklightPort>> backlightPorts = new HashMap<Long, List<BacklightPort>>();

    private final Map<Long, List<Macro>> macros = new HashMap<Long, List<Macro>>();

    private final Map<Long, List<MotorPort>> motorPorts = new HashMap<Long, List<MotorPort>>();

    private final List<Node> nodes = new LinkedList<Node>();

    private final Map<Long, List<ServoPort>> servoPorts = new HashMap<Long, List<ServoPort>>();

    private final Map<Long, List<SoundPort>> soundPorts = new HashMap<Long, List<SoundPort>>();

    private final Map<Long, List<SwitchPort>> switchPorts = new HashMap<Long, List<SwitchPort>>();

    private PreferencesPortType configuredPortType;

    private PreferencesPortType connectedPortType;

    private int resetReconnectDelay = 1500;

    private int responseTimeout = BidibInterface.DEFAULT_TIMEOUT;

    private BidibInterface bidib;

    protected final ScheduledExecutorService dccAccessoryWorker = Executors.newScheduledThreadPool(1);

    /**
     * Creates a new instance of BidibCommunication with the connected comm port.
     */
    public BidibCommunication(PreferencesPortType configuredPortType) {
        this.configuredPortType = configuredPortType;

        resetReconnectDelay = Preferences.getInstance().getResetReconnectDelay();
        responseTimeout = Preferences.getInstance().getResponseTimeout();

        LOGGER.info("Initialized dmxMappingOffset: {}, resetReconnectDelay: {}, responseTimeout: {}",
            DMX_MAPPING_OFFSET, resetReconnectDelay, responseTimeout);

        // add preferences listener
        Preferences.getInstance().addPropertyChangeListener(Preferences.PROPERTY_SELECTED_PORTTYPE,
            new PropertyChangeListener() {

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    PreferencesPortType portType = Preferences.getInstance().getSelectedPortType();

                    LOGGER.info("The selected port type has changed: {}", portType);
                    PreferencesPortType connectedPort = null;
                    boolean portClosed = false;
                    // check if the comm port has changed
                    synchronized (CommunicationFactory.class) {
                        try {
                            if (bidib != null) {
                                connectedPort = connectedPortType;

                                if (!portType.equals(getConnectedPortType()) || (portType.getConnectionName() != null
                                    && !portType.getConnectionName().equals(connectedPort.getConnectionName()))) {

                                    // the comm port has changed
                                    LOGGER.info("An instance of Bidib is available and will be closed: {}", bidib);
                                    try {
                                        if (bidib.isOpened()) {
                                            bidib.close();
                                        }
                                    }
                                    catch (Exception ex) {
                                        LOGGER.warn("Close Bidib instance failed.", ex);
                                    }

                                    bidib = null;
                                    portClosed = true;
                                    LOGGER.info("Released instance of BidibCommunication.");
                                }
                                else {
                                    LOGGER.info("The connected port was not changed.");
                                }

                            }
                        }
                        catch (Exception ex) {
                            LOGGER.warn("Check for changed port failed.", ex);
                        }
                    }
                    if (connectedPort != null && portClosed) {
                        LOGGER.info("Port was closed: {}", connectedPort);
                        fireClosed(connectedPort.toString());
                    }

                    LOGGER.info("Update the configuredPortType: {}", portType);
                    setConfiguredPortType(portType);
                }
            });
    }

    @Override
    public void addCommunicationListener(CommunicationListener listener) {
        communicationListeners.add(listener);
    }

    @Override
    public void removeCommunicationListener(CommunicationListener listener) {
        communicationListeners.remove(listener);
    }

    protected void setConfiguredPortType(PreferencesPortType portType) {
        LOGGER.info("Set the configuredPortType: {}", portType);
        configuredPortType = portType;
    }

    private BidibInterface getBidib() {
        return bidib;
    }

    public boolean isOpened() {
        return (getBidib() != null && getBidib().isOpened());
    }

    @Override
    public void open(PreferencesPortType portType, Context context) {
        LOGGER.info("Open is called with portType: {}", portType);
        if (portType == null) {
            portType = configuredPortType;
        }
        LOGGER.info("Prepare the bidib instance for portType: {}", portType);

        String commPort = portType.getConnectionName();

        // open the comm port and get the magic from the root node
        try {
            if (PreferencesPortType.isSimulation(portType)) {
                LOGGER.info("Prepare simulation instance of bidib.");

                bidib = BidibFactory.createBidib("org.bidib.jbidibc.simulation.comm.SimulationBidib");
            }
            else {
                LOGGER.info("Prepare serial instance of bidib.");
                switch (portType.getConnectionPortType()) {
                    case UdpPort:
                        bidib = BidibFactory.createBidib("org.bidib.jbidibc.net.NetBidib");
                        if (!commPort.startsWith("udp:")) {
                            commPort = "udp:" + commPort;
                        }
                        break;
                    case TcpPort:
                        bidib = BidibFactory.createBidib("org.bidib.jbidibc.net.NetBidib");
                        if (!commPort.startsWith("tcp:")) {
                            commPort = "tcp:" + commPort;
                        }
                        break;
                    case SerialOverTcpPort:
                        bidib = BidibFactory.createBidib("org.bidib.jbidibc.net.serialovertcp.NetBidib");
                        break;
                    case SerialSymLink:
                        String osName = System.getProperty("os.name");
                        if (osName.toLowerCase().indexOf("windows") == -1) {
                            LOGGER.info("Add the symlink as serialPort: {}", commPort);
                            System.setProperty("gnu.io.rxtx.SerialPorts", commPort);

                            context.register("symlink", Boolean.TRUE);
                        }

                    default:
                        String serialPortProvider = Preferences.getInstance().getSerialPortProvider().toLowerCase();
                        LOGGER.info("Selected serial port provider: {}", serialPortProvider);
                        switch (serialPortProvider) {
                            case "scm":
                                bidib = BidibFactory.createBidib("org.bidib.jbidibc.scm.ScmSerialBidib");
                                break;
                            case "spsw":
                                bidib = BidibFactory.createBidib("org.bidib.jbidibc.spsw.SpswSerialBidib");
                                break;
                            default:
                                bidib = BidibFactory.createBidib("org.bidib.jbidibc.rxtx.RxtxSerialBidib");
                                break;
                        }
                        break;
                }
            }

            boolean ignoreWaitTimeout = Preferences.getInstance().isIgnoreWaitTimeout();
            getBidib().setIgnoreWaitTimeout(ignoreWaitTimeout);
            getBidib().setResponseTimeout(responseTimeout);

            int firmwarePacketTimeout = Preferences.getInstance().getFirmwarePacketTimeout();
            getBidib().setFirmwarePacketTimeout(firmwarePacketTimeout);

            String labelPath = Preferences.getInstance().getLabelV2Path();
            context.register("LabelPath", labelPath);

            Boolean useHardwareFlowControl = Preferences.getInstance().isSerialUseHardwareFlowControl();
            context.register("serial.useHardwareFlowControl", useHardwareFlowControl);

            LOGGER.info("Try to open port: {}, bidib: {}", commPort, bidib);

            final ConnectionListener connectionListener = new ConnectionListener() {

                @Override
                public void opened(String port) {
                    LOGGER.info("The port was opened: {}", port);
                }

                @Override
                public void closed(String port) {
                    fireClosed(port);
                }

                @Override
                public void status(String messageKey) {
                    LOGGER.info("Fire the status message with key: {}", messageKey);
                    fireStatusMessage(Resources.getString(BidibCommunication.class, messageKey),
                        StatusBar.DISPLAY_NORMAL);
                }
            };

            // open the port
            getBidib().open(commPort, connectionListener, nodeListeners, messageListeners, transferListeners, context);

            LOGGER.info("The port was opened.");
        }
        catch (NoAnswerException ex) {
            LOGGER.warn("Establish communication with interface failed.", ex);

            fireClosed(commPort);
            LOGGER.info("Rethrow the exception.");
            throw ex;
        }
        catch (PortNotFoundException ex) {
            LOGGER.warn("Open port failed.", ex);
            fireClosed(commPort);
            InvalidConfigurationException icex = new InvalidConfigurationException("Open port failed.", ex);
            throw icex;
        }
        catch (PortNotOpenedException ex) {
            LOGGER.warn("Open port failed.", ex);
            fireClosed(commPort);
            InvalidConfigurationException icex = new InvalidConfigurationException("Open port failed.", ex);
            icex.setReason(ex.getReason());
            throw icex;
        }
        catch (InvalidConfigurationException ex) {
            LOGGER.warn("Open port failed due to invalid configuration.", ex);
            fireClosed(commPort);
            throw ex;
        }
        catch (Exception ex) {
            LOGGER.warn("Open port failed.", ex);
            fireClosed(commPort);
            throw new RuntimeException("Open port failed.", ex);
        }

        this.connectedPortType = portType;

        fireOpened(commPort);

        // // prepare the flags
        // for (int id = 0; id < 16; id++) {
        // Flag flag = new Flag();
        //
        // flag.setId(id);
        // flags.add(flag);
        // }

        try {
            resetRootAndFetchNodes(false, false);
        }
        catch (InternalStartupException ex) {
            LOGGER.warn("Reset root and fetch nodes failed.", ex);
            throw ex;
        }
        catch (Exception ex) {
            LOGGER.warn("Reset root and fetch nodes failed.", ex);
            // fireClosed(commPort);
            throw new RuntimeException("Reset root and fetch nodes failed.", ex);
        }

        fireInitialized();
    }

    private void resetRootAndFetchNodes(boolean performReset, boolean releaseRootNode) {
        LOGGER.info(
            "Reset the root node from Bidib. The connected portType is: {}, resetReconnectDelay: {}, performReset: {}, releaseRootNode: {}",
            connectedPortType, resetReconnectDelay, performReset, releaseRootNode);

        if (performReset) {
            LOGGER.info("Perform reset on the rootNode");
            try {
                getBidib().getRootNode().reset();
            }
            catch (Exception e) {
                LOGGER.warn("Reset root node failed.", e);
                throw new RuntimeException(e);
            }
        }
        else {
            LOGGER.info("Perform reset on the rootNode is skipped.");
        }

        getBidib().getRootNode().setReadNodesPassed(false);

        // clear all nodes
        Node topNode = null;
        synchronized (nodes) {
            topNode = NodeUtils.findNodeByAddress(nodes, RootNode.ROOTNODE_ADDR);
            // LOGGER.info("Clear the node list: {}", nodes);
            // nodes.clear();
        }

        LOGGER.info("Current topNode: {}", topNode);

        if (topNode != null && nodeListeners != null) {
            synchronized (nodeListeners) {
                for (NodeListener nodeListener : nodeListeners) {
                    nodeListener.nodeLost(topNode);
                }
            }
        }

        synchronized (nodes) {
            LOGGER.info("Clear the node list: {}", nodes);
            nodes.clear();
        }

        // clear all cached data
        clearCachedData(null);

        if (releaseRootNode) {
            // release the root node because it could have changed
            LOGGER.info("Release the root node because it could have changed during restart.");
            getBidib().releaseRootNode();
        }

        if (performReset) {
            LOGGER.info("Wait for complete reset of BiDiBus for {}ms", resetReconnectDelay);

            try {
                Thread.sleep(resetReconnectDelay);
            }
            catch (InterruptedException e) {
                LOGGER.warn("Wait for bus reset failed.", e);
            }
            LOGGER.info("Reset BiDiBus passed.");
            fireStatus(Resources.getString(getClass(), "bidib-reset-passed"), StatusBar.DISPLAY_NORMAL);

            try {
                int magic = getBidib().getRootNode().getMagic(1500);

                LOGGER.info("The root node returned magic: {} (0x{})", magic, ByteUtils.magicToHex(magic));
            }
            catch (NoAnswerException ex) {
                LOGGER.warn(
                    "Get magic from root node failed. This indicates a problem in the communication with the master!",
                    ex);

                close();
                fireStatus(Resources.getString(getClass(), "bidib-communication-failure"), StatusBar.DISPLAY_NORMAL);
                return;
            }
            catch (ProtocolException ex) {
                LOGGER.warn(
                    "Get magic from root node failed. This indicates a problem in the communication with the master!",
                    ex);

                close();
                fireStatus(Resources.getString(getClass(), "bidib-communication-failure"), StatusBar.DISPLAY_NORMAL);
                return;
            }
        }
        else {
            LOGGER.info("The reset of BiDiBus was skipped.");

            LOGGER.info("Wait for complete restart of BiDiBus for {}ms", resetReconnectDelay);

            fireStatus(Resources.getString(getClass(), "bidib-wait-restart-rootnode"), StatusBar.DISPLAY_NORMAL);
            try {
                Thread.sleep(resetReconnectDelay);
            }
            catch (InterruptedException e) {
                LOGGER.warn("Wait for bus restart failed.", e);

                // if the thread is interrupted we must return ...
                throw new InternalStartupException("Wait for bus restart was interrupted.");
            }
        }

        // get the root node
        LOGGER.info("Get the root node from Bidib.");
        RootNode rootNode = getBidib().getRootNode();
        LOGGER.info("Fetched rootNode: {}", rootNode);

        fireStatus(Resources.getString(getClass(), "bidib-fetched-root"), StatusBar.DISPLAY_NORMAL);

        LOGGER.info("Before fetchInterfaceNode, nodes: {}", nodes);
        List<Node> nodeList = new LinkedList<>();
        if (rootNode.isBootloaderNode()) {
            // the root node is a bootloader node
            LOGGER.info("Prepare a new node from the bootloader root node.");
            try {
                Node node = new Node(0, rootNode.getAddr(), rootNode.getUniqueId());
                nodeList.add(node);
            }
            catch (ProtocolException ex) {
                LOGGER.error("Get the unique id of the bootloader root node failed.", ex);
            }

            LOGGER.info("The read subNodes from root node has passed.");
            rootNode.setReadNodesPassed(true);
        }
        else {
            fetchInterfaceNodes(rootNode, nodeList);
        }

        LOGGER.info("Fetched nodes: {}", nodeList);

        // get the new master node
        topNode = null;
        synchronized (nodeList) {
            topNode = NodeUtils.findNodeByAddress(nodeList, RootNode.ROOTNODE_ADDR);
        }

        if (topNode != null) {
            LOGGER.info("Found the interface node to signal to the NodeListener: {}", topNode);
            if (nodeListeners != null) {

                synchronized (nodeListeners) {
                    for (NodeListener nodeListener : nodeListeners) {
                        LOGGER.info("Notify the listener about the interface node: {}", nodeListener);
                        nodeListener.nodeNew(topNode);
                    }
                }
            }
            else {
                LOGGER.warn("No nodeListeners registered.");
            }

            fireStatus(Resources.getString(getClass(), "bidib-nodes-loaded"), StatusBar.DISPLAY_NORMAL);
        }
        else {
            LOGGER.warn("Loading the interface node failed.");

            fireStatus(Resources.getString(getClass(), "bidib-load-nodes-failed"), StatusBar.DISPLAY_ERROR);
        }

    }

    @Override
    public void close() {

        // the comm port has changed
        LOGGER.info("Close the Bidib port.");
        try {
            if (bidib != null) {
                bidib.close();
                bidib = null;
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Close Bidib instance failed.", ex);
        }

        try {
            dccAccessoryWorker.shutdown();
        }
        catch (Exception ex) {
            LOGGER.warn("Shutdown accessory worker failed.", ex);
        }

        // notify the listeners that the port is closed
        fireClosed((connectedPortType != null ? connectedPortType.getConnectionName() : null));

        // clear the listeners
        clearListeners();

        // clear all cached data
        clearCachedData(null);
    }

    private void clearListeners() {
        messageListeners.clear();
        transferListeners.clear();
        communicationListeners.clear();
        synchronized (nodeListeners) {
            nodeListeners.clear();
        }
    }

    private void fetchInterfaceNodes(RootNode rootNode, Collection<Node> nodeList) {
        LOGGER.info("Fetch the subnodes of the interface node, rootNode: {}", rootNode);

        int maxRetry = 3;
        int retry = 0;
        do {
            try {
                // get the subnodes of the root node (interface)
                int count = rootNode.getNodeCount();
                LOGGER.info("Fetch subNodes of interface node: {}, subnodesCount: {}", rootNode, count);

                if (count < 1 && retry < maxRetry) {
                    retry++;
                    LOGGER.warn(
                        "The subnodes of the interface node are not available yet. Wait 1500 ms and try again. Current retry: {}",
                        retry);
                    Thread.sleep(1500);
                    continue;
                }
                retry = maxRetry + 1;

                // iterate over the subnodes and register the subnodes in the cache
                for (int index = 1; index <= count; index++) {
                    Node node = rootNode.getNextNode();
                    LOGGER.info("Process current new node: {}", node);
                    if (node != null && Arrays.equals(node.getAddr(), RootNode.ROOTNODE_ADDR)) {
                        LOGGER.debug("Found interface node, add node to nodeList: {}", node);
                        nodeList.add(node);

                        LOGGER.debug("Get magic for root node: {}", node);
                        int magic = getBidib().getNode(node).getMagic(null);
                        LOGGER.info("Magic of root node: {}, node: {}", magic, node);

                        break;
                    }
                    else {
                        LOGGER.debug("Node is already in list: {}", node);
                    }
                }
            }
            catch (Exception e) {
                LOGGER.error("Iterate over subnodes of rootNode from Bidib failed.", e);
                throw new RuntimeException(e);
            }
        }
        while (retry < maxRetry);

        LOGGER.info("The read subNodes from root node has passed.");
        rootNode.setReadNodesPassed(true);
    }

    private void fetchSubNodes(BidibNode parent, Collection<Node> nodeList) {
        LOGGER.info("Fetch subnodes of the parent: {}", parent);

        List<Node> newNodes = new LinkedList<Node>();
        int maxRetry = 3;
        int retry = 0;
        do {
            try {
                // get the subnodes of the root node (interface)
                int count = parent.getNodeCount();
                LOGGER.info("Fetch subNodes of node: {}, subnodesCount: {}", parent, count);

                if (count < 1 && retry < maxRetry) {
                    retry++;
                    LOGGER.warn("The subnodes are not available yet. Wait 1500 ms and try again. Current retry: {}",
                        retry);
                    Thread.sleep(1500);
                    continue;
                }
                retry = maxRetry + 1;

                // iterate over the subnodes and register the subnodes in the cache
                for (int index = 1; index <= count; index++) {
                    Node node = parent.getNextNode();
                    LOGGER.info("Process current new node: {}", node);
                    if (node != null && !nodeList.contains(node)) {
                        LOGGER.debug("Add new node to nodeList: {}", node);
                        nodeList.add(node);
                        newNodes.add(node);

                        LOGGER.debug("Get magic for new node: {}", node);

                        int magic = getBidib().getNode(node).getMagic(null);
                        LOGGER.info("Magic of new node: {}, node: {}", magic, node);
                    }
                    else {
                        LOGGER.info("New node is already in list: {}", node);
                    }
                }
            }
            catch (Exception e) {
                LOGGER.error("Get nodes from Bidib failed.", e);
                throw new RuntimeException(e);
            }
        }
        while (retry < maxRetry);

        // fetch the subnodes of the new nodes
        for (Node newNode : newNodes) {
            if (NodeUtils.hasSubNodesFunctions(newNode.getUniqueId())) {
                LOGGER.info("Found node with subnodes function, addr: {}, parent.addr: {}", newNode.getAddr(),
                    parent.getAddr());
                if (!Arrays.equals(parent.getAddr(), newNode.getAddr())) {
                    BidibNode newBidibNode = getBidib().getNode(newNode);
                    fetchSubNodes(newBidibNode, nodeList);
                }
                else {
                    LOGGER.debug("Don't fetch subnodes of provided parent again.");
                }
            }
        }
        LOGGER.debug("fetchSubNodes, current nodeList: {}", nodeList);
    }

    @Override
    public void registerNode(Node node) {
        synchronized (nodes) {
            LOGGER.info("Register new node: {}", node);

            if (!nodes.contains(node)) {
                nodes.add(node);
            }
            else {
                LOGGER.warn("The new node is already registered.");
            }
        }
    }

    @Override
    public boolean isNodeRegistered(Node node) {
        synchronized (nodes) {
            LOGGER.debug("Check if node is registered: {}", node);

            return nodes.contains(node);
        }
    }

    /**
     * Remove a node from the list of nodes.
     * 
     * @param node
     *            the node to remove
     */
    @Override
    public void removeNode(Node node) {
        synchronized (nodes) {
            LOGGER.info("Remove node: {}", node);

            // if a hub node is removed then we must remove all children of this hub
            List<Node> nodesToRemove = new LinkedList<Node>();
            nodesToRemove.add(node);

            if (NodeUtils.hasSubNodesFunctions(node.getUniqueId())) {
                byte[] addr = node.getAddr();
                LOGGER.info(
                    "The removed node has subnode functions. We must remove all subnodes, too. Address of current node: {}",
                    addr);
                if (addr != null && addr.length > 0) {
                    for (Node currentNode : nodes) {
                        LOGGER.debug("Check if we must remove the current node: {}", currentNode);
                        byte[] currentAddr = currentNode.getAddr();
                        if (currentAddr.length > addr.length) {
                            // potential subnode
                            if (currentAddr[addr.length - 1] == addr[addr.length - 1]) {
                                // this is a subnode
                                LOGGER.info("Found a subnode to be removed: {}", currentNode);
                                nodesToRemove.add(currentNode);
                            }
                        }
                    }
                }
            }

            LOGGER.info("Currently registered nodes: {}", nodes);
            if (nodes.removeAll(nodesToRemove)) {
                LOGGER.debug("Removed all nodes that must be removed: {}", nodesToRemove);
            }
            else {
                LOGGER.warn("Remove nodes from nodes list failed: {}", nodesToRemove);
            }

            LOGGER.info("Remaining nodes in list: {}", nodes);
        }
    }

    @Override
    public void clearCachedData(Node node) {

        if (node != null) {
            LOGGER.info("Clear all cached data of node: {}", node);

            synchronized (configurationVariables) {
                configurationVariables.remove(node.getUniqueId());
            }
            synchronized (accessories) {
                accessories.remove(node.getUniqueId());
            }
            synchronized (analogPorts) {
                analogPorts.remove(node.getUniqueId());
            }
            synchronized (feedbackPorts) {
                feedbackPorts.remove(node.getUniqueId());
            }
            synchronized (inputPorts) {
                inputPorts.remove(node.getUniqueId());
            }
            synchronized (lightPorts) {
                lightPorts.remove(node.getUniqueId());
            }
            synchronized (backlightPorts) {
                backlightPorts.remove(node.getUniqueId());
            }
            synchronized (macros) {
                macros.remove(node.getUniqueId());
            }
            synchronized (motorPorts) {
                motorPorts.remove(node.getUniqueId());
            }
            synchronized (servoPorts) {
                servoPorts.remove(node.getUniqueId());
            }
            synchronized (soundPorts) {
                soundPorts.remove(node.getUniqueId());
            }
            synchronized (switchPorts) {
                switchPorts.remove(node.getUniqueId());
            }
        }
        else {
            LOGGER.info("Clear all cached data.");

            synchronized (configurationVariables) {
                configurationVariables.clear();
            }
            synchronized (accessories) {
                accessories.clear();
            }
            synchronized (analogPorts) {
                analogPorts.clear();
            }
            synchronized (feedbackPorts) {
                feedbackPorts.clear();
            }
            synchronized (inputPorts) {
                inputPorts.clear();
            }
            synchronized (lightPorts) {
                lightPorts.clear();
            }
            synchronized (backlightPorts) {
                backlightPorts.clear();
            }
            synchronized (macros) {
                macros.clear();
            }
            synchronized (motorPorts) {
                motorPorts.clear();
            }
            synchronized (servoPorts) {
                servoPorts.clear();
            }
            synchronized (soundPorts) {
                soundPorts.clear();
            }
            synchronized (switchPorts) {
                switchPorts.clear();
            }
        }
    }

    public PreferencesPortType getConnectedPortType() {
        return connectedPortType;
    }

    @Override
    public List<Feature> getFeatures(Node node, boolean discardCache) {
        LOGGER.info("Get the features from the node: {}, discardCache: {}", node, discardCache);

        if (discardCache) {
            // clear all features
            node.setFeatures(null);
        }

        List<Feature> featureList = new LinkedList<Feature>();
        List<Feature> features = node.getFeatures();

        if (CollectionUtils.isEmpty(features)) {
            // no features stored in node, get the features from the node

            try {
                BidibNode bidibNode = getBidib().getNode(node);
                int numFeatures = bidibNode.getFeatureCount();
                LOGGER.info("Get the number of features: {}", numFeatures);
                if (numFeatures > 0) {
                    features = bidibNode.getFeaturesBulk(numFeatures);

                    if (CollectionUtils.isNotEmpty(features)) {
                        node.setFeatures(features);

                        featureList.addAll(features);

                        Feature secureAckEnabled =
                            Feature.findFeature(features, FeatureEnum.FEATURE_BM_SECACK_ON.getType());
                        if (secureAckEnabled != null) {
                            LOGGER.info("The node has the FEATURE_BM_SECACK_ON and the value is: {}",
                                secureAckEnabled.getValue());
                            if (secureAckEnabled.getValue() > 0) {
                                bidibNode.setSecureAckEnabled(true);
                            }
                        }
                    }
                    else {
                        LOGGER.warn("No features available! Current node: {}", node);
                    }
                }
            }
            catch (Exception ex) {
                LOGGER.warn("Get the features from the noide failed.", ex);
                throw new RuntimeException(ex);
            }
        }
        else {
            LOGGER.info("Return cached features from node.");
            featureList.addAll(features);
        }

        return featureList;
    }

    @Override
    public void writeFeature(final Node node, final Feature feature) {

        try {
            BidibNode bidibNode = getBidib().getNode(node);
            Feature result = bidibNode.setFeature(feature.getType(), feature.getValue());

            node.setFeature(result);
        }
        catch (Exception ex) {
            LOGGER.warn("Write new feature value to the node failed.", ex);
            throw new RuntimeException(ex);
        }

        // some features force internal data to be updated
        // e.g. BiDiB-S88-Brigde allows to configure the number feedback ports
        try {
            switch (FeatureEnum.valueOf((byte) feature.getType())) {
                case FEATURE_BM_SIZE:
                    // clear number of feedback ports
                    LOGGER.debug("remove cached feedback ports for node: {}", node);
                    feedbackPorts.remove(node.getUniqueId());
                    break;
                default:
                    break;
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Update internal data with new feature value failed.", ex);
        }
    }

    @Override
    public void activateAnalogPort(Node node, int port, AnalogPortStatus status) {
        try {
            getBidib().getNode(node).setOutput(PortModelEnum.getPortModel(node), LcOutputType.ANALOGPORT, port,
                status.getType().getType());
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void activateLightPort(Node node, int port, LightPortStatus status) {
        try {
            getBidib().getNode(node).setOutput(PortModelEnum.getPortModel(node), LcOutputType.LIGHTPORT, port,
                status.getType().getType());
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void activateBacklightPort(Node node, BacklightPort port, int value) {

        // store the config values
        int dimSlopeDown = port.getDimSlopeDown();
        int dimSlopeUp = port.getDimSlopeUp();
        int dmxMapping = port.getDmxMapping();

        LOGGER.debug(
            "activate backlight port '{}' on node '{}' with value: {}, dimSlopeDown: {}, dimSlopeUp: {}, dmxMapping: {}",
            port, node, value, dimSlopeDown, dimSlopeUp, dmxMapping);

        boolean isAdjusting = port.isAdjusting();

        if (isAdjusting) {

            // change the port config to have immediate output change
            try {
                LOGGER.info("Change the configuration of the backlight port to immediate output change!");
                setBacklightPortParameters(node, port, BacklightPort.DIMM_STRETCHMAX8_8,
                    BacklightPort.DIMM_STRETCHMAX8_8, dmxMapping);
            }
            catch (Exception e) {
                LOGGER.warn("Change configuration of backlight port failed.", e);
            }

            try {
                getBidib().getNode(node).setOutput(PortModelEnum.getPortModel(node), LcOutputType.BACKLIGHTPORT,
                    port.getId(), value);
            }
            catch (Exception e) {
                LOGGER.warn("Activate backlight port failed.", e);
                throw new RuntimeException(e);
            }
            finally {
                // restore the original configuration values
                try {
                    LOGGER.info("Restore the original configuration of the backlight port!");
                    setBacklightPortParameters(node, port, dimSlopeDown, dimSlopeUp, dmxMapping);
                }
                catch (Exception e) {
                    LOGGER.warn("Change configuration of backlight port failed.", e);
                }
            }
        }
        else {
            if (port.getPreAdjustingValue() > -1) {
                LOGGER.info("Restore the pre-adjusting value: {}", port.getPreAdjustingValue());

                // change the port config to have immediate output change
                try {
                    LOGGER.info("Change the configuration of the backlight port to immediate output change!");
                    setBacklightPortParameters(node, port, BacklightPort.DIMM_STRETCHMAX8_8,
                        BacklightPort.DIMM_STRETCHMAX8_8, dmxMapping);
                }
                catch (Exception e) {
                    LOGGER.warn("Change configuration of backlight port failed.", e);
                }

                try {
                    // keep the pre-adjusting value
                    int preAdjustingValue = port.getPreAdjustingValue();

                    getBidib().getNode(node).setOutput(PortModelEnum.getPortModel(node), LcOutputType.BACKLIGHTPORT,
                        port.getId(), preAdjustingValue);
                }
                catch (Exception e) {
                    LOGGER.warn("Activate backlight port failed.", e);
                    throw new RuntimeException(e);
                }
                finally {
                    // // reset the pre-adjusting value
                    // port.setPreAdjustingValue(-1);

                    // restore the original configuration values
                    try {
                        LOGGER.info("Restore the original configuration of the backlight port!");
                        setBacklightPortParameters(node, port, dimSlopeDown, dimSlopeUp, dmxMapping);
                    }
                    catch (Exception e) {
                        LOGGER.warn("Change configuration of backlight port failed.", e);
                    }
                }
            }

            LOGGER.info("Set the new value: {}", value);
            try {
                getBidib().getNode(node).setOutput(PortModelEnum.getPortModel(node), LcOutputType.BACKLIGHTPORT,
                    port.getId(), value);
            }
            catch (Exception e) {
                LOGGER.warn("Activate backlight port failed.", e);
                throw new RuntimeException(e);
            }
            finally {
                // reset the pre-adjusting value
                port.setPreAdjustingValue(-1);
            }
        }
    }

    @Override
    public void activateMotorPort(Node node, int port, int speed) {
        try {
            int value = convertSpeed(speed);

            LOGGER.info("Prepared value for motor port: {}", ByteUtils.byteToHex(value));
            getBidib().getNode(node).setOutput(PortModelEnum.getPortModel(node), LcOutputType.MOTORPORT, port, value);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static int convertSpeed(int speed) {
        int result = 0;

        DirectionEnum direction = (speed > 0 ? DirectionEnum.FORWARD : DirectionEnum.BACKWARD);
        LOGGER.info("Convert speed: {}, direction: {}", speed, direction);

        // don't care about emergency stop (=> FS1)
        if (speed == 0) {
            // no change
            result = speed;
        }
        else if (speed > 0) {
            // forward
            result = speed + 1; // +1 to skip emergency stop
        }
        else {
            // backward
            speed = -(speed - 1);
            result = speed;
        }

        result = result & 0x7F;

        if (direction == DirectionEnum.FORWARD) {

            result |= 0x80;
        }

        LOGGER.info("Converted speed value: {}", result);

        return result;
    }

    @Override
    public void activateServoPort(Node node, int port, int value) {
        try {
            getBidib().getNode(node).setOutput(PortModelEnum.getPortModel(node), LcOutputType.SERVOPORT, port, value);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void activateSoundPort(Node node, int port, SoundPortStatus status) {
        try {
            getBidib().getNode(node).setOutput(PortModelEnum.getPortModel(node), LcOutputType.SOUNDPORT, port,
                status.getType().getType());
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void activateSwitchPort(Node node, int port, SwitchPortStatus status) {
        try {
            getBidib().getNode(node).setOutput(PortModelEnum.getPortModel(node), LcOutputType.SWITCHPORT, port,
                status.getType().getType());
        }
        catch (Exception ex) {
            LOGGER.warn("Activate switch port failed.", ex);
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void activateSwitchPairPort(Node node, int port, SwitchPortStatus status) {
        try {
            getBidib().getNode(node).setOutput(PortModelEnum.getPortModel(node), LcOutputType.SWITCHPAIRPORT, port,
                status.getType().getType());
        }
        catch (Exception ex) {
            LOGGER.warn("Activate switchPair port failed.", ex);
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void boosterOff(Node node) {
        LOGGER.info("Switch command station and booster off! Selected node: {}", node);
        try {
            // switch off command station and booster
            byte broadcast = BoostOnMessage.BROADCAST_OFF;
            if (NodeUtils.hasCommandStationFunctions(node.getUniqueId())) {
                broadcast = BoostOnMessage.BROADCAST_MESSAGE;

                // switch command station off
                getBidib().getCommandStationNode(node).setState(CommandStationState.OFF);
            }

            // if performed on command station we broadcast this message ...
            getBidib().getBoosterNode(node).boosterOff(broadcast);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void boosterOn(Node node) {
        LOGGER.info("Switch command station and booster on! Selected node: {}", node);
        try {
            // // make sure all locos are removed from loco buffer
            // if (NodeUtils.hasCommandStationFunctions(node.getUniqueId())) {
            // LOGGER.info("The current node is a command station!");
            //
            // DriveAcknowledge acknowledge = getBidib().getCommandStationNode(node).releaseLoco(0);
            // LOGGER.info("Clear locobuffer returned: {}", acknowledge);
            //
            // // get the current watchdog interval
            // Feature watchdogInterval =
            // getBidib().getNode(node).getFeature(FeatureEnum.FEATURE_GEN_WATCHDOG.getType());
            // LOGGER.info("Configured watchdog feature: {}", watchdogInterval);
            //
            // // try to start the command station with ignore watchdog
            // CommandStationState commandStationState =
            // getBidib().getCommandStationNode(node).setState(CommandStationState.GO_IGN_WD);
            // LOGGER.info("New commandStationState after send GO_IGN_WD: {}", commandStationState);
            // if (CommandStationState.OFF.equals(commandStationState)) {
            // // no watchdog support available
            // commandStationState = getBidib().getCommandStationNode(node).setState(CommandStationState.GO);
            // LOGGER.info("New commandStationState after send GO: {}", commandStationState);
            // }
            // }
            // else {
            // LOGGER.info("The current node is not a command station! No CommandStation state GO message is sent.");
            // }

            // if performed on command station we broadcast this message ...
            byte broadcast = BoostOnMessage.BROADCAST_OFF;
            if (NodeUtils.hasCommandStationFunctions(node.getUniqueId())) {
                broadcast = BoostOnMessage.BROADCAST_MESSAGE;
            }
            getBidib().getBoosterNode(node).boosterOn(broadcast);
        }
        catch (Exception e) {
            LOGGER.warn("Switch booster on failed.");
            throw new RuntimeException("Switch booster on failed.", e);
        }
    }

    @Override
    public void boosterQuery(Node node) {
        try {
            getBidib().getBoosterNode(node).boosterQuery();
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void setCommandStationState(Node node, CommandStationState commandStationState) {
        LOGGER.info("Set the command station to the new state: {}", commandStationState);

        if (NodeUtils.hasCommandStationFunctions(node.getUniqueId())) {
            LOGGER.info("The current node is a command station!");

            try {
                commandStationState = getBidib().getCommandStationNode(node).setState(commandStationState);
            }
            catch (ProtocolException ex) {
                LOGGER.warn("Set the new commandStationState failed.", ex);
                throw new RuntimeException("Set the new commandStationState failed.", ex);
            }
            catch (Exception ex) {
                LOGGER.warn("Set the new commandStationState failed.", ex);
                throw new RuntimeException("Set the new commandStationState failed.", ex);
            }
        }
        else {
            LOGGER.error("The current node is not a command station!");
        }
    }

    @Override
    public CommandStationState queryCommandStationState(Node node) {
        LOGGER.info("Query the command station state.");

        if (NodeUtils.hasCommandStationFunctions(node.getUniqueId())) {
            LOGGER.info("The current node is a command station!");

            try {
                CommandStationState commandStationState = getBidib().getCommandStationNode(node).queryState();
                return commandStationState;
            }
            catch (ProtocolException ex) {
                LOGGER.warn("Get the current commandStationState failed.", ex);
            }
        }
        else {
            LOGGER.error("The current node is not a command station!");
        }
        return null;
    }

    @Override
    public void queryCommandStationValue(Node node, CsQueryTypeEnum csQueryValue, Integer address) {
        LOGGER.info("Query the command station value: {}, address: {}", csQueryValue, address);

        if (NodeUtils.hasCommandStationFunctions(node.getUniqueId())) {
            LOGGER.info("The current node is a command station!");

            try {
                getBidib().getCommandStationNode(node).queryValue(csQueryValue, address);
            }
            catch (ProtocolException ex) {
                LOGGER.warn("Get the current commandStation value failed.", ex);
            }
        }
        else {
            LOGGER.error("The current node is not a command station!");
        }
    }

    @Override
    public BoosterState queryBoosterState(Node node) {
        LOGGER.info("Query the booster state.");

        if (NodeUtils.hasBoosterFunctions(node.getUniqueId())) {
            LOGGER.info("The current node is a booster!");

            try {
                BoosterState boosterState = getBidib().getBoosterNode(node).queryState();
                LOGGER.info("Return the current booster state: {}", boosterState);
                return boosterState;
            }
            catch (Exception ex) {
                LOGGER.warn("Get the current boosterState failed.", ex);
            }
        }
        else {
            LOGGER.error("The current node is not a booster!");
        }
        return null;
    }

    @Override
    public void disable(Node node) {
        try {
            if (node != null) {
                getBidib().getNode(node).sysDisable();
            }
            else {
                getBidib().getRootNode().sysDisable();
            }
        }
        catch (Exception e) {
            LOGGER.warn("Disable node failed.", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void enable(Node node) {
        if (node != null) {
            BidibNode bidibNode = getBidib().getNode(node);
            if (bidibNode != null) {
                try {
                    bidibNode.sysEnable();
                }
                catch (ProtocolException ex) {
                    LOGGER.warn("Enable node failed, node: {}", node, ex);
                    throw new RuntimeException(ex);
                }
            }
            else {
                LOGGER.warn("Enable skipped because the node is not available: {}", bidibNode);
                throw new IllegalArgumentException("The requested node is not available");
            }
        }
        else {
            try {
                getBidib().getRootNode().sysEnable();
            }
            catch (Exception e) {
                LOGGER.warn("Enable node failed.", e);
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public List<Accessory> getAccessories(Node node) {
        synchronized (accessories) {
            List<Accessory> accessoryList = accessories.get(node.getUniqueId());

            if (accessoryList == null) {
                accessoryList = new LinkedList<Accessory>();
                accessories.put(node.getUniqueId(), accessoryList);
                try {
                    AccessoryNode accessoryNode = getBidib().getAccessoryNode(node);

                    if (accessoryNode != null) {
                        Feature accessoryCount =
                            Feature.findFeature(node.getFeatures(), BidibLibrary.FEATURE_ACCESSORY_COUNT);
                        // node.setFeature(accessoryCount);
                        LOGGER.info("The accessory count: {}", accessoryCount);

                        if (accessoryCount != null && accessoryCount.getValue() > 0) {
                            int accessoryMacroMapped = 0;

                            // get the number of aspects that can be applied to an accessory
                            Feature featureAccessoryMacroMapped =
                                Feature.findFeature(node.getFeatures(), BidibLibrary.FEATURE_ACCESSORY_MACROMAPPED);

                            if (featureAccessoryMacroMapped != null) {
                                accessoryMacroMapped = featureAccessoryMacroMapped.getValue();
                            }
                            LOGGER.info("Current value of accessoryMacroMapped: {}", accessoryMacroMapped);

                            for (int accessoryNumber = 0; accessoryNumber < accessoryCount
                                .getValue(); accessoryNumber++) {
                                Accessory accessory = new Accessory();
                                accessory.setId(accessoryNumber);
                                accessory.setMacroMapped(accessoryMacroMapped > 0);

                                // get the content of the accessory
                                getAccessoryContent(node, accessoryNode, accessory, accessoryMacroMapped);

                                accessoryList.add(accessory);
                            }
                        }
                        else {
                            LOGGER.info("The current node has no accessories: {}", node);
                        }
                    }
                }
                catch (NoAnswerException e) {
                    LOGGER.warn("Get accessories failed.", e);
                    throw e;
                }
                catch (Exception e) {
                    LOGGER.warn("Get accessories failed.", e);
                    throw new RuntimeException(e);
                }
            }
            return Collections.unmodifiableList(accessoryList);
        }
    }

    /**
     * Get the content of the accessory.
     * 
     * @param accessoryNode
     *            the node
     * @param accessory
     *            the accessory
     * @param accessoryMacroMapped
     *            the number of aspects that can be applied to the accessory
     */
    private void getAccessoryContent(
        final Node node, AccessoryNode accessoryNode, Accessory accessory, int accessoryMacroMapped) {

        try {
            if (accessoryNode != null) {

                accessory.setMacroSize(accessoryMacroMapped);

                // evaluate the accessory startup
                // TODO this is not very good design because we only get the values
                try {
                    byte[] parameterStartup =
                        accessoryNode.getAccessoryParameter(accessory.getId(),
                            BidibLibrary.BIDIB_ACCESSORY_PARA_STARTUP);

                    if (parameterStartup != null) {
                        LOGGER.info("Current accessory: {}, startup: {}", accessory,
                            ByteUtils.bytesToHex(parameterStartup));

                        accessory.setStartupState(ByteUtils.getInt(parameterStartup[0]));
                    }
                    else {
                        LOGGER.debug("No startup state available for accessory: {}", accessory);
                        accessory.setStartupState(null);
                    }
                }
                catch (ProtocolInvalidParamException ex) {
                    LOGGER.warn("Get the accessory startup param failed.");
                }

                // evaluate the accessory switch time
                try {
                    byte[] parameterSwitchTime =
                        accessoryNode.getAccessoryParameter(accessory.getId(),
                            BidibLibrary.BIDIB_ACCESSORY_SWITCH_TIME);

                    if (parameterSwitchTime != null) {
                        LOGGER.info("Current accessory: {}, switch time: {}", accessory,
                            ByteUtils.bytesToHex(parameterSwitchTime));

                        accessory.setSwitchTime(ByteUtils.getInt(parameterSwitchTime[0], 0x7F));
                        accessory
                            .setTimeBaseUnit(TimeBaseUnitEnum.valueOf(ByteUtils.getBit(parameterSwitchTime[0], 7)));
                    }
                    else {
                        LOGGER.debug("No switch time available for accessory: {}", accessory);
                        accessory.setSwitchTime(null);
                        accessory.setTimeBaseUnit(null);
                    }
                }
                catch (ProtocolInvalidParamException ex) {
                    LOGGER.warn("Get the accessory switch time param failed.");
                }

                if (accessoryMacroMapped > 0) {

                    try {
                        byte[] parameter =
                            accessoryNode.getAccessoryParameter(accessory.getId(),
                                BidibLibrary.BIDIB_ACCESSORY_PARA_MACROMAP);

                        if (parameter != null) {
                            List<Macro> macroList = internalGetMacros(node, accessoryNode);

                            accessory.clearAspects();
                            for (int aspectNumber = 0; aspectNumber < parameter.length; aspectNumber++) {
                                byte macroNumber = parameter[aspectNumber];

                                if (macroNumber >= 0 && macroNumber < macroList.size()
                                    && macroNumber != AccessoryParaResponse.UNKNOWN_PARA) {
                                    accessory
                                        .addAspect(new MacroRef(macroList.get(ByteUtils.getInt(macroNumber)).getId()));
                                }
                            }
                        }
                    }
                    catch (ProtocolInvalidParamException ex) {
                        LOGGER.warn("Get the accessory param macromap param failed.");
                    }
                }

                accessory.setAccessorySaveState(AccessorySaveState.PERMANENTLY_STORED_ON_NODE);
            }
        }
        // TODO why was this caught here?
        catch (ProtocolNoAnswerException e) {
            LOGGER.warn("Get accessory content failed.", e);
            throw new NoAnswerException("Get accessory content failed.", e);
        }
        catch (Exception e) {
            LOGGER.warn("Get accessory content failed.", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public int getAccessoryLength(Node node) {
        int result = 0;

        try {
            AccessoryNode accessoryNode = getBidib().getAccessoryNode(node);

            if (accessoryNode != null) {
                Feature feature = Feature.findFeature(node.getFeatures(), BidibLibrary.FEATURE_ACCESSORY_MACROMAPPED);
                if (feature != null) {
                    // node.setFeature(feature);
                    result = feature.getValue();
                }
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Get FEATURE_ACCESSORY_MACROMAPPED from node failed.", ex);
            throw new RuntimeException(ex);
        }
        return result;
    }

    @Override
    public List<AnalogPort> getAnalogPorts(Node node) {
        synchronized (analogPorts) {
            List<AnalogPort> ports = analogPorts.get(node.getUniqueId());

            if (ports == null) {
                ports = new LinkedList<AnalogPort>();
                analogPorts.put(node.getUniqueId(), ports);
                try {
                    BidibNode bidibNode = getBidib().getNode(node);

                    if (bidibNode != null) {
                        Feature feature =
                            Feature.findFeature(node.getFeatures(), BidibLibrary.FEATURE_CTRL_ANALOGOUT_COUNT);

                        if (feature != null) {
                            // node.setFeature(feature);
                            for (int index = 0; index < feature.getValue(); index++) {
                                AnalogPort port = new AnalogPort();

                                port.setId(index);
                                ports.add(port);
                            }
                        }
                    }
                }
                catch (Exception e) {
                    LOGGER.warn("Get analog ports from node failed.", e);
                    throw new RuntimeException(e);
                }
            }
            return Collections.unmodifiableList(ports);
        }
    }

    @Override
    public int getBoosterMaximumCurrent(Node node) {
        int result = 0;

        try {
            Feature feature = Feature.findFeature(node.getFeatures(), BidibLibrary.FEATURE_BST_AMPERE);

            if (feature != null) {
                // node.setFeature(feature);

                result = MessageUtils.convertCurrent(feature.getValue());

                LOGGER.info("Fetched the maximum current value from the booster: {} mA, uniqueId: {}", result,
                    NodeUtils.getUniqueIdAsString(node.getUniqueId()));
            }
            else {
                LOGGER.info("The feature FEATURE_BST_AMPERE is not available.");
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Get FEATURE_BST_AMPERE from node failed.", ex);
            throw new NoAnswerException("Get FEATURE_BST_AMPERE from node failed.", ex);
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<FeedbackPort> getFeedbackPorts(Node node) {
        synchronized (feedbackPorts) {
            List<FeedbackPort> ports = feedbackPorts.get(node.getUniqueId());

            if (ports == null) {
                ports = new LinkedList<FeedbackPort>();
                feedbackPorts.put(node.getUniqueId(), ports);
                try {
                    BidibNode bidibNode = getBidib().getNode(node);

                    if (bidibNode != null) {
                        // get the number of feedback ports from the system
                        Feature feature = Feature.findFeature(node.getFeatures(), BidibLibrary.FEATURE_BM_SIZE);

                        if (feature != null) {
                            LOGGER.info("Number of feedback ports: {}", feature.getValue());
                            // set the feature
                            // node.setFeature(feature);

                            for (int index = 0; index < feature.getValue(); index++) {
                                FeedbackPort port = new FeedbackPort();

                                port.setId(index);
                                ports.add(port);
                            }
                        }
                    }
                    else {
                        LOGGER.warn("Requested node is not available in system.");
                    }
                }
                catch (Exception e) {
                    LOGGER.warn("Get requested node from system failed: {}", node, e);
                    throw new RuntimeException(e);
                }
            }
            return Collections.unmodifiableList(ports);
        }
    }

    @Override
    public void getFeedbackPortStatus(Node node, int begin, int end) {
        LOGGER.info("Query the state and addresses of feedback ports from node: {}, begin: {}, end: {}", node, begin,
            end);
        try {
            BidibNode bidibNode = getBidib().getNode(node);

            bidibNode.getFeedbackState(begin, end);

            // check if the feature FEATURE_BM_ADDR_DETECT_AVAILABLE is set
            Feature featureAddrDetectAvailable =
                Feature.findFeature(node.getFeatures(), BidibLibrary.FEATURE_BM_ADDR_DETECT_AVAILABLE);

            if (featureAddrDetectAvailable != null && featureAddrDetectAvailable.getValue() > 0) {
                bidibNode.getAddressState(begin, end);
            }
            else {
                LOGGER.info("The node does not support address messages on feedback ports.");
            }

            bidibNode.getConfidence();
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    // @Override
    // public List<Flag> getFlags() {
    // synchronized (flags) {
    // return Collections.unmodifiableList(flags);
    // }
    // }

    @Override
    public List<InputPort> getInputPorts(Node node) {
        synchronized (inputPorts) {
            List<InputPort> ports = inputPorts.get(node.getUniqueId());

            if (ports == null) {
                ports = new LinkedList<InputPort>();
                inputPorts.put(node.getUniqueId(), ports);
                try {
                    BidibNode bidibNode = getBidib().getNode(node);

                    if (bidibNode != null) {
                        Feature feature =
                            Feature.findFeature(node.getFeatures(), BidibLibrary.FEATURE_CTRL_INPUT_COUNT);

                        if (feature != null) {
                            LOGGER.info("Found number of input ports: {}", feature.getValue());
                            // node.setFeature(feature);

                            if (node.getProtocolVersion().isHigherThan(ProtocolVersion.VERSION_0_5)) {
                                for (int keyNumber = 0; keyNumber < feature.getValue(); keyNumber++) {
                                    InputPort port = new InputPort();

                                    port.setId(keyNumber);
                                    ports.add(port);
                                }
                            }
                            else {
                                for (int keyNumber = 0; keyNumber < feature.getValue(); keyNumber++) {
                                    InputPort port = new InputPort();

                                    port.setId(keyNumber);
                                    ports.add(port);
                                }
                            }
                        }
                    }
                    else {
                        LOGGER.warn("BidibNode is not available for node: {}", node);
                    }
                }
                catch (Exception e) {
                    LOGGER.warn("Get the input ports from the node failed.", e);
                    throw new RuntimeException(e);
                }
            }
            return Collections.unmodifiableList(ports);
        }
    }

    @Override
    public void queryInputPortConfig(Node node) {
        LOGGER.info("Query the input port config for node: {}", node);
        if (node != null && node.getProtocolVersion().isHigherThan(ProtocolVersion.VERSION_0_5)) {
            LOGGER.info("Query input port config for protocol version > 0.5");
            try {
                BidibNode bidibNode = getBidib().getNode(node);

                List<InputPort> ports = inputPorts.get(node.getUniqueId());

                if (CollectionUtils.isNotEmpty(ports)) {
                    int portCount = ports.size();

                    int[] outputNumbers = new int[portCount];
                    for (int index = 0; index < portCount; index++) {
                        outputNumbers[index] = index;
                    }
                    LOGGER.info("Prepared outputNumbers: {}", outputNumbers);
                    bidibNode.getConfigXBulk(PortModelEnum.getPortModel(node), LcOutputType.INPUTPORT,
                        1 /* window size */, outputNumbers);
                }
                else {
                    LOGGER.info("No input ports on node.");
                }
            }
            catch (Exception ex) {
                LOGGER.warn("Query input port config with bulk failed.", ex);
                throw new RuntimeException(ex);
            }
        }
    }

    @Override
    public void getInputPortStatus(Node node, List<Integer> portIds) {
        try {
            getBidib().getNode(node).getKeyState(portIds);
        }
        catch (Exception e) {
            LOGGER.warn("Get status of input ports failed.", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void queryLightPortConfig(Node node) {
        LOGGER.info("Query the light port config for node: {}", node);
        if (node != null) {

            try {
                BidibNode bidibNode = getBidib().getNode(node);

                List<LightPort> ports = lightPorts.get(node.getUniqueId());

                if (CollectionUtils.isNotEmpty(ports)) {
                    int portCount = ports.size();

                    int[] outputNumbers = new int[portCount];
                    for (int index = 0; index < portCount; index++) {
                        outputNumbers[index] = ports.get(index).getId();
                    }
                    LOGGER.info("Prepared light port outputNumbers: {}", outputNumbers);

                    if (!node.getProtocolVersion().isLowerThan(ProtocolVersion.VERSION_0_6)) {

                        bidibNode.getConfigXBulk(PortModelEnum.getPortModel(node), LcOutputType.LIGHTPORT,
                            1 /*
                               * window size
                               */, outputNumbers);
                    }
                    else {
                        LOGGER.info("Query light port config for protocol version 0.5.");

                        bidibNode.getConfigBulk(PortModelEnum.getPortModel(node), LcOutputType.LIGHTPORT,
                            outputNumbers);
                    }
                }
                else {
                    LOGGER.info("No light ports on node.");
                }
            }
            catch (Exception e) {
                LOGGER.warn("Query light port config failed.", e);
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public List<LightPort> getLightPorts(final Node node) {
        synchronized (lightPorts) {
            List<LightPort> ports = lightPorts.get(node.getUniqueId());

            if (ports == null) {
                LOGGER.info("Fetch lightports from Bidib for node: {}", node);
                ports = new LinkedList<LightPort>();
                lightPorts.put(node.getUniqueId(), ports);
                try {
                    BidibNode bidibNode = getBidib().getNode(node);

                    if (bidibNode != null) {
                        Feature featureLightPortCount =
                            Feature.findFeature(node.getFeatures(), BidibLibrary.FEATURE_CTRL_LIGHT_COUNT);

                        if (featureLightPortCount != null) {
                            // node.setFeature(featureLightPortCount);

                            int portCount = featureLightPortCount.getValue();
                            // int[] outputNumbers = new int[portCount];
                            // for (int index = 0; index < portCount; index++) {
                            // outputNumbers[index] = index;
                            // }

                            LOGGER.info("Prepare the light ports, portCount: {}", portCount);

                            // bulk read the port config
                            if (node.getProtocolVersion().isHigherThan(ProtocolVersion.VERSION_0_5)) {

                                for (int index = 0; index < portCount; index++) {
                                    final LightPort port = new LightPort();
                                    port.setId(index);
                                    ports.add(port);
                                }
                            }
                            else {
                                // List<LcConfig> portConfigs =
                                // bidibNode.getConfigBulk(PortModelEnum.getPortModel(node), LcOutputType.LIGHTPORT,
                                // outputNumbers);
                                // LOGGER.info("Returned light port config: {}", portConfigs);

                                for (int index = 0; index < portCount; index++) {
                                    final LightPort port = new LightPort();
                                    port.setId(index);
                                    ports.add(port);

                                    // // find the matching port config
                                    // LcConfig config = CollectionUtils.find(portConfigs, new Predicate<LcConfig>() {
                                    // public boolean evaluate(LcConfig sample) {
                                    // return sample.getOutputNumber(PortModelEnum.getPortModel(node)) == port
                                    // .getId();
                                    // }
                                    // });
                                    //
                                    // if (config != null) {
                                    // port.setDimMax(config.getValue3());
                                    // port.setDimMin(config.getValue4());
                                    // port.setPwmMax(config.getValue2());
                                    // port.setPwmMin(config.getValue1());
                                    // }
                                    // else {
                                    // LOGGER.warn("No light port configuration available for port: {}", port);
                                    // }
                                }
                            }
                        }
                    }
                    else {
                        LOGGER.debug("The node '{}' is not an bidib node.", node);
                    }
                }
                catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
            return Collections.unmodifiableList(ports);
        }
    }

    @Override
    public void queryBacklightPortConfig(Node node) {
        LOGGER.info("Query the backlight port config for node: {}", node);
        if (node != null) {
            try {
                BidibNode bidibNode = getBidib().getNode(node);

                List<BacklightPort> ports = backlightPorts.get(node.getUniqueId());

                if (CollectionUtils.isNotEmpty(ports)) {
                    int portCount = ports.size();

                    int[] outputNumbers = new int[portCount];
                    for (int index = 0; index < portCount; index++) {
                        outputNumbers[index] = ports.get(index).getId();
                    }
                    LOGGER.info("Prepared backlight port outputNumbers: {}", outputNumbers);

                    if (!node.getProtocolVersion().isLowerThan(ProtocolVersion.VERSION_0_6)) {
                        bidibNode.getConfigXBulk(PortModelEnum.getPortModel(node), LcOutputType.BACKLIGHTPORT, 1,
                            outputNumbers);
                    }
                    else {
                        LOGGER.info("Query backlight port config for protocol version 0.5.");

                        bidibNode.getConfigBulk(PortModelEnum.getPortModel(node), LcOutputType.BACKLIGHTPORT,
                            outputNumbers);
                    }
                }
                else {
                    LOGGER.info("No backlight ports on node.");
                }
            }
            catch (Exception e) {
                LOGGER.warn("Query backlight port config failed.", e);
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public List<BacklightPort> getBacklightPorts(final Node node) {
        synchronized (backlightPorts) {
            List<BacklightPort> ports = backlightPorts.get(node.getUniqueId());

            if (ports == null) {
                LOGGER.debug("Fetch lightports from Bidib for node: {}", node);
                ports = new LinkedList<BacklightPort>();
                backlightPorts.put(node.getUniqueId(), ports);
                try {
                    BidibNode bidibNode = getBidib().getNode(node);

                    if (bidibNode != null) {
                        Feature featureBacklightPortCount =
                            Feature.findFeature(node.getFeatures(), BidibLibrary.FEATURE_CTRL_BACKLIGHT_COUNT);

                        if (featureBacklightPortCount != null) {
                            node.setFeature(featureBacklightPortCount);

                            int portCount = featureBacklightPortCount.getValue();

                            // bulk read the port config
                            if (node != null && !node.getProtocolVersion().isLowerThan(ProtocolVersion.VERSION_0_6)) {
                                for (int index = 0; index < portCount; index++) {
                                    final BacklightPort port = new BacklightPort();
                                    port.setId(index);
                                    ports.add(port);
                                }
                            }
                            else {

                                for (int index = 0; index < portCount; index++) {
                                    final BacklightPort port = new BacklightPort();
                                    port.setId(index);
                                    ports.add(port);
                                }
                            }
                            LOGGER.info("Fetched backlight ports from node: {}", ports);
                        }
                        else {
                            LOGGER.info("No backlight ports available on node.");
                        }
                    }
                    else {
                        LOGGER.debug("The node '{}' is not an accessory node.", node);
                    }
                }
                catch (Exception e) {
                    LOGGER.warn("Fetch backlight count failed.", e);
                    throw new RuntimeException(e);
                }
            }
            return Collections.unmodifiableList(ports);
        }
    }

    /**
     * Restore the macro functions. Get the macro steps from the node and store the steps in the {@code functions}
     * parameter.
     * 
     * @param node
     *            the node
     * @param macro
     *            the macro
     * @param functions
     *            the list to add the macro steps
     * @throws ProtocolException
     */
    private void restoreMacroFunctions(
        final org.bidib.wizard.mvc.main.model.Node node, Macro macro, List<Function<?>> functions)
        throws ProtocolException {

        AccessoryNode accessoryNode = getBidib().getAccessoryNode(node.getNode());
        if (accessoryNode == null) {
            LOGGER.error("The provided node is not an accessory node: {}", node);
            return;
        }

        int stepNumber = 0;

        final PortModelEnum portModel = PortModelEnum.getPortModel(node.getNode());

        for (;;) {
            final LcMacro macroStep = accessoryNode.getMacroStep(macro.getId(), stepNumber++);

            if (macroStep == null
                || (macroStep.isSystemFunction() && macroStep.getSystemFunctionType() == LcOutputType.END_OF_MACRO)) {
                LOGGER.info("No more steps or end of macro detected.");
                break;
            }

            if (stepNumber > macro.getFunctionSize()) {
                LOGGER.warn("Abort reading macro because maximum number of functions in macro exceeded for macro: {}",
                    macro.getLabel());
                break;
            }

            Function<?> function = null;
            LcOutputType outputType = null;
            Port<?> port = null;
            int portNumber = -1;

            if (!macroStep.isSystemFunction()) {
                // the current step is not a system function
                BidibPort bidibPort = macroStep.getBidibPort();
                portNumber = bidibPort.getPortNumber(portModel);

                LOGGER.info("Get port with number: {}", portNumber);

                if (portModel == PortModelEnum.type) {
                    outputType = macroStep.getSystemFunctionType();
                    port = ConversionUtils.getPort(this, node, outputType, portNumber);
                }
                else {
                    port = ConversionUtils.getPort(this, node, portNumber);

                    // TODO if no matching port was found this will cause an NPE ...
                    outputType = port.getGenericPort().getCurrentPortType();
                }

                LOGGER.info("Port to use: {}, outputType: {}", port, outputType);
            }
            else {
                // the current step is a system function
                outputType = macroStep.getSystemFunctionType();
            }

            // if no matching status is found for this port type, this will cause an InvalidConfigurationException
            // ...
            function =
                ConversionUtils.getFunction(outputType, ConversionUtils.toBidibStatus(macroStep.getStatus(outputType)));

            LOGGER.info("The prepared function: {}", function);

            if (function instanceof PortAction) {
                ((Delayable) function).setDelay(ByteUtils.getInt(macroStep.getDelay()));
                LOGGER.info("Get port for function: {}", Function.getDebugString(function));

                LOGGER.info("The current function is a port action for port: {}", port);
                ((PortAware<Port<?>>) function).setPort(port);

                if (function instanceof PortValueAware) {
                    ((PortValueAware) function).setValue(ByteUtils.getInt(macroStep.getPortValue(outputType)));
                }
            }
            // system functions following
            else if (function instanceof AccessoryOkayFunction) {

                BidibPort bidibPort =
                    BidibPort.prepareSystemFunctionBidibPort(portModel, macroStep.getSystemFunctionValue(),
                        macroStep.getSystemFunctionExtValue());
                portNumber = bidibPort.getPortNumber(portModel);
                InputPort inputPort =
                    (InputPort) ConversionUtils.getPort(this, node, LcOutputType.INPUTPORT, portNumber);

                if (!AccessoryOkayStatus.NO_FEEDBACK.equals(((AccessoryOkayFunction) function).getAction())) {
                    ((AccessoryOkayFunction) function).setInput(inputPort);
                }
                else {
                    ((AccessoryOkayFunction) function).setInput(null);
                }
            }
            else if (function instanceof DelayFunction) {
                ((DelayFunction) function).setDelay(ByteUtils.getInt(macroStep.getSystemFunctionValue()));
            }
            else if (function instanceof FlagFunction) {
                List<Flag> flags = node.getFlags();
                int flagId = ByteUtils.getInt(macroStep.getSystemFunctionValue());

                if (flagId >= 0 && flagId < flags.size()) {
                    ((FlagFunction) function).setFlag(flags.get(flagId));
                }
            }
            else if (function instanceof InputFunction) {

                BidibPort bidibPort =
                    BidibPort.prepareSystemFunctionBidibPort(portModel, macroStep.getSystemFunctionValue(),
                        macroStep.getSystemFunctionExtValue());
                portNumber = bidibPort.getPortNumber(portModel);
                InputPort inputPort = (InputPort) ConversionUtils.getPort(this, node, outputType, portNumber);
                LOGGER.info("The current function is an input action for port: {}", inputPort);

                ((InputFunction) function).setInput(inputPort);
            }
            else if (function instanceof MacroFunction) {
                ((MacroFunction) function).setMacroId(ByteUtils.getInt(macroStep.getSystemFunctionValue()));
            }
            else if (function instanceof ServoMoveQueryFunction) {
                BidibPort bidibPort =
                    BidibPort.prepareSystemFunctionBidibPort(portModel, macroStep.getSystemFunctionValue(),
                        macroStep.getSystemFunctionExtValue());

                portNumber = bidibPort.getPortNumber(portModel);

                ServoPort servoPort =
                    (ServoPort) ConversionUtils.getPort(this, node, LcOutputType.SERVOPORT, portNumber);
                LOGGER.info("The current function is a servo move query action for port: {}", servoPort);
                ((ServoMoveQueryFunction) function).setPort(servoPort);
            }
            else if (function instanceof RandomDelayFunction) {
                ((RandomDelayFunction) function).setMaximumValue(ByteUtils.getInt(macroStep.getSystemFunctionValue()));
            }
            else if (function instanceof CriticalFunction) {
                // nothing to do but prevent the warning
            }
            else {
                LOGGER.warn("Unsupported macro step detected: {}", macroStep);
            }
            functions.add(function);
        }

    }

    @Override
    public Macro getMacroContent(org.bidib.wizard.mvc.main.model.Node node, Macro macro) {
        if (macro.getFunctionCount() == 0) {
            try {
                AccessoryNode accessoryNode = getBidib().getAccessoryNode(node.getNode());

                if (accessoryNode != null) {
                    macro.clearFunctions();
                    macro.clearStartConditions();

                    LOGGER.info("Get the macro content for macro: {}", macro);

                    // set the flat port model flag
                    macro.setFlatPortModel(node.getNode().isPortFlatModelAvailable());

                    // ensure servo port parameters are available to calculate
                    // the relative servo position
                    getServoPorts(node.getNode());

                    List<Function<?>> functions = new LinkedList<Function<?>>();

                    try {
                        restoreMacroFunctions(node, macro, functions);
                    }
                    finally {
                        macro.addFunctionsBefore(0, functions.toArray(new Function<?>[0]));

                        // read the start condition
                        byte[] startClk =
                            accessoryNode
                                .getMacroParameter(macro.getId(), BidibLibrary.BIDIB_MACRO_PARA_START_CLK).getValue();
                        MacroUtils.applyMacroStartClk(startClk, macro);
                    }

                    // reset the changed flag on the macro
                    macro.setMacroSaveState(MacroSaveState.PERMANENTLY_STORED_ON_NODE);
                }
            }
            catch (InvalidConfigurationException ex) {
                LOGGER.warn("Restore macro functions failed.", ex);
                throw ex;
            }
            catch (Exception e) {
                LOGGER.warn("Get the macro content failed.", e);

                // set the error flag on the macro
                macro.setContainsError(true);
            }
        }
        return macro;
    }

    @Override
    public int getMaxMacroLength(Node node) {
        int result = 0;

        try {
            AccessoryNode accessoryNode = getBidib().getAccessoryNode(node);
            if (accessoryNode != null) {
                result = internalGetMacroLength(node, accessoryNode);
            }
            else {
                LOGGER.debug("The current node is not an accessoryNode: {}", node);
            }
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    private int internalGetMacroLength(Node node, AccessoryNode accessoryNode) {
        int result = 0;
        try {
            // get the maximum number of macro points per macro
            Feature feature = Feature.findFeature(node.getFeatures(), BidibLibrary.FEATURE_CTRL_MAC_SIZE);
            LOGGER.info("returned macro length feature: {}", feature);
            if (feature != null) {
                // node.setFeature(feature);
                result = feature.getValue();
            }
        }
        catch (Exception e) {
            LOGGER.warn("Get macro size value failed.", e);
            throw new RuntimeException(e);
        }
        return result;
    }

    @Override
    public List<Macro> getMacros(Node node) {
        try {
            AccessoryNode accessoryNode = getBidib().getAccessoryNode(node);
            if (accessoryNode != null) {
                return Collections.unmodifiableList(internalGetMacros(node, accessoryNode));
            }
            else {
                LOGGER.debug("The current node is not an accessoryNode: {}", node);
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Get macros from node failed.", ex);
            throw new RuntimeException(ex);
        }

        return Collections.emptyList();
    }

    @Override
    public List<MotorPort> getMotorPorts(Node node) {
        synchronized (motorPorts) {
            List<MotorPort> ports = motorPorts.get(node.getUniqueId());

            if (ports == null) {
                ports = new LinkedList<MotorPort>();
                motorPorts.put(node.getUniqueId(), ports);
                try {
                    BidibNode bidibNode = getBidib().getNode(node);

                    if (bidibNode != null) {
                        Feature feature =
                            Feature.findFeature(node.getFeatures(), BidibLibrary.FEATURE_CTRL_MOTOR_COUNT);
                        if (feature != null) {
                            // node.setFeature(feature);

                            for (int index = 0; index < feature.getValue(); index++) {
                                MotorPort port = new MotorPort();

                                port.setId(index);
                                ports.add(port);
                            }
                        }
                        else {
                            LOGGER.debug("No motor ports available on node: {}", node);
                        }
                    }
                    else {
                        LOGGER.info("Don't fetch motorPorts because the current node is not an accessory node: {}",
                            node);
                    }
                }
                catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
            return Collections.unmodifiableList(ports);
        }
    }

    @Override
    public List<Node> loadSubNodes(Node node) {
        LOGGER.info("Load the subnodes of the provided node: {}", node);
        BidibNode bidibNode = getBidib().getNode(node);

        List<Node> subNodes = Collections.emptyList();
        if (NodeUtils.hasSubNodesFunctions(node.getUniqueId())) {
            LOGGER.info("The current node has subnode functions: {}. Start loading subnodes.", bidibNode);
            synchronized (nodes) {
                List<Node> originalNodes = new LinkedList<Node>(nodes);
                LOGGER.info("Before fetchSubNodes, originalNodes: {}", originalNodes);
                fetchSubNodes(bidibNode, nodes);
                LOGGER.info("After fetchSubNodes, nodes: {}", nodes);

                // we must not return the nodes from the original list
                List<Node> allNodes = new LinkedList<Node>(nodes);
                // remove the original nodes from the new list
                allNodes.removeAll(originalNodes);

                LOGGER.info("All nodes after remove original nodes: {}", allNodes);

                subNodes = Collections.unmodifiableList(allNodes);

                LOGGER.info("Returning subNodes: {}", subNodes);
            }
        }
        else {
            LOGGER.info("The provided node does not support subnode functions: {}", node);
        }
        LOGGER.info("Return the subnodes of the current node: {}", subNodes);
        return subNodes;
    }

    @Override
    public List<Node> getSubNodes(Node node) {
        LOGGER.debug("Get the subnodes of the provided node: {}", node);

        List<Node> subNodes = new LinkedList<>();
        if (NodeUtils.hasSubNodesFunctions(node.getUniqueId())) {
            LOGGER.info("The current node has subnode functions: {}", node);

            synchronized (nodes) {
                for (Node cachedNode : nodes) {
                    if (NodeUtils.isSubNode(node, cachedNode)) {
                        LOGGER.info("Found a subnode: {}", cachedNode);
                        subNodes.add(cachedNode);
                    }
                }
            }
        }

        return subNodes;
    }

    @Override
    public List<Node> getNodes() {
        synchronized (nodes) {
            return Collections.unmodifiableList(nodes);
        }
    }

    @Override
    public ProtocolVersion getProtocolVersion(Node node) {
        ProtocolVersion result = null;

        try {
            result = getBidib().getNode(node).getProtocolVersion();
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    @Override
    public void queryServoPortConfig(Node node) {
        LOGGER.info("Query the servo port config for node: {}", node);
        if (node != null) {
            try {
                BidibNode bidibNode = getBidib().getNode(node);

                List<ServoPort> ports = servoPorts.get(node.getUniqueId());

                if (CollectionUtils.isNotEmpty(ports)) {
                    int portCount = ports.size();

                    int[] outputNumbers = new int[portCount];
                    for (int index = 0; index < portCount; index++) {
                        outputNumbers[index] = ports.get(index).getId();
                    }

                    LOGGER.info("Prepared servo port outputNumbers: {}", outputNumbers);

                    if (node.getProtocolVersion().isHigherThan(ProtocolVersion.VERSION_0_5)) {
                        bidibNode.getConfigXBulk(PortModelEnum.getPortModel(node), LcOutputType.SERVOPORT,
                            1 /*
                               * window size
                               */, outputNumbers);
                    }
                    else {
                        bidibNode.getConfigBulk(PortModelEnum.getPortModel(node), LcOutputType.SERVOPORT,
                            outputNumbers);
                    }
                }
                else {
                    LOGGER.info("No servo ports on node.");
                }
            }
            catch (Exception e) {
                LOGGER.warn("Query servo port config failed.", e);
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public List<ServoPort> getServoPorts(final Node node) {
        LOGGER.info("Get the servo ports for node: {}", node);

        synchronized (servoPorts) {

            List<ServoPort> ports = servoPorts.get(node.getUniqueId());

            if (ports == null) {
                ports = new LinkedList<ServoPort>();
                servoPorts.put(node.getUniqueId(), ports);
                try {
                    BidibNode bidibNode = getBidib().getNode(node);

                    if (bidibNode != null) {
                        Feature featureServoCount =
                            Feature.findFeature(node.getFeatures(), BidibLibrary.FEATURE_CTRL_SERVO_COUNT);

                        if (featureServoCount != null) {

                            int portCount = featureServoCount.getValue();
                            int[] outputNumbers = new int[portCount];
                            for (int index = 0; index < portCount; index++) {
                                outputNumbers[index] = index;
                            }

                            // bulk read the port config
                            if (node != null && node.getProtocolVersion().isHigherThan(ProtocolVersion.VERSION_0_5)) {
                                // prepare the servo ports

                                for (int index = 0; index < featureServoCount.getValue(); index++) {
                                    final ServoPort port = new ServoPort();
                                    port.setId(index);
                                    ports.add(port);
                                }
                            }
                            else {
                                // protocol is <= 0.5
                                for (int index = 0; index < featureServoCount.getValue(); index++) {
                                    final ServoPort port = new ServoPort();
                                    port.setPortConfigEnabled(false);
                                    port.setId(index);
                                    ports.add(port);
                                }
                            }
                        }
                    }
                }
                catch (Exception e) {
                    LOGGER.warn("Load servo ports failed.", e);
                    throw new RuntimeException(e);
                }
            }
            return Collections.unmodifiableList(ports);
        }
    }

    public boolean isQueryPortStatusEnabled(Node node) {
        boolean queryPorts = false;
        try {
            Feature featurePortQuery =
                Feature.findFeature(node.getFeatures(), BidibLibrary.FEATURE_CTRL_PORT_QUERY_AVAILABLE);

            if (featurePortQuery != null) {
                queryPorts = featurePortQuery.getValue() > 0;
            }
            LOGGER.info("Query the ports enabled: {}, featurePortQuery: {}", queryPorts, featurePortQuery);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
        return queryPorts;
    }

    @Override
    public void queryPortStatus(LcOutputType lcOutputType, Node node, int portNumber) {
        try {
            BidibNode bidibNode = getBidib().getNode(node);
            LOGGER.info("Query the port state for lcOutputType: {}, portNumber: {}", lcOutputType, portNumber);

            bidibNode.queryOutputState(PortModelEnum.getPortModel(node), lcOutputType, portNumber);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void queryPortStatus(LcOutputType lcOutputType, Node node, List<Integer> portIds) {
        try {
            BidibNode bidibNode = getBidib().getNode(node);
            LOGGER.info("Query the port state for lcOutputType: {}, portIds: {}", lcOutputType, portIds);

            bidibNode.queryPortStates(PortModelEnum.getPortModel(node), lcOutputType, portIds);
        }
        catch (Exception e) {
            LOGGER.warn("Query port status failed for lcOutputType: {}", lcOutputType, e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void queryPortStatusAll(Node node, int portTypeMask, int rangeFrom, int rangeTo) {

        try {
            BidibNode bidibNode = getBidib().getNode(node);
            LOGGER.info("Query the port state for all ports, portTypeMask: {}, rangeFrom: {}, rangeTo: {}",
                portTypeMask, rangeFrom, rangeTo);

            bidibNode.queryPortStatesAll(PortModelEnum.getPortModel(node), portTypeMask, rangeFrom, rangeTo);
        }
        catch (Exception e) {
            LOGGER.warn("Query port status for all ports failed for portTypeMask: {}, rangeFrom: {}, rangeTo: {}",
                portTypeMask, rangeFrom, rangeTo, e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public SoftwareVersion getSoftwareVersion(Node node) {
        SoftwareVersion result = null;

        try {
            result = getBidib().getNode(node).getSwVersion();
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    @Override
    public List<SoundPort> getSoundPorts(Node node) {
        synchronized (soundPorts) {
            List<SoundPort> ports = soundPorts.get(node.getUniqueId());

            if (ports == null) {
                ports = new LinkedList<SoundPort>();
                soundPorts.put(node.getUniqueId(), ports);
                try {
                    BidibNode bidibNode = getBidib().getNode(node);

                    if (bidibNode != null) {
                        Feature feature =
                            Feature.findFeature(node.getFeatures(), BidibLibrary.FEATURE_CTRL_SOUND_COUNT);

                        if (feature != null) {

                            for (int index = 0; index < feature.getValue(); index++) {
                                SoundPort port = new SoundPort();

                                port.setId(index);
                                ports.add(port);
                            }
                        }
                    }
                }
                catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
            return Collections.unmodifiableList(ports);
        }
    }

    @Override
    public int getStorableMacroCount(Node node) {
        int result = 0;

        try {
            AccessoryNode accessoryNode = getBidib().getAccessoryNode(node);

            if (accessoryNode != null) {
                Feature feature = Feature.findFeature(node.getFeatures(), BidibLibrary.FEATURE_CTRL_MAC_SAVE);
                if (feature != null) {
                    result = feature.getValue();
                }
            }
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    @Override
    public List<SwitchPort> getSwitchPorts(final Node node) {
        synchronized (switchPorts) {
            List<SwitchPort> ports = switchPorts.get(node.getUniqueId());

            if (ports == null) {
                ports = new LinkedList<SwitchPort>();
                switchPorts.put(node.getUniqueId(), ports);
                try {
                    BidibNode bidibNode = getBidib().getNode(node);

                    if (bidibNode != null) {
                        Feature featureSwitchPortCount =
                            Feature.findFeature(node.getFeatures(), BidibLibrary.FEATURE_CTRL_SWITCH_COUNT);
                        if (featureSwitchPortCount != null) {
                            LOGGER.info("Number of switch ports: {}", featureSwitchPortCount.getValue());

                            int portCount = featureSwitchPortCount.getValue();
                            int[] outputNumbers = new int[portCount];
                            for (int index = 0; index < portCount; index++) {
                                outputNumbers[index] = index;
                            }

                            for (int index = 0; index < featureSwitchPortCount.getValue(); index++) {
                                final SwitchPort port = new SwitchPort();
                                port.setId(index);
                                ports.add(port);
                            }
                        }
                        else {
                            LOGGER.info("The number of switch ports is not available as feature.");
                        }
                    }
                }
                catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
            return Collections.unmodifiableList(ports);
        }
    }

    @Override
    public void querySwitchPortConfig(final Node node) {
        LOGGER.info("Query the switch port config for node: {}", node);
        if (node != null) {
            LOGGER.info("Query switch port config for protocol version > 0.5");
            try {
                BidibNode bidibNode = getBidib().getNode(node);

                List<SwitchPort> ports = switchPorts.get(node.getUniqueId());

                if (CollectionUtils.isNotEmpty(ports)) {
                    int portCount = ports.size();

                    int[] outputNumbers = new int[portCount];
                    for (int index = 0; index < portCount; index++) {
                        outputNumbers[index] = index;
                    }
                    LOGGER.info("Prepared switch port outputNumbers: {}", outputNumbers);

                    if (node.getProtocolVersion().isHigherThan(ProtocolVersion.VERSION_0_5)) {
                        bidibNode.getConfigXBulk(PortModelEnum.getPortModel(node), LcOutputType.SWITCHPORT,
                            1 /*
                               * window size
                               */, outputNumbers);
                    }
                    else {
                        Feature feature =
                            Feature.findFeature(node.getFeatures(), BidibLibrary.FEATURE_SWITCH_CONFIG_AVAILABLE);
                        if (feature != null && feature.getValue() > 0) {
                            bidibNode.getConfigBulk(PortModelEnum.getPortModel(node), LcOutputType.SWITCHPORT,
                                outputNumbers);
                        }
                        else {
                            LOGGER.info("The current node does not support reading switch port config: {}", node);
                        }
                    }
                }
                else {
                    LOGGER.info("No switch ports on node.");
                }
            }
            catch (Exception ex) {
                LOGGER.warn("Query switch port config with bulk failed.", ex);
                throw new RuntimeException(ex);
            }
        }
    }

    @Override
    public int getMagic(Node node) {
        LOGGER.info("Get magic from node: {}", node);
        try {
            int magic = getBidib().getNode(node).getMagic(null);
            LOGGER.info("Node returned magic: {}", magic);
            return magic;
        }
        catch (ProtocolNoAnswerException ex) {
            LOGGER.warn("Get magic from node failed.", ex);
            throw new NoAnswerException(ex);
        }
        catch (Exception ex) {
            LOGGER.warn("Get magic from node failed.", ex);
            throw new RuntimeException(ex);
        }
    }

    @Override
    public Long readUniqueId(Node node) {
        LOGGER.info("Get the uniqueId from the node: {}", node);

        try {
            // get the uniqueId from the node
            long uniqueId = getBidib().getNode(node).getUniqueId(true);
            LOGGER.info("Node returned uniqueId: {}, node: {}", uniqueId, node);
            return uniqueId;
        }
        catch (ProtocolNoAnswerException ex) {
            LOGGER.warn("Get uniqueId from node failed.", ex);
            throw new NoAnswerException(ex);
        }
        catch (Exception ex) {
            LOGGER.warn("Get uniqueId from node failed.", ex);
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void identify(Node node, IdentifyState state) {
        try {
            getBidib().getNode(node).identify(state);
        }
        catch (Exception ex) {
            LOGGER.warn("Set the identify state failed on node: {}", node, ex);
            throw new RuntimeException(ex);
        }
    }

    /**
     * Get the macros from the cached list of macros. If the macros of the node are not cached, the macro "metadata"
     * without the macro content is fetched from the node and stored in the list of macros. The macro content must be
     * fetched with @see {@link #getMacroContent(Node, Macro)}.
     * 
     * @param accessoryNode
     *            the node to fetch the macros
     * @return the list of macro (meta-data) from the node
     * @throws ProtocolException
     */
    private List<Macro> internalGetMacros(final Node node, AccessoryNode accessoryNode) throws ProtocolException {
        synchronized (macros) {
            LOGGER.info("Get the macros for node: {}", accessoryNode);
            List<Macro> macroList = macros.get(accessoryNode.getBidibNode().getUniqueId());

            if (macroList == null) {
                macroList = new LinkedList<Macro>();
                try {

                    if (accessoryNode != null) {
                        int macroCount = 0;
                        Feature feature = Feature.findFeature(node.getFeatures(), BidibLibrary.FEATURE_CTRL_MAC_COUNT);
                        // get the number of macros that is available in the node
                        if (feature != null) {
                            macroCount = feature.getValue();
                        }

                        LOGGER.info("Current macroCount: {}", macroCount);
                        // get the configured maximum macro steps from the node
                        int macroLength = internalGetMacroLength(node, accessoryNode);

                        for (int macroNumber = 0; macroNumber < macroCount; macroNumber++) {

                            Macro macro = new Macro(macroLength);
                            try {
                                macro.setId(macroNumber);

                                // set the flat port model flag
                                macro.setFlatPortModel(node.isPortFlatModelAvailable());

                                List<LcMacroParaValue> paramValues =
                                    accessoryNode.getMacroParameters(macroNumber,
                                        BidibLibrary.BIDIB_MACRO_PARA_START_CLK, BidibLibrary.BIDIB_MACRO_PARA_REPEAT,
                                        BidibLibrary.BIDIB_MACRO_PARA_SLOWDOWN);

                                if (paramValues == null || paramValues.size() != 3) {
                                    LOGGER.error("Received incomplete paramValues: {}, macroNumber: {}", paramValues,
                                        macroNumber);
                                    macro.setContainsError(true);
                                }

                                byte[] startClk = paramValues.get(0).getValue();
                                MacroUtils.applyMacroStartClk(startClk, macro);
                                macro.setCycles(paramValues.get(1).getValue()[0]);
                                macro.setSpeed(paramValues.get(2).getValue()[0]);
                                macroList.add(macro);
                            }
                            catch (Exception ex) {
                                // set the error flag
                                if (macro != null) {
                                    macro.setContainsError(true);
                                }
                                LOGGER.error("Prepare macro failed. current macro: {}", macro, ex);
                            }
                        }
                    }
                }
                catch (Exception e) {
                    LOGGER.warn("Fetch macros from node failed.", e);
                    throw new RuntimeException(e);
                }
                // store macro list of node
                macros.put(accessoryNode.getBidibNode().getUniqueId(), macroList);
            }
            else {
                LOGGER.info("Return the cached macro list: {}", macroList);
            }
            return macroList;
        }
    }

    @Override
    public Boolean isAddressMessagesEnabled(Node node) {
        Boolean result = null;
        ;

        try {
            BidibNode bidibNode = getBidib().getNode(node);

            if (bidibNode != null) {
                Feature feature = Feature.findFeature(node.getFeatures(), BidibLibrary.FEATURE_BM_ADDR_DETECT_ON);
                if (feature != null) {
                    // node.setFeature(feature);
                    result = feature.getValue() > 0;
                }
            }
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    @Override
    public Boolean isBooster(Node node) {
        LOGGER.debug("Check if node is a booster node: {}", node);

        boolean isBoosterNode = NodeUtils.hasBoosterFunctions(node.getUniqueId());

        LOGGER.debug("Node is a booster node: {}", isBoosterNode);
        return isBoosterNode;
    }

    @Override
    public Boolean isCommandStation(Node node) {
        LOGGER.debug("Check if node is command station node: {}", node);

        boolean isCSNode = NodeUtils.hasCommandStationFunctions(node.getUniqueId());

        LOGGER.debug("Node is command station node: {}", isCSNode);
        return isCSNode;
    }

    @Override
    public Boolean isDccStartEnabled(Node node) {
        Boolean result = null;

        try {
            AccessoryNode accessoryNode = getBidib().getAccessoryNode(node);

            if (accessoryNode != null) {
                Feature feature = Feature.findFeature(node.getFeatures(), BidibLibrary.FEATURE_CTRL_MAC_START_DCC);

                if (feature != null) {
                    // node.setFeature(feature);
                    result = feature.getValue() > 0;
                }
            }
            else {
                LOGGER.warn("The requested node is not an accessory node: {}", node);
            }
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    @Override
    public Boolean isExternalStartMacroEnabled(Node node) {
        Boolean result = null;

        try {
            AccessoryNode accessoryNode = getBidib().getAccessoryNode(node);

            if (accessoryNode != null) {
                Feature feature = Feature.findFeature(node.getFeatures(), BidibLibrary.FEATURE_CTRL_MAC_START_MAN);

                if (feature != null) {
                    // node.setFeature(feature);
                    result = feature.getValue() > 0;
                }
            }
            else {
                LOGGER.warn("The requested node is not an accessory node: {}", node);
            }
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    @Override
    public Boolean isFeedbackMessagesEnabled(Node node) {
        Boolean result = null;

        try {
            BidibNode bidibNode = getBidib().getNode(node);

            if (bidibNode != null) {
                Feature feature = Feature.findFeature(node.getFeatures(), BidibLibrary.FEATURE_BM_ON);
                if (feature != null) {
                    // node.setFeature(feature);
                    result = feature.getValue() > 0;
                }
            }
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    @Override
    public Boolean isFeedbackMirrorDisabled(Node node) {
        Boolean result = null;

        try {
            BidibNode bidibNode = getBidib().getNode(node);

            if (bidibNode != null) {

                if (bidibNode.isSecureAckEnabled()) {
                    LOGGER.warn("Get the secureAck from the feature of the node.");
                    Feature feature = Feature.findFeature(node.getFeatures(), BidibLibrary.FEATURE_BM_ON);
                    if (feature != null) {
                        result = feature.getValue() == 0;
                    }
                }
                else {
                    LOGGER.warn("The secureAck flag is disabled on this node.");
                    result = !bidibNode.isSecureAckEnabled();
                }
            }
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    @Override
    public Boolean isKeyMessagesEnabled(Node node) {
        Boolean result = null;

        try {
            BidibNode bidibNode = getBidib().getNode(node);

            if (bidibNode != null) {
                Feature feature = Feature.findFeature(node.getFeatures(), BidibLibrary.FEATURE_CTRL_INPUT_NOTIFY);
                if (feature != null) {
                    // node.setFeature(feature);
                    result = feature.getValue() > 0;
                }
            }
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    @Override
    public boolean isUpdatable(Node node) {
        boolean result = false;

        Feature feature = Feature.findFeature(node.getFeatures(), BidibLibrary.FEATURE_FW_UPDATE_MODE);
        if (feature != null) {
            node.setFeature(feature);
            result = feature.getValue() > 0;
        }
        return result;

    }

    @Override
    public void reloadAccessory(Node node, Accessory accessory) {
        try {
            AccessoryNode accessoryNode = getBidib().getAccessoryNode(node);

            int accessoryMacroMapped = 0;
            Feature featureMacroMapped =
                Feature.findFeature(node.getFeatures(), BidibLibrary.FEATURE_ACCESSORY_MACROMAPPED);
            if (featureMacroMapped != null) {
                accessoryMacroMapped = featureMacroMapped.getValue();
            }

            getAccessoryContent(node, accessoryNode, accessory, accessoryMacroMapped);
        }
        catch (NoAnswerException e) {
            LOGGER.warn("Get accessories failed.", e);
            throw e;
        }
        catch (Exception e) {
            LOGGER.warn("Get accessories failed.", e);
            throw new RuntimeException(e);
        }

    }

    @Override
    public void reset(Node node) {
        fireStatus(Resources.getString(getClass(), "bidib-reset"), -1);

        try {
            if (node == null || Arrays.equals(node.getAddr(), RootNode.ROOTNODE_ADDR)) {
                Thread worker = new Thread(new Runnable() {
                    public void run() {
                        resetRootAndFetchNodes(true, false);
                        LOGGER.info("Reset root node worker has finished.");
                    }
                });
                worker.start();
            }
            else {
                getBidib().getNode(node).reset();
                LOGGER.info("Reset node passed: {}", node);
            }
        }
        catch (Exception e) {
            LOGGER.warn("Reset node failed.", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void ping(Node node, byte data) {

        try {
            BidibInterface bidib = getBidib();
            bidib.getNode(node).ping(data);
        }
        catch (Exception ex) {
            LOGGER.warn("Ping node failed.", ex);
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void saveAccessory(Node node, Accessory accessory) {

        try {

            if (CollectionUtils.isNotEmpty(accessory.getAspects())) {
                MacroRef[] aspects = accessory.getAspects().toArray(new MacroRef[0]);
                byte[] value = new byte[aspects.length + 1];

                // add the macro numbers
                for (int index = 0; index < aspects.length; index++) {
                    // get the macro id
                    value[index] = (byte) aspects[index].getId().intValue();
                }

                // the termination of the list is 255
                value[value.length - 1] = ByteUtils.getLowByte(0xFF);

                getBidib().getAccessoryNode(node).setAccessoryParameter(accessory.getId(),
                    BidibLibrary.BIDIB_ACCESSORY_PARA_MACROMAP, value);
            }
            if (accessory.getStartupState() != null) {
                try {
                    byte[] startupValue = new byte[1];
                    startupValue[0] = ByteUtils.getLowByte(accessory.getStartupState());
                    LOGGER.info("Set the new startupValue: {}", ByteUtils.bytesToHex(startupValue));
                    getBidib().getAccessoryNode(node).setAccessoryParameter(accessory.getId(),
                        BidibLibrary.BIDIB_ACCESSORY_PARA_STARTUP, startupValue);
                }
                catch (Exception ex) {
                    LOGGER.warn("Set the startup state of the accessory failed.", ex);
                }
            }
            else {
                LOGGER.info("No startup value available.");
            }

            if (accessory.getSwitchTime() != null) {
                try {
                    byte[] switchTimeValue = new byte[1];

                    int switchTime = accessory.getSwitchTime().intValue();
                    TimeBaseUnitEnum timeBaseUnit = accessory.getTimeBaseUnit();

                    int value = switchTime | (timeBaseUnit.getType() << 7);
                    LOGGER.info("Prepared switch time value: {}", ByteUtils.byteToHex(value));

                    switchTimeValue[0] = ByteUtils.getLowByte(value);
                    LOGGER.info("Set the new switchTimeValue: {}", ByteUtils.bytesToHex(switchTimeValue));
                    getBidib().getAccessoryNode(node).setAccessoryParameter(accessory.getId(),
                        BidibLibrary.BIDIB_ACCESSORY_SWITCH_TIME, switchTimeValue);
                }
                catch (Exception ex) {
                    LOGGER.warn("Set the switch time of the accessory failed.", ex);
                }
            }
            else {
                LOGGER.info("No switch time available.");
            }

            // accessory.setPendingChanges(false);
            accessory.setAccessorySaveState(AccessorySaveState.PERMANENTLY_STORED_ON_NODE);

        }
        catch (Exception e) {
            LOGGER.warn("Save accessory failed.", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public FirmwareUpdateStat sendFirmwareUpdateOperation(Node node, FirmwareUpdateOperation operation, byte... data) {
        FirmwareUpdateStat result = null;

        try {
            result = getBidib().getNode(node).sendFirmwareUpdateOperation(operation, data);
        }
        catch (ProtocolNoAnswerException e) {
            throw new NoAnswerException("Send firmware update failed for operation: " + operation.name(), e);
        }
        catch (Exception e) {
            throw new RuntimeException("Send firmware update failed for operation: " + operation.name(), e);
        }
        return result;
    }

    @Override
    public void releaseAndReloadRootNode(final Node node) {
        LOGGER.info("Release the root node and reload the nodes.");

        fireStatus(Resources.getString(getClass(), "bidib-release-and-reload-rootnode"), -1);

        try {
            if (Arrays.equals(node.getAddr(), RootNode.ROOTNODE_ADDR)) {
                Thread worker = new Thread(new Runnable() {
                    public void run() {
                        try {
                            LOGGER.info("Start reset root node and fetch nodes worker.");
                            resetRootAndFetchNodes(false, true);
                            LOGGER.info("Reset root node and fetch nodes worker has finished.");
                        }
                        catch (Exception ex) {
                            LOGGER.warn("Reset root node and fetch nodes worker failed.", ex);
                        }
                    }
                });
                worker.start();
            }
            else {
                LOGGER.warn("The provided node is not a root node: {}", node);
            }
        }
        catch (Exception e) {
            LOGGER.warn("Release and reload root node failed.", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void sendTime(Calendar time, int factor) {
        try {
            BidibInterface bidib = getBidib();
            if (bidib != null && bidib.isOpened() && bidib.getRootNode() != null
                && !bidib.getRootNode().isBootloaderNode()) {
                bidib.getRootNode().clock(time, factor);
            }
            else {
                LOGGER.warn("No open bidib instance available or root node is bootloader node.");
            }
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Send time failed.", ex);
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void setAddressMessagesEnabled(Node node, boolean enabled) {
        try {
            Feature feature =
                getBidib().getNode(node).setFeature(BidibLibrary.FEATURE_BM_ADDR_DETECT_ON, (enabled ? 1 : 0));
            // update the feature on the node
            node.setFeature(feature);
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Set address messages enabled failed.", ex);
            // TODO handle the exception
        }
    }

    @Override
    public void setBinaryState(Node node, int address, int state, boolean value) {
        try {
            getBidib().getCommandStationNode(node).setBinaryState(address, state, value);
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Set binary state failed.", ex);
            // TODO handle the exception
        }
    }

    @Override
    public void setDccStartEnabled(Node node, boolean enabled) {
        try {
            Feature feature =
                getBidib().getAccessoryNode(node).getBidibNode().setFeature(BidibLibrary.FEATURE_CTRL_MAC_START_DCC,
                    (enabled ? 1 : 0));
            // update the feature on the node
            node.setFeature(feature);
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Set DCC start enabled failed.", ex);
            // TODO handle the exception
        }
    }

    @Override
    public void setExternalStartMacroEnabled(Node node, boolean enabled) {
        try {
            Feature feature =
                getBidib().getAccessoryNode(node).getBidibNode().setFeature(BidibLibrary.FEATURE_CTRL_MAC_START_MAN,
                    (enabled ? 1 : 0));
            // update the feature on the node
            node.setFeature(feature);
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Set external start enabled failed.", ex);
            // TODO handle the exception
        }
    }

    @Override
    public void setFeedbackMessagesEnabled(Node node, boolean enabled) {
        try {
            Feature feature = getBidib().getNode(node).setFeature(BidibLibrary.FEATURE_BM_ON, (enabled ? 1 : 0));
            // update the feature on the node
            node.setFeature(feature);
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Set feedback messages enabled failed.", ex);
            // TODO handle the exception
        }
    }

    @Override
    public void setFeedbackMirrorDisabled(Node node, boolean disable) {
        getBidib().getNode(node).setSecureAckEnabled(!disable);
    }

    @Override
    public void setKeyMessagesEnabled(Node node, boolean enabled) {
        try {
            Feature feature =
                getBidib().getNode(node).setFeature(BidibLibrary.FEATURE_CTRL_INPUT_NOTIFY, (enabled ? 1 : 0));
            // update the feature on the node
            node.setFeature(feature);
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Set key messages enabled failed.", ex);
            // TODO handle the exception
        }
    }

    @Override
    public void setLightPortParameters(
        final Node node, LightPort port, int pwmMin, int pwmMax, int dimMin, int dimMax, Integer rgbValue) {

        LOGGER.info("setLightPortParameters, new dimMin: {}, dimMax: {}", dimMin, dimMax);

        try {
            if (node != null && node.getProtocolVersion().isHigherThan(ProtocolVersion.VERSION_0_5)) {

                Map<Byte, PortConfigValue<?>> values = new LinkedHashMap<>();

                // check if the keys are available on the port
                if (port.isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_LEVEL_PORT_ON)) {
                    values.put(BidibLibrary.BIDIB_PCFG_LEVEL_PORT_ON,
                        new BytePortConfigValue(ByteUtils.getLowByte(pwmMax)));
                }

                if (port.isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_LEVEL_PORT_OFF)) {
                    values.put(BidibLibrary.BIDIB_PCFG_LEVEL_PORT_OFF,
                        new BytePortConfigValue(ByteUtils.getLowByte(pwmMin)));
                }

                if (port.isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_DIMM_UP_8_8)) {
                    values.put(BidibLibrary.BIDIB_PCFG_DIMM_UP_8_8,
                        new Int16PortConfigValue(ByteUtils.getWORD(dimMax)));
                }
                else if (port.isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_DIMM_UP)) {
                    values.put(BidibLibrary.BIDIB_PCFG_DIMM_UP, new BytePortConfigValue(ByteUtils.getLowByte(dimMax)));
                }

                if (port.isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_DIMM_DOWN_8_8)) {
                    values.put(BidibLibrary.BIDIB_PCFG_DIMM_DOWN_8_8,
                        new Int16PortConfigValue(ByteUtils.getWORD(dimMin)));
                }
                else if (port.isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_DIMM_DOWN)) {
                    values.put(BidibLibrary.BIDIB_PCFG_DIMM_DOWN,
                        new BytePortConfigValue(ByteUtils.getLowByte(dimMin)));
                }

                // if the lightport supports RGB we must update the RGB value
                if (port.isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_RGB) && port.getRgbValue() != null
                    && rgbValue != null) {
                    LOGGER.info("The lightport has RGB support: {}", port);

                    values.put(BidibLibrary.BIDIB_PCFG_RGB, new RgbPortConfigValue(ByteUtils.getRGB(rgbValue)));
                }

                LOGGER.info("Send the port config for LightPort: {}", values);

                setConfigX(node, LcOutputType.LIGHTPORT, port.getId(), values);
            }
            else {
                BidibPort bidibPort =
                    BidibPort.prepareBidibPort(LcOutputType.LIGHTPORT.getType(), ByteUtils.getLowByte(port.getId()));

                getBidib().getNode(node).setConfig(PortModelEnum.getPortModel(node),
                    new LcConfig(bidibPort, pwmMin, pwmMax, dimMin, dimMax));
            }
        }
        catch (Exception e) {
            LOGGER.warn("Set lightport config failed, port: {}", port, e);
            throw new RuntimeException(e);
        }
    }

    private void setConfigX(
        final Node node, LcOutputType lcOutputType, int port, final Map<Byte, PortConfigValue<?>> values)
        throws ProtocolException {

        PortModelEnum portModelEnum = PortModelEnum.getPortModel(node);
        BidibPort bidibPort = null;
        if (portModelEnum == PortModelEnum.type) {
            bidibPort = new BidibPort(new byte[] { lcOutputType.getType(), ByteUtils.getLowByte(port) });
        }
        else {
            bidibPort = new BidibPort(new byte[] { ByteUtils.getLowByte(port), ByteUtils.getHighByte(port) });
        }
        LcConfigX lcConfigX = new LcConfigX(bidibPort, values);
        LOGGER.debug("Prepared lcConfigX to send: {}", lcConfigX);

        // this call will block until the response was recieved
        getBidib().getNode(node).setConfigX(portModelEnum, lcConfigX);
    }

    @Override
    public void setBacklightPortParameters(
        Node node, BacklightPort port, int dimSlopeDown, int dimSlopeUp, int dmxMapping) {
        LOGGER.info("Set the backlight parameters for port: {}, dimSlopeDown: {}, dimSlopeUp: {}, dmxMapping: {}", port,
            dimSlopeDown, dimSlopeUp, dmxMapping);

        // we must subtract dmxMappingOffset to the dmxMapping because DMX address starts from 0
        try {
            if (node != null && node.getProtocolVersion().isHigherThan(ProtocolVersion.VERSION_0_5)) {

                // only send the known keys
                Map<Byte, PortConfigValue<?>> values = new LinkedHashMap<>();
                if (port.isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_DIMM_UP_8_8)) {
                    values.put(BidibLibrary.BIDIB_PCFG_DIMM_UP_8_8,
                        new Int16PortConfigValue(ByteUtils.getWORD(dimSlopeUp)));
                }
                else if (port.isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_DIMM_UP)) {
                    values.put(BidibLibrary.BIDIB_PCFG_DIMM_UP,
                        new BytePortConfigValue(ByteUtils.getLowByte(dimSlopeUp)));
                }

                if (port.isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_DIMM_DOWN_8_8)) {
                    values.put(BidibLibrary.BIDIB_PCFG_DIMM_DOWN_8_8,
                        new Int16PortConfigValue(ByteUtils.getWORD(dimSlopeDown)));
                }
                else if (port.isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_DIMM_DOWN)) {
                    values.put(BidibLibrary.BIDIB_PCFG_DIMM_DOWN,
                        new BytePortConfigValue(ByteUtils.getLowByte(dimSlopeDown)));
                }

                if (port.isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_OUTPUT_MAP)) {
                    values.put(BidibLibrary.BIDIB_PCFG_OUTPUT_MAP,
                        new BytePortConfigValue(ByteUtils.getLowByte(dmxMapping - DMX_MAPPING_OFFSET)));
                }

                setConfigX(node, LcOutputType.BACKLIGHTPORT, port.getId(), values);
            }
            else {
                BidibPort bidibPort =
                    BidibPort.prepareBidibPort(LcOutputType.BACKLIGHTPORT.getType(),
                        ByteUtils.getLowByte(port.getId()));
                LcConfig lcConfig =
                    new LcConfig(bidibPort, dimSlopeDown, dimSlopeUp, dmxMapping - DMX_MAPPING_OFFSET, 0);
                LOGGER.debug("Prepared lcConfig to send: {}", lcConfig);
                getBidib().getNode(node).setConfig(PortModelEnum.getPortModel(node), lcConfig);
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Set backlight port parameters failed.", ex);
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void setServoPortParameters(Node node, ServoPort port, int trimDown, int trimUp, int speed) {
        LOGGER.info("Set the servo parameters for port: {}, trimDown: {}, trimUp: {}, speed: {}", port, trimDown,
            trimUp, speed);
        try {
            if (node != null && node.getProtocolVersion().isHigherThan(ProtocolVersion.VERSION_0_5)) {

                Map<Byte, PortConfigValue<?>> values = new LinkedHashMap<>();

                // only send the known keys
                if (port.isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_SERVO_ADJ_L)) {
                    values.put(BidibLibrary.BIDIB_PCFG_SERVO_ADJ_L,
                        new BytePortConfigValue(ByteUtils.getLowByte(trimDown)));
                }
                if (port.isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_SERVO_ADJ_H)) {
                    values.put(BidibLibrary.BIDIB_PCFG_SERVO_ADJ_H,
                        new BytePortConfigValue(ByteUtils.getLowByte(trimUp)));
                }
                if (port.isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_SERVO_SPEED)) {
                    values.put(BidibLibrary.BIDIB_PCFG_SERVO_SPEED,
                        new BytePortConfigValue(ByteUtils.getLowByte(speed)));
                }

                if (MapUtils.isEmpty(values)) {
                    LOGGER.warn("No values to write to node available.");
                }

                setConfigX(node, LcOutputType.SERVOPORT, port.getId(), values);
            }
            else {
                BidibPort bidibPort =
                    BidibPort.prepareBidibPort(LcOutputType.SERVOPORT.getType(), ByteUtils.getLowByte(port.getId()));

                getBidib().getNode(node).setConfig(PortModelEnum.getPortModel(node),
                    new LcConfig(bidibPort, trimDown, trimUp, speed, 0));
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Set servo port parameters failed.", ex);
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void setSwitchPortParameters(
        Node node, SwitchPort port, IoBehaviourSwitchEnum outputBehaviour, int switchOffTime) {
        LOGGER.info("Set the switch port parameters, port: {}, ioBehaviour: {}, switchOffTime: {}", port,
            outputBehaviour, switchOffTime);
        try {
            if (node != null && node.getProtocolVersion().isHigherThan(ProtocolVersion.VERSION_0_5)) {

                Map<Byte, PortConfigValue<?>> values = new LinkedHashMap<>();

                // only send the known keys
                if (port.isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_SWITCH_CTRL)) {
                    values.put(BidibLibrary.BIDIB_PCFG_SWITCH_CTRL, new BytePortConfigValue(outputBehaviour.getType()));
                }
                if (port.isPortConfigKeySupported(BidibLibrary.BIDIB_PCFG_TICKS)) {
                    values.put(BidibLibrary.BIDIB_PCFG_TICKS,
                        new BytePortConfigValue(ByteUtils.getLowByte(switchOffTime)));
                }

                setConfigX(node, LcOutputType.SWITCHPORT, port.getId(), values);
            }
            else {
                BidibPort bidibPort =
                    BidibPort.prepareBidibPort(LcOutputType.SWITCHPORT.getType(), ByteUtils.getLowByte(port.getId()));

                getBidib().getNode(node).setConfig(PortModelEnum.getPortModel(node),
                    new LcConfig(bidibPort, ByteUtils.getInt(outputBehaviour.getType()), switchOffTime, 0, 0));
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Set switch port parameters failed.", ex);
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void setPortParameters(
        Node node, Port<?> port, LcOutputType portType, Map<Byte, PortConfigValue<?>> values) {
        LOGGER.info("Set the port parameters, port: {}, portType: {}, values: {}", port, portType, values);
        try {
            if (node != null && node.getProtocolVersion().isHigherThan(ProtocolVersion.VERSION_0_5)) {
                if (values == null) {
                    values = new LinkedHashMap<>();
                }
                if (node.isPortFlatModelAvailable() && portType != null) {
                    LOGGER.info("Add BIDIB_PCFG_RECONFIG for portType: {}", portType.getType());
                    // ignore port map
                    values.put(BidibLibrary.BIDIB_PCFG_RECONFIG,
                        new ReconfigPortConfigValue(ByteUtils.getInt(portType.getType()), 0x0000));
                }
                LOGGER.info("Prepared values for port type: {}, values: {}", portType, values);

                if (node.isPortFlatModelAvailable()) {
                    setConfigX(node, LcOutputType.SWITCHPORT, port.getId(), values);
                }
                else {
                    // fetch the port type
                    if (portType == null) {
                        portType = Port.getPortType(port);
                        LOGGER.info("Fetched the port type from the provided port: {}", portType);
                    }
                    else {
                        LOGGER.warn("Switch port for type-oriented model is not implemented.");
                    }
                    setConfigX(node, portType, port.getId(), values);
                }
            }
            else {
                LOGGER.warn("Change the port parameters is currently not supported for protocol version < 0.6");
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Set port parameters failed.", ex);
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void clearLocoBuffer(final Node node, int address) {
        LOGGER.info("Clear the DCC address from loco buffer: {}", address);

        try {
            getBidib().getCommandStationNode(node).clearLoco(address);
        }
        catch (ProtocolNoAnswerException ex) {
            LOGGER.warn("Clear address from loco buffer failed.", ex);
            throw new NoAnswerException("Clear address from loco buffer failed.", ex);
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Clear address from loco buffer failed.", ex);
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void setSpeed(
        Node node, int address, SpeedSteps speedSteps, Integer speed, Direction direction, BitSet activeFunctions,
        BitSet functions) {
        LOGGER.info(
            "set speed, address: {}, speedSteps: {}, speed: {}, direction: {}, activeFunctions: {}, functions: {}",
            address, speedSteps, speed, direction, activeFunctions, functions);
        try {
            getBidib().getCommandStationNode(node).setDrive(address, speedSteps.getType(), speed, direction.getType(),
                activeFunctions, functions);
        }
        catch (ProtocolNoAnswerException ex) {
            LOGGER.warn("Set speed failed.", ex);
            throw new NoAnswerException("Set speed failed.", ex);
        }
        catch (ProtocolException ex) {
            LOGGER.warn("Set speed failed.", ex);
            throw new RuntimeException(ex);
        }
    }

    @Override
    public AccessoryAcknowledge setDccAccessory(
        Node node, AddressData dccAddress, int aspect, Integer switchTime, TimeBaseUnitEnum timeBaseUnit,
        TimingControlEnum timingControl) {
        LOGGER.info("set DCC accessory, dccAddress: {}, aspect: {}", dccAddress, aspect);
        try {
            // TODO activate is fix set to 1
            int activate = 1;
            ActivateCoilEnum activateCoil = (activate != 0 ? ActivateCoilEnum.COIL_ON : ActivateCoilEnum.COIL_OFF);

            AccessoryAcknowledge acknowledge =
                setCsDccAccessory(node, dccAddress, activateCoil, aspect, switchTime, timeBaseUnit, timingControl);

            if (TimingControlEnum.COIL_ON_OFF.equals(timingControl)) {
                LOGGER.info("Schedule timer for coil off/on.");
                activateCoil = (activate != 0 ? ActivateCoilEnum.COIL_OFF : ActivateCoilEnum.COIL_ON);
                scheduleActivateCoil(node, dccAddress, activateCoil, aspect, switchTime, timeBaseUnit, timingControl);
            }
            return acknowledge;
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private AccessoryAcknowledge setCsDccAccessory(
        Node node, AddressData dccAddress, ActivateCoilEnum activateCoil, int aspect, Integer switchTime,
        TimeBaseUnitEnum timeBaseUnit, TimingControlEnum timingControl) throws ProtocolException {
        AccessoryAcknowledge acknowledge =
            getBidib().getCommandStationNode(node).setAccessory(dccAddress.getAddress(), dccAddress.getType(),
                timingControl, activateCoil, aspect, timeBaseUnit, switchTime);

        return acknowledge;
    }

    private void scheduleActivateCoil(
        final Node node, final AddressData dccAddress, final ActivateCoilEnum activateCoil, final int aspect,
        final Integer switchTime, final TimeBaseUnitEnum timeBaseUnit, final TimingControlEnum timingControl) {
        LOGGER.info(
            "Schedule activate coil, node: {}, dccAddress: {}, activateCoil: {}, switchTime: {}, timeBaseUnit: {}",
            node, dccAddress, activateCoil, switchTime, timeBaseUnit);

        int delay = 0;
        if (TimeBaseUnitEnum.UNIT_1S.equals(timeBaseUnit)) {
            delay = switchTime * 1000;
        }
        else {
            delay = switchTime * 10;
        }

        // use executor to send response
        dccAccessoryWorker.schedule(new Runnable() {
            @Override
            public void run() {
                LOGGER.info("Send accessory, node: {}, dccAddress: {}, activateCoil: {}", node, dccAddress,
                    activateCoil);
                try {
                    setCsDccAccessory(node, dccAddress, activateCoil, aspect, switchTime, timeBaseUnit, timingControl);
                }
                catch (ProtocolException ex) {
                    LOGGER.warn("Send dcc accessory command failed, address: {}", dccAddress, ex);
                }
                LOGGER.info("Send dcc accessory worker has finished.");
            }
        }, delay, TimeUnit.MILLISECONDS);
    }

    @Override
    public void startAccessory(Node node, Accessory accessory, int aspect) {
        LOGGER.info("Start accessory: {}, aspect: {}", accessory, aspect);
        try {
            // set the running state on the accessory
            accessory.setAccessoryExecutionState(AccessoryExecutionState.RUNNING, aspect);

            // and set the new aspect of the accessory
            getBidib().getAccessoryNode(node).setAccessoryState(accessory.getId(), aspect);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void queryAccessoryState(Node node, Accessory... accessory) {
        LOGGER.info("Query the accessory state, node: {}, accessory: {}", node, accessory);
        try {
            int[] accessoryIds = new int[accessory.length];
            for (int i = 0; i < accessoryIds.length; i++) {
                accessoryIds[i] = accessory[i].getId();
            }
            getBidib().getAccessoryNode(node).getAccessoryState(accessoryIds);
        }
        catch (Exception ex) {
            LOGGER.warn("Query the accessory states failed.", ex);
            throw new RuntimeException(ex);
        }
    }

    @Override
    public LcMacroState startMacro(
        org.bidib.wizard.mvc.main.model.Node node, Macro macro, boolean transferBeforeStart) {
        LcMacroState lcMacroState = null;
        try {
            LOGGER.info("Start macro: {}, transferBeforeStart: {}", macro, transferBeforeStart);
            if (transferBeforeStart) {
                transferMacro(node, macro);
            }

            LOGGER.info("Send start macro command for macro: {}", macro);
            lcMacroState =
                getBidib().getAccessoryNode(node.getNode()).handleMacro(macro.getId(), LcMacroOperationCode.START);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
        return lcMacroState;
    }

    @Override
    public LcMacroState stopMacro(Node node, Macro macro) {
        LcMacroState lcMacroState = null;
        try {
            lcMacroState = getBidib().getAccessoryNode(node).handleMacro(macro.getId(), LcMacroOperationCode.OFF);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
        return lcMacroState;
    }

    @Override
    public LcMacroState saveMacro(org.bidib.wizard.mvc.main.model.Node node, Macro macro) {
        LcMacroState lcMacroState = null;
        try {
            // transfer the macro to the node ...
            transferMacro(node, macro);

            // and store the macro permanently
            lcMacroState =
                getBidib().getAccessoryNode(node.getNode()).handleMacro(macro.getId(), LcMacroOperationCode.SAVE);

            // macro was permanently stored on the node
            macro.setMacroSaveState(MacroSaveState.PERMANENTLY_STORED_ON_NODE);
        }
        catch (InvalidConfigurationException ex) {
            LOGGER.warn("Transfer macro failed.", ex);
            throw ex;
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
        return lcMacroState;
    }

    @Override
    public LcMacroState reloadMacro(org.bidib.wizard.mvc.main.model.Node node, Macro macro) {
        LcMacroState lcMacroState = null;
        try {
            lcMacroState =
                getBidib().getAccessoryNode(node.getNode()).handleMacro(macro.getId(), LcMacroOperationCode.RESTORE);
            // clear the stored functions
            macro.clearFunctions();

            // get the macro points of the macro
            getMacroContent(node, macro);
        }
        catch (InvalidConfigurationException ex) {
            LOGGER.warn("Reload macro failed.", ex);
            throw ex;
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
        return lcMacroState;
    }

    @Override
    public void transferMacro(final org.bidib.wizard.mvc.main.model.Node node, Macro macro) {
        LOGGER.info("Transfer macro: {} to node: {}", macro, node);
        try {
            AccessoryNode accessoryNode = getBidib().getAccessoryNode(node.getNode());

            if (accessoryNode != null) {
                // delete the macro
                LcMacroState macroState = accessoryNode.handleMacro(macro.getId(), LcMacroOperationCode.DELETE);
                LOGGER.info("Delete macro returned: {}", macroState);

                LOGGER.info("Transfer the macro points.");

                final PortModelEnum portModel = PortModelEnum.getPortModel(node.getNode());

                // add the steps
                MacroConversionHelper helper = new MacroConversionHelper();
                int stepNumber = helper.prepareLcMacroSteps(accessoryNode, portModel, macro);

                // if there's a free entry add the end_of_macro step
                if (macro.getFunctionCount() < macro.getFunctionSize()) {
                    LOGGER.info("Prepare the end of macro point.");
                    accessoryNode.setMacro(new LcMacro(ByteUtils.getLowByte(macro.getId()),
                        ByteUtils.getLowByte(stepNumber++), ByteUtils.getLowByte(255),
                        LcOutputType.END_OF_MACRO.getType(), (byte) 0, ByteUtils.getLowByte(0)));
                }

                // set the execution time
                LOGGER.info("Set the execution time parameters.");
                accessoryNode.setMacroParameter(macro.getId(), BidibLibrary.BIDIB_MACRO_PARA_START_CLK,
                    MacroUtils.MACRO_START_OFF);

                accessoryNode.setMacroParameter(macro.getId(), BidibLibrary.BIDIB_MACRO_PARA_SLOWDOWN,
                    (byte) macro.getSpeed());
                accessoryNode.setMacroParameter(macro.getId(), BidibLibrary.BIDIB_MACRO_PARA_REPEAT,
                    (byte) macro.getCycles());

                for (StartCondition startCondition : macro.getStartConditions()) {
                    if (startCondition instanceof TimeStartCondition) {
                        Calendar time = ((TimeStartCondition) startCondition).getTime();
                        LOGGER.info("Use time as start condition: {}", time.getTime());
                        if (time != null) {
                            byte[] parameter = new byte[4];
                            int minute = time.get(Calendar.MINUTE);

                            if (((TimeStartCondition) startCondition).getRepeatTime() == MacroRepeatTime.MINUTELY) {
                                minute = 60;
                            }
                            else if (((TimeStartCondition) startCondition)
                                .getRepeatTime() == MacroRepeatTime.QUARTER_HOURLY) {
                                minute = 61;
                            }
                            else if (((TimeStartCondition) startCondition)
                                .getRepeatTime() == MacroRepeatTime.HALF_HOURLY) {
                                minute = 62;
                            }
                            LOGGER.info("Value for minute: {}", minute);
                            // the minute
                            parameter[0] = SysClockMessage.getMinute(minute);
                            // the hour
                            MacroRepeatTime repeatTime = ((TimeStartCondition) startCondition).getRepeatTime();
                            parameter[1] =
                                SysClockMessage
                                    .getHour(repeatTime != MacroRepeatTime.NONE ? 24 : time.get(Calendar.HOUR_OF_DAY));
                            // the day of week
                            MacroRepeatDay repeatDay = ((TimeStartCondition) startCondition).getRepeatDay();
                            if (repeatDay == null && repeatTime == MacroRepeatTime.NONE) {
                                // of no repetition fire on every day
                                repeatDay = MacroRepeatDay.ALL;
                                LOGGER.info("Set repeat day to every day.");
                            }
                            parameter[2] = SysClockMessage.getDay(repeatDay.ordinal());
                            // the acceleration factor
                            parameter[3] = SysClockMessage.getAccelerationFactor(0);
                            LOGGER.info(
                                "Set macro start clk with param[0]: {}, param[1]: {}, param[2]: {}, param[3]: {}",
                                new Object[] { ByteUtils.getInt(parameter[0]), ByteUtils.getInt(parameter[1]),
                                    ByteUtils.getInt(parameter[2]), ByteUtils.getInt(parameter[3]) });
                            accessoryNode.setMacroParameter(macro.getId(), BidibLibrary.BIDIB_MACRO_PARA_START_CLK,
                                parameter);
                        }
                    }
                }
                LOGGER.info("Transfer macro has finished.");

                // macro was transfered, reset the pending changes
                macro.setMacroSaveState(MacroSaveState.SAVED_ON_NODE);
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Transfer macro has failed.", ex);
            throw new RuntimeException(ex);
        }
    }

    @Override
    public List<ConfigurationVariable> getConfigurationVariables(
        Node node, List<ConfigurationVariable> configVariables) {
        // we must know what CVs to fetch. This could be done with the configured CVs template of the BiDiB Monitor
        // prepare the CVs
        LOGGER.info("Get the CVs from the node, count: {}", configVariables.size());

        List<ConfigurationVariable> cvList = null;
        // TODO allow to force a reload of the CV values from the node
        if (cvList == null) {
            cvList = new LinkedList<ConfigurationVariable>();
        }

        // store the CVs in a map to have direct access
        LinkedHashMap<String, ConfigurationVariable> map = new LinkedHashMap<String, ConfigurationVariable>();
        for (ConfigurationVariable cv : configVariables) {
            map.put(cv.getName(), cv);
        }

        //
        boolean vendorEnabled = false;
        try {
            vendorEnabled = getBidib().getNode(node).vendorEnable(node.getUniqueId());

            if (vendorEnabled) {

                // use bulk read always
                boolean useBulkRead = true;

                if (useBulkRead) {
                    // prepare the list with all configuration variables to read
                    List<ConfigurationVariable> cvToLoad = new LinkedList<ConfigurationVariable>(configVariables);
                    int minCvNumberToSkip = -1;
                    int maxCvNumberToSkip = -1;
                    String cvNumberToCheck = null;

                    while (cvToLoad.size() > 0) {
                        // prepare the numbers
                        List<String> cvNumbers = new LinkedList<String>();

                        for (ConfigurationVariable cv : cvToLoad) {
                            // set the default values
                            cv.setValue("?");
                            cv.setTimeout(true);

                            // if CV name is a number we must check if we must skip this CV
                            String cvName = cv.getName();
                            if (minCvNumberToSkip > -1 && maxCvNumberToSkip > -1 && StringUtils.isNumeric(cvName)) {
                                int cvNumberInt = Integer.valueOf(cv.getName());
                                // check if we must skip the CV
                                if (cvNumberInt >= minCvNumberToSkip && cvNumberInt <= maxCvNumberToSkip) {

                                    // skip
                                    LOGGER.debug("Skip read of CV number: {}", cvNumberInt);
                                    cv.setValue("?");
                                    cv.setTimeout(true);
                                    // add to list of CV to return
                                    cvList.add(cv);

                                    continue;
                                }
                                LOGGER.debug("Read CV number is not skipped: {}", cvNumberInt);
                                minCvNumberToSkip = -1;
                                maxCvNumberToSkip = -1;
                            }

                            LOGGER.trace("Add CV to read: {}", cv);

                            cvNumbers.add(cv.getName());

                            cvList.add(cv);

                            if (cv.isSkipOnTimeout()) {
                                cvNumberToCheck = cvName;
                                minCvNumberToSkip = cv.getMinCvNumber();
                                maxCvNumberToSkip = cv.getMaxCvNumber();
                                LOGGER.info(
                                    "The CV is configured as skipOnTimeout provider, minCvNumberToSkip: {}, maxCvNumberToSkip: {}, cvNumberToCheck: {}",
                                    minCvNumberToSkip, maxCvNumberToSkip, cvNumberToCheck);
                                // leave the for loop and read the prepared CV values
                                break;
                            }
                        }
                        // remove the cv that are already prepared to load
                        cvToLoad.removeAll(cvList);

                        List<VendorData> vendorDatas = null;

                        if (cvNumbers.size() > 0) {
                            try {
                                // get the cv values with bulk read
                                vendorDatas = getBidib().getNode(node).vendorGetBulk(cvNumbers);
                            }
                            catch (ProtocolNoAnswerException ex) {
                                LOGGER.warn("Get vendorData with bulk read failed.", ex);
                            }
                        }
                        else {
                            LOGGER.info("No CV values to get.");
                        }
                        cvNumbers.clear();

                        if (vendorDatas != null) {
                            for (VendorData vendorData : vendorDatas) {
                                if (vendorData != null) {
                                    String cvNumber = vendorData.getName();
                                    String value = getVendorDataValue(vendorData, cvNumber);
                                    LOGGER.info("Read value of CV with number: {}, value: {}", cvNumber, value);

                                    ConfigurationVariable cv = map.get(cvNumber);
                                    if (cv != null) {
                                        cv.setValue(value);
                                        cv.setTimeout(false);
                                    }
                                    else {
                                        LOGGER.warn("No CV found with cvNumber: {}", cvNumber);
                                    }
                                }
                            }
                        }
                        else {
                            LOGGER.warn("No vendor data received.");
                        }

                        if (cvNumberToCheck != null) {
                            // we must check for timeout
                            ConfigurationVariable cv = map.get(cvNumberToCheck);
                            if (cv != null && cv.isTimeout()) {
                                // timeout on check cv, skip the next values
                                LOGGER.warn("Found CV that is marked for skipOnTimeout and has timeout flag set: {}",
                                    cvNumberToCheck);
                            }
                            else {
                                // reset the check for skip on timeout
                                cvNumberToCheck = null;
                                minCvNumberToSkip = -1;
                                maxCvNumberToSkip = -1;
                            }
                        }
                    }
                    LOGGER.info("Bulk read CV values has finished.");
                }
                else {
                    // get the configured CVs from the vendor CV file ...
                    int minCvNumberToSkip = -1;
                    int maxCvNumberToSkip = -1;
                    for (ConfigurationVariable cv : configVariables) {

                        // if CV name is a number we must check if we must skip this CV
                        String cvName = cv.getName();
                        if (minCvNumberToSkip > -1 && maxCvNumberToSkip > -1) {
                            int cvNumberInt = Integer.valueOf(cv.getName());
                            // check if we must skip the CV
                            if (cvNumberInt >= minCvNumberToSkip && cvNumberInt <= maxCvNumberToSkip) {
                                // skip
                                LOGGER.warn("Skip read of CV number: {}", cvNumberInt);
                                cv.setValue("?");
                                cv.setTimeout(true);
                                continue;
                            }
                            LOGGER.info("Read CV number is not skipped: {}", cvNumberInt);
                            minCvNumberToSkip = -1;
                            maxCvNumberToSkip = -1;
                        }

                        try {
                            // read the CV from the node
                            VendorData vendorData = getBidib().getNode(node).vendorGet(String.valueOf(cvName));
                            if (vendorData != null) {
                                String value = getVendorDataValue(vendorData, cvName);
                                LOGGER.debug("Read value of CV with name: {}, value: {}", cvName, value);
                                cv.setValue(value);
                                cv.setTimeout(false);
                            }
                            else {
                                LOGGER.warn("No value received for CV number: {}", cvName);
                                cv.setValue("?");
                                cv.setTimeout(true);

                                if (cv.isSkipOnTimeout()) {
                                    minCvNumberToSkip = cv.getMinCvNumber();
                                    maxCvNumberToSkip = cv.getMaxCvNumber();
                                    LOGGER.info(
                                        "The cv is configured as skipOnTimeout provider, minCvNumberToSkip: {}, maxCvNumberToSkip: {}",
                                        minCvNumberToSkip, maxCvNumberToSkip);
                                }
                            }
                            cvList.add(cv);
                        }
                        catch (Exception ex) {
                            LOGGER.warn("Read CV value failed.", ex);
                            throw new RuntimeException(ex);
                        }
                    }
                }
            }
        }
        catch (ProtocolException e) {
            LOGGER.warn("Get configuration variables failed.", e);
            throw new RuntimeException(e);
        }
        finally {
            if (vendorEnabled) {
                try {
                    getBidib().getNode(node).vendorDisable();
                }
                catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        }
        return Collections.unmodifiableList(cvList);
    }

    @Override
    public List<ConfigurationVariable> writeConfigurationVariables(
        Node node, List<ConfigurationVariable> configVariables) {

        boolean vendorEnabled = false;
        try {
            vendorEnabled = getBidib().getNode(node).vendorEnable(node.getUniqueId());
            if (vendorEnabled) {
                // iterate over the changed values
                for (ConfigurationVariable cv : configVariables) {

                    try {
                        String cvName = cv.getName();
                        String cvValue = cv.getValue();

                        LOGGER.info("Sending CV name: {}, value: {}", cvName, cvValue);

                        VendorData response = getBidib().getNode(node).vendorSet(cv.getName(), cv.getValue());
                        String responseValue = getVendorDataValue(response, cvName);

                        LOGGER.info("Returned from vendorSet, CV: {}, value is: {}", cvName, response);
                        if (!cvValue.equals(responseValue)) {
                            LOGGER.warn("The new value was not accepted! CV: {}, response value: {}, request value: {}",
                                cvName, responseValue, cvValue);
                        }
                    }
                    catch (Exception ex) {
                        LOGGER.warn("Write CV to node failed.", ex);
                        cv.setTimeout(true);
                    }
                }
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Write vendor CV failed.", ex);
            throw new RuntimeException(ex);
        }
        finally {
            if (vendorEnabled) {
                try {
                    getBidib().getNode(node).vendorDisable();
                }
                catch (Exception e) {
                    LOGGER.warn("Send vendorDisable failed.");
                    throw new RuntimeException(e);
                }
            }
        }
        // TODO we should remove the values that were not accepted by the node ...
        return Collections.unmodifiableList(configVariables);
    }

    /**
     * Parse the vendor data value.
     * 
     * @param vendorData
     *            the vendor data
     * @return valid number value or -1 if parse to integer failed or vendorData is null
     */
    private String getVendorDataValue(final VendorData vendorData, String cvNumber) {
        try {
            if (vendorData != null) {
                return vendorData.getValue();
            }
            LOGGER.warn("No vendor data value received for CV: {}, result: {}", cvNumber, vendorData);
        }
        catch (Exception ex) {
            LOGGER.warn("Get the vendor data value failed for CV: {}, result: {}, cause: {}", cvNumber, vendorData,
                ex.getMessage());
        }
        return null;
    }

    // @Override
    // public int readCv(Node node, int cvNumber) {
    // VendorData result = null;
    //
    // boolean vendorEnabled = false;
    // try {
    // vendorEnabled = getBidib().getNode(node).vendorEnable(node.getUniqueId());
    // if (vendorEnabled) {
    // result = getBidib().getNode(node).vendorGet(String.valueOf(cvNumber));
    // }
    // }
    // catch (ProtocolException e) {
    // throw new RuntimeException(e);
    // }
    // finally {
    // if (vendorEnabled) {
    // try {
    // getBidib().getNode(node).vendorDisable();
    // }
    // catch (Exception e) {
    // throw new RuntimeException(e);
    // }
    // }
    // }
    //
    // return getVendorDataValue(result, cvNumber);
    // }

    @Override
    public PomAcknowledge sendCvPomRequest(
        Node node, AddressData locoAddress, CommandStationPom opCode, int cvNumber, int cvValue) {

        try {
            PomAcknowledge pomAck = null;
            CommandStationNode csNode = getBidib().getCommandStationNode(node);
            switch (opCode) {
                case WR_BYTE:
                case WR_BIT:
                    pomAck = csNode.writePom(locoAddress, opCode, cvNumber, cvValue);
                    break;
                // TODO add support for XWR_BYTE2
                case XWR_BYTE2:
                    pomAck = csNode.writePom(locoAddress, opCode, cvNumber, cvValue);
                    break;

                default:
                    pomAck = csNode.readPom(locoAddress, opCode, cvNumber);
                    break;
            }
            return pomAck;
        }
        catch (ProtocolException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public PomAcknowledge sendReadCvPomRequest(
        Node node, AddressData locoAddress, CommandStationPom opCode, int cvNumber) {

        try {
            PomAcknowledge pomAck = null;
            CommandStationNode csNode = getBidib().getCommandStationNode(node);
            switch (opCode) {
                case WR_BYTE:
                case WR_BIT:
                case X_WR_BIT:
                case X_WR_BYTE1:
                case X_WR_BYTE2:
                case X_WR_BYTE3:
                case X_WR_BYTE4:
                case XWR_BYTE1:
                case XWR_BYTE2:
                    throw new IllegalArgumentException("No write command allowed without cvValue!");
                default:
                    pomAck = csNode.readPom(locoAddress, opCode, cvNumber);
                    break;
            }
            return pomAck;
        }
        catch (ProtocolException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public PomAcknowledge sendReadCvPomRequest(
        Node node, DecoderIdAddressData locoAddress, CommandStationPom opCode, int cvNumber) {

        try {
            PomAcknowledge pomAck = null;
            CommandStationNode csNode = getBidib().getCommandStationNode(node);
            switch (opCode) {
                case WR_BYTE:
                case WR_BIT:
                case X_WR_BIT:
                case X_WR_BYTE1:
                case X_WR_BYTE2:
                case X_WR_BYTE3:
                case X_WR_BYTE4:
                case XWR_BYTE1:
                case XWR_BYTE2:
                    throw new IllegalArgumentException("No write command allowed without cvValue!");
                default:
                    pomAck = csNode.readPom(locoAddress, opCode, cvNumber);
                    break;
            }
            return pomAck;
        }
        catch (ProtocolException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void sendCvPtRequest(Node node, CommandStationPt opCode, int cvNumber, int cvValue) {

        try {
            CommandStationNode csNode = getBidib().getCommandStationNode(node);
            switch (opCode) {
                case BIDIB_CS_PROG_WR_BYTE:
                    csNode.writePt(opCode, cvNumber, cvValue);
                    break;
                case BIDIB_CS_PROG_RDWR_BIT:
                    csNode.writePt(opCode, cvNumber, cvValue);
                    break;
                default:
                    csNode.readPt(opCode, cvNumber);
                    break;
            }
        }
        catch (ProtocolException e) {
            throw new RuntimeException(e);
        }
    }

    private void fireStatus(String statusText, int displayDuration) {
        List<CommunicationListener> communicationListeners = new LinkedList<>(this.communicationListeners);
        for (CommunicationListener listener : communicationListeners) {
            listener.status(statusText, displayDuration);
        }
    }

    private void fireOpening() {
        List<CommunicationListener> communicationListeners = new LinkedList<>(this.communicationListeners);
        for (CommunicationListener listener : communicationListeners) {
            listener.opening();
        }
    }

    private void fireOpened(String port) {
        List<CommunicationListener> communicationListeners = new LinkedList<>(this.communicationListeners);
        for (CommunicationListener listener : communicationListeners) {
            listener.opened(port);
        }
    }

    private void fireInitialized() {
        List<CommunicationListener> communicationListeners = new LinkedList<>(this.communicationListeners);
        for (CommunicationListener listener : communicationListeners) {
            listener.initialized();
        }
    }

    private void fireClosed(String port) {
        List<CommunicationListener> communicationListeners = new LinkedList<>(this.communicationListeners);
        for (CommunicationListener listener : communicationListeners) {
            listener.closed(port);
        }
    }

    private void fireStatusMessage(String statusText, int displayDuration) {
        List<CommunicationListener> communicationListeners = new LinkedList<>(this.communicationListeners);
        for (CommunicationListener listener : communicationListeners) {
            listener.status(statusText, displayDuration);
        }
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer(getClass().getSimpleName());
        sb.append("connected port: ").append(connectedPortType);
        return sb.toString();
    }

    @Override
    public void setResetReconnectDelay(int resetReconnectDelay) {
        LOGGER.info("Set the new resetReconnectDelay: {}", resetReconnectDelay);
        this.resetReconnectDelay = resetReconnectDelay;
    }

    @Override
    public void setResponseTimeout(int responseTimeout) {
        LOGGER.info("Set the new response timeout: {}", responseTimeout);
        this.responseTimeout = responseTimeout;
    }

    @Override
    public void addTransferListener(TransferListener listener) {
        LOGGER.info("Add transfer listener.");
        transferListeners.add(listener);
    }

    @Override
    public void addMessageListener(MessageListener listener) {
        LOGGER.info("Add message listener.");
        synchronized (messageListeners) {
            messageListeners.add(listener);
        }

        if (isOpened()) {
            BidibMessageProcessor messageReceiver = getBidib().getMessageReceiver();
            messageReceiver.addMessageListener(listener);
        }
    }

    @Override
    public void removeMessageListener(MessageListener listener) {
        LOGGER.info("remove message listener.");
        synchronized (messageListeners) {
            messageListeners.remove(listener);
        }

        if (isOpened()) {
            BidibMessageProcessor messageReceiver = getBidib().getMessageReceiver();
            messageReceiver.removeMessageListener(listener);
        }
    }

    @Override
    public void addNodeListener(NodeListener nodeListener) {
        LOGGER.info("Add node listener: {}", nodeListener);
        synchronized (nodeListeners) {
            nodeListeners.add(nodeListener);
        }

        if (getBidib() != null) {
            // add node listener to bidib instance
            BidibMessageProcessor messageReceiver = getBidib().getMessageReceiver();
            if (messageReceiver != null) {
                LOGGER.info("Add node listener to message receiver.");
                messageReceiver.addNodeListener(nodeListener);
            }
        }
    }

    @Override
    public void removeNodeListener(NodeListener nodeListener) {
        LOGGER.info("Remove node listener: {}", nodeListener);
        synchronized (nodeListeners) {
            nodeListeners.remove(nodeListener);
        }

        if (getBidib() != null) {
            // add node listener to bidib instance
            BidibMessageProcessor messageReceiver = getBidib().getMessageReceiver();
            if (messageReceiver != null) {
                LOGGER.info("Remove node listener from message receiver.");
                messageReceiver.removeNodeListener(nodeListener);
            }
        }
    }

    @Override
    public String setString(Node node, int namespace, int index, String value) {
        LOGGER.debug("Set the string value, namespace: {}, index: {}, value: {}, node: {}", namespace, index, value,
            node);
        try {
            StringData stringData = getBidib().getNode(node).setString(namespace, index, value);
            if (stringData != null) {
                if (namespace != stringData.getNamespace() || index != stringData.getIndex()) {
                    throw new RuntimeException("The returned namespace or index do not match the requested values.");
                }
                LOGGER.debug("Returned string data: '{}'", stringData);
                String returnedValue = stringData.getValue();
                node.setStoredString(index, returnedValue);
                return returnedValue;
            }
            LOGGER.warn("No string data received.");
            return null;
        }
        catch (Exception e) {
            LOGGER.warn("Set string data on node failed.", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public String getString(Node node, int namespace, int index) {
        LOGGER.debug("Get the string value, namespace: {}, index: {}, node: {}", namespace, index, node);
        try {
            StringData stringData = getBidib().getNode(node).getString(namespace, index);
            if (stringData != null) {
                if (namespace != stringData.getNamespace() || index != stringData.getIndex()) {
                    throw new RuntimeException("The returned namespace or index do not match the requested values.");
                }
                LOGGER.info("Returned string data: '{}'", stringData);
                return stringData.getValue();
            }
            LOGGER.warn("No string data received.");
            return null;
        }
        catch (Exception e) {
            LOGGER.warn("Get string data from node failed.", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void queryPortConfig(Node node, List<GenericPort> ports) {

        LOGGER.info("Query the port config for ports: {}", ports);
        if (CollectionUtils.isNotEmpty(ports)) {
            try {
                BidibNode bidibNode = getBidib().getNode(node);

                int portCount = ports.size();

                int[] outputNumbers = new int[portCount];
                for (int index = 0; index < portCount; index++) {
                    outputNumbers[index] = index;
                }
                bidibNode.getConfigXBulk(PortModelEnum.getPortModel(node), LcOutputType.SWITCHPORT /* ignored */,
                    1 /* window size */, outputNumbers);
            }
            catch (Exception e) {
                LOGGER.warn("Query port config failed.", e);
                throw new RuntimeException(e);
            }
        }
        else {
            LOGGER.warn("No ports available on node.");
        }
    }

    @Override
    public void queryAllPortConfig(Node node, LcOutputType outputType, Integer rangeFrom, Integer rangeTo) {

        LOGGER.info("Query the port config for all ports of the node: {}, outputType: {}, rangeFrom: {}, rangeTo: {}",
            node, outputType, rangeFrom, rangeTo);

        try {
            BidibNode bidibNode = getBidib().getNode(node);

            int totalPorts = 0;
            if (node.getPortFlatModel() != null && node.getPortFlatModel().intValue() > 0) {

                totalPorts = node.getPortFlatModel().intValue();

                rangeFrom = 0;
                rangeTo = totalPorts;
            }
            else {

                if (rangeTo != null && rangeTo.intValue() > 0) {
                    LOGGER.info("Use delivered port ranges!");
                }
                else {
                    List<?> ports = analogPorts.get(node.getUniqueId());
                    if (ports != null) {
                        totalPorts += ports.size();
                    }

                    ports = backlightPorts.get(node.getUniqueId());
                    if (ports != null) {
                        totalPorts += ports.size();
                    }

                    ports = lightPorts.get(node.getUniqueId());
                    if (ports != null) {
                        totalPorts += ports.size();
                    }

                    ports = motorPorts.get(node.getUniqueId());
                    if (ports != null) {
                        totalPorts += ports.size();
                    }

                    ports = servoPorts.get(node.getUniqueId());
                    if (ports != null) {
                        totalPorts += ports.size();
                    }

                    ports = soundPorts.get(node.getUniqueId());
                    if (ports != null) {
                        totalPorts += ports.size();
                    }

                    ports = switchPorts.get(node.getUniqueId());
                    if (ports != null) {
                        totalPorts += ports.size();
                    }

                    rangeFrom = 0;
                    rangeTo = totalPorts;
                }
            }

            bidibNode.getAllConfigX(PortModelEnum.getPortModel(node), outputType, rangeFrom, rangeTo);
        }
        catch (Exception ex) {
            LOGGER.warn("Query all port config failed.", ex);
            throw new RuntimeException(ex);
        }
    }

    @Override
    public Boolean isRailComPlusAvailable(Node node) {
        Boolean result = null;

        try {
            BidibNode bidibNode = getBidib().getNode(node);

            if (bidibNode != null) {
                Feature feature = Feature.findFeature(node.getFeatures(), BidibLibrary.FEATURE_GEN_RCPLUS_AVAILABLE);
                if (feature != null) {
                    // node.setFeature(feature);
                    result = feature.getValue() > 0;
                }
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Check if the RailCom+ feature is available failed.", ex);
            throw new RuntimeException(ex);
        }
        return result;
    }

    @Override
    public void getRcPlusTid(Node node) {
        try {
            getBidib().getCommandStationNode(node).getRcPlusTid();
        }
        catch (Exception ex) {
            LOGGER.warn("Get the RcPlus TID failed.", ex);
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void setRcPlusTid(Node node, TidData tid) {
        LOGGER.info("Set the TID on the command station node: {}", tid);
        try {
            getBidib().getCommandStationNode(node).setRcPlusTid(tid);
        }
        catch (Exception ex) {
            LOGGER.warn("Set the RcPlus TID failed.", ex);
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void sendPingOnce(Node node, RcPlusPhase phase) {
        LOGGER.info("Send the PingOnce with phase: {}", phase);
        try {
            getBidib().getCommandStationNode(node).sendPingOnce(phase);
        }
        catch (Exception ex) {
            LOGGER.warn("Send the RcPlus pingOnce failed.", ex);
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void sendPing(Node node, int interval) {
        LOGGER.info("Send the Ping with interval: {}", interval);
        try {
            getBidib().getCommandStationNode(node).sendPing(interval);
        }
        catch (Exception ex) {
            LOGGER.warn("Send the RcPlus ping failed.", ex);
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void sendFind(Node node, RcPlusPhase phase, DecoderUniqueIdData decoder) {
        LOGGER.info("Send the Find with phase: {}", phase);
        try {
            getBidib().getCommandStationNode(node).sendFind(phase, decoder);
        }
        catch (Exception ex) {
            LOGGER.warn("Send the RcPlus find failed.", ex);
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void sendBind(Node node, RcPlusBindData bindData) {
        LOGGER.info("Send the Bind with bindData: {}", bindData);
        try {
            getBidib().getCommandStationNode(node).sendBind(bindData);
        }
        catch (Exception ex) {
            LOGGER.warn("Send the RcPlus bind failed.", ex);
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void notifyOpening() {
        LOGGER.info("Opening port.");
        fireOpening();
    }
}
