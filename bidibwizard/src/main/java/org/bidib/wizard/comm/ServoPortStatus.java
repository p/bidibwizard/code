package org.bidib.wizard.comm;

import org.bidib.jbidibc.core.enumeration.ServoPortEnum;
import org.bidib.wizard.common.locale.Resources;

public enum ServoPortStatus implements BidibStatus {
    // @formatter:off
    START(ServoPortEnum.START, "start");
    // @formatter:on

    private final ServoPortEnum type;

    private final String key;

    private final String label;

    ServoPortStatus(ServoPortEnum type, String key) {
        this.type = type;
        this.key = key;
        this.label = Resources.getString(ServoPortStatus.class, key);
    }

    public ServoPortEnum getType() {
        return type;
    }

    public String getKey() {
        return key;
    }

    public String toString() {
        return label;
    }

    public static ServoPortStatus valueOf(ServoPortEnum type) {
        ServoPortStatus result = null;

        for (ServoPortStatus e : values()) {
            if (e.type == type) {
                result = e;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("cannot map " + type + " to a servo port status");
        }
        return result;
    }

    @Override
    public BidibStatus[] getValues() {
        return new BidibStatus[] { START };
    }
}
