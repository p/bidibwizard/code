package org.bidib.wizard.comm;

import org.bidib.jbidibc.core.enumeration.AccessoryOkayEnum;
import org.bidib.wizard.common.locale.Resources;

public enum AccessoryOkayStatus implements BidibStatus {
    // @formatter:off
    NO_FEEDBACK(AccessoryOkayEnum.NO_FEEDBACK, "no_feedback"), QUERY0(AccessoryOkayEnum.QUERY0, "query0"), QUERY1(
        AccessoryOkayEnum.QUERY1, "query1");
    // @formatter:on

    private final AccessoryOkayEnum type;

    private final String key;

    private final String label;

    AccessoryOkayStatus(AccessoryOkayEnum type, String key) {
        this.type = type;
        this.key = key;
        this.label = Resources.getString(AccessoryOkayStatus.class, key);
    }

    public AccessoryOkayEnum getType() {
        return type;
    }

    public String getKey() {
        return key;
    }

    public String toString() {
        return label;
    }

    public static AccessoryOkayStatus valueOf(AccessoryOkayEnum type) {
        AccessoryOkayStatus result = null;

        for (AccessoryOkayStatus e : values()) {
            if (e.type == type) {
                result = e;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("cannot map " + type + " to a accessory okay status");
        }
        return result;
    }

    @Override
    public BidibStatus[] getValues() {
        return new BidibStatus[] { NO_FEEDBACK, QUERY0, QUERY1 };
    }
}
