package org.bidib.wizard.comm;

import org.bidib.jbidibc.core.enumeration.SoundPortEnum;
import org.bidib.wizard.common.locale.Resources;

public enum SoundPortStatus implements BidibStatus {
    // @formatter:off
    PLAY(SoundPortEnum.PLAY, "play"), STOP(SoundPortEnum.STOP, "stop"), TEST(SoundPortEnum.TEST, "test");
    // @formatter:on

    private final SoundPortEnum type;

    private final String key;

    private final String label;

    SoundPortStatus(SoundPortEnum type, String key) {
        this.type = type;
        this.key = key;
        this.label = Resources.getString(SoundPortStatus.class, key);
    }

    public SoundPortEnum getType() {
        return type;
    }

    public String getKey() {
        return key;
    }

    public String toString() {
        return label;
    }

    public static SoundPortStatus valueOf(SoundPortEnum type) {
        SoundPortStatus result = null;

        for (SoundPortStatus e : values()) {
            if (e.type == type) {
                result = e;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("cannot map " + type + " to a sound port status");
        }
        return result;
    }

    @Override
    public BidibStatus[] getValues() {
        return new BidibStatus[] { PLAY, STOP };
    }
}
