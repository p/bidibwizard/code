package org.bidib.wizard.comm.exception;

public class InternalStartupException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public InternalStartupException(String message) {
        super(message);
    }
}
