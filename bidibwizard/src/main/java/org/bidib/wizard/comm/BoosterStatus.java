package org.bidib.wizard.comm;

import org.bidib.jbidibc.core.enumeration.BoosterState;
import org.bidib.wizard.common.locale.Resources;

public enum BoosterStatus {
    // @formatter:off
    OFF(BoosterState.OFF, Resources.getString(BoosterStatus.class, "off")), SHORT(BoosterState.OFF_SHORT, Resources
        .getString(BoosterStatus.class, "short")), OFF_HOT(BoosterState.OFF_HOT, Resources.getString(
        BoosterStatus.class, "off_hot")), NO_POWER(BoosterState.OFF_NO_POWER, Resources.getString(BoosterStatus.class,
        "no_power")), GO_REQ(BoosterState.OFF_GO_REQ, Resources.getString(BoosterStatus.class, "go_req")), OFF_HERE(
        BoosterState.OFF_HERE, Resources.getString(BoosterStatus.class, "off_here")), OFF_NO_DCC(
        BoosterState.OFF_NO_DCC, Resources.getString(BoosterStatus.class, "off_no_dcc")), ON(BoosterState.ON, Resources
        .getString(BoosterStatus.class, "on")), ON_LIMIT(BoosterState.ON_LIMIT, Resources.getString(
        BoosterStatus.class, "on_limit")), ON_HOT(BoosterState.ON_HOT, Resources.getString(BoosterStatus.class,
        "on_hot")), ON_STOP_REQ(BoosterState.ON_STOP_REQ, Resources.getString(BoosterStatus.class, "on_stop_req")), ON_HERE(
        BoosterState.ON_HERE, Resources.getString(BoosterStatus.class, "on_here"));
    // @formatter:on

    private final BoosterState boosterState;

    private final String label;

    BoosterStatus(BoosterState boosterState, String label) {
        this.boosterState = boosterState;
        this.label = label;
    }

    public BoosterState getBoosterState() {
        return boosterState;
    }

    public String toString() {
        return label;
    }

    public static boolean isOffState(BoosterStatus boosterState) {
        return isOffState(boosterState.getBoosterState());
    }

    public static boolean isOffState(BoosterState boosterState) {
        return ((boosterState.getType() & 0x80) == 0);
    }

    public static BoosterStatus valueOf(BoosterState boosterState) {
        BoosterStatus result = null;

        for (BoosterStatus e : values()) {
            if (e.boosterState == boosterState) {
                result = e;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("cannot map " + boosterState + " to a booster status");
        }
        return result;
    }
}
