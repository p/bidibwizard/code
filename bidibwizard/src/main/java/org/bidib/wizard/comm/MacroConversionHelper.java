package org.bidib.wizard.comm;

import org.bidib.jbidibc.core.BidibPort;
import org.bidib.jbidibc.core.LcMacro;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.PortModelEnum;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.node.MacroNode;
import org.bidib.jbidibc.core.utils.ByteUtils;
import org.bidib.wizard.mvc.main.model.Macro;
import org.bidib.wizard.mvc.main.model.function.EmptyFunction;
import org.bidib.wizard.mvc.main.model.function.Function;
import org.bidib.wizard.mvc.main.model.function.FunctionUtils;
import org.bidib.wizard.mvc.main.model.function.MacroFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MacroConversionHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(MacroConversionHelper.class);

    public int prepareLcMacroSteps(final MacroNode macroNode, final PortModelEnum portModel, final Macro macro)
        throws ProtocolException {

        if (macro == null) {
            LOGGER.warn("No macro provided.");
            throw new IllegalArgumentException("No macro provided.");
        }

        LOGGER.info("Prepare the macro points to transfer.");
        int stepNumber = 0;

        // add the steps
        for (Function<?> function : macro.getFunctions()) {
            LOGGER.info("Prepare macro function to transfer: {}", function);

            // don't transfer the empty function
            if (function != null && !(function instanceof EmptyFunction)) {
                // default value of delay is used in MacroFunctions, FlagFuntions,
                // DelayFunctions, CriticalFunctions and InputQueryFunctions,
                // see protocol spec MSG_LC_MACRO_SET parameter
                byte delay = FunctionUtils.getDelay(function);

                int outputNumber = FunctionUtils.getOutputNumber(function);
                Integer value = FunctionUtils.getValue(function);

                BidibStatus status = function.getAction();

                BidibPort bidibPort = null;
                LcOutputType portType = null;
                if (portModel != PortModelEnum.type && delay != ByteUtils.getLowByte(255)) {
                    // flat model always use 0x00 as type
                    portType = LcOutputType.SWITCHPORT;
                    bidibPort = BidibPort.prepareBidibPort(outputNumber);
                }
                else {
                    portType = ConversionUtils.getOutputType(function);
                    bidibPort = BidibPort.prepareBidibPort(portType.getType(), ByteUtils.getLowByte(outputNumber));
                }

                LcMacro lcMacro = null;
                // TODO fix this ...
                if (delay != ByteUtils.getLowByte(255)) {
                    byte byte5 = (value != null ? ByteUtils.getLowByte(value) : status.getType().getType());

                    // handle port function
                    lcMacro =
                        new LcMacro(ByteUtils.getLowByte(macro.getId()), ByteUtils.getLowByte(stepNumber++), delay,
                            bidibPort.getLowValue(), bidibPort.getHighValue(), byte5);
                }
                else {
                    // byte byte5 = (value != null ? ByteUtils.getLowByte(value) : status.getType().getType());

                    // handle system function
                    lcMacro =
                        new LcMacro(ByteUtils.getLowByte(macro.getId()), ByteUtils.getLowByte(stepNumber++), delay,
                            portType.getType(), ByteUtils.getLowByte(outputNumber), (byte) 0);
                }
                LOGGER.info("Prepared new macro point: {}", lcMacro);
                macroNode.setMacro(lcMacro);

                if (function instanceof MacroFunction && ((MacroFunction) function).getAction() == MacroStatus.END) {
                    break;
                }
            }
        }
        return stepNumber;
    }
}
