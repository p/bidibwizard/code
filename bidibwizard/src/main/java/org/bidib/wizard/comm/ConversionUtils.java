package org.bidib.wizard.comm;

import java.util.List;

import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.collections4.Predicate;
import org.bidib.jbidibc.core.enumeration.AccessoryOkayEnum;
import org.bidib.jbidibc.core.enumeration.AnalogPortEnum;
import org.bidib.jbidibc.core.enumeration.BacklightPortEnum;
import org.bidib.jbidibc.core.enumeration.BidibEnum;
import org.bidib.jbidibc.core.enumeration.CriticalFunctionEnum;
import org.bidib.jbidibc.core.enumeration.FlagEnum;
import org.bidib.jbidibc.core.enumeration.InputEnum;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.LightPortEnum;
import org.bidib.jbidibc.core.enumeration.MacroEnum;
import org.bidib.jbidibc.core.enumeration.MotorPortEnum;
import org.bidib.jbidibc.core.enumeration.ServoPortEnum;
import org.bidib.jbidibc.core.enumeration.SoundPortEnum;
import org.bidib.jbidibc.core.enumeration.SwitchPortEnum;
import org.bidib.wizard.mvc.main.model.GenericPort;
import org.bidib.wizard.mvc.main.model.Port;
import org.bidib.wizard.mvc.main.model.function.AccessoryOkayFunction;
import org.bidib.wizard.mvc.main.model.function.AnalogPortAction;
import org.bidib.wizard.mvc.main.model.function.BacklightPortAction;
import org.bidib.wizard.mvc.main.model.function.CriticalFunction;
import org.bidib.wizard.mvc.main.model.function.DelayFunction;
import org.bidib.wizard.mvc.main.model.function.FlagFunction;
import org.bidib.wizard.mvc.main.model.function.Function;
import org.bidib.wizard.mvc.main.model.function.InputFunction;
import org.bidib.wizard.mvc.main.model.function.LightPortAction;
import org.bidib.wizard.mvc.main.model.function.MacroFunction;
import org.bidib.wizard.mvc.main.model.function.MotorPortAction;
import org.bidib.wizard.mvc.main.model.function.RandomDelayFunction;
import org.bidib.wizard.mvc.main.model.function.ServoMoveQueryFunction;
import org.bidib.wizard.mvc.main.model.function.ServoPortAction;
import org.bidib.wizard.mvc.main.model.function.SoundPortAction;
import org.bidib.wizard.mvc.main.model.function.SwitchPairPortAction;
import org.bidib.wizard.mvc.main.model.function.SwitchPortAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConversionUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConversionUtils.class);

    /**
     * Map status between BiDiB type and LightControl type, which also contains a localized label.
     * 
     * @param status
     *            status from BiDiB library
     * 
     * @return LightControl status object
     */
    public static BidibStatus toBidibStatus(BidibEnum status) {
        BidibStatus result = null;

        if (status != null) {
            if (status instanceof AccessoryOkayEnum) {
                result = AccessoryOkayStatus.valueOf((AccessoryOkayEnum) status);
            }
            else if (status instanceof AnalogPortEnum) {
                result = AnalogPortStatus.valueOf((AnalogPortEnum) status);
            }
            else if (status instanceof BacklightPortEnum) {
                result = BacklightPortStatus.valueOf((BacklightPortEnum) status);
            }
            else if (status instanceof CriticalFunctionEnum) {
                result = CriticalFunctionStatus.valueOf((CriticalFunctionEnum) status);
            }
            else if (status instanceof FlagEnum) {
                result = FlagStatus.valueOf((FlagEnum) status);
            }
            else if (status instanceof InputEnum) {
                result = InputStatus.valueOf((InputEnum) status);
            }
            else if (status instanceof LightPortEnum) {
                result = LightPortStatus.valueOf((LightPortEnum) status);
            }
            else if (status instanceof MacroEnum) {
                result = MacroStatus.valueOf((MacroEnum) status);
            }
            else if (status instanceof MotorPortEnum) {
                result = MotorPortStatus.valueOf((MotorPortEnum) status);
            }
            else if (status instanceof ServoPortEnum) {
                result = ServoPortStatus.valueOf((ServoPortEnum) status);
            }
            else if (status instanceof SoundPortEnum) {
                result = SoundPortStatus.valueOf((SoundPortEnum) status);
            }
            else if (status instanceof SwitchPortEnum) {
                result = SwitchPortStatus.valueOf((SwitchPortEnum) status);
            }
            else {
                throw new RuntimeException("cannot map " + status.getClass().getSimpleName() + " to a status");
            }
        }
        return result;
    }

    public static Function<?> getFunction(LcOutputType outputType, BidibStatus status) {
        Function<?> result = null;
        LOGGER.info("Prepare the function for outputType: {}, status: {}", outputType, status);

        switch (outputType) {
            case ACCESSORY_OKAY_INPUTQUERY0:
                result = new AccessoryOkayFunction(AccessoryOkayStatus.QUERY0);
                break;
            case ACCESSORY_OKAY_INPUTQUERY1:
                result = new AccessoryOkayFunction(AccessoryOkayStatus.QUERY1);
                break;
            case ACCESSORY_OKAY_NF:
                result = new AccessoryOkayFunction(AccessoryOkayStatus.NO_FEEDBACK);
                break;
            case ANALOGPORT:
                result = new AnalogPortAction((AnalogPortStatus) status);
                break;
            case BACKLIGHTPORT:
                result = new BacklightPortAction((BacklightPortStatus) status);
                break;
            case LIGHTPORT:
                result = new LightPortAction((LightPortStatus) status);
                break;
            case MOTORPORT:
                result = new MotorPortAction((MotorPortStatus) status);
                break;
            case SERVOPORT:
                result = new ServoPortAction((ServoPortStatus) status);
                break;
            case SOUNDPORT:
                result = new SoundPortAction((SoundPortStatus) status);
                break;
            case SWITCHPORT:
                result = new SwitchPortAction((SwitchPortStatus) status);
                break;
            case SWITCHPAIRPORT:
                result = new SwitchPairPortAction((SwitchPortStatus) status);
                break;
            case DELAY_FIXED:
                result = new DelayFunction();
                break;
            case RANDOM_DELAY:
                result = new RandomDelayFunction();
                break;
            case INPUT_QUERY0:
                result = new InputFunction(InputStatus.QUERY0);
                break;
            case INPUT_QUERY1:
                result = new InputFunction(InputStatus.QUERY1);
                break;
            case FLAG_CLEAR:
                result = new FlagFunction(FlagStatus.CLEAR);
                break;
            case FLAG_SET:
                result = new FlagFunction(FlagStatus.SET);
                break;
            case FLAG_QUERY:
            case FLAG_QUERY1:
                result = new FlagFunction(FlagStatus.QUERY_1);
                break;
            case FLAG_QUERY0:
                result = new FlagFunction(FlagStatus.QUERY_0);
                break;
            case BEGIN_CRITICAL:
                result = new CriticalFunction(CriticalFunctionStatus.BEGIN);
                break;
            case END_CRITICAL:
                result = new CriticalFunction(CriticalFunctionStatus.END);
                break;
            case START_MACRO:
                result = new MacroFunction(MacroStatus.START);
                break;
            case STOP_MACRO:
                result = new MacroFunction(MacroStatus.STOP);
                break;
            case END_OF_MACRO:
                result = new MacroFunction(MacroStatus.END);
                break;
            case SERVOMOVE_QUERY:
                result = new ServoMoveQueryFunction();
                break;
            default:
                throw new RuntimeException(
                    "Cannot map output type " + outputType.getClass().getSimpleName() + " to a function");
        }
        return result;
    }

    public static Port<? extends BidibStatus> getPort(
        Communication communication, org.bidib.wizard.mvc.main.model.Node node, LcOutputType outputType,
        final int portNumber) {
        Port<? extends BidibStatus> result = null;
        List<? extends Port<?>> ports = null;

        if (node.getNode().isPortFlatModelAvailable()) {
            // flat port model
            switch (outputType) {
                case ANALOGPORT:
                    ports = node.getAnalogPorts();
                    break;
                case BACKLIGHTPORT:
                    ports = node.getBacklightPorts();
                    break;
                case LIGHTPORT:
                    ports = node.getLightPorts();
                    break;
                case MOTORPORT:
                    ports = node.getMotorPorts();
                    break;
                case SERVOPORT:
                    ports = node.getServoPorts();
                    break;
                case SOUNDPORT:
                    ports = node.getSoundPorts();
                    break;
                case INPUTPORT:
                case INPUT_QUERY0:
                case INPUT_QUERY1:
                    ports = node.getInputPorts();
                    break;
                case SWITCHPORT:
                    ports = node.getSwitchPorts();
                    break;
                case SWITCHPAIRPORT:
                    ports = node.getSwitchPairPorts();
                    break;
                default:
                    throw new RuntimeException(
                        "Cannot map output type " + outputType.getDeclaringClass().getSimpleName() + " to a port.");
            }
        }
        else {
            // type-oriented model
            switch (outputType) {
                case ANALOGPORT:
                    ports = communication.getAnalogPorts(node.getNode());
                    break;
                case BACKLIGHTPORT:
                    ports = communication.getBacklightPorts(node.getNode());
                    break;
                case LIGHTPORT:
                    ports = communication.getLightPorts(node.getNode());
                    break;
                case MOTORPORT:
                    ports = communication.getMotorPorts(node.getNode());
                    break;
                case SERVOPORT:
                    ports = communication.getServoPorts(node.getNode());
                    break;
                case SOUNDPORT:
                    ports = communication.getSoundPorts(node.getNode());
                    break;
                case INPUTPORT:
                case INPUT_QUERY0:
                case INPUT_QUERY1:
                    ports = communication.getInputPorts(node.getNode());
                    break;
                case SWITCHPORT:
                    ports = communication.getSwitchPorts(node.getNode());
                    break;
                // NO SUPPORT FOR SWITCHPORT IN TYPE-ORIENTED MODEL
                default:
                    throw new RuntimeException(
                        "Cannot map output type " + outputType.getDeclaringClass().getSimpleName() + " to a port.");
            }
        }

        if (ports != null && portNumber >= 0) {
            // search the matching port
            result = IterableUtils.find(ports, new Predicate<Port<?>>() {

                @Override
                public boolean evaluate(Port<?> port) {
                    return port.getId() == portNumber;
                }
            });
            LOGGER.info("Search for port with portNumber: {}, found port: {}", portNumber, result);
        }
        else {
            LOGGER.warn("No ports available or portNumber out of range for outputType: {} on node: {}", outputType,
                node);
        }
        return result;
    }

    public static Port<? extends BidibStatus> getPort(
        Communication communication, org.bidib.wizard.mvc.main.model.Node node, final int portNumber) {
        Port<? extends BidibStatus> port = null;

        if (node.getNode().isPortFlatModelAvailable()) {
            GenericPort genericPort = IterableUtils.find(node.getGenericPorts(), new Predicate<GenericPort>() {

                @Override
                public boolean evaluate(GenericPort genericPort) {
                    return genericPort.getPortNumber() == portNumber;
                }
            });
            if (genericPort != null) {
                // search the port
                port = getPort(communication, node, genericPort.getCurrentPortType(), portNumber);

                LOGGER.info("Found port to return: {}", port);
            }
        }
        else {
            // type port model
        }
        return port;
    }

    /**
     * Returns the output type of the function
     * 
     * @param function
     *            the function
     * @return the output type
     */
    public static LcOutputType getOutputType(Function<?> function) {
        LcOutputType result = null;

        if (function instanceof AccessoryOkayFunction && function.getAction() == AccessoryOkayStatus.QUERY0) {
            result = LcOutputType.ACCESSORY_OKAY_INPUTQUERY0;
        }
        else if (function instanceof AccessoryOkayFunction && function.getAction() == AccessoryOkayStatus.QUERY1) {
            result = LcOutputType.ACCESSORY_OKAY_INPUTQUERY1;
        }
        else if (function instanceof AccessoryOkayFunction && function.getAction() == AccessoryOkayStatus.NO_FEEDBACK) {
            result = LcOutputType.ACCESSORY_OKAY_NF;
        }
        else if (function instanceof AnalogPortAction) {
            result = LcOutputType.ANALOGPORT;
        }
        else if (function instanceof CriticalFunction && function.getAction() == CriticalFunctionStatus.END) {
            result = LcOutputType.END_CRITICAL;
        }
        else if (function instanceof CriticalFunction && function.getAction() == CriticalFunctionStatus.BEGIN) {
            result = LcOutputType.BEGIN_CRITICAL;
        }
        else if (function instanceof DelayFunction) {
            result = LcOutputType.DELAY_FIXED;
        }
        else if (function instanceof FlagFunction && function.getAction() == FlagStatus.CLEAR) {
            result = LcOutputType.FLAG_CLEAR;
        }
        else if (function instanceof FlagFunction && function.getAction() == FlagStatus.SET) {
            result = LcOutputType.FLAG_SET;
        }
        else if (function instanceof FlagFunction && function.getAction() == FlagStatus.QUERY_1) {
            result = LcOutputType.FLAG_QUERY1;
        }
        else if (function instanceof FlagFunction && function.getAction() == FlagStatus.QUERY_0) {
            result = LcOutputType.FLAG_QUERY0;
        }
        else if (function instanceof InputFunction && function.getAction() == InputStatus.QUERY0) {
            result = LcOutputType.INPUT_QUERY0;
        }
        else if (function instanceof InputFunction && function.getAction() == InputStatus.QUERY1) {
            result = LcOutputType.INPUT_QUERY1;
        }
        else if (function instanceof BacklightPortAction) {
            result = LcOutputType.BACKLIGHTPORT;
        }
        else if (function instanceof LightPortAction) {
            result = LcOutputType.LIGHTPORT;
        }
        else if (function instanceof MacroFunction && function.getAction() == MacroStatus.STOP) {
            result = LcOutputType.STOP_MACRO;
        }
        else if (function instanceof MacroFunction && function.getAction() == MacroStatus.START) {
            result = LcOutputType.START_MACRO;
        }
        else if (function instanceof MacroFunction && function.getAction() == MacroStatus.END) {
            result = LcOutputType.END_OF_MACRO;
        }
        else if (function instanceof MotorPortAction) {
            result = LcOutputType.MOTORPORT;
        }
        else if (function instanceof RandomDelayFunction) {
            result = LcOutputType.RANDOM_DELAY;
        }
        else if (function instanceof ServoPortAction) {
            result = LcOutputType.SERVOPORT;
        }
        else if (function instanceof SoundPortAction) {
            result = LcOutputType.SOUNDPORT;
        }
        else if (function instanceof SwitchPortAction) {
            result = LcOutputType.SWITCHPORT;
        }
        else if (function instanceof SwitchPairPortAction) {
            result = LcOutputType.SWITCHPAIRPORT;
        }
        else if (function instanceof ServoMoveQueryFunction) {
            result = LcOutputType.SERVOMOVE_QUERY;
        }
        else {
            throw new RuntimeException(
                "cannot map function type " + function.getClass().getSimpleName() + " to an output type");
        }
        return result;
    }

}
