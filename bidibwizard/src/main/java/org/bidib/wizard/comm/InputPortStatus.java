package org.bidib.wizard.comm;

import org.bidib.jbidibc.core.enumeration.InputPortEnum;

public enum InputPortStatus implements BidibStatus {
    // @formatter:off
    ON(InputPortEnum.ON, "on"), OFF(InputPortEnum.OFF, "off");
    // @formatter:on

    private final InputPortEnum type;

    private final String key;

    InputPortStatus(InputPortEnum type, String key) {
        this.type = type;
        this.key = key;
    }

    public InputPortEnum getType() {
        return type;
    }

    public String getKey() {
        return key;
    }

    public String toString() {
        return type.toString();
    }

    public static InputPortStatus valueOf(InputPortEnum type) {
        InputPortStatus result = null;

        for (InputPortStatus e : values()) {
            if (e.type == type) {
                result = e;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("cannot map " + type + " to an input port status");
        }
        return result;
    }

    @Override
    public BidibStatus[] getValues() {
        return new BidibStatus[] { ON, OFF };
    }
}
