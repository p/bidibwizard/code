package org.bidib.wizard.comm;

import java.util.LinkedHashSet;
import java.util.Set;

import org.bidib.jbidibc.core.MessageListener;
import org.bidib.jbidibc.core.NodeListener;
import org.bidib.jbidibc.core.exception.NoAnswerException;
import org.bidib.jbidibc.core.node.listener.TransferListener;
import org.bidib.wizard.comm.bidib.BidibCommunication;
import org.bidib.wizard.comm.listener.CommunicationListener;
import org.bidib.wizard.common.locale.Resources;
import org.bidib.wizard.mvc.common.model.PreferencesPortType;
import org.bidib.wizard.mvc.preferences.model.Preferences;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class CommunicationFactory {
    private static final Logger LOGGER = LoggerFactory.getLogger(CommunicationFactory.class.getName());

    private static Communication instance = null;

    private static Set<CommunicationListener> listeners = new LinkedHashSet<CommunicationListener>();

    private static Set<TransferListener> transferListeners = new LinkedHashSet<TransferListener>();

    private static Set<MessageListener> messageListeners = new LinkedHashSet<MessageListener>();

    private static Set<NodeListener> nodeListeners = new LinkedHashSet<NodeListener>();

    // TODO handle the change of the port type only if a instance is available.

    private CommunicationFactory() {

    }

    public synchronized static Communication getInstance() {
        if (instance == null) {

            fireStatus(Resources.getString(CommunicationFactory.class, "open-port"), -1);

            PreferencesPortType commPort = null;
            try {
                commPort = Preferences.getInstance().getSelectedPortType();
                LOGGER.debug("Start to open the Bidib port: {}", commPort);

                if (commPort != null) {
                    LOGGER.info("Create new instance of BidibCommunication for commPort: {}", commPort);
                    instance = new BidibCommunication(commPort);

                    LOGGER.info("Assigned new instance of BidibCommunication for commPort: {}", commPort);

                    for (TransferListener transferListener : transferListeners) {
                        instance.addTransferListener(transferListener);
                    }

                    for (MessageListener messageListener : messageListeners) {
                        instance.addMessageListener(messageListener);
                    }

                    for (NodeListener nodeListener : nodeListeners) {
                        instance.addNodeListener(nodeListener);
                    }

                    instance.addCommunicationListener(new CommunicationListener() {

                        @Override
                        public void opening() {
                            LOGGER.info("The port is opening.");
                            fireOpening();
                        }

                        @Override
                        public void opened(String port) {
                            fireOpened(port);
                        }

                        @Override
                        public void closed(String port) {
                            fireClosed(port);
                        }

                        @Override
                        public void initialized() {
                            fireInitialized();
                        }

                        @Override
                        public void status(String statusText, int displayDuration) {
                            fireStatus(statusText, displayDuration);
                        }
                    });
                }
                else {
                    LOGGER.info("No comm port selected, do not create BidibCommunication.");
                    fireStatus(Resources.getString(CommunicationFactory.class, "no-port-selected"), -1);
                }
            }
            catch (NoAnswerException ex) {
                LOGGER.warn("Establish communication with interface failed.");
                fireStatus(Resources.getString(CommunicationFactory.class, "establish-communication-failed"), -1);

                // release the instance
                instance = null;
                throw ex;
            }
            catch (Exception e) {
                LOGGER.warn("Open Bidib failed.", e);
                fireStatus(Resources.getString(CommunicationFactory.class, "open-port-failed") + " " + commPort, -1);
            }
        }
        return instance;
    }

    public static void addCommunicationListener(CommunicationListener listener) {
        LOGGER.info("Adding communication listener: {}", listener);
        synchronized (listeners) {
            listeners.add(listener);
        }
    }

    public static void addTransferListener(TransferListener listener) {
        LOGGER.info("Adding transfer listener: {}", listener);
        synchronized (transferListeners) {
            transferListeners.add(listener);
        }
    }

    public static void addMessageListener(MessageListener listener) {
        LOGGER.info("Adding message listener: {}", listener);
        synchronized (messageListeners) {
            messageListeners.add(listener);

            LOGGER.info("Currently registered messageListeners: {}", messageListeners);

            // if the instance is already assigned we must add the listener there
            if (instance != null) {
                LOGGER.info("Add the message listener to the exising instance.");
                instance.addMessageListener(listener);
            }
        }
    }

    public static void removeMessageListener(MessageListener listener) {
        LOGGER.info("Remove message listener: {}", listener);
        synchronized (messageListeners) {
            messageListeners.remove(listener);
        }

        if (instance != null) {
            instance.removeMessageListener(listener);
        }
    }

    public static void addNodeListener(NodeListener listener) {
        LOGGER.info("Adding node listener: {}", listener);
        synchronized (nodeListeners) {
            nodeListeners.add(listener);
        }

        if (instance != null) {
            instance.addNodeListener(listener);
        }
    }

    public static void removeNodeListener(NodeListener listener) {
        LOGGER.info("Remove node listener: {}", listener);
        synchronized (nodeListeners) {
            nodeListeners.remove(listener);
        }

        if (instance != null) {
            instance.removeNodeListener(listener);
        }
    }

    public static void disconnect() {

        synchronized (CommunicationFactory.class) {
            LOGGER.info("Disconnect the communication.");

            if (instance != null) {
                LOGGER.info("Close instance of BidibCommunication: {}", instance);
                instance.close();

                LOGGER.info("Release instance of BidibCommunication: {}", instance);
                instance = null;
                LOGGER.info("Released instance of BidibCommunication.");
            }
        }
    }

    private static void fireOpening() {
        for (CommunicationListener listener : listeners) {
            listener.opening();
        }
    }

    private static void fireOpened(String port) {
        for (CommunicationListener listener : listeners) {
            listener.opened(port);
        }
    }

    private static void fireClosed(String port) {
        for (CommunicationListener listener : listeners) {
            listener.closed(port);
        }
    }

    private static void fireInitialized() {
        for (CommunicationListener listener : listeners) {
            listener.initialized();
        }
    }

    private static void fireStatus(String statusText, int displayDuration) {
        for (CommunicationListener listener : listeners) {
            listener.status(statusText, displayDuration);
        }
    }
}
