package org.bidib.wizard.comm;

import org.bidib.jbidibc.core.enumeration.FlagEnum;
import org.bidib.wizard.common.locale.Resources;

public enum FlagStatus implements BidibStatus {
    // @formatter:off
    CLEAR(FlagEnum.CLEAR, "clear"), QUERY_1(FlagEnum.QUERY1, "query1"), QUERY_0(FlagEnum.QUERY0, "query0"), SET(
        FlagEnum.SET, "set");
    // @formatter:on

    private final FlagEnum type;

    private final String key;

    private final String label;

    FlagStatus(FlagEnum type, String key) {
        this.type = type;
        this.key = key;
        this.label = Resources.getString(FlagStatus.class, key);
    }

    public FlagEnum getType() {
        return type;
    }

    public String getKey() {
        return key;
    }

    public String toString() {
        return label;
    }

    public static FlagStatus valueOf(FlagEnum type) {
        FlagStatus result = null;

        for (FlagStatus e : values()) {
            if (e.type == type) {
                result = e;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("cannot map " + type + " to a flag status");
        }
        return result;
    }

    @Override
    public BidibStatus[] getValues() {
        return new BidibStatus[] { CLEAR, QUERY_1, QUERY_0, SET };
    }
}
