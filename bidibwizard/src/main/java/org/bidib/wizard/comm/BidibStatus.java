package org.bidib.wizard.comm;

import org.bidib.jbidibc.core.enumeration.BidibEnum;

public interface BidibStatus {
    BidibEnum getType();

    String getKey();

    BidibStatus[] getValues();
}
