package org.bidib.wizard.comm;

import org.bidib.jbidibc.core.enumeration.MacroEnum;
import org.bidib.wizard.common.locale.Resources;

public enum AccessoryAspectStatus implements BidibStatus {
    // @formatter:off
    START(MacroEnum.START, "start");
    // @formatter:on

    private final MacroEnum type;

    private final String key;

    private final String label;

    AccessoryAspectStatus(MacroEnum type, String key) {
        this.type = type;
        this.key = key;
        this.label = Resources.getString(AccessoryAspectStatus.class, key);
    }

    public MacroEnum getType() {
        return type;
    }

    public String getKey() {
        return key;
    }

    public String toString() {
        return label;
    }

    public static AccessoryAspectStatus valueOf(MacroEnum type) {
        AccessoryAspectStatus result = null;

        for (AccessoryAspectStatus e : values()) {
            if (e.type == type) {
                result = e;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("cannot map " + type + " to a accessory aspect status");
        }
        return result;
    }

    @Override
    public BidibStatus[] getValues() {
        return new BidibStatus[] { START };
    }
}
