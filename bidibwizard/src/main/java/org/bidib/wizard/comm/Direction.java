package org.bidib.wizard.comm;

import org.bidib.jbidibc.core.enumeration.DirectionEnum;

public enum Direction {
    // @formatter:off
    FORWARD(DirectionEnum.FORWARD), BACKWARD(DirectionEnum.BACKWARD);
    // @formatter:on

    private final DirectionEnum type;

    Direction(DirectionEnum type) {
        this.type = type;
    }

    public DirectionEnum getType() {
        return type;
    }

    public static Direction valueOf(DirectionEnum type) {
        Direction result = null;

        for (Direction e : values()) {
            if (e.type == type) {
                result = e;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("cannot map " + type + " to a direction");
        }
        return result;
    }
}
