package org.bidib.wizard.comm.listener;

public interface CommunicationListener {

    /**
     * The communication is opening the port.
     */
    void opening();

    /**
     * The communication was opened.
     * 
     * @param port
     *            the port identifier
     */
    void opened(String port);

    /**
     * The communication was closed.
     * 
     * @param port
     *            the port identifier
     */
    void closed(String port);

    /**
     * The communication is initialized. The initial loading of nodes in system has passed.
     */
    void initialized();

    /**
     * An internal status has changed.
     * 
     * @param statusText
     *            the status message
     * @param displayDuration
     *            the display duration in seconds or -1 to display forever
     */
    void status(String statusText, int displayDuration);
}
