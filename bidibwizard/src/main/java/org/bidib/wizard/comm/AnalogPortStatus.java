package org.bidib.wizard.comm;

import org.bidib.jbidibc.core.enumeration.AnalogPortEnum;
import org.bidib.wizard.common.locale.Resources;

public enum AnalogPortStatus implements BidibStatus {
    // @formatter:off
    START(AnalogPortEnum.START, "start");
    // @formatter:on

    private final AnalogPortEnum type;

    private final String key;

    private final String label;

    AnalogPortStatus(AnalogPortEnum type, String key) {
        this.type = type;
        this.key = key;
        this.label = Resources.getString(AnalogPortStatus.class, key);
    }

    public AnalogPortEnum getType() {
        return type;
    }

    public String getKey() {
        return key;
    }

    public String toString() {
        return label;
    }

    public static AnalogPortStatus valueOf(AnalogPortEnum type) {
        AnalogPortStatus result = null;

        for (AnalogPortStatus e : values()) {
            if (e.type == type) {
                result = e;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("cannot map " + type + " to a analog port status");
        }
        return result;
    }

    @Override
    public BidibStatus[] getValues() {
        return new BidibStatus[] { START };
    }
}
