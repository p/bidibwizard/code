package org.bidib.wizard.comm;

import org.bidib.jbidibc.core.enumeration.InputEnum;
import org.bidib.wizard.common.locale.Resources;

public enum InputStatus implements BidibStatus {
    // @formatter:off
    QUERY0(InputEnum.QUERY0, "query0"), QUERY1(InputEnum.QUERY1, "query1");
    // @formatter:on

    private final InputEnum type;

    private final String key;

    private final String label;

    InputStatus(InputEnum type, String key) {
        this.type = type;
        this.key = key;
        this.label = Resources.getString(InputStatus.class, key);
    }

    public InputEnum getType() {
        return type;
    }

    public String getKey() {
        return key;
    }

    public String toString() {
        return label;
    }

    public static InputStatus valueOf(InputEnum type) {
        InputStatus result = null;

        for (InputStatus e : values()) {
            if (e.type == type) {
                result = e;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("cannot map " + type + " to a input status");
        }
        return result;
    }

    @Override
    public BidibStatus[] getValues() {
        return new BidibStatus[] { QUERY0, QUERY1 };
    }
}
