package org.bidib.wizard.comm;

import org.bidib.jbidibc.core.enumeration.BidibEnum;
import org.bidib.jbidibc.core.enumeration.FeedbackPortEnum;
import org.bidib.wizard.common.locale.Resources;

public enum FeedbackPortStatus implements BidibStatus {
    // @formatter:off
    FREE(FeedbackPortEnum.FREE, "free"), OCCUPIED(FeedbackPortEnum.OCCUPIED, "occupied");
    // @formatter:on

    private final FeedbackPortEnum type;

    private final String key;

    private final String label;

    FeedbackPortStatus(FeedbackPortEnum type, String key) {
        this.type = type;
        this.key = key;
        this.label = Resources.getString(FeedbackPortStatus.class, key);
    }

    @Override
    public BidibEnum getType() {
        return type;
    }

    public String getKey() {
        return key;
    }

    public String toString() {
        return label;
    }

    public static FeedbackPortStatus valueOf(FeedbackPortEnum type) {
        FeedbackPortStatus result = null;

        for (FeedbackPortStatus e : values()) {
            if (e.type == type) {
                result = e;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("cannot map " + type + " to a feedback port status");
        }
        return result;
    }

    @Override
    public BidibStatus[] getValues() {
        return new BidibStatus[] { FREE, OCCUPIED };
    }
}
