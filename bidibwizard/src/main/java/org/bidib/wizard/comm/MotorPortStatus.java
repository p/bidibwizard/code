package org.bidib.wizard.comm;

import org.bidib.jbidibc.core.enumeration.MotorPortEnum;
import org.bidib.wizard.common.locale.Resources;

public enum MotorPortStatus implements BidibStatus {
    // @formatter:off
    FORWARD(MotorPortEnum.FORWARD, "forward"), BACKWARD(MotorPortEnum.BACKWARD, "backward"), TEST(MotorPortEnum.TEST, "test");
    // @formatter:on

    private final MotorPortEnum type;

    private final String key;

    private final String label;

    MotorPortStatus(MotorPortEnum type, String key) {
        this.type = type;
        this.key = key;
        this.label = Resources.getString(MotorPortStatus.class, key);
    }

    public MotorPortEnum getType() {
        return type;
    }

    public String getKey() {
        return key;
    }

    public String toString() {
        return label;
    }

    public static MotorPortStatus valueOf(MotorPortEnum type) {
        MotorPortStatus result = null;

        for (MotorPortStatus e : values()) {
            if (e.type == type) {
                result = e;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("cannot map " + type + " to a motor port status");
        }
        return result;
    }

    @Override
    public BidibStatus[] getValues() {
        return new BidibStatus[] { FORWARD, BACKWARD };
    }
}
