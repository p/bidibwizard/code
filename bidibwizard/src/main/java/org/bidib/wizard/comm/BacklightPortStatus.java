package org.bidib.wizard.comm;

import org.bidib.jbidibc.core.enumeration.BacklightPortEnum;
import org.bidib.wizard.common.locale.Resources;

public enum BacklightPortStatus implements BidibStatus {
    // @formatter:off
    START(BacklightPortEnum.START, "start");
    // @formatter:on

    private final BacklightPortEnum type;

    private final String key;

    private final String label;

    BacklightPortStatus(BacklightPortEnum type, String key) {
        this.type = type;
        this.key = key;
        this.label = Resources.getString(BacklightPortStatus.class, key);
    }

    public BacklightPortEnum getType() {
        return type;
    }

    public String getKey() {
        return key;
    }

    public String toString() {
        return label;
    }

    public static BacklightPortStatus valueOf(BacklightPortEnum type) {
        BacklightPortStatus result = null;

        for (BacklightPortStatus e : values()) {
            if (e.type == type) {
                result = e;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("cannot map " + type + " to a backlight port status");
        }
        return result;
    }

    @Override
    public BidibStatus[] getValues() {
        return new BidibStatus[] { START };
    }
}
