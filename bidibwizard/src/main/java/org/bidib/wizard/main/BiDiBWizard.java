package org.bidib.wizard.main;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.plaf.ColorUIResource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.bidib.wizard.common.context.ApplicationContext;
import org.bidib.wizard.mvc.common.view.BidibDefaultDockableContainerFactory;
import org.bidib.wizard.mvc.error.controller.ErrorController;
import org.bidib.wizard.mvc.main.controller.DefaultBidibPiController;
import org.bidib.wizard.mvc.main.controller.MainController;
import org.bidib.wizard.mvc.preferences.model.Preferences;
import org.bushe.swing.event.EventServiceExistsException;
import org.bushe.swing.event.EventServiceLocator;
import org.bushe.swing.event.ThreadSafeEventService;
import org.jdesktop.swingx.plaf.windows.WindowsLookAndFeelAddons;
import org.jdesktop.swingx.util.OS;
import org.slf4j.LoggerFactory;
import org.slf4j.bridge.SLF4JBridgeHandler;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import com.jidesoft.plaf.LookAndFeelFactory;
import com.vlsolutions.swing.docking.DockableContainerFactory;
import com.vlsolutions.swing.docking.ui.DockingUISettings;

public final class BiDiBWizard {

    private BiDiBWizard() {
        // initialize the default application context
        DefaultApplicationContext.getInstance();
    }

    public void startApp(String[] args) {

        // get the preferences path from a jvm property (set with -Dbidib.preferencesPath=....
        String preferencesPath = System.getProperty("bidib.preferencesPath");

        // if the path is not set, use the value from the environment
        if (StringUtils.isBlank(preferencesPath)) {
            preferencesPath = System.getenv("bidib.preferencesPath");
            // if the path is not set use the user.home
            if (StringUtils.isBlank(preferencesPath)) {
                preferencesPath = System.getProperty("user.home");

                System.setProperty("bidib.preferencesPath", preferencesPath);
            }
        }

        // set path to preferences
        Preferences.setPath(new File(preferencesPath).getPath());

        Preferences.getInstance().reloadLoggerConfiguration(true);

        SLF4JBridgeHandler.removeHandlersForRootLogger();
        SLF4JBridgeHandler.install();

        Preferences.getInstance().printPreferences();

        // add listener for logfile append property
        Preferences.getInstance().addPropertyChangeListener(Preferences.PROPERTY_LOGFILE_APPEND,
            new PropertyChangeListener() {

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    boolean isLogLileAppend = Preferences.getInstance().isLogFileAppend();
                    LoggerFactory.getLogger(BiDiBWizard.class).info(
                        "The logfile append flag has changed, isLogLileAppend: {}", isLogLileAppend);

                    Preferences.getInstance().reloadLoggerConfiguration(false);
                }
            });

        try {
            EventServiceLocator.setEventService(EventServiceLocator.SERVICE_NAME_SWING_EVENT_SERVICE,
                new ThreadSafeEventService());
        }
        catch (EventServiceExistsException ex) {
            LoggerFactory.getLogger(BiDiBWizard.class).warn("Configure event service failed.", ex);
        }

        // set system look and feel only under windows
        try {
            if (SystemUtils.IS_OS_MAC_OSX) {
                try {
                    UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
                }
                catch (Exception ex) {
                    LoggerFactory.getLogger(BiDiBWizard.class).warn("Set cross platform L&F on MacOSX failed.", ex);
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                }
            }
            else {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            }
        }
        catch (ClassNotFoundException | InstantiationException | IllegalAccessException
            | UnsupportedLookAndFeelException ex) {
            LoggerFactory.getLogger(BiDiBWizard.class).warn("Set the system L&F failed.", ex);
        }

        if (OS.isUsingWindowsVisualStyles()) {
            String xpStyle = OS.getWindowsVisualStyle();

            if (WindowsLookAndFeelAddons.SILVER_VISUAL_STYLE.equalsIgnoreCase(xpStyle)
                || WindowsLookAndFeelAddons.VISTA_VISUAL_STYLE.equalsIgnoreCase(xpStyle)) {
                UIManager.put("StatusBar.rightImage", "/icons/silver-statusbar-right.png");
            }
            else {
                UIManager.put("StatusBar.rightImage", "/icons/statusbar-right.png");
            }
        }

        com.jidesoft.utils.Lm.verifyLicense("Andreas Kuhtz", "BiDiB-Wizard", "T1xTEztBM2Obp3w39SfBqkRoQBicczu");
        // LookAndFeelFactory.installJideExtension();
        LookAndFeelFactory.installJideExtension(LookAndFeelFactory.VSNET_STYLE_WITHOUT_MENU);

        try {
            String osName = System.getProperty("os.name");
            if (osName.startsWith("Windows 10")) {
                ColorUIResource tableSelectionBackground = (ColorUIResource) UIManager.get("Table.selectionBackground");
                tableSelectionBackground = new ColorUIResource(tableSelectionBackground.darker());
                UIManager.put("Table.selectionBackground", tableSelectionBackground);

                LoggerFactory.getLogger(BiDiBWizard.class).info(
                    "Tweak the Table.selectionBackground color because the slider thumb has the same color: {}",
                    tableSelectionBackground);
            }
        }
        catch (Exception ex) {
            LoggerFactory.getLogger(BiDiBWizard.class).warn("Tweak table selection background failed.", ex);
        }

        startControllers(args);
    }

    private void startControllers(final String[] args) {
        LoggerFactory.getLogger(BiDiBWizard.class).info("Provided args: {}", new Object[] { args });

        // prepared and register the startup args in the application context
        WizardStartupParams startupParams = new WizardStartupParams();
        try {
            JCommander.newBuilder().addObject(startupParams).build().parse(args);

            LoggerFactory.getLogger(BiDiBWizard.class).info("Parsed params: {}", startupParams);

            DefaultApplicationContext.getInstance().register(DefaultApplicationContext.KEY_STARTUP_PROPERTIES,
                startupParams);

        }
        catch (ParameterException ex) {
            LoggerFactory.getLogger(BiDiBWizard.class).warn("Execution of {} command failed: {}",
                startupParams.getClass().getSimpleName(), ex.getMessage());
            StringBuilder sb = new StringBuilder();
            JCommander.newBuilder().addObject(startupParams).build().usage(sb);
            LoggerFactory.getLogger(BiDiBWizard.class).warn(sb.toString());
        }

        DockableContainerFactory.setFactory(new BidibDefaultDockableContainerFactory());
        DockingUISettings.getInstance().installUI();

        ApplicationContext applicationContext = DefaultApplicationContext.getInstance();

        if (OS.isLinux()) {
            LoggerFactory.getLogger(BiDiBWizard.class).info("Check if we are running on BiDiB-Pi infrastructure.");

            try {
                DefaultBidibPiController bidibPiController = new DefaultBidibPiController();
                bidibPiController.start(applicationContext);

                LoggerFactory.getLogger(BiDiBWizard.class).info("The Pi connector was initialized.");
            }
            catch (IllegalArgumentException ex) {
                LoggerFactory.getLogger(BiDiBWizard.class).warn("The Pi connector was not initialized.");
            }
        }

        new ErrorController().start();

        MainController mainController = new MainController();
        DefaultApplicationContext.getInstance().register(DefaultApplicationContext.KEY_MAIN_CONTROLLER, mainController);
        mainController.start();
    }

    public static void main(final String[] args) {
        // open GUI
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new BiDiBWizard().startApp(args);
            }
        });
    }
}
