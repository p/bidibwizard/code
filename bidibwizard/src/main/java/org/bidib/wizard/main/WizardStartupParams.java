package org.bidib.wizard.main;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

@Parameters(separators = "=")
public class WizardStartupParams {

    public static final String MODE_ICONIFIED = "iconified";

    @Parameter(names = { "-autoconnect",
        "-autoConnect" }, description = "Automatically connect the wizard after startup, value=true|false", arity = 1, required = false)
    private boolean autoConnect;

    @Parameter(names = { "-startupmode", "-startupMode" }, description = "The startup mode to use, value=normal|"
        + MODE_ICONIFIED, required = false)
    private String startupMode;

    public boolean isAutoConnect() {
        return autoConnect;
    }

    public String getStartupMode() {
        return startupMode;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
