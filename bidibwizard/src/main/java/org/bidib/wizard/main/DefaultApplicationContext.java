package org.bidib.wizard.main;

import java.util.LinkedHashMap;
import java.util.Map;

import org.bidib.wizard.common.context.ApplicationContext;

public class DefaultApplicationContext implements ApplicationContext {

    private Map<String, Object> registry = new LinkedHashMap<>();

    public static final String KEY_NODE_LABELS = "nodeLabels";

    public static final String KEY_MACRO_LABELS = "macroLabels";

    public static final String KEY_FLAG_LABELS = "flagLabels";

    public static final String KEY_ACCESSORY_LABELS = "accessoryLabels";

    public static final String KEY_SWITCHPORT_LABELS = "switchPortLabels";

    public static final String KEY_SWITCHPAIRPORT_LABELS = "switchPairPortLabels";

    public static final String KEY_SERVOPORT_LABELS = "servoPortLabels";

    public static final String KEY_INPUTPORT_LABELS = "inputPortLabels";

    public static final String KEY_ANALOGPORT_LABELS = "analogPortLabels";

    public static final String KEY_FEEDBACKPORT_LABELS = "feedbackPortLabels";

    public static final String KEY_LIGHTPORT_LABELS = "lightPortLabels";

    public static final String KEY_BACKLIGHTPORT_LABELS = "backlightPortLabels";

    public static final String KEY_MOTORPORT_LABELS = "motorPortLabels";

    public static final String KEY_SOUNDPORT_LABELS = "soundPortLabels";

    public static final String KEY_PORTS_PROVIDER = "portsProvider";

    public static final String KEY_MAINNODELISTLISTENER = "mainNodeListListener";

    public static final String KEY_CONSOLE_MODEL = "consoleModel";

    public static final String KEY_FEEDBACKPORTSTATUSCHANGEPROVIDER = "feedbackPortStatusChangeProvider";

    public static final String KEY_STEPCONTROL_CONTROLLER = "stepControlController";

    public static final String KEY_COMMAND_STATION_SERVICE = "commandStationService";

    public static final String KEY_MAIN_FRAME = "mainFrame";

    public static final String KEY_STATUS_BAR = "statusBar";

    public static final String KEY_MAINMENU_LISTENER = "mainMenuListener";

    public static final String KEY_NODE_TREE = "nodeTree";

    public static final String KEY_CV_CONSOLE_MODEL = "cvConsoleModel";

    public static final String KEY_STARTUP_PROPERTIES = "startupProperties";

    public static final String KEY_MAIN_CONTROLLER = "mainController";

    public static final String KEY_MACROLIST_CONTROLLER = "macroListController";

    public static final String KEY_DESKTOP = "desktop";

    public static final String KEY_TOPTOOLBARPANEL = "topToolBarPanel";

    private static ApplicationContext INSTANCE;

    public static synchronized ApplicationContext getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new DefaultApplicationContext();
        }
        return INSTANCE;
    }

    @Override
    public Object register(String key, Object content) {
        return registry.put(key, content);
    }

    @Override
    public Object unregister(String key) {
        return registry.remove(key);
    }

    @Override
    public Object get(String key) {
        return registry.get(key);
    }

    @Override
    public <T> T get(String key, Class<T> type) {
        return get(type, get(key));
    }

    protected <T> T get(Class<T> type, Object body) {
        // if same type
        if (type.isInstance(body)) {
            return type.cast(body);
        }

        return null;
    }

}
