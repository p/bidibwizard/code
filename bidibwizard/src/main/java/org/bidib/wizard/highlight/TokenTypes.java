package org.bidib.wizard.highlight;

// Public domain, no restrictions, Ian Holyer, University of Bristol.

/**
 * The TokenTypes interface defines the integer constants representing different types of tokens, for use with any
 * languages. The constants are used in symbols to represent the types of similar tokens, and in scanners as scanner
 * states, and in highlighters to determine the colour or style of tokens. There is also an array typeNames of textual
 * names, indexed by type, for descriptive purposes.
 * 
 * <p>
 * The UNRECOGNIZED constant (zero) is for tokens which are completely unrecognized, usually consisting of a single
 * illegal character. Other error tokens are represented by negative types, where -t represents an incomplete or
 * malformed token of type t. An error token usually consists of the maximal legal substring of the source text.
 * 
 * <p>
 * The WHITESPACE constant is used to classify tokens which are to be discarded, it acts as a suitable scanner state at
 * the beginning of a document, and it is used for the usual end-of-text sentinel token which marks the end of the
 * document. Comments can optionally be classified as WHITESPACE and discarded, if they are not needed for highlighting.
 * No other types besides UNRECOGNIZED and WHITESPACE are treated specially.
 * 
 * <p>
 * The constants are presented as an interface so that any class can implement the interface and use the names of the
 * constants directly, without prefixing them with a class name.
 */
public interface TokenTypes { // NOSONAR

    static final int UNRECOGNIZED = 0;

    static final int WHITESPACE = 1;

    static final int WORD = 2;

    static final int NUMBER = 3;

    static final int PUNCTUATION = 4;

    static final int COMMENT = 5;

    static final int START_COMMENT = 6;

    static final int MID_COMMENT = 7;

    static final int END_COMMENT = 8;

    static final int TAG = 9;

    static final int END_TAG = 10;

    static final int KEYWORD = 11;

    static final int KEYWORD2 = 12;

    static final int IDENTIFIER = 13;

    static final int LITERAL = 14;

    static final int STRING = 15;

    static final int CHARACTER = 16;

    static final int OPERATOR = 17;

    static final int BRACKET = 18;

    static final int SEPARATOR = 19;

    static final int URL = 20;

    static final int VARIABLE = 21;

    /**
     * The names of the token types, indexed by type, are provided for descriptive purposes.
     */
    static final String[] TYPENAMES = { // NOSONAR
        "bad token", "whitespace", "word", "number", "punctuation", "comment", "start of comment", "middle of comment",
            "end of comment", "tag", "end tag", "keyword", "keyword 2", "identifier", "literal", "string", "character",
            "operator", "bracket", "separator", "url", "variable" };
}
