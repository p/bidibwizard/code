## Dieses Script definiert ein Asseccory, 
## um die zwei/vier LED eines Licht-Speersignal (DB)
## anzusteuern.
##
## Hardware: 2 Lightport (Port 0 = rt, 1 = ws)
## Software: 1 accessory
##           2 macros, 0 = Sh0, 1 = Sh1
##
## history: 2016-12-06 A.Tillner, erste Version
##
## how to use:
## A) configure
##    In section 1, define your ports and the desired accessory number
##    In section 2, define your settings (like brightness and speed)
## B) load and run this script
##    what will happen?
##    1. the ports will be configured with the given parameters
##    2. three new macros for the aspects will be defined
##    3. these macros will be assigned to the accessory
## C) have fun
##
## -------------- 1. Select Target
## #var(int macro0, int this_accessory) 
## <-- ## set ( $my_accessory = 1)
## <-- ## set ( $my_macro = 10)         ## wir verwenden drei Macros, Nr. 10 und die folgenden zwei
## <-- ## set ( $start_led = 2)         ## bei ws2811, die erste LED ist an Lightport 2 angeschlossen
##instruction(text:de="Konfiguration eines DB Licht-Sperrsignal: <br>Erster Lightport = rt, ws <br>Erstes Macro = Sh0, Sh1", text:en="Configuration of a stop signal (DB): first lightport = rt, ws")
##
##input($my_accessory:accessory, text:de="Nummer des zu erzeugenden Accessory", text:en="Nummber of the Accessory", default=0)
##input($my_macro:macro, text:de="Nummer des ersten Macros (Sh0)", text:en="Number of the first macro (Sh0)", default=0)
##input($start_led:light, text:de="Nummer des ersten Lightport (rt LED)", text:en="Number of the first Lightport (rt LED)", default=0)
##input($prevent_replace_labels:boolean, text:de="Keine Namen für Accessory, Makros, Ports ersetzen", text:en="Prevent replace labels for accessory, macro and ports", default=false)

#set($led_gap = 1)           ## die anderen vier LED sind an den folgenden vier Lightports angeschlossen
#set($AccessoyName = "Licht-Sperrsignal")
##
## -------------- 2. set Parameters for Ports
#set($WertbeiOff = 0)
#set($WertbeiOn = 200)
#set($DimmzeitOff = 10)
#set($DimmzeitOn = 10)
#set($Ueberblenden = 15)
#set($OnTime = 0)

## -------------- 3. execute part of the script

#set($macro0 = $my_macro)    ## need some additional macros
#set($macro1 = $macro0 + 1)    ## need some additional macros

#set($led_rt = $start_led)    ## LED rt1 an erstem lightport, der eingegeben wurde
#set($led_ws = 1 * $led_gap + $start_led) ## LED rt2 an nÃ¤chstem lightport

#######################################
## Setzt label, wenn ${prevent_replace_labels} NICHT true
#if (!${prevent_replace_labels})

## Set label of Ports
set light ${led_rt} name="${AccessoyName}${my_accessory}_${led_rt}_rt"
set light ${led_ws} name="${AccessoyName}${my_accessory}_${led_ws}_ws"

## Set label of Macro
set macro ${macro0} name="${AccessoyName}${my_accessory}_Sp0"
set macro ${macro1} name="${AccessoyName}${my_accessory}_Sp1"

## Set label of Asseccories
set accessory ${my_accessory} name="${AccessoyName}${my_accessory}"

#end
#######################################

## 
## Ports: (set Parameters)

config port ptype=light ValueOff=${WertbeiOff} ValueOn=${WertbeiOn} DimmOff=${DimmzeitOff} DimmOn=${DimmzeitOn} number=${led_rt}
config port ptype=light ValueOff=${WertbeiOff} ValueOn=${WertbeiOn} DimmOff=${DimmzeitOff} DimmOn=${DimmzeitOn} number=${led_ws}

################################################/
## Macro Ausfahrsignal Sp0
##
select macro ${macro0}
config macro repeat=1 slowdown=1
## Signal dunkel
add step ptype=light action=down number=${led_rt}
add step ptype=light action=down number=${led_ws}

## Sp0 setzten, $Ueberblenden Ticks dunkel
add step ptype=light action=up delay=${Ueberblenden} number=${led_rt}
##
## Ende Macro Ausfahrsignal Sp0
##
################################################/
## Macro Ausfahrsignal Sp1
##
select macro ${macro1}
config macro repeat=1 slowdown=1
## Signal dunkel
add step ptype=light action=down number=${led_rt}
add step ptype=light action=down number=${led_ws}

## Sp1 setzten, $Ueberblenden Ticks dunkel
add step ptype=light action=up delay=${Ueberblenden} number=${led_ws}
##
## Ende Macro Ausfahrsignal Sp1
##
################################################/

## Define Accessory
select accessory ${my_accessory}
add aspect 0 macroname="${AccessoyName}${my_accessory}_Sp0"
add aspect 1 macroname="${AccessoyName}${my_accessory}_Sp1"
