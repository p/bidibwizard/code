## Dieses Script definiert ein Asseccory, 
## um die fuenf/sechs LED eines Lichtvorsignal (DB)
## anzusteuern.
##
## Hardware: 4 Lightport (Port 0 = gn1, 1 = gn2, 2 = ge1, 3 = ge2)
## Software: 1 accessory
##           4 macros, 0 = Vr0, 1 = Vr1, 2 = Vr2, 3 = dark
##
## history: 2016-12-07 A.Tillner, erste Version
##
## how to use:
## A) configure
##    In section 1, define your ports and the desired accessory number
##    In section 2, define your settings (like brightness and speed)
## B) load and run this script
##    what will happen?
##    1. the ports will be configured with the given parameters
##    2. three new macros for the aspects will be defined
##    3. these macros will be assigned to the accessory
## C) have fun
##
## -------------- 1. Select Target
## #var(int macro0, int this_accessory) 
## <-- ## set ( $my_accessory = 1)
## <-- ## set ( $my_macro = 10)         ## wir verwenden drei Macros, Nr. 10 und die folgenden zwei
## <-- ## set ( $start_led = 2)         ## bei ws2811, die erste LED ist an Lightport 2 angeschlossen
##instruction(text:de="Konfiguration eines DB Lichtvorsignal: <br>Erster Lightport = ge1, ge2, gn1, gn2 <br>Erstes Macro = Vr0, Vr1, Vr2, Dunkel", text:en="Configuration of a light signal (DB): <br>first lightport = ge1, ge2, gn1, gn2")
##
##input($my_accessory:accessory, text:de="Nummer des zu erzeugenden Accessory", text:en="Nummber of the Accessory", default=2)
##input($my_macro:macro, text:de="Nummer des ersten Macros (Vr0)", text:en="Number of the first macro (Vr0)", default=2)
##input($start_led:int, text:de="Nummer des ersten Lightport (gn1)", text:en="Number of the first Lightport (gn1)", default=10)
##input($prevent_replace_labels:boolean, text:de="Keine Namen f�r Accessory, Makros, Ports ersetzen", text:en="Prevent replace labels for accessory, macro and ports", default=false)

#set($led_gap = 1)           ## die anderen vier LED sind an den folgenden vier Lightports angeschlossen
#set($AccessoyName = "Lichtvorsignal")
##
## -------------- 2. set Parameters for Ports
#set($WertbeiOff = 0)
#set($WertbeiOn = 200)
#set($DimmzeitOff = 10)
#set($DimmzeitOn = 10)
#set($Ueberblenden = 15)
#set($OnTime = 0)

## -------------- 3. execute part of the script

#set($macro0 = $my_macro)    ## need some additional macros
#set($macro1 = $macro0 + 1)    ## need some additional macros
#set($macro2 = $macro0 + 2)    ## need some additional macros
#set($macro3 = $macro0 + 3)    ## need some additional macros
#set($led_gn1 = $start_led)    ## LED rt1 an erstem lightport, der eingegeben wurde
#set($led_gn2 = 1 * $led_gap + $start_led) ## LED rt2 an nächstem lightport
#set($led_ge1 = 2 * $led_gap + $start_led)  ## LED gn an nächstem lightport
#set($led_ge2 = 3 * $led_gap + $start_led)  ## LED ge an nächstem lightport

#######################################
## Setzt label, wenn ${prevent_replace_labels} NICHT true
#if (!${prevent_replace_labels})
## 
## Set label of Ports
set light ${led_ge1} name="${AccessoyName}${my_accessory}_${led_gn1}_gn1"
set light ${led_ge2} name="${AccessoyName}${my_accessory}_${led_gn2}_gn2"
set light ${led_gn1} name="${AccessoyName}${my_accessory}_${led_ge1}_ge1"
set light ${led_gn2} name="${AccessoyName}${my_accessory}_${led_ge2}_ge2"

## Set label of Macro
set macro ${macro0} name="${AccessoyName}${my_accessory}_Vr0"
set macro ${macro1} name="${AccessoyName}${my_accessory}_Vr1"
set macro ${macro2} name="${AccessoyName}${my_accessory}_Vr2"
set macro ${macro3} name="${AccessoyName}${my_accessory}_Dunkel"

## Set label of Accessories
set accessory ${my_accessory} name="${AccessoyName}${my_accessory}"

#end
#######################################

## 
## Ports: (set Parameters)
config port ptype=light ValueOff=${WertbeiOff} ValueOn=${WertbeiOn} DimmOff=${DimmzeitOff} DimmOn=${DimmzeitOn} number=${led_ge1}
config port ptype=light ValueOff=${WertbeiOff} ValueOn=${WertbeiOn} DimmOff=${DimmzeitOff} DimmOn=${DimmzeitOn} number=${led_ge2}
config port ptype=light ValueOff=${WertbeiOff} ValueOn=${WertbeiOn} DimmOff=${DimmzeitOff} DimmOn=${DimmzeitOn} number=${led_gn1}
config port ptype=light ValueOff=${WertbeiOff} ValueOn=${WertbeiOn} DimmOff=${DimmzeitOff} DimmOn=${DimmzeitOn} number=${led_gn2}



################################################/
## Macro Ausfahrsignal Vr0
##
select macro ${macro0}
config macro repeat=1 slowdown=1
## Signal dunkel
add step ptype=light action=down number=${led_ge1}
add step ptype=light action=down number=${led_ge2}
add step ptype=light action=down number=${led_gn1}
add step ptype=light action=down number=${led_gn2}

## Vr0 setzten, $Ueberblenden Ticks dunkel
add step ptype=light action=up delay=${Ueberblenden} number=${led_ge1}
add step ptype=light action=up number=${led_ge2}
##
## Ende Macro Ausfahrsignal Vr0
##
################################################/
## Macro Ausfahrsignal Vr1
##
select macro ${macro1}
config macro repeat=1 slowdown=1
## Signal dunkel
add step ptype=light action=down number=${led_ge1}
add step ptype=light action=down number=${led_ge2}
add step ptype=light action=down number=${led_gn1}
add step ptype=light action=down number=${led_gn2}

## Vr1 setzten, $Ueberblenden Ticks dunkel
add step ptype=light action=up delay=${Ueberblenden} number=${led_gn1}
add step ptype=light action=up number=${led_gn2}
##
## Ende Macro Ausfahrsignal Vr1
##
################################################/
## Macro Ausfahrsignal Vr2
##
select macro ${macro2}
config macro repeat=1 slowdown=1
## Signal dunkel
add step ptype=light action=down number=${led_ge1}
add step ptype=light action=down number=${led_ge2}
add step ptype=light action=down number=${led_gn1}
add step ptype=light action=down number=${led_gn2}

## Vr2 setzten, $Ueberblenden Ticks dunkel
add step ptype=light action=up delay=${Ueberblenden} number=${led_gn1}
add step ptype=light action=up number=${led_ge2}
##
## Ende Macro Ausfahrsignal Vr2
##
################################################/
## Macro Ausfahrsignal Dunkel
##
select macro ${macro3}
config macro repeat=1 slowdown=1
## Signal dunkel
add step ptype=light action=down number=${led_ge1}
add step ptype=light action=down number=${led_ge2}
add step ptype=light action=down number=${led_gn1}
add step ptype=light action=down number=${led_gn2}

##
## Ende Macro Ausfahrsignal Dunkel
##

## Define Accessory
select accessory ${my_accessory}
add aspect 0 macroname="${AccessoyName}${my_accessory}_Vr0"
add aspect 1 macroname="${AccessoyName}${my_accessory}_Vr1"
add aspect 2 macroname="${AccessoyName}${my_accessory}_Vr2"
add aspect 3 macroname="${AccessoyName}${my_accessory}_Dunkel"