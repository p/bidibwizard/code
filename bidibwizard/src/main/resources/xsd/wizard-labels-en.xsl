<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:labels="http://www.bidib.org/schema/labels">
	<xsl:output method="html" encoding="UTF-8" />

	<!-- <xsl:strip-space elements="*" /> -->

	<xsl:template match="/labels:Labels">

		<html>
			<head>
				<title>BiDiB-Wizard Accessory Label report</title>
			</head>
			<body>
				<h2>BiDiB-Wizard Accessory Label</h2>
				<xsl:apply-templates />
			</body>
		</html>

	</xsl:template>

	<xsl:template match="labels:UniqueId">
		UniqueID:
		<b>
			<xsl:value-of select="." />
		</b>
		<br />
	</xsl:template>

	<xsl:template match="labels:LabelNode/labels:Label">
		Accessory: Id: <xsl:value-of select="./labels:Index" />, Label: 
		<xsl:value-of select="./labels:LabelString" />
		<br />
		<xsl:apply-templates select="labels:ChildLabels" />
	</xsl:template>

	<xsl:template match="labels:ChildLabels">
		<h3>Aspects</h3>
		<table border="0" rules="none" cellpadding="4" cellspacing="0"
			width="90%">
		<colgroup>
			<col span="1" style="width: 5%;" />
			<col span="1" style="width: 85%;" />
		</colgroup>
		<tr>
			<th bgcolor="lightgrey" >Index</th>
			<th bgcolor="lightgrey">Label</th>
		</tr>
		<xsl:for-each select="labels:Label">
			<tr>
				<td>
					<xsl:value-of select="labels:Index" />
				</td>
				<td>
					<xsl:value-of select="labels:LabelString" />
				</td>
			</tr>
		</xsl:for-each>
		</table>
		<!-- 
		Aspect: Id:
		<xsl:value-of select="./labels:Index" />
		, Label:
		<xsl:value-of select="./labels:LabelString" />
		<br />
		 -->
	</xsl:template>

	<xsl:template match="labels:LabelNode">
		<br />

		<xsl:apply-templates>
			<xsl:sort select="LabelNode/Label/Index" order="ascending"
				data-type="number" />
		</xsl:apply-templates>

	</xsl:template>

</xsl:stylesheet>
