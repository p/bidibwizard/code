package org.bidib.wizard.labels;

import java.io.File;
import java.net.URISyntaxException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.bidib.wizard.mvc.preferences.model.Preferences;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class AccessoryLabelFactoryTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(AccessoryLabelFactoryTest.class);

    private static final String OUTPUT_TARGET_DIR = "target/accessoryAspectLabels";

    private static final String OUTPUT_FILENAME_ACCESSORY = "accessoryLabels-test%d.xml";

    @BeforeClass
    public void prepareOutputDirectory() {
        try {
            File outputDir = new File(OUTPUT_TARGET_DIR);
            if (!outputDir.exists()) {
                outputDir.mkdir();
            }
            else {
                FileUtils.cleanDirectory(outputDir);
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Create output directory failed: {}", OUTPUT_TARGET_DIR, ex);
        }

        // set the headless flag
        System.setProperty("java.awt.headless", "true");

        String path = AccessoryLabelFactoryTest.class.getResource("/.").getPath();
        LOGGER.info("Set path for preferences: {}", path);
        Preferences.setPath(path);

        File oldLabelPath = new File(path, "../_BiDiBWizard");
        Preferences.getInstance().setLabelPath(oldLabelPath.getPath());
    }

    @Test
    public void saveAccessoryLabelsTest() {

        AccessoryLabelFactory factory = new AccessoryLabelFactory();
        // factory.setUseBiDiBLabelFormat(false);

        Labels accessoryLabels = new Labels();
        String fileName = OUTPUT_TARGET_DIR + "/" + String.format(OUTPUT_FILENAME_ACCESSORY, 1);
        boolean gzip = false;

        LabelNodeType labelNode = new LabelNodeType();
        labelNode.setUniqueId(1L);
        LabelType accessoryLabel = new LabelType();
        accessoryLabel.setIndex(0);
        accessoryLabel.setLabelString("Test 1");

        labelNode.getLabel().add(accessoryLabel);

        //
        accessoryLabel = new LabelType();
        accessoryLabel.setIndex(1);
        accessoryLabel.setLabelString("Test 2");

        labelNode.getLabel().add(accessoryLabel);

        accessoryLabels.getLabelNode().add(labelNode);

        factory.saveLabels(labelNode.getUniqueId(), accessoryLabels, fileName, gzip);
    }

    @Test
    public void saveAccessoryLabels2Test() {

        AccessoryLabelFactory factory = new AccessoryLabelFactory();
        // factory.setUseBiDiBLabelFormat(false);

        Labels accessoryLabels = new Labels();
        String fileName = OUTPUT_TARGET_DIR + "/" + String.format(OUTPUT_FILENAME_ACCESSORY, 2);
        boolean gzip = false;

        LabelNodeType labelNode = new LabelNodeType();
        labelNode.setType("accessories");
        labelNode.setUniqueId(1L);
        labelNode.setLabelString("Node 1");

        accessoryLabels.getLabelNode().add(labelNode);

        // create the 1st accessory
        LabelType accessoryLabel = new LabelType();
        accessoryLabel.setType("accessory");
        accessoryLabel.setIndex(0);
        accessoryLabel.setLabelString("Accessory 1");

        labelNode.getLabel().add(accessoryLabel);

        // create the 2nd accessory
        accessoryLabel = new LabelType();
        accessoryLabel.setType("accessory");
        accessoryLabel.setIndex(1);
        accessoryLabel.setLabelString("Accessory 2");

        labelNode.getLabel().add(accessoryLabel);

        ChildLabelType childLabel = new ChildLabelType();
        accessoryLabel.setChildLabels(childLabel);

        // create the 1st aspect
        LabelType accessoryAspectLabel = new LabelType();
        accessoryAspectLabel.setType("aspect");
        accessoryAspectLabel.setIndex(0);
        accessoryAspectLabel.setLabelString("Aspect 0");
        childLabel.getLabel().add(accessoryAspectLabel);

        // create the 2nd aspect
        accessoryAspectLabel = new LabelType();
        accessoryAspectLabel.setType("aspect");
        accessoryAspectLabel.setIndex(1);
        accessoryAspectLabel.setLabelString("Aspect 1");
        childLabel.getLabel().add(accessoryAspectLabel);

        factory.saveLabels(labelNode.getUniqueId(), accessoryLabels, fileName, gzip);
    }

    @Test
    public void loadAccessoryLabelsTest() throws URISyntaxException {
        WizardLabelFactory.setSkipMigration(true);

        final String filePath =
            AccessoryLabelFactory.class.getResource("/accessoryAspectLabels/Node_00000000000001.xml").toURI().getPath();

        long uniqueId = 1L;
        AccessoryLabelFactory factory = new AccessoryLabelFactory() {
            @Override
            protected String getDefaultSearchPath() {
                return FilenameUtils.getPath(filePath);
            }
        };
        // factory.setUseBiDiBLabelFormat(false);

        Labels labels = factory.getLabels(uniqueId, filePath);
        Assert.assertNotNull(labels);

        Assert.assertNotNull(labels.getLabelNode());
        Assert.assertEquals(labels.getLabelNode().size(), 1);

        LabelNodeType labelNode = labels.getLabelNode().get(0);
        Assert.assertNotNull(labelNode);

        Assert.assertEquals(labelNode.getType(), "node");
        Assert.assertEquals(labelNode.getUniqueId(), uniqueId);

        Assert.assertNotNull(labelNode.getLabel());
        Assert.assertEquals(labelNode.getLabel().size(), 2);

        // get accessory
        LabelType label = labelNode.getLabel().get(1);
        Assert.assertNotNull(label);

        Assert.assertEquals(label.getType(), "accessory");
        Assert.assertEquals(label.getIndex(), 1);
        Assert.assertEquals(label.getLabelString(), "Signal 1");

        Assert.assertNotNull(label.getChildLabels());
        Assert.assertNotNull(label.getChildLabels().getLabel());
        Assert.assertEquals(label.getChildLabels().getLabel().size(), 2);

        // get aspect 0
        LabelType aspectLabel = label.getChildLabels().getLabel().get(0);
        Assert.assertNotNull(aspectLabel);

        Assert.assertEquals(aspectLabel.getType(), "aspect");
        Assert.assertEquals(aspectLabel.getIndex(), 0);
        Assert.assertEquals(aspectLabel.getLabelString(), "Hp0");

        // get aspect 1
        aspectLabel = label.getChildLabels().getLabel().get(1);
        Assert.assertNotNull(aspectLabel);

        Assert.assertEquals(aspectLabel.getType(), "aspect");
        Assert.assertEquals(aspectLabel.getIndex(), 1);
        Assert.assertEquals(aspectLabel.getLabelString(), "Hp1");
    }

}
