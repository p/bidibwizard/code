package org.bidib.wizard.labels;

import java.io.File;
import java.net.URISyntaxException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class LightPortLabelFactoryTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(LightPortLabelFactoryTest.class);

    private static final String OUTPUT_TARGET_DIR = "target/lightPortLabels";

    private static final String OUTPUT_FILENAME_LIGHTPORTS = "Node_0000000000000%d.xml";

    @BeforeClass
    public void prepareOutputDirectory() {
        try {
            File outputDir = new File(OUTPUT_TARGET_DIR);
            if (!outputDir.exists()) {
                outputDir.mkdir();
            }
            else {
                FileUtils.cleanDirectory(outputDir);
            }
        }
        catch (Exception ex) {
            LOGGER.warn("Create output directory failed: {}", OUTPUT_TARGET_DIR, ex);
        }

        // set the headless flag
        System.setProperty("java.awt.headless", "true");
    }

    @Test
    public void saveLightPortLabelsTest() {

        LightPortLabelFactory factory = new LightPortLabelFactory();

        Labels lightPortLabels = new Labels();
        String fileName = OUTPUT_TARGET_DIR + "/" + String.format(OUTPUT_FILENAME_LIGHTPORTS, 1);
        boolean gzip = false;

        LabelNodeType labelNode = new LabelNodeType();
        labelNode.setUniqueId(1L);
        LabelType lightPortLabel = new LabelType();
        lightPortLabel.setIndex(0);
        lightPortLabel.setLabelString("Test 1");

        labelNode.getLabel().add(lightPortLabel);

        // new label
        lightPortLabel = new LabelType();
        lightPortLabel.setIndex(1);
        lightPortLabel.setLabelString("Test äüöé");

        labelNode.getLabel().add(lightPortLabel);

        // new label
        lightPortLabel = new LabelType();
        lightPortLabel.setIndex(2);
        lightPortLabel.setLabelString("Test Gruß");

        labelNode.getLabel().add(lightPortLabel);

        lightPortLabels.getLabelNode().add(labelNode);

        factory.saveLabels(labelNode.getUniqueId(), lightPortLabels, fileName, gzip);
    }

    @Test
    public void loadLightPortLabelsTest() throws URISyntaxException {
        WizardLabelFactory.setSkipMigration(true);

        Long uniqueId = Long.valueOf(1L);

        final String filePath =
            LightPortLabelFactory.class.getResource("lightPortLabels/Node_00000000000001.xml").toURI().getPath();

        LightPortLabelFactory factory = new LightPortLabelFactory() {
            @Override
            protected String getDefaultSearchPath() {
                return FilenameUtils.getPath(filePath);
            }
        };

        Labels labels = factory.getLabels(uniqueId, filePath);
        Assert.assertNotNull(labels);

        Assert.assertNotNull(labels.getLabelNode());
        Assert.assertEquals(labels.getLabelNode().size(), 1);

        LabelNodeType labelNode = labels.getLabelNode().get(0);
        Assert.assertNotNull(labelNode);

        Assert.assertNotNull(labelNode.getLabel());
        Assert.assertEquals(labelNode.getLabel().size(), 3);

        // get light port
        LabelType label = labelNode.getLabel().get(1);
        Assert.assertNotNull(label);

        LOGGER.info("Current label: {}", label);

        Assert.assertEquals(label.getIndex(), 1);
        Assert.assertEquals(label.getLabelString(), "Test äüöé");

        // get light port
        label = labelNode.getLabel().get(2);
        Assert.assertNotNull(label);

        LOGGER.info("Current label: {}", label);

        Assert.assertEquals(label.getIndex(), 2);
        Assert.assertEquals(label.getLabelString(), "Test Gruß");
    }

    @Test
    public void saveAndLoadLightPortLabelsTest() throws URISyntaxException {

        WizardLabelFactory.setSkipMigration(true);

        Long uniqueId = Long.valueOf(2L);

        final String filePath =
            FilenameUtils.getPath(LightPortLabelFactory.class
                .getResource("lightPortLabels/Node_00000000000001.xml").toURI().getPath());

        final String fileName = filePath + String.format(OUTPUT_FILENAME_LIGHTPORTS, uniqueId);

        LOGGER.info("Prepared filePath: {}, fileName: {}", filePath, fileName);

        LightPortLabelFactory factory = new LightPortLabelFactory() {
            @Override
            protected String getDefaultSearchPath() {
                return filePath;
            }
        };

        Labels lightPortLabels = new Labels();

        boolean gzip = false;

        LabelNodeType labelNode = new LabelNodeType();
        labelNode.setUniqueId(uniqueId);
        LabelType lightPortLabel = new LabelType();
        lightPortLabel.setIndex(0);
        lightPortLabel.setLabelString("Test 1");

        labelNode.getLabel().add(lightPortLabel);

        // new label
        lightPortLabel = new LabelType();
        lightPortLabel.setIndex(1);
        lightPortLabel.setLabelString("Test äüöé");

        labelNode.getLabel().add(lightPortLabel);

        // new label
        lightPortLabel = new LabelType();
        lightPortLabel.setIndex(2);
        lightPortLabel.setLabelString("Test Gruß");

        labelNode.getLabel().add(lightPortLabel);

        lightPortLabels.getLabelNode().add(labelNode);

        factory.saveLabels(labelNode.getUniqueId(), lightPortLabels, fileName, gzip);

        // release labelNode
        labelNode = null;

        // load labels from new created file
        Labels labels = factory.getLabels(uniqueId, fileName);
        Assert.assertNotNull(labels);

        Assert.assertNotNull(labels.getLabelNode());
        Assert.assertEquals(labels.getLabelNode().size(), 1);

        labelNode = labels.getLabelNode().get(0);
        Assert.assertNotNull(labelNode);

        Assert.assertNotNull(labelNode.getLabel());
        Assert.assertEquals(labelNode.getLabel().size(), 3);

        // get light port
        LabelType label = labelNode.getLabel().get(1);
        Assert.assertNotNull(label);

        LOGGER.info("Current label: {}", label);

        Assert.assertEquals(label.getIndex(), 1);
        Assert.assertEquals(label.getLabelString(), "Test äüöé");

        // get light port
        label = labelNode.getLabel().get(2);
        Assert.assertNotNull(label);

        LOGGER.info("Current label: {}", label);

        Assert.assertEquals(label.getIndex(), 2);
        Assert.assertEquals(label.getLabelString(), "Test Gruß");
    }

}
