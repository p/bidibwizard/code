package org.bidib.wizard.mvc.main.view.table;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TwoDimensionalArrayTest {

    @Test
    public void clear() {
        TwoDimensionalArray<SliderEditor> sliderEditors = new TwoDimensionalArray<SliderEditor>(16);
        Assert.assertNull(sliderEditors.get(0, 0));

        SliderEditor sliderEditor = new SliderEditor();
        sliderEditors.set(sliderEditor, 0, 0);
        Assert.assertNotNull(sliderEditors.get(0, 0));
        Assert.assertEquals(sliderEditors.get(0, 0), sliderEditor);

        sliderEditor = new SliderEditor(0, 100, null);
        sliderEditors.set(sliderEditor, 3, 5);
        Assert.assertNotNull(sliderEditors.get(3, 5));
        Assert.assertEquals(sliderEditors.get(3, 5), sliderEditor);

        // clear the sliderEditors
        sliderEditors.clear();

        Assert.assertNull(sliderEditors.get(0, 0));
        Assert.assertNull(sliderEditors.get(3, 5));
    }

    @Test
    public void get() {
        TwoDimensionalArray<SliderEditor> sliderEditors = new TwoDimensionalArray<SliderEditor>(16);
        SliderEditor ed1 = sliderEditors.get(0, 0);
        Assert.assertNull(ed1);

        Assert.assertNull(sliderEditors.get(0, 2));
        Assert.assertNull(sliderEditors.get(0, 4));
        Assert.assertNull(sliderEditors.get(0, 7));

        Assert.assertNull(sliderEditors.get(1, 3));
        Assert.assertNull(sliderEditors.get(2, 3));
        Assert.assertNull(sliderEditors.get(5, 3));
    }

    @Test
    public void set() {
        TwoDimensionalArray<SliderEditor> sliderEditors = new TwoDimensionalArray<SliderEditor>(16);
        Assert.assertNull(sliderEditors.get(0, 0));

        SliderEditor sliderEditor = new SliderEditor();
        sliderEditors.set(sliderEditor, 0, 0);
        Assert.assertNotNull(sliderEditors.get(0, 0));
        Assert.assertEquals(sliderEditors.get(0, 0), sliderEditor);

        sliderEditor = new SliderEditor(0, 100, null);
        sliderEditors.set(sliderEditor, 3, 5);
        Assert.assertNotNull(sliderEditors.get(3, 5));
        Assert.assertEquals(sliderEditors.get(3, 5), sliderEditor);
    }
}
