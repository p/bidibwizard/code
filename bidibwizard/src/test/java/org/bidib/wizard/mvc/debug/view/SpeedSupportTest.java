package org.bidib.wizard.mvc.debug.view;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.testng.Assert;
import org.testng.annotations.Test;

public class SpeedSupportTest {

    private final SimpleDateFormat sdf = new SimpleDateFormat("mm:ss.SSS");

    @Test
    public void parseSpeed() {

        StringBuilder input = new StringBuilder();
        // add the timestamp to the line
        input.append(sdf.format(new Date()));
        input.append(" - ");
        int start = 12;

        input.append("time = 1234; length =345*87; speed =100km/h");

        String speed = SpeedSupport.parseSpeed(input, start);
        Assert.assertNotNull(speed);
        Assert.assertEquals(speed, "100");
    }

    @Test
    public void parseSpeedWithoutTimestamp() {

        StringBuilder input = new StringBuilder();
        int start = 0;

        input.append("time = 1234; length =345*87; speed =100km/h");

        String speed = SpeedSupport.parseSpeed(input, start);
        Assert.assertNotNull(speed);
        Assert.assertEquals(speed, "100");
    }
}
