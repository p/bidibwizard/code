package org.bidib.wizard.mvc.script.view.input;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.bidib.wizard.mvc.script.view.input.InputParametersDialog.InputLineParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class InputParametersDialogTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(InputParametersDialogTest.class);

    // private static final String REGEX = "text\\:[a-z]{2}=\"([^\"]*)\"|default=(\"([^\"]*)\"|([0-9a-zA-Z]*))|(\\S+)";

    @Test
    public void testScanInstruction() {
        // ##instruction(text:de="Konfiguration eines Lichtsignal der DB", text:en="Configuration of a licht signal of
        // DB")

        String line =
            "##instruction(text:de=\"Konfiguration eines Lichtsignal der DB\", text:en=\"Configuration of a licht signal of DB\")";
        // parse the line
        String caption = line.substring(line.indexOf("(") + 1, line.lastIndexOf(")"));

        String regex = "text\\:[a-z]{2}=\"([^\"]*)\"|(\\S+)";

        int foundCounter = 0;
        Matcher m = Pattern.compile(regex).matcher(caption);
        while (m.find()) {
            if (m.group(1) != null) {
                LOGGER.info("Found text: [" + m.group(0) + "]");
                foundCounter++;
            }
            // else {
            // LOGGER.info("Plain [" + m.group(2) + "]");
            // }
        }

        Assert.assertEquals(foundCounter, 2);
    }

    @Test
    public void testScanInstructionSingle() {
        // ##instruction(text:de="Konfiguration eines Lichtsignal der DB")

        String line = "##instruction(text:de=\"Konfiguration eines Lichtsignal der DB\")";
        // parse the line
        String caption = line.substring(line.indexOf("(") + 1, line.lastIndexOf(")"));

        String regex = "text\\:[a-z]{2}=\"([^\"]*)\"|(\\S+)";

        int foundCounter = 0;
        Matcher m = Pattern.compile(regex).matcher(caption);
        while (m.find()) {
            if (m.group(1) != null) {
                LOGGER.info("Found text: [" + m.group(0) + "]");
                foundCounter++;
                Assert.assertEquals(m.group(1), "Konfiguration eines Lichtsignal der DB");
            }
            // else {
            // LOGGER.info("Plain [" + m.group(2) + "]");
            // }
        }
        Assert.assertEquals(foundCounter, 1);
    }

    @Test
    public void testScanInputDefaultNumber() {
        // ##input(my_accessory, text:de="Eingabe Nummer des zu erzeugenden Accessory", default=3)
        // ##input(my_accessory:accessory, text:de="Auswahl des zu erzeugenden Accessory", default=3)

        String line = "##input(my_accessory, text:de=\"Eingabe Nummer des zu erzeugenden Accessory\", default=3)";
        // parse the line
        String caption = line.substring(line.indexOf("(") + 1, line.lastIndexOf(")"));

        // String regex = "text\\:[a-z]{2}=\"([^\"]*)\"|default=(\"([^\"]*)\"|([0-9]*))|(\\S+)";

        int foundCounter = 0;
        int foundPlainCounter = 0;
        Matcher m = Pattern.compile(InputParametersDialog.REGEX_INPUT).matcher(caption);
        while (m.find()) {
            if (m.group(1) != null) {
                LOGGER.info("1. Found text: [" + m.group(0) + "]");
                foundCounter++;
                // Assert.assertEquals(m.group(1), "Konfiguration eines Lichtsignal der DB");
            }
            else if (m.group(2) != null) {
                LOGGER.info("2. Found text: [" + m.group(0) + "]");
                foundCounter++;
            }
            else {
                LOGGER.info("Plain [" + m.group(0) + "]");
                String text = m.group(0);
                text = StringUtils.strip(text, ",").trim();

                if (StringUtils.isNotBlank(text)) {
                    LOGGER.info("3. Found text: [{}]", text);
                    foundPlainCounter++;
                }
            }
        }
        Assert.assertEquals(foundCounter, 2);
        Assert.assertEquals(foundPlainCounter, 1);
    }

    @Test
    public void testScanInputDefaultText() {
        // ##input(my_accessory, text:de="Eingabe Nummer des zu erzeugenden Accessory", default=3)
        // ##input(my_accessory:accessory, text:de="Auswahl des zu erzeugenden Accessory", default=3)

        String line =
            "##input(my_accessory, text:de=\"Eingabe Nummer des zu erzeugenden Accessory\", default=\"Test\")";
        // parse the line
        String caption = line.substring(line.indexOf("(") + 1, line.lastIndexOf(")"));

        // String regex = "text\\:[a-z]{2}=\"([^\"]*)\"|default=(\"([^\"]*)\"|([0-9a-zA-Z]*))|(\\S+)";

        int foundCounter = 0;
        int foundPlainCounter = 0;
        Matcher m = Pattern.compile(InputParametersDialog.REGEX_INPUT).matcher(caption);
        while (m.find()) {
            if (m.group(1) != null) {
                LOGGER.info("1. Found text: [" + m.group(0) + "]");
                foundCounter++;
                // Assert.assertEquals(m.group(1), "Konfiguration eines Lichtsignal der DB");
            }
            else if (m.group(2) != null) {
                LOGGER.info("2. Found text: [" + m.group(0) + "]");
                foundCounter++;
            }
            else {
                LOGGER.info("Plain [" + m.group(0) + "]");
                String text = m.group(0);
                text = StringUtils.strip(text, ",").trim();

                if (StringUtils.isNotBlank(text)) {
                    LOGGER.info("3. Found text: [{}]", text);
                    foundPlainCounter++;
                }
            }
        }
        Assert.assertEquals(foundCounter, 2);
        Assert.assertEquals(foundPlainCounter, 1);
    }

    @Test
    public void testScanInputDefaultBoolean() {
        // ##input(my_accessory, text:de="Eingabe Nummer des zu erzeugenden Accessory", default=3)
        // ##input(my_accessory:accessory, text:de="Auswahl des zu erzeugenden Accessory", default=3)

        String line = "##input(my_accessory, text:de=\"Eingabe Nummer des zu erzeugenden Accessory\", default=false)";
        // parse the line
        String caption = line.substring(line.indexOf("(") + 1, line.lastIndexOf(")"));

        // String regex = "text\\:[a-z]{2}=\"([^\"]*)\"|default=(\"([^\"]*)\"|([0-9a-zA-Z]*))|(\\S+)";

        int foundCounter = 0;
        int foundPlainCounter = 0;
        Matcher m = Pattern.compile(InputParametersDialog.REGEX_INPUT).matcher(caption);
        while (m.find()) {
            if (m.group(1) != null) {
                LOGGER.info("1. Found text: [" + m.group(0) + "]");
                foundCounter++;
                // Assert.assertEquals(m.group(1), "Konfiguration eines Lichtsignal der DB");
            }
            else if (m.group(2) != null) {
                LOGGER.info("2. Found text: [" + m.group(0) + "]");
                foundCounter++;
            }
            else {
                LOGGER.info("Plain [" + m.group(0) + "]");
                String text = m.group(0);
                text = StringUtils.strip(text, ",").trim();

                if (StringUtils.isNotBlank(text)) {
                    LOGGER.info("3. Found text: [{}]", text);
                    foundPlainCounter++;
                }
            }
        }
        Assert.assertEquals(foundCounter, 2);
        Assert.assertEquals(foundPlainCounter, 1);
    }

    @Test
    public void testScanInputDefaultBooleanTyped() {

        String line =
            "##input(my_accessory:boolean, text:de=\"Eingabe Nummer des zu erzeugenden Accessory\", default=false)";
        // parse the line
        String caption = line.substring(line.indexOf("(") + 1, line.lastIndexOf(")"));

        int foundCounter = 0;
        int foundPlainCounter = 0;
        Matcher m = Pattern.compile(InputParametersDialog.REGEX_INPUT).matcher(caption);
        while (m.find()) {
            if (m.group(1) != null) {
                LOGGER.info("1. Found text: [" + m.group(0) + "]");
                foundCounter++;
                Assert.assertEquals(m.group(1), "Eingabe Nummer des zu erzeugenden Accessory");
            }
            else if (m.group(2) != null) {
                LOGGER.info("2. Found text: [" + m.group(0) + "]");
                foundCounter++;
            }
            else {
                LOGGER.info("Plain [" + m.group(0) + "]");
                String text = m.group(0);
                text = StringUtils.strip(text, ",").trim();

                if (StringUtils.isNotBlank(text)) {
                    LOGGER.info("3. Found text: [{}]", text);
                    foundPlainCounter++;
                }
            }
        }
        Assert.assertEquals(foundCounter, 2);
        Assert.assertEquals(foundPlainCounter, 1);
    }

    @Test
    public void inputLineTest() {
        String line =
            "##input($my_accessory:boolean, text:de=\"Eingabe Nummer des zu erzeugenden Accessory\", text:en=\"Select the accessory to create\", default=false)";

        InputParametersDialog dialog = new InputParametersDialog();
        InputLineParams params = dialog.scanInputLine("de", line, null);

        Assert.assertNotNull(params);

        Assert.assertEquals(params.variableName, "my_accessory");
        Assert.assertEquals(params.variableType, "boolean");
        Assert.assertEquals(params.defaultValue, "false");
        Assert.assertEquals(params.caption, "Eingabe Nummer des zu erzeugenden Accessory");
    }

    @Test
    public void inputLineTest2() {
        String line =
            "##input($my_accessory, text:de=\"Eingabe Nummer des zu erzeugenden Accessory\", text:en=\"Select the accessory to create\", default=false)";

        InputParametersDialog dialog = new InputParametersDialog();
        InputLineParams params = dialog.scanInputLine("de", line, null);

        Assert.assertNotNull(params);

        Assert.assertEquals(params.variableName, "my_accessory");
        Assert.assertEquals(params.variableType, "string");
        Assert.assertEquals(params.defaultValue, "false");
        Assert.assertEquals(params.caption, "Eingabe Nummer des zu erzeugenden Accessory");
    }

    @Test
    public void inputLineTestLangEn() {
        String line =
            "##input($my_accessory:boolean, text:de=\"Eingabe Nummer des zu erzeugenden Accessory\", text:en=\"Select the accessory to create\", default=false)";

        InputParametersDialog dialog = new InputParametersDialog();
        InputLineParams params = dialog.scanInputLine("en", line, null);

        Assert.assertNotNull(params);

        Assert.assertEquals(params.variableName, "my_accessory");
        Assert.assertEquals(params.variableType, "boolean");
        Assert.assertEquals(params.defaultValue, "false");
        Assert.assertEquals(params.caption, "Select the accessory to create");
    }
}
