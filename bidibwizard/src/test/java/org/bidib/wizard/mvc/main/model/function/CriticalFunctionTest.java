package org.bidib.wizard.mvc.main.model.function;

import org.bidib.jbidibc.exchange.lcmacro.CriticalSectionActionType;
import org.bidib.jbidibc.exchange.lcmacro.CriticalSectionPoint;
import org.bidib.jbidibc.exchange.lcmacro.LcMacroPointType;
import org.bidib.wizard.comm.CriticalFunctionStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CriticalFunctionTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(CriticalFunctionTest.class);

    @Test
    public void toLcMacroPointCriticalSectionBegin() {
        CriticalFunction criticalFunction = new CriticalFunction();
        criticalFunction.setAction(CriticalFunctionStatus.BEGIN);

        LcMacroPointType lcMacroPoint = criticalFunction.toLcMacroPoint();
        Assert.assertNotNull(lcMacroPoint);

        LOGGER.info("Current lcMacroPoint: {}", lcMacroPoint);
        Assert.assertTrue(lcMacroPoint instanceof CriticalSectionPoint);

        CriticalSectionPoint criticalSectionPoint = (CriticalSectionPoint) lcMacroPoint;
        Assert.assertNotNull(criticalSectionPoint);
        Assert.assertEquals(criticalSectionPoint.getCriticalSectionActionType(), CriticalSectionActionType.BEGIN);
    }

    @Test
    public void toLcMacroPointCriticalSectionEnd() {
        CriticalFunction criticalFunction = new CriticalFunction();
        criticalFunction.setAction(CriticalFunctionStatus.END);

        LcMacroPointType lcMacroPoint = criticalFunction.toLcMacroPoint();
        Assert.assertNotNull(lcMacroPoint);

        LOGGER.info("Current lcMacroPoint: {}", lcMacroPoint);
        Assert.assertTrue(lcMacroPoint instanceof CriticalSectionPoint);

        CriticalSectionPoint criticalSectionPoint = (CriticalSectionPoint) lcMacroPoint;
        Assert.assertNotNull(criticalSectionPoint);
        Assert.assertEquals(criticalSectionPoint.getCriticalSectionActionType(), CriticalSectionActionType.END);
    }
}
