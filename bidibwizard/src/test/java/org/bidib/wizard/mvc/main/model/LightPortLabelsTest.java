package org.bidib.wizard.mvc.main.model;

import java.io.File;

import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

public class LightPortLabelsTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(LightPortLabelsTest.class);

    @Test
    public void saveLabelsTest() {
        LOGGER.info("Save labels in very old format.");

        TestLightPortLabels lightPortLabels = new TestLightPortLabels();

        lightPortLabels.setLabel(0x5340D75001236L, 0, "LightPort_0_Test");
        lightPortLabels.setLabel(0x5340D75001236L, 1, "LightPort_1_Test");
        lightPortLabels.setLabel(0x5340D75001236L, 4, "LightPort_2_Test");

        lightPortLabels.setLabel(0x5340D75001237L, 0, "LightPort_0_Test");
        lightPortLabels.setLabel(0x5340D75001237L, 1, "LightPort_1_Test");
        lightPortLabels.setLabel(0x5340D75001237L, 4, "LightPort_2_Test");

        lightPortLabels.modified = true;

        lightPortLabels.save();

        Assert.assertTrue(new File(lightPortLabels.getFileName()).exists());
    }
}
