package org.bidib.wizard.mvc.script.view.utils;

import java.util.Locale;

import org.testng.Assert;
import org.testng.annotations.Test;

public class NodeScriptUtilsTest {

    @Test
    public void extractInstruction() {
        final String line =
            "##instruction(text:de=\"Konfiguration eines Lichtsignal der DB\", text:en=\"Configuration of a light signal of DB\")";

        Locale.setDefault(Locale.GERMAN);

        String instruction = NodeScriptUtils.extractInstruction(line);

        Assert.assertNotNull(instruction);
        Assert.assertEquals(instruction, "Konfiguration eines Lichtsignal der DB");

        Locale.setDefault(Locale.ENGLISH);

        instruction = NodeScriptUtils.extractInstruction(line);

        Assert.assertNotNull(instruction);
        Assert.assertEquals(instruction, "Configuration of a light signal of DB");
    }

    @Test
    public void extractInstructionHyperlink() {
        final String line =
            "##instruction(text:de=\"Konfiguration eines Lichtsignal der DB\", text:en=\"Configuration of a light signal of DB\", link=\"http://www.bidib.org\")";

        String hyperlink = NodeScriptUtils.extractInstructionHyperlink(line);

        Assert.assertNotNull(hyperlink);
        Assert.assertEquals(hyperlink, "http://www.bidib.org");
    }

    @Test
    public void extractInstructionHyperlink2() {
        final String line =
            "##instruction(text:de=\"Konfiguration eines Lichtsignal der DB\", text:en=\"Configuration of a light signal of DB\")";

        String hyperlink = NodeScriptUtils.extractInstructionHyperlink(line);

        Assert.assertNull(hyperlink);
    }

    @Test
    public void extractRequire() {
        final String line = "##require(vid=\"13\", pid=\"125\")";
        NodeRequirement requirement = NodeScriptUtils.extractRequire(line);

        Assert.assertNotNull(requirement);
        Assert.assertEquals(requirement.getVid(), "13");
        Assert.assertEquals(requirement.getPid(), "125");
    }

    @Test
    public void extractRequireMultiPid() {
        final String line = "##require(vid=\"13\", pid=\"125,126,127\")";
        NodeRequirement requirement = NodeScriptUtils.extractRequire(line);

        Assert.assertNotNull(requirement);
        Assert.assertEquals(requirement.getVid(), "13");
        Assert.assertEquals(requirement.getPid(), "125,126,127");
    }

    @Test
    public void extractRequireMultiPidWithSpaces() {
        final String line = "##require(vid=\"13\", pid=\"125, 126, 127\")";
        NodeRequirement requirement = NodeScriptUtils.extractRequire(line);

        Assert.assertNotNull(requirement);
        Assert.assertEquals(requirement.getVid(), "13");
        Assert.assertEquals(requirement.getPid(), "125,126,127");
    }

    @Test
    public void extractRequireEmpty() {
        final String line = "##require(vid=\"13\", pid=\"\")";
        NodeRequirement requirement = NodeScriptUtils.extractRequire(line);

        Assert.assertNotNull(requirement);
        Assert.assertEquals(requirement.getVid(), "13");
        Assert.assertEquals(requirement.getPid(), "");
    }

    @Test
    public void extractRequireWithSpaces() {
        final String line = "##require(vid = \"13\", pid = \"125\")";
        NodeRequirement requirement = NodeScriptUtils.extractRequire(line);

        Assert.assertNotNull(requirement);
        Assert.assertEquals(requirement.getVid(), "13");
        Assert.assertEquals(requirement.getPid(), "125");
    }
}
