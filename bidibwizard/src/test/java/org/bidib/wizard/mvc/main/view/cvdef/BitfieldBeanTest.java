package org.bidib.wizard.mvc.main.view.cvdef;

import org.testng.Assert;
import org.testng.annotations.Test;

public class BitfieldBeanTest {

    @Test
    public void setRadioBits() {
        BitfieldBean bean = new BitfieldBean();
        bean.setRadioBits(0x06);

        bean.setBit0(true); // 0x01
        Assert.assertEquals(bean.getBitfield().byteValue(), (byte) 0x01);

        bean.setBit1(true); // 0x03
        Assert.assertEquals(bean.getBitfield().byteValue(), (byte) 0x03);

        bean.setBit2(true); // 0x05
        Assert.assertEquals(bean.getBitfield().byteValue(), (byte) 0x05);

        bean.setBit3(true); // 0x0D
        Assert.assertEquals(bean.getBitfield().byteValue(), (byte) 0x0D);

        bean.setBit4(true); // 0x1D
        Assert.assertEquals(bean.getBitfield().byteValue(), (byte) 0x1D);

        bean.setBit1(true); // 0x1B
        Assert.assertEquals(bean.getBitfield().byteValue(), (byte) 0x1B);
    }
}
