package org.bidib.wizard.mvc.main.view.panel;

import org.bidib.wizard.mvc.main.model.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class NodeTreeTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(NodeTreeTest.class);

    private NodeTree nodeTree;

    @BeforeClass
    private void prepareEnv() {
        LOGGER.info("Set the jide license.");
        com.jidesoft.utils.Lm.verifyLicense("Andreas Kuhtz", "BiDiB-Wizard", "T1xTEztBM2Obp3w39SfBqkRoQBicczu");

        // create the node tree for testing purposes
        nodeTree = new NodeTree() {
            private static final long serialVersionUID = 1L;

            @Override
            protected void initialize() {
                // do nothing here
            }
        };

    }

    @Test
    public void isChildOfNode() {
        org.bidib.jbidibc.core.Node bidibParentNode =
            new org.bidib.jbidibc.core.Node(0, new byte[] { 0, 0, 0, 0 }, 0x12345678901233L);
        Node parentNode = new Node(bidibParentNode);
        NodeTree.NodeTreeNode parentTreeNode = new NodeTree.NodeTreeNode(parentNode);

        org.bidib.jbidibc.core.Node bidibNode1 =
            new org.bidib.jbidibc.core.Node(0, new byte[] { 1, 0, 0, 0 }, 0x12345678901234L);
        Node node1 = new Node(bidibNode1);

        boolean isChildOfNode = nodeTree.isChildOfNode(parentTreeNode, node1);

        Assert.assertTrue(isChildOfNode);

        org.bidib.jbidibc.core.Node bidibNode2 =
            new org.bidib.jbidibc.core.Node(0, new byte[] { 2, 0, 0, 0 }, 0x12345678901235L);
        Node node2 = new Node(bidibNode2);

        isChildOfNode = nodeTree.isChildOfNode(parentTreeNode, node2);

        Assert.assertTrue(isChildOfNode);

        LOGGER.info("processing subnode.");

        org.bidib.jbidibc.core.Node bidibNode3 =
            new org.bidib.jbidibc.core.Node(0, new byte[] { 1, 1, 0, 0 }, 0x12345678901235L);
        Node node3 = new Node(bidibNode3);

        NodeTree.NodeTreeNode childTreeNode = new NodeTree.NodeTreeNode(node1);
        isChildOfNode = nodeTree.isChildOfNode(childTreeNode, node3);

        Assert.assertTrue(isChildOfNode);

    }

    @Test
    public void testRootAndSubLevelNodes() {
        org.bidib.jbidibc.core.Node bidibNode1 =
            new org.bidib.jbidibc.core.Node(0, new byte[] { 0, 0, 0, 0 }, 0x12345678901234L);
        Node node1 = new Node(bidibNode1);
        org.bidib.jbidibc.core.Node bidibNode2 =
            new org.bidib.jbidibc.core.Node(0, new byte[] { 2, 1, 0, 0 }, 0x12345678901235L);
        Node node2 = new Node(bidibNode2);

        NodeTree.NodeTreeNode childTreeNode = new NodeTree.NodeTreeNode(node1);
        boolean isChildOfNode = nodeTree.isChildOfNode(childTreeNode, node2);

        Assert.assertFalse(isChildOfNode);
    }

    @Test
    public void testSameLevelNodes() {
        org.bidib.jbidibc.core.Node bidibNode1 =
            new org.bidib.jbidibc.core.Node(0, new byte[] { 1, 0, 0, 0 }, 0x12345678901234L);
        Node node1 = new Node(bidibNode1);
        org.bidib.jbidibc.core.Node bidibNode2 =
            new org.bidib.jbidibc.core.Node(0, new byte[] { 2, 0, 0, 0 }, 0x12345678901235L);
        Node node2 = new Node(bidibNode2);

        NodeTree.NodeTreeNode childTreeNode = new NodeTree.NodeTreeNode(node1);
        boolean isChildOfNode = nodeTree.isChildOfNode(childTreeNode, node2);

        Assert.assertFalse(isChildOfNode);
    }

    @Test
    public void testInvalidChild() {

        org.bidib.jbidibc.core.Node bidibNode2 =
            new org.bidib.jbidibc.core.Node(0, new byte[] { 2, 0, 0, 0 }, 0x12345678901235L);
        Node node2 = new Node(bidibNode2);
        NodeTree.NodeTreeNode parentTreeNode = new NodeTree.NodeTreeNode(node2);

        org.bidib.jbidibc.core.Node bidibNode3 =
            new org.bidib.jbidibc.core.Node(0, new byte[] { 1, 1, 0, 0 }, 0x12345678901235L);
        Node node3 = new Node(bidibNode3);

        boolean isChildOfNode = nodeTree.isChildOfNode(parentTreeNode, node3);

        Assert.assertFalse(isChildOfNode);

    }

    @Test
    public void testInvalidLevel() {

        org.bidib.jbidibc.core.Node bidibNode2 =
            new org.bidib.jbidibc.core.Node(0, new byte[] { 2, 0, 0, 0 }, 0x12345678901235L);
        Node node2 = new Node(bidibNode2);
        NodeTree.NodeTreeNode parentTreeNode = new NodeTree.NodeTreeNode(node2);

        org.bidib.jbidibc.core.Node bidibNode3 =
            new org.bidib.jbidibc.core.Node(0, new byte[] { 2, 1, 1, 0 }, 0x12345678901235L);
        Node node3 = new Node(bidibNode3);

        boolean isChildOfNode = nodeTree.isChildOfNode(parentTreeNode, node3);

        Assert.assertFalse(isChildOfNode);

    }
}
