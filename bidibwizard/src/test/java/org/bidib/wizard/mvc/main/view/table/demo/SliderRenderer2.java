package org.bidib.wizard.mvc.main.view.table.demo;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SliderRenderer2 extends JPanel implements TableCellRenderer {

    private static final Logger LOGGER = LoggerFactory.getLogger(SliderRenderer2.class);

    private JSlider slider;

    private JLabel label;

    public SliderRenderer2(int orientation, int min, int max, int value) {
        slider = new JSlider(orientation, min, max, value);
        label = new JLabel();

        setLayout(new BorderLayout());
        add(slider, BorderLayout.CENTER);

        add(label, BorderLayout.EAST);
    }

    public Component getTableCellRendererComponent(
        JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        if (isSelected) {
            setForeground(table.getSelectionForeground());
            setBackground(table.getSelectionBackground());
        }
        else {
            setForeground(table.getForeground());
            setBackground(table.getBackground());
        }

        TableColumnModel columnModel = table.getColumnModel();
        TableColumn selectedColumn = columnModel.getColumn(column);
        int columnWidth = selectedColumn.getWidth();
        int columnHeight = table.getRowHeight();
        setSize(new Dimension(columnWidth, columnHeight));

        LOGGER.info("Set the value: {}", value);

        slider.setValue(((Integer) value).intValue());

        if (value != null) {
            label.setText(Integer.toString(((Integer) value).intValue()));
        }
        // else {
        // label.setText(null);
        // }

        updateUI();
        return this;
    }

}
