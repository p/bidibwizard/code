package org.bidib.wizard.mvc.common.view.cvdefinition;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.bidib.jbidibc.core.enumeration.SysErrorEnum;
import org.bidib.jbidibc.core.node.ConfigurationVariable;
import org.bidib.jbidibc.exchange.vendorcv.CVType;
import org.bidib.jbidibc.exchange.vendorcv.DataType;
import org.bidib.jbidibc.exchange.vendorcv.DescriptionType;
import org.bidib.jbidibc.exchange.vendorcv.NodeType;
import org.bidib.jbidibc.exchange.vendorcv.NodetextType;
import org.bidib.jbidibc.exchange.vendorcv.VendorCVUtils;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.view.cvdef.CvNode;
import org.bidib.wizard.mvc.main.view.cvdef.LongCvNode;
import org.bidib.wizard.mvc.main.view.cvdef.LongNodeNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CvValueUtilsTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(CvValueUtilsTest.class);

    @Test
    public void writeCvValuesTest() {

        Node selectedNode = new Node(new org.bidib.jbidibc.core.Node(1, new byte[] { 0 }, 1L)) {

            private static final long serialVersionUID = 1L;

            private SysErrorEnum sysErrorTest;

            @Override
            public void setErrorState(SysErrorEnum sysError, byte[] reasonData) {
                this.sysErrorTest = sysError;
            }

            @Override
            public boolean isNodeHasRestartPendingError() {
                return SysErrorEnum.BIDIB_ERR_RESET_REQUIRED.equals(sysErrorTest);
            }
        };
        List<ConfigurationVariable> cvList = new LinkedList<ConfigurationVariable>();
        Map<String, CvNode> cvNumberToNodeMap = new LinkedHashMap<String, CvNode>();

        CVType cv = new CVType();
        cv.setNumber("100");
        cv.setType(DataType.BYTE);
        ConfigurationVariable configVar = new ConfigurationVariable("100", "10");
        CvNode cvNode = new CvNode(cv, configVar);

        cvList.add(configVar);
        cvNumberToNodeMap.put(configVar.getName(), cvNode);

        final CvDefintionPanelProvider cvDefintionPanelProvider = new CvDefintionPanelProvider() {

            @Override
            public void writeConfigVariables(List<ConfigurationVariable> cvList) {
            }

            @Override
            public void refreshDisplayedValues() {
            }

            @Override
            public void checkPendingChanges() {
            }
        };

        CvValueUtils.writeCvValues(selectedNode, cvList, cvNumberToNodeMap, cvDefintionPanelProvider);

        Assert.assertFalse(selectedNode.isNodeHasRestartPendingError());

        CVType cv2 = new CVType();
        cv2.setNumber("100");
        cv2.setType(DataType.BYTE);
        cv2.setRebootneeded(Boolean.TRUE);
        ConfigurationVariable configVar2 = new ConfigurationVariable("101", "11");
        CvNode cvNode2 = new CvNode(cv2, configVar2);

        cvList.add(configVar2);
        cvNumberToNodeMap.put(configVar2.getName(), cvNode2);

        CvValueUtils.writeCvValues(selectedNode, cvList, cvNumberToNodeMap, cvDefintionPanelProvider);

        Assert.assertTrue(selectedNode.isNodeHasRestartPendingError());
    }

    @Test
    public void prepareLongCvValuesTest() {

        List<ConfigurationVariable> cvList = new LinkedList<ConfigurationVariable>();
        Map<String, CvNode> cvNumberToNodeMap = new LinkedHashMap<String, CvNode>();

        CvNode cvNode = null;
        List<ConfigurationVariable> configVariables = new LinkedList<ConfigurationVariable>();
        String lang = "de";

        CVType cv = new CVType();
        cv.setNumber("100");
        cv.setType(DataType.LONG);
        DescriptionType dt0 = new DescriptionType();
        dt0.setText("Sample Text");
        dt0.setLang(lang);
        cv.getDescription().add(0, dt0);

        int offset = 100;
        int index = 0;
        int step = 1;
        int indexOffset = (index * step);
        String keyword = "sample";
        boolean skipOnTimeout = false;

        boolean firstCv = true;

        NodeType longNodeType = new NodeType();

        for (DescriptionType dt : cv.getDescription()) {

            NodetextType nodetext = new NodetextType();
            nodetext.setLang(dt.getLang());
            nodetext.setText(dt.getText());
            longNodeType.getNodetext().add(nodetext);
        }

        LongNodeNode longNode = new LongNodeNode(longNodeType, index, lang);

        // Create 4 CV values
        LongCvNode masterNodeLong = null;
        for (int indexLong = 0; indexLong < 4; indexLong++) {

            CVType cvClone = VendorCVUtils.prepareCVClone(cv, indexLong + indexOffset, offset, 1);

            ConfigurationVariable configVar = prepareConfigVar(cvClone);

            if (firstCv) {
                // set some extra info
                configVar.setSkipOnTimeout(skipOnTimeout);
                configVar.setMinCvNumber((index * step) + offset);
                configVar.setMaxCvNumber(((index + 1) * step) + offset - 1);
                firstCv = false;
            }
            // add to list of CVs of this node
            configVariables.add(configVar);

            LongCvNode newNode = new LongCvNode(cvClone, configVar);

            if (masterNodeLong == null) {
                masterNodeLong = (LongCvNode) newNode;

                // prepare the keyword for the master
                cvClone = VendorCVUtils.processKeyword(cvClone, keyword, index);
            }
            else {
                masterNodeLong.addSlaveNode(newNode);
                newNode.setMasterNode(masterNodeLong);
                // clear the keyword because we only want the keyword on the
                // master
                cvClone.setKeyword(null);
            }
            cvNode = newNode;

            // store new node in map
            cvNumberToNodeMap.put(cvClone.getNumber(), cvNode);
            LOGGER.trace("Add new CV node: {}", cvNode);

        }

        // set the master node as reference
        longNode.setSubCvMasterNode(masterNodeLong);

        String newValue = "123456";
        CvValueUtils.compareAndAddNewValue(masterNodeLong, newValue, cvList, cvNumberToNodeMap);

        LOGGER.info("cvList: {}", cvList);

        Assert.assertFalse(cvList.isEmpty());
        Assert.assertEquals(cvList.size(), 4);
    }

    @Test
    public void writeCvValuesWithInvalidValueTest() {

        Node selectedNode = new Node(new org.bidib.jbidibc.core.Node(1, new byte[] { 0 }, 1L)) {

            private static final long serialVersionUID = 1L;

            private SysErrorEnum sysErrorTest;

            @Override
            public void setErrorState(SysErrorEnum sysError, byte[] reasonData) {
                this.sysErrorTest = sysError;
            }

            @Override
            public boolean isNodeHasRestartPendingError() {
                return SysErrorEnum.BIDIB_ERR_RESET_REQUIRED.equals(sysErrorTest);
            }
        };
        List<ConfigurationVariable> cvList = new LinkedList<ConfigurationVariable>();
        Map<String, CvNode> cvNumberToNodeMap = new LinkedHashMap<String, CvNode>();

        CVType cv = new CVType();
        cv.setNumber("100");
        cv.setType(DataType.BYTE);
        ConfigurationVariable configVar = new ConfigurationVariable("100", "10");
        CvNode cvNode = new CvNode(cv, configVar);

        cvList.add(configVar);
        cvNumberToNodeMap.put(configVar.getName(), cvNode);

        CVType cv2 = new CVType();
        cv2.setNumber("101");
        cv2.setType(DataType.BYTE);
        ConfigurationVariable configVar2 = new ConfigurationVariable("101", null);
        CvNode cvNode2 = new CvNode(cv2, configVar2);

        cvList.add(configVar2);
        cvNumberToNodeMap.put(configVar2.getName(), cvNode2);

        final CvDefintionPanelProvider cvDefintionPanelProvider = new CvDefintionPanelProvider() {

            @Override
            public void writeConfigVariables(List<ConfigurationVariable> cvList) {
                LOGGER.info("Write CVs: {}", cvList);

                Assert.assertEquals(cvList.size(), 2);
            }

            @Override
            public void refreshDisplayedValues() {
            }

            @Override
            public void checkPendingChanges() {
            }
        };

        CvValueUtils.writeCvValues(selectedNode, cvList, cvNumberToNodeMap, cvDefintionPanelProvider);

        Assert.assertFalse(selectedNode.isNodeHasRestartPendingError());
    }

    private ConfigurationVariable prepareConfigVar(CVType cv) {
        ConfigurationVariable configVar = new ConfigurationVariable(cv.getNumber(), null /* "?" */);

        return configVar;
    }
}
