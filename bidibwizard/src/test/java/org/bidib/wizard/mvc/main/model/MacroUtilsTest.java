package org.bidib.wizard.mvc.main.model;

import java.util.LinkedList;
import java.util.List;

import org.bidib.wizard.mvc.main.model.function.DelayFunction.DelayFunctionBuilder;
import org.bidib.wizard.mvc.main.model.function.Function;
import org.testng.annotations.Test;

import junit.framework.Assert;

public class MacroUtilsTest {

    @Test
    public void hasEmptySteps() {
        Macro macro = new Macro(10);

        List<Function<?>> functions = new LinkedList<>();

        functions.add(null);
        functions.add(DelayFunctionBuilder.delayFunction().withDelay(10).build());
        functions.add(null);
        macro.setFunctions(functions);

        boolean hasEmptySteps = MacroUtils.hasEmptySteps(macro);
        Assert.assertTrue(hasEmptySteps);

        // clear functions
        functions.clear();

        functions.add(DelayFunctionBuilder.delayFunction().withDelay(10).build());
        functions.add(DelayFunctionBuilder.delayFunction().withDelay(20).build());
        macro.setFunctions(functions);

        hasEmptySteps = MacroUtils.hasEmptySteps(macro);
        Assert.assertFalse(hasEmptySteps);
    }

    @Test
    public void removeEmptySteps() {
        Macro macro = new Macro(10);

        List<Function<?>> functions = new LinkedList<>();

        functions.add(null);
        functions.add(DelayFunctionBuilder.delayFunction().withDelay(10).build());
        functions.add(null);
        macro.setFunctions(functions);

        boolean removedEmptySteps = MacroUtils.removeEmptySteps(macro);
        Assert.assertTrue(removedEmptySteps);

        // clear functions
        functions.clear();

        functions.add(DelayFunctionBuilder.delayFunction().withDelay(10).build());
        functions.add(DelayFunctionBuilder.delayFunction().withDelay(20).build());
        macro.setFunctions(functions);

        removedEmptySteps = MacroUtils.removeEmptySteps(macro);
        Assert.assertFalse(removedEmptySteps);
    }
}
