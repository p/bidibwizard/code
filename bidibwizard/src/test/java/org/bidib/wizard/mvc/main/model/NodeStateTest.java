package org.bidib.wizard.mvc.main.model;

import java.io.File;

import org.bidib.wizard.comm.LightPortStatus;
import org.bidib.wizard.mvc.main.model.function.Function;
import org.bidib.wizard.mvc.main.model.function.LightPortAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class NodeStateTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(NodeStateTest.class);

    @Test
    public void loadLC04() {
        String fileName = NodeStateTest.class.getResource("/macros/LC-04(links).node").getPath();
        NodeState nodeState = NodeState.load(fileName);

        Assert.assertNotNull(nodeState);

        LOGGER.info("Loaded nodeState: {}", nodeState);

        Assert.assertNotNull(nodeState.getMacros());
        Assert.assertEquals(nodeState.getMacros().size(), 64);

        Macro macro = nodeState.getMacros().iterator().next();
        Assert.assertNotNull(macro);

        Assert.assertEquals(macro.getLabel(), "W1-27-gerade");
        Assert.assertEquals(macro.getFunctionCount(), 0);
        Assert.assertEquals(macro.getFunctions().size(), 0);
    }

    @Test(enabled = false)
    public void save() {
        throw new RuntimeException("Test not implemented");
    }

    @Test
    public void loadVID56PID78901236() {
        String pathName = NodeStateTest.class.getResource("/macros/").getPath();
        File file = new File(pathName, "VID 56 PID 78901236.node");
        NodeState nodeState = NodeState.load(file.getPath());

        Assert.assertNotNull(nodeState);

        LOGGER.info("Loaded nodeState: {}", nodeState);

        Assert.assertNotNull(nodeState.getMacros());
        Assert.assertEquals(nodeState.getMacros().size(), 10);

        Macro macro = nodeState.getMacros().iterator().next();
        Assert.assertNotNull(macro);
        Assert.assertEquals(macro.getFunctions().size(), 4);
        LightPort lightPort = new LightPort();
        lightPort.setId(0);
        LightPortAction lightPortAction = new LightPortAction(LightPortStatus.ON, lightPort, 0);
        Function<?> function = macro.getFunctions().get(0);
        Assert.assertNotNull(function);
        LOGGER.info("First function: {}", function);
        Assert.assertTrue(function instanceof LightPortAction);

        LightPortAction testLightPortAction = (LightPortAction) function;
        Assert.assertEquals(testLightPortAction.getAction(), lightPortAction.getAction());
        Assert.assertEquals(testLightPortAction.getDelay(), lightPortAction.getDelay());
        Assert.assertEquals(testLightPortAction.getPort().getId(), lightPortAction.getPort().getId());
    }

    @Test
    public void loadOneControl() {
        String pathName = NodeStateTest.class.getResource("/macros/").getPath();
        File file = new File(pathName, "OneControl 1_20160319.node");
        NodeState nodeState = NodeState.load(file.getPath());

        Assert.assertNotNull(nodeState);

        LOGGER.info("Loaded nodeState: {}", nodeState);

        Assert.assertNotNull(nodeState.getMacros());
        Assert.assertEquals(nodeState.getMacros().size(), 32);
    }

    @Test
    public void loadOneControl2() {
        String pathName = NodeStateTest.class.getResource("/macros/").getPath();
        File file = new File(pathName, "OneControl 1.node");
        NodeState nodeState = NodeState.load(file.getPath());

        Assert.assertNotNull(nodeState);

        LOGGER.info("Loaded nodeState: {}", nodeState);

        Assert.assertNotNull(nodeState.getMacros());
        Assert.assertEquals(nodeState.getMacros().size(), 32);
    }

}
