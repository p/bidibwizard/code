package org.bidib.wizard.mvc.main.shaped;

/* -------------------
 * RingChartDemo1.java
 * -------------------
 * (C) Copyright 2005-2013, by Object Refinery Limited.
 *
 */

import java.awt.Color;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.entity.ChartEntity;
import org.jfree.chart.entity.EntityCollection;
import org.jfree.chart.entity.PieSectionEntity;
import org.jfree.chart.plot.RingPlot;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A simple demonstration application showing how to create a ring chart using data from a {@link DefaultPieDataset}.
 */
public class RingChartDemo extends ApplicationFrame {

    private static final long serialVersionUID = 1L;

    /**
     * Default constructor.
     * 
     * @param title
     *            the frame title.
     */
    public RingChartDemo(String title) {
        super(title);
        JPanel panel = createDemoPanel();
        panel.setPreferredSize(new java.awt.Dimension(500, 500));
        setContentPane(panel);
    }

    /**
     * Creates a sample dataset.
     * 
     * @return a sample dataset.
     */
    private static PieDataset createDataset() {
        DefaultPieDataset dataset = new DefaultPieDataset();

        for (int i = 0; i < 12; i++) {
            dataset.setValue("Aspect_" + i, new Integer(10));
        }
        // dataset.setValue("One", new Double(10.0));
        // dataset.setValue("Two", new Double(10.0));
        // dataset.setValue("Three", new Double(10.0));
        // dataset.setValue("Four", new Double(10.0));
        // dataset.setValue("Five", new Double(10.0));
        // dataset.setValue("Six", new Double(10.0));
        return dataset;
    }

    /**
     * Creates a chart.
     * 
     * @param dataset
     *            the dataset.
     * 
     * @return a chart.
     */
    private static JFreeChart createChart(PieDataset dataset) {

        JFreeChart chart = ChartFactory.createRingChart("Ring Chart Demo 1", // chart title
            dataset, // data
            false, // include legend
            true, false);

        RingPlot plot = (RingPlot) chart.getPlot();
        plot.setLabelFont(new Font("SansSerif", Font.PLAIN, 12));
        plot.setNoDataMessage("No data available");
        plot.setSectionDepth(0.35);
        plot.setCircular(true);
        plot.setLabelGap(0.02);
        // plot.setOutlineVisible(false);
        plot.setLabelGenerator(null);

        plot.setSectionPaint("Aspect_0", Color.MAGENTA);
        plot.setSectionPaint("Aspect_1", Color.BLUE);

        LOGGER.info("Shaddow offset, x: {}, y: {}", plot.getShadowXOffset(), plot.getShadowYOffset());
        plot.setShadowXOffset(0.0);
        plot.setShadowYOffset(0.0);

        return chart;

    }

    private static final Logger LOGGER = LoggerFactory.getLogger(RingChartDemo.class);

    /**
     * Creates a panel for the demo (used by SuperDemo.java).
     * 
     * @return A panel.
     */
    public static JPanel createDemoPanel() {
        JFreeChart chart = createChart(createDataset());
        ChartPanel panel = new ChartPanel(chart) {
            @Override
            public void mousePressed(MouseEvent event) {

                if (event.getButton() == MouseEvent.BUTTON1) {
                    Insets insets = getInsets();
                    int x = (int) ((event.getX() - insets.left) / getScaleX());
                    int y = (int) ((event.getY() - insets.top) / getScaleY());

                    if (getChart() == null) {
                        return;
                    }

                    ChartEntity entity = null;
                    if (getChartRenderingInfo() != null) {
                        EntityCollection entities = getChartRenderingInfo().getEntityCollection();
                        if (entities != null) {
                            entity = entities.getEntity(x, y);
                        }
                    }

                    if (entity != null && entity instanceof PieSectionEntity) {
                        LOGGER.info("Entity selected: {}", entity);
                    }
                    else {
                        super.mousePressed(event);
                    }
                }
            }
        };
        panel.setMouseWheelEnabled(true);
        return panel;
    }

    /**
     * Starting point for the demonstration application.
     * 
     * @param args
     *            ignored.
     */
    public static void main(String[] args) {
        RingChartDemo demo = new RingChartDemo("JFreeChart: RingChartDemo1.java");
        demo.pack();
        RefineryUtilities.centerFrameOnScreen(demo);
        demo.setVisible(true);
    }

}
