package org.bidib.wizard.mvc.script.view;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import org.bidib.wizard.highlight.BidibScriptScanner;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.script.ScriptCommand;
import org.bidib.wizard.script.node.types.LightPortType;
import org.bidib.wizard.script.node.types.MacroTargetType;
import org.bidib.wizard.script.node.types.ScriptingTargetType;
import org.bidib.wizard.script.node.types.TargetType;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ScriptParserTest {

    @Test
    public void parseSetTargetType() {
        List<ScriptCommand<NodeScripting>> scriptCommands = new LinkedList<>();

        org.bidib.jbidibc.core.Node coreNode = new org.bidib.jbidibc.core.Node(1, new byte[] { 0 }, 0x12345678);
        Node selectedNode = new Node(coreNode);

        // prepare the context
        HashMap<String, Object> context = new LinkedHashMap<>();
        context.put(ScriptParser.KEY_SELECTED_NODE, selectedNode);

        TargetType portType = new LightPortType();

        ScriptParser parser = new ScriptParser();
        BidibScriptScanner scanner = new BidibScriptScanner();
        int index = 0;

        int oldLen = -1;

        // String line = "set light 16 name=%AS-gn%";
        String line = "set light 16 name=%AS-gn%";

        context.put("%as-gn%", "SampleA");

        // TODO I don't know why the next 2 lines are is needed ...
        // scanner.change(0, 0, (oldLen > -1 ? oldLen : line.length()));
        // oldLen = line.trim().length();

        scanner.change(0, 0, line.length());
        scanner.scan(line.toCharArray(), 0, line.length());

        parser.parseSetTargetType(scanner, index, context, scriptCommands, portType);
    }

    @Test
    public void parseSetMacroTargetType() {
        List<ScriptCommand<NodeScripting>> scriptCommands = new LinkedList<>();

        org.bidib.jbidibc.core.Node coreNode = new org.bidib.jbidibc.core.Node(1, new byte[] { 0 }, 0x12345678);
        Node selectedNode = new Node(coreNode);

        // prepare the context
        HashMap<String, Object> context = new LinkedHashMap<>();
        context.put(ScriptParser.KEY_SELECTED_NODE, selectedNode);

        TargetType portType = new MacroTargetType();

        ScriptParser parser = new ScriptParser();
        BidibScriptScanner scanner = new BidibScriptScanner();
        int index = 0;

        String line = "set macro %%m0% name=\"Sig_\"&%this_accessory%&\"_hp0\" // dummy values";
        // String line2 = "set light 16 name=%AS-gn-oweweo%";

        context.put("%%m0%", "7");
        context.put("%this_accessory%", "2");

        scanner.change(0, 0, line.length());
        scanner.scan(line.toCharArray(), 0, line.length());

        parser.parseSetTargetType(scanner, index, context, scriptCommands, portType);

        Assert.assertEquals(portType.getPortNum(), Integer.valueOf(7));
        Assert.assertNotNull(portType.getScriptingTargetType());
        Assert.assertEquals(portType.getLabel(), "Sig_2_hp0");
    }

    @Test
    public void parseSetMacroTargetType2() {
        List<ScriptCommand<NodeScripting>> scriptCommands = new LinkedList<>();

        org.bidib.jbidibc.core.Node coreNode = new org.bidib.jbidibc.core.Node(1, new byte[] { 0 }, 0x12345678);
        Node selectedNode = new Node(coreNode);

        // prepare the context
        HashMap<String, Object> context = new LinkedHashMap<>();
        context.put(ScriptParser.KEY_SELECTED_NODE, selectedNode);

        TargetType portType = new MacroTargetType();

        ScriptParser parser = new ScriptParser();
        BidibScriptScanner scanner = new BidibScriptScanner();
        int index = 0;

        String line = "set macro %%m0% name=\"Sig_\"&%this_accessory%&\"_hp0\" action=stop // dummy values";
        // String line2 = "set light 16 name=%AS-gn-oweweo%";

        context.put("%%m0%", "7");
        context.put("%this_accessory%", "2");

        scanner.change(0, 0, line.length());
        scanner.scan(line.toCharArray(), 0, line.length());

        parser.parseSetTargetType(scanner, index, context, scriptCommands, portType);

        Assert.assertEquals(portType.getPortNum(), Integer.valueOf(7));
        Assert.assertNotNull(portType.getScriptingTargetType());
        Assert.assertEquals(portType.getLabel(), "Sig_2_hp0");
        Assert.assertEquals(portType.getScriptingTargetType(), ScriptingTargetType.MACRO);
    }

    // select macro %%m1%
    @Test
    public void parseSelectMacroTargetType() {
        List<ScriptCommand<NodeScripting>> scriptCommands = new LinkedList<>();

        org.bidib.jbidibc.core.Node coreNode = new org.bidib.jbidibc.core.Node(1, new byte[] { 0 }, 0x12345678);
        Node selectedNode = new Node(coreNode);

        // prepare the context
        HashMap<String, Object> context = new LinkedHashMap<>();
        context.put(ScriptParser.KEY_SELECTED_NODE, selectedNode);

        TargetType portType = new MacroTargetType();

        ScriptParser parser = new ScriptParser();
        BidibScriptScanner scanner = new BidibScriptScanner();
        int index = 0;

        String line = "select macro %%m1%";
        // String line2 = "set light 16 name=%AS-gn-oweweo%";

        context.put("%%m1%", "8");
        context.put("%this_accessory%", "2");

        scanner.change(0, 0, line.length());
        scanner.scan(line.toCharArray(), 0, line.length());

        parser.parseSetTargetType(scanner, index, context, scriptCommands, portType);

        Assert.assertEquals(portType.getPortNum(), Integer.valueOf(8));
    }
}
