package org.bidib.wizard.mvc.postprocess.dccdevices;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.UnmarshallerHandler;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.apache.commons.io.IOUtils;
import org.bidib.wizard.dccdevices.DecoderType;
import org.bidib.wizard.dccdevices.UserDevice;
import org.codehaus.stax2.XMLStreamWriter2;
import org.custommonkey.xmlunit.XMLAssert;
import org.custommonkey.xmlunit.XMLUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLFilter;
import org.xml.sax.XMLReader;

public class DecoderTypeTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(DecoderTypeTest.class);

    private static final String JAXB_PACKAGE = "org.bidib.wizard.dccdevices";

    public static final String XSD_LOCATION = "/xsd/dcc-devices.xsd";

    @Test
    public void writeDccDevices() throws JAXBException, SAXException, IOException, ParserConfigurationException,
        XMLStreamException {

        JAXBContext jaxbContext = JAXBContext.newInstance(JAXB_PACKAGE);

        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        StreamSource streamSource = new StreamSource(DecoderTypeTest.class.getResourceAsStream(XSD_LOCATION));
        Schema schema = schemaFactory.newSchema(streamSource);
        marshaller.setSchema(schema);

        UserDevice userDevice = new UserDevice();

        DecoderType decoder = new DecoderType();
        decoder.setValue("Test Loco");
        decoder.setSpeedsteps(128);
        decoder.setPomAddress(33);
        decoder.setDecoderType("Loco");
        userDevice.getDecoder().add(decoder);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        final XMLStreamWriter2 xmlStreamWriter =
            (XMLStreamWriter2) XMLOutputFactory.newInstance().createXMLStreamWriter(baos, "utf-8");
        xmlStreamWriter.setDefaultNamespace("http://www.bidib.org/wizard/dccdevices");

        final XMLStreamWriter xmlStreamWriter2 = new IndentingXMLStreamWriter(xmlStreamWriter);

        marshaller.marshal(userDevice, xmlStreamWriter2);

        LOGGER.info("marshalled userDevice: {}", baos);

        String d = baos.toString("UTF-8");

        Document testDoc = XMLUnit.buildControlDocument(d);

        InputStream is = DecoderTypeTest.class.getResourceAsStream("/dccdevices/DccDevices-test.xml");
        final String xmlContent = IOUtils.toString(is, "UTF-8");
        Document controlDoc = XMLUnit.buildControlDocument(xmlContent);

        XMLAssert.assertXMLEqual(controlDoc, testDoc);
    }

    @Test
    public void readDccDevices() throws JAXBException, SAXException, ParserConfigurationException, IOException {

        JAXBContext jaxbContext = JAXBContext.newInstance(JAXB_PACKAGE);

        // Create the XMLFilter
        XMLFilter filter = new NamespaceFilter();

        // Set the parent XMLReader on the XMLFilter
        SAXParserFactory spf = SAXParserFactory.newInstance();
        SAXParser sp = spf.newSAXParser();
        XMLReader xr = sp.getXMLReader();
        filter.setParent(xr);

        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

        // Set UnmarshallerHandler as ContentHandler on XMLFilter
        UnmarshallerHandler unmarshallerHandler = unmarshaller.getUnmarshallerHandler();
        filter.setContentHandler(unmarshallerHandler);

        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        StreamSource streamSource = new StreamSource(DecoderTypeTest.class.getResourceAsStream(XSD_LOCATION));
        Schema schema = schemaFactory.newSchema(streamSource);
        unmarshaller.setSchema(schema);

        InputStream is = DecoderTypeTest.class.getResourceAsStream("/dccdevices/DccDevices.bidib");
        InputSource xml = new InputSource(is);

        filter.parse(xml);
        UserDevice userDevice = (UserDevice) unmarshallerHandler.getResult();

        LOGGER.info("unmarshalled userDevice: {}", userDevice);
        Assert.assertNotNull(userDevice);
        Assert.assertNotNull(userDevice.getDecoder());

        Assert.assertEquals(userDevice.getDecoder().size(), 2);
        DecoderType decoder0 = userDevice.getDecoder().get(0);
        Assert.assertNotNull(decoder0);
        Assert.assertEquals(decoder0.getValue(), "Ee 3/3");
        Assert.assertEquals(decoder0.getPomAddress(), Integer.valueOf(33));

        DecoderType decoder1 = userDevice.getDecoder().get(1);
        Assert.assertNotNull(decoder1);
        Assert.assertEquals(decoder1.getValue(), "Testdecoder");
        Assert.assertEquals(decoder1.getPomAddress(), Integer.valueOf(3));
    }
}
