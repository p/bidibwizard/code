package org.bidib.wizard.mvc.script.view.utils;

import org.bidib.jbidibc.core.Node;
import org.testng.Assert;
import org.testng.annotations.Test;

public class NodeRequirementTest {

    @Test
    public void matches() {
        long uniqueId = 0x05000d9F001234L;
        NodeRequirement requirement = new NodeRequirement("13", "159");
        Node node = new Node(1, new byte[] { 0 }, uniqueId);

        Assert.assertTrue(requirement.matches(node));
    }

    @Test
    public void matchesNot() {
        long uniqueId = 0x05000d9F001234L;
        NodeRequirement requirement = new NodeRequirement("13", "125");
        Node node = new Node(1, new byte[] { 0 }, uniqueId);

        Assert.assertFalse(requirement.matches(node));
    }

    @Test
    public void matchesMultiplePid() {
        long uniqueId = 0x05000d9F001234L;
        NodeRequirement requirement = new NodeRequirement("13", "159,160,123");
        Node node = new Node(1, new byte[] { 0 }, uniqueId);

        Assert.assertTrue(requirement.matches(node));
    }

    @Test
    public void matchesNotMultiplePid() {
        long uniqueId = 0x05000d9F001234L;
        NodeRequirement requirement = new NodeRequirement("13", "125,123,144");
        Node node = new Node(1, new byte[] { 0 }, uniqueId);

        Assert.assertFalse(requirement.matches(node));
    }

    @Test
    public void matchesMultiplePidWithSpaces() {
        long uniqueId = 0x05000d9F001234L;
        NodeRequirement requirement = new NodeRequirement("13", "159, 160, 123");
        Node node = new Node(1, new byte[] { 0 }, uniqueId);

        Assert.assertTrue(requirement.matches(node));
    }

    @Test
    public void matchesMultipleVidWithSpaces() {
        long uniqueId = 0x05000d9F001234L;
        NodeRequirement requirement = new NodeRequirement("13, 12", "159, 160, 123");
        Node node = new Node(1, new byte[] { 0 }, uniqueId);

        Assert.assertTrue(requirement.matches(node));
    }

    @Test
    public void matchesNotMultiplePidWithSpaces() {
        long uniqueId = 0x05000d9F001234L;
        NodeRequirement requirement = new NodeRequirement("13", "125, 123, 144");
        Node node = new Node(1, new byte[] { 0 }, uniqueId);

        Assert.assertFalse(requirement.matches(node));
    }

    @Test
    public void matchesNotMultipleVidWithSpaces() {
        long uniqueId = 0x05000d9F001234L;
        NodeRequirement requirement = new NodeRequirement("12, 15", "125, 123, 144");
        Node node = new Node(1, new byte[] { 0 }, uniqueId);

        Assert.assertFalse(requirement.matches(node));
    }

    @Test
    public void matchesEmptyVid() {
        long uniqueId = 0x05000d9F001234L;
        NodeRequirement requirement = new NodeRequirement("", "");
        Node node = new Node(1, new byte[] { 0 }, uniqueId);

        Assert.assertFalse(requirement.matches(node));
    }

    @Test
    public void matchesEmptyPid() {
        long uniqueId = 0x05000d9F001234L;
        NodeRequirement requirement = new NodeRequirement("13", "");
        Node node = new Node(1, new byte[] { 0 }, uniqueId);

        Assert.assertFalse(requirement.matches(node));
    }

}
