package org.bidib.wizard.mvc.main.model.function;

import org.bidib.jbidibc.exchange.lcmacro.AnalogActionType;
import org.bidib.jbidibc.exchange.lcmacro.AnalogPortPoint;
import org.bidib.jbidibc.exchange.lcmacro.LcMacroPointType;
import org.bidib.wizard.comm.AnalogPortStatus;
import org.bidib.wizard.mvc.main.model.AnalogPort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class AnalogPortActionTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(AnalogPortActionTest.class);

    @Test
    public void toLcMacroPoint() {
        AnalogPort analogPort = new AnalogPort();
        analogPort.setId(1);
        AnalogPortAction action = new AnalogPortAction(AnalogPortStatus.START, analogPort, 90, 20);
        action.setAction(AnalogPortStatus.START);

        LcMacroPointType lcMacroPoint = action.toLcMacroPoint();
        Assert.assertNotNull(lcMacroPoint);

        LOGGER.info("Current lcMacroPoint: {}", lcMacroPoint);
        Assert.assertTrue(lcMacroPoint instanceof AnalogPortPoint);
        AnalogPortPoint analogPortPoint = (AnalogPortPoint) lcMacroPoint;

        Assert.assertEquals(analogPortPoint.getOutputNumber(), 1);
        Assert.assertNotNull(analogPortPoint.getDelay());
        Assert.assertEquals(analogPortPoint.getDelay(), Integer.valueOf(90));
        Assert.assertNotNull(analogPortPoint.getAnalogPortActionType());
        Assert.assertEquals(analogPortPoint.getAnalogPortActionType().getAction(), AnalogActionType.START);
        Assert.assertEquals(analogPortPoint.getAnalogPortActionType().getValue(), 20);
    }
}
