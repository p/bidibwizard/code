package org.bidib.wizard.mvc.main.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.bidib.wizard.comm.AnalogPortStatus;
import org.bidib.wizard.comm.FlagStatus;
import org.bidib.wizard.comm.InputStatus;
import org.bidib.wizard.comm.MotorPortStatus;
import org.bidib.wizard.comm.SoundPortStatus;
import org.bidib.wizard.comm.SwitchPortStatus;
import org.bidib.wizard.mvc.main.model.MacroFactory.ExportFormat;
import org.bidib.wizard.mvc.main.model.function.AnalogPortAction;
import org.bidib.wizard.mvc.main.model.function.FlagFunction;
import org.bidib.wizard.mvc.main.model.function.Function;
import org.bidib.wizard.mvc.main.model.function.InputFunction;
import org.bidib.wizard.mvc.main.model.function.LightPortAction;
import org.bidib.wizard.mvc.main.model.function.MacroFunction;
import org.bidib.wizard.mvc.main.model.function.MotorPortAction;
import org.bidib.wizard.mvc.main.model.function.ServoPortAction;
import org.bidib.wizard.mvc.main.model.function.SoundPortAction;
import org.bidib.wizard.mvc.main.model.function.SwitchPortAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class MacroFactoryTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(MacroFactoryTest.class);

    private static final String EXPORTED_MACRO_TARGET_DIR = "target/exported";

    private PortsProvider portsProvider;

    @BeforeTest
    public void prepare() {
        LOGGER.info("Create report directory: {}", EXPORTED_MACRO_TARGET_DIR);

        try {
            FileUtils.forceMkdir(new File(EXPORTED_MACRO_TARGET_DIR));
        }
        catch (IOException e) {
            LOGGER.warn("Create report directory failed: " + EXPORTED_MACRO_TARGET_DIR, e);
        }

        portsProvider = new PortsProvider() {

            private List<SwitchPort> switchPorts;

            private List<SwitchPairPort> switchPairPorts;

            private List<SoundPort> soundPorts;

            private List<ServoPort> servoPorts;

            private List<MotorPort> motorPorts;

            private List<LightPort> lightPorts;

            private List<BacklightPort> backlightPorts;

            private List<InputPort> inputPorts;

            private List<Flag> flags;

            private List<FeedbackPort> feedbackPorts;

            private List<AnalogPort> analogPorts;

            private List<Macro> macros;

            private List<Accessory> accessories;

            @Override
            public List<SwitchPort> getSwitchPorts() {
                return getEnabledSwitchPorts();
            }

            @Override
            public List<SwitchPort> getEnabledSwitchPorts() {
                if (switchPorts == null) {
                    switchPorts = new ArrayList<>();
                    for (int portNumber = 0; portNumber < 4; portNumber++) {
                        SwitchPort switchPort = new SwitchPort();
                        switchPort.setId(portNumber);
                        switchPort.setLabel("SwitchPort" + portNumber);
                        switchPorts.add(switchPort);
                    }
                }
                return switchPorts;
            }

            @Override
            public List<SwitchPairPort> getSwitchPairPorts() {
                return getEnabledSwitchPairPorts();
            }

            @Override
            public List<SwitchPairPort> getEnabledSwitchPairPorts() {
                if (switchPairPorts == null) {
                    switchPairPorts = new ArrayList<>();
                    for (int portNumber = 0; portNumber < 4; portNumber++) {
                        SwitchPairPort switchPairPort = new SwitchPairPort();
                        switchPairPort.setId(portNumber);
                        switchPairPort.setLabel("SwitchPairPort" + portNumber);
                        switchPairPorts.add(switchPairPort);
                    }
                }
                return switchPairPorts;
            }

            @Override
            public List<SoundPort> getSoundPorts() {
                if (soundPorts == null) {
                    soundPorts = new ArrayList<SoundPort>();
                    for (int portNumber = 0; portNumber < 4; portNumber++) {
                        SoundPort soundPort = new SoundPort();
                        soundPort.setId(portNumber);
                        soundPort.setLabel("SoundPort" + portNumber);
                        soundPorts.add(soundPort);
                    }
                }
                return soundPorts;
            }

            @Override
            public List<ServoPort> getServoPorts() {
                if (servoPorts == null) {
                    servoPorts = new ArrayList<ServoPort>();
                    for (int portNumber = 0; portNumber < 4; portNumber++) {
                        ServoPort servoPort = new ServoPort();
                        servoPort.setId(portNumber);
                        servoPort.setLabel("ServoPort" + portNumber);
                        servoPorts.add(servoPort);
                    }
                }
                return servoPorts;
            }

            @Override
            public List<MotorPort> getMotorPorts() {
                if (motorPorts == null) {
                    motorPorts = new ArrayList<MotorPort>();
                    for (int portNumber = 0; portNumber < 4; portNumber++) {
                        MotorPort motorPort = new MotorPort();
                        motorPort.setId(portNumber);
                        motorPort.setLabel("MotorPort" + portNumber);
                        motorPorts.add(motorPort);
                    }
                }
                return motorPorts;
            }

            @Override
            public List<LightPort> getLightPorts() {
                if (lightPorts == null) {
                    lightPorts = new ArrayList<LightPort>();
                    for (int portNumber = 0; portNumber < 12; portNumber++) {
                        LightPort lightPort = new LightPort();
                        lightPort.setId(portNumber);
                        lightPort.setLabel("LightPort" + portNumber);
                        lightPorts.add(lightPort);
                    }
                }
                return lightPorts;
            }

            @Override
            public List<BacklightPort> getBacklightPorts() {
                if (backlightPorts == null) {
                    backlightPorts = new ArrayList<BacklightPort>();
                    for (int portNumber = 0; portNumber < 12; portNumber++) {
                        BacklightPort lightPort = new BacklightPort();
                        lightPort.setId(portNumber);
                        lightPort.setLabel("BacklightPort" + portNumber);
                        backlightPorts.add(lightPort);
                    }
                }
                return backlightPorts;
            }

            @Override
            public List<InputPort> getInputPorts() {
                return getEnabledInputPorts();
            }

            @Override
            public List<InputPort> getEnabledInputPorts() {
                if (inputPorts == null) {
                    inputPorts = new ArrayList<InputPort>();
                    for (int portNumber = 0; portNumber < 12; portNumber++) {
                        InputPort inputPort = new InputPort();
                        inputPort.setId(portNumber);
                        inputPort.setLabel("InputPort" + portNumber);
                        inputPorts.add(inputPort);
                    }
                }
                return inputPorts;
            }

            @Override
            public List<Flag> getFlags() {
                if (flags == null) {
                    flags = new ArrayList<Flag>();
                    for (int flagNumber = 0; flagNumber < 8; flagNumber++) {
                        Flag flag = new Flag();
                        flag.setId(flagNumber);
                        flags.add(flag);
                    }
                }
                return flags;
            }

            @Override
            public List<FeedbackPort> getFeedbackPorts() {
                if (feedbackPorts == null) {
                    feedbackPorts = new ArrayList<FeedbackPort>();
                    for (int portNumber = 0; portNumber < 48; portNumber++) {
                        FeedbackPort feedbackPort = new FeedbackPort();
                        feedbackPort.setId(portNumber);
                        feedbackPort.setLabel("FeedbackPort" + portNumber);
                        feedbackPorts.add(feedbackPort);
                    }
                }
                return feedbackPorts;
            }

            @Override
            public List<AnalogPort> getAnalogPorts() {
                if (analogPorts == null) {
                    analogPorts = new ArrayList<AnalogPort>();
                    for (int portNumber = 0; portNumber < 12; portNumber++) {
                        AnalogPort analogPort = new AnalogPort();
                        analogPort.setId(portNumber);
                        analogPort.setLabel("AnalogPort" + portNumber);
                        analogPorts.add(analogPort);
                    }
                }
                return analogPorts;
            }

            @Override
            public List<Macro> getMacros() {
                if (macros == null) {
                    macros = new ArrayList<Macro>();
                    for (int macroNumber = 0; macroNumber < 8; macroNumber++) {
                        Macro macro = new Macro(38 /* max macro points */);
                        macro.setId(macroNumber);
                        macros.add(macro);
                    }
                }
                return macros;
            }

            @Override
            public boolean isFlatPortModel() {
                return false;
            }

            @Override
            public List<Accessory> getAccessories() {
                if (accessories == null) {
                    accessories = new ArrayList<Accessory>();
                    for (int accessoryNumber = 0; accessoryNumber < 8; accessoryNumber++) {
                        Accessory accessory = new Accessory();
                        accessory.setId(accessoryNumber);
                        accessories.add(accessory);
                    }
                }
                return accessories;
            }

        };
    }

    @Test
    public void loadMacroWithXmlDecoder() {
        String fileName = MacroFactoryTest.class.getResource("/macros/Macro_0.macro").getPath();
        Macro macro = MacroFactory.loadMacro(fileName, ExportFormat.serialization, portsProvider);

        Assert.assertNotNull(macro);

        LOGGER.info("Loaded macro: {}", macro);

        Assert.assertEquals(macro.getId(), 0);
        Assert.assertEquals(macro.getLabel(), "Macro_0");
        Assert.assertEquals(macro.getFunctions().size(), 4);

        Assert.assertTrue(macro.getFunctions().get(0) instanceof LightPortAction);
        Assert.assertTrue(macro.getFunctions().get(1) instanceof MacroFunction);
        Assert.assertTrue(macro.getFunctions().get(2) instanceof AnalogPortAction);
        Assert.assertTrue(macro.getFunctions().get(3) instanceof ServoPortAction);
    }

    @Test
    public void loadMacroWithJaxb() {
        String fileName = MacroFactoryTest.class.getResource("/macros/Macro_0.mxml").getPath();
        Macro macro = MacroFactory.loadMacro(fileName, ExportFormat.jaxb, portsProvider);

        Assert.assertNotNull(macro);

        LOGGER.info("Loaded macro: {}", macro);

        Assert.assertEquals(macro.getId(), 0);
        Assert.assertEquals(macro.getLabel(), "Macro_0");
        Assert.assertEquals(macro.getFunctions().size(), 15);

        Assert.assertTrue(macro.getFunctions().get(0) instanceof LightPortAction);
        Assert.assertTrue(macro.getFunctions().get(1) instanceof ServoPortAction);
        Assert.assertTrue(macro.getFunctions().get(2) instanceof LightPortAction);
        Assert.assertTrue(macro.getFunctions().get(3) instanceof ServoPortAction);
        Assert.assertTrue(macro.getFunctions().get(4) instanceof LightPortAction);
        Assert.assertTrue(macro.getFunctions().get(5) instanceof LightPortAction);
        Assert.assertTrue(macro.getFunctions().get(6) instanceof SwitchPortAction);
        Assert.assertTrue(macro.getFunctions().get(7) instanceof SoundPortAction);
        Assert.assertTrue(macro.getFunctions().get(8) instanceof MotorPortAction);
        Assert.assertTrue(macro.getFunctions().get(9) instanceof InputFunction);
        Assert.assertTrue(macro.getFunctions().get(10) instanceof InputFunction);
        Assert.assertTrue(macro.getFunctions().get(11) instanceof FlagFunction);
        Assert.assertTrue(macro.getFunctions().get(12) instanceof FlagFunction);
        Assert.assertTrue(macro.getFunctions().get(13) instanceof FlagFunction);

        FlagFunction flagFunction1 = (FlagFunction) macro.getFunctions().get(11);
        Assert.assertEquals(flagFunction1.getAction(), FlagStatus.SET);

        FlagFunction flagFunction2 = (FlagFunction) macro.getFunctions().get(12);
        Assert.assertEquals(flagFunction2.getAction(), FlagStatus.CLEAR);

        FlagFunction flagFunction3 = (FlagFunction) macro.getFunctions().get(13);
        Assert.assertEquals(flagFunction3.getAction(), FlagStatus.QUERY_1);

        FlagFunction flagFunction4 = (FlagFunction) macro.getFunctions().get(14);
        Assert.assertEquals(flagFunction4.getAction(), FlagStatus.QUERY_0);
    }

    @Test
    public void loadMacroWithJaxbWithInvalidPortNumber() {
        String fileName = MacroFactoryTest.class.getResource("/macros/Macro_0.mxml").getPath();
        LightPort removedLightPort = null;
        Macro macro = null;
        try {
            // remove light port 0 to have the exception thrown
            removedLightPort = portsProvider.getLightPorts().remove(0);
            macro = MacroFactory.loadMacro(fileName, ExportFormat.jaxb, portsProvider);
        }
        finally {
            portsProvider.getLightPorts().add(removedLightPort);
        }

        Assert.assertNotNull(macro);
        Assert.assertNotNull(macro.getFunctions());
        for (Function<?> function : macro.getFunctions()) {
            if (function instanceof LightPortAction) {
                LightPortAction lightPortAction = (LightPortAction) function;
                if (lightPortAction.getPort().getId() == removedLightPort.getId()) {
                    // should have been replaced by EmptyFunction
                    Assert.fail("Found a lightport with the id of the removed lightport!");
                }
            }
        }
    }

    @Test
    public void loadMacroWithJaxbAnalogPorts() {
        String fileName = MacroFactoryTest.class.getResource("/macros/Makro_AnalogPorts.mxml").getPath();
        Macro macro = MacroFactory.loadMacro(fileName, ExportFormat.jaxb, portsProvider);

        Assert.assertNotNull(macro);

        LOGGER.info("Loaded macro: {}", macro);

        Assert.assertEquals(macro.getId(), 3);
        Assert.assertEquals(macro.getLabel(), "Makro_3");
        Assert.assertEquals(macro.getFunctions().size(), 2);

        Assert.assertTrue(macro.getFunctions().get(0) instanceof AnalogPortAction);
        AnalogPortAction analogPortAction = (AnalogPortAction) macro.getFunctions().get(0);
        Assert.assertNotNull(analogPortAction.getPort());
        Assert.assertEquals(analogPortAction.getPort().getId(), 0);
        Assert.assertEquals(analogPortAction.getAction(), AnalogPortStatus.START);
        Assert.assertEquals(analogPortAction.getValue(), 23);
        Assert.assertEquals(analogPortAction.getDelay(), 20);

        Assert.assertTrue(macro.getFunctions().get(1) instanceof AnalogPortAction);
        analogPortAction = (AnalogPortAction) macro.getFunctions().get(1);
        Assert.assertNotNull(analogPortAction.getPort());
        Assert.assertEquals(analogPortAction.getPort().getId(), 10);
        Assert.assertEquals(analogPortAction.getAction(), AnalogPortStatus.START);
        Assert.assertEquals(analogPortAction.getValue(), 12);
        Assert.assertEquals(analogPortAction.getDelay(), 30);
    }

    @Test
    public void saveMacroWithJaxbAndSerialization() throws FileNotFoundException, IOException {

        String fileName = MacroFactoryTest.class.getResource("/macros/Macro_0.macro").getPath();
        Macro macro = MacroFactory.loadMacro(fileName, ExportFormat.serialization, portsProvider);

        Assert.assertNotNull(macro);

        LOGGER.info("Loaded macro: {}", macro);

        Assert.assertEquals(macro.getId(), 0);
        Assert.assertEquals(macro.getLabel(), "Macro_0");
        Assert.assertEquals(macro.getFunctions().size(), 4);

        Assert.assertTrue(macro.getFunctions().get(0) instanceof LightPortAction);
        Assert.assertTrue(macro.getFunctions().get(1) instanceof MacroFunction);
        Assert.assertTrue(macro.getFunctions().get(2) instanceof AnalogPortAction);
        Assert.assertTrue(macro.getFunctions().get(3) instanceof ServoPortAction);

        File exportFile = new File(EXPORTED_MACRO_TARGET_DIR, "macro_0.xml");
        MacroFactory.saveMacro(exportFile.getPath(), macro, ExportFormat.jaxb, false);

        exportFile = new File(EXPORTED_MACRO_TARGET_DIR, "macro_0-serial.xml");
        MacroFactory.saveMacro(exportFile.getPath(), macro, ExportFormat.serialization, false);

    }

    @Test
    public void saveMacroWithJaxb() throws FileNotFoundException, IOException {

        String fileName = MacroFactoryTest.class.getResource("/macros/Macro_0.macro").getPath();
        Macro macro = MacroFactory.loadMacro(fileName, ExportFormat.serialization, portsProvider);

        Assert.assertNotNull(macro);

        LOGGER.info("Loaded macro: {}", macro);

        Assert.assertEquals(macro.getId(), 0);
        Assert.assertEquals(macro.getLabel(), "Macro_0");
        Assert.assertEquals(macro.getFunctions().size(), 4);

        Assert.assertTrue(macro.getFunctions().get(0) instanceof LightPortAction);
        Assert.assertTrue(macro.getFunctions().get(1) instanceof MacroFunction);
        Assert.assertTrue(macro.getFunctions().get(2) instanceof AnalogPortAction);
        Assert.assertTrue(macro.getFunctions().get(3) instanceof ServoPortAction);

        List<Function<?>> newFunctions = new ArrayList<Function<?>>(macro.getFunctions());

        // add more ports
        SwitchPort switchPort = new SwitchPort();
        switchPort.setId(2);
        switchPort.setLabel("SwitchPort2");
        SwitchPortAction switchPortAction = new SwitchPortAction(SwitchPortStatus.ON, switchPort, 30);
        newFunctions.add(switchPortAction);

        SoundPort soundPort = new SoundPort();
        soundPort.setId(2);
        soundPort.setLabel("SoundPort2");
        SoundPortAction soundPortAction = new SoundPortAction(SoundPortStatus.PLAY, soundPort, 25, 10);
        newFunctions.add(soundPortAction);

        MotorPort motorPort = new MotorPort();
        motorPort.setId(2);
        motorPort.setLabel("MotorPort2");
        MotorPortAction motorPortAction = new MotorPortAction(MotorPortStatus.FORWARD, motorPort, 25, 10);
        newFunctions.add(motorPortAction);

        InputPort inputPort2 = new InputPort();
        inputPort2.setId(2);
        inputPort2.setLabel("InputPort2");
        InputFunction inputFunction0 = new InputFunction(InputStatus.QUERY0, inputPort2);
        newFunctions.add(inputFunction0);

        InputPort inputPort3 = new InputPort();
        inputPort3.setId(3);
        inputPort3.setLabel("InputPort3");
        InputFunction inputFunction1 = new InputFunction(InputStatus.QUERY1, inputPort3);
        newFunctions.add(inputFunction1);

        Flag flag1 = new Flag(1);
        FlagFunction flagFunction1 = new FlagFunction(FlagStatus.SET, flag1);
        newFunctions.add(flagFunction1);

        Flag flag2 = new Flag(2);
        FlagFunction flagFunction2 = new FlagFunction(FlagStatus.CLEAR, flag2);
        newFunctions.add(flagFunction2);

        Flag flag3 = new Flag(3);
        FlagFunction flagFunction3 = new FlagFunction(FlagStatus.QUERY_1, flag3);
        newFunctions.add(flagFunction3);

        Flag flag4 = new Flag(4);
        FlagFunction flagFunction4 = new FlagFunction(FlagStatus.QUERY_0, flag4);
        newFunctions.add(flagFunction4);

        macro.setFunctions(newFunctions);

        File exportFile = new File(EXPORTED_MACRO_TARGET_DIR, "macro_2.xml");
        MacroFactory.saveMacro(exportFile.getPath(), macro, ExportFormat.jaxb, false);
    }

    @Test(enabled = false)
    public void saveMacroWithXmlEncoder() {
        throw new RuntimeException("Test not implemented");
    }
}
