package org.bidib.wizard.mvc.main.model.function;

import org.bidib.jbidibc.exchange.lcmacro.AccessoryOkayActionType;
import org.bidib.jbidibc.exchange.lcmacro.AccessoryOkayPoint;
import org.bidib.jbidibc.exchange.lcmacro.LcMacroPointType;
import org.bidib.wizard.comm.AccessoryOkayStatus;
import org.bidib.wizard.mvc.main.model.InputPort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class AccessoryOkayFunctionTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(AccessoryOkayFunctionTest.class);

    @Test
    public void toLcMacroPointNoFeedback() {
        InputPort inputPort = new InputPort();
        inputPort.setId(1);
        AccessoryOkayFunction action = new AccessoryOkayFunction(AccessoryOkayStatus.NO_FEEDBACK, inputPort);

        LcMacroPointType lcMacroPoint = action.toLcMacroPoint();
        Assert.assertNotNull(lcMacroPoint);

        LOGGER.info("Current lcMacroPoint: {}", lcMacroPoint);
        Assert.assertTrue(lcMacroPoint instanceof AccessoryOkayPoint);
        AccessoryOkayPoint accessoryOkayPoint = (AccessoryOkayPoint) lcMacroPoint;

        Assert.assertNull(accessoryOkayPoint.getInputNumber());
        Assert.assertEquals(accessoryOkayPoint.getAccessoryOkayActionType(), AccessoryOkayActionType.NO_FEEDBACK);
    }

    @Test
    public void toLcMacroPointQuery0() {
        InputPort inputPort = new InputPort();
        inputPort.setId(12);
        AccessoryOkayFunction action = new AccessoryOkayFunction(AccessoryOkayStatus.QUERY0, inputPort);

        LcMacroPointType lcMacroPoint = action.toLcMacroPoint();
        Assert.assertNotNull(lcMacroPoint);

        LOGGER.info("Current lcMacroPoint: {}", lcMacroPoint);
        Assert.assertTrue(lcMacroPoint instanceof AccessoryOkayPoint);
        AccessoryOkayPoint accessoryOkayPoint = (AccessoryOkayPoint) lcMacroPoint;

        Assert.assertNotNull(accessoryOkayPoint.getInputNumber());
        Assert.assertEquals(accessoryOkayPoint.getInputNumber(), Integer.valueOf(12));
        Assert.assertEquals(accessoryOkayPoint.getAccessoryOkayActionType(), AccessoryOkayActionType.QUERY_0);
    }

    @Test
    public void toLcMacroPointQuery1() {
        InputPort inputPort = new InputPort();
        inputPort.setId(3);
        AccessoryOkayFunction action = new AccessoryOkayFunction(AccessoryOkayStatus.QUERY1, inputPort);

        LcMacroPointType lcMacroPoint = action.toLcMacroPoint();
        Assert.assertNotNull(lcMacroPoint);

        LOGGER.info("Current lcMacroPoint: {}", lcMacroPoint);
        Assert.assertTrue(lcMacroPoint instanceof AccessoryOkayPoint);
        AccessoryOkayPoint accessoryOkayPoint = (AccessoryOkayPoint) lcMacroPoint;

        Assert.assertNotNull(accessoryOkayPoint.getInputNumber());
        Assert.assertEquals(accessoryOkayPoint.getInputNumber(), Integer.valueOf(3));
        Assert.assertEquals(accessoryOkayPoint.getAccessoryOkayActionType(), AccessoryOkayActionType.QUERY_1);
    }
}
