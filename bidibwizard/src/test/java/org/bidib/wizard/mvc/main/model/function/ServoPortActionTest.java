package org.bidib.wizard.mvc.main.model.function;

import org.bidib.jbidibc.exchange.lcmacro.LcMacroPointType;
import org.bidib.jbidibc.exchange.lcmacro.ServoActionType;
import org.bidib.jbidibc.exchange.lcmacro.ServoPortPoint;
import org.bidib.wizard.comm.ServoPortStatus;
import org.bidib.wizard.mvc.main.model.ServoPort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ServoPortActionTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(ServoPortActionTest.class);

    @Test
    public void toLcMacroPoint() {
        ServoPort servoPort = new ServoPort();
        servoPort.setId(1);
        servoPort.setSpeed(4); // not included in macro step because config only
        servoPort.setTrimDown(12); // not included in macro step because config only
        servoPort.setTrimUp(89); // not included in macro step because config only
        ServoPortAction action = new ServoPortAction(ServoPortStatus.START, servoPort, 90, 20);

        LcMacroPointType lcMacroPoint = action.toLcMacroPoint();
        Assert.assertNotNull(lcMacroPoint);

        LOGGER.info("Current lcMacroPoint: {}", lcMacroPoint);
        Assert.assertTrue(lcMacroPoint instanceof ServoPortPoint);
        ServoPortPoint servoPortPoint = (ServoPortPoint) lcMacroPoint;

        Assert.assertEquals(servoPortPoint.getOutputNumber(), 1);
        Assert.assertNotNull(servoPortPoint.getDelay());
        Assert.assertEquals(servoPortPoint.getDelay(), Integer.valueOf(90));
        Assert.assertEquals(servoPortPoint.getServoPortActionType().getDestination(), 20);
        Assert.assertEquals(servoPortPoint.getServoPortActionType().getAction(), ServoActionType.START);
    }
}
