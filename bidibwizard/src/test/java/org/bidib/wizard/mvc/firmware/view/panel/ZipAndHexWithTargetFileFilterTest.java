package org.bidib.wizard.mvc.firmware.view.panel;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import junit.framework.Assert;

public class ZipAndHexWithTargetFileFilterTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ZipAndHexWithTargetFileFilterTest.class);

    @Test
    public void acceptFirmwareZipTest() {
        File file = new File("");
        file = new File(file.getAbsoluteFile(), "target/test-classes/firmware/bidib_if2_v2.04.01.zip");
        LOGGER.info("Prepared file: {}", file.getAbsolutePath());

        ZipAndHexWithTargetFileFilter filter = new ZipAndHexWithTargetFileFilter();

        Assert.assertTrue(filter.accept(file));
    }

    @Test
    public void acceptFirmwareHexWithoutDestinationTest() {
        File file = new File("");
        file = new File(file.getAbsoluteFile(), "target/test-classes/firmware/Testboard.hex");
        LOGGER.info("Prepared file: {}", file.getAbsolutePath());

        ZipAndHexWithTargetFileFilter filter = new ZipAndHexWithTargetFileFilter();

        Assert.assertTrue(filter.accept(file));
    }

    @Test
    public void acceptFirmwareHexWithDestinationTest() {
        File file = new File("");
        file = new File(file.getAbsoluteFile(), "target/test-classes/firmware/Testboard_V_1.01.01.000.hex");
        LOGGER.info("Prepared file: {}", file.getAbsolutePath());

        ZipAndHexWithTargetFileFilter filter = new ZipAndHexWithTargetFileFilter();

        Assert.assertTrue(filter.accept(file));
    }

    @Test
    public void acceptFirmwareInvalidExtensionTest() {
        File file = new File("");
        file = new File(file.getAbsoluteFile(), "target/test-classes/firmware/error.txt");
        LOGGER.info("Prepared file: {}", file.getAbsolutePath());

        ZipAndHexWithTargetFileFilter filter = new ZipAndHexWithTargetFileFilter();

        Assert.assertFalse(filter.accept(file));
    }
}
