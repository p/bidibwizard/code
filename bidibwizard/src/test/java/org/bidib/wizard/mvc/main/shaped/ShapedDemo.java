package org.bidib.wizard.mvc.main.shaped;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Shape;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Arc2D;
import java.awt.geom.Area;
import java.awt.geom.Path2D;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShapedDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(ShapedDemo.class);

    public static void main(String[] args) {

        JFrame frame = new JFrame("ShapedDemo");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        JPanel demoPanel = new JPanel(null);
        demoPanel.setPreferredSize(new Dimension(400, 400));

        int width = 160;
        int height = 160;

        Area area1 = preparePie(210, 210, width, height, 90, 45);
        ShapeComponent comp3 = new ShapeComponent(area1, Color.ORANGE);
        comp3.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                LOGGER.info("Mouse clicked: {}", "comp3");
            }
        });
        demoPanel.add(comp3);
        comp3.setBounds(200, 200, width, height);

        Area area2 = preparePie(210, 210, width, height, 45, 45);
        final ShapeComponent comp4 = new ShapeComponent(area2, Color.GREEN);
        comp4.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                LOGGER.info("Mouse clicked: {}", "comp4");
            }
        });
        demoPanel.add(comp4);
        comp4.setBounds(258, 200, width, height);

        Area area3 = preparePie(210, 210, width, height, 0, 45);
        final ShapeComponent comp5 = new ShapeComponent(area3, Color.BLUE);
        comp5.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                LOGGER.info("Mouse clicked: {}", "comp5");
            }
        });
        demoPanel.add(comp5);
        comp5.setBounds(287, 224, width, height);

        frame.getContentPane().add(demoPanel);

        frame.pack();

        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

    }

    public static Area preparePie(int x1, int y1, int w, int h, int start, int extend) {
        Arc2D.Float arc = new Arc2D.Float(x1, y1, w, h, start, extend, Arc2D.PIE);
        Area area = new Area(arc);
        area.subtract(new Area(new Arc2D.Float(x1 + w / 4, y1 + h / 4, w / 2, h / 2, start, extend, Arc2D.PIE)));
        return area;
    }

    public static Shape prepareArc(/* Grahics2D g, */int x1, int y1, int x2, int y2, int x3, int y3) {
        Path2D.Float path = new Path2D.Float();
        path.moveTo(x1, y1);
        path.quadTo(x2, y2, x3, y3);

        return path;
    }

}
