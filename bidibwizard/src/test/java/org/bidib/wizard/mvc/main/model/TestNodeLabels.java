package org.bidib.wizard.mvc.main.model;

import java.io.File;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestNodeLabels extends NodeLabels {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(TestLightPortLabels.class);

    public TestNodeLabels() {

    }

    protected String getFileName() {

        URL url = LightPortLabelsTest.class.getResource("/");
        LOGGER.info("Current url: {}", url);
        return new File(url.getPath(), "NodeLabels").toString();
    };

}
