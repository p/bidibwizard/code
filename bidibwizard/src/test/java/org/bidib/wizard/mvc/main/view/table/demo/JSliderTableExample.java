package org.bidib.wizard.mvc.main.view.table.demo;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.DefaultCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

public class JSliderTableExample extends JFrame {

    private static final long serialVersionUID = 1L;

    public JSliderTableExample() {
        super("JSliderTableExample Example");

        DefaultTableModel dtm = new DefaultTableModel();
        dtm.setDataVector(new Object[][] { { "JSlider1", new Integer(10), new Integer(15) },
            { "JSlider2", new Integer(12), new Integer(17) } }, new Object[] { "String", "JSlider", "JSlider2" });

        JTable table = new JTable(dtm);
        table.getColumn("JSlider").setCellRenderer(new SliderRenderer2(JSlider.HORIZONTAL, 0, 20, 10));
        table.getColumn("JSlider").setCellEditor(new SliderEditor2(JSlider.HORIZONTAL, 0, 20, 10));

        table.getColumn("JSlider2").setCellRenderer(new SliderRenderer2(JSlider.HORIZONTAL, 0, 30, 10));
        table.getColumn("JSlider2").setCellEditor(new SliderEditor2(JSlider.HORIZONTAL, 0, 30, 10));

        table.setRowHeight(20);
        JScrollPane scroll = new JScrollPane(table);
        getContentPane().add(scroll);

        setSize(400, 100);
        setVisible(true);
    }

    public static void main(String[] args) {
        JSliderTableExample frame = new JSliderTableExample();
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
    }
}

class SliderRenderer extends JSlider implements TableCellRenderer {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public SliderRenderer(int orientation, int min, int max, int value) {
        super(orientation, min, max, value);
    }

    public Component getTableCellRendererComponent(
        JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        if (isSelected) {
            setForeground(table.getSelectionForeground());
            setBackground(table.getSelectionBackground());
        }
        else {
            setForeground(table.getForeground());
            setBackground(table.getBackground());
        }

        TableColumnModel columnModel = table.getColumnModel();
        TableColumn selectedColumn = columnModel.getColumn(column);
        int columnWidth = selectedColumn.getWidth();
        int columnHeight = table.getRowHeight();
        setSize(new Dimension(columnWidth, columnHeight));

        setValue(((Integer) value).intValue());
        updateUI();
        return this;
    }
}

class SliderEditor extends DefaultCellEditor {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    protected JSlider slider;

    public SliderEditor(int orientation, int min, int max, int value) {
        super(new JCheckBox());
        slider = new JSlider(orientation, min, max, value);
        slider.setOpaque(true);
    }

    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        if (isSelected) {
            slider.setForeground(table.getSelectionForeground());
            slider.setBackground(table.getSelectionBackground());
        }
        else {
            slider.setForeground(table.getForeground());
            slider.setBackground(table.getBackground());
        }
        slider.setValue(((Integer) value).intValue());

        return slider;
    }

    public Object getCellEditorValue() {
        return new Integer(slider.getValue());
    }

    public boolean stopCellEditing() {
        return super.stopCellEditing();
    }

    protected void fireEditingStopped() {
        super.fireEditingStopped();
    }
}