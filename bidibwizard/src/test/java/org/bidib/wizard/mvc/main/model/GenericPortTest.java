package org.bidib.wizard.mvc.main.model;

import java.util.HashMap;
import java.util.Map;

import org.bidib.jbidibc.core.BidibLibrary;
import org.bidib.jbidibc.core.port.PortConfigValue;
import org.bidib.jbidibc.core.port.ReconfigPortConfigValue;
import org.testng.Assert;
import org.testng.annotations.Test;

public class GenericPortTest {

    @Test
    public void getPortNumber() {

        GenericPort genericPort = new GenericPort(1);

        Assert.assertNotNull(genericPort);
        Assert.assertEquals(genericPort.getPortNumber(), Integer.valueOf(1));
    }

    @Test
    public void isSupportsSwitchPortFailNoConfigValue() {

        GenericPort genericPort = new GenericPort(1);

        Assert.assertNotNull(genericPort);
        Assert.assertFalse(genericPort.isSupportsSwitchPort());
    }

    @Test
    public void isSupportsSwitchPortFailNoSwitchPort() {

        GenericPort genericPort = new GenericPort(1);

        Map<Byte, PortConfigValue<?>> portConfig = new HashMap<Byte, PortConfigValue<?>>();
        portConfig.put(BidibLibrary.BIDIB_PCFG_RECONFIG, new ReconfigPortConfigValue(Integer.valueOf(0x800201)));

        Assert.assertNotNull(genericPort);
        genericPort.setPortConfig(portConfig);

        Assert.assertFalse(genericPort.isSupportsSwitchPort());
    }

    @Test
    public void isSupportsSwitchPort() {

        GenericPort genericPort = new GenericPort(1);

        Map<Byte, PortConfigValue<?>> portConfig = new HashMap<Byte, PortConfigValue<?>>();
        portConfig.put(BidibLibrary.BIDIB_PCFG_RECONFIG, new ReconfigPortConfigValue(Integer.valueOf(0x800301)));

        Assert.assertNotNull(genericPort);
        genericPort.setPortConfig(portConfig);

        Assert.assertTrue(genericPort.isSupportsSwitchPort());
    }

    @Test
    public void isSupportsSwitchPort2() {

        GenericPort genericPort = new GenericPort(1);

        Map<Byte, PortConfigValue<?>> portConfig = new HashMap<Byte, PortConfigValue<?>>();
        portConfig.put(BidibLibrary.BIDIB_PCFG_RECONFIG, new ReconfigPortConfigValue(Integer.valueOf(0x800100)));

        Assert.assertNotNull(genericPort);
        genericPort.setPortConfig(portConfig);

        Assert.assertTrue(genericPort.isSupportsSwitchPort());
    }

    @Test
    public void isSupportsServoPort() {

        GenericPort genericPort = new GenericPort(1);

        Map<Byte, PortConfigValue<?>> portConfig = new HashMap<Byte, PortConfigValue<?>>();
        portConfig.put(BidibLibrary.BIDIB_PCFG_RECONFIG, new ReconfigPortConfigValue(Integer.valueOf(0x000402)));

        Assert.assertNotNull(genericPort);
        genericPort.setPortConfig(portConfig);

        Assert.assertTrue(genericPort.isSupportsServoPort());
    }

    @Test
    public void isSupportsInputPort() {

        GenericPort genericPort = new GenericPort(1);

        Map<Byte, PortConfigValue<?>> portConfig = new HashMap<Byte, PortConfigValue<?>>();
        portConfig.put(BidibLibrary.BIDIB_PCFG_RECONFIG, new ReconfigPortConfigValue(Integer.valueOf(0x8003FF)));

        Assert.assertNotNull(genericPort);
        genericPort.setPortConfig(portConfig);

        Assert.assertTrue(genericPort.isSupportsInputPort());
    }
}
