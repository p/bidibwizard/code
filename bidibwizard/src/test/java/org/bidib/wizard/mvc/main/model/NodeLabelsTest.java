package org.bidib.wizard.mvc.main.model;

import java.io.File;

import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

public class NodeLabelsTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(NodeLabelsTest.class);

    @Test
    public void saveLabelsTest() {

        LOGGER.info("Save labels in very old format.");

        TestNodeLabels nodeLabels = new TestNodeLabels();

        nodeLabels.setLabel(0x5340D75001236L, "Node_0_Test");
        nodeLabels.setLabel(0x5340D75001238L, "Node_1_Test");

        nodeLabels.save();

        Assert.assertTrue(new File(nodeLabels.getFileName()).exists());
    }
}
