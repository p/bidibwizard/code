package org.bidib.wizard.mvc.main.view.panel;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.bidib.jbidibc.core.Node;
import org.bidib.jbidibc.core.SoftwareVersion;
import org.bidib.jbidibc.core.helpers.Context;
import org.bidib.jbidibc.core.helpers.DefaultContext;
import org.bidib.jbidibc.core.node.ConfigurationVariable;
import org.bidib.jbidibc.exchange.vendorcv.NodeType;
import org.bidib.jbidibc.exchange.vendorcv.TemplateType;
import org.bidib.jbidibc.exchange.vendorcv.VendorCvData;
import org.bidib.jbidibc.exchange.vendorcv.VendorCvFactory;
import org.bidib.wizard.mvc.common.view.cvdefinition.CvDefinitionTreeTableModel;
import org.bidib.wizard.mvc.main.view.cvdef.CvNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.jidesoft.grid.DefaultExpandableRow;

public class CvDefinitionTreeHelperTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(CvDefinitionTreeHelperTest.class);

    private static final long UUID_ONECONTROL = 0x05000d75002e00L;

    @BeforeClass
    public void initialize() {
        LOGGER.info("Set the jide license.");
        com.jidesoft.utils.Lm.verifyLicense("Andreas Kuhtz", "BiDiB-Wizard", "T1xTEztBM2Obp3w39SfBqkRoQBicczu");
    }

    @Test
    public void addSubNodes() {
        LOGGER.info("Add subnodes test.");

        Map<String, CvNode> cvNumberToNodeMap = new LinkedHashMap<String, CvNode>();

        String lang = "de-DE";
        Node node = new Node(0, new byte[] { 1 }, UUID_ONECONTROL);
        node.setSoftwareVersion(new SoftwareVersion(2, 0, 5));

        final Context context = new DefaultContext();

        VendorCvData vendorCV = VendorCvFactory.getCvDefinition(node, context, "classpath:/bidib");

        List<NodeType> nodes = vendorCV.getVendorCV().getCVDefinition().getNode();
        List<ConfigurationVariable> configVariables = null;
        // final DeviceNode root = new DeviceNode(new Object[] { "root" });

        CvDefinitionTreeTableModel treeModel = new CvDefinitionTreeTableModel();
        final DefaultExpandableRow root = (DefaultExpandableRow) treeModel.getRoot();

        Map<String, TemplateType> templatesMap = new HashMap<String, TemplateType>();
        if (vendorCV.getVendorCV().getTemplates() != null) {
            for (TemplateType template : vendorCV.getVendorCV().getTemplates().getTemplate()) {
                LOGGER.trace("Add template: {}", template.getName());
                templatesMap.put(template.getName(), template);
            }
            LOGGER.trace("Prepared templatesMap: {}", templatesMap.keySet());
        }
        else {
            LOGGER.info("No templates available.");
        }

        configVariables =
            new CvDefinitionTreeHelper().addSubNodes(root, nodes, treeModel, templatesMap, null, null, false, lang,
                cvNumberToNodeMap);

        Assert.assertNotNull(configVariables);
        // expect 406 CV values
        Assert.assertEquals(configVariables.size(), 406);
    }

    @Test
    public void addSubNodes2() {

        Map<String, CvNode> cvNumberToNodeMap = new LinkedHashMap<String, CvNode>();

        String lang = "de-DE";
        Node node = new Node(0, new byte[] { 1 }, UUID_ONECONTROL);
        node.setSoftwareVersion(new SoftwareVersion(1, 0, 0));

        final Context context = new DefaultContext();

        VendorCvData vendorCV = VendorCvFactory.getCvDefinition(node, context, "classpath:/bidib");

        List<NodeType> nodes = vendorCV.getVendorCV().getCVDefinition().getNode();
        List<ConfigurationVariable> configVariables = null;
        // final DeviceNode root = new DeviceNode(new Object[] { "root" });

        CvDefinitionTreeTableModel treeModel = new CvDefinitionTreeTableModel();
        final DefaultExpandableRow root = (DefaultExpandableRow) treeModel.getRoot();

        Map<String, TemplateType> templatesMap = new HashMap<String, TemplateType>();
        if (vendorCV.getVendorCV().getTemplates() != null) {
            for (TemplateType template : vendorCV.getVendorCV().getTemplates().getTemplate()) {
                LOGGER.trace("Add template: {}", template.getName());
                templatesMap.put(template.getName(), template);
            }
            LOGGER.trace("Prepared templatesMap: {}", templatesMap.keySet());
        }
        else {
            LOGGER.info("No templates available.");
        }

        configVariables =
            new CvDefinitionTreeHelper().addSubNodes(root, nodes, treeModel, templatesMap, null, null, false, lang,
                cvNumberToNodeMap);

        Assert.assertNotNull(configVariables);
        // expect 393 CV values
        Assert.assertEquals(configVariables.size(), 393);
    }
}
