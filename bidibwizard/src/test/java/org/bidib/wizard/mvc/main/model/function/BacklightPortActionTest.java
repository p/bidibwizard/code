package org.bidib.wizard.mvc.main.model.function;

import org.bidib.jbidibc.exchange.lcmacro.BacklightActionType;
import org.bidib.jbidibc.exchange.lcmacro.BacklightPortPoint;
import org.bidib.jbidibc.exchange.lcmacro.LcMacroPointType;
import org.bidib.wizard.comm.BacklightPortStatus;
import org.bidib.wizard.mvc.main.model.BacklightPort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class BacklightPortActionTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(BacklightPortActionTest.class);

    @Test
    public void toLcMacroPoint() {
        BacklightPort backlightPort = new BacklightPort();
        backlightPort.setId(1);
        backlightPort.setDimSlopeDown(4); // not included in macro step because config only
        backlightPort.setDimSlopeUp(12); // not included in macro step because config only
        backlightPort.setDmxMapping(89); // not included in macro step because config only
        BacklightPortAction action =
            new BacklightPortAction(BacklightPortStatus.START, backlightPort, 90 /* delay */, 20);

        LcMacroPointType lcMacroPoint = action.toLcMacroPoint();
        Assert.assertNotNull(lcMacroPoint);

        LOGGER.info("Current lcMacroPoint: {}", lcMacroPoint);
        Assert.assertTrue(lcMacroPoint instanceof BacklightPortPoint);
        BacklightPortPoint backlightPortPoint = (BacklightPortPoint) lcMacroPoint;

        Assert.assertEquals(backlightPortPoint.getOutputNumber(), 1);
        Assert.assertNotNull(backlightPortPoint.getDelay());
        Assert.assertEquals(backlightPortPoint.getDelay(), Integer.valueOf(90));
        Assert.assertEquals(backlightPortPoint.getBacklightPortActionType().getBrightness(), 20);
        Assert.assertEquals(backlightPortPoint.getBacklightPortActionType().getAction(), BacklightActionType.START);
    }
}
