package org.bidib.wizard.mvc.preferences.model;

import org.bidib.wizard.mvc.common.model.PreferencesPortType;
import org.bidib.wizard.mvc.common.model.PreferencesPortType.ConnectionPortType;
import org.testng.Assert;
import org.testng.annotations.Test;

public class PreferencesPortTypeTest {

    @Test
    public void getValue() {
        Assert.assertEquals(PreferencesPortType.getValue("SerialSimulation"), new PreferencesPortType(
            ConnectionPortType.SerialSimulation));
        Assert.assertEquals(PreferencesPortType.getValue("UdpSimulation"), new PreferencesPortType(
            ConnectionPortType.UdpSimulation));

        Assert.assertEquals(PreferencesPortType.getValue("SerialPort"), new PreferencesPortType(
            ConnectionPortType.SerialPort));
        Assert.assertEquals(PreferencesPortType.getValue("SerialSymLink:tty/USB0"), new PreferencesPortType(
            ConnectionPortType.SerialSymLink, "tty/USB0"));
        Assert.assertEquals(PreferencesPortType.getValue("SerialSymLink:tty/USB0").getConnectionName(), "tty/USB0");
        Assert.assertEquals(PreferencesPortType.getValue("SerialSymLink:").getConnectionName(), null);

        Assert.assertEquals(PreferencesPortType.getValue("UdpPort:localhost:61628").getConnectionName(),
            "localhost:61628");
        Assert.assertEquals(PreferencesPortType.getValue("UdpPort:").getConnectionName(), null);
    }

    @Test
    public void isSimulation() {
        Assert.assertTrue(PreferencesPortType
            .isSimulation(new PreferencesPortType(ConnectionPortType.SerialSimulation)));
        Assert.assertTrue(PreferencesPortType.isSimulation(new PreferencesPortType(ConnectionPortType.UdpSimulation)));

        Assert.assertFalse(PreferencesPortType.isSimulation(new PreferencesPortType(ConnectionPortType.SerialPort)));
        Assert.assertFalse(PreferencesPortType.isSimulation(new PreferencesPortType(ConnectionPortType.SerialSymLink)));
        Assert.assertFalse(PreferencesPortType.isSimulation(new PreferencesPortType(ConnectionPortType.UdpPort)));
    }
}
