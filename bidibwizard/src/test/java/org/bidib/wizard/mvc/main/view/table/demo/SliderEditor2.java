package org.bidib.wizard.mvc.main.view.table.demo;

import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.DefaultCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SliderEditor2 extends DefaultCellEditor {

    private static final Logger LOGGER = LoggerFactory.getLogger(SliderEditor2.class);

    protected JSlider slider;

    private JLabel label;

    private JPanel panel;

    public SliderEditor2(int orientation, int min, int max, int value) {
        super(new JCheckBox());

        panel = new JPanel();
        panel.setLayout(new BorderLayout());

        label = new JLabel();

        slider = new JSlider(orientation, min, max, value);
        slider.setOpaque(true);

        panel.add(slider, BorderLayout.CENTER);

        panel.add(label, BorderLayout.EAST);
    }

    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        if (isSelected) {
            slider.setForeground(table.getSelectionForeground());
            slider.setBackground(table.getSelectionBackground());
        }
        else {
            slider.setForeground(table.getForeground());
            slider.setBackground(table.getBackground());
        }

        LOGGER.info("Set the value: {}", value);

        slider.setValue(((Integer) value).intValue());

        if (value != null) {
            label.setText(Integer.toString(((Integer) value).intValue()));
        }
        // else {
        // label.setText(null);
        // }

        // return slider;
        return panel;
    }

    public Object getCellEditorValue() {
        return new Integer(slider.getValue());
    }

    public boolean stopCellEditing() {
        return super.stopCellEditing();
    }

    protected void fireEditingStopped() {
        super.fireEditingStopped();
    }

}
