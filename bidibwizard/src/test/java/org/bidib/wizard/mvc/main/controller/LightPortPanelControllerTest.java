package org.bidib.wizard.mvc.main.controller;

import org.bidib.wizard.mvc.main.model.LightPort;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LightPortPanelControllerTest {

    private LightPortPanelController controller = new LightPortPanelController(null);

    @Test
    public void testValidRange() {
        final LightPort firstPort = new LightPort();
        firstPort.setId(59);
        int portsCount = 2;
        Integer channelBStartWS28xxNumber = Integer.valueOf(20);

        Integer upperRangeValue = controller.testValidRange(firstPort, portsCount, channelBStartWS28xxNumber);

        Assert.assertNotNull(upperRangeValue);
        Assert.assertEquals(upperRangeValue, Integer.valueOf(channelBStartWS28xxNumber * 3));
    }

    @Test
    public void testValidRange2() {
        final LightPort firstPort = new LightPort();
        firstPort.setId(58);
        int portsCount = 2;
        Integer channelBStartWS28xxNumber = Integer.valueOf(20);

        Integer upperRangeValue = controller.testValidRange(firstPort, portsCount, channelBStartWS28xxNumber);

        Assert.assertNotNull(upperRangeValue);
        Assert.assertEquals(upperRangeValue, Integer.valueOf(channelBStartWS28xxNumber * 3));
    }

    @Test
    public void testValidRangeInChannelB() {
        final LightPort firstPort = new LightPort();
        firstPort.setId(63);
        int portsCount = 4;
        Integer channelBStartWs28xxNumber = Integer.valueOf(20);

        Integer upperRangeValue = controller.testValidRange(firstPort, portsCount, channelBStartWs28xxNumber);

        Assert.assertNull(upperRangeValue);
    }

    @Test
    public void testValidRangeInChannelB2() {
        final LightPort firstPort = new LightPort();
        firstPort.setId(60);
        int portsCount = 4;
        Integer channelBStartWs28xxNumber = Integer.valueOf(20);

        Integer upperRangeValue = controller.testValidRange(firstPort, portsCount, channelBStartWs28xxNumber);

        Assert.assertNull(upperRangeValue);
    }
}
