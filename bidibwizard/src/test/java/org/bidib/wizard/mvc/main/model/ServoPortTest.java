package org.bidib.wizard.mvc.main.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ServoPortTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(ServoPortTest.class);

    @Test
    public void getAbsoluteValue() {
        ServoPort servoPort = new ServoPort();
        int absoluteValue = servoPort.getAbsoluteValue(0);
        LOGGER.info("absolute value of 0: {}", absoluteValue);
        Assert.assertEquals(absoluteValue, 0);

        absoluteValue = servoPort.getAbsoluteValue(50);
        LOGGER.info("absolute value of 50: {}", absoluteValue);
        Assert.assertEquals(absoluteValue, 127);

        absoluteValue = servoPort.getAbsoluteValue(100);
        LOGGER.info("absolute value of 100: {}", absoluteValue);
        Assert.assertEquals(absoluteValue, 255);
    }

    @Test(expectedExceptions = { IllegalArgumentException.class })
    public void getAbsoluteValueLowerLimitExceeded() {
        ServoPort servoPort = new ServoPort();
        servoPort.getAbsoluteValue(-1);
    }

    @Test(expectedExceptions = { IllegalArgumentException.class })
    public void getAbsoluteValueUpperLimitExceeded() {
        ServoPort servoPort = new ServoPort();
        servoPort.getAbsoluteValue(101);
    }

    @Test
    public void getRelativeValue() {
        ServoPort servoPort = new ServoPort();
        int relativeValue = servoPort.getRelativeValue(0);
        LOGGER.info("relative value of 0: {}", relativeValue);
        Assert.assertEquals(relativeValue, 0);

        relativeValue = servoPort.getRelativeValue(127);
        LOGGER.info("relative value of 127: {}", relativeValue);
        Assert.assertEquals(relativeValue, 50);

        relativeValue = servoPort.getRelativeValue(255);
        LOGGER.info("relative value of 255: {}", relativeValue);
        Assert.assertEquals(relativeValue, 100);
    }

    @Test(expectedExceptions = { IllegalArgumentException.class })
    public void getRelativeValueLowerLimitExceeded() {
        ServoPort servoPort = new ServoPort();
        servoPort.getRelativeValue(-1);
    }

    @Test(expectedExceptions = { IllegalArgumentException.class })
    public void getRelativeValueUpperLimitExceeded() {
        ServoPort servoPort = new ServoPort();
        servoPort.getRelativeValue(256);
    }

}
