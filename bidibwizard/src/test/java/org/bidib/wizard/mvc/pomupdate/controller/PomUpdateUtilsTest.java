package org.bidib.wizard.mvc.pomupdate.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import junit.framework.Assert;

public class PomUpdateUtilsTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(PomUpdateUtilsTest.class);

    @Test
    public void calculateResendPacketCountTest() {

        int requiredMinPomRepeat = 6;

        for (int pomRepeat = 1; pomRepeat < 10; pomRepeat++) {
            LOGGER.info("Current pomRepeat: {}", pomRepeat);

            int resendPacketCount = PomUpdateUtils.calculateResendPacketCount(pomRepeat, requiredMinPomRepeat);

            LOGGER.info("Current pomRepeat: {}, resendPacketCount: {}, rest: {}", pomRepeat, resendPacketCount,
                (requiredMinPomRepeat % pomRepeat));

            Assert.assertTrue(resendPacketCount * pomRepeat >= requiredMinPomRepeat);
        }
    }
}
