package org.bidib.wizard.mvc.common.view.slider;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Polygon;
import java.util.Hashtable;

import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class SliderSample {

    public static void main(final String args[]) {
        Runnable runner = new Runnable() {
            public void run() {
                JFrame frame = new JFrame("Sample Sliders");
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                ChangeListener listener = new SliderChangeListener();
                JSlider js1 = new JSlider();
                js1.setPaintLabels(true);
                js1.setPaintTicks(true);
                js1.setMajorTickSpacing(10);
                js1.setMinorTickSpacing(5);
                js1.addChangeListener(listener);
                JSlider js2 = new JSlider();
                js2.setInverted(true);
                js2.setPaintTrack(false);
                js2.addChangeListener(listener);
                JSlider js3 = new JSlider(JSlider.VERTICAL);
                js3.setPaintLabels(true);
                js3.setPaintTicks(true);
                js3.setMajorTickSpacing(10);
                js3.setMinorTickSpacing(5);
                js3.addChangeListener(listener);

                JSlider js4 = new JSlider(JSlider.VERTICAL);
                js4.setInverted(true);

                Hashtable<Integer, JComponent> table = new Hashtable<Integer, JComponent>();
                table.put(new Integer(0), new JLabel(new DiamondIcon(Color.RED)));
                table.put(new Integer(0), new JLabel(new DiamondIcon(Color.GREEN)));
                table.put(new Integer(0), new JLabel(new DiamondIcon(Color.BLUE)));
                table.put(new Integer(11), new JLabel("Eleven"));
                table.put(new Integer(22), new JLabel("Twenty-Two"));
                table.put(new Integer(33), new JLabel("Thirty-Three"));
                table.put(new Integer(44), new JLabel("Fourty-Four"));
                table.put(new Integer(55), new JLabel("Fifty-Five"));
                table.put(new Integer(66), new JLabel("Sixty-Six"));
                table.put(new Integer(77), new JLabel("Seventy-Seven"));
                table.put(new Integer(88), new JLabel("Eighty-Eight"));
                table.put(new Integer(100), new JLabel(new DiamondIcon(Color.BLACK)));
                js4.setLabelTable(table);
                js4.setPaintLabels(true);
                js4.addChangeListener(listener);

                frame.add(js1, BorderLayout.NORTH);
                frame.add(js2, BorderLayout.SOUTH);
                frame.add(js3, BorderLayout.WEST);
                frame.add(js4, BorderLayout.EAST);
                frame.setSize(400, 300);
                frame.setVisible(true);
            }
        };
        EventQueue.invokeLater(runner);
    }

    public static class SliderChangeListener implements ChangeListener {
        public void stateChanged(ChangeEvent changeEvent) {
            Object source = changeEvent.getSource();
            JSlider theJSlider = (JSlider) source;
            if (!theJSlider.getValueIsAdjusting()) {
                System.out.println("Slider changed: " + theJSlider.getValue());
            }
        }
    }

    public static class DiamondIcon implements Icon {
        private Color color;

        private boolean selected;

        private int width;

        private int height;

        private Polygon poly;

        private static final int DEFAULT_WIDTH = 10;

        private static final int DEFAULT_HEIGHT = 10;

        public DiamondIcon(Color color) {
            this(color, true, DEFAULT_WIDTH, DEFAULT_HEIGHT);
        }

        public DiamondIcon(Color color, boolean selected) {
            this(color, selected, DEFAULT_WIDTH, DEFAULT_HEIGHT);
        }

        public DiamondIcon(Color color, boolean selected, int width, int height) {
            this.color = color;
            this.selected = selected;
            this.width = width;
            this.height = height;
            initPolygon();
        }

        private void initPolygon() {
            poly = new Polygon();
            int halfWidth = width / 2;
            int halfHeight = height / 2;
            poly.addPoint(0, halfHeight);
            poly.addPoint(halfWidth, 0);
            poly.addPoint(width, halfHeight);
            poly.addPoint(halfWidth, height);
        }

        public int getIconHeight() {
            return height;
        }

        public int getIconWidth() {
            return width;
        }

        public void paintIcon(Component c, Graphics g, int x, int y) {
            g.setColor(color);
            g.translate(x, y);
            if (selected) {
                g.fillPolygon(poly);
            }
            else {
                g.drawPolygon(poly);
            }
            g.translate(-x, -y);
        }
    }
}