package org.bidib.wizard.mvc.common.view.graph;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Enumeration;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LedBarGraphDemo {

    private static final Logger LOGGER = LoggerFactory.getLogger(LedBarGraphDemo.class);

    private Timer animation;

    private Random random = new Random();

    private int min = 0;

    public void startAnimation(final LedBarGraph graphing) {
        animation = new Timer(100, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                int randomNumber = random.nextInt(graphing.getTotalLedsCount() - min + 1) + min;
                graphing.setValue(randomNumber * graphing.getTotalLedsCount());
            }
        });
        animation.setInitialDelay(3000);
        animation.start();
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {

                try {
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                }
                catch (ClassNotFoundException | InstantiationException | IllegalAccessException
                    | UnsupportedLookAndFeelException e) {
                    LOGGER.warn("Set look and feel failed.", e);
                }

                // print all fonts
                Enumeration<Object> keys = UIManager.getDefaults().keys();
                while (keys.hasMoreElements()) {
                    Object key = keys.nextElement();
                    if (key instanceof String && ((String) key).contains("font")) {
                        LOGGER.info("{}={}", key, UIManager.getDefaults().get(key));
                    }
                }

                JFrame frame = new JFrame("LedBarGraph Demo");
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.getContentPane().setLayout(new BorderLayout());

                LedBarGraphDemo barGraphDemo = new LedBarGraphDemo();
                LedBarGraph ledBarGraph = new LedBarGraph(10);
                ledBarGraph.setBorder(new EmptyBorder(10, 10, 10, 10));
                frame.getContentPane().add(ledBarGraph);
                frame.getContentPane().add(new JLabel("10%"), BorderLayout.SOUTH);
                barGraphDemo.startAnimation(ledBarGraph);

                frame.setPreferredSize(new Dimension(350, 200));
                frame.pack();
                frame.setVisible(true);
            }
        });
    }
}
