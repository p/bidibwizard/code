package org.bidib.wizard.mvc.main.model.function;

import org.bidib.jbidibc.exchange.lcmacro.FlagOperationType;
import org.bidib.jbidibc.exchange.lcmacro.FlagPoint;
import org.bidib.jbidibc.exchange.lcmacro.LcMacroPointType;
import org.bidib.wizard.comm.FlagStatus;
import org.bidib.wizard.mvc.main.model.Flag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class FlagFunctionTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(FlagFunctionTest.class);

    @Test
    public void toLcMacroPointQuery1() {
        FlagFunction flagFunction = new FlagFunction(FlagStatus.QUERY_1);
        Flag flag = new Flag(1);
        flagFunction.setFlag(flag);

        LcMacroPointType lcMacroPoint = flagFunction.toLcMacroPoint();
        Assert.assertNotNull(lcMacroPoint);

        LOGGER.info("Current lcMacroPoint: {}", lcMacroPoint);
        Assert.assertTrue(lcMacroPoint instanceof FlagPoint);

        FlagPoint flagPoint = (FlagPoint) lcMacroPoint;
        Assert.assertNotNull(flagPoint.getFlagActionType());
        Assert.assertEquals(flagPoint.getFlagActionType().getFlagNumber(), 1);
        Assert.assertEquals(flagPoint.getFlagActionType().getOperation(), FlagOperationType.QUERY_1);
    }

    @Test
    public void toLcMacroPointQuery0() {
        FlagFunction flagFunction = new FlagFunction(FlagStatus.QUERY_0);
        Flag flag = new Flag(2);
        flagFunction.setFlag(flag);

        LcMacroPointType lcMacroPoint = flagFunction.toLcMacroPoint();
        Assert.assertNotNull(lcMacroPoint);

        LOGGER.info("Current lcMacroPoint: {}", lcMacroPoint);
        Assert.assertTrue(lcMacroPoint instanceof FlagPoint);

        FlagPoint flagPoint = (FlagPoint) lcMacroPoint;
        Assert.assertNotNull(flagPoint.getFlagActionType());
        Assert.assertEquals(flagPoint.getFlagActionType().getFlagNumber(), 2);
        Assert.assertEquals(flagPoint.getFlagActionType().getOperation(), FlagOperationType.QUERY_0);
    }

    @Test
    public void toLcMacroPointClear() {
        FlagFunction flagFunction = new FlagFunction(FlagStatus.CLEAR);
        Flag flag = new Flag(2);
        flagFunction.setFlag(flag);

        LcMacroPointType lcMacroPoint = flagFunction.toLcMacroPoint();
        Assert.assertNotNull(lcMacroPoint);

        LOGGER.info("Current lcMacroPoint: {}", lcMacroPoint);
        Assert.assertTrue(lcMacroPoint instanceof FlagPoint);

        FlagPoint flagPoint = (FlagPoint) lcMacroPoint;
        Assert.assertNotNull(flagPoint.getFlagActionType());
        Assert.assertEquals(flagPoint.getFlagActionType().getFlagNumber(), 2);
        Assert.assertEquals(flagPoint.getFlagActionType().getOperation(), FlagOperationType.CLEAR);
    }

    @Test
    public void toLcMacroPointSet() {
        FlagFunction flagFunction = new FlagFunction(FlagStatus.SET);
        Flag flag = new Flag(5);
        flagFunction.setFlag(flag);

        LcMacroPointType lcMacroPoint = flagFunction.toLcMacroPoint();
        Assert.assertNotNull(lcMacroPoint);

        LOGGER.info("Current lcMacroPoint: {}", lcMacroPoint);
        Assert.assertTrue(lcMacroPoint instanceof FlagPoint);

        FlagPoint flagPoint = (FlagPoint) lcMacroPoint;
        Assert.assertNotNull(flagPoint.getFlagActionType());
        Assert.assertEquals(flagPoint.getFlagActionType().getFlagNumber(), 5);
        Assert.assertEquals(flagPoint.getFlagActionType().getOperation(), FlagOperationType.SET);
    }

}
