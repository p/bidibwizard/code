package org.bidib.wizard.script.node;

import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.bidib.wizard.comm.MacroStatus;
import org.bidib.wizard.highlight.BidibScriptScanner;
import org.bidib.wizard.highlight.Scanner;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.script.view.ScriptParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class AddMacroStepCommandTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(AddMacroStepCommandTest.class);

    @Test
    public void parseRandomDelayStepTest() throws IOException {
        char[] buffer = IOUtils.toCharArray(new StringReader("add step ptype=randomdelay delay=15"));

        Map<String, Object> context = new HashMap<>();
        MainModel mainModel = new MainModel();
        context.put(ScriptParser.KEY_MAIN_MODEL, mainModel);

        Scanner scanner = new BidibScriptScanner();
        scanner.change(0, 0, buffer.length);
        scanner.scan(buffer, 0, buffer.length);
        LOGGER.info("Total number of tokens: {}", scanner.size());

        AddMacroStepCommand addMacroStepCommand = new AddMacroStepCommand(1L);

        addMacroStepCommand.scan(scanner, 0, context);

        Assert.assertEquals(addMacroStepCommand.getDelay(), 15);
    }

    @Test
    public void parseServoStepTest() throws IOException {
        char[] buffer =
            IOUtils.toCharArray(new StringReader("add step ptype=servo action=start name=\"W1\" target=30"));

        Map<String, Object> context = new HashMap<>();
        MainModel mainModel = new MainModel();
        context.put(ScriptParser.KEY_MAIN_MODEL, mainModel);

        Scanner scanner = new BidibScriptScanner();
        scanner.change(0, 0, buffer.length);
        scanner.scan(buffer, 0, buffer.length);
        LOGGER.info("Total number of tokens: {}", scanner.size());

        AddMacroStepCommand addMacroStepCommand = new AddMacroStepCommand(1L);

        addMacroStepCommand.scan(scanner, 0, context);

        Assert.assertEquals(addMacroStepCommand.getDelay(), 0);
        Assert.assertEquals(addMacroStepCommand.getTarget(), 30);
        Assert.assertEquals(addMacroStepCommand.getPortLabel(), "W1");
    }

    @Test
    public void parseServoStep2Test() throws IOException {
        char[] buffer = IOUtils.toCharArray(new StringReader("add step ptype=servo action=start name='W1' target=30"));

        Map<String, Object> context = new HashMap<>();
        MainModel mainModel = new MainModel();
        context.put(ScriptParser.KEY_MAIN_MODEL, mainModel);

        Scanner scanner = new BidibScriptScanner();
        scanner.change(0, 0, buffer.length);
        scanner.scan(buffer, 0, buffer.length);
        LOGGER.info("Total number of tokens: {}", scanner.size());

        AddMacroStepCommand addMacroStepCommand = new AddMacroStepCommand(1L);

        addMacroStepCommand.scan(scanner, 0, context);

        Assert.assertEquals(addMacroStepCommand.getDelay(), 0);
        Assert.assertEquals(addMacroStepCommand.getTarget(), 30);
        Assert.assertEquals(addMacroStepCommand.getPortLabel(), "W1");
    }

    @Test
    public void parseMacroStepTest() throws IOException {
        char[] buffer =
            IOUtils.toCharArray(new StringReader(
                "add step ptype=macro name=\"Sig_\"&%this_accessory%&\"_hp0\" action=stop // dummy values"));

        Map<String, Object> context = new HashMap<>();
        MainModel mainModel = new MainModel();
        context.put(ScriptParser.KEY_MAIN_MODEL, mainModel);
        context.put("%this_accessory%", "2");

        Scanner scanner = new BidibScriptScanner();
        scanner.change(0, 0, buffer.length);
        scanner.scan(buffer, 0, buffer.length);
        LOGGER.info("Total number of tokens: {}", scanner.size());

        AddMacroStepCommand addMacroStepCommand = new AddMacroStepCommand(1L);

        addMacroStepCommand.scan(scanner, 0, context);

        Assert.assertEquals(addMacroStepCommand.getDelay(), 0);
        Assert.assertEquals(addMacroStepCommand.getFunction().getAction(), MacroStatus.STOP);
        Assert.assertEquals(addMacroStepCommand.getPortLabel(), "Sig_2_hp0");
    }

}
