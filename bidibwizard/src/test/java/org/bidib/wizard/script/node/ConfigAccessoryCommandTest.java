package org.bidib.wizard.script.node;

import java.io.IOException;
import java.io.StringReader;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.bidib.wizard.highlight.BidibScriptScanner;
import org.bidib.wizard.highlight.Scanner;
import org.bidib.wizard.script.DefaultScriptContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ConfigAccessoryCommandTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigAccessoryCommandTest.class);

    @Test
    public void parseStartupAspect1() throws IOException {

        char[] buffer = IOUtils.toCharArray(new StringReader("config accessory startup aspect=1"));

        DefaultScriptContext context = new DefaultScriptContext();

        Scanner scanner = new BidibScriptScanner();
        scanner.change(0, 0, buffer.length);
        scanner.scan(buffer, 0, buffer.length);
        LOGGER.info("Total number of tokens: {}", scanner.size());

        ConfigAccessoryCommand configAccessoryCommand = new ConfigAccessoryCommand(1L);

        int index = configAccessoryCommand.scan(scanner, 0, context.getRegistry());
        Assert.assertEquals(8, index);

        Map<String, Integer> accessoryConfig = configAccessoryCommand.getAccessoryConfig();
        Assert.assertNotNull(accessoryConfig);

        Assert.assertNotNull(accessoryConfig.get("startup"));

        Assert.assertEquals(Integer.valueOf(1), accessoryConfig.get("startup"));
    }

    @Test
    public void parseStartupAspectRestore() throws IOException {

        char[] buffer = IOUtils.toCharArray(new StringReader("config accessory startup restore"));

        DefaultScriptContext context = new DefaultScriptContext();

        Scanner scanner = new BidibScriptScanner();
        scanner.change(0, 0, buffer.length);
        scanner.scan(buffer, 0, buffer.length);
        LOGGER.info("Total number of tokens: {}", scanner.size());

        ConfigAccessoryCommand configAccessoryCommand = new ConfigAccessoryCommand(1L);

        int index = configAccessoryCommand.scan(scanner, 0, context.getRegistry());
        Assert.assertEquals(6, index);

        Map<String, Integer> accessoryConfig = configAccessoryCommand.getAccessoryConfig();
        Assert.assertNotNull(accessoryConfig);

        Assert.assertNotNull(accessoryConfig.get("startup"));

        Assert.assertEquals(Integer.valueOf(ConfigAccessoryCommand.ASPECT_PARAM_RESTORE),
            accessoryConfig.get("startup"));
    }

    @Test
    public void parseStartupAspectNode() throws IOException {

        char[] buffer = IOUtils.toCharArray(new StringReader("config accessory startup none"));

        DefaultScriptContext context = new DefaultScriptContext();

        Scanner scanner = new BidibScriptScanner();
        scanner.change(0, 0, buffer.length);
        scanner.scan(buffer, 0, buffer.length);
        LOGGER.info("Total number of tokens: {}", scanner.size());

        ConfigAccessoryCommand configAccessoryCommand = new ConfigAccessoryCommand(1L);

        int index = configAccessoryCommand.scan(scanner, 0, context.getRegistry());
        Assert.assertEquals(6, index);

        Map<String, Integer> accessoryConfig = configAccessoryCommand.getAccessoryConfig();
        Assert.assertNotNull(accessoryConfig);

        Assert.assertNotNull(accessoryConfig.get("startup"));

        Assert.assertEquals(Integer.valueOf(ConfigAccessoryCommand.ASPECT_PARAM_UNCHANGED),
            accessoryConfig.get("startup"));
    }
}
