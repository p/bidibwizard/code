package org.bidib.wizard.script.node;

import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.bidib.wizard.highlight.BidibScriptScanner;
import org.bidib.wizard.highlight.Scanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ConfigMacroTimeCommandTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigMacroTimeCommandTest.class);

    @Test
    public void scanMacroTime() throws IOException {

        char[] buffer = IOUtils.toCharArray(new StringReader("config macrotime day=7 hour=24 minute=0"));

        Map<String, Object> context = new HashMap<>();

        Scanner scanner = new BidibScriptScanner();
        scanner.change(0, 0, buffer.length);
        scanner.scan(buffer, 0, buffer.length);
        LOGGER.info("Total number of tokens: {}", scanner.size());

        ConfigMacroTimeCommand configMacroTimeCommand = new ConfigMacroTimeCommand(1L);

        int index = configMacroTimeCommand.scan(scanner, 0, context);
        Assert.assertEquals(12, index);

        Map<String, Integer> macroTimeConfig = configMacroTimeCommand.getMacroTimeConfig();
        Assert.assertNotNull(macroTimeConfig);

        Assert.assertNotNull(macroTimeConfig.get("day"));
        Assert.assertNotNull(macroTimeConfig.get("hour"));
        Assert.assertNotNull(macroTimeConfig.get("minute"));

        Assert.assertEquals(Integer.valueOf(7), macroTimeConfig.get("day"));
        Assert.assertEquals(Integer.valueOf(24), macroTimeConfig.get("hour"));
        Assert.assertEquals(Integer.valueOf(0), macroTimeConfig.get("minute"));
    }

    @Test
    public void scanMacroTime2() throws IOException {

        char[] buffer = IOUtils.toCharArray(new StringReader("config macrotime day=7 hour=25 minute=0"));

        Map<String, Object> context = new HashMap<>();

        Scanner scanner = new BidibScriptScanner();
        scanner.change(0, 0, buffer.length);
        scanner.scan(buffer, 0, buffer.length);
        LOGGER.info("Total number of tokens: {}", scanner.size());

        ConfigMacroTimeCommand configMacroTimeCommand = new ConfigMacroTimeCommand(1L);

        int index = configMacroTimeCommand.scan(scanner, 0, context);
        Assert.assertEquals(12, index);

        Map<String, Integer> macroTimeConfig = configMacroTimeCommand.getMacroTimeConfig();
        Assert.assertNotNull(macroTimeConfig);

        Assert.assertNotNull(macroTimeConfig.get("day"));
        Assert.assertNotNull(macroTimeConfig.get("hour"));
        Assert.assertNotNull(macroTimeConfig.get("minute"));

        Assert.assertEquals(Integer.valueOf(7), macroTimeConfig.get("day"));
        Assert.assertEquals(Integer.valueOf(25), macroTimeConfig.get("hour"));
        Assert.assertEquals(Integer.valueOf(0), macroTimeConfig.get("minute"));
    }

    @Test
    public void scanMacroTime3() throws IOException {

        char[] buffer = IOUtils.toCharArray(new StringReader("config macrotime day=7 hour=everyfull minute=0"));

        Map<String, Object> context = new HashMap<>();

        Scanner scanner = new BidibScriptScanner();
        scanner.change(0, 0, buffer.length);
        scanner.scan(buffer, 0, buffer.length);
        LOGGER.info("Total number of tokens: {}", scanner.size());

        ConfigMacroTimeCommand configMacroTimeCommand = new ConfigMacroTimeCommand(1L);

        int index = configMacroTimeCommand.scan(scanner, 0, context);
        Assert.assertEquals(12, index);

        Map<String, Integer> macroTimeConfig = configMacroTimeCommand.getMacroTimeConfig();
        Assert.assertNotNull(macroTimeConfig);

        Assert.assertNotNull(macroTimeConfig.get("day"));
        Assert.assertNotNull(macroTimeConfig.get("hour"));
        Assert.assertNotNull(macroTimeConfig.get("minute"));

        Assert.assertEquals(Integer.valueOf(7), macroTimeConfig.get("day"));
        Assert.assertEquals(Integer.valueOf(24), macroTimeConfig.get("hour"));
        Assert.assertEquals(Integer.valueOf(0), macroTimeConfig.get("minute"));
    }

    @Test
    public void scanMacroTime4() throws IOException {

        char[] buffer = IOUtils.toCharArray(new StringReader("config macrotime day=7 hour=everyfullatday minute=0"));

        Map<String, Object> context = new HashMap<>();

        Scanner scanner = new BidibScriptScanner();
        scanner.change(0, 0, buffer.length);
        scanner.scan(buffer, 0, buffer.length);
        LOGGER.info("Total number of tokens: {}", scanner.size());

        ConfigMacroTimeCommand configMacroTimeCommand = new ConfigMacroTimeCommand(1L);

        int index = configMacroTimeCommand.scan(scanner, 0, context);
        Assert.assertEquals(12, index);

        Map<String, Integer> macroTimeConfig = configMacroTimeCommand.getMacroTimeConfig();
        Assert.assertNotNull(macroTimeConfig);

        Assert.assertNotNull(macroTimeConfig.get("day"));
        Assert.assertNotNull(macroTimeConfig.get("hour"));
        Assert.assertNotNull(macroTimeConfig.get("minute"));

        Assert.assertEquals(Integer.valueOf(7), macroTimeConfig.get("day"));
        Assert.assertEquals(Integer.valueOf(25), macroTimeConfig.get("hour"));
        Assert.assertEquals(Integer.valueOf(0), macroTimeConfig.get("minute"));
    }

    @Test
    public void scanMacroTime5() throws IOException {

        char[] buffer = IOUtils.toCharArray(new StringReader("config macrotime day=7 hour=2 minute=3"));

        Map<String, Object> context = new HashMap<>();

        Scanner scanner = new BidibScriptScanner();
        scanner.change(0, 0, buffer.length);
        scanner.scan(buffer, 0, buffer.length);
        LOGGER.info("Total number of tokens: {}", scanner.size());

        ConfigMacroTimeCommand configMacroTimeCommand = new ConfigMacroTimeCommand(1L);

        int index = configMacroTimeCommand.scan(scanner, 0, context);
        Assert.assertEquals(12, index);

        Map<String, Integer> macroTimeConfig = configMacroTimeCommand.getMacroTimeConfig();
        Assert.assertNotNull(macroTimeConfig);

        Assert.assertNotNull(macroTimeConfig.get("day"));
        Assert.assertNotNull(macroTimeConfig.get("hour"));
        Assert.assertNotNull(macroTimeConfig.get("minute"));

        Assert.assertEquals(Integer.valueOf(7), macroTimeConfig.get("day"));
        Assert.assertEquals(Integer.valueOf(2), macroTimeConfig.get("hour"));
        Assert.assertEquals(Integer.valueOf(3), macroTimeConfig.get("minute"));
    }

    @Test
    public void scanMacroTime6() throws IOException {

        char[] buffer = IOUtils.toCharArray(new StringReader("config macrotime day=7 hour=2 minute=everyminute"));

        Map<String, Object> context = new HashMap<>();

        Scanner scanner = new BidibScriptScanner();
        scanner.change(0, 0, buffer.length);
        scanner.scan(buffer, 0, buffer.length);
        LOGGER.info("Total number of tokens: {}", scanner.size());

        ConfigMacroTimeCommand configMacroTimeCommand = new ConfigMacroTimeCommand(1L);

        int index = configMacroTimeCommand.scan(scanner, 0, context);
        Assert.assertEquals(12, index);

        Map<String, Integer> macroTimeConfig = configMacroTimeCommand.getMacroTimeConfig();
        Assert.assertNotNull(macroTimeConfig);

        Assert.assertNotNull(macroTimeConfig.get("day"));
        Assert.assertNotNull(macroTimeConfig.get("hour"));
        Assert.assertNotNull(macroTimeConfig.get("minute"));

        Assert.assertEquals(Integer.valueOf(7), macroTimeConfig.get("day"));
        Assert.assertEquals(Integer.valueOf(2), macroTimeConfig.get("hour"));
        Assert.assertEquals(Integer.valueOf(60), macroTimeConfig.get("minute"));
    }

    @Test
    public void scanMacroTime7() throws IOException {

        char[] buffer = IOUtils.toCharArray(new StringReader("config macrotime day=7 hour=2 minute=everyhalfhour"));

        Map<String, Object> context = new HashMap<>();

        Scanner scanner = new BidibScriptScanner();
        scanner.change(0, 0, buffer.length);
        scanner.scan(buffer, 0, buffer.length);
        LOGGER.info("Total number of tokens: {}", scanner.size());

        ConfigMacroTimeCommand configMacroTimeCommand = new ConfigMacroTimeCommand(1L);

        int index = configMacroTimeCommand.scan(scanner, 0, context);
        Assert.assertEquals(12, index);

        Map<String, Integer> macroTimeConfig = configMacroTimeCommand.getMacroTimeConfig();
        Assert.assertNotNull(macroTimeConfig);

        Assert.assertNotNull(macroTimeConfig.get("day"));
        Assert.assertNotNull(macroTimeConfig.get("hour"));
        Assert.assertNotNull(macroTimeConfig.get("minute"));

        Assert.assertEquals(Integer.valueOf(7), macroTimeConfig.get("day"));
        Assert.assertEquals(Integer.valueOf(2), macroTimeConfig.get("hour"));
        Assert.assertEquals(Integer.valueOf(61), macroTimeConfig.get("minute"));
    }

    @Test
    public void scanMacroTime8() throws IOException {

        char[] buffer = IOUtils.toCharArray(new StringReader("config macrotime day=7 hour=2 minute=everyquarter"));

        Map<String, Object> context = new HashMap<>();

        Scanner scanner = new BidibScriptScanner();
        scanner.change(0, 0, buffer.length);
        scanner.scan(buffer, 0, buffer.length);
        LOGGER.info("Total number of tokens: {}", scanner.size());

        ConfigMacroTimeCommand configMacroTimeCommand = new ConfigMacroTimeCommand(1L);

        int index = configMacroTimeCommand.scan(scanner, 0, context);
        Assert.assertEquals(12, index);

        Map<String, Integer> macroTimeConfig = configMacroTimeCommand.getMacroTimeConfig();
        Assert.assertNotNull(macroTimeConfig);

        Assert.assertNotNull(macroTimeConfig.get("day"));
        Assert.assertNotNull(macroTimeConfig.get("hour"));
        Assert.assertNotNull(macroTimeConfig.get("minute"));

        Assert.assertEquals(Integer.valueOf(7), macroTimeConfig.get("day"));
        Assert.assertEquals(Integer.valueOf(2), macroTimeConfig.get("hour"));
        Assert.assertEquals(Integer.valueOf(62), macroTimeConfig.get("minute"));
    }
}
