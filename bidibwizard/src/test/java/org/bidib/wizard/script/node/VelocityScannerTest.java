package org.bidib.wizard.script.node;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.text.ParseException;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.context.Context;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.RuntimeSingleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class VelocityScannerTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(VelocityScannerTest.class);

    @Test
    public void firstTest() throws ParseException, IOException {

        InputStream is = VelocityScannerTest.class.getResourceAsStream("/scripts/nodescript1.nodescript");

        StringWriter output = null;
        String rendered = null;
        try {
            output = new StringWriter();

            Context velocityContext = new VelocityContext();
            String logTag = "[velocity]";

            String encoding =
                RuntimeSingleton.getString(RuntimeConstants.INPUT_ENCODING, RuntimeConstants.ENCODING_DEFAULT);
            Reader inReader = new InputStreamReader(is, encoding);
            boolean passed = Velocity.evaluate(velocityContext, output, logTag, inReader);

            Assert.assertTrue(passed);

            rendered = output.toString();
        }
        finally {
            if (output != null) {
                try {
                    output.close();
                }
                catch (Exception ex1) {
                    LOGGER.warn("Close output writer failed.", ex1);
                }
            }
        }

        LOGGER.info(rendered);
        Assert.assertNotNull(rendered);
    }
}
