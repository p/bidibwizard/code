package org.bidib.wizard.comm;

import java.util.ArrayList;
import java.util.List;

import org.bidib.jbidibc.core.LcMacro;
import org.bidib.jbidibc.core.enumeration.LcOutputType;
import org.bidib.jbidibc.core.enumeration.PortModelEnum;
import org.bidib.jbidibc.core.enumeration.SwitchPortEnum;
import org.bidib.jbidibc.core.exception.ProtocolException;
import org.bidib.jbidibc.core.node.MacroNode;
import org.bidib.wizard.mvc.main.model.Macro;
import org.bidib.wizard.mvc.main.model.ServoPort;
import org.bidib.wizard.mvc.main.model.SwitchPort;
import org.bidib.wizard.mvc.main.model.function.Function;
import org.bidib.wizard.mvc.main.model.function.ServoPortAction;
import org.bidib.wizard.mvc.main.model.function.SwitchPortAction;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class MacroConversionHelperTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(MacroConversionHelperTest.class);

    private MacroNode macroNode;

    @BeforeTest
    public void init() {
        LOGGER.info("Prepare the mocks.");
        macroNode = Mockito.mock(MacroNode.class);
    }

    @BeforeMethod
    public void prepare() {
        LOGGER.info("Reset the mocks.");
        Mockito.reset(macroNode);
    }

    @Test(expectedExceptions = { IllegalArgumentException.class })
    public void prepareLcMacroNull() throws ProtocolException {

        Macro macro = null;

        MacroConversionHelper helper = new MacroConversionHelper();
        try {
            helper.prepareLcMacroSteps(macroNode, PortModelEnum.type, macro);
        }
        finally {
            // verify that setMacro was never called
            Mockito.verify(macroNode, Mockito.times(0)).setMacro(Mockito.any(LcMacro.class));
        }
    }

    @Test
    public void prepareLcMacroServoPort() throws ProtocolException {

        ServoPort servoPort = new ServoPort();
        servoPort.setId(1);
        servoPort.setSpeed(4); // not included in macro step because config only
        servoPort.setTrimDown(12); // not included in macro step because config only
        servoPort.setTrimUp(89); // not included in macro step because config only

        Macro macro = new Macro(20);
        List<Function<? extends BidibStatus>> functions = new ArrayList<>();

        Function<?> function = new ServoPortAction(ServoPortStatus.START, servoPort, 90, 60);
        functions.add(function);

        // set the functions as last step because they are copied!
        macro.setFunctions(functions);

        MacroConversionHelper helper = new MacroConversionHelper();
        helper.prepareLcMacroSteps(macroNode, PortModelEnum.type, macro);

        LcMacro expectedMacroStep =
            new LcMacro((byte) 0, (byte) 0, (byte) 90, LcOutputType.SERVOPORT.getType(), (byte) 1, (byte) 60);

        // verify that setMacro was called
        Mockito.verify(macroNode, Mockito.times(1)).setMacro(Mockito.eq(expectedMacroStep));
    }

    @Test
    public void prepareLcMacroServoPortFlatModel() throws ProtocolException {

        ServoPort servoPort = new ServoPort();
        servoPort.setId(1);
        servoPort.setSpeed(4); // not included in macro step because config only
        servoPort.setTrimDown(12); // not included in macro step because config only
        servoPort.setTrimUp(89); // not included in macro step because config only

        Macro macro = new Macro(20);
        List<Function<? extends BidibStatus>> functions = new ArrayList<>();

        Function<?> function = new ServoPortAction(ServoPortStatus.START, servoPort, 90, 60);
        functions.add(function);

        // set the functions as last step because they are copied!
        macro.setFunctions(functions);

        MacroConversionHelper helper = new MacroConversionHelper();
        helper.prepareLcMacroSteps(macroNode, PortModelEnum.flat, macro);

        LcMacro expectedMacroStep = new LcMacro((byte) 0, (byte) 0, (byte) 90, (byte) 1, (byte) 0, (byte) 60);

        // verify that setMacro was called
        Mockito.verify(macroNode, Mockito.times(1)).setMacro(Mockito.eq(expectedMacroStep));
    }

    @Test
    public void prepareLcMacroSwitchPort() throws ProtocolException {

        SwitchPort switchPort = new SwitchPort();
        switchPort.setId(1);

        Macro macro = new Macro(20);
        List<Function<? extends BidibStatus>> functions = new ArrayList<>();

        Function<?> function = new SwitchPortAction(SwitchPortStatus.ON, switchPort, 30);
        functions.add(function);

        // set the functions as last step because they are copied!
        macro.setFunctions(functions);

        MacroConversionHelper helper = new MacroConversionHelper();
        helper.prepareLcMacroSteps(macroNode, PortModelEnum.type, macro);

        LcMacro expectedMacroStep =
            new LcMacro((byte) 0, (byte) 0, (byte) 30, LcOutputType.SWITCHPORT.getType(), (byte) 1,
                SwitchPortEnum.ON.getType());

        // verify that setMacro was called
        Mockito.verify(macroNode, Mockito.times(1)).setMacro(Mockito.eq(expectedMacroStep));
    }

    @Test
    public void prepareLcMacroSwitchPortFlatModel() throws ProtocolException {

        SwitchPort switchPort = new SwitchPort();
        switchPort.setId(10);

        Macro macro = new Macro(20);
        List<Function<? extends BidibStatus>> functions = new ArrayList<>();

        Function<?> function = new SwitchPortAction(SwitchPortStatus.ON, switchPort, 30);
        functions.add(function);

        // set the functions as last step because they are copied!
        macro.setFunctions(functions);

        MacroConversionHelper helper = new MacroConversionHelper();
        helper.prepareLcMacroSteps(macroNode, PortModelEnum.flat, macro);

        LcMacro expectedMacroStep =
            new LcMacro((byte) 0, (byte) 0, (byte) 30, (byte) 10, (byte) 0, SwitchPortEnum.ON.getType());

        // verify that setMacro was called
        Mockito.verify(macroNode, Mockito.times(1)).setMacro(Mockito.eq(expectedMacroStep));
    }
}
