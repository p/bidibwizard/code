package org.bidib.wizard.highlight.sample;

// A program illustrating the use of the SyntaxHighlighter class.
// Public domain, no restrictions, Ian Holyer, University of Bristol.

import java.awt.Container;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import org.bidib.wizard.highlight.BidibScriptScanner;
import org.bidib.wizard.highlight.Scanner;
import org.bidib.wizard.highlight.SyntaxHighlighter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class HighlightEdit extends JFrame {

    private static final Logger LOGGER = LoggerFactory.getLogger(HighlightEdit.class);

    private static final long serialVersionUID = 1L;

    String filename;

    SyntaxHighlighter text;

    public static void main(String[] args) {
        String filename = null;
        if (args.length != 1) {
            // System.err.println("Usage: java HighlightEdit filename");
            // System.exit(1);
            // filename = "Scan.java";
        }
        else {
            filename = args[0];
        }
        Runner runner = new Runner(filename);
        SwingUtilities.invokeLater(runner);
    }

    static class Runner implements Runnable {
        String filename;

        Runner(String f) {
            filename = f;
        }

        public void run() {
            HighlightEdit program = new HighlightEdit();
            program.display(filename);
        }
    }

    void display(String s) {
        filename = s;
        String localStyle = UIManager.getSystemLookAndFeelClassName();
        try {
            UIManager.setLookAndFeel(localStyle);
        }
        catch (Exception e) {
        }

        setTitle("HighlightEdit " + filename);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        // uncomment the following line if you want to save the changes on exit ...
        // addWindowListener(new Closer());
        // Scanner scanner = new JavaScanner();
        // Scanner scanner = new SQLScanner();
        Scanner scanner = new BidibScriptScanner();
        text = new SyntaxHighlighter(24, 80, scanner);
        JScrollPane scroller = new JScrollPane(text);
        scroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        Container pane = getContentPane();
        pane.add(scroller);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);

        if (filename == null) {
            JFileChooser chooser = new JFileChooser();
            chooser.setCurrentDirectory(new File("./src/test/resources/org/bidib/wizard/highlight"));

            ExampleFileFilter filter = new ExampleFileFilter();
            // filter.addExtension("java");
            filter.addExtension("txt");
            // filter.addExtension("sql");
            // filter.addExtension("gif");
            filter.setDescription("BiDiB Scripts");
            // filter.setDescription("Java Images");
            // filter.setDescription("JPG & GIF Images");
            chooser.setFileFilter(filter);
            int returnVal = chooser.showOpenDialog(this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                filename = chooser.getSelectedFile().getPath();
                LOGGER.info("You chose to open this file: " + filename);

                setTitle("HighlightEdit " + filename);
            }
        }

        if (filename == null) {
            LOGGER.warn("no file to display seletected");
            JOptionPane.showMessageDialog(this, "You must select a file to run this sample!");
            System.exit(1);
        }

        try {
            text.read(new FileReader(filename), null);
        }
        catch (IOException ex) {
            LOGGER.warn(ex.getMessage());
            System.exit(1);
        }
        // Workaround for bug 4782232 in Java 1.4
        try {
            text.setCaretPosition(1);
            text.setCaretPosition(0);
        }
        catch (Exception ex) {
            LOGGER.warn(ex.getMessage());
        }
    }

    class Closer extends WindowAdapter {
        public void windowClosing(WindowEvent e) {
            try {
                if (text != null && filename != null) {
                    text.write(new FileWriter(filename));
                }
            }
            catch (IOException err) {
                LOGGER.warn(err.getMessage());
                System.exit(1);
            }
            System.exit(0);
        }
    }
}
