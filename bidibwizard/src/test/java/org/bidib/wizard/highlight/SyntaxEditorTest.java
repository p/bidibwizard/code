package org.bidib.wizard.highlight;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * SyntaxEditorTest
 */
public class SyntaxEditorTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(SyntaxEditorTest.class);

    private static SyntaxHighlighter txtSQLQuery = null;

    private void showFrame() {

        JPanel base = new JPanel(new BorderLayout());

        Scanner scanner = new SQLScanner();
        txtSQLQuery = new SyntaxHighlighter(10, 80, scanner);

        JScrollPane scroller = new JScrollPane(txtSQLQuery);

        JButton toggleHighLighting = new JButton("Toggle highlight");
        toggleHighLighting.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                txtSQLQuery.setStopHighlighting(!txtSQLQuery.isStopHighlighting());

                LOGGER.info("Toggled stop highlighting: {}", txtSQLQuery.isStopHighlighting());
            }

        });
        base.add(toggleHighLighting, BorderLayout.NORTH);
        base.add(scroller, BorderLayout.CENTER);

        JFrame f = new JFrame(getClass().getName());
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.getContentPane().add(base);
        f.setSize(500, 500);
        f.setVisible(true);
    }

    /**
     * Test the frame
     * 
     * @param args
     *            not used
     */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new SyntaxEditorTest().showFrame();
            }
        });
    }

}
