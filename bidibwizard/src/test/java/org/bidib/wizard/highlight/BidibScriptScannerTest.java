package org.bidib.wizard.highlight;

// Illustrate the use of the scanner by reading in a file and displaying its
// tokens. Public domain, no restrictions, Ian Holyer, University of Bristol.

import java.io.FileReader;
import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * ScannerTest
 */
public class BidibScriptScannerTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(BidibScriptScannerTest.class);

    @Test
    public void scanBidibScript() throws IOException {
        String filename = getClass().getResource("bidib-script-test.txt").getPath();
        char[] buffer = IOUtils.toCharArray(new FileReader(filename));

        int lineSeparatorLen = System.lineSeparator().length();

        Token[] expectedTokens = new Token[11];
        expectedTokens[0] =
            new Token(new Symbol(TokenTypes.COMMENT, "//# this is a test script" + System.lineSeparator()), 0);
        expectedTokens[1] = new Token(new Symbol(TokenTypes.KEYWORD, "define"), 25 + lineSeparatorLen);
        expectedTokens[2] = new Token(new Symbol(TokenTypes.IDENTIFIER, "porta"), 32 + lineSeparatorLen);
        expectedTokens[3] = new Token(new Symbol(TokenTypes.STRING, "\"This is port A\""), 38 + lineSeparatorLen); // line
                                                                                                                   // is
                                                                                                                   // terminated
                                                                                                                   // with
                                                                                                                   // a
                                                                                                                   // line
                                                                                                                   // break
        // empty line is terminated with a line break
        expectedTokens[4] = new Token(new Symbol(TokenTypes.KEYWORD, "set"), 54 + 3 * lineSeparatorLen);
        expectedTokens[5] = new Token(new Symbol(TokenTypes.KEYWORD2, "input"), 58 + 3 * lineSeparatorLen);
        expectedTokens[6] = new Token(new Symbol(TokenTypes.NUMBER, "1"), 64 + 3 * lineSeparatorLen);
        expectedTokens[7] = new Token(new Symbol(TokenTypes.IDENTIFIER, "text"), 66 + 3 * lineSeparatorLen);
        expectedTokens[8] = new Token(new Symbol(TokenTypes.OPERATOR, "="), 70 + 3 * lineSeparatorLen);
        expectedTokens[9] = new Token(new Symbol(TokenTypes.VARIABLE, "%porta%"), 71 + 3 * lineSeparatorLen); // line is
                                                                                                              // terminated
                                                                                                              // with a
                                                                                                              // line
                                                                                                              // break
        expectedTokens[10] = new Token(new Symbol(TokenTypes.WHITESPACE, ""), 78 + 4 * lineSeparatorLen);

        Scanner scanner = new BidibScriptScanner();
        scanner.change(0, 0, buffer.length);
        scanner.scan(buffer, 0, buffer.length);
        LOGGER.info("Total number of tokens: {}", scanner.size());

        for (int i = 0; i < scanner.size(); i++) {
            Token t = scanner.getToken(i);
            StringBuilder sb = new StringBuilder();
            sb.append(t.position).append(": '").append(t.symbol.name).append("' ")
                .append(TokenTypes.TYPENAMES[t.symbol.type]);
            LOGGER.info(sb.toString());

            Assert.assertEquals(t.position, expectedTokens[i].position);
            Assert.assertEquals(t.symbol.name, expectedTokens[i].symbol.name);
            Assert.assertEquals(t.symbol.type, expectedTokens[i].symbol.type);
        }
    }
}
