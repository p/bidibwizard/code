package org.bidib.wizard.highlight;

// Illustrate the use of the scanner by reading in a file and displaying its
// tokens. Public domain, no restrictions, Ian Holyer, University of Bristol.

import java.io.FileReader;
import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * ScannerTest
 */
public class ScannerTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ScannerTest.class);

    private int lineSeparatorLen = System.lineSeparator().length();

    @Test
    public void scanText() throws IOException {
        String filename = getClass().getResource("text-test.txt").getPath();
        char[] buffer = IOUtils.toCharArray(new FileReader(filename));

        LOGGER.info("Len of lineSeparator: {}", lineSeparatorLen);

        Token[] expectedTokens = new Token[18];
        expectedTokens[0] = new Token(new Symbol(TokenTypes.WORD, "This"), 0);
        expectedTokens[1] = new Token(new Symbol(TokenTypes.UNRECOGNIZED, " "), 4);
        expectedTokens[2] = new Token(new Symbol(TokenTypes.WORD, "is"), 5);
        expectedTokens[3] = new Token(new Symbol(TokenTypes.UNRECOGNIZED, " "), 7);
        expectedTokens[4] = new Token(new Symbol(TokenTypes.WORD, "a"), 8);
        expectedTokens[5] = new Token(new Symbol(TokenTypes.UNRECOGNIZED, " "), 9);
        expectedTokens[6] = new Token(new Symbol(TokenTypes.WORD, "test"), 10);
        expectedTokens[7] = new Token(new Symbol(TokenTypes.PUNCTUATION, "."), 14); // line is terminated with a line
                                                                                    // break
        expectedTokens[8] = new Token(new Symbol(TokenTypes.UNRECOGNIZED, "\n"), 14 + lineSeparatorLen);
        expectedTokens[9] = new Token(new Symbol(TokenTypes.WORD, "And"), 15 + lineSeparatorLen);
        expectedTokens[10] = new Token(new Symbol(TokenTypes.UNRECOGNIZED, " "), 18 + lineSeparatorLen);
        expectedTokens[11] = new Token(new Symbol(TokenTypes.WORD, "a"), 19 + lineSeparatorLen);
        expectedTokens[12] = new Token(new Symbol(TokenTypes.UNRECOGNIZED, " "), 20 + lineSeparatorLen);
        expectedTokens[13] = new Token(new Symbol(TokenTypes.WORD, "new"), 21 + lineSeparatorLen);
        expectedTokens[14] = new Token(new Symbol(TokenTypes.UNRECOGNIZED, " "), 24 + lineSeparatorLen);
        expectedTokens[15] = new Token(new Symbol(TokenTypes.WORD, "line"), 25 + lineSeparatorLen);
        expectedTokens[16] = new Token(new Symbol(TokenTypes.PUNCTUATION, "."), 29 + lineSeparatorLen);
        expectedTokens[17] = new Token(new Symbol(TokenTypes.WHITESPACE, ""), 30 + lineSeparatorLen);

        Scanner scanner = new TextScanner();
        scanner.change(0, 0, buffer.length);
        scanner.scan(buffer, 0, buffer.length);

        for (int i = 0; i < scanner.size(); i++) {
            Token t = scanner.getToken(i);
            StringBuilder sb = new StringBuilder();
            sb.append(t.position).append(": '").append(t.symbol.name).append("' ")
                .append(TokenTypes.TYPENAMES[t.symbol.type]);
            LOGGER.info(sb.toString());

            Assert.assertEquals(t.position, expectedTokens[i].position);
            Assert.assertEquals(t.symbol.name, expectedTokens[i].symbol.name);
            Assert.assertEquals(t.symbol.type, expectedTokens[i].symbol.type);
        }
    }

    @Test
    public void scanSql() throws IOException {
        String filename = getClass().getResource("sql-test.txt").getPath();
        char[] buffer = IOUtils.toCharArray(new FileReader(filename));

        Token[] expectedTokens = new Token[9];
        expectedTokens[0] = new Token(new Symbol(TokenTypes.KEYWORD, "select"), 0);
        expectedTokens[1] = new Token(new Symbol(TokenTypes.IDENTIFIER, "*"), 7);
        expectedTokens[2] = new Token(new Symbol(TokenTypes.KEYWORD, "from"), 9);
        expectedTokens[3] = new Token(new Symbol(TokenTypes.IDENTIFIER, "table"), 14); // line is terminated with a line
                                                                                       // break
        expectedTokens[4] = new Token(new Symbol(TokenTypes.KEYWORD, "where"), 19 + lineSeparatorLen);
        expectedTokens[5] = new Token(new Symbol(TokenTypes.IDENTIFIER, "a"), 25 + lineSeparatorLen);
        expectedTokens[6] = new Token(new Symbol(TokenTypes.OPERATOR, "="), 26 + lineSeparatorLen);
        expectedTokens[7] = new Token(new Symbol(TokenTypes.IDENTIFIER, "b"), 27 + lineSeparatorLen);
        expectedTokens[8] = new Token(new Symbol(TokenTypes.WHITESPACE, ""), 28 + lineSeparatorLen);

        Scanner scanner = new SQLScanner();
        scanner.change(0, 0, buffer.length);
        scanner.scan(buffer, 0, buffer.length);

        for (int i = 0; i < scanner.size(); i++) {
            Token t = scanner.getToken(i);
            StringBuilder sb = new StringBuilder();
            sb.append(t.position).append(": '").append(t.symbol.name).append("' ")
                .append(TokenTypes.TYPENAMES[t.symbol.type]);
            LOGGER.info(sb.toString());

            Assert.assertEquals(t.position, expectedTokens[i].position);
            Assert.assertEquals(t.symbol.name, expectedTokens[i].symbol.name);
            Assert.assertEquals(t.symbol.type, expectedTokens[i].symbol.type);
        }
    }

    @Test
    public void scanJava() throws IOException {
        String filename = getClass().getResource("java-test.txt").getPath();
        char[] buffer = IOUtils.toCharArray(new FileReader(filename));

        Token[] expectedTokens = new Token[2];
        expectedTokens[0] =
            new Token(new Symbol(TokenTypes.COMMENT, "// This is a comment line" + System.lineSeparator()), 0);
        expectedTokens[1] = new Token(new Symbol(TokenTypes.WHITESPACE, ""), 25 + lineSeparatorLen);

        Scanner scanner = new JavaScanner();
        scanner.change(0, 0, buffer.length);
        scanner.scan(buffer, 0, buffer.length);

        for (int i = 0; i < scanner.size(); i++) {
            Token t = scanner.getToken(i);
            StringBuilder sb = new StringBuilder();
            sb.append(t.position).append(": '").append(t.symbol.name).append("' ")
                .append(TokenTypes.TYPENAMES[t.symbol.type]);
            LOGGER.debug(sb.toString());

            Assert.assertEquals(t.position, expectedTokens[i].position);
            Assert.assertEquals(t.symbol.name, expectedTokens[i].symbol.name);
            Assert.assertEquals(t.symbol.type, expectedTokens[i].symbol.type);
        }
    }
}
