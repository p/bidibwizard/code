package org.bidib.wizard.utils;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.bidib.jbidibc.core.Feature;
import org.bidib.wizard.comm.Communication;
import org.bidib.wizard.labels.Labels;
import org.bidib.wizard.main.DefaultApplicationContext;
import org.bidib.wizard.mvc.main.model.AnalogPort;
import org.bidib.wizard.mvc.main.model.InputPort;
import org.bidib.wizard.mvc.main.model.MainModel;
import org.bidib.wizard.mvc.main.model.Node;
import org.bidib.wizard.mvc.main.model.NodeState;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class NodeStateUtilsTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(NodeStateUtilsTest.class);

    private Communication communication;

    @BeforeTest
    public void prepare() {
        communication = Mockito.mock(Communication.class);
    }

    @Test
    public void prepareNodeStateTest() {
        LOGGER.info("Prepare the node state.");

        final MainModel model = Mockito.mock(MainModel.class);

        final org.bidib.jbidibc.core.Node coreNode = Mockito.mock(org.bidib.jbidibc.core.Node.class);
        final Node node = Mockito.mock(Node.class);

        Mockito.when(node.getNode()).thenReturn(coreNode);
        Mockito.when(coreNode.getPortFlatModel()).thenReturn(40);

        boolean discardCache = true;
        List<Feature> features = new LinkedList<>();
        Mockito.when(communication.getFeatures(coreNode, discardCache)).thenReturn(features);

        Boolean checkLoadMacroContent = Boolean.FALSE;
        Boolean checkLoadFeatures = Boolean.FALSE;
        Boolean checkLoadCVs = Boolean.FALSE;
        NodeState nodeState =
            NodeStateUtils.prepareNodeState(model, communication, node, checkLoadMacroContent, checkLoadFeatures,
                checkLoadCVs);

        Assert.assertNotNull(nodeState);
    }

    @Test(enabled = false)
    public void loadNodeStateTest() {
        LOGGER.info("Load the node state.");

        // TODO this test does not work with the new packages

        String fileName = NodeStateUtilsTest.class.getResource("/nodes/Test_1_2_li_A.xml").getPath();

        final Map<String, Object> importParams = new HashMap<>();

        final Boolean checkRestoreNodeString = false;
        final Boolean checkRestoreMacroContent = false;
        final Boolean checkRestoreFeatures = false;
        final Boolean checkRestoreCVs = false;

        DefaultApplicationContext applicationContext = new DefaultApplicationContext();

        Labels analogPortLabels = null;
        Labels feedbackPortLabels = null;
        Labels inputPortLabels = null;
        Labels lightPortLabels = null;
        Labels backlightPortLabels = null;
        Labels motorPortLabels = null;
        Labels servoPortLabels = null;
        Labels soundPortLabels = null;
        Labels switchPortLabels = null;
        Labels nodeLabels = null;
        Labels macroLabels = null;
        Labels accessoryLabels = null;

        applicationContext.register(DefaultApplicationContext.KEY_ANALOGPORT_LABELS, analogPortLabels);
        applicationContext.register(DefaultApplicationContext.KEY_BACKLIGHTPORT_LABELS, backlightPortLabels);
        applicationContext.register(DefaultApplicationContext.KEY_FEEDBACKPORT_LABELS, feedbackPortLabels);
        applicationContext.register(DefaultApplicationContext.KEY_INPUTPORT_LABELS, inputPortLabels);
        applicationContext.register(DefaultApplicationContext.KEY_LIGHTPORT_LABELS, lightPortLabels);
        applicationContext.register(DefaultApplicationContext.KEY_MOTORPORT_LABELS, motorPortLabels);
        applicationContext.register(DefaultApplicationContext.KEY_SERVOPORT_LABELS, servoPortLabels);
        applicationContext.register(DefaultApplicationContext.KEY_SOUNDPORT_LABELS, soundPortLabels);
        applicationContext.register(DefaultApplicationContext.KEY_SWITCHPORT_LABELS, switchPortLabels);

        applicationContext.register(DefaultApplicationContext.KEY_NODE_LABELS, nodeLabels);
        applicationContext.register(DefaultApplicationContext.KEY_MACRO_LABELS, macroLabels);
        applicationContext.register(DefaultApplicationContext.KEY_ACCESSORY_LABELS, accessoryLabels);

        final MainModel model = Mockito.mock(MainModel.class);

        final org.bidib.jbidibc.core.Node coreNode = Mockito.mock(org.bidib.jbidibc.core.Node.class);
        final Node node = Mockito.mock(Node.class);

        Mockito.when(model.getSelectedNode()).thenReturn(node);

        Mockito.when(node.getNode()).thenReturn(coreNode);
        Mockito.when(coreNode.getPortFlatModel()).thenReturn(40);
        Mockito.when(coreNode.isPortFlatModelAvailable()).thenReturn(Boolean.TRUE);

        List<AnalogPort> analogPorts = new LinkedList<>();
        Mockito.when(model.getAnalogPorts()).thenReturn(analogPorts);

        List<InputPort> inputPorts = new LinkedList<>();

        inputPorts.add(new InputPort.Builder(8).setLabel("Input_8").build());
        Mockito.when(model.getInputPorts()).thenReturn(inputPorts);

        boolean discardCache = true;
        List<Feature> features = new LinkedList<>();
        Mockito.when(communication.getFeatures(coreNode, discardCache)).thenReturn(features);

        // TODO
        NodeStateUtils.loadNodeState(null, fileName, model, node, importParams, applicationContext,
            checkRestoreNodeString, checkRestoreMacroContent, checkRestoreFeatures, checkRestoreCVs);

        Assert.assertNotNull(importParams.get("NodeState_sourceIsFlatModel"));
        Assert.assertEquals(importParams.get("NodeState_sourceIsFlatModel"), Boolean.TRUE);
    }

    @Test(enabled = false)
    public void loadNodeStateFromTypedBackupTest() {
        LOGGER.info("Load the node state.");

        // TODO this test does not work with the new packages

        String fileName = NodeStateUtilsTest.class.getResource("/nodes/V_0D_P_75000AEB.xml").getPath();

        final Map<String, Object> importParams = new HashMap<>();

        final Boolean checkRestoreNodeString = false;
        final Boolean checkRestoreMacroContent = false;
        final Boolean checkRestoreFeatures = false;
        final Boolean checkRestoreCVs = false;

        DefaultApplicationContext applicationContext = new DefaultApplicationContext();

        Labels analogPortLabels = null;
        Labels feedbackPortLabels = null;
        Labels inputPortLabels = new Labels();
        Labels lightPortLabels = null;
        Labels backlightPortLabels = null;
        Labels motorPortLabels = null;
        Labels servoPortLabels = null;
        Labels soundPortLabels = null;
        Labels switchPortLabels = null;
        Labels nodeLabels = null;
        Labels macroLabels = null;
        Labels accessoryLabels = null;

        applicationContext.register(DefaultApplicationContext.KEY_ANALOGPORT_LABELS, analogPortLabels);
        applicationContext.register(DefaultApplicationContext.KEY_BACKLIGHTPORT_LABELS, backlightPortLabels);
        applicationContext.register(DefaultApplicationContext.KEY_FEEDBACKPORT_LABELS, feedbackPortLabels);
        applicationContext.register(DefaultApplicationContext.KEY_INPUTPORT_LABELS, inputPortLabels);
        applicationContext.register(DefaultApplicationContext.KEY_LIGHTPORT_LABELS, lightPortLabels);
        applicationContext.register(DefaultApplicationContext.KEY_MOTORPORT_LABELS, motorPortLabels);
        applicationContext.register(DefaultApplicationContext.KEY_SERVOPORT_LABELS, servoPortLabels);
        applicationContext.register(DefaultApplicationContext.KEY_SOUNDPORT_LABELS, soundPortLabels);
        applicationContext.register(DefaultApplicationContext.KEY_SWITCHPORT_LABELS, switchPortLabels);

        applicationContext.register(DefaultApplicationContext.KEY_NODE_LABELS, nodeLabels);
        applicationContext.register(DefaultApplicationContext.KEY_MACRO_LABELS, macroLabels);
        applicationContext.register(DefaultApplicationContext.KEY_ACCESSORY_LABELS, accessoryLabels);

        final MainModel model = Mockito.mock(MainModel.class);

        final org.bidib.jbidibc.core.Node coreNode = Mockito.mock(org.bidib.jbidibc.core.Node.class);
        final Node node = Mockito.mock(Node.class);

        Mockito.when(model.getSelectedNode()).thenReturn(node);

        Mockito.when(node.getNode()).thenReturn(coreNode);
        Mockito.when(coreNode.getPortFlatModel()).thenReturn(40);
        Mockito.when(coreNode.isPortFlatModelAvailable()).thenReturn(Boolean.TRUE);

        List<AnalogPort> analogPorts = new LinkedList<>();
        Mockito.when(model.getAnalogPorts()).thenReturn(analogPorts);

        List<InputPort> inputPorts = new LinkedList<>();

        inputPorts.add(new InputPort.Builder(8).setLabel("Input_8").build());
        Mockito.when(model.getInputPorts()).thenReturn(inputPorts);

        boolean discardCache = true;
        List<Feature> features = new LinkedList<>();
        Mockito.when(communication.getFeatures(coreNode, discardCache)).thenReturn(features);

        // TODO
        NodeStateUtils.loadNodeState(null, fileName, model, node, importParams, applicationContext,
            checkRestoreNodeString, checkRestoreMacroContent, checkRestoreFeatures, checkRestoreCVs);

        Assert.assertNotNull(importParams.get("NodeState_sourceIsFlatModel"));
        Assert.assertEquals(importParams.get("NodeState_sourceIsFlatModel"), Boolean.FALSE);
    }
}
