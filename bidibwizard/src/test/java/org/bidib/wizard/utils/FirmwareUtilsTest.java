package org.bidib.wizard.utils;

import org.testng.Assert;
import org.testng.annotations.Test;

public class FirmwareUtilsTest {

    private static final String FILENAME_NO_VERSION = "BiDiBCV-13-120.xml";

    private static final String FILENAME_MAJOR_VERSION = "BiDiBCV-13-120-1.xml";

    private static final String FILENAME_MAJOR_AND_MINOR_VERSION = "BiDiBCV-13-120-1.01.xml";

    private static final String FILENAME_FULL_VERSION = "BiDiBCV-13-120-1.02.03.xml";

    @Test
    public void hasVersionInFilename() {

        Assert.assertFalse(FirmwareUtils.hasVersionInFilename(FILENAME_NO_VERSION));

        Assert.assertTrue(FirmwareUtils.hasVersionInFilename(FILENAME_MAJOR_VERSION));

        Assert.assertTrue(FirmwareUtils.hasVersionInFilename(FILENAME_MAJOR_AND_MINOR_VERSION));

        Assert.assertTrue(FirmwareUtils.hasVersionInFilename(FILENAME_FULL_VERSION));
    }
}
