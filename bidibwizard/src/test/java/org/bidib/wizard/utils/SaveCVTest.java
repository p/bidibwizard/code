package org.bidib.wizard.utils;

import java.io.FileNotFoundException;

import org.bidib.cvexchange.SaveCV;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SaveCVTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(SaveCVTest.class);

    @Test
    public void loadSaveCV() throws FileNotFoundException {

        SaveCV saveCVType = CvExchangeFactory.loadSaveCV("/savecv/save-cv-test.xml");
        LOGGER.info("Loaded saveCVType: {}", saveCVType);

        Assert.assertNotNull(saveCVType);
    }
}
