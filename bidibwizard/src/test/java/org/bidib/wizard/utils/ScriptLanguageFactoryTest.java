package org.bidib.wizard.utils;

import java.io.FileNotFoundException;

import org.bidib.script.BiDiBScript;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ScriptLanguageFactoryTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(ScriptLanguageFactoryTest.class);

    @Test
    public void loadBiDiBScript() throws FileNotFoundException {

        BiDiBScript biDiBScript = ScriptLanguageFactory.loadBiDiBScript("/script/ScriptLanguage.bidib");
        LOGGER.info("Loaded biDiBScript: {}", biDiBScript);

        Assert.assertNotNull(biDiBScript);
    }
}
