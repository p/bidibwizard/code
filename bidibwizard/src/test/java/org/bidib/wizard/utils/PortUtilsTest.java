package org.bidib.wizard.utils;

import org.testng.Assert;
import org.testng.annotations.Test;

public class PortUtilsTest {

    @Test
    public void calculateDimmDuration() {
        // delta = 1
        Assert.assertEquals(PortUtils.calculateDimmDuration(0, 255, 1), 652800);

        // delta = 4
        Assert.assertEquals(PortUtils.calculateDimmDuration(0, 255, 4), 163200);

        // delta = 255
        Assert.assertEquals(PortUtils.calculateDimmDuration(0, 255, 255), 2560);
        Assert.assertEquals(PortUtils.calculateDimmDuration(255, 0, 255), 2560);

        // delta = 1
        Assert.assertEquals(PortUtils.calculateDimmDuration(255, 1), 652800);
        Assert.assertEquals(PortUtils.calculateDimmDuration(255, 65535), 9);
        Assert.assertEquals(PortUtils.calculateDimmDuration(255, 65500), 9);

        Assert.assertEquals(PortUtils.calculateDimmDuration(255, 64500), 10);
        Assert.assertEquals(PortUtils.calculateDimmDuration(120, 64500), 4);

        Assert.assertEquals(PortUtils.calculateDimmDuration(255, 40), 16320);
        Assert.assertEquals(PortUtils.calculateDimmDuration(120, 40), 7680);
    }

    @Test
    public void calculateDelayTicks() {
        // timeDelta = 1s, stretch = 1
        Assert.assertEquals(PortUtils.calculateDelayTicks(1000, 1), 50);

        // timeDelta = 2s, stretch = 1
        Assert.assertEquals(PortUtils.calculateDelayTicks(2000, 1), 100);

        // timeDelta = 2s, stretch = 4
        Assert.assertEquals(PortUtils.calculateDelayTicks(2000, 4), 25);

        // timeDelta = 20s, stretch = 255
        Assert.assertEquals(PortUtils.calculateDelayTicks(20 * 1000, 255), 3);

        // timeDelta = 1s, stretch = 50
        Assert.assertEquals(PortUtils.calculateDelayTicks(1000, 50), 1);
    }
}
