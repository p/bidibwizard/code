package org.bidib.wizard.utils;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.text.StrSubstitutor;
import org.bidib.schema.nodescriptsources.ApplicationCategoryType;
import org.bidib.schema.nodescriptsources.CategoryType;
import org.bidib.schema.nodescriptsources.NodeScriptSourceType;
import org.bidib.schema.nodescriptsources.NodeScriptSources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class NodeScriptSourcesFactoryTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(NodeScriptSourcesFactoryTest.class);

    @Test
    public void loadSingleNodeScriptSources() throws FileNotFoundException, URISyntaxException {

        String filePath = "classpath:nodescript/nodeScriptSources.xml";
        LOGGER.info("Load nodeScriptSources from path: {}", filePath);

        NodeScriptSources nodeScriptSources = NodeScriptSourcesFactory.loadNodeScriptSources(filePath);
        Assert.assertNotNull(nodeScriptSources);

        Assert.assertNotNull(nodeScriptSources);

        LOGGER.info("Loaded nodeScriptSources: {}", nodeScriptSources);

        Assert.assertNotNull(nodeScriptSources.getNodeScriptSource());
        Assert.assertEquals(nodeScriptSources.getNodeScriptSource().size(), 1);

        NodeScriptSourceType nodeScriptSource = nodeScriptSources.getNodeScriptSource().get(0);
        Assert.assertNotNull(nodeScriptSource);
        Assert.assertEquals(nodeScriptSource.getLocation(), "${bidib.dir}/data/nodescripts");

        Assert.assertNotNull(nodeScriptSource.getCategory());
        Assert.assertEquals(nodeScriptSource.getCategory().size(), 2);

        CategoryType category = nodeScriptSource.getCategory().get(0);
        Assert.assertNotNull(category);
        Assert.assertEquals(category.getApplicationCategory(), ApplicationCategoryType.SIGNAL);
        Assert.assertEquals(category.getSubDirectory(), "Signals");

        category = nodeScriptSource.getCategory().get(1);
        Assert.assertNotNull(category);
        Assert.assertEquals(category.getApplicationCategory(), ApplicationCategoryType.TURNOUT);
        Assert.assertEquals(category.getSubDirectory(), "turnout");
    }

    @Test
    public void replacePlaceholdersTest() {

        Map<String, String> valuesMap = new HashMap<>();
        valuesMap.put("bidib.dir", "C:/users/akuhtz/.bidib");

        String templateString = "${bidib.dir}/data/nodescripts";
        StrSubstitutor sub = new StrSubstitutor(valuesMap);
        String resolvedString = sub.replace(templateString);

        Assert.assertEquals(resolvedString, "C:/users/akuhtz/.bidib/data/nodescripts");
    }
}
