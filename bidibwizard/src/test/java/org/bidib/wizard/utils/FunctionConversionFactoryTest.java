package org.bidib.wizard.utils;

import org.bidib.jbidibc.core.schema.bidib2.FunctionAccessoryNotification;
import org.bidib.jbidibc.core.schema.bidib2.FunctionInput;
import org.bidib.jbidibc.core.schema.bidib2.MacroPoint;
import org.bidib.jbidibc.core.schema.bidib2.MacroPointAccessoryNotification;
import org.bidib.jbidibc.core.schema.bidib2.MacroPointInput;
import org.bidib.jbidibc.core.schema.bidib2.MacroPointOutputSwitch;
import org.bidib.wizard.comm.AccessoryOkayStatus;
import org.bidib.wizard.comm.InputStatus;
import org.bidib.wizard.mvc.main.model.InputPort;
import org.bidib.wizard.mvc.main.model.SwitchPort;
import org.bidib.wizard.mvc.main.model.function.AccessoryOkayFunction;
import org.bidib.wizard.mvc.main.model.function.InputFunction;
import org.bidib.wizard.mvc.main.model.function.SwitchPortAction;
import org.testng.Assert;
import org.testng.annotations.Test;

public class FunctionConversionFactoryTest {

    private FunctionConversionFactory factory = new FunctionConversionFactory();

    @Test
    public void convertSwitchPortAction() {
        SwitchPort port = new SwitchPort();
        port.setId(1);

        SwitchPortAction action = new SwitchPortAction(port, 50);

        MacroPoint macroPoint = factory.convert(action);
        Assert.assertNotNull(macroPoint);
        Assert.assertTrue(macroPoint instanceof MacroPointOutputSwitch);

        MacroPointOutputSwitch outputSwitch = (MacroPointOutputSwitch) macroPoint;
        Assert.assertEquals(outputSwitch.getOutputNumber(), 1);
        Assert.assertEquals(outputSwitch.getDelay(), 50);
    }

    @Test
    public void convertSwitchPortAction2() {
        SwitchPort port = new SwitchPort();
        port.setId(1);

        SwitchPortAction action = new SwitchPortAction(port, 50);

        MacroPoint macroPoint = factory.convertSwitchPortAction(action);
        Assert.assertNotNull(macroPoint);
        Assert.assertTrue(macroPoint instanceof MacroPointOutputSwitch);

        MacroPointOutputSwitch outputSwitch = (MacroPointOutputSwitch) macroPoint;
        Assert.assertEquals(outputSwitch.getOutputNumber(), 1);
        Assert.assertEquals(outputSwitch.getDelay(), 50);
    }

    @Test
    public void convertInputPortAction() {
        InputPort port = new InputPort();
        port.setId(1);

        InputFunction action = new InputFunction(InputStatus.QUERY1, port);

        MacroPoint macroPoint = factory.convertInputAction(action);
        Assert.assertNotNull(macroPoint);
        Assert.assertTrue(macroPoint instanceof MacroPointInput);

        MacroPointInput input = (MacroPointInput) macroPoint;
        Assert.assertEquals(input.getInputNumber(), 1);
        Assert.assertEquals(input.getFunction(), FunctionInput.WAIT_FOR_1);
    }

    @Test
    public void convertAccessoryNotifyOkayAction() {
        AccessoryOkayFunction action = new AccessoryOkayFunction(AccessoryOkayStatus.NO_FEEDBACK);

        MacroPoint macroPoint = factory.convert(action);
        Assert.assertNotNull(macroPoint);
        Assert.assertTrue(macroPoint instanceof MacroPointAccessoryNotification);
        MacroPointAccessoryNotification accessoryNotification = (MacroPointAccessoryNotification) macroPoint;
        Assert.assertNotNull(accessoryNotification.getFunction());
        Assert.assertEquals(accessoryNotification.getFunction(), FunctionAccessoryNotification.OKAY);
    }

    @Test
    public void convertAccessoryNotifyQuery0Action() {
        InputPort port = new InputPort();
        port.setId(1);
        AccessoryOkayFunction action = new AccessoryOkayFunction(AccessoryOkayStatus.QUERY0, port);

        MacroPoint macroPoint = factory.convert(action);
        Assert.assertNotNull(macroPoint);
        Assert.assertTrue(macroPoint instanceof MacroPointAccessoryNotification);
        MacroPointAccessoryNotification accessoryNotification = (MacroPointAccessoryNotification) macroPoint;
        Assert.assertNotNull(accessoryNotification.getFunction());
        Assert.assertEquals(accessoryNotification.getFunction(), FunctionAccessoryNotification.OKAY_IF_INPUT_0);

        Assert.assertNotNull(accessoryNotification.getInputNumber());
        Assert.assertEquals(accessoryNotification.getInputNumber(), Integer.valueOf(1));
    }

    @Test
    public void convertAccessoryNotifyQuery1Action() {
        InputPort port = new InputPort();
        port.setId(1);
        AccessoryOkayFunction action = new AccessoryOkayFunction(AccessoryOkayStatus.QUERY1, port);

        MacroPoint macroPoint = factory.convert(action);
        Assert.assertNotNull(macroPoint);
        Assert.assertTrue(macroPoint instanceof MacroPointAccessoryNotification);
        MacroPointAccessoryNotification accessoryNotification = (MacroPointAccessoryNotification) macroPoint;
        Assert.assertNotNull(accessoryNotification.getFunction());
        Assert.assertEquals(accessoryNotification.getFunction(), FunctionAccessoryNotification.OKAY_IF_INPUT_1);

        Assert.assertNotNull(accessoryNotification.getInputNumber());
        Assert.assertEquals(accessoryNotification.getInputNumber(), Integer.valueOf(1));
    }
}
